﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;
using LendersOffice.Common.TextImport;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Admin;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Xml;
using System.Web;
using LendersOffice.ObjLib.MortgagePool;
using System.Text.RegularExpressions;
using LendersOffice.ObjLib.DocMagicLib;
using DataAccess.Trust;
using CommonProjectLib.Common.Lib;

namespace LendersOffice.ObjLib.Conversions.ProvidentFunding
{
    public class TaxScheduleItem
    {
        private string m_dueD_rep;
        private decimal m_amtDue;
        private int m_pmtNum;

        public TaxScheduleItem(string dueD_rep, decimal amtDue, int pmtNum)
        {
            m_dueD_rep = dueD_rep;
            m_amtDue = amtDue;
            m_pmtNum = pmtNum;
        }

        public string DueD_rep
        {
            get { return m_dueD_rep; }
        }

        public decimal AmtDue
        {
            get { return m_amtDue; }
        }

        public int PmtNum
        {
            get { return m_pmtNum; }
        }
    }

    public class ProvidentFundingExport
    {
        #region member variable declarations
        private Dictionary<string, string> m_countyIdMap = null;
        private Dictionary<Guid, string> m_loanProgamIdMap = null;
        private List<Guid>  m_loanIds            = null;
        private DataTable m_loanSaleFileTable    = null;
        private DataTable m_armLoanTable         = null;
        private DataTable m_borrowerTable        = null;
        private DataTable m_borrowerMailingTable = null;
        private DataTable m_contactEmailTable    = null;
        private DataTable m_contactPhoneTable    = null;
        private DataTable m_floodCertTable       = null;
        private DataTable m_insPolicyTable       = null;
        private DataTable m_loanTable            = null;
        private DataTable m_propertyTable        = null;
        private DataTable m_taxItemTable         = null;
        private DataTable m_taxScheduleTable     = null;
        private DataTable m_governmentLoanTable  = null;
        private DataTable m_poolTable            = null;
        
        private string m_rateFormat = "#0.00000";
        private string m_dollarFormat = "##############0.00";

        #endregion

        public ProvidentFundingExport(List<Guid> loanIds)
        {
            m_loanIds = loanIds;
            CreateLoanSaleFileTable();
            CreateZipTables();
        }

        public string GetLoanSaleFileCsv()
        {
            ExportLoanSaleFile();
            return m_loanSaleFileTable.ToCSVWithHeader();
        }

        #region zip helpers
        private static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        private void AddFileToZip(ZipOutputStream zipoutput, DataTable table, string fileName)
        {
            ASCIIEncoding ascii = new ASCIIEncoding();
            byte[] data = ascii.GetBytes(table.ToCSVWithHeader());
            ZipEntry e = new ZipEntry(fileName);
            e.IsUnicodeText = false;
            e.Size = data.Length;

            zipoutput.PutNextEntry(e);
            zipoutput.Write(data, 0, (int)data.Length);
        }

        #endregion

        public void WriteToZip(Stream outputStream)
        {
            LoadDictionaries();
            ExportZipFiles(); // Fill all of the tables
            ZipOutputStream zipoutput = new ZipOutputStream(outputStream);

            AddFileToZip(zipoutput, m_armLoanTable, "arm_loan.csv");
            AddFileToZip(zipoutput, m_borrowerTable, "borrower.csv");
            AddFileToZip(zipoutput, m_borrowerMailingTable, "borrower_mailing.csv");
            AddFileToZip(zipoutput, m_contactEmailTable, "contact_email.csv");
            AddFileToZip(zipoutput, m_contactPhoneTable, "contact_phone.csv");
            AddFileToZip(zipoutput, m_floodCertTable, "flood_cert.csv");
            AddFileToZip(zipoutput, m_insPolicyTable, "ins_policy.csv");
            AddFileToZip(zipoutput, m_loanTable, "loan.csv");
            AddFileToZip(zipoutput, m_propertyTable, "property.csv");
            AddFileToZip(zipoutput, m_taxItemTable, "tax_item.csv");
            AddFileToZip(zipoutput, m_taxScheduleTable, "tax_schedule.csv");
            AddFileToZip(zipoutput, m_governmentLoanTable, "government_loan.csv");
            AddFileToZip(zipoutput, m_poolTable, "pool.csv");

            zipoutput.Finish();
            zipoutput.Close();

        }

        private void LoadDictionaries()
        {
            m_countyIdMap     = new Dictionary<string, string>(StringComparer.CurrentCultureIgnoreCase);
            m_loanProgamIdMap = new Dictionary<Guid, string>();

            string xmlPath = "";
            
            xmlPath = Tools.GetServerMapPath("~/los/Reports/ProvidentFundingMappings.xml.config");

            XmlDocument doc = new XmlDocument();

            doc.Load(xmlPath);

            XmlElement root = (XmlElement)doc.SelectSingleNode("//ProvidentFundingExportMap");

            foreach (XmlElement categoryElement in root.ChildNodes) //ProvidentFundingExportMap//category
            {
                try
                {
                    string categoryName = categoryElement.GetAttribute("name").TrimWhitespaceAndBOM();
                    string loCountyVal = "";
                    string loLPVal = "";
                    foreach (XmlElement valueMap in categoryElement.SelectNodes("value_map")) //IntegrationFields//category//value_map
                    {
                        try
                        {
                            switch (categoryName)
                            {
                                case "CountyIds":
                                    loCountyVal = valueMap.GetAttribute("LOValue");
                                    m_countyIdMap.Add(valueMap.GetAttribute("LOValue"), valueMap.GetAttribute("ProvidentFundingValue"));
                                    break;
                                case "LoanProgramIds":
                                    loLPVal = valueMap.GetAttribute("LOValue");
                                    m_loanProgamIdMap.Add(new Guid(valueMap.GetAttribute("LOValue")), valueMap.GetAttribute("ProvidentFundingValue"));
                                    break;
                            }
                        }
                        catch (ArgumentException e)
                        {
                            Tools.LogError("Duplicate key error while loading a LO/Provident Funding value map from the XML field list in file: " + xmlPath, e);
                        }
                    }
                }
                catch (System.Xml.XPath.XPathException e)
                {
                    Tools.LogError("Error while loading a LO/Provident Funding category from the XML field list in file: " + xmlPath, e);
                }
            }
        }

        private void ExportLoanSaleFile()
        {
            foreach (Guid sLId in m_loanIds)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(ProvidentFundingExport));
                dataLoan.InitLoad();

                AddRowToLoanSaleFileTable(dataLoan);
            }
        }

        public void ExportZipFiles()
        {

            foreach (Guid sLId in m_loanIds)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(ProvidentFundingExport));
                dataLoan.InitLoad();

                int numApps = dataLoan.nApps;
                CAppData primaryApp = dataLoan.GetAppData(0);

                // One row per loan.
                AddRowToBorrowerMailingTable(dataLoan, primaryApp);
                AddRowToFloodCertTable(dataLoan);
                AddRowToLoanTable(dataLoan, primaryApp);
                AddRowToPropertyTable(dataLoan);

                // 3/28/14 gf - opm 174292, If we are in "Interim" mode for a file
                // when exporting the automated data file, don't add to pool table.
                if (!IsInterimMode(dataLoan, GetMortgagePoolForLoan(dataLoan), true))
                {
                    AddRowToPoolTable(dataLoan);
                }

                // Potentially multiple rows per loan.
                AddRowsToInsPolicyTable(dataLoan);
                AddRowsToBorrowerTables(dataLoan);


                // Conditional additions. All are 1 row per loan.
                if (dataLoan.sFinMethT == E_sFinMethT.ARM)
                {
                    AddRowToArmLoanTable(dataLoan);
                }

                if (dataLoan.sLT == E_sLT.FHA
                    || dataLoan.sLT == E_sLT.VA
                    || dataLoan.sLT == E_sLT.UsdaRural)
                {
                    AddRowToGovernmentLoanTable(dataLoan);
                }

                // OPM 111115 - Just 1 row per loan in tax tables
                if (dataLoan.sTaxTableRealETxParcelNum.TrimWhitespaceAndBOM() != ""
                    || dataLoan.sTaxTableSchoolParcelNum.TrimWhitespaceAndBOM() != ""
                    || dataLoan.sTaxTable1008ParcelNum.TrimWhitespaceAndBOM() != ""
                    || dataLoan.sTaxTable1009ParcelNum.TrimWhitespaceAndBOM() != ""
                    || dataLoan.sTaxTableU3ParcelNum.TrimWhitespaceAndBOM() != ""
                    || dataLoan.sTaxTableU4ParcelNum.TrimWhitespaceAndBOM() != "")
                {
                    AddRowToTaxScheduleTable(dataLoan);
                    AddRowToTaxItemTable(dataLoan); 
                }
            }
        }
        
        private void CreateZipTables()
        {
            CreateArmLoanTable();
            CreateBorrowerTable();
            CreateBorrowerMailingTable();
            CreateContactEmailTable();
            CreateContactPhoneTable();
            CreateFloodCertTable();
            CreateInsPolicyTable();
            CreateLoanTable();
            CreatePropertyTable();
            CreateTaxItemTable();
            CreateTaxScheduleTable();
            CreateGovernmentLoanTable();
            CreatePoolTable();
        }

        private void AddRowsToInsPolicyTable(CPageData dataLoan)
        {
            // Add a row for each non-blank policy number
            if (dataLoan.sMiCertId.TrimWhitespaceAndBOM() != "")
                AddRowToInsPolicyTable(dataLoan, E_InsuranceT.MortgageInsurance);
            if (dataLoan.sHazInsPolicyNum.TrimWhitespaceAndBOM() != "")
                AddRowToInsPolicyTable(dataLoan, E_InsuranceT.HazardInsurance);
            if (dataLoan.sFloodInsPolicyNum.TrimWhitespaceAndBOM() != "")
                AddRowToInsPolicyTable(dataLoan, E_InsuranceT.FloodInsurance);
            if (dataLoan.sWindInsPolicyNum.TrimWhitespaceAndBOM() != "")
                AddRowToInsPolicyTable(dataLoan, E_InsuranceT.WindstormInsurance);
            if (dataLoan.sCondoHO6InsPolicyNum.TrimWhitespaceAndBOM() != "")
                AddRowToInsPolicyTable(dataLoan, E_InsuranceT.CondoHO6Insurance);
        }

        private void AddRowsToBorrowerTables(CPageData dataLoan)
        {
            // Adds rows to borrower table, contact email table, and contact phone table.
            int numApps = dataLoan.nApps;
            int borrowerNum = 1;

            for (int i = 0; i < numApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                bool hasCoborrower = dataApp.aCIsDefined;
                bool includeBorrower = dataApp.aBTypeT == E_aTypeT.CoSigner || dataApp.aBTypeT == E_aTypeT.Individual;
                bool includeCoborrower = hasCoborrower && (dataApp.aCTypeT == E_aTypeT.CoSigner || dataApp.aCTypeT == E_aTypeT.Individual);

                if (includeBorrower && includeCoborrower)
                {
                    int coborrowerNum = borrowerNum + 1;
                    
                    AddRowToBorrowerTable(dataLoan, dataApp, borrowerNum, coborrowerNum, borrowerNum, E_BorrowerModeT.Borrower);
                    AddRowToContactEmailTable(dataLoan, dataApp, borrowerNum, E_BorrowerModeT.Borrower);
                    AddRowsToContactPhoneTable(dataLoan, dataApp, borrowerNum, E_BorrowerModeT.Borrower);

                    AddRowToBorrowerTable(dataLoan, dataApp, coborrowerNum, borrowerNum, coborrowerNum, E_BorrowerModeT.Coborrower);
                    AddRowToContactEmailTable(dataLoan, dataApp, coborrowerNum, E_BorrowerModeT.Coborrower);
                    AddRowsToContactPhoneTable(dataLoan, dataApp, coborrowerNum, E_BorrowerModeT.Coborrower);

                    borrowerNum += 2;
                }
                else if (includeBorrower)
                {
                    AddRowToBorrowerTable(dataLoan, dataApp, borrowerNum, 0, borrowerNum, E_BorrowerModeT.Borrower);
                    AddRowToContactEmailTable(dataLoan, dataApp, borrowerNum, E_BorrowerModeT.Borrower);
                    AddRowsToContactPhoneTable(dataLoan, dataApp, borrowerNum, E_BorrowerModeT.Borrower);

                    borrowerNum++;
                }
                else if (includeCoborrower)
                {
                    AddRowToBorrowerTable(dataLoan, dataApp, borrowerNum, 0, borrowerNum, E_BorrowerModeT.Coborrower);
                    AddRowToContactEmailTable(dataLoan, dataApp, borrowerNum, E_BorrowerModeT.Coborrower);
                    AddRowsToContactPhoneTable(dataLoan, dataApp, borrowerNum, E_BorrowerModeT.Coborrower);

                    borrowerNum++;
                }
            }
        }

        private void AddRowsToContactPhoneTable(CPageData dataLoan, CAppData dataApp, int borrowerNum, E_BorrowerModeT borrT)
        {
            switch (borrT)
            {
                case E_BorrowerModeT.Borrower:
                    if (dataApp.aBHPhone.TrimWhitespaceAndBOM() != "")
                        AddRowToContactPhoneTable(dataLoan, dataApp, E_BorrowerModeT.Borrower, E_ContactT.Home, borrowerNum);
                    if (dataApp.aBCellPhone.TrimWhitespaceAndBOM() != "")
                        AddRowToContactPhoneTable(dataLoan, dataApp, E_BorrowerModeT.Borrower, E_ContactT.Mobile, borrowerNum);
                    if (dataApp.aBBusPhone.TrimWhitespaceAndBOM() != "")
                        AddRowToContactPhoneTable(dataLoan, dataApp, E_BorrowerModeT.Borrower, E_ContactT.Work, borrowerNum);
                    if (dataApp.aBFax.TrimWhitespaceAndBOM() != "")
                        AddRowToContactPhoneTable(dataLoan, dataApp, E_BorrowerModeT.Borrower, E_ContactT.WorkFax, borrowerNum);
                    break;
                case E_BorrowerModeT.Coborrower:
                    if (!dataApp.aCIsDefined)
                    {
                        Tools.LogBug("Tried to add coborrower phone to Provident Funding Export when there was not a valid coborrower.");
                        return;
                    }
                    if (dataApp.aCHPhone.TrimWhitespaceAndBOM() != "")
                        AddRowToContactPhoneTable(dataLoan, dataApp, E_BorrowerModeT.Coborrower, E_ContactT.Home, borrowerNum);
                    if (dataApp.aCCellPhone.TrimWhitespaceAndBOM() != "")
                        AddRowToContactPhoneTable(dataLoan, dataApp, E_BorrowerModeT.Coborrower, E_ContactT.Mobile, borrowerNum);
                    if (dataApp.aCBusPhone.TrimWhitespaceAndBOM() != "")
                        AddRowToContactPhoneTable(dataLoan, dataApp, E_BorrowerModeT.Coborrower, E_ContactT.Work, borrowerNum);
                    if (dataApp.aCFax.TrimWhitespaceAndBOM() != "")
                        AddRowToContactPhoneTable(dataLoan, dataApp, E_BorrowerModeT.Coborrower, E_ContactT.WorkFax, borrowerNum);
                    break;
            }
        }

        #region old tax methods
        /*
         * OPM 111115 - The tax tables will now have just 1 row per loan file.
        private void AddRowsToTaxTables(CPageData dataLoan)
        {
            // For TaxItem table, need to add up to 4 per loan file, depending on how many parcel numbers are filled out
            int taxItemId = 1;
            if (dataLoan.sTaxTableRealETxParcelNum.TrimWhitespaceAndBOM() != "")
            {
                AddRowToTaxItemTable(dataLoan, taxItemId, E_TaxTableTaxItemParcelT.RealETx);
                AddRowsToTaxScheduleTable(dataLoan, taxItemId, E_TaxTableTaxItemParcelT.RealETx);
                taxItemId++;
            }
            if (dataLoan.sTaxTableSchoolParcelNum.TrimWhitespaceAndBOM() != "")
            {
                AddRowToTaxItemTable(dataLoan, taxItemId, E_TaxTableTaxItemParcelT.SchoolTx);
                AddRowsToTaxScheduleTable(dataLoan, taxItemId, E_TaxTableTaxItemParcelT.SchoolTx);
                taxItemId++;
            }
            if (dataLoan.sTaxTable1008ParcelNum.TrimWhitespaceAndBOM() != "")
            {
                AddRowToTaxItemTable(dataLoan, taxItemId, E_TaxTableTaxItemParcelT.FirstDDLTx);
                AddRowsToTaxScheduleTable(dataLoan, taxItemId, E_TaxTableTaxItemParcelT.FirstDDLTx);
                taxItemId++;
            }
            if (dataLoan.sTaxTable1009ParcelNum.TrimWhitespaceAndBOM() != "")
            {
                AddRowToTaxItemTable(dataLoan, taxItemId, E_TaxTableTaxItemParcelT.SecondDDLTx);
                AddRowsToTaxScheduleTable(dataLoan, taxItemId, E_TaxTableTaxItemParcelT.SecondDDLTx);
            }
        }
        */
        /*
         * OPM 111115 - The tax tables will now have just 1 row per loan file.
        private void AddRowsToTaxScheduleTable(CPageData dataLoan, int taxItemId, E_TaxTableTaxItemParcelT taxT)
        {
            switch (taxT)
            {
                case E_TaxTableTaxItemParcelT.RealETx:
                    foreach (TaxScheduleItem item in dataLoan.sRealETxScheduleItems)
                    {
                        AddRowToTaxScheduleTable(dataLoan, item.DueD_rep, item.AmtDue, taxItemId, item.PmtNum);
                    }
                    break;
                case E_TaxTableTaxItemParcelT.SchoolTx:
                    foreach (TaxScheduleItem item in dataLoan.sSchoolTxScheduleItems)
                    {
                        AddRowToTaxScheduleTable(dataLoan, item.DueD_rep, item.AmtDue, taxItemId, item.PmtNum);
                    }
                    break;
                case E_TaxTableTaxItemParcelT.FirstDDLTx:
                    foreach (TaxScheduleItem item in dataLoan.sTaxTableTaxDDL1ScheduleItems)
                    {
                        AddRowToTaxScheduleTable(dataLoan, item.DueD_rep, item.AmtDue, taxItemId, item.PmtNum);
                    }
                    break;
                case E_TaxTableTaxItemParcelT.SecondDDLTx:
                    foreach (TaxScheduleItem item in dataLoan.sTaxTableTaxDDL2ScheduleItems)
                    {
                        AddRowToTaxScheduleTable(dataLoan, item.DueD_rep, item.AmtDue, taxItemId, item.PmtNum);
                    }
                    break;
            }
        }
        */
        /*
         * OPM 111115 - The tax tables will now have just 1 row per loan file.
        private void AddRowToTaxItemTable(CPageData dataLoan, int taxItemId, E_TaxTableTaxItemParcelT parcelType)
        {
            DataRow row = m_taxItemTable.NewRow();

            switch (parcelType)
            {
                case E_TaxTableTaxItemParcelT.RealETx:
                    row["apn"] = dataLoan.sTaxTableRealETxParcelNum;
                    row["impound_amt"] = dataLoan.sSettlementRealETxImpoundAmt.ToString(m_dollarFormat);
                    row["initial_monthly_amt"] = dataLoan.sProvExpRealETxInitMoAmt.ToString(m_dollarFormat);
                    row["initial_mos_in_escrow"] = dataLoan.sSettlementRealETxRsrvMon_rep;
                    row["most_recent_paid"] = dataLoan.sTaxTableRealEtxNumOfPmt;
                    row["next_to_pay"] = dataLoan.sTaxTableRealEtxNumOfNextPmt.ToString();
                    row["paid_by_id"] = GetTaxTablePaidByIdRep(dataLoan.sTaxTableRealETxPaidByT);
                    row["payee_id"] = dataLoan.sTaxTableRealEtxPayeeId;
                    row["tax_type_id"] = GetTaxTypeIdRep(dataLoan.sTaxTableRealETxT);
                    break;
                case E_TaxTableTaxItemParcelT.SchoolTx:
                    row["apn"] = dataLoan.sTaxTableSchoolParcelNum;
                    row["impound_amt"] = dataLoan.sSettlementSchoolTxImpoundAmt;
                    row["initial_monthly_amt"] = decimal.Round(dataLoan.sProSchoolTx, 2).ToString();
                    row["initial_mos_in_escrow"] = dataLoan.sSettlementSchoolTxRsrvMon_rep;
                    row["most_recent_paid"] = dataLoan.sTaxTableSchoolNumOfPmt;
                    row["next_to_pay"] = dataLoan.sTaxTableSchoolNumOfNextPmt.ToString();
                    row["paid_by_id"] = GetTaxTablePaidByIdRep(dataLoan.sTaxTableSchoolPaidByT);
                    row["payee_id"] = dataLoan.sTaxTableSchoolPayeeId;
                    row["tax_type_id"] = GetTaxTypeIdRep(dataLoan.sTaxTableSchoolT);
                    break;
                case E_TaxTableTaxItemParcelT.FirstDDLTx:
                    row["apn"] = dataLoan.sTaxTable1008ParcelNum;
                    row["impound_amt"] = dataLoan.sTaxTableTaxDDL1ImpoundAmt.ToString(m_dollarFormat);
                    row["initial_monthly_amt"] = dataLoan.sTaxTableTaxDDL1InitMonAmt.ToString(m_dollarFormat);
                    row["initial_mos_in_escrow"] = dataLoan.sTaxTableTaxDDL1InitMonInEscrow.ToString();
                    row["most_recent_paid"] = dataLoan.sTaxTable1008NumOfPmt;
                    row["next_to_pay"] = dataLoan.sTaxTable1008NumOfNextPmt.ToString();
                    row["paid_by_id"] = GetTaxTablePaidByIdRep(dataLoan.sTaxTable1008PaidByT);
                    row["payee_id"] = dataLoan.sTaxTable1008PayeeId;
                    row["tax_type_id"] = GetTaxTypeIdRep(dataLoan.sTaxTable1008T);
                    break;
                case E_TaxTableTaxItemParcelT.SecondDDLTx:
                    row["apn"] = dataLoan.sTaxTable1009ParcelNum;
                    row["impound_amt"] = dataLoan.sTaxTableTaxDDL2ImpoundAmt.ToString(m_dollarFormat);
                    row["initial_monthly_amt"] = dataLoan.sTaxTableTaxDDL2InitMonAmt.ToString(m_dollarFormat);
                    row["initial_mos_in_escrow"] = dataLoan.sTaxTableTaxDDL2InitMonInEscrow.ToString();
                    row["most_recent_paid"] = dataLoan.sTaxTable1009NumOfPmt;
                    row["next_to_pay"] = dataLoan.sTaxTable1009NumOfNextPmt.ToString();
                    row["paid_by_id"] = GetTaxTablePaidByIdRep(dataLoan.sTaxTable1009PaidByT);
                    row["payee_id"] = dataLoan.sTaxTable1009PayeeId;
                    row["tax_type_id"] = GetTaxTypeIdRep(dataLoan.sTaxTable1009T);
                    break;
            }

            row["old_loan_id"] = dataLoan.sLNm;
            row["tax_item_id"] = taxItemId.ToString();

            m_taxItemTable.Rows.Add(row);
        }
        */
        /*
         * OPM 111115 - The tax tables will now have just 1 row per loan file.
        private void AddRowToTaxScheduleTable(CPageData dataLoan, string dueD, decimal antAmt, int taxItemId, int paymentNum)
        {
            DataRow row = m_taxScheduleTable.NewRow();

            row["anticipated_due_date"] = dueD;
            row["curr_anticipated_amt"] = antAmt.ToString(m_dollarFormat);
            row["old_loan_id"] = dataLoan.sLNm;
            row["tax_item_id"] = taxItemId.ToString();
            row["pmt_num"] = paymentNum.ToString();

            m_taxScheduleTable.Rows.Add(row);
        }
        */
        #endregion

        #region creation of table schema
        private void CreateLoanSaleFileTable()
        {
            m_loanSaleFileTable = new DataTable();
            m_loanSaleFileTable.Columns.Add("old_loan_id", typeof(string));            
            m_loanSaleFileTable.Columns.Add("participation_id", typeof(string));
            m_loanSaleFileTable.Columns.Add("investor_loan_num", typeof(string));
            m_loanSaleFileTable.Columns.Add("pool_num", typeof(string));
            m_loanSaleFileTable.Columns.Add("start_date", typeof(string));
            m_loanSaleFileTable.Columns.Add("service_fee_fixed", typeof(string));
            m_loanSaleFileTable.Columns.Add("service_fee_pct", typeof(string));
            m_loanSaleFileTable.Columns.Add("g_fee_pct", typeof(string));
            m_loanSaleFileTable.Columns.Add("settlement_date", typeof(string));
            m_loanSaleFileTable.Columns.Add("note_custodian_id", typeof(string));
            m_loanSaleFileTable.Columns.Add("sold_principal_bal", typeof(string));
            m_loanSaleFileTable.Columns.Add("payoff_remit_day_id", typeof(string));
            m_loanSaleFileTable.Columns.Add("remit_proj_day_id", typeof(string));
        }

        private void CreateArmLoanTable()
        {
            m_armLoanTable = new DataTable();
            m_armLoanTable.Columns.Add("ceiling_rate", typeof(string));
            m_armLoanTable.Columns.Add("current_index_value", typeof(string));
            m_armLoanTable.Columns.Add("floor_rate", typeof(string));
            m_armLoanTable.Columns.Add("margin", typeof(string));
            m_armLoanTable.Columns.Add("monthly_pmt", typeof(string));
            m_armLoanTable.Columns.Add("next_pi_adj_date", typeof(string));
            m_armLoanTable.Columns.Add("next_pmt_recast_date", typeof(string));
            m_armLoanTable.Columns.Add("next_rate_adj_date", typeof(string));
            m_armLoanTable.Columns.Add("old_loan_id", typeof(string));
            m_armLoanTable.Columns.Add("original_index_value", typeof(string));
            m_armLoanTable.Columns.Add("original_interest_rate", typeof(string));
            m_armLoanTable.Columns.Add("original_pi_pmt", typeof(string));
        }

        private void CreateBorrowerTable()
        {
            m_borrowerTable = new DataTable();
            m_borrowerTable.Columns.Add("borrower_num", typeof(string));
            m_borrowerTable.Columns.Add("borrower_order", typeof(string));
            m_borrowerTable.Columns.Add("date_of_birth", typeof(string));
            m_borrowerTable.Columns.Add("ethnicity_type_id", typeof(string));
            m_borrowerTable.Columns.Add("fico_score", typeof(string));
            // 12/2/2013 gf - opm 143503, split up the first/middle names.
            //m_borrowerTable.Columns.Add("first_middle_name", typeof(string));
            m_borrowerTable.Columns.Add("first_name", typeof(string));
            m_borrowerTable.Columns.Add("middle_name", typeof(string));
            m_borrowerTable.Columns.Add("gender_id", typeof(string));
            m_borrowerTable.Columns.Add("generation_id", typeof(string));
            m_borrowerTable.Columns.Add("hmda_data_source_type_id", typeof(string));
            m_borrowerTable.Columns.Add("language_id", typeof(string));
            m_borrowerTable.Columns.Add("last_name", typeof(string));
            m_borrowerTable.Columns.Add("married_to", typeof(string));
            m_borrowerTable.Columns.Add("name_prefix_id", typeof(string));
            m_borrowerTable.Columns.Add("occupant", typeof(string));
            m_borrowerTable.Columns.Add("old_loan_id", typeof(string));
            m_borrowerTable.Columns.Add("race_type_id", typeof(string));
            m_borrowerTable.Columns.Add("tax_id_num", typeof(string));
        }

        private void CreateBorrowerMailingTable()
        {
            m_borrowerMailingTable = new DataTable();
            m_borrowerMailingTable.Columns.Add("country_id", typeof(string));
            m_borrowerMailingTable.Columns.Add("foreign_prov_postal_code", typeof(string));
            m_borrowerMailingTable.Columns.Add("mail_city", typeof(string));
            m_borrowerMailingTable.Columns.Add("mail_state_id", typeof(string));
            m_borrowerMailingTable.Columns.Add("mail_street1", typeof(string));
            m_borrowerMailingTable.Columns.Add("mail_street2", typeof(string));
            m_borrowerMailingTable.Columns.Add("mail_zip", typeof(string));
            m_borrowerMailingTable.Columns.Add("old_loan_id", typeof(string));
        }

        private void CreateContactEmailTable()
        {
            m_contactEmailTable = new DataTable();
            m_contactEmailTable.Columns.Add("borrower_num", typeof(string));
            m_contactEmailTable.Columns.Add("contact_type_id", typeof(string));
            m_contactEmailTable.Columns.Add("email_addr", typeof(string));
            m_contactEmailTable.Columns.Add("old_loan_id", typeof(string));
            m_contactEmailTable.Columns.Add("other_info", typeof(string));
        }

        private void CreateContactPhoneTable()
        {
            m_contactPhoneTable = new DataTable();
            m_contactPhoneTable.Columns.Add("best_time_end", typeof(string));
            m_contactPhoneTable.Columns.Add("best_time_start", typeof(string));
            m_contactPhoneTable.Columns.Add("borrower_num", typeof(string));
            m_contactPhoneTable.Columns.Add("contact_type_id", typeof(string));
            m_contactPhoneTable.Columns.Add("dst_participation", typeof(string));
            m_contactPhoneTable.Columns.Add("old_loan_id", typeof(string));
            m_contactPhoneTable.Columns.Add("other_info", typeof(string));
            m_contactPhoneTable.Columns.Add("phone_num", typeof(string));
            m_contactPhoneTable.Columns.Add("time_zone_id", typeof(string));            
        }

        private void CreateFloodCertTable()
        {
            m_floodCertTable = new DataTable();
            m_floodCertTable.Columns.Add("flood_base_elevation", typeof(string));
            m_floodCertTable.Columns.Add("flood_CBRA_date", typeof(string));
            m_floodCertTable.Columns.Add("flood_cert_cancel_conf_date", typeof(string));
            m_floodCertTable.Columns.Add("flood_cert_cancel_order_date", typeof(string));
            m_floodCertTable.Columns.Add("flood_cert_co_id", typeof(string));
            m_floodCertTable.Columns.Add("flood_cert_date", typeof(string));
            m_floodCertTable.Columns.Add("flood_cert_num", typeof(string));
            m_floodCertTable.Columns.Add("flood_comm_name", typeof(string));
            m_floodCertTable.Columns.Add("flood_comm_num", typeof(string));
            m_floodCertTable.Columns.Add("flood_comm_part_status", typeof(string));
            m_floodCertTable.Columns.Add("flood_ins_not_available", typeof(string));
            m_floodCertTable.Columns.Add("flood_LOL_upgrade_date", typeof(string));
            m_floodCertTable.Columns.Add("flood_LOL_upgraded", typeof(string));
            m_floodCertTable.Columns.Add("flood_LOMR_date", typeof(string));
            m_floodCertTable.Columns.Add("flood_map_date", typeof(string));
            m_floodCertTable.Columns.Add("flood_map_num", typeof(string));
            m_floodCertTable.Columns.Add("flood_map_panel", typeof(string));
            m_floodCertTable.Columns.Add("flood_panel_suffix", typeof(string));
            m_floodCertTable.Columns.Add("flood_partial", typeof(string));
            m_floodCertTable.Columns.Add("flood_zone", typeof(string));
            m_floodCertTable.Columns.Add("hfiaa_escrow_required", typeof(string));
            m_floodCertTable.Columns.Add("old_loan_id", typeof(string));
        }

        private void CreateInsPolicyTable()
        {
            m_insPolicyTable = new DataTable();
            m_insPolicyTable.Columns.Add("activation_date", typeof(string));
            m_insPolicyTable.Columns.Add("anticipated_prem_due", typeof(string));
            m_insPolicyTable.Columns.Add("billing_period", typeof(string));
            m_insPolicyTable.Columns.Add("condo_pud_policy", typeof(string));
            m_insPolicyTable.Columns.Add("continuous_coverage", typeof(string));
            m_insPolicyTable.Columns.Add("coverage_amt", typeof(string));
            m_insPolicyTable.Columns.Add("extended_repl", typeof(string));
            m_insPolicyTable.Columns.Add("extended_repl_cost_pct", typeof(string));
            m_insPolicyTable.Columns.Add("guaranteed_repl_cost", typeof(string));
            m_insPolicyTable.Columns.Add("impound_amt", typeof(string));
            m_insPolicyTable.Columns.Add("initial_monthly_amt", typeof(string));
            m_insPolicyTable.Columns.Add("initial_mos_in_escrow", typeof(string));
            m_insPolicyTable.Columns.Add("ins_co_payee_id", typeof(string));
            m_insPolicyTable.Columns.Add("ins_type_id", typeof(string));
            m_insPolicyTable.Columns.Add("mi_policy_type", typeof(string));
            m_insPolicyTable.Columns.Add("mi_factor1", typeof(string));
            m_insPolicyTable.Columns.Add("mi_factor2", typeof(string));
            m_insPolicyTable.Columns.Add("mi_factor2_eff_date", typeof(string));
            m_insPolicyTable.Columns.Add("mi_factor3", typeof(string));
            m_insPolicyTable.Columns.Add("mi_factor3_eff_date", typeof(string));
            m_insPolicyTable.Columns.Add("mi_first_prorata", typeof(string));
            m_insPolicyTable.Columns.Add("mi_funded_month", typeof(string));
            m_insPolicyTable.Columns.Add("mi_last_prorata", typeof(string));
            m_insPolicyTable.Columns.Add("mi_no_reserve", typeof(string));
            m_insPolicyTable.Columns.Add("mi_pct_coverage", typeof(string));
            m_insPolicyTable.Columns.Add("mi_tax_amt", typeof(string));
            m_insPolicyTable.Columns.Add("old_loan_id", typeof(string));
            m_insPolicyTable.Columns.Add("paid_by_id", typeof(string));
            m_insPolicyTable.Columns.Add("paid_through_date", typeof(string));
            m_insPolicyTable.Columns.Add("policy_deductible_amt", typeof(string));
            m_insPolicyTable.Columns.Add("policy_due_date", typeof(string));
            m_insPolicyTable.Columns.Add("policy_num", typeof(string));
            m_insPolicyTable.Columns.Add("policy_paid_date", typeof(string));
            m_insPolicyTable.Columns.Add("prem_due", typeof(string));
            m_insPolicyTable.Columns.Add("rental_policy", typeof(string));
            m_insPolicyTable.Columns.Add("unit_num", typeof(string));
        }

        private void CreateLoanTable()
        {
            m_loanTable = new DataTable();
            m_loanTable.Columns.Add("assist_bal", typeof(string));
            m_loanTable.Columns.Add("assist_pmt", typeof(string));
            m_loanTable.Columns.Add("attn_msg", typeof(string));
            m_loanTable.Columns.Add("broker_id", typeof(string));
            m_loanTable.Columns.Add("curtailment_bal", typeof(string));
            m_loanTable.Columns.Add("date_funded", typeof(string));
            m_loanTable.Columns.Add("date_next_pmt_due", typeof(string));
            m_loanTable.Columns.Add("escrow_bal", typeof(string));
            m_loanTable.Columns.Add("escrow_pmt", typeof(string));
            m_loanTable.Columns.Add("fee_bal", typeof(string));
            m_loanTable.Columns.Add("fico_score", typeof(string));
            m_loanTable.Columns.Add("first_pmt_date", typeof(string));
            m_loanTable.Columns.Add("g_fee_pct", typeof(string));
            m_loanTable.Columns.Add("hpml_type_id", typeof(string));
            m_loanTable.Columns.Add("initial_aggregate_adj", typeof(string));
            m_loanTable.Columns.Add("interest_paid", typeof(string));
            m_loanTable.Columns.Add("interest_pmt", typeof(string));
            m_loanTable.Columns.Add("interest_rate", typeof(string));
            m_loanTable.Columns.Add("investor_loan_num", typeof(string));
            m_loanTable.Columns.Add("last_pmt_recv_prior_to_boarding", typeof(string));
            m_loanTable.Columns.Add("late_chg_bal", typeof(string));
            m_loanTable.Columns.Add("lc_type_id", typeof(string));
            m_loanTable.Columns.Add("lien_position", typeof(string));
            m_loanTable.Columns.Add("loan_amt", typeof(string));
            m_loanTable.Columns.Add("loan_program_id", typeof(string));
            m_loanTable.Columns.Add("loan_purpose_id", typeof(string));
            m_loanTable.Columns.Add("maturity_date", typeof(string));
            m_loanTable.Columns.Add("note_custodian_id", typeof(string));
            m_loanTable.Columns.Add("old_loan_id", typeof(string));
            m_loanTable.Columns.Add("originator_id", typeof(string));
            m_loanTable.Columns.Add("owner_occupied", typeof(string));
            m_loanTable.Columns.Add("participation_id", typeof(string));
            m_loanTable.Columns.Add("participation_start_date", typeof(string));
            m_loanTable.Columns.Add("payoff_remit_day_id", typeof(string));
            m_loanTable.Columns.Add("pi_pmt", typeof(string));
            m_loanTable.Columns.Add("points_paid", typeof(string));
            m_loanTable.Columns.Add("prepaid_interest", typeof(string));
            m_loanTable.Columns.Add("prepay_type_id", typeof(string));
            m_loanTable.Columns.Add("principal_bal", typeof(string));
            m_loanTable.Columns.Add("principal_pmt", typeof(string));
            m_loanTable.Columns.Add("remit_proj_day_id", typeof(string));
            m_loanTable.Columns.Add("second_home", typeof(string));
            m_loanTable.Columns.Add("security_bal", typeof(string));
            m_loanTable.Columns.Add("service_fee_pct", typeof(string));
            m_loanTable.Columns.Add("settlement_date", typeof(string));
            m_loanTable.Columns.Add("unappl_bal", typeof(string));
            m_loanTable.Columns.Add("mi_exempt_reason_id", typeof(string));
            m_loanTable.Columns.Add("tx_50a6_refi", typeof(string));
            m_loanTable.Columns.Add("prior_servicer_id", typeof(string));
        }

        private void CreatePropertyTable()
        {
            m_propertyTable = new DataTable();
            m_propertyTable.Columns.Add("attached", typeof(string));
            m_propertyTable.Columns.Add("beneficiary", typeof(string));
            m_propertyTable.Columns.Add("beneficiary_mail_csz", typeof(string));
            m_propertyTable.Columns.Add("beneficiary_mail_street", typeof(string));
            m_propertyTable.Columns.Add("census_tract", typeof(string));
            m_propertyTable.Columns.Add("county_id", typeof(string));
            m_propertyTable.Columns.Add("deed_trustee", typeof(string));
            m_propertyTable.Columns.Add("doc_date", typeof(string));
            m_propertyTable.Columns.Add("estimated_repl_cost", typeof(string));
            m_propertyTable.Columns.Add("hoa_city", typeof(string));
            m_propertyTable.Columns.Add("hoa_contact", typeof(string));
            m_propertyTable.Columns.Add("hoa_email_addr", typeof(string));
            m_propertyTable.Columns.Add("hoa_fax_num", typeof(string));
            m_propertyTable.Columns.Add("hoa_name", typeof(string));
            m_propertyTable.Columns.Add("hoa_phone_num", typeof(string));
            m_propertyTable.Columns.Add("hoa_state_id", typeof(string));
            m_propertyTable.Columns.Add("hoa_street1", typeof(string));
            m_propertyTable.Columns.Add("hoa_street2", typeof(string));
            m_propertyTable.Columns.Add("hoa_zip", typeof(string));
            m_propertyTable.Columns.Add("legal_description", typeof(string));
            m_propertyTable.Columns.Add("mers_min", typeof(string));
            m_propertyTable.Columns.Add("name_of_project", typeof(string));
            m_propertyTable.Columns.Add("num_units", typeof(string));
            m_propertyTable.Columns.Add("old_loan_id", typeof(string));
            m_propertyTable.Columns.Add("original_appraised_value", typeof(string));
            m_propertyTable.Columns.Add("original_ltv", typeof(string));
            m_propertyTable.Columns.Add("prop_city", typeof(string));
            m_propertyTable.Columns.Add("prop_state_id", typeof(string));
            m_propertyTable.Columns.Add("prop_street1", typeof(string));
            m_propertyTable.Columns.Add("prop_street2", typeof(string));
            m_propertyTable.Columns.Add("prop_type_id", typeof(string));
            m_propertyTable.Columns.Add("prop_zip", typeof(string));
            m_propertyTable.Columns.Add("recording_book_num", typeof(string));
            m_propertyTable.Columns.Add("recording_date", typeof(string));
            m_propertyTable.Columns.Add("recording_page_num", typeof(string));
            m_propertyTable.Columns.Add("recording_series_num", typeof(string));
            m_propertyTable.Columns.Add("sales_price", typeof(string));
            m_propertyTable.Columns.Add("vesting", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_1_first_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_1_middle_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_1_last_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_2_first_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_2_middle_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_2_last_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_3_first_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_3_middle_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_3_last_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_4_first_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_4_middle_name", typeof(string));
            m_propertyTable.Columns.Add("mortgagor_4_last_name", typeof(string));
        }

        private void CreateTaxItemTable()
        {
            m_taxItemTable = new DataTable();
            m_taxItemTable.Columns.Add("apn", typeof(string));
            m_taxItemTable.Columns.Add("impound_amt", typeof(string));
            m_taxItemTable.Columns.Add("initial_monthly_amt", typeof(string));
            m_taxItemTable.Columns.Add("initial_mos_in_escrow", typeof(string));
            m_taxItemTable.Columns.Add("most_recent_paid", typeof(string));
            m_taxItemTable.Columns.Add("next_to_pay", typeof(string));
            m_taxItemTable.Columns.Add("old_loan_id", typeof(string));
            m_taxItemTable.Columns.Add("paid_by_id", typeof(string));
            m_taxItemTable.Columns.Add("payee_id", typeof(string));
            m_taxItemTable.Columns.Add("tax_item_id", typeof(string));
            m_taxItemTable.Columns.Add("tax_type_id", typeof(string));
            m_taxItemTable.Columns.Add("wi_tax_pmt_type_id", typeof(string));

        }

        private void CreateTaxScheduleTable()
        {
            m_taxScheduleTable = new DataTable();
            m_taxScheduleTable.Columns.Add("anticipated_due_date", typeof(string));
            m_taxScheduleTable.Columns.Add("curr_anticipated_amt", typeof(string));
            m_taxScheduleTable.Columns.Add("old_loan_id", typeof(string));
            m_taxScheduleTable.Columns.Add("pmt_num", typeof(string));
            m_taxScheduleTable.Columns.Add("tax_item_id", typeof(string));

        }

        private void CreateGovernmentLoanTable()
        {
            m_governmentLoanTable = new DataTable();
            m_governmentLoanTable.Columns.Add("activation_date", typeof(string));
            m_governmentLoanTable.Columns.Add("adp_code", typeof(string));
            m_governmentLoanTable.Columns.Add("base_loan_amt", typeof(string));
            m_governmentLoanTable.Columns.Add("case_num", typeof(string));
            m_governmentLoanTable.Columns.Add("old_loan_id", typeof(string));
            m_governmentLoanTable.Columns.Add("ufmip", typeof(string));
            m_governmentLoanTable.Columns.Add("ufmip_financed", typeof(string));
            m_governmentLoanTable.Columns.Add("acquisition_price", typeof(string));
            m_governmentLoanTable.Columns.Add("prop_owned_12_mos", typeof(string));
            m_governmentLoanTable.Columns.Add("prior_fha", typeof(string));

        }

        private void CreatePoolTable()
        {
            m_poolTable = new DataTable();
            m_poolTable.Columns.Add("old_loan_id", typeof(string));
            m_poolTable.Columns.Add("pool_id", typeof(string));
        }
        #endregion

        #region adding rows

        #region AddRowToLoanSaleFileTable helper methods
        private string GetCustodianIdRep(string name)
        {
            switch (name.ToUpper().TrimWhitespaceAndBOM())
            {
                case "FHLMC": return "1";
                case "USBANK": return "2";
                case "BNY - FHLMC": return "3";
                case "CHASE": return "4";
                case "CFSB": return "5";
                case "PFA": return "6";
                case "DEUTSCHE BANK/SS1": return "7";
                case "PF SERV": return "8";
                case "PF CORP": return "9";
                case "WELLS FARGO/SS1": return "10";
                case "BNY - FNMA": return "11";
                case "UNION BANK": return "12";
                case "DEUTSCHE BANK": return "13";
                case "BNY MELLON/SS6": return "14";
                case "ALLY BANK": return "16";
                case "PNC BANK": return "17";
                case "BNY MELLON/SS": return "18";
                case "BB&T": return "21";
                default: return "0";
            }
        }

        // 2/18/14 gf - opm 150355 use same mapping for pool_num and pool_id.
        //private string GetPoolNumRep(MortgagePool.MortgagePool pool)
        //{
        //    if (pool == null)
        //        return "";
            
        //    return pool.PoolNumberByAgency;
        //}

        /// <summary>
        /// </summary>
        /// <param name="dataLoan"></param>
        /// <param name="pool"></param>
        /// <param name="isForAutomatedDataFile">Should only be false when calling from the loan sale file table.</param>
        /// <returns></returns>
        private bool IsInterimMode(CPageData dataLoan, MortgagePool.MortgagePool pool, bool isForAutomatedDataFile)
        {
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                return GetParticipationIdRep(dataLoan, pool, isForAutomatedDataFile) == "3337";
            }
            return false;
        }

        /// <summary>
        /// </summary>
        /// <param name="dataLoan"></param>
        /// <param name="pool"></param>
        /// <param name="isForAutomatedDataFile">Should only be false when calling from the loan sale file table.</param>
        /// <returns></returns>
        private string GetParticipationIdRep(CPageData dataLoan, MortgagePool.MortgagePool pool, bool isForAutomatedDataFile)
        {
            // iServe specific mapping
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                if (isForAutomatedDataFile || string.IsNullOrEmpty(GetPoolIdRep(dataLoan)))
                {
                    // 2/27/14 gf - opm 174292, we always want to be in interim mode in ADF or when the pool_id is empty.
                    return "3337";
                }
                else if (pool != null && pool.AgencyT == E_MortgagePoolAgencyT.GinnieMae && pool.IssueTypeCode.TrimWhitespaceAndBOM().ToUpper() == "X")
                {
                    return "3320";
                }
                else if (pool != null && pool.AgencyT == E_MortgagePoolAgencyT.GinnieMae
                    && (pool.IssueTypeCode.TrimWhitespaceAndBOM().ToUpper() == "C" || pool.IssueTypeCode.TrimWhitespaceAndBOM().ToUpper() == "M"))
                {
                    return "3321";
                }
                else
                {
                    return "3337"; // if it is not in a mapped pool, return interim participation id. OPM 150355
                }
            }
            
            return "3150"; // test participation id
        }

        /// <summary>
        /// Obtains Prior Servicer Id.
        /// </summary>
        /// <param name="dataLoan">The loan object.</param>
        /// <returns>Returns string value of 4 for iServe, blank string otherwise.</returns>
        private static string GetPriorServicerId(CPageData dataLoan)
        {
            //
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                return "4";
            }
            else
            {
                return string.Empty;
            }
        }

        private MortgagePool.MortgagePool GetMortgagePoolForLoan(CPageData dataLoan)
        {
            if (dataLoan.sMortgagePoolId == null)
            {
                return null;
            }
            else
            {
                return new MortgagePool.MortgagePool(dataLoan.sMortgagePoolId.Value);
            }
        }
        #endregion
        private void AddRowToLoanSaleFileTable(CPageData dataLoan)
        {
            DataRow row = m_loanSaleFileTable.NewRow();
            var pool = GetMortgagePoolForLoan(dataLoan);
            bool isInterimMode = IsInterimMode(dataLoan, pool, false);

            row["old_loan_id"]          = dataLoan.sLNm;
            row["participation_id"]     = GetParticipationIdRep(dataLoan, pool, false);
            row["investor_loan_num"]    = isInterimMode ? dataLoan.sLNm : dataLoan.sPurchaseAdviceSummaryInvLoanNm;
            row["pool_num"]             = GetPoolIdRep(dataLoan);
            row["start_date"]           = dataLoan.sInvSchedDueD1_rep;
            row["service_fee_fixed"]    = isInterimMode ? "0" : dataLoan.sProvExpGseDeliveryServicingF.ToString(m_dollarFormat); // 2/7/2014 gf - opm 150355
            row["service_fee_pct"]      = isInterimMode ? "0" : dataLoan.sProvExpGseDeliveryServicingFPc.ToString(m_rateFormat); // 2/7/2014 gf - opm 150355
            row["g_fee_pct"]            = isInterimMode ? "0" : dataLoan.sProvExpGseDeliveryGuaranteeF.ToString(m_rateFormat);   // 2/7/2014 gf - opm 150355
            row["settlement_date"]      = dataLoan.sLPurchaseD_rep;
            row["note_custodian_id"]    = GetCustodianIdRep(dataLoan.sGseDeliveryDocumentCustodianName);
            row["sold_principal_bal"]   = dataLoan.sProvExpPurchaseAdviceUnpaidPBal.ToString(m_dollarFormat);
            row["payoff_remit_day_id"]  = dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac ? dataLoan.sGseDeliveryRemittanceNumOfDays_rep : "1"; //OPM 193822
            row["remit_proj_day_id"]    = "16"; //OPM 193822 - Freddie Mac remittance types are not supported at this time.

            m_loanSaleFileTable.Rows.Add(row);
        }

        private void AddRowToArmLoanTable(CPageData dataLoan)
        {
            DataRow row = m_armLoanTable.NewRow();

            row["old_loan_id"] = dataLoan.sLNm;

            row["ceiling_rate"]           = dataLoan.sProvExpArmCeilingR.ToString(m_rateFormat);            
            row["current_index_value"]    = dataLoan.sProvExpServicingCurrentArmIndexR.ToString(m_rateFormat);
            row["floor_rate"]             = dataLoan.sProvExpRAdjFloorR.ToString(m_rateFormat);
            row["margin"]                 = dataLoan.sProvExpRAdjMarginR.ToString(m_rateFormat);
            row["monthly_pmt"]            = dataLoan.sProvExpServicingNextPmtAmt.ToString(m_dollarFormat);
            row["next_pi_adj_date"]       = dataLoan.sSchedDueD2_rep;
            row["next_pmt_recast_date"]   = dataLoan.sProvExpArmNextPmtRecastD_rep;
            row["next_rate_adj_date"]     = dataLoan.sProvExpArmNextRateAdjD_rep;
            row["original_index_value"]   = dataLoan.sProvExpRAdjIndexR.ToString(m_rateFormat);
            row["original_interest_rate"] = dataLoan.sProvExpNoteIR.ToString(m_rateFormat);
            row["original_pi_pmt"]        = dataLoan.sProvExpSchedPmt1.ToString(m_dollarFormat);

            m_armLoanTable.Rows.Add(row);
        }
        #region AddRowToBorrowerTable helpers
        private string GetGenerationIdRep(string suffix)
        {
            string lowerTrimmedSuffix = suffix.ToLower().TrimWhitespaceAndBOM();

            switch (lowerTrimmedSuffix)
            {
                case "junior":
                case "jr": 
                    return "1";
                case "senior":
                case "sr": 
                    return "2";
                case "iii": return "3";
                case "iv": return "4";
                case "v": return "5";
                case "ii": return "6";
                default: return "0";
            }
        }

        private string GetRaceIdRep(bool isAmericanIndian, bool isAsian, bool isBlack, bool isPacificIslander, bool isWhite)
        {
            if (isAmericanIndian)
                return "1";
            else if (isAsian)
                return "2";
            else if (isBlack)
                return "3";
            else if (isPacificIslander)
                return "4";
            else if (isWhite)
                return "5";
            else
                return "6";
        }

        private string GetEthnicityTypeIdRep(E_aHispanicT eth)
        {
            switch (eth)
            {
                case E_aHispanicT.LeaveBlank: return "3";
                case E_aHispanicT.Hispanic: return "1";
                case E_aHispanicT.NotHispanic: return "2";
                default:
                    Tools.LogBug("Unhandled enum value of E_aHispanicT");
                    return "3";
            }
        }

        private string GetGenderIdRep(E_GenderT g)
        {
            switch (g)
            {
                case E_GenderT.Male: return "1";
                case E_GenderT.Female: return "2";
                default: return "3";
            }
        }

        private string GetInterviewerMethodRep(E_aIntrvwrMethodT method)
        {
            switch (method)
            {
                case E_aIntrvwrMethodT.ByMail: return "2";
                case E_aIntrvwrMethodT.ByTelephone: return "3";
                case E_aIntrvwrMethodT.FaceToFace: return "1";
                case E_aIntrvwrMethodT.Internet: return "4";
                case E_aIntrvwrMethodT.LeaveBlank: return "5";
                default:
                    Tools.LogBug("Unhandled enum value of E_aIntrvwrMethodT");
                    return "5";
            }
        }
        #endregion
        private void AddRowToBorrowerTable(CPageData dataLoan, CAppData dataApp, int borrowerNum, int attachedBorrowerNum,
                                           int borrowerOrder, E_BorrowerModeT borrType)
        {
            DataRow row = m_borrowerTable.NewRow();

            row["borrower_num"]             = borrowerNum.ToString();
            row["borrower_order"]           = borrowerOrder.ToString();
            row["hmda_data_source_type_id"] = GetInterviewerMethodRep(dataApp.aIntrvwrMethodT);
            row["old_loan_id"]              = dataLoan.sLNm;
            row["language_id"]              = "4";
            row["name_prefix_id"]           = "0";
            row["occupant"]                 = (dataApp.aOccT == E_aOccT.Investment) ? "0" : "1";

            if (borrType == E_BorrowerModeT.Borrower)
            {
                row["date_of_birth"]     = dataApp.aBDob_rep;
                row["ethnicity_type_id"] = GetEthnicityTypeIdRep(dataApp.aBHispanicTFallback);
                row["tax_id_num"]        = Regex.Replace(dataApp.aBSsn, @"[^\d]", "");
                // 12/2/2013 gf - opm 143503, split up the first/middle names.
                //row["first_middle_name"] = String.Format("{0} {1}", dataApp.aBFirstNm, dataApp.aBMidNm).TrimWhitespaceAndBOM();
                row["first_name"]        = dataApp.aBFirstNm;
                row["middle_name"]       = dataApp.aBMidNm;
                row["gender_id"]         = GetGenderIdRep(dataApp.aBGenderFallback);
                row["last_name"]         = dataApp.aBLastNm;
                row["married_to"]        = (dataApp.aBMaritalStatT == E_aBMaritalStatT.Married) ? attachedBorrowerNum.ToString() : "0";
                row["generation_id"]     = GetGenerationIdRep(dataApp.aBSuffix);
                row["race_type_id"]      = GetRaceIdRep(dataApp.aBIsAmericanIndian, dataApp.aBIsAsian,
                                                        dataApp.aBIsBlack, dataApp.aBIsPacificIslander, dataApp.aBIsWhite);
                row["fico_score"]        = dataApp.aBFicoScore;
            }
            else
            {
                row["date_of_birth"]     = dataApp.aCDob_rep;
                row["ethnicity_type_id"] = GetEthnicityTypeIdRep(dataApp.aBHispanicTFallback);
                row["tax_id_num"]        = Regex.Replace(dataApp.aCSsn, @"[^\d]", "");
                // 12/2/2013 gf - opm 143503, split up the first/middle names.
                //row["first_middle_name"] = String.Format("{0} {1}", dataApp.aCFirstNm, dataApp.aCMidNm).TrimWhitespaceAndBOM();
                row["first_name"]        = dataApp.aCFirstNm;
                row["middle_name"]       = dataApp.aCMidNm;
                row["gender_id"]         = GetGenderIdRep(dataApp.aCGenderFallback);
                row["last_name"]         = dataApp.aCLastNm;
                row["married_to"]        = (dataApp.aCMaritalStatT == E_aCMaritalStatT.Married) ? attachedBorrowerNum.ToString() : "0";
                row["generation_id"]     = GetGenerationIdRep(dataApp.aCSuffix);
                row["race_type_id"]      = GetRaceIdRep(dataApp.aCIsAmericanIndian, dataApp.aCIsAsian,
                                                        dataApp.aCIsBlack, dataApp.aCIsPacificIslander, dataApp.aCIsWhite);
                row["fico_score"]        = dataApp.aCFicoScore;
            }

            m_borrowerTable.Rows.Add(row);
        }
        #region AddRowToBorrowerMailingTable helper methods
        private string GetStateIdRep(string state)
        {
            switch (state.ToUpper().TrimWhitespaceAndBOM())
            {
                case "AK": return "1";
                case "AL": return "2";
                case "AR": return "3";
                case "AZ": return "4";
                case "CA": return "5";
                case "CO": return "6";
                case "CT": return "7";
                case "DC": return "8";
                case "DE": return "9";
                case "FL": return "10";
                case "GA": return "11";
                case "HI": return "12";
                case "IA": return "13";
                case "ID": return "14";
                case "IL": return "15";
                case "IN": return "16";
                case "KS": return "17";
                case "KY": return "18";
                case "LA": return "19";
                case "MA": return "20";
                case "MD": return "21";
                case "ME": return "22";
                case "MI": return "23";
                case "MN": return "24";
                case "MO": return "25";
                case "MS": return "26";
                case "MT": return "27";
                case "NC": return "28";
                case "ND": return "29";
                case "NE": return "30";
                case "NH": return "31";
                case "NJ": return "32";
                case "NM": return "33";
                case "NV": return "34";
                case "NY": return "35";
                case "OH": return "36";
                case "OK": return "37";
                case "OR": return "38";
                case "PA": return "39";
                case "RI": return "40";
                case "SC": return "41";
                case "SD": return "42";
                case "TN": return "43";
                case "TX": return "44";
                case "UT": return "45";
                case "VA": return "46";
                case "VT": return "47";
                case "WA": return "48";
                case "WI": return "49";
                case "WV": return "50";
                case "WY": return "51";
                default: return "0";
            }
        }
        private string GetMailComponent(CPageData dataLoan, CAppData dataApp, E_MailComponent component)
        {
            if (dataApp.aOccT == E_aOccT.PrimaryResidence &&
                dataLoan.sLPurposeT == E_sLPurposeT.Purchase)
            {
                switch (component)
                {
                    case E_MailComponent.City: return dataLoan.sSpCity;
                    case E_MailComponent.State: return GetStateIdRep(dataLoan.sSpState);
                    case E_MailComponent.Street: return dataLoan.sSpAddr;
                    case E_MailComponent.Zip: return dataLoan.sSpZip;
                    default: return "";
                }
            }
            else
            {
                // These fields have logic that will return the correct value
                // if "same as present address" is checked.
                switch (component)
                {
                    case E_MailComponent.City: return dataApp.aBCityMail;
                    case E_MailComponent.State: return GetStateIdRep(dataApp.aBStateMail);
                    case E_MailComponent.Street: return dataApp.aBAddrMail;
                    case E_MailComponent.Zip: return dataApp.aBZipMail;
                    default: return "";
                }
            }
        }

        #endregion
        private void AddRowToBorrowerMailingTable(CPageData dataLoan, CAppData dataApp)
        {
            DataRow row = m_borrowerMailingTable.NewRow();

            row["country_id"]               = "1";
            row["foreign_prov_postal_code"] = "";
            row["mail_city"]                = GetMailComponent(dataLoan, dataApp, E_MailComponent.City);
            row["mail_state_id"]            = GetMailComponent(dataLoan, dataApp, E_MailComponent.State);
            row["mail_street1"]             = GetMailComponent(dataLoan, dataApp, E_MailComponent.Street);
            row["mail_street2"]             = "";
            row["mail_zip"]                 = GetMailComponent(dataLoan, dataApp, E_MailComponent.Zip);
            row["old_loan_id"]              = dataLoan.sLNm;

            m_borrowerMailingTable.Rows.Add(row);
        }

        private void AddRowToContactEmailTable(CPageData dataLoan, CAppData dataApp, int borrowerNum, E_BorrowerModeT borrType)
        {
            DataRow row = m_contactEmailTable.NewRow();

            row["borrower_num"]     = borrowerNum.ToString();
            row["contact_type_id"]  = "2";

            if (borrType == E_BorrowerModeT.Borrower)
            {
                row["email_addr"] = dataApp.aBEmail;
            }
            else
            {
                row["email_addr"] = dataApp.aCEmail;
            }

            row["old_loan_id"]  = dataLoan.sLNm;
            row["other_info"]   = "";

            m_contactEmailTable.Rows.Add(row);
        }

        private string GetContactTypeIdRep(E_ContactT contactType)
        {
            switch (contactType)
            {
                case E_ContactT.Home: return "2";
                case E_ContactT.Mobile: return "5";
                case E_ContactT.Work: return "1";
                case E_ContactT.WorkFax: return "4";
                default:
                    Tools.LogBug("Unhandled enum value for E_ContactT");
                    return "";
            }
        }

        private void AddRowToContactPhoneTable(CPageData dataLoan, CAppData dataApp, E_BorrowerModeT borrType, 
            E_ContactT contactType, int borrowerNum)
        {
            bool isBorrower = (borrType == E_BorrowerModeT.Borrower);
            DataRow row = m_contactPhoneTable.NewRow();

            row["best_time_end"]     = "0";
            row["best_time_start"]   = "0";
            row["borrower_num"]      = borrowerNum.ToString();
            row["contact_type_id"]   = GetContactTypeIdRep(contactType);
            row["dst_participation"] = "1";
            row["old_loan_id"]       = dataLoan.sLNm;
            row["other_info"]        = "0";
            string phoneNum          = "";
            switch (contactType)
            {
                case E_ContactT.Home:
                    phoneNum = isBorrower ? dataApp.aBHPhone : dataApp.aCHPhone;
                    break;
                case E_ContactT.Work:
                    phoneNum = isBorrower ? dataApp.aBBusPhone : dataApp.aCBusPhone;
                    break;
                case E_ContactT.WorkFax:
                    phoneNum = isBorrower ? dataApp.aBFax : dataApp.aCFax;
                    break;
                case E_ContactT.Mobile:
                    phoneNum = isBorrower ? dataApp.aBCellPhone : dataApp.aCCellPhone;
                    break;
            }
            // 2/28/13 gf - OPM 111115
            string phoneDigits  = Regex.Replace(phoneNum, @"[^\d]", "");
            row["phone_num"]    = (phoneDigits.Length > 10) ? phoneDigits.Substring(0, 10) : phoneDigits;
            row["time_zone_id"] = "0";

            m_contactPhoneTable.Rows.Add(row);
        }
        private string GetFloodCoIdRep(E_FloodCertificationPreparerT preparerT)
        {
            switch (preparerT)
            {
                case E_FloodCertificationPreparerT.ChicagoTitle: return "1";
                case E_FloodCertificationPreparerT.Fidelity: return "2";
                case E_FloodCertificationPreparerT.FirstAm_Corelogic: return "3";
                case E_FloodCertificationPreparerT.Leretta: return "4";
                case E_FloodCertificationPreparerT.USFlood: return "5";
                case E_FloodCertificationPreparerT.LandSafe: return "6";
                case E_FloodCertificationPreparerT.CBCInnovis_FZDS: return "7"; // 11/20/2015 BS - Case 228701
                // 10/16/2015 BS - Case 228700. Update LPS to ServiceLink
                case E_FloodCertificationPreparerT.ServiceLink: return "8";
                case E_FloodCertificationPreparerT.DataQuick: return "9";
                case E_FloodCertificationPreparerT.KrollFactualData: // [BB] 10/1/13 - apparently PF has no value for Kroll at this time
                default: return "";
            }
        }

        private void AddRowToFloodCertTable(CPageData dataLoan)
        {
            DataRow row = m_floodCertTable.NewRow();

            row["flood_base_elevation"]         = "";
            row["flood_CBRA_date"]              = dataLoan.sFloodCertificationDesignationD_rep;
            row["flood_cert_cancel_conf_date"]  = dataLoan.sFloodCertificationCancellationReceivedD_rep;
            row["flood_cert_cancel_order_date"] = dataLoan.sFloodCertificationCancellationOrderedD_rep;
            row["flood_cert_co_id"]             = GetFloodCoIdRep(dataLoan.sFloodCertificationPreparerT);
            row["flood_cert_date"]              = dataLoan.sFloodCertificationDeterminationD_rep;
            row["flood_cert_num"]               = dataLoan.sFloodCertId;
            row["flood_comm_name"]              = dataLoan.sFloodHazardCommunityDesc;
            row["flood_comm_num"]               = dataLoan.sFloodCertificationCommunityNum;
            row["flood_comm_part_status"]       = dataLoan.sFloodCertificationParticipationStatus;
            row["flood_ins_not_available"]      = dataLoan.sFloodCertificationParticipationStatus == "N" ? "1" : "0";
            row["flood_LOL_upgrade_date"]       = dataLoan.sFloodCertificationLOLUpgradeD_rep;
            row["flood_LOL_upgraded"]           = dataLoan.sFloodCertificationIsLOLUpgraded ? "1" : "0";
            row["flood_LOMR_date"]              = dataLoan.sFloodCertificationLOMChangedD_rep;
            row["flood_map_date"]               = dataLoan.sFloodCertificationMapD_rep;
            row["flood_map_num"]                = dataLoan.sFloodCertificationMapNum;
            row["flood_map_panel"]              = dataLoan.sFloodCertificationPanelNums;
            row["flood_panel_suffix"]           = dataLoan.sFloodCertificationPanelSuffix;
            row["flood_partial"]                = dataLoan.sNfipIsPartialZone ? "1" : "0";
            row["flood_zone"]                   = dataLoan.sNfipFloodZoneId;
            row["hfiaa_escrow_required"]        = (string.IsNullOrWhiteSpace(dataLoan.sNfipFloodZoneId) || dataLoan.sNfipFloodZoneId.StartsWith("X", StringComparison.OrdinalIgnoreCase) || dataLoan.sProdSpT == E_sProdSpT.Condo) ? "0" : "1";
            row["old_loan_id"]                  = dataLoan.sLNm;

            m_floodCertTable.Rows.Add(row);
        }

        #region AddRowToInsPolicyTable helper methods
        private string GetMIPayeeId(E_sMiCompanyNmT nm)
        {
            switch (nm)
            {
                case E_sMiCompanyNmT.CMG: return "1123";
                case E_sMiCompanyNmT.Essent: return "38074";
                case E_sMiCompanyNmT.Genworth: return "653";
                case E_sMiCompanyNmT.MGIC: return "652";
                case E_sMiCompanyNmT.Radian: return "656";
                case E_sMiCompanyNmT.UnitedGuaranty: return "35049";
                case E_sMiCompanyNmT.PMI: return "654";
                case E_sMiCompanyNmT.RMIC: return "657";
                case E_sMiCompanyNmT.Triad: return "36002";
                case E_sMiCompanyNmT.FHA: return "36049";
                case E_sMiCompanyNmT.USDA: return "40082";
                case E_sMiCompanyNmT.VA: return "40172"; // OPM 111115
                case E_sMiCompanyNmT.Amerin:
                case E_sMiCompanyNmT.Arch:
                case E_sMiCompanyNmT.CAHLIF:
                case E_sMiCompanyNmT.CMGPre94:
                case E_sMiCompanyNmT.Commonwealth:
                case E_sMiCompanyNmT.MDHousing:
                case E_sMiCompanyNmT.MIF:
                case E_sMiCompanyNmT.RMICNC:
                case E_sMiCompanyNmT.SONYMA:
                case E_sMiCompanyNmT.Verex:
                case E_sMiCompanyNmT.WiscMtgAssr:
                case E_sMiCompanyNmT.LeaveBlank:
                case E_sMiCompanyNmT.MassHousing:
                default:
                    return "1121";
            }
        }

        private bool IsCondoPUDPolicy(CPageData dataLoan, E_InsuranceT insuranceT)
        {
            switch (insuranceT)
            {
                case E_InsuranceT.MortgageInsurance:
                case E_InsuranceT.CondoHO6Insurance:
                    return false;
                case E_InsuranceT.HazardInsurance:
                    return dataLoan.sHazInsPaidByT == E_sInsPaidByT.HOA;
                case E_InsuranceT.FloodInsurance:
                    return dataLoan.sFloodInsPaidByT == E_sInsPaidByT.HOA;
                case E_InsuranceT.WindstormInsurance:
                    return dataLoan.sWindInsPaidByT == E_sInsPaidByT.HOA;
                default:
                    Tools.LogBug("Unhandled enum value of E_InsuranceT");
                    return false;
            }
                
        }

        private string GetProrataRep(E_MiProrata type)
        {
            switch (type)
            {
                case E_MiProrata.FullMonth: return "0";
                case E_MiProrata.PartialPremium: return "1";
                case E_MiProrata.NoInfo: return "2";
                default:
                    Tools.LogBug("Unhandled enum value of E_MiProrata");
                    return "";
            }
        }

        private string GetMIFundedMonthRep(CPageData dataLoan)
        {
            if (dataLoan.sMiPmtDueD_rep.TrimWhitespaceAndBOM() == "" || dataLoan.sFundD_rep.TrimWhitespaceAndBOM() == "")
                return "2";
            else if (dataLoan.sMiPmtDueD.IsValid && dataLoan.sFundD.IsValid &&
                     (dataLoan.sMiPmtDueD.DateTimeForComputation.Month ==
                      dataLoan.sFundD.DateTimeForComputation.Month))
                return "1";
            else
                return "0";
        }
/*
        private string GetMIPaidByIdRep(CPageData dataLoan)
        {

            // 11/25/2013 gf - opm 145629 From Provident's perspective, MI will always be lender paid.
            return "1";

            //switch (dataLoan.sMiInsuranceT)
            //{
            //    case E_sMiInsuranceT.BorrowerPaid: return "2";
            //    case E_sMiInsuranceT.LenderPaid: return "1";
            //    case E_sMiInsuranceT.InvestorPaid:
            //    case E_sMiInsuranceT.None:
            //    default:
            //        return "";
            //}

        }
*/
        private string GetOtherInsPaidByIdRep(E_sInsPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_sInsPaidByT.Borrower: return "2";
                case E_sInsPaidByT.Lender: return "1";
                case E_sInsPaidByT.HOA: return "3";
                case E_sInsPaidByT.None:
                default: return "";
            }
        }

        private string GetInsTypeIdRep(E_InsuranceT insType)
        {
            switch (insType)
            {
                case E_InsuranceT.CondoHO6Insurance: return "11";
                case E_InsuranceT.FloodInsurance: return "1";
                case E_InsuranceT.HazardInsurance: return "2";
                case E_InsuranceT.MortgageInsurance: return "5";
                case E_InsuranceT.WindstormInsurance: return "3";
                default: 
                    Tools.LogBug("Unhandled enum value of E_InsuranceT");
                    return "";
            }
        }

        private bool IsGovernmentLoan(CPageData dataLoan)
        {
            return (dataLoan.sLT == E_sLT.FHA || dataLoan.sLT == E_sLT.VA || dataLoan.sLT == E_sLT.UsdaRural);
        }

        private bool IsDecliningMIPolicy(CPageData dataLoan)
        {
            // 12/12/2013 gf - opm 142123 Hardcode VA as 'constant' if monthly MI amount is 0.
            if (dataLoan.sLT == E_sLT.VA && dataLoan.sProMIns == 0)
            {
                return false;
            }

            var mInsT = dataLoan.sProMInsT;
            return mInsT == E_PercentBaseT.AverageOutstandingBalance || mInsT == E_PercentBaseT.DecliningRenewalsAnnually
                || mInsT == E_PercentBaseT.DecliningRenewalsMonthly;
        }

        #endregion

        private void AddRowToInsPolicyTable(CPageData dataLoan, E_InsuranceT insType)
        {
            DataRow row = m_insPolicyTable.NewRow();
            if (insType != E_InsuranceT.MortgageInsurance)
            {
                row["mi_policy_type"]      = "";
                row["mi_factor1"]          = "";
                row["mi_factor2"]          = "0";
                row["mi_factor2_eff_date"] = "";
                row["mi_factor3"]          = "0";
                row["mi_factor3_eff_date"] = "";
                row["mi_first_prorata"]    = "2";
                row["mi_funded_month"]     = "2";
                row["mi_last_prorata"]     = "2";
                row["mi_no_reserve"]       = "0";
                row["mi_pct_coverage"]     = "0";
                row["mi_tax_amt"]          = "0";
            }

            row["condo_pud_policy"]       = IsCondoPUDPolicy(dataLoan, insType) ? "1" : "0";
            row["continuous_coverage"]    = "1";
            row["extended_repl"]          = "0";
            row["extended_repl_cost_pct"] = "0";
            row["guaranteed_repl_cost"]   = "0";
            row["impound_amt"]            = "0";
            row["initial_monthly_amt"]    = "0";
            row["initial_mos_in_escrow"]  = "0";
            row["old_loan_id"]            = dataLoan.sLNm;
            row["rental_policy"]          = dataLoan.sOccT == E_sOccT.Investment ? "1" : "0";
            row["unit_num"]               = "";

            // OPM 111115 - made anticipated_prem_due the same as prem_due
            switch (insType)
            {
                case E_InsuranceT.MortgageInsurance:
                    row["activation_date"]       = dataLoan.sMiCertIssuedD_rep;
                    row["anticipated_prem_due"]  = dataLoan.sProvExpMIPremDue.ToString(m_dollarFormat);
                    row["billing_period"]        = (dataLoan.sLT == E_sLT.VA && dataLoan.sProMIns == 0) ? "0" : dataLoan.sProvExpMIBillingPeriod.ToString(); // 11/20/2013 gf - opm 142123
                    row["coverage_amt"]          = "0";
                    row["ins_co_payee_id"]       = GetMIPayeeId(dataLoan.sMiCompanyNmT);
                    row["ins_type_id"]           = IsGovernmentLoan(dataLoan) ? "10" : "5";
                    row["mi_policy_type"]        = IsDecliningMIPolicy(dataLoan) ? "2" : "1";
                    row["mi_factor1"]            = IsDecliningMIPolicy(dataLoan) ? dataLoan.sProvExpProMInsR.ToString(m_rateFormat) : "";
                    row["mi_factor2"]            = dataLoan.sProvExpProMInsR2.ToString(m_rateFormat); // Will default to correct value
                    row["mi_factor2_eff_date"]   = dataLoan.sProvExpMIFactor2EffectiveD_rep; // Will default to correct value
                    row["mi_factor3"]            = "0";
                    row["mi_factor3_eff_date"]   = "";
                    row["mi_first_prorata"]      = GetProrataRep(dataLoan.sMiFirstProrataT);
                    row["mi_funded_month"]       = GetMIFundedMonthRep(dataLoan);
                    row["mi_last_prorata"]       = GetProrataRep(dataLoan.sMiLastProrataT);
                    row["mi_no_reserve"]         = dataLoan.sMiIsNoReserve ? "1" : "0";
                    row["mi_pct_coverage"]       = dataLoan.sProvExpMiLenderPaidCoverage.ToString(m_rateFormat);
                    row["mi_tax_amt"]            = "0";
                    row["paid_by_id"]            = "1"; // 11/25/2013 gf - opm 145629 From Provident's perspective, MI will always be lender paid.
                    row["paid_through_date"]     = dataLoan.sMiPaidThruD_rep;
                    row["policy_deductible_amt"] = "0";
                    row["policy_due_date"]       = dataLoan.sMiPmtDueD_rep;
                    row["policy_num"]            = dataLoan.sMiCertId;
                    row["policy_paid_date"]      = dataLoan.sMiPmtMadeD_rep;
                    row["prem_due"]              = dataLoan.sProvExpMIPremDue.ToString(m_dollarFormat);
                    break;
                case E_InsuranceT.HazardInsurance:
                    row["activation_date"]       = dataLoan.sHazInsPolicyActivationD_rep;
                    row["anticipated_prem_due"]  = decimal.Round(dataLoan.sProvExpHazInsPaymentDueAmt, 2).ToString(m_dollarFormat);
                    row["billing_period"]        = dataLoan.sHazInsBillingPeriod.ToString();
                    row["coverage_amt"]          = decimal.Round(dataLoan.sHazInsCoverageAmt, 2).ToString(m_dollarFormat);
                    row["ins_co_payee_id"]       = dataLoan.sHazInsCompanyId;
                    row["ins_type_id"]           = GetInsTypeIdRep(insType);
                    row["paid_by_id"]            = GetOtherInsPaidByIdRep(dataLoan.sHazInsPaidByT);
                    row["paid_through_date"]     = dataLoan.sHazInsPaidThroughD_rep;
                    row["policy_deductible_amt"] = decimal.Round(dataLoan.sHazInsDeductibleAmt, 2).ToString(m_dollarFormat);
                    row["policy_due_date"]       = dataLoan.sHazInsPaymentDueD_rep;
                    row["policy_num"]            = dataLoan.sHazInsPolicyNum;
                    row["policy_paid_date"]      = dataLoan.sHazInsPaymentMadeD_rep;
                    row["prem_due"]              = decimal.Round(dataLoan.sProvExpHazInsPaymentDueAmt, 2).ToString(m_dollarFormat);
                    break;
                case E_InsuranceT.FloodInsurance:
                    row["activation_date"]       = dataLoan.sFloodInsPolicyActivationD_rep;
                    row["anticipated_prem_due"]  = decimal.Round(dataLoan.sProvExpFloodInsPaymentDueAmt, 2).ToString(m_dollarFormat);
                    row["billing_period"]        = dataLoan.sFloodInsBillingPeriod.ToString();
                    row["coverage_amt"]          = decimal.Round(dataLoan.sFloodInsCoverageAmt, 2).ToString(m_dollarFormat);
                    row["ins_co_payee_id"]       = dataLoan.sFloodInsCompanyId;
                    row["ins_type_id"]           = GetInsTypeIdRep(insType);
                    row["paid_by_id"]            = GetOtherInsPaidByIdRep(dataLoan.sFloodInsPaidByT);
                    row["paid_through_date"]     = dataLoan.sFloodInsPaidThroughD_rep;
                    row["policy_deductible_amt"] = decimal.Round(dataLoan.sFloodInsDeductibleAmt, 2).ToString(m_dollarFormat);
                    row["policy_due_date"]       = dataLoan.sFloodInsPaymentDueD_rep;
                    row["policy_num"]            = dataLoan.sFloodInsPolicyNum;
                    row["policy_paid_date"]      = dataLoan.sFloodInsPaymentMadeD_rep;
                    row["prem_due"]              = decimal.Round(dataLoan.sProvExpFloodInsPaymentDueAmt, 2).ToString(m_dollarFormat);
                    break;
                case E_InsuranceT.WindstormInsurance:
                    row["activation_date"]       = dataLoan.sWindInsPolicyActivationD_rep;
                    row["anticipated_prem_due"]  = decimal.Round(dataLoan.sProvExpWindInsPaymentDueAmt, 2).ToString(m_dollarFormat);
                    row["billing_period"]        = dataLoan.sWindInsBillingPeriod.ToString();
                    row["coverage_amt"]          = decimal.Round(dataLoan.sWindInsCoverageAmt, 2).ToString(m_dollarFormat);
                    row["ins_co_payee_id"]       = dataLoan.sWindInsCompanyId;
                    row["ins_type_id"]           = GetInsTypeIdRep(insType);
                    row["paid_by_id"]            = GetOtherInsPaidByIdRep(dataLoan.sWindInsPaidByT);
                    row["paid_through_date"]     = dataLoan.sWindInsPaidThroughD_rep;
                    row["policy_deductible_amt"] = decimal.Round(dataLoan.sWindInsDeductibleAmt, 2).ToString(m_dollarFormat);
                    row["policy_due_date"]       = dataLoan.sWindInsPaymentDueD_rep;
                    row["policy_num"]            = dataLoan.sWindInsPolicyNum;
                    row["policy_paid_date"]      = dataLoan.sWindInsPaymentMadeD_rep;
                    row["prem_due"]              = decimal.Round(dataLoan.sProvExpWindInsPaymentDueAmt, 2).ToString(m_dollarFormat);
                    break;
                case E_InsuranceT.CondoHO6Insurance:
                    row["activation_date"]       = dataLoan.sCondoHO6InsPolicyActivationD_rep;
                    row["anticipated_prem_due"]  = decimal.Round(dataLoan.sProvExpCondoHO6InsPaymentDueAmt, 2).ToString(m_dollarFormat);
                    row["billing_period"]        = dataLoan.sCondoHO6InsBillingPeriod.ToString();
                    row["coverage_amt"]          = decimal.Round(dataLoan.sCondoHO6InsCoverageAmt, 2).ToString(m_dollarFormat);
                    row["ins_co_payee_id"]       = dataLoan.sCondoHO6InsCompanyId;
                    row["ins_type_id"]           = GetInsTypeIdRep(insType);
                    row["paid_by_id"]            = GetOtherInsPaidByIdRep(dataLoan.sCondoHO6InsPaidByT);
                    row["paid_through_date"]     = dataLoan.sCondoHO6InsPaidThroughD_rep;
                    row["policy_deductible_amt"] = decimal.Round(dataLoan.sCondoHO6InsDeductibleAmt, 2).ToString(m_dollarFormat);
                    row["policy_due_date"]       = dataLoan.sCondoHO6InsPaymentDueD_rep;
                    row["policy_num"]            = dataLoan.sCondoHO6InsPolicyNum;
                    row["policy_paid_date"]      = dataLoan.sCondoHO6InsPaymentMadeD_rep;
                    row["prem_due"]              = decimal.Round(dataLoan.sProvExpCondoHO6InsPaymentDueAmt, 2).ToString(m_dollarFormat);
                    break;
            }

            m_insPolicyTable.Rows.Add(row);
        }

        #region AddRowToLoanTable helper methods

        /// <summary>
        /// Gets the last payment date prior to boarding according to Provident specifications.
        /// </summary>
        /// <param name="dataLoan">Loan object from with last payment date prior to boarding is calculated.</param>
        /// <returns>Returns empty string if servicing first payment due date is same as servicing next payment date, otherwise non-blank servicing last payment made date or investor purchase date.</returns>
        private string GetLastPaymentReceivedPriorToBoarding(CPageData dataLoan)
        {
            if (dataLoan.sSchedDueD1_rep.Equals(dataLoan.sServicingNextPmtDue_rep))
            {
                return string.Empty; //Field only required when first payment date != next payment date
            }
            else if (!string.IsNullOrEmpty(dataLoan.sServicingLastPaymentMadePaymentD_rep))
            {
                return dataLoan.sServicingLastPaymentMadePaymentD_rep;
            }
            else
            {
                return dataLoan.sLPurchaseD_rep; //Equivalent to settlement_date
            }
        }

        private string GetLienPosTRep(E_sLienPosT type)
        {
            switch (type)
            {
                case E_sLienPosT.First: return "1";
                case E_sLienPosT.Second: return "2";
                default:
                    Tools.LogBug("Unhandled enum value for E_sLientPosT");
                    return "";
            }
        }

        private string GetLateChargeIdRep(CPageData dataLoan)
        {
            // iServe wants this hardcoded to 4% of PI/15day, $0 min, no max.
            // ***ARCHIVE*** 4/25/14 gf - opm 173997, this should be hard-coded to 11, not 16
            // 3/23/15 ir - opm 207287 - iServe advises using PITI "11" for NC/SC, PI "16" otherwise.
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                string[] pitiStates = { "NC", "SC" }; 
                if (pitiStates.Contains(dataLoan.sSpState))
                {
                    return "11";
                }

                return "16";
            }

            switch (dataLoan.sServicingCurrentLateChargeT)
            {
                case E_TaxLateChargeT.v5PctPI_15day_5min_nomax: return "1";
                case E_TaxLateChargeT.Fixed_15day: return "2";
                case E_TaxLateChargeT.v4PctPI_15day_5min_nomax: return "3";
                case E_TaxLateChargeT.v3PctPI_15day_5min_nomax: return "4";
                case E_TaxLateChargeT.v2PctPI_15day_5min_nomax: return "5";
                case E_TaxLateChargeT.v5PctPI_10day_5min_nomax: return "6";
                case E_TaxLateChargeT.v4PctPI_10day_5min_nomax: return "7";
                case E_TaxLateChargeT.v3PctPI_10day_5min_nomax: return "8";
                case E_TaxLateChargeT.v2PctPI_10day_5min_nomax: return "9";
                case E_TaxLateChargeT.v10PctPI_15day_5min_nomax: return "10";
                case E_TaxLateChargeT.v4PctPITI_15day_0min_nomax: return "11";
                case E_TaxLateChargeT.None: return "12";
                case E_TaxLateChargeT.v2PctPI_15day_0min_nomax: return "13";
                case E_TaxLateChargeT.v3PctPI_15day_0min_nomax: return "14";                
                case E_TaxLateChargeT.v4PctPI_10day_0min_nomax: return "15";                
                case E_TaxLateChargeT.v4PctPI_15day_0min_nomax: return "16";
                case E_TaxLateChargeT.v5PctPI_10day_0min_nomax: return "17";
                case E_TaxLateChargeT.v5PctPI_15day_0min_nomax: return "18";
                case E_TaxLateChargeT.v5PctPI_15day_0min_10max: return "19";
                case E_TaxLateChargeT.v5PctPI_15day_0min_25max: return "20";
                case E_TaxLateChargeT.v6PctPI_15day_0min_nomax: return "21";
                case E_TaxLateChargeT.v5PctPI_15day_15min_nomax: return "22";
                case E_TaxLateChargeT.v5PctPI_15day_18min_100max: return "23";
                case E_TaxLateChargeT.v3PctPITI_15day_0min_nomax: return "24";
                case E_TaxLateChargeT.v5PctPITI_15day_0min_nomax: return "25";
                case E_TaxLateChargeT.v5PctPI_10day_0min_15max: return "26";
                case E_TaxLateChargeT.v1PctPI_15day_0min_nomax: return "27";
                case E_TaxLateChargeT.v5PctPI_15day_0min_15max: return "28";
                case E_TaxLateChargeT.NoInfo:
                default: return "12"; // OPM 111115 - default to None if not set.
            }
        }

        private string GetLoanPurposeIdRep(CPageData dataLoan)
        {
            if (dataLoan.sIsRenovationLoan)
            {
                return "3";
            }

            switch (dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.Purchase:
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    return "2";
                case E_sLPurposeT.RefinCashout: 
                case E_sLPurposeT.HomeEquity:
                    return "4";
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                    return "5";
                case E_sLPurposeT.Other: 
                    return "";
                default:
                    Tools.LogBug("Unhandled enum value of E_sLPurposeT");
                    return "";
            }
        }

        private string GetOwnerOccupiedRep(E_aOccT occType)
        {
            switch (occType)
            {
                case E_aOccT.PrimaryResidence:
                case E_aOccT.SecondaryResidence:
                    return "1";
                case E_aOccT.Investment:
                default: 
                    return "0";
            }
        }

        private string GetSecondHomeRep(E_aOccT occType)
        {
            switch (occType)
            {
                case E_aOccT.PrimaryResidence:
                case E_aOccT.Investment:
                    return "0";
                case E_aOccT.SecondaryResidence: 
                    return "1";
                default:
                    return "";
            }
        }

        private string GetMaturityDateRep(CPageData dataLoan)
        {
            AmortTable table;
            try
            {
                table = dataLoan.GetAmortTable(E_AmortizationScheduleT.Standard, false);
            }
            catch (AmortTableInvalidArgumentException)
            {
                // Return empty string, let their validator catch
                return "";
            }

            if (table.Items.Length == 0)
                return "";

            AmortItem item = table.Items[table.Items.Length - 1];
            return item.DueDate_rep;
        }

        private string GetPmlBrokerCompanyId(CPageData dataLoan)
        {
            PmlBroker pb = PmlBroker.RetrievePmlBrokerByLoanId(dataLoan.sBrokerId, dataLoan.sLId);
            if (pb == null)
                return "0";
            else
                return pb.CompanyId;
        }

        private string GetLoanProgramIdRep(CPageData dataLoan)
        {
            string idRep;
            if (m_loanProgamIdMap.TryGetValue(dataLoan.sLpTemplateId, out idRep))
            {
                return idRep;
            }

            // iServe wanted to do this based off of the term of the loan, as 
            // sLpTemplateId usually isn't filled out when they run this. OPM 117186
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                if (dataLoan.sLT == E_sLT.FHA)
                {
                    if (dataLoan.sTerm == 360)
                    {
                        return "1240";
                    }
                    else if (dataLoan.sTerm == 180)
                    {
                        return "1210";
                    }
                }
                else if (dataLoan.sLT == E_sLT.UsdaRural)
                {
                    return "3875";
                }
                else if (dataLoan.sLT == E_sLT.VA && dataLoan.sFinMethT == E_sFinMethT.Fixed)
                {
                    // 15 yr fixed
                    if (dataLoan.sTerm == 180)
                    {
                        return "2470";
                    }
                    // 20 yr fixed
                    else if (dataLoan.sTerm == 240)
                    {
                        return "3868";
                    }
                    // 25 yr fixed
                    else if (dataLoan.sTerm == 300)
                    {
                        return "3866";
                    }
                    // 30 yr fixed
                    else if (dataLoan.sTerm == 360 && !dataLoan.sBuydown)
                    {
                        return "2500";
                    }
                }
            }

            return "";
        }

        private string GetMiExemptReasonIdRep(CPageData dataLoan)
        {
            if (dataLoan.sLT == E_sLT.FHA
                || dataLoan.sLT == E_sLT.UsdaRural
                || dataLoan.sLT == E_sLT.VA)
            {
                return "0";
            }
            else if (dataLoan.sLtvRForMi < 80m)
            {
                return "0";
            }
            else if (dataLoan.sLT == E_sLT.Conventional
                     && !String.IsNullOrEmpty(dataLoan.sMiCertId))
            {
                return "0";
            }
            else if (dataLoan.sProdIsDuRefiPlus
                     || dataLoan.sLpProductT == E_sLpProductT.FhlmcReliefRefi)
            {
                return "1";
            }
            else
            {
                return "2";
            }
        }

        private string GetHpmlTypeId(CPageData dataLoan)
        {
            bool isFHA = dataLoan.sLT == E_sLT.FHA;
            switch (dataLoan.sHighPricedMortgageT)
            {
                case E_HighPricedMortgageT.None: return "0";
                case E_HighPricedMortgageT.HigherPricedQmNotHpml: return isFHA ? "2" : "0";
                case E_HighPricedMortgageT.HPML:
                case E_HighPricedMortgageT.HigherPricedQm: return isFHA ? "2" : "1";
                case E_HighPricedMortgageT.Unknown:
                default:
                    return string.Empty;
            }
        }

        // After call on 7/3/13 the loan/loansalefile table participation ids could be merged into one.
        // Related to OPM 125616, 120097. GF
        //private string GetParticipationIdRepForLoanTable(CPageData dataLoan)
        //{
        //    // iServe has been given this participation id for the zip files.
        //    if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
        //    {
        //        return "3337";
        //    }
        //    else
        //    {
        //        return "3150"; // 3150 is the test participation id.
        //    }
        //}

        private string GetOriginatorIdRep(CPageData dataLoan)
        {
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                return "10788";
            }
            else
            {
                return "0";
            }
        }

        #endregion

        private void AddRowToLoanTable(CPageData dataLoan, CAppData dataApp)
        {
            DataRow row = m_loanTable.NewRow();
            var pool = GetMortgagePoolForLoan(dataLoan);
            bool isInterimMode = IsInterimMode(dataLoan, pool, true);

            row["assist_bal"]                       = "0";
            row["assist_pmt"]                       = "0";
            row["attn_msg"]                         = "0";
            row["broker_id"]                        = GetPmlBrokerCompanyId(dataLoan);
            row["curtailment_bal"]                  = "0";
            row["date_funded"]                      = dataLoan.sFundD_rep;
            row["date_next_pmt_due"]                = dataLoan.sServicingNextPmtDue_rep;
            row["escrow_bal"]                       = dataLoan.sProvExpServicingCurrentTotalFunds_Escrow.ToString(m_dollarFormat);
            row["escrow_pmt"]                       = dataLoan.sProvExpServicingEscrowPmt.ToString(m_dollarFormat);
            row["fee_bal"]                          = "0";
            row["fico_score"]                       = dataLoan.sCreditScoreLpeQual.ToString();
            row["first_pmt_date"]                   = dataLoan.sSchedDueD1_rep;
            row["g_fee_pct"]                        = isInterimMode ? "0" : dataLoan.sProvExpGseDeliveryGuaranteeF.ToString(m_rateFormat); // 2/11/2014 gf - opm 150355
            row["hpml_type_id"]                     = GetHpmlTypeId(dataLoan);
            row["initial_aggregate_adj"]            = dataLoan.sProvExpSettlementAggregateAdjRsrv.ToString(m_dollarFormat);
            row["interest_paid"]                    = dataLoan.sProvExpServicingCollectedFunds_Interest.ToString(m_dollarFormat);
            row["interest_pmt"]                     = dataLoan.sProvExpServicingInterestDue.ToString(m_dollarFormat);
            row["interest_rate"]                    = dataLoan.sProvExpNoteIR.ToString(m_rateFormat);
            row["investor_loan_num"]                = isInterimMode ? dataLoan.sLNm : dataLoan.sPurchaseAdviceSummaryInvLoanNm; // Switch from sPurchaseAdviceSummaryInvLoanNm to sLNm OPM 117204. Switch back OPM 126516/120097. // 2/12/2014 gf - see opm 150355
            row["last_pmt_recv_prior_to_boarding"]  = GetLastPaymentReceivedPriorToBoarding(dataLoan);
            row["late_chg_bal"]                     = "0";
            row["lc_type_id"]                       = GetLateChargeIdRep(dataLoan);
            row["lien_position"]                    = GetLienPosTRep(dataLoan.sLienPosT);
            row["loan_amt"]                         = decimal.Round(dataLoan.sFinalLAmt, 2).ToString(m_dollarFormat);
            row["loan_program_id"]                  = GetLoanProgramIdRep(dataLoan);
            row["loan_purpose_id"]                  = GetLoanPurposeIdRep(dataLoan);
            row["maturity_date"]                    = GetMaturityDateRep(dataLoan);
            row["note_custodian_id"]                = GetCustodianIdRep(dataLoan.sGseDeliveryDocumentCustodianName);
            row["old_loan_id"]                      = dataLoan.sLNm;
            row["originator_id"]                    = GetOriginatorIdRep(dataLoan); // OPM 117186
            row["owner_occupied"]                   = GetOwnerOccupiedRep(dataApp.aOccT);
            row["participation_id"]                 = GetParticipationIdRep(dataLoan, GetMortgagePoolForLoan(dataLoan), true);
            row["participation_start_date"]         = dataLoan.sServicingNextPmtDue_rep;
            row["payoff_remit_day_id"]              = dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac ? dataLoan.sGseDeliveryRemittanceNumOfDays_rep : "1"; //OPM 193822
            row["pi_pmt"]                           = dataLoan.sProvExpPIPmtAmt.ToString(m_dollarFormat);
            row["points_paid"]                      = decimal.Round(dataLoan.sSettlementDiscountPointF, 2).ToString(m_dollarFormat);
            row["prepaid_interest"]                 = LosConvert.GfeItemProps_Poc(dataLoan.sSettlementIPiaProps) ? "0" : dataLoan.sSettlementIPia.ToString(m_dollarFormat);
            row["prepay_type_id"]                   = "0";
            row["principal_bal"]                    = dataLoan.sProvExpServicingUnpaidPrincipalBalance.ToString(m_dollarFormat);
            row["principal_pmt"]                    = dataLoan.sServicingNextPmtAmtPrincipal.ToString(m_dollarFormat);
            row["remit_proj_day_id"]                = "16"; //OPM 193822 - Freddie Mac remittance types are not supported at this time.
            row["second_home"]                      = GetSecondHomeRep(dataApp.aOccT);
            row["security_bal"]                     = "0";
            row["service_fee_pct"]                  = isInterimMode ? "0" : dataLoan.sProvExpGseDeliveryServicingFPc.ToString(m_rateFormat); // 2/11/2014 gf - opm 150355
            row["settlement_date"]                  = dataLoan.sLPurchaseD_rep; // OPM 120097
            row["unappl_bal"]                       = "0";
            row["mi_exempt_reason_id"]              = GetMiExemptReasonIdRep(dataLoan);
            row["tx_50a6_refi"]                     = dataLoan.sSpState.Equals("tx", StringComparison.CurrentCultureIgnoreCase) && 
                                                        (dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity) ? "1" : "0";
            row["prior_servicer_id"]                = GetPriorServicerId(dataLoan);

            m_loanTable.Rows.Add(row);
        }

        #region AddRowToPropertyTable helper methods
        private string GetProdSpStructureT_rep(E_sProdSpStructureT type)
        {
            switch (type)
            {
                case E_sProdSpStructureT.Attached: return "1";
                case E_sProdSpStructureT.Detached: return "0";
                default:
                    Tools.LogBug("Unhandled enum value of E_sProdSpStructureT");
                    return "";
            }
        }

        private string GetGSESpTRep(E_sGseSpT type)
        {
            switch (type)
            {
                case E_sGseSpT.PUD: return "3";
                case E_sGseSpT.Condominium:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                case E_sGseSpT.ManufacturedHomeCondominium:
                    return "1";
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return "4";
                default:
                    return "2";
            }
        }

        private string GetCountyIdRep(string countyName, string stateName)
        {
            string key = String.Format("{0},{1}", countyName.TrimWhitespaceAndBOM(), stateName.TrimWhitespaceAndBOM());
            // 9/17/2013 gf - opm 136705 Provident didn't update their county id
            // mapping to include miami dade, so switch it here. The map should probably
            // be changed to map FIPS codes => Provident Codes. Would still need to add
            // Miami-Dade. I am applying the fix here and not in the mappings file 
            // so the mapping represents what is provided by provident.
            if (key.Equals("miami dade,fl", StringComparison.InvariantCultureIgnoreCase))
            {
                key = "dade,fl";
            }
            string idRep = "";
            if (m_countyIdMap.TryGetValue(key, out idRep))
            {
                return idRep;
            }
            else
            {
                return "";
            }
        }

        private Beneficiary GetBeneficiary(CPageData dataLoan)
        {
            if (dataLoan.sDocMagicAlternateLenderId == Guid.Empty)
            {
                string csz = String.Format("{0}, {1}, {2}",
                                           dataLoan.BrokerDB.Address.City,
                                           dataLoan.BrokerDB.Address.State,
                                           dataLoan.BrokerDB.Address.Zipcode);
                return new Beneficiary(dataLoan.BrokerDB.Name,
                                       csz,
                                       dataLoan.BrokerDB.Address.StreetAddress);
            }
            else
            {
                var altLender = new DocMagicAlternateLender(dataLoan.sBrokerId, dataLoan.sDocMagicAlternateLenderId);
                string csz = String.Format("{0}, {1}, {2}", 
                                           altLender.BeneficiaryCity, 
                                           altLender.BeneficiaryState, 
                                           altLender.BeneficiaryZip);
                return new Beneficiary(altLender.BeneficiaryName,
                                       csz,
                                       altLender.BeneficiaryAddress);
                                       
            }
        }
        
        private string GetDeedTrusteeRep(CPageData dataLoan)
        {
            /*
             * Disabling this after a call on 7/3/13. This wasn't appropriate for all states. Related to OPM 126516, 120097. GF
            // For iServe, populate this from the Title Company in Official Contact List.
            // If this agent isn't populated then check the normal location.
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                CAgentFields titleAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.Title, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (!String.IsNullOrEmpty(titleAgent.CompanyName))
                {
                    return titleAgent.CompanyName;
                }
            }
            */

            List<TrustCollection.Trustee> trustees = dataLoan.sTrustCollection.Trustees;
            if (trustees.Any() && !trustees.First().IsEmpty())
            {
                // Be careful about returning null.
                return String.IsNullOrEmpty(trustees.First().Name) ? "" : trustees.First().Name;
            }

            return "";
        }

        private string GetDocDateRep(CPageData dataLoan)
        {
            // iServe wanted this to come from the Document Date of the Note (Status -> General)
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                return dataLoan.sDocumentNoteD_rep;
            }

            return dataLoan.sDeedGeneratedD_rep;
        }

        private string GetOriginalAppraisedValueRep(CPageData dataLoan)
        {
            decimal appraisedValToUse = dataLoan.sApprVal;
            if (dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr)
            {
                appraisedValToUse = dataLoan.sOriginalAppraisedValue;
            }
            return decimal.Round(appraisedValToUse, 2).ToString(m_dollarFormat);
        }

        #endregion
        private struct Beneficiary
        {
            public readonly string Name;
            public readonly string MailCityStreetZip;
            public readonly string MailStreet;

            public Beneficiary(string name, string csz, string street)
            {
                Name = String.IsNullOrEmpty(name) ? String.Empty : name;
                MailCityStreetZip = String.IsNullOrEmpty(csz) ? String.Empty : csz;
                MailStreet = String.IsNullOrEmpty(street) ? String.Empty : street;
            }
        }

        private void AddRowToPropertyTable(CPageData dataLoan)
        {
            DataRow row = m_propertyTable.NewRow();

            row["attached"]                 = GetProdSpStructureT_rep(dataLoan.sProdSpStructureT);
            Beneficiary beneficiary = GetBeneficiary(dataLoan);
            row["beneficiary"]              = beneficiary.Name;
            row["beneficiary_mail_csz"]     = beneficiary.MailCityStreetZip;
            row["beneficiary_mail_street"]  = beneficiary.MailStreet;
            row["census_tract"]             = dataLoan.sHmdaCensusTract;
            row["county_id"]                = GetCountyIdRep(dataLoan.sSpCounty, dataLoan.sSpState);
            row["deed_trustee"]             = GetDeedTrusteeRep(dataLoan);
            row["doc_date"]                 = GetDocDateRep(dataLoan);
            row["estimated_repl_cost"]      = "";
            row["hoa_city"]                 = "";
            row["hoa_contact"]              = "";
            row["hoa_email_addr"]           = "";
            row["hoa_fax_num"]              = "";
            row["hoa_name"]                 = "";
            row["hoa_phone_num"]            = "";
            row["hoa_state_id"]             = "0";
            row["hoa_street1"]              = "";
            row["hoa_street2"]              = "";
            row["hoa_zip"]                  = "";
            // legal_description is an optional field, and Provident couldn't parse
            // our valid csv output for this field, so we're going to leave it out
            // for now. OPM 117186
            row["legal_description"]        = ""; // dataLoan.sSpLegalDesc;
            row["mers_min"]                 = dataLoan.sMersMin;
            row["name_of_project"]          = dataLoan.sProjNm;
            row["num_units"]                = dataLoan.sUnitsNum;
            row["old_loan_id"]              = dataLoan.sLNm;
            row["original_appraised_value"] = GetOriginalAppraisedValueRep(dataLoan);
            row["original_ltv"]             = dataLoan.sProvExpLtvR.ToString(m_rateFormat);
            row["prop_city"]                = dataLoan.sSpCity;
            row["prop_state_id"]            = GetStateIdRep(dataLoan.sSpState);
            row["prop_street1"]             = dataLoan.sSpAddr;
            row["prop_street2"]             = "";
            row["prop_type_id"]             = GetGSESpTRep(dataLoan.sGseSpT);
            row["prop_zip"]                 = dataLoan.sSpZip;
            row["recording_book_num"]       = "";
            row["recording_date"]           = dataLoan.sRecordedD_rep;
            row["recording_page_num"]       = "";
            row["recording_series_num"]     = "";
            row["sales_price"]              = decimal.Round(dataLoan.sPurchPrice, 2).ToString(m_dollarFormat);
            row["vesting"]                  = dataLoan.sVestingToRead;
            var mortgagor = dataLoan.sTitleOnlyBorrowerNames.ElementAtOrDefault(0);
            row["mortgagor_1_first_name"]   = mortgagor == null ? "" : mortgagor.Item1;
            row["mortgagor_1_middle_name"]  = mortgagor == null ? "" : mortgagor.Item2;
            row["mortgagor_1_last_name"]    = mortgagor == null ? "" : mortgagor.Item3;
            mortgagor = dataLoan.sTitleOnlyBorrowerNames.ElementAtOrDefault(1);
            row["mortgagor_2_first_name"]   = mortgagor == null ? "" : mortgagor.Item1;
            row["mortgagor_2_middle_name"]  = mortgagor == null ? "" : mortgagor.Item2;
            row["mortgagor_2_last_name"]    = mortgagor == null ? "" : mortgagor.Item3;
            mortgagor = dataLoan.sTitleOnlyBorrowerNames.ElementAtOrDefault(2);
            row["mortgagor_3_first_name"]   = mortgagor == null ? "" : mortgagor.Item1;
            row["mortgagor_3_middle_name"]  = mortgagor == null ? "" : mortgagor.Item2;
            row["mortgagor_3_last_name"]    = mortgagor == null ? "" : mortgagor.Item3;
            mortgagor = dataLoan.sTitleOnlyBorrowerNames.ElementAtOrDefault(3);
            row["mortgagor_4_first_name"]   = mortgagor == null ? "" : mortgagor.Item1;
            row["mortgagor_4_middle_name"]  = mortgagor == null ? "" : mortgagor.Item2;
            row["mortgagor_4_last_name"]    = mortgagor == null ? "" : mortgagor.Item3;

            m_propertyTable.Rows.Add(row);
        }

        #region AddRowToTaxItemTable helper methods
        private string GetTaxTablePaidByIdRep(E_TaxTablePaidBy paidBy)
        {
            switch (paidBy)
            {
                case E_TaxTablePaidBy.Borrower: return "2";
                case E_TaxTablePaidBy.Lender: return "1";
                case E_TaxTablePaidBy.NoInfo:
                default: return "";
            }
        }

        private string GetTaxTypeIdRep(E_TaxTableTaxT taxType)
        {
            switch (taxType)
            {
                case E_TaxTableTaxT.Borough: return "9";
                case E_TaxTableTaxT.City: return "2";
                case E_TaxTableTaxT.County: return "1";
                case E_TaxTableTaxT.FireDist: return "21";
                case E_TaxTableTaxT.LocalImprovementDist: return "17";
                case E_TaxTableTaxT.Miscellaneous: return "15";
                case E_TaxTableTaxT.MunicipalUtilDist: return "23";
                case E_TaxTableTaxT.School: return "3";
                case E_TaxTableTaxT.SpecialAssessmentDist: return "12";
                case E_TaxTableTaxT.Town: return "6";
                case E_TaxTableTaxT.Township: return "7";
                case E_TaxTableTaxT.Utility: return "5";
                case E_TaxTableTaxT.Village: return "14";
                case E_TaxTableTaxT.WasteFeeDist: return "10";
                case E_TaxTableTaxT.Water_Irrigation: return "4";
                case E_TaxTableTaxT.Water_Sewer: return "11";
                case E_TaxTableTaxT.LeaveBlank:
                default: return "";
            }
        }

        #endregion

        private void AddRowToTaxItemTable(CPageData dataLoan)
        {
            DataRow row = m_taxItemTable.NewRow();

            row["apn"] = "1";
            row["impound_amt"] = "1";
            row["initial_monthly_amt"] = "1";
            row["initial_mos_in_escrow"] = "1";
            row["most_recent_paid"] = "1";
            row["next_to_pay"] = "1";
            row["old_loan_id"] = dataLoan.sLNm;
            row["paid_by_id"] = GetTaxTablePaidByIdRep(dataLoan.sProvExpTaxItemPaidByT);
            row["payee_id"] = "1120";
            row["tax_item_id"] = "1";
            row["tax_type_id"] = "1";
            row["wi_tax_pmt_type_id"] = ""; // OPM 128893

            m_taxItemTable.Rows.Add(row);
        }

        private void AddRowToTaxScheduleTable(CPageData dataLoan)
        {
            DataRow row = m_taxScheduleTable.NewRow();
            
            row["anticipated_due_date"] = dataLoan.sProvExpTaxSchedDueD_rep;
            row["curr_anticipated_amt"] = "1";
            row["old_loan_id"] = dataLoan.sLNm;
            row["pmt_num"] = "1";
            row["tax_item_id"] = "1";
            

            m_taxScheduleTable.Rows.Add(row);
        }

        private void AddRowToGovernmentLoanTable(CPageData dataLoan)
        {
            DataRow row = m_governmentLoanTable.NewRow();

            row["old_loan_id"] = dataLoan.sLNm;

            row["activation_date"]      = dataLoan.sMiCertIssuedD_rep;
            row["adp_code"]             = dataLoan.sFHAADPCode;
            row["base_loan_amt"]        = decimal.Round(dataLoan.sLAmtCalc, 2).ToString(m_dollarFormat);
            string caseNum              = Regex.Replace(dataLoan.sAgencyCaseNum, @"[^a-zA-Z0-9]", "");
            // 10/22/2013 gf - opm 142123 only take first 10 alphanum chars if it's FHA, otherwise, just strip non-alphanum
            row["case_num"]             = (caseNum.Length > 10 && dataLoan.sLT == E_sLT.FHA) ? caseNum.Substring(0, 10) : caseNum;
            row["ufmip"]                = decimal.Round(dataLoan.sFfUfmip1003, 2).ToString(m_dollarFormat);
            row["ufmip_financed"]       = GetUFMIPFinancedRep(dataLoan);
            row["acquisition_price"]    = GetAcquisitionPrice(dataLoan);
            row["prop_owned_12_mos"]    = GetPropOwned12Months(dataLoan);
            row["prior_fha"]            = GetPriorFha(dataLoan);

            m_governmentLoanTable.Rows.Add(row);
        }

        #region AddRowToGovernmentLoanTable helper methods
        private static string GetUFMIPFinancedRep(CPageData dataLoan)
        {
            switch (dataLoan.sLT)
            {
                case E_sLT.FHA:
                    return dataLoan.sFfUfMipIsBeingFinanced ? "1" : "0";
                case E_sLT.VA:
                    return dataLoan.sFfUfMipIsBeingFinanced ? "1" : "0";
                case E_sLT.UsdaRural:
                    return "0";
                default:
                    return string.Empty;
            }
        }

        private string GetAcquisitionPrice(CPageData dataLoan)
        {
            if ( dataLoan.sLT == E_sLT.FHA)
            {
                decimal acquisitionPrice = 0.0M;
                switch (dataLoan.sLPurposeT)
                {
                    case E_sLPurposeT.Purchase:
                        acquisitionPrice = dataLoan.sPurchPrice;
                        break;
                    case E_sLPurposeT.Construct:
                        acquisitionPrice = dataLoan.sLotOrigC;
                        break;
                    case E_sLPurposeT.ConstructPerm:
                        acquisitionPrice = dataLoan.sLotOrigC;
                        break;
                    default:
                        acquisitionPrice = dataLoan.sSpOrigC;
                        break;
                }

                return decimal.Round(acquisitionPrice, 2).ToString(m_dollarFormat);
            }
            
            return string.Empty;
        }

        private string GetPropOwned12Months(CPageData dataLoan)
        {
            switch (dataLoan.sLT)
            {
                case E_sLT.FHA:
                    int acquisitionYear = 0;
                    switch (dataLoan.sLPurposeT)
                    {
                        case E_sLPurposeT.Purchase:
                            return "0";
                        case E_sLPurposeT.Construct:
                            int.TryParse(dataLoan.sLotAcqYr, out acquisitionYear);
                            return acquisitionYear + 1 <= DateTime.Now.Year ? "1" : "0";
                        case E_sLPurposeT.ConstructPerm:
                            int.TryParse(dataLoan.sLotAcqYr, out acquisitionYear);
                            return acquisitionYear + 1 <= DateTime.Now.Year ? "1" : "0";
                        default:
                            int.TryParse(dataLoan.sSpAcqYr, out acquisitionYear);
                            return acquisitionYear + 1 <= DateTime.Now.Year ? "1" : "0";
                    }
                case E_sLT.VA:
                    return "0";
                case E_sLT.UsdaRural:
                    return "0";
                default:
                    return string.Empty;
            }
        }

        private string GetPriorFha(CPageData dataLoan)
        {
            switch (dataLoan.sLT)
            {
                case E_sLT.FHA:
                    switch (dataLoan.sLPurposeT)
                    {
                        case E_sLPurposeT.Purchase:
                            return "0";
                        case E_sLPurposeT.FhaStreamlinedRefinance:
                            return "1";
                        default:
                            return (!string.IsNullOrEmpty(dataLoan.sSpFhaCaseNumCurrentLoan) || !string.IsNullOrEmpty(dataLoan.sFhaEndorsementD_rep)) ? "1" : "0";
                    }
                case E_sLT.VA:
                    return "0";
                case E_sLT.UsdaRural:
                    return "0";
                default:
                    return string.Empty;
            }
        }

        #endregion

        private string GetPoolIdRep(CPageData dataLoan)
        {
            // iServe
            if (dataLoan.sBrokerId.ToString().ToUpper() == "CCCCF4A3-A076-4E74-A195-9D0E5F2E52EB")
            {
                return dataLoan.sCustomField9Notes.TrimWhitespaceAndBOM();
            }

            return "";
        }

        private void AddRowToPoolTable(CPageData dataLoan)
        {
            DataRow row = m_poolTable.NewRow();

            row["old_loan_id"] = dataLoan.sLNm;
            row["pool_id"]     = GetPoolIdRep(dataLoan);

            m_poolTable.Rows.Add(row);
        }
        
        #endregion
        
    }
}
