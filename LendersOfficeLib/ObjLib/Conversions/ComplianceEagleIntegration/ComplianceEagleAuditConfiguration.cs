﻿// <copyright file="ComplianceEagleAuditConfiguration.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   08/09/2014 7:30 PM 
// </summary>
namespace LendersOffice.Conversions.ComplianceEagleIntegration
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using ObjLib.Conversions;

    /// <summary>
    /// Provides data objects for the ComplianceEagle automated audit configuration.
    /// </summary>
    public class ComplianceEagleAuditConfiguration
    {
        /// <summary>
        /// Default status at which loans should experience automated audits.
        /// </summary>
        public const E_sStatusT DefaultBeginStatus = E_sStatusT.Loan_Processing;

        /// <summary>
        /// The statuses that we want to exclude by default for new configurations.
        /// A lender may choose to enable some of these manually through the settings.
        /// </summary>
        public static readonly IEnumerable<E_sStatusT> DefaultExcludedStatuses = new[]
        {
            E_sStatusT.Loan_Canceled,
            E_sStatusT.Loan_OnHold,
            E_sStatusT.Loan_Rejected,
            E_sStatusT.Loan_Withdrawn,
            E_sStatusT.Loan_CounterOffer,
            E_sStatusT.Loan_Archived,
            E_sStatusT.Loan_LoanPurchased
        };

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplianceEagleAuditConfiguration" /> class.
        /// </summary>
        /// <param name="beginRunningAuditsLoanStatus">The loan status at which to begin sending audit requests to ComplianceEagle.</param>
        /// <param name="excludedStatuses">The loan statuses the lender wants to exclude from automated audit requests to ComplianceEagle.</param>
        public ComplianceEagleAuditConfiguration(E_sStatusT beginRunningAuditsLoanStatus, IEnumerable<E_sStatusT> excludedStatuses)
        {
            this.BeginRunningAuditsStatus = beginRunningAuditsLoanStatus;
            this.ExcludedStatuses = excludedStatuses;
        }

        /// <summary>
        /// Gets the loan status at which to begin sending audit requests to ComplianceEagle. The default is Processing.
        /// </summary>
        /// <value>The loan status for which ComplianceEagle audit requests should begin transmitting to CE.</value>
        public E_sStatusT BeginRunningAuditsStatus
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the loan statuses that should not trigger compliance analysis.
        /// </summary>
        /// <value>The loan statuses that should not trigger compliance analysis.</value>
        public IEnumerable<E_sStatusT> ExcludedStatuses
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a list of loan statuses during which the ComplianceEagle audit service should not send any requests to CE.
        /// </summary>
        /// <value>If a loan file is in one of these loan statuses then the ComplianceEagle audit service should not send any requests to CE for that loan.</value>
        public IEnumerable<E_sStatusT> IgnoreLoanStatuses
        {
            get
            {
                return ComplianceExportStatusConfig.GetIgnoredLoanStatuses(
                    this.BeginRunningAuditsStatus,
                    this.ExcludedStatuses);
            }
        }

        /// <summary>
        /// Returns a boolean indicating whether the given loan status is included in the list of loan statuses that should exclude the loan from automated ComplianceEagle export.
        /// </summary>
        /// <param name="loanStatus">The loan status [sStatusT] of a given loan file.</param>
        /// <returns>True if the loan status is included in the list of statuses that mark the loan as excluded from automated ComplianceEagle export. Otherwise False.</returns>
        public bool ShouldIgnoreLoan(E_sStatusT loanStatus)
        {
            return this.IgnoreLoanStatuses.Contains(loanStatus);
        }
    }
}
