﻿// <copyright file="ComplianceEagleRequestProvider.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   7/28/2014 05:00 PM 
// </summary>

namespace LendersOffice.Conversions.ComplianceEagleIntegration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using ComplianceEagle;
    using ComplianceEagle.QuestsoftComplianceReportRequest;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions.GenericMismoClosing26;
    using LendersOffice.Conversions.Mismo3.Version4;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Security;

    /// <summary>
    /// Instantiates a ComplianceEagle request object and maps the required data from the lender account and loan file.
    /// </summary>
    public class ComplianceEagleRequestProvider
    {
        /// <summary>
        /// This is used to lock the thread when generating the list of field dependencies for the request.
        /// </summary>
        private static object fieldLock = new object();

        /// <summary>
        /// The select statement provider for the request, which is stored here so that the dependent fields only need to be loaded once.
        /// </summary>
        private static CSelectStatementProvider selectStatementProvider;

        /// <summary>
        /// The lender's QuestSoft customer ID. 
        /// </summary>
        private string customerID = string.Empty;

        /// <summary>
        /// An instance of the application data object.
        /// </summary>
        private CAppData dataApp = null;

        /// <summary>
        /// An instance of the loan data object.
        /// </summary>
        private CPageData dataLoan = null;

        /// <summary>
        /// The sLId of the loan file.
        /// </summary>
        private Guid loanID = Guid.Empty;

        /// <summary>
        /// The unique ID for the transaction.
        /// </summary>
        private Guid transactionId = Guid.Empty;

        /// <summary>
        /// The user's QuestSoft account password.
        /// </summary>
        private string passWord = string.Empty;

        /// <summary>
        /// True if the product was already ordered and is being re-ordered.
        /// </summary>
        private bool reorderRequest = false;

        /// <summary>
        /// True if the product is being reissued for review.
        /// </summary>
        private bool reviewRequest = false;

        /// <summary>
        /// The user's QuestSoft account.
        /// </summary>
        private string userName = string.Empty;

        /// <summary>
        /// A collection of products included in this order.
        /// </summary>
        private List<ComplianceEagleProductT> orderedProducts = new List<ComplianceEagleProductT>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplianceEagleRequestProvider"/> class.
        /// </summary>
        /// <param name="username">The user's QuestSoft ComplianceEagle account.</param>
        /// <param name="password">The user's QuestSoft ComplianceEagle account password.</param>
        /// <param name="customerIdent">The ComplianceEagle customer ID of the lender.</param>
        /// <param name="loanIdent">The sLId of the LQB loan file.</param>
        /// <param name="productList">A collection of products to include in the order.</param>
        public ComplianceEagleRequestProvider(
            string username,
            string password,
            string customerIdent,
            Guid loanIdent,
            List<ComplianceEagleProductT> productList = null)
        {
            this.loanID = loanIdent;
            this.GetLoanDataWithDependencies();

            this.GetAppAndSetBorrowerMode(0, true);
            
            this.userName = username;
            this.passWord = password;

            this.customerID = customerIdent;
            this.transactionId = Guid.NewGuid();

            if (productList == null || !productList.Any())
            {
                productList = DefaultProductList;
            }

            this.orderedProducts = productList;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplianceEagleRequestProvider"/> class.  This constructor is for testing.
        /// </summary>
        /// <param name="username">The user's QuestSoft ComplianceEagle account.</param>
        /// <param name="password">The user's QuestSoft ComplianceEagle account password.</param>
        /// <param name="customerIdent">The ComplianceEagle customer ID of the lender.</param>
        /// <param name="loan">The loan to export with <see cref="CPageData.InitLoad()"/> already run.</param>
        /// /// <param name="productList">A collection of products to include in the order.</param>
        public ComplianceEagleRequestProvider(
            string username,
            string password,
            string customerIdent,
            CPageData loan,
            List<ComplianceEagleProductT> productList = null)
        {
            this.loanID = loan.sLId;
            this.dataLoan = loan;
            this.userName = username;
            this.passWord = password;
            this.customerID = customerIdent;

            this.transactionId = Guid.NewGuid();

            if (productList == null || !productList.Any())
            {
                productList = DefaultProductList;
            }

            this.orderedProducts = productList;
        }

        /// <summary>
        /// Gets the default product list for a ComplianceEagle order.
        /// </summary>
        /// <remarks>
        /// By default, only the Mavent Review is ordered. This is used primarily for asynchronous requests.
        /// </remarks>
        public static List<ComplianceEagleProductT> DefaultProductList
        {
            get
            {
                return new List<ComplianceEagleProductT> { ComplianceEagleProductT.MaventReview };
            }
        }

        /// <summary>
        /// Gets the <see cref="CSelectStatementProvider"/> required to load the data included in the data wrapper of the ComplianceEagle request.
        /// </summary>
        /// <value>A <see cref="CSelectStatementProvider"/> containing the fields required for the ComplianceEagle request data wrapper.</value>
        public static CSelectStatementProvider SelectStatementProvider
        {
            get
            {
                lock (fieldLock)
                {
                    if (selectStatementProvider != null)
                    {
                        return selectStatementProvider;
                    }

                    IEnumerable<string> ceagleFields = CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(ComplianceEagleRequestProvider));
                    IEnumerable<string> extraLoanFields = ComplianceEagleUtil.NormalizeFieldListForDependencyLookup(ComplianceEagleUtil.GetLoanFieldList());
                    IEnumerable<string> extraAppFields = ComplianceEagleUtil.NormalizeFieldListForDependencyLookup(ComplianceEagleUtil.GetAppFieldList());

                    HashSet<string> fields = new HashSet<string>(ceagleFields);

                    if (extraLoanFields != null)
                    {
                        fields.UnionWith(extraLoanFields);
                    }

                    if (extraAppFields != null)
                    {
                        fields.UnionWith(extraAppFields);
                    }

                    selectStatementProvider = CSelectStatementProvider.GetProviderForTargets(fields, expandTriggersIfNeeded: false);
                    return selectStatementProvider;
                }
            }
        }

        /// <summary>
        /// Gets the ID for the transaction.
        /// </summary>
        public Guid TransactionId => this.transactionId;

        /// <summary>
        /// Gets the current loan status of the loan file.
        /// </summary>
        /// <value>The loan file's loan status [sStatusT].</value>
        public E_sStatusT? LoanStatus
        {
            get
            {
                if (this.dataLoan == null)
                {
                    return null;
                }

                return this.dataLoan.sStatusT;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the loan file is valid or is deleted.
        /// </summary>
        /// <value>True of IsValid. Otherwise (loan is deleted) returns false.</value>
        public bool ValidLoan
        {
            get
            {
                if (this.dataLoan == null)
                {
                    return false;
                }

                return this.dataLoan.IsValid;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the loan estimate data is in a valid state.
        /// There are some files that may have a loan estimate date marked as intitial
        /// but associated with an invliad archive. These files are considered to have
        /// invalid loan estimate data and should not be exported until the user fixes
        /// the issue.
        /// </summary>
        /// <value>True if the loan estimate data is valid. False if invalid.</value>
        public bool IsLoanEstimateDataValid
        {
            get
            {
                if (this.dataLoan == null)
                {
                    return true;
                }
                else if (this.dataLoan.sDisclosureRegulationT != E_sDisclosureRegulationT.TRID)
                {
                    return true;
                }

                return this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID &&
                    !this.dataLoan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive;
            }
        }

        /// <summary>
        /// Creates a ComplianceEagle request.
        /// </summary>
        /// <param name="initialOrder">Indicates whether this is the initial order or a subsequent re-order.</param>
        /// <param name="isReviewOrder">Indicates whether this is a review order.</param>
        /// <param name="principal">Abstract User Principal object.</param>
        /// <returns>A request string containing MISMO 2.6 formatted data with an integration-specific data wrapper.</returns>
        public string CreateComplianceEagleRequest(bool initialOrder, bool isReviewOrder, AbstractUserPrincipal principal)
        {
            this.reorderRequest = !initialOrder;
            this.reviewRequest = isReviewOrder;

            string request = string.Empty;
            QuestsoftComplianceReportRequest complianceReportRequest = this.CreateQSComplianceReportRequest();
            
            byte[] bytes = null;
            int contentLength = 0;

            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;

                using (XmlWriter writer = XmlWriter.Create(stream, writerSettings))
                {
                    complianceReportRequest.WriteXml(writer);
                    writer.Flush();
                    contentLength = (int)stream.Position;
                }

                bytes = stream.GetBuffer();
            }

            request = Encoding.UTF8.GetString(bytes, 0, contentLength);
            string mismoXML = string.Empty;
            var payloadFormat = this.DeterminePayloadFormat();

            if (payloadFormat == MismoPayloadFormat.Mismo34)
            {
                var payloadOptions = new Mismo34ExporterOptions()
                {
                    LoanId = this.dataLoan.sLId,
                    VendorAccountId = complianceReportRequest.Authentication.CustomerID,
                    IncludeIntegratedDisclosureExtension = true,
                    Principal = principal
                };

                var exporter = new Mismo34Exporter(payloadOptions);
                mismoXML = exporter.SerializePayload();
            }
            else if (payloadFormat == MismoPayloadFormat.Mismo33)
            {
                mismoXML = Mismo3.Version3.Mismo33RequestProvider.SerializeMessage(
                    this.dataLoan.sLId, 
                    complianceReportRequest.Authentication.CustomerID, 
                    transactionID: null, 
                    docPackage: null, 
                    includeIntegratedDisclosureExtension: true,
                    principal: principal);
            }
            else
            {
                mismoXML = GenericMismoClosing26Exporter.ExportCustom(this.dataLoan.sLId, "ComplianceEagle", principal);
            }

            // 9/9/2014 BB - This escapes any $ characters so that they will be treated as literals by Regex.Replace
            mismoXML = mismoXML.Replace("$", "$$").TrimWhitespaceAndBOM();

            request = Regex.Replace(request, "<LOAN_PLACEHOLDER[\\s]*/>", mismoXML);

            return request;
        }

        /// <summary>
        /// Creates a ComplianceEagle data wrapper.
        /// </summary>
        /// <returns>A data wrapper object for the Compliance Eagle request.</returns>
        public QuestsoftComplianceReportRequest CreateQSComplianceReportRequest()
        {
            QuestsoftComplianceReportRequest complianceReportRequest = new QuestsoftComplianceReportRequest();

            complianceReportRequest.Authentication = this.CreateAuthentication();
            complianceReportRequest.General = this.CreateGeneral();
            complianceReportRequest.LoanData = this.CreateLoanData();

            return complianceReportRequest;
        }

        /// <summary>
        /// Creates an authentication object with lender ID and credentials for QS, and LQB integrator ID.
        /// </summary>
        /// <returns>An authentication object.</returns>
        private Authentication CreateAuthentication()
        {
            Authentication auth = new Authentication();
            
            auth.CustomerID = this.customerID;
            auth.IntegratorID = "LendingQB"; //// Confirmed with QS that this just serves to identify the request source. Therefore there is no need to add it to stage.
            auth.Password = this.passWord;
            auth.Username = this.userName;
            auth.Version = "1.0";

            return auth;
        }

        /// <summary>
        /// Creates a General object with product, loan, and branch information for the request.
        /// </summary>
        /// <returns>An object with product, loan, and branch information for the request.</returns>
        private General CreateGeneral()
        {
            General general = new General();

            general.BranchCode = this.dataLoan.BranchCode;
            general.LoanID = this.dataLoan.sLNm;
            general.LoanStatusCode = ComplianceEagleDataMap.ToCEagle(this.dataLoan.sStatusT, this.dataLoan.sWithdrawnD, this.dataLoan.sApprovD);
            general.LoanStatusDate = this.dataLoan.sStatusD_rep;
            general.RequestCode = this.GenerateRequestCode();
            general.RequestMethod = E_RequestMethod.PDFXML;
            general.VendorTransactionId = this.transactionId.ToString();

            return general;
        }

        /// <summary>
        /// Retrieves the ComplianceEagle request code including all selected products.
        /// </summary>
        /// <returns>The full request code.</returns>
        private string GenerateRequestCode()
        {
            if (!this.orderedProducts.Any())
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("No products selected for this ComplianceEagle order."));
            }

            var requestCodes = this.orderedProducts.Select(p => this.GetRequestCodeForSingleProduct(p));
            return string.Join(",", requestCodes);
        }

        /// <summary>
        /// Retrieves the request code for a single product, along with any relevant process modifiers.
        /// </summary>
        /// <param name="product">A ComplianceEagle product.</param>
        /// <returns>The request code for that product.</returns>
        private string GetRequestCodeForSingleProduct(ComplianceEagleProductT product)
        {
            var code = ComplianceEagleUtil.GetRequestCodeForProduct(product);

            if (this.reviewRequest)
            {
                code = $"{code}:REVIEW";
            }
            else if (this.reorderRequest)
            {
                code = $"{code}:REORDER";
            }

            return code;
        }

        /// <summary>
        /// Creates a LoanData object with the MISMO 2.6 and lists of key-value pairs for LQB fields not supported by MISMO 2.6.
        /// </summary>
        /// <returns>A LoanData object.</returns>
        private LoanData CreateLoanData()
        {
            LoanData mismoLoanData = new LoanData();
            string[] appFields = ComplianceEagleUtil.GetAppFieldList();

            this.GetAppAndSetBorrowerMode(0, true);
            int numApps = this.dataLoan.nApps;

            foreach (string field in mismoLoanData.ExtraFieldList)
            {
                mismoLoanData.KeyValuePairList.Add(this.CreateKVPairDynamically(field));
            }

            for (int appIndex = 0; appIndex < numApps; appIndex++)
            {
                this.GetAppAndSetBorrowerMode(appIndex, true);

                if (!this.dataApp.aBIsValidNameSsn && !this.dataApp.aCIsValidNameSsn)
                {
                    continue;
                }

                if (!this.dataApp.aBIsValidNameSsn)
                {
                    this.dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                }

                mismoLoanData.BorrowerList.Add(this.CreateBorrower(appFields));
            }

            // Reset app and borrower mode.
            this.GetAppAndSetBorrowerMode(0, true);

            mismoLoanData.LoanPlaceholder = new LOAN_PLACEHOLDER(); // <LOAN_PLACEHOLDER />

            return mismoLoanData;
        }

        /// <summary>
        /// Creates a Borrower object with key-value pairs for app-level fields not support by MISMO 2.6.
        /// </summary>
        /// <param name="appFields">An array of app-level field names to be included as KVPairs within the Borrower element.</param>
        /// <returns>A borrower object.</returns>
        private Borrower CreateBorrower(string[] appFields)
        {
            Borrower borrower = new Borrower();

            borrower.BorrowerID = this.dataApp.aMismoId;

            foreach (string field in appFields)
            {
                if (string.IsNullOrEmpty(field))
                {
                    continue;
                }

                borrower.KeyValuePairList.Add(this.CreateKVPairDynamically(field));
            }

            return borrower;
        }

        /// <summary>
        /// Uses reflection to create a KVPair with field name and value.
        /// </summary>
        /// <param name="fieldID">The LQB field id to load.</param>
        /// <returns>A KVPair with field name and value.</returns>
        private KVPair CreateKVPairDynamically(string fieldID)
        {
            KVPair keyValuePair = new KVPair();
            string invalidFieldMessage = string.Format("{0} Invalid field ID: {1}", ErrorMessages.ComplianceEagle.InvalidFieldID, fieldID ?? "Field ID was omitted.");

            if (string.IsNullOrEmpty(fieldID))
            {
                throw new CBaseException(invalidFieldMessage, invalidFieldMessage);
            }

            fieldID = fieldID.TrimWhitespaceAndBOM();
            keyValuePair.Name = fieldID;

            if (PageDataUtilities.ContainsField(fieldID))
            {
                try
                {
                    string value = PageDataUtilities.GetValue(this.dataLoan, this.dataApp, fieldID);
                    E_PageDataFieldType fieldType;

                    if (PageDataUtilities.GetFieldType(fieldID, out fieldType))
                    {
                        if (fieldType == E_PageDataFieldType.Bool)
                        {
                            // MISMO 2.6 compliant.
                            if (value == "Yes")
                            {
                                value = "Y";
                            }
                            else
                            {
                                value = "N";
                            }
                        }
                    }

                    keyValuePair.Value = value;
                }
                catch (CBaseException exc)
                {
                    Tools.LogError("Error " + fieldID, exc);
                    throw new CBaseException(invalidFieldMessage, invalidFieldMessage);
                }
            }
            else
            {
                throw new CBaseException(invalidFieldMessage, invalidFieldMessage);
            }

            return keyValuePair;
        }

        /// <summary>
        /// Loads an application to the dataApp member and sets the borrower mode to B|C.
        /// </summary>
        /// <param name="appIndex">The zero-based application index.</param>
        /// <param name="isBorrower">True for borrower mode, false for co-borrower mode.</param>
        private void GetAppAndSetBorrowerMode(int appIndex, bool isBorrower)
        {
            if (this.dataLoan.nApps > appIndex)
            {
                this.dataApp = this.dataLoan.GetAppData(appIndex);
            }

            this.dataApp.BorrowerModeT = isBorrower ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;
        }

        /// <summary>
        /// Initialize a CPageData object with the necessary dependencies to load all of the fields for the data wrapper.
        /// </summary>
        private void GetLoanDataWithDependencies()
        {
            this.dataLoan = new CFullAccessPageData(this.loanID, nameof(ComplianceEagleRequestProvider), SelectStatementProvider);
            this.dataLoan.InitLoad();
            this.dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
        }

        /// <summary>
        /// Determine which MISMO format to export.
        /// </summary>
        /// <returns>The payload format to be exported.</returns>
        private MismoPayloadFormat DeterminePayloadFormat()
        {
            if (this.dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy
                || this.dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE)
            {
                return MismoPayloadFormat.Mismo26;
            }
            else if (this.dataLoan.sBorrowerApplicationCollectionT != E_sLqbCollectionT.Legacy && this.dataLoan.BrokerDB.ComplianceEagleEnableMismo34)
            {
                return MismoPayloadFormat.Mismo34;
            }
            else
            {
                return MismoPayloadFormat.Mismo33;
            }
        }
    }
}
