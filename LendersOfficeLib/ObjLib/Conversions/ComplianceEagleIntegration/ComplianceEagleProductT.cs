﻿namespace LendersOffice.Conversions.ComplianceEagleIntegration
{
    /// <summary>
    /// A collection of products that can be ordered from ComplianceEagle.
    /// </summary>
    public enum ComplianceEagleProductT
    {
        Undefined,
        MaventReview,
        HighCost,
        MaventNmls,
        HmdaCheck,
        Geocode,
        RateSpreadCheck,
        CraReview,
        FraudComplete,
        FraudBorrower,
        FraudCollateral,
        ExclusionaryList,
        ServiceProviderSearch,
        FloodQuickCheck,
        Flood,
        RecordingFee,
        LefCheck,
        IncomeReview,
        SsnReview,
        QmReview,
        AvmReview,
        AusReview,
        NmlsMcrReview
    }
}
