﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:asp="remove">
    <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes" />
  <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
  <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />  
  <xsl:template match="/">
      <xsl:apply-templates select="CEReports"/>
  </xsl:template>
  <xsl:template match="CEReports">
    <div align="left">
      <xsl:variable name="ReportList" select="Report">
      </xsl:variable>
      <xsl:if test="count($ReportList) &gt; 0">
      <table style="BORDER-COLLAPSE: collapse;" width="*" border="1" cellspacing="0" cellpadding="4">
        <xsl:for-each select="$ReportList">
            <xsl:if test="(@Service != '') and (@Status != '')">
              <tr>
                <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Service</td>
                <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Date Performed</td>
                <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Status</td>
                <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Report ID</td>
                <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">ComplianceEagle Loan ID</td>
                <td style="background-color:#5778ab;"></td>
              </tr>
              <tr>
                <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                  <xsl:value-of select="@Service"/>
                </td>
                <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                  <xsl:value-of select="@Performed"/>
                </td>
                <xsl:variable name="Status">
                  <xsl:value-of select="@Status"/>
                </xsl:variable>
                <xsl:choose>
                  <xsl:when test="translate($Status, $smallcase, $uppercase) = 'ALERT'">
                    <td style="font-weight:bold; color:red; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@Status"/>
                    </td>
                  </xsl:when>
                  <xsl:when test="translate($Status, $smallcase, $uppercase) = 'FAIL'">
                    <td style="font-weight:bold; color:red; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@Status"/>
                    </td>
                  </xsl:when>
                  <xsl:when test="translate($Status, $smallcase, $uppercase) = 'FAILEXCEPTION'">
                    <td style="font-weight:bold; color:red; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@Status"/>
                    </td>
                  </xsl:when>
                  <xsl:when test="translate($Status, $smallcase, $uppercase) = 'USERINPUT'">
                    <td style="font-weight:bold; color:red; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@Status"/>
                    </td>
                  </xsl:when>
                  <xsl:when test="translate($Status, $smallcase, $uppercase) = 'REVIEW'">
                    <td style="font-weight:bold; color:orange; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@Status"/>
                    </td>
                  </xsl:when>
                  <xsl:when test="translate($Status, $smallcase, $uppercase) = 'WARNING'">
                    <td style="font-weight:bold; color:orange; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@Status"/>
                    </td>
                  </xsl:when>
                  <xsl:when test="translate($Status, $smallcase, $uppercase) = 'PASS'">
                    <td style="font-weight:bold; color:green; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@Status"/>
                    </td>
                  </xsl:when>
                  <xsl:otherwise>
                    <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@Status"/>
                    </td>
                  </xsl:otherwise>
                </xsl:choose>
                <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                  <xsl:value-of select="@ID"/>
                </td>
                <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                  <xsl:value-of select="@CEID"/>
                </td>
                <td>
                  <xsl:variable name="URLRPT">
                    <xsl:value-of select="@URLRPT"/>
                  </xsl:variable>
                  <span style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                    <a href='#' onclick="window.open('{$URLRPT}')">CLICK HERE TO ACCESS COMPLIANCEEAGLE</a>
                  </span>
                </td>
              </tr>
            <tr>
              <td colspan="6"></td>
            </tr>
          </xsl:if>
        </xsl:for-each>
        <xsl:if test="Error">
          <tr>
            <td colspan="6" style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Errors</td>
          </tr>
          <tr>
            <td colspan="6">
              <ul>
                <xsl:for-each select="Error">
                  <li style="color: red;">
                    <xsl:value-of select="text()"/>
                  </li>
                </xsl:for-each>
              </ul>
            </td>
          </tr>
        </xsl:if>
      </table>
      </xsl:if>
    </div>
  </xsl:template>
</xsl:stylesheet>
