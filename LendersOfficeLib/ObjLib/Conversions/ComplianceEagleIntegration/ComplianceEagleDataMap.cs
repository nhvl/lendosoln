﻿// <copyright file="ComplianceEagleDataMap.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   7/28/2014 05:00 PM 
// </summary>

namespace LendersOffice.Conversions.ComplianceEagleIntegration
{
    using System.Collections.Generic;
    using ComplianceEagle;
    using DataAccess;

    /// <summary>
    /// Converts LQB data to ComplianceEagle format.
    /// </summary>
    public static class ComplianceEagleDataMap
    {
        /// <summary>
        /// Converts the LQB loan status to ComplianceEagle LoanStatusCode.
        /// Active: The Final Disposition has not been Assigned for this Application.
        /// Cancelled: The loan application was Cancelled.
        /// Closed: The loan has reached a closed status.
        /// Denied: The loan application was denied by the Lender.
        /// Incomplete File: Incomplete application.  {Faxed Lock, Duplicate Loan, etc..}.
        /// Withdrawn: The loan application was withdrawn by the applicant prior to Approval.
        /// Withdrawn After Approval: The loan application was withdrawn by the applicant after Approval.
        /// </summary>
        /// <param name="loanStatus">The loan's $sStatusT$.</param>
        /// <param name="withdrawnDate">The loan's $sWithdrawnD$.</param>
        /// <param name="approvedDate">The loan's $sApprovD$.</param>
        /// <returns>The ComplianceEagle LoanStatusCode.</returns>
        public static E_LoanStatusCode ToCEagle(E_sStatusT loanStatus, CDateTime withdrawnDate, CDateTime approvedDate)
        {
            switch (loanStatus)
            {
                case E_sStatusT.Lead_New: 
                case E_sStatusT.Lead_Other:
                case E_sStatusT.Loan_Approved:
                case E_sStatusT.Loan_ClearToClose:
                case E_sStatusT.Loan_ClearToPurchase:
                case E_sStatusT.Loan_Closed:
                case E_sStatusT.Loan_ConditionReview:
                case E_sStatusT.Loan_CounterOffer:
                case E_sStatusT.Loan_Docs:
                case E_sStatusT.Loan_DocsBack:
                case E_sStatusT.Loan_DocsDrawn:
                case E_sStatusT.Loan_DocsOrdered:
                case E_sStatusT.Loan_DocumentCheck:
                case E_sStatusT.Loan_DocumentCheckFailed:
                case E_sStatusT.Loan_FinalDocs:
                case E_sStatusT.Loan_FinalUnderwriting:
                case E_sStatusT.Loan_Funded:
                case E_sStatusT.Loan_FundingConditions:
                case E_sStatusT.Loan_InFinalPurchaseReview:
                case E_sStatusT.Loan_InPurchaseReview:
                case E_sStatusT.Loan_InvestorConditions:
                case E_sStatusT.Loan_InvestorConditionsSent:
                case E_sStatusT.Loan_LoanSubmitted:
                case E_sStatusT.Loan_OnHold:
                case E_sStatusT.Loan_Open:
                case E_sStatusT.Loan_Other:
                case E_sStatusT.Loan_Preapproval:
                case E_sStatusT.Loan_PreDocQC:
                case E_sStatusT.Loan_PreProcessing:
                case E_sStatusT.Loan_PrePurchaseConditions:
                case E_sStatusT.Loan_Prequal:
                case E_sStatusT.Loan_PreUnderwriting:
                case E_sStatusT.Loan_Processing:
                case E_sStatusT.Loan_Purchased:
                case E_sStatusT.Loan_ReadyForSale:
                case E_sStatusT.Loan_Recorded:
                case E_sStatusT.Loan_Registered:
                case E_sStatusT.Loan_Shipped:
                case E_sStatusT.Loan_SubmittedForFinalPurchaseReview:
                case E_sStatusT.Loan_SubmittedForPurchaseReview:
                case E_sStatusT.Loan_Suspended:
                case E_sStatusT.Loan_Underwriting:
                case E_sStatusT.Loan_WebConsumer:
                    return E_LoanStatusCode.Active;
                case E_sStatusT.Lead_Canceled:
                case E_sStatusT.Loan_Canceled:
                    return E_LoanStatusCode.Cancelled;
                case E_sStatusT.Loan_Archived:
                case E_sStatusT.Loan_LoanPurchased: // "Loan Sold" - should be safe to assume that this is an endpoint regardless of biz channel.
                    return E_LoanStatusCode.Closed;
                case E_sStatusT.Lead_Declined:
                case E_sStatusT.Loan_Rejected:
                    return E_LoanStatusCode.Denied;
                case E_sStatusT.Loan_Withdrawn:
                    return (withdrawnDate.CompareTo(approvedDate, false) < 0) ? E_LoanStatusCode.Withdrawn : E_LoanStatusCode.WithdrawnAfterApproval;
                default:
                    throw new UnhandledEnumException(loanStatus);
            }
        }
        
        /// <summary>
        /// Converts the status returned by ComplianceEagle for a given report to LQB compliance status format.
        /// The ComplianceEase status type was extended to support ComplianceEagle.
        /// </summary>
        /// <param name="complianceEagleReportStatus">The status returned by ComplianceEagle.</param>
        /// <returns>The LQB compliance status [E_sComplianceEaseStatusT] that corresponds to the status from ComplianceEagle.</returns>
        public static E_sComplianceEaseStatusT ToLQB(E_CEReportStatusEnum complianceEagleReportStatus)
        {
            switch (complianceEagleReportStatus)
            {
                case E_CEReportStatusEnum.Alert:
                case E_CEReportStatusEnum.ALERT:
                    return E_sComplianceEaseStatusT.Alert;
                case E_CEReportStatusEnum.Delayed:
                case E_CEReportStatusEnum.DELAYED:
                    return E_sComplianceEaseStatusT.Delayed;
                case E_CEReportStatusEnum.Fail:
                case E_CEReportStatusEnum.FAIL:
                    return E_sComplianceEaseStatusT.Fail;
                case E_CEReportStatusEnum.FailException:
                case E_CEReportStatusEnum.FAILEXCEPTION:
                    return E_sComplianceEaseStatusT.FailException;
                case E_CEReportStatusEnum.Pass:
                case E_CEReportStatusEnum.PASS:
                    return E_sComplianceEaseStatusT.Pass;
                case E_CEReportStatusEnum.Review:
                case E_CEReportStatusEnum.REVIEW:
                    return E_sComplianceEaseStatusT.Review;
                case E_CEReportStatusEnum.Undefined:
                    return E_sComplianceEaseStatusT.Undefined;
                case E_CEReportStatusEnum.UserInput:
                case E_CEReportStatusEnum.USERINPUT:
                    return E_sComplianceEaseStatusT.UserInput;
                case E_CEReportStatusEnum.Warning:
                case E_CEReportStatusEnum.WARNING:
                    return E_sComplianceEaseStatusT.Warning;
                default:
                    throw new UnhandledEnumException(complianceEagleReportStatus);
            }
        }

        /// <summary>
        /// Converts the statuses returned by ComplianceEagle for a given list of reports to a worst-case LQB compliance status.
        /// The ComplianceEase status type was extended to support ComplianceEagle.
        /// </summary>
        /// <param name="complianceEagleReportStatus">A list of ComplianceEagle report statuses.</param>
        /// <returns>The LQB compliance status [E_sComplianceEaseStatusT] that corresponds to the worst-case status from ComplianceEagle for a given set of reports.</returns>
        public static E_sComplianceEaseStatusT ToLQBWorstCase(List<E_CEReportStatusEnum> complianceEagleReportStatus)
        {
            E_sComplianceEaseStatusT worstCase = E_sComplianceEaseStatusT.Undefined;
            List<E_sComplianceEaseStatusT> status = new List<E_sComplianceEaseStatusT>(complianceEagleReportStatus.Count);

            foreach (E_CEReportStatusEnum reportStatus in complianceEagleReportStatus)
            {
                status.Add(ToLQB(reportStatus));
            }

            if (status.Contains(E_sComplianceEaseStatusT.FailException))
            {
                worstCase = E_sComplianceEaseStatusT.FailException;
            }
            else if (status.Contains(E_sComplianceEaseStatusT.Fail))
            {
                worstCase = E_sComplianceEaseStatusT.Fail;
            }
            else if (status.Contains(E_sComplianceEaseStatusT.Alert))
            {
                worstCase = E_sComplianceEaseStatusT.Alert;
            }
            else if (status.Contains(E_sComplianceEaseStatusT.UserInput))
            {
                worstCase = E_sComplianceEaseStatusT.UserInput;
            }
            else if (status.Contains(E_sComplianceEaseStatusT.Warning))
            {
                worstCase = E_sComplianceEaseStatusT.Warning;
            }
            else if (status.Contains(E_sComplianceEaseStatusT.Review))
            {
                worstCase = E_sComplianceEaseStatusT.Review;
            }
            else if (status.Contains(E_sComplianceEaseStatusT.Delayed))
            {
                worstCase = E_sComplianceEaseStatusT.Delayed;
            }
            else if (status.Contains(E_sComplianceEaseStatusT.Pass))
            {
                worstCase = E_sComplianceEaseStatusT.Pass;
            }

            //// Any unexpected status will result in value Undefined.

            return worstCase;
        }
    }
}
