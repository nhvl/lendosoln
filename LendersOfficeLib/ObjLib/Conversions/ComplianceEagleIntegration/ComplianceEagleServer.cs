﻿// <copyright file="ComplianceEagleServer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   7/28/2014 05:00 PM 
// </summary>

namespace LendersOffice.Conversions.ComplianceEagleIntegration
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using ComplianceEagle;
    using ComplianceEagle.QuestsoftComplianceReportResponse;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Security;

    /// <summary>
    /// Sends a ComplianceEagle request to QuestSoft and gets a response.
    /// </summary>
    public static class ComplianceEagleServer
    {
        /// <summary>
        /// Transmits a request XML packet to QuestSoft ComplianceEagle.
        /// </summary>
        /// <param name="brokerID">The BrokerID associated with the user submitting the request.</param>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="applicationID">The aAppId of the application.</param>
        /// <param name="transactionId">The ID for the transaction.</param>
        /// <param name="requestXML">The XML packet to send.</param>
        /// <param name="orderedProducts">A list of products ordered by the user.</param>
        /// <param name="successSummary">A summary of any successful submissions.</param>
        /// <param name="transmissionErrors">Any errors that occurred during transmission.</param>
        /// <param name="principal">The user principal.</param>
        /// <returns>True if the submission to ComplianceEagle was successful. False if there was an error.</returns>
        public static bool SubmitRequest(
            Guid brokerID,
            Guid loanID,
            Guid applicationID,
            Guid transactionId,
            string requestXML,
            List<ComplianceEagleProductT> orderedProducts,
            out string successSummary,
            out string transmissionErrors,
            AbstractUserPrincipal principal)
        {
            return SubmitRequest(brokerID, loanID, applicationID, transactionId, requestXML, orderedProducts, out successSummary, out transmissionErrors, principal, asynchronous: false);
        }

        /// <summary>
        /// Transmits a request XML packet to QuestSoft ComplianceEagle. Used to transmit requests asynchronously via a DB message queue.
        /// </summary>
        /// <param name="brokerID">The BrokerID associated with the lender that the loan file belongs to.</param>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="applicationID">The aAppId of the primary application on the loan file.</param>
        /// <param name="username">The username to send to ComplianceEagle in the request packet.</param>
        /// <param name="password">The password to send to ComplianceEagle in the request packet.</param>
        /// <param name="fileVersion">The current loan file version.</param>
        /// <param name="customerID">The ComplianceEagle Customer ID of the lender that the loan belongs to.</param>
        /// <param name="principal">Abstract User Principal object.</param>
        public static void SubmitAsyncRequest(Guid brokerID, Guid loanID, Guid applicationID, string username, string password, int fileVersion, string customerID, AbstractUserPrincipal principal)
        {
            ComplianceEagleRequestProvider requestProvider = null;
            ComplianceEagleAuditConfiguration auditConfig = null;
            string requestXML = string.Empty;

            BrokerDB lender = BrokerDB.RetrieveById(brokerID);
            bool shouldSendRequest = false;

            shouldSendRequest = ComplianceEagleUtil.ValidateAsyncRequestFromQueue(
                brokerID,
                loanID,
                username,
                fileVersion,
                customerID,
                lender.ComplianceEagleUserName,
                lender.ComplianceEagleCompanyID,
                lender.BillingVersion,
                lender.IsEnableComplianceEagleIntegration,
                lender.IsEnablePTMComplianceEagleAuditIndicator);

            if (shouldSendRequest)
            {
                requestProvider = new ComplianceEagleRequestProvider(username, password, customerID, loanID);

                shouldSendRequest = requestProvider.ValidLoan;
            }

            if (shouldSendRequest && requestProvider != null && !requestProvider.IsLoanEstimateDataValid)
            {
                var statusHelper = new ComplianceEagleStatusHelper(loanID);
                statusHelper.UpdateLoan_ComplianceStatusOnly(E_sComplianceEaseStatusT.Fail);

                shouldSendRequest = false;
            }

            if (shouldSendRequest)
            {
                auditConfig = lender.ComplianceEagleAuditConfiguration;

                shouldSendRequest = (requestProvider.LoanStatus != null) && !auditConfig.ShouldIgnoreLoan(requestProvider.LoanStatus ?? E_sStatusT.Loan_Open);
            }

            if (shouldSendRequest)
            {
                requestXML = requestProvider.CreateComplianceEagleRequest(initialOrder: false, isReviewOrder: false, principal: principal);
                ComplianceEagleStatusHelper checksumChecker = new ComplianceEagleStatusHelper(loanID, requestXML);

                shouldSendRequest = !checksumChecker.IsSameChecksum;
            }

            if (shouldSendRequest)
            {
                var orderedProducts = ComplianceEagleRequestProvider.DefaultProductList;

                string successSummary;
                string transmissionErrors;

                SubmitRequest(
                    brokerID,
                    loanID,
                    applicationID,
                    requestProvider.TransactionId,
                    requestXML,
                    orderedProducts,
                    out successSummary,
                    out transmissionErrors,
                    principal: null,
                    asynchronous: true);
            }
        }

        /// <summary>
        /// Transmits a request XML packet to QuestSoft ComplianceEagle.
        /// </summary>
        /// <param name="brokerID">The BrokerID associated with the user submitting the request.</param>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="applicationID">The aAppId of the application.</param>
        /// <param name="transactionId">The ID of the transaction.</param>
        /// <param name="requestXML">The XML packet to send.</param>
        /// <param name="orderedProducts">A list of products ordered.</param>
        /// <param name="successSummary">A summary of any successful submissions.</param>
        /// <param name="transmissionErrors">Any errors that occurred during transmission.</param>
        /// <param name="principal">The user principal.</param>
        /// <param name="asynchronous">True if the request originates from an async process such as the DB message queue. False otherwise.</param>
        /// <returns>True if the submission to ComplianceEagle was successful (even partially). False if the order could not be processed.</returns>
        private static bool SubmitRequest(
            Guid brokerID,
            Guid loanID,
            Guid applicationID,
            Guid transactionId,
            string requestXML,
            List<ComplianceEagleProductT> orderedProducts,
            out string successSummary,
            out string transmissionErrors,
            AbstractUserPrincipal principal,
            bool asynchronous)
        {
            Guid? userId;
            if (principal == null)
            {
                if (!asynchronous)
                {
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("Synchronous ComplianceEagle requests must have an associated principal"));
                }

                userId = new Guid(ConstStage.ComplianceEaglePTMEditorUserId);
            }
            else
            {
                userId = principal.UserId;
            }

            if (Tools.IsLogLargeRequest(requestXML.Length))
            {
                Tools.LogInfo("ComplianceEagle_Request", ComplianceEagleUtil.FormatRequestForLogging(requestXML));
            }
            else
            {
                Tools.LogInfo("ComplianceEagle_Request", requestXML.Length + " bytes is exceeding logging threshold.");
            }

            CEReports response = null;
            string responseToLog = string.Empty;
            int docsUploaded = 0;
            successSummary = string.Empty;
            transmissionErrors = string.Empty;
            bool success = true;

            byte[] bytes = Encoding.UTF8.GetBytes(requestXML);
            var webRequest = CreateWebRequest(bytes.Length);

            string ceagleErrorMessage = string.Empty;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            try
            {
                response = GetWebResponse(webRequest);
            }
            catch (CBaseException exc)
            {
                Tools.LogError(exc);
                throw;
            }
            finally
            {
                sw.Stop();
                Tools.LogInfo("ComplianceEagle GetResponseTook: " + sw.ElapsedMilliseconds + "ms.");
            }

            if (response != null)
            {
                responseToLog = ComplianceEagleUtil.FormatResponseForLogging(response);
                Tools.LogInfo("ComplianceEagle_Response", responseToLog);
            }
            else
            {
                transmissionErrors = ErrorMessages.ComplianceEagle.ParseError;
                Tools.LogInfo("ComplianceEagle_Response", successSummary);

                response = new CEReports();
                success = false;
            }

            var reportList = response.ReportList.CoalesceWithEmpty().ToList();

            // The ComplianceEase status enum has been extended to support ComplianceEagle.
            var reportStatuses = new List<E_CEReportStatusEnum>();

            foreach (Report ceagleReport in reportList)
            {
                if (ceagleReport.PDF == null)
                {
                    continue;
                }

                if (ComplianceEagleUtil.SaveEDoc(brokerID, loanID, applicationID, userId, ceagleReport.PDF.CDataContents, ceagleReport.Service, asynchronous))
                {
                    docsUploaded++;
                }

                reportStatuses.Add(ceagleReport.Status);
            }

            if (response.Error != null)
            {
                if (!string.IsNullOrEmpty(response.Error.ErrorMessage))
                {
                    transmissionErrors += $"{Environment.NewLine}An error was encountered: {response.Error.ErrorMessage}";

                    if (!reportList.Any() || reportList.All(r => r.Service == E_CEReportServiceEnum.Undefined))
                    {
                        success = false;
                    }
                }
            }

            if (docsUploaded > 0)
            {
                successSummary += $"Review complete. {docsUploaded} documents were uploaded to EDocs.";
            }

            ComplianceEagleStatusHelper loanSetter = new ComplianceEagleStatusHelper(loanID, requestXML);
            loanSetter.UpdateLoan(responseToLog, reportStatuses, reportList);

            if (!asynchronous && success)
            {
                ComplianceEagleUtil.RecordTransactionToDb(principal, loanID, transactionId, orderedProducts, reportList);
            }

            return success;
        }

        /// <summary>
        /// Initializes an HTTP web request.
        /// </summary>
        /// <param name="requestLength">The length of the request when represented as a byte array.</param>
        /// <returns>An HTTP web request initialized for transmission to ComplianceEagle.</returns>
        private static HttpWebRequest CreateWebRequest(int requestLength)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(GetExportPath());
            
            webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
            webRequest.Method = "POST";
            webRequest.Timeout = 200000;
            webRequest.ContentType = "application/xml";
            webRequest.ContentLength = requestLength;
            
            return webRequest;
        }

        /// <summary>
        /// Retrieves the response from ComplianceEagle.
        /// </summary>
        /// <param name="webRequest">The HTTP web request to which the request XML has been written.</param>
        /// <returns>The response as a CEReports object.</returns>
        private static CEReports GetWebResponse(HttpWebRequest webRequest)
        {
            try
            {
                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        return (CEReports)SerializationHelper.XmlDeserialize(stream, typeof(CEReports));
                    }
                }
            }
            catch (InvalidOperationException exc)
            {
                Tools.LogBug("Failed to deserialize the ComplianceEagle response.");
                throw new CBaseException(ErrorMessages.ComplianceEagle.ParseError, exc);
            }
        }

        /// <summary>
        /// This method is used to grab the raw response for debugging purposes.
        /// </summary>
        /// <param name="webRequest">The HTTP web request to which the request XML has been written.</param>
        /// <returns>The response as a string.</returns>
        private static string GetWebResponseAsString(HttpWebRequest webRequest)
        {
            StringBuilder responseString = new StringBuilder();

            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    byte[] buffer = new byte[60000];
                    int size = stream.Read(buffer, 0, buffer.Length);

                    while (size > 0)
                    {
                        string chunk = System.Text.Encoding.UTF8.GetString(buffer, 0, size);
                        responseString.Append(chunk);
                        size = stream.Read(buffer, 0, buffer.Length);
                    }
                }
            }

            return responseString.ToString();
        }

        /// <summary>
        /// Looks up the ComplianceEagle URL based on the domain (production | not).
        /// </summary>
        /// <returns>The URL as a string.</returns>
        private static string GetExportPath()
        {
            return (ConstAppDavid.CurrentServerLocation == ServerLocation.Production) ? ConstStage.ComplianceEagleProductionURL : ConstStage.ComplianceEagleTestURL;
        }
    }
}
