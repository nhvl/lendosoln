﻿// <copyright file="ComplianceEagleStatusHelper.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   8/10/2014 09:20 PM 
// </summary>
namespace LendersOffice.Conversions.ComplianceEagleIntegration
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using System.Text;
    using System.Text.RegularExpressions;
    using ComplianceEagle;
    using ComplianceEagle.QuestsoftComplianceReportResponse;
    using DataAccess;
    using FloodOrder;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Provides access to loan data resultant from the ComplianceEagle export, such as checksum, status, and error data.
    /// </summary>
    public class ComplianceEagleStatusHelper
    {
        /// <summary>
        /// A hash of the ComplianceEagle request XML.
        /// </summary>
        private string checksumValue = string.Empty;

        /// <summary>
        /// An instance of the loan data object.
        /// </summary>
        private CPageData dataLoan = null;

        /// <summary>
        /// The sLId of the loan file.
        /// </summary>
        private Guid loanID = Guid.Empty;

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplianceEagleStatusHelper"/> class.
        /// </summary>
        /// <param name="loanIdent">The sLId of the LQB loan file.</param>
        /// <param name="requestXML">The ComplianceEagle request as an XML string.</param>
        public ComplianceEagleStatusHelper(Guid loanIdent, string requestXML)
        {
            this.Init(loanIdent);

            if (!string.IsNullOrEmpty(requestXML))
            {
                SHA256 sha = new SHA256Managed();
                StringBuilder checksumSB = new StringBuilder();

                var hashBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(System.Text.RegularExpressions.Regex.Replace(requestXML, "<Authentication[^>]*>", string.Empty)));

                foreach (byte b in hashBytes)
                {
                    checksumSB.Append(b.ToString("X2"));
                }

                this.checksumValue = checksumSB.ToString();
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ComplianceEagleStatusHelper"/> class.
        /// <para>Only use this version of the constructor if there is no associated request object to produce a checksum, as when calling GetResponseHTMLFromResponseXML to convert the stored XML response.</para>
        /// </summary>
        /// <param name="loanIdent">The sLId of the LQB loan file.</param>
        public ComplianceEagleStatusHelper(Guid loanIdent)
        {
            this.Init(loanIdent);
        }

        /// <summary>
        /// Gets a value indicating whether this ComplianceEagleStatusHelper has a computed checksum from a request XML string, i.e. whether the associated version of the constructor was called when the object was instantiated.
        /// </summary>
        /// <value>True if a checksum was computed from a request XML string. Otherwise false.</value>
        public bool HasChecksum
        {
            get
            {
                return !string.IsNullOrEmpty(this.checksumValue);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the checksum for the present request matches the one stored on the loan file from the prior request.
        /// </summary>
        /// <value>True if the current checksum matches the previous one stored on the loan file. Otherwise false.</value>
        public bool IsSameChecksum
        {
            get
            {
                if (this.dataLoan == null)
                {
                    return false;
                }

                return this.checksumValue.Equals(this.dataLoan.sComplianceEaseChecksumValue);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the loan's loan estimate data is currently invalid.
        /// </summary>
        /// <value>True if the data is invalid. Otherwise, false.</value>
        public bool IsLoanEstimateDateDataCurrentlyInvalid
        {
            get
            {
                if (this.dataLoan == null)
                {
                    return false;
                }

                return this.dataLoan.sIsInitialLoanEstimateDateAssociatedWithInvalidArchive;
            }
        }

        /// <summary>
        /// Uses an XSLT to convert the XML response from ComplianceEagle to HTML.
        /// The Compliance Ease Error Message field [sComplianceEaseErrorMessage] has been repurposed to store the XML result.
        /// </summary>
        /// <returns>An HTML string converted from the most recent ComplianceEagle response.</returns>
        public string GetResponseHTMLFromResponseXML()
        {
            StringBuilder htmlStringBuilder = new StringBuilder();

            if (!string.IsNullOrEmpty(this.dataLoan.sComplianceEaseErrorMessage))
            {
                try
                {
                    using (TextReader inputReader = new StringReader(this.dataLoan.sComplianceEaseErrorMessage))
                    {
                        using (TextWriter outputWriter = new StringWriter(htmlStringBuilder))
                        {
                            XslTransformHelper.TransformFromEmbeddedResource("LendersOffice.ObjLib.Conversions.ComplianceEagleIntegration.ComplianceEagleResult.xslt", inputReader, outputWriter, null);
                        }
                    }
                }
                catch (System.Xml.XmlException xmlExc)
                {
                    Tools.LogError("The compliance result stored in sComplianceEaseErrorMessage could not be parsed as ComplianceEagle XML. LoanID: " + this.loanID.ToString(), xmlExc);
                }
                catch (System.Xml.Xsl.XsltException xsltExc)
                {
                    Tools.LogError("The ComplianceEagle xslt transform threw an exception for LoanID: " + this.loanID.ToString(), xsltExc);
                }
            }

            return htmlStringBuilder.ToString();
        }

        /// <summary>
        /// Updates the loan's compliance status without changing the checksum or the error message.
        /// </summary>
        /// <param name="status">The new compliance status.</param>
        public void UpdateLoan_ComplianceStatusOnly(E_sComplianceEaseStatusT status)
        {
            this.dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            this.dataLoan.sComplianceEaseStatus = status.ToString("G");
            bool oldSuppressClosingCostSetFlush = this.dataLoan.SuppressClosingCostSetFlush;
            this.dataLoan.SuppressClosingCostSetFlush = true;
            this.dataLoan.Save();
            this.dataLoan.SuppressClosingCostSetFlush = oldSuppressClosingCostSetFlush;
        }

        /// <summary>
        /// Save the given values to the loan file along with the checksum of the request.
        /// </summary>
        /// <param name="complianceEagleResponseXML">The response from ComplianceEagle as an XML string.</param>
        /// <param name="complianceEagleReportStatus">A list of ComplianceEagle report statuses.</param>
        /// <param name="reports">A collection of reports containing data that should be saved to the loan.</param>
        public void UpdateLoan(
            string complianceEagleResponseXML,
            List<E_CEReportStatusEnum> complianceEagleReportStatus,
            List<Report> reports)
        {
            this.dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            this.dataLoan.DisableComplianceAutomation = true;
            E_sComplianceEaseStatusT worstCaseStatus = ComplianceEagleDataMap.ToLQBWorstCase(complianceEagleReportStatus);
            string status = worstCaseStatus.ToString("G");

            this.dataLoan.sComplianceEaseErrorMessage = complianceEagleResponseXML;
            this.dataLoan.sComplianceEaseStatus = status;

            if (this.HasChecksum)
            {
                this.dataLoan.sComplianceEaseChecksum = status + ":" + this.checksumValue;
            }

            foreach (var report in reports)
            {
                if (report.Service == E_CEReportServiceEnum.CRAReview
                    || report.Service == E_CEReportServiceEnum.GEOCODE)
                {
                    this.ProcessCraReviewOrGeocodeData(report, this.dataLoan);
                }
                else if (report.Service == E_CEReportServiceEnum.Flood)
                {
                    this.ProcessFloodData(report, this.dataLoan);
                }
            }

            bool oldSuppressClosingCostSetFlush = this.dataLoan.SuppressClosingCostSetFlush;
            this.dataLoan.SuppressClosingCostSetFlush = true;
            this.dataLoan.Save();
            this.dataLoan.SuppressClosingCostSetFlush = oldSuppressClosingCostSetFlush;
        }

        /// <summary>
        /// Saves data from a CRA Review or GEOCODE report to the loan.
        /// </summary>
        /// <param name="report">The report containing the data.</param>
        /// <param name="dataLoan">A loan object.</param>
        private void ProcessCraReviewOrGeocodeData(Report report, CPageData dataLoan)
        {
            foreach (var field in (report?.Data?.FieldList).CoalesceWithEmpty())
            {
                if (field.Name.Equals("Geocodes.STCODE"))
                {
                    dataLoan.sHmdaStateCode = field.Value;
                }
                else if (field.Name.Equals("Geocodes.MACODE"))
                {
                    dataLoan.sHmdaMsaNum = field.Value;
                }
                else if (field.Name.Equals("Geocodes.CNTYCODE"))
                {
                    dataLoan.sHmdaCountyCode = field.Value;
                }
                else if (field.Name.Equals("Geocodes.TRACT"))
                {
                    dataLoan.sHmdaCensusTract = field.Value;
                }
            }
        }

        /// <summary>
        /// Saves data from a Flood report to the loan.
        /// </summary>
        /// <param name="report">The report containing the data.</param>
        /// <param name="dataLoan">A loan object.</param>
        private void ProcessFloodData(Report report, CPageData dataLoan)
        {
            foreach (var field in (report?.Data?.FieldList).CoalesceWithEmpty())
            {
                if (field.Name.Equals("Cert.CertificateNumber"))
                {
                    dataLoan.sFloodCertId = field.Value;
                }
                else if (field.Name.Equals("Cert.CertificateDate"))
                {
                    try
                    {
                        dataLoan.sFloodCertificationDeterminationD_rep = field.Value;
                    }
                    catch (FieldInvalidValueException exc)
                    {
                        Tools.LogError(ErrorMessages.ComplianceEagle.InvalidDateValue(field.Value), exc);
                    }
                }
                else if (field.Name == "Cert.LifeOfLoan")
                {
                    dataLoan.sFloodCertificationIsLOLUpgraded = field.Value.Equals("Yes", StringComparison.OrdinalIgnoreCase);
                }
                else if (field.Name.Equals("Cert.CommunityName"))
                {
                    dataLoan.sFloodHazardCommunityDesc = field.Value;
                }
                else if (field.Name.Equals("Cert.CommunityNumber"))
                {
                    dataLoan.sFloodCertificationCommunityNum = field.Value;
                }
                else if (field.Name.Equals("Cert.PanelNumber"))
                {
                    Match match = FloodResponseConvert.MapNumPanelSuffixRegex.Match(field.Value);
                    if (field.Value.Length > 6 && match.Success)
                    {
                        dataLoan.sFloodCertificationMapNum = match.Groups["map"].Value;
                        dataLoan.sFloodCertificationPanelNums = match.Groups["panel"].Value;
                        dataLoan.sFloodCertificationPanelSuffix = match.Groups["suffix"].Value;
                    }
                }
                else if (field.Name.Equals("Cert.PanelEffDate"))
                {
                    try
                    {
                        dataLoan.sFloodCertificationMapD_rep = field.Value;
                    }
                    catch (FieldInvalidValueException exc)
                    {
                        Tools.LogError(ErrorMessages.ComplianceEagle.InvalidDateValue(field.Value), exc);
                    }
                }
                else if (field.Name.Equals("Cert.FloodZone"))
                {
                    dataLoan.sNfipFloodZoneId = field.Value;
                }
                else if (field.Name.Equals("Cert.CensusTract"))
                {
                    dataLoan.sHmdaCensusTract = field.Value;
                }
                else if (field.Name.Equals("Cert.MSA"))
                {
                    dataLoan.sHmdaMsaNum = field.Value;
                }
                else if (field.Name.Equals("Cert.CountyFIPSCode"))
                {
                    dataLoan.sHmdaCountyCode = field.Value;
                }
                else if (field.Name.Equals("Cert.StateFIPSCode"))
                {
                    dataLoan.sHmdaStateCode = field.Value;
                }
                else if (field.Name.Equals("Cert.COBRADate"))
                {
                    try
                    {
                        dataLoan.sFloodCertificationDesignationD_rep = field.Value;
                    }
                    catch (FieldInvalidValueException exc)
                    {
                        Tools.LogError(ErrorMessages.ComplianceEagle.InvalidDateValue(field.Value), exc);
                    }
                }
                else if (field.Name.Equals("Cert.NFIPCommunityParticipationType"))
                {
                    if (field.Value.Equals("Regular", StringComparison.OrdinalIgnoreCase))
                    {
                        dataLoan.sFloodCertificationParticipationStatus = "R";
                    }
                    else if (field.Value.Equals("Emergency", StringComparison.OrdinalIgnoreCase))
                    {
                        dataLoan.sFloodCertificationParticipationStatus = "E";
                    }
                    else if (field.Value.Equals("Probation", StringComparison.OrdinalIgnoreCase))
                    {
                        dataLoan.sFloodCertificationParticipationStatus = "P";
                    }
                    else if (field.Value.Equals("Suspended", StringComparison.OrdinalIgnoreCase))
                    {
                        dataLoan.sFloodCertificationParticipationStatus = "S";
                    }
                    else
                    {
                        dataLoan.sFloodCertificationParticipationStatus = "N";
                    }
                }
                else if (field.Name.Equals("Cert.Provider.Name"))
                {
                    if (field.Value.Equals("CoreLogic Flood Services", StringComparison.OrdinalIgnoreCase))
                    {
                        dataLoan.sFloodCertificationPreparerT = E_FloodCertificationPreparerT.FirstAm_Corelogic;
                    }
                    else if (field.Value.Equals("LPS National Flood", StringComparison.OrdinalIgnoreCase))
                    {
                        dataLoan.sFloodCertificationPreparerT = E_FloodCertificationPreparerT.ServiceLink;
                    }
                    else if (field.Value.Equals("Lereta, LLC", StringComparison.OrdinalIgnoreCase))
                    {
                        dataLoan.sFloodCertificationPreparerT = E_FloodCertificationPreparerT.Leretta;
                    }
                    else
                    {
                        dataLoan.sFloodCertificationPreparerT = E_FloodCertificationPreparerT.Other;
                        dataLoan.sFloodCertificationPreparerOtherName = field.Value;
                    }
                }
                else if (field.Name.Equals("Cert.SFHA"))
                {
                    dataLoan.sFloodCertificationIsInSpecialArea = field.Value.Equals("Yes", StringComparison.OrdinalIgnoreCase);
                }
            }
        }

        /// <summary>
        /// Initializes the ComplianceEagleStatusHelper.
        /// </summary>
        /// <param name="loanIdent">The sLId of the LQB loan file.</param>
        private void Init(Guid loanIdent)
        {
            this.loanID = loanIdent;
            this.dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.loanID, typeof(ComplianceEagleStatusHelper));
            this.dataLoan.InitLoad();
        }
    }
}
