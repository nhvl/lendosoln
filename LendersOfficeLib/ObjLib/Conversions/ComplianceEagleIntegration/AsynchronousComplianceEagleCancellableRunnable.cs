﻿// <copyright file="AsynchronousComplianceEagleCancellableRunnable.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   08/07/2014 05:00 PM 
// </summary>

namespace LendersOffice.Conversions.ComplianceEagleIntegration
{
    using System;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.Security;

    /// <summary>
    /// Implements asynchronous requests from a DB message queue to QuestSoft ComplianceEagle.
    /// </summary>
    public class AsynchronousComplianceEagleCancellableRunnable : CommonProjectLib.Runnable.ICancellableRunnable
    {
        /// <summary>
        /// Gets a description of the runnable service.
        /// </summary>
        /// <value>A description of the asynchronous request service for the ComplianceEagle integration.</value>
        public string Description
        {
            get
            {
                return "Runs ComplianceEagle on loans queued in ComplianceEagleAutoSubmitQueue.";
            }
        }

        /// <summary>
        /// Run ComplianceEagle on loans queued in ComplianceEagleAutoSubmitQueue.
        /// </summary>
        /// <param name="cancellationToken">A cancellation token that can be used to cancel the work.</param>
        public void Run(CancellationToken cancellationToken)
        {
            Tools.SetupServicePointManager();
            Tools.SetupServicePointCallback();
            DBMessageQueue mQ = new DBMessageQueue(ConstMsg.ComplianceEagleAutoSubmitQueue);
            string userName, password;

            while (!cancellationToken.IsCancellationRequested)
            {
                Tools.ResetLogCorrelationId();
                bool success = false;
                DBMessage message = null;
                Credentials creds = null;

                try
                {
                    message = mQ.Receive();

                    if (message == null)
                    {
                        return; // Empty Queue.
                    }

                    Thread.CurrentPrincipal = SystemUserPrincipal.ComplianceEagleSystemUser;

                    creds = ObsoleteSerializationHelper.JsonDeserialize<Credentials>(message.Data);
                    userName = creds.Username;
                    password = creds.Password;

                    string loanID = message.Subject1;
                    string fileVersion = message.Subject2;
                    Guid brokerID = GetBrokerIdByLoanId(loanID);
                    Guid applicationID = GetPrimaryAppIdByLoanId(brokerID, loanID);
                    string ceagleCompanyID = GetComplianceEagleCompanyId(brokerID);

                    System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

                    //// message.Subject1 is the loan ID [sLId]; message.Subject2 is the loan version number [sFileVersion].
                    Tools.LogInfo("[CEAGLEQUEUE]  Running " + loanID + " for fileVersion " + fileVersion);
                    sw.Start();

                    ComplianceEagleServer.SubmitAsyncRequest(brokerID, new Guid(loanID), applicationID, userName, password, Convert.ToInt32(fileVersion), ceagleCompanyID, SystemUserPrincipal.ComplianceEagleSystemUser);
                    sw.Stop();

                    Tools.LogInfo(string.Format("[CEAGLEQUEUE] Processed slid {0} in {1}ms", loanID, sw.ElapsedMilliseconds));
                    success = true;
                }
                catch (DBMessageQueueException)
                {
                    return;
                }
                catch (CBaseException exc)
                {
                    string slid = "n/a";

                    if (message != null)
                    {
                        slid = message.Subject1;
                    }

                    Tools.LogError("[CEAGLEQUEUE] ERROR with loan " + slid, exc);
                    throw;
                }
                catch (Exception e)
                {
                    if (message != null)
                    {
                        Tools.LogError("[CEAGLEQUEUE] ERROR slid: " + message.Subject1, e);
                    }

                    throw;
                }
                finally
                {
                    if ((message != null) && !success && (creds != null))
                    {
                        creds.AttemptCount++;

                        //// Add it back to the queue for retry.
                        if (creds.AttemptCount < 5)
                        {
                            mQ.Send(message.Subject1, ObsoleteSerializationHelper.JsonSerialize(creds), message.Subject2);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Uses the loanId to lookup the BrokerID of the associated lender.
        /// </summary>
        /// <param name="loanId">The sLId of the loan file.</param> 
        /// <returns>The BrokerID of the given loan file.</returns>
        private static Guid GetBrokerIdByLoanId(string loanId)
        {
            try
            {
                Guid guidLoanId = new Guid(loanId);

                Guid brokerId = Guid.Empty;

                DbConnectionInfo.GetConnectionInfoByLoanId(guidLoanId, out brokerId);

                return brokerId;
            }
            catch (LoanNotFoundException)
            {
            }
            catch (FormatException)
            {
            }

            throw new CBaseException(ErrorMessages.ComplianceEagle.BrokerIDLookUpFailed, ErrorMessages.ComplianceEagle.BrokerIDLookUpFailed);
        }

        /// <summary>
        /// Uses the loanId to lookup the application ID [aAppId] of the primary application on the loan file.
        /// </summary>
        /// <param name="brokerId">The broker id of the loan file.</param>
        /// <param name="loanId">The sLId of the loan file.</param> 
        /// <returns>The application ID of the primary app on the given loan file.</returns>
        private static Guid GetPrimaryAppIdByLoanId(Guid brokerId, string loanId)
        {
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@LoanId", loanId) };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "App_GetPrimaryAppIdFromLoanId", para))
            {
                if (reader.Read())
                {
                    return (Guid)reader["aAppId"];
                }
            }

            throw new CBaseException(ErrorMessages.ComplianceEagle.AppIDLookUpFailed, ErrorMessages.ComplianceEagle.AppIDLookUpFailed);
        }

        /// <summary>
        /// Uses the BrokerID of the lender to lookup their ComplianceEagle Company ID.
        /// </summary>
        /// <param name="brokerId">The Broker ID of the lender.</param>
        /// <returns>The ComplianceEagle Company ID of the given lender.</returns>
        private static string GetComplianceEagleCompanyId(Guid brokerId)
        {
            SqlParameter[] para = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "GetComplianceEagleCompanyIdByBrokerId", para))
            {
                if (reader.Read())
                {
                    string customerID = reader["ComplianceEagleCompanyID"].ToString();

                    if (string.IsNullOrEmpty(customerID))
                    {
                        throw new CBaseException(
                            ErrorMessages.ComplianceEagle.CustomerIDLookUpFailed,
                            ErrorMessages.ComplianceEagle.CustomerIDLookUpFailed + " Review the lender configuration and fill out their customer ID.");
                    }

                    return customerID;
                }
            }

            throw new CBaseException(ErrorMessages.ComplianceEagle.CustomerIDLookUpFailed, ErrorMessages.ComplianceEagle.CustomerIDLookUpFailed);
        }
    }
}