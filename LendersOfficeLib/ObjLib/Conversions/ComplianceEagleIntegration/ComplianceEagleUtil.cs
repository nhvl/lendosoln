﻿// <copyright file="ComplianceEagleUtil.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Brian Beery
//    Date:   7/30/2014 11:31 AM 
// </summary>
namespace LendersOffice.Conversions.ComplianceEagleIntegration
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Web.UI.WebControls;
    using System.Xml;
    using ComplianceEagle;
    using ComplianceEagle.QuestsoftComplianceReportRequest;
    using ComplianceEagle.QuestsoftComplianceReportResponse;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Edocs.AutoSaveDocType;
    using Security;

    /// <summary>
    /// Provides helper methods for the ComplianceEagle integration.
    /// </summary>
    public static class ComplianceEagleUtil
    {
        /// <summary>
        /// Maps a ComplianceEagle service enum to an orderable product.
        /// </summary>
        private static readonly Dictionary<E_CEReportServiceEnum, ComplianceEagleProductT> ProductByServiceEnum
            = new Dictionary<E_CEReportServiceEnum, ComplianceEagleProductT>()
            {
                // These services represent current products that can be ordered directly.
                [E_CEReportServiceEnum.AUSReview] = ComplianceEagleProductT.AusReview,
                [E_CEReportServiceEnum.AVMReview] = ComplianceEagleProductT.AvmReview,
                [E_CEReportServiceEnum.CRAReview] = ComplianceEagleProductT.CraReview,
                [E_CEReportServiceEnum.ExclusionaryList] = ComplianceEagleProductT.ExclusionaryList,
                [E_CEReportServiceEnum.Flood] = ComplianceEagleProductT.Flood,
                [E_CEReportServiceEnum.FloodQuickCheck] = ComplianceEagleProductT.FloodQuickCheck,
                [E_CEReportServiceEnum.FraudBorrower] = ComplianceEagleProductT.FraudBorrower,
                [E_CEReportServiceEnum.FraudCollateral] = ComplianceEagleProductT.FraudCollateral,
                [E_CEReportServiceEnum.FraudComplete] = ComplianceEagleProductT.FraudComplete,
                [E_CEReportServiceEnum.GEOCODE] = ComplianceEagleProductT.Geocode,
                [E_CEReportServiceEnum.HighCost] = ComplianceEagleProductT.HighCost,
                [E_CEReportServiceEnum.HMDACheck] = ComplianceEagleProductT.HmdaCheck,
                [E_CEReportServiceEnum.IncomeReview] = ComplianceEagleProductT.IncomeReview,
                [E_CEReportServiceEnum.LEFCheck] = ComplianceEagleProductT.LefCheck,
                [E_CEReportServiceEnum.MaventNMLS] = ComplianceEagleProductT.MaventNmls,
                [E_CEReportServiceEnum.MaventReview] = ComplianceEagleProductT.MaventReview,
                [E_CEReportServiceEnum.NMLSMCRReview] = ComplianceEagleProductT.NmlsMcrReview,
                [E_CEReportServiceEnum.QMReview] = ComplianceEagleProductT.QmReview,
                [E_CEReportServiceEnum.RateSpreadCheck] = ComplianceEagleProductT.RateSpreadCheck,
                [E_CEReportServiceEnum.RecordingFee] = ComplianceEagleProductT.RecordingFee,
                [E_CEReportServiceEnum.ServiceProviderSearch] = ComplianceEagleProductT.ServiceProviderSearch,
                [E_CEReportServiceEnum.SSNReview] = ComplianceEagleProductT.SsnReview,

                // These services are special codes, deprecated services, or sub-services.
                [E_CEReportServiceEnum.AvailableServices] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.ComplianceSummaryReport] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.FHATOTALScorecardReview] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventAdditionalDetail] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventGSEDetail] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventLicenseDetail] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventOFACSSNDetail] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventQMATRDetail] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventRESPADetail] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventRESPATILADetail] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventSummary] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.MaventTILADetail] = ComplianceEagleProductT.Undefined,
                [E_CEReportServiceEnum.Undefined] = ComplianceEagleProductT.Undefined
            };

        /// <summary>
        /// Binds a dropdown list with the <see cref="CEagleChangedCircumstanceCategory"/> enumeration.
        /// </summary>
        /// <param name="dropDownList">The dropdown list to bind.</param>
        public static void Bind_CEagleChangedCircumstanceCategory(DropDownList dropDownList)
        {
            foreach (int i in Enum.GetValues(typeof(CEagleChangedCircumstanceCategory)))
            {
                string label = Utilities.ConvertEnumToString((CEagleChangedCircumstanceCategory)i);

                dropDownList.Items.Add(new ListItem(label, i.ToString()));
            }
        }

        /// <summary>
        /// Masks sensitive data out of the ComplianceEagle request prior to logging.
        /// </summary>
        /// <param name="request">The request XML string.</param>
        /// <returns>A string that has been prepped for PB logging.</returns>
        public static string FormatRequestForLogging(string request)
        {
            if (string.IsNullOrEmpty(request))
            {
                return "Request is empty.";
            }

            request = Regex.Replace(request, "Password=\"[^\"]*?\"", "Password=\"*****\"");

            return request;
        }

        /// <summary>
        /// Masks sensitive data out of the ComplianceEagle response prior to logging.
        /// </summary>
        /// <param name="response">The response from ComplianceEagle as a CEReports object.</param>
        /// <returns>A string that has been prepped for PB logging.</returns>
        public static string FormatResponseForLogging(CEReports response)
        {
            //// Using serialization to clone the response object so that the masking only occurs within the logged copy. 
            if (response == null)
            {
                return string.Empty;
            }

            byte[] bytes = null;
            int contentLength = 0;

            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = new System.Text.UTF8Encoding(false); // 7/8/2015 DD - omit BOM.
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    response.WriteXmlStripOutCData(writer);
                    writer.Flush();
                    contentLength = (int)stream.Position;
                }

                bytes = stream.GetBuffer();
            }

            return Encoding.UTF8.GetString(bytes, 0, contentLength);
        }

        /// <summary>
        /// Pulls the list of app-level fields for the ComplianceEagle integration that are not supported by the MISMO 2.6 standard.
        /// </summary>
        /// <returns>A list of app-level field names that can be loaded using reflection.</returns>
        public static string[] GetAppFieldList()
        {
            Borrower borrower = new Borrower();
            return borrower.ExtraFieldList;
        }

        /// <summary>
        /// Pulls the list of loan-level fields for the ComplianceEagle integration that are not supported by the MISMO 2.6 standard.
        /// </summary>
        /// <returns>A list of loan-level field names that can be loaded using reflection.</returns>
        public static string[] GetLoanFieldList()
        {
            LoanData data = new LoanData();
            return data.ExtraFieldList;
        }

        /// <summary>
        /// Given a list of loan and/or app fields that will be pulled via reflection, returns a list that can be used to load the required dependencies.
        /// </summary>
        /// <param name="fields">The string array of desired fields.</param>
        /// <returns>A list of field names that can be used to look-up the required dependencies.</returns>
        public static List<string> NormalizeFieldListForDependencyLookup(string[] fields)
        {
            List<string> loanFieldList = new List<string>();

            foreach (string field in fields)
            {
                if (string.IsNullOrEmpty(field))
                {
                    continue;
                }
                else if (field.StartsWith("GetPreparerOfForm", StringComparison.OrdinalIgnoreCase))
                {
                    loanFieldList.Add("sfGetPreparerOfForm");
                }
                else if (field.StartsWith("GetAgentOfRole", StringComparison.OrdinalIgnoreCase))
                {
                    loanFieldList.Add("sfGetAgentOfRole");
                }
                else
                {
                    loanFieldList.Add(field.TrimWhitespaceAndBOM());
                }
            }

            return loanFieldList;
        }

        /// <summary>
        /// Saves a PDF as an EDoc. If the ComplianceEagle request was made asynchronously then any existing EDoc that matches the service type will be re-used and overwritten.
        /// </summary>
        /// <param name="brokerID">The BrokerID associated with the user performing the save, or with the loan file if asynchronous.</param>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="applicationID">The aAppId of the application.</param>
        /// <param name="userID">The userID of the user performing the save, or a system dummy user if asynchronous.</param>
        /// <param name="cdataPDFcontainer">The CData block from ComplianceEagle that contains the base64 encoded PDF.</param>
        /// <param name="ceagleServiceType">The type of ComplianceEagle service or report that the PDF corresponds to. $Mavent$ Review for example.</param>
        /// <param name="asynchronous">True if the request was made asynchronously, as from a DB queue. False if the request was made synchronously, i.e. manually.</param>
        /// <returns>True if the PDF was uploaded to EDocs. Otherwise false.</returns>
        public static bool SaveEDoc(Guid brokerID, Guid loanID, Guid applicationID, Guid? userID, string cdataPDFcontainer, E_CEReportServiceEnum ceagleServiceType, bool asynchronous)
        {
            bool uploaded = false;
            string encodedPDFContents = GetBase64EncodedPDF(cdataPDFcontainer);
            string edocDescription = GetEDocDescription(ceagleServiceType);

            string decodedFile = string.Empty;
            string fileDbKey = string.Empty;
            E_FileDB fileDB = asynchronous ? E_FileDB.Temp : E_FileDB.Normal;

            if (!string.IsNullOrEmpty(encodedPDFContents))
            {
                decodedFile = Utilities.Base64StringToFile(encodedPDFContents, ConstAppDavid.ComplianceEagle_Report_Tag + "_pdf");
                fileDbKey = loanID.ToString("N") + ConstAppDavid.ComplianceEagle_Report_Tag;

                FileDBTools.WriteFile(fileDB, fileDbKey, decodedFile);

                if (asynchronous)
                {
                    UploadEDocTryReuse(brokerID, loanID, applicationID, userID, edocDescription, decodedFile);
                }
                else
                {
                    UploadEDoc(brokerID, loanID, applicationID, userID, edocDescription, fileDbKey, fileDB);
                }

                uploaded = true;
            }

            return uploaded;
        }

        /// <summary>
        /// Performs sanity checks for a pending async request to ComplianceEagle, validating the associated CEagle account and the lender's CEagle config.
        /// </summary>
        /// <param name="brokerID">The BrokerID associated with the request.</param>
        /// <param name="loanID">The sLId of the loan file associated with the request.</param>
        /// <param name="username">The username associated with the request.</param>
        /// <param name="fileVersion">The expected file version of the loan file associated with the request.</param>
        /// <param name="customerID">The ComplianceEagle customer ID associated with the request.</param>
        /// <param name="lenderCEagleAuditUsername">The ComplianceEagle PTM audit username belonging to the lender with the given brokerID.</param>
        /// <param name="lenderCEagleCompanyID">The ComplianceEagle customer ID belonging to the lender with the given brokerID.</param>
        /// <param name="lenderBilling">The billing version of the lender with the given brokerID.</param>
        /// <param name="ceagleIsEnabled">Is the ComplianceEagle integration enabled for the lender with the given brokerID.</param>
        /// <param name="ceaglePTMAuditIsEnabled">Is the ComplianceEagle PTM audit enabled for the lender with the given brokerID.</param>
        /// <returns>True if all sanity checks pass. Otherwise false.</returns>
        public static bool ValidateAsyncRequestFromQueue(Guid brokerID, Guid loanID, string username, int fileVersion, string customerID, string lenderCEagleAuditUsername, string lenderCEagleCompanyID, E_BrokerBillingVersion lenderBilling, bool ceagleIsEnabled, bool ceaglePTMAuditIsEnabled)
        {
            string validationError = string.Empty;

            bool checksPass = ComplianceEagleUtil.VerifyAsyncAccount(username, customerID, lenderCEagleAuditUsername, lenderCEagleCompanyID);

            validationError = checksPass ? string.Empty : string.Format(
                    "{0} username: {1}, customer ID: {2}, brokerId: {3}, loanId: {4}", 
                    ErrorMessages.ComplianceEagle.AsyncAccountMismatch,
                    username, 
                    customerID, 
                    brokerID.ToString(), 
                    loanID.ToString());

            if (checksPass)
            {
                checksPass = ComplianceEagleUtil.VerifyPTMAuditConfig(lenderBilling, ceagleIsEnabled, ceaglePTMAuditIsEnabled);

                validationError = checksPass ? string.Empty : string.Format(
                    "{0} brokerId: {1}, loanId: {2}", 
                    ErrorMessages.ComplianceEagle.IgnoreAsyncRequestDueToFeatureDisable,
                    brokerID.ToString(), 
                    loanID.ToString());
            }

            //// The ComplianceEase integration also matches the expected file version when sanity checking an async request.
            //// The ComplianceEagle integration will refrain from that for now because the queue addition only forces unique on the loan ID and not based on the file version.
            //// If an additional loan save has occurred between queue time and async request time, the queue will still only have the original item 
            ////  and therefore the request should still be made with the latest data.
            //// That said, if it becomes necessary to abort based on file version mismatch, that can be done here.

            if (!checksPass)
            {
                Tools.LogError(validationError);
            }

            return checksPass;
        }

        /// <summary>
        /// Records a successful transaction to the database.
        /// </summary>
        /// <param name="principal">The user principal.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="transactionId">The ID for the transaction.</param>
        /// <param name="orderedProducts">A list of the products ordered in the transaction.</param>
        /// <param name="returnedReports">A list of reports returned from ComplianceEase.</param>
        public static void RecordTransactionToDb(AbstractUserPrincipal principal, Guid loanId, Guid transactionId, List<ComplianceEagleProductT> orderedProducts, List<Report> returnedReports)
        {
            using (CStoredProcedureExec exec = new CStoredProcedureExec(principal.BrokerId))
            {
                var complianceEagleLoanId = returnedReports.First().CEID;

                try
                {
                    exec.BeginTransactionForWrite();

                    SqlParameter[] orderParameters =
                    {
                        new SqlParameter("@BrokerId", principal.BrokerId),
                        new SqlParameter("@LoanId", loanId),
                        new SqlParameter("@TransactionId", transactionId),
                        new SqlParameter("@VendorLoanId", complianceEagleLoanId),
                        new SqlParameter("@OrderedDate", DateTime.Now),
                        new SqlParameter("@OrderedBy", principal.DisplayName)
                    };

                    exec.ExecuteNonQuery("COMPLIANCEEAGLE_ORDER_Save", orderParameters);

                    foreach (var report in returnedReports)
                    {
                        var type = ProductByServiceEnum[report.Service];
                        if (type == ComplianceEagleProductT.Undefined || !orderedProducts.Contains(type))
                        {
                            continue;
                        }

                        SqlParameter[] productParameters =
                        {
                            new SqlParameter("@BrokerId", principal.BrokerId),
                            new SqlParameter("@LoanId", loanId),
                            new SqlParameter("@TransactionId", transactionId),
                            new SqlParameter("@ReportId", report.ID),
                            new SqlParameter("@ProductType", type.ToString("D")),
                            new SqlParameter("@ProductDesc", type.ToString())
                        };

                        exec.ExecuteNonQuery("COMPLIANCEEAGLE_ORDERED_PRODUCT_Save", productParameters);
                    }

                    exec.CommitTransaction();
                }
                catch (SqlException exc)
                {
                    Tools.LogError($"Unable to save ComplianceEagle order to the database for loan {loanId}.", exc);
                    exec.RollbackTransaction();
                }
            }
        }

        /// <summary>
        /// Maps a ComplianceEagle product to the vendor request code.
        /// </summary>
        /// <param name="product">A ComplianceEagle product.</param>
        /// <returns>The corresponding request code.</returns>
        /// <remarks>
        /// This contains many of the same mappings as the data class enum in
        /// <see cref="ComplianceEagle.EnumMappings.E_CEReportServiceEnum"/> .
        /// </remarks>
        public static string GetRequestCodeForProduct(ComplianceEagleProductT product)
        {
            switch (product)
            {
                case ComplianceEagleProductT.AusReview:
                    return "AUS-Review";
                case ComplianceEagleProductT.AvmReview:
                    return "AVM-Review";
                case ComplianceEagleProductT.CraReview:
                    return "CRA-Review";
                case ComplianceEagleProductT.ExclusionaryList:
                    return "ExclusionaryList";
                case ComplianceEagleProductT.Flood:
                    return "Flood";
                case ComplianceEagleProductT.FloodQuickCheck:
                    return "Flood-Quick-Check";
                case ComplianceEagleProductT.FraudBorrower:
                    return "Fraud-Borrower";
                case ComplianceEagleProductT.FraudCollateral:
                    return "Fraud-Collateral";
                case ComplianceEagleProductT.FraudComplete:
                    return "Fraud-Complete";
                case ComplianceEagleProductT.Geocode:
                    return "GEOCODE";
                case ComplianceEagleProductT.HighCost:
                    return "High-Cost";
                case ComplianceEagleProductT.HmdaCheck:
                    return "HMDA-Check";
                case ComplianceEagleProductT.IncomeReview:
                    return "IncomeReview";
                case ComplianceEagleProductT.LefCheck:
                    return "LEF-Check";
                case ComplianceEagleProductT.MaventNmls:
                    return "Mavent-NMLS";
                case ComplianceEagleProductT.MaventReview:
                    return "Mavent-Review";
                case ComplianceEagleProductT.NmlsMcrReview:
                    return "MCR-Review";
                case ComplianceEagleProductT.QmReview:
                    return "QM-Review";
                case ComplianceEagleProductT.RateSpreadCheck:
                    return "Rate-Spread-Check";
                case ComplianceEagleProductT.RecordingFee:
                    return "RecordingFee";
                case ComplianceEagleProductT.ServiceProviderSearch:
                    return "ServiceProviderSearch";
                case ComplianceEagleProductT.SsnReview:
                    return "SSNReview";
                default:
                    throw new UnhandledEnumException(product);
            }
        }

        /// <summary>
        /// Extracts the base64 encoded PDF included in the ComplianceEagle PDF CData block.
        /// </summary>
        /// <param name="ceagleCData">A ComplianceEagle CData block including a base64 encoded PDF.</param>
        /// <returns>A base64 encoded PDF string.</returns>
        private static string GetBase64EncodedPDF(string ceagleCData)
        {
            string pdf = string.Empty;

            if (!string.IsNullOrEmpty(ceagleCData))
            {
                int indexOfPDF = ceagleCData.LastIndexOf("</a>");
                indexOfPDF = (indexOfPDF >= 0) ? indexOfPDF + 4 : 0;

                pdf = ceagleCData.Substring(indexOfPDF);
            }

            return pdf;
        }

        /// <summary>
        /// Converts a ComplianceEagle service type to an acceptable EDoc description.
        /// </summary>
        /// <param name="serviceType">The ComplianceEagle service type.</param>
        /// <returns>A string that can be used as an EDoc description.</returns>
        private static string GetEDocDescription(E_CEReportServiceEnum serviceType)
        {
            return serviceType.ToString("G");
        }

        /// <summary>
        /// Uploads a PDF stored on FileDB to EDocs.
        /// </summary>
        /// <param name="brokerID">The BrokerID associated with the user performing the save.</param>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="applicationID">The aAppId of the application.</param>
        /// <param name="userID">The userID of the user performing the save.</param>
        /// <param name="edocDescription">The EDoc description.</param>
        /// <param name="fileDBKey">The key that was applied to the file when it was written to FileDB.</param>
        /// <param name="fileDB">The FileDB that the PDF was written to.</param>
        private static void UploadEDoc(Guid brokerID, Guid loanID, Guid applicationID, Guid? userID, string edocDescription, string fileDBKey, E_FileDB fileDB)
        {
            //// ComplianceEase auto save type is not a typo. The actual doctype name will be reset to a "ComplianceEagle" doctype in the auto save options config.
            AutoSaveDocTypeFactory.SavePdfDoc(E_AutoSavePage.ComplianceEase, fileDBKey, fileDB, brokerID, loanID, applicationID, userID, edocDescription);
        }

        /// <summary>
        /// Uploads a PDF stored on FileDB to EDocs. Attempts to re-use an existing EDoc by matching the DocType and Description.
        /// This is done to avoid clogging EDMS with a new batch of EDocs every time that a ComplianceEagle request and PDF upload are performed from a loan save action.
        /// </summary>
        /// <param name="brokerID">The BrokerID associated with the request.</param>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="applicationID">The aAppId of the application.</param>
        /// <param name="userID">The userID associated with the request.</param>
        /// <param name="edocDescription">The EDoc description.</param>
        /// <param name="filePath">The temporary file path to which the PDF was saved.</param>
        private static void UploadEDocTryReuse(Guid brokerID, Guid loanID, Guid applicationID, Guid? userID, string edocDescription, string filePath)
        {
            EDocumentRepository repository = EDocumentRepository.GetSystemRepository(brokerID);
            int docTypeId = 0;

            //// ComplianceEase auto save type is not a typo. The actual doctype name will be reset to a "ComplianceEagle" doctype in the auto save options config.
            DocType targetDocType = AutoSaveDocTypeFactory.GetDocTypeForPageId(E_AutoSavePage.ComplianceEase, brokerID, E_EnforceFolderPermissions.True).DocType;

            var key = GeneratedKeyForRequestOrThreadCache.CEDocTypeKey(brokerID);
            object cacheDocTypeId = CurrentRequestOrThreadCache.GetRequestOrThreadObjectByName(key);

            if (cacheDocTypeId == null || ((int)cacheDocTypeId) == 0)
            {
                docTypeId = int.Parse(targetDocType.DocTypeId);

                CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(key, docTypeId);
            }
            else
            {
                docTypeId = (int)cacheDocTypeId;
            }

            if (docTypeId == 0)
            {
                throw new CBaseException(ErrorMessages.ComplianceEagle.DocumentAutoSaveOptionMissing, ErrorMessages.ComplianceEagle.DocumentAutoSaveOptionMissing);
            }

            IList<EDocument> docList = repository.GetDocumentsByLoanId(loanID);
            EDocument edoc;
            string nonDestructivePdfDocumentId = string.Empty;
            try
            {
                edoc = docList.First(d => d.DocTypeName.Equals(targetDocType.DocTypeName, StringComparison.OrdinalIgnoreCase) && d.PublicDescription.Equals(edocDescription, StringComparison.OrdinalIgnoreCase));
                nonDestructivePdfDocumentId = edoc.OverwritePDFContentForExistingDocs(filePath);
            }
            catch (InvalidOperationException)
            {
                try
                {
                    docList = repository.GetDeletedDocumentsByLoanId(loanID, brokerID);
                    edoc = docList.First(d => d.DocTypeName.Equals(targetDocType.DocTypeName, StringComparison.OrdinalIgnoreCase) && d.PublicDescription.Equals(edocDescription, StringComparison.OrdinalIgnoreCase));
                    EDocument.RestoreDocument(brokerID, edoc.DocumentId, new Guid(ConstStage.ComplianceEaglePTMEditorUserId), "Automated report update");
                    edoc = repository.GetDocumentById(edoc.DocumentId);
                    nonDestructivePdfDocumentId = edoc.OverwritePDFContentForExistingDocs(filePath);
                }
                catch (InvalidOperationException)
                {
                    edoc = repository.CreateDocument(E_EDocumentSource.GeneratedDocs);
                    edoc.UpdatePDFContentOnSave(filePath);
                }
            }

            edoc.AppId = applicationID;
            edoc.LoanId = loanID;
            edoc.DocumentTypeId = docTypeId;
            edoc.IsUploadedByPmlUser = false;
            edoc.PublicDescription = edocDescription;
            edoc.Save(new Guid(ConstStage.ComplianceEaglePTMEditorUserId));

            if (!string.IsNullOrEmpty(nonDestructivePdfDocumentId))
            {
                // 7/17/2017 - dd - OPM 453261 - Enqueue the document for pdf rasterizer after save. Otherwise the meta data from pdf rasterizer will get override in save.
                EDocument.EnqueueNonDestructivePdfRasterizeRequest(edoc.BrokerId, edoc.DocumentId, nonDestructivePdfDocumentId, EDocument.E_NonDestructiveSourceT.OverwriteExisting, EDocument.POST_ACTION_UPDATE_METADATA_EXCLUDE_ORIGINAL);
                EDocument.UpdateImageStatus(edoc.BrokerId, edoc.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
            }
        }

        /// <summary>
        /// Compares the given username and customer ID to the lender's ComplianceEagle username and customer ID to ensure that an async request is transmitted using the correct CEagle account.
        /// </summary>
        /// <param name="username">The username associated with the request.</param>
        /// <param name="customerID">The ComplianceEagle customer ID associated with the request.</param>
        /// <param name="lenderCEagleAuditUsername">The target lender's ComplianceEagle PTM audit username.</param>
        /// <param name="lenderCEagleCompanyID">The target lender's ComplianceEagle customer ID.</param>
        /// <returns>True if the account matches the lender's ComplianceEagle account. Otherwise false.</returns>
        private static bool VerifyAsyncAccount(string username, string customerID, string lenderCEagleAuditUsername, string lenderCEagleCompanyID)
        {
            bool match = lenderCEagleAuditUsername.Equals(username, StringComparison.OrdinalIgnoreCase);
            match = match && lenderCEagleCompanyID.Equals(customerID, StringComparison.OrdinalIgnoreCase);

            return match;
        }

        /// <summary>
        /// Verifies that the given lender is on the per transaction billing model and has the ComplianceEagle PTM audit enabled.
        /// </summary>
        /// <param name="lenderBilling">The billing version of the lender.</param>
        /// <param name="ceagleIsEnabled">Is the ComplianceEagle integration enabled for the lender.</param>
        /// <param name="ceaglePTMAuditIsEnabled">Is the ComplianceEagle PTM audit enabled for the lender.</param>
        /// <returns>True if the lender is PTM and has the ComplianceEagle audit enabled.</returns>
        private static bool VerifyPTMAuditConfig(E_BrokerBillingVersion lenderBilling, bool ceagleIsEnabled, bool ceaglePTMAuditIsEnabled)
        {
            return (lenderBilling == E_BrokerBillingVersion.PerTransaction) && ceagleIsEnabled && ceaglePTMAuditIsEnabled;
        }
    }
}
