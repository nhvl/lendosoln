﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOffice.Conversions
{
    public class FeeDiscrepancyException : CBaseException
    {
        private List<string> m_messages;

        public FeeDiscrepancyException(IEnumerable<string> sMessages)
            : base("Unable to complete export due to fee discrepancies: " + string.Join(" ", sMessages.ToArray()), 
                string.Join(" ", sMessages.ToArray()))
        {
            m_messages = new List<string>(sMessages);
        }

        public IEnumerable<string> Messages
        {
            get { return m_messages; }
        }
    }
}
