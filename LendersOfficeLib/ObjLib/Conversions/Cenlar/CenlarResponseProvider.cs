﻿namespace LendersOffice.Conversions.Cenlar
{
    using System;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Conversions.Templates;
    using LendersOffice.Integration.Cenlar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Scrapes Cenlar's response XML and processes it into a human-readable form for emailing.
    /// </summary>
    public class CenlarResponseProvider : AbstractResponseProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarResponseProvider"/> class.
        /// </summary>
        /// <param name="responsePayload">The response payload.</param>
        /// <param name="requestData">The request data for the transaction.</param>
        public CenlarResponseProvider(string responsePayload, CenlarRequestData requestData)
            : base(responsePayload, requestData)
        {
            base.ResponseData = new CenlarResponseData(responsePayload);
            this.ResponsePayload = GenerateXDocument(responsePayload);
            this.LogResponse();
        }

        /// <summary>
        /// Gets an <see cref="XDocument"/> containing the response payload.
        /// </summary>
        protected new XDocument ResponsePayload { get; }

        /// <summary>
        /// Gets the request data for the transaction.
        /// </summary>
        protected new CenlarRequestData RequestData => base.RequestData as CenlarRequestData;

        /// <summary>
        /// Gets the response data object to be populated by this class.
        /// </summary>
        protected new CenlarResponseData ResponseData => base.ResponseData as CenlarResponseData;

        /// <summary>
        /// Scrapes a File ID from a Cenlar Response.
        /// </summary>
        /// <param name="response">A raw string response.</param>
        /// <returns>The API key contained in the response.</returns>
        public static string ScrapeFileId(string response)
        {
            return ScrapeSingleElement(response, elementName: "FILEID");
        }

        /// <summary>
        /// Scrapes an API key from a Cenlar response.
        /// </summary>
        /// <param name="response">A raw string response.</param>
        /// <returns>The API key contained in the response.</returns>
        public static string ScrapeApiKey(string response)
        {
            return ScrapeSingleElement(response, elementName: "APIKEY");
        }

        /// <summary>
        /// Records the response to Paul Bunyan logs.
        /// </summary>
        public override void LogResponse()
        {
            Tools.LogInfo($"=== CENLAR DATA INTERFACE RESPONSE ==={Environment.NewLine}{Environment.NewLine}{this.ResponseData.RawResponse}");
        }

        /// <summary>
        /// Parses the response and scrapes data into a <see cref="CenlarResponseData"/> object.
        /// </summary>
        public override void ParseResponse()
        {
            this.LogResponse();
            this.ScrapeTransmissionErrors();
            this.ScrapeLoansFromResponse();
            this.ScrapeAggregateSummariesFromResponse();
        }

        /// <summary>
        /// Scrapes a single element out of a response.
        /// </summary>
        /// <param name="response">The raw string response.</param>
        /// <param name="elementName">The element to scrape.</param>
        /// <returns>The value of the scraped element.</returns>
        private static string ScrapeSingleElement(string response, string elementName)
        {
            var document = GenerateXDocument(response);
            var value = document.Descendants(elementName).FirstOrDefault()?.Value;
            return value;
        }

        /// <summary>
        /// Attempts to parse the given Cenlar response into an XML document.
        /// </summary>
        /// <param name="response">A raw string response.</param>
        /// <returns>The XML representing the response.</returns>
        /// <exception cref="DeveloperException">Throws if the response is not valid XML.</exception>
        private static XDocument GenerateXDocument(string response)
        {
            try
            {
                return XDocument.Parse(response);
            }
            catch (XmlException)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Could not parse the following XML from Cenlar:{Environment.NewLine}{Environment.NewLine}{response}"));
            }
        }

        /// <summary>
        /// Scrapes the payload for any errors that occurred during transmission.
        /// </summary>
        private void ScrapeTransmissionErrors()
        {
            // Cenlar returns transmission-level errors in either an ERROR or RETREC element. The spec they
            // provided is not clear on why they have two possible element names serving the same purpose.
            var errorList = this.ResponsePayload.Descendants("ERROR").Union(this.ResponsePayload.Descendants("RETREC"));
            foreach (XElement error in errorList)
            {
                this.Errors.Add(error.Value);
            }
        }

        /// <summary>
        /// Scrapes aggregated data on accepted and rejected loans.
        /// </summary>
        private void ScrapeAggregateSummariesFromResponse()
        {
            // The FILEBALANCING container holds aggregate information about transmitted loans.
            var fileBalancingElement = this.ResponsePayload.Descendants("FILEBALANCING").FirstOrDefault();

            if (fileBalancingElement == null)
            {
                return;
            }

            var acceptedElement = fileBalancingElement.Descendants("ACCEPTED").FirstOrDefault();
            if (acceptedElement != null)
            {
                this.ResponseData.AcceptedLoansSummary = this.ScrapeAggregateSummary(acceptedElement);
            }

            var rejectedElement = fileBalancingElement.Descendants("REJECTED").FirstOrDefault();
            if (rejectedElement != null)
            {
                this.ResponseData.RejectedLoansSummary = this.ScrapeAggregateSummary(rejectedElement);
            }
        }

        /// <summary>
        /// Scrapes an element for aggregated summary data.
        /// </summary>
        /// <param name="summaryElement">The summary element.</param>
        /// <returns>A populated Cenlar boarding summary.</returns>
        private CenlarBoardingSummary ScrapeAggregateSummary(XElement summaryElement)
        {
            var summary = new CenlarBoardingSummary();
            string defaultDecimal = default(decimal).ToString();
            string defaultInt = default(int).ToString();

            summary.LoanCount = int.Parse(summaryElement.Descendants("LoanCount").FirstOrDefault()?.Value ?? defaultInt);
            summary.Principal = decimal.Parse(summaryElement.Descendants("Principal").FirstOrDefault()?.Value ?? defaultDecimal);
            summary.EscrowBalance = decimal.Parse(summaryElement.Descendants("EscrowBalance").FirstOrDefault()?.Value ?? defaultDecimal);
            summary.InterimInterestCash = decimal.Parse(summaryElement.Descendants("InterimInterestCash").FirstOrDefault()?.Value ?? defaultDecimal);
            summary.PrincipalAndInterest = decimal.Parse(summaryElement.Descendants("PrincipalAndInterest").FirstOrDefault()?.Value ?? defaultDecimal);
            summary.TaxAndInsurance = decimal.Parse(summaryElement.Descendants("TaxAndInsurance").FirstOrDefault()?.Value ?? defaultDecimal);
            summary.RestrictedEscrow = decimal.Parse(summaryElement.Descendants("RestrictedEscrow").FirstOrDefault()?.Value ?? defaultDecimal);
            summary.BuydownBalance = decimal.Parse(summaryElement.Descendants("BuydownBalance").FirstOrDefault()?.Value ?? defaultDecimal);
            summary.TotalFunds = decimal.Parse(summaryElement.Descendants("TotalFunds").FirstOrDefault()?.Value ?? defaultDecimal);

            return summary;
        }

        /// <summary>
        /// Retrieves loan results from Cenlar's XML response.
        /// </summary>
        /// <remarks>
        /// Cenlar's response implementation is unintuitive. If only one loan was transmitted, the LOANNUMBER
        /// element will hold the loan number. However, if multiple loans were transmitted, LOANNUMBER will instead
        /// be the parent element of all information on a given loan and subelement LOANID will hold the loan number.
        /// </remarks>
        private void ScrapeLoansFromResponse()
        {
            var loanNumberElements = this.ResponsePayload.Descendants("LOANNUMBER");

            if (loanNumberElements.Count() == 1)
            {
                var cenlarIdentifier = loanNumberElements.FirstOrDefault().Value;
                this.ScrapeLoanResultData(this.ResponsePayload.Root, cenlarIdentifier);
            }
            else
            {
                foreach (var loan in loanNumberElements)
                {
                    var cenlarIdentifier = loan.Descendants("LOANID").FirstOrDefault().Value;
                    this.ScrapeLoanResultData(loan, cenlarIdentifier);
                }
            }
        }

        /// <summary>
        /// Retrieves a list of errors and warnings for a particular loan.
        /// </summary>
        /// <param name="loanElement">The XML describing the results for a loan.</param>
        /// <param name="cenlarIdentifier">The Cenlar identifier for the loan.</param>
        private void ScrapeLoanResultData(XElement loanElement, string cenlarIdentifier)
        {
            var loanIdentifiers = this.RequestData.LoansForTransmission.First(loan => loan.CenlarIdentifier == cenlarIdentifier);
            var status = this.GetBoardingStatus(loanElement);

            var result = new CenlarLoanResult(loanIdentifiers, status);

            var warningElements = loanElement.Descendants("WARNING");
            if (warningElements.Any())
            {
                foreach (var warningText in warningElements.Descendants("TEXT"))
                {
                    result.Warnings.Add(warningText.Value);
                }
            }

            if (result.Status.Equals(CenlarLoanStatusT.Rejected))
            {
                var errors = loanElement.Descendants("REJECT");
                if (errors.Any())
                {
                    foreach (var errorText in errors.Descendants("TEXT"))
                    {
                        result.Errors.Add(errorText.Value);
                    }
                }
            }

            this.ResponseData.LoanResults.Add(result);
        }

        /// <summary>
        /// Gets the boarding status of a loan.
        /// </summary>
        /// <param name="loanElement">The loan element to scrape.</param>
        /// <returns>The status of the loan.</returns>
        private CenlarLoanStatusT GetBoardingStatus(XElement loanElement)
        {
            string status = loanElement.Descendants("STATUS").FirstOrDefault()?.Value;

            switch (status)
            {
                case "Loan Accepted":
                    return CenlarLoanStatusT.Accepted;
                case "Loan Rejected":
                    return CenlarLoanStatusT.Rejected;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Cenlar returned a loan status \"{status}\" that is not handled."));
            }
        }
    }
}