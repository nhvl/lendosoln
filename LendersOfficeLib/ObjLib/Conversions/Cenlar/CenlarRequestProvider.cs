﻿namespace LendersOffice.Conversions.Cenlar
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions.Templates;
    using LendersOffice.Integration.Cenlar;
    using LendersOffice.Integration.Templates;
    using LendersOffice.Security;
    using LendersOffice.XsltExportReport;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Generates a payload for transmission to Cenlar.
    /// </summary>
    /// <remarks>
    /// Unlike most of our request providers, this doesn't manually build a payload using data classes.
    /// The Cenlar integration uses lender-specific batch exports to generate the payload.
    /// </remarks>
    public class CenlarRequestProvider : IRequestProvider
    {
        /// <summary>
        /// A collection of characters that can cause Cenlar's system to break. We provide
        /// a broker bit to determine whether we want to strip these from the payload.
        /// </summary>
        private static readonly char[] ProblematicCenlarCharacters = { '`', '\'' };

        /// <summary>
        /// The request data for the transaction.
        /// </summary>
        private CenlarRequestData requestData;

        /// <summary>
        /// The batch export result.
        /// </summary>
        private XsltExportResult result;

        /// <summary>
        /// Initializes a new instance of the <see cref="CenlarRequestProvider"/> class.
        /// </summary>
        /// <param name="requestData">The request data for the transaction.</param>
        public CenlarRequestProvider(CenlarRequestData requestData)
        {
            this.requestData = requestData;
            this.RunBatchExport(requestData.DummyUser);
        }

        /// <summary>
        /// Audits a request prior to transmission.
        /// </summary>
        /// <returns>An integration audit result.</returns>
        /// <exception cref="NotImplementedException">
        /// Not implemented because Cenlar does not have a concept of request auditing.
        /// </exception>
        /// <remarks>
        /// In the future we could implement our own auditing based on datapoints that
        /// frequently need to be corrected during the boarding process.
        /// </remarks>
        public IntegrationAuditResult AuditRequest()
        {
            throw new NotImplementedException("Cenlar does not have the concept of request auditing.");
        }

        /// <summary>
        /// Logs a payload to PB.
        /// </summary>
        /// <param name="request">The payload to log.</param>
        public void LogRequest(string request = null)
        {
            if (string.IsNullOrEmpty(request))
            {
                return;
            }

            Tools.LogInfo($"=== CENLAR DATA INTERFACE REQUEST ==={Environment.NewLine}{Environment.NewLine}{request}");
        }

        /// <summary>
        /// Uses the batch export system to prepare a payload for transmission to Cenlar.
        /// </summary>
        /// <returns>The exported payload.</returns>
        public string SerializeRequest()
        {
            string payload = this.RetrieveBatchExportResultContent();
            XDocument payloadXml = this.ValidateXml(payload);

            var rootElement = payloadXml.Descendants().FirstOrDefault();
            rootElement.Add(this.ConstructAuthenticationElement());

            // Hacky solution to get rid of all non-breaking spaces and leftover namespaces
            // from the batch export, including xmlns.
            rootElement.Attributes().Where(x => x.IsNamespaceDeclaration).Remove();
            payload = payloadXml.ToString();
            payload = payload.Replace(" xmlns=\"http://www.w3.org/1999/xhtml\"", string.Empty);
            payload = payload.Replace(" xmlns=\"\"", string.Empty);
            payload = payload.Replace(@"\u00A0", string.Empty);

            if (this.requestData.Configuration.RemoveProblematicPayloadCharacters)
            {
                foreach (var character in ProblematicCenlarCharacters)
                {
                    payload = payload.Replace(character.ToString(), string.Empty);
                }
            }

            // Verify that the string replace calls did not break the XML before returning.
            this.ValidateXml(payload);
            this.LogRequest(payload);

            return payload;
        }

        /// <summary>
        /// Runs the batch export and saves the result.
        /// </summary>
        /// <param name="principal">User principal.</param>
        private void RunBatchExport(AbstractUserPrincipal principal = null)
        {
            var emptyParameterSet = new List<KeyValuePair<string, string>>();
            var request = new XsltExportRequest(
                this.requestData.DummyUser.UserId,
                this.requestData.LoansForTransmission.Select(loan => loan.LoanId),
                this.requestData.Configuration.BatchExportName,
                callback: null,
                callbackParameters: emptyParameterSet);

            var worker = new XsltExportWorker(request, principal);
            if (!worker.UserHasFieldLevelAccess(this.requestData.DummyUser as BrokerUserPrincipal))
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"The user {this.requestData.DummyUser.DisplayName} ({this.requestData.DummyUser.UserId}) does not have sufficient permissions to run the batch export {this.requestData.Configuration.BatchExportName}."));
            }

            try
            {
                this.result = XsltExport.ExecuteSynchronously(request, principal);
            }
            catch (NotFoundException)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"The batch export name {this.requestData.Configuration.BatchExportName} could not be found."));
            }
        }

        /// <summary>
        /// After a batch export is run, the output is stored in a temp file.
        /// Retrieves the export results.
        /// </summary>
        /// <returns>The output of the batch export.</returns>
        private string RetrieveBatchExportResultContent()
        {
            var path = this.result.OutputFileLocation;
            var resultBytes = File.ReadAllBytes(path);
            var resultContent = Encoding.UTF8.GetString(resultBytes);

            return resultContent;
        }

        /// <summary>
        /// Cenlar requires that an authentication element be tacked onto any boarding request with
        /// the client's static identifiers, a loan template, and the per-request file ID.
        /// </summary>
        /// <returns>A constructed authentication element.</returns>
        private XElement ConstructAuthenticationElement()
        {
            return new XElement(
                "USERDATA",
                new XElement("USERID", this.requestData.Credentials.UserId),
                new XElement("UTEMPLATE", this.requestData.Credentials.TemplateName),
                new XElement("FILEID", this.requestData.FileId),
                new XElement("ENTITY", this.requestData.Credentials.CustomerId));
        }

        /// <summary>
        /// Validates a payload string to confirm that it contains parsable XML.
        /// </summary>
        /// <param name="payload">The payload string.</param>
        /// <returns>An XML Document generated from the XML string.</returns>
        /// <exception cref="DeveloperException">Throws if the XML string is invalid.</exception>
        private XDocument ValidateXml(string payload)
        {
            try
            {
                return XDocument.Parse(payload);
            }
            catch (XmlException)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Could not parse the following XML: {Environment.NewLine}{payload}"));
            }
        }
    }
}
