﻿namespace LendersOffice.ObjLib.Conversions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;

    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.ObjLib.Security;
    using LendersOffice.Security;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// Utilities class for exporting TPO users to CSV.
    /// </summary>
    public static class TpoUserSearchUtils
    {
        /// <summary>
        /// CSV column headers. 
        /// </summary>
        private static string[] headers = new string[]
        {
            "Login",
            "First Name",
            "Last Name",
            "Company",
            "Address",
            "City",
            "State",
            "Zip",
            "Phone",
            "Fax",
            "Cell Phone",
            "Pager",
            "Email",
            "Branch",
            "Supervisor",
            "IsSupervisor",
            "LenderAcc",
            "LockDesk",
            "Manager",
            "Underwriter",
            "Processor",
            "Price Group",
            "IsActive",
            "Last Login",
            "Lending Licenses",
            "Notes",
            "Originator Compensation Plan Level",
            "Originator Compensation Plan Effective Date",
            "Originator Compensation Is Only Paid For 1st Lien Of Combo",
            "Originator Compensation %",
            "Originator Compensation Basis",
            "Originator Compensation Minimum",
            "Originator Compensation Maximum",
            "Originator Compensation Fixed Amount",
            "Originator Compensation Notes",
            "Junior Underwriter",
            "Junior Processor",

            "Mini-Correspondent Branch",
            "Mini-Correspondent Price Group",
            "Mini-Correspondent Manager",
            "Mini-Correspondent Processor",
            "Mini-Correspondent Junior Processor",
            "Mini-Correspondent Account Exec",
            "Mini-Correspondent Underwriter",
            "Mini-Correspondent Junior Underwriter",
            "Mini-Correspondent Credit Auditor",
            "Mini-Correspondent Legal Auditor",
            "Mini-Correspondent Lock Desk",
            "Mini-Correspondent Purchaser",
            "Mini-Correspondent Secondary",

            "Correspondent Branch",
            "Correspondent Price Group",
            "Correspondent Manager",
            "Correspondent Processor",
            "Correspondent Junior Processor",
            "Correspondent Account Exec",
            "Correspondent Underwriter",
            "Correspondent Junior Underwriter",
            "Correspondent Credit Auditor",
            "Correspondent Legal Auditor",
            "Correspondent Lock Desk",
            "Correspondent Purchaser",
            "Correspondent Secondary",

            "IP Address Restriction",
            "IPs on Global Whitelist",
            "Devices with a Certificate",
            "Whitelisted IPs",
            "Multi-Factor Authentication",
            "Allow Installing Digital Certificates",
            "List of Digital Certificates",
            "IP Access",

            "Enable Auth Code via SMS",
            "Has Cell Phone Number",
            "NMLS ID",
            "Roles"
        };

        /// <summary>
        /// Exports the given TPO users to CSV.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="isActive">If true, search active users, if false search inactive users.</param>
        /// <param name="parameters">SQL parameters to filter user results.</param>
        /// <returns>DataTable of user data.</returns>
        public static DataTable SearchTpoUsers(Guid brokerId, bool isActive, IEnumerable<SqlParameter> parameters)
        {
            Hashtable employeeNameHash = LoadEmployeeNameHash(brokerId);
            Dictionary<Guid, string> priceGroupHash = LoadPriceGroupHash(brokerId);
            Dictionary<Guid, BranchDB> branches = LoadBranches(brokerId);

            DataTable dataTable = BuildDataTable();

            string procedureName = isActive ? "ListActivePmlUsersByBrokerId" : "ListInactivePmlUsersByBrokerId";

            using (var connection = DbAccessUtils.GetConnection(brokerId))
            using (var reader = LendersOffice.Drivers.SqlServerDB.StoredProcedureDriverHelper.ExecuteReader(connection, null, StoredProcedureName.Create(procedureName).Value, parameters, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    DataRow row = dataTable.NewRow();

                    string employeeId = reader["EmployeeId"].ToString();
                    row["EmployeeId"] = employeeId;

                    row["LoginNm"] = (string)reader["LoginNm"];
                    row["UserName"] = (string)reader["UserName"];
                    row["LenderAccExecEmployee"] = GetEmployeeName(employeeNameHash, reader["LenderAccExecEmployeeId"]);
                    row["LockDeskEmployee"] = GetEmployeeName(employeeNameHash, reader["LockDeskEmployeeId"]);
                    row["UnderwriterEmployee"] = GetEmployeeName(employeeNameHash, reader["UnderwriterEmployeeId"]);
                    row["ManagerEmployee"] = GetEmployeeName(employeeNameHash, reader["ManagerEmployeeId"]);
                    row["ProcessorEmployee"] = GetEmployeeName(employeeNameHash, reader["ProcessorEmployeeId"]);

                    string supervisor = GetSupervisor(employeeNameHash, reader["IsPmlManager"], reader["PmlExternalManagerEmployeeId"]);
                    row["Supervisor"] = supervisor;
                    row["IsSupervisor"] = supervisor == "[N/A]" ? "Yes" : "No";

                    string pmlBrokerId = reader["PmlBrokerId"].ToString();
                    row["PmlBrokerId"] = pmlBrokerId;
                    row["PmlBrokerName"] = (string)reader["PmlBrokerName"];

                    row["LpePriceGroup"] = GetLpePriceGroupName(priceGroupHash, branches, brokerId, reader["LpePriceGroupId"], reader["BranchId"]);
                    row["PricingEngine"] = GetPricingEngine(brokerId, reader["Permissions"], reader["PopulatePmlPermissionsT"], employeeId, pmlBrokerId);

                    row["IsActive"] = BuildIsActiveString(reader["IsActive"], reader["CanLogin"]);
                    row["RecentLoginD"] = CDateTime.Create(reader["RecentLoginD"].ToString(), null);
                    row["BranchNm"] = (string)reader["BranchNm"];

                    string roles = (string)reader["role"];
                    if (roles.EndsWith(", "))
                    {
                        roles = roles.Substring(0, roles.Length - 2);
                    }

                    row["Roles"] = roles;

                    row["JuniorUnderwriterEmployee"] = GetEmployeeName(employeeNameHash, reader["JuniorUnderwriterEmployeeId"]);
                    row["JuniorProcessorEmployee"] = GetEmployeeName(employeeNameHash, reader["JuniorProcessorEmployeeId"]);

                    row["MiniCorrManagerEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrManagerEmployeeId"]);
                    row["MiniCorrProcessorEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrProcessorEmployeeId"]);
                    row["MiniCorrJuniorProcessorEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrJuniorProcessorEmployeeId"]);
                    row["MiniCorrLenderAccExecEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrLenderAccExecEmployeeId"]);
                    row["MiniCorrUnderwriterEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrUnderwriterEmployeeId"]);
                    row["MiniCorrJuniorUnderwriterEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrJuniorUnderwriterEmployeeId"]);
                    row["MiniCorrCreditAuditorEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrCreditAuditorEmployeeId"]);
                    row["MiniCorrLegalAuditorEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrLegalAuditorEmployeeId"]);
                    row["MiniCorrLockDeskEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrLockDeskEmployeeId"]);
                    row["MiniCorrPurchaserEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrPurchaserEmployeeId"]);
                    row["MiniCorrSecondaryEmployee"] = GetEmployeeName(employeeNameHash, reader["MiniCorrSecondaryEmployeeId"]);
                    row["MiniCorrLpePriceGroup"] = GetLpePriceGroupName(priceGroupHash, branches, brokerId, reader["MiniCorrespondentPriceGroupId"], reader["MiniCorrespondentBranchId"]);
                    row["MiniCorrBranchNm"] = GetBranchName(branches, reader["MiniCorrespondentBranchId"]);

                    row["CorrManagerEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrManagerEmployeeId"]);
                    row["CorrProcessorEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrProcessorEmployeeId"]);
                    row["CorrJuniorProcessorEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrJuniorProcessorEmployeeId"]);
                    row["CorrLenderAccExecEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrLenderAccExecEmployeeId"]);
                    row["CorrUnderwriterEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrUnderwriterEmployeeId"]);
                    row["CorrJuniorUnderwriterEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrJuniorUnderwriterEmployeeId"]);
                    row["CorrCreditAuditorEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrCreditAuditorEmployeeId"]);
                    row["CorrLegalAuditorEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrLegalAuditorEmployeeId"]);
                    row["CorrLockDeskEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrLockDeskEmployeeId"]);
                    row["CorrPurchaserEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrPurchaserEmployeeId"]);
                    row["CorrSecondaryEmployee"] = GetEmployeeName(employeeNameHash, reader["CorrSecondaryEmployeeId"]);
                    row["CorrLpePriceGroup"] = GetLpePriceGroupName(priceGroupHash, branches, brokerId, reader["CorrespondentPriceGroupId"], reader["CorrespondentBranchId"]);
                    row["CorrBranchNm"] = GetBranchName(branches, reader["CorrespondentBranchId"]);

                    dataTable.Rows.Add(row);
                }
            }

            return dataTable;
        }

        /// <summary>
        /// Exports TPO user data for all of the given broker's TPO users to CSV.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>CSV containing TPO user info.</returns>
        public static string ExportAllTpoUsersToCsv(Guid brokerId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@LimitResultSet", false));

            DataTable userData = SearchTpoUsers(brokerId, true, parameters);
            return ExportTpoUsersToCsv(brokerId, userData);
        }

        /// <summary>
        /// Exports users from the givin data table to CSV.
        /// </summary>
        /// <param name="brokerId">Broker Id.</param>
        /// <param name="userTable">DataTable of user data to export to CSV.</param>
        /// <returns>User data exported to CSV.</returns>
        public static string ExportTpoUsersToCsv(Guid brokerId, DataTable userTable)
        {
            // If the data table is empty, then no search has been made, or
            // nothing showed up in the search
            if (userTable == null || userTable.Rows.Count == 0)
            {
                return string.Empty;
            }

            // The indexes for the order or the data coming from table of searched data
            ArrayList order = new ArrayList();

            order.Add(15);  // Branch
            order.Add(9);   // Supervisor
            order.Add(8);   // IsSupervisor
            order.Add(3);   // Lender Account Exec 
            order.Add(4);   // Lock Desk
            order.Add(6);   // Manager
            order.Add(5);   // Underwriter
            order.Add(7);   // Processor
            order.Add(11);  // Price Group
            order.Add(13);  // IsActive
            order.Add(14);  // Last Login

            StringBuilder csv = new StringBuilder();
            csv.Append(string.Join(",", headers) + Environment.NewLine);

            LosConvert losConvert = new LosConvert();

            for (int i = 0; i < userTable.Rows.Count; i++)
            {
                // Holds the info for each entry in order.  Needs both lists 
                // to maintain consistency with the order from the ContactInfoExport
                // file
                ArrayList info1 = new ArrayList();
                ArrayList info2 = new ArrayList();

                // Manually call retrieve to avoid fetching the data while the data
                // reader is open.
                var clientCertificates = new ClientCertificates(brokerId);
                clientCertificates.Retrieve();
                var registeredIpAddresses = new UserRegisteredIpAddresses(brokerId);
                registeredIpAddresses.Retrieve();

                // Uses the EmployeeId from the entries of the searched data to 
                // get the rest of the information about the Employee
                Guid id = new Guid(userTable.Rows[i].ItemArray.GetValue(0).ToString());
                SqlParameter[] parameters =
                    {
                        new SqlParameter("@EmployeeId", id)
                    };

                using (IDataReader sR = StoredProcedureHelper.ExecuteReader(brokerId, "ListEmployeeDetailsByEmployeeId", parameters))
                {
                    while (sR.Read() == true)
                    {
                        info1.Add(sR["LoginNm"]);
                        info1.Add(sR["UserFirstNm"]);
                        info1.Add(sR["UserLastNm"]);
                        info1.Add(sR["PmlBrokerCompany"]);
                        info1.Add(sR["Addr"]);
                        info1.Add(sR["City"]);
                        info1.Add(sR["State"]);
                        info1.Add(sR["Zip"]);
                        info1.Add(sR["Phone"]);
                        info1.Add(sR["Fax"]);
                        bool isCellPhoneForMfaOnly = (bool)sR["IsCellphoneForMultiFactorOnly"];
                        info1.Add(EmployeeDB.CalculatedCellphoneValue((string)sR["CellPhone"], isCellPhoneForMfaOnly));
                        info1.Add(sR["Pager"]);
                        info1.Add(sR["Email"]);

                        info2.Add(GetLicenseCsv((string)sR["LicenseXmlContent"]));
                        info2.Add(sR["NotesByEmployer"]);

                        PmlBroker pmlBroker = PmlBroker.RetrievePmlBrokerById((Guid)sR["PmlBrokerId"], (Guid)sR["BrokerId"]);

                        // Use same logic as EmployeeDB.OriginatorCompensationLastModifiedDate
                        DateTime? user_OriginatorCompensationLastModifiedDate = null;
                        if (!Convert.IsDBNull(sR["OriginatorCompensationLastModifiedDate"]))
                        {
                            user_OriginatorCompensationLastModifiedDate = Convert.ToDateTime(sR["OriginatorCompensationLastModifiedDate"]);
                        }

                        DateTime? originatorCompensationLastModifiedDate = ChooseOriginatorCompensationLastModifiedDate(user_OriginatorCompensationLastModifiedDate, pmlBroker.OriginatorCompensationLastModifiedDate);
                        string originatorCompensationLastModifiedDate_rep = string.Empty;
                        if (originatorCompensationLastModifiedDate.HasValue)
                        {
                            originatorCompensationLastModifiedDate_rep = losConvert.ToDateTimeString(originatorCompensationLastModifiedDate.Value);
                        }

                        E_OrigiantorCompenationLevelT planLevel = (E_OrigiantorCompenationLevelT)sR["OriginatorCompensationSetLevelT"];
                        if (planLevel == E_OrigiantorCompenationLevelT.IndividualUser)
                        {
                            info2.Add("Individual User");
                            info2.Add(originatorCompensationLastModifiedDate_rep);
                            info2.Add((bool)sR["OriginatorCompensationIsOnlyPaidForFirstLienOfCombo"] ? "Yes" : string.Empty);
                            info2.Add(losConvert.ToRateString((decimal)sR["OriginatorCompensationPercent"]).Replace(",", string.Empty));
                            info2.Add(FormatOriginatorCompensationBasis((E_PercentBaseT)sR["OriginatorCompensationBaseT"]));
                            info2.Add(losConvert.ToMoneyString((decimal)sR["OriginatorCompensationMinAmount"], FormatDirection.ToRep).Replace(",", string.Empty));
                            info2.Add(losConvert.ToMoneyString((decimal)sR["OriginatorCompensationMaxAmount"], FormatDirection.ToRep).Replace(",", string.Empty));
                            info2.Add(losConvert.ToMoneyString((decimal)sR["OriginatorCompensationFixedAmount"], FormatDirection.ToRep).Replace(",", string.Empty));
                            info2.Add(sR["OriginatorCompensationNotes"].ToString().Replace(",", string.Empty));
                        }
                        else
                        {
                            info2.Add("Originating Company");
                            info2.Add(originatorCompensationLastModifiedDate_rep);
                            info2.Add(pmlBroker.OriginatorCompensationIsOnlyPaidForFirstLienOfCombo ? "Yes" : string.Empty);
                            info2.Add(losConvert.ToRateString(pmlBroker.OriginatorCompensationPercent).Replace(",", string.Empty));
                            info2.Add(FormatOriginatorCompensationBasis(pmlBroker.OriginatorCompensationBaseT));
                            info2.Add(losConvert.ToMoneyString(pmlBroker.OriginatorCompensationMinAmount, FormatDirection.ToRep).Replace(",", string.Empty));
                            info2.Add(losConvert.ToMoneyString(pmlBroker.OriginatorCompensationMaxAmount, FormatDirection.ToRep).Replace(",", string.Empty));
                            info2.Add(losConvert.ToMoneyString(pmlBroker.OriginatorCompensationFixedAmount, FormatDirection.ToRep).Replace(",", string.Empty));
                            info2.Add(pmlBroker.OriginatorCompensationNotes.Replace(",", string.Empty));
                        }

                        // 12/3/2013 gf - opm 145015 the junior underwriter and junior processor
                        // are being added to the end of the contact info export, mimic that order.
                        info2.Add(userTable.Rows[i]["JuniorUnderwriterEmployee"]);
                        info2.Add(userTable.Rows[i]["JuniorProcessorEmployee"]);

                        info2.Add(userTable.Rows[i]["MiniCorrBranchNm"]);
                        info2.Add(userTable.Rows[i]["MiniCorrLpePriceGroup"]);

                        info2.Add(userTable.Rows[i]["MiniCorrManagerEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrProcessorEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrJuniorProcessorEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrLenderAccExecEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrUnderwriterEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrJuniorUnderwriterEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrCreditAuditorEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrLegalAuditorEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrLockDeskEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrPurchaserEmployee"]);
                        info2.Add(userTable.Rows[i]["MiniCorrSecondaryEmployee"]);

                        info2.Add(userTable.Rows[i]["CorrBranchNm"]);
                        info2.Add(userTable.Rows[i]["CorrLpePriceGroup"]);

                        info2.Add(userTable.Rows[i]["CorrManagerEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrProcessorEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrJuniorProcessorEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrLenderAccExecEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrUnderwriterEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrJuniorUnderwriterEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrCreditAuditorEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrLegalAuditorEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrLockDeskEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrPurchaserEmployee"]);
                        info2.Add(userTable.Rows[i]["CorrSecondaryEmployee"]);

                        info2.Add((bool)sR["EnabledIpRestriction"] ? "Restricted" : "Unrestricted");
                        info2.Add((bool)sR["EnabledIpRestriction"] ? "Yes" : string.Empty);
                        info2.Add((bool)sR["EnabledIpRestriction"] ? "Yes" : string.Empty);

                        string mustLogInFromTheseIpAddresses = (string)sR["MustLogInFromTheseIpAddresses"];
                        string whiteListedIps = string.IsNullOrEmpty(mustLogInFromTheseIpAddresses)
                            ? string.Empty
                            : string.Join("+", mustLogInFromTheseIpAddresses.Split(';').Where(s => !string.IsNullOrEmpty(s)));
                        info2.Add(whiteListedIps);

                        info2.Add((bool)sR["EnabledMultiFactorAuthentication"] ? "Yes" : "No");
                        info2.Add((bool)sR["EnabledClientDigitalCertificateInstall"] ? "Yes" : "No");

                        var certificates = clientCertificates.GetCertificatesForUser((Guid)sR["UserId"])
                            .OrderByDescending(c => c.CreatedDate)
                            .Select(c => c.Description + "::" + c.CreatedDate);
                        info2.Add(string.Join("+", certificates));

                        var ipAddrs = registeredIpAddresses.GetRegisteredIpsForUser((Guid)sR["UserId"])
                            .Select(ip => ip.IpAddress);
                        info2.Add(string.Join("+", ipAddrs));

                        info2.Add((bool)sR["EnableAuthCodeViaSms"] ? "Yes" : "No");
                        info2.Add(!string.IsNullOrEmpty((string)sR["CellPhone"]) ? "Yes" : "No");

                        info2.Add((string)sR["NmLsIdentifier"]);

                        var rolesCsv = (string)userTable.Rows[i]["Roles"];
                        var rolesForExport = rolesCsv.Split(new[] { ',' });
                        info2.Add(string.Join(" + ", rolesForExport.Select(r => r.Trim())));
                    }
                }

                // Adds the first part of the list to the file
                foreach (string s in info1)
                {
                    if (s.IndexOf(',') != -1)
                    {
                        csv.Append("\"" + s + "\"");
                    }
                    else
                    {
                        csv.Append(s);
                    }

                    csv.Append(",");
                }

                // Adds the middle information, taken from the search data to
                // the list
                for (int j = 0; j < order.Count; j++)
                {
                    int k = (int)order[j];

                    string s = userTable.Rows[i].ItemArray.GetValue(k).ToString();

                    if (s.IndexOf(',') != -1)
                    {
                        csv.Append("\"" + s + "\"");
                    }
                    else
                    {
                        csv.Append(s);
                    }

                    csv.Append(",");
                }

                // Adds the last part of the list to the file
                foreach (string s in info2)
                {
                    // For Case 34667 - Adding strings to the table which contain multiple lines are causing
                    // extra lines to appear in the table and the exported data becomes incorrectly formatted
                    // Due to this, new lines in the string are removed
                    string oneLineString = s.Replace(Environment.NewLine, " ");

                    if (s.IndexOf(',') != -1)
                    {
                        csv.Append("\"" + oneLineString + "\"");
                    }
                    else
                    {
                        csv.Append(oneLineString);
                    }

                    csv.Append(",");
                }

                csv.Append(Environment.NewLine);
            }

            return csv.ToString();
        }

        /// <summary>
        /// Builds an empty data table for holding TPO user data.
        /// </summary>
        /// <returns>Empty DataTable with Columns set.</returns>
        private static DataTable BuildDataTable()
        {
            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("EmployeeId", typeof(string));
            dataTable.Columns.Add("LoginNm", typeof(string));
            dataTable.Columns.Add("UserName", typeof(string));
            dataTable.Columns.Add("LenderAccExecEmployee", typeof(string));
            dataTable.Columns.Add("LockDeskEmployee", typeof(string));
            dataTable.Columns.Add("UnderwriterEmployee", typeof(string));
            dataTable.Columns.Add("ManagerEmployee", typeof(string));
            dataTable.Columns.Add("ProcessorEmployee", typeof(string));
            dataTable.Columns.Add("IsSupervisor", typeof(string));
            dataTable.Columns.Add("Supervisor", typeof(string));
            dataTable.Columns.Add("PmlBrokerName", typeof(string));
            dataTable.Columns.Add("LpePriceGroup", typeof(string));
            dataTable.Columns.Add("PricingEngine", typeof(string));
            dataTable.Columns.Add("IsActive", typeof(string));
            dataTable.Columns.Add("RecentLoginD", typeof(CDateTime));
            dataTable.Columns.Add("BranchNm", typeof(string));
            dataTable.Columns.Add("Roles", typeof(string));
            dataTable.Columns.Add("JuniorUnderwriterEmployee", typeof(string));
            dataTable.Columns.Add("JuniorProcessorEmployee", typeof(string));
            dataTable.Columns.Add("PmlBrokerId", typeof(string));

            dataTable.Columns.Add("MiniCorrManagerEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrProcessorEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrJuniorProcessorEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrLenderAccExecEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrUnderwriterEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrJuniorUnderwriterEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrCreditAuditorEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrLegalAuditorEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrLockDeskEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrPurchaserEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrSecondaryEmployee", typeof(string));
            dataTable.Columns.Add("MiniCorrLpePriceGroup", typeof(string));
            dataTable.Columns.Add("MiniCorrBranchNm", typeof(string));

            dataTable.Columns.Add("CorrManagerEmployee", typeof(string));
            dataTable.Columns.Add("CorrProcessorEmployee", typeof(string));
            dataTable.Columns.Add("CorrJuniorProcessorEmployee", typeof(string));
            dataTable.Columns.Add("CorrLenderAccExecEmployee", typeof(string));
            dataTable.Columns.Add("CorrUnderwriterEmployee", typeof(string));
            dataTable.Columns.Add("CorrJuniorUnderwriterEmployee", typeof(string));
            dataTable.Columns.Add("CorrCreditAuditorEmployee", typeof(string));
            dataTable.Columns.Add("CorrLegalAuditorEmployee", typeof(string));
            dataTable.Columns.Add("CorrLockDeskEmployee", typeof(string));
            dataTable.Columns.Add("CorrPurchaserEmployee", typeof(string));
            dataTable.Columns.Add("CorrSecondaryEmployee", typeof(string));
            dataTable.Columns.Add("CorrLpePriceGroup", typeof(string));
            dataTable.Columns.Add("CorrBranchNm", typeof(string));

            return dataTable;
        }

        /// <summary>
        /// Caches employee names into a hash table.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>Hashtable of cached employee names.</returns>
        private static Hashtable LoadEmployeeNameHash(Guid brokerId)
        {
            Hashtable employeeNameHash = new Hashtable(40);
            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListPmlUsersRelationship", parameters))
            {
                while (reader.Read())
                {
                    employeeNameHash.Add((Guid)reader["EmployeeId"], (string)reader["UserName"]);
                }
            }

            return employeeNameHash;
        }

        /// <summary>
        /// Caches Price group names into a Dictionary.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>Dictionary of Price Group IDs to Names.</returns>
        private static Dictionary<Guid, string> LoadPriceGroupHash(Guid brokerId)
        {
            Dictionary<Guid, string> priceGroupHash = new Dictionary<Guid, string>(10);
            SqlParameter[] parameters = { new SqlParameter("@BrokerId", brokerId) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "ListPricingGroupByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    priceGroupHash.Add((Guid)reader["LpePriceGroupId"], (string)reader["LpePriceGroupName"]);
                }
            }

            return priceGroupHash;
        }

        /// <summary>
        /// Caches BranchDBs into a Dictionary.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <returns>Dictionary of Branch IDs to BranchDB objects.</returns>
        private static Dictionary<Guid, BranchDB> LoadBranches(Guid brokerId)
        {
            Dictionary<Guid, BranchDB> branches = BranchDB.GetBranchObjects(brokerId).ToDictionary(branch => branch.BranchID);
            return branches;
        }

        /// <summary>
        /// Fixes the licenses (from ContactInfoExport.aspx.cs).
        /// </summary>
        /// <param name="license">License info as string.</param>
        /// <returns>LicenseInfoList output to CSV.</returns>
        private static string GetLicenseCsv(string license)
        {
            try
            {
                return LicenseInfoList.ToObject(license).ToCsv();
            }
            catch (CBaseException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Chooses the Originator Compensation Last Modified Date.
        /// </summary>
        /// <param name="userD">Latest plan effective date from the user record.</param>
        /// <param name="pmlBrokerD">Latest plan effective date from the OC record.</param>
        /// <returns>Latest plan effective date between the user and OC records.</returns>
        /// <remarks>Logic duplicated from EmployeeDB.OriginatorCompensationLastModifiedDate.</remarks>
        private static DateTime? ChooseOriginatorCompensationLastModifiedDate(DateTime? userD, DateTime? pmlBrokerD)
        {
            // The latest plan effective date between the user record and the OC record should be used for the plan effective date.
            // If one of these dates is null, return the other. If they're both null, return null.
            if (!userD.HasValue)
            {
                return pmlBrokerD;
            }
            else if (!pmlBrokerD.HasValue)
            {
                return userD;
            }
            else if (userD < pmlBrokerD)
            {
                // Otherwise return the most recently modified OriginatorCompensationLastModifiedDate
                return pmlBrokerD;
            }
            else
            {
                return userD;
            }
        }

        /// <summary>
        /// Gets friendly description for originator compensation basis.
        /// </summary>
        /// <param name="baseT">Originator compensation percent base type.</param>
        /// <returns>Friendly description for originator compensation basis.</returns>
        private static string FormatOriginatorCompensationBasis(E_PercentBaseT baseT)
        {
            switch (baseT)
            {
                case E_PercentBaseT.LoanAmount:
                    return "Loan Amount";
                case E_PercentBaseT.TotalLoanAmount:
                    return "Total Loan Amount";
                default:
                    throw new UnhandledEnumException(baseT);
            }
        }

        /// <summary>
        /// Gets an employees name using their ID.
        /// </summary>
        /// <param name="employeeNameHash">Hashtable of cached employee IDs to names.</param>
        /// <param name="obj">Either null or an Employee ID.</param>
        /// <returns>Employee name.</returns>
        private static string GetEmployeeName(Hashtable employeeNameHash, object obj)
        {
            if (obj is Guid)
            {
                return (string)employeeNameHash[obj];
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets employee supervisor name.
        /// </summary>
        /// <param name="employeeNameHash">Hashtable of cached employee IDs to names.</param>
        /// <param name="isPmlManager">DB object that is either null or a bool determining wheter the user is a manager.</param>
        /// <param name="pmlExternalManagerEmployeeId">Employee ID for PML External Manager.</param>
        /// <returns>Supervisor Name or "[N/A]" if not applicable.</returns>
        private static string GetSupervisor(Hashtable employeeNameHash, object isPmlManager, object pmlExternalManagerEmployeeId)
        {
            if (isPmlManager is bool)
            {
                if ((bool)isPmlManager)
                {
                    return "[N/A]";
                }
            }

            return GetEmployeeName(employeeNameHash, pmlExternalManagerEmployeeId);
        }

        /// <summary>
        /// Get friendly description of pricing engine permissions.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="permissionString">User permissions.</param>
        /// <param name="permissionPopulationType">PML broker permission population type.</param>
        /// <param name="employeeId">Employee ID.</param>
        /// <param name="pmlBrokerId">PML Broker ID.</param>
        /// <returns>Friendly description of pricing engine permissions.</returns>
        private static string GetPricingEngine(
            Guid brokerId,
            object permissionString,
            object permissionPopulationType,
            string employeeId,
            string pmlBrokerId)
        {
            var enginePermissions = string.Empty;

            if (!(permissionString is string))
            {
                return enginePermissions;
            }

            var pmlBrokerGuid = Guid.Empty;

            Guid.TryParse(pmlBrokerId, out pmlBrokerGuid);

            EmployeePopulationMethodT? permissionPopulationMethod = null;

            if (!Convert.IsDBNull(permissionPopulationType))
            {
                permissionPopulationMethod = (EmployeePopulationMethodT)permissionPopulationType;
            }

            var permissions = new BrokerUserPermissions(
                brokerId,
                Guid.Parse(employeeId),
                (string)permissionString,
                "P",
                pmlBrokerGuid,
                permissionPopulationMethod);

            if (permissions.HasPermission(Permission.CanRunPricingEngineWithoutCreditReport))
            {
                enginePermissions = "Can run pricing";
            }

            if (permissions.HasPermission(Permission.CanSubmitWithoutCreditReport))
            {
                enginePermissions += (enginePermissions.Length > 0) ? " and submit" : "Can submit";
            }

            if (enginePermissions.Length > 0)
            {
                enginePermissions += " without credit report";
            }

            if (permissions.HasPermission(Permission.CanApplyForIneligibleLoanPrograms))
            {
                if (enginePermissions.Length > 0)
                {
                    enginePermissions += ", <br>Can apply for ineligible loan programs";
                }
                else
                {
                    enginePermissions += "Can apply for ineligible loan programs";
                }
            }

            return enginePermissions;
        }

        /// <summary>
        /// Friendly description detailing if the user is active and can login.
        /// </summary>
        /// <param name="isActive">Is the user active?.</param>
        /// <param name="canLogin">Can the user login?.</param>
        /// <returns>A friendly decription of user status.</returns>
        private static string BuildIsActiveString(object isActive, object canLogin)
        {
            if (isActive is bool && ((bool)isActive))
            {
                if (canLogin is bool && ((bool)canLogin))
                {
                    return "Active";
                }
                else
                {
                    return "Disabled";
                }
            }

            return "Inactive";
        }

        /// <summary>
        /// Gets LPE Price Group Name.
        /// </summary>
        /// <param name="priceGroupHash">Dictionary of cached price group IDs to names.</param>
        /// <param name="branches">Dictionary of cached branch IDs to BranchDB objects.</param>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="lpePriceGroupId">Price group ID.</param>
        /// <param name="branchId">Branch ID.</param>
        /// <returns>Price group name.</returns>
        private static string GetLpePriceGroupName(Dictionary<Guid, string> priceGroupHash, Dictionary<Guid, BranchDB> branches, Guid brokerId, object lpePriceGroupId, object branchId)
        {
            if (lpePriceGroupId is Guid)
            {
                return priceGroupHash[(Guid)lpePriceGroupId];
            }

            string priceGroupName = string.Empty;

            if (branchId is Guid)
            {
                BranchDB branch;

                if (branches.TryGetValue((Guid)branchId, out branch) &&
                    priceGroupHash.TryGetValue(branch.BranchLpePriceGroupIdDefault, out priceGroupName))
                {
                    return priceGroupName;
                }
            }

            if (priceGroupHash.TryGetValue(BrokerDB.RetrieveById(brokerId).LpePriceGroupIdDefault, out priceGroupName))
            {
                return priceGroupName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets branch name.
        /// </summary>
        /// <param name="branches">Dictionary of cached branch IDs to BranchDB objects.</param>
        /// <param name="branchIDColumnValue">Branch ID.</param>
        /// <returns>Branch name.</returns>
        private static string GetBranchName(Dictionary<Guid, BranchDB> branches, object branchIDColumnValue)
        {
            if (!Convert.IsDBNull(branchIDColumnValue))
            {
                BranchDB branch;

                if (branches.TryGetValue((Guid)branchIDColumnValue, out branch))
                {
                    return branch.Name;
                }
            }

            return string.Empty;
        }
    }
}
