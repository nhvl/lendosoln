﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    using System.Collections.Generic;

    /// <summary>
    /// Contains fields representing the result of auditing a <see cref="DataAccess.CAppData"/> object for a particular borrower and purpose.
    /// </summary>
    /// <remarks>
    /// This class is serialized to JSON and sent to the client for displaying.
    /// Note that this is intended to be borrower-level, not application-level.
    /// </remarks>
    public class BorrowerAuditResult
    {
        /// <summary>
        /// Gets or sets the full name of the borrower whose data was checked.
        /// </summary>
        /// <value>A borrower name.</value>
        public string FullName
        {
            get; set;
        }

        /// <summary>
        /// Gets a list of Borrower-level errors found by an audit.
        /// </summary>
        /// <value>A list of borrower errors.</value>
        public List<AuditError> Errors { get; } = new List<AuditError>();
    }
}
