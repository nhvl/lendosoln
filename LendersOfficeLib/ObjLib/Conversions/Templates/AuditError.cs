﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    /// <summary>
    /// Represents an error encountered when auditing a loan/borrower field.
    /// This is serialized to JSON for displaying audit errors to the user.
    /// </summary>
    public class AuditError
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuditError"/> class.
        /// </summary>
        /// <param name="fieldName">The name of the relevant field for the error.</param>
        /// <param name="value">The value of the field in error.</param>
        /// <param name="message">The error message to display to the user.</param>
        /// <param name="url">The URL to use for correcting the error.</param>
        /// <param name="pageTitle">The page title of the destination of <paramref name="url"/>.</param>
        /// <param name="urlOption">
        /// The <see cref="DataAccess.E_UrlOption"/> value as an int for navigating to the erroring field's page. 
        /// Should correspond to <paramref name="url"/>. Used in Embedded PML.
        /// </param>
        public AuditError(string fieldName = "", string value = "", string message = "", string url = "", string pageTitle = "", int? urlOption = null)
        {
            this.FieldName = fieldName;
            this.Value = value;
            this.Message = message;
            this.Url = url;
            this.PageTitle = pageTitle;
            this.HighUrl = urlOption;
        }

        /// <summary>
        /// Gets or sets the user-friendly name of the relevant field for the error.
        /// </summary>
        /// <value>Field name.</value>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the value of the field in error.
        /// </summary>
        /// <value>Field value.</value>
        public string Value
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the URL to use for editing the error value in the loan editor.
        /// </summary>
        /// <value>Loan editor field link.</value>
        public string Url
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the Page Title to use for giving an indicator to the user of where the link will take them.
        /// </summary>
        /// <value>Loan editor field link.</value>
        public string PageTitle
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the error message to display to the user.
        /// </summary>
        /// <value>Error message.</value>
        public string Message
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the value for the "High URL" <see cref="DataAccess.E_UrlOption"/> parameter value (as an int) to be passed to the loanapp.aspx page.
        /// </summary>
        /// <value>The high URL.</value>
        public int? HighUrl
        {
            get; set;
        }
    }
}
