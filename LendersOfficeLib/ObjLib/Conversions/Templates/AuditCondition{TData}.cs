﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    using System;
    using DataAccess;

    /// <summary>
    /// A single condition containing data about auditing a particular field in a <see cref="TData"/> object.
    /// </summary>
    /// <typeparam name="TData">
    /// The type of data object that this data object applies to.
    /// E.g. <see cref="CPageData"/>, <see cref="CAppData"/>.
    /// </typeparam>
    public class AuditCondition<TData> : IAuditCondition<TData>
    {
        /// <summary>
        /// Private field for the <see cref="TpoPage"/> property.
        /// </summary>
        private LoanEditorPage tpoPage = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditCondition{TData}"/> class.
        /// </summary>
        /// <param name="dataPointName">
        /// The user-friendly name of the data point for which the condition is being checked.</param>
        /// <param name="valueToDisplay">
        /// A function which retrieves the value from a <typeparamref name="TData"/> instance, 
        /// to be displayed to the user in an error.</param>
        /// <param name="requiredIf">
        /// A function which uses a <typeparamref name="TData"/> to determine whether or not the <see cref="ValueToDisplay"/> is required to be non-empty.
        /// Defaults to always true (always required).</param>
        /// <param name="valueIsValid">
        /// A function which returns whether or not the field's value is valid, based on <typeparamref name="TData"/> data.
        /// Defaults to always true (any value valid).</param>
        /// <param name="displayMessage">
        /// The message to display if the value is invalid. 
        /// Note that a different message is shown for required fields that are missing.</param>
        /// <param name="requiredMessage">
        /// The message to display if the value returned from <see cref="ValueToDisplay"/> is required but missing. 
        /// Note that a different message is shown for invalid fields.</param>
        /// <param name="lqbFieldId">
        /// The LQB field ID to use for determining the element to highlight when clicking a link to correct an error.</param>
        /// <param name="page">
        /// The page information necessary for generating a URL to link for viewing and fixing any condition failure.</param>
        /// <param name="tpoPage">
        /// The page information for generating a URL to link in the TPO portal. Use this if the TPO portal page is different from the main loan editor.
        /// If the TPO portal doesn't have a field anywhere, use <see cref="LoanEditorPage.NoPage"/> to represent that fact.</param>
        public AuditCondition(
            string dataPointName,
            Func<TData, string> valueToDisplay,
            Func<TData, bool> requiredIf = null,
            Func<TData, bool> valueIsValid = null,
            string displayMessage = null,
            string requiredMessage = null,
            string lqbFieldId = null,
            LoanEditorPage page = null,
            LoanEditorPage tpoPage = null)
        {
            this.DataPointName = dataPointName;
            this.ValueToDisplay = valueToDisplay;
            this.RequiredIf = requiredIf ?? AlwaysTrue;
            this.ValueIsValid = valueIsValid ?? AlwaysTrue;
            this.DisplayMessage = displayMessage;
            this.RequiredMessage = requiredMessage;
            this.LqbFieldId = lqbFieldId;
            this.Page = page;
            this.TpoPage = tpoPage;
        }

        /// <summary>
        /// Gets or sets the LQB field ID, for use in highlighting UI inputs.
        /// </summary>
        /// <value>LQB UI field ID.</value>
        public string LqbFieldId
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets the user-friendly name of the field.
        /// </summary>
        /// <value>User-friendly name.</value>
        public string DataPointName
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets a function to use to retrieve the display (string) value of the field.
        /// </summary>
        /// <value>Display value.</value>
        public Func<TData, string> ValueToDisplay
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets a function to determine if the field's value is required.
        /// <para>
        /// Note: This is only a condition that enables checking <see cref="ValueToDisplay"/> for empty.
        /// For more complex logic, use <see cref="ValueIsValid"/>.
        /// </para>
        /// </summary>
        /// <value>Function to get required status.</value>
        public Func<TData, bool> RequiredIf
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets a message to display as an error when the <see cref="RequiredIf"/> function evaluates to true and the value is null or empty.
        /// </summary>
        /// <value>Required message.</value>
        public string RequiredMessage
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets a function to determine if the field's value is valid.
        /// </summary>
        /// <value>Function to get valid status.</value>
        public Func<TData, bool> ValueIsValid
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets the error message to display if the field's value is invalid.
        /// </summary>
        /// <value>Invalid value message.</value>
        public string DisplayMessage
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets the <see cref="LoanEditorPage"/> to use for changing the value in the loan editor.
        /// </summary>
        /// <value>The Loan Editor page for generating the Url to link to in the loan editor.</value>
        public LoanEditorPage Page
        {
            get; protected set;
        }

        /// <summary>
        /// Gets or sets the <see cref="LoanEditorPage"/> to use for changing the value in PML. If null, <see cref="Page"/> is returned instead. 
        /// Ideally, this property wouldn't be necessary. However, some fields are in different pages in the 2 projects,
        /// and some are not available in the TPO portal at all.
        /// <see cref="LoanEditorPage.NoPage"/> indicates that no link is available.
        /// </summary>
        /// <value>The Loan Editor page for generating the Url to link to in the loan editor.</value>
        public LoanEditorPage TpoPage
        {
            get
            {
                return this.tpoPage ?? this.Page;
            }

            protected set
            {
                this.tpoPage = value;
            }
        }

        /// <summary>
        /// Gets an optional name for the integration to display in errors.
        /// </summary>
        /// <value>Integration name.</value>
        protected virtual string IntegrationName
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Evaluates whether this condition is met for the given <typeparamref name="TData"/> instance and returns an error if the audit fails.
        /// </summary>
        /// <param name="data">An instance of the <typeparamref name="TData"/> class to evaluate for meeting the audit condition.</param>
        /// <param name="errorUrl">The error URL to use for initializing the returned error. Should link to a location in the UI to fix the error.</param>
        /// <param name="errorPage">A page object for initializing the returned error. Contains page name and url option for sending to the UI.</param>
        /// <returns>
        /// If the condition fails or encounters errors, a <see cref="AuditError"/> is returned with details.
        /// If the condition is met, null is returned.
        /// </returns>
        public AuditError Audit(TData data, string errorUrl, LoanEditorPage errorPage)
        {
            AuditError error = new AuditError(
                fieldName: this.DataPointName,
                url: errorUrl,
                pageTitle: errorPage?.PageName,
                urlOption: (int?)errorPage?.UrlOption);
            try
            {
                error.Value = this.ValueToDisplay(data);
                if (string.IsNullOrEmpty(error.Value) && this.RequiredIf(data))
                {
                    error.Message = this.RequiredMessage ?? $"\"{this.DataPointName}\" is a required field for{(string.IsNullOrEmpty(IntegrationName) ? null : " ")}{IntegrationName} submission.";
                    return error;
                }
                else if (!this.ValueIsValid(data))
                {
                    error.Message = this.DisplayMessage ?? $"\"{error.Value}\" is not a valid value for \"{this.DataPointName}\".";
                    return error;
                }
            }
            catch (CBaseException e)
            {
                error.Message = e.UserMessage;
                return error;
            }

            // Return null to signify success
            return null;
        }

        /// <summary>
        /// Sets up a <see cref="Func{TData, Boolean}"/> that always returns true, for using as a default.
        /// </summary>
        /// <param name="input">A <typeparamref name="TData"/> instance.</param>
        /// <returns>Always true.</returns>
        protected static bool AlwaysTrue(TData input)
        {
            return true;
        }
    }
}
