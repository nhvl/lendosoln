﻿namespace LendersOffice.Conversions.Templates
{
    using LendersOffice.Integration.Templates;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// An abstract integration payload request auditor.
    /// </summary>
    public abstract class AbstractIntegrationRequestAuditor
    {
        /// <summary>
        /// An audit result.
        /// </summary>
        private IntegrationAuditResult results;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractIntegrationRequestAuditor"/> class.
        /// </summary>
        protected AbstractIntegrationRequestAuditor()
        {
            this.results = new IntegrationAuditResult();
        }

        /// <summary>
        /// Gets an audit result.
        /// </summary>
        /// <value>An audit result.</value>
        public virtual IntegrationAuditResult Results
        {
            get
            {
                return this.results;
            }
        }

        /// <summary>
        /// Records an error if the data point is empty. If not empty, it records the section.
        /// </summary>
        /// <param name="sectionName">The section name for the data point.</param>
        /// <param name="fieldName">The name of the data point.</param>
        /// <param name="fieldUrl">The url where the data point can be found.</param>
        /// <param name="value">The current value of the data point.</param>
        /// <param name="errorMessage">The error message if the data point is not populated.</param>
        protected virtual void RecordErrorIfEmpty(string sectionName, string fieldName, string fieldUrl, string value, string errorMessage)
        {
            this.RecordErrorOnTest(string.IsNullOrEmpty(value), sectionName, fieldName, fieldUrl, value, errorMessage);
        }

        /// <summary>
        /// Records an error if <paramref name="test"/> is true. If false, it records the section.
        /// </summary>
        /// <param name="test">The test result to use.</param>
        /// <param name="sectionName">The section name for the data point.</param>
        /// <param name="fieldName">The name of the data point.</param>
        /// <param name="fieldUrl">The url where the data point can be found.</param>
        /// <param name="value">The current value of the data point.</param>
        /// <param name="errorMessage">The error message if the data point is not populated.</param>
        protected virtual void RecordErrorOnTest(bool test, string sectionName, string fieldName, string fieldUrl, string value, string errorMessage)
        {
            if (test)
            {
                var auditError = this.GenerateAuditError(sectionName, fieldName, fieldUrl, value, errorMessage);
                this.Results.AddError(auditError);
            }
            else
            {
                this.Results.AddSection(sectionName);
            }
        }

        /// <summary>
        /// Creates an audit error item.
        /// </summary>
        /// <param name="sectionName">The name of the audit data point section.</param>
        /// <param name="fieldName">The name of the data point being audited.</param>
        /// <param name="fieldUrl">The URL where the data point can be edited.</param>
        /// <param name="value">The value of the data point.</param>
        /// <param name="errorMessage">The error message triggered by the data point.</param>
        /// <returns>An audit error item.</returns>
        protected virtual IntegrationAuditErrorItem GenerateAuditError(string sectionName, string fieldName, string fieldUrl, string value, string errorMessage)
        {
            return new IntegrationAuditErrorItem(sectionName, fieldName, fieldUrl, value, errorMessage);
        }

        /// <summary>
        /// Will throw a developer exception if value is empty.
        /// </summary>
        /// <param name="fieldName">The field.</param>
        /// <param name="value">The value.</param>
        protected virtual void ThrowIfEmpty(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.InvalidFieldValue, fieldName, value));
            }
        }

        /// <summary>
        /// Will throw a developer exception if value is empty or not matching a required value.
        /// </summary>
        /// <param name="fieldName">The field.</param>
        /// <param name="value">The value.</param>
        /// <param name="requiredValue">The required value.</param>
        protected virtual void ThrowIfEmptyOrNotMatching(string fieldName, string value, string requiredValue)
        {
            if (string.IsNullOrEmpty(value) || value != requiredValue)
            {
                throw new DeveloperException(ErrorMessage.FromTemplate(ErrorMessageTemplate.InvalidFieldValue, fieldName, value));
            }
        }
    }
}
