﻿namespace LendersOffice.Conversions.Templates
{
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Integration.Templates;

    /// <summary>
    /// The ResponseProvider is a component that takes in a vendor response
    /// and parses out relevant data into an object that can be used by
    /// other integration components.
    /// </summary>
    public abstract class AbstractResponseProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractResponseProvider"/> class.
        /// </summary>
        /// <param name="responsePayload">The response payload.</param>
        /// <param name="requestData">The request data.</param>
        public AbstractResponseProvider(string responsePayload, IRequestData requestData)
        {
            this.ResponsePayload = responsePayload;
            this.RequestData = requestData;
        }

        /// <summary>
        /// Gets or sets the data parsed out of the response payload.
        /// </summary>
        /// <value>The data parsed out of the response payload.</value>
        public IResponseData ResponseData { get; protected set; } = null;

        /// <summary>
        /// Gets or sets the errors generated during parsing.
        /// </summary>
        /// <value>Any errors.</value>
        public List<string> Errors { get; protected set; } = new List<string>();

        /// <summary>
        /// Gets a value indicating whether any errors were encountered.
        /// </summary>
        /// <value>Whether any errors were encountered.</value>
        public bool HasErrors => this.Errors.Any();

        /// <summary>
        /// Gets the payload returned by the vendor.
        /// </summary>
        /// <value>The payload returned by the vendor.</value>
        protected string ResponsePayload { get; }

        /// <summary>
        /// Gets or sets the data for the request.
        /// </summary>
        /// <value>The data for the request.</value>
        protected IRequestData RequestData { get; set; }

        /// <summary>
        /// Logs the response.
        /// </summary>
        public abstract void LogResponse();

        /// <summary>
        /// Parses the response payload into a system object.
        /// </summary>
        public abstract void ParseResponse();
    }
}
