﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;

    /// <summary>
    /// Describes the different Loan Editor (or TPO) pages that would be linked to from a loan file Audit.
    /// </summary>
    public class LoanEditorPage
    {
        /// <summary>
        /// Represents an absence of any page to link to (in the TPO portal, for instance).
        /// </summary>
        public static readonly LoanEditorPage NoPage =
            new LoanEditorPage(
                pageName: "Field editing link not available.",
                pmlUrlFunction: (loanId, appId, fieldId) => null);

        /// <summary>
        /// Page info for linking to the 1003, page 1.
        /// </summary>
        public static readonly LoanEditorPage Application1003Page1 =
            new LoanEditorPage(
                pageName: "1003 Page 1",
                pmlUrlFunction: (loanId, appId, fieldId) => $"/webapp/Loan1003.aspx?loanID={loanId:D}&applicationId={appId:D}&src=&p=0&highlightId=Loan1003pg1_{fieldId}",
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/Forms/Loan1003.aspx?pg=0&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_1003_1);

        /// <summary>
        /// Page info for linking to the 1003, page 2.
        /// </summary>
        public static readonly LoanEditorPage Application1003Page2 =
            new LoanEditorPage(
                pageName: "1003 Page 2",
                pmlUrlFunction: (loanId, appId, fieldId) => $"/webapp/Loan1003.aspx?loanID={loanId:D}&applicationId={appId:D}&src=&p=1&highlightId=Loan1003pg2_{fieldId}",
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/Forms/Loan1003.aspx?pg=1&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_1003_2);

        /// <summary>
        /// Page info for linking to the 1003, page 3.
        /// </summary>
        public static readonly LoanEditorPage Application1003Page3 =
            new LoanEditorPage(
                pageName: "1003 Page 3",
                pmlUrlFunction: (loanId, appId, fieldId) => $"/webapp/Loan1003.aspx?loanID={loanId:D}&applicationId={appId:D}&src=&p=2&highlightId=Loan1003pg3_{fieldId}",
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/Forms/Loan1003.aspx?pg=2&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_1003_3);

        /// <summary>
        /// Page info for linking to the Fannie Addendum.
        /// </summary>
        public static readonly LoanEditorPage FannieAddendum =
            new LoanEditorPage(
                pageName: "Fannie Mae Addendum",
                pmlUrlFunction: (loanId, appId, fieldId) => $"/webapp/LoanInformation.aspx?&loanid={loanId:D}&tab=FannieMaeAddendumSections&highlightId={fieldId}",
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/FannieAddendum/FannieAddendum.aspx?mode=exportdu&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_FannieAddendum);

        /// <summary>
        /// Page info for linking to the PML Summary.
        /// </summary>
        public static readonly LoanEditorPage PmlSummary =
            new LoanEditorPage(
                pageName: "PML Summary",
                pmlUrlFunction: (loanId, appId, fieldId) => $"/webapp/pml.aspx?&loanid={loanId:D}&highlightId={fieldId}",
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/Underwriting/PmlReviewInfo.aspx?loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_PmlReviewInfo);

        /// <summary>
        /// Page info for linking to the "This Loan Info" page. PML doesn't have this page, so the quickpricer page is used instead.
        /// </summary>
        public static readonly LoanEditorPage ThisLoanInfo =
            new LoanEditorPage(
                pageName: "This Loan Info",
                pmlUrlFunction: PmlSummary.PmlUrlFunction,
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/LoanInfo.aspx?pg=0&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_LoanInformation);

        /// <summary>
        /// Page info for linking to the Loan Terms page.
        /// </summary>
        public static readonly LoanEditorPage LoanTerms =
            new LoanEditorPage(
                pageName: "Loan Terms",
                pmlUrlFunction: (loanId, appId, fieldId) => $"/webapp/LoanInformation.aspx?&loanid={loanId:D}&highlightId={fieldId}#?open-tab=0",
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/Forms/LoanTerms.aspx?pg=0&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_LoanTerms);

        /// <summary>
        /// Page info for linking to Borrower Employment.
        /// </summary>
        public static readonly LoanEditorPage BorrowerEmployment =
            new LoanEditorPage(
                pageName: "Borrower Employment",
                pmlUrlFunction: Application1003Page1.PmlUrlFunction,
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/BorrowerInfo.aspx?pg=1&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_BorrowerInfoNoTab);

        /// <summary>
        /// Page info for linking to the "Submit to LPA" page.
        /// </summary>
        public static readonly LoanEditorPage SubmitToLpa =
            new LoanEditorPage(
                pageName: "Submit to LPA",
                pmlUrlFunction: (loanId, appId, fieldId) => $"/webapp/LoanInformation.aspx?&loanid={loanId:D}&tab=FreddieMacAddendumSections&highlightId={fieldId}",
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/FreddieExport.aspx?loanid={loanId:D}&printid=&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_SubmitToLPA);

        /// <summary>
        /// Page info for linking to the "Liabilities" page.
        /// </summary>
        /// // TODO LPA Seamless: set up the urloptions for liabilities and assets pages here.
        public static readonly LoanEditorPage Liabilities =
            new LoanEditorPage(
                pageName: "Liabilities",
                pmlUrlFunction: Application1003Page2.PmlUrlFunction,
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/BorrowerInfo.aspx?pg=4&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_Liabilities);

        /// <summary>
        /// Page info for linking to the "Assets" page.
        /// </summary>
        public static readonly LoanEditorPage Assets =
            new LoanEditorPage(
                pageName: "Assets",
                pmlUrlFunction: Application1003Page2.PmlUrlFunction,
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/BorrowerInfo.aspx?pg=5&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_Assets);

        /// <summary>
        /// Page info for linking to the "FHA Addendum" page. This page doesn't appear to be available in the TPO portal.
        /// </summary>
        public static readonly LoanEditorPage FhaAddendum = 
            new LoanEditorPage(
                pageName: "FHA Addendum",
                pmlUrlFunction: (loanId, appId, fieldId) => null,
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/FHA/FHAAddendum.aspx?highlightId={fieldId}&loanid={loanId:D}&appid={appId:D}",
                urlOption: E_UrlOption.Page_FHAAddendum);

        /// <summary>
        /// Page info for linking to the "Monthly Income" page.
        /// </summary>
        public static readonly LoanEditorPage MonthlyIncome =
            new LoanEditorPage(
                pageName: "Monthly Income",
                pmlUrlFunction: Application1003Page2.PmlUrlFunction,
                loanEditorUrlFunction: (loanId, appId, fieldId) => $"/newlos/BorrowerInfo.aspx?pg=3&loanid={loanId:D}&appid={appId:D}&highlightId={fieldId}",
                urlOption: E_UrlOption.Page_BorrowerMonthlyIncome);

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanEditorPage"/> class.
        /// </summary>
        /// <param name="pageName">The name of the page, suitable for display.</param>
        /// <param name="pmlUrlFunction">The function for generating a link to the page in the TPO Portal (PML project) with a field ID.</param>
        /// <param name="loanEditorUrlFunction">The function for generating a link to the page in the Loan Editor (LendersOfficeApp project) with a field ID.</param>
        /// <param name="urlOption">The <see cref="E_UrlOption"/> value corresponding to the page. Used for Embedded PML navigation.</param>
        private LoanEditorPage(string pageName, UrlFunction pmlUrlFunction, UrlFunction loanEditorUrlFunction = null, E_UrlOption? urlOption = null)
        {
            this.PageName = pageName;
            this.PmlUrlFunction = pmlUrlFunction;
            this.LoUrlFunction = loanEditorUrlFunction ?? pmlUrlFunction;
            this.UrlOption = urlOption;
        }

        /// <summary>
        /// Defines a delegate type for generating URLs from loan/app data.
        /// </summary>
        /// <param name="loanId">The loan ID of the loan context for the page.</param>
        /// <param name="appId">The app ID, if needed, for the application context of the page (e.g. 1003).</param>
        /// <param name="fieldId">The field ID, for highlighting the field on the page, if implemented.</param>
        /// <returns>The URL built.</returns>
        public delegate string UrlFunction(Guid loanId, Guid? appId, string fieldId);
        
        /// <summary>
        /// Gets the display-friendly name of the page.
        /// </summary>
        /// <value>Display name.</value>
        public string PageName
        {
            get; private set;
        }

        /// <summary>
        /// Gets a function for generating a TPO portal URL from loan ID, app ID, and field ID.
        /// </summary>
        /// <value>Url generation function for the TPO portal / PML project.</value>
        public UrlFunction PmlUrlFunction
        {
            get; private set;
        }

        /// <summary>
        /// Gets a function for generating a "main loan editor" (LendersOffice[App]) URL from loan ID, app ID, and field ID.
        /// </summary>
        /// <value>Url generation function for the loan editor / LendersOfficeApp project.</value>
        public UrlFunction LoUrlFunction
        {
            get; private set;
        }

        /// <summary>
        /// Gets the "URL Option" for the page, allowing the loanapp.aspx page to navigate to the correct "highURL" page using <see cref="Tools.UrlOptionToUrl"/>.
        /// Used for navigation from embedded PML.
        /// </summary>
        /// <value>URL Option value corresponding to the page.</value>
        public E_UrlOption? UrlOption
        {
            get; private set;
        }
    }
}
