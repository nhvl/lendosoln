﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Represents the result of running a loan data audit.
    /// This class is serialized to JSON for displaying feedback in the UI.
    /// </summary>
    public class LoanAuditResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanAuditResult"/> class.
        /// </summary>
        /// <param name="loanErrors">A list of loan errors found by the audit.</param>
        /// <param name="borrowerAudits">An audit result from auditing each borrower on the loan.</param>
        public LoanAuditResult(IEnumerable<AuditError> loanErrors, IEnumerable<BorrowerAuditResult> borrowerAudits)
        {
            this.LoanErrors = loanErrors.ToList();
            this.BorrowerAudits = borrowerAudits.ToList();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanAuditResult"/> class.
        /// Parameter-less constructor for serializability.
        /// </summary>
        public LoanAuditResult()
        {
            this.LoanErrors = new List<AuditError>();
            this.BorrowerAudits = new List<BorrowerAuditResult>();
        }

        /// <summary>
        /// Gets or sets a list of Loan-level errors found by the audit.
        /// </summary>
        /// <value>A list of loan errors.</value>
        public List<AuditError> LoanErrors
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets a list of audit results for each borrower on the loan.
        /// </summary>
        /// <value>A list of borrower audit results.</value>
        public List<BorrowerAuditResult> BorrowerAudits
        {
            get; set;
        }
    }
}
