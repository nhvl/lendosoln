﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// An interface which provides audit conditions for loan-level data and for borrower-level data.
    /// </summary>
    public interface ILoanAuditConditions
    {
        /// <summary>
        /// Gets an enumerable list of data audit conditions for loan-level data.
        /// </summary>
        /// <returns>Loan-level audit conditions.</returns>
        IEnumerable<IAuditCondition<CPageData>> GetLoanDataConditions();

        /// <summary>
        /// Gets an enumerable list of data audit conditions for borrower-level data.
        /// </summary>
        /// <returns>Borrower-level audit conditions.</returns>
        IEnumerable<IAuditCondition<CAppData>> GetBorrowerDataConditions();
    }
}
