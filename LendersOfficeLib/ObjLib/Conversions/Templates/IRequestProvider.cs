﻿namespace LendersOffice.Conversions.Templates
{
    using LendersOffice.Integration.Templates;

    /// <summary>
    /// Specifies an implementation that should be followed by all Title request providers.
    /// </summary>
    public interface IRequestProvider
    {
        /// <summary>
        /// Audits the request and returns any errors found.
        /// </summary>
        /// <returns>An audit result.</returns>
        IntegrationAuditResult AuditRequest();

        /// <summary>
        /// Populates the request payload and serializes it to a string.
        /// </summary>
        /// <returns>A payload string.</returns>
        string SerializeRequest();

        /// <summary>
        /// Logs the request.
        /// </summary>
        /// <param name="request">The request string, in case it is needed.</param>
        void LogRequest(string request = null);
    }
}