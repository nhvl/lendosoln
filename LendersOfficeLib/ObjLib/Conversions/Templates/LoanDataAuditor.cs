﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using Common;
    using DataAccess;

    /// <summary>
    /// Audits the data in a loan file and produces a <see cref="LoanAuditResult"/> representing the result.
    /// </summary>
    public class LoanDataAuditor
    {
        /// <summary>
        /// The loan data object to audit.
        /// </summary>
        private CPageData loanData;

        /// <summary>
        /// Whether the audit is being run from PML.
        /// This is used when determining which links to return to the UI.
        /// </summary>
        private bool isPml;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanDataAuditor"/> class with the given loan data.
        /// </summary>
        /// <param name="loanData">Loan data to check in the audit.</param>
        /// <param name="loanAuditConditions">A collection of audit conditions to check at the loan level.</param>
        /// <param name="borrowerAuditConditions">A collection of audit conditions to check at the borrower level.</param>
        /// <param name="isPml">Whether the audit is being run from PML.</param>
        public LoanDataAuditor(CPageData loanData, IEnumerable<IAuditCondition<CPageData>> loanAuditConditions, IEnumerable<IAuditCondition<CAppData>> borrowerAuditConditions, bool isPml = false)
        {
            this.loanData = loanData;
            this.isPml = isPml;
            this.LoanConditions = loanAuditConditions;
            this.BorrowerConditions = borrowerAuditConditions;
        }

        /// <summary>
        /// Gets or sets collection of audit conditions to check in the loan audit.
        /// </summary>
        private IEnumerable<IAuditCondition<CPageData>> LoanConditions
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets collection of audit conditions to check for each borrower on the loan file.
        /// </summary>
        private IEnumerable<IAuditCondition<CAppData>> BorrowerConditions
        {
            get; set;
        }

        /// <summary>
        /// Runs a loan audit statically, without the context of a <see cref="LoanDataAuditor"/>.
        /// This method loads its own <see cref="CPageData"/> and auditor and runs the audit itself, using default settings.
        /// </summary>
        /// <param name="loanId">The loan ID of the loan to run the audit for.</param>
        /// <param name="auditConditionCollection">A collection of audit conditions to check at the loan and borrower levels.</param>
        /// <param name="isPml">Whether the audit is being run from PML.</param>
        /// <returns>The result of running the audit.</returns>
        public static LoanAuditResult RunAudit(
            Guid loanId, 
            ILoanAuditConditions auditConditionCollection, 
            bool isPml = false)
        {
            // loanData needs the dependencies for the specific ILoanAuditConditions type, 
            // as well as a few field dependencies for this class.
            // CreateUsingSmartDependency won't have a compile-time ILoanAuditConditions type to use.
            // So explicitly build the dependency list for the run-time type and merge with the dependencies of LoanDataAuditor.
            IEnumerable<string> dependentFields =
                CPageData.GetCPageBaseAndCAppDataDependencyList(auditConditionCollection.GetType())
                .Union(CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(LoanDataAuditor)));
            var loanData = new CPageData(loanId, dependentFields);
            loanData.InitLoad();
            var auditor = new LoanDataAuditor(loanData, auditConditionCollection.GetLoanDataConditions(), auditConditionCollection.GetBorrowerDataConditions(), isPml);
            return auditor.RunAudit();
        }

        /// <summary>
        /// Runs an audit on this Auditor instance's own loan data object.
        /// Determines if the necessary field values are filled out and valid according to the audit.
        /// </summary>
        /// <returns>The result of running the audit.</returns>
        public LoanAuditResult RunAudit()
        {
            var auditResult = new LoanAuditResult(
                loanErrors: this.RunLoanAudit(),
                borrowerAudits: this.loanData.Apps.SelectMany(app => this.RunBorrowerAudits(app, this.isPml)).ToList());

            return auditResult;
        }

        /// <summary>
        /// Runs borrower audit(s) on a particular app to determine if the necessary values are filled out and valid for DU Submission.
        /// </summary>
        /// <param name="app">The application to get borrower values from.</param>
        /// <param name="isPml">Whether the audit is being run from PML.</param>
        /// <returns>The result of the borrower audit(s).</returns>
        private IEnumerable<BorrowerAuditResult> RunBorrowerAudits(CAppData app, bool isPml)
        {
            // Main Borrower audit
            yield return this.RunBorrowerAudit(app, E_BorrowerModeT.Borrower, isPml);

            // Co-borrower audit
            if (app.aHasSpouse)
            {
                yield return this.RunBorrowerAudit(app, E_BorrowerModeT.Coborrower, isPml);
            }
        }

        /// <summary>
        /// Runs a single borrower audit using the specified borrower mode, determining 
        /// if the necessary values are filled out and valid for DU Submission..
        /// </summary>
        /// <param name="app">The application to get borrower values from.</param>
        /// <param name="borrowerMode">The mode (Borrower or Coborrower) for the audit.</param>
        /// <param name="isPml">Whether the audit is being run from PML.</param>
        /// <returns>The result of the borrower audit.</returns>
        private BorrowerAuditResult RunBorrowerAudit(CAppData app, E_BorrowerModeT borrowerMode, bool isPml)
        {
            var borrowerAudit = new BorrowerAuditResult();
            app.BorrowerModeT = borrowerMode;
            borrowerAudit.FullName = app.aNm;

            foreach (IAuditCondition<CAppData> condition in this.BorrowerConditions)
            {
                LoanEditorPage conditionPage = isPml ? condition.TpoPage : condition.Page;
                string fieldHighlightId = condition.LqbFieldId;

                // Convert borrower field IDs to coborrower field ids.
                if (app.BorrowerModeT == E_BorrowerModeT.Coborrower && fieldHighlightId != null)
                {
                    fieldHighlightId = Regex.Replace(fieldHighlightId, "^aB", "aC");
                }

                string conditionUrl = isPml ?
                    conditionPage?.PmlUrlFunction(app.LoanData.sLId, app.aAppId, fieldHighlightId)
                    : conditionPage?.LoUrlFunction(app.LoanData.sLId, app.aAppId, fieldHighlightId);

                borrowerAudit.Errors.AddIfNotNull(condition.Audit(app, conditionUrl, conditionPage));
            }

            return borrowerAudit;
        }

        /// <summary>
        /// Runs an audit on loan data to determine if the necessary values are filled out and valid for DU Submission.
        /// </summary>
        /// <returns>A list of audit errors that occured while checking. Empty if no errors.</returns>
        private List<AuditError> RunLoanAudit()
        {
            List<AuditError> loanErrors = new List<AuditError>();
            foreach (IAuditCondition<CPageData> condition in this.LoanConditions)
            {
                LoanEditorPage conditionPage = this.isPml ? condition.TpoPage : condition.Page;
                string conditionUrl = this.isPml ?
                    conditionPage?.PmlUrlFunction(this.loanData.sLId, this.loanData.GetAppData(0)?.aAppId ?? Guid.Empty, condition.LqbFieldId)
                    : conditionPage?.LoUrlFunction(this.loanData.sLId, this.loanData.GetAppData(0)?.aAppId ?? Guid.Empty, condition.LqbFieldId);
                loanErrors.AddIfNotNull(condition.Audit(this.loanData, conditionUrl, conditionPage));
            }

            return loanErrors;
        }
    }
}
