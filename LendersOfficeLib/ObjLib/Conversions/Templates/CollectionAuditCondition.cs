﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;

    /// <summary>
    /// Represents an audit condition for auditing a collection of items as a single audit condition.
    /// </summary>
    /// <example>This could be used to validate each asset on a loan file, each employment record on an application, etc.</example>
    /// <typeparam name="TData">
    /// The type of data object being audited (<see cref="CPageData"/>, <see cref="CAppData"/>),
    /// which contains the collection to be audited.</typeparam>
    /// <typeparam name="TCollectionItem">The type of item contained within the collection.</typeparam>
    public class CollectionAuditCondition<TData, TCollectionItem> : IAuditCondition<TData>
    {
        /// <summary>
        /// Private field for the <see cref="TpoPage"/> property.
        /// </summary>
        private LoanEditorPage tpoPage;

        /// <summary>
        /// Initializes a new instance of the <see cref="CollectionAuditCondition{TData, TCollectionItem}"/> class.
        /// </summary>
        /// <param name="dataPointName">
        /// The user-friendly name to use for the data point in the UI.</param>
        /// <param name="collectionToAudit">
        /// A function for obtaining the collection from a <see cref="TData"/> instance being audited.</param>
        /// <param name="collectionItemValueToDisplay">
        /// A function for converting a collection item into a string representing the audited value in the UI.</param>
        /// <param name="collectionItemIsValid">
        /// A function for determining the validity of a collection item from TData and data.</param>
        /// <param name="displayMessage">
        /// The message to display if the value is invalid.</param>
        /// <param name="lqbFieldId">
        /// The LQB field ID to use for determining the element to highlight when clicking a link to correct an error.</param>
        /// <param name="page">
        /// The page information necessary for generating a URL to link for viewing and fixing the data value.</param>
        /// <param name="tpoPage">
        /// The page information for generating a URL to link in the TPO portal. Use this if the TPO portal page is different from the main loan editor.
        /// If the TPO portal doesn't have a field anywhere, use <see cref="LoanEditorPage.NoPage"/> to represent that fact.</param>
        public CollectionAuditCondition(
            string dataPointName, 
            Func<TData, IEnumerable<TCollectionItem>> collectionToAudit,
            Func<TCollectionItem, string> collectionItemValueToDisplay,
            Func<TData, TCollectionItem, bool> collectionItemIsValid, 
            string displayMessage = null, 
            string lqbFieldId = null, 
            LoanEditorPage page = null, 
            LoanEditorPage tpoPage = null) 
        {
            this.DataPointName = dataPointName;
            this.CollectionToAudit = collectionToAudit;
            this.CollectionItemValueToDisplay = collectionItemValueToDisplay;
            this.CollectionItemIsValid = collectionItemIsValid;
            this.DisplayMessage = displayMessage;
            this.LqbFieldId = lqbFieldId;
            this.Page = page;
            this.tpoPage = tpoPage;
        }

        /// <summary>
        /// Gets the user-friendly name of the field.
        /// </summary>
        /// <value>User-friendly name.</value>
        public string DataPointName { get; private set; }

        /// <summary>
        /// Gets the error message to display if the audit condition results in a failure.
        /// </summary>
        /// <value>Invalid value message.</value>
        public string DisplayMessage { get; }

        /// <summary>
        /// Gets the LQB field ID, for use in highlighting UI inputs.
        /// </summary>
        /// <value>LQB UI field ID.</value>
        public string LqbFieldId { get; }

        /// <summary>
        /// Gets the <see cref="LoanEditorPage"/> to use for changing the value in the loan editor.
        /// </summary>
        /// <value>The Loan Editor page for generating the Url to link to in the loan editor.</value>
        public LoanEditorPage Page { get; }

        /// <summary>
        /// Gets the <see cref="LoanEditorPage"/> to use for changing the value in PML. If null, <see cref="Page"/> is returned instead. 
        /// Ideally, this property wouldn't be necessary. However, some fields are in different pages in the 2 projects,
        /// and some are not available in the TPO portal at all.
        /// <see cref="LoanEditorPage.NoPage"/> indicates that no link is available.
        /// </summary>
        /// <value>The Loan Editor page for generating the Url to link to in the loan editor.</value>
        public LoanEditorPage TpoPage
        {
            get
            {
                return this.tpoPage ?? this.Page;
            }
        }

        /// <summary>
        /// Gets a function to extract a collection to audit from a <see cref="TData"/> object.
        /// </summary>
        /// <value>Function to retrieve a collection to audit.</value>
        private Func<TData, IEnumerable<TCollectionItem>> CollectionToAudit { get; }

        /// <summary>
        /// Gets a function to extract the audited value from a <see cref="TCollectionItem"/> object.
        /// </summary>
        /// <value>Function to retrieve a display value.</value>
        private Func<TCollectionItem, string> CollectionItemValueToDisplay { get; }

        /// <summary>
        /// Gets a function to determine whether a collection item is valid.
        /// </summary>
        /// <value>Function to determine if a collection item is valid.</value>
        private Func<TData, TCollectionItem, bool> CollectionItemIsValid { get; }

        /// <summary>
        /// Evaluates whether this condition is met for the given <typeparamref name="TData"/> instance and returns an error if the audit fails.
        /// </summary>
        /// <param name="data">An instance of the <typeparamref name="TData"/> class to evaluate for meeting the audit condition.</param>
        /// <param name="errorUrl">The error URL to use for initializing the returned error. Should link to a location in the UI to fix the error.</param>
        /// <param name="errorPage">A page object for initializing the returned error. Contains page name and url option for sending to the UI.</param>
        /// <returns>
        /// If the condition fails or encounters errors, a <see cref="AuditError"/> is returned with details.
        /// If the condition is met, null is returned.
        /// </returns>
        public AuditError Audit(TData data, string errorUrl, LoanEditorPage errorPage)
        {
            AuditError error = new AuditError(
                fieldName: this.DataPointName,
                url: errorUrl,
                pageTitle: errorPage?.PageName,
                urlOption: (int?)errorPage?.UrlOption);

            try
            {
                List<TCollectionItem> collection = this.CollectionToAudit(data).ToList();
                List<TCollectionItem> invalidItems = collection.Where((item) => !this.CollectionItemIsValid(data, item)).ToList();
                error.Value = string.Join(", ", invalidItems.Select(this.CollectionItemValueToDisplay).Where(value => !string.IsNullOrEmpty(value)));
                if (!invalidItems.Any())
                {
                    // Successful validation, no error.
                    return null;
                }
                else
                {
                    if (string.IsNullOrEmpty(error.Value))
                    {
                        error.Value = $"{invalidItems.Count} Empty Value{(invalidItems.Count > 1 ? "s" : string.Empty)}";
                    }

                    error.Message = this.DisplayMessage ?? $"One or more values for \"{this.DataPointName}\" were invalid.";
                    return error;
                }
            }
            catch (CBaseException e)
            {
                error.Message = e.UserMessage;
                return error;
            }
        }
    }
}
