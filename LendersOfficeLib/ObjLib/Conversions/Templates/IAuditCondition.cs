﻿namespace LendersOffice.ObjLib.Conversions.Templates
{
    /// <summary>
    /// Defines an interface for different types of audit conditions to implement.
    /// </summary>
    /// <typeparam name="TData">The type of data class to be audited.</typeparam>
    public interface IAuditCondition<TData>
    {
        /// <summary>
        /// Gets the <see cref="LoanEditorPage"/> to use for changing the value in the loan editor.
        /// </summary>
        /// <value>The Loan Editor page for generating the Url to link to in the loan editor.</value>
        LoanEditorPage Page { get; }

        /// <summary>
        /// Gets the <see cref="LoanEditorPage"/> to use for changing the value in PML. If null, <see cref="Page"/> is returned instead. 
        /// Ideally, this property wouldn't be necessary. However, some fields are in different pages in the 2 projects,
        /// and some are not available in the TPO portal at all.
        /// <see cref="LoanEditorPage.NoPage"/> indicates that no link is available.
        /// </summary>
        /// <value>The Loan Editor page for generating the Url to link to in the loan editor.</value>
        LoanEditorPage TpoPage { get; }

        /// <summary>
        /// Gets the LQB field ID, for use in highlighting UI inputs.
        /// </summary>
        /// <value>LQB UI field ID.</value>
        string LqbFieldId { get; }

        /// <summary>
        /// Evaluates whether this audit condition is met for the given <typeparamref name="TData"/> instance and returns an error if the audit fails.
        /// </summary>
        /// <param name="data">An instance of the <typeparamref name="TData"/> class to evaluate for meeting the audit condition.</param>
        /// <param name="errorUrl">The error URL to use for initializing the returned error. Should link to a location in the UI to fix the error.</param>
        /// <param name="errorPage">A page object for initializing the returned error. Contains page name and url option for sending to the UI.</param>
        /// <returns>
        /// If the condition fails or encounters errors, a <see cref="AuditError"/> will be populated with details.
        /// If the condition is met, null is returned.
        /// </returns>
        AuditError Audit(TData data, string errorUrl, LoanEditorPage errorPage);
    }
}
