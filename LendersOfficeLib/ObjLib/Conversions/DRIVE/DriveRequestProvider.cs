﻿namespace LendersOffice.Conversions.Drive
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions.Drive.Request;

    /// <summary>
    /// Creates and populates a DRIVE request.
    /// </summary>
    public class DriveRequestProvider
    {
        protected enum E_SendParticipantAs
        {
            Person,
            Company
        }

        private DRIVERequest m_request; // The internal representation of our data
        
        private Guid m_sLId; // The loan we're exporting
        private CPageData m_dataLoan;
        private CAppData[] m_dataApps;
        private CAppData m_dataApp;

        private string m_username;
        private string m_password;

        private int m_participantAppraiserCount;
        private int m_participantLoanOfficerCount;
        private int m_participantSellerCount;
        private int m_participantListingAgentCount;
        private int m_participantSellingAgentCount;
        private int m_participantCloserCount;
        private int m_participantProcessorCount;
        private int m_participantUnderwriterCount;
        private int m_participantEscrowCount;

        public DriveRequestProvider(string username, string password, Guid sLId)
        {
            m_sLId = sLId;
            
            m_dataLoan = CPageData.CreateUsingSmartDependency(m_sLId, typeof(DriveRequestProvider));
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing); // MPTODO: create a new format_target for DRIVE
            m_dataLoan.ByPassFieldSecurityCheck = true;

            m_dataApps = new CAppData[m_dataLoan.nApps];
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                m_dataApps[i] = m_dataLoan.GetAppData(i);
            }
            if (m_dataLoan.nApps > 0)
            {
                m_dataApp = m_dataApps[0];
            }

            m_username = username;
            m_password = password;

            m_participantAppraiserCount = 0;
            m_participantLoanOfficerCount = 0;
            m_participantSellerCount = 0;
            m_participantListingAgentCount = 0;
            m_participantSellingAgentCount = 0;
            m_participantCloserCount = 0;
            m_participantProcessorCount = 0;
            m_participantUnderwriterCount = 0;
            m_participantEscrowCount = 0;
        }

        private Authentication CreateAuthentication()
        {
            Authentication authentication = new Authentication();
            authentication.Username = m_username;
            authentication.Password = m_password;
            authentication.PortalID = "LENDINGQB";
            return authentication;
        }
        private AccessGroup CreateAccessGroup()
        {
            AccessGroup accessgroup = new AccessGroup();
            // accessgroup.Code = null;
            return accessgroup;
        }
        private LoanHeader CreateLoanHeader()
        {
            LoanHeader loanheader = new LoanHeader();
            // loanheader.AccessGroup = null;
            return loanheader;
        }
        private PropertyInformation CreatePropertyInformation()
        {
            PropertyInformation propertyinformation = new PropertyInformation();
            propertyinformation.StreetAddress = m_dataLoan.sSpAddr;
            propertyinformation.City = m_dataLoan.sSpCity;
            propertyinformation.State = m_dataLoan.sSpState;
            propertyinformation.ZIP = m_dataLoan.sSpZip;
            propertyinformation.LoanPurpose = DriveConvert.ToDrive(m_dataLoan.sLPurposeT);
            propertyinformation.RefinancePurpose = DriveConvert.ToDrive(m_dataLoan.sRefPurpose, m_dataLoan.sLPurposeT);
            propertyinformation.PropertyUsageType = DriveConvert.ToDrive(m_dataLoan.sOccT);
            propertyinformation.AppraisedValue = m_dataLoan.sApprVal_rep;
            propertyinformation.PropertyType = DriveConvert.ToDrive(m_dataLoan.sGseSpT, m_dataLoan.sUnitsNum);
            propertyinformation.ValuationDate = m_dataLoan.sSpValuationEffectiveD_rep;
            propertyinformation.APN = m_dataLoan.sAssessorsParcelId;
            propertyinformation.OccupancyType = DriveConvert.ToDrive(m_dataLoan.sLPurposeT, m_dataLoan.sOccT, m_dataLoan.sOccR);
            propertyinformation.StructureBuiltYear = m_dataLoan.sYrBuilt;
            propertyinformation.LienPriorityType = DriveConvert.ToDrive(m_dataLoan.sLienPosT);
            propertyinformation.NumberOfFinancedUnits = m_dataLoan.sUnitsNum_rep;
            return propertyinformation;
        }
        private MailingAddress CreateMailingAddress(CAppData dataApp)
        {
            MailingAddress mailingaddress = new MailingAddress();
            mailingaddress.StreetAddress = dataApp.aBAddrMail;
            mailingaddress.City = dataApp.aBCityMail;
            mailingaddress.State = dataApp.aBStateMail;
            mailingaddress.ZIP = dataApp.aBZipMail;
            return mailingaddress;
        }

        private Employer CreateEmployer(IRegularEmploymentRecord record)
        {
            Employer employer = new Employer();
            employer.CompanyName = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.ZIP = record.EmplrZip;
            employer.SelfEmployed = DriveConvert.ToYN(record.IsSelfEmplmt);
            employer.YearsOnJob = record.EmplmtLenInYrs_rep;
            employer.PositionTitle = record.JobTitle;

            string phone = Regex.Replace(record.EmplrBusPhone, @"[() -.]", "");
            employer.EmployerPhone = phone;

            // employer.LineOfWorkYears = record.ProfLen_rep;
            employer.CurrentEmployerIndicator = DriveConvert.ToYN(record.IsCurrent);
            return employer;
        }

        private Employer CreateEmployer(IPrimaryEmploymentRecord record)
        {
            Employer employer = new Employer();
            employer.CompanyName = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.ZIP = record.EmplrZip;
            employer.SelfEmployed = DriveConvert.ToYN(record.IsSelfEmplmt);
            employer.YearsOnJob = record.EmplmtLen_rep;
            employer.PositionTitle = record.JobTitle;
            
            string phone = Regex.Replace(record.EmplrBusPhone, @"[() -.]", "");
            employer.EmployerPhone = phone;
            
            employer.LineOfWorkYears = record.ProfLen_rep;
            employer.CurrentEmployerIndicator = DriveConvert.ToYN(record.IsCurrent);
            return employer;
        }
        private REO CreateREO(IRealEstateOwned rEOFields, int iREOSequenceNumber)
        {
            REO reo = new REO();
            reo.SequenceNumber = iREOSequenceNumber.ToString();
            reo.StreetAddress = rEOFields.Addr;
            reo.City = rEOFields.City;
            reo.State = rEOFields.State;
            reo.ZIP = rEOFields.Zip;
            //reo.ZIP4 = null;
            reo.CurrentResidenceIndicator = DriveConvert.ToYN(rEOFields.StatT == E_ReoStatusT.Residence);
            reo.DispositionStatusType = DriveConvert.ToDrive(rEOFields.StatT);
            reo.GSEPropertyType = DriveConvert.ToDrive(rEOFields.TypeT);
            reo.LienInstallmentAmount = rEOFields.MPmt_rep;
            reo.LienUPBAmount = rEOFields.MAmt_rep;
            reo.MaintenanceExpenseAmount = rEOFields.HExp_rep;
            reo.MarketValueAmount = rEOFields.Val_rep;
            reo.RentalIncomeGrossAmount = rEOFields.GrossRentI_rep;
            reo.RentalIncomeNetAmount = rEOFields.NetRentI_rep;
            reo.SubjectIndicator = DriveConvert.ToYN(rEOFields.IsSubjectProp);

            return reo;
        }
        private Asset CreateAsset()
        {
            Asset asset = new Asset();
            // asset.AssetType = null;
            // asset.HolderName = null;
            // asset.HolderStreetAddress = null;
            // asset.HolderCity = null;
            // asset.HolderState = null;
            // asset.HolderPostalCode = null;
            // asset.CashOrMarketValueAmount = null;
            // asset.FaceValueAmount = null;
            // asset.Qty = null;
            // asset.ShortDescription = null;
            // asset.LongDescription = null;
            // asset.AccountIdentifier = null;
            return asset;
        }

        private Borrower CreateBorrower(CAppData dataApp, int sequenceNumber)
        {
            Borrower borrower = new Borrower();
            borrower.SequenceNumber = sequenceNumber.ToString();
            // borrower.Prefix = null;
            borrower.FirstName = dataApp.aFirstNm;
            borrower.MiddleName = dataApp.aMidNm;
            borrower.LastName = dataApp.aLastNm;
            borrower.Suffix = dataApp.aSuffix;
            borrower.SSN = dataApp.aSsn;
            borrower.DOB = dataApp.aDob_rep;
            borrower.StreetAddress = dataApp.aAddr;
            borrower.City = dataApp.aCity;
            borrower.State = dataApp.aState;
            borrower.ZIP = dataApp.aZip;
            borrower.AgeAtApplicationYears = dataApp.aAge_rep;

            YearMonthParser yearMonthParser = new YearMonthParser();
            yearMonthParser.Parse(dataApp.aAddrYrs); //isBorrower ? dataApp.aBAddrYrs : dataApp.aCAddrYrs);
            borrower.YearsAtAddress = yearMonthParser.NumberOfYears.ToString();
            borrower.MonthsAtAddress = yearMonthParser.NumberOfMonths.ToString();

            borrower.USCitizenship = DriveConvert.ToDriveUSCitizenship(dataApp.aDecCitizen, dataApp.aDecResidency);

            borrower.BaseIncome = dataApp.aBaseI_rep;

            borrower.Overtime = dataApp.aOvertimeI_rep;
            borrower.Bonus = dataApp.aBonusesI_rep;
            borrower.Commissions = dataApp.aCommisionI_rep;
            borrower.DividendsInterest = dataApp.aDividendI_rep;
            borrower.NetRentalIncome = dataApp.aNetRentI1003_rep;

            // aTotOI and aTotI sum the borrower and coborrower amounts, rather
            // than selecting between them. We must choose manually.
            borrower.URLABorrowerTotalOtherIncomeAmount = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower
                ? dataApp.aBTotOI_rep
                : dataApp.aCTotOI_rep;
            borrower.TotalIncome = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower
                ? dataApp.aBTotI_rep
                : dataApp.aCTotI_rep;

            string phone = Regex.Replace(dataApp.aHPhone, @"[() -.]", "");
            borrower.HomePhone = phone;

            borrower.EmailAddress = dataApp.aEmail;
            // borrower.IPAddress = null;

            List<int> scores = new List<int>();
            if (dataApp.aExperianScore > 0)
                scores.Add(dataApp.aExperianScore);
            if (dataApp.aTransUnionScore > 0)
                scores.Add(dataApp.aTransUnionScore);
            if (dataApp.aEquifaxScore > 0)
                scores.Add(dataApp.aEquifaxScore);
            scores.Sort();

            // If there is 1, use that
            // If there are 2, take the lower score
            // If there are 3, take the median
            if (scores.Count == 1 || scores.Count == 2)
                borrower.FICOScore = scores[0].ToString();
            else if (scores.Count == 3)
                borrower.FICOScore = scores[1].ToString();

            borrower.MailingAddress = CreateMailingAddress(dataApp);

            borrower.CreditReportDate = dataApp.aCrOd_rep;
            borrower.BankruptcyIndicator = DriveConvert.ToYN(dataApp.aDecBankrupt);
            borrower.OutstandingJudgementsIndicator = DriveConvert.ToYN(dataApp.aDecJudgment);
            borrower.PropertyForeclosedPastSevenYearsIndicator = DriveConvert.ToYN(dataApp.aDecForeclosure);
            borrower.LoanForeclosureOrJudgementIndicator = DriveConvert.ToYN(dataApp.aDecObligated);
            borrower.BorrowerResidencyBasisType = DriveConvert.ToDrive(dataApp.aAddrT);
            borrower.IntentToOccupyIndicator = DriveConvert.ToYN(dataApp.aDecOcc);
            borrower.HomeownerPastThreeYearsIndicatorIndicator = DriveConvert.ToYN(dataApp.aDecPastOwnership);
            borrower.PropertyUsageTypeDesc = DriveConvert.ToDrive(dataApp.aDecPastOwnedPropT);
            borrower.PropertyTitleTypeDesc = DriveConvert.ToDrive(dataApp.aDecPastOwnedPropTitleT);

            int employerCount = 0; // Include up to 2 employers per borrower (2 most recent probably)
            IEmpCollection recordList = dataApp.aEmpCollection;

            // Take a look at the primary employer record
            IPrimaryEmploymentRecord primaryRecord = recordList.GetPrimaryEmp(false);
            if (primaryRecord != null)
            {
                borrower.EmployerList.Add(CreateEmployer(primaryRecord));
                employerCount++;
            }

            // Get the regular employer records and look through those
            var mainList = recordList.GetSubcollection(true, E_EmpGroupT.Regular);
            if (mainList.Count > 0)
            {
                for (int i = 0; i < mainList.Count; i++)
                {
                    if (2 <= employerCount)
                        break;

                    IRegularEmploymentRecord regRecord = (IRegularEmploymentRecord) mainList.GetRecordAt(i);
                    if (regRecord != null)
                    {
                        borrower.EmployerList.Add(CreateEmployer(regRecord));
                        employerCount++;
                    }
                }
            }

            var REORecordList = dataApp.aReCollection;
            var REOSubCollection = REORecordList.GetSubcollection(true, E_ReoGroupT.All);
            int iREOCount = 0;

            foreach (var item in REOSubCollection)
            {
                var rEOFields = (IRealEstateOwned)item;
                if (BorrowerOwnsREO(rEOFields.ReOwnerT, dataApp.BorrowerModeT))
                {
                    iREOCount++;
                    borrower.REOList.Add(CreateREO(rEOFields, iREOCount));
                }

                // 3/18/2015 BB - DRIVE schema caps the REO list at 30; OPM 206661
                if (iREOCount == 30)
                {
                    break;
                }
            }

            // borrower.Asset = null;
            return borrower;
        }

        private bool BorrowerOwnsREO(E_ReOwnerT rEOOwner, E_BorrowerModeT borrowerMode)
        {
            bool bOwns = false;
            switch(rEOOwner)
            {
                case E_ReOwnerT.Joint:
                    bOwns = true;
                    break;
                case E_ReOwnerT.Borrower:
                    bOwns = (borrowerMode == E_BorrowerModeT.Borrower);
                    break;
                case E_ReOwnerT.CoBorrower:
                    bOwns = (borrowerMode == E_BorrowerModeT.Coborrower);
                    break;
                default:
                    throw new UnhandledEnumException(rEOOwner);
            }
            
            return bOwns;
        }

        // Per Case 454289, "Changing the template/structure of how each of the participant gets sent has the potential to break all participants so we need a way to do a more controlled rollout."
        private Participant CreateParticipant(CAgentFields data, E_ParticipantParticipantType participantType, E_SendParticipantAs sendParticipantAs)
        {
            Participant participant = new Participant();
            participant.ParticipantType = participantType;

            if (sendParticipantAs == E_SendParticipantAs.Company)
            {
                participant.CompanyName = data.CompanyName;
                participant.LicenseOrCertNum = data.LicenseNumOfCompany;
                participant.ADSCompanyID = data.CompanyLoanOriginatorIdentifier;
                participant.Phone = Regex.Replace(data.PhoneOfCompany, @"[() -.]", "");
            }
            else if (sendParticipantAs == E_SendParticipantAs.Person)
            {
                try
                {
                    // OPM 262141, 11/28/2016, ML
                    // Replace ad hoc name parsing with MCL name parser.
                    var parser = new CommonLib.Name();
                    parser.ParseName(data.AgentName);

                    participant.FirstName = parser.FirstName;
                    participant.LastName = parser.LastName;
                }
                catch (NullReferenceException exc)
                {
                    throw new CBaseException($"DRIVE export failed. Could not parse the name {data.AgentName}", exc);
                }

                participant.LicenseOrCertNum = data.LicenseNumOfAgent;
                participant.ADSID = data.LoanOriginatorIdentifier;
                participant.Phone = Regex.Replace(data.Phone, @"[() -.]", "");
                participant.EmailAddress = data.EmailAddr;
            }

            participant.StreetAddress = data.StreetAddr;
            participant.City = data.City;
            participant.State = data.State;
            participant.ZIP = data.Zip;
            return participant;
        }

        private Participant CreateParticipant_Old(CAgentFields data, E_ParticipantParticipantType participantType, E_SendParticipantAs sendParticipantAs)
        {

            Participant participant = new Participant();
            participant.ParticipantType = participantType;

            if (sendParticipantAs == E_SendParticipantAs.Person)
            {
                try
                {
                    // OPM 262141, 11/28/2016, ML
                    // Replace ad hoc name parsing with MCL name parser.
                    var parser = new CommonLib.Name();
                    parser.ParseName(data.AgentName);

                    participant.FirstName = parser.FirstName;
                    participant.LastName = parser.LastName;
                }
                catch (NullReferenceException exc)
                {
                    throw new CBaseException($"DRIVE export failed. Could not parse the name {data.AgentName}", exc);
                }
            }
            // Else don't send any first name / last name data.

            participant.LicenseOrCertNum = data.LicenseNumOfAgent;
            participant.CompanyName = data.CompanyName;
            participant.StreetAddress = data.StreetAddr;
            participant.City = data.City;
            participant.State = data.State;
            participant.ZIP = data.Zip;

            string phone = Regex.Replace(data.PhoneOfCompany, @"[() -.]", "");
            participant.Phone = phone;
            // participant.MailingAddress = null;
            // participant.MailingCity = null;
            // participant.MailingState = null;
            // participant.MailingZip = null;
            // participant.ParticipantUniqueId = null;
            participant.EmailAddress = data.EmailAddr;
            // participant.IPAddress = null;
            participant.ADSID = data.LoanOriginatorIdentifier;
            participant.ADSCompanyID = data.CompanyLoanOriginatorIdentifier;
            // participant.DBANameList.Add(null);
            return participant;
        }


        private CustomerDefinedField CreateCustomerDefinedField(string name, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            CustomerDefinedField customerdefinedfield = new CustomerDefinedField();
            
            customerdefinedfield.FieldName = name;
            customerdefinedfield.FieldValue = value;
            
            return customerdefinedfield;
        }
        private LoanFile CreateLoanFile()
        {
            LoanFile loanfile = new LoanFile();
            // loanfile.LoanFileType = null;
            return loanfile;
        }
        private Loan CreateLoan()
        {
            Loan loan = new Loan();
            loan.LoanNumber = m_dataLoan.sLNm;
            loan.ApplicationDate = m_dataLoan.sOpenedD_rep;
            loan.CloseDate = m_dataLoan.sEstCloseD_rep;
            loan.FundingDate = m_dataLoan.sSchedFundD_rep;
            loan.LoanAmount = m_dataLoan.sFinalLAmt_rep;
            loan.PurchasePrice = m_dataLoan.sPurchPrice_rep;

            decimal sum_aAsstLiqTot = 0;
            decimal sum_aAsstValTot = 0;
            decimal sum_aLiaBalTot = 0;
            decimal sum_aNetWorth = 0;
            decimal sum_aReTotVal = 0;
            foreach (var dataApp in m_dataApps)
            {
                sum_aAsstLiqTot += dataApp.aAsstLiqTot;
                sum_aAsstValTot += dataApp.aAsstValTot;
                sum_aLiaBalTot += dataApp.aLiaBalTot;
                sum_aNetWorth += dataApp.aNetWorth;
                sum_aReTotVal += dataApp.aReTotVal;
            }
            loan.SubtotalLiquidAssets = sum_aAsstLiqTot.ToString("F2"); // aAsstLiqTot : sum all apps
            loan.TotalAssets = sum_aAsstValTot.ToString("F2"); // aAsstValTot : sum all apps
            loan.NetWorth = sum_aNetWorth.ToString("F2");
            loan.TotalLiabilities = sum_aLiaBalTot.ToString("F2");
            loan.REOMktValue = sum_aReTotVal.ToString("F2");

            loan.ClientLoanStatus = this.m_dataLoan.sStatusT_rep;
            loan.SubordinateFinancing = m_dataLoan.sConcurSubFin_rep;
            loan.LTV = m_dataLoan.sLtvR_rep; // make sure this is 0-100 instead of 0-1
            loan.CLTV = m_dataLoan.sCltvR_rep;
            loan.DTI = m_dataLoan.sQualBottomR_rep;
            loan.NoteRate = m_dataLoan.sNoteIR_rep;
            loan.UnpaidPrincipalBalance = m_dataLoan.sSpLien_rep;
            loan.RefiCashoutAmount = m_dataLoan.sTransNetCash < 0 ? m_dataLoan.sTransNetCash_rep : string.Empty; // sTransNetCash : send if negative
            loan.UnderwritingDate = m_dataLoan.sUnderwritingD_rep;
            loan.ApprovedDate = m_dataLoan.sApprovD_rep;
            loan.DocumentsDrawnDate = m_dataLoan.sDocsD_rep;
            loan.DeclinedDate = m_dataLoan.sRejectD_rep;
            loan.WithdrawnDate = m_dataLoan.sCanceledD_rep;
            loan.InitialSubmissionDate = m_dataLoan.sAppSubmittedD_rep;
            loan.DocumentationType = DriveConvert.ToDrive(m_dataLoan.sFannieDocT);
            loan.MortgageType = DriveConvert.ToDrive(m_dataLoan.sLT, m_dataLoan.sLPurposeT, m_dataLoan.sIsLineOfCredit);
            loan.FHALenderIdentifier = m_dataLoan.sFHALenderIdCode;
            loan.FHASponsorIdentifier = m_dataLoan.sFHASponsorAgentIdCode;
            // loan.JointCreditRequested = null;
            // loan.BillingIdentifier = null;
            // loan.OriginatorLoanNumber = null;
            loan.MERSMinIdentifier = m_dataLoan.sMersMin;
            loan.DUCaseFileId = m_dataLoan.sDuCaseId;
            loan.DUType = DriveConvert.ToDrive(m_dataLoan.sProd3rdPartyUwResultT);
            loan.LPKeyNum = m_dataLoan.sLpAusKey;
            loan.LPType = DriveConvert.ToDriveLPType(m_dataLoan.sProd3rdPartyUwResultT);
            loan.AttachmentType = DriveConvert.ToDrive(m_dataLoan.sGseSpT);
            loan.RateLockType = DriveConvert.ToDrive(m_dataLoan.sInvestorLockCommitmentT);
            loan.IndexMarginPercent = m_dataLoan.sRAdjMarginR_rep;
            loan.FNMCommunityLendingProductType = DriveConvert.ToDrive(m_dataLoan.sFannieCommunityLendingT);
            loan.FNMNeighborsMortgageEligibilityIndicator = DriveConvert.ToYN(m_dataLoan.sIsFannieNeighbors);
            loan.SectionOfActType = DriveConvert.ToDriveSectionOfActType(m_dataLoan.sFHAHousingActSection);
            loan.LoanAmortizationTermMonths = m_dataLoan.sTerm_rep;
            loan.BalloonIndicator = DriveConvert.ToYN(m_dataLoan.sGfeIsBalloon);
            //loan.LoanRepaymentType
            loan.AlterationsImprovementsAndRepairsAmount = m_dataLoan.sAltCost_rep;
            loan.LandEstimatedValueAmount = m_dataLoan.sLandCost_rep;
            loan.MIAndFundingFeeFinancedAmount = m_dataLoan.sFfUfmipFinanced_rep;
            loan.RefinanceIncludingDebtsToBePaidOffAmount = m_dataLoan.sRefPdOffAmt1003_rep;
            loan.TotalMonthlyPayments = m_dataLoan.sTransmTotMonPmt_rep;
            // loan.LoanHeader = null;
            loan.PropertyInformation = CreatePropertyInformation();

            int sequenceNumber = 1;
            foreach (var dataApp in m_dataApps)
            {
                // Borrower
                dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                var borrower = CreateBorrower(dataApp, sequenceNumber);
                if (borrower != null && sequenceNumber <= 6) // MAX 6 BORROWERS
                {
                    loan.BorrowerList.Add(borrower);
                    sequenceNumber++;
                }

                // Coborrower
                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                var coborrower = CreateBorrower(dataApp, sequenceNumber);
                if (coborrower != null && sequenceNumber <= 6 && !string.IsNullOrEmpty(dataApp.aCSsn)) // Do not send co-borrowers with blank SSNs
                {
                    loan.BorrowerList.Add(coborrower);
                    sequenceNumber++;
                }

                dataApp.BorrowerModeT = E_BorrowerModeT.Borrower; // reset the borrower mode
            }

            PopulateParticipantList(loan.ParticipantList);

            loan.CustomerDefinedFieldList.Add(this.CreateCustomerDefinedField("InitialCreditPullDate", this.m_dataLoan.sCrOd_rep)); // 11/13/2014 BB - OPM 195632
            // loan.LoanFile = null;
            return loan;
        }
        private BatchRequest CreateBatchRequest()
        {
            BatchRequest batchrequest = new BatchRequest();
            // batchrequest.CustomerBatchIdentifier = "custid";
            batchrequest.LoanList.Add(CreateLoan());
            return batchrequest;
        }
        private DRIVERequest CreateDRIVERequest()
        {
            DRIVERequest driverequest = new DRIVERequest();
            driverequest.Version = "1.00";
            driverequest.Authentication = CreateAuthentication();
            driverequest.BatchRequest = CreateBatchRequest();
            return driverequest;
        }

        public DRIVERequest CreateRequest()
        {
            m_request = CreateDRIVERequest();
            return m_request;
        }

        /// <summary>
        /// Based on the mapping in Participants.xlsx in the spec for OPM 67724.
        /// SIDE EFFECTS:
        /// May increment one of the following:
        ///     - m_participantAppraiserCount
        ///     - m_participantLoanOfficerCount
        ///     - m_participantSellerCount
        ///     - m_participantListingAgentCount
        ///     - m_participantSellingAgentCount
        ///     - m_participantCloserCount
        ///     - m_participantProcessorCount
        ///     - m_participantUnderwriterCount
        ///     - m_participantEscrowCount
        /// Adds the rows in participantData to participantList.
        /// </summary>
        /// <param name="participantList">The list of participants to be populated by LQB agents.</param>
        private void PopulateParticipantList(List<Participant> participantList)
        {
            var agentCollection = m_dataLoan.sAgentCollection;
            int recordCount = agentCollection.GetAgentRecordCount();

            var createParticipant_Either = this.m_dataLoan.BrokerDB.UseUpdatedCompanyAndPersonTemplateForDRIVE
                ? (Func<CAgentFields, E_ParticipantParticipantType, E_SendParticipantAs, Participant>)this.CreateParticipant
                : this.CreateParticipant_Old;

            for (int i = 0; i < recordCount; i++)
            {
                CAgentFields participant = agentCollection.GetAgentFields(i);

                E_ParticipantParticipantType personType;
                E_ParticipantParticipantType companyType;
                E_ParticipantParticipantType secondaryPersonType;
                RetrieveParticipantTypes(participant, out personType, out companyType, out secondaryPersonType);

                if (personType != E_ParticipantParticipantType.None)
                {
                    participantList.Add(createParticipant_Either(participant, personType, E_SendParticipantAs.Person));
                }

                if (companyType != E_ParticipantParticipantType.None)
                {
                    participantList.Add(createParticipant_Either(participant, companyType, E_SendParticipantAs.Company));
                }

                if (secondaryPersonType != E_ParticipantParticipantType.None)
                {
                    participantList.Add(createParticipant_Either(participant, secondaryPersonType, E_SendParticipantAs.Person));
                }
            }
        }

        /// <summary>
        /// Maps an agent to up to three participant types by role.
        /// </summary>
        /// <param name="agent">An agent to map to a set of participant types.</param>
        /// <param name="personType">A participant type to be sent as a Person.</param>
        /// <param name="companyType">A participant type to be sent as a Company.</param>
        /// <param name="secondaryPersonType">An auxiliary Person type. Only used for 
        /// LoanOfficer and Lender roles.</param>
        private void RetrieveParticipantTypes(
            CAgentFields agent, 
            out E_ParticipantParticipantType personType, 
            out E_ParticipantParticipantType companyType,
            out E_ParticipantParticipantType secondaryPersonType)
        {
            var role = agent.AgentRoleT;
            personType = E_ParticipantParticipantType.None;
            companyType = E_ParticipantParticipantType.None;
            secondaryPersonType = E_ParticipantParticipantType.None;

            if (role == E_AgentRoleT.AppraisalManagementCompany)
            {
                companyType = E_ParticipantParticipantType.AppraisalManagementCompany;
            }
            else if (role == E_AgentRoleT.Appraiser)
            {
                if (m_participantAppraiserCount == 0)
                {
                    personType = E_ParticipantParticipantType.Appraiser;
                    companyType = E_ParticipantParticipantType.AppraisalCompany;
                }
                else
                {
                    personType = E_ParticipantParticipantType.Appraiser2;
                    companyType = E_ParticipantParticipantType.AppraisalCompany2;
                }

                m_participantAppraiserCount++;
            }
            else if (role == E_AgentRoleT.Bank)
            {
                personType = E_ParticipantParticipantType.PrivateBanker;
            }
            else if (role == E_AgentRoleT.BrokerRep)
            {
                personType = E_ParticipantParticipantType.ThirdPartyOriginator;
            }
            else if (role == E_AgentRoleT.Builder)
            {
                personType = E_ParticipantParticipantType.Builder;
                companyType = E_ParticipantParticipantType.BuilderCompany;
            }
            else if (role == E_AgentRoleT.BuyerAttorney)
            {
                personType = E_ParticipantParticipantType.Attorney;
            }
            else if (role == E_AgentRoleT.ClosingAgent)
            {
                if (m_participantCloserCount == 0)
                {
                    personType = E_ParticipantParticipantType.Closer;
                    companyType = E_ParticipantParticipantType.ClosingCompany;
                }
                else
                {
                    personType = E_ParticipantParticipantType.Closer2;
                }

                m_participantCloserCount++;
            }
            else if (role == E_AgentRoleT.CreditReport)
            {
                companyType = E_ParticipantParticipantType.CreditBureauCompany;
            }
            else if (role == E_AgentRoleT.DocDrawer)
            {
                personType = E_ParticipantParticipantType.DocDrawer;
            }
            else if (role == E_AgentRoleT.Escrow)
            {
                if (m_participantEscrowCount == 0)
                {
                    personType = E_ParticipantParticipantType.EscrowOfficer;
                    companyType = E_ParticipantParticipantType.EscrowCompany;
                }
                else
                {
                    personType = E_ParticipantParticipantType.EscrowOfficer2;
                }

                m_participantEscrowCount++;
            }
            else if (role == E_AgentRoleT.HomeOwnerInsurance || role == E_AgentRoleT.HazardInsurance)
            {
                personType = E_ParticipantParticipantType.HomeownersInsuranceContact;
                companyType = E_ParticipantParticipantType.HomeownersInsuranceCompany;
            }
            else if (role == E_AgentRoleT.Lender)
            {
                companyType = E_ParticipantParticipantType.Lender;
                personType = E_ParticipantParticipantType.AccountExecutive;
            }
            else if (role == E_AgentRoleT.ListingAgent)
            {
                if (m_participantListingAgentCount == 0)
                {
                    personType = E_ParticipantParticipantType.ListingRealtor;
                    companyType = E_ParticipantParticipantType.ListingRealEstateOffice;
                }
                else if (m_participantListingAgentCount == 1)
                {
                    personType = E_ParticipantParticipantType.ListingRealtor2;
                }
                else
                {
                    personType = E_ParticipantParticipantType.ListingRealtor3;
                }

                m_participantListingAgentCount++;
            }
            else if (role == E_AgentRoleT.LoanOfficer)
            {
                if (m_participantLoanOfficerCount == 0)
                {
                    personType = E_ParticipantParticipantType.LoanOfficer;
                }
                else
                {
                    personType = E_ParticipantParticipantType.LoanOfficer2;
                }

                m_participantLoanOfficerCount++;

                if (m_dataLoan.sGfeIsTPOTransaction)
                {
                    companyType = E_ParticipantParticipantType.Broker;
                }
            }
            else if (role == E_AgentRoleT.LoanOfficerAssistant)
            {
                personType = E_ParticipantParticipantType.LoanOfficerAssistant;
            }
            else if (role == E_AgentRoleT.Processor || role == E_AgentRoleT.JuniorProcessor || role == E_AgentRoleT.BrokerProcessor)
            {
                if (m_participantProcessorCount == 0)
                {
                    personType = E_ParticipantParticipantType.Processor;
                }
                else if (m_participantProcessorCount == 1)
                {
                    personType = E_ParticipantParticipantType.Processor2;
                }
                else
                {
                    personType = E_ParticipantParticipantType.Processor3;
                }

                m_participantProcessorCount++;
            }
            else if (role == E_AgentRoleT.Purchaser)
            {
                personType = E_ParticipantParticipantType.PostServicingPurchaser;
            }
            else if (role == E_AgentRoleT.Seller)
            {
                if (m_participantSellerCount == 0)
                {
                    personType = E_ParticipantParticipantType.Seller;
                    companyType = E_ParticipantParticipantType.SellerCompany;
                }
                else if (m_participantSellerCount == 1)
                {
                    personType = E_ParticipantParticipantType.secondSeller;
                    companyType = E_ParticipantParticipantType.secondSellerCompany;
                }
                else if (m_participantSellerCount == 2)
                {
                    personType = E_ParticipantParticipantType.Seller3;
                }
                else if (m_participantSellerCount == 3)
                {
                    personType = E_ParticipantParticipantType.Seller4;
                }
                else if (m_participantSellerCount == 4)
                {
                    personType = E_ParticipantParticipantType.Seller5;
                }
                else
                {
                    personType = E_ParticipantParticipantType.Seller6;
                }

                m_participantSellerCount++;
            }
            else if (role == E_AgentRoleT.SellerAttorney)
            {
                personType = E_ParticipantParticipantType.SellerAttorney;
                companyType = E_ParticipantParticipantType.SellerAttorneyCompany;
            }
            else if (role == E_AgentRoleT.SellingAgent)
            {
                if (m_participantSellingAgentCount == 0)
                {
                    personType = E_ParticipantParticipantType.SellingRealtor;
                    companyType = E_ParticipantParticipantType.SellingRealEstateOffice;
                }
                else if (m_participantSellingAgentCount == 1)
                {
                    personType = E_ParticipantParticipantType.SellingRealtor2;
                }
                else
                {
                    personType = E_ParticipantParticipantType.SellingRealtor3;
                }

                m_participantSellingAgentCount++;
            }
            else if (role == E_AgentRoleT.Title)
            {
                personType = E_ParticipantParticipantType.TitleOfficer;
                companyType = E_ParticipantParticipantType.TitleCompany;
            }
            else if (role == E_AgentRoleT.TitleUnderwriter)
            {
                personType = E_ParticipantParticipantType.TitleUnderwriter;
                companyType = E_ParticipantParticipantType.TitleUnderwriterCompany;
            }
            else if (role == E_AgentRoleT.Underwriter || role == E_AgentRoleT.JuniorUnderwriter)
            {
                if (m_participantUnderwriterCount == 0)
                {
                    personType = E_ParticipantParticipantType.Underwriter;
                }
                else
                {
                    personType = E_ParticipantParticipantType.Underwriter2;
                }

                m_participantUnderwriterCount++;
            }
            else if (role == E_AgentRoleT.BuyerAgent)
            {
                personType = E_ParticipantParticipantType.BuyerAgent;
            }
            else
            {
                // Don't do anything!
            }
        }

        private E_AgentRoleT GetAgentRole(string agentRoleCode)
        {
            int v;

            if (string.IsNullOrEmpty(agentRoleCode) == false)
            {
                if (int.TryParse(agentRoleCode, out v))
                {
                    return (E_AgentRoleT)v;
                }
            }
            return E_AgentRoleT.Other;
        }
    }

    public class DriveConvert
    {
        public static E_LoanDocumentationType ToDrive(E_sFannieDocT sFannieDocT)
        {
            switch (sFannieDocT)
            {
                case E_sFannieDocT.LeaveBlank:
                    return E_LoanDocumentationType.None;
                case E_sFannieDocT.Alternative:
                    return E_LoanDocumentationType.Alternative;
                case E_sFannieDocT.Full:
                    return E_LoanDocumentationType.FullDocumentation;
                case E_sFannieDocT.NoDocumentation:
                    return E_LoanDocumentationType.NoDocumentation;
                case E_sFannieDocT.Reduced:
                    return E_LoanDocumentationType.Reduced;
                case E_sFannieDocT.StreamlinedRefinanced:
                    return E_LoanDocumentationType.StreamlineRefinance;
                case E_sFannieDocT.NoRatio:
                    return E_LoanDocumentationType.NoRatio;
                case E_sFannieDocT.LimitedDocumentation:
                    return E_LoanDocumentationType.None;
                case E_sFannieDocT.NoIncomeNoEmploymentNoAssets:
                    return E_LoanDocumentationType.NoIncomeNoEmploymentNoAssetsOn1003;
                case E_sFannieDocT.NoIncomeNoAssets:
                    return E_LoanDocumentationType.None;
                case E_sFannieDocT.NoAssets:
                    return E_LoanDocumentationType.NoDepositVerification;
                case E_sFannieDocT.NoIncomeNoEmployment:
                    return E_LoanDocumentationType.NoEmploymentVerificationOrIncomeVerification;
                case E_sFannieDocT.NoIncome:
                    return E_LoanDocumentationType.NoIncomeOn1003;
                case E_sFannieDocT.NoVerificationStatedIncomeEmploymentAssets:
                    return E_LoanDocumentationType.NoVerificationOfStatedIncomeEmploymentOrAssets;
                case E_sFannieDocT.NoVerificationStatedIncomeAssets:
                    return E_LoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;
                case E_sFannieDocT.NoVerificationStatedAssets:
                    return E_LoanDocumentationType.NoVerificationOfStatedAssets;
                case E_sFannieDocT.NoVerificationStatedIncomeEmployment:
                    return E_LoanDocumentationType.NoVerificationOfStatedIncomeOrEmployment;
                case E_sFannieDocT.NoVerificationStatedIncome:
                    return E_LoanDocumentationType.NoVerificationOfStatedIncome;
                case E_sFannieDocT.VerbalVOE:
                    return E_LoanDocumentationType.VerbalVerificationofEmployment;
                case E_sFannieDocT.OnePaystub:
                    return E_LoanDocumentationType.OnePaystub;
                case E_sFannieDocT.OnePaystubAndVerbalVOE:
                    return E_LoanDocumentationType.OnePaystubAndVerbalVerificationOfEmployment;
                case E_sFannieDocT.OnePaystubOneW2VerbalVOE:
                    return E_LoanDocumentationType.OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040;
                default:
                    throw new UnhandledEnumException(sFannieDocT);
            }
        }

        public static E_PropertyInformationLoanPurpose ToDrive(E_sLPurposeT sLPurposeT)
        {
            switch (sLPurposeT)
            {
                case E_sLPurposeT.Purchase:
                    return E_PropertyInformationLoanPurpose.Purchase;
                case E_sLPurposeT.Refin:
                    return E_PropertyInformationLoanPurpose.Refinance;
                case E_sLPurposeT.RefinCashout:
                    return E_PropertyInformationLoanPurpose.Refinance;
                case E_sLPurposeT.Construct:
                    return E_PropertyInformationLoanPurpose.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm:
                    return E_PropertyInformationLoanPurpose.ConstructionToPermanent;
                case E_sLPurposeT.Other:
                    return E_PropertyInformationLoanPurpose.Other;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                    return E_PropertyInformationLoanPurpose.Refinance;
                case E_sLPurposeT.VaIrrrl:
                    return E_PropertyInformationLoanPurpose.Refinance;
                case E_sLPurposeT.HomeEquity:
                    return E_PropertyInformationLoanPurpose.Refinance; // BB 12/11/13 - OPM 145735
                default:
                    throw new UnhandledEnumException(sLPurposeT);
            }
        }

        public static E_PropertyInformationRefinancePurpose ToDrive(string sRefPurpose, E_sLPurposeT sLPurposeT)
        {
            if (sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                return E_PropertyInformationRefinancePurpose.CashOutOther; // BB 12/11/13 - OPM 145735
            }

            switch (sRefPurpose.ToLower())
            {
                case "no cash-out rate/term": return E_PropertyInformationRefinancePurpose.NoCashOut;
                case "cash-out/other": return E_PropertyInformationRefinancePurpose.CashOutOther;
                case "cash-out/home improvement": return E_PropertyInformationRefinancePurpose.CashOutHomeImprovement;
                case "cash-out/debt consolidation": return E_PropertyInformationRefinancePurpose.CashOutDebtConsolidation;
                case "limited cash-out rate/term": return E_PropertyInformationRefinancePurpose.LimitedCashOut;
                default: return E_PropertyInformationRefinancePurpose.None;
            }
        }

        public static E_PropertyInformationPropertyUsageType ToDrive(E_sOccT sOccT)
        {
            switch (sOccT)
            {
                case E_sOccT.PrimaryResidence:
                    return E_PropertyInformationPropertyUsageType.PrimaryResidence;
                case E_sOccT.SecondaryResidence:
                    return E_PropertyInformationPropertyUsageType.SecondHome;
                case E_sOccT.Investment:
                    return E_PropertyInformationPropertyUsageType.Investor;
                default:
                    throw new UnhandledEnumException(sOccT);
            }
        }

        public static E_PropertyInformationPropertyType ToDrive(E_sGseSpT sGseSpT, int sUnitsNum)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.LeaveBlank:
                    return E_PropertyInformationPropertyType.None;
                
                case E_sGseSpT.Attached:
                case E_sGseSpT.Detached:
                case E_sGseSpT.Modular:
                    if (sUnitsNum == 1)
                        return E_PropertyInformationPropertyType.SingleFamily;
                    else if (2 <= sUnitsNum && sUnitsNum <= 4)
                        return E_PropertyInformationPropertyType.TwotoFourUnit;
                    else if (4 < sUnitsNum)
                        return E_PropertyInformationPropertyType.Multifamily4unit;
                    break;
                
                case E_sGseSpT.Condominium:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                case E_sGseSpT.ManufacturedHomeCondominium:
                    return E_PropertyInformationPropertyType.Condominium;
                
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return E_PropertyInformationPropertyType.ManufacturedMobileHome;
                
                case E_sGseSpT.Cooperative:
                    return E_PropertyInformationPropertyType.Cooperative;
                
                case E_sGseSpT.PUD:
                    return E_PropertyInformationPropertyType.PUD;
                
                default:
                    throw new UnhandledEnumException(sGseSpT);
            }
            return E_PropertyInformationPropertyType.None;
        }

        public static E_PropertyInformationLienPriorityType ToDrive(E_sLienPosT sLienPosT)
        {
            switch(sLienPosT)
            {
                case E_sLienPosT.First:
                    return E_PropertyInformationLienPriorityType.FirstMortgage;
                case E_sLienPosT.Second:
                    return E_PropertyInformationLienPriorityType.SecondMortgage;
                default:
                    throw new UnhandledEnumException(sLienPosT);
            }
        }

        public static E_PropertyInformationOccupancyType ToDrive(E_sLPurposeT sLPurposeT, E_sOccT sOccT, decimal sOccR)
        {
            switch(sLPurposeT)
            {
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                case E_sLPurposeT.Other:
                case E_sLPurposeT.Purchase:
                    return E_PropertyInformationOccupancyType.None;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.HomeEquity:
                    switch(sOccT)
                    {
                        case E_sOccT.Investment:
                            return (sOccR > 0) ? E_PropertyInformationOccupancyType.TenantOccupied : E_PropertyInformationOccupancyType.Vacant;
                        case E_sOccT.PrimaryResidence:
                        case E_sOccT.SecondaryResidence:
                            return E_PropertyInformationOccupancyType.OwnerOccupied;
                        default:
                            throw new UnhandledEnumException(sOccT);
                    }
                default:
                    throw new UnhandledEnumException(sLPurposeT);
            }
        }

        public static E_LoanMortgageType ToDrive(E_sLT sLT, E_sLPurposeT sLPurposeT, bool bIsLineOfCredit)
        {
            if (sLPurposeT == E_sLPurposeT.HomeEquity)
            {
                return (bIsLineOfCredit) ? E_LoanMortgageType.HELOC : E_LoanMortgageType.Other; // BB 12/11/13 - OPM 145735
            }

            switch (sLT)
            {
                case E_sLT.Conventional:
                    return E_LoanMortgageType.Conventional;
                case E_sLT.FHA:
                    return E_LoanMortgageType.FHA;
                case E_sLT.VA:
                    return E_LoanMortgageType.VA;
                case E_sLT.UsdaRural:
                    return E_LoanMortgageType.FarmersHomeAdministration;
                case E_sLT.Other:
                    return E_LoanMortgageType.Other;
                default:
                    throw new UnhandledEnumException(sLT);
            }
        }

        public static E_LoanDUType ToDrive(E_sProd3rdPartyUwResultT sProd3rdPartyUwResultT)
        {
            switch(sProd3rdPartyUwResultT)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                    return E_LoanDUType.ApproveEligible;
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                    return E_LoanDUType.ApproveIneligible;
                case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                    return E_LoanDUType.ExpandedApproval1Eligible;
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                    return E_LoanDUType.ExpandedApproval2Eligible;
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                    return E_LoanDUType.ExpandedApproval3Eligible;
                case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                    return E_LoanDUType.ReferEligible;
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                    return E_LoanDUType.ReferIneligible;
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                    return E_LoanDUType.ReferWithCaution;
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                case E_sProd3rdPartyUwResultT.NA:
                    return E_LoanDUType.None;
                case E_sProd3rdPartyUwResultT.OutOfScope:
                    return E_LoanDUType.OutOfScope;
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    return E_LoanDUType.None;
                default:
                    throw new UnhandledEnumException(sProd3rdPartyUwResultT);
            }
        }

        public static E_LoanLPType ToDriveLPType(E_sProd3rdPartyUwResultT sProd3rdPartyUwResultT)
        {
            switch (sProd3rdPartyUwResultT)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                    return E_LoanLPType.None;
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                    return E_LoanLPType.Approved;
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                    return E_LoanLPType.None;
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                    return E_LoanLPType.Refer;
                case E_sProd3rdPartyUwResultT.NA:
                case E_sProd3rdPartyUwResultT.OutOfScope:
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    return E_LoanLPType.None;
                default:
                    throw new UnhandledEnumException(sProd3rdPartyUwResultT);
            }
        }

        public static E_LoanAttachmentType ToDrive(E_sGseSpT sGseSpT)
        {
            switch(sGseSpT)
            {
                case E_sGseSpT.Attached:
                case E_sGseSpT.Condominium:
                    return E_LoanAttachmentType.Attached;
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.Detached:
                case E_sGseSpT.DetachedCondominium:
                    return E_LoanAttachmentType.Detached;
                case E_sGseSpT.HighRiseCondominium:
                    return E_LoanAttachmentType.Attached;
                case E_sGseSpT.LeaveBlank:
                    return E_LoanAttachmentType.None;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return E_LoanAttachmentType.Detached;
                default:
                    throw new UnhandledEnumException(sGseSpT);
            }
        }

        public static E_LoanRateLockType ToDrive(E_sInvestorLockCommitmentT sInvestorLockCommitmentT)
        {
            switch(sInvestorLockCommitmentT)
            {
                case E_sInvestorLockCommitmentT.BestEfforts:
                    return E_LoanRateLockType.BestEfforts;
                case E_sInvestorLockCommitmentT.Mandatory:
                    return E_LoanRateLockType.Mandatory;
                case E_sInvestorLockCommitmentT.Hedged:
                    return E_LoanRateLockType.None;
                case E_sInvestorLockCommitmentT.Securitized:
                    return E_LoanRateLockType.None;
                default:
                    throw new UnhandledEnumException(sInvestorLockCommitmentT);
            }
        }

        public static E_LoanFNMCommunityLendingProductType ToDrive(E_sFannieCommunityLendingT sFannieCommunityLendingT)
        {
            switch(sFannieCommunityLendingT)
            {
                case E_sFannieCommunityLendingT.CommunityHomeBuyer:
                    return E_LoanFNMCommunityLendingProductType.CommunityHomeBuyersProgram;
                case E_sFannieCommunityLendingT.Fannie32:
                    return E_LoanFNMCommunityLendingProductType.Fannie32;
                case E_sFannieCommunityLendingT.Fannie97:
                    return E_LoanFNMCommunityLendingProductType.Fannie97;
                case E_sFannieCommunityLendingT.HFAPreferred:
                    return E_LoanFNMCommunityLendingProductType.HFAPreferred;
                case E_sFannieCommunityLendingT.HFAPreferredRiskSharing:
                    return E_LoanFNMCommunityLendingProductType.HFAPreferredRiskSharing;
                case E_sFannieCommunityLendingT.LeaveBlank:
                    return E_LoanFNMCommunityLendingProductType.None;
                case E_sFannieCommunityLendingT.MyCommunityMortgage:
                    return E_LoanFNMCommunityLendingProductType.MyCommunityMortgage;
                case E_sFannieCommunityLendingT.HomeReady:
                    return E_LoanFNMCommunityLendingProductType.Other;
                default:
                    throw new UnhandledEnumException(sFannieCommunityLendingT);
            }
        }

        public static E_LoanSectionOfActType ToDriveSectionOfActType(string sFHAHousingActSection)
        {
            switch(sFHAHousingActSection.ToLower().TrimWhitespaceAndBOM())
            {
                case "184":
                    return E_LoanSectionOfActType.S184;
                case "203(b)":
                case "203b":
                case "203.b":
                    return E_LoanSectionOfActType.S203B;
                case "203(b)2":
                case "203b2":
                    return E_LoanSectionOfActType.S203B2;
                case "203(b)/251":
                case "203b/251":
                case "203b251":
                    return E_LoanSectionOfActType.S203B251;
                case "203h":
                case "203(h)":
                    return E_LoanSectionOfActType.S203H;
                case "203(k)":
                case "203k":
                    return E_LoanSectionOfActType.S203K;
                case "203(k)/251":
                case "203k/251":
                case "203k251":
                    return E_LoanSectionOfActType.S203K251;
                case "221d2":
                    return E_LoanSectionOfActType.S221D2;
                case "221d2251":
                    return E_LoanSectionOfActType.S221D2251;
                case "234(c)":
                case "234c":
                    return E_LoanSectionOfActType.S234C;
                case "234(c)/251":
                case "234c/251":
                case "234c251":
                    return E_LoanSectionOfActType.S234C251;
                case "248":
                    return E_LoanSectionOfActType.S248;
                case "251":
                    return E_LoanSectionOfActType.S251;
                case "255":
                    return E_LoanSectionOfActType.S255;
                case "257":
                    return E_LoanSectionOfActType.S257;
                case "502":
                    return E_LoanSectionOfActType.S502;
                default: 
                    return E_LoanSectionOfActType.None;
            }
        }

        public static E_BorrowerBorrowerResidencyBasisType ToDrive(E_aBAddrT aAddrT)
        {
            switch (aAddrT)
            {
                case E_aBAddrT.LivingRentFree:
                    return E_BorrowerBorrowerResidencyBasisType.LivingRentFree;
                case E_aBAddrT.Own:
                    return E_BorrowerBorrowerResidencyBasisType.Own;
                case E_aBAddrT.Rent:
                    return E_BorrowerBorrowerResidencyBasisType.Rent;
                case E_aBAddrT.LeaveBlank:
                    return E_BorrowerBorrowerResidencyBasisType.Unknown;
                default:
                    throw new UnhandledEnumException(aAddrT);
            }
        }

        public static E_BorrowerUSCitizenship ToDriveUSCitizenship(string decCitizen, string decResidency)
        {
            if (decCitizen == "Y")
                return E_BorrowerUSCitizenship.USCitizen;
            else if (decCitizen == "N" && decResidency == "Y")
                return E_BorrowerUSCitizenship.PermanentResidentAlien;
            else if (decCitizen == "N" && decResidency == "N")
                return E_BorrowerUSCitizenship.NonPermanentResidentAlien;
            else
                return E_BorrowerUSCitizenship.Unknown;

        }

        public static E_BorrowerPropertyUsageTypeDesc ToDrive(E_aBDecPastOwnedPropT aDecPastOwnedPropT)
        {
            switch(aDecPastOwnedPropT)
            {
                case E_aBDecPastOwnedPropT.Empty:
                    return E_BorrowerPropertyUsageTypeDesc.None;
                case E_aBDecPastOwnedPropT.IP:
                    return E_BorrowerPropertyUsageTypeDesc.Investor;
                case E_aBDecPastOwnedPropT.PR:
                    return E_BorrowerPropertyUsageTypeDesc.PrimaryResidence;
                case E_aBDecPastOwnedPropT.SH:
                    return E_BorrowerPropertyUsageTypeDesc.SecondHome;
                default:
                    throw new UnhandledEnumException(aDecPastOwnedPropT);
            }
        }

        public static E_BorrowerPropertyTitleTypeDesc ToDrive(E_aBDecPastOwnedPropTitleT aDecPastOwnedPropTitleT)
        {
            switch (aDecPastOwnedPropTitleT)
            {
                case E_aBDecPastOwnedPropTitleT.Empty:
                    return E_BorrowerPropertyTitleTypeDesc.None;
                case E_aBDecPastOwnedPropTitleT.O:
                    return E_BorrowerPropertyTitleTypeDesc.JointlyWithAnotherPerson;
                case E_aBDecPastOwnedPropTitleT.S:
                    return E_BorrowerPropertyTitleTypeDesc.SolelyByYourself;
                case E_aBDecPastOwnedPropTitleT.SP:
                    return E_BorrowerPropertyTitleTypeDesc.JointlyWithYourSpouse;
                default:
                    throw new UnhandledEnumException(aDecPastOwnedPropTitleT);
            }
        }

        public static E_REODispositionStatusType ToDrive(E_ReoStatusT rEOStatusT)
        {
            switch(rEOStatusT)
            {
                case E_ReoStatusT.PendingSale: return E_REODispositionStatusType.PendingSale;
                case E_ReoStatusT.Rental: return E_REODispositionStatusType.RetainForRental;
                case E_ReoStatusT.Residence: return E_REODispositionStatusType.RetainForPrimaryOrSecondaryResidence;
                case E_ReoStatusT.Sale: return E_REODispositionStatusType.Sold;
                default:
                    throw new UnhandledEnumException(rEOStatusT);
            }
        }

        public static E_GSEPropertyType ToDrive(E_ReoTypeT rEOPropertyTypeT)
        {
            switch(rEOPropertyTypeT)
            {
                case E_ReoTypeT._2_4Plx: return E_GSEPropertyType.TwoToFourUnitProperty;
                case E_ReoTypeT.ComNR: return E_GSEPropertyType.CommercialNonResidential;
                case E_ReoTypeT.ComR: return E_GSEPropertyType.HomeAndBusinessCombined;
                case E_ReoTypeT.Condo: return E_GSEPropertyType.Condominium;
                case E_ReoTypeT.Coop: return E_GSEPropertyType.Cooperative;
                case E_ReoTypeT.Farm: return E_GSEPropertyType.Farm;
                case E_ReoTypeT.Land: return E_GSEPropertyType.Land;
                case E_ReoTypeT.LeaveBlank: return E_GSEPropertyType.None;
                case E_ReoTypeT.Mixed: return E_GSEPropertyType.MixedUseResidential;
                case E_ReoTypeT.Mobil: return E_GSEPropertyType.ManufacturedMobileHome;
                case E_ReoTypeT.Multi: return E_GSEPropertyType.MultifamilyMoreThanFourUnits;
                case E_ReoTypeT.Other: return E_GSEPropertyType.None;
                case E_ReoTypeT.SFR: return E_GSEPropertyType.SingleFamily;
                case E_ReoTypeT.Town: return E_GSEPropertyType.Townhouse;
                default:
                    throw new UnhandledEnumException(rEOPropertyTypeT);
            }
        }

        public static E_YNIndicator ToYN(bool val)
        {
            return val ? E_YNIndicator.Y : E_YNIndicator.N;
        }

        /// <summary>
        /// To be used with strings "Y" and "N"
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static E_YNIndicator ToYN(string val)
        {
            return val == "Y" ? E_YNIndicator.Y : E_YNIndicator.N;
        }
    }
}
