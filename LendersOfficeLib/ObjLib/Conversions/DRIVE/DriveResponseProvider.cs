﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Conversions.Drive.Response;
using System.Xml;
using System.Xml.Serialization;

namespace LendersOffice.Conversions.Drive
{
    /// <summary>
    /// Creates and populates a DRIVE response.
    /// </summary>
    public class DriveResponseProvider
    {
        private DRIVEResponse m_response;

        public DRIVEResponse CreateResponse(XmlReader reader)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(DRIVEResponse));
            
            m_response = (DRIVEResponse) deserializer.Deserialize(reader);

            return m_response;
        }

    }
}
