﻿<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:asp="remove">

  <xsl:param name="ToPdf" />
  <xsl:output method="xml" indent="yes" encoding="utf-8" omit-xml-declaration="yes" />

  <xsl:template match="DRIVEResponse">
    <style>
      .Severity_H
      {
      color:#FF3933;
      }
      .Severity_M
      {
      color:#DBD764;
      }
      .Severity_L
      {
      color:#65A752;
      }
      tr
      {
      page-break-inside: avoid;
      }
    </style>
    <xsl:variable name="ResponseDateTime">
      <xsl:value-of select="@ResponseDateTime"/>
    </xsl:variable>
    <xsl:variable name="TimeZone">
      <xsl:value-of select="@TimeZone"/>
    </xsl:variable>
    <div align="left">
      <xsl:for-each select="Loan">
        <table width="650">
          <tr>
            <td>
              <table style="BORDER-COLLAPSE: collapse;" width="620" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">DRIVE Score</td>
                  <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">DRIVE Status</td>
                  <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Response Date/Time</td>
                </tr>
                <tr>
                  <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                    <xsl:choose>
                      <xsl:when test="@DRIVEAdjustedScore">
                        <xsl:value-of select="@DRIVEAdjustedScore"/>
                        <br />(Unadj. Score: <xsl:value-of select="@DRIVEScore" />)
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="@DRIVEScore"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                  <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                    <xsl:choose>
                      <xsl:when test="@DRIVEAdjustedStatus">
                        <xsl:value-of select="@DRIVEAdjustedStatus"/>
                        <br />(Unadj. Status: <xsl:value-of select="@DRIVEStatus" />)
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="@DRIVEStatus"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                  <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                    <xsl:value-of select="$ResponseDateTime"/>&#160;
                    <xsl:value-of select="$TimeZone"/>
                  </td>
                </tr>
              </table>
              <br />
              <table style="BORDER-COLLAPSE: collapse;" width="620" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;" colspan="3">DRIVE Component Scores</td>
                </tr>
                <tr>
                  <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">IDVerify Score</td>
                  <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">AppVerify Score</td>
                  <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Property Verify Score</td>
                </tr>
                <tr>
                  <xsl:for-each select="Scores/ComponentScore">
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:choose>
                        <xsl:when test="@AdjustedScore">
                          <xsl:value-of select="@AdjustedScore"/>
                          <br />(Unadj. Score: <xsl:value-of select="@Score" />)
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="@Score" />
                        </xsl:otherwise>
                      </xsl:choose>
                    </td>
                  </xsl:for-each>
                </tr>
                <tr>
                  <xsl:for-each select="Scores/ComponentScore">
                      <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                        <xsl:choose>
                          <xsl:when test="@AdjustedStatus">
                            <xsl:value-of select="@AdjustedStatus"/>
                            <br />(Unadj. Status: <xsl:value-of select="@Status" />)
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="@Status" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </td>
                  </xsl:for-each>
                </tr>
              </table>
              <br />
              <table style="BORDER-COLLAPSE: collapse;" width="620" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td colspan="5" style="color:white; background-color:#212191; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Valuations</td>
                </tr>
                <tr>
                  <td style="background-color:#f0f0f0; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Model</td>
                  <td style="background-color:#f0f0f0; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Value</td>
                  <td style="background-color:#f0f0f0; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Low Value</td>
                  <td style="background-color:#f0f0f0; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">High Value</td>
                  <td style="background-color:#f0f0f0; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Confidence</td>
                </tr>
                <xsl:for-each select="Valuations/ValuationResult">
                  <tr>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@ValuationModel"/>
                    </td>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="format-number(@Valuation, '$###,###.00')"/>
                    </td>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="format-number(@ValuationLow, '$###,###.00')"/>
                    </td>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="format-number(@ValuationHigh, '$###,###.00')"/>
                    </td>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@ConfidenceScore"/>
                    </td>

                  </tr>
                </xsl:for-each>
              </table>
              <br/>
              <table style="BORDER-COLLAPSE: collapse;" width="740" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;" colspan="4">Completed Order Requests</td>
                </tr>
                <tr>
                  <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Product Type</td>
                  <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">SSN</td>
                  <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">DVPin</td>
                  <td style="font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Order ID</td>
                </tr>
                <xsl:for-each select="DVPinCodes/DVPinCode">
                  <tr>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@ProductType"/>
                    </td>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@SSN"/>
                    </td>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@DVPin"/>
                    </td>
                    <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                      <xsl:value-of select="@OrderID"/>
                    </td>
                  </tr>
                </xsl:for-each>
              </table>
              <br />
              <xsl:variable name="ResultsURL">
                <xsl:value-of select="@ResultsURL"/>
              </xsl:variable>

              <xsl:if test="$ToPdf != 'True'">
                <span style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                  <a href='#' onclick="window.open('{$ResultsURL}')">CLICK HERE TO VIEW LOAN</a>
                </span>
                <br />
                <br />

                <xsl:variable name="PrintURL">
                  <xsl:value-of select="@PrintURL"/>
                </xsl:variable>

                <span style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">
                  <a href='#' onclick="window.open('{$PrintURL}');">CLICK HERE TO PRINT LOAN</a>
                </span>  
              </xsl:if>

              <br />
              <br />
              <table style="BORDER-COLLAPSE: collapse;" width="1020" border="1" cellspacing="0" cellpadding="2">
                <tr>
                  <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px; color:red; font-weight:bold; font-style:italic">Conditions</td>
                  <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px; font-weight:bold" align="right">
                    Loan Number&#x20;<xsl:value-of select="@LoanNumber"/>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <table style="BORDER-COLLAPSE: collapse;" width="100%" border="1" cellspacing="0" cellpadding="2">
                      <tr>
                        <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Code</td>
                        <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Subject</td>
                        <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Description</td>
                        <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Action(s) to Resolve</td>
                        <td style="background-color:#5778ab; color:white; font-weight:bold; font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;">Scoring Analysis</td>
                      </tr>
                      <xsl:for-each select="Conditions/Condition">
                        <tr style="background-color:white;">
                          <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;" valign="top">
                            <xsl:value-of select="@Code" />
                            <xsl:variable name="Severity">
                              <xsl:value-of select="@Severity"/>
                            </xsl:variable>
                            <span class="Severity_{$Severity}">
                              (<xsl:value-of select="@Severity"/>)
                            </span>
                          </td>
                          <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;" valign="top">
                            <xsl:value-of select="@Subject" />
                          </td>
                          <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;" valign="top">
                            <xsl:value-of select="@Description" />
                          </td>
                          <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;" valign="top">
                            <xsl:value-of select="@ActionStep"/>
                          </td>
                          <td style="font-family: verdana,arial,helvetica,sans-serif;font-size: 11px;" valign="top">
                            <xsl:value-of select="@ScoringAnalysis"/>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </table>
                  </td>
                </tr>
              </table>
              <br/>
              <br />
            </td>
          </tr>
        </table>
        <br />
        <hr width="100%" style="color:black" />
        <br />
      </xsl:for-each>
    </div>
  </xsl:template>
</xsl:stylesheet>