﻿namespace LendersOffice.Conversions.Drive
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using DataAccess;
    using EDocs;
    using ExpertPdf.HtmlToPdf;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Conversions.Drive.Response;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Reminders;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using LendersOffice.Constants;
    using PdfRasterizerLib;

    public static class DriveImporter
    {
        /// <summary>
        /// Import a list of conditions into the sLId owned by a brokerId
        /// </summary>
        /// <param name="conditions"></param>
        /// <param name="sLId"></param>
        public static void ImportConditions(IEnumerable<Condition> conditions, Guid sLId, Guid brokerId)
        {
            AbstractUserPrincipal currentUser = PrincipalFactory.CurrentPrincipal;
            BrokerDB currentBroker = BrokerDB.RetrieveById(brokerId);
            if(false == currentBroker.IsEnableDriveConditionImporting)
            {
                return;
            }
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DriveImporter));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck); // sFileVersion

            
            if (!currentBroker.IsUseNewTaskSystem) // if old task system
            {
                ImportConditionsOldTaskSystem(conditions, currentUser, currentBroker, dataLoan);
            }
            else
            {
                ImportConditionsNewTaskSystem(conditions, currentUser, currentBroker, dataLoan);
            }

            dataLoan.Save();
                
        }

        private static void ImportConditionsOldTaskSystem(
            IEnumerable<Condition> conditions,
            AbstractUserPrincipal currentUser,
            BrokerDB currentBroker,
            CPageData dataLoan)
        {
            CConditionSetObsolete currentLoanConditions = new CConditionSetObsolete(dataLoan.sLId);

            var conditionsToImport = new List<CLoanConditionObsolete>();
            foreach (var condition in conditions)
            {
                string code = condition.Code;
                string subject = condition.Subject;
                string description = condition.Description;
                string actionStep = condition.ActionStep;
                string scoringAnalysis = condition.ScoringAnalysis;

                string desc = string.Format(
                    "{0}: {1}\n" +
                    "{2}\n" +
                    "Action(s) to Resolve: {3}\n" +
                    "Scoring Analysis: {4}",
                    code, subject,
                    description,
                    actionStep,
                    scoringAnalysis);

                // Dedupe based on the description
                if (currentLoanConditions.ToList().Exists((cond) => cond.CondDesc == desc))
                {
                    continue;
                }

                // Create the condition and add it to the import list
                try
                {
                    CLoanConditionObsolete cdItem = new CLoanConditionObsolete(currentBroker.BrokerID);
                    cdItem.CondCategoryDesc = "DataVerify DRIVE";
                    cdItem.CondDesc = desc;
                    cdItem.LoanId = dataLoan.sLId;
                    
                    conditionsToImport.Add(cdItem);
                }
                catch (CBaseException e)
                {
                    Tools.LogError("DataVerify DRIVE Condition import failed. ", e);
                }
            }

            if (conditionsToImport.Count > 0)
            {
                // Import conditions, rollback all currently importing conditions if an error is encountered
                using (CStoredProcedureExec exec = new CStoredProcedureExec(dataLoan.sBrokerId))
                {
                    try
                    {
                        exec.BeginTransactionForWrite();
                        conditionsToImport.ForEach(p => p.Save(exec));
                        exec.CommitTransaction();
                    }

                    catch (Exception e)
                    {
                        Tools.LogError("DataVerify DRIVE Condition import failed. ", e);
                        exec.RollbackTransaction();
                        throw new CBaseException(ErrorMessages.Generic, "Could not save conditions.");
                    }
                }
            }
        }

        private static void ImportConditionsNewTaskSystem(
            IEnumerable<Condition> conditions,
            AbstractUserPrincipal currentUser,
            BrokerDB currentBroker,
            CPageData dataLoan)
        {
            List<Task> currentLoanTasks = Task.GetTasksInLoan(dataLoan.sBrokerId, dataLoan.sLId);

            // Find the existing underwriter, if it exists
            Guid underwriterEmployeeId = dataLoan.sEmployeeUnderwriterId;
            EmployeeDB db = new EmployeeDB(underwriterEmployeeId, dataLoan.sBrokerId);
            db.Retrieve();
            Guid underwriterUserId = db.UserID;

            foreach (var condition in conditions)
            {
                string code = condition.Code;
                string subject = condition.Subject;
                string description = condition.Description;
                string actionStep = condition.ActionStep;
                string scoringAnalysis = condition.ScoringAnalysis;

                string taskSubject = string.Format(
                        "DataVerify DRIVE: {0}\r\n" +
                        "Action(s) to Resolve: {1}",
                        description,
                        actionStep);

                // We are de-duping by task subject.
                if (currentLoanTasks.Exists((t) => t.TaskSubject == taskSubject))
                {
                    continue;
                }

                try
                {
                    Task t = Task.Create(dataLoan.sLId, dataLoan.sBrokerId, dataLoan.sPrimaryNm, dataLoan.sLNm);
                    t.TaskSubject = taskSubject;
                    t.TaskIsCondition = true;
                    t.TaskCreatedByUserId = currentUser.UserId;
                    
                    // First, check if there's an existing underwriter in the loan
                    if (underwriterUserId != Guid.Empty)
                    {
                        t.TaskAssignedUserId = underwriterUserId;
                        t.TaskOwnerUserId = underwriterUserId;
                    }
                    else // if not, assign to the underwriter role ID
                    {
                        t.TaskToBeAssignedRoleId = CEmployeeFields.s_UnderwriterRoleId;
                        t.TaskToBeOwnerRoleId = CEmployeeFields.s_UnderwriterRoleId;
                    }
                    
                    t.TaskStatus = E_TaskStatus.Active;
                    
                    string taskDueDateCalcField = currentBroker.DriveDueDateCalculatedFromField; // Item 4 in the integration setup area in LOAdmin
                    int taskDueDateCalcDays = currentBroker.DriveDueDateCalculationOffset;
                    if (taskDueDateCalcField.ToLower() == "sdatetriggered") // sDateTriggered: The date that the trigger was, uh, triggered
                    {
                        t.TaskDueDateLocked = true;
                        t.TaskDueDate = dataLoan.CalculateOffset(DateTime.Now, taskDueDateCalcDays);
                    }
                    else
                    {
                        t.TaskDueDateLocked = false;
                        t.TaskDueDateCalcField = taskDueDateCalcField;
                        t.TaskDueDateCalcDays = taskDueDateCalcDays;   
                    }

                    if (currentBroker.DriveConditionCategoryID.HasValue)
                        t.CondCategoryId = currentBroker.DriveConditionCategoryID.Value; // item 2 in the setup area
                    else
                        throw new CBaseException("Cannot import DRIVE condition: no default DRIVE condition category ID specified.",
                                                 "Cannot import DRIVE condition: no default DRIVE condition category ID specified.");

                    if (currentBroker.DriveTaskPermissionLevelID.HasValue)
                        t.TaskPermissionLevelId = currentBroker.DriveTaskPermissionLevelID.Value; // item 5 in the setup area
                    else
                        throw new CBaseException("Cannot import DRIVE condition: no default DRIVE condition permission level ID specified.",
                                                 "Cannot import DRIVE condition: no default DRIVE condition permission level ID specified.");

                    t.Comments = string.Format(
                        "{0}\n" +
                        "Code {1}: {2}\n" +
                        "Scoring Analysis: {3}",
                        taskSubject,
                        code, subject,
                        scoringAnalysis);

                    t.EnforcePermissions = false;
                    t.Save(false);
                }
                catch (CBaseException e)
                {
                    Tools.LogError("DataVerify DRIVE Condition import failed. ", e);
                }
            }
            TaskUtilities.EnqueueTasksDueDateUpdate(currentBroker.BrokerID, dataLoan.sLId);
        }

        public static void UploadReportToEDocs(string encodedEDoc, int docTypeId, Guid aAppId, Guid sLId)
        {
            // Only upload the document if the XML response contained the encoded PDF report
            if (!String.IsNullOrEmpty(encodedEDoc))
            {
                string pdfFile = TempFileUtils.NewTempFilePath();
                BinaryFileHelper.WriteAllBytes(pdfFile, Convert.FromBase64String(encodedEDoc));


                var repo = EDocumentRepository.GetUserRepository();
                var doc = repo.CreateDocument(E_EDocumentSource.CBCDrive);
                doc.InternalDescription = "DRIVE Report " + DateTime.Today.ToShortDateString();

                doc.DocumentTypeId = docTypeId;
                doc.LoanId = sLId;
                doc.AppId = aAppId;
                doc.IsUploadedByPmlUser = PrincipalFactory.CurrentPrincipal.Type == "P";
                doc.PublicDescription = "DRIVE Report";
                doc.UpdatePDFContentOnSave(pdfFile);
                doc.EDocOrigin = doc.IsUploadedByPmlUser ? E_EDocOrigin.PML : E_EDocOrigin.LO;
                repo.Save(doc);
            }
        }
    }
}
