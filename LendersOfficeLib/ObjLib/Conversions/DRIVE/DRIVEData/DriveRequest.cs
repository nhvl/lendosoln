﻿// WARNING: Please see comments before replacing this file with a new generated version

using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using LendersOffice.Common;


namespace LendersOffice.Conversions.Drive.Request
{
    public enum E_YNIndicator
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Y")]
        Y,
        [XmlEnum("N")]
        N,
    }
    public enum E_LoanAttachmentType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Attached")]
        Attached,
        [XmlEnum("Detached")]
        Detached,
        [XmlEnum("SemiDetached")]
        SemiDetached,
    }
    public enum E_LoanDocumentationType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Alternative")]
        Alternative,
        [XmlEnum("Full Documentation")]
        FullDocumentation,
        [XmlEnum("No Deposit Verification")]
        NoDepositVerification,
        [XmlEnum("No Deposit Verification Employment Verification Or Income Verification")]
        NoDepositVerificationEmploymentVerificationOrIncomeVerification,
        [XmlEnum("No Documentation")]
        NoDocumentation,
        [XmlEnum("No Employment Verification Or Income Verification")]
        NoEmploymentVerificationOrIncomeVerification,
        [XmlEnum("No Income No Employment No Assets On 1003")]
        NoIncomeNoEmploymentNoAssetsOn1003,
        [XmlEnum("No Ratio")]
        NoRatio,
        [XmlEnum("No Verification Of Stated Assets")]
        NoVerificationOfStatedAssets,
        [XmlEnum("No Income On 1003")]
        NoIncomeOn1003,
        [XmlEnum("No Verification Of Stated Income")]
        NoVerificationOfStatedIncome,
        [XmlEnum("No Verification Of Stated Income Employment Or Assets")]
        NoVerificationOfStatedIncomeEmploymentOrAssets,
        [XmlEnum("No Verification Of Stated Income Or Assets")]
        NoVerificationOfStatedIncomeOrAssets,
        [XmlEnum("No Verification Of Stated Income Or Employment")]
        NoVerificationOfStatedIncomeOrEmployment,
        [XmlEnum("One Paystub")]
        OnePaystub,
        [XmlEnum("One Paystub And One W2 And Verbal Verification Of Employment Or One Year 1040")]
        OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040,
        [XmlEnum("One Paystub And Verbal Verification Of Employment")]
        OnePaystubAndVerbalVerificationOfEmployment,
        [XmlEnum("Reduced")]
        Reduced,
        [XmlEnum("Streamline Refinance")]
        StreamlineRefinance,
        [XmlEnum("Verbal Verification of Employment")]
        VerbalVerificationofEmployment,
    }
    public enum E_LoanDUType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Approve Eligible")]
        ApproveEligible,
        [XmlEnum("Approve Ineligible")]
        ApproveIneligible,
        [XmlEnum("Expanded Approval 1 Eligible")]
        ExpandedApproval1Eligible,
        [XmlEnum("Expanded Approval 1 Ineligible")]
        ExpandedApproval1Ineligible,
        [XmlEnum("Expanded Approval 2 Eligible")]
        ExpandedApproval2Eligible,
        [XmlEnum("Expanded Approval 2 Ineligible")]
        ExpandedApproval2Ineligible,
        [XmlEnum("Expanded Approval 3 Eligible")]
        ExpandedApproval3Eligible,
        [XmlEnum("Expanded Approval 3 Ineligible")]
        ExpandedApproval3Ineligible,
        [XmlEnum("Out Of Scope")]
        OutOfScope,
        [XmlEnum("Refer Eligible")]
        ReferEligible,
        [XmlEnum("Refer Ineligible")]
        ReferIneligible,
        [XmlEnum("Refer With Caution 4")]
        ReferWithCaution,
        [XmlEnum("Error")]
        Error,
    }
    public enum E_LoanFNMCommunityLendingProductType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("HomeReady")]
        HomeReady,
        [XmlEnum("My Community Mortgage")]
        MyCommunityMortgage,
        [XmlEnum("HFA Preferred Risk Sharing")]
        HFAPreferredRiskSharing,
        [XmlEnum("HFA Preferred")]
        HFAPreferred,
        [XmlEnum("Community Home Buyers Program")]
        CommunityHomeBuyersProgram,
        [XmlEnum("Fannie 32")]
        Fannie32,
        [XmlEnum("Fannie 97")]
        Fannie97,
        [XmlEnum("Other")]
        Other,
    }
    public enum E_LoanLPType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Approved")]
        Approved,
        [XmlEnum("Refer")]
        Refer,
        [XmlEnum("Incomplete")]
        Incomplete,
        [XmlEnum("Error")]
        Error,
    }
    public enum E_LoanMortgageType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Conventional")]
        Conventional,
        [XmlEnum("Farmers Home Administration")]
        FarmersHomeAdministration,
        [XmlEnum("FHA")]
        FHA,
        [XmlEnum("Other")]
        Other,
        [XmlEnum("VA")]
        VA,
        [XmlEnum("HELOC")]
        HELOC,
    }
    public enum E_LoanRepaymentType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Fully Amortizing")]
        FullyAmortizing,
        [XmlEnum("Scheduled Amortization")]
        ScheduledAmortization,
        [XmlEnum("Interest Only")]
        InterestOnly,
        [XmlEnum("Possible Negative Amortization")]
        PossibleNegativeAmortization,
        [XmlEnum("Scheduled Negative Amortization")]
        ScheduledNegativeAmortization,
        [XmlEnum("Principal Payment Option")]
        PrincipalPaymentOption,
        [XmlEnum("Constant Principal")]
        ConstantPrincipal,
        [XmlEnum("Other")]
        Other,
    }
    public enum E_LoanRateLockType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Best Efforts")]
        BestEfforts,
        [XmlEnum("Mandatory")]
        Mandatory,
    }
    public enum E_LoanSectionOfActType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("203B")]
        S203B,
        [XmlEnum("203B251")]
        S203B251,
        [XmlEnum("203K")]
        S203K,
        [XmlEnum("203K251")]
        S203K251,
        [XmlEnum("234C")]
        S234C,
        [XmlEnum("234C251")]
        S234C251,
        [XmlEnum("257")]
        S257,
        [XmlEnum("184")]
        S184,
        [XmlEnum("184A")]
        S184A,
        [XmlEnum("201S")]
        S201S,
        [XmlEnum("201SD")]
        S201SD,
        [XmlEnum("201U")]
        S201U,
        [XmlEnum("201UD")]
        S201UD,
        [XmlEnum("203B2")]
        S203B2,
        [XmlEnum("203H")]
        S203H,
        [XmlEnum("203I")]
        S203I,
        [XmlEnum("203K241")]
        S203K241,
        [XmlEnum("204A")]
        S204A,
        [XmlEnum("204G Single Family Property Disposition")]
        S204GSingleFamilyPropertyDisposition,
        [XmlEnum("204G FHA Good Neighbor Next Door")]
        S204GFHAGoodNeighborNextDoor,
        [XmlEnum("222")]
        S222,
        [XmlEnum("233")]
        S233,
        [XmlEnum("235")]
        S235,
        [XmlEnum("237")]
        S237,
        [XmlEnum("240")]
        S240,
        [XmlEnum("245")]
        S245,
        [XmlEnum("245A")]
        S245A,
        [XmlEnum("247")]
        S247,
        [XmlEnum("248")]
        S248,
        [XmlEnum("251")]
        S251,
        [XmlEnum("255")]
        S255,
        [XmlEnum("26101")]
        S26101,
        [XmlEnum("26102")]
        S26102,
        [XmlEnum("26201")]
        S26201,
        [XmlEnum("26202")]
        S26202,
        [XmlEnum("27001")]
        S27001,
        [XmlEnum("27002")]
        S27002,
        [XmlEnum("27003")]
        S27003,
        [XmlEnum("3710")]
        S3710,
        [XmlEnum("502")]
        S502,
        [XmlEnum("513")]
        S513,
        [XmlEnum("8Y")]
        S8Y,
        [XmlEnum("FHA Title I Insurance for Manufactured Homes")]
        SFHATitleIInsuranceForManufacturedHomes,
        [XmlEnum("FHA Title I Insurance for Property Improvement Loans")]
        SFHATitleIInsuranceForPropertyImprovementLoans,
        [XmlEnum("221D2")]
        S221D2,
        [XmlEnum("221D2251")]
        S221D2251,
        [XmlEnum("Other")]
        Other,
    }
    public enum E_PropertyInformationLoanPurpose
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Purchase")]
        Purchase,
        [XmlEnum("Refinance")]
        Refinance,
        [XmlEnum("Construction Only")]
        ConstructionOnly,
        [XmlEnum("Construction To Permanent")]
        ConstructionToPermanent,
        [XmlEnum("Other")]
        Other,
        [XmlEnum("Unknown")]
        Unknown,
    }
    public enum E_PropertyInformationLienPriorityType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("First Mortgage")]
        FirstMortgage,
        [XmlEnum("Second Mortgage")]
        SecondMortgage,
        [XmlEnum("Third Mortgage")]
        ThirdMortgage,
        [XmlEnum("Fourth Mortgage")]
        FourthMortgage,
        [XmlEnum("Other Mortgage")]
        OtherMortgage,
    }
    public enum E_PropertyInformationRefinancePurpose
    {
        [XmlEnum("")]
        None,
        [XmlEnum("No Cash-Out")]
        NoCashOut,
        [XmlEnum("Cash-Out/Other")]
        CashOutOther,
        [XmlEnum("Cash-Out/Home Improvement")]
        CashOutHomeImprovement,
        [XmlEnum("Cash-Out/Debt Consolidation")]
        CashOutDebtConsolidation,
        [XmlEnum("Limited Cash-Out")]
        LimitedCashOut,
    }
    public enum E_PropertyInformationOccupancyType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Abandoned")]
        Abandoned,
        [XmlEnum("AdverseOccupied")]
        AdverseOccupied,
        [XmlEnum("OwnerOccupied")]
        OwnerOccupied,
        [XmlEnum("RemovedOrDestroyed")]
        RemovedOrDestroyed,
        [XmlEnum("TenantOccupied")]
        TenantOccupied,
        [XmlEnum("Unknown")]
        Unknown,
        [XmlEnum("Vacant")]
        Vacant,
    }
    public enum E_PropertyInformationPropertyUsageType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Primary Residence")]
        PrimaryResidence,
        [XmlEnum("Second Home")]
        SecondHome,
        [XmlEnum("Investor")]
        Investor,
    }
    public enum E_PropertyInformationPropertyType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Single Family")]
        SingleFamily,
        [XmlEnum("Condominium")]
        Condominium,
        [XmlEnum("Townhouse")]
        Townhouse,
        [XmlEnum("Co-operative")]
        Cooperative,
        [XmlEnum("Two to Four Unit")]
        TwotoFourUnit,
        [XmlEnum("Multifamily (4+ unit)")]
        Multifamily4unit,
        [XmlEnum("Manufactured/Mobile Home")]
        ManufacturedMobileHome,
        [XmlEnum("Commercial / Non-Residential")]
        CommercialNonResidential,
        [XmlEnum("Mixed Use - Residential")]
        MixedUseResidential,
        [XmlEnum("Farm")]
        Farm,
        [XmlEnum("Home and Business Combined")]
        HomeandBusinessCombined,
        [XmlEnum("Land")]
        Land,
        [XmlEnum("PUD")]
        PUD,
    }
    public enum E_BorrowerPropertyUsageTypeDesc
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Investor")]
        Investor,
        [XmlEnum("Primary Residence")]
        PrimaryResidence,
        [XmlEnum("Second Home")]
        SecondHome,
    }
    public enum E_BorrowerPropertyTitleTypeDesc
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Solely by yourself")]
        SolelyByYourself,
        [XmlEnum("Jointly with your spouse")]
        JointlyWithYourSpouse,
        [XmlEnum("Jointly with another person")]
        JointlyWithAnotherPerson,
    }
    public enum E_BorrowerUSCitizenship
    {
        [XmlEnum("")]
        None,
        [XmlEnum("US Citizen")]
        USCitizen,
        [XmlEnum("Permanent Resident Alien")]
        PermanentResidentAlien,
        [XmlEnum("Non-Permanent Resident Alien")]
        NonPermanentResidentAlien,
        [XmlEnum("Unknown")]
        Unknown,
    }
    public enum E_BorrowerBorrowerResidencyBasisType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Living Rent Free")]
        LivingRentFree,
        [XmlEnum("Own")]
        Own,
        [XmlEnum("Rent")]
        Rent,
        [XmlEnum("Unknown")]
        Unknown,
    }
    public enum E_REODispositionStatusType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Pending Sale")]
        PendingSale,
        [XmlEnum("Retain For Rental")]
        RetainForRental,
        [XmlEnum("Retain For Primary Or Secondary Residence")]
        RetainForPrimaryOrSecondaryResidence,
        [XmlEnum("Sold")]
        Sold,
    }
    public enum E_GSEPropertyType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Single Family")]
        SingleFamily,
        [XmlEnum("Condominium")]
        Condominium,
        [XmlEnum("Townhouse")]
        Townhouse,
        [XmlEnum("Cooperative")]
        Cooperative,
        [XmlEnum("Two To Four Unit Property")]
        TwoToFourUnitProperty,
        [XmlEnum("Multifamily More Than Four Units")]
        MultifamilyMoreThanFourUnits,
        [XmlEnum("Manufactured Mobile Home")]
        ManufacturedMobileHome,
        [XmlEnum("Commercial Non Residential")]
        CommercialNonResidential,
        [XmlEnum("Mixed Use Residential")]
        MixedUseResidential,
        [XmlEnum("Farm")]
        Farm,
        [XmlEnum("Home And Business Combined")]
        HomeAndBusinessCombined,
        [XmlEnum("Land")]
        Land,
        [XmlEnum("PUD")]
        PUD,
    }
    public enum E_AssetAssetType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Stock")]
        Stock,
        [XmlEnum("Bond")]
        Bond,
        [XmlEnum("Mutual Fund")]
        MutualFund,
        [XmlEnum("Checking Account")]
        CheckingAccount,
        [XmlEnum("Savings Account")]
        SavingsAccount,
        [XmlEnum("Certificate of Deposit Time Deposit")]
        CertificateofDepositTimeDeposit,
        [XmlEnum("Money Market Fund")]
        MoneyMarketFund,
        [XmlEnum("Earnest Money Cash Deposit Toward Purchase")]
        EarnestMoneyCashDepositTowardPurchase,
        [XmlEnum("Life Insurance")]
        LifeInsurance,
        [XmlEnum("Net Worth Of Business Owned")]
        NetWorthOfBusinessOwned,
        [XmlEnum("Retirement Fund")]
        RetirementFund,
        [XmlEnum("Other Non Liquid Assets")]
        OtherNonLiquidAssets,
        [XmlEnum("Cash Deposit on Sales Contract")]
        CashDepositonSalesContract,
        [XmlEnum("Gift")]
        Gift,
        [XmlEnum("Secured Borrowed Fund")]
        SecuredBorrowedFund,
        [XmlEnum("Bridge Loan")]
        BridgeLoan,
        [XmlEnum("Trust Fund")]
        TrustFund,
        [XmlEnum("Other Liquid Assets")]
        OtherLiquidAssets,
        [XmlEnum("Net Equity")]
        NetEquity,
        [XmlEnum("Automobile")]
        Automobile,
    }
    public enum E_ParticipantParticipantType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Appraiser")]
        Appraiser,
        [XmlEnum("Appraisal Company")]
        AppraisalCompany,
        [XmlEnum("Appraiser Trainee")]
        AppraiserTrainee,
        [XmlEnum("Appraisal Trainee Company")]
        AppraisalTraineeCompany,
        [XmlEnum("Loan Officer")]
        LoanOfficer,
        [XmlEnum("Loan Officer 2")]
        LoanOfficer2,
        [XmlEnum("Seller")]
        Seller,
        [XmlEnum("Gift Donor")]
        GiftDonor,
        [XmlEnum("Lender")]
        Lender,
        [XmlEnum("Broker")]
        Broker,
        [XmlEnum("Underwriter")]
        Underwriter,
        [XmlEnum("Processor")]
        Processor,
        [XmlEnum("Closer")]
        Closer,
        [XmlEnum("Listing Realtor")]
        ListingRealtor,
        [XmlEnum("Listing Realtor 2")]
        ListingRealtor2,
        [XmlEnum("Listing Realtor 3")]
        ListingRealtor3,
        [XmlEnum("Listing Real Estate Office")]
        ListingRealEstateOffice,
        [XmlEnum("Selling Realtor")]
        SellingRealtor,
        [XmlEnum("Selling Realtor 2")]
        SellingRealtor2,
        [XmlEnum("Selling Realtor 3")]
        SellingRealtor3,
        [XmlEnum("Selling Real Estate Office")]
        SellingRealEstateOffice,
        [XmlEnum("Title Company")]
        TitleCompany,
        [XmlEnum("Closing Company")]
        ClosingCompany,
        [XmlEnum("Loan Title Holder")]
        LoanTitleHolder,
        [XmlEnum("Loan Title Holder 2")]
        LoanTitleHolder2,
        [XmlEnum("Loan Title Holder Company")]
        LoanTitleHolderCompany,
        [XmlEnum("Loan Title Holder 2 Company")]
        LoanTitleHolder2Company,
        [XmlEnum("Account Executive")]
        AccountExecutive,
        [XmlEnum("2nd Gift Donor")]
        secondGiftDonor,
        [XmlEnum("2nd Seller")]
        secondSeller,
        [XmlEnum("Builder Company")]
        BuilderCompany,
        [XmlEnum("Builder")]
        Builder,
        [XmlEnum("Notary")]
        Notary,
        [XmlEnum("Landlord")]
        Landlord,
        [XmlEnum("Condo Project Name")]
        CondoProjectName,
        [XmlEnum("CPA/Tax Preparer")]
        CPATaxPreparer,
        [XmlEnum("CPA/Tax Preparer Company")]
        CPATaxPreparerCompany,
        [XmlEnum("Seller Company")]
        SellerCompany,
        [XmlEnum("2nd Seller Company")]
        secondSellerCompany,
        [XmlEnum("Land lord company")]
        Landlordcompany,
        [XmlEnum("Credit Bureau Company")]
        CreditBureauCompany,
        [XmlEnum("Appraiser 2")]
        Appraiser2,
        [XmlEnum("Appraisal Company 2")]
        AppraisalCompany2,
        [XmlEnum("Post-Servicing Purchaser")]
        PostServicingPurchaser,
        [XmlEnum("Power of Attorney")]
        PowerofAttorney,
        [XmlEnum("Third Party Negotiator")]
        ThirdPartyNegotiator,
        [XmlEnum("Correspondent Lender")]
        CorrespondentLender,
        [XmlEnum("Short Sale Buyer")]
        ShortSaleBuyer,
        [XmlEnum("Consumer Name AKA")]
        ConsumerNameAKA,
        [XmlEnum("Consumer Name AKA 2")]
        ConsumerNameAKA2,
        [XmlEnum("Consumer Name AKA 3")]
        ConsumerNameAKA3,
        [XmlEnum("Consumer Name AKA 4")]
        ConsumerNameAKA4,
        [XmlEnum("Appraisal Reviewer")]
        AppraisalReviewer,
        [XmlEnum("Appraisal Reviewer Company")]
        AppraisalReviewerCompany,
        [XmlEnum("Loan Originator")]
        LoanOriginator,
        [XmlEnum("Settlement Agent")]
        SettlementAgent,
        [XmlEnum("Settlement Company")]
        SettlementCompany,
        [XmlEnum("Attorney")]
        Attorney,
        [XmlEnum("Seller Attorney")]
        SellerAttorney,
        [XmlEnum("FHA Inspector")]
        FHAInspector,
        [XmlEnum("FHA/HUD 203k Consultant")]
        FHAHUD203kConsultant,
        [XmlEnum("FHA Consultant")]
        FHAConsultant,
        [XmlEnum("Appraisal Underwriter")]
        AppraisalUnderwriter,
        [XmlEnum("Homeowners Insurance Contact")]
        HomeownersInsuranceContact,
        [XmlEnum("Bank Attorney")]
        BankAttorney,
        [XmlEnum("Applicants Attorney")]
        ApplicantsAttorney,
        [XmlEnum("Homeowners Insurance Company")]
        HomeownersInsuranceCompany,
        [XmlEnum("Third Party Originator (TPO)")]
        ThirdPartyOriginator,
        [XmlEnum("Wealth Advisor")]
        WealthAdvisor,
        [XmlEnum("Private Banker")]
        PrivateBanker,
        [XmlEnum("Supervisory Appraiser")]
        SupervisoryAppraiser,
        [XmlEnum("Escrow Company")]
        EscrowCompany,
        [XmlEnum("Closer 2")]
        Closer2,
        [XmlEnum("Processor 2")]
        Processor2,
        [XmlEnum("Underwriter 2")]
        Underwriter2,
        [XmlEnum("Title Underwriter Company")]
        TitleUnderwriterCompany,
        [XmlEnum("Title Underwriter")]
        TitleUnderwriter,
        [XmlEnum("Appraisal Management Company")]
        AppraisalManagementCompany,
        [XmlEnum("Attorney Company")]
        AttorneyCompany,
        [XmlEnum("Seller Attorney Company")]
        SellerAttorneyCompany,
        [XmlEnum("Contractor")]
        Contractor,
        [XmlEnum("Contractor 2")]
        Contractor2,
        [XmlEnum("Contractor Company")]
        ContractorCompany,
        [XmlEnum("Contractor Company 2")]
        ContractorCompany2,
        [XmlEnum("Seller 3")]
        Seller3,
        [XmlEnum("Seller 4")]
        Seller4,
        [XmlEnum("Seller 5")]
        Seller5,
        [XmlEnum("Seller 6")]
        Seller6,
        [XmlEnum("Third Party Negotiator 2")]
        ThirdPartyNegotiator2,
        [XmlEnum("Third Party Negotiator Company")]
        ThirdPartyNegotiatorCompany,
        [XmlEnum("Third Party Negotiator Company 2")]
        ThirdPartyNegotiatorCompany2,
        [XmlEnum("Escrow Officer")]
        EscrowOfficer,
        [XmlEnum("Escrow Officer 2")]
        EscrowOfficer2,
        [XmlEnum("Title Officer")]
        TitleOfficer,
        [XmlEnum("Escrow Agent")]
        EscrowAgent,
        [XmlEnum("Closing Agent")]
        ClosingAgent,
        [XmlEnum("Doc Drawer")]
        DocDrawer,
        [XmlEnum("Notary 2")]
        Notary2,
        [XmlEnum("CPA/Tax Preparer 2")]
        CPATaxPreparer2,
        [XmlEnum("CPA/Tax Preparer 3")]
        CPATaxPreparer3,
        [XmlEnum("CPA/Tax Preparer 4")]
        CPATaxPreparer4,
        [XmlEnum("CPA/Tax Preparer Company 2")]
        CPATaxPreparerCompany2,
        [XmlEnum("CPA/Tax Preparer Company 3")]
        CPATaxPreparerCompany3,
        [XmlEnum("CPA/Tax Preparer Company 4")]
        CPATaxPreparerCompany4,
        [XmlEnum("Consumer Name AKA 5")]
        ConsumerNameAKA5,
        [XmlEnum("Consumer Name AKA 6")]
        ConsumerNameAKA6,
        [XmlEnum("Consumer Name AKA 7")]
        ConsumerNameAKA7,
        [XmlEnum("Consumer Name AKA 8")]
        ConsumerNameAKA8,
        [XmlEnum("Consumer Name AKA 9")]
        ConsumerNameAKA9,
        [XmlEnum("Consumer Name AKA 10")]
        ConsumerNameAKA10,
        [XmlEnum("Mortgage Insurance Company")]
        MortgageInsuranceCompany,
        [XmlEnum("Property Inspector")]
        PropertyInspector,
        [XmlEnum("Property Inspection Company")]
        PropertyInspectionCompany,
        [XmlEnum("Buyer Attorney")]
        BuyerAttorney,
        [XmlEnum("Non-Obligated Borrower")]
        NonObligatedBorrower,
        [XmlEnum("Non-Borrowing Spouse")]
        NonBorrowingSpouse,
        [XmlEnum("Operations Manager")]
        OperationsManager,
        [XmlEnum("Branch Manager")]
        BranchManager,
        [XmlEnum("Applicant Attorney 2")]
        ApplicantAttorney2,
        [XmlEnum("Applicant Attorney Company")]
        ApplicantAttorneyCompany,
        [XmlEnum("Bank Attorney Company")]
        BankAttorneyCompany,
        [XmlEnum("Loan Officer Assistant")]
        LoanOfficerAssistant,
        [XmlEnum("Account Executive 2")]
        AccountExecutive2,
        [XmlEnum("Lender 2")]
        Lender2,
        [XmlEnum("Processor 3")]
        Processor3,
        [XmlEnum("Underwriter 3")]
        Underwriter3,
        [XmlEnum("Closer 3")]
        Closer3,
        [XmlEnum("Buyer Agent")]
        BuyerAgent,
    }

    public enum E_LoanFileLoanFileType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("DU")]
        DU,
        [XmlEnum("CUSTOM")]
        CUSTOM,
    }
}
namespace LendersOffice.Conversions.Drive.Request
{

    public class Authentication
    {
        [XmlAttribute()]
        public string Username;
        public bool ShouldSerializeUsername() { return !string.IsNullOrEmpty(Username); }
        [XmlAttribute()]
        public string Password;
        public bool ShouldSerializePassword() { return !string.IsNullOrEmpty(Password); }
        [XmlAttribute()]
        public string PortalID;
        public bool ShouldSerializePortalID() { return !string.IsNullOrEmpty(PortalID); }
    }
    public class AccessGroup
    {
        [XmlAttribute()]
        public string Code;
        public bool ShouldSerializeCode() { return !string.IsNullOrEmpty(Code); }
    }
    public class LoanHeader
    {
        [XmlElement(Order = 1, ElementName = "AccessGroup")]
        public List<AccessGroup> AccessGroupList = new List<AccessGroup>();
    }
    public class PropertyInformation
    {
        [XmlAttribute()]
        public string StreetAddress;
        public bool ShouldSerializeStreetAddress() { return !string.IsNullOrEmpty(StreetAddress); }
        [XmlAttribute()]
        public string City;
        public bool ShouldSerializeCity() { return !string.IsNullOrEmpty(City); }
        [XmlAttribute()]
        public string State;
        public bool ShouldSerializeState() { return !string.IsNullOrEmpty(State); }
        [XmlAttribute()]
        public string ZIP;
        public bool ShouldSerializeZIP() { return !string.IsNullOrEmpty(ZIP); }
        [XmlIgnore()]
        public E_PropertyInformationLoanPurpose LoanPurpose;
        [XmlAttribute(AttributeName = "LoanPurpose")]
        public string LoanPurposeSerialized { get { return Utilities.ConvertEnumToString(LoanPurpose); } set { } }
        public bool ShouldSerializeLoanPurposeSerialized() { return !string.IsNullOrEmpty(LoanPurposeSerialized); }
        [XmlIgnore()]
        public E_PropertyInformationRefinancePurpose RefinancePurpose;
        [XmlAttribute(AttributeName = "RefinancePurpose")]
        public string RefinancePurposeSerialized { get { return Utilities.ConvertEnumToString(RefinancePurpose); } set { } }
        public bool ShouldSerializeRefinancePurposeSerialized() { return !string.IsNullOrEmpty(RefinancePurposeSerialized); }
        [XmlIgnore()]
        public E_PropertyInformationPropertyUsageType PropertyUsageType;
        [XmlAttribute(AttributeName = "PropertyUsageType")]
        public string PropertyUsageTypeSerialized { get { return Utilities.ConvertEnumToString(PropertyUsageType); } set { } }
        public bool ShouldSerializePropertyUsageTypeSerialized() { return !string.IsNullOrEmpty(PropertyUsageTypeSerialized); }
        [XmlAttribute()]
        public string AppraisedValue;
        public bool ShouldSerializeAppraisedValue() { return !string.IsNullOrEmpty(AppraisedValue); }
        [XmlIgnore()]
        public E_PropertyInformationPropertyType PropertyType;
        [XmlAttribute(AttributeName = "PropertyType")]
        public string PropertyTypeSerialized { get { return Utilities.ConvertEnumToString(PropertyType); } set { } }
        public bool ShouldSerializePropertyTypeSerialized() { return !string.IsNullOrEmpty(PropertyTypeSerialized); }
        [XmlAttribute()]
        public string ValuationDate;
        public bool ShouldSerializeValuationDate() { return !string.IsNullOrEmpty(ValuationDate); }
        [XmlAttribute()]
        public string APN;
        public bool ShouldSerializeAPN() { return !string.IsNullOrEmpty(APN); }
        [XmlIgnore()]
        public E_PropertyInformationOccupancyType OccupancyType;
        [XmlAttribute(AttributeName = "OccupancyType")]
        public string OccupancyTypeSerialized { get { return Utilities.ConvertEnumToString(OccupancyType); } set { } }
        public bool ShouldSerializeOccupancyTypeSerialized() { return !string.IsNullOrEmpty(OccupancyTypeSerialized); }
        [XmlAttribute()]
        public string StructureBuiltYear;
        public bool ShouldSerializeStructureBuiltYear() { return !string.IsNullOrEmpty(StructureBuiltYear); }
        [XmlIgnore()]
        public E_PropertyInformationLienPriorityType LienPriorityType;
        [XmlAttribute(AttributeName = "LienPriorityType")]
        public string LienPriorityTypeSerialized { get { return Utilities.ConvertEnumToString(LienPriorityType); } set { } }
        public bool ShouldSerializeLienPriorityTypeSerialized() { return !string.IsNullOrEmpty(LienPriorityTypeSerialized); }
        [XmlAttribute()]
        public string NumberOfFinancedUnits;
        public bool ShouldSerializeNumberOfFinancedUnits() { return !string.IsNullOrEmpty(NumberOfFinancedUnits); }
    }
    public class MailingAddress
    {
        [XmlAttribute()]
        public string StreetAddress;
        public bool ShouldSerializeStreetAddress() { return !string.IsNullOrEmpty(StreetAddress); }
        [XmlAttribute()]
        public string City;
        public bool ShouldSerializeCity() { return !string.IsNullOrEmpty(City); }
        [XmlAttribute()]
        public string State;
        public bool ShouldSerializeState() { return !string.IsNullOrEmpty(State); }
        [XmlAttribute()]
        public string ZIP;
        public bool ShouldSerializeZIP() { return !string.IsNullOrEmpty(ZIP); }
    }
    public class Employer
    {
        [XmlAttribute()]
        public string CompanyName;
        public bool ShouldSerializeCompanyName() { return !string.IsNullOrEmpty(CompanyName); }
        [XmlAttribute()]
        public string StreetAddress;
        public bool ShouldSerializeStreetAddress() { return !string.IsNullOrEmpty(StreetAddress); }
        [XmlAttribute()]
        public string City;
        public bool ShouldSerializeCity() { return !string.IsNullOrEmpty(City); }
        [XmlAttribute()]
        public string State;
        public bool ShouldSerializeState() { return !string.IsNullOrEmpty(State); }
        [XmlAttribute()]
        public string ZIP;
        public bool ShouldSerializeZIP() { return !string.IsNullOrEmpty(ZIP); }
        [XmlIgnore()]
        public E_YNIndicator SelfEmployed;
        [XmlAttribute(AttributeName = "SelfEmployed")]
        public string SelfEmployedSerialized { get { return Utilities.ConvertEnumToString(SelfEmployed); } set { } }
        public bool ShouldSerializeSelfEmployedSerialized() { return !string.IsNullOrEmpty(SelfEmployedSerialized); }
        [XmlAttribute()]
        public string YearsOnJob;
        public bool ShouldSerializeYearsOnJob() { return !string.IsNullOrEmpty(YearsOnJob); }
        [XmlAttribute()]
        public string PositionTitle;
        public bool ShouldSerializePositionTitle() { return !string.IsNullOrEmpty(PositionTitle); }
        [XmlAttribute()]
        public string EmployerPhone;
        public bool ShouldSerializeEmployerPhone() { return !string.IsNullOrEmpty(EmployerPhone); }
        [XmlAttribute()]
        public string LineOfWorkYears;
        public bool ShouldSerializeLineOfWorkYears() { return !string.IsNullOrEmpty(LineOfWorkYears); }
        [XmlIgnore()]
        public E_YNIndicator CurrentEmployerIndicator;
        [XmlAttribute(AttributeName = "CurrentEmployerIndicator")]
        public string CurrentEmployerIndicatorSerialized { get { return Utilities.ConvertEnumToString(CurrentEmployerIndicator); } set { } }
        public bool ShouldSerializeCurrentEmployerIndicatorSerialized() { return !string.IsNullOrEmpty(CurrentEmployerIndicatorSerialized); }
    }
    public class REO
    {
        [XmlAttribute()]
        public string SequenceNumber;
        public bool ShouldSerializeSequenceNumber() { return !string.IsNullOrEmpty(SequenceNumber); }
        [XmlAttribute()]
        public string StreetAddress;
        public bool ShouldSerializeStreetAddress() { return !string.IsNullOrEmpty(StreetAddress); }
        [XmlAttribute()]
        public string City;
        public bool ShouldSerializeCity() { return !string.IsNullOrEmpty(City); }
        [XmlAttribute()]
        public string State;
        public bool ShouldSerializeState() { return !string.IsNullOrEmpty(State); }
        [XmlAttribute()]
        public string ZIP;
        public bool ShouldSerializeZIP() { return !string.IsNullOrEmpty(ZIP); }
        [XmlAttribute()]
        public string ZIP4;
        public bool ShouldSerializeZIP4() { return !string.IsNullOrEmpty(ZIP4); }
        [XmlIgnore()]
        public E_YNIndicator CurrentResidenceIndicator;
        [XmlAttribute(AttributeName = "CurrentResidenceIndicator")]
        public string CurrentResidenceIndicatorSerialized { get { return Utilities.ConvertEnumToString(CurrentResidenceIndicator); } set { } }
        public bool ShouldSerializeCurrentResidenceIndicatorSerialized() { return !string.IsNullOrEmpty(CurrentResidenceIndicatorSerialized); }
        [XmlIgnore()]
        public E_REODispositionStatusType DispositionStatusType;
        [XmlAttribute(AttributeName = "DispositionStatusType")]
        public string DispositionStatusTypeSerialized { get { return Utilities.ConvertEnumToString(DispositionStatusType); } set { } }
        public bool ShouldSerializeDispositionStatusTypeSerialized() { return !string.IsNullOrEmpty(DispositionStatusTypeSerialized); }
        [XmlIgnore()]
        public E_GSEPropertyType GSEPropertyType;
        [XmlAttribute(AttributeName = "GSEPropertyType")]
        public string GSEPropertyTypeSerialized { get { return Utilities.ConvertEnumToString(GSEPropertyType); } set { } }
        public bool ShouldSerializeGSEPropertyTypeSerialized() { return !string.IsNullOrEmpty(GSEPropertyTypeSerialized); }
        [XmlAttribute()]
        public string LienInstallmentAmount;
        public bool ShouldSerializeLienInstallmentAmount() { return !string.IsNullOrEmpty(LienInstallmentAmount); }
        [XmlAttribute()]
        public string LienUPBAmount;
        public bool ShouldSerializeLienUPBAmount() { return !string.IsNullOrEmpty(LienUPBAmount); }
        [XmlAttribute()]
        public string MaintenanceExpenseAmount;
        public bool ShouldSerializeMaintenanceExpenseAmount() { return !string.IsNullOrEmpty(MaintenanceExpenseAmount); }
        [XmlAttribute()]
        public string MarketValueAmount;
        public bool ShouldSerializeMarketValueAmount() { return !string.IsNullOrEmpty(MarketValueAmount); }
        [XmlAttribute()]
        public string RentalIncomeGrossAmount;
        public bool ShouldSerializeRentalIncomeGrossAmount() { return !string.IsNullOrEmpty(RentalIncomeGrossAmount); }
        [XmlAttribute()]
        public string RentalIncomeNetAmount;
        public bool ShouldSerializeRentalIncomeNetAmount() { return !string.IsNullOrEmpty(RentalIncomeNetAmount); }
        [XmlIgnore()]
        public E_YNIndicator SubjectIndicator;
        [XmlAttribute(AttributeName = "SubjectIndicator")]
        public string SubjectIndicatorSerialized { get { return Utilities.ConvertEnumToString(SubjectIndicator); } set { } }
        public bool ShouldSerializeSubjectIndicatorSerialized() { return !string.IsNullOrEmpty(SubjectIndicatorSerialized); }
    }
    public class Asset
    {
        [XmlIgnore()]
        public E_AssetAssetType AssetType;
        [XmlAttribute(AttributeName = "AssetType")]
        public string AssetTypeSerialized { get { return Utilities.ConvertEnumToString(AssetType); } set { } }
        public bool ShouldSerializeAssetTypeSerialized() { return !string.IsNullOrEmpty(AssetTypeSerialized); }
        [XmlAttribute()]
        public string HolderName;
        public bool ShouldSerializeHolderName() { return !string.IsNullOrEmpty(HolderName); }
        [XmlAttribute()]
        public string HolderStreetAddress;
        public bool ShouldSerializeHolderStreetAddress() { return !string.IsNullOrEmpty(HolderStreetAddress); }
        [XmlAttribute()]
        public string HolderCity;
        public bool ShouldSerializeHolderCity() { return !string.IsNullOrEmpty(HolderCity); }
        [XmlAttribute()]
        public string HolderState;
        public bool ShouldSerializeHolderState() { return !string.IsNullOrEmpty(HolderState); }
        [XmlAttribute()]
        public string HolderPostalCode;
        public bool ShouldSerializeHolderPostalCode() { return !string.IsNullOrEmpty(HolderPostalCode); }
        [XmlAttribute()]
        public string CashOrMarketValueAmount;
        public bool ShouldSerializeCashOrMarketValueAmount() { return !string.IsNullOrEmpty(CashOrMarketValueAmount); }
        [XmlAttribute()]
        public string FaceValueAmount;
        public bool ShouldSerializeFaceValueAmount() { return !string.IsNullOrEmpty(FaceValueAmount); }
        [XmlAttribute()]
        public string Qty;
        public bool ShouldSerializeQty() { return !string.IsNullOrEmpty(Qty); }
        [XmlAttribute()]
        public string ShortDescription;
        public bool ShouldSerializeShortDescription() { return !string.IsNullOrEmpty(ShortDescription); }
        [XmlAttribute()]
        public string LongDescription;
        public bool ShouldSerializeLongDescription() { return !string.IsNullOrEmpty(LongDescription); }
        [XmlAttribute()]
        public string AccountIdentifier;
        public bool ShouldSerializeAccountIdentifier() { return !string.IsNullOrEmpty(AccountIdentifier); }
    }
    public class Borrower
    {
        [XmlAttribute()]
        public string SequenceNumber;
        public bool ShouldSerializeSequenceNumber() { return !string.IsNullOrEmpty(SequenceNumber); }
        [XmlAttribute()]
        public string Prefix;
        public bool ShouldSerializePrefix() { return !string.IsNullOrEmpty(Prefix); }
        [XmlAttribute()]
        public string FirstName;
        public bool ShouldSerializeFirstName() { return !string.IsNullOrEmpty(FirstName); }
        [XmlAttribute()]
        public string MiddleName;
        public bool ShouldSerializeMiddleName() { return !string.IsNullOrEmpty(MiddleName); }
        [XmlAttribute()]
        public string LastName;
        public bool ShouldSerializeLastName() { return !string.IsNullOrEmpty(LastName); }
        [XmlAttribute()]
        public string Suffix;
        public bool ShouldSerializeSuffix() { return !string.IsNullOrEmpty(Suffix); }
        [XmlAttribute()]
        public string SSN;
        public bool ShouldSerializeSSN() { return !string.IsNullOrEmpty(SSN); }
        [XmlAttribute()]
        public string DOB;
        public bool ShouldSerializeDOB() { return !string.IsNullOrEmpty(DOB); }
        [XmlAttribute()]
        public string StreetAddress;
        public bool ShouldSerializeStreetAddress() { return !string.IsNullOrEmpty(StreetAddress); }
        [XmlAttribute()]
        public string City;
        public bool ShouldSerializeCity() { return !string.IsNullOrEmpty(City); }
        [XmlAttribute()]
        public string State;
        public bool ShouldSerializeState() { return !string.IsNullOrEmpty(State); }
        [XmlAttribute()]
        public string ZIP;
        public bool ShouldSerializeZIP() { return !string.IsNullOrEmpty(ZIP); }
        [XmlIgnore()]
        public E_BorrowerUSCitizenship USCitizenship;
        [XmlAttribute(AttributeName = "USCitizenship")]
        public string USCitizenshipSerialized { get { return Utilities.ConvertEnumToString(USCitizenship); } set { } }
        public bool ShouldSerializeUSCitizenshipSerialized() { return !string.IsNullOrEmpty(USCitizenshipSerialized); }
        [XmlAttribute()]
        public string BaseIncome;
        public bool ShouldSerializeBaseIncome() { return !string.IsNullOrEmpty(BaseIncome); }
        [XmlAttribute()]
        public string TotalIncome;
        public bool ShouldSerializeTotalIncome() { return !string.IsNullOrEmpty(TotalIncome); }
        [XmlAttribute()]
        public string HomePhone;
        public bool ShouldSerializeHomePhone() { return !string.IsNullOrEmpty(HomePhone); }
        [XmlAttribute()]
        public string EmailAddress;
        public bool ShouldSerializeEmailAddress() { return !string.IsNullOrEmpty(EmailAddress); }
        [XmlAttribute()]
        public string IPAddress;
        public bool ShouldSerializeIPAddress() { return !string.IsNullOrEmpty(IPAddress); }
        [XmlAttribute()]
        public string FICOScore;
        public bool ShouldSerializeFICOScore() { return !string.IsNullOrEmpty(FICOScore); }
        [XmlAttribute()]
        public string CreditReportDate;
        public bool ShouldSerializeCreditReportDate() { return !string.IsNullOrEmpty(CreditReportDate); }
        [XmlIgnore()]
        public E_YNIndicator BankruptcyIndicator;
        [XmlAttribute(AttributeName = "BankruptcyIndicator")]
        public string BankruptcyIndicatorSerialized { get { return Utilities.ConvertEnumToString(BankruptcyIndicator); } set { } }
        public bool ShouldSerializeBankruptcyIndicatorSerialized() { return !string.IsNullOrEmpty(BankruptcyIndicatorSerialized); }
        [XmlIgnore()]
        public E_YNIndicator OutstandingJudgementsIndicator;
        [XmlAttribute(AttributeName = "OutstandingJudgementsIndicator")]
        public string OutstandingJudgementsIndicatorSerialized { get { return Utilities.ConvertEnumToString(OutstandingJudgementsIndicator); } set { } }
        public bool ShouldSerializeOutstandingJudgementsIndicatorSerialized() { return !string.IsNullOrEmpty(OutstandingJudgementsIndicatorSerialized); }
        [XmlIgnore()]
        public E_YNIndicator PropertyForeclosedPastSevenYearsIndicator;
        [XmlAttribute(AttributeName = "PropertyForeclosedPastSevenYearsIndicator")]
        public string PropertyForeclosedPastSevenYearsIndicatorSerialized { get { return Utilities.ConvertEnumToString(PropertyForeclosedPastSevenYearsIndicator); } set { } }
        public bool ShouldSerializePropertyForeclosedPastSevenYearsIndicatorSerialized() { return !string.IsNullOrEmpty(PropertyForeclosedPastSevenYearsIndicatorSerialized); }
        [XmlIgnore()]
        public E_YNIndicator LoanForeclosureOrJudgementIndicator;
        [XmlAttribute(AttributeName = "LoanForeclosureOrJudgementIndicator")]
        public string LoanForeclosureOrJudgementIndicatorSerialized { get { return Utilities.ConvertEnumToString(LoanForeclosureOrJudgementIndicator); } set { } }
        public bool ShouldSerializeLoanForeclosureOrJudgementIndicatorSerialized() { return !string.IsNullOrEmpty(LoanForeclosureOrJudgementIndicatorSerialized); }
        [XmlIgnore()]
        public E_BorrowerBorrowerResidencyBasisType BorrowerResidencyBasisType;
        [XmlAttribute(AttributeName = "BorrowerResidencyBasisType")]
        public string BorrowerResidencyBasisTypeSerialized { get { return Utilities.ConvertEnumToString(BorrowerResidencyBasisType); } set { } }
        public bool ShouldSerializeBorrowerResidencyBasisTypeSerialized() { return !string.IsNullOrEmpty(BorrowerResidencyBasisTypeSerialized); }
        [XmlAttribute()]
        public string Overtime;
        public bool ShouldSerializeOvertime() { return !string.IsNullOrEmpty(Overtime); }
        [XmlAttribute()]
        public string Bonus;
        public bool ShouldSerializeBonus() { return !string.IsNullOrEmpty(Bonus); }
        [XmlAttribute()]
        public string Commissions;
        public bool ShouldSerializeCommissions() { return !string.IsNullOrEmpty(Commissions); }
        [XmlAttribute()]
        public string DividendsInterest;
        public bool ShouldSerializeDividendsInterest() { return !string.IsNullOrEmpty(DividendsInterest); }
        [XmlAttribute()]
        public string NetRentalIncome;
        public bool ShouldSerializeNetRentalIncome() { return !string.IsNullOrEmpty(NetRentalIncome); }
        [XmlAttribute()]
        public string URLABorrowerTotalOtherIncomeAmount;
        public bool ShouldSerializeURLABorrowerTotalOtherIncomeAmount() { return !string.IsNullOrEmpty(URLABorrowerTotalOtherIncomeAmount); }
        [XmlIgnore()]
        public E_YNIndicator IntentToOccupyIndicator;
        [XmlAttribute(AttributeName = "IntentToOccupyIndicator")]
        public string IntentToOccupyIndicatorSerialized { get { return Utilities.ConvertEnumToString(IntentToOccupyIndicator); } set { } }
        public bool ShouldSerializeIntentToOccupyIndicatorSerialized() { return !string.IsNullOrEmpty(IntentToOccupyIndicatorSerialized); }
        [XmlIgnore()]
        public E_YNIndicator HomeownerPastThreeYearsIndicatorIndicator;
        [XmlAttribute(AttributeName = "HomeownerPastThreeYearsIndicatorIndicator")]
        public string HomeownerPastThreeYearsIndicatorIndicatorSerialized { get { return Utilities.ConvertEnumToString(HomeownerPastThreeYearsIndicatorIndicator); } set { } }
        public bool ShouldSerializeHomeownerPastThreeYearsIndicatorIndicatorSerialized() { return !string.IsNullOrEmpty(HomeownerPastThreeYearsIndicatorIndicatorSerialized); }
        [XmlIgnore()]
        public E_BorrowerPropertyUsageTypeDesc PropertyUsageTypeDesc;
        [XmlAttribute(AttributeName = "PropertyUsageTypeDesc")]
        public string PropertyUsageTypeDescSerialized { get { return Utilities.ConvertEnumToString(PropertyUsageTypeDesc); } set { } }
        public bool ShouldSerializePropertyUsageTypeDescSerialized() { return !string.IsNullOrEmpty(PropertyUsageTypeDescSerialized); }
        [XmlIgnore()]
        public E_BorrowerPropertyTitleTypeDesc PropertyTitleTypeDesc;
        [XmlAttribute(AttributeName = "PropertyTitleTypeDesc")]
        public string PropertyTitleTypeDescSerialized { get { return Utilities.ConvertEnumToString(PropertyTitleTypeDesc); } set { } }
        public bool ShouldSerializePropertyTitleTypeDescSerialized() { return !string.IsNullOrEmpty(PropertyTitleTypeDescSerialized); }
        [XmlAttribute()]
        public string YearsAtAddress;
        public bool ShouldSerializeYearsAtAddress() { return !string.IsNullOrEmpty(YearsAtAddress); }
        [XmlAttribute()]
        public string MonthsAtAddress;
        public bool ShouldSerializeMonthsAtAddress() { return !string.IsNullOrEmpty(MonthsAtAddress); }
        [XmlAttribute()]
        public string AgeAtApplicationYears;
        public bool ShouldSerializeAgeAtApplicationYears() { return !string.IsNullOrEmpty(AgeAtApplicationYears); }
        [XmlElement(Order = 1)]
        public MailingAddress MailingAddress = new MailingAddress();
        [XmlElement(Order = 2, ElementName = "Employer")]
        public List<Employer> EmployerList = new List<Employer>();
        [XmlElement(Order = 3, ElementName = "REO")]
        public List<REO> REOList = new List<REO>();
        [XmlElement(Order = 4, ElementName = "Asset")]
        public List<Asset> AssetList = new List<Asset>();
    }
    public class Participant
    {
        [XmlIgnore()]
        public E_ParticipantParticipantType ParticipantType;
        [XmlAttribute(AttributeName = "ParticipantType")]
        public string ParticipantTypeSerialized { get { return Utilities.ConvertEnumToString(ParticipantType); } set { } }
        public bool ShouldSerializeParticipantTypeSerialized() { return !string.IsNullOrEmpty(ParticipantTypeSerialized); }
        [XmlAttribute()]
        public string FirstName;
        public bool ShouldSerializeFirstName() { return !string.IsNullOrEmpty(FirstName); }
        [XmlAttribute()]
        public string MiddleName;
        public bool ShouldSerializeMiddleName() { return !string.IsNullOrEmpty(MiddleName); }
        [XmlAttribute()]
        public string LastName;
        public bool ShouldSerializeLastName() { return !string.IsNullOrEmpty(LastName); }
        [XmlAttribute()]
        public string LicenseOrCertNum;
        public bool ShouldSerializeLicenseOrCertNum() { return !string.IsNullOrEmpty(LicenseOrCertNum); }
        [XmlAttribute()]
        public string CompanyName;
        public bool ShouldSerializeCompanyName() { return !string.IsNullOrEmpty(CompanyName); }
        [XmlAttribute()]
        public string StreetAddress;
        public bool ShouldSerializeStreetAddress() { return !string.IsNullOrEmpty(StreetAddress); }
        [XmlAttribute()]
        public string City;
        public bool ShouldSerializeCity() { return !string.IsNullOrEmpty(City); }
        [XmlAttribute()]
        public string State;
        public bool ShouldSerializeState() { return !string.IsNullOrEmpty(State); }
        [XmlAttribute()]
        public string ZIP;
        public bool ShouldSerializeZIP() { return !string.IsNullOrEmpty(ZIP); }
        [XmlAttribute()]
        public string Phone;
        public bool ShouldSerializePhone() { return !string.IsNullOrEmpty(Phone); }
        [XmlAttribute()]
        public string MailingAddress;
        public bool ShouldSerializeMailingAddress() { return !string.IsNullOrEmpty(MailingAddress); }
        [XmlAttribute()]
        public string MailingCity;
        public bool ShouldSerializeMailingCity() { return !string.IsNullOrEmpty(MailingCity); }
        [XmlAttribute()]
        public string MailingState;
        public bool ShouldSerializeMailingState() { return !string.IsNullOrEmpty(MailingState); }
        [XmlAttribute()]
        public string MailingZip;
        public bool ShouldSerializeMailingZip() { return !string.IsNullOrEmpty(MailingZip); }
        [XmlAttribute()]
        public string ParticipantUniqueId;
        public bool ShouldSerializeParticipantUniqueId() { return !string.IsNullOrEmpty(ParticipantUniqueId); }
        [XmlAttribute()]
        public string EmailAddress;
        public bool ShouldSerializeEmailAddress() { return !string.IsNullOrEmpty(EmailAddress); }
        [XmlAttribute()]
        public string IPAddress;
        public bool ShouldSerializeIPAddress() { return !string.IsNullOrEmpty(IPAddress); }
        [XmlAttribute()]
        public string ADSID;
        public bool ShouldSerializeADSID() { return !string.IsNullOrEmpty(ADSID); }
        [XmlAttribute()]
        public string ADSCompanyID;
        public bool ShouldSerializeADSCompanyID() { return !string.IsNullOrEmpty(ADSCompanyID); }
        [XmlElement(Order = 1, ElementName = "DBAName")]
        public List<string> DBANameList;
    }
    public class CustomerDefinedField
    {
        [XmlAttribute()]
        public string FieldName;
        public bool ShouldSerializeFieldName() { return !string.IsNullOrEmpty(FieldName); }
        [XmlAttribute()]
        public string FieldValue;
        public bool ShouldSerializeFieldValue() { return !string.IsNullOrEmpty(FieldValue); }
    }
    public class LoanFile
    {
        [XmlIgnore()]
        public E_LoanFileLoanFileType LoanFileType;
        [XmlAttribute(AttributeName = "LoanFileType")]
        public string LoanFileTypeSerialized { get { return Utilities.ConvertEnumToString(LoanFileType); } set { } }
        public bool ShouldSerializeLoanFileTypeSerialized() { return !string.IsNullOrEmpty(LoanFileTypeSerialized); }
    }
    public class Loan
    {
        [XmlAttribute()]
        public string LoanNumber;
        public bool ShouldSerializeLoanNumber() { return !string.IsNullOrEmpty(LoanNumber); }
        [XmlAttribute()]
        public string ApplicationDate;
        public bool ShouldSerializeApplicationDate() { return !string.IsNullOrEmpty(ApplicationDate); }
        [XmlAttribute()]
        public string CloseDate;
        public bool ShouldSerializeCloseDate() { return !string.IsNullOrEmpty(CloseDate); }
        [XmlAttribute()]
        public string FundingDate;
        public bool ShouldSerializeFundingDate() { return !string.IsNullOrEmpty(FundingDate); }
        [XmlAttribute()]
        public string LoanAmount;
        public bool ShouldSerializeLoanAmount() { return !string.IsNullOrEmpty(LoanAmount); }
        [XmlAttribute()]
        public string PurchasePrice;
        public bool ShouldSerializePurchasePrice() { return !string.IsNullOrEmpty(PurchasePrice); }
        [XmlAttribute()]
        public string SubtotalLiquidAssets;
        public bool ShouldSerializeSubtotalLiquidAssets() { return !string.IsNullOrEmpty(SubtotalLiquidAssets); }
        [XmlAttribute()]
        public string TotalAssets;
        public bool ShouldSerializeTotalAssets() { return !string.IsNullOrEmpty(TotalAssets); }
        [XmlAttribute()]
        public string SubordinateFinancing;
        public bool ShouldSerializeSubordinateFinancing() { return !string.IsNullOrEmpty(SubordinateFinancing); }
        [XmlAttribute()]
        public string LTV;
        public bool ShouldSerializeLTV() { return !string.IsNullOrEmpty(LTV); }
        [XmlAttribute()]
        public string CLTV;
        public bool ShouldSerializeCLTV() { return !string.IsNullOrEmpty(CLTV); }
        [XmlAttribute()]
        public string DTI;
        public bool ShouldSerializeDTI() { return !string.IsNullOrEmpty(DTI); }
        [XmlAttribute()]
        public string NoteRate;
        public bool ShouldSerializeNoteRate() { return !string.IsNullOrEmpty(NoteRate); }
        [XmlAttribute()]
        public string UnpaidPrincipalBalance;
        public bool ShouldSerializeUnpaidPrincipalBalance() { return !string.IsNullOrEmpty(UnpaidPrincipalBalance); }
        [XmlAttribute()]
        public string RefiCashoutAmount;
        public bool ShouldSerializeRefiCashoutAmount() { return !string.IsNullOrEmpty(RefiCashoutAmount); }
        [XmlAttribute()]
        public string UnderwritingDate;
        public bool ShouldSerializeUnderwritingDate() { return !string.IsNullOrEmpty(UnderwritingDate); }
        [XmlAttribute()]
        public string ApprovedDate;
        public bool ShouldSerializeApprovedDate() { return !string.IsNullOrEmpty(ApprovedDate); }
        [XmlAttribute()]
        public string DocumentsDrawnDate;
        public bool ShouldSerializeDocumentsDrawnDate() { return !string.IsNullOrEmpty(DocumentsDrawnDate); }
        [XmlAttribute()]
        public string DeclinedDate;
        public bool ShouldSerializeDeclinedDate() { return !string.IsNullOrEmpty(DeclinedDate); }
        [XmlAttribute()]
        public string WithdrawnDate;
        public bool ShouldSerializeWithdrawnDate() { return !string.IsNullOrEmpty(WithdrawnDate); }
        [XmlAttribute()]
        public string InitialSubmissionDate;
        public bool ShouldSerializeInitialSubmissionDate() { return !string.IsNullOrEmpty(InitialSubmissionDate); }
        [XmlIgnore()]
        public E_LoanDocumentationType DocumentationType;
        [XmlAttribute(AttributeName = "DocumentationType")]
        public string DocumentationTypeSerialized { get { return Utilities.ConvertEnumToString(DocumentationType); } set { } }
        public bool ShouldSerializeDocumentationTypeSerialized() { return !string.IsNullOrEmpty(DocumentationTypeSerialized); }
        [XmlIgnore()]
        public E_LoanMortgageType MortgageType;
        [XmlAttribute(AttributeName = "MortgageType")]
        public string MortgageTypeSerialized { get { return Utilities.ConvertEnumToString(MortgageType); } set { } }
        public bool ShouldSerializeMortgageTypeSerialized() { return !string.IsNullOrEmpty(MortgageTypeSerialized); }
        [XmlAttribute()]
        public string FHALenderIdentifier;
        public bool ShouldSerializeFHALenderIdentifier() { return !string.IsNullOrEmpty(FHALenderIdentifier); }
        [XmlAttribute()]
        public string FHASponsorIdentifier;
        public bool ShouldSerializeFHASponsorIdentifier() { return !string.IsNullOrEmpty(FHASponsorIdentifier); }
        [XmlIgnore()]
        public E_YNIndicator JointCreditRequested;
        [XmlAttribute(AttributeName = "JointCreditRequested")]
        public string JointCreditRequestedSerialized { get { return Utilities.ConvertEnumToString(JointCreditRequested); } set { } }
        public bool ShouldSerializeJointCreditRequestedSerialized() { return !string.IsNullOrEmpty(JointCreditRequestedSerialized); }
        [XmlAttribute()]
        public string BillingIdentifier;
        public bool ShouldSerializeBillingIdentifier() { return !string.IsNullOrEmpty(BillingIdentifier); }
        [XmlAttribute()]
        public string OriginatorLoanNumber;
        public bool ShouldSerializeOriginatorLoanNumber() { return !string.IsNullOrEmpty(OriginatorLoanNumber); }
        [XmlAttribute()]
        public string REOMktValue;
        public bool ShouldSerializeREOMktValue() { return !string.IsNullOrEmpty(REOMktValue); }
        [XmlAttribute()]
        public string MERSMinIdentifier;
        public bool ShouldSerializeMERSMinIdentifier() { return !string.IsNullOrEmpty(MERSMinIdentifier); }
        [XmlAttribute()]
        public string DUCaseFileId;
        public bool ShouldSerializeDUCaseFileId() { return !string.IsNullOrEmpty(DUCaseFileId); }
        [XmlIgnore()]
        public E_LoanDUType DUType;
        [XmlAttribute(AttributeName = "DUType")]
        public string DUTypeSerialized { get { return Utilities.ConvertEnumToString(DUType); } set { } }
        public bool ShouldSerializeDUTypeSerialized() { return !string.IsNullOrEmpty(DUTypeSerialized); }
        [XmlAttribute()]
        public string LPKeyNum;
        public bool ShouldSerializeLPKeyNum() { return !string.IsNullOrEmpty(LPKeyNum); }
        [XmlIgnore()]
        public E_LoanLPType LPType;
        [XmlAttribute(AttributeName = "LPType")]
        public string LPTypeSerialized { get { return Utilities.ConvertEnumToString(LPType); } set { } }
        public bool ShouldSerializeLPTypeSerialized() { return !string.IsNullOrEmpty(LPTypeSerialized); }
        [XmlIgnore()]
        public E_LoanAttachmentType AttachmentType;
        [XmlAttribute(AttributeName = "AttachmentType")]
        public string AttachmentTypeSerialized { get { return Utilities.ConvertEnumToString(AttachmentType); } set { } }
        public bool ShouldSerializeAttachmentTypeSerialized() { return !string.IsNullOrEmpty(AttachmentTypeSerialized); }
        [XmlIgnore()]
        public E_LoanRateLockType RateLockType;
        [XmlAttribute(AttributeName = "RateLockType")]
        public string RateLockTypeSerialized { get { return Utilities.ConvertEnumToString(RateLockType); } set { } }
        public bool ShouldSerializeRateLockTypeSerialized() { return !string.IsNullOrEmpty(RateLockTypeSerialized); }
        [XmlAttribute()]
        public string IndexMarginPercent;
        public bool ShouldSerializeIndexMarginPercent() { return !string.IsNullOrEmpty(IndexMarginPercent); }
        [XmlIgnore()]
        public E_LoanFNMCommunityLendingProductType FNMCommunityLendingProductType;
        [XmlAttribute(AttributeName = "FNMCommunityLendingProductType")]
        public string FNMCommunityLendingProductTypeSerialized { get { return Utilities.ConvertEnumToString(FNMCommunityLendingProductType); } set { } }
        public bool ShouldSerializeFNMCommunityLendingProductTypeSerialized() { return !string.IsNullOrEmpty(FNMCommunityLendingProductTypeSerialized); }
        [XmlIgnore()]
        public E_YNIndicator FNMNeighborsMortgageEligibilityIndicator;
        [XmlAttribute(AttributeName = "FNMNeighborsMortgageEligibilityIndicator")]
        public string FNMNeighborsMortgageEligibilityIndicatorSerialized { get { return Utilities.ConvertEnumToString(FNMNeighborsMortgageEligibilityIndicator); } set { } }
        public bool ShouldSerializeFNMNeighborsMortgageEligibilityIndicatorSerialized() { return !string.IsNullOrEmpty(FNMNeighborsMortgageEligibilityIndicatorSerialized); }
        [XmlIgnore()]
        public E_LoanSectionOfActType SectionOfActType;
        [XmlAttribute(AttributeName = "SectionOfActType")]
        public string SectionOfActTypeSerialized { get { return Utilities.ConvertEnumToString(SectionOfActType); } set { } }
        public bool ShouldSerializeSectionOfActTypeSerialized() { return !string.IsNullOrEmpty(SectionOfActTypeSerialized); }
        [XmlAttribute()]
        public string LoanAmortizationTermMonths;
        public bool ShouldSerializeLoanAmortizationTermMonths() { return !string.IsNullOrEmpty(LoanAmortizationTermMonths); }
        [XmlIgnore()]
        public E_YNIndicator BalloonIndicator;
        [XmlAttribute(AttributeName = "BalloonIndicator")]
        public string BalloonIndicatorSerialized { get { return Utilities.ConvertEnumToString(BalloonIndicator); } set { } }
        public bool ShouldSerializeBalloonIndicatorSerialized() { return !string.IsNullOrEmpty(BalloonIndicatorSerialized); }
        [XmlIgnore()]
        public E_LoanRepaymentType LoanRepaymentType;
        [XmlAttribute(AttributeName = "LoanRepaymentType")]
        public string LoanRepaymentTypeSerialized { get { return Utilities.ConvertEnumToString(LoanRepaymentType); } set { } }
        public bool ShouldSerializeLoanRepaymentTypeSerialized() { return !string.IsNullOrEmpty(LoanRepaymentTypeSerialized); }
        [XmlAttribute()]
        public string AlterationsImprovementsAndRepairsAmount;
        public bool ShouldSerializeAlterationsImprovementsAndRepairsAmount() { return !string.IsNullOrEmpty(AlterationsImprovementsAndRepairsAmount); }
        [XmlAttribute()]
        public string LandEstimatedValueAmount;
        public bool ShouldSerializeLandEstimatedValueAmount() { return !string.IsNullOrEmpty(LandEstimatedValueAmount); }
        [XmlAttribute()]
        public string MIAndFundingFeeFinancedAmount;
        public bool ShouldSerializeMIAndFundingFeeFinancedAmount() { return !string.IsNullOrEmpty(MIAndFundingFeeFinancedAmount); }
        [XmlAttribute()]
        public string RefinanceIncludingDebtsToBePaidOffAmount;
        public bool ShouldSerializeRefinanceIncludingDebtsToBePaidOffAmount() { return !string.IsNullOrEmpty(RefinanceIncludingDebtsToBePaidOffAmount); }
        [XmlAttribute()]
        public string TotalMonthlyPayments;
        public bool ShouldSerializeTotalMonthlyPayments() { return !string.IsNullOrEmpty(TotalMonthlyPayments); }
        [XmlAttribute()]
        public string NetWorth;
        public bool ShouldSerializeNetWorth() { return !string.IsNullOrEmpty(NetWorth); }
        [XmlAttribute()]
        public string TotalLiabilities;
        public bool ShouldSerializeTotalLiabilities() { return !string.IsNullOrEmpty(TotalLiabilities); }
        [XmlAttribute()]
        public string ClientLoanStatus;
        public bool ShouldSerializeClientLoanStatus() { return !string.IsNullOrEmpty(ClientLoanStatus); }
        [XmlElement(Order = 1)]
        public LoanHeader LoanHeader = new LoanHeader();
        [XmlElement(Order = 2)]
        public PropertyInformation PropertyInformation = new PropertyInformation();
        [XmlElement(Order = 3, ElementName = "Borrower")]
        public List<Borrower> BorrowerList = new List<Borrower>();
        [XmlElement(Order = 4, ElementName = "Participant")]
        public List<Participant> ParticipantList = new List<Participant>();
        [XmlElement(Order = 5, ElementName = "CustomerDefinedField")]
        public List<CustomerDefinedField> CustomerDefinedFieldList = new List<CustomerDefinedField>();
        [XmlElement(Order = 6)]
        public LoanFile LoanFile = new LoanFile();
    }
    public class BatchRequest
    {
        [XmlAttribute()]
        public string CustomerBatchIdentifier;
        public bool ShouldSerializeCustomerBatchIdentifier() { return !string.IsNullOrEmpty(CustomerBatchIdentifier); }
        [XmlElement(Order = 1, ElementName = "Loan")]
        public List<Loan> LoanList = new List<Loan>();
    }
    public class DRIVERequest
    {
        [XmlAttribute()]
        public string Version;
        public bool ShouldSerializeVersion() { return !string.IsNullOrEmpty(Version); }
        [XmlElement(Order = 1)]
        public Authentication Authentication = new Authentication();
        [XmlElement(Order = 2)]
        public BatchRequest BatchRequest = new BatchRequest();

    }
}