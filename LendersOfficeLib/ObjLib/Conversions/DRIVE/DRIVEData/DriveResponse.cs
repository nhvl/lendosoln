﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;


namespace LendersOffice.Conversions.Drive.Response
{

    public enum E_YNIndicator
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Y")]
        Y,
        [XmlEnum("N")]
        N,
    }
    public enum E_LoanDRIVEStatus
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Auto Refer")]
        AutoRefer,
        [XmlEnum("High Caution")]
        HighCaution,
        [XmlEnum("Moderate Caution")]
        ModerateCaution,
        [XmlEnum("Low Caution")]
        LowCaution,
        [XmlEnum("Pass")]
        Pass,
        [XmlEnum("ERROR")]
        ERROR,
    }
    public enum E_ComponentScoreScoreType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("IDVerify")]
        IDVerify,
        [XmlEnum("AppVerify")]
        AppVerify,
        [XmlEnum("PropertyVerify")]
        PropertyVerify,
        [XmlEnum("PreQual")]
        PreQual,
    }
    public enum E_ValuationResultValuationMethodType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("AVM")]
        AVM,
    }
    public enum E_ConditionSeverity
    {
        [XmlEnum("")]
        None,
        [XmlEnum("L")]
        L,
        [XmlEnum("M")]
        M,
        [XmlEnum("H")]
        H,
    }
    public enum E_DocumentFileType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("PDF")]
        PDF,
    }

    public enum E_ProductType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("4506T")]
        Type4506T,
        [XmlEnum("VOE")]
        VOE,
        [XmlEnum("VOI")]
        VOI,
    }
}
namespace LendersOffice.Conversions.Drive.Response
{

    public class ComponentScore
    {
        [XmlAttribute()]
        public E_ComponentScoreScoreType ScoreType;
        [XmlAttribute()]
        public String Score;
        [XmlAttribute()]
        public String Status;
        [XmlAttribute()]
        public String AdjustedScore;
        [XmlAttribute()]
        public String AdjustedStatus;
    }
    public class Scores
    {
        [XmlElement(Order = 1, ElementName = "ComponentScore")]
        public List<ComponentScore> ComponentScoreList = new List<ComponentScore>();
    }
    public class ValuationResult
    {
        [XmlAttribute()]
        public E_ValuationResultValuationMethodType ValuationMethodType;
        [XmlAttribute()]
        public String ValuationModel;
        [XmlAttribute()]
        public String Valuation;
        [XmlAttribute()]
        public String ValuationLow;
        [XmlAttribute()]
        public String ValuationHigh;
        [XmlAttribute()]
        public String ConfidenceScore;
        [XmlAttribute()]
        public String ConfidenceDesc;
    }
    public class Valuations
    {
        [XmlElement(Order = 1, ElementName = "ValuationResult")]
        public List<ValuationResult> ValuationResultList = new List<ValuationResult>();
    }
    public class EIVBorrower
    {
        [XmlAttribute()]
        public String FirstName;
        [XmlAttribute()]
        public String LastName;
        [XmlAttribute()]
        public String RequestTypeDesc;
        [XmlAttribute()]
        public String OrderStatus;
    }
    public class EIVStatus
    {
        [XmlElement(Order = 1, ElementName = "EIVBorrower")]
        public List<EIVBorrower> EIVBorrowerList = new List<EIVBorrower>();
    }
    public class CreditReportBorrower
    {
        [XmlAttribute()]
        public String FirstName;
        [XmlAttribute()]
        public String LastName;
        [XmlAttribute()]
        public String CreditReportStatus;
    }
    public class CreditReportStatus
    {
        [XmlElement(Order = 1, ElementName = "CreditReportBorrower")]
        public List<CreditReportBorrower> CreditReportBorrowerList = new List<CreditReportBorrower>();
    }
    public class Condition
    {
        [XmlAttribute()]
        public String Sequence;
        [XmlAttribute()]
        public String Code;
        [XmlAttribute()]
        public String Category;
        [XmlAttribute()]
        public String Subcategory;
        [XmlAttribute()]
        public String Subject;
        [XmlAttribute()]
        public String Description;
        [XmlAttribute()]
        public String ActionStep;
        [XmlAttribute()]
        public String ScoringAnalysis;
        [XmlAttribute()]
        public E_ConditionSeverity Severity;
    }
    public class Conditions
    {
        [XmlElement(Order = 1, ElementName = "Condition")]
        public List<Condition> ConditionList = new List<Condition>();
    }
    public class Error
    {
        [XmlAttribute()]
        public String ErrorCode;
        [XmlAttribute()]
        public String ErrorDesc;
    }
    public class LoanErrors
    {
        [XmlElement(Order = 1, ElementName = "Error")]
        public List<Error> ErrorList = new List<Error>();
    }    
    public class Loan
    {
        [XmlAttribute()]
        public String LoanNumber;
        [XmlAttribute()]
        public String DRIVEScore;
        [XmlAttribute()]
        public E_LoanDRIVEStatus DRIVEStatus;
        [XmlAttribute()]
        public String ResultsURL;
        [XmlAttribute()]
        public String PrintURL;
        [XmlAttribute()]
        public String DRIVEAdjustedScore;
        [XmlAttribute()]
        public String DRIVEAdjustedStatus;
        [XmlAttribute()]
        public String NumberTimesScored;
        [XmlElement(Order = 1)]
        public Scores Scores = new Scores();
        [XmlElement(Order = 2)]
        public Valuations Valuations = new Valuations();
        [XmlElement(Order = 3)]
        public EIVStatus EIVStatus = new EIVStatus();
        [XmlElement(Order = 4)]
        public CreditReportStatus CreditReportStatus = new CreditReportStatus();
        [XmlElement(Order = 5)]
        public Conditions Conditions = new Conditions();
        [XmlElement(Order = 6)]
        public DVPinCodes DVPinCodes = new DVPinCodes();
        [XmlElement(Order = 7, ElementName="Errors")]
        public LoanErrors Errors = new LoanErrors();
        [XmlElement(Order = 8, ElementName ="Document")]
        public string EncodedEDoc;
    }
    public class RootErrors
    {
        [XmlElement(Order = 1, ElementName = "Error")]
        public List<Error> ErrorsList = new List<Error>();
    }
    public class DVPinCodes
    {
        [XmlElement(Order = 1, ElementName = "MostRecentDVPin")]
        public DVPinCode DVPinCode = new DVPinCode();
        [XmlElement(Order = 2, ElementName = "DVPinCode")]
        public List<DVPinCode> DVPinCodeList = new List<DVPinCode>();
    }
    public class DVPinCode
    {
        [XmlAttribute()]
        public E_ProductType ProductType;
        [XmlAttribute()]
        public String DVPin;
        [XmlAttribute()]
        public String OrderID;
        [XmlAttribute()]
        public String SSN;
        [XmlAttribute()]
        public String ProviderName;
    }
    public class Authentication
    {
        [XmlAttribute()]
        public String Username;
        [XmlAttribute()]
        public String Password;
    }
    public class DRIVEResponse
    {
        [XmlAttribute()]
        public String ResponseDateTime;
        [XmlElement(Order = 1, ElementName = "Authentication")]
        public Authentication Authentication = new Authentication();
        [XmlElement(Order = 2, ElementName = "Loan")]
        public List<Loan> LoanList = new List<Loan>();
        [XmlElement(Order = 3, ElementName="Errors")]
        public RootErrors Errors = new RootErrors();
    }
}

