﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Conversions.Drive.Request;
using LendersOffice.Conversions.Drive.Response;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using DataAccess;
using System.Net;
using System.Web;
using System.Text.RegularExpressions;
using LendersOffice.Constants;
using System.Xml.Linq;

namespace LendersOffice.Conversions.Drive
{
    /// <summary>
    /// Sends out a DRIVE request. Gets a response back.
    /// Refer to \\megatron\LendersOffice\Integration\CBC\DRIVE for docs.
    /// 
    /// Two sets of credentials, CBC and DRIVE
    /// CBC for all of LendingQB
    /// DRIVE for a single lender
    /// </summary>
    public static class DriveServer
    {
        private const string LOG_CONTEXT = "DRIVE";
        private const string DRIVE_BETA_SERVER_URL = "https://staging.dataverify.com/dvweb/xml/xmlgw.aspx";
        private const string DRIVE_PRODUCTION_SERVER_URL = "https://gateway.dataverify.com/dvweb/xml/xmlgw.aspx";

        public static DRIVERequest CreateRequest(string username, string password, Guid sLId)
        {
            var provider = new DriveRequestProvider(username, password, sLId);

            return provider.CreateRequest();
        }

        public static DRIVEResponse Submit(DRIVERequest request)
        {
            string throwaway;
            return DriveServer.Submit(request, out throwaway);
        }

        public static byte[] SerializeDriveRequestToBytes(DRIVERequest request)
        {
            // Serialize to XML
            XmlSerializer serializer = new XmlSerializer(typeof(DRIVERequest));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            byte[] bytes = null;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                using (XmlTextWriter tw = new XmlTextWriter(stream, System.Text.Encoding.ASCII)) // DRIVE doesn't do UTF8 at the moment
                {
                    serializer.Serialize(tw, request, ns);
                }
                bytes = stream.ToArray();
            }

            return bytes;
        }

        public static DRIVEResponse Submit(DRIVERequest request, out string responseXml)
        {
            byte[] bytes = SerializeDriveRequestToBytes(request); ;
            string requestString = System.Text.Encoding.ASCII.GetString(bytes);
            requestString = Regex.Replace(requestString, " Password=\"[^ ]+\"", " Password=\"******\""); // Mask out the password. Use nongreedy matching.
            Tools.LogInfo("DRIVE Request:" + Environment.NewLine + requestString);

            // Submit to DRIVE
            string driveUrl = DRIVE_PRODUCTION_SERVER_URL;

            string queryString = "";
            queryString += "?logid=" + HttpUtility.UrlEncode(Constants.ConstStage.CBCDriveLoginId);
            queryString += "&pw=" + HttpUtility.UrlEncode(Constants.ConstStage.CBCDriveLoginPassword);
            queryString += "&command=" + HttpUtility.UrlEncode("apiordretpost");
            queryString += "&options=" + "REVL%3DY+REVF%3DX2+TEXT%3DN+PA%3DXM+ORD%3DMS+SENDDATA%3DN+FORMAT%3DN+PS%3DN+REP%3DDATAV";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(driveUrl + queryString);
            webRequest.Method = "POST";

            webRequest.ContentType = "text/xml";
            webRequest.ContentLength = bytes.Length;
            webRequest.Timeout = ConstStage.DataVerifyDRIVETimeOutInMilliseconds;

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            // Get the response
            StringBuilder sb = new StringBuilder();
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    byte[] buffer = new byte[60000];
                    int size = stream.Read(buffer, 0, buffer.Length);
                    while (size > 0)
                    {
                        sb.Append(System.Text.Encoding.ASCII.GetString(buffer, 0, size));
                        size = stream.Read(buffer, 0, buffer.Length);
                    }
                }
            }
            responseXml = sb.ToString();
            // Convert to object
            DRIVEResponse response = null;

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.XmlResolver = null;
#if LQB_NET45
            xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;
#else
            xmlReaderSettings.ProhibitDtd = false;
#endif
            // parsing for logging purposes only
            try
            {
                XElement doc = XElement.Parse(responseXml);
                if (doc.Descendants("Loan").Descendants("Document").ToList().Count != 0)
                {
                    doc.Descendants("Loan").Descendants("Document").First().Value = "Redacted to save space. Original size: " + doc.Descendants("Loan").Descendants("Document").First().Value.Length;
                }

                Tools.LogInfo("DRIVE Response:" + Environment.NewLine + doc.ToString());
            }
            catch (XmlException exc)
            {
                Tools.LogErrorWithCriticalTracking(@"DRIVE returned an unexpected response. Review the DRIVE response log in PB. The LQB integration password may need to be reset. Refer to the DataVerify DRIVE wiki page for pw reset procedure. " + responseXml, exc);
                return null;
            }

            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(responseXml), xmlReaderSettings))
                {
                    // Deserialize to object
                    var responseProvider = new DriveResponseProvider();
                    response = responseProvider.CreateResponse(reader);
                }
            }
            catch (InvalidOperationException) // Could not read the XML
            {
                // shouldn't reach here.                
                response = null;
            }
            
            return response;
        }
    }
}