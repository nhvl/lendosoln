namespace LendersOffice.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using global::DocMagic.DsiDocRequest;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.DocMagicLib;
    using LendersOffice.ObjLib.Hmda;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using Mismo.Closing2_6;
    using XmlSerializableCommon;

    /// <summary>
    /// Manages a set of parameters for a websheet request.
    /// </summary>
    public class WebsheetRequestParameters
    {
        /// <summary>
        /// The LendingQB loan number.
        /// </summary>
        private Guid loanNumber;

        /// <summary>
        /// The websheet number for the loan.
        /// </summary>
        private string websheetNumber;

        /// <summary>
        /// The websheet request type.
        /// </summary>
        private E_WebsheetRequestType requestType;

        /// <summary>
        /// Indicates whether the request should bypass field security.
        /// </summary>
        private bool bypassFieldSecurityCheck;

        /// <summary>
        /// Indicates whether custom export options are being used as opposed to those
        /// stored in the loan.
        /// </summary>
        private bool useCustomExportOptions;

        /// <summary>
        /// The custom export source.
        /// </summary>
        private E_SettlementChargesExportSource exportSource;

        /// <summary>
        /// A custom export option that indicates whether to include GFE data for comparison
        /// to the settlement data.
        /// </summary>
        private bool includeGfeDataForDocMagicComparison;

        /// <summary>
        /// Indicates what TRID disclosure type to export.
        /// </summary>
        private E_DocumentsPackageType documentPackageType;

        /// <summary>
        /// Populates the parameters object with default values.
        /// </summary>
        /// <param name="loanNumber">The LendingQB loan number.</param>
        /// <param name="websheetNumber">The websheet number.</param>
        public WebsheetRequestParameters(Guid loanNumber, string websheetNumber)
        {
            this.loanNumber = loanNumber;
            this.websheetNumber = websheetNumber;
            this.requestType = E_WebsheetRequestType.Undefined;
            this.bypassFieldSecurityCheck = false;
            this.useCustomExportOptions = false;
            this.exportSource = E_SettlementChargesExportSource.GFE;
            this.includeGfeDataForDocMagicComparison = false;
            this.documentPackageType = E_DocumentsPackageType.Initial;
        }

        /// <summary>
        /// Gets or sets the LendingQB loan number.
        /// </summary>
        public Guid LoanNumber 
        { 
            get 
            { 
                return loanNumber; 
            }
            
            set 
            {
                loanNumber = value; 
            }
        }

        /// <summary>
        /// Gets or sets the websheet number.
        /// </summary>
        public string WebsheetNumber 
        {
            get
            {
                return websheetNumber;
            }
            
            set
            {
                websheetNumber = value;
            }
        }

        /// <summary>
        /// Gets or sets the websheet request type.
        /// </summary>
        public E_WebsheetRequestType RequestType
        {
            get
            {
                return requestType;
            }
            set
            {
                requestType = value;
            }
        }

        /// <summary>
        /// Gets or gets the field security bypass indicator.
        /// </summary>
        public bool ByPassFieldSecurityCheck
        {
            get
            {
                return bypassFieldSecurityCheck;
            }
            set
            {
                bypassFieldSecurityCheck = value;
            }
        }

        /// <summary>
        /// Gets or sets the indicator for whether custom export options will be used.
        /// </summary>
        public bool UseCustomExportOptions
        {
            get
            {
                return useCustomExportOptions;
            }
            set
            {
                useCustomExportOptions = value;
            }
        }

        /// <summary>
        /// Gets or sets the custom export source.
        /// </summary>
        public E_SettlementChargesExportSource ExportSource
        {
            get
            {
                return exportSource;
            }
            set
            {
                exportSource = value;
            }
        }

        /// <summary>
        /// Gets or sets the indicator for whether to include GFE data.
        /// </summary>
        public bool IncludeGfeDataForDocMagicComparison
        {
            get
            {
                return includeGfeDataForDocMagicComparison;
            }
            set
            {
                includeGfeDataForDocMagicComparison = value;
            }
        }

        /// <summary>
        /// Gets or sets the TRID disclosure type to be exported.
        /// </summary>
        public E_DocumentsPackageType DocumentPackageType
        {
            get
            {
                return documentPackageType;
            }
            set
            {
                documentPackageType = value;
            }
        }
    }

    /// <summary>
    /// Refer to \\megatron\LendersOffice\Integration\DOCMagic for docs.
    /// If the DocMagic object does not contain the required fields, you may need to add them in LendingQBInfrequentChangeLib.
    /// </summary>
    public static class DocMagicMismoRequest
    {
        public static DsiDocumentServerRequest CreateLaunchAppletRequest(string customerId, string userName, string password, Guid sLId, string websheetNumber)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest(sLId.ToString(), customerId, userName, password);
            dsiDocumentServerRequest.AppletRequest.WebsheetNumber = CleanupWebsheetNumber(websheetNumber);
            return dsiDocumentServerRequest;
        }

        public static DsiDocumentServerRequest CreatePackageTypeListRequest(string customerId, string userName, string password)
        {
            // MPTODO: figure out what name to give the transactionId
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest("Authorized Package Types", customerId, userName, password);
            dsiDocumentServerRequest.InitRequest = CreateInitRequest(E_InitRequestType.PackageType);
            return dsiDocumentServerRequest;
        }

        public static DsiDocumentServerRequest CreateAddPlanToLenderRequest(string customerId, string userName, string password, string planCode)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest("Plan Request: Add plan to lender", customerId, userName, password);
            dsiDocumentServerRequest.PlanRequestList.Add(CreatePlanRequest(planCode, E_PlanRequestType.AddPlanToLender, false));
            return dsiDocumentServerRequest;
        }

        public static DsiDocumentServerRequest CreateLoadCurrentPlansRequest(string customerId, string userName, string password)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest("Plan Request: Load current plans", customerId, userName, password);
            dsiDocumentServerRequest.PlanRequestList.Add(CreatePlanRequest("", E_PlanRequestType.LoadCurrentPlans, false));
            return dsiDocumentServerRequest;
        }

        public static DsiDocumentServerRequest CreateGetDefaultValuesPlanRequest(string customerId, string userName, string password, string planCode)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest("Plan Request: Get default values", customerId, userName, password);
            dsiDocumentServerRequest.PlanRequestList.Add(CreatePlanRequest(planCode, E_PlanRequestType.GetDefaultValues, true));
            return dsiDocumentServerRequest;
        }

        public static DsiDocumentServerRequest CreateFormListRequest(string customerId, string userName, string password, string websheetNumber)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest("Form list request", customerId, userName, password);
            dsiDocumentServerRequest.FormRequestList.Add(CreateFormRequest(websheetNumber));
            return dsiDocumentServerRequest;
        }

        /// <summary>
        /// Overload for CreateSaveRequest() that uses a default value for the TRID package type. Will typically be
        /// used for Legacy loans.
        /// </summary>
        /// <param name="customerId">The customer's ID.</param>
        /// <param name="userName">The customer's username.</param>
        /// <param name="password">The customer's password</param>
        /// <param name="sLId">The loan ID.</param>
        /// <param name="websheetNumber">The websheet number.</param>
        /// <returns>A <see cref="DsiDocumentServerRequest"/> object.</returns>
        public static DsiDocumentServerRequest CreateSaveRequest(string customerId, string userName, string password, Guid sLId, string websheetNumber)
        {
            return CreateSaveRequest(customerId, userName, password, sLId, websheetNumber, E_DocumentsPackageType.Initial);
        }

        /// <summary>
        /// Performs loan export
        /// </summary>
        /// <returns></returns>
        public static DsiDocumentServerRequest CreateSaveRequest(string customerId, string userName, string password, Guid sLId, string websheetNumber, E_DocumentsPackageType packageType)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest(sLId.ToString(), customerId, userName, password);

            var parameters = new WebsheetRequestParameters(sLId, websheetNumber);
            parameters.RequestType = E_WebsheetRequestType.Save;
            parameters.ByPassFieldSecurityCheck = false;
            parameters.DocumentPackageType = packageType;

            List<string> feeDiscrepancies;
            dsiDocumentServerRequest.WebsheetRequestList.Add(CreateWebsheetRequest(parameters, out feeDiscrepancies));
            return dsiDocumentServerRequest;
        }

        /// <summary>
        /// Performs loan export.
        /// </summary>
        /// <returns></returns>
        public static DsiDocumentServerRequest CreateAuditRequest(string customerId, string userName, string password, Guid sLId, string websheetNumber, E_AuditPackageType packageType, out List<string> feeDiscrepancies)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest(sLId.ToString(), customerId, userName, password);

            var parameters = new WebsheetRequestParameters(sLId, websheetNumber);
            dsiDocumentServerRequest.WebsheetRequestList.Add(CreateAuditWebsheetRequest(parameters, packageType, out feeDiscrepancies));
            return dsiDocumentServerRequest;
        }

        /// <summary>
        /// Tell DocMagic to generate a document set.
        /// 
        /// Performs loan export.
        /// </summary>
        /// <returns></returns>
        public static DsiDocumentServerRequest CreateProcessRequest(string customerId, string userName, string password, Guid sLId, string websheetNumber, Process processOptions)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = CreateDsiDocumentServerRequest(sLId.ToString(), customerId, userName, password);
            dsiDocumentServerRequest.WebsheetRequestList.Add(CreateProcessWebsheetRequest(sLId, websheetNumber, processOptions));
            return dsiDocumentServerRequest;
        }

        /// <summary>
        /// Create a DsiDocumentServerRequest.
        /// </summary>
        /// <param name="transactionId">Usually sLId, though it could just be descriptive.</param>
        /// <returns></returns>
        private static DsiDocumentServerRequest CreateDsiDocumentServerRequest(string transactionId, string customerId, string userName, string password)
        {
            DsiDocumentServerRequest dsiDocumentServerRequest = new DsiDocumentServerRequest();
            dsiDocumentServerRequest.RequestOriginator = E_DsiDocumentServerRequestRequestOriginator.ThirdPartyServer;

            Guid brokerId = ((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal).BrokerId;
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            if (brokerDB.IsEnablePTMDocMagicPartnerBilling)
            {
                dsiDocumentServerRequest.PartnerInformation = CreatePartnerInformation();
            }

            dsiDocumentServerRequest.CustomerInformation = CreateCustomerInformation(customerId, userName, password);
            dsiDocumentServerRequest.DataSource = CreateDataSource(transactionId);

            return dsiDocumentServerRequest;
        }

        private static DocMagic.Common.PartnerInformation CreatePartnerInformation()
        {
            DocMagic.Common.PartnerInformation partnerInformation = new DocMagic.Common.PartnerInformation();

            partnerInformation.PartnerId = "lendingqb"; // HARDCODE
            partnerInformation.Password = "4LQBDocs"; // HARDCODE

            return partnerInformation;
        }

        private static DocMagic.Common.DataSource CreateDataSource(string transactionId)
        {
            DocMagic.Common.DataSource dataSource = new DocMagic.Common.DataSource();
            dataSource.DsName = "LENDERSOFFICE"; // HARDCODE
            dataSource.TransactionId = transactionId;
            return dataSource;

        }
        private static DocMagic.Common.CustomerInformation CreateCustomerInformation(string customerId, string userName, string password)
        {
            DocMagic.Common.CustomerInformation customerInformation = new DocMagic.Common.CustomerInformation();

            customerInformation.CustomerId = customerId;
            customerInformation.UserName = userName;
            customerInformation.Password = password;

            return customerInformation;
        }

        private static InitRequest CreateInitRequest(E_InitRequestType type)
        {
            InitRequest initRequest = new InitRequest();
            initRequest.type = type;

            return initRequest;
        }

        private static PlanRequest CreatePlanRequest(string planCode, E_PlanRequestType type, bool includePlanData)
        {
            PlanRequest planRequest = new PlanRequest();
            planRequest.Type = type;
            planRequest.IncludePlanData = includePlanData ? E_YesNo.Yes : E_YesNo.No;
            planRequest.PlanCodeList.Add(planCode);
            return planRequest;
        }

        private static FormRequest CreateFormRequest(string websheetNumber)
        {
            FormRequest formRequest = new FormRequest();
            formRequest.Type = E_FormRequestType.List;
            formRequest.PackageType = E_FormRequestPackageType.FormList;
            formRequest.WebsheetNumber = websheetNumber;
            return formRequest;
        }

        /// <summary>
        /// Performs loan export.
        /// </summary>
        /// <param name="sLId"></param>
        /// <param name="websheetNumber"></param>
        /// <param name="packageType"></param>
        /// <returns></returns>
        private static WebsheetRequest CreateAuditWebsheetRequest(WebsheetRequestParameters parameters, E_AuditPackageType packageType, out List<string> feeDiscrepancies)
        {
            E_SettlementChargesExportSource exportSource;
            bool includeGfeDataForDocMagicComparison;
            bool useCustomExportSource = Tools.GetDocMagicExportOptionsForAudit(packageType, out exportSource, out includeGfeDataForDocMagicComparison);

            parameters.RequestType = E_WebsheetRequestType.Audit;
            parameters.ByPassFieldSecurityCheck = true;
            parameters.UseCustomExportOptions = useCustomExportSource;
            parameters.ExportSource = exportSource;
            parameters.IncludeGfeDataForDocMagicComparison = includeGfeDataForDocMagicComparison;

            WebsheetRequest websheetRequest = CreateWebsheetRequest(parameters, out feeDiscrepancies);
            websheetRequest.Audit = CreateAudit(packageType);
            
            return websheetRequest;
        }

        private static DocMagic.DsiDocRequest.Audit CreateAudit(E_AuditPackageType packageType)
        {
            DocMagic.DsiDocRequest.Audit audit = new DocMagic.DsiDocRequest.Audit();
            audit.PackageType = packageType;
            return audit;
        }

        private static CPageData GetPageData(Guid loanid)
        {
            CPageData dataLoan = new CPageData(loanid, "DocMagicMismoExporter",
                CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(DocMagicMismoRequest)).Union(
                CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release
                ));
            dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            return dataLoan;
        }

        /// <summary>
        /// Performs loan export.
        /// </summary>
        /// <param name="sLId"></param>
        /// <param name="websheetNumber"></param>
        /// <param name="processOptions"></param>
        /// <returns></returns>
        private static WebsheetRequest CreateProcessWebsheetRequest(Guid sLId, string websheetNumber, Process processOptions)
        {
            E_SettlementChargesExportSource exportSource;
            bool includeGfeDataForDocMagicComparison;
            bool useCustomExportSource = Tools.GetDocMagicExportOptionsForProcess(processOptions.PackageType, out exportSource, out includeGfeDataForDocMagicComparison);

            var parameters = new WebsheetRequestParameters(sLId, websheetNumber);
            parameters.RequestType = E_WebsheetRequestType.Process;
            parameters.ByPassFieldSecurityCheck = true;
            parameters.UseCustomExportOptions = useCustomExportSource;
            parameters.ExportSource = exportSource;
            parameters.IncludeGfeDataForDocMagicComparison = includeGfeDataForDocMagicComparison;

            List<string> feeDiscrepancies;
            WebsheetRequest websheetRequest = CreateWebsheetRequest(parameters, out feeDiscrepancies);
            websheetRequest.Process = processOptions;
            
            return websheetRequest;
        }

        /// <summary>
        /// This will create a websheet request, exporting the GFE or settlement charges as defined in the loan file.
        /// 
        /// Performs loan export.
        /// </summary>
        /// <returns></returns>
        private static WebsheetRequest CreateWebsheetRequest(WebsheetRequestParameters parameters, out List<string> feeDiscrepancies)
        {
            WebsheetRequest websheetRequest = new WebsheetRequest();
            websheetRequest.WebsheetNumber = CleanupWebsheetNumber(parameters.WebsheetNumber);
            websheetRequest.Type = parameters.RequestType;

            // Really not a fan of hitting the loan twice here
            DocMagicMismoExporter exporter = new DocMagicMismoExporter();
            websheetRequest.MismoClosing.Loan = exporter.CreateMismoLoan(parameters, out feeDiscrepancies);

            /// 10/15/2015 BS - Case 228678 A list of fees to exclude from the payload
            List<Guid> ignoreList = new List<Guid>();
            ignoreList = exporter.GetFeeIgnoreList();

            CPageData dataLoan = GetPageData(parameters.LoanNumber);
            dataLoan.InitLoad();

            CPageData dataLoanWithGfeArchiveApplied = GetPageData(parameters.LoanNumber);
            dataLoanWithGfeArchiveApplied.InitLoad();

            bool exportTridData = dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            bool use2015DataLayer = dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;

            if (dataLoanWithGfeArchiveApplied.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                if (use2015DataLayer)
                {
                    if (parameters.DocumentPackageType == E_DocumentsPackageType.Initial && dataLoanWithGfeArchiveApplied.sLastDisclosedClosingCostArchive != null)
                    {
                        dataLoanWithGfeArchiveApplied.ApplyClosingCostArchive(dataLoanWithGfeArchiveApplied.sLastDisclosedClosingCostArchive);
                    }
                }
                else
                {
                    if (dataLoanWithGfeArchiveApplied.LastDisclosedGFEArchive != null)
                    {
                        dataLoanWithGfeArchiveApplied.ApplyGFEArchiveForExport(dataLoanWithGfeArchiveApplied.LastDisclosedGFEArchive);
                    }

                    if (dataLoanWithGfeArchiveApplied.CoCArchives.Count() > 0)
                    {
                        dataLoanWithGfeArchiveApplied.ApplyCoCArchive(dataLoanWithGfeArchiveApplied.CoCArchives.First());
                    }
                }
            }
            
            if (parameters.UseCustomExportOptions)
            {
                dataLoan.sSettlementChargesExportSource = parameters.ExportSource;
                dataLoan.sIncludeGfeDataForDocMagicComparison = parameters.IncludeGfeDataForDocMagicComparison;
            }

            dataLoan.ByPassFieldSecurityCheck = parameters.ByPassFieldSecurityCheck;
            dataLoanWithGfeArchiveApplied.ByPassFieldSecurityCheck = parameters.ByPassFieldSecurityCheck;
            
            websheetRequest.MismoClosing.PlanCode = dataLoan.sDocMagicPlanCodeNm; // OPM 13035

            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                if (dataApp.aBIsValidNameSsn)
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    websheetRequest.Losdata.BorrowerDetailList.Add(CreateLosBorrowerDetail(dataApp));
                    websheetRequest.Losdata.TaxTranscriptList.Add(CreateTaxTranscript(dataApp, websheetRequest.Losdata.PartyList));
                }
                if (dataApp.aCIsValidNameSsn)
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    websheetRequest.Losdata.BorrowerDetailList.Add(CreateLosBorrowerDetail(dataApp));

                    if (dataApp.aIs4506TFiledTaxesSeparately)
                    {
                        websheetRequest.Losdata.TaxTranscriptList.Add(CreateTaxTranscript(dataApp, websheetRequest.Losdata.PartyList));
                    }
                }
            }

            PopulatePartyList(websheetRequest.Losdata.PartyList, dataLoan);

            // 7/14/2011 dd - Based from Request for DocMagic
            switch (dataLoan.sBranchChannelT)
            {
                case E_BranchChannelT.Blank:
                    break;
                case E_BranchChannelT.Retail:
                    websheetRequest.Losdata.OriginationChannel.Type = E_OriginationChannelType.Retail;
                    break;
                case E_BranchChannelT.Wholesale:
                    websheetRequest.Losdata.OriginationChannel.Type = E_OriginationChannelType.Wholesale;
                    break;
                case E_BranchChannelT.Correspondent:
                    websheetRequest.Losdata.OriginationChannel.Type = E_OriginationChannelType.Correspondent;
                    break;
                case E_BranchChannelT.Broker:
                    websheetRequest.Losdata.OriginationChannel.Type = E_OriginationChannelType.BrokeredOut;
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sBranchChannelT);
            }

            websheetRequest.Losdata.General.RateLockDate = dataLoan.sDocMagicRateLockD_rep;
            websheetRequest.Losdata.General.LoanProceedsToType = ToMismo(dataLoan.sLoanProceedsToT);
            websheetRequest.Losdata.General.AgencyCaseIdentifierAssignmentDate = dataLoan.sCaseAssignmentD_rep;
            websheetRequest.Losdata.General.PreDiscountedInterestRatePercent = dataLoan.sQMParR_rep;
            websheetRequest.Losdata.General.InterestRateDeterminationDate = dataLoan.sRLckdD_rep;
            websheetRequest.Losdata.General.LoanCommitmentExpirationDate = dataLoan.sCommitExpD_rep;
            websheetRequest.Losdata.General.LoanApprovalExpirationDate = dataLoan.sAppExpD_rep;

            // Support for anti-steering disclosure. OPM 74043 - m.p.
            websheetRequest.Losdata.DocumentSpecificContentList.Add(CreateDocumentSpecificContent(dataLoan));

            if (use2015DataLayer)
            {
                var lastDisclosedCoC = dataLoan.sLastDisclosedClosingCostCoCArchive;
                var loanFeeList = websheetRequest.MismoClosing.Loan?.Application?.RespaFeeList ?? new List<Mismo.Closing2_6.RespaFee>();
                PopulateChangeOfCircumstanceData_2015DataLayer(lastDisclosedCoC, websheetRequest.Losdata.RespaFeeList, loanFeeList, ignoreList);
            }
            else
            {
                // Change of circumstance info. OPM 75461 - m.p.
                PopulateChangeOfCircumstanceData(dataLoan, dataLoanWithGfeArchiveApplied, websheetRequest.Losdata.RespaFeeList, websheetRequest.Losdata.CompensationList);
            }

            websheetRequest.Losdata.DisclosureDetail = CreateDisclosureDetail(dataLoan, dataLoanWithGfeArchiveApplied);
            websheetRequest.Losdata.ManufacturedHome = CreateManufacturedHome(dataLoan);

            PopulateCreditDenialList(websheetRequest.Losdata.CreditDenialList, dataLoan);

            websheetRequest.Losdata.Underwriting.AbilityToRepay = CreateAbilityToRepay(dataLoan);

            // Prior lien. OPM 84022 - m.p.
            websheetRequest.Losdata.PriorLienList.Add(CreatePriorLien(dataLoan));
            // more prior liens opm 82162
            websheetRequest.Losdata.PriorLienList.Add(CreatePriorLien(dataLoan.sLien1PriorityBefore, dataLoan.sLien1AmtBefore_rep, dataLoan.sLienholder1NmBefore,
                dataLoan.sLien1AmtAfter_rep, dataLoan.sLienholder1NmAfter));
            websheetRequest.Losdata.PriorLienList.Add(CreatePriorLien(dataLoan.sLien2PriorityBefore, dataLoan.sLien2AmtBefore_rep, dataLoan.sLienholder2NmBefore,
                dataLoan.sLien2AmtAfter_rep, dataLoan.sLienholder2NmAfter));
            websheetRequest.Losdata.PriorLienList.Add(CreatePriorLien(dataLoan.sLien3PriorityBefore, dataLoan.sLien3AmtBefore_rep, dataLoan.sLienholder3NmBefore,
                dataLoan.sLien3AmtAfter_rep, dataLoan.sLienholder3NmAfter));

            // VA Addendum OPM 137655
                 if (dataLoan.sFHAPurposeIsPurchaseExistHome) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.PurchaseExistingHomePreviouslyOccupied;
            else if (dataLoan.sFHAPurposeIsFinanceImprovement) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.FinanceImprovementsToExistingProperty;
            else if (dataLoan.sFHAPurposeIsRefinance) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.Refinance;
            else if (dataLoan.sFHAPurposeIsPurchaseNewCondo) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.PurchaseNewCondominiumUnit;
            else if (dataLoan.sFHAPurposeIsPurchaseExistCondo) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.PurchaseExistingCondominiumUnit;
            else if (dataLoan.sFHAPurposeIsPurchaseNewHome) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.PurchaseExistingHomeNotPreviouslyOccupied;
            else if (dataLoan.sFHAPurposeIsConstructHome) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.ConstructHome;
            else if (dataLoan.sFHAPurposeIsFinanceCoopPurchase) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.FinanceCooperativePurchase;
            else if (dataLoan.sFHAPurposeIsPurchaseManufacturedHome) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.PurchasePermanentlySitedManufacturedHome;
            else if (dataLoan.sFHAPurposeIsManufacturedHomeAndLot) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.PurchasePermanentlySitedManufacturedHomeAndLot;
            else if (dataLoan.sFHAPurposeIsRefiManufacturedHomeToBuyLot) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.RefinancePermanentlySitedManufacturedHomeToBuyLot;
            else if (dataLoan.sFHAPurposeIsRefiManufacturedHomeOrLotLoan) 
                     websheetRequest.Losdata.VALoan.FHAVALoanPurposeType = E_FHAVALoanPurposeType.RefinancePermanentlySitedManufacturedHomeLotLoan;
            
            // Custom data.
            // Purchase contract date. OPM 92912 - mp
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("ContractSalesDate", dataLoan.sPurchaseContractDate_rep));
            if (dataLoan.sLT == E_sLT.VA)
            {
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("VALoanComparisonClosingCosts", dataLoan.aVaTotalClosingCost_rep));
                
                //OPM 90290: VA Form Fields - 1/28/2013 PA
                var appData = dataLoan.GetAppData(0);
                if (appData.aVaOwnOtherVaLoanT == E_aVaOwnOtherVaLoanT.NA)
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("NeverObtainedVAGuaranteedLoanIndicator", "Y"));
                if (appData.aVaApplyOneTimeRestorationTri == E_TriState.Yes)
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("OneTimeOnlyRestorationOfEntitlementLoanIndicator", "Y"));
                else if (appData.aVaApplyOneTimeRestorationTri == E_TriState.No)
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("OneTimeOnlyRestorationOfEntitlementLoanIndicator", "N"));
                
                DateTime dtm;
                if(DateTime.TryParse(appData.aVaApplyOneTimeRestorationDateOfLoan, out dtm)) websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("OneTimeOnlyRestorationOfEntitlementLoanDate", dtm.ToShortDateString()));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("OneTimeOnlyRestorationOfEntitlementLoanStreetAddress", appData.aVaApplyOneTimeRestorationStAddr));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("OneTimeOnlyRestorationOfEntitlementLoanCity", appData.aVaApplyOneTimeRestorationCityState));

                if (appData.aVaApplyRegularRefiCashoutTri == E_TriState.Yes)
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("RestorationOfEntitlementToObtainRegularRefinanceCashOutIndicator", "Y"));
                else if (appData.aVaApplyRegularRefiCashoutTri == E_TriState.No)
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("RestorationOfEntitlementToObtainRegularRefinanceCashOutIndicator", "N"));
                if (DateTime.TryParse(appData.aVaApplyRegularRefiCashoutDateOfLoan, out dtm)) websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("RestorationOfEntitlementToObtainRegularRefinanceCashOutDate", dtm.ToShortDateString()));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("RestorationOfEntitlementToObtainRegularRefinanceCashOutStreetAddress", appData.aVaApplyRegularRefiCashoutStAddr));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("RestorationOfEntitlementToObtainRegularRefinanceCashOutCity", appData.aVaApplyRegularRefiCashoutCityState));

                if (appData.aVaApplyRefiNoCashoutTri == E_TriState.Yes)
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("InterestRateReductionRefinanceLoanIndicator", "Y"));
                else if (appData.aVaApplyRefiNoCashoutTri == E_TriState.No)
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("InterestRateReductionRefinanceLoanIndicator", "N"));
                if (DateTime.TryParse(appData.aVaApplyRefiNoCashoutDateOfLoan, out dtm)) websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("InterestRateReductionRefinanceLoanDate", dtm.ToShortDateString()));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("InterestRateReductionRefinanceLoanStreetAddress", appData.aVaApplyRefiNoCashoutStAddr));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("InterestRateReductionRefinanceLoanCity", appData.aVaApplyRefiNoCashoutCityState));
            }

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementMortgageeRelationshipIndicator", dataLoan.sFHAMortgageHaveFinanceInterestTri == E_TriState.Yes ? "Y" : dataLoan.sFHAMortgageHaveFinanceInterestTri == E_TriState.No ? "N" : null));

            if (dataLoan.sFHAApprovedSubj)
            {
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementMortgageApprovalDate", dataLoan.sFHAApprovedD_rep));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementMortgageApprovalExpirationDate", dataLoan.sFHAApprovedExpiresD_rep));
            }

            if (dataLoan.sFHAApprovedModified)
            {
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementModifiedLoanAmount", dataLoan.sFHAModifiedFinalLAmt_rep));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementModifiedInterestRatePercent", dataLoan.sFHAModifiedNoteIR_rep));

                int directEndorsementModifiedMaturityTermMonths = (dataLoan.sFHAModifiedTermInYr * 12) + dataLoan.sFHAModifiedTermInMonths;
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementModifiedMaturityTermMonths", dataLoan.m_convertLos.ToCountString(directEndorsementModifiedMaturityTermMonths)));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementModifiedMonthlyPaymentAmount", dataLoan.sFHAModifiedMonthlyPmt_rep));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementModifiedMIUpfrontPremiumAmount", dataLoan.sFHAModifiedFfUfmip1003_rep));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementModifiedMIMonthlyPremiumAmount", dataLoan.sFHAModifiedProMIns_rep));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementModifiedMIMonthlyPremiumTermMonths", dataLoan.sFHAModifiedProMInsMon_rep));
            }

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementProposedConstructionHUDComplianceIndicator", dataLoan.sFHACondIfProposedConstBuilderCertified ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementNewConstructionHUDComplianceIndicator", dataLoan.sFHACondIfNewConstLenderCertified ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementBuildersWarrantyRequiredIndicator", dataLoan.sFHACondBuilderWarrantyRequired ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementProperty10YearWarrantyIndicator", dataLoan.sFHACond10YrsWarranty ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementOwnerOccupancyRequiredIndicator", dataLoan.sFHACondOwnerOccNotRequired ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("HighLTVMortgageForNonOccupantMortgagorInMilitary", dataLoan.sFHACondMortgageHighLtvForNonOccupant ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementOtherConditionDescription", dataLoan.sFHACondOtherDesc));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DirectEndorsementMortgageeRelationshipIndicator", dataLoan.sFHAMortgageHaveFinanceInterestTri == E_TriState.Yes ? "Y" : "N" ));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("ContractSalesDate", dataLoan.sPurchaseContractDate_rep));

            // TX Mortgage Broker: OPM 112247
            if (dataLoan.sTexasDiscWillSubmitToLender)
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DutiesAndNatureOfRelationship", "WillSubmitYourLoanApplication"));
            if (dataLoan.sTexasDiscAsIndependentContractor)
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DutiesAndNatureOfRelationship", "WillMakeYourLoanOurselves"));
            if (dataLoan.sTexasDiscWillActAsFollows)
            {
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("DutiesAndNatureOfRelationship", "WillBeActingAsFollows"));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("WillBeActingAsFollowsDescription", dataLoan.sTexasDiscWillActAsFollowsDesc));
            }
            if (dataLoan.sTexasDiscCompensationIncluded)
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("HowWeWillBeCompensated", "RetailPriceWeOfferYou"));
            if (dataLoan.sTexasDiscChargeVaried)
            {
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("HowWeWillBeCompensated", "PricingForYourLoanIsBasedUpon"));
                websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("PricingForYourLoanIsBasedUponDescription", dataLoan.sTexasDiscChargeVariedDesc));
            }

            // 10/27/2014 tj - 174522
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("AdditionalFinancingIsANewLoan", dataLoan.sIsOFinNew ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("AdditionalFinancingIsALineOfCredit", dataLoan.sIsOFinCreditLineInDrawPeriod ? "Y" : "N"));

            #region  Add the custom fields to custom data.
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField1Desc", dataLoan.sCustomField1Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField1D", dataLoan.sCustomField1D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField1Money", dataLoan.sCustomField1Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField1Pc", dataLoan.sCustomField1Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField1Bit", dataLoan.sCustomField1Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField1Notes", dataLoan.sCustomField1Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField2Desc", dataLoan.sCustomField2Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField2D", dataLoan.sCustomField2D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField2Money", dataLoan.sCustomField2Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField2Pc", dataLoan.sCustomField2Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField2Bit", dataLoan.sCustomField2Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField2Notes", dataLoan.sCustomField2Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField3Desc", dataLoan.sCustomField3Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField3D", dataLoan.sCustomField3D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField3Money", dataLoan.sCustomField3Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField3Pc", dataLoan.sCustomField3Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField3Bit", dataLoan.sCustomField3Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField3Notes", dataLoan.sCustomField3Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField4Desc", dataLoan.sCustomField4Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField4D", dataLoan.sCustomField4D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField4Money", dataLoan.sCustomField4Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField4Pc", dataLoan.sCustomField4Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField4Bit", dataLoan.sCustomField4Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField4Notes", dataLoan.sCustomField4Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField5Desc", dataLoan.sCustomField5Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField5D", dataLoan.sCustomField5D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField5Money", dataLoan.sCustomField5Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField5Pc", dataLoan.sCustomField5Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField5Bit", dataLoan.sCustomField5Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField5Notes", dataLoan.sCustomField5Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField6Desc", dataLoan.sCustomField6Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField6D", dataLoan.sCustomField6D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField6Money", dataLoan.sCustomField6Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField6Pc", dataLoan.sCustomField6Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField6Bit", dataLoan.sCustomField6Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField6Notes", dataLoan.sCustomField6Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField7Desc", dataLoan.sCustomField7Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField7D", dataLoan.sCustomField7D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField7Money", dataLoan.sCustomField7Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField7Pc", dataLoan.sCustomField7Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField7Bit", dataLoan.sCustomField7Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField7Notes", dataLoan.sCustomField7Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField8Desc", dataLoan.sCustomField8Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField8D", dataLoan.sCustomField8D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField8Money", dataLoan.sCustomField8Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField8Pc", dataLoan.sCustomField8Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField8Bit", dataLoan.sCustomField8Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField8Notes", dataLoan.sCustomField8Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField9Desc", dataLoan.sCustomField9Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField9D", dataLoan.sCustomField9D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField9Money", dataLoan.sCustomField9Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField9Pc", dataLoan.sCustomField9Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField9Bit", dataLoan.sCustomField9Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField9Notes", dataLoan.sCustomField9Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField10Desc", dataLoan.sCustomField10Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField10D", dataLoan.sCustomField10D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField10Money", dataLoan.sCustomField10Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField10Pc", dataLoan.sCustomField10Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField10Bit", dataLoan.sCustomField10Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField10Notes", dataLoan.sCustomField10Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField11Desc", dataLoan.sCustomField11Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField11D", dataLoan.sCustomField11D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField11Money", dataLoan.sCustomField11Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField11Pc", dataLoan.sCustomField11Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField11Bit", dataLoan.sCustomField11Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField11Notes", dataLoan.sCustomField11Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField12Desc", dataLoan.sCustomField12Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField12D", dataLoan.sCustomField12D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField12Money", dataLoan.sCustomField12Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField12Pc", dataLoan.sCustomField12Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField12Bit", dataLoan.sCustomField12Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField12Notes", dataLoan.sCustomField12Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField13Desc", dataLoan.sCustomField13Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField13D", dataLoan.sCustomField13D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField13Money", dataLoan.sCustomField13Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField13Pc", dataLoan.sCustomField13Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField13Bit", dataLoan.sCustomField13Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField13Notes", dataLoan.sCustomField13Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField14Desc", dataLoan.sCustomField14Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField14D", dataLoan.sCustomField14D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField14Money", dataLoan.sCustomField14Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField14Pc", dataLoan.sCustomField14Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField14Bit", dataLoan.sCustomField14Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField14Notes", dataLoan.sCustomField14Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField15Desc", dataLoan.sCustomField15Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField15D", dataLoan.sCustomField15D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField15Money", dataLoan.sCustomField15Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField15Pc", dataLoan.sCustomField15Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField15Bit", dataLoan.sCustomField15Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField15Notes", dataLoan.sCustomField15Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField16Desc", dataLoan.sCustomField16Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField16D", dataLoan.sCustomField16D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField16Money", dataLoan.sCustomField16Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField16Pc", dataLoan.sCustomField16Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField16Bit", dataLoan.sCustomField16Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField16Notes", dataLoan.sCustomField16Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField17Desc", dataLoan.sCustomField17Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField17D", dataLoan.sCustomField17D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField17Money", dataLoan.sCustomField17Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField17Pc", dataLoan.sCustomField17Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField17Bit", dataLoan.sCustomField17Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField17Notes", dataLoan.sCustomField17Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField18Desc", dataLoan.sCustomField18Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField18D", dataLoan.sCustomField18D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField18Money", dataLoan.sCustomField18Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField18Pc", dataLoan.sCustomField18Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField18Bit", dataLoan.sCustomField18Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField18Notes", dataLoan.sCustomField18Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField19Desc", dataLoan.sCustomField19Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField19D", dataLoan.sCustomField19D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField19Money", dataLoan.sCustomField19Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField19Pc", dataLoan.sCustomField19Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField19Bit", dataLoan.sCustomField19Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField19Notes", dataLoan.sCustomField19Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField20Desc", dataLoan.sCustomField20Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField20D", dataLoan.sCustomField20D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField20Money", dataLoan.sCustomField20Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField20Pc", dataLoan.sCustomField20Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField20Bit", dataLoan.sCustomField20Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField20Notes", dataLoan.sCustomField20Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField21Desc", dataLoan.sCustomField21Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField21D", dataLoan.sCustomField21D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField21Money", dataLoan.sCustomField21Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField21Pc", dataLoan.sCustomField21Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField21Bit", dataLoan.sCustomField21Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField21Notes", dataLoan.sCustomField21Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField22Desc", dataLoan.sCustomField22Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField22D", dataLoan.sCustomField22D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField22Money", dataLoan.sCustomField22Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField22Pc", dataLoan.sCustomField22Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField22Bit", dataLoan.sCustomField22Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField22Notes", dataLoan.sCustomField22Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField23Desc", dataLoan.sCustomField23Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField23D", dataLoan.sCustomField23D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField23Money", dataLoan.sCustomField23Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField23Pc", dataLoan.sCustomField23Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField23Bit", dataLoan.sCustomField23Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField23Notes", dataLoan.sCustomField23Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField24Desc", dataLoan.sCustomField24Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField24D", dataLoan.sCustomField24D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField24Money", dataLoan.sCustomField24Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField24Pc", dataLoan.sCustomField24Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField24Bit", dataLoan.sCustomField24Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField24Notes", dataLoan.sCustomField24Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField25Desc", dataLoan.sCustomField25Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField25D", dataLoan.sCustomField25D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField25Money", dataLoan.sCustomField25Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField25Pc", dataLoan.sCustomField25Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField25Bit", dataLoan.sCustomField25Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField25Notes", dataLoan.sCustomField25Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField26Desc", dataLoan.sCustomField26Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField26D", dataLoan.sCustomField26D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField26Money", dataLoan.sCustomField26Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField26Pc", dataLoan.sCustomField26Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField26Bit", dataLoan.sCustomField26Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField26Notes", dataLoan.sCustomField26Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField27Desc", dataLoan.sCustomField27Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField27D", dataLoan.sCustomField27D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField27Money", dataLoan.sCustomField27Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField27Pc", dataLoan.sCustomField27Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField27Bit", dataLoan.sCustomField27Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField27Notes", dataLoan.sCustomField27Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField28Desc", dataLoan.sCustomField28Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField28D", dataLoan.sCustomField28D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField28Money", dataLoan.sCustomField28Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField28Pc", dataLoan.sCustomField28Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField28Bit", dataLoan.sCustomField28Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField28Notes", dataLoan.sCustomField28Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField29Desc", dataLoan.sCustomField29Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField29D", dataLoan.sCustomField29D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField29Money", dataLoan.sCustomField29Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField29Pc", dataLoan.sCustomField29Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField29Bit", dataLoan.sCustomField29Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField29Notes", dataLoan.sCustomField29Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField30Desc", dataLoan.sCustomField30Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField30D", dataLoan.sCustomField30D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField30Money", dataLoan.sCustomField30Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField30Pc", dataLoan.sCustomField30Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField30Bit", dataLoan.sCustomField30Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField30Notes", dataLoan.sCustomField30Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField31Desc", dataLoan.sCustomField31Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField31D", dataLoan.sCustomField31D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField31Money", dataLoan.sCustomField31Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField31Pc", dataLoan.sCustomField31Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField31Bit", dataLoan.sCustomField31Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField31Notes", dataLoan.sCustomField31Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField32Desc", dataLoan.sCustomField32Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField32D", dataLoan.sCustomField32D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField32Money", dataLoan.sCustomField32Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField32Pc", dataLoan.sCustomField32Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField32Bit", dataLoan.sCustomField32Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField32Notes", dataLoan.sCustomField32Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField33Desc", dataLoan.sCustomField33Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField33D", dataLoan.sCustomField33D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField33Money", dataLoan.sCustomField33Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField33Pc", dataLoan.sCustomField33Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField33Bit", dataLoan.sCustomField33Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField33Notes", dataLoan.sCustomField33Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField34Desc", dataLoan.sCustomField34Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField34D", dataLoan.sCustomField34D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField34Money", dataLoan.sCustomField34Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField34Pc", dataLoan.sCustomField34Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField34Bit", dataLoan.sCustomField34Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField34Notes", dataLoan.sCustomField34Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField35Desc", dataLoan.sCustomField35Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField35D", dataLoan.sCustomField35D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField35Money", dataLoan.sCustomField35Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField35Pc", dataLoan.sCustomField35Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField35Bit", dataLoan.sCustomField35Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField35Notes", dataLoan.sCustomField35Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField36Desc", dataLoan.sCustomField36Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField36D", dataLoan.sCustomField36D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField36Money", dataLoan.sCustomField36Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField36Pc", dataLoan.sCustomField36Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField36Bit", dataLoan.sCustomField36Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField36Notes", dataLoan.sCustomField36Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField37Desc", dataLoan.sCustomField37Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField37D", dataLoan.sCustomField37D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField37Money", dataLoan.sCustomField37Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField37Pc", dataLoan.sCustomField37Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField37Bit", dataLoan.sCustomField37Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField37Notes", dataLoan.sCustomField37Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField38Desc", dataLoan.sCustomField38Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField38D", dataLoan.sCustomField38D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField38Money", dataLoan.sCustomField38Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField38Pc", dataLoan.sCustomField38Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField38Bit", dataLoan.sCustomField38Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField38Notes", dataLoan.sCustomField38Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField39Desc", dataLoan.sCustomField39Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField39D", dataLoan.sCustomField39D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField39Money", dataLoan.sCustomField39Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField39Pc", dataLoan.sCustomField39Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField39Bit", dataLoan.sCustomField39Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField39Notes", dataLoan.sCustomField39Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField40Desc", dataLoan.sCustomField40Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField40D", dataLoan.sCustomField40D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField40Money", dataLoan.sCustomField40Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField40Pc", dataLoan.sCustomField40Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField40Bit", dataLoan.sCustomField40Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField40Notes", dataLoan.sCustomField40Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField41Desc", dataLoan.sCustomField41Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField41D", dataLoan.sCustomField41D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField41Money", dataLoan.sCustomField41Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField41Pc", dataLoan.sCustomField41Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField41Bit", dataLoan.sCustomField41Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField41Notes", dataLoan.sCustomField41Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField42Desc", dataLoan.sCustomField42Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField42D", dataLoan.sCustomField42D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField42Money", dataLoan.sCustomField42Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField42Pc", dataLoan.sCustomField42Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField42Bit", dataLoan.sCustomField42Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField42Notes", dataLoan.sCustomField42Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField43Desc", dataLoan.sCustomField43Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField43D", dataLoan.sCustomField43D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField43Money", dataLoan.sCustomField43Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField43Pc", dataLoan.sCustomField43Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField43Bit", dataLoan.sCustomField43Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField43Notes", dataLoan.sCustomField43Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField44Desc", dataLoan.sCustomField44Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField44D", dataLoan.sCustomField44D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField44Money", dataLoan.sCustomField44Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField44Pc", dataLoan.sCustomField44Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField44Bit", dataLoan.sCustomField44Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField44Notes", dataLoan.sCustomField44Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField45Desc", dataLoan.sCustomField45Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField45D", dataLoan.sCustomField45D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField45Money", dataLoan.sCustomField45Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField45Pc", dataLoan.sCustomField45Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField45Bit", dataLoan.sCustomField45Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField45Notes", dataLoan.sCustomField45Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField46Desc", dataLoan.sCustomField46Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField46D", dataLoan.sCustomField46D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField46Money", dataLoan.sCustomField46Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField46Pc", dataLoan.sCustomField46Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField46Bit", dataLoan.sCustomField46Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField46Notes", dataLoan.sCustomField46Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField47Desc", dataLoan.sCustomField47Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField47D", dataLoan.sCustomField47D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField47Money", dataLoan.sCustomField47Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField47Pc", dataLoan.sCustomField47Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField47Bit", dataLoan.sCustomField47Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField47Notes", dataLoan.sCustomField47Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField48Desc", dataLoan.sCustomField48Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField48D", dataLoan.sCustomField48D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField48Money", dataLoan.sCustomField48Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField48Pc", dataLoan.sCustomField48Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField48Bit", dataLoan.sCustomField48Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField48Notes", dataLoan.sCustomField48Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField49Desc", dataLoan.sCustomField49Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField49D", dataLoan.sCustomField49D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField49Money", dataLoan.sCustomField49Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField49Pc", dataLoan.sCustomField49Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField49Bit", dataLoan.sCustomField49Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField49Notes", dataLoan.sCustomField49Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField50Desc", dataLoan.sCustomField50Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField50D", dataLoan.sCustomField50D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField50Money", dataLoan.sCustomField50Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField50Pc", dataLoan.sCustomField50Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField50Bit", dataLoan.sCustomField50Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField50Notes", dataLoan.sCustomField50Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField51Desc", dataLoan.sCustomField51Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField51D", dataLoan.sCustomField51D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField51Money", dataLoan.sCustomField51Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField51Pc", dataLoan.sCustomField51Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField51Bit", dataLoan.sCustomField51Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField51Notes", dataLoan.sCustomField51Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField52Desc", dataLoan.sCustomField52Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField52D", dataLoan.sCustomField52D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField52Money", dataLoan.sCustomField52Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField52Pc", dataLoan.sCustomField52Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField52Bit", dataLoan.sCustomField52Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField52Notes", dataLoan.sCustomField52Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField53Desc", dataLoan.sCustomField53Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField53D", dataLoan.sCustomField53D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField53Money", dataLoan.sCustomField53Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField53Pc", dataLoan.sCustomField53Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField53Bit", dataLoan.sCustomField53Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField53Notes", dataLoan.sCustomField53Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField54Desc", dataLoan.sCustomField54Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField54D", dataLoan.sCustomField54D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField54Money", dataLoan.sCustomField54Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField54Pc", dataLoan.sCustomField54Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField54Bit", dataLoan.sCustomField54Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField54Notes", dataLoan.sCustomField54Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField55Desc", dataLoan.sCustomField55Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField55D", dataLoan.sCustomField55D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField55Money", dataLoan.sCustomField55Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField55Pc", dataLoan.sCustomField55Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField55Bit", dataLoan.sCustomField55Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField55Notes", dataLoan.sCustomField55Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField56Desc", dataLoan.sCustomField56Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField56D", dataLoan.sCustomField56D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField56Money", dataLoan.sCustomField56Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField56Pc", dataLoan.sCustomField56Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField56Bit", dataLoan.sCustomField56Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField56Notes", dataLoan.sCustomField56Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField57Desc", dataLoan.sCustomField57Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField57D", dataLoan.sCustomField57D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField57Money", dataLoan.sCustomField57Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField57Pc", dataLoan.sCustomField57Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField57Bit", dataLoan.sCustomField57Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField57Notes", dataLoan.sCustomField57Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField58Desc", dataLoan.sCustomField58Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField58D", dataLoan.sCustomField58D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField58Money", dataLoan.sCustomField58Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField58Pc", dataLoan.sCustomField58Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField58Bit", dataLoan.sCustomField58Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField58Notes", dataLoan.sCustomField58Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField59Desc", dataLoan.sCustomField59Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField59D", dataLoan.sCustomField59D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField59Money", dataLoan.sCustomField59Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField59Pc", dataLoan.sCustomField59Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField59Bit", dataLoan.sCustomField59Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField59Notes", dataLoan.sCustomField59Notes));

            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField60Desc", dataLoan.sCustomField60Desc));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField60D", dataLoan.sCustomField60D_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField60Money", dataLoan.sCustomField60Money_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField60Pc", dataLoan.sCustomField60Pc_rep));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField60Bit", dataLoan.sCustomField60Bit ? "Y" : "N"));
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("sCustomField60Notes", dataLoan.sCustomField60Notes)); 
 
            #endregion

            #region Customdata for sBrokControlledFundT
            switch (dataLoan.sBrokControlledFundT)  // opm 82162
            {
                case E_sBrokControlledFundT.May:
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("MaySubjectToSection10241JIndicator", "Y"));
                    break;
                case E_sBrokControlledFundT.Will:
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("WillSubjectToSection10241JIndicator", "Y"));
                    break;
                case E_sBrokControlledFundT.WillNot:
                    websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("WillNotSubjectToSection10241JIndicator", "Y"));
                    break;
                case E_sBrokControlledFundT.LeaveBlank:
                    break;
                default:
                    Tools.LogErrorWithCriticalTracking(new UnhandledEnumException(dataLoan.sBrokControlledFundT));
                    break;
            }
            #endregion

            var gfepreparer = dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            websheetRequest.Losdata.CustomDataList.Add(CreateCustomData("GFEOriginatorEmail", gfepreparer.EmailAddr));

            return websheetRequest;
        }

        /// <summary>
        /// Populates the party list with information on settlement service providers.
        /// </summary>
        /// <param name="partyList">The party list to populate.</param>
        /// <param name="dataLoan">Information on the loan.</param>
        private static void PopulatePartyList(List<Party> partyList, CPageData dataLoan)
        {
            // OPM 189752 Send over line 15. & 17. on the FHA Addendum page
            IPreparerFields fhaAddendumLenderFields = dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumLender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            var fhaVaLenderParty = new Party();
            fhaVaLenderParty.Type = E_PartyType.FHAVALender;
            fhaVaLenderParty.Name = fhaAddendumLenderFields.CompanyName;
            fhaVaLenderParty.Street = fhaAddendumLenderFields.StreetAddr;
            fhaVaLenderParty.City = fhaAddendumLenderFields.City;
            fhaVaLenderParty.State = fhaAddendumLenderFields.State;
            fhaVaLenderParty.Zip = fhaAddendumLenderFields.Zip;
            fhaVaLenderParty.ContactPointList.Add(CreateContactPoint(DocMagic.DsiDocRequest.E_ContactPointType.Phone, fhaAddendumLenderFields.PhoneOfCompany));
            partyList.Add(fhaVaLenderParty);

            partyList.AddRange(dataLoan.sAllSettlementServiceProviders.Select(provider => CreateParty(provider)));

            if (!dataLoan.sIsLegacyClosingCostVersion)
            {
                var parties = dataLoan.sAvailableSettlementServiceProviders
                    .GetAllProviders()
                    .Select(provider => CreateParty(provider));

                partyList.AddRange(parties);
            }
        }

        /// <summary>
        /// Create a party representing a settlement service provider.
        /// </summary>
        /// <param name="provider">The service provider information.</param>
        /// <returns>A Party element.</returns>
        private static Party CreateParty(SettlementServiceProvider provider)
        {
            var party = new Party();
            party.Type = GetSettlementServiceProviderType(provider.ServiceT);
            party.Name = provider.CompanyName;
            party.Street = provider.StreetAddress;
            party.City = provider.City;
            party.State = provider.State;
            party.Zip = provider.Zip;
            party.ProposedSettlementServicesProviderIndicator = E_YNIndicator.Y;

            party.PartyContacts = new PartyContacts();
            party.PartyContacts.PartyContactList.Add(new PartyContact()
            {
                ContactName = provider.ContactName,
                ContactPoint = CreateContactPoint(DocMagic.DsiDocRequest.E_ContactPointType.Phone, provider.Phone)
            });

            if (party.Type == E_PartyType.Other)
            {
                party.TypeOtherDescription = provider.ServiceTDesc;
            }

            return party;
        }

        /// <summary>
        /// Maps an agent role to a settlement service provider type.
        /// </summary>
        /// <param name="role">The agent role.</param>
        /// <returns>The corresponding SSP type.</returns>
        private static E_PartyType GetSettlementServiceProviderType(E_AgentRoleT role)
        {
            if (role == E_AgentRoleT.Title)
            {
                return E_PartyType.TitleInsurance;
            }
            else if (role == E_AgentRoleT.Escrow)
            {
                return E_PartyType.SettlementClosing;
            }
            else if (role == E_AgentRoleT.Surveyor)
            {
                return E_PartyType.Survey;
            }
            else if (role == E_AgentRoleT.PestInspection)
            {
                return E_PartyType.PestInspection;
            }
            else
            {
                return E_PartyType.Other;
            }
        }

        /// <summary>
        /// Creates the Process element required for a document generation request.
        /// </summary>
        /// <param name="packageType">Package Type: DO NOT use Flood Certification packages</param>
        /// <param name="forms">Forms for use with FormList packages</param>
        /// <param name="sendEDisclosures"></param>
        /// <param name="allowOnlineSigning"></param>
        /// <param name="emailDocuments"></param>
        /// <param name="emailAddress">Email</param>
        /// <param name="emailRequirePassword">Email</param>
        /// <param name="emailPasswordHint">Email</param>
        /// <param name="emailPassword">Email</param>
        /// <param name="emailNotifyOnRetrieval">Email</param>
        /// <param name="printAndDeliver"></param>
        /// <param name="attention">Delivery Information</param>
        /// <param name="name">Delivery Information</param>
        /// <param name="street">Delivery Information</param>
        /// <param name="city">Delivery Information</param>
        /// <param name="state">Delivery Information</param>
        /// <param name="zip">Delivery Information</param>
        /// <param name="phone">Delivery Information</param>
        /// <param name="notesForDsi">Delivery Information</param>
        /// <param name="mersRegistration"></param>
        /// <returns></returns>
        public static Process CreateProcessOptions(
            E_ProcessPackageType packageType,
            List<Form> forms,
            bool sendEDisclosures, bool allowOnlineSigning,
            bool emailDocuments,
                string emailAddress,
                bool emailRequirePassword, string emailPasswordHint, string emailPassword,
                bool emailNotifyOnRetrieval,
            bool printAndDeliver,
                string attention, string name,
                string street, string city, string state, string zip,
                string phone, string notesForDsi,
            bool mersRegistration,
            string currentUserEmail, E_DocumentFormatType format)
        {
            // WAY TOO MANY PARAMETERS
            Process process = new Process();

            process.PackageType = packageType;
            if (packageType == E_ProcessPackageType.FormList && forms != null)
            {
                foreach (var form in forms)
                {
                    process.FormList.Add(form);
                }
            }
            process.IncludeBlockument = E_YesNo.Yes; // Always on
            process.DsiToPrintAndDeliver = printAndDeliver ? E_YesNo.Yes : E_YesNo.No; // if yes, include DeliveryData
            process.SafeDocs = E_YesNo.No; // Always off
            process.SendConfirmationEmail = E_YesNo.No; // Always off
            
            process.EDisclosure = sendEDisclosures ? E_YesNo.Yes : E_YesNo.No; // Should send a copy of the documents to all borrower addresses

            process.RegisterMersNumber = mersRegistration ? E_YesNo.Yes : E_YesNo.No;
            process.ClickSign = allowOnlineSigning ? E_YesNo.Yes : E_YesNo.No;
            process.DocumentFormat = CreateDocumentFormat(format);

            if (printAndDeliver)
            {
                process.DeliveryData = CreateDeliveryDataOptions(attention,
                    name, street, city, state, zip, phone, notesForDsi);
            }

            //process.SelectedFormats = new SelectedFormats(); // unused

            if (emailDocuments) // should send a copy of the documents to $emailAddress
            {
                // Send notification on read
                process.WebPickup = CreateWebPickup(currentUserEmail,
                    emailRequirePassword, emailPasswordHint, emailPassword,
                    emailNotifyOnRetrieval);

                // Send a link of the documents to this email
                process.EmailService = E_YesNo.Yes;
                process.EmailInfo = CreateEmailInfo(emailAddress, "", null);
            }

            //process.SignatureInfo = new SignatureInfo();
            //process.EDisclosureInfo = new EDisclosureInfo();
            //process.MersInfo = new MersInfo();
            //process.FloodInfo = new FloodInfo();
            //process.AttachmentList

            return process;
        }

        private static DeliveryData CreateDeliveryDataOptions(string attention, string name,
            string street, string city, string state, string zip, string phone,
            string notesForDsi)
        {
            DeliveryData deliveryData = new DeliveryData();
            deliveryData.Attention = attention;
            deliveryData.Name = name;
            deliveryData.Street = street;
            deliveryData.City = city;
            deliveryData.State = state;
            deliveryData.Zip = zip;
            deliveryData.Phone = phone;
            deliveryData.DsiManagerNotesList.Add(notesForDsi);
            return deliveryData;
        }

        private static WebPickup CreateWebPickup(string emailAddress, bool emailRequirePassword,
            string emailPasswordHint, string emailPassword, bool emailNotifyOnRetrieval)
        {
            WebPickup webPickup = new WebPickup();
            if (emailNotifyOnRetrieval)
            {
                // This address will be notified when the recipient of EmailInfo opens the webPickup link
                webPickup.EmailAddress = emailAddress;
            }
            if (emailRequirePassword)
            {
                webPickup.PassType = emailPasswordHint;
                webPickup.Password = emailPassword;
            }
            webPickup.Notify = emailNotifyOnRetrieval ? E_YesNo.Yes : E_YesNo.No;
            return webPickup;
        }

        private static EmailInfo CreateEmailInfo(string emailAddress, string subject, List<string> notes)
        {
            EmailInfo emailInfo = new EmailInfo();
            emailInfo.EmailAddress = emailAddress;
            emailInfo.Subject = subject;

            if (notes != null)
            {
                foreach (string note in notes)
                {
                    emailInfo.NoteList.Add(note);
                }
            }
            return emailInfo;
        }

        private static global::DocMagic.DsiDocRequest.DocumentFormat CreateDocumentFormat(E_DocumentFormatType formatType)
        {
            global::DocMagic.DsiDocRequest.DocumentFormat df = new global::DocMagic.DsiDocRequest.DocumentFormat();
            df.Type = formatType;
            df.CompressionType = E_DocumentFormatCompressionType.Zip;
            df.BundleType = E_DocumentFormatBundleType.Set;
            df.IncludeInResponse = E_YesNo.Yes;
            //df.IncludeSignatureFields = E_YesNo.Undefined;
            return df;
        }

        private static DocMagic.DsiDocRequest.ManufacturedHome CreateManufacturedHome(CPageData dataLoan)
        {
            DocMagic.DsiDocRequest.ManufacturedHome mh = new DocMagic.DsiDocRequest.ManufacturedHome();
            mh.MakeIdentifier = dataLoan.sManufacturedHomeMake;
            mh.ModelIdentifier = dataLoan.sManufacturedHomeModel;
            mh.ManufactureYear = dataLoan.sManufacturedHomeYear;
            switch (dataLoan.sManufacturedHomeConditionT)
            {
                case E_ManufacturedHomeConditionT.LeaveBlank:
                    mh.ConditionDescriptionType = DocMagic.DsiDocRequest.E_ManufacturedHomeConditionDescriptionType.Undefined;
                    break;
                case E_ManufacturedHomeConditionT.New:
                    mh.ConditionDescriptionType = DocMagic.DsiDocRequest.E_ManufacturedHomeConditionDescriptionType.New;
                    break;
                case E_ManufacturedHomeConditionT.Used:
                    mh.ConditionDescriptionType = DocMagic.DsiDocRequest.E_ManufacturedHomeConditionDescriptionType.Used;
                    break;
                default:
                    throw new UnhandledEnumException(dataLoan.sManufacturedHomeConditionT);
            }
            
            mh.LengthFeetCount = dataLoan.sManufacturedHomeLengthFeet_rep;
            mh.WidthFeetCount = dataLoan.sManufacturedHomeWidthFeet_rep;
            mh.Manufacturer = dataLoan.sManufacturedHomeManufacturer;
            mh.SerialNumberIdentifier = dataLoan.sManufacturedHomeSerialNumber;
            mh.HudCertificationLabelIdentifier = dataLoan.sManufacturedHomeHUDLabelNumber;
            switch (dataLoan.sManufacturedHomeAttachedToFoundation)
            {
                case E_TriState.Blank:
                    mh.AttachedToFoundationIndicator = E_YNIndicator.Undefined;
                    break;
                case E_TriState.No:
                    mh.AttachedToFoundationIndicator = E_YNIndicator.N;
                    break;
                case E_TriState.Yes:
                    mh.AttachedToFoundationIndicator = E_YNIndicator.Y;
                    break;
            }
            mh.CertificateOfTitleIdentifier = dataLoan.sManufacturedHomeCertificateofTitle;
            mh.CertificateOfTitleType = ToMismo(dataLoan.sManufacturedHomeCertificateofTitleT);
            //mh.PropertyID = null; // Leave unassigned until needed. This will link with //LOAN/_APPLICATION/PROPERTY/@_ID
            return mh;
        }

        private static void PopulateCreditDenialList(List<CreditDenial> ls, CPageData dataLoan)
        {
            if (dataLoan.sHmdaLoanDenied)
            {
                int nApps = dataLoan.nApps;
                for (int appIndex = 0; appIndex < nApps; appIndex++)
                {
                    CAppData dataApp = dataLoan.GetAppData(appIndex);
                    E_BorrowerModeT prevMode = dataApp.BorrowerModeT;
                    if (dataApp.aBIsValidNameSsn)
                    {
                        dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                        ls.Add(CreateCreditDenial(dataLoan, dataApp));
                    }
                    if (dataApp.aCIsValidNameSsn)
                    {
                        dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                        ls.Add(CreateCreditDenial(dataLoan, dataApp));
                    }
                    dataApp.BorrowerModeT = prevMode;
                }
            }
        }

        private static CreditDenial CreateCreditDenial(CPageData dataLoan, CAppData dataApp)
        {
            CreditDenial creditDenial = new CreditDenial();
            creditDenial.BorrowerId = dataApp.aMismoId;
            creditDenial.DenialDate = dataLoan.sRejectD_rep;
            creditDenial.DenialNoticeSentDate = dataLoan.sHmdaDeniedFormDoneD_rep;
            creditDenial.DeniedByName = dataLoan.sHmdaLoanDeniedBy;

            bool isAtLeastV22 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(dataLoan.sLoanVersionT, LoanVersionT.V22_HmdaDataPointImprovement);

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                creditDenial.AdditionalStatementDescription = dataApp.aDenialAdditionalStatement;
                PopulateDenialReasonListForBorrower(creditDenial.DenialReasonList, dataApp);
            }
            else if (dataApp.BorrowerModeT == E_BorrowerModeT.Coborrower)
            {
                creditDenial.AdditionalStatementDescription = dataApp.aCDenialAdditionalStatement;
                PopulateDenialReasonListForCoborrower(creditDenial.DenialReasonList, dataApp);
            }

            if (dataLoan.sHmdaCounterOfferMade)
            {
                creditDenial.CounterOffer = CreateCounterOffer(dataLoan);
                creditDenial.ActionTakenType = E_CreditDenialActionTakenType.CounterOffer;
            }
            else if ((isAtLeastV22 && dataLoan.sHmdaActionTakenT == HmdaActionTaken.ApplicationDenied)
                || (!isAtLeastV22 && dataLoan.sHmdaActionTaken == "Application denied"))
            {
                creditDenial.ActionTakenType = E_CreditDenialActionTakenType.Denied;
            }
            else if ((isAtLeastV22 && dataLoan.sHmdaActionTakenT == HmdaActionTaken.ApplicationWithdrawnByApplicant)
                || (!isAtLeastV22 && dataLoan.sHmdaActionTaken == "Application withdrawn"))
            {
                creditDenial.ActionTakenType = E_CreditDenialActionTakenType.Withdrawn;
            }
            else
            {
                creditDenial.ActionTakenType = E_CreditDenialActionTakenType.Other;
                creditDenial.ActionTakenTypeOtherDescription = dataLoan.sHmdaActionTaken;
            }
            return creditDenial;
        }

        private static void PopulateDenialReasonListForBorrower(List<DenialReason> ls, CAppData dataApp)
        {
            DenialReason denialReason;
            #region A - Credit
            if (dataApp.aDenialNoCreditFile)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "No Credit File";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialInsufficientCreditRef)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Insufficient Credit Reference";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialInsufficientCreditFile)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Insufficient Credit File";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialUnableVerifyCreditRef)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Unable to Verify Credit References";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialGarnishment)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Garnishment, Attachment, Foreclosure, Repossesion or Suit";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialExcessiveObligations)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Excessive Obligations";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialInsufficientIncome)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Insufficient Income for Total Obligations";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialUnacceptablePmtRecord)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Unacceptable Payment Record on Previous Mortgage";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialLackOfCashReserves)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Lack of Cash Reserves";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialDeliquentCreditObligations)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Delinquent Credit Obligations";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialBankruptcy)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Bankruptcy";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialInfoFromConsumerReportAgency)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Information From a Consumer Reporting Agency";
                ls.Add(denialReason);
            }
            #endregion
            #region B - Employment Status
            if (dataApp.aDenialUnableVerifyEmployment)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unable to Verify Employment";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialLenOfEmployment)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Length of Employment";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialTemporaryEmployment)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Temporary or Irregular Employment, Insufficient Stability of Income";
                ls.Add(denialReason);
            }
            #endregion
            #region C - Income
            if (dataApp.aDenialInsufficientIncomeForMortgagePmt)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Insufficient Income for Mortgage Payments";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialUnableVerifyIncome)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unable to Verify Income";
                ls.Add(denialReason);
            }
            #endregion
            #region D - Residency
            if (dataApp.aDenialTempResidence)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Temporary Residence";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialShortResidencePeriod)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Too Short a Period of Residence";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialUnableVerifyResidence)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unable to Verify Residence";
                ls.Add(denialReason);
            }
            #endregion
            #region E - INS, GUARANTY OR PURCH DENIED BY:
            if (dataApp.aDenialByHUD)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = "Department of Housing and Urban Development";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialByVA)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = "Department of Veterans Affairs";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialByFedNationalMortAssoc)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = "Federal National Mortgage Association";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialByFedHomeLoanMortCorp)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = "Federal Home Loan Mortgage Corporation";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialByOther)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = dataApp.aDenialByOtherDesc;
                ls.Add(denialReason);
            }
            #endregion
            #region F - Other
            if (dataApp.aDenialInsufficientFundsToClose)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Insufficient Funds to Close the Loan";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialCreditAppIncomplete)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Credit Application Incomplete";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialInadequateCollateral)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Inadequate Collateral";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialUnacceptableProp)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unacceptable Property";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialInsufficientPropData)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Insufficient Date - Property";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialUnacceptableAppraisal)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unacceptable Appraisal";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialUnacceptableLeasehold)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unacceptable Leasehold Estate";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "We do not grant credit to any applicant on the terms and conditions you have requested";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialWithdrawnByApp)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Withdrawn by Applicant";
                ls.Add(denialReason);
            }
            if (dataApp.aDenialOtherReason1)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = dataApp.aDenialOtherReason1Desc;
                ls.Add(denialReason);
            }
            if (dataApp.aDenialOtherReason2)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = dataApp.aDenialOtherReason2Desc;
                ls.Add(denialReason);
            }
            #endregion
        }

        private static void PopulateDenialReasonListForCoborrower(List<DenialReason> ls, CAppData dataApp)
        {
            DenialReason denialReason;
            #region A - Credit
            if (dataApp.aCDenialNoCreditFile)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "No Credit File";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialInsufficientCreditRef)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Insufficient Credit Reference";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialInsufficientCreditFile)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Insufficient Credit File";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialUnableVerifyCreditRef)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Unable to Verify Credit References";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialGarnishment)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Garnishment, Attachment, Foreclosure, Repossesion or Suit";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialExcessiveObligations)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Excessive Obligations";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialInsufficientIncome)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Insufficient Income for Total Obligations";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialUnacceptablePmtRecord)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Unacceptable Payment Record on Previous Mortgage";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialLackOfCashReserves)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Lack of Cash Reserves";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialDeliquentCreditObligations)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Delinquent Credit Obligations";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialBankruptcy)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Bankruptcy";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialInfoFromConsumerReportAgency)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Credit;
                denialReason.InnerText = "Information From a Consumer Reporting Agency";
                ls.Add(denialReason);
            }
            #endregion
            #region B - Employment Status
            if (dataApp.aCDenialUnableVerifyEmployment)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unable to Verify Employment";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialLenOfEmployment)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Length of Employment";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialTemporaryEmployment)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Temporary or Irregular Employment, Insufficient Stability of Income";
                ls.Add(denialReason);
            }
            #endregion
            #region C - Income
            if (dataApp.aCDenialInsufficientIncomeForMortgagePmt)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Insufficient Income for Mortgage Payments";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialUnableVerifyIncome)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unable to Verify Income";
                ls.Add(denialReason);
            }
            #endregion
            #region D - Residency
            if (dataApp.aCDenialTempResidence)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Temporary Residence";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialShortResidencePeriod)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Too Short a Period of Residence";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialUnableVerifyResidence)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unable to Verify Residence";
                ls.Add(denialReason);
            }
            #endregion
            #region E - INS, GUARANTY OR PURCH DENIED BY:
            if (dataApp.aCDenialByHUD)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = "Department of Housing and Urban Development";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialByVA)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = "Department of Veterans Affairs";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialByFedNationalMortAssoc)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = "Federal National Mortgage Association";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialByFedHomeLoanMortCorp)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = "Federal Home Loan Mortgage Corporation";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialByOther)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Outside;
                denialReason.InnerText = dataApp.aCDenialByOtherDesc;
                ls.Add(denialReason);
            }
            #endregion
            #region F - Other
            if (dataApp.aCDenialInsufficientFundsToClose)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Insufficient Funds to Close the Loan";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialCreditAppIncomplete)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Credit Application Incomplete";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialInadequateCollateral)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Inadequate Collateral";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialUnacceptableProp)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unacceptable Property";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialInsufficientPropData)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Insufficient Date - Property";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialUnacceptableAppraisal)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unacceptable Appraisal";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialUnacceptableLeasehold)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Unacceptable Leasehold Estate";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialNotGrantCreditToAnyAppOnSuchTermsAndConds)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "We do not grant credit to any applicant on the terms and conditions you have requested";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialWithdrawnByApp)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = "Withdrawn by Applicant";
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialOtherReason1)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = dataApp.aCDenialOtherReason1Desc;
                ls.Add(denialReason);
            }
            if (dataApp.aCDenialOtherReason2)
            {
                denialReason = new DenialReason();
                denialReason.SourceType = E_DenialReasonSourceType.Other;
                denialReason.InnerText = dataApp.aCDenialOtherReason2Desc;
                ls.Add(denialReason);
            }
            #endregion
        }

        private static CounterOffer CreateCounterOffer(CPageData dataLoan)
        {
            CounterOffer counterOffer = new CounterOffer();
            counterOffer.Date = dataLoan.sHmdaCounterOfferMadeD_rep;
            counterOffer.Description = dataLoan.sHmdaCounterOfferDetails;
            return counterOffer;
        }

        private static AbilityToRepay CreateAbilityToRepay(CPageData dataLoan)
        {
            AbilityToRepay abilityToRepay = new AbilityToRepay();
            if (dataLoan.sIsLineOfCredit)
            {
                abilityToRepay.AbilityToRepayExemptionLoanProgramType = E_AbilityToRepayAbilityToRepayExemptionLoanProgramType.HELOC;
                abilityToRepay.AbilityToRepayExemptionReasonType = E_AbilityToRepayAbilityToRepayExemptionReasonType.LoanProgram;
            }
            if (dataLoan.sQMIsEligibleByLoanPurchaseAgency)
            {
                abilityToRepay.QualifiedMortgageType = E_AbilityToRepayQualifiedMortgageType.TemporaryAgencyGSE;

                if (dataLoan.sQMLoanPurchaseAgency == E_sQMLoanPurchaseAgency.FannieMae)
                    abilityToRepay.QualifiedMortgageTemporaryGseType = E_AbilityToRepayQualifiedMortgageTemporaryGseType.FannieMae;
                else if (dataLoan.sQMLoanPurchaseAgency == E_sQMLoanPurchaseAgency.FreddieMac)
                    abilityToRepay.QualifiedMortgageTemporaryGseType = E_AbilityToRepayQualifiedMortgageTemporaryGseType.FreddieMac;
            }
            else
                abilityToRepay.QualifiedMortgageType = E_AbilityToRepayQualifiedMortgageType.Standard;

            abilityToRepay.QualifiedMortgageIncomeAndAssetTotalAmount = dataLoan.sLTotI_rep;
            decimal d;
            if (decimal.TryParse(dataLoan.sQMQualBottom_rep, out d))
            {
                abilityToRepay.QualifiedMortgageDebtToIncomeRatioPercent = dataLoan.sQMQualBottom_rep;
            }

            return abilityToRepay;
        }

        private static DisclosureDetail CreateDisclosureDetail(CPageData dataLoan, CPageData dataLoanWithGfeArchiveApplied)
        {
            DisclosureDetail dd = new DisclosureDetail();
            dd.ChangedCircumstanceDate = dataLoanWithGfeArchiveApplied.sCircumstanceChangeD_rep;
            dd.DisclosureSendDate = dataLoan.sDocMagicPreZSentD_rep;
            dd.ReDisclosureSendDate = dataLoan.sDocMagicReDiscSendD_rep; // Most recent date between GFE and TIL
            dd.ReDisclosureReceivedDate = dataLoan.sDocMagicReDiscReceivedD_rep;

            // OPM 90968 5/23/13 GF
            BrokerDB brokerDB = dataLoan.BrokerDB;
            if (!brokerDB.DisableDocMagicLastDiscApr)
            {
                dd.LastDisclosedAPR = dataLoan.sLastDiscAPR_rep;
            }

            dd.ReDisclosureMethod = ToMismo(dataLoan.sDocMagicRedisclosureMethodT);
            return dd;
        }

        private static PriorLien CreatePriorLien(CPageData dataLoan)
        {
            PriorLien priorLien = new PriorLien();
            
            //priorLien.RelatedLoanId;
            priorLien.LienPriority = E_PriorLienLienPriority._1st;
            priorLien.RelationshipToSubjectLoanType = E_PriorLienRelationshipToSubjectLoanType.RefinancedBySubjectLoan;

            DateTime dummyDate = new DateTime(1800, 1, 1); // 1800 so we clearly know it's a dummy date
            priorLien.MortgageDate = dataLoan.m_convertLos.ToDateTimeString(dummyDate);
            //priorLien.BorrowerVesting;
            priorLien.PrincipalAmount = dataLoan.sVaLoanAmtCurrentLoan_rep;
            priorLien.InterestRate = dataLoan.sVaNoteIrCurrentLoan_rep;
            priorLien.PaymentAmount = dataLoan.sVaPICurrentLoan_rep;

            DateTime maturityDate;
            if (dataLoan.sVaTermCurrentLoan > 0)
                maturityDate = dummyDate.AddMonths(dataLoan.sVaTermCurrentLoan - 1);
            else
                maturityDate = dummyDate;

            priorLien.MaturityDate = dataLoan.m_convertLos.ToDateTimeString(maturityDate);
            //priorLien.BalloonPaymentAmount;
            //priorLien.CurrentBalance;
            //priorLien.TrusteeName;
            //priorLien.NoteDate;
            //priorLien.LenderName;
            //priorLien.AssignedDate;
            //priorLien.AssignedToName;
            //priorLien.RetainedByName;
            //priorLien.PriorRecordingList;
            priorLien.InvestorLoanIdentifier = dataLoan.sVaLoanNumCurrentLoan;
            priorLien.PITIPaymentAmount = dataLoan.sVaMonthlyPmtCurrentLoan_rep;
            
            List<string> borrowerNames = new List<string>();
            int nApps = dataLoan.nApps;
            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                if (null != dataApp)
                {
                    borrowerNames.Add(dataApp.aBNm);
                }
            }

            if (borrowerNames.Count > 0)
            {
                priorLien.BorrowerName = string.Join(", ", borrowerNames.ToArray());
            }

            return priorLien;
        }

        private static PriorLien CreatePriorLien(string sLienXPriorityBefore, string sLienXAmtBefore_rep,
            string sLienHolderXNameBefore, string sLienXAmtAfter_rep, string sLienholderXNmAfter) // opm 82162
        {
            PriorLien priorLien = new PriorLien();

            priorLien.Set_LienPriority(sLienXPriorityBefore.ToLower());  // because these can be typed in, we may export as "" (undefined)
            priorLien.PrincipalAmount = sLienXAmtBefore_rep;
            priorLien.LenderName = sLienHolderXNameBefore;
            if (sLienHolderXNameBefore == sLienholderXNmAfter && sLienXAmtAfter_rep == sLienXAmtBefore_rep)
            {
                priorLien.RelationshipToSubjectLoanType = E_PriorLienRelationshipToSubjectLoanType.SubordinateLien;
            }

            return priorLien;
        }

        private static CustomData CreateCustomData(string name, string value)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(name))
                return null;

            CustomData customData = new CustomData();
            customData.Name = name;
            customData.Value = value;
            return customData;
        }

        /// <summary>
        /// Creates a <seealso cref="DocMagic.DsiDocRequest.ContactPoint"/> for a given type and value.
        /// </summary>
        /// <param name="type">The type of contact point.</param>
        /// <param name="value">The value of the contact point.</param>
        /// <returns>A contact point containing the type and value; null if value is null or empty.</returns>
        /// <exception cref="GenericUserErrorMessageException"><paramref name="type"/> is undefined (blank). Sending this value will cause a validation error for DocMagic.</exception>
        private static DocMagic.DsiDocRequest.ContactPoint CreateContactPoint(DocMagic.DsiDocRequest.E_ContactPointType type, string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            else if (type == DocMagic.DsiDocRequest.E_ContactPointType.Undefined)
                throw new GenericUserErrorMessageException("ContactPoint requires a valid type");

            DocMagic.DsiDocRequest.ContactPoint contactPoint = new DocMagic.DsiDocRequest.ContactPoint();
            contactPoint.Type = type;
            contactPoint.Value = value;
            return contactPoint;
        }

        /// <summary>
        /// Populates CoC data from the new datalayer.
        /// </summary>
        /// <param name="archive">The CoC archive.</param>
        /// <param name="respaFeeList">The RESPA fee list to populate.</param>
        /// <param name="loanFeeList">The fee list exported for the loan.</param>
        /// <param name="ignoreList">Fees that should be excluded from the payload.</param>
        private static void PopulateChangeOfCircumstanceData_2015DataLayer(
            ClosingCostCoCArchive archive,
            List<DocMagic.DsiDocRequest.RespaFee> respaFeeList,
            List<Mismo.Closing2_6.RespaFee> loanFeeList,
            List<Guid> ignoreList)
        {
            if (archive == null || loanFeeList == null || !loanFeeList.Any())
            {
                return;
            }

            foreach (var fee in archive.Fees)
            {
                // 10/15/2015 BS - Case 228678. Exclude aggregate, MI, & escrows from CoCData
                if (ignoreList != null && ignoreList.Any(id => id == fee.FeeId))
                {
                    continue;
                }

                // The ID field value cannot begin with a number, so we prepend with an underscore to prevent errors.
                var feeId = "_" + fee.FeeId.ToString();

                if (loanFeeList.Any(f => f != null && f.Id == feeId))
                {
                    var respaFee = new DocMagic.DsiDocRequest.RespaFee(); // This is different from the MISMO RESPA fee
                    respaFee.AssociatedWithChangedCircumstanceIndicator = E_YNIndicator.Y;
                    respaFee.RespaFeeID = feeId;

                    respaFeeList.Add(respaFee);
                }
            }
        }

        private static void PopulateChangeOfCircumstanceData(CPageData dataLoan, CPageData dataLoanWithGfeArchiveApplied, List<DocMagic.DsiDocRequest.RespaFee> respaFeeList, List<DocMagic.DsiDocRequest.Compensation> compensationList)
        {
            if (dataLoan.sSettlementChargesExportSource != E_SettlementChargesExportSource.GFE) // mxGfe
                return; // Only send the change of circumstance info if we're sending the GFE, since their IDs refer to GFE fees

            // For each change of circumstance, add a respa fee with RespaFeeID
            var fees = new[] {
                new { ID = dataLoanWithGfeArchiveApplied.sCircumstanceChange1FeeId, amt = dataLoanWithGfeArchiveApplied.sCircumstanceChange1Amount },
                new { ID = dataLoanWithGfeArchiveApplied.sCircumstanceChange2FeeId, amt = dataLoanWithGfeArchiveApplied.sCircumstanceChange2Amount },
                new { ID = dataLoanWithGfeArchiveApplied.sCircumstanceChange3FeeId, amt = dataLoanWithGfeArchiveApplied.sCircumstanceChange3Amount },
                new { ID = dataLoanWithGfeArchiveApplied.sCircumstanceChange4FeeId, amt = dataLoanWithGfeArchiveApplied.sCircumstanceChange4Amount },
                new { ID = dataLoanWithGfeArchiveApplied.sCircumstanceChange5FeeId, amt = dataLoanWithGfeArchiveApplied.sCircumstanceChange5Amount },
                new { ID = dataLoanWithGfeArchiveApplied.sCircumstanceChange6FeeId, amt = dataLoanWithGfeArchiveApplied.sCircumstanceChange6Amount },
                new { ID = dataLoanWithGfeArchiveApplied.sCircumstanceChange7FeeId, amt = dataLoanWithGfeArchiveApplied.sCircumstanceChange7Amount },
                new { ID = dataLoanWithGfeArchiveApplied.sCircumstanceChange8FeeId, amt = dataLoanWithGfeArchiveApplied.sCircumstanceChange8Amount },
            };

            foreach (var fee in fees)
            {
                if (string.IsNullOrEmpty(fee.ID) || fee.amt == 0 || IsSkipChangeOfCircumstances(fee.ID))
                {
                    continue;
                }

                if (fee.ID == "LQBGFE802") // OPM 87402 mp
                {
                    // If we send a yield spread premium, then also send this compensation element
                    if (dataLoan.sGfeLenderCreditYieldSpreadDifferentialAmount != 0)
                    {
                        var compensation = new DocMagic.DsiDocRequest.Compensation()
                        {
                            AssociatedWithChangedCircumstanceIndicator = E_YNIndicator.Y,
                            CompensationID = "LQBGFE802Compensation"
                        };
                        compensationList.Add(compensation);
                    }
                    // If we send a respa fee, then send this element
                    if (dataLoanWithGfeArchiveApplied.sGfeDiscountPointF != 0)
                    {
                        var respaFee = new DocMagic.DsiDocRequest.RespaFee() // This is different from the MISMO RESPA fee
                        {
                            AssociatedWithChangedCircumstanceIndicator = E_YNIndicator.Y,
                            RespaFeeID = "LQBGFE802RESPA"
                        };
                        respaFeeList.Add(respaFee);
                    }
                    // There are cases when we will send both elements
                }
                else if (fee.ID == "LQBGFE1201" && !dataLoanWithGfeArchiveApplied.sRecFLckd)
                {
                    if (!string.IsNullOrEmpty(dataLoanWithGfeArchiveApplied.sRecDeed_rep) &&
                        dataLoanWithGfeArchiveApplied.sRecDeed_rep != "0.00")
                    {
                        var deedRecordingFee = new DocMagic.DsiDocRequest.RespaFee()
                        {
                            AssociatedWithChangedCircumstanceIndicator = E_YNIndicator.Y,
                            RespaFeeID = "LQBGFE1202A"
                        };
                        respaFeeList.Add(deedRecordingFee);
                    }

                    if (!string.IsNullOrEmpty(dataLoanWithGfeArchiveApplied.sRecMortgage_rep) &&
                        dataLoanWithGfeArchiveApplied.sRecMortgage_rep != "0.00")
                    {
                        var mortgageRecordingFee = new DocMagic.DsiDocRequest.RespaFee()
                        {
                            AssociatedWithChangedCircumstanceIndicator = E_YNIndicator.Y,
                            RespaFeeID = "LQBGFE1202B"
                        };
                        respaFeeList.Add(mortgageRecordingFee);
                    }

                    if (!string.IsNullOrEmpty(dataLoanWithGfeArchiveApplied.sRecRelease_rep) &&
                        dataLoanWithGfeArchiveApplied.sRecRelease_rep != "0.00")
                    {
                        var releaseRecordingFee = new DocMagic.DsiDocRequest.RespaFee()
                        {
                            AssociatedWithChangedCircumstanceIndicator = E_YNIndicator.Y,
                            RespaFeeID = "LQBGFE1202C"
                        };
                        respaFeeList.Add(releaseRecordingFee);
                    }
                }
                else
                {
                    var respaFee = new DocMagic.DsiDocRequest.RespaFee() // This is different from the MISMO RESPA fee
                    {
                        AssociatedWithChangedCircumstanceIndicator = E_YNIndicator.Y,
                        RespaFeeID = fee.ID
                    };
                    respaFeeList.Add(respaFee);
                }
            }
        }

        /// <summary>
        /// Skips lines 901, 903, and the 1000 section.<para />
        /// Operates under the assumption that the line number
        /// is the first continuous set of digits in the ID.
        /// </summary>
        /// <param name="id">The ID of the line number.</param>
        /// <returns>true if the line is found and matches 901, 902, or the 1000 section; false otherwise.</returns>
        private static bool IsSkipChangeOfCircumstances(string id)
        {
            int startIndex = 0;
            while (startIndex < id.Length && !char.IsDigit(id[startIndex]))
                ++startIndex;

            int length = 0;
            for (int i = startIndex; i < id.Length && char.IsDigit(id[i]); ++i)
                ++length;

            int lineNumber;
            return int.TryParse(id.Substring(startIndex, length), out lineNumber)
                && (lineNumber == 901 || lineNumber == 903 || (1000 < lineNumber && lineNumber < 1100));
        }

        private static DocumentSpecificContent CreateDocumentSpecificContent(CPageData dataLoan)
        {
            dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            var documentSpecificContent = new DocMagic.DsiDocRequest.DocumentSpecificContent();

            // OPM 74043: Add support for anti-steering disclosure - m.p.
            PopulateLoanOptionsDisclosure(dataLoan, documentSpecificContent.LoanOptionsDisclosure);

            return documentSpecificContent;
        }
        
        private static void PopulateLoanOptionsDisclosure(CPageData dataLoan, LoanOptionsDisclosure disclosure)
        {
            var comparisonItemList = new List<LoanOptionComparisonItem>(4);

            var lowestInterestRate = new LoanOptionComparisonItem()
            {
                ComparisonType = E_LoanOptionComparisonItemComparisonType.LowestInterestRate,
                DemandFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestInterestRateIsDemandFeature),
                SharedAppreciationFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestInterestRateIsSharedAppreciation),
                SharedEquityFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestInterestRateIsSharedEquity),

                LenderName = dataLoan.sDocMagicAntiSteeringLowestInterestRateLenderName,
                LoanProgramName = dataLoan.sDocMagicAntiSteeringLowestInterestRateDescription,
                InterestRatePercent = dataLoan.sDocMagicAntiSteeringLowestInterestRateInitialRate_rep,
                OriginationFeeAmount = dataLoan.sDocMagicAntiSteeringLowestInterestRateOriginationFee_rep,
                DiscountPointsPercent = dataLoan.sDocMagicAntiSteeringLowestInterestRateDiscountPoints_rep,
                PrepaymentFeeAmount = dataLoan.sDocMagicAntiSteeringLowestInterestRatePrepaymentFeeAmount != 0 ? dataLoan.sDocMagicAntiSteeringLowestInterestRatePrepaymentFeeAmount_rep : "0",
                NegativeAmortization = dataLoan.sDocMagicAntiSteeringLowestInterestRateIsNegArm ? "Y" : "N",
                BalloonTermMonths = dataLoan.sDocMagicAntiSteeringLowestInterestRateBalloonDue != 0 ? dataLoan.sDocMagicAntiSteeringLowestInterestRateBalloonDue_rep : "0",
            };
            comparisonItemList.Add(lowestInterestRate);

            var lowestFee = new LoanOptionComparisonItem()
            {
                ComparisonType = E_LoanOptionComparisonItemComparisonType.LowestFees,
                DemandFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestFeeIsDemandFeature),
                SharedAppreciationFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestFeeIsSharedAppreciation),
                SharedEquityFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestFeeIsSharedEquity),

                LenderName = dataLoan.sDocMagicAntiSteeringLowestFeeLenderName,
                LoanProgramName = dataLoan.sDocMagicAntiSteeringLowestFeeDescription,
                InterestRatePercent = dataLoan.sDocMagicAntiSteeringLowestFeeInitialRate_rep,
                OriginationFeeAmount = dataLoan.sDocMagicAntiSteeringLowestFeeOriginationFee_rep,
                DiscountPointsPercent = dataLoan.sDocMagicAntiSteeringLowestFeeDiscountPoints_rep,
                PrepaymentFeeAmount = dataLoan.sDocMagicAntiSteeringLowestFeePrepaymentFeeAmount != 0 ? dataLoan.sDocMagicAntiSteeringLowestFeePrepaymentFeeAmount_rep : "0",
                NegativeAmortization = dataLoan.sDocMagicAntiSteeringLowestFeeIsNegArm ? "Y" : "N",
                BalloonTermMonths = dataLoan.sDocMagicAntiSteeringLowestFeeBalloonDue != 0 ? dataLoan.sDocMagicAntiSteeringLowestFeeBalloonDue_rep : "0",
            };
            comparisonItemList.Add(lowestFee);

            var lowestRateNoRiskyFeature = new LoanOptionComparisonItem()
            {
                ComparisonType = E_LoanOptionComparisonItemComparisonType.LowestInterestRateWithNoRiskyFeatures,
                DemandFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsDemandFeature),
                SharedAppreciationFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedAppreciation),
                SharedEquityFeatureIndicator = ToMismo(dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsSharedEquity),

                LenderName = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureLenderName,
                LoanProgramName = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureDescription,
                InterestRatePercent = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureInitialRate_rep,
                OriginationFeeAmount = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureOriginationFee_rep,
                DiscountPointsPercent = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureDiscountPoints_rep,
                PrepaymentFeeAmount = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeaturePrepaymentFeeAmount != 0 ? dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeaturePrepaymentFeeAmount_rep : "0",
                NegativeAmortization = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureIsNegArm ? "Y" : "N",
                BalloonTermMonths = dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureBalloonDue != 0 ? dataLoan.sDocMagicAntiSteeringLowestRateNoRiskyFeatureBalloonDue_rep : "0",
            };
            comparisonItemList.Add(lowestRateNoRiskyFeature);

            disclosure.LoanOptionComparisonItemList.AddRange(comparisonItemList);

            var disclosureDetail = new LoanOptionsDisclosureDetail()
            {
                CompensationPlanMinimumAmount = dataLoan.sDocMagicAntiSteeringCompensationPlanMinAmount_rep,
                CompensationPlanMaximumAmount = dataLoan.sDocMagicAntiSteeringCompensationPlanMaxAmount_rep
            };
            disclosure.LoanOptionsDisclosureDetail = disclosureDetail;
            return;

        }

        /// <summary>
        /// Side effect: populates the partyList with 4506 third party info.
        /// Wait a second... why is the partyList even populated in here?
        /// </summary>
        /// <param name="dataApp"></param>
        /// <param name="partyList"></param>
        /// <returns></returns>
        private static TaxTranscript CreateTaxTranscript(CAppData dataApp, List<Party> partyList)
        {
            if (string.IsNullOrEmpty(dataApp.a4506TTranscript))
            {
                return null;
            }
            TaxTranscript taxTranscript = new TaxTranscript();

            if (!dataApp.aIs4506TFiledTaxesSeparately && dataApp.aCIsValidNameSsn)
            {
                taxTranscript.BorrowerID = dataApp.aBMismoId + " " + dataApp.aCMismoId;
            }
            else
            {
                taxTranscript.BorrowerID = dataApp.aMismoId;
            }

            taxTranscript.TaxFormId = dataApp.a4506TTranscript;
            taxTranscript.ReturnTranscriptIndicator = ToMismo(dataApp.a4056TIsReturnTranscript);
            taxTranscript.AccountTranscriptIndicator = ToMismo(dataApp.a4506TIsAccountTranscript);
            taxTranscript.RecordOfAccountIndicator = ToMismo(dataApp.a4056TIsRecordAccountTranscript);
            taxTranscript.VerificationOfNonfilingIndicator = ToMismo(dataApp.a4506TVerificationNonfiling);
            taxTranscript.FormSeriesTranscriptIndicator = ToMismo(dataApp.a4506TSeriesTranscript);
            taxTranscript.IdentityTheftIndicator = ToMismo(dataApp.a4506TRequestYrHadIdentityTheft);

            taxTranscript.TaxPeriodDateList.Add(dataApp.a4506TYear1_rep);
            taxTranscript.TaxPeriodDateList.Add(dataApp.a4506TYear2_rep);
            taxTranscript.TaxPeriodDateList.Add(dataApp.a4506TYear3_rep);
            taxTranscript.TaxPeriodDateList.Add(dataApp.a4506TYear4_rep);

            PreviousFilingAddress addr = new PreviousFilingAddress();
            addr.Street = dataApp.a4506TPrevStreetAddr;
            addr.City = dataApp.a4506TPrevCity;
            addr.State = dataApp.a4506TPrevState;
            addr.Zip = dataApp.a4506TPrevZip;
            taxTranscript.PreviousFilingAddressList.Add(addr);

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                if (!string.IsNullOrEmpty(dataApp.aB4506TThirdPartyName))
                {
                    // If we didn't find the third party name in the party list, add it
                    if (!partyList.Any(party => party.Name.Equals(dataApp.aB4506TThirdPartyName, StringComparison.OrdinalIgnoreCase)))
                    {
                        Party party = new Party();
                        party.Type = E_PartyType.TaxTranscript;
                        party.Name = dataApp.aB4506TThirdPartyName;
                        party.Street = dataApp.aB4506TThirdPartyStreetAddr;
                        party.City = dataApp.aB4506TThirdPartyCity;
                        party.State = dataApp.aB4506TThirdPartyState;
                        party.Zip = dataApp.aB4506TThirdPartyZip;

                        if (!string.IsNullOrEmpty(dataApp.aB4506TThirdPartyPhone))
                        {
                            var contactPoint = new DocMagic.DsiDocRequest.ContactPoint();
                            contactPoint.Type = DocMagic.DsiDocRequest.E_ContactPointType.Phone;
                            contactPoint.Value = dataApp.aB4506TThirdPartyPhone;
                            party.ContactPointList.Add(contactPoint);
                        }

                        partyList.Add(party);
                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(dataApp.aC4506TThirdPartyName))
                {
                    // If we didn't find the third party name in the party list, add it
                    if (!partyList.Any(party => party.Name.Equals(dataApp.aC4506TThirdPartyName, StringComparison.OrdinalIgnoreCase)))
                    {
                        Party party = new Party();
                        party.Type = E_PartyType.TaxTranscript;
                        party.Name = dataApp.aC4506TThirdPartyName;
                        party.Street = dataApp.aC4506TThirdPartyStreetAddr;
                        party.City = dataApp.aC4506TThirdPartyCity;
                        party.State = dataApp.aC4506TThirdPartyState;
                        party.Zip = dataApp.aC4506TThirdPartyZip;
                        if (!string.IsNullOrEmpty(dataApp.aC4506TThirdPartyPhone))
                        {
                            var contactPoint = new DocMagic.DsiDocRequest.ContactPoint();
                            contactPoint.Type = DocMagic.DsiDocRequest.E_ContactPointType.Phone;
                            contactPoint.Value = dataApp.aC4506TThirdPartyPhone;
                            party.ContactPointList.Add(contactPoint);
                        }

                        partyList.Add(party);
                    }
                }
            }

            return taxTranscript;


        }
        private static BorrowerDetail CreateLosBorrowerDetail(CAppData dataApp)
        {
            BorrowerDetail borrowerDetail = new BorrowerDetail();
            borrowerDetail.BorrowerId = dataApp.aMismoId;
            if (string.IsNullOrEmpty(dataApp.aEquifaxScore_rep) == false)
            {
                borrowerDetail.CreditScoreList.Add(CreateCreditScore(dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.Equifax, dataApp.aEquifaxPercentile_rep));
            }
            if (string.IsNullOrEmpty(dataApp.aExperianScore_rep) == false)
            {
                borrowerDetail.CreditScoreList.Add(CreateCreditScore(dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.Experian, dataApp.aExperianPercentile_rep));
            }
            if (string.IsNullOrEmpty(dataApp.aTransUnionScore_rep) == false)
            {
                borrowerDetail.CreditScoreList.Add(CreateCreditScore(dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.TransUnion, dataApp.aTransUnionPercentile_rep));
            }

            borrowerDetail.VaBorrower = CreateBorrowerDetailVaBorrower(dataApp);

            return borrowerDetail;
        }

        private static DocMagic.DsiDocRequest.VaBorrower CreateBorrowerDetailVaBorrower(CAppData dataApp)
        {
            DocMagic.DsiDocRequest.VaBorrower vaBorrower = new DocMagic.DsiDocRequest.VaBorrower();
            vaBorrower.VaBenefitRelatedIndebtednessIndicator = ToMismo(dataApp.aVaIndebtCertifyTri);
            vaBorrower.VaDisabilityBenefitClaimIndicator = ToMismo(dataApp.aVAFileClaimDisabilityCertifyTri);
            vaBorrower.SeparatedFromMilitaryDueToDisabilityIndicator = dataApp.aVaDischargedDisabilityIs ? "Y" : "N";
            if (dataApp.aWereActiveMilitaryDutyDayAfterTri == E_TriState.Yes)
                vaBorrower.ActiveDutyOnDayFollowingDateOfSeparationIndicator = "Y";
            else if (dataApp.aWereActiveMilitaryDutyDayAfterTri == E_TriState.No)
                vaBorrower.ActiveDutyOnDayFollowingDateOfSeparationIndicator = "N";
            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                if(dataApp.LoanData.sLT==E_sLT.VA)
                    vaBorrower.MilitaryServiceList.AddRange(CreateLosMilitaryServices(dataApp));
                vaBorrower.PreviousVaHomeLoanIndicator = ToMismo(dataApp.aVaBorrCertHadVaLoanTri);
            }
            return vaBorrower;
        }

        private static List<DocMagic.DsiDocRequest.MilitaryService> CreateLosMilitaryServices(CAppData dataApp)
        {
            var services = new List<DocMagic.DsiDocRequest.MilitaryService>();
            services.Add(CreateLosMilitaryService(dataApp.aVaServ1Title, dataApp.aVaServ1Num, 1, dataApp.aMismoId));
            services.Add(CreateLosMilitaryService(dataApp.aVaServ2Title, dataApp.aVaServ2Num, 2, dataApp.aMismoId));
            services.Add(CreateLosMilitaryService(dataApp.aVaServ3Title, dataApp.aVaServ3Num, 3, dataApp.aMismoId));
            services.Add(CreateLosMilitaryService(dataApp.aVaServ4Title, dataApp.aVaServ4Num, 4, dataApp.aMismoId));
            services.Add(CreateLosMilitaryService(dataApp.aVaServ5Title, dataApp.aVaServ5Num, 5, dataApp.aMismoId));
            services.Add(CreateLosMilitaryService(dataApp.aVaServ6Title, dataApp.aVaServ6Num, 6, dataApp.aMismoId));
            services.Add(CreateLosMilitaryService(dataApp.aVaServ7Title, dataApp.aVaServ7Num, 7, dataApp.aMismoId));

            return services;
        }
        private static DocMagic.DsiDocRequest.MilitaryService CreateLosMilitaryService(string title, string num, int rowNumber, string borrowerId)
        {
            var service = new DocMagic.DsiDocRequest.MilitaryService();
            service.MilitaryServiceType = title;
            service.MilitaryServiceID = borrowerId+"_M"+rowNumber; //Create ID to match MISMO for linking
            return service;
        }

        private static DocMagic.DsiDocRequest.CreditScore CreateCreditScore(string borrowerId, E_CreditScoreCreditRepositorySourceType source, string rankPercent)
        {
            DocMagic.DsiDocRequest.CreditScore creditScore = new DocMagic.DsiDocRequest.CreditScore();
            creditScore.CreditScoreId = borrowerId + "_" + source.ToString();
            creditScore.CreditScoreRankPercent = rankPercent;
            return creditScore;
        }

        private static string CleanupWebsheetNumber(string websheetNumber)
        {
            if (string.IsNullOrEmpty(websheetNumber))
            {
                return null;
            }
            else
            {
                websheetNumber = websheetNumber.TrimWhitespaceAndBOM();
                if (websheetNumber.ToLower() == "new")
                {
                    return null;
                }
                else
                {
                    return websheetNumber;
                }
            }
        }

        private static E_YNIndicator ToMismo(bool b)
        {
            return b ? E_YNIndicator.Y : E_YNIndicator.N;
        }

        private static E_YNIndicator ToMismo(E_TriState b)
        {
            return b == E_TriState.Yes ? E_YNIndicator.Y : E_YNIndicator.N;
        }

        private static E_YesNo ToMismoYesNo(bool b)
        {
            return b ? E_YesNo.Yes : E_YesNo.No;
        }

        private static string ToMismo(E_ManufacturedHomeCertificateofTitleT type)
        {
            switch (type)
            {
                case E_ManufacturedHomeCertificateofTitleT.LeaveBlank:
                    return "";
                case E_ManufacturedHomeCertificateofTitleT.HomeShallBeCoveredByCertificateOfTitle:
                    return "Home shall be covered by Certificate of title";
                case E_ManufacturedHomeCertificateofTitleT.TitleCertificateShallBeEliminated:
                    return "Title Certificate shall be eliminated";
                case E_ManufacturedHomeCertificateofTitleT.TitleCertificateHasBeenEliminated:
                    return "Title Certificate has been eliminated";
                case E_ManufacturedHomeCertificateofTitleT.ManufacturersCertificateShallBeEliminated:
                    return "Manufacturer's Certificate shall be eliminated";
                case E_ManufacturedHomeCertificateofTitleT.ManufacturersCertificateHasBeenEliminated:
                    return "Manufacturer's Certificate has been eliminated";
                case E_ManufacturedHomeCertificateofTitleT.HomeIsNotCoveredOrCertIsAttached:
                    return "Home is not covered/Cert is attached";
                case E_ManufacturedHomeCertificateofTitleT.HomeIsNotCoveredOrNotAbleToProduceCert:
                    return "Home is not covered/Not able to produce Cert";
                default:
                    return "";
            }
        }

        private static E_GeneralLoanProceedsToType ToMismo(E_LoanProceedsToT type)
        {
            switch (type)
            {
                case E_LoanProceedsToT.LeaveBlank:
                    return E_GeneralLoanProceedsToType.Undefined;
                case E_LoanProceedsToT.Borrower:
                    return E_GeneralLoanProceedsToType.Borrower;
                case E_LoanProceedsToT.ClosingCompany:
                    return E_GeneralLoanProceedsToType.ClosingCompany;
                case E_LoanProceedsToT.TitleCompany:
                    return E_GeneralLoanProceedsToType.TitleCompany;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        private static E_DisclosureDetailRedisclosureMethod ToMismo(E_RedisclosureMethodT type)
        {
            switch (type)
            {
                case E_RedisclosureMethodT.LeaveBlank:
                    return E_DisclosureDetailRedisclosureMethod.Undefined;
                case E_RedisclosureMethodT.Email:
                    return E_DisclosureDetailRedisclosureMethod.Email;
                case E_RedisclosureMethodT.Fax:
                    return E_DisclosureDetailRedisclosureMethod.Fax;
                case E_RedisclosureMethodT.InPerson:
                    return E_DisclosureDetailRedisclosureMethod.InPerson;
                case E_RedisclosureMethodT.Mail:
                    return E_DisclosureDetailRedisclosureMethod.Mail;
                case E_RedisclosureMethodT.Overnight:
                    return E_DisclosureDetailRedisclosureMethod.Overnight;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
    }

    public class DocMagicMismoExporter
    {
        private Guid slid;
        private CPageData m_dataLoan = null;
        private CPageData m_dataLoanWithGfeArchiveApplied = null;
        private E_SettlementChargesExportSource m_exportSource;
        private bool mxGfe;
        private bool m_includeGfeDataForDocMagicComparison;

        /// <summary>
        /// Indicates whether the exporter should use the new datalayer GFE or LE data.
        /// </summary>
        private bool m_use2015DataLayer;

        /// <summary>
        /// Indicates whether the exporter should use TRID data as opposed to GFE data.
        /// </summary>
        private bool m_exportTridData;

        /// <summary>
        /// Indicates whether we're exporting an initial disclosure or redisclosure package.
        /// </summary>
        private bool m_isDisclosurePackage;

        /// <summary>
        /// True when the user is exporting settlement data and they want to include the gfe data along with the settlement. x_gfe has to be false.
        /// </summary>
        private bool miGfe;

        private bool m_setConformingYear = false;
        private string m_yieldSpreadDiffAmt = string.Empty;

        private List<string> m_feeDiscrepancies = null; // 12/17/2013 gf - opm 146376 keep list of settlement charge/GFE fee discrepancies.

        /// <summary>
        /// A list of fees to exclude from the payload
        /// </summary>
        private List<Guid> feeIgnoreList;
        public List<Guid> GetFeeIgnoreList()
        {
            return feeIgnoreList;
        }

        private static CPageData GetPageData(Guid loanid)
        {
            return new CPageData(loanid, "DocMagicMismoExporter",
             CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(DocMagicMismoExporter)).Union(
             CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release
             ));
        }

        public Loan CreateMismoLoan(WebsheetRequestParameters parameters, out List<string> feeDiscrepancies)
        {
            m_dataLoan = GetPageData(parameters.LoanNumber);
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            m_dataLoan.ByPassFieldSecurityCheck = parameters.ByPassFieldSecurityCheck;

            m_dataLoanWithGfeArchiveApplied = GetPageData(parameters.LoanNumber);
            m_dataLoanWithGfeArchiveApplied.InitLoad();

            m_exportTridData = m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            m_use2015DataLayer = m_dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;

            m_isDisclosurePackage = (m_exportTridData && parameters.DocumentPackageType == E_DocumentsPackageType.Initial)
                || (!m_exportTridData && parameters.ExportSource == E_SettlementChargesExportSource.GFE);

            if (m_dataLoanWithGfeArchiveApplied.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                if (m_use2015DataLayer)
                {
                    if (parameters.DocumentPackageType == E_DocumentsPackageType.Initial && m_dataLoanWithGfeArchiveApplied.sLastDisclosedClosingCostArchive != null)
                    {
                        m_dataLoanWithGfeArchiveApplied.ApplyClosingCostArchive(m_dataLoanWithGfeArchiveApplied.sLastDisclosedClosingCostArchive);
                    }

                    if (m_dataLoanWithGfeArchiveApplied.sClosingCostCoCArchive.Count() > 0)
                    {
                        m_dataLoanWithGfeArchiveApplied.ApplyClosingCostCoCData(m_dataLoanWithGfeArchiveApplied.sClosingCostCoCArchive.First());
                    }
                }
                else
                {
                    if (m_dataLoanWithGfeArchiveApplied.LastDisclosedGFEArchive != null)
                    {
                        m_dataLoanWithGfeArchiveApplied.ApplyGFEArchiveForExport(m_dataLoanWithGfeArchiveApplied.LastDisclosedGFEArchive);
                    }

                    if (m_dataLoanWithGfeArchiveApplied.CoCArchives.Count() > 0)
                    {
                        m_dataLoanWithGfeArchiveApplied.ApplyCoCArchive(m_dataLoanWithGfeArchiveApplied.CoCArchives.First());
                    }
                }
            }

            m_dataLoanWithGfeArchiveApplied.SetFormatTarget(FormatTarget.MismoClosing);
            m_dataLoanWithGfeArchiveApplied.ByPassFieldSecurityCheck = parameters.ByPassFieldSecurityCheck;

            m_exportSource = parameters.UseCustomExportOptions ? parameters.ExportSource : m_dataLoan.sSettlementChargesExportSource;
            mxGfe = m_exportSource == E_SettlementChargesExportSource.GFE;

            // For TRID, exclude data points based on includeGfeDataForDocMagicComparison. Otherwise use existing behavior.
            m_includeGfeDataForDocMagicComparison = parameters.UseCustomExportOptions ? parameters.IncludeGfeDataForDocMagicComparison : m_dataLoan.sIncludeGfeDataForDocMagicComparison;
            miGfe = m_use2015DataLayer ? false : (!mxGfe && m_includeGfeDataForDocMagicComparison);

            m_feeDiscrepancies = new List<string>();

            slid = parameters.LoanNumber;
            Loan loan = CreateLoan();

            feeDiscrepancies = new List<string>(m_feeDiscrepancies);

            return loan;
        }

        private Loan CreateLoan()
        {
            Loan loan = new Loan();
            loan.MismoVersionIdentifier = E_LoanMismoVersionIdentifier._2_6;
            loan.ClosingDocuments = CreateClosingDocuments(); // this sets a bool create application uses...
            loan.Application = CreateApplication();
            return loan;
        }

        private Application CreateApplication()
        {
            Application application = new Application();
            application.AdditionalCaseData = CreateAdditionalCaseData();
            application.InterviewerInformation = CreateInterviewerInformation();
            application.LoanProductData = CreateLoanProductData();
            application.MortgageTerms = CreateMortgageTerms();
            application.Property = CreateProperty();
            application.TransactionDetail = CreateTransactionDetail();
            application.UrlaTotalList.Add(CreateUrlaTotal());

            /// 10/15/2015 BS - Case 228946. Add MI and escrows to ignore fee list
            feeIgnoreList = new List<Guid>();
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId);
            // 10/8/2015 BS - Case 228348. Add Aggregate adjustment and Prepaid Interest to ignore fee list
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId);

            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId);
            feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId);

            int nApps = m_dataLoan.nApps;
            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            
            for (int borrowerIndex = 0; borrowerIndex < nApps; borrowerIndex++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(borrowerIndex);

                if (dataApp.aBIsValidNameSsn || dataApp.aCIsValidNameSsn)
                {

                    #region Add Assets
                    IAssetCollection assetCollection = dataApp.aAssetCollection;
                    application.AssetList.Add(CreateAsset(assetCollection.GetBusinessWorth(false), dataApp.aBMismoId));
                    application.AssetList.Add(CreateAsset(assetCollection.GetLifeInsurance(false), dataApp.aBMismoId));
                    application.AssetList.Add(CreateAsset(assetCollection.GetRetirement(false), dataApp.aBMismoId));
                    application.AssetList.Add(CreateAsset(assetCollection.GetCashDeposit1(false), dataApp.aBMismoId));
                    application.AssetList.Add(CreateAsset(assetCollection.GetCashDeposit2(false), dataApp.aBMismoId));
                    for (int i = 0; i < assetCollection.CountRegular; i++)
                    {
                        application.AssetList.Add(CreateAsset(assetCollection.GetRegularRecordAt(i), dataApp));
                    }
                    #endregion

                    #region Add Liabilities
                    ILiaCollection liaCollection = dataApp.aLiaCollection;

                    application.LiabilityList.Add(CreateLiability(liaCollection.GetAlimony(false), dataApp.aBMismoId, borrowerIndex));
                    application.LiabilityList.Add(CreateLiability(liaCollection.GetChildSupport(false), dataApp.aBMismoId, borrowerIndex));
                    application.LiabilityList.Add(CreateLiability(liaCollection.GetJobRelated1(false), dataApp.aBMismoId, borrowerIndex));
                    application.LiabilityList.Add(CreateLiability(liaCollection.GetJobRelated2(false), dataApp.aBMismoId, borrowerIndex));
                    for (int recordIndex = 0; recordIndex < liaCollection.CountRegular; recordIndex++)
                    {
                        application.LiabilityList.Add(CreateLiability(liaCollection.GetRegularRecordAt(recordIndex), dataApp, borrowerIndex));
                    }
                    #endregion

                    #region Add REO Properties
                    var reCollection = dataApp.aReCollection;

                    for (int recordIndex = 0; recordIndex < reCollection.CountRegular; recordIndex++)
                    {
                        application.ReoPropertyList.Add(CreateReoProperty(reCollection.GetRegularRecordAt(recordIndex), dataApp, borrowerIndex));
                    }
                    #endregion

                    // Create the borrowers from this dataApp
                    var borrowers = CreateBorrowers(dataApp, interviewer);
                    application.BorrowerList.AddRange(borrowers);
                }
            }
            List<TitleBorrower> titleBorrowers = m_dataLoan.sTitleBorrowers;
            for (int titleBorrowerIndex = 0; titleBorrowerIndex < titleBorrowers.Count; titleBorrowerIndex++)
            {
                application.BorrowerList.Add(CreateTitleBorrower(titleBorrowerIndex, titleBorrowers[titleBorrowerIndex]));
            }

            if (application.BorrowerList.Count == 0)
            { // There needs to be at least one borrower element or else the doc will fail validation
                application.BorrowerList.Add(CreateBorrower_Empty());
            }

            // OPM 82549: To pass over the name and address of the relative not living with the veteran,
            //   we will need to use the _NEAREST_LIVING_RELATIVE element and associate it with the
            //   primary BORROWER element when the loan type is VA or FHA
            if (m_dataLoan.sLT == E_sLT.VA || m_dataLoan.sLT == E_sLT.FHA)
            {
                application.BorrowerList.First().NearestLivingRelative = CreateNearestLivingRelative();
            }

            if (nApps > 0)
            {
                application.GovernmentLoan = CreateGovernmentLoan(m_dataLoan.GetAppData(0));
                application.LoanPurpose = CreateLoanPurpose(m_dataLoan.GetAppData(0));
                application.LoanUnderwritingList.Add(CreateLoanUnderwriting(m_dataLoan.GetAppData(0)));
            }

            // Borrower last = application.BorrowerList.Last();
            // OPM 115667: Removed code for dealing with commas added in OPM 104257
            if (!string.IsNullOrEmpty(application.LoanPurpose.GseTitleMannerHeldDescription))
            {
                application.LoanPurpose.GseTitleMannerHeldDescription = ", " + application.LoanPurpose.GseTitleMannerHeldDescription;
            }

            application.GovernmentReporting = CreateGovernmentReporting();
            application.LoanQualification = CreateLoanQualification();
            application.DownPaymentList.Add(CreateDownPayment());
            application.EscrowAccountSummary = CreateEscrowAccountSummary();

            if (m_use2015DataLayer)
            {
                // 10/15/2015 BS - Change method call order here because ignore fee list requires escrow fee first before populate RespaFeeList
                this.PopulateEscrowList_2015DataLayer(application.EscrowList);
                this.PopulateRespaFeeList_2015DataLayer(application.RespaFeeList, application.EscrowList);
            }
            else
            {
                this.PopulateRespaFeeList(application.RespaFeeList);
                this.PopulateEscrowList(application.EscrowList);
            }

            this.PopulateProposedHousingExpenseList(application.ProposedHousingExpenseList);

            if (m_dataLoan.sLT != E_sLT.VA)
            {
                application.MiData = CreateMiData();
            }

            //application.Id = null;
            //application.DataInformation = CreateDataInformation();
            //application.AffordableLending = CreateAffordableLending();

            if (!string.IsNullOrEmpty(m_dataLoan.sMersMin))
            {
                application.Mers = CreateMers();
            }

            CreateLoanOriginator(application);
            //application.TitleHolderList.Add(CreateTitleHolder());
            //application.InvestorFeatureList.Add(CreateInvestorFeature());
            //application.LoanOriginationSystem = CreateLoanOriginationSystem();

            if (m_dataLoan.sLT == E_sLT.VA)
            {
                application.RelatedLoanList.Add(CreateRelatedLoan());
            }

            return application;
        }

        /// <summary>
        /// Populates the Escrow list with GFE or TRID data using the 2015 data layer.
        /// </summary>
        /// <param name="escrowList">The escrow list to be populated.</param>
        private void PopulateEscrowList_2015DataLayer(List<Escrow> escrowList)
        {
            foreach (var expense in m_dataLoanWithGfeArchiveApplied.sHousingExpenses.ExpensesToUse)
            {
                if (expense.IsEscrowedAtClosing.Equals(E_TriState.Yes))
                {
                    escrowList.Add(CreateEscrow_2015DataLayer(expense));
                }
            }
        }

        /// <summary>
        /// Creates an escrow object with new datalayer information.
        /// </summary>
        /// <param name="expense">Information on the escrow expense.</param>
        /// <returns>An <see cref="Escrow"/> object.</returns>
        private Escrow CreateEscrow_2015DataLayer(BaseHousingExpense expense)
        {
            Escrow escrow = new Escrow();

            escrow.PaidTo = CreatePaidTo(expense.PaidTo);
            escrow.SpecifiedHud1LineNumber = expense.LineNumAsString;
            escrow.ItemType = ToMismo_EscrowExpenseType(expense.HousingExpenseType);
            escrow.ItemTypeOtherDescription = expense.ExpenseDescription;
            escrow.AccountSummary = CreateAccountSummary(expense.CushionMonths);

            BorrowerClosingCostFee prepaidFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionF);
            if (prepaidFee != null)
            {
                escrow.PremiumAmount = expense.PrepaidAmt_rep;
                escrow.PremiumDurationMonthsCount = expense.PrepaidMonths_rep;
                escrow.GfeDisclosedPremiumAmount = expense.PrepaidAmt_rep;

                var firstPayment = prepaidFee.Payments.FirstOrDefault();
                if (firstPayment != null)
                {
                    escrow.PremiumPaidByType = ToMismo_2015DataLayerEscrowPremiumPaidBy(firstPayment.PaidByT);
                }
            }

            escrow.MonthlyPaymentAmount = expense.MonthlyAmtTotal_rep;
            escrow.CollectedNumberOfMonthsCount = expense.ReserveMonths_rep;
            escrow.AnnualPaymentAmount = expense.AnnualAmt_rep;

            BorrowerClosingCostFee reserveFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionG);
            // 10/15/2015 BS - add escrow to fee to ignore list
            if (reserveFee != null && reserveFee.ClosingCostFeeTypeId != Guid.Empty)
            {
                feeIgnoreList.Add(reserveFee.ClosingCostFeeTypeId);
            }

            if (reserveFee != null)
            {
                var firstPayment = reserveFee.Payments.FirstOrDefault();
                if (firstPayment != null)
                {
                    escrow.PaidByType = ToMismo_2015DataLayerEscrowPaidBy(firstPayment.PaidByT);
                }
            }

            int numberOfPayments = 1;
            if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                foreach (var disbursement in expense.ActualDisbursementsList)
                {
                    escrow.PaymentsList.Add(CreateEscrowPayments_2015DataLayer(disbursement, numberOfPayments++));
                }
            }
            else
            {
                foreach (var disbursement in expense.ProjectedDisbursements)
                {
                    escrow.PaymentsList.Add(CreateEscrowPayments_2015DataLayer(disbursement, numberOfPayments++));
                }
            }

            return escrow;
        }

        /// <summary>
        /// Populates the escrow list with GFE or TRID data using the 2015 data layer.
        /// </summary>
        /// <param name="disbursement"></param>
        /// <returns></returns>
        private Payments CreateEscrowPayments_2015DataLayer(SingleDisbursement disbursement, int sequenceIdentifier)
        {
            var payments = new Payments();
            payments.DueDate = disbursement.DueDate_rep;
            payments.PaymentAmount = disbursement.DisbursementAmt_rep;
            payments.SequenceIdentifier = sequenceIdentifier.ToString();

            return payments;
        }

        /// <summary>
        /// Gets the prepaid fee associated with a housing expense. The fee will either be from 
        /// section 900 (GFE) or section F (TRID).
        /// </summary>
        /// <param name="expense">The housing expense associated with a closing cost.</param>
        /// <returns>The <see cref="BorrowerClosingCostFee"/> object representing the prepaid fee associated with
        /// the housing expense, or null if no such fee exists.</returns>
        public BorrowerClosingCostFee GetRelated900OrFFee(BaseHousingExpense expense)
        {
            Func<BaseClosingCostFee, bool> borrowerSetFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(m_dataLoan.sClosingCostSet,
                                                                                                                        false,
                                                                                                                        m_dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                                                        m_dataLoan.sDisclosureRegulationT,
                                                                                                                        m_dataLoan.sGfeIsTPOTransaction);

            foreach (BorrowerClosingCostFee fee in m_dataLoan.sClosingCostSet.GetFees(borrowerSetFilter))
            {
                if (expense.HousingExpenseType == E_HousingExpenseTypeT.CondoHO6Insurance &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.WindstormInsurance &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.HazardInsurance &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.FloodInsurance &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.RealEstateTaxes &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.SchoolTaxes &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes1 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes2 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes3 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes4 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.HomeownersAsscDues &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.GroundRent &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId)
                {
                    return fee;
                }
                else if (expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1008 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId)
                {
                    return fee;
                }
                else if (expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1009 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId)
                {
                    return fee;
                }
                else if (expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1010 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId)
                {
                    return fee;
                }
                else if (expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1011 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId)
                {
                    return fee;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the reserve fee associated with a housing expense. The fee will either be from 
        /// section 1000 (GFE) or section G (TRID).
        /// </summary>
        /// <param name="expense">The housing expense associated with a closing cost.</param>
        /// <returns>The <see cref="BorrowerClosingCostFee"/> object representing the fee associated with the housing
        /// expense, or null if no such fee exists.</returns>
        public BorrowerClosingCostFee GetRelated1000OrGFee(BaseHousingExpense expense)
        {
            Func<BaseClosingCostFee, bool> borrowerSetFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(m_dataLoan.sClosingCostSet,
                                                                                                                        false,
                                                                                                                        m_dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                                                        m_dataLoan.sDisclosureRegulationT,
                                                                                                                        m_dataLoan.sGfeIsTPOTransaction);

            foreach (BorrowerClosingCostFee fee in m_dataLoan.sClosingCostSet.GetFees(borrowerSetFilter))
            {
                if (expense.HousingExpenseType == E_HousingExpenseTypeT.CondoHO6Insurance &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000CondoHo6InsuranceFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.WindstormInsurance &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000WindStormInsuranceFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.HazardInsurance &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000HazardInsuranceReserveFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.FloodInsurance &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000FloodInsuranceReserveFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.RealEstateTaxes &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000TaxReserveFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.SchoolTaxes &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000SchoolTaxFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes1 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax1FeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes2 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax2FeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes3 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax3FeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.OtherTaxes4 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000OtherTax4FeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.HomeownersAsscDues &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000HomeOwnerAssociationDuesFeeTypeId)
                {
                    return fee;
                }
                else if (expense.HousingExpenseType == E_HousingExpenseTypeT.GroundRent &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000GroundRentFeeTypeId)
                {
                    return fee;
                }
                else if (expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1008 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000Custom1FeeTypeId)
                {
                    return fee;
                }
                else if (expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1009 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000Custom2FeeTypeId)
                {
                    return fee;
                }
                else if (expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1010 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000Custom3FeeTypeId)
                {
                    return fee;
                }
                else if (expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Line1011 &&
                    fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000Custom4FeeTypeId)
                {
                    return fee;
                }
            }

            return null;
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid an escrow premium.
        /// </summary>
        /// <param name="paidBy">The entity who paid the escrow premium.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidByType"/> value.</returns>
        public E_EscrowPremiumPaidByType ToMismo_2015DataLayerEscrowPremiumPaidBy(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_EscrowPremiumPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_EscrowPremiumPaidByType.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_EscrowPremiumPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return E_EscrowPremiumPaidByType.Other;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return E_EscrowPremiumPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid an escrow reserve.
        /// </summary>
        /// <param name="paidBy">The entity who paid the escrow reserve.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidByType"/> value.</returns>
        public E_EscrowPaidByType ToMismo_2015DataLayerEscrowPaidBy(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_EscrowPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_EscrowPaidByType.LenderPremium;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_EscrowPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return E_EscrowPaidByType.Other;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return E_EscrowPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Populates the RESPA fee list with GFE or TRID data using the 2015 data layer.
        /// </summary>
        /// <param name="respaFeeList">The RESPA fee list to populate.</param>
        /// <param name="escrowList">The escrow list to be populated with fees marked as B11 that are not paid outside of closing.</param>
        private void PopulateRespaFeeList_2015DataLayer(List<Mismo.Closing2_6.RespaFee> respaFeeList, List<Escrow> escrowList)
        {
            Func<BaseClosingCostFee, bool> borrowerSetFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(m_dataLoanWithGfeArchiveApplied.sClosingCostSet,
                                                                                                                        true,
                                                                                                                        m_dataLoanWithGfeArchiveApplied.sOriginatorCompensationPaymentSourceT,
                                                                                                                        m_dataLoanWithGfeArchiveApplied.sDisclosureRegulationT,
                                                                                                                        m_dataLoanWithGfeArchiveApplied.sGfeIsTPOTransaction);

            var feeSet = m_dataLoanWithGfeArchiveApplied.sClosingCostSet.GetFees(borrowerSetFilter);
            foreach (var closingCost in feeSet)
            {
                if (this.IsApplicableRespaFeeItem_2015Datalayer(closingCost))
                {
                    var fee = CreateRespaFee_2015DataLayer((BorrowerClosingCostFee)closingCost);

                    if (fee != null)
                    {
                        respaFeeList.Add(fee);
                    }
                }
                else
                {
                    // Certain fees should be sent to DocMagic as ESCROW elements.
                    escrowList.Add(this.CreateEscrowFromFee_2015DataLayer((BorrowerClosingCostFee)closingCost));
                }
            }

            // Create dummy RespaFee elements for any fees that have been deleted after being archived.
            var cocArchive = m_dataLoanWithGfeArchiveApplied.sLastDisclosedClosingCostCoCArchive;
            if (cocArchive != null)
            {
                foreach (var changedFee in cocArchive.Fees)
                {
                    if (!feeSet.Any(f => f.ClosingCostFeeTypeId == changedFee.FeeId))
                    {
                        foreach (var archive in m_dataLoanWithGfeArchiveApplied.sClosingCostArchive)
                        {
                            // We don't need any specific amount info, just the generic fee information. So any archive
                            // with the applicable FeeTypeID will suffice.
                            var archiveFeeSet = archive.GetClosingCostSet();
                            if (archiveFeeSet.GetFees(null).Any(f => f.ClosingCostFeeTypeId == changedFee.FeeId))
                            {
                                var baseFee = archiveFeeSet.FindFeeByTypeId(changedFee.FeeId);
                                if (baseFee != null)
                                {
                                    respaFeeList.Add(CreateRespaFee_Empty(baseFee));
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Determines whether the specified <paramref name="fee"/> should be
        /// added to the list of RESPA fee items.
        /// </summary>
        /// <param name="fee">
        /// The fee to check.
        /// </param>
        /// <returns>
        /// True if the fee is applicable, false otherwise.
        /// </returns>
        private bool IsApplicableRespaFeeItem_2015Datalayer(BaseClosingCostFee fee)
        {
            var borrowerFee = fee as BorrowerClosingCostFee;
            if (borrowerFee != null && borrowerFee.GfeSectionT == E_GfeSectionT.B11)
            {
                return borrowerFee.Payments.All(payment => 
                    payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing);
            }

            return true;
        }

        /// <summary>
        /// Creates an ESCROW element using the specified <paramref name="fee"/>.
        /// </summary>
        /// <param name="fee">
        /// The fee to populate data.
        /// </param>
        /// <returns>
        /// An ESCROW element populated with data from the specified <paramref name="fee"/>.
        /// </returns>
        /// <remarks>
        /// This method is meant to handle B11 fees, which DocMagic expects to be sent 
        /// in ESCROW elements marked as "Hazard Insurance" items.
        /// </remarks>
        private Escrow CreateEscrowFromFee_2015DataLayer(BorrowerClosingCostFee fee)
        {
            var escrow = new Escrow()
            {
                SpecifiedHud1LineNumber = fee.HudLine_rep,
                ItemType = E_EscrowItemType.HazardInsurance,
                ItemTypeOtherDescription = fee.Description,
                PremiumAmount = fee.TotalAmount_rep
            };

            var payment = fee.Payments.FirstOrDefault();
            if (payment != null)
            {
                escrow.PremiumPaidByType = ToMismo_2015DataLayerEscrowPremiumPaidBy(payment.PaidByT);
            }

            return escrow;
        }

        /// <summary>
        /// Creates a dummy closing cost fee with an amount of $0. Should be used when a previously archived
        /// fee is deleted in a CoC.
        /// </summary>
        /// <param name="closingCost">The closing cost fee information.</param>
        /// <returns>A <see cref="Mismo.Closing2_6.RespaFee"/> object.</returns>
        private Mismo.Closing2_6.RespaFee CreateRespaFee_Empty(BaseClosingCostFee closingCost)
        {
            if (closingCost.HudLine == 901 || (feeIgnoreList != null && feeIgnoreList.Any(id => id == (closingCost.SourceFeeTypeId ?? closingCost.ClosingCostFeeTypeId))))
            {
                return null;
            }

            var fee = new Mismo.Closing2_6.RespaFee();

            fee.RespaSectionClassificationType = GetRespaSectionClassificationType(closingCost.HudLine_rep);
            fee.SpecifiedHudLineNumber = closingCost.HudLine_rep;
            fee.Type = ToMismo(closingCost.MismoFeeT);
            fee.TypeOtherDescription = closingCost.Description;
            fee.GfeAggregationType = ToMismo(closingCost.GfeSectionT);
            fee.SpecifiedFixedAmount = "0.00";
            fee.TotalPercent = "0";
            fee.PaidToType = ToMismo(closingCost.Beneficiary);
            fee.TotalAmount = "0.00";
            fee.Id = "_" + closingCost.ClosingCostFeeTypeId.ToString();

            return fee;
        }

        /// <summary>
        /// Creates an object with new datalater information on a closing cost fee.
        /// </summary>
        /// <param name="closingCost">The closing cost fee information.</param>
        /// <returns>A <see cref="Mismo.Closing2_6.RespaFee"/> object.</returns>
        private Mismo.Closing2_6.RespaFee CreateRespaFee_2015DataLayer(BorrowerClosingCostFee closingCost)
        {
            // 10/8/2015 BS - Case 228348. Omit prepaid interest from Respa Fee
            // 10/15/2015 BS - Case 228946. Exclude MI and Escrow
            if (closingCost.HudLine == 901 || (feeIgnoreList != null && feeIgnoreList.Any(id => id == (closingCost.SourceFeeTypeId ?? closingCost.ClosingCostFeeTypeId))))
            {
                return null;
            }

            var fee = new Mismo.Closing2_6.RespaFee();

            fee.RespaSectionClassificationType = GetRespaSectionClassificationType(closingCost.HudLine_rep);
            fee.SpecifiedHudLineNumber = closingCost.HudLine_rep;
            fee.Type = ToMismo(closingCost.MismoFeeT);
            fee.TypeOtherDescription = closingCost.Description;
            fee.GfeAggregationType = ToMismo(closingCost.GfeSectionT);
            fee.SpecifiedFixedAmount = closingCost.BaseAmount_rep;
            fee.TotalPercent = closingCost.Percent_rep;
            fee.BorrowerChosenProviderIndicator = closingCost.DidShop ? E_YNIndicator.Y : E_YNIndicator.N;
            fee.PaidTo = CreatePaidTo(closingCost.BeneficiaryDescription);
            fee.PaidToType = ToMismo(closingCost.Beneficiary);

            if (m_isDisclosurePackage)
            {
                // We're using archived data and need to pull the TotalAmount value from the live loan object.
                fee.GfeDisclosedAmount = closingCost.TotalAmount_rep;

                Func<BaseClosingCostFee, bool> initialFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(m_dataLoan.sClosingCostSet,
                                                                                                                        false,
                                                                                                                        m_dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                                                        m_dataLoan.sDisclosureRegulationT,
                                                                                                                        m_dataLoan.sGfeIsTPOTransaction);
                Func<BaseClosingCostFee, bool> liveFeeFilter = feeToFilter => initialFilter(feeToFilter) && feeToFilter.ClosingCostFeeTypeId == closingCost.ClosingCostFeeTypeId;
                
                BorrowerClosingCostFee liveFee = (BorrowerClosingCostFee)m_dataLoan.sClosingCostSet.GetFees(liveFeeFilter).FirstOrDefault();
                fee.TotalAmount = liveFee != null ? liveFee.TotalAmount_rep : closingCost.TotalAmount_rep;
            }
            else
            {
                // We're using live data and need to pull the GfeDisclosedAmount value from the last archive.
                ClosingCostArchive lastDisclosedArchive = m_dataLoan.sLastDisclosedClosingCostArchive;

                if (lastDisclosedArchive != null)
                {
                    // This filter is used for a Set made from an archive AND it doesn't want to include the originator compensation fee.
                    Func<BaseClosingCostFee, bool> archivedFeeFilter = feeToFilter => ClosingCostSetUtils.ExOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostFee)feeToFilter) &&
                                                                                      feeToFilter.ClosingCostFeeTypeId == closingCost.ClosingCostFeeTypeId;
                    BorrowerClosingCostFee archivedFee = (BorrowerClosingCostFee)lastDisclosedArchive.GetClosingCostSet().GetFees(archivedFeeFilter).FirstOrDefault();
                    if (archivedFee != null)
                    {
                        fee.GfeDisclosedAmount = m_dataLoan.m_convertLos.ToDecimalString(archivedFee.TotalAmount, FormatDirection.ToRep);
                    }
                }

                fee.TotalAmount = closingCost.TotalAmount_rep;
            }

            // The ID field value cannot begin with a number, so we prepend with an underscore to prevent errors.
            fee.Id = "_" + closingCost.ClosingCostFeeTypeId.ToString();

            if (m_exportTridData)
            {
                fee.ResponsiblePartyType = ToMismo(closingCost.GfeResponsiblePartyT);
            }

            foreach (var payment in closingCost.Payments)
            {
                fee.PaymentList.Add(CreateRespaFeePayment_2015DataLayer(payment, closingCost.IsApr, closingCost.IsFhaAllowable));
            }

            return fee;
        }

        /// <summary>
        /// Retrieves the RESPA section classification type based on a fee's HUD line.
        /// </summary>
        /// <param name="hudLine">The HUD line for a fee.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeRespaSectionClassificationType"/> value.</returns>
        private E_RespaFeeRespaSectionClassificationType GetRespaSectionClassificationType(string hudLine)
        {
            if (StaticMethodsAndExtensions.IsInRange(hudLine, 100, 199))
            {
                return E_RespaFeeRespaSectionClassificationType._100_GrossAmountDueFromBorrower;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 200, 299))
            {
                return E_RespaFeeRespaSectionClassificationType._200_AmountsPaidByOrInBehalfOfBorrower;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 300, 399))
            {
                return E_RespaFeeRespaSectionClassificationType._300_CashAtSettlementFromToBorrower;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 400, 499))
            {
                return E_RespaFeeRespaSectionClassificationType._400_GrossAmountDueToSeller;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 500, 599))
            {
                return E_RespaFeeRespaSectionClassificationType._500_ReductionsInAmountDueSeller;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 600, 699))
            {
                return E_RespaFeeRespaSectionClassificationType._600_CashAtSettlementToFromSeller;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 700, 799))
            {
                return E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 800, 899))
            {
                return E_RespaFeeRespaSectionClassificationType._800_LoanFees;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 900, 999))
            {
                return E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1000, 1099))
            {
                return E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1100, 1199))
            {
                return E_RespaFeeRespaSectionClassificationType._1100_TitleCharges;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1200, 1299))
            {
                return E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges;
            }
            else if (StaticMethodsAndExtensions.IsInRange(hudLine, 1300, 1399))
            {
                return E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges;
            }
            else
            {
                return E_RespaFeeRespaSectionClassificationType.Undefined;
            }
        }

        /// <summary>
        /// Creates an object with new datalayer information on a closing cost fee payment.
        /// </summary>
        /// <param name="payment">The payment information.</param>
        /// <param name="isApr">Indicates whether the fee is reflected in the APR.</param>
        /// <param name="isFhaAllowable">Indicates whether the fee is an allowable FHA closing cost.</param>
        /// <returns>A <see cref="Payment"/> object.</returns>
        private Payment CreateRespaFeePayment_2015DataLayer(LoanClosingCostFeePayment payment, bool isApr, bool isFhaAllowable)
        {
            var respaPayment = new Payment();
            respaPayment.Amount = payment.Amount_rep;
            respaPayment.IncludedInAprIndicator = isApr ? E_YNIndicator.Y : E_YNIndicator.N;
            respaPayment.PaidOutsideOfClosingIndicator = payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing ? E_YNIndicator.Y : E_YNIndicator.N;
            respaPayment.FinancedIndicator = payment.IsFinanced ? E_YNIndicator.Y : E_YNIndicator.N;
            respaPayment.PaidByType = ToMismo(payment.PaidByT);
            respaPayment.AllowableFhaClosingCostIndicator = isFhaAllowable ? E_YNIndicator.Y : E_YNIndicator.N;

            return respaPayment;
        }

        /// <summary>
        /// Populates the proposed housing expense list with old datalayer information.
        /// </summary>
        /// <param name="proposedHousingExpenseList">The proposed housing expense list to be populated.</param>
        private void PopulateProposedHousingExpenseList(List<ProposedHousingExpense> proposedHousingExpenseList)
        {
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.MI, m_dataLoan.sProMIns_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.HazardInsurance, m_dataLoan.sProHazIns_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, m_dataLoan.sProSecondMPmt_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense, m_dataLoan.sProOHExp_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.RealEstateTax, m_dataLoan.sProRealETx_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, m_dataLoan.sProHoAssocDues_rep));
            proposedHousingExpenseList.Add(CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, m_dataLoan.sProFirstMPmt_rep));
        }

        /// <summary>
        /// Retrieves a value indicating the entity who paid an escrow item.
        /// </summary>
        /// <param name="paidBy">The entity who paid the expense.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidByType"/> value.</returns>
        public E_EscrowPaidByType ToMismo(E_DisbursementPaidByT? paidBy)
        {
            switch (paidBy)
            {
                case E_DisbursementPaidByT.Borrower:
                    return E_EscrowPaidByType.Buyer;
                case E_DisbursementPaidByT.Lender:
                    return E_EscrowPaidByType.LenderPremium;
                case E_DisbursementPaidByT.Seller:
                    return E_EscrowPaidByType.Seller;
                case E_DisbursementPaidByT.EscrowImpounds:
                case null:
                    return E_EscrowPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Retrieves the proposed housing expense type.
        /// </summary>
        /// <param name="type">The type of the housing expense.</param>
        /// <returns>The corresponding <see cref="E_ProposedHousingExpenseHousingExpenseType"/> value.</returns>
        private E_ProposedHousingExpenseHousingExpenseType ToMismo_HousingExpenseType(E_HousingExpenseTypeT type)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.GroundRent:
                    return E_ProposedHousingExpenseHousingExpenseType.GroundRent;
                case E_HousingExpenseTypeT.HazardInsurance:
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return E_ProposedHousingExpenseHousingExpenseType.HazardInsurance;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                    return E_ProposedHousingExpenseHousingExpenseType.RealEstateTax;
                case E_HousingExpenseTypeT.WindstormInsurance:
                case E_HousingExpenseTypeT.SchoolTaxes:
                case E_HousingExpenseTypeT.FloodInsurance:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                    return E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense;
                case E_HousingExpenseTypeT.Unassigned:
                    return E_ProposedHousingExpenseHousingExpenseType.Undefined;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Retrieves the escrow item type corresponding to the housing expense type.
        /// </summary>
        /// <param name="type">The type of a housing expense.</param>
        /// <returns>A</returns>
        public E_EscrowItemType ToMismo_EscrowExpenseType(E_HousingExpenseTypeT type)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.HazardInsurance:
                    return E_EscrowItemType.HazardInsurance;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return E_EscrowItemType.FloodInsurance;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return E_EscrowItemType.WindstormInsurance;
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return E_EscrowItemType.SchoolPropertyTax;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                case E_HousingExpenseTypeT.GroundRent:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.Unassigned:
                    return E_EscrowItemType.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Retrieves the value representing the entity a fee was paid to.
        /// </summary>
        /// <param name="role">The agent to whom a fee will be paid.</param>
        /// <returns>The corresponding <see cref="E_RespaFeePaidToType"/> value.</returns>
        private E_RespaFeePaidToType ToMismo(E_AgentRoleT role)
        {
            var agent = m_dataLoan.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (Enum.Equals(role, E_AgentRoleT.Broker))
            {
                return E_RespaFeePaidToType.Broker;
            }
            else if (Enum.Equals(role, E_AgentRoleT.Investor))
            {
                return E_RespaFeePaidToType.Investor;
            }
            else if (Enum.Equals(role, E_AgentRoleT.Lender))
            {
                return E_RespaFeePaidToType.Lender;
            }
            else if (agent.IsLender)
            {
                return E_RespaFeePaidToType.Lender;
            }
            else if (agent.IsOriginator)
            {
                return E_RespaFeePaidToType.Broker;
            }
            else
            {
                return E_RespaFeePaidToType.Other;
            }
        }

        /// <summary>
        /// Retrieves the responsible party for a closing cost fee.
        /// </summary>
        /// <param name="type">The responsible party for a fee.</param>
        /// <returns>The corresponding <see cref=" E_RespaFeeResponsiblePartyType"/> value.</returns>
        private E_RespaFeeResponsiblePartyType ToMismo(E_GfeResponsiblePartyT type)
        {
            switch (type)
            {
                case E_GfeResponsiblePartyT.Buyer:
                    return E_RespaFeeResponsiblePartyType.Buyer;
                case E_GfeResponsiblePartyT.Seller:
                    return E_RespaFeeResponsiblePartyType.Seller;
                case E_GfeResponsiblePartyT.Broker:
                case E_GfeResponsiblePartyT.Lender:
                case E_GfeResponsiblePartyT.LeaveBlank:
                    return E_RespaFeeResponsiblePartyType.Undefined;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Retrieves the RESPA fee type closest to the given closing cost fee type.
        /// </summary>
        /// <param name="feeType">A closing cost fee type.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeType"/> value.</returns>
        private E_RespaFeeType ToMismo(E_ClosingCostFeeMismoFeeT feeType)
        {
            switch (feeType)
            {
                case E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee:
                    return E_RespaFeeType._203KArchitecturalAndEngineeringFee;
                case E_ClosingCostFeeMismoFeeT._203KConsultantFee:
                    return E_RespaFeeType._203KConsultantFee;
                case E_ClosingCostFeeMismoFeeT._203KDiscountOnRepairs:
                    return E_RespaFeeType._203KDiscountOnRepairs;
                case E_ClosingCostFeeMismoFeeT._203KInspectionFee:
                    return E_RespaFeeType._203KInspectionFee;
                case E_ClosingCostFeeMismoFeeT._203KPermits:
                    return E_RespaFeeType._203KPermits;
                case E_ClosingCostFeeMismoFeeT._203KSupplementalOriginationFee:
                    return E_RespaFeeType._203KSupplementalOriginationFee;
                case E_ClosingCostFeeMismoFeeT._203KTitleUpdate:
                    return E_RespaFeeType._203KTitleUpdate;
                case E_ClosingCostFeeMismoFeeT.AbstractOrTitleSearchFee:
                    return E_RespaFeeType.AbstractOrTitleSearchFee;
                case E_ClosingCostFeeMismoFeeT.AmortizationFee:
                    return E_RespaFeeType.AmortizationFee;
                case E_ClosingCostFeeMismoFeeT.ApplicationFee:
                    return E_RespaFeeType.ApplicationFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFee:
                    return E_RespaFeeType.AppraisalFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentFee:
                    return E_RespaFeeType.AssignmentFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee:
                    return E_RespaFeeType.AssignmentRecordingFee;
                case E_ClosingCostFeeMismoFeeT.AssumptionFee:
                    return E_RespaFeeType.AssumptionFee;
                case E_ClosingCostFeeMismoFeeT.AttorneyFee:
                    return E_RespaFeeType.AttorneyFee;
                case E_ClosingCostFeeMismoFeeT.BondReviewFee:
                    return E_RespaFeeType.BondReviewFee;
                case E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CityDeedTaxStampFee:
                    return E_RespaFeeType.CityCountyDeedTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CityMortgageTaxStampFee:
                    return E_RespaFeeType.CityCountyMortgageTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.CLOAccessFee:
                    return E_RespaFeeType.CLOAccessFee;
                case E_ClosingCostFeeMismoFeeT.CommitmentFee:
                    return E_RespaFeeType.CommitmentFee;
                case E_ClosingCostFeeMismoFeeT.CopyFaxFee:
                    return E_RespaFeeType.CopyFaxFee;
                case E_ClosingCostFeeMismoFeeT.CourierFee:
                    return E_RespaFeeType.CourierFee;
                case E_ClosingCostFeeMismoFeeT.CreditReportFee:
                    return E_RespaFeeType.CreditReportFee;
                case E_ClosingCostFeeMismoFeeT.DeedRecordingFee:
                    return E_RespaFeeType.DeedRecordingFee;
                case E_ClosingCostFeeMismoFeeT.DocumentaryStampFee:
                    return E_RespaFeeType.DocumentaryStampFee;
                case E_ClosingCostFeeMismoFeeT.DocumentPreparationFee:
                    return E_RespaFeeType.DocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.EscrowWaiverFee:
                    return E_RespaFeeType.EscrowWaiverFee;
                case E_ClosingCostFeeMismoFeeT.FloodCertification:
                    return E_RespaFeeType.FloodCertification;
                case E_ClosingCostFeeMismoFeeT.GeneralCounselFee:
                    return E_RespaFeeType.GeneralCounselFee;
                case E_ClosingCostFeeMismoFeeT.InspectionFee:
                    return E_RespaFeeType.InspectionFee;
                case E_ClosingCostFeeMismoFeeT.LoanDiscountPoints:
                    return E_RespaFeeType.LoanDiscountPoints;
                case E_ClosingCostFeeMismoFeeT.LoanOriginationFee:
                    return E_RespaFeeType.LoanOriginationFee;
                case E_ClosingCostFeeMismoFeeT.ModificationFee:
                    return E_RespaFeeType.ModificationFee;
                case E_ClosingCostFeeMismoFeeT.MortgageBrokerFee:
                    return E_RespaFeeType.MortgageBrokerFee;
                case E_ClosingCostFeeMismoFeeT.MortgageRecordingFee:
                    return E_RespaFeeType.MortgageRecordingFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateFee:
                    return E_RespaFeeType.MunicipalLienCertificateFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee:
                    return E_RespaFeeType.MunicipalLienCertificateRecordingFee;
                case E_ClosingCostFeeMismoFeeT.NewLoanAdministrationFee:
                    return E_RespaFeeType.NewLoanAdministrationFee;
                case E_ClosingCostFeeMismoFeeT.NotaryFee:
                    return E_RespaFeeType.NotaryFee;
                case E_ClosingCostFeeMismoFeeT.PestInspectionFee:
                    return E_RespaFeeType.PestInspectionFee;
                case E_ClosingCostFeeMismoFeeT.ProcessingFee:
                    return E_RespaFeeType.ProcessingFee;
                case E_ClosingCostFeeMismoFeeT.RealEstateCommission:
                case E_ClosingCostFeeMismoFeeT.RealEstateCommissionSellersBroker:
                    return E_RespaFeeType.RealEstateCommission;
                case E_ClosingCostFeeMismoFeeT.RedrawFee:
                    return E_RespaFeeType.RedrawFee;
                case E_ClosingCostFeeMismoFeeT.ReinspectionFee:
                    return E_RespaFeeType.ReinspectionFee;
                case E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee:
                    return E_RespaFeeType.ReleaseRecordingFee;
                case E_ClosingCostFeeMismoFeeT.RuralHousingFee:
                    return E_RespaFeeType.RuralHousingFee;
                case E_ClosingCostFeeMismoFeeT.SettlementOrClosingFee:
                    return E_RespaFeeType.SettlementOrClosingFee;
                case E_ClosingCostFeeMismoFeeT.StateDeedTaxStampFee:
                    return E_RespaFeeType.StateDeedTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee:
                    return E_RespaFeeType.StateMortgageTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.SurveyFee:
                    return E_RespaFeeType.SurveyFee;
                case E_ClosingCostFeeMismoFeeT.TaxRelatedServiceFee:
                    return E_RespaFeeType.TaxRelatedServiceFee;
                case E_ClosingCostFeeMismoFeeT.TitleExaminationFee:
                    return E_RespaFeeType.TitleExaminationFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceBinderFee:
                    return E_RespaFeeType.TitleInsuranceBinderFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceFee:
                case E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium:
                case E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium:
                    return E_RespaFeeType.TitleInsuranceFee;
                case E_ClosingCostFeeMismoFeeT.UnderwritingFee:
                    return E_RespaFeeType.UnderwritingFee;
                // OPM 227381 - Both MISMO 2.6 and 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.VAFundingFee:
                    return E_RespaFeeType.VAFundingFee;
                case E_ClosingCostFeeMismoFeeT.MIInitialPremium:
                    return E_RespaFeeType.MIInitialPremium;
                case E_ClosingCostFeeMismoFeeT.ChosenInterestRateCreditOrChargeTotal:
                    return E_RespaFeeType.ChosenInterestRateCreditOrChargeTotal;
                case E_ClosingCostFeeMismoFeeT.OurOriginationChargeTotal:
                    return E_RespaFeeType.OurOriginationChargeTotal;
                case E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal:
                    return E_RespaFeeType.TitleServicesFeeTotal;
                case E_ClosingCostFeeMismoFeeT.MIUpfrontPremium:
                    return E_RespaFeeType.MI_FHAUpfrontPremium;
                case E_ClosingCostFeeMismoFeeT.Undefined:
                case E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation:
                case E_ClosingCostFeeMismoFeeT.WireTransferFee:
                case E_ClosingCostFeeMismoFeeT.TitleEndorsementFee:
                case E_ClosingCostFeeMismoFeeT.SubordinationFee:
                case E_ClosingCostFeeMismoFeeT.SigningAgentFee:
                case E_ClosingCostFeeMismoFeeT.PayoffRequestFee:
                case E_ClosingCostFeeMismoFeeT.MERSRegistrationFee:
                case E_ClosingCostFeeMismoFeeT.EscrowServiceFee:
                case E_ClosingCostFeeMismoFeeT.ElectronicDocumentDeliveryFee:
                case E_ClosingCostFeeMismoFeeT.ClosingProtectionLetterFee:
                case E_ClosingCostFeeMismoFeeT.CertificationFee:
                case E_ClosingCostFeeMismoFeeT.BondFee:
                case E_ClosingCostFeeMismoFeeT.BankruptcyMonitoringFee:
                case E_ClosingCostFeeMismoFeeT.AppraisalFieldReviewFee:
                case E_ClosingCostFeeMismoFeeT.AppraisalDeskReviewFee:
                case E_ClosingCostFeeMismoFeeT.Other:
                // OPM 227381 - MISMO 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.AppraisalManagementCompanyFee:
                case E_ClosingCostFeeMismoFeeT.AsbestosInspectionFee:
                case E_ClosingCostFeeMismoFeeT.AutomatedUnderwritingFee:
                case E_ClosingCostFeeMismoFeeT.AVMFee:
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationDues:
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationDues:
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.CreditDisabilityInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditLifeInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditPropertyInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditUnemploymentInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.DebtCancellationInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.DeedPreparationFee:
                case E_ClosingCostFeeMismoFeeT.DisasterInspectionFee:
                case E_ClosingCostFeeMismoFeeT.DryWallInspectionFee:
                case E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee:
                case E_ClosingCostFeeMismoFeeT.EnvironmentalInspectionFee:
                case E_ClosingCostFeeMismoFeeT.FilingFee:
                case E_ClosingCostFeeMismoFeeT.FoundationInspectionFee:
                case E_ClosingCostFeeMismoFeeT.HeatingCoolingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.HighCostMortgageCounselingFee:
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationDues:
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.HomeWarrantyFee:
                case E_ClosingCostFeeMismoFeeT.LeadInspectionFee:
                case E_ClosingCostFeeMismoFeeT.LendersAttorneyFee:
                case E_ClosingCostFeeMismoFeeT.LoanLevelPriceAdjustment:
                case E_ClosingCostFeeMismoFeeT.ManualUnderwritingFee:
                case E_ClosingCostFeeMismoFeeT.ManufacturedHousingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.MoldInspectionFee:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeCountyOrParish:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeMunicipal:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeState:
                case E_ClosingCostFeeMismoFeeT.PlumbingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyPreparationFee:
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyRecordingFee:
                case E_ClosingCostFeeMismoFeeT.PreclosingVerificationControlFee:
                case E_ClosingCostFeeMismoFeeT.PropertyInspectionWaiverFee:
                case E_ClosingCostFeeMismoFeeT.PropertyTaxStatusResearchFee:
                case E_ClosingCostFeeMismoFeeT.RadonInspectionFee:
                case E_ClosingCostFeeMismoFeeT.RateLockFee:
                case E_ClosingCostFeeMismoFeeT.ReconveyanceFee:
                case E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination:
                case E_ClosingCostFeeMismoFeeT.RecordingFeeTotal:
                case E_ClosingCostFeeMismoFeeT.RepairsFee:
                case E_ClosingCostFeeMismoFeeT.RoofInspectionFee:
                case E_ClosingCostFeeMismoFeeT.SepticInspectionFee:
                case E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee:
                case E_ClosingCostFeeMismoFeeT.StateTitleInsuranceFee:
                case E_ClosingCostFeeMismoFeeT.StructuralInspectionFee:
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownAdministrationFee:
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownPoints:
                case E_ClosingCostFeeMismoFeeT.TitleCertificationFee:
                case E_ClosingCostFeeMismoFeeT.TitleClosingFee:
                case E_ClosingCostFeeMismoFeeT.TitleDocumentPreparationFee:
                case E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee:
                case E_ClosingCostFeeMismoFeeT.TitleNotaryFee:
                case E_ClosingCostFeeMismoFeeT.TitleServicesSalesTax:
                case E_ClosingCostFeeMismoFeeT.TitleUnderwritingIssueResolutionFee:
                case E_ClosingCostFeeMismoFeeT.TransferTaxTotal:
                case E_ClosingCostFeeMismoFeeT.VerificationOfAssetsFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfEmploymentFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfIncomeFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfResidencyStatusFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxpayerIdentificationFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxReturnFee:
                case E_ClosingCostFeeMismoFeeT.WaterTestingFee:
                case E_ClosingCostFeeMismoFeeT.WellInspectionFee:
                    return E_RespaFeeType.Other;
                default:
                    throw new UnhandledEnumException(feeType);
            }
        }

        /// <summary>
        /// Retrieves the fee value corresponding to the entity who paid it.
        /// </summary>
        /// <param name="paidBy">The entity who paid the fee.</param>
        /// <returns>An <see cref="E_PaymentPaidByType"/> value corresponding to that entity.</returns>
        private E_PaymentPaidByType ToMismo(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_PaymentPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return E_PaymentPaidByType.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_PaymentPaidByType.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_PaymentPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return E_PaymentPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Populates the escrow list with old datalayer information.
        /// </summary>
        /// <param name="escrowList">The list object to be populated.</param>
        private void PopulateEscrowList(List<Escrow> escrowList)
        {
            bool is903POCChecked = LosConvert.GfeItemProps_Poc(mxGfe ? m_dataLoan.sHazInsPiaProps : m_dataLoan.sSettlementHazInsPiaProps);
            if (false == is903POCChecked)
            {
                escrowList.Add(
                    CreateEscrow(
                        "903",
                        "",
                        "",
                        1,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sHazInsPiaMon_rep : m_dataLoan.sSettlementHazInsPiaMon_rep,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sHazInsPia_rep : m_dataLoan.sSettlementHazInsPia_rep,
                        mxGfe ? m_dataLoan.sHazInsPiaProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sHazInsPiaProps, m_dataLoan.sSettlementHazInsPiaProps),
                        miGfe ? m_dataLoanWithGfeArchiveApplied.sHazInsPia_rep : null,
                        m_dataLoan.sHazInsPiaPaidTo));
            }
            //else if is903POCChecked - add it as a respa fee.


            E_GfeSectionT escrowSection904 = mxGfe ? m_dataLoanWithGfeArchiveApplied.s904PiaGfeSection : m_dataLoan.s904PiaSettlementSection;
            // sk 105109
            bool is904POCChecked = LosConvert.GfeItemProps_Poc(mxGfe ? m_dataLoan.s904PiaProps : m_dataLoan.sSettlement904PiaProps);// sk 110595 POC affects 903,904,906
            if ((escrowSection904 == E_GfeSectionT.B11 && false == is904POCChecked) || E_GfeSectionT.NotApplicable == escrowSection904) // if B11 or N/A is selected, create an escrow for this line. Otherwise, B3 is selected, and we should create a RESPA fee
            {
                var escrow904 = CreateEscrow(
                    "904",
                    mxGfe ? m_dataLoanWithGfeArchiveApplied.s904PiaDesc : m_dataLoan.sSettlement904PiaDesc,
                    "",
                    1,
                    "",
                    mxGfe ? m_dataLoanWithGfeArchiveApplied.s904Pia_rep : m_dataLoan.sSettlement904Pia_rep,
                    mxGfe ? m_dataLoan.s904PiaProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s904PiaProps, m_dataLoan.sSettlement904PiaProps),
                    miGfe ? m_dataLoanWithGfeArchiveApplied.s904Pia_rep : null,
                    "");

                if (escrow904 != null)
                {
                    // if the description contains flood, pass in FloodInsurance to @_ItemType
                    if (escrow904.ItemTypeOtherDescription.ToLower().Contains("flood"))
                    {
                        escrow904.ItemType = E_EscrowItemType.FloodInsurance;
                    }
                    else // else pass in HazardInsurance
                    {
                        escrow904.ItemType = E_EscrowItemType.HazardInsurance;
                    }
                    if (E_GfeSectionT.NotApplicable == escrowSection904)
                    {
                        escrow904.ItemType = E_EscrowItemType.Other;
                    }
                }
                escrowList.Add(escrow904);
            }



            E_GfeSectionT escrowSection906 = mxGfe ? m_dataLoanWithGfeArchiveApplied.s900U1PiaGfeSection : m_dataLoan.s900U1PiaSettlementSection;
            bool is906POCChecked = LosConvert.GfeItemProps_Poc(mxGfe ? m_dataLoan.s900U1PiaProps : m_dataLoan.sSettlement900U1PiaProps);
            if ((escrowSection906 == E_GfeSectionT.B11 && false == is906POCChecked) || escrowSection906 == E_GfeSectionT.NotApplicable) // if B11 or N/A is selected, create an escrow for this line
            {
                var escrow906 = CreateEscrow(
                        "906",
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.s900U1PiaDesc : m_dataLoan.sSettlement900U1PiaDesc,
                        "",
                        1,
                        "",
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.s900U1Pia_rep : m_dataLoan.sSettlement900U1Pia_rep,
                        mxGfe ? m_dataLoan.s900U1PiaProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s900U1PiaProps, m_dataLoan.sSettlement900U1PiaProps),
                        miGfe ? m_dataLoanWithGfeArchiveApplied.s900U1Pia_rep : null,
                        "");

                if (escrow906 != null)
                {
                    // if the description contains flood, pass in FloodInsurance to @_ItemType
                    if (escrow906.ItemTypeOtherDescription.ToLower().Contains("flood"))
                    {
                        escrow906.ItemType = E_EscrowItemType.FloodInsurance;
                    }
                    else // else pass in HazardInsurance
                    {
                        escrow906.ItemType = E_EscrowItemType.HazardInsurance;
                    }
                    if (E_GfeSectionT.NotApplicable == escrowSection906)
                    {
                        escrow906.ItemType = E_EscrowItemType.Other;
                    }
                }

                escrowList.Add(escrow906);
            }

            if (m_dataLoan.sHazInsRsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        "1001",
                        "",
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProHazIns_rep, // this is the same in both pages
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProHazIns,     // this too
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sHazInsRsrvMon_rep : m_dataLoan.sSettlementHazInsRsrvMon_rep,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sHazInsRsrv_rep : m_dataLoan.sSettlementHazInsRsrvMon_rep,
                        mxGfe ? m_dataLoan.sHazInsRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sHazInsRsrvProps, m_dataLoan.sSettlementHazInsRsrvProps),
                        null,
                        ""));    //dont need this for 1000's      
            }

            if (m_dataLoan.sSchoolTxRsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        "1003",
                        "",
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProSchoolTx_rep, // this amount is the same in both the GFE and settlement pages
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProSchoolTx,     // same as above
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sSchoolTxRsrvMon_rep : m_dataLoan.sSettlementSchoolTxRsrvMon_rep,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sSchoolTxRsrv_rep : m_dataLoan.sSettlementSchoolTxRsrv_rep,
                        mxGfe ? m_dataLoan.sSchoolTxRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sSchoolTxRsrvProps, m_dataLoan.sSettlementSchoolTxRsrvProps),
                        null,
                        ""));
            }

            if (m_dataLoan.sRealETxRsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        "1004",
                        "",
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProRealETx_rep, // same on both pages linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProRealETx,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sRealETxRsrvMon_rep : m_dataLoan.sSettlementRealETxRsrvMon_rep,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sRealETxRsrv_rep : m_dataLoan.sSettlementRealETxRsrv_rep,
                        mxGfe ? m_dataLoan.sRealETxRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRealETxRsrvProps, m_dataLoan.sSettlementRealETxRsrvProps),
                        null,
                        ""));
            }

            if (m_dataLoan.sFloodInsRsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        "1005",
                        "",
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProFloodIns_rep, // this field is used by both pages
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProFloodIns,     // this field is used by both pages
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sFloodInsRsrvMon_rep : m_dataLoan.sSettlementFloodInsRsrvMon_rep,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sFloodInsRsrv_rep : m_dataLoan.sSettlementFloodInsRsrv_rep,
                        mxGfe ? m_dataLoan.sFloodInsRsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sFloodInsRsrvProps, m_dataLoan.sSettlementFloodInsRsrvProps),
                        null,
                        ""));   //include the total 
            }

            if (m_dataLoan.s1006RsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        "1006",
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1006ProHExpDesc, // fields are linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1006ProHExp_rep, // fields are linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1006ProHExp,     // fields are linked
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.s1006RsrvMon_rep : m_dataLoan.sSettlement1008RsrvMon_rep, // not quite sure why its 1006 and 1008 in the different pages, but then again this mismo doc says 1006
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.s1006Rsrv_rep : m_dataLoan.sSettlement1008Rsrv_rep,
                        mxGfe ? m_dataLoan.s1006RsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s1006RsrvProps, m_dataLoan.sSettlement1008RsrvProps),
                        null,
                        ""));
            }

            if (m_dataLoan.s1007RsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        "1007",
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1007ProHExpDesc, // linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1007ProHExp_rep, // linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).s1007ProHExp,     // linked
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.s1007RsrvMon_rep : m_dataLoan.sSettlement1009RsrvMon_rep,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.s1007Rsrv_rep : m_dataLoan.sSettlement1009Rsrv_rep,
                        mxGfe ? m_dataLoan.s1007RsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s1007RsrvProps, m_dataLoan.sSettlement1009RsrvProps),
                        null,
                        ""));
            }

            if (m_dataLoan.sU3RsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        "1008",
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sU3RsrvDesc,    // linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProU3Rsrv_rep, // linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProU3Rsrv,     // linked
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sU3RsrvMon_rep : m_dataLoan.sSettlementU3RsrvMon_rep,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sU3Rsrv_rep : m_dataLoan.sSettlementU3Rsrv_rep,
                        mxGfe ? m_dataLoan.sU3RsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3RsrvProps, m_dataLoan.sSettlementU3RsrvProps),
                        null,
                        ""));
            }

            if (m_dataLoan.sU4RsrvEscrowedTri == E_TriState.Yes)
            {
                escrowList.Add(
                    CreateEscrow(
                        "1009",
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sU4RsrvDesc,    // linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProU4Rsrv_rep, // linked
                        (mxGfe ? m_dataLoanWithGfeArchiveApplied : m_dataLoan).sProU4Rsrv,     // linked
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sU4RsrvMon_rep : m_dataLoan.sSettlementU4RsrvMon_rep,
                        mxGfe ? m_dataLoanWithGfeArchiveApplied.sU4Rsrv_rep : m_dataLoan.sSettlementU4Rsrv_rep,
                        mxGfe ? m_dataLoan.sU4RsrvProps : LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU4RsrvProps, m_dataLoan.sSettlementU4RsrvProps),
                        null,
                        ""));
            }
        }

        /// <summary>
        /// Populates the RESPA fee list with old datalayer information.
        /// </summary>
        /// <param name="respaFeeList">The RESPA fee list object to be populated.</param>
        private void PopulateRespaFeeList(List<Mismo.Closing2_6.RespaFee> respaFeeList)
        {
            bool is903POCChecked = LosConvert.GfeItemProps_Poc(mxGfe ? m_dataLoan.sHazInsPiaProps : m_dataLoan.sSettlementHazInsPiaProps);
            bool is904POCChecked = LosConvert.GfeItemProps_Poc(mxGfe ? m_dataLoan.s904PiaProps : m_dataLoan.sSettlement904PiaProps);// sk 110595 POC affects 903,904,906
            bool is906POCChecked = LosConvert.GfeItemProps_Poc(mxGfe ? m_dataLoan.s900U1PiaProps : m_dataLoan.sSettlement900U1PiaProps);

            //// 801 Broker Commission
            //respaFeeList.Add(
            //    CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
            //    E_RespaFeeGfeAggregationType.OurOriginationCharge, E_RespaFeeType.LoanOriginationFee, "801", "", false,
            //    mxGfe ? m_dataLoan.sGfeBrokerComp_rep : m_dataLoan.sSettlementBrokerComp_rep,
            //    mxGfe ? m_dataLoanWithGfeArchiveApplied.sLOrigFProps : LosConvert.GfeItemProps_CopyAprFrom(m_dataLoanWithGfeArchiveApplied.sLOrigFProps, m_dataLoan.sSettlementLOrigFProps),
            //    miGfe ? m_dataLoan.sGfeBrokerComp_rep : null, ""));

            decimal sLDiscnt = mxGfe ? m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointF : m_dataLoan.sSettlementDiscountPointF;
            if (sLDiscnt >= 0)
            {
                respaFeeList.Add(GetSpecialFee("802"));
            }

            // MPTODO: this is NOT the way I want to call GetSpecialFee. It should already have these special fields.
            var fee902 = GetSpecialFee("902");
            if (fee902 != null)
            {
                fee902.PaidToType = E_RespaFeePaidToType.Other;
                if (m_dataLoan.sLT == E_sLT.VA)
                {
                    fee902.PaidToTypeOtherDescription = "VA";
                }
                else
                {
                    fee902.PaidToTypeOtherDescription = "PMI";
                    fee902.ResponsiblePartyType = E_RespaFeeResponsiblePartyType.Buyer;
                }

                respaFeeList.Add(fee902);
            }

            if (is903POCChecked)
            {
                var fee = GetSpecialFee("903");
                if (null != fee)
                {
                    fee.Type = E_RespaFeeType.Other;
                    fee.ItemDescription = "Homeowner's Insurance Premium";
                    fee.GfeAggregationType = null; // E_RespaFeeGfeAggregationType.Undefined;
                    fee.TypeOtherDescription = "Hazard Insurance Premium";
                    foreach (var payment in fee.PaymentList)
                    {
                        payment.PaidOutsideOfClosingIndicator = E_YNIndicator.Y;
                    }
                    respaFeeList.Add(fee);
                }
            }

            // Pass in 904/906 if B3 is selected. Otherwise, B11 is selected and we should send an ESCROW element. OPM 83184 -mp
            E_GfeSectionT section904 = mxGfe ? m_dataLoanWithGfeArchiveApplied.s904PiaGfeSection : m_dataLoan.s904PiaSettlementSection;
            if (section904 == E_GfeSectionT.B3)
            {
                respaFeeList.Add(GetSpecialFee("904"));
            }
            else if (section904 == E_GfeSectionT.B11 && is904POCChecked)
            {
                var fee = GetSpecialFee("904");
                if (null != fee)
                {
                    fee.Type = E_RespaFeeType.Other;
                    fee.ItemDescription = "Homeowner's Insurance Premium";
                    fee.GfeAggregationType = null; // E_RespaFeeGfeAggregationType.Undefined;
                    fee.TypeOtherDescription = "Hazard Insurance Premium"; // silliness https://lendingqb.basecamphq.com/projects/8427282-docmagic-integration/posts/71873140/comments#comment_219028660
                    foreach (var payment in fee.PaymentList)
                    {
                        payment.PaidOutsideOfClosingIndicator = E_YNIndicator.Y;
                    }
                    respaFeeList.Add(fee);
                }
            }

            var fee905 = GetSpecialFee("905");
            if (fee905 != null)
            {
                respaFeeList.Add(fee905);
            }

            E_GfeSectionT section906 = mxGfe ? m_dataLoanWithGfeArchiveApplied.s900U1PiaGfeSection : m_dataLoan.s900U1PiaSettlementSection;
            if (section906 == E_GfeSectionT.B3)
            {
                respaFeeList.Add(GetSpecialFee("906"));
            }
            else if (section906 == E_GfeSectionT.B11 && is906POCChecked)
            {
                var fee = GetSpecialFee("906");
                if (null != fee)
                {
                    fee.Type = E_RespaFeeType.Other;
                    fee.ItemDescription = "Homeowner's Insurance Premium";
                    fee.GfeAggregationType = null; // E_RespaFeeGfeAggregationType.Undefined;
                    fee.TypeOtherDescription = "Hazard Insurance Premium";  // silliness https://lendingqb.basecamphq.com/projects/8427282-docmagic-integration/posts/71873140/comments#comment_219028660
                    foreach (var payment in fee.PaymentList)
                    {
                        payment.PaidOutsideOfClosingIndicator = E_YNIndicator.Y;
                    }
                    respaFeeList.Add(fee);
                }
            }

            // respaFeeList.Add(CreateRespaFee(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, E_RespaFeeType.Other,"1201", "", false, m_dataLoanWithGfeArchiveApplied.sRecF_rep, m_dataLoanWithGfeArchiveApplied.sRecFProps));
            // 1201/1202 Recording Fee
            bool use1201 = mxGfe ? m_dataLoanWithGfeArchiveApplied.sRecFLckd : m_dataLoan.sSettlementRecFLckd;
            if (use1201)
            {
                respaFeeList.Add(GetSpecialFee("1201"));
            }
            else
            {
                respaFeeList.Add(GetSpecialFee("1202_Deed"));
                respaFeeList.Add(GetSpecialFee("1202_Mortgage"));
                respaFeeList.Add(GetSpecialFee("1202_Release"));
            }

            List<MISMOFeeItem> feeItems;
            if (mxGfe)
                feeItems = GFEFeeItems;
            else
                feeItems = SCFeeItems;

            foreach (var feeItem in feeItems)
            {
                if (!mxGfe)
                {
                    ValidateSCFeeWithGFEFee(feeItem,
                        GFEFeeItems.FirstOrDefault(gfeFee => feeItem.TrueHUDLineNumber == gfeFee.TrueHUDLineNumber));
                }

                respaFeeList.Add(CreateRespaFee(
                    feeItem.ClassificationType,
                    feeItem.AggregationType,
                    feeItem.Type,
                    feeItem.DMLineNumber,
                    feeItem.Description,
                    feeItem.IsPaid,
                    feeItem.Amount,
                    feeItem.Props,
                    feeItem.Point,
                    feeItem.FixedAmount,
                    miGfe ? feeItem.GFEDisclosedAmount : null,
                    feeItem.PaidToOverride,
                    feeItem.ID,
                    feeItem.TotalAmount));
            }

            // Begin fees added by OPM 75461 4/9/12
            if (m_exportSource == E_SettlementChargesExportSource.SETTLEMENT)
            {
                // 700. Total Real Estate Broker Fees
                // 701
                //OPM 129020: Don't export these if they're zero.
                var zero = m_dataLoan.m_convertLos.ToMoneyString(0, FormatDirection.ToRep);
                if (m_dataLoan.sRealEstateBrokerFees1F_rep != zero)
                {
                    var realEstateBrokerFees1 = new Mismo.Closing2_6.RespaFee()
                    {
                        RespaSectionClassificationType = E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions,
                        SpecifiedHudLineNumber = "701",
                        Type = E_RespaFeeType.RealEstateCommission,
                        TotalAmount = m_dataLoan.sRealEstateBrokerFees1F_rep
                    };
                    realEstateBrokerFees1.PaidTo = CreatePaidTo(m_dataLoan.sRealEstateBrokerFees1FAgentName);
                    realEstateBrokerFees1.PaymentList.Add(new Payment()
                    {
                        Amount = m_dataLoan.sRealEstateBrokerFees1F_rep
                    });
                    respaFeeList.Add(realEstateBrokerFees1);
                }

                // 702
                if (m_dataLoan.sRealEstateBrokerFees2F_rep != zero)
                {
                    var realEstateBrokerFees2 = new Mismo.Closing2_6.RespaFee()
                    {
                        RespaSectionClassificationType = E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions,
                        SpecifiedHudLineNumber = "702",
                        Type = E_RespaFeeType.RealEstateCommission,
                        TotalAmount = m_dataLoan.sRealEstateBrokerFees2F_rep
                    };
                    realEstateBrokerFees2.PaidTo = CreatePaidTo(m_dataLoan.sRealEstateBrokerFees2FAgentName);
                    realEstateBrokerFees2.PaymentList.Add(new Payment()
                    {
                        Amount = m_dataLoan.sRealEstateBrokerFees2F_rep
                    });
                    respaFeeList.Add(realEstateBrokerFees2);
                }

                // 703 Commission paid at settlement
                if (new[] { m_dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementF_rep,
                            m_dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr_rep,
                            m_dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller_rep
                          }.Any(a => a != zero))
                {
                    var realEstateBrokerFeesCommissionPdAtSettlement = new Mismo.Closing2_6.RespaFee()
                    {
                        RespaSectionClassificationType = E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions,
                        SpecifiedHudLineNumber = "703",
                        Type = E_RespaFeeType.RealEstateCommission,
                        TotalAmount = m_dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementF_rep
                    };
                    realEstateBrokerFeesCommissionPdAtSettlement.PaymentList.Add(new Payment()
                    {
                        Amount = m_dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromBorr_rep,
                        PaidByType = E_PaymentPaidByType.Buyer
                    });
                    realEstateBrokerFeesCommissionPdAtSettlement.PaymentList.Add(new Payment()
                    {
                        Amount = m_dataLoan.sRealEstateBrokerFeesCommissionPdAtSettlementFFromSeller_rep,
                        PaidByType = E_PaymentPaidByType.Seller
                    });
                    respaFeeList.Add(realEstateBrokerFeesCommissionPdAtSettlement);
                }

                // 704
                if (new[] { m_dataLoan.sRealEstateBrokerFeesU1F_rep, 
                            m_dataLoan.sRealEstateBrokerFeesU1FFromBorr_rep, 
                            m_dataLoan.sRealEstateBrokerFeesU1FFromSeller_rep 
                          }.Any(a => a != zero))
                {
                    var realEstateBrokerFeesU1 = new Mismo.Closing2_6.RespaFee()
                    {
                        RespaSectionClassificationType = E_RespaFeeRespaSectionClassificationType._700_DivisionOfCommissions,
                        SpecifiedHudLineNumber = "704",
                        Type = E_RespaFeeType.RealEstateCommission,
                        TotalAmount = m_dataLoan.sRealEstateBrokerFeesU1F_rep,
                        ItemDescription = m_dataLoan.sRealEstateBrokerFeesU1FDesc
                    };
                    realEstateBrokerFeesU1.PaymentList.Add(new Payment()
                    {
                        Amount = m_dataLoan.sRealEstateBrokerFeesU1FFromBorr_rep,
                        PaidByType = E_PaymentPaidByType.Buyer
                    });
                    realEstateBrokerFeesU1.PaymentList.Add(new Payment()
                    {
                        Amount = m_dataLoan.sRealEstateBrokerFeesU1FFromSeller_rep,
                        PaidByType = E_PaymentPaidByType.Seller
                    });
                    respaFeeList.Add(realEstateBrokerFeesU1);
                }


                // Seller's Column Settlement Charges
                //
                var SCFeeDict = new Dictionary<string, MISMOFeeItem>();
                foreach (var feeItem in SCFeeItems)
                {
                    try
                    {
                        SCFeeDict.Add(feeItem.TrueHUDLineNumber, feeItem);
                    }
                    catch (ArgumentException)
                    {
                        // Ignore. We'll deal with these special cases later.
                    }
                }

                var sellerColumn = new[] {
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU01FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU01F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU02FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU02F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU03FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU03F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU04FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU04F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU05FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU05F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU06FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU06F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU07FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU07F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU08FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU08F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU09FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU09F_rep },
                    new { Hudline = m_dataLoan.sSellerSettlementChargesU10FHudline_rep, TotalAmount = m_dataLoan.sSellerSettlementChargesU10F_rep },
                };

                foreach (var row in sellerColumn)
                {
                    // For each row in the seller column, use the same elements and attributes as the RESPA_FEE elements
                    // generated from the Settlement Charges page for the corresponding line with the following changes:
                    //   Set RESPA_FEE@_ResponsiblePartyType to "Seller"
                    //   Omit _GFEDisclosedAmount
                    //   Omit _PercentBasisType
                    //   Omit _SpecifiedFixedAmount
                    //   Omit _TotalPercent
                    //   Set RESPA_FEE@_TotalAmount to the Amount field on the HUD-1 page
                    //   Set PAYMENT@_Amount to the Amount field on the HUD-1 page
                    //   Set PAYMENT@_PaidByType to "Seller"
                    //   Set PAYMENT@_PaidOutsideOfClosingIndicator to "N"

                    if (string.IsNullOrEmpty(row.Hudline))
                        continue;

                    Mismo.Closing2_6.RespaFee respaFeeItem = null;

                    // Dealing with the special cases
                    if (row.Hudline == "801") // See case 80613 comment 4/11/2012 2:48PM.
                    {
                        // 801 Originator Compensation
                        respaFeeItem = CreateRespaFee(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                            E_RespaFeeGfeAggregationType.OurOriginationCharge,
                            E_RespaFeeType.LoanOriginationFee,
                            "801",
                            "",
                            false,
                            row.TotalAmount,
                            m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFProps,
                            null,
                            null,
                            null,
                            null,
                            null,
                            row.TotalAmount);
                    }
                    else if (row.Hudline == "802" || row.Hudline == "901" ||
                        row.Hudline == "902" || row.Hudline == "903" ||
                        row.Hudline == "904" || row.Hudline == "905" ||
                        row.Hudline == "906" || row.Hudline == "1201")
                    {
                        var feeItem = SCSpecialFeeItems[row.Hudline];

                        respaFeeItem = CreateRespaFee(
                            feeItem.ClassificationType,
                            feeItem.AggregationType,
                            feeItem.Type,
                            feeItem.DMLineNumber,
                            feeItem.Description,
                            false,
                            row.TotalAmount,
                            feeItem.Props,
                            null, // TotalPercent
                            null, // SpecifiedFixedAmount
                            null, // GFEDisclosedAmount
                            feeItem.PaidToOverride,
                            "",
                            row.TotalAmount);
                    }
                    else
                    {
                        // Otherwise, proceed as usual
                        var feeItem = SCFeeDict[row.Hudline];
                        respaFeeItem = CreateRespaFee(
                            feeItem.ClassificationType,
                            feeItem.AggregationType,
                            feeItem.Type,
                            feeItem.DMLineNumber,
                            feeItem.Description,
                            false,
                            row.TotalAmount,
                            feeItem.Props,
                            null, // TotalPercent
                            null, // SpecifiedFixedAmount
                            null, // GFEDisclosedAmount
                            feeItem.PaidToOverride,
                            "",
                            row.TotalAmount);
                    }

                    if (respaFeeItem != null)
                    {
                        respaFeeItem.PercentBasisType = E_RespaFeePercentBasisType.Undefined;

                        respaFeeItem.ResponsiblePartyType = E_RespaFeeResponsiblePartyType.Seller;
                        respaFeeItem.PaymentList[0].PaidByType = E_PaymentPaidByType.Seller;
                        respaFeeItem.PaymentList[0].PaidOutsideOfClosingIndicator = E_YNIndicator.N;
                        respaFeeItem.Id = null;

                        respaFeeList.Add(respaFeeItem);
                    }
                }
            }
            // End fees added by OPM 75461
        }

        private static E_RespaFeeType RespaFee902Type(E_sLT sLT)
        {
            switch (sLT)
            {
                case E_sLT.FHA: return E_RespaFeeType.MI_FHAUpfrontPremium;
                case E_sLT.VA: return E_RespaFeeType.VAFundingFee;
                case E_sLT.UsdaRural: return E_RespaFeeType.RuralHousingFee;
                case E_sLT.Other:
                case E_sLT.Conventional: return E_RespaFeeType.MIInitialPremium;
                default:
                    throw new UnhandledEnumException(sLT);
            }
        }

        private List<MISMOFeeItem> m_GFEFeeItems = null;
        private List<MISMOFeeItem> GFEFeeItems
        {
            get
            {
                if (m_GFEFeeItems != null)
                    return m_GFEFeeItems;

                m_GFEFeeItems = new List<MISMOFeeItem>()
                {
                    // 801 Loan Origination Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge, 
                        E_RespaFeeType.LoanOriginationFee,
                        "801",
                        "801",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sLOrigF_rep,
                        m_dataLoan.sLOrigFProps,
                        m_dataLoanWithGfeArchiveApplied.sLOrigFPc_rep,
                        m_dataLoanWithGfeArchiveApplied.sLOrigFMb_rep,
                        m_dataLoanWithGfeArchiveApplied.sLOrigF_rep,
                        "",
                        "LQBGFE801a",
                        null),

                    // 801 Originator Compensation
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge, 
                        E_RespaFeeType.LoanOriginationFee,
                        "801",
                        "801",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep,
                        LosConvert.GfeItemProps_UpdateToBr(m_dataLoan.sGfeOriginatorCompFProps, true ),
                        "",
                        m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep,
                        m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep,
                        "",
                        "LQBGFE801b",
                        null),//m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep),

                    // 804 Appraisal Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                        E_RespaFeeType.AppraisalFee,
                        "804",
                        "803",
                        "",
                        m_dataLoan.sApprFPaid,
                        m_dataLoanWithGfeArchiveApplied.sApprF_rep,
                        m_dataLoan.sApprFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sApprF_rep,
                        m_dataLoan.sApprFPaidTo,
                        "LQBGFE804",
                        null),

                    // 805 Credit Report
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected, 
                        E_RespaFeeType.CreditReportFee,
                        "805",
                        "804",
                        "",
                        m_dataLoan.sCrFPaid,
                        m_dataLoanWithGfeArchiveApplied.sCrF_rep,
                        m_dataLoan.sCrFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sCrF_rep,
                        m_dataLoan.sCrFPaidTo,
                        "LQBGFE805",
                        null),

                    // 806 Tax Service Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected, 
                        E_RespaFeeType.TaxRelatedServiceFee,
                        "806",
                        "809",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sTxServF_rep,
                        m_dataLoan.sTxServFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sTxServF_rep,
                        m_dataLoan.sTxServFPaidTo,
                        "LQBGFE806",
                        null),

                    // 807 Flood Certification
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected, 
                        E_RespaFeeType.FloodCertification,
                        "807",
                        "807",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sFloodCertificationF_rep,
                        m_dataLoan.sFloodCertificationFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sFloodCertificationF_rep,
                        m_dataLoan.sFloodCertificationFPaidTo,
                        "LQBGFE807",
                        null),

                    // 808 Mortgage Broker Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge, 
                        E_RespaFeeType.MortgageBrokerFee,
                        "808",
                        "808",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sMBrokF_rep,
                        m_dataLoan.sMBrokFProps,
                        m_dataLoanWithGfeArchiveApplied.sMBrokFPc_rep,
                        m_dataLoanWithGfeArchiveApplied.sMBrokFMb_rep,
                        m_dataLoanWithGfeArchiveApplied.sMBrokF_rep,
                        "",
                        "LQBGFE808",
                        null),

                    // 809 Lender Inspection Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge, 
                        E_RespaFeeType.InspectionFee,
                        "809",
                        "805",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sInspectF_rep,
                        m_dataLoan.sInspectFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sInspectF_rep,
                        m_dataLoan.sInspectFPaidTo,
                        "LQBGFE809",
                        null),

                    // 810 Processing Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge, 
                        E_RespaFeeType.ProcessingFee,
                        "810",
                        "810",
                        "",
                        m_dataLoan.sProcFPaid,
                        m_dataLoanWithGfeArchiveApplied.sProcF_rep,
                        m_dataLoan.sProcFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sProcF_rep,
                        m_dataLoan.sProcFPaidTo,
                        "LQBGFE810",
                        null),

                    // 811 Underwriting Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge, 
                        E_RespaFeeType.UnderwritingFee,
                        "811",
                        "811",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sUwF_rep,
                        m_dataLoan.sUwFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sUwF_rep,
                        m_dataLoan.sUwFPaidTo,
                        "LQBGFE811",
                        null),

                    // 812 Wire transfer
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge, 
                        E_RespaFeeType.WireTransferFee,
                        "812",
                        "812",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sWireF_rep,
                        m_dataLoan.sWireFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sWireF_rep,
                        m_dataLoan.sWireFPaidTo,
                        "LQBGFE812",
                        null),

                    // 813 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.s800U1FGfeSection), 
                        E_RespaFeeType.Other,
                        "813",
                        "813" /* m_dataLoan.s800U1FCode */,
                        m_dataLoanWithGfeArchiveApplied.s800U1FDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.s800U1F_rep,
                        m_dataLoan.s800U1FProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U1F_rep,
                        m_dataLoan.s800U1FPaidTo,
                        "LQBGFE813",
                        null),

                    // 814 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.s800U2FGfeSection), 
                        E_RespaFeeType.Other,
                        "814",
                        "814" /* m_dataLoan.s800U2FCode */,
                        m_dataLoanWithGfeArchiveApplied.s800U2FDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.s800U2F_rep,
                        m_dataLoan.s800U2FProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U2F_rep,
                        m_dataLoan.s800U2FPaidTo,
                        "LQBGFE814",
                        null),

                    // 815 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.s800U3FGfeSection), 
                        E_RespaFeeType.Other,
                        "815",
                        "815" /* m_dataLoan.s800U3FCode */,
                        m_dataLoanWithGfeArchiveApplied.s800U3FDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.s800U3F_rep,
                        m_dataLoan.s800U3FProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U3F_rep,
                        m_dataLoan.s800U3FPaidTo,
                        "LQBGFE815",
                        null),

                    // 816 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.s800U4FGfeSection), 
                        E_RespaFeeType.Other,
                        "816",
                        "816" /* m_dataLoan.s800U4FCode */,
                        m_dataLoanWithGfeArchiveApplied.s800U4FDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.s800U4F_rep,
                        m_dataLoan.s800U4FProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U4F_rep,
                        m_dataLoan.s800U4FPaidTo,
                        "LQBGFE816",
                        null),

                    // 817 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.s800U5FGfeSection), 
                        E_RespaFeeType.Other,
                        "817",
                        "817" /* m_dataLoan.s800U5FCode */,
                        m_dataLoanWithGfeArchiveApplied.s800U5FDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.s800U5F_rep,
                        m_dataLoan.s800U5FProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U5F_rep,
                        m_dataLoan.s800U5FPaidTo,
                        "LQBGFE817",
                        null),

                    // 1102 Closing / Escrow Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sEscrowFGfeSection), 
                        E_RespaFeeType.SettlementOrClosingFee,
                        "1102",
                        "1101",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sEscrowF_rep,
                        m_dataLoan.sEscrowFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sEscrowF_rep,
                        m_dataLoan.sEscrowFTable,
                        "LQBGFE1102",
                        null),

                    // 1103 Owners Title Insurance (Owner's Title Insurance Fee)
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        E_RespaFeeGfeAggregationType.OwnersTitleInsurance, 
                        E_RespaFeeType.Other,
                        "1103",
                        "1103",
                        "Owner Coverage",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sOwnerTitleInsF_rep,
                        m_dataLoan.sOwnerTitleInsProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sOwnerTitleInsF_rep,
                        m_dataLoan.sOwnerTitleInsPaidTo,
                        "LQBGFE1103",
                        null),

                    // 1104 Lenders Title Insurance (Lender's Title Insurance Fee)
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        E_RespaFeeGfeAggregationType.TitleServices, 
                        E_RespaFeeType.Other,
                        "1104",
                        "1104",
                        "Lender Coverage",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sTitleInsF_rep,
                        m_dataLoan.sTitleInsFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sTitleInsF_rep,
                        m_dataLoan.sTitleInsFTable,
                        "LQBGFE1104",
                        null),

                    // 1109 Doc Preparation Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sDocPrepFGfeSection), 
                        E_RespaFeeType.DocumentPreparationFee,
                        "1109",
                        "1105",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sDocPrepF_rep,
                        m_dataLoan.sDocPrepFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sDocPrepF_rep,
                        m_dataLoan.sDocPrepFPaidTo,
                        "LQBGFE1109",
                        null),

                    // 1110 Notary Fees
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sNotaryFGfeSection), 
                        E_RespaFeeType.NotaryFee,
                        "1110",
                        "1106",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sNotaryF_rep,
                        m_dataLoan.sNotaryFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sNotaryF_rep,
                        m_dataLoan.sNotaryFPaidTo,
                        "LQBGFE1110",
                        null),

                    // 1111 Attorney Fees
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sAttorneyFGfeSection), 
                        E_RespaFeeType.AttorneyFee,
                        "1111",
                        "1107",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sAttorneyF_rep,
                        m_dataLoan.sAttorneyFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sAttorneyF_rep,
                        m_dataLoan.sAttorneyFPaidTo,
                        "LQBGFE1111",
                        null),

                    //1112 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.sU1TcGfeSection),
                        E_RespaFeeType.Other,
                        "1112",
                        "1112" /* m_dataLoan.sU1TcCode */,
                        m_dataLoanWithGfeArchiveApplied.sU1TcDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU1Tc_rep,
                        m_dataLoan.sU1TcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU1Tc_rep,
                        m_dataLoan.sU1TcPaidTo,
                        "LQBGFE1112",
                        null),

                    //1113 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.sU2TcGfeSection),
                        E_RespaFeeType.Other,
                        "1113",
                        "1113" /* m_dataLoan.sU2TcCode */,
                        m_dataLoanWithGfeArchiveApplied.sU2TcDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU2Tc_rep,
                        m_dataLoan.sU2TcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU2Tc_rep,
                        m_dataLoan.sU2TcPaidTo,
                        "LQBGFE1113",
                        null),

                    //1114 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.sU3TcGfeSection), 
                        E_RespaFeeType.Other,
                        "1114",
                        "1114" /*m_dataLoan.sU3TcCode */,
                        m_dataLoanWithGfeArchiveApplied.sU3TcDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU3Tc_rep,
                        m_dataLoan.sU3TcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU3Tc_rep,
                        m_dataLoan.sU3TcPaidTo,
                        "LQBGFE1114",
                        null),

                    //1115 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.sU4TcGfeSection), 
                        E_RespaFeeType.Other,
                        "1115",
                        "1115" /* m_dataLoan.sU4TcCode */,
                        m_dataLoanWithGfeArchiveApplied.sU4TcDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU4Tc_rep,
                        m_dataLoan.sU4TcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU4Tc_rep,
                        m_dataLoan.sU4TcPaidTo,
                        "LQBGFE1115",
                        null),

                    // 1204 City tax/stamps
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        E_RespaFeeGfeAggregationType.TransferTaxes, 
                        E_RespaFeeType.CityCountyMortgageTaxStampFee,
                        "1204",
                        "1202",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sCountyRtc_rep,
                        m_dataLoan.sCountyRtcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sCountyRtc_rep,
                        m_dataLoan.sCountyRtcDesc,
                        "LQBGFE1204",
                        null),

                    // 1205 State tax/stamps
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        E_RespaFeeGfeAggregationType.TransferTaxes, 
                        E_RespaFeeType.StateMortgageTaxStampFee,
                        "1205",
                        "1203",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sStateRtc_rep,
                        m_dataLoan.sStateRtcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sStateRtc_rep,
                        m_dataLoan.sStateRtcDesc,
                        "LQBGFE1205",
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.sU1GovRtcGfeSection),
                        E_RespaFeeType.Other,
                        "1206",
                        "1206" /*m_dataLoan.sU1GovRtcCode */,
                        m_dataLoanWithGfeArchiveApplied.sU1GovRtcDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU1GovRtc_rep,
                        m_dataLoan.sU1GovRtcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU1GovRtc_rep,
                        m_dataLoan.sU1GovRtcPaidTo,
                        "LQBGFE1206",
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.sU2GovRtcGfeSection),
                        E_RespaFeeType.Other,
                        "1207",
                        "1207" /* m_dataLoan.sU2GovRtcCode */,
                        m_dataLoanWithGfeArchiveApplied.sU2GovRtcDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU2GovRtc_rep,
                        m_dataLoan.sU2GovRtcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU2GovRtc_rep,
                        m_dataLoan.sU2GovRtcPaidTo,
                        "LQBGFE1207",
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(m_dataLoanWithGfeArchiveApplied.sU3GovRtcGfeSection),
                        E_RespaFeeType.Other,
                        "1208",
                        "1208" /* m_dataLoan.sU3GovRtcCode */,
                        m_dataLoanWithGfeArchiveApplied.sU3GovRtcDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU3GovRtc_rep,
                        m_dataLoan.sU3GovRtcProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU3GovRtc_rep,
                        m_dataLoan.sU3GovRtcPaidTo,
                        "LQBGFE1208",
                        null),

                    // 1302 Pest Inspection
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor, 
                        E_RespaFeeType.PestInspectionFee,
                        "1302",
                        "1302",
                        "",
                        false,
                        m_dataLoanWithGfeArchiveApplied.sPestInspectF_rep,
                        m_dataLoan.sPestInspectFProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sPestInspectF_rep,
                        m_dataLoan.sPestInspectPaidTo,
                        "LQBGFE1302",
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU1ScGfeSection), 
                        E_RespaFeeType.Other,
                        "1303",
                        "1303" /* m_dataLoan.sU1ScCode */,
                        m_dataLoanWithGfeArchiveApplied.sU1ScDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU1Sc_rep,
                        m_dataLoan.sU1ScProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU1Sc_rep,
                        m_dataLoan.sU1ScPaidTo,
                        "LQBGFE1303",
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU2ScGfeSection), 
                        E_RespaFeeType.Other,
                        "1304",
                        "1304" /* m_dataLoan.sU2ScCode */,
                        m_dataLoanWithGfeArchiveApplied.sU2ScDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU2Sc_rep,
                        m_dataLoan.sU2ScProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU2Sc_rep,
                        m_dataLoan.sU2ScPaidTo,
                        "LQBGFE1304",
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU3ScGfeSection), 
                        E_RespaFeeType.Other,
                        "1305",
                        "1305" /* m_dataLoan.sU3ScCode */,
                        m_dataLoanWithGfeArchiveApplied.sU3ScDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU3Sc_rep,
                        m_dataLoan.sU3ScProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU3Sc_rep,
                        m_dataLoan.sU3ScPaidTo,
                        "LQBGFE1305",
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU4ScGfeSection), 
                        E_RespaFeeType.Other,
                        "1306",
                        "1306" /* m_dataLoan.sU4ScCode */,
                        m_dataLoanWithGfeArchiveApplied.sU4ScDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU4Sc_rep,
                        m_dataLoan.sU4ScProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU4Sc_rep,
                        m_dataLoan.sU4ScPaidTo,
                        "LQBGFE1306",
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU5ScGfeSection), 
                        E_RespaFeeType.Other,
                        "1307",
                        "1307" /* m_dataLoan.sU5ScCode */,
                        m_dataLoanWithGfeArchiveApplied.sU5ScDesc,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sU5Sc_rep,
                        m_dataLoan.sU5ScProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU5Sc_rep,
                        m_dataLoan.sU5ScPaidTo,
                        "LQBGFE1307",
                        null)
                };
                return m_GFEFeeItems;
            }
        }

        private List<MISMOFeeItem> m_SCFeeItems = null;
        /// <summary>
        /// Settlement charges
        /// </summary>
        private List<MISMOFeeItem> SCFeeItems
        {
            get
            {
                if (m_exportSource != E_SettlementChargesExportSource.SETTLEMENT)
                {
                    // Return an empty list if we're not exporting settlement charges
                    return new List<MISMOFeeItem>();
                }

                if (m_SCFeeItems != null)
                    return m_SCFeeItems;

                m_SCFeeItems = new List<MISMOFeeItem>()
                {
                    // 801 Loan Origination Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge,
                        E_RespaFeeType.LoanOriginationFee,
                        "801",
                        "801",
                        "",
                        false,
                        m_dataLoan.sSettlementLOrigF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sLOrigFProps, m_dataLoan.sSettlementLOrigFProps),
                        m_dataLoan.sSettlementLOrigFPc_rep,
                        m_dataLoan.sSettlementLOrigFMb_rep,
                        m_dataLoanWithGfeArchiveApplied.sLOrigF_rep,
                        "",
                        null,
                        null),

                    // 801 Originator Compensation
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge,
                        E_RespaFeeType.LoanOriginationFee,
                        "801",
                        "801",
                        "",
                        false,
                        m_dataLoan.sGfeOriginatorCompF_rep,
                        LosConvert.GfeItemProps_UpdateToBr(m_dataLoan.sGfeOriginatorCompFProps, true),
                        "", 
                        m_dataLoan.sGfeOriginatorCompF_rep,
                        m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep,
                        "",
                        null,
                        null),//m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep),

                    // 804 Appraisal Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                        E_RespaFeeType.AppraisalFee,
                        "804",
                        "803",
                        "",
                        m_dataLoan.sSettlementApprFPaid,
                        m_dataLoan.sSettlementApprF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sApprFProps, m_dataLoan.sSettlementApprFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sApprF_rep,
                        m_dataLoan.sApprFPaidTo,
                        null,
                        null),

                    // 805 Credit Report
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                        E_RespaFeeType.CreditReportFee,
                        "805",
                        "804",
                        "",
                        m_dataLoan.sSettlementCrFPaid,
                        m_dataLoan.sSettlementCrF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sCrFProps, m_dataLoan.sSettlementCrFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sCrF_rep,
                        m_dataLoan.sCrFPaidTo,
                        null,
                        null),

                    // 806 Tax Service Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                        E_RespaFeeType.TaxRelatedServiceFee,
                        "806",
                        "809",
                        "",
                        false,
                        m_dataLoan.sSettlementTxServF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sTxServFProps, m_dataLoan.sSettlementTxServFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sTxServF_rep,
                        m_dataLoan.sTxServFPaidTo,
                        null,
                        null),

                    // 807 Flood Certification
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                        E_RespaFeeType.FloodCertification,
                        "807",
                        "807",
                        "",
                        false,
                        m_dataLoan.sSettlementFloodCertificationF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sFloodCertificationFProps, m_dataLoan.sSettlementFloodCertificationFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sFloodCertificationF_rep,
                        m_dataLoan.sFloodCertificationFPaidTo,
                        null,
                        null),

                    // 808 Mortgage Broker Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge,
                        E_RespaFeeType.MortgageBrokerFee,
                        "808",
                        "808",
                        "",
                        false,
                        m_dataLoan.sSettlementMBrokF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sMBrokFProps, m_dataLoan.sSettlementMBrokFProps),
                        m_dataLoan.sSettlementMBrokFPc_rep,
                        m_dataLoan.sSettlementMBrokFMb_rep,
                        m_dataLoanWithGfeArchiveApplied.sMBrokF_rep,
                        "",
                        null,
                        null),

                    // 809 Lender Inspection Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge,
                        E_RespaFeeType.InspectionFee,
                        "809",
                        "805",
                        "",
                        false,
                        m_dataLoan.sSettlementInspectF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sInspectFProps, m_dataLoan.sSettlementInspectFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sInspectF_rep,
                        m_dataLoan.sInspectFPaidTo,
                        null,
                        null),

                    // 810 Processing Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge,
                        E_RespaFeeType.ProcessingFee,
                        "810",
                        "810",
                        "",
                        m_dataLoan.sSettlementProcFPaid,
                        m_dataLoan.sSettlementProcF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sProcFProps, m_dataLoan.sSettlementProcFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sProcF_rep,
                        m_dataLoan.sProcFPaidTo,
                        null,
                        null),

                    // 811 Underwriting Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge,
                        E_RespaFeeType.UnderwritingFee,
                        "811",
                        "811",
                        "",
                        false,
                        m_dataLoan.sSettlementUwF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sUwFProps, m_dataLoan.sSettlementUwFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sUwF_rep,
                        m_dataLoan.sUwFPaidTo,
                        null,
                        null),

                    // 812 Wire transfer
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        E_RespaFeeGfeAggregationType.OurOriginationCharge,
                        E_RespaFeeType.WireTransferFee,
                        "812",
                        "812",
                        "",
                        false,
                        m_dataLoan.sSettlementWireF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sWireFProps, m_dataLoan.sSettlementWireFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sWireF_rep,
                        m_dataLoan.sWireFPaidTo,
                        null,
                        null),

                    // 813 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoan.s800U1FSettlementSection),
                        E_RespaFeeType.Other,
                        "813",
                        "813" /* m_dataLoan.s800U1FCode */,
                        m_dataLoan.sSettlement800U1FDesc,
                        false,
                        m_dataLoan.sSettlement800U1F_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U1FProps, m_dataLoan.sSettlement800U1FProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U1F_rep,
                        m_dataLoan.s800U1FPaidTo,
                        null,
                        null),

                    // 814 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoan.s800U2FSettlementSection),
                        E_RespaFeeType.Other,
                        "814",
                        "814" /* m_dataLoan.s800U2FCode */,
                        m_dataLoan.sSettlement800U2FDesc,
                        false,
                        m_dataLoan.sSettlement800U2F_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U2FProps, m_dataLoan.sSettlement800U2FProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U2F_rep,
                        m_dataLoan.s800U2FPaidTo,
                        null,
                        null),

                    // 815 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoan.s800U3FSettlementSection),
                        E_RespaFeeType.Other,
                        "815",
                        "815" /* m_dataLoan.s800U3FCode */,
                        m_dataLoan.sSettlement800U3FDesc,
                        false,
                        m_dataLoan.sSettlement800U3F_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U3FProps, m_dataLoan.sSettlement800U3FProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U3F_rep,
                        m_dataLoan.s800U3FPaidTo,
                        null,
                        null),

                    // 816 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoan.s800U4FSettlementSection),
                        E_RespaFeeType.Other,
                        "816",
                        "816" /* m_dataLoan.s800U4FCode */,
                        m_dataLoan.sSettlement800U4FDesc,
                        false,
                        m_dataLoan.sSettlement800U4F_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U4FProps, m_dataLoan.sSettlement800U4FProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U4F_rep,
                        m_dataLoan.s800U4FPaidTo,
                        null,
                        null),

                    // 817 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._800_LoanFees,
                        ToMismo(m_dataLoan.s800U5FSettlementSection),
                        E_RespaFeeType.Other,
                        "817",
                        "817" /* m_dataLoan.s800U5FCode */,
                        m_dataLoan.sSettlement800U5FDesc,
                        false,
                        m_dataLoan.sSettlement800U5F_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U5FProps, m_dataLoan.sSettlement800U5FProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.s800U5F_rep,
                        m_dataLoan.s800U5FPaidTo,
                        null,
                        null),

                    // 1102 Closing / Escrow Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sEscrowFGfeSection),
                        E_RespaFeeType.SettlementOrClosingFee,
                        "1102",
                        "1101",
                        "",
                        false,
                        m_dataLoan.sSettlementEscrowF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sEscrowFProps, m_dataLoan.sSettlementEscrowFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sEscrowF_rep,
                        m_dataLoan.sEscrowFTable,
                        null,
                        null),

                    // 1103 Owners Title Insurance
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        E_RespaFeeGfeAggregationType.OwnersTitleInsurance,
                        E_RespaFeeType.Other,
                        "1103",
                        "1103",
                        "Owner Coverage",
                        false,
                        m_dataLoan.sSettlementOwnerTitleInsF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sOwnerTitleInsProps, m_dataLoan.sSettlementOwnerTitleInsFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sOwnerTitleInsF_rep,
                        m_dataLoan.sOwnerTitleInsPaidTo,
                        null,
                        null),

                    // 1104 Lenders Title Insurance
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        E_RespaFeeGfeAggregationType.TitleServices,
                        E_RespaFeeType.Other,
                        "1104",
                        "1104",
                        "Lender Coverage",
                        false,
                        m_dataLoan.sSettlementTitleInsF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sTitleInsFProps, m_dataLoan.sSettlementTitleInsFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sTitleInsF_rep,
                        m_dataLoan.sTitleInsFTable,
                        null,
                        null),

                    // 1109 Doc Preparation Fee
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sDocPrepFGfeSection),
                        E_RespaFeeType.DocumentPreparationFee,
                        "1109",
                        "1105",
                        "",
                        false,
                        m_dataLoan.sSettlementDocPrepF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sDocPrepFProps, m_dataLoan.sSettlementDocPrepFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sDocPrepF_rep,
                        m_dataLoan.sDocPrepFPaidTo,
                        null,
                        null),

                    // 1110 Notary Fees
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sNotaryFGfeSection),
                        E_RespaFeeType.NotaryFee,
                        "1110",
                        "1106",
                        "",
                        false,
                        m_dataLoan.sSettlementNotaryF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sNotaryFProps, m_dataLoan.sSettlementNotaryFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sNotaryF_rep,
                        m_dataLoan.sNotaryFPaidTo,
                        null,
                        null),

                    // 1111 Attorney Fees
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sAttorneyFGfeSection),
                        E_RespaFeeType.AttorneyFee,
                        "1111",
                        "1107",
                        "",
                        false,
                        m_dataLoan.sSettlementAttorneyF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sAttorneyFProps, m_dataLoan.sSettlementAttorneyFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sAttorneyF_rep,
                        m_dataLoan.sAttorneyFPaidTo,
                        null,
                        null),

                    //1112 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sU1TcSettlementSection),
                        E_RespaFeeType.Other,
                        "1112",
                        "1112" /* m_dataLoan.sU1TcCode */,
                        m_dataLoan.sSettlementU1TcDesc,
                        false,
                        m_dataLoan.sSettlementU1Tc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1TcProps, m_dataLoan.sSettlementU1TcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU1Tc_rep,
                        m_dataLoan.sU1TcPaidTo,
                        null,
                        null),

                    //1113 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sU2TcSettlementSection),
                        E_RespaFeeType.Other,
                        "1113",
                        "1113" /* m_dataLoan.sU2TcCode */,
                        m_dataLoan.sSettlementU2TcDesc,
                        false,
                        m_dataLoan.sSettlementU2Tc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2TcProps, m_dataLoan.sSettlementU2TcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU2Tc_rep,
                        m_dataLoan.sU2TcPaidTo,
                        null,
                        null),

                    //1114 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sU3TcSettlementSection),
                        E_RespaFeeType.Other,
                        "1114",
                        "1114" /*m_dataLoan.sU3TcCode */,
                        m_dataLoan.sSettlementU3TcDesc,
                        false,
                        m_dataLoan.sSettlementU3Tc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3TcProps, m_dataLoan.sSettlementU3TcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU3Tc_rep,
                        m_dataLoan.sU3TcPaidTo,
                        null,
                        null),

                    //1115 - Others
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1100_TitleCharges,
                        ToMismo(m_dataLoan.sU4TcSettlementSection),
                        E_RespaFeeType.Other,
                        "1115",
                        "1115" /* m_dataLoan.sU4TcCode */,
                        m_dataLoan.sSettlementU4TcDesc,
                        false,
                        m_dataLoan.sSettlementU4Tc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU4TcProps, m_dataLoan.sSettlementU4TcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU4Tc_rep,
                        m_dataLoan.sU4TcPaidTo,
                        null,
                        null),

                    // 1204 City tax/stamps
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        E_RespaFeeGfeAggregationType.TransferTaxes,
                        E_RespaFeeType.CityCountyMortgageTaxStampFee,
                        "1204",
                        "1202",
                        "",
                        false,
                        m_dataLoan.sSettlementCountyRtc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sCountyRtcProps, m_dataLoan.sSettlementCountyRtcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sCountyRtc_rep,
                        m_dataLoan.sCountyRtcDesc,
                        null,
                        null),

                    // 1205 State tax/stamps
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        E_RespaFeeGfeAggregationType.TransferTaxes,
                        E_RespaFeeType.StateMortgageTaxStampFee,
                        "1205",
                        "1203",
                        "",
                        false,
                        m_dataLoan.sSettlementStateRtc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sStateRtcProps, m_dataLoan.sSettlementStateRtcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sStateRtc_rep,
                        m_dataLoan.sStateRtcDesc,
                        null,
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(m_dataLoan.sU1GovRtcSettlementSection),
                        E_RespaFeeType.Other,
                        "1206",
                        "1206" /*m_dataLoan.sU1GovRtcCode */,
                        m_dataLoan.sSettlementU1GovRtcDesc,
                        false,
                        m_dataLoan.sSettlementU1GovRtc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1GovRtcProps, m_dataLoan.sSettlementU1GovRtcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU1GovRtc_rep,
                        m_dataLoan.sU1GovRtcPaidTo,
                        null,
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(m_dataLoan.sU2GovRtcSettlementSection),
                        E_RespaFeeType.Other,
                        "1207",
                        "1207" /* m_dataLoan.sU2GovRtcCode */,
                        m_dataLoan.sSettlementU2GovRtcDesc,
                        false,
                        m_dataLoan.sSettlementU2GovRtc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2GovRtcProps, m_dataLoan.sSettlementU2GovRtcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU2GovRtc_rep,
                        m_dataLoan.sU2GovRtcPaidTo,
                        null,
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                        ToMismo(m_dataLoan.sU3GovRtcSettlementSection),
                        E_RespaFeeType.Other,
                        "1208",
                        "1208" /* m_dataLoan.sU3GovRtcCode */,
                        m_dataLoan.sSettlementU3GovRtcDesc,
                        false,
                        m_dataLoan.sSettlementU3GovRtc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3GovRtcProps, m_dataLoan.sSettlementU3GovRtcProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU3GovRtc_rep,
                        m_dataLoan.sU3GovRtcPaidTo,
                        null,
                        null),

                    // 1302 Pest Inspection
                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor,
                        E_RespaFeeType.PestInspectionFee,
                        "1302",
                        "1302",
                        "",
                        false,
                        m_dataLoan.sSettlementPestInspectF_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sPestInspectFProps, m_dataLoan.sSettlementPestInspectFProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sPestInspectF_rep,
                        m_dataLoan.sPestInspectPaidTo,
                        null,
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU1ScSettlementSection),
                        E_RespaFeeType.Other,
                        "1303",
                        "1303" /* m_dataLoan.sU1ScCode */,
                        m_dataLoan.sSettlementU1ScDesc,
                        false,
                        m_dataLoan.sSettlementU1Sc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1ScProps, m_dataLoan.sSettlementU1ScProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU1Sc_rep,
                        m_dataLoan.sU1ScPaidTo,
                        null,
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU2ScSettlementSection),
                        E_RespaFeeType.Other,
                        "1304",
                        "1304" /* m_dataLoan.sU2ScCode */,
                        m_dataLoan.sSettlementU2ScDesc,
                        false,
                        m_dataLoan.sSettlementU2Sc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2ScProps, m_dataLoan.sSettlementU2ScProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU2Sc_rep,
                        m_dataLoan.sU2ScPaidTo,
                        null,
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU3ScSettlementSection),
                        E_RespaFeeType.Other,
                        "1305",
                        "1305" /* m_dataLoan.sU3ScCode */,
                        m_dataLoan.sSettlementU3ScDesc,
                        false,
                        m_dataLoan.sSettlementU3Sc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3ScProps, m_dataLoan.sSettlementU3ScProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU3Sc_rep,
                        m_dataLoan.sU3ScPaidTo,
                        null,
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU4ScSettlementSection),
                        E_RespaFeeType.Other,
                        "1306",
                        "1306" /* m_dataLoan.sU4ScCode */,
                        m_dataLoan.sSettlementU4ScDesc,
                        false,
                        m_dataLoan.sSettlementU4Sc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU4ScProps, m_dataLoan.sSettlementU4ScProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU4Sc_rep,
                        m_dataLoan.sU4ScPaidTo,
                        null,
                        null),

                    new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges,
                        ToMismo(m_dataLoan.sU5ScSettlementSection),
                        E_RespaFeeType.Other,
                        "1307",
                        "1307" /* m_dataLoan.sU5ScCode */,
                        m_dataLoan.sSettlementU5ScDesc,
                        false,
                        m_dataLoan.sSettlementU5Sc_rep,
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU5ScProps, m_dataLoan.sSettlementU5ScProps),
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sU5Sc_rep,
                        m_dataLoan.sU5ScPaidTo,
                        null,
                        null),
                };
                return m_SCFeeItems;
            }
        }

        private Dictionary<string, MISMOFeeItem> m_GFESpecialFeeItems;
        /// <summary>
        /// A Dictionary<HUDLine, RespaFee> of special fees.
        /// The special fees are:
        /// 801
        /// 802
        /// 902
        /// 903-906
        /// 1201
        /// These fees cannot be handled solely by the CreateRespaFee function.
        /// </summary>
        private Dictionary<string, MISMOFeeItem> GFESpecialFeeItems
        {
            get
            {
                if (m_GFESpecialFeeItems != null)
                    return m_GFESpecialFeeItems;

                m_GFESpecialFeeItems = new Dictionary<string, MISMOFeeItem>();

                // 802 Credit or Charge
                {
                    MISMOFeeItem mismoFee = null;
                    //OPM 69510 av 8/26  only send this if its positive else send a YSP 
                    mismoFee = new MISMOFeeItem(
                        E_RespaFeeRespaSectionClassificationType._800_LoanFees, // SectionClassificationType
                        E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, // AggregationType
                        E_RespaFeeType.LoanDiscountPoints, // FeeType
                        "802", // trueHUDLineNumber
                        "802", // dmHUDLineNumber
                        "Discount Point", // description
                        false, // isPaid
                        m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointF_rep, // amount
                        m_dataLoan.sGfeDiscountPointFProps, // properties containing Paid by, APR, FHA allowable, etc.
                        m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointFPc_rep, // point
                        m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointFRoundingError_rep, // opm 70657: compensate for the rounding error. 12/1/11 M.P.
                        m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointF_rep, // gfeDisclosedAmount, opm 128959: use sGfeDiscountPointF_rep instead of sLDiscnt_rep. 7/23/13 GF.
                        null, // paidToOverrideString
                        "LQBGFE802RESPA", // id
                        m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointF_rep); // opm 70657: include a TotalAmount. 11/16/11 M.P.

                    m_GFESpecialFeeItems.Add("802", mismoFee);
                }

                // 902 Mortgage Insurance Premium
                {
                    string mipFee = m_dataLoanWithGfeArchiveApplied.sFfUfmip1003_rep;

                    MISMOFeeItem mismoFee = new MISMOFeeItem(
                        E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                        RespaFee902Type(m_dataLoan.sLT),
                        "902",
                        "902",
                        null,
                        false,
                        m_dataLoanWithGfeArchiveApplied.sMipPia_rep,
                        m_dataLoan.sMipPiaProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sMipPia_rep,
                        m_dataLoan.sMipPiaPaidTo,
                        "LQBGFE902",
                        null);

                    // 902 Mortgage Insurance Premium
                    m_GFESpecialFeeItems.Add("902", mismoFee);
                }

                // 903-906
                {
                    m_GFESpecialFeeItems.Add("903",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.Undefined,
                            E_RespaFeeType.Undefined,
                            "903",                      // true Hud line # 
                            "903",                      // dm line number
                            "",                         // description
                            false,                      // isPaid
                            m_dataLoan.sHazInsPia_rep,  // amount
                            m_dataLoan.sHazInsPiaProps, // props
                            null,                       // point
                            null,                       // fixed amount
                            m_dataLoan.sHazInsPia_rep,  // gfe disclosed amount
                            null,                       // paidToOverride
                            null,                       // id
                            null));                     // total Amount.

                    m_GFESpecialFeeItems.Add("904",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                            E_RespaFeeType.Other,
                            "904",
                            "904",
                            m_dataLoanWithGfeArchiveApplied.s904PiaDesc,
                            false,
                            m_dataLoanWithGfeArchiveApplied.s904Pia_rep,
                            m_dataLoan.s904PiaProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.s904Pia_rep,
                            null,
                            null,
                            null));

                    m_GFESpecialFeeItems.Add("905",
                        new MISMOFeeItem(
                            E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                            E_RespaFeeType.VAFundingFee,
                            "905",
                            "905",
                            string.Empty,
                            false,
                            m_dataLoanWithGfeArchiveApplied.sVaFf_rep,
                            m_dataLoan.sVaFfProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sVaFf_rep,
                            m_dataLoan.sVaFfPaidTo,
                            "LQBGFE905",
                            null));

                    m_GFESpecialFeeItems.Add("906",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                            E_RespaFeeType.Other,
                            "906",
                            m_dataLoan.s900U1PiaCode,
                            m_dataLoanWithGfeArchiveApplied.s900U1PiaDesc,
                            false,
                            m_dataLoanWithGfeArchiveApplied.s900U1Pia_rep,
                            m_dataLoan.s900U1PiaProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.s900U1Pia_rep,
                            null,
                            null,
                            null));
                }

                // 1201/1202 Recording Fee
                {
                    m_GFESpecialFeeItems.Add("1201",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                            E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.Other,
                            "1201",
                            "1201",
                            "RecFilingFee",
                            false,
                            m_dataLoanWithGfeArchiveApplied.sRecF_rep,
                            m_dataLoan.sRecFProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sRecF_rep,
                            m_dataLoan.sRecFDesc,
                            "LQBGFE1201",
                            null));

                    m_GFESpecialFeeItems.Add("1202_Deed",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                            E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.DeedRecordingFee,
                            "1202",
                            "1202",
                            "DeedRecordingFee",
                            false,
                            m_dataLoanWithGfeArchiveApplied.sRecDeed_rep,
                            m_dataLoan.sRecFProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sRecDeed_rep,
                            m_dataLoan.sRecFDesc,
                            "LQBGFE1202A",
                            null));

                    m_GFESpecialFeeItems.Add("1202_Mortgage",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                            E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.MortgageRecordingFee,
                            "1202",
                            "1202",
                            "MortgageRecordingFee",
                            false,
                            m_dataLoanWithGfeArchiveApplied.sRecMortgage_rep,
                            m_dataLoan.sRecFProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sRecMortgage_rep,
                            m_dataLoan.sRecFDesc,
                            "LQBGFE1202B",
                            null));

                    m_GFESpecialFeeItems.Add("1202_Release",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                            E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.ReleaseRecordingFee,
                            "1202",
                            "1202",
                            "ReleaseRecordingFee",
                            false,
                            m_dataLoanWithGfeArchiveApplied.sRecRelease_rep,
                            m_dataLoan.sRecFProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sRecRelease_rep,
                            m_dataLoan.sRecFDesc,
                            "LQBGFE1202C",
                            null));
                }
                
                return m_GFESpecialFeeItems;
            }
        }

        private Dictionary<string, MISMOFeeItem> m_SCSpecialFeeItems;
        /// <summary>
        /// A Dictionary<HUDLine, RespaFee> of special fees for settlement charges.
        /// The special fees are:
        /// 801
        /// 802 : sLDiscnt dependency, TotalAmount
        /// 902 : switch statement based on loan type
        /// 903-906 : not exported normally
        /// 1201 : TypeOtherDescription
        /// These fees cannot be handled solely by the CreateRespaFee function.
        /// </summary>
        private Dictionary<string, MISMOFeeItem> SCSpecialFeeItems
        {
            get
            {
                if (m_exportSource != E_SettlementChargesExportSource.SETTLEMENT)
                {
                    // Return an empty dict, since we shouldn't be exporting these
                    return new Dictionary<string, MISMOFeeItem>();
                }

                if (m_SCSpecialFeeItems != null)
                    return m_SCSpecialFeeItems;

                m_SCSpecialFeeItems = new Dictionary<string, MISMOFeeItem>();

                // 802 Credit or Charge
                {
                    MISMOFeeItem mismoFee = null;
                    //OPM 69510 av 8/26  only send this if its positive else send a YSP 
                    mismoFee = new MISMOFeeItem(
                        E_RespaFeeRespaSectionClassificationType._800_LoanFees, // SectionClassificationType
                        E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge, // AggregationType
                        E_RespaFeeType.LoanDiscountPoints, // FeeType
                        "802", // trueHUDLineNumber
                        "802", // dmHUDLineNumber
                        "Discount Point", // description
                        false, // isPaid
                        m_dataLoan.sSettlementDiscountPointF_rep, // amount
                        LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sGfeDiscountPointFProps, m_dataLoan.sSettlementLDiscntProps), // properties containing Paid by, APR, FHA allowable, etc.
                        m_dataLoan.sSettlementDiscountPointFPc_rep, // point
                        m_dataLoan.sSettlementDiscountPointFRoundingError_rep, // opm 70657: compensate for the rounding error. 12/1/11 M.P.
                        m_dataLoanWithGfeArchiveApplied.sGfeDiscountPointF_rep, // gfeDisclosedAmount, opm 128959: use sGfeDiscountPointF_rep instead of sLDiscnt_rep. 7/23/13 GF.
                        null, // paidToOverrideString
                        null, // id
                        m_dataLoan.sSettlementDiscountPointF_rep); // opm 70657: include a TotalAmount. 11/16/11 M.P.
                    m_SCSpecialFeeItems.Add("802", mismoFee);
                }

                // 901 Interest
                {
                    m_SCSpecialFeeItems.Add("901",
                        new MISMOFeeItem(
                            E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.Undefined,
                            E_RespaFeeType.Other,
                            "901",
                            "901",
                            "Interest",
                            false,
                            null,
                            512, //Sets all props to "N" and _PaidByType="Seller"
                            null,
                            null,
                            null,
                            null,
                            null,
                            null));
                }

                // 902 Mortgage Insurance Premium
                {
                    string mipFee = m_dataLoan.sFfUfmip1003_rep;

                    MISMOFeeItem mismoFee = new MISMOFeeItem(
                        E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                        E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                        RespaFee902Type(m_dataLoan.sLT),
                        "902",
                        "902",
                        null,
                        false,
                        m_dataLoan.sMipPia_rep,
                        m_dataLoan.sMipPiaProps,
                        null,
                        null,
                        m_dataLoanWithGfeArchiveApplied.sMipPia_rep,
                        m_dataLoan.sMipPiaPaidTo,
                        null,
                        m_dataLoan.sMipPia_rep);

                    // 902 Mortgage Insurance Premium
                    m_SCSpecialFeeItems.Add("902", mismoFee);
                }

                // 903-906
                {
                    m_SCSpecialFeeItems.Add("903",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.Undefined,
                            E_RespaFeeType.Undefined,
                            "903",
                            "903",
                            "",
                            false,
                            m_dataLoan.sSettlementHazInsPia_rep,
                            m_dataLoan.sSettlementHazInsPiaProps,
                            null,
                            null,
                            m_dataLoan.sHazInsPia_rep,
                            null,
                            null,
                            null));

                    m_SCSpecialFeeItems.Add("904",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                            E_RespaFeeType.Other,
                            "904",
                            "904",
                            m_dataLoan.sSettlement904PiaDesc,
                            false,
                            m_dataLoan.sSettlement904Pia_rep,
                            m_dataLoan.sSettlement904PiaProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.s904Pia_rep,
                            null,
                            null,
                            null));

                    m_SCSpecialFeeItems.Add("905",
                        new MISMOFeeItem(
                            E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                            E_RespaFeeType.VAFundingFee,
                            "905",
                            "905",
                            "",
                            false,
                            m_dataLoan.sVaFf_rep,
                            m_dataLoan.sVaFfProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sVaFf_rep,
                            m_dataLoan.sVaFfPaidTo,
                            null,
                            m_dataLoan.sVaFf_rep));

                    m_SCSpecialFeeItems.Add("906",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance,
                            E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected,
                            E_RespaFeeType.Other,
                            "906",
                            "906",
                            m_dataLoan.sSettlement900U1PiaDesc,
                            false,
                            m_dataLoan.sSettlement900U1Pia_rep,
                            m_dataLoan.sSettlement900U1PiaProps,
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.s900U1Pia_rep,
                            null,
                            null,
                            null));
                }

                // 1201 Recording Fee
                {
                    m_SCSpecialFeeItems.Add("1201",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                            E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.Other,
                            "1201",
                            "1201",
                            "RecFilingFee",
                            false,
                            m_dataLoan.sSettlementRecF_rep,
                            LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps),
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sRecF_rep,
                            m_dataLoan.sRecFDesc,
                            null,
                            null));

                    m_SCSpecialFeeItems.Add("1202_Deed",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                            E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.DeedRecordingFee,
                            "1202",
                            "1202",
                            "DeedRecordingFee",
                            false,
                            m_dataLoan.sSettlementRecDeed_rep,
                            LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps),
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sRecDeed_rep,
                            m_dataLoan.sRecFDesc,
                            null,
                            null));

                    m_SCSpecialFeeItems.Add("1202_Mortgage",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                            E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.MortgageRecordingFee,
                            "1202",
                            "1202",
                            "MortgageRecordingFee",
                            false,
                            m_dataLoan.sSettlementRecMortgage_rep,
                            LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps),
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sRecMortgage_rep,
                            m_dataLoan.sRecFDesc,
                            null,
                            null));

                    m_SCSpecialFeeItems.Add("1202_Release",
                        new MISMOFeeItem(E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges,
                            E_RespaFeeGfeAggregationType.GovernmentRecordingCharges, E_RespaFeeType.ReleaseRecordingFee,
                            "1202",
                            "1202",
                            "ReleaseRecordingFee",
                            false,
                            m_dataLoan.sSettlementRecRelease_rep,
                            LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps),
                            null,
                            null,
                            m_dataLoanWithGfeArchiveApplied.sRecRelease_rep,
                            m_dataLoan.sRecFDesc,
                            null,
                            null));
                }

                return m_SCSpecialFeeItems;
            }
        }

        // 12/17/2013 gf - opm 146376. Want to validate/update fees that have custom descriptions or multiple box options.
        // 1008 and 1009 are not included because the same fields are used for SC and GFE.
        private static HashSet<string> x_settlementChargeFeesToVerify = new HashSet<string>()
        {
            "813",
            "814",
            "815",
            "816",
            "817",
            "904",
            "906",
            "1112",
            "1113",
            "1114",
            "1115",
            "1206",
            "1207",
            "1208",
            "1303",
            "1304",
            "1305",
            "1306",
            "1307"
        };

        /// <summary>
        /// Update the SC fee with the corresponding GFE fee if one value is 0 and the other is non-zero.
        /// If both fees are non-zero and there is a discrepancy between descriptions or GFE box, add to member discrepancies.
        /// </summary>
        /// <param name="scFee">The SC fee. May be modified.</param>
        /// <param name="gfeFee">The GFE fee.</param>
        private void ValidateSCFeeWithGFEFee(MISMOFeeItem scFee, MISMOFeeItem gfeFee)
        {
            if (!x_settlementChargeFeesToVerify.Contains(scFee.TrueHUDLineNumber))
            {
                return;
            }

            var gfeFeeAmt = m_dataLoan.m_convertLos.ToMoney(gfeFee.Amount);
            var scFeeAmt = m_dataLoan.m_convertLos.ToMoney(scFee.Amount);

            if (gfeFeeAmt != 0 && scFeeAmt != 0)
            {
                if (gfeFee.Description != scFee.Description)
                {
                    m_feeDiscrepancies.Add(string.Format("The description for the fee on line {0} does not"
                        + " match between the GFE and Settlement Charges.", scFee.TrueHUDLineNumber));
                }
                if (gfeFee.AggregationType != scFee.AggregationType)
                {
                    m_feeDiscrepancies.Add(string.Format("The GFE box for the fee on line {0} does not"
                        + " match between the GFE and Settlement Charges.", scFee.TrueHUDLineNumber));
                }
            }
            else if (gfeFeeAmt != 0 && scFeeAmt == 0)
            {
                scFee.Description = gfeFee.Description;
                scFee.AggregationType = gfeFee.AggregationType;
            }
            else if (gfeFeeAmt == 0 && scFeeAmt != 0)
            {
                scFee.GFEDisclosedAmount = m_dataLoan.m_convertLos.ToMoneyString(0, FormatDirection.ToRep);
            }
        }

        private Mismo.Closing2_6.RespaFee GetSpecialFee(string trueHUDLine)
        {
            return GetSpecialFee(trueHUDLine, mxGfe, miGfe);
        }
        /// <summary>
        /// If there's an exception here, we have a code error, since the special fees
        /// will always have the same HUDLine numbers.
        /// </summary>
        /// <param name="trueHUDLine">The line number on our form.</param>
        /// <returns></returns>
        private Mismo.Closing2_6.RespaFee GetSpecialFee(string trueHUDLine, bool gfe, bool includeGFE)
        {
            Mismo.Closing2_6.RespaFee fee;
            MISMOFeeItem mismoFee;
            if (gfe)
                mismoFee = GFESpecialFeeItems[trueHUDLine];
            else
            {
                mismoFee = SCSpecialFeeItems[trueHUDLine];
                ValidateSCFeeWithGFEFee(mismoFee, GFESpecialFeeItems[trueHUDLine]);
            }

            if (mismoFee == null)
            {
                return null;
            }

            fee = CreateRespaFee(
                mismoFee.ClassificationType,
                mismoFee.AggregationType,
                mismoFee.Type,
                mismoFee.DMLineNumber,
                mismoFee.Description,
                mismoFee.IsPaid,
                mismoFee.Amount,
                mismoFee.Props,
                mismoFee.Point,
                mismoFee.FixedAmount,
                includeGFE ? mismoFee.GFEDisclosedAmount : null,
                mismoFee.PaidToOverride,
                mismoFee.ID,
                mismoFee.TotalAmount);

            return fee;
        }

        private void CreateLoanOriginator(Application application)
        {
            // 11/11/2010 dd - OPM 59574 - Support nationwide mortgage licensing system.
            // 11/11/2010 dd - Order of these two objects are important. The first record must be company information.
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            LoanOriginator originatingCompany = new LoanOriginator();
            originatingCompany.UnparsedName = preparer.CompanyName;
            originatingCompany.NonPersonEntityIndicator = E_YNIndicator.Y;
            originatingCompany.NationwideMortgageLicensingSystemAssignedIdentifier = preparer.CompanyLoanOriginatorIdentifier;
            originatingCompany.StreetAddress = preparer.StreetAddr;
            originatingCompany.City = preparer.City;
            originatingCompany.State = preparer.State;
            originatingCompany.PostalCode = preparer.Zip;
            originatingCompany.ContactDetail.Name = preparer.PreparerName;
            originatingCompany.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Phone, preparer.PhoneOfCompany));
            originatingCompany.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Fax, preparer.FaxOfCompany));
            originatingCompany.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Email, preparer.EmailAddr));
            originatingCompany.LicenseNumberIdentifier = preparer.LicenseNumOfCompany;

            application.LoanOriginatorList.Add(originatingCompany);

            LoanOriginator loanOriginator = new LoanOriginator();
            loanOriginator.UnparsedName = preparer.PreparerName;
            loanOriginator.NonPersonEntityIndicator = E_YNIndicator.N;
            loanOriginator.NationwideMortgageLicensingSystemAssignedIdentifier = preparer.LoanOriginatorIdentifier;
            loanOriginator.LicenseNumberIdentifier = preparer.LicenseNumOfAgent;
            loanOriginator.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Phone, preparer.Phone));
            loanOriginator.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Fax, preparer.FaxNum));
            loanOriginator.ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Email, preparer.EmailAddr));

            application.LoanOriginatorList.Add(loanOriginator);

        }
        private DataInformation CreateDataInformation()
        {
            DataInformation dataInformation = new DataInformation();
            //dataInformation.Id = null;
            //dataInformation.DataVersionList.Add(CreateDataVersion());
            return dataInformation;
        }

        private DataVersion CreateDataVersion()
        {
            DataVersion dataVersion = new DataVersion();
            //dataVersion.Id = null;
            //dataVersion.Name = null;
            //dataVersion.Number = null;
            return dataVersion;
        }

        private AdditionalCaseData CreateAdditionalCaseData()
        {
            AdditionalCaseData additionalCaseData = new AdditionalCaseData();
            //additionalCaseData.Id = null;
            //additionalCaseData.MortgageScoreList.Add(CreateMortgageScore());
            additionalCaseData.TransmittalData = CreateTransmittalData();
            return additionalCaseData;
        }

        private MortgageScore CreateMortgageScore()
        {
            MortgageScore mortgageScore = new MortgageScore();
            //mortgageScore.Id = null;
            //mortgageScore.Date = null;
            //mortgageScore.Type = null;
            //mortgageScore.TypeOtherDescription = null;
            //mortgageScore.Value = null;
            return mortgageScore;
        }

        private TransmittalData CreateTransmittalData()
        {
            TransmittalData transmittalData = new TransmittalData();
            transmittalData.PropertyAppraisedValueAmount = m_dataLoan.sApprValFor_LTV_CLTV_HCLTV_rep;
            transmittalData.RateLockPeriodDays = m_dataLoan.sRLckdDays_rep;
            transmittalData.InvestorLoanIdentifier = m_dataLoan.sInvestorLockLoanNum;
            // 7/14/2011 dd - Based from Request for DocMagic

            switch (m_dataLoan.sBranchChannelT)
            {
                case E_BranchChannelT.Blank:
                case E_BranchChannelT.Retail:
                case E_BranchChannelT.Wholesale:
                    transmittalData.LoanOriginatorType = E_TransmittalDataLoanOriginatorType.Lender;
                    break;
                case E_BranchChannelT.Correspondent:
                    transmittalData.LoanOriginatorType = E_TransmittalDataLoanOriginatorType.Correspondent;
                    break;
                case E_BranchChannelT.Broker:
                    transmittalData.LoanOriginatorType = E_TransmittalDataLoanOriginatorType.Broker;
                    break;
                default:
                    throw new UnhandledEnumException(m_dataLoan.sBranchChannelT);
            }

            //transmittalData.Id = null;
            //transmittalData.ArmsLengthIndicator = null;
            //transmittalData.BelowMarketSubordinateFinancingIndicator = null;
            //transmittalData.BuydownRatePercent = null;
            //transmittalData.CaseStateType = null;
            //transmittalData.CaseStateTypeOtherDescription = null;
            //transmittalData.CommitmentReferenceIdentifier = null;
            //transmittalData.ConcurrentOriginationIndicator = null;
            //transmittalData.ConcurrentOriginationLenderIndicator = null;
            //transmittalData.CreditReportAuthorizationIndicator = null;
            //transmittalData.CurrentFirstMortgageHolderType = null;
            //transmittalData.CurrentFirstMortgageHolderTypeOtherDescription = null;
            //transmittalData.InvestorInstitutionIdentifier = null;
            //transmittalData.InvestorLoanIdentifier = null;
            //transmittalData.LenderBranchIdentifier = null;
            //transmittalData.LenderRegistrationIdentifier = null;
            //transmittalData.LoanOriginationSystemLoanIdentifier = null;
            //transmittalData.LoanOriginatorTypeOtherDescription = null;
            //transmittalData.PropertiesFinancedByLenderCount = null;
            //transmittalData.PropertyEstimatedValueAmount = null;
            //transmittalData.RateLockRequestedExtensionDays = null;
            //transmittalData.RateLockType = null;
            return transmittalData;
        }

        private AffordableLending CreateAffordableLending()
        {
            AffordableLending affordableLending = new AffordableLending();
            //affordableLending.Id = null;
            //affordableLending.FnmCommunityLendingProductName = null;
            //affordableLending.FnmCommunityLendingProductType = null;
            //affordableLending.FnmCommunityLendingProductTypeOtherDescription = null;
            //affordableLending.FnmCommunitySecondsIndicator = null;
            //affordableLending.FnmNeighborsMortgageEligibilityIndicator = null;
            //affordableLending.FreAffordableProgramIdentifier = null;
            //affordableLending.HudIncomeLimitAdjustmentFactor = null;
            //affordableLending.HudLendingIncomeLimitAmount = null;
            //affordableLending.HudMedianIncomeAmount = null;
            //affordableLending.MsaIdentifier = null;
            return affordableLending;
        }

        private Asset CreateAsset(IAssetRetirement retirement, string borrowerId)
        {
            if (retirement == null) { return null; }
            Asset asset = new Asset()
            {
                BorrowerId = borrowerId,
                Type = E_AssetType.RetirementFund,
                CashOrMarketValueAmount = retirement.Val_rep
            };
            return asset;
        }
        private Asset CreateAsset(IAssetBusiness business, string borrowerId)
        {
            if (null == business) return null;

            Asset asset = new Asset()
            {
                BorrowerId = borrowerId,
                Type = E_AssetType.NetWorthOfBusinessOwned,
                CashOrMarketValueAmount = business.Val_rep,

            };
            return asset;
        }
        private Asset CreateAsset(IAssetLifeInsurance lifeIns, string borrowerId)
        {
            if (null == lifeIns) return null;

            Asset asset = new Asset()
            {
                BorrowerId = borrowerId,
                Type = E_AssetType.LifeInsurance,
                CashOrMarketValueAmount = lifeIns.Val_rep,
                LifeInsuranceFaceValueAmount = lifeIns.FaceVal_rep
            };
            return asset;
        }
        private Asset CreateAsset(IAssetCashDeposit cashDeposit, string borrowerId)
        {
            if (null == cashDeposit) return null;

            Asset asset = new Asset()
            {
                BorrowerId = borrowerId,
                Type = E_AssetType.EarnestMoneyCashDepositTowardPurchase,
                CashOrMarketValueAmount = cashDeposit.Val_rep,
                HolderName = cashDeposit.Desc
            };
            //asset.Id = null;
            //asset.BorrowerId = null;
            //asset.AssetDescription = null;
            //asset.AutomobileMakeDescription = null;
            //asset.AutomobileModelYear = null;
            //asset.OtherAssetTypeDescription = null;
            //asset.LifeInsuranceFaceValueAmount = null;
            //asset.StockBondMutualFundShareCount = null;
            //asset.AccountIdentifier = null;
            //asset.HolderCity = null;
            //asset.HolderPostalCode = null;
            //asset.HolderState = null;
            //asset.HolderStreetAddress = null;
            //asset.HolderStreetAddress2 = null;
            //asset.VerifiedIndicator = null;
            return asset;
        }
        private Asset CreateAsset(IAssetRegular assetField, CAppData dataApp)
        {
            if (null == assetField) return null;
            Asset asset = new Asset()
            {
                AccountIdentifier = assetField.AccNum.Value,
                Type = ToMismo(assetField.AssetT),
                CashOrMarketValueAmount = assetField.Val_rep,
                HolderName = assetField.ComNm,
                HolderStreetAddress = assetField.StAddr,
                HolderCity = assetField.City,
                HolderState = assetField.State,
                HolderPostalCode = assetField.Zip,
                OtherAssetTypeDescription = assetField.OtherTypeDesc
            };
            if (assetField.OwnerT == E_AssetOwnerT.Borrower)
            {
                asset.BorrowerId = dataApp.aBMismoId;
            }
            else if (assetField.OwnerT == E_AssetOwnerT.CoBorrower)
            {
                asset.BorrowerId = dataApp.aCMismoId;
            }
            else if (assetField.OwnerT == E_AssetOwnerT.Joint)
            {
                if (dataApp.aCIsValidNameSsn)
                {
                    asset.BorrowerId = dataApp.aBMismoId + " " + dataApp.aCMismoId;
                }
                else
                {
                    asset.BorrowerId = dataApp.aBMismoId;
                }
            }

            if (assetField.AssetT == E_AssetRegularT.Auto)
            {
                asset.AutomobileMakeDescription = assetField.Desc;
            }


            return asset;
        }

        private E_AssetType ToMismo(E_AssetRegularT assetRegularT)
        {
            switch (assetRegularT)
            {
                case E_AssetRegularT.Auto: return E_AssetType.Automobile;
                case E_AssetRegularT.Bonds: return E_AssetType.Bond;
                case E_AssetRegularT.Checking: return E_AssetType.CheckingAccount;
                case E_AssetRegularT.GiftFunds: return E_AssetType.GiftsNotDeposited;
                case E_AssetRegularT.Savings: return E_AssetType.SavingsAccount;
                case E_AssetRegularT.Stocks: return E_AssetType.Stock;
                case E_AssetRegularT.OtherIlliquidAsset: return E_AssetType.OtherNonLiquidAssets;
                case E_AssetRegularT.OtherLiquidAsset: return E_AssetType.OtherLiquidAssets;
                case E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets: return E_AssetType.PendingNetSaleProceedsFromRealEstateAssets;
                case E_AssetRegularT.GiftEquity: return E_AssetType.GiftsTotal;
                case E_AssetRegularT.CertificateOfDeposit: return E_AssetType.CertificateOfDepositTimeDeposit;
                case E_AssetRegularT.MoneyMarketFund: return E_AssetType.MoneyMarketFund;
                case E_AssetRegularT.MutualFunds: return E_AssetType.MutualFund;
                case E_AssetRegularT.SecuredBorrowedFundsNotDeposit: return E_AssetType.SecuredBorrowedFundsNotDeposited;
                case E_AssetRegularT.BridgeLoanNotDeposited: return E_AssetType.BridgeLoanNotDeposited;
                case E_AssetRegularT.TrustFunds: return E_AssetType.TrustAccount;
                default:
                    return E_AssetType.Undefined;
            }
        }
        private DownPayment CreateDownPayment()
        {
            // OPM 110774 - Allow DownPayment on other loan types
            //if (m_dataLoan.sLPurposeT != E_sLPurposeT.Purchase)
            //    return null;

            DownPayment downPayment = new DownPayment();
            //downPayment.Id = null;
            downPayment.Amount = m_dataLoan.sEquityCalc_rep;
            downPayment.SourceDescription = m_dataLoan.sDwnPmtSrcExplain;
            downPayment.Type = ToMismoDownPaymentType(m_dataLoan.sDwnPmtSrc);
            //downPayment.TypeOtherDescription = null;
            return downPayment;
        }

        private E_DownPaymentType ToMismoDownPaymentType(string src)
        {
            switch (src.ToLower())
            {
                case "checking/savings": return E_DownPaymentType.CheckingSavings;
                case "gift funds": return E_DownPaymentType.GiftFunds;
                case "stocks & bonds": return E_DownPaymentType.StocksAndBonds;
                case "lot equity": return E_DownPaymentType.LotEquity;
                case "bridge loan": return E_DownPaymentType.BridgeLoan;
                case "trust funds": return E_DownPaymentType.TrustFunds;
                case "retirement funds": return E_DownPaymentType.RetirementFunds;
                case "life insurance cash value": return E_DownPaymentType.LifeInsuranceCashValue;
                case "sale of chattel": return E_DownPaymentType.SaleOfChattel;
                case "trade equity": return E_DownPaymentType.TradeEquity;
                case "sweat equity": return E_DownPaymentType.SweatEquity;
                case "cash on hand": return E_DownPaymentType.CashOnHand;
                case "deposit on sales contract": return E_DownPaymentType.DepositOnSalesContract;
                case "equity from pending sale": return E_DownPaymentType.EquityOnPendingSale;
                case "equity from subject property": return E_DownPaymentType.EquityOnSubjectProperty;
                case "equity on sold property": return E_DownPaymentType.EquityOnSoldProperty;
                case "other type of down payment": return E_DownPaymentType.OtherTypeOfDownPayment;
                case "rent with option to purchase": return E_DownPaymentType.RentWithOptionToPurchase;
                case "secured borrowed funds": return E_DownPaymentType.SecuredBorrowedFunds;
                case "unsecured borrowed funds": return E_DownPaymentType.UnsecuredBorrowedFunds;
                case "forgivable secured loan": return E_DownPaymentType.ForgivableSecuredLoan;

                default:
                    return E_DownPaymentType.Undefined;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="props">An integer (?!) representing the Paid By and checkboxes on the rightmost columns of the GFE</param>
        /// <returns></returns>
        private Escrow CreateEscrow(string hudLineNumber, string otherDesc, string monthlyPaymentAmount_rep, decimal monthlyPaymentAmount, string numOfMonths, string premiumAmount, int props, string gfeDisclosedAmount, string paidToOverrideString)
        {
            if (string.IsNullOrEmpty(premiumAmount) || premiumAmount == "0.00")
                return null;

            int colIndex = -1;
            Escrow escrow = new Escrow();
            bool isPremium = false;
            E_EscrowItemType itemType = E_EscrowItemType.Undefined;
            switch (hudLineNumber)
            {
                case "903":
                    itemType = E_EscrowItemType.HazardInsurance;
                    isPremium = true;
                    if (string.IsNullOrEmpty(paidToOverrideString) == false)
                    {
                        escrow.PaidTo = CreatePaidTo(paidToOverrideString);
                    }
                    else
                    {
                        escrow.PaidTo = CreatePaidTo(E_AgentRoleT.HazardInsurance);
                    }
                    break;
                case "904":
                    itemType = E_EscrowItemType.Other;
                    isPremium = true;
                    break;
                case "905":
                    itemType = E_EscrowItemType.Other;
                    isPremium = true;
                    break;
                case "906":
                    itemType = E_EscrowItemType.Other;
                    isPremium = true;
                    break;
                case "1001":
                    itemType = E_EscrowItemType.HazardInsurance;
                    escrow.PaidTo = CreatePaidTo(E_AgentRoleT.HazardInsurance);
                    colIndex = 1; // HAZARD
                    break;
                case "1003":
                    itemType = E_EscrowItemType.SchoolPropertyTax;
                    colIndex = 4; // SCHOOL TAX
                    break;
                case "1004":
                    itemType = E_EscrowItemType.CountyPropertyTax;
                    colIndex = 0; // PROPERTY TAX
                    break;
                case "1005":
                    itemType = E_EscrowItemType.FloodInsurance;
                    colIndex = 3; // FLOOD
                    escrow.PaidTo = CreatePaidTo(E_AgentRoleT.FloodProvider);
                    break;
                case "1006":
                    itemType = E_EscrowItemType.Other;
                    colIndex = 5; // OTHER 1
                    break;
                case "1007":
                    itemType = E_EscrowItemType.Other;
                    colIndex = 6; // OTHER 2
                    break;
                case "1008":
                    itemType = E_EscrowItemType.Other;
                    colIndex = 7; // OTHER 3
                    break;
                case "1009":
                    itemType = E_EscrowItemType.Other;
                    colIndex = 8; // OTHER 4
                    break;
            }

            escrow.SpecifiedHud1LineNumber = hudLineNumber;
            escrow.ItemType = itemType;
            escrow.ItemTypeOtherDescription = otherDesc;

            if (isPremium)
            {
                escrow.PremiumAmount = premiumAmount;
                escrow.PremiumDurationMonthsCount = numOfMonths;

                if (string.IsNullOrEmpty(gfeDisclosedAmount) == false)
                {
                    escrow.GfeDisclosedPremiumAmount = gfeDisclosedAmount;
                }

                int paidBy = LosConvert.GfeItemProps_Payer(props);
                switch (paidBy)
                {
                    case LosConvert.BORR_PAID_OUTOFPOCKET:
                    case LosConvert.BORR_PAID_FINANCED:
                        escrow.PremiumPaidByType = E_EscrowPremiumPaidByType.Buyer;
                        break;
                    case LosConvert.SELLER_PAID:
                        escrow.PremiumPaidByType = E_EscrowPremiumPaidByType.Seller;
                        break;
                    case LosConvert.LENDER_PAID:
                        escrow.PremiumPaidByType = E_EscrowPremiumPaidByType.Lender;
                        break;
                    case LosConvert.BROKER_PAID:
                        escrow.PremiumPaidByType = E_EscrowPremiumPaidByType.Other;
                        break;
                    default:
                        break;
                }


            }
            else
            {

                escrow.MonthlyPaymentAmount = monthlyPaymentAmount_rep;
                escrow.CollectedNumberOfMonthsCount = numOfMonths;
                escrow.AnnualPaymentAmount = m_dataLoan.m_convertLos.ToMoneyString(monthlyPaymentAmount * 12, FormatDirection.ToRep);


                bool hasSchedDueD1 = false;
                DateTime sSchedDueD1 = DateTime.MinValue;

                try
                {
                    sSchedDueD1 = m_dataLoan.sSchedDueD1.DateTimeForComputation;
                    hasSchedDueD1 = true;
                }
                catch (CBaseException)
                {
                    // It is okay to swallow because sSchedDueD1 is missing.
                }
                catch (FormatException)
                {
                    // It is okay to swallow because sSchedDueD1 is missing.
                }
                catch (Exception e)
                {
                    Tools.LogError("DocMagicMismoExporter:: Error trying to get sSchedDueD1 - previously was being swallowed on purpose.", e);
                    throw;
                }

                if (hasSchedDueD1)
                {
                    int[,] sInitialEscrowAcc = m_dataLoan.sInitialEscrowAcc;
                    int index = 1;
                    for (int offset = 0; offset < 12; offset++)
                    {
                        DateTime dueDate = sSchedDueD1.AddMonths(offset);
                        int nMonths = sInitialEscrowAcc[dueDate.Month, colIndex];
                        if (nMonths > 0)
                        {

                            decimal amount = (decimal)nMonths * monthlyPaymentAmount;
                            escrow.PaymentsList.Add(CreatePayments(m_dataLoan.m_convertLos.ToDateTimeString(dueDate), m_dataLoan.m_convertLos.ToMoneyString(amount, FormatDirection.ToRep), index.ToString()));
                            index++;
                        }
                    }
                }

                int paidBy = LosConvert.GfeItemProps_Payer(props);
                switch (paidBy)
                {
                    case LosConvert.BORR_PAID_OUTOFPOCKET:
                    case LosConvert.BORR_PAID_FINANCED:
                        escrow.PaidByType = E_EscrowPaidByType.Buyer;
                        break;
                    case LosConvert.SELLER_PAID:
                        escrow.PaidByType = E_EscrowPaidByType.Seller;
                        break;
                    case LosConvert.LENDER_PAID:
                        escrow.PaidByType = E_EscrowPaidByType.LenderPremium;
                        break;
                    case LosConvert.BROKER_PAID:
                        escrow.PaidByType = E_EscrowPaidByType.Other;
                        break;
                    default:
                        break;
                }
            }

            if (m_exportSource == E_SettlementChargesExportSource.GFE)
            {
                escrow.Id = "LQBGFE" + hudLineNumber;
            }

            return escrow;
        }

        private AccountSummary CreateAccountSummary(int cushionMonths)
        {
            AccountSummary accountSummary = new AccountSummary();
            //accountSummary.Id = null;
            //accountSummary.EscrowAggregateAccountingAdjustmentAmount = null;
            accountSummary.EscrowCushionNumberOfMonthsCount = cushionMonths.ToString();
            return accountSummary;
        }

        private Payments CreatePayments(string dueDate, string paymentAmount, string sequenceIdentifier)
        {
            Payments payments = new Payments();

            payments.DueDate = dueDate;
            payments.PaymentAmount = paymentAmount;
            payments.SequenceIdentifier = sequenceIdentifier;

            //payments.Id = null;
            return payments;
        }

        // OPM 43776
        private PaidTo CreatePaidTo(string paidToName)
        {
            PaidTo paidTo = new PaidTo();
            paidTo.Name = paidToName;
            return paidTo;
        }

        private PaidTo CreatePaidTo(E_AgentRoleT agentRole)
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }

            PaidTo paidTo = new PaidTo();
            //paidTo.Id = null;
            //paidTo.NonPersonEntityIndicator = null;
            paidTo.City = agent.City;
            //paidTo.Country = null;
            //paidTo.County = null;
            paidTo.Name = agent.CompanyName;
            paidTo.PostalCode = agent.Zip;
            paidTo.State = agent.State;
            paidTo.StreetAddress = agent.StreetAddr;
            //paidTo.StreetAddress2 = null;
            paidTo.ContactDetail = new ContactDetail();

            paidTo.ContactDetail.Name = agent.AgentName;
            paidTo.ContactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Phone, Value = agent.Phone });
            paidTo.ContactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Fax, Value = agent.FaxNum });
            paidTo.ContactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Email, Value = agent.EmailAddr });


            //paidTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return paidTo;
        }


        private ContactDetail CreateContactDetail(IPreparerFields preparer)
        {
            ContactDetail contactDetail = new ContactDetail();
            contactDetail.Name = preparer.PreparerName;

            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Phone, preparer.Phone));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Fax, preparer.FaxNum));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Undefined, Mismo.Closing2_6.E_ContactPointType.Email, preparer.EmailAddr));

            //contactDetail.Id = null;
            //contactDetail.FirstName = null;
            //contactDetail.Identifier = null;
            //contactDetail.LastName = null;
            //contactDetail.MiddleName = null;
            //contactDetail.NameSuffix = null;
            //contactDetail.SequenceIdentifier = null;
            return contactDetail;
        }
        /// <summary>
        /// Im going to assume that if you are using this call with an agent that the contact info is all work. 
        /// </summary>
        /// <param name="agent"></param>
        /// <returns></returns>
        private ContactDetail CreateContactDetail(CAgentFields agent)
        {
            ContactDetail contactDetail = new ContactDetail();
            contactDetail.Name = agent.AgentName;

            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Phone, agent.Phone));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Fax, agent.FaxNum));
            contactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Email, agent.EmailAddr));

            //contactDetail.Id = null;
            //contactDetail.FirstName = null;
            //contactDetail.Identifier = null;
            //contactDetail.LastName = null;
            //contactDetail.MiddleName = null;
            //contactDetail.NameSuffix = null;
            //contactDetail.SequenceIdentifier = null;
            return contactDetail;
        }

        private Mismo.Closing2_6.ContactPoint CreateContactPoint(E_ContactPointRoleType roleType, Mismo.Closing2_6.E_ContactPointType type, string value)
        {
            if (null == value || "" == value)
                return null;
            Mismo.Closing2_6.ContactPoint contactPoint = new Mismo.Closing2_6.ContactPoint();
            contactPoint.RoleType = roleType;
            contactPoint.Type = type;
            contactPoint.Value = value;

            //contactPoint.Id = null;
            //contactPoint.PreferenceIndicator = null;
            //contactPoint.RoleTypeOtherDescription = null;
            //contactPoint.TypeOtherDescription = null;
            return contactPoint;
        }

        private NonPersonEntityDetail CreateNonPersonEntityDetail()
        {
            NonPersonEntityDetail nonPersonEntityDetail = new NonPersonEntityDetail();
            //nonPersonEntityDetail.Id = null;
            //nonPersonEntityDetail.MersOrganizationIdentifier = null;
            //nonPersonEntityDetail.OrganizationLicensingTypeDescription = null;
            //nonPersonEntityDetail.OrganizationType = null;
            //nonPersonEntityDetail.OrganizationTypeOtherDescription = null;
            //nonPersonEntityDetail.OrganizedUnderTheLawsOfJurisdictionName = null;
            //nonPersonEntityDetail.SuccessorClauseTextDescription = null;
            //nonPersonEntityDetail.TaxIdentificationNumberIdentifier = null;
            //nonPersonEntityDetail.AuthorizedRepresentativeList.Add(CreateAuthorizedRepresentative());
            return nonPersonEntityDetail;
        }

        private AuthorizedRepresentative CreateAuthorizedRepresentative()
        {
            AuthorizedRepresentative authorizedRepresentative = new AuthorizedRepresentative();
            //authorizedRepresentative.Id = null;
            //authorizedRepresentative.AuthorizedToSignIndicator = null;
            //authorizedRepresentative.TitleDescription = null;
            //authorizedRepresentative.UnparsedName = null;
            //authorizedRepresentative.ContactDetail = CreateContactDetail();
            return authorizedRepresentative;
        }

        private GovernmentLoan CreateGovernmentLoan(CAppData pageData)
        {
            if (m_dataLoan.sLT != E_sLT.FHA && m_dataLoan.sLT != E_sLT.VA)
                return null;

            GovernmentLoan governmentLoan = new GovernmentLoan();

            governmentLoan.FhaLoan = CreateFhaLoan();
            if (m_dataLoan.sLT == E_sLT.VA)
            {
                governmentLoan.VaLoan = CreateVaLoan(pageData);
            }

            if (m_dataLoan.sVaPropDesignationT == E_sVaPropDesignationT.EnergyImprovement)
            {
                governmentLoan.FhaVaLoan = CreateFhaVaLoan();
            }


            //governmentLoan.Id = null;

            return governmentLoan;
        }

        private FhaLoan CreateFhaLoan()
        {
            FhaLoan fhaLoan = new FhaLoan();

            if (m_dataLoan.sLT == E_sLT.FHA)
            {
                fhaLoan.FhaEnergyRelatedRepairsOrImprovementsAmount = m_dataLoan.sFHAEnergyEffImprov_rep;
                fhaLoan.FhaUpfrontMiPremiumPercent = m_dataLoanWithGfeArchiveApplied.sFfUfmipR_rep;
                fhaLoan.FhaMiPremiumRefundAmount = m_dataLoan.sFHA203kFHAMipRefund_rep;
                fhaLoan.SectionOfActType = ToMismoLoanSectionOfAct(m_dataLoan.sFHAHousingActSection);
                fhaLoan.FhaCoverageRenewalRatePercent = m_dataLoan.sProMInsR_rep;
            }

            // 4/21/2009 dd - Allow VA loans to communicate Lender and Sponsor ID codes
            fhaLoan.LenderIdentifier = m_dataLoan.sFHALenderIdCode;
            fhaLoan.SponsorIdentifier = m_dataLoan.sFHASponsorAgentIdCode;

            //fhaLoan.Id = null;
            //fhaLoan.BorrowerFinancedFhaDiscountPointsAmount = null;
            //fhaLoan.BorrowerHomeInspectionChosenIndicator = null;
            //fhaLoan.DaysToFhaMiEligibilityCount = null;
            //fhaLoan.FhaAlimonyLiabilityTreatmentType = null;

            //fhaLoan.FhaGeneralServicesAdministrationCodeIdentifier = null;
            //fhaLoan.FhaGeneralServicesAdminstrationCodeIdentifier = null;
            //fhaLoan.FhaLimitedDenialParticipationIdentifier = null;
            //fhaLoan.FhaNonOwnerOccupancyRiderRule248Indicator = null;
            //fhaLoan.FhaRefinanceInterestOnExistingLienAmount = null;
            //fhaLoan.FhaRefinanceOriginalExistingFhaCaseIdentifier = null;
            //fhaLoan.FhaRefinanceOriginalExistingUpFrontMipAmount = null;

            // OPM 112210 GF
            if (m_dataLoan.sFHARatedReferByTotalScorecard)
            {
                fhaLoan.FhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier = m_dataLoan.sFHAAddendumUnderwriterChumsId;
            }

            //fhaLoan.FhaUpfrontMiPremiumPercent = null;

            //fhaLoan.HudAdequateAvailableAssetsIndicator = null;
            //fhaLoan.HudAdequateEffectiveIncomeIndicator = null;
            //fhaLoan.HudCreditCharacteristicsIndicator = null;
            //fhaLoan.HudStableEffectiveIncomeIndicator = null;

            //fhaLoan.SectionOfActTypeOtherDescription = null;
            //fhaLoan.SoldUnderHudSingleFamilyPropertyDispositionProgramIndicator = null;
            IPreparerFields fhaAddendumSponsor = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (fhaAddendumSponsor.IsValid)
            {
                fhaLoan.Sponsor = CreateSponsor(fhaAddendumSponsor);
            }

            return fhaLoan;
        }

        private Sponsor CreateSponsor(IPreparerFields preparerFields)
        {
            Sponsor sponsor = new Sponsor();
            sponsor.UnparsedName = preparerFields.CompanyName;
            sponsor.StreetAddress = preparerFields.StreetAddr;
            sponsor.City = preparerFields.City;
            sponsor.State = preparerFields.State;
            sponsor.PostalCode = preparerFields.Zip;
            sponsor.ContactDetail = CreateContactDetail(preparerFields);
            return sponsor;
        }

        private E_FhaLoanSectionOfActType ToMismoLoanSectionOfAct(string type)
        {
            switch (type.ToLower())
            {
                case "203(b)": return E_FhaLoanSectionOfActType._203B;
                case "203(b)2": return E_FhaLoanSectionOfActType._203B2;
                case "203(b)/251": return E_FhaLoanSectionOfActType._203B251;
                case "203(k)": return E_FhaLoanSectionOfActType._203K;
                case "203(k)/251": return E_FhaLoanSectionOfActType._203K251;
                case "234(c)": return E_FhaLoanSectionOfActType._234C;
                case "234(c)/251": return E_FhaLoanSectionOfActType._234C251;
                default: return E_FhaLoanSectionOfActType.Undefined;
            }
        }
        
        //todo THIS IS NULL in Closing231
        private FhaVaLoan CreateFhaVaLoan()
        {
            FhaVaLoan fhaVaLoan = new FhaVaLoan();
            //fhaVaLoan.Id = null;
            //fhaVaLoan.BorrowerPaidFhaVaClosingCostsAmount = null;
            //fhaVaLoan.BorrowerPaidFhaVaClosingCostsPercent = null;
            //fhaVaLoan.GovernmentMortgageCreditCertificateAmount = null;
            //fhaVaLoan.GovernmentRefinanceType = null;
            //fhaVaLoan.GovernmentRefinanceTypeOtherDescription = null;
            //fhaVaLoan.OtherPartyPaidFhaVaClosingCostsAmount = null;
            //fhaVaLoan.OtherPartyPaidFhaVaClosingCostsPercent = null;
            if (m_dataLoan.sVaPropDesignationT == E_sVaPropDesignationT.EnergyImprovement)
            {
                fhaVaLoan.PropertyEnergyEfficientHomeIndicator = E_YNIndicator.Y;
            }

            //fhaVaLoan.SellerPaidFhaVaClosingCostsPercent = null;
            //fhaVaLoan.OriginatorIdentifier = null;
            return fhaVaLoan;
        }

        private Mismo.Closing2_6.VaLoan CreateVaLoan(CAppData dataApp)
        {
            Mismo.Closing2_6.VaLoan vaLoan = new Mismo.Closing2_6.VaLoan();

            // OPM 82549: send ProcedureType on VA or FHA loans
            if (m_dataLoan.sVaIsAutoProc)
            {
                vaLoan.ProcedureType = E_VaLoanProcedureType.Automatic;
            }
            else if (m_dataLoan.sVaIsAutoIrrrlProc)
            {
                vaLoan.ProcedureType = E_VaLoanProcedureType.AutomaticInterestRateReductionRefinanceLoan;
            }
            else if (m_dataLoan.sVaIsPriorApprovalProc)
            {
                vaLoan.ProcedureType = E_VaLoanProcedureType.VAPriorApproval;
            }

            vaLoan.VaEntitlementAmount = dataApp.aVaEntitleAmt_rep;
            vaLoan.VaEntitlementCodeIdentifier = dataApp.aVaEntitleCode;
            vaLoan.VaMaintenanceExpenseMonthlyAmount = m_dataLoan.sVaProMaintenancePmt_rep;
            
            vaLoan.VaResidualIncomeAmount = dataApp.aVaFamilySupportBal_rep;
            vaLoan.VaResidualIncomeGuidelineAmount = dataApp.aVaFamilySuportGuidelineAmt_rep;
            
            vaLoan.VaUtilityExpenseMonthlyAmount = m_dataLoan.sVaProUtilityPmt_rep;
            //vaLoan.Id = null;
            vaLoan.BorrowerFundingFeePercent = m_dataLoanWithGfeArchiveApplied.sFfUfmipR_rep;

            if (dataApp.aBMaritalStatT == E_aBMaritalStatT.Married && dataApp.aCMaritalStatT == E_aCMaritalStatT.Married)
            {
                vaLoan.VaBorrowerCoBorrowerMarriedIndicator = E_YNIndicator.Y;
            }
            
            vaLoan.VaHouseholdSizeCount = m_dataLoan.sSpRoomCount;
            //vaLoan.VaResidualIncomeAmount = null;
            //vaLoan.VaUtilityExpenseMonthlyAmount = null;

            vaLoan.LenderAppraisal = CreateLenderAppraisal();

            vaLoan.MasterCertificateOfReasonableValueIdentifier = m_dataLoan.sVaMcrvNum;

            vaLoan.VaAppraisalType = ToMismo(m_dataLoan.sVaApprT);

            vaLoan.TitleVestingTypeOtherDescription = dataApp.aVaVestTitleODesc;

            return vaLoan;
        }

        private LenderAppraisal CreateLenderAppraisal()
        {
            LenderAppraisal lenderAppraisal = new LenderAppraisal();

            lenderAppraisal.StaffAppraisalReviewerIdentifier = m_dataLoan.sVaLenSarId;
            lenderAppraisal.StaffAppraisalReviewValuationAdjustmentIndicator = ToMismo(m_dataLoan.sVaAppraisalOrSarAdjustmentTri);
            lenderAppraisal.StaffAppraisalReviewValueNotificationDate = m_dataLoan.sVaSarNotifIssuedD_rep;

            return lenderAppraisal;
        }

        private E_VaLoanVaAppraisalType ToMismo(E_sVaApprT sVaApprT)
        {
            switch (sVaApprT)
            {
                case E_sVaApprT.LeaveBlank:
                    return E_VaLoanVaAppraisalType.Undefined;
                case E_sVaApprT.SingleProp:
                    return E_VaLoanVaAppraisalType.SingleProperty;
                case E_sVaApprT.MasterCrvCase:
                    return E_VaLoanVaAppraisalType.MasterCertificateOfReasonableValueCase;
                case E_sVaApprT.LappLenderAppr:
                    return E_VaLoanVaAppraisalType.LenderAppraisal;
                case E_sVaApprT.ManufacturedHome:
                    return E_VaLoanVaAppraisalType.ManufacturedHome;
                case E_sVaApprT.HudVaConversion:
                    return E_VaLoanVaAppraisalType.HUDConversion;
                case E_sVaApprT.PropMgmtCase:
                    return E_VaLoanVaAppraisalType.PropertyManagementCase;
                default:
                    throw new UnhandledEnumException(sVaApprT);
            }
        }

        private GovernmentReporting CreateGovernmentReporting()
        {
            GovernmentReporting governmentReporting = new GovernmentReporting();
            //governmentReporting.Id = null;


            governmentReporting.HmdaHoepaLoanStatusIndicator = ToMismo(m_dataLoan.sHmdaReportAsHoepaLoan);
            governmentReporting.HmdaPreapprovalType = ToMismo(m_dataLoan.sHmdaPreapprovalT);
            switch (m_dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.Purchase:
                    governmentReporting.HmdaPurposeOfLoanType = E_GovernmentReportingHmdaPurposeOfLoanType.HomePurchase;
                    break;
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    if (m_dataLoan.sHmdaReportAsHomeImprov)
                    {
                        governmentReporting.HmdaPurposeOfLoanType = E_GovernmentReportingHmdaPurposeOfLoanType.HomeImprovement;
                    }
                    else
                    {
                        governmentReporting.HmdaPurposeOfLoanType = E_GovernmentReportingHmdaPurposeOfLoanType.Refinancing;
                    }
                    break;
                default:
                    governmentReporting.HmdaPurposeOfLoanType = E_GovernmentReportingHmdaPurposeOfLoanType.Refinancing;
                    break;
            }


            governmentReporting.HmdaRateSpreadPercent = m_dataLoan.sHmdaAprRateSpread;


            return governmentReporting;
        }

        private E_GovernmentReportingHmdaPreapprovalType ToMismo(E_sHmdaPreapprovalT hmdaPreapprovalT)
        {
            switch (hmdaPreapprovalT)
            {
                case E_sHmdaPreapprovalT.LeaveBlank: return E_GovernmentReportingHmdaPreapprovalType.Undefined;
                case E_sHmdaPreapprovalT.NotApplicable: return E_GovernmentReportingHmdaPreapprovalType.NotApplicable;
                case E_sHmdaPreapprovalT.PreapprovalNotRequested: return E_GovernmentReportingHmdaPreapprovalType.PreapprovalWasNotRequested;
                case E_sHmdaPreapprovalT.PreapprovalRequested: return E_GovernmentReportingHmdaPreapprovalType.PreapprovalWasRequested;
                default:
                    LogInvalidEnum("E_sHmdaPreapprovalT", hmdaPreapprovalT);
                    return E_GovernmentReportingHmdaPreapprovalType.Undefined;
            }
        }

        private InterviewerInformation CreateInterviewerInformation()
        {
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            InterviewerInformation interviewerInformation = new InterviewerInformation();
            //interviewerInformation.Id = null;
            interviewerInformation.ApplicationTakenMethodType = ToMismo(m_dataLoan.GetAppData(0).aIntrvwrMethodT);
            interviewerInformation.InterviewerApplicationSignedDate = m_dataLoan.GetAppData(0).a1003InterviewD_rep;
            interviewerInformation.InterviewersEmployerCity = preparer.City;
            interviewerInformation.InterviewersEmployerName = preparer.CompanyName;
            interviewerInformation.InterviewersEmployerPostalCode = preparer.Zip;
            interviewerInformation.InterviewersEmployerState = preparer.State;
            interviewerInformation.InterviewersEmployerStreetAddress = preparer.StreetAddr;
            //interviewerInformation.InterviewersEmployerStreetAddress2 = null;
            interviewerInformation.InterviewersName = preparer.PreparerName;
            interviewerInformation.InterviewersTelephoneNumber = preparer.Phone;
            return interviewerInformation;
        }

        private string GenerateLiabilityId(string basicId, int appNumber)
        {
            return basicId + appNumber;
        }

        private Liability CreateLiability(ILiabilityAlimony alimony, string borrowerId, int appNumber)
        {
            if (null == alimony)
                return null;

            Liability liability = new Liability();
            liability.Id = GenerateLiabilityId("LIA_" + alimony.RecordId.ToString("N"), appNumber);
            liability.BorrowerId = borrowerId;
            liability.Type = E_LiabilityType.Alimony;
            liability.AlimonyOwedToName = alimony.OwedTo;
            liability.RemainingTermMonths = alimony.RemainMons_rep;
            liability.MonthlyPaymentAmount = alimony.Pmt_rep;
            return liability;
        }
        private Liability CreateLiability(ILiabilityChildSupport childSupport, string borrowerId, int appNumber)
        {
            if (null == childSupport)
                return null;

            Liability liability = new Liability();
            liability.Id = GenerateLiabilityId("LIA_" + childSupport.RecordId.ToString("N"), appNumber);
            liability.BorrowerId = borrowerId;
            liability.Type = E_LiabilityType.ChildSupport;
            liability.RemainingTermMonths = childSupport.RemainMons_rep;
            liability.MonthlyPaymentAmount = childSupport.Pmt_rep;
            return liability;
        }
        private Liability CreateLiability(ILiabilityJobExpense jobExpense, string borrowerId, int appNumber)
        {
            if (null == jobExpense)
                return null;

            Liability liability = new Liability();
            liability.Id = GenerateLiabilityId("LIA_" + jobExpense.RecordId.ToString("N"), appNumber);
            liability.BorrowerId = borrowerId;
            liability.Type = E_LiabilityType.JobRelatedExpenses;
            liability.HolderName = jobExpense.ExpenseDesc;
            liability.MonthlyPaymentAmount = jobExpense.Pmt_rep;
            return liability;
        }
        private Liability CreateLiability(ILiabilityRegular field, CAppData dataApp, int appNumber)
        {
            if (null == field)
                return null;

            Liability liability = new Liability();
            liability.Id = GenerateLiabilityId("LIA_" + field.RecordId.ToString("N"), appNumber);
            liability.BorrowerId = field.OwnerT == E_LiaOwnerT.CoBorrower ? dataApp.aCMismoId : dataApp.aBMismoId;
            if (field.DebtT == E_DebtRegularT.Mortgage && field.MatchedReRecordId != Guid.Empty)
            {
                liability.ReoId = "REO_" + field.MatchedReRecordId.ToString("N");
            }
            liability.HolderStreetAddress = field.ComAddr;
            liability.HolderCity = field.ComCity;
            liability.HolderState = field.ComState;
            liability.HolderPostalCode = field.ComZip;
            liability.AccountIdentifier = field.AccNum.Value;
            liability.HolderName = field.ComNm;
            liability.MonthlyPaymentAmount = field.Pmt_rep;
            liability.PayoffStatusIndicator = ToMismo(field.WillBePdOff);
            liability.RemainingTermMonths = field.RemainMons_rep;
            liability.Type = ToMismo(field.DebtT);
            liability.UnpaidBalanceAmount = field.Bal_rep;

            if (field.DebtT == E_DebtRegularT.Mortgage)
            {
                // 5/7/2008 dd - LendingQB always exclude mortgage liability to be include in DTI calculation. However
                // when we export to LP, we have to turn off ExclusionIndicator if liability is not paid off.
                liability.ExclusionIndicator = ToMismo(field.WillBePdOff);
            }
            else
            {
                liability.ExclusionIndicator = ToMismo(field.NotUsedInRatio);
            }


            return liability;
        }

        private E_LiabilityType ToMismo(E_DebtRegularT e_DebtRegularT)
        {
            switch (e_DebtRegularT)
            {
                case E_DebtRegularT.Installment: return E_LiabilityType.Installment;
                case E_DebtRegularT.Mortgage: return E_LiabilityType.MortgageLoan;
                case E_DebtRegularT.Open: return E_LiabilityType.Open30DayChargeAccount;
                case E_DebtRegularT.Other: return E_LiabilityType.OtherLiability;
                case E_DebtRegularT.Revolving: return E_LiabilityType.Revolving;
                default:
                    LogInvalidEnum("E_DebtRegularT", e_DebtRegularT);
                    return E_LiabilityType.Undefined;
            }
        }
        private void LogInvalidEnum(string type, Enum value)
        {
            Tools.LogBug("Unhandle enum value='" + value + "' for " + type);
        }
        private static E_YNIndicator ToMismo(bool p)
        {
            return p ? E_YNIndicator.Y : E_YNIndicator.N;
        }

        private Liability CreateLiability()
        {
            Liability liability = new Liability();
            //liability.Id = null;
            //liability.ReoId = null;
            //liability.BorrowerId = null;
            //liability.AlimonyOwedToName = null;
            //liability.LiabilityDescription = null;
            //liability.SubjectLoanResubordinationIndicator = null;
            //liability.AccountIdentifier = null;
            //liability.ExclusionIndicator = null;
            //liability.HolderCity = null;
            //liability.HolderName = null;
            //liability.HolderPostalCode = null;
            //liability.HolderState = null;
            //liability.HolderStreetAddress = null;
            //liability.HolderStreetAddress2 = null;
            //liability.MonthlyPaymentAmount = null;
            //liability.PayoffStatusIndicator = null;
            //liability.PayoffWithCurrentAssetsIndicator = null;
            //liability.RemainingTermMonths = null;
            //liability.Type = null;
            //liability.TypeOtherDescription = null;
            //liability.UnpaidBalanceAmount = null;
            return liability;
        }

        private LoanProductData CreateLoanProductData()
        {
            LoanProductData loanProductData = new LoanProductData();
            loanProductData.Arm = CreateArm();
            loanProductData.LoanFeatures = CreateLoanFeatures();
            loanProductData.PaymentAdjustmentList.Add(CreatePaymentAdjustment());
            loanProductData.RateAdjustmentList.Add(CreateRateAdjustment());
            loanProductData.InterestOnly = CreateInterestOnly();

            //loanProductData.Id = null;
            loanProductData.BuydownList.Add(CreateBuydown());

            if (m_dataLoan.BrokerDB.IsEnableHELOC && m_dataLoan.sIsLineOfCredit)
            {
                // 10/25/2013 dd - OPM 142536 (added to DM 02/03/2014 dt OPM 144462)
                loanProductData.Heloc = CreateHeloc();
            }

            loanProductData.PrepaymentPenaltyList.Add(CreatePrepaymentPenalty(m_dataLoan.sPpmtPenaltyMon_rep, null, m_dataLoan.sGfeMaxPpmtPenaltyAmt_rep));
            if (m_dataLoan.sPrepmtPenaltyT == E_sPrepmtPenaltyT.May)
            {
                loanProductData.PrepaymentPenaltyList.Add(CreatePrepaymentPenalty(m_dataLoan.sPrepmtPeriodMonths_rep, "Hard", null));
                loanProductData.PrepaymentPenaltyList.Add(CreatePrepaymentPenalty(m_dataLoan.sSoftPrepmtPeriodMonths_rep, "Soft", null));
            }

            //loanProductData.InterestCalculationRuleList.Add(CreateInterestCalculationRule());
            return loanProductData;
        }

        private Arm CreateArm()
        {
            Arm arm = new Arm();
            arm.RateAdjustmentLifetimeCapPercent = m_dataLoan.sRAdjLifeCapR_rep;
            arm.IndexMarginPercent = m_dataLoan.sRAdjMarginR_rep;
            arm.LifetimeFloorPercent = m_dataLoan.sRAdjFloorR_rep;
            arm.QualifyingRatePercent = m_dataLoan.sQualIR_rep;
            arm.LifetimeCapRate = m_dataLoan.sRLifeCapR_rep;
            arm.IndexCurrentValuePercent = m_dataLoan.sRAdjIndexR_rep;



            //arm.Id = null;
            //arm.FnmTreasuryYieldForCurrentIndexDivisorNumber = null;
            //arm.FnmTreasuryYieldForIndexDivisorNumber = null;
            //arm.PaymentAdjustmentLifetimeCapAmount = null;
            //arm.PaymentAdjustmentLifetimeCapPercent = null;
            //arm.ConversionOptionIndicator = null;
            //arm.IndexType = null;
            //arm.IndexTypeOtherDescription = null;
            //arm.InterestRateRoundingFactor = null;
            //arm.InterestRateRoundingType = null;
            //arm.ConversionOption = CreateConversionOption();
            return arm;
        }

        private ConversionOption CreateConversionOption()
        {
            ConversionOption conversionOption = new ConversionOption();
            //conversionOption.Id = null;
            //conversionOption.ConversionOptionPeriodFeePercent = null;
            //conversionOption.EndingChangeDatePeriodDescription = null;
            //conversionOption.FeeAmount = null;
            //conversionOption.NoteTermGreaterThanFifteenYearsAdditionalPercent = null;
            //conversionOption.NoteTermLessThanFifteenYearsAdditionalPercent = null;
            //conversionOption.PeriodEndDate = null;
            //conversionOption.PeriodStartDate = null;
            //conversionOption.StartingChangeDatePeriodDescription = null;
            return conversionOption;
        }

        private Buydown CreateBuydown()
        {
            Buydown buydown = new Buydown();

            var buydownRows = new[]
            {
                //    R = Rate Reduction, Mon = Term (mths)
                new { R = m_dataLoan.sBuydwnR1, R_rep = m_dataLoan.sBuydwnR1_rep, Mon = m_dataLoan.sBuydwnMon1, Mon_rep = m_dataLoan.sBuydwnMon1_rep, Num = "1"},
                new { R = m_dataLoan.sBuydwnR2, R_rep = m_dataLoan.sBuydwnR2_rep, Mon = m_dataLoan.sBuydwnMon2, Mon_rep = m_dataLoan.sBuydwnMon2_rep, Num = "2"},
                new { R = m_dataLoan.sBuydwnR3, R_rep = m_dataLoan.sBuydwnR3_rep, Mon = m_dataLoan.sBuydwnMon3, Mon_rep = m_dataLoan.sBuydwnMon3_rep, Num = "3"},
                new { R = m_dataLoan.sBuydwnR4, R_rep = m_dataLoan.sBuydwnR4_rep, Mon = m_dataLoan.sBuydwnMon4, Mon_rep = m_dataLoan.sBuydwnMon4_rep, Num = "4"},
                new { R = m_dataLoan.sBuydwnR5, R_rep = m_dataLoan.sBuydwnR5_rep, Mon = m_dataLoan.sBuydwnMon5, Mon_rep = m_dataLoan.sBuydwnMon5_rep, Num = "5"},
            };

            // Get nonempty rows. A row is nonzero if both fields have a nonzero value.
            var buydownRowsFiltered = buydownRows.Where((row) => row.R != 0 && row.Mon != 0);
            
            bool isIncreaseRatePercentConstant = true;
            decimal increaseRatePercent = 0.0M;
            bool isChangeFrequencyMonthsConstant = true;
            int changeFrequencyMonths = 0;
            int durationMonths = 0;

            if (buydownRowsFiltered.Count() == 0)
            {
                return null;
            }
            else
            {
                increaseRatePercent = buydownRowsFiltered.First().R;
                changeFrequencyMonths = buydownRowsFiltered.First().Mon;
            }

            foreach (var row in buydownRowsFiltered)
            {
                buydown.SubsidyScheduleList.Add(CreateSubsidySchedule(row.R_rep, row.Num, row.Mon_rep));

                durationMonths += row.Mon;

                // Detect increase rate percent: iff the value in sBuydwnR1 through sBuydwnR5 drops at the same rate
                if (row.R != increaseRatePercent)
                    isIncreaseRatePercentConstant = false;

                // Detect change frequency months: iff the value in sBuydwnMon1 through sBuydwnMon5 remains constant
                if (row.Mon != changeFrequencyMonths)
                    isChangeFrequencyMonthsConstant = false;
            }
            
            //buydown.Id = null;
            buydown.BaseDateType = E_BuydownBaseDateType.FirstPaymentDate;
            //buydown.BaseDateTypeOtherDescription = null;
            if (isChangeFrequencyMonthsConstant)
                buydown.ChangeFrequencyMonths = m_dataLoan.m_convertLos.ToCountString(changeFrequencyMonths);
            //buydown.ContributorType = null;
            //buydown.ContributorTypeOtherDescription = null;
            buydown.DurationMonths = m_dataLoan.m_convertLos.ToCountString(durationMonths);
            if (isIncreaseRatePercentConstant)
                buydown.IncreaseRatePercent = m_dataLoan.m_convertLos.ToRateString(increaseRatePercent);
            //buydown.LenderFundingIndicator = null;
            //buydown.OriginalBalanceAmount = null;
            //buydown.PermanentIndicator = null;
            //buydown.SubsidyCalculationType = null;
            //buydown.TotalSubsidyAmount = null;
            //buydown.ContributorList.Add(CreateContributor());
            
            return buydown;
        }

        private Contributor CreateContributor()
        {
            Contributor contributor = new Contributor();
            //contributor.Id = null;
            //contributor.Amount = null;
            //contributor.Percent = null;
            //contributor.RoleType = null;
            //contributor.RoleTypeOtherDescription = null;
            //contributor.UnparsedName = null;
            return contributor;
        }

        private SubsidySchedule CreateSubsidySchedule(string adjustmentPercent, string periodIdentifier, string periodicTerm)
        {
            SubsidySchedule subsidySchedule = new SubsidySchedule();
            //subsidySchedule.Id = null;
            subsidySchedule.AdjustmentPercent = adjustmentPercent;
            subsidySchedule.PeriodIdentifier = periodIdentifier;
            //subsidySchedule.PeriodicPaymentEffectiveDate = null;
            //subsidySchedule.PeriodicPaymentSubsidyAmount = null;
            subsidySchedule.PeriodicTerm = periodicTerm;
            return subsidySchedule;
        }

        private LoanFeatures CreateLoanFeatures()
        {
            LoanFeatures loanFeatures = new LoanFeatures();
            //loanFeatures.Id = null;

            loanFeatures.BalloonIndicator = ToMismo(m_dataLoan.sBalloonPmt);
            loanFeatures.BalloonLoanMaturityTermMonths = m_dataLoan.sDue_rep;
            loanFeatures.DemandFeatureIndicator = ToMismo(m_dataLoan.sHasDemandFeature);
            loanFeatures.EscrowWaiverIndicator = ToMismo(m_dataLoan.sWillEscrowBeWaived);
            loanFeatures.EstimatedPrepaidDays = m_dataLoan.sIPiaDy_rep;
            loanFeatures.FnmProjectClassificationType = ToMismo(m_dataLoan.sSpProjectClassFannieT);
            loanFeatures.GsePropertyType = ToMismo(m_dataLoan.sGseSpT);
            if (m_setConformingYear)
            {
                loanFeatures.RespaConformingYearType = E_LoanFeaturesRespaConformingYearType.January2010;
            }

            loanFeatures.InterestOnlyTerm = m_dataLoan.sIOnlyMon_rep;
            loanFeatures.LienPriorityType = ToMismo(m_dataLoan.sLienPosT);
            loanFeatures.LoanOriginalMaturityTermMonths = m_dataLoan.sDue_rep;
            loanFeatures.LoanScheduledClosingDate = m_dataLoan.sDocMagicClosingD_rep;
            loanFeatures.NegativeAmortizationLimitPercent = m_dataLoan.sPmtAdjMaxBalPc_rep;
            loanFeatures.OriginalBalloonTermMonths = m_dataLoan.sTerm_rep;
            loanFeatures.OriginalPrincipalAndInterestPaymentAmount = m_dataLoan.sProThisMPmt_rep;
            loanFeatures.PaymentFrequencyType = E_LoanFeaturesPaymentFrequencyType.Monthly;
            loanFeatures.ProductName = m_dataLoan.sLpTemplateNm;
            loanFeatures.ProductIdentifier = m_dataLoan.sLoanProductIdentifier;

            loanFeatures.RequiredDepositIndicator = ToMismo(m_dataLoan.sAprIncludesReqDeposit);
            loanFeatures.ScheduledFirstPaymentDate = m_dataLoan.sSchedDueD1_rep;


            switch (m_dataLoan.sAssumeLT)
            {
                case E_sAssumeLT.May:
                    loanFeatures.AssumabilityIndicator = E_YNIndicator.Y;
                    loanFeatures.ConditionsToAssumabilityIndicator = E_YNIndicator.N;
                    break;
                case E_sAssumeLT.MayNot:
                    loanFeatures.AssumabilityIndicator = E_YNIndicator.N;
                    break;
                case E_sAssumeLT.MaySubjectToCondition:
                    loanFeatures.AssumabilityIndicator = E_YNIndicator.Y;
                    loanFeatures.ConditionsToAssumabilityIndicator = E_YNIndicator.Y;
                    break;
            }

            switch (m_dataLoan.sPrepmtPenaltyT)
            {
                case E_sPrepmtPenaltyT.May:
                    loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.Y;
                    break;
                case E_sPrepmtPenaltyT.WillNot:
                    loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.N;
                    break;
                default:
                    loanFeatures.PrepaymentPenaltyIndicator = E_YNIndicator.Undefined;
                    break;
            }
            switch (m_dataLoan.sPrepmtRefundT)
            {
                case E_sPrepmtRefundT.May:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YNIndicator.Y;
                    break;
                case E_sPrepmtRefundT.WillNot:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YNIndicator.N;
                    break;
                default:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YNIndicator.Undefined;
                    break;

            }
            //loanFeatures.BuydownTemporarySubsidyIndicator = null;

            //loanFeatures.ConformingIndicator = null;
            //loanFeatures.CounselingConfirmationIndicator = null;
            //loanFeatures.CounselingConfirmationType = null;
            //loanFeatures.DownPaymentOptionType = null;
            //loanFeatures.DownPaymentOptionTypeOtherDescription = null;
            //loanFeatures.EstimatedPrepaidDaysPaidByOtherTypeDescription = null;
            E_LoanFeaturesEstimatedPrepaidDaysPaidByType paidBy;
            switch (LosConvert.GfeItemProps_Payer(m_dataLoan.sIPiaProps))
            {
                case LosConvert.BORR_PAID_OUTOFPOCKET:
                case LosConvert.BORR_PAID_FINANCED:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Buyer;
                    break;
                case LosConvert.SELLER_PAID:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Seller;
                    break;
                case LosConvert.LENDER_PAID:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Lender;
                    break;
                case LosConvert.BROKER_PAID:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Other;
                    loanFeatures.EstimatedPrepaidDaysPaidByOtherTypeDescription = "Broker";
                    break;
                default:
                    paidBy = E_LoanFeaturesEstimatedPrepaidDaysPaidByType.Undefined;
                    break;
            }
            loanFeatures.EstimatedPrepaidDaysPaidByType = paidBy;

            loanFeatures.LoanDocumentationType = ConvertsProdDocTypeToMismo(m_dataLoan.sProdDocT); // opm 84022
                
            //loanFeatures.FnmProductPlanIdentifier = null;
            //loanFeatures.FnmProductPlanIndentifier = null;
            //loanFeatures.FnmProjectClassificationType = null;
            //loanFeatures.FnmProjectClassificationTypeOtherDescription = null;
            //loanFeatures.FreOfferingIdentifier = null;
            //loanFeatures.FreProjectClassificationType = null;
            //loanFeatures.FreProjectClassificationTypeOtherDescription = null;
            //loanFeatures.FullPrepaymentPenaltyOptionType = null;
            //loanFeatures.GraduatedPaymentMultiplierFactor = null;
            //loanFeatures.GrowingEquityLoanPayoffYearsCount = null;
            if (m_dataLoan.BrokerDB.IsEnableHELOC && m_dataLoan.sIsLineOfCredit)
            {
                // 10/25/2013 dd - OPM 142536 (added to DM 02/03/2014 dt OPM 144462)
                loanFeatures.HelocInitialAdvanceAmount = m_dataLoan.sLAmtCalc_rep;
                loanFeatures.HelocMaximumBalanceAmount = m_dataLoan.sCreditLineAmt_rep;
            }
            //loanFeatures.InitialPaymentRatePercent = null;
            //loanFeatures.InitialPaymentDiscountPercent = null;
            //loanFeatures.LenderSelfInsuredIndicator = null;
            //loanFeatures.LienPriorityTypeOtherDescription = null;
            //loanFeatures.LoanClosingStatusType = null;
            //loanFeatures.LoanDocumentationType = null;
            //loanFeatures.LoanDocumentationTypeOtherDescription = null;
            //loanFeatures.LoanMaturityDate = null;
            //loanFeatures.LoanRepaymentType = null;
            //loanFeatures.LoanRepaymentTypeOtherDescription = null;
            //loanFeatures.MiCertificationStatusType = null;
            //loanFeatures.MiCertificationStatusTypeOtherDescription = null;
            //loanFeatures.MiCompanyNameType = null;
            //loanFeatures.MiCompanyNameTypeOtherDescription = null;
            //loanFeatures.MiCoveragePercent = null;
            //loanFeatures.NameDocumentsDrawnInType = null;
            //loanFeatures.NameDocumentsDrawnInTypeOtherDescription = null;
            //loanFeatures.NegativeAmortizationLimitMonthsCount = null;
            //loanFeatures.NegativeAmortizationType = null;
            //loanFeatures.PaymentFrequencyTypeOtherDescription = null;


            //loanFeatures.PrepaymentPenaltyTermMonths = null;
            //loanFeatures.PrepaymentRestrictionIndicator = null;
            //loanFeatures.ProductDescription = null;
            //loanFeatures.ProductIdentifier = null;
            //loanFeatures.RefundableApplicationFeeIndicator = null;
            //loanFeatures.ServicingTransferStatusType = null;
            //loanFeatures.TimelyPaymentRateReductionIndicator = null;
            //loanFeatures.TimelyPaymentRateReductionPercent = null;
            //loanFeatures.LateCharge = CreateLateCharge();
            loanFeatures.NotePayTo = CreateNotePayTo();


            return loanFeatures;
        }

        private static E_LoanFeaturesLoanDocumentationType ConvertsProdDocTypeToMismo(E_sProdDocT prodDocType) // opm 84022
        {
            switch(prodDocType)
            {
                case E_sProdDocT.Full:
                    return E_LoanFeaturesLoanDocumentationType.FullDocumentation;
                case E_sProdDocT.Alt:
                    return E_LoanFeaturesLoanDocumentationType.Other;
                case E_sProdDocT.Light:
                    return E_LoanFeaturesLoanDocumentationType.Other;
                case E_sProdDocT.NINA:
                    return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                case E_sProdDocT.NISA:
                    return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedAssets;
                case E_sProdDocT.NINANE:
                    return E_LoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification;
                case E_sProdDocT.NIVA:
                    return E_LoanFeaturesLoanDocumentationType.NoIncomeOn1003;
                case E_sProdDocT.SISA:
                    return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssests;
                case E_sProdDocT.SIVA:
                    return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncome;
                case E_sProdDocT.VISA:
                    return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedAssets;
                case E_sProdDocT.NIVANE:
                    return E_LoanFeaturesLoanDocumentationType.NoIncomeOn1003;
                case E_sProdDocT.VINA:
                    return E_LoanFeaturesLoanDocumentationType.Other;
                case E_sProdDocT.Streamline:
                    return E_LoanFeaturesLoanDocumentationType.StreamlineRefinance;
                case E_sProdDocT._12MoPersonalBankStatements:
                case E_sProdDocT._24MoPersonalBankStatements:
                case E_sProdDocT._12MoBusinessBankStatements:
                case E_sProdDocT._24MoBusinessBankStatements:
                case E_sProdDocT.OtherBankStatements:
                case E_sProdDocT._1YrTaxReturns:
                case E_sProdDocT.Voe:
                    return E_LoanFeaturesLoanDocumentationType.Alternative;
                case E_sProdDocT.AssetUtilization:
                case E_sProdDocT.DebtServiceCoverage:
                    return E_LoanFeaturesLoanDocumentationType.NoRatio;
                case E_sProdDocT.NoIncome:
                    return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                default:
                    Tools.LogErrorWithCriticalTracking(new UnhandledEnumException(prodDocType)); // enum won't change much but if it does...
                    return E_LoanFeaturesLoanDocumentationType.Other;
            }
        }

        private E_LoanFeaturesFnmProjectClassificationType ToMismo(E_sSpProjectClassFannieT sSpProjectClassFannieT)
        {
            switch (sSpProjectClassFannieT)
            {
                case E_sSpProjectClassFannieT.LeaveBlank:
                case E_sSpProjectClassFannieT.GNotInProject:
                    return E_LoanFeaturesFnmProjectClassificationType.Undefined;
                case E_sSpProjectClassFannieT.PLimitedReviewNew:
                    return E_LoanFeaturesFnmProjectClassificationType.PCondominium;
                case E_sSpProjectClassFannieT.QLimitedReviewEst:
                    return E_LoanFeaturesFnmProjectClassificationType.QCondominium;
                case E_sSpProjectClassFannieT.RExpeditedReviewNew:
                    return E_LoanFeaturesFnmProjectClassificationType.RCondominium;
                case E_sSpProjectClassFannieT.SExpeditedReviewEst:
                    return E_LoanFeaturesFnmProjectClassificationType.SCondominium;
                case E_sSpProjectClassFannieT.TFannieReview:
                    return E_LoanFeaturesFnmProjectClassificationType.TCondominium;
                case E_sSpProjectClassFannieT.UFhaApproved:
                    return E_LoanFeaturesFnmProjectClassificationType.UCondominium;
                case E_sSpProjectClassFannieT.VRefiPlus:
                    return E_LoanFeaturesFnmProjectClassificationType.Undefined;
                case E_sSpProjectClassFannieT.EPud:
                    return E_LoanFeaturesFnmProjectClassificationType.E_PUD;
                case E_sSpProjectClassFannieT.FPud:
                    return E_LoanFeaturesFnmProjectClassificationType.F_PUD;
                case E_sSpProjectClassFannieT.TPud:
                    return E_LoanFeaturesFnmProjectClassificationType.TCondominium;
                case E_sSpProjectClassFannieT._1Coop:
                    return E_LoanFeaturesFnmProjectClassificationType.OneCooperative;
                case E_sSpProjectClassFannieT._2Coop:
                    return E_LoanFeaturesFnmProjectClassificationType.TwoCooperative;
                case E_sSpProjectClassFannieT.TCoop:
                    return E_LoanFeaturesFnmProjectClassificationType.Undefined;
                default:
                    throw new UnhandledEnumException(sSpProjectClassFannieT);
            }

        }


        private LateCharge CreateLateCharge()
        {
            LateCharge lateCharge = new LateCharge();
            //lateCharge.Id = null;
            //lateCharge.Amount = null;
            //lateCharge.GracePeriod = null;
            //lateCharge.LoanPaymentAmount = null;
            //lateCharge.MaximumAmount = null;
            //lateCharge.MinimumAmount = null;
            //lateCharge.Rate = null;
            //lateCharge.Type = null;
            return lateCharge;
        }

        private NotePayTo CreateNotePayTo()
        {
            DocMagicAlternateLender altLender = new DocMagicAlternateLender(m_dataLoan.sBrokerId, m_dataLoan.sDocMagicAlternateLenderId);
            NotePayTo notePayTo = new NotePayTo();
            //notePayTo.Id = null;
            notePayTo.City = altLender.MakePaymentsToCity;
            //notePayTo.Country = null;
            notePayTo.PostalCode = altLender.MakePaymentsToZip;
            notePayTo.State = altLender.MakePaymentsToState;
            notePayTo.StreetAddress = altLender.MakePaymentsToAddress;
            //notePayTo.StreetAddress2 = null;
            notePayTo.UnparsedName = altLender.MakePaymentsToName;
            return notePayTo;
        }

        private PaymentAdjustment CreatePaymentAdjustment()
        {
            PaymentAdjustment paymentAdjustment = new PaymentAdjustment();
            paymentAdjustment.PeriodNumber = m_dataLoan.sPmtAdjCapMon_rep;
            paymentAdjustment.PeriodicCapPercent = m_dataLoan.sPmtAdjCapR_rep;

            //paymentAdjustment.Id = null;
            paymentAdjustment.FirstPaymentAdjustmentDate = m_dataLoan.sRAdj1stD_rep;
            //paymentAdjustment.FirstPaymentAdjustmentMonths = null;
            //paymentAdjustment.LastPaymentAdjustmentDate = null;
            //paymentAdjustment.SubsequentPaymentAdjustmentMonths = null;
            //paymentAdjustment.Amount = null;
            //paymentAdjustment.CalculationType = null;
            //paymentAdjustment.CalculationTypeOtherDescription = null;
            //paymentAdjustment.DurationMonths = null;
            //paymentAdjustment.Percent = null;
            //paymentAdjustment.PeriodicCapAmount = null;
            return paymentAdjustment;
        }

        private RateAdjustment CreateRateAdjustment()
        {
            RateAdjustment rateAdjustment = new RateAdjustment();
            try
            {
                rateAdjustment.FirstRateAdjustmentMonths = m_dataLoan.sRAdj1stCapMon_rep;
                rateAdjustment.SubsequentRateAdjustmentMonths = m_dataLoan.sRAdjCapMon_rep;
                rateAdjustment.FirstChangeCapRate = m_dataLoan.sR1stCapR_rep;
                rateAdjustment.InitialCapPercent = m_dataLoan.sRAdj1stCapR_rep;
                rateAdjustment.SubsequentCapPercent = m_dataLoan.sRAdjCapR_rep;

                //rateAdjustment.Id = null;
                rateAdjustment.FirstRateAdjustmentDate = m_dataLoan.sRAdj1stInterestChangeD_rep;
                //rateAdjustment.CalculationType = null;
                //rateAdjustment.CalculationTypeOtherDescription = null;
                //rateAdjustment.DurationMonths = null;
                //rateAdjustment.FirstChangeFloorPercent = null;
                //rateAdjustment.FirstChangeFloorRate = null;
                //rateAdjustment.Percent = null;
                //rateAdjustment.PeriodNumber = null;
            }
            catch (CBaseException e)
            {
                //OPM 128967: sRAdj1stInterestChangeD_rep will occasionally throw 
                //if 1st Payment Date is not filled out, but we don't want that to kill the export.
                Tools.LogWarning("An exception was thrown while creating a rate adjustment for the DocMagicMismoExporter", e);
            }
            return rateAdjustment;
        }

        private Heloc CreateHeloc()
        {
            Heloc heloc = new Heloc();
            //heloc.Id = null;
            heloc.AnnualFeeAmount = m_dataLoan.sHelocAnnualFee_rep;
            //heloc.CreditCardAccountIdentifier = null;
            //heloc.CreditCardIndicator = null;
            if (m_dataLoan.sDaysInYr_rep == "360")
            {
                heloc.DailyPeriodicInterestRateCalculationType = E_HelocDailyPeriodicInterestRateCalculationType._360;
            }
            else if (m_dataLoan.sDaysInYr_rep == "365")
            {
                heloc.DailyPeriodicInterestRateCalculationType = E_HelocDailyPeriodicInterestRateCalculationType._365;

            }
            heloc.DailyPeriodicInterestRatePercent = m_dataLoan.sDailyPeriodicR_rep;
            heloc.DrawPeriodMonthsCount = m_dataLoan.sHelocDraw_rep;
            heloc.FirstLienBookNumber = m_dataLoan.sFirstLienRecordingBookNum;
            //heloc.FirstLienDate = null;
            heloc.FirstLienHolderName = m_dataLoan.sFirstLienHolderNm;
            heloc.FirstLienIndicator = m_dataLoan.sLienPosT == E_sLienPosT.First ? E_YNIndicator.Y : E_YNIndicator.N;
            heloc.FirstLienInstrumentNumber = m_dataLoan.sFirstLienRecordingInstrumentNum;
            heloc.FirstLienPageNumber = m_dataLoan.sFirstLienRecordingPageNum;
            heloc.FirstLienPrincipalBalanceAmount = m_dataLoan.sRemain1stMBal_rep;
            heloc.FirstLienRecordedDate = m_dataLoan.sFirstLienRecordingD_rep;
            heloc.InitialAdvanceAmount = m_dataLoan.sLAmtCalc_rep;
            heloc.MaximumAprRate = m_dataLoan.sRLifeCapR_rep;
            heloc.MinimumAdvanceAmount = m_dataLoan.sHelocMinimumAdvanceAmt_rep;
            heloc.MinimumPaymentAmount = m_dataLoan.sHelocMinimumPayment_rep;
            heloc.MinimumPaymentPercent = m_dataLoan.sHelocMinimumPaymentPc_rep;
            heloc.RepayPeriodMonthsCount = m_dataLoan.sHelocRepay_rep;
            heloc.ReturnedCheckChargeAmount = m_dataLoan.sHelocReturnedCheckFee_rep;
            heloc.StopPaymentChargeAmount = m_dataLoan.sHelocStopPaymentFee_rep;
            //heloc.TeaserTermEndDate = null;
            heloc.TeaserTermMonthsCount = m_dataLoan.sRAdj1stCapMon_rep;
            heloc.TerminationFeeAmount = m_dataLoan.sHelocTerminationFee_rep;
            heloc.TerminationPeriodMonthsCount = m_dataLoan.sHelocTerminationFeePeriod_rep;
            return heloc;
        }

        private InterestOnly CreateInterestOnly()
        {
            InterestOnly interestOnly = new InterestOnly();
            //interestOnly.Id = null;
            interestOnly.MonthlyPaymentAmount = m_dataLoan.sProThisMPmt_rep;
            interestOnly.TermMonthsCount = m_dataLoan.sIOnlyMon_rep;
            return interestOnly;
        }

        private PrepaymentPenalty CreatePrepaymentPenalty(string termMonths, string textDescription, string gfePrepaymentPenaltyMaximumAmount)
        {
            PrepaymentPenalty prepaymentPenalty = new PrepaymentPenalty();

            //prepaymentPenalty.Id = null;
            //prepaymentPenalty.PenaltyFixedAmount = null;
            //prepaymentPenalty.Percent = null;
            //prepaymentPenalty.PeriodSequenceIdentifier = null;
            prepaymentPenalty.TermMonths = termMonths;
            prepaymentPenalty.GfePrepaymentPenaltyMaximumAmount = gfePrepaymentPenaltyMaximumAmount;
            prepaymentPenalty.TextDescription = textDescription;

            return prepaymentPenalty;
        }

        private InterestCalculationRule CreateInterestCalculationRule()
        {
            InterestCalculationRule interestCalculationRule = new InterestCalculationRule();
            //interestCalculationRule.Id = null;
            //interestCalculationRule.InterestCalculationBasisDaysInPeriodType = null;
            //interestCalculationRule.InterestCalculationBasisDaysInPeriodTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationBasisDaysInYearCount = null;
            //interestCalculationRule.InterestCalculationBasisType = null;
            //interestCalculationRule.InterestCalculationBasisTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationEffectiveDate = null;
            //interestCalculationRule.InterestCalculationEffectiveMonthsCount = null;
            //interestCalculationRule.InterestCalculationExpirationDate = null;
            //interestCalculationRule.InterestCalculationPeriodAdjustmentIndicator = null;
            //interestCalculationRule.InterestCalculationPeriodType = null;
            //interestCalculationRule.InterestCalculationPurposeType = null;
            //interestCalculationRule.InterestCalculationPurposeTypeOtherDescription = null;
            //interestCalculationRule.InterestCalculationType = null;
            //interestCalculationRule.InterestCalculationTypeOtherDescription = null;
            //interestCalculationRule.InterestInAdvanceIndicator = null;
            //interestCalculationRule.LoanInterestAccrualStartDate = null;
            return interestCalculationRule;
        }

        private LoanPurpose CreateLoanPurpose(CAppData dataApp)
        {
            LoanPurpose loanPurpose = new LoanPurpose();
            //loanPurpose.Id = null;

            //loanPurpose.OtherLoanPurposeDescription = null;

            //loanPurpose.PropertyRightsTypeOtherDescription = null;

            //loanPurpose.PropertyUsageTypeOtherDescription = null;

            loanPurpose.GseTitleMannerHeldDescription = dataApp.aManner;
            loanPurpose.OtherLoanPurposeDescription = m_dataLoan.sOLPurposeDesc;
            loanPurpose.PropertyLeaseholdExpirationDate = m_dataLoan.sLeaseHoldExpireD_rep;
            loanPurpose.PropertyRightsType = ToMismo(m_dataLoan.sEstateHeldT);
            loanPurpose.PropertyUsageType = ToMismo(dataApp.aOccT);
            loanPurpose.Type = ToMismo(m_dataLoan.sLPurposeT);

            loanPurpose.ConstructionRefinanceData = CreateConstructionRefinanceData();

            return loanPurpose;
        }

        private E_LoanPurposeType ToMismo(E_sLPurposeT sLPurposeT)
        {
            switch (sLPurposeT)
            {
                case E_sLPurposeT.Construct: return E_LoanPurposeType.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm: return E_LoanPurposeType.ConstructionToPermanent;
                case E_sLPurposeT.Other: return E_LoanPurposeType.Other;
                case E_sLPurposeT.Purchase: return E_LoanPurposeType.Purchase;
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    return E_LoanPurposeType.Refinance;
                default:
                    LogInvalidEnum("sLPurposeT", sLPurposeT);
                    return E_LoanPurposeType.Undefined;

            }
        }

        private E_LoanPurposePropertyUsageType ToMismo(E_aOccT aOccT)
        {
            switch (aOccT)
            {
                case E_aOccT.PrimaryResidence: return E_LoanPurposePropertyUsageType.PrimaryResidence;
                case E_aOccT.SecondaryResidence: return E_LoanPurposePropertyUsageType.SecondHome;
                case E_aOccT.Investment: return E_LoanPurposePropertyUsageType.Investor;
                default:
                    LogInvalidEnum("aOccT", aOccT);
                    return E_LoanPurposePropertyUsageType.Undefined;
            }
        }

        private E_LoanPurposePropertyRightsType ToMismo(E_sEstateHeldT sEstateHeldT)
        {
            switch (sEstateHeldT)
            {
                case E_sEstateHeldT.FeeSimple: return E_LoanPurposePropertyRightsType.FeeSimple;
                case E_sEstateHeldT.LeaseHold: return E_LoanPurposePropertyRightsType.Leasehold;
                default:
                    LogInvalidEnum("sEstateHeldT", sEstateHeldT);
                    return E_LoanPurposePropertyRightsType.Undefined;
            }
        }

        private ConstructionRefinanceData CreateConstructionRefinanceData()
        {
            ConstructionRefinanceData constructionRefinanceData = new ConstructionRefinanceData();

            switch (m_dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    constructionRefinanceData.GseRefinancePurposeType = ToMismoGseRefinancePurposeType(m_dataLoan.sRefPurpose);
                    constructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sSpAcqYr;
                    constructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sSpLien_rep;
                    constructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sSpOrigC_rep;
                    constructionRefinanceData.RefinanceImprovementCostsAmount = m_dataLoan.sSpImprovC_rep;
                    constructionRefinanceData.RefinanceImprovementsType = ToMismo(m_dataLoan.sSpImprovTimeFrameT);
                    constructionRefinanceData.FreCashOutAmount = m_dataLoan.sProdCashoutAmt_rep;
                    constructionRefinanceData.RefinanceProposedImprovementsDescription = m_dataLoan.sSpImprovDesc;
                    constructionRefinanceData.SecondaryFinancingRefinanceIndicator = ToMismo(m_dataLoan.sPayingOffSubordinate);
                    break;
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    constructionRefinanceData.ConstructionPeriodInterestRatePercent = m_dataLoan.sConstructionPeriodIR_rep;
                    constructionRefinanceData.ConstructionPeriodNumberOfMonthsCount = m_dataLoan.sConstructionPeriodMon_rep;
                    constructionRefinanceData.ConstructionImprovementCostsAmount = m_dataLoan.sConstructionImprovementAmt_rep;
                    constructionRefinanceData.ConstructionPurposeType = m_dataLoan.sLPurposeT == E_sLPurposeT.Construct ? E_ConstructionRefinanceDataConstructionPurposeType.ConstructionOnly : E_ConstructionRefinanceDataConstructionPurposeType.ConstructionToPermanent;
                    constructionRefinanceData.LandEstimatedValueAmount = m_dataLoan.sLotVal_rep;
                    constructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sLotAcqYr;
                    constructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sLotLien_rep;
                    constructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sLotOrigC_rep;
                    break;
            };

            //constructionRefinanceData.Id = null;

            //constructionRefinanceData.ConstructionToPermanentClosingFeatureType = null;
            //constructionRefinanceData.ConstructionToPermanentClosingFeatureTypeOtherDescription = null;
            //constructionRefinanceData.ConstructionToPermanentClosingType = null;
            //constructionRefinanceData.ConstructionToPermanentClosingTypeOtherDescription = null;
            //constructionRefinanceData.FnmSecondMortgageFinancingOriginalPropertyIndicator = null;
            //constructionRefinanceData.FreCashOutAmount = null;
            //constructionRefinanceData.GseRefinancePurposeType = null;
            //constructionRefinanceData.GseRefinancePurposeTypeOtherDescription = null;
            //constructionRefinanceData.LandEstimatedValueAmount = null;
            //constructionRefinanceData.LandOriginalCostAmount = null;
            //constructionRefinanceData.NonStructuralAlterationsConventionalAmount = null;
            //constructionRefinanceData.PropertyAcquiredYear = null;
            //constructionRefinanceData.PropertyExistingLienAmount = null;
            //constructionRefinanceData.PropertyOriginalCostAmount = null;
            //constructionRefinanceData.RefinanceImprovementCostsAmount = null;
            //constructionRefinanceData.RefinanceImprovementsType = null;
            //constructionRefinanceData.RefinanceProposedImprovementsDescription = null;
            //constructionRefinanceData.SecondaryFinancingRefinanceIndicator = null;
            //constructionRefinanceData.StructuralAlterationsConventionalAmount = null;
            return constructionRefinanceData;
        }

        private E_ConstructionRefinanceDataRefinanceImprovementsType ToMismo(E_sSpImprovTimeFrameT sSpImprovTimeFrameT)
        {
            switch (sSpImprovTimeFrameT)
            {
                case E_sSpImprovTimeFrameT.LeaveBlank: return E_ConstructionRefinanceDataRefinanceImprovementsType.Unknown;
                case E_sSpImprovTimeFrameT.Made: return E_ConstructionRefinanceDataRefinanceImprovementsType.Made;
                case E_sSpImprovTimeFrameT.ToBeMade: return E_ConstructionRefinanceDataRefinanceImprovementsType.ToBeMade;
                default:
                    LogInvalidEnum("sSpImprovTimeFrameT", sSpImprovTimeFrameT);
                    return E_ConstructionRefinanceDataRefinanceImprovementsType.Undefined;
            }
        }

        private E_ConstructionRefinanceDataGseRefinancePurposeType ToMismoGseRefinancePurposeType(string desc)
        {
            switch (desc.ToLower())
            {
                case "no cash-out rate/term": return E_ConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm;
                case "limited cash-out rate/term": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited;
                case "cash-out/home improvement": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement;
                case "cash-out/debt consolidation": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation;
                case "cash-out/other": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                default:
                    if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance 
                        || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl || m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther;
                    else if (m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout)
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                    else
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.Undefined;

            }
        }

        private LoanQualification CreateLoanQualification()
        {
            LoanQualification loanQualification = new LoanQualification();
            //loanQualification.Id = null;
            loanQualification.AdditionalBorrowerAssetsConsideredIndicator = ToMismo(m_dataLoan.sMultiApps);

            if (m_dataLoan.sLT == E_sLT.VA)
            {
                loanQualification.AdditionalBorrowerAssetsNotConsideredIndicator = ToMismo(m_dataLoan.GetAppData(0).aVaIsCoborIConsidered);
            }
            else
            {
                loanQualification.AdditionalBorrowerAssetsNotConsideredIndicator = ToMismo(m_dataLoan.GetAppData(0).aSpouseIExcl);
            }
            
            return loanQualification;
        }

        private Mers CreateMers()
        {
            Mers mers = new Mers();
            //mers.Id = null;
            //mers.MersMortgageeOfRecordIndicator = null;
            mers.MersOriginalMortgageeOfRecordIndicator = ToMismo(m_dataLoan.sMersIsOriginalMortgagee);
            //mers.MersRegistrationDate = null;
            //mers.MersRegistrationIndicator = null;
            //mers.MersRegistrationStatusType = null;
            //mers.MersRegistrationStatusTypeOtherDescription = null;
            //mers.MersTaxNumberIdentifier = null;
            mers.MersMinNumber = m_dataLoan.sMersMin;
            return mers;
        }

        private MiData CreateMiData()
        {
            MiData miData = new MiData();
            miData.MiPremiumFinancedIndicator = (m_dataLoanWithGfeArchiveApplied.sFfUfmipFinanced) > 0 ? E_YNIndicator.Y : E_YNIndicator.N;
            if (m_dataLoan.sLT == E_sLT.FHA)
            {
                // For FHA transaction, only need to pass initial "Upfront" Premium in this property.
                miData.MiFhaUpfrontPremiumAmount = m_dataLoanWithGfeArchiveApplied.sFfUfmip1003_rep;
                miData.MiSourceType = E_MiDataMiSourceType.FHA;
            }
            else
            {
                miData.MiInitialPremiumAmount = m_dataLoanWithGfeArchiveApplied.sFfUfmip1003_rep;
                miData.MiInitialPremiumRatePercent = m_dataLoanWithGfeArchiveApplied.sFfUfmipR_rep;
                miData.MiInitialPremiumRateDurationMonths = m_dataLoanWithGfeArchiveApplied.sMipPiaMon_rep;
                if (m_dataLoan.sLT != E_sLT.UsdaRural) // If the loan type is USDA, omit the MiSourceType. OPM 84763, OPM 84461
                {
                    miData.MiSourceType = E_MiDataMiSourceType.PMI;
                }
            }

            if (m_dataLoan.sLT == E_sLT.FHA || // 10/17/2011 dd - OPM 72590
                m_dataLoan.sLT == E_sLT.UsdaRural) // 05/04/2012 mp - OPM 74723
            {
                miData.MiPremiumFromClosingAmount = m_dataLoanWithGfeArchiveApplied.sUfCashPd_rep;
            }
            //miData.Id = null;
            //miData.BorrowerMiTerminationDate = null;
            //miData.MiCertificateIdentifier = null;
            if (m_dataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes)
            {
                // sk 106227
                switch (m_exportSource)
                {
                    case E_SettlementChargesExportSource.SETTLEMENT:
                        miData.MiCollectedNumberOfMonthsCount = m_dataLoan.sSettlementMInsRsrvMon_rep;
                        break;
                    case E_SettlementChargesExportSource.GFE:
                        miData.MiCollectedNumberOfMonthsCount = m_dataLoan.sMInsRsrvMon_rep;
                        break;
                    default:
                        throw new UnhandledEnumException(m_exportSource);
                }

                miData.MiCushionNumberOfMonthsCount = m_dataLoan.m_convertLos.ToCountString(m_dataLoan.sInitialEscrowAcc[0, 2]); // The mortgage insurance column of the cushion row - OPM 90303
            }

            //miData.MiCompanyName = null;
            //miData.MiDurationType = null;
            miData.MiEscrowIncludedInAggregateIndicator = E_YNIndicator.Y;
            //miData.MiInitialPremiumAtClosingType = null;
            //miData.MiPremiumFinancedIndicator = null;
            //miData.MiPremiumFromClosingAmount = null;
            if (m_dataLoan.sMiInsuranceT == E_sMiInsuranceT.LenderPaid || m_dataLoan.sMiInsuranceT == E_sMiInsuranceT.InvestorPaid)
                miData.MiPremiumPaymentType = E_MiDataMiPremiumPaymentType.LenderPaid;
            else if (m_dataLoan.sMiInsuranceT == E_sMiInsuranceT.BorrowerPaid)
                miData.MiPremiumPaymentType = E_MiDataMiPremiumPaymentType.BorrowerPaid;
            //miData.MiPremiumRefundableType = null;
            //miData.MiPremiumTermMonths = null;
            //miData.MiRenewalCalculationType = null;
            //miData.MiScheduledTerminationDate = null;
            //miData.MiSourceType = null;
            miData.MiLtvCutoffPercent = m_dataLoan.sProMInsCancelLtv_rep;
            //miData.MiLtvCutoffType = null;
            //miData.ScheduledAmortizationMidpointDate = null;
            //miData.MiPremiumTax = CreateMiPremiumTax();
            miData.MiRenewalPremiumList.Add(CreateMiRenewalPremium(E_MiRenewalPremiumSequence.First, m_dataLoan.sProMInsR_rep, m_dataLoan.sProMInsMon_rep, m_dataLoan.sProMIns_rep));
            miData.MiRenewalPremiumList.Add(CreateMiRenewalPremium(E_MiRenewalPremiumSequence.Second, m_dataLoan.sProMInsR2_rep, m_dataLoan.sProMIns2Mon_rep, m_dataLoan.sProMIns2_rep));

            miData.PaidTo = CreatePaidTo(E_AgentRoleT.MortgageInsurance);
            return miData;
        }

        private MiPremiumTax CreateMiPremiumTax()
        {
            MiPremiumTax miPremiumTax = new MiPremiumTax();
            //miPremiumTax.Id = null;
            //miPremiumTax.CodeAmount = null;
            //miPremiumTax.CodePercent = null;
            //miPremiumTax.CodeType = null;
            return miPremiumTax;
        }

        private MiRenewalPremium CreateMiRenewalPremium(E_MiRenewalPremiumSequence sequence, string rate, string term, string monthlyPayment)
        {
            if (rate == "")
            {
                return null;
            }
            MiRenewalPremium miRenewalPremium = new MiRenewalPremium();
            //miRenewalPremium.Id = null;
            miRenewalPremium.MonthlyPaymentAmount = monthlyPayment;
            //miRenewalPremium.MonthlyPaymentRoundingType = null;
            miRenewalPremium.Rate = rate;
            miRenewalPremium.RateDurationMonths = term;
            miRenewalPremium.Sequence = sequence;
            return miRenewalPremium;
        }

        private MortgageTerms CreateMortgageTerms()
        {
            MortgageTerms mortgageTerms = new MortgageTerms();

            mortgageTerms.AgencyCaseIdentifier = m_dataLoan.sAgencyCaseNum;

            mortgageTerms.BaseLoanAmount = (m_dataLoan.sIsLineOfCredit ? m_dataLoan.sCreditLineAmt_rep : m_dataLoan.sLAmtCalc_rep);
            mortgageTerms.BorrowerRequestedLoanAmount = m_dataLoan.sFinalLAmt_rep;
            mortgageTerms.LenderCaseIdentifier = m_dataLoan.sLenderCaseNum;
            mortgageTerms.LenderLoanIdentifier = m_dataLoan.sLNm;
            mortgageTerms.LoanAmortizationTermMonths = m_dataLoan.sTerm_rep;
            mortgageTerms.LoanEstimatedClosingDate = m_dataLoan.sDocMagicClosingD_rep;

            mortgageTerms.MortgageType = ToMismo(m_dataLoan.sLT, m_dataLoan.sLpTemplateNm, m_dataLoan.sIsLineOfCredit);
            mortgageTerms.OtherMortgageTypeDescription = m_dataLoan.sLTODesc;
            mortgageTerms.LoanAmortizationType = ToMismo(m_dataLoan.sFinMethT);

            mortgageTerms.NoteRatePercent = m_dataLoan.sNoteIR_rep;
            mortgageTerms.OriginalLoanAmount = (m_dataLoan.sIsLineOfCredit ? m_dataLoan.sCreditLineAmt_rep : m_dataLoan.sFinalLAmt_rep);;
            mortgageTerms.OtherAmortizationTypeDescription = m_dataLoan.sFinMethDesc;
            
            mortgageTerms.RequestedInterestRatePercent = m_dataLoan.sNoteIR_rep;
            //mortgageTerms.Id = null;
            //mortgageTerms.ArmTypeDescription = null;

            //mortgageTerms.LendersContactPrimaryTelephoneNumber = null;

            //mortgageTerms.PaymentRemittanceDay = null;

            return mortgageTerms;
        }


        private Mismo.Closing2_6.Property CreateProperty()
        {
            Mismo.Closing2_6.Property property = new Mismo.Closing2_6.Property();
            property.City = m_dataLoan.sSpCity;
            property.County = m_dataLoan.sSpCounty;
            property.FinancedNumberOfUnits = m_dataLoan.sUnitsNum_rep;

            property.PostalCode = m_dataLoan.sSpZip;

            property.State = m_dataLoan.sSpState;
            property.StreetAddress = m_dataLoan.sSpAddr;

            property.StructureBuiltYear = m_dataLoan.sYrBuilt;

            property.LegalDescriptionList.Add(CreateLegalDescription(m_dataLoan.sSubjPropertyMineralAbbrLegalDesc, E_LegalDescriptionType.ShortLegal, ""));
            property.LegalDescriptionList.Add(CreateLegalDescription(m_dataLoan.sSpLegalDesc, E_LegalDescriptionType.Other, "1003"));

            //property.Id = null; // This should link with LOSDATA/ManufacturedHome/@PropertyID
            property.AssessorsParcelIdentifier = m_dataLoan.sAssessorsParcelId;
            //property.AssessorsSecondParcelIdentifier = null;
            
            //property.CurrentVacancyStatusType = null;
            if (m_dataLoan.sLT == E_sLT.VA)
            {
                property.GrossLivingAreaSquareFeetCount = m_dataLoan.sSpLivingSqf;
            }
            //property.ManufacturedHomeManufactureYear = null;
            //property.NativeAmericanLandsType = null;
            //property.NativeAmericanLandsTypeOtherDescription = null;
            //property.PlannedUnitDevelopmentIndicator = null;
            //property.StoriesCount = null;
            //property.UniqueDwellingType = null;
            //property.UniqueDwellingTypeOtherDescription = null;
            //property.AcquiredDate = null;
            //property.AcreageNumber = null;
            //property.CommunityLandTrustIndicator = null;
            //property.ConditionDescription = null;
            //property.Country = null;
            //property.InclusionaryZoningIndicator = null;
            //property.NeighborhoodLocationType = null;
            //property.NeighborhoodLocationTypeOtherDescription = null;

            
            if (m_dataLoan.sGseSpT == E_sGseSpT.Cooperative)
                property.OwnershipType = E_PropertyOwnershipType.Cooperative;

            //property.OwnershipTypeOtherDescription = null;

            if (m_dataLoan.sLT == E_sLT.VA)
            {
                switch (m_dataLoan.sVaPropDesignationT)
	            {
		            case E_sVaPropDesignationT.LeaveBlank:
                        break;
                    case E_sVaPropDesignationT.ExistingOcc:
                        property.BuildingStatusType = E_PropertyBuildingStatusType.Existing;
                        property.PreviouslyOccupiedIndicator = E_YNIndicator.Y;
                        break;
                    case E_sVaPropDesignationT.ProposedConstruction:
                        property.BuildingStatusType = E_PropertyBuildingStatusType.Proposed;
                        break;
                    case E_sVaPropDesignationT.ExistingNew:
                        property.BuildingStatusType = E_PropertyBuildingStatusType.Existing;
                        property.PreviouslyOccupiedIndicator = E_YNIndicator.N;
                        break;
                    case E_sVaPropDesignationT.EnergyImprovement:
                        throw new UnhandledEnumException(m_dataLoan.sVaPropDesignationT);
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sVaPropDesignationT);
	            }
            }
            else
            {
                switch (m_dataLoan.sFHAConstructionT)
                {
                    case E_sFHAConstructionT.Blank:
                        break;
                    case E_sFHAConstructionT.Existing:
                        property.PreviouslyOccupiedIndicator = E_YNIndicator.Y;
                        break;
                    case E_sFHAConstructionT.Proposed:
                    case E_sFHAConstructionT.New:
                        property.PreviouslyOccupiedIndicator = E_YNIndicator.N;
                        break;
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFHAConstructionT);
                }
            }
            //property.StreetAddress2 = null;
            //property.ZoningCategoryType = null;
            //property.ZoningCategoryTypeOtherDescription = null;
            property.ParsedStreetAddress = CreateParsedStreetAddress();
            property.ValuationList.Add(CreateValuation());
            property.Details = CreateDetails();
            //property.HomeownersAssociation = CreateHomeownersAssociation();
            //property.Project = CreateProject();
            //property.CategoryList.Add(CreateCategory());
            
            property.DwellingUnitList.Add(CreateDwellingUnit());
            
            //property.ManufacturedHomeList.Add(CreateManufacturedHome());

            if (m_dataLoan.sLT == E_sLT.FHA || m_dataLoan.sLT == E_sLT.VA)
            {
                property.PlattedLandList.Add(CreatePlattedLand());
            }
            
            //property.UnplattedLand = CreateUnplattedLand();
            property.FloodDeterminationList.Add(CreateFloodDetermination());
            return property;
        }

        private LegalDescription CreateLegalDescription(string desc, E_LegalDescriptionType type, string typeDescription)
        {
            LegalDescription legalDescription = new LegalDescription();
            //legalDescription.Id = null;
            legalDescription.TextDescription = desc;
            legalDescription.Type = type;
            legalDescription.TypeOtherDescription = typeDescription;
            //legalDescription.PlattedLand = CreatePlattedLand();
            return legalDescription;
        }

        private ParsedStreetAddress CreateParsedStreetAddress()
        {
            ParsedStreetAddress parsedStreetAddress = new ParsedStreetAddress();
            //parsedStreetAddress.Id = null;
            if (m_dataLoan.sGseSpT == E_sGseSpT.Cooperative) // Only send when Cooperative: refer to OPM 61315, 4/16/12 4:21PM
                parsedStreetAddress.ApartmentOrUnit = m_dataLoan.sCooperativeApartmentNumber;
            //parsedStreetAddress.BuildingNumber = null;
            //parsedStreetAddress.DirectionPrefix = null;
            //parsedStreetAddress.DirectionSuffix = null;
            //parsedStreetAddress.HouseNumber = null;
            //parsedStreetAddress.MilitaryApoFpo = null;
            //parsedStreetAddress.PostOfficeBox = null;
            //parsedStreetAddress.RuralRoute = null;
            //parsedStreetAddress.StreetName = null;
            //parsedStreetAddress.StreetSuffix = null;
            return parsedStreetAddress;
        }

        private Valuation CreateValuation()
        {
            Valuation valuation = new Valuation();
            //valuation.Id = null;
            //valuation.AppraisalFormType = null;
            //valuation.AppraisalFormTypeOtherDescription = null;
            //valuation.AppraisalFormVersionIdentifier = null;
            //valuation.AppraisalInspectionType = null;
            //valuation.AppraisalInspectionTypeOtherDescription = null;

            if (m_dataLoan.sPriorAppraisalUsed)
            {
                valuation.MethodType = E_ValuationMethodType.PriorAppraisalUsed;
            }
            //valuation.MethodTypeOtherDescription = null;
            valuation.AppraiserList.Add(CreateAppraiser());
            return valuation;
        }

        private Appraiser CreateAppraiser()
        {
            Appraiser appraiser = new Appraiser();
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }

            //appraiser.Id = null;
            appraiser.LicenseIdentifier = agent.LicenseNumOfAgent;
            appraiser.LicenseState = agent.State;

            appraiser.CompanyName = agent.CompanyName;

            if (!string.IsNullOrEmpty(agent.LicenseNumOfCompany))
            {
                appraiser.CompanyLicenseIdentifier = agent.LicenseNumOfCompany;
                appraiser.CompanyLicenseState = agent.State;
            }
            
            
            appraiser.Name = agent.AgentName;
            return appraiser;
        }

        private Details CreateDetails()
        {
            Details details = new Details();
            //details.Id = null;
            //details.CondominiumIndicator = null;
            //details.CondominiumPudDeclarationsDescription = null;
            //details.JudicialDistrictName = null;
            //details.JudicialDivisionName = null;
            //details.ManufacturedHomeIndicator = null;
            //details.NfipCommunityIdentifier = null;
            //details.NfipCommunityName = null;
            //details.NfipCommunityParticipationStatusType = null;
            //details.NfipCommunityParticipationStatusTypeOtherDescription = null;
            details.NfipFloodZoneIdentifier = m_dataLoan.sNfipFloodZoneId;

            if ((m_dataLoan.sGseSpT == E_sGseSpT.Attached || m_dataLoan.sGseSpT == E_sGseSpT.Detached)
                    && m_dataLoan.sUnitsNum > 1)
                details.OneToFourFamilyIndicator = E_YNIndicator.Y;
            
            details.ProjectName = m_dataLoan.sProjNm;
            if (m_dataLoan.sGseSpT == E_sGseSpT.Cooperative) // Only send when Cooperative: refer to OPM 61315, 4/16/12 4:21PM
                details.ProjectTotalSharesCount = m_dataLoan.sCooperativeNumberOfShares_rep;
            //details.PropertyUnincorporatedAreaName = null;
            //details.RecordingJurisdictionName = null;
            //details.RecordingJurisdictionType = null;
            //details.RecordingJurisdictionTypeOtherDescription = null;
            return details;
        }

        private HomeownersAssociation CreateHomeownersAssociation()
        {
            HomeownersAssociation homeownersAssociation = new HomeownersAssociation();
            //homeownersAssociation.Id = null;
            //homeownersAssociation.City = null;
            //homeownersAssociation.Country = null;
            //homeownersAssociation.County = null;
            //homeownersAssociation.Name = null;
            //homeownersAssociation.PostalCode = null;
            //homeownersAssociation.State = null;
            //homeownersAssociation.StreetAddress = null;
            //homeownersAssociation.StreetAddress2 = null;
            //homeownersAssociation.ContactDetail = CreateContactDetail();
            return homeownersAssociation;
        }

        private Project CreateProject()
        {
            Project project = new Project();
            //project.Id = null;
            //project.LivingUnitCount = null;
            //project.ClassificationType = null;
            //project.ClassificationTypeOtherDescription = null;
            //project.DesignType = null;
            //project.DesignTypeOtherDescription = null;
            return project;
        }

        private Category CreateCategory()
        {
            Category category = new Category();
            //category.Id = null;
            //category.Type = null;
            //category.TypeOtherDescription = null;
            return category;
        }

        private DwellingUnit CreateDwellingUnit()
        {
            DwellingUnit dwellingUnit = new DwellingUnit();
            //dwellingUnit.Id = null;
            dwellingUnit.BedroomCount = m_dataLoan.sSpBedroomCount;
            dwellingUnit.BathroomCountDescription = m_dataLoan.sSpBathCount;
            //dwellingUnit.PropertyRehabilitationCompletionDate = null;
            //dwellingUnit.EligibleRentAmount = null;
            //dwellingUnit.LeaseProvidedIndicator = null;
            return dwellingUnit;
        }

        private Mismo.Closing2_6.ManufacturedHome CreateManufacturedHome()
        {
            Mismo.Closing2_6.ManufacturedHome manufacturedHome = new Mismo.Closing2_6.ManufacturedHome();
            //manufacturedHome.Id = null;
            //manufacturedHome.LengthFeetCount = null;
            //manufacturedHome.WidthFeetCount = null;
            //manufacturedHome.AttachedToFoundationIndicator = null;
            //manufacturedHome.ConditionDescriptionType = null;
            //manufacturedHome.HudCertificationLabelIdentifier = null;
            //manufacturedHome.MakeIdentifier = null;
            //manufacturedHome.ModelIdentifier = null;
            //manufacturedHome.SerialNumberIdentifier = null;
            //manufacturedHome.WidthType = null;
            return manufacturedHome;
        }

        private PlattedLand CreatePlattedLand()
        {
            PlattedLand plattedLand = new PlattedLand();
            //plattedLand.Id = null;
            //plattedLand.PlatName = null;
            plattedLand.PropertyBlockIdentifier = m_dataLoan.sFHAPropImprovBorrRelativeBlock;
            plattedLand.PropertyLotIdentifier = m_dataLoan.sFHAPropImprovBorrRelativeLot;
            //plattedLand.PropertySectionIdentifier = null;
            plattedLand.PropertySubdivisionIdentifier = m_dataLoan.sFHAPropImprovBorrRelativeSubdivision;
            //plattedLand.PropertyTractIdentifier = null;
            //plattedLand.RecordedDocumentBook = null;
            //plattedLand.RecordedDocumentPage = null;
            //plattedLand.AdditionalParcelDescription = null;
            //plattedLand.AdditionalParcelIdentifier = null;
            //plattedLand.AppurtenanceDescription = null;
            //plattedLand.AppurtenanceIdentifier = null;
            //plattedLand.BuildingIdentifier = null;
            //plattedLand.PlatCodeIdentifier = null;
            //plattedLand.PlatInstrumentIdentifier = null;
            //plattedLand.SequenceIdentifier = null;
            //plattedLand.Type = null;
            //plattedLand.TypeOtherDescription = null;
            //plattedLand.UnitNumberIdentifier = null;
            return plattedLand;
        }

        private UnplattedLand CreateUnplattedLand()
        {
            UnplattedLand unplattedLand = new UnplattedLand();
            //unplattedLand.Id = null;
            //unplattedLand.PropertyRangeIdentifier = null;
            //unplattedLand.PropertySectionIdentifier = null;
            //unplattedLand.PropertyTownshipIdentifier = null;
            //unplattedLand.AbstractNumberIdentifier = null;
            //unplattedLand.BaseIdentifier = null;
            //unplattedLand.DescriptionType = null;
            //unplattedLand.DescriptionTypeOtherDescription = null;
            //unplattedLand.LandGrantIdentifier = null;
            //unplattedLand.MeridianIdentifier = null;
            //unplattedLand.MetesAndBoundsRemainingDescription = null;
            //unplattedLand.QuarterSectionIdentifier = null;
            //unplattedLand.SequenceIdentifier = null;
            return unplattedLand;
        }

        private FloodDetermination CreateFloodDetermination()
        {
            FloodDetermination floodDetermination = new FloodDetermination();
            //floodDetermination.FloodDeterminationId = null;
            floodDetermination.FloodCertificationIdentifier = m_dataLoan.sFloodCertId;
            //floodDetermination.FloodContractFeeAmount = null;
            //floodDetermination.FloodDeterminationLifeofLoanIndicator = null;
            //floodDetermination.FloodPartialIndicator = null;
            //floodDetermination.FloodProductCertifyDate = null;
            //floodDetermination.NfipCommunityIdentifier = null;
            //floodDetermination.NfipCommunityName = null;
            //floodDetermination.NfipCommunityParticipationStatusType = null;
            //floodDetermination.NfipCommunityParticipationStatusTypeOtherDescription = null;
            //floodDetermination.NfipFloodZoneIdentifier = m_dataLoan.sNfipFloodZoneId; // We already export it through Property/Details, so leave it out for now
            //floodDetermination.NfipMapIdentifier = null;
            //floodDetermination.NfipMapPanelDate = null;
            //floodDetermination.NfipMapPanelIdentifier = null;
            //floodDetermination.SpecialFloodHazardAreaIndicator = null;
            return floodDetermination;
        }

        private ProposedHousingExpense CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType type, string amount)
        {
            ProposedHousingExpense proposedHousingExpense = new ProposedHousingExpense();
            //proposedHousingExpense.Id = null;

            //proposedHousingExpense.HousingExpenseTypeOtherDescription = null;


            proposedHousingExpense.HousingExpenseType = type;
            proposedHousingExpense.PaymentAmount = amount;
            return proposedHousingExpense;
        }

        private ReoProperty CreateReoProperty(IRealEstateOwned field, CAppData dataApp, int appNumber)
        {
            ReoProperty reoProperty = new ReoProperty();
            reoProperty.ReoId = "REO_" + field.RecordId.ToString("N");
            reoProperty.BorrowerId = field.ReOwnerT == E_ReOwnerT.CoBorrower ? dataApp.aCMismoId : dataApp.aBMismoId;

            Guid[] matchedLiabilities = dataApp.FindMatchedLiabilities(field.RecordId);
            string str = "";
            foreach (Guid id in matchedLiabilities)
            {
                str += GenerateLiabilityId("LIA_" + id.ToString("N"), appNumber) + " ";
            }
            if (matchedLiabilities.Length > 0)
            {
                reoProperty.LiabilityId = str.TrimWhitespaceAndBOM();
            }
            reoProperty.StreetAddress = field.Addr;
            reoProperty.City = field.City;
            reoProperty.State = field.State;
            reoProperty.PostalCode = field.Zip;
            reoProperty.GsePropertyType = ToMismoReoType(field.TypeT);
            reoProperty.DispositionStatusType = ToMismo(field.StatT);
            reoProperty.LienInstallmentAmount = field.MPmt_rep;
            reoProperty.LienUpbAmount = field.MAmt_rep;
            reoProperty.MaintenanceExpenseAmount = field.HExp_rep;
            reoProperty.MarketValueAmount = field.Val_rep;
            reoProperty.RentalIncomeGrossAmount = field.GrossRentI_rep;
            reoProperty.RentalIncomeNetAmount = field.NetRentI_rep;
            reoProperty.SubjectIndicator = ToMismo(field.IsSubjectProp);

            return reoProperty;
        }

        private E_ReoPropertyDispositionStatusType ToMismo(E_ReoStatusT e_ReoStatusT)
        {
            switch (e_ReoStatusT)
            {
                case E_ReoStatusT.PendingSale: return E_ReoPropertyDispositionStatusType.PendingSale;
                case E_ReoStatusT.Rental: return E_ReoPropertyDispositionStatusType.RetainForRental;
                case E_ReoStatusT.Residence: return E_ReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence;
                case E_ReoStatusT.Sale: return E_ReoPropertyDispositionStatusType.Sold;
                default:
                    LogInvalidEnum("E_ReoStatusT", e_ReoStatusT);
                    return E_ReoPropertyDispositionStatusType.Undefined;

            }
        }

        private E_ReoPropertyGsePropertyType ToMismoReoType(E_ReoTypeT type)
        {
            switch (type)
            {
                case E_ReoTypeT._2_4Plx: return E_ReoPropertyGsePropertyType.TwoToFourUnitProperty;
                case E_ReoTypeT.ComNR: return E_ReoPropertyGsePropertyType.CommercialNonResidential;
                case E_ReoTypeT.ComR: return E_ReoPropertyGsePropertyType.HomeAndBusinessCombined;
                case E_ReoTypeT.Condo: return E_ReoPropertyGsePropertyType.Condominium;
                case E_ReoTypeT.Coop: return E_ReoPropertyGsePropertyType.Cooperative;
                case E_ReoTypeT.Farm: return E_ReoPropertyGsePropertyType.Farm;
                case E_ReoTypeT.Land: return E_ReoPropertyGsePropertyType.Land;
                case E_ReoTypeT.Mixed: return E_ReoPropertyGsePropertyType.MixedUseResidential;
                case E_ReoTypeT.Mobil: return E_ReoPropertyGsePropertyType.ManufacturedMobileHome;
                case E_ReoTypeT.Multi: return E_ReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits;
                case E_ReoTypeT.SFR: return E_ReoPropertyGsePropertyType.SingleFamily;
                case E_ReoTypeT.Town: return E_ReoPropertyGsePropertyType.Townhouse;
                case E_ReoTypeT.Other: return E_ReoPropertyGsePropertyType.Undefined;
                default:
                    return E_ReoPropertyGsePropertyType.Undefined;
            }
        }

        private PaidTo CreateRespaFeePaidTo(string hudline)
        {
            E_AgentRoleT agentRole;
            switch (hudline)
            {
                case "803":
                    agentRole = E_AgentRoleT.Appraiser;
                    break;
                case "804":
                    agentRole = E_AgentRoleT.CreditReport;
                    break;
                default:
                    return null;
            }
            return CreatePaidTo(agentRole);
        }

        /// <summary>
        /// Creates a RESPA_FEE element with a PAID_TO element and a PAYMENT element containing the $amount
        /// </summary>
        /// <param name="sectionClassificationType"></param>
        /// <param name="aggregationType"></param>
        /// <param name="feeType"></param>
        /// <param name="hudLineNumber"></param>
        /// <param name="description"></param>
        /// <param name="isPaid"></param>
        /// <param name="amount"></param>
        /// <param name="props"></param>
        /// <param name="point"></param>
        /// <param name="fixedAmount"></param>
        /// <param name="gfeDisclosedAmount"></param>
        /// <param name="paidToOverrideString"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private Mismo.Closing2_6.RespaFee CreateRespaFee(
            E_RespaFeeRespaSectionClassificationType sectionClassificationType,
            E_RespaFeeGfeAggregationType aggregationType,
            E_RespaFeeType feeType,
            string hudLineNumber,
            string description,
            bool isPaid,
            string amount,
            int props,
            string point,
            string fixedAmount,
            string gfeDisclosedAmount,
            string paidToOverrideString,
            string id,
            string totalAmount)
        {
            // if ((string.IsNullOrEmpty(amount) || "0.00" == amount) && ) { return null; }
            if ((string.IsNullOrEmpty(amount) || amount == "0.00") &&
                (string.IsNullOrEmpty(gfeDisclosedAmount) || gfeDisclosedAmount == "0.00"))
            { return null; }

            Mismo.Closing2_6.RespaFee respaFee = new Mismo.Closing2_6.RespaFee();

            respaFee.RespaSectionClassificationType = sectionClassificationType;
            respaFee.SpecifiedHudLineNumber = hudLineNumber;
            respaFee.Type = feeType;
            respaFee.TypeOtherDescription = description;
            respaFee.GfeAggregationType = aggregationType;

            CPageData dateLoan = m_exportSource == E_SettlementChargesExportSource.GFE ? m_dataLoanWithGfeArchiveApplied : m_dataLoan;
            if (hudLineNumber == "802" && LosConvert.GfeItemProps_BF(props)
                && dateLoan.sQMNonExcludableDiscountPointsPc != 0)
            {
                respaFee.PaymentList.Add(CreatePayment(dateLoan.sQMDiscountPointsF_SansNonExcludable_rep, isPaid, props));

                // Copy props, but uncheck bona fide
                int nonExcludableProps = LosConvert.GfeItemProps_UpdateBF(props, false);
                respaFee.PaymentList.Add(CreatePayment(dateLoan.sQMNonExcludableDiscountPointsF_rep, isPaid, nonExcludableProps));
            }
            else
            {
                respaFee.PaymentList.Add(CreatePayment(amount, isPaid, props));
            }

            if (m_exportSource == E_SettlementChargesExportSource.GFE)
            {
                respaFee.Id = string.IsNullOrEmpty(id) ? ("LQBGFE" + hudLineNumber) : id;
            }
            //respaFee.ItemDescription = null;
            //respaFee.PaidToTypeOtherDescription = null;
            //respaFee.PercentBasisType = null;
            //respaFee.PercentBasisTypeOtherDescription = null;
            //respaFee.RequiredProviderOfServiceIndicator = null;
            //respaFee.ResponsiblePartyType = null;
            respaFee.SpecifiedFixedAmount = fixedAmount;
            respaFee.TotalAmount = totalAmount;
            respaFee.TotalPercent = point;


            if (   StaticMethodsAndExtensions.IsInRange(hudLineNumber, 1100, 1199)
                || StaticMethodsAndExtensions.IsInRange(hudLineNumber, 1300, 1399))
            {
                respaFee.BorrowerChosenProviderIndicator = ToMismo(LosConvert.GfeItemProps_Borr(props));
            }
            //respaFee.RequiredServiceProvider = CreateRequiredServiceProvider();

            if (string.IsNullOrEmpty(gfeDisclosedAmount) == false)
            {
                respaFee.GfeDisclosedAmount = gfeDisclosedAmount;
            }

            if (LosConvert.GfeItemProps_ToBr(props) || hudLineNumber == "808")
            {
                // 11/17/2010 dd - Always set PaidTo Type to Broker for hudline 808
                respaFee.PaidToType = E_RespaFeePaidToType.Broker;
            }
            else
            {
                // 3/18/2009 dd - In order for SERVICE_PROVIDER is created on DocMagic need to set PaidToType = Other.
                // I intentionally skip hudline=803 because we don't want to export Appraiser Info twice.
                if (hudLineNumber == "804")
                {
                    respaFee.PaidToType = E_RespaFeePaidToType.Other;
                }
                else
                {
                    respaFee.PaidToType = E_RespaFeePaidToType.Lender;
                }
            }
            if (sectionClassificationType == E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges)
            {
                // 11/17/2010 dd - By default these HUD section is paid to other.
                respaFee.PaidToType = E_RespaFeePaidToType.Other;
            }
            
            if (!string.IsNullOrEmpty(paidToOverrideString))
            {
                respaFee.PaidTo = CreatePaidTo(paidToOverrideString);
                respaFee.PaidToType = E_RespaFeePaidToType.Other; // 11/17/2010 dd - When user fill out Paid To then always set PaidToType to other for value to transfer to DocMagic correctly.
            }
            else
            {
                var paidTo = CreateRespaFeePaidTo(hudLineNumber);
                if (paidTo != null)
                {
                    respaFee.PaidTo = paidTo;
                    respaFee.PaidToType = E_RespaFeePaidToType.Other;
                } // otherwise keep the PaidToType as before
            }

            return respaFee;
        }

        private Payment CreatePayment(string amount, bool isPaid, int props)
        {
            Payment payment = new Payment();
            payment.AllowableFhaClosingCostIndicator = ToMismo(LosConvert.GfeItemProps_FhaAllow(props));
            payment.Amount = amount ?? "0.00";
            payment.IncludedInAprIndicator = ToMismo(LosConvert.GfeItemProps_AprAndPdByBorrower(props));
            payment.PaidOutsideOfClosingIndicator = ToMismo(LosConvert.GfeItemProps_Poc(props));

            int paidBy = LosConvert.GfeItemProps_Payer(props);
            if (paidBy == LosConvert.BORR_PAID_OUTOFPOCKET || paidBy == LosConvert.BORR_PAID_FINANCED)
            {
                payment.PaidByType = E_PaymentPaidByType.Buyer;
                if (paidBy == LosConvert.BORR_PAID_FINANCED)
                    payment.FinancedIndicator = E_YNIndicator.Y;
            }
            else if (paidBy == LosConvert.SELLER_PAID)
            {
                payment.PaidByType = E_PaymentPaidByType.Seller;
            }
            else if (paidBy == LosConvert.LENDER_PAID)
            {
                payment.PaidByType = E_PaymentPaidByType.Lender;
            }
            else if (paidBy == LosConvert.BROKER_PAID)
            {
                payment.PaidByType = E_PaymentPaidByType.Broker;
            }
            //payment.Id = null;
            //payment.CollectedByType = null;
            if (LosConvert.GfeItemProps_BF(props))
            {
                payment.IncludedInStateHighCostIndicator = ToMismo(false); // Send "N" if the BF checkbox is selected, else send nothing
                payment.Section32Indicator = ToMismo(false);
            }
            //payment.NetDueAmount = null;
            //payment.PaidByType = null;
            //payment.PaidByTypeThirdPartyName = null;
            //payment.Percent = null;
            //payment.ProcessType = null;
            //payment.Section32Indicator = null;
            return payment;
        }

        private RequiredServiceProvider CreateRequiredServiceProvider()
        {
            RequiredServiceProvider requiredServiceProvider = new RequiredServiceProvider();
            //requiredServiceProvider.Id = null;
            //requiredServiceProvider.City = null;
            //requiredServiceProvider.Name = null;
            //requiredServiceProvider.NatureOfRelationshipDescription = null;
            //requiredServiceProvider.PostalCode = null;
            //requiredServiceProvider.ReferenceIdentifier = null;
            //requiredServiceProvider.State = null;
            //requiredServiceProvider.StreetAddress = null;
            //requiredServiceProvider.StreetAddress2 = null;
            //requiredServiceProvider.TelephoneNumber = null;
            return requiredServiceProvider;
        }

        private TitleHolder CreateTitleHolder()
        {
            TitleHolder titleHolder = new TitleHolder();
            //titleHolder.Id = null;
            //titleHolder.LandTrustType = null;
            //titleHolder.LandTrustTypeOtherDescription = null;
            //titleHolder.Name = null;
            return titleHolder;
        }

        private TransactionDetail CreateTransactionDetail()
        {
            TransactionDetail transactionDetail = new TransactionDetail();
            transactionDetail.BorrowerPaidDiscountPointsTotalAmount = m_dataLoan.sLDiscnt1003_rep;
            transactionDetail.AlterationsImprovementsAndRepairsAmount = m_dataLoan.sAltCost_rep;
            transactionDetail.EstimatedClosingCostsAmount = m_dataLoan.sTotEstCcNoDiscnt1003_rep;
            transactionDetail.MiAndFundingFeeFinancedAmount = m_dataLoanWithGfeArchiveApplied.sFfUfmipFinanced_rep;
            transactionDetail.MiAndFundingFeeTotalAmount = m_dataLoanWithGfeArchiveApplied.sFfUfmip1003_rep;
            transactionDetail.PrepaidItemsEstimatedAmount = m_dataLoan.sTotEstPp1003_rep;
            transactionDetail.PurchasePriceAmount = m_dataLoan.sPurchPrice_rep;

            if (m_dataLoan.sLPurposeT != E_sLPurposeT.Purchase)
            {
                transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount = m_dataLoan.sRefPdOffAmt1003_rep;
            }

            //transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount = m_dataLoan.sRefPdOffAmt1003_rep;
            transactionDetail.SellerPaidClosingCostsAmount = m_dataLoan.sTotCcPbs_rep;

            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit1Desc, m_dataLoan.sOCredit1Amt_rep));
            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit2Desc, m_dataLoan.sOCredit2Amt_rep));
            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit3Desc, m_dataLoan.sOCredit3Amt_rep));
            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit4Desc, m_dataLoan.sOCredit4Amt_rep));
            transactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit5Desc, m_dataLoan.sOCredit5Amt_rep));
            
            //if (!string.IsNullOrEmpty(m_yieldSpreadDiffAmt))
            //{
            //    var yieldSpreadDiffPurchaseCredit = new PurchaseCredit();
            //    yieldSpreadDiffPurchaseCredit.Amount = m_yieldSpreadDiffAmt;
            //    yieldSpreadDiffPurchaseCredit.SourceType = E_PurchaseCreditSourceType.Lender;
            //    yieldSpreadDiffPurchaseCredit.Type = E_PurchaseCreditType.Other;
            //    yieldSpreadDiffPurchaseCredit.TypeOtherDescription = "Interest rate credit";
            //    transactionDetail.PurchaseCreditList.Add(yieldSpreadDiffPurchaseCredit);
            //}
            
            transactionDetail.SalesConcessionAmount = m_dataLoan.sFHASalesConcessions_rep;
            //transactionDetail.Id = null;


            //transactionDetail.FreReserveAmount = null;
            transactionDetail.FreReservesAmount = m_dataLoan.sFredieReservesAmt_rep;

            //transactionDetail.SalesConcessionAmount = null;

            if (m_dataLoan.sLienPosT == E_sLienPosT.First)
            {
                if (m_dataLoan.sIsOFinCreditLineInDrawPeriod)
                {
                    transactionDetail.SubordinateLienAmount = m_dataLoan.sSubFin_rep;
                    transactionDetail.SubordinateLienHelocAmount = m_dataLoan.sConcurSubFin_rep;
                }
                else
                {
                    transactionDetail.SubordinateLienAmount = m_dataLoan.sConcurSubFin_rep;
                }
            }

            if (m_dataLoan.sIsOFinNew)
            {
                transactionDetail.SubordinateLienPurposeType = E_TransactionDetailSubordinateLienPurposeType.Other;
                transactionDetail.SubordinateLienPurposeTypeOtherDescription = "NewLoan";
            }

            return transactionDetail;
        }

        private PurchaseCredit CreatePurchaseCredit(string desc, string amount)
        {
            if (null == amount || "" == amount || "0.00" == amount || null == desc)
                return null;

            PurchaseCredit purchaseCredit = new PurchaseCredit();
            switch (desc.ToLower())
            {
                case "cash deposit on sales contract":
                    purchaseCredit.Type = E_PurchaseCreditType.EarnestMoney;
                    break;
                case "employer assisted housing":
                    purchaseCredit.Type = E_PurchaseCreditType.EmployerAssistedHousing;
                    break;
                case "lease purchase fund":
                    purchaseCredit.Type = E_PurchaseCreditType.LeasePurchaseFund;
                    break;
                case "relocation funds":
                    purchaseCredit.Type = E_PurchaseCreditType.RelocationFunds;
                    break;
                case "seller credit":
                    purchaseCredit.SourceType = E_PurchaseCreditSourceType.PropertySeller;
                    purchaseCredit.Type = E_PurchaseCreditType.Other;
                    purchaseCredit.TypeOtherDescription = desc;
                    break;
                case "lender credit":
                    purchaseCredit.SourceType = E_PurchaseCreditSourceType.Lender;
                    purchaseCredit.Type = E_PurchaseCreditType.Other;
                    purchaseCredit.TypeOtherDescription = desc;
                    break;

                default:
                    purchaseCredit.Type = E_PurchaseCreditType.Other;
                    purchaseCredit.TypeOtherDescription = desc;
                    break;
            }
            purchaseCredit.Amount = amount;
            //purchaseCredit.Id = null;
            return purchaseCredit;
        }

        private List<Borrower> CreateBorrowers(CAppData dataApp, IPreparerFields interviewer)
        {
            List<Borrower> borrowers = new List<Borrower>();
            E_BorrowerModeT prevMode = dataApp.BorrowerModeT;

            // Borrower
            dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            Borrower borr;
            E_aTypeT borrowerType = dataApp.aBTypeT;

            switch (borrowerType)
            {
                case E_aTypeT.Individual:
                    borr = CreateBorrower_Individual(dataApp);
                    break;
                case E_aTypeT.CoSigner:
                    borr = CreateBorrower_CoSigner(dataApp);
                    break;
                case E_aTypeT.NonTitleSpouse:
                    borr = CreateBorrower_NonTitleSpouse(dataApp);
                    break;
                case E_aTypeT.TitleOnly:
                    borr = CreateBorrower_TitleOnly(dataApp);
                    break;
                default:
                    throw new UnhandledEnumException(borrowerType);
            }
            if (borr != null)
            {
                if (borrowerType == E_aTypeT.Individual)
                {
                    borr.SummaryList.Add(new Summary() { AmountType = E_SummaryAmountType.SubtotalLiquidAssetsNotIncludingGift, Amount = dataApp.aAsstLiqTot_rep });
                    borr.SummaryList.Add(new Summary() { AmountType = E_SummaryAmountType.SubtotalNonLiquidAssets, Amount = dataApp.aAsstNonReSolidTot_rep });
                    borr.SummaryList.Add(new Summary() { AmountType = E_SummaryAmountType.SubtotalLiabilitesMonthlyPayment, Amount = dataApp.aLiaMonTot_rep });
                    borr.SummaryList.Add(new Summary() { AmountType = E_SummaryAmountType.TotalLiabilitiesBalance, Amount = dataApp.aLiaBalTot_rep });

                    if (interviewer.IsValid)
                    {
                        borr.ApplicationSignedDate = m_dataLoan.sDocMagicApplicationD_rep;
                    }
                }
                //OPM 90034: if the coborrower is title-only, do not set the borrower's JointAssetBorrowerId.
                if (dataApp.aCTypeT == E_aTypeT.TitleOnly) borr.JointAssetBorrowerId = null;

                borrowers.Add(borr);
            }

            // Coborrower
            dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
            Borrower coborr;
            E_aTypeT coborrowerType = dataApp.aCTypeT;

            switch (coborrowerType)
            {
                case E_aTypeT.Individual:
                    coborr = CreateBorrower_Individual(dataApp);
                    break;
                case E_aTypeT.CoSigner:
                    coborr = CreateBorrower_CoSigner(dataApp);
                    break;
                case E_aTypeT.NonTitleSpouse:
                    coborr = CreateBorrower_NonTitleSpouse(dataApp);
                    break;
                case E_aTypeT.TitleOnly:
                    coborr = CreateBorrower_TitleOnly(dataApp);
                    break;
                default:
                    throw new UnhandledEnumException(coborrowerType);
            }
            if (coborr != null)
                borrowers.Add(coborr);

            dataApp.BorrowerModeT = prevMode;
            return borrowers;
        }

        private Borrower CreateTitleBorrower(int index, TitleBorrower titleBorrower)
        {
            Borrower borrower = new Borrower();
            borrower.BorrowerId = String.Concat("T", index.ToString());
            borrower.DependentCount = "0";
            borrower.JointAssetLiabilityReportingType = E_BorrowerJointAssetLiabilityReportingType.NotJointly;

            borrower.MaritalStatusType = E_BorrowerMaritalStatusType.NotProvided;
            borrower.SchoolingYears = "0";
            borrower.AgeAtApplicationYears = "0";
            borrower.BirthDate = "";
            borrower.FirstName = titleBorrower.FirstNm;
            borrower.HomeTelephoneNumber = "";
            borrower.LastName = titleBorrower.LastNm;
            borrower.MiddleName = titleBorrower.MidNm;
            borrower.NameSuffix = "";
            borrower.PrintPositionType = E_BorrowerPrintPositionType.Undefined;
            borrower.Ssn = titleBorrower.SSNForExport;
            borrower.UnparsedName = String.Concat(titleBorrower.FirstNm, " ", titleBorrower.MidNm, " " ,titleBorrower.LastNm);
            borrower.MailTo = CreateMailTo(titleBorrower);

            if (!string.IsNullOrEmpty(titleBorrower.POA))
            {
                var borrPOA = new PowerOfAttorney();
                borrPOA.UnparsedName = titleBorrower.POA;
                borrower.PowerOfAttorney = borrPOA;
            }

            foreach (string alias in titleBorrower.Aliases)
            {
                if (string.IsNullOrEmpty(alias))
                    continue;
                else
                    borrower.AliasList.Add(CreateAlias(alias));
            }

            borrower.NonPersonEntityIndicator = ToMismo(false);
            borrower.BorrowerNonObligatedIndicator = ToMismo(true);

            var aRelationshipTitleT = titleBorrower.RelationshipTitleT; 
            var dmRelationshipTitleT = ToMismo(aRelationshipTitleT);
            borrower.RelationshipTitleType = dmRelationshipTitleT;

            // If the value in our system doesn't have a docmagic equivalent,
            //   then send it as an Other description.
            if (aRelationshipTitleT != E_aRelationshipTitleT.Other && dmRelationshipTitleT == E_BorrowerRelationshipTitleType.Other)
                borrower.RelationshipTitleTypeOtherDescription = CAppBase.aRelationshipTitleT_Map_rep(aRelationshipTitleT);
            else
                borrower.RelationshipTitleTypeOtherDescription = titleBorrower.RelationshipTitleTOtherDesc;

            //OPM 104257: Add commas between unrelated borrowers and before final relation 
            borrower.RelationshipTitleType = E_BorrowerRelationshipTitleType.Other;

            if (dmRelationshipTitleT != E_BorrowerRelationshipTitleType.Other)
                borrower.RelationshipTitleTypeOtherDescription = CAppBase.aRelationshipTitleT_Map_rep(aRelationshipTitleT);

            return borrower;
        }

        private Borrower CreateBorrower(CAppData dataApp)
        {
            Borrower borrower = new Borrower();
            borrower.BorrowerId = dataApp.aMismoId;

            borrower.JointAssetBorrowerId = dataApp.aSpouseMismoId;
            borrower.DependentCount = string.IsNullOrEmpty(dataApp.aDependNum_rep) ? "0" : dataApp.aDependNum_rep; // Send 0 if it's blank
            borrower.JointAssetLiabilityReportingType = dataApp.aAsstLiaCompletedNotJointly ? E_BorrowerJointAssetLiabilityReportingType.NotJointly : E_BorrowerJointAssetLiabilityReportingType.Jointly;
            borrower.MaritalStatusType = ToMismo(dataApp.aMaritalStatT);
            borrower.SchoolingYears = dataApp.aSchoolYrs_rep;
            borrower.AgeAtApplicationYears = dataApp.aAge_rep;
            borrower.BirthDate = dataApp.aDob_rep;
            borrower.FirstName = dataApp.aFirstNm;
            borrower.HomeTelephoneNumber = dataApp.aHPhone;
            borrower.LastName = dataApp.aLastNm;
            borrower.MiddleName = dataApp.aMidNm;
            borrower.NameSuffix = dataApp.aSuffix;
            borrower.PrintPositionType = ToMismo(dataApp.BorrowerModeT);
            borrower.Ssn = dataApp.aSsn;
            borrower.UnparsedName = dataApp.aNm;
            borrower.MailTo = CreateMailTo(dataApp);

            var aRelationshipTitleT = dataApp.aRelationshipTitleT;
            var dmRelationshipTitleT = ToMismo(aRelationshipTitleT);
            borrower.RelationshipTitleType = dmRelationshipTitleT;

            // If the value in our system doesn't have a docmagic equivalent,
            //   then send it as an Other description.
            if (aRelationshipTitleT != E_aRelationshipTitleT.Other && dmRelationshipTitleT == E_BorrowerRelationshipTitleType.Other)
                borrower.RelationshipTitleTypeOtherDescription = CAppBase.aRelationshipTitleT_Map_rep(aRelationshipTitleT);
            else
                borrower.RelationshipTitleTypeOtherDescription = dataApp.aRelationshipTitleOtherDesc;

            //OPM 104257: Add commas between unrelated borrowers and before final relation 
            borrower.RelationshipTitleType = E_BorrowerRelationshipTitleType.Other;

            if (dmRelationshipTitleT != E_BorrowerRelationshipTitleType.Other)
                borrower.RelationshipTitleTypeOtherDescription = CAppBase.aRelationshipTitleT_Map_rep(aRelationshipTitleT);
            //OPM 115667: Do not add commas after relationships
            //if(dmRelationshipTitleT != E_BorrowerRelationshipTitleType.Undefined)
            //    borrower.RelationshipTitleTypeOtherDescription += ",";

            borrower.ResidenceList.Add(CreateResidence(dataApp.aAddr, dataApp.aCity, dataApp.aState, dataApp.aZip, dataApp.aAddrT, dataApp.aAddrYrs, true));
            borrower.ResidenceList.Add(CreateResidence(dataApp.aPrev1Addr, dataApp.aPrev1City, dataApp.aPrev1State, dataApp.aPrev1Zip, (E_aBAddrT)dataApp.aPrev1AddrT, dataApp.aPrev1AddrYrs, false));
            borrower.ResidenceList.Add(CreateResidence(dataApp.aPrev2Addr, dataApp.aPrev2City, dataApp.aPrev2State, dataApp.aPrev2Zip, (E_aBAddrT)dataApp.aPrev2AddrT, dataApp.aPrev2AddrYrs, false));

            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Base, dataApp.aBaseI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Overtime, dataApp.aOvertimeI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Bonus, dataApp.aBonusesI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Commissions, dataApp.aCommisionI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.DividendsInterest, dataApp.aDividendI_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.NetRentalIncome, dataApp.aNetRentI1003_rep));
            borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow, dataApp.aSpPosCf_rep));
            borrower.Declaration = CreateDeclaration(dataApp);


            foreach (var otherIncome in dataApp.aOtherIncomeList)
            {
                if ((otherIncome.IsForCoBorrower && dataApp.BorrowerModeT == E_BorrowerModeT.Coborrower) || (!otherIncome.IsForCoBorrower && dataApp.BorrowerModeT == E_BorrowerModeT.Borrower))
                {
                    borrower.CurrentIncomeList.Add(CreateCurrentIncome(ToMismo(OtherIncome.Get_aOIDescT(otherIncome.Desc)), dataApp.m_convertLos.ToMoneyString(otherIncome.Amount,FormatDirection.ToRep), otherIncome.Desc));
                }                
            }

            string[] dependentAges = dataApp.aDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':');
            dependentAges.Where(p => !string.IsNullOrEmpty(p)).ToList().ForEach(p => borrower.DependentList.Add(CreateDependent(p)));


            IEmpCollection empCollection = dataApp.aEmpCollection;
            borrower.EmployerList.Add(CreateEmployer(empCollection.GetPrimaryEmp(false)));

            foreach (IRegularEmploymentRecord record in empCollection.GetSubcollection(true, E_EmpGroupT.Previous))
            {
                borrower.EmployerList.Add(CreateEmployer(record));
            }

            borrower.GovernmentMonitoring = CreateGovernmentMonitoring(dataApp);

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                // Only add present housing for Borrower.
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.Rent, dataApp.aPresRent_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, dataApp.aPres1stM_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, dataApp.aPresOFin_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.HazardInsurance, dataApp.aPresHazIns_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.RealEstateTax, dataApp.aPresRealETx_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.MI, dataApp.aPresMIns_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, dataApp.aPresHoAssocDues_rep));
                borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.OtherHousingExpense, dataApp.aPresOHExp_rep));
            }


            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Phone, dataApp.aBusPhone));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Mobile, Mismo.Closing2_6.E_ContactPointType.Phone, dataApp.aCellPhone));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, Mismo.Closing2_6.E_ContactPointType.Fax, dataApp.aFax));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, Mismo.Closing2_6.E_ContactPointType.Email, dataApp.aEmail));
            borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, Mismo.Closing2_6.E_ContactPointType.Phone, dataApp.aHPhone));

            List<string> denialFactors = new List<string>();
            #region Build Credit Denial Factors
            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                if (!string.IsNullOrEmpty(dataApp.aDenialCreditScoreFactor1))
                    denialFactors.Add(dataApp.aDenialCreditScoreFactor1);
                if (!string.IsNullOrEmpty(dataApp.aDenialCreditScoreFactor2))
                    denialFactors.Add(dataApp.aDenialCreditScoreFactor2);
                if (!string.IsNullOrEmpty(dataApp.aDenialCreditScoreFactor3))
                    denialFactors.Add(dataApp.aDenialCreditScoreFactor3);
                if (!string.IsNullOrEmpty(dataApp.aDenialCreditScoreFactor4))
                    denialFactors.Add(dataApp.aDenialCreditScoreFactor4);
                if (dataApp.aDenialIsFactorNumberOfRecentInquiries)
                    denialFactors.Add("Number of recent inquiries on consumer report.");
            }
            else if (dataApp.BorrowerModeT == E_BorrowerModeT.Coborrower)
            {
                if (!string.IsNullOrEmpty(dataApp.aCDenialCreditScoreFactor1))
                    denialFactors.Add(dataApp.aCDenialCreditScoreFactor1);
                if (!string.IsNullOrEmpty(dataApp.aCDenialCreditScoreFactor2))
                    denialFactors.Add(dataApp.aCDenialCreditScoreFactor2);
                if (!string.IsNullOrEmpty(dataApp.aCDenialCreditScoreFactor3))
                    denialFactors.Add(dataApp.aCDenialCreditScoreFactor3);
                if (!string.IsNullOrEmpty(dataApp.aCDenialCreditScoreFactor4))
                    denialFactors.Add(dataApp.aCDenialCreditScoreFactor4);
                if (dataApp.aCDenialIsFactorNumberOfRecentInquiries)
                    denialFactors.Add("Number of recent inquiries on consumer report.");
            }
            #endregion
            borrower.CreditScoreList.Add(CreateCreditScore(
                dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.Equifax, dataApp.aEquifaxScore_rep,
                dataApp.aCrOd_rep, dataApp.aEquifaxFactors,
                m_dataLoan.sEquifaxScoreFrom_rep, m_dataLoan.sEquifaxScoreTo_rep, dataApp.aEquifaxCreatedD_rep, denialFactors));
            borrower.CreditScoreList.Add(CreateCreditScore(
                dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.Experian, dataApp.aExperianScore_rep,
                dataApp.aCrOd_rep, dataApp.aExperianFactors,
                m_dataLoan.sExperianScoreFrom_rep, m_dataLoan.sExperianScoreTo_rep, dataApp.aExperianCreatedD_rep, denialFactors));
            borrower.CreditScoreList.Add(CreateCreditScore(
                dataApp.aMismoId, E_CreditScoreCreditRepositorySourceType.TransUnion, dataApp.aTransUnionScore_rep,
                dataApp.aCrOd_rep, dataApp.aTransUnionFactors,
                m_dataLoan.sTransUnionScoreFrom_rep, m_dataLoan.sTransUnionScoreTo_rep, dataApp.aTransUnionCreatedD_rep, denialFactors));

            //borrower.BorrowerIsCosignerIndicator = null;
            //borrower.BorrowerNonObligatedIndicator = null;
            //borrower.BorrowerNonTitleSpouseIndicator = null;
            //borrower.CreditReportIdentifier = null;
            //borrower.NonPersonEntityIndicator = null;
            //borrower.UrlaBorrowerTotalMonthlyIncomeAmount = null;
            //borrower.UrlaBorrowerTotalOtherIncomeAmount = null;
            //borrower.ApplicationSignedDate = m_dataLoan.Gfe;
            //borrower.RelationshipTitleType = null;
            //borrower.RelationshipTitleTypeOtherDescription = null;
            //borrower.SequenceIdentifier = null;

            PopulateBorrowerAliasList(borrower.AliasList, dataApp);

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower &&
                (m_dataLoan.sLT == E_sLT.FHA || m_dataLoan.sLT == E_sLT.VA)) // Export both FHA and VA - OPM 80346
            {
                borrower.FhaBorrower = CreateFhaBorrower(dataApp);
            }

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower &&
                (m_dataLoan.sLT == E_sLT.FHA || m_dataLoan.sLT == E_sLT.VA))
            {
                borrower.FhaVaBorrower = CreateFhaVaBorrower(dataApp);
            }

            //borrower.SummaryList.Add(CreateSummary());
            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower &&
                (m_dataLoan.sLT == E_sLT.VA || m_dataLoan.sLT == E_sLT.FHA)) // Export both FHA and VA - OPM 80346
            {
                borrower.VaBorrower = CreateVaBorrower(dataApp);
            }
            borrower.NearestLivingRelative = CreateNearestLivingRelative();
            //borrower.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            borrower.PowerOfAttorney = CreatePowerOfAttorney(dataApp);
            return borrower;
        }

        private Borrower CreateBorrower_Individual(CAppData dataApp)
        {
            if (!dataApp.aIsValidNameSsn)
            {
                return null;
            }
            return CreateBorrower(dataApp);
            
        }

        private Borrower CreateBorrower_CoSigner(CAppData dataApp)
        {
            if (string.IsNullOrEmpty(dataApp.aNm))
            {
                return null;
            }
            Borrower borrower = CreateBorrower(dataApp);
            borrower.NonPersonEntityIndicator = ToMismo(false);
            borrower.BorrowerNonObligatedIndicator = ToMismo(false);
            borrower.BorrowerIsCosignerIndicator = ToMismo(true);

            return borrower;
        }

        private Borrower CreateBorrower_TitleOnly(CAppData dataApp)
        {
            if (string.IsNullOrEmpty(dataApp.aNm))
            {
                return null;
            }
            Borrower borrower = CreateBorrower(dataApp);
            borrower.NonPersonEntityIndicator = ToMismo(false);
            borrower.BorrowerNonObligatedIndicator = ToMismo(true);

            return borrower;
        }

        private Borrower CreateBorrower_NonTitleSpouse(CAppData dataApp)
        {
            if (string.IsNullOrEmpty(dataApp.aNm))
            {
                return null;
            }
            Borrower borrower = CreateBorrower(dataApp);
            borrower.NonPersonEntityIndicator = ToMismo(false);
            borrower.BorrowerNonTitleSpouseIndicator = ToMismo(true);

            return borrower;
        }

        private Borrower CreateBorrower_Empty()
        {
            return new Borrower();
        }

        private void PopulateBorrowerAliasList(List<Alias> aliasList, CAppData dataApp)
        {
            // Decide whether we're using borrower or coborrower aliases
            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                aliasList.AddRange(dataApp.aBAliases.Select(name => CreateAlias(name)));
                aliasList.Add(CreateAlias(dataApp.aB4506TNm, true));
            }
            else
            {
                aliasList.AddRange(dataApp.aCAliases.Select(name => CreateAlias(name)));
                aliasList.Add(CreateAlias(dataApp.aC4506TNm, true));
            }
        }

        private E_BorrowerRelationshipTitleType ToMismo(E_aRelationshipTitleT e_aRelationshipTitleT)
        {
            switch (e_aRelationshipTitleT)
            {
                case E_aRelationshipTitleT.LeaveBlank:
                    return E_BorrowerRelationshipTitleType.Undefined;
                case E_aRelationshipTitleT.AHusbandAndWife:
                    return E_BorrowerRelationshipTitleType.AHusbandAndWife;
                case E_aRelationshipTitleT.AMarriedMan:
                    return E_BorrowerRelationshipTitleType.AMarriedMan;
                case E_aRelationshipTitleT.AMarriedPerson:
                    return E_BorrowerRelationshipTitleType.AMarriedPerson;
                case E_aRelationshipTitleT.AMarriedWoman:
                    return E_BorrowerRelationshipTitleType.AMarriedWoman;
                case E_aRelationshipTitleT.AnUnmarriedMan:
                    return E_BorrowerRelationshipTitleType.AnUnmarriedMan;
                case E_aRelationshipTitleT.AnUnmarriedPerson:
                    return E_BorrowerRelationshipTitleType.AnUnmarriedPerson;
                case E_aRelationshipTitleT.AnUnmarriedWoman:
                    return E_BorrowerRelationshipTitleType.AnUnmarriedWoman;
                case E_aRelationshipTitleT.ASingleMan:
                    return E_BorrowerRelationshipTitleType.ASingleMan;
                case E_aRelationshipTitleT.ASinglePerson:
                    return E_BorrowerRelationshipTitleType.ASinglePerson;
                case E_aRelationshipTitleT.ASingleWoman:
                    return E_BorrowerRelationshipTitleType.ASingleWoman;
                case E_aRelationshipTitleT.AWidow:
                    return E_BorrowerRelationshipTitleType.AWidow;
                case E_aRelationshipTitleT.AWidower:
                    return E_BorrowerRelationshipTitleType.AWidower;
                case E_aRelationshipTitleT.AWifeAndHusband:
                    return E_BorrowerRelationshipTitleType.AWifeAndHusband;
                case E_aRelationshipTitleT.HerHusband:
                    return E_BorrowerRelationshipTitleType.HerHusband;
                case E_aRelationshipTitleT.HisWife:
                    return E_BorrowerRelationshipTitleType.HisWife;
                case E_aRelationshipTitleT.HusbandAndWife:
                    return E_BorrowerRelationshipTitleType.HusbandAndWife;
                case E_aRelationshipTitleT.WifeAndHusband:
                    return E_BorrowerRelationshipTitleType.WifeAndHusband;
                case E_aRelationshipTitleT.NotApplicable:
                    return E_BorrowerRelationshipTitleType.NotApplicable;
                case E_aRelationshipTitleT.AMarriedManAsHisSoleAndSeparateProperty: // fields in our system that don't completely map to MISMO fields
                case E_aRelationshipTitleT.AMarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AMarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.WifeAndHusbandAsCommunityProperty:
                case E_aRelationshipTitleT.WifeAndHusbandAsJointTenants:
                case E_aRelationshipTitleT.WifeAndHusbandAsTenantsInCommon:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityProperty:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityPropertyWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenants:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenantsWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsInCommon:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsByTheEntirety:
                case E_aRelationshipTitleT.HusbandAndWifeTenancyByTheEntirety:
                case E_aRelationshipTitleT.HusbandAndWifeAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.GeneralPartner:
                case E_aRelationshipTitleT.DomesticPartners:
                case E_aRelationshipTitleT.ChiefExecutiveOfficer:
                case E_aRelationshipTitleT.PersonalGuarantor:
                case E_aRelationshipTitleT.President:
                case E_aRelationshipTitleT.Secretary:
                case E_aRelationshipTitleT.TenancyByEntirety:
                case E_aRelationshipTitleT.TenantsByTheEntirety:
                case E_aRelationshipTitleT.Treasurer:
                case E_aRelationshipTitleT.Trustee:
                case E_aRelationshipTitleT.VicePresident:
                case E_aRelationshipTitleT.Other:
                    return E_BorrowerRelationshipTitleType.Other;
                default:
                    throw new UnhandledEnumException(e_aRelationshipTitleT);
            }
        }

        private E_CurrentIncomeIncomeType ToMismo(E_aOIDescT e_aOIDescT)
        {
            E_CurrentIncomeIncomeType type = E_CurrentIncomeIncomeType.Undefined;
            switch (e_aOIDescT)
            {
                case E_aOIDescT.Other:
                case E_aOIDescT.CapitalGains:
                case E_aOIDescT.EmploymentRelatedAssets:
                case E_aOIDescT.ForeignIncome:
                case E_aOIDescT.RoyaltyPayment:
                case E_aOIDescT.SeasonalIncome:
                case E_aOIDescT.TemporaryLeave:
                case E_aOIDescT.TipIncome:
                case E_aOIDescT.AccessoryUnitIncome:
                case E_aOIDescT.NonBorrowerHouseholdIncome:
                case E_aOIDescT.DefinedContributionPlan:
                case E_aOIDescT.HousingAllowance:
                case E_aOIDescT.MiscellaneousIncome:
                    type = E_CurrentIncomeIncomeType.OtherTypesOfIncome;
                    break;
                case E_aOIDescT.WorkersCompensation:
                    type = E_CurrentIncomeIncomeType.WorkersCompensation;
                    break;
                case E_aOIDescT.PublicAssistance:
                    type = E_CurrentIncomeIncomeType.PublicAssistance;
                    break;
                case E_aOIDescT.ContractBasis:
                    type = E_CurrentIncomeIncomeType.ContractBasis;
                    break;
                case E_aOIDescT.MilitaryBasePay:
                    type = E_CurrentIncomeIncomeType.MilitaryBasePay;
                    break;
                case E_aOIDescT.MilitaryRationsAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryRationsAllowance;
                    break;
                case E_aOIDescT.MilitaryFlightPay:
                    type = E_CurrentIncomeIncomeType.MilitaryFlightPay;
                    break;
                case E_aOIDescT.MilitaryHazardPay:
                    type = E_CurrentIncomeIncomeType.MilitaryHazardPay;
                    break;
                case E_aOIDescT.MilitaryClothesAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryClothesAllowance;
                    break;
                case E_aOIDescT.MilitaryQuartersAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryQuartersAllowance;
                    break;
                case E_aOIDescT.MilitaryPropPay:
                    type = E_CurrentIncomeIncomeType.MilitaryPropPay;
                    break;
                case E_aOIDescT.MilitaryOverseasPay:
                    type = E_CurrentIncomeIncomeType.MilitaryOverseasPay;
                    break;
                case E_aOIDescT.MilitaryCombatPay:
                    type = E_CurrentIncomeIncomeType.MilitaryCombatPay;
                    break;
                case E_aOIDescT.MilitaryVariableHousingAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryVariableHousingAllowance;
                    break;
                case E_aOIDescT.Alimony:
                case E_aOIDescT.ChildSupport:
                case E_aOIDescT.AlimonyChildSupport:
                    type = E_CurrentIncomeIncomeType.AlimonyChildSupport;
                    break;
                case E_aOIDescT.NotesReceivableInstallment:
                    type = E_CurrentIncomeIncomeType.NotesReceivableInstallment;
                    break;
                case E_aOIDescT.PensionRetirement:
                    type = E_CurrentIncomeIncomeType.Pension;
                    break;
                case E_aOIDescT.Disability:
                case E_aOIDescT.SocialSecurity:
                case E_aOIDescT.SocialSecurityDisability:
                    type = E_CurrentIncomeIncomeType.SocialSecurity;
                    break;
                case E_aOIDescT.RealEstateMortgageDifferential:
                    type = E_CurrentIncomeIncomeType.MortgageDifferential;
                    break;
                case E_aOIDescT.Trust:
                    type = E_CurrentIncomeIncomeType.Trust;
                    break;
                case E_aOIDescT.UnemploymentWelfare:
                    type = E_CurrentIncomeIncomeType.Unemployment;
                    break;
                case E_aOIDescT.AutomobileExpenseAccount:
                    type = E_CurrentIncomeIncomeType.AutomobileExpenseAccount;
                    break;
                case E_aOIDescT.FosterCare:
                    type = E_CurrentIncomeIncomeType.FosterCare;
                    break;
                case E_aOIDescT.VABenefitsNonEducation:
                    type = E_CurrentIncomeIncomeType.VABenefitsNonEducational;
                    break;
                case E_aOIDescT.SubjPropNetCashFlow:
                    type = E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow;
                    break;
                case E_aOIDescT.BoarderIncome:
                    type = E_CurrentIncomeIncomeType.BoarderIncome;
                    break;
                case E_aOIDescT.MortgageCreditCertificate:
                    type = E_CurrentIncomeIncomeType.MortgageCreditCertificate;
                    break;
                case E_aOIDescT.TrailingCoBorrowerIncome:
                    type = E_CurrentIncomeIncomeType.TrailingCoBorrowerIncome;
                    break;
                case E_aOIDescT.HousingChoiceVoucher:
                    type = E_CurrentIncomeIncomeType.PublicAssistance;
                    break;
                default:
                    LogInvalidEnum("Current income type", e_aOIDescT);
                    break;
            }
            return type;
        }

        private Alias CreateAlias(string unparsedName)
        {
            if (string.IsNullOrEmpty(unparsedName))
            {
                return null;
            }

            Alias alias = new Alias();
            //alias.Id = null;
            //alias.AccountIdentifier = null;
            //alias.CreditorName = null;
            //alias.FirstName = null;
            //alias.LastName = null;
            //alias.MiddleName = null;
            //alias.NameSuffix = null;
            //alias.SequenceIdentifier = null;
            //alias.Type = null;
            //alias.TypeOtherDescription = null;
            alias.UnparsedName = unparsedName;
            return alias;
        }

        private Alias CreateAlias(string unparsedName, bool taxReturnIndicator)
        {
            if (string.IsNullOrEmpty(unparsedName))
            {
                return null;
            }

            Alias alias = new Alias();
            alias.UnparsedName = unparsedName;
            alias.TaxReturnIndicator = ToMismo(taxReturnIndicator);
            return alias;
        }

        private MailTo CreateMailTo(TitleBorrower borrower)
        {
            MailTo mailTo = new MailTo();
            mailTo.City = borrower.City;
            //mailTo.Country = null;
            mailTo.PostalCode = borrower.Zip;
            mailTo.State = borrower.State;
            mailTo.StreetAddress = borrower.Address;
            return mailTo;
        }

        private MailTo CreateMailTo(CAppData appData)
        {
            MailTo mailTo = new MailTo();
            //mailTo.AddressSameAsPropertyIndicator = ToMismo(appData.aAddrMailUsePresentAddr); 
            mailTo.City = appData.aCityMail;
            //mailTo.Country = null;
            mailTo.PostalCode = appData.aZipMail;
            mailTo.State = appData.aStateMail;
            mailTo.StreetAddress = appData.aAddrMail;
            return mailTo;
        }

        private Residence CreateResidence(string address, string city, string state, string zip, E_aBAddrT type, string years, bool current)
        {
            if (string.IsNullOrEmpty(address) && string.IsNullOrEmpty(city) && string.IsNullOrEmpty(zip) && string.IsNullOrEmpty(state))
            {
                return null;
            }
            Residence residence = new Residence();
            YearMonthParser parser = new YearMonthParser();
            parser.Parse(years);

            //residence.Id = null;
            residence.BorrowerResidencyBasisType = ToMismo(type);
            residence.BorrowerResidencyDurationMonths = parser.NumberOfMonths.ToString();
            residence.BorrowerResidencyDurationYears = parser.NumberOfYears.ToString();
            residence.BorrowerResidencyType = current ? E_ResidenceBorrowerResidencyType.Current : E_ResidenceBorrowerResidencyType.Prior;
            residence.City = city;
            //residence.Country = null;
            //residence.County = null;
            residence.PostalCode = zip;
            residence.State = state;
            residence.StreetAddress = address;
            //residence.StreetAddress2 = null;
            //residence.Landlord = CreateLandlord();
            return residence;
        }

        private E_RespaFeeGfeAggregationType ToMismo(E_GfeSectionT type)
        {
            switch (type)
            {
                case E_GfeSectionT.LeaveBlank:
                    return E_RespaFeeGfeAggregationType.Undefined;
                case E_GfeSectionT.B1:
                    return E_RespaFeeGfeAggregationType.OurOriginationCharge;
                case E_GfeSectionT.B2:
                    return E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge;
                case E_GfeSectionT.B3:
                    return E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected;
                case E_GfeSectionT.B4:
                    return E_RespaFeeGfeAggregationType.TitleServices;
                case E_GfeSectionT.B5:
                    return E_RespaFeeGfeAggregationType.OwnersTitleInsurance;
                case E_GfeSectionT.B6:
                    return E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor;
                case E_GfeSectionT.B7:
                    return E_RespaFeeGfeAggregationType.GovernmentRecordingCharges;
                case E_GfeSectionT.B8:
                    return E_RespaFeeGfeAggregationType.TransferTaxes;
                case E_GfeSectionT.B9:
                case E_GfeSectionT.B10:
                case E_GfeSectionT.B11:
                    return E_RespaFeeGfeAggregationType.Undefined;
                case E_GfeSectionT.NotApplicable:
                    return E_RespaFeeGfeAggregationType.None;
                default:
                    throw new UnhandledEnumException(type);
            }
        }
        private E_ResidenceBorrowerResidencyBasisType ToMismo(E_aBAddrT type)
        {
            switch (type)
            {
                case E_aBAddrT.Own: return E_ResidenceBorrowerResidencyBasisType.Own;
                case E_aBAddrT.Rent: return E_ResidenceBorrowerResidencyBasisType.Rent;
                case E_aBAddrT.LeaveBlank: return E_ResidenceBorrowerResidencyBasisType.Undefined;
                case E_aBAddrT.LivingRentFree: return E_ResidenceBorrowerResidencyBasisType.LivingRentFree; // OPM 46198
                default:
                    LogInvalidEnum("E_aBAddrT", type);
                    return E_ResidenceBorrowerResidencyBasisType.Undefined;
            }
        }

        private Landlord CreateLandlord()
        {
            Landlord landlord = new Landlord();
            //landlord.Id = null;
            //landlord.City = null;
            //landlord.Name = null;
            //landlord.PostalCode = null;
            //landlord.State = null;
            //landlord.StreetAddress = null;
            //landlord.StreetAddress2 = null;
            //landlord.ContactDetail = CreateContactDetail();
            return landlord;
        }

        private CurrentIncome CreateCurrentIncome(E_CurrentIncomeIncomeType type, string monthlyTotalAmount)
        {
            return CreateCurrentIncome(type, monthlyTotalAmount, "");
        }

        private CurrentIncome CreateCurrentIncome(E_CurrentIncomeIncomeType type, string monthlyTotalAmount, string otherDesc)
        {
            if (monthlyTotalAmount == "" || monthlyTotalAmount == "0.00")
            {
                return null;
            }
            CurrentIncome currentIncome = new CurrentIncome();
            currentIncome.IncomeType = type;
            if (currentIncome.IncomeType == E_CurrentIncomeIncomeType.OtherTypesOfIncome)
            {
                currentIncome.IncomeTypeOtherDescription = otherDesc;
            }
            currentIncome.MonthlyTotalAmount = monthlyTotalAmount;
            //currentIncome.Id = null;
            //currentIncome.IncomeFederalTaxExemptIndicator = null;
            //currentIncome.IncomeType = null;
            //currentIncome.IncomeTypeOtherDescription = null;
            //currentIncome.MonthlyTotalAmount = null;
            return currentIncome;
        }

        // OPM 41047
        private bool DetermineFirstTimeHomeBuyerValue(CAppData dataApp)
        {
            if (m_dataLoan.sLT == E_sLT.FHA || m_dataLoan.sLT == E_sLT.VA)
            {
                return dataApp.aHas1stTimeBuyerTri == E_TriState.Yes;
            }
            else
            {
                return m_dataLoan.sHas1stTimeBuyer;
            }
        }

        private Declaration CreateDeclaration(CAppData dataApp)
        {
            Declaration declaration = new Declaration();
            declaration.AlimonyChildSupportObligationIndicator = ToMismoFromDeclaration(dataApp.aDecAlimony);
            declaration.BankruptcyIndicator = ToMismoFromDeclaration(dataApp.aDecBankrupt);
            declaration.BorrowedDownPaymentIndicator = ToMismoFromDeclaration(dataApp.aDecBorrowing);
            declaration.CoMakerEndorserOfNoteIndicator = ToMismoFromDeclaration(dataApp.aDecEndorser);
            declaration.LoanForeclosureOrJudgementIndicator = ToMismoFromDeclaration(dataApp.aDecObligated);
            declaration.OutstandingJudgementsIndicator = ToMismoFromDeclaration(dataApp.aDecJudgment);
            declaration.PartyToLawsuitIndicator = ToMismoFromDeclaration(dataApp.aDecLawsuit);
            declaration.PresentlyDelinquentIndicator = ToMismoFromDeclaration(dataApp.aDecDelinquent);
            declaration.PropertyForeclosedPastSevenYearsIndicator = ToMismoFromDeclaration(dataApp.aDecForeclosure);
            declaration.BorrowerFirstTimeHomebuyerIndicator = ToMismo(DetermineFirstTimeHomeBuyerValue(dataApp));

            declaration.PriorPropertyTitleType = ToMismo(dataApp.aDecPastOwnedPropTitleT);
            declaration.PriorPropertyUsageType = ToMismo(dataApp.aDecPastOwnedPropT);

            switch (dataApp.aDecPastOwnership)
            {
                case "Y":
                    declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.Yes;
                    break;
                case "N":
                    declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.No;
                    break;
                default:
                    declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.Undefined;
                    break;

            }


            switch (dataApp.aDecOcc)
            {
                case "Y":
                    declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.Yes;
                    break;
                case "N":
                    declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.No;
                    break;
                default:
                    declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.Undefined;
                    break;
            }




            //declaration.Id = null;

            if (dataApp.aDecCitizen.ToUpper() == "Y")
            {
                declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.USCitizen;

            }
            else if (dataApp.aDecResidency.ToUpper() == "Y")
            {
                declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.PermanentResidentAlien;
            }
            else
            {
                declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.NonResidentAlien;
            }

            //declaration.ExplanationList.Add(CreateExplanation());
            return declaration;
        }

        private E_DeclarationPriorPropertyUsageType ToMismo(E_aBDecPastOwnedPropT e_aBDecPastOwnedPropT)
        {
            switch (e_aBDecPastOwnedPropT)
            {
                case E_aBDecPastOwnedPropT.Empty: return E_DeclarationPriorPropertyUsageType.Undefined;
                case E_aBDecPastOwnedPropT.IP: return E_DeclarationPriorPropertyUsageType.Investment;
                case E_aBDecPastOwnedPropT.PR: return E_DeclarationPriorPropertyUsageType.PrimaryResidence;
                case E_aBDecPastOwnedPropT.SH: return E_DeclarationPriorPropertyUsageType.SecondaryResidence;
                default:
                    LogInvalidEnum("E_aBDecPastOwnedPropT", e_aBDecPastOwnedPropT);
                    return E_DeclarationPriorPropertyUsageType.Undefined;
            }
        }

        private E_DeclarationPriorPropertyTitleType ToMismo(E_aBDecPastOwnedPropTitleT e_aBDecPastOwnedPropTitleT)
        {
            switch (e_aBDecPastOwnedPropTitleT)
            {
                case E_aBDecPastOwnedPropTitleT.Empty: return E_DeclarationPriorPropertyTitleType.Undefined;
                case E_aBDecPastOwnedPropTitleT.O: return E_DeclarationPriorPropertyTitleType.JointWithOtherThanSpouse;
                case E_aBDecPastOwnedPropTitleT.S: return E_DeclarationPriorPropertyTitleType.Sole;
                case E_aBDecPastOwnedPropTitleT.SP: return E_DeclarationPriorPropertyTitleType.JointWithSpouse;
                default:
                    LogInvalidEnum("E_aBDecPastOwnedPropTitleT", e_aBDecPastOwnedPropTitleT);
                    return E_DeclarationPriorPropertyTitleType.Undefined;
            }
        }

        private E_YNIndicator ToMismoFromDeclaration(string p)
        {
            switch (p.ToUpper())
            {
                case "Y": return E_YNIndicator.Y;
                case "N": return E_YNIndicator.N;
                default: return E_YNIndicator.Undefined;
            }
        }

        private Explanation CreateExplanation()
        {
            Explanation explanation = new Explanation();
            //explanation.Id = null;
            //explanation.Type = E_ExplanationType.
            //explanation.Type = E_ExplanationType.R
            return explanation;
        }

        private Dependent CreateDependent(string years)
        {
            Dependent dependent = new Dependent();
            //dependent.Id = null;
            dependent.AgeYears = years;
            return dependent;
        }
        private Employer CreateEmployer(IRegularEmploymentRecord record)
        {
            if (null == record)
                return null;

            Employer employer = new Employer();
            employer.Name = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.PostalCode = record.EmplrZip;
            employer.TelephoneNumber = record.EmplrBusPhone;
            employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            employer.EmploymentPositionDescription = record.JobTitle;
            employer.EmploymentPrimaryIndicator = ToMismo(record.IsPrimaryEmp);
            employer.IncomeEmploymentMonthlyAmount = record.MonI_rep;
            employer.PreviousEmploymentEndDate = record.EmplmtEndD_rep;
            employer.PreviousEmploymentStartDate = record.EmplmtStartD_rep;
            employer.EmploymentCurrentIndicator = record.IsCurrent ? E_YNIndicator.Y : E_YNIndicator.N;
            return employer;

        }
        private Employer CreateEmployer(IPrimaryEmploymentRecord record)
        {
            if (record == null)
            {
                return null;
            }
            Employer employer = new Employer();
            employer.Name = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.PostalCode = record.EmplrZip;
            employer.TelephoneNumber = record.EmplrBusPhone;
            employer.CurrentEmploymentMonthsOnJob = record.EmplmtLenInMonths_rep;
            employer.CurrentEmploymentYearsOnJob = record.EmplmtLenInYrs_rep;
            employer.CurrentEmploymentTimeInLineOfWorkYears = record.ProfLen_rep;
            employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            employer.EmploymentCurrentIndicator = E_YNIndicator.Y;
            employer.EmploymentPositionDescription = record.JobTitle;
            employer.EmploymentPrimaryIndicator = E_YNIndicator.Y;
            //            employer.IncomeEmploymentMonthlyAmount;
            //            employer.PreviousEmploymentEndDate;
            //            employer.PreviousEmploymentStartDate;
            //employer.Id = null;

            //employer.CurrentEmploymentStartDate = null;
            //employer.EmployedAbroadIndicator = null;
            //employer.EmploymentBorrowerSelfEmployedIndicator = null;
            //employer.EmploymentCurrentIndicator = null;
            //employer.EmploymentPositionDescription = null;
            //employer.EmploymentPrimaryIndicator = null;
            //employer.IncomeEmploymentMonthlyAmount = null;
            //employer.PreviousEmploymentEndDate = null;
            //employer.PreviousEmploymentStartDate = null;
            //employer.SpecialBorrowerEmployerRelationshipType = null;
            //employer.SpecialBorrowerEmployerRelationshipTypeOtherDescription = null;

            //employer.Country = null;

            return employer;
        }

        private FhaVaBorrower CreateFhaVaBorrower(CAppData dataApp)
        {
            FhaVaBorrower fhaVaBorrower = new FhaVaBorrower();
            //fhaVaBorrower.Id = null;
            //fhaVaBorrower.CaivrsIdentifier = null;
            //fhaVaBorrower.FnmBankruptcyCount = null;
            //fhaVaBorrower.FnmBorrowerCreditRating = null;
            //fhaVaBorrower.FnmCreditReportScoreType = null;
            //fhaVaBorrower.FnmForeclosureCount = null;

            fhaVaBorrower.VeteranStatusIndicator = ToMismo(dataApp.aIsVeteran);

            if (dataApp.aFHABorrCertInformedPropValAwareAtContractSigning)
                fhaVaBorrower.CertificationSalesPriceExceedsAppraisedValueType = E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType.A;
            else if (dataApp.aFHABorrCertInformedPropValNotAwareAtContractSigning)
                fhaVaBorrower.CertificationSalesPriceExceedsAppraisedValueType = E_FhaVaBorrowerCertificationSalesPriceExceedsAppraisedValueType.B;
            return fhaVaBorrower;
        }

        private GovernmentMonitoring CreateGovernmentMonitoring(CAppData dataApp)
        {
            GovernmentMonitoring governmentMonitoring = new GovernmentMonitoring();
            governmentMonitoring.HmdaEthnicityType = ToMismo(dataApp.aHispanicTFallback);
            governmentMonitoring.GenderType = ToMismo(dataApp.aGenderFallback);
            governmentMonitoring.RaceNationalOriginRefusalIndicator = ToMismo(dataApp.aNoFurnish);

            if ((dataApp.aIsAmericanIndian))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.AmericanIndianOrAlaskaNative));

            if ((dataApp.aIsAsian))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.Asian));

            if ((dataApp.aIsBlack))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.BlackOrAfricanAmerican));

            if ((dataApp.aIsPacificIslander))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.NativeHawaiianOrOtherPacificIslander));

            if ((dataApp.aIsWhite))
                governmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.White));

            //governmentMonitoring.Id = null;


            //governmentMonitoring.OtherRaceNationalOriginDescription = null;

            //governmentMonitoring.RaceNationalOriginType = null;

            return governmentMonitoring;
        }

        private E_GovernmentMonitoringGenderType ToMismo(E_GenderT e_GenderT)
        {
            switch (e_GenderT)
            {
                case E_GenderT.Female: return E_GovernmentMonitoringGenderType.Female;
                case E_GenderT.Male: return E_GovernmentMonitoringGenderType.Male;
                case E_GenderT.LeaveBlank: return E_GovernmentMonitoringGenderType.Undefined;
                case E_GenderT.NA: return E_GovernmentMonitoringGenderType.NotApplicable;
                case E_GenderT.Unfurnished: return E_GovernmentMonitoringGenderType.InformationNotProvidedUnknown;
                default:
                    LogInvalidEnum("E_GenderT", e_GenderT);
                    return E_GovernmentMonitoringGenderType.Undefined;
            }
        }

        private E_GovernmentMonitoringHmdaEthnicityType ToMismo(E_aHispanicT e_aHispanicT)
        {
            switch (e_aHispanicT)
            {
                case E_aHispanicT.Hispanic: return E_GovernmentMonitoringHmdaEthnicityType.HispanicOrLatino;
                case E_aHispanicT.LeaveBlank: return E_GovernmentMonitoringHmdaEthnicityType.Undefined;
                case E_aHispanicT.NotHispanic: return E_GovernmentMonitoringHmdaEthnicityType.NotHispanicOrLatino;
                default:
                    LogInvalidEnum("E_aHispanicT", e_aHispanicT);
                    return E_GovernmentMonitoringHmdaEthnicityType.Undefined;
            }
        }

        private HmdaRace CreateHmdaRace(E_HmdaRaceType type)
        {
            HmdaRace hmdaRace = new HmdaRace();
            //hmdaRace.Id = null;
            hmdaRace.Type = type;
            return hmdaRace;
        }

        private PresentHousingExpense CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType type, string amount)
        {
            if ("" == amount || "0.00" == amount || "$0.00" == amount)
                return null;

            PresentHousingExpense presentHousingExpense = new PresentHousingExpense();
            //presentHousingExpense.Id = null;
            presentHousingExpense.HousingExpenseType = type;
            //presentHousingExpense.HousingExpenseTypeOtherDescription = null;
            presentHousingExpense.PaymentAmount = amount;
            return presentHousingExpense;
        }

        private Summary CreateSummary()
        {
            Summary summary = new Summary();
            //summary.Id = null;
            //summary.Amount = null;
            //summary.AmountType = null;
            //summary.AmountTypeOtherDescription = null;
            return summary;
        }

        private Mismo.Closing2_6.VaBorrower CreateVaBorrower(CAppData dataApp)
        {
            Mismo.Closing2_6.VaBorrower vaBorrower = new Mismo.Closing2_6.VaBorrower();
            //vaBorrower.Id = null;
            //vaBorrower.VaCoBorrowerNonTaxableIncomeAmount = null;
            //vaBorrower.VaCoBorrowerTaxableIncomeAmount = null;
            //vaBorrower.VaFederalTaxAmount = null;
            //vaBorrower.VaLocalTaxAmount = null;
            //vaBorrower.VaPrimaryBorrowerNonTaxableIncomeAmount = null;
            //vaBorrower.VaPrimaryBorrowerTaxableIncomeAmount = null;
            //vaBorrower.VaSocialSecurityTaxAmount = null;
            //vaBorrower.VaStateTaxAmount = null;

            if (m_dataLoan.sLT == E_sLT.VA)
            {
                vaBorrower.MilitaryServiceList.AddRange(CreateMilitaryServices(dataApp));
                vaBorrower.PreviousVaLoanList.AddRange(CreatePreviousVaLoans(dataApp));
            }

            // OPM 80346: If multiples are selected, send the one closest to the top of the FHA addendum -m.p.
            if (dataApp.aFHABorrCertOccIsAsHome)
            {
                vaBorrower.CertificationOccupancyType = E_VaBorrowerCertificationOccupancyType.A;
            }
            else if (dataApp.aFHABorrCertOccIsAsHomeForActiveSpouse)
            {
                vaBorrower.CertificationOccupancyType = E_VaBorrowerCertificationOccupancyType.B;
            }
            else if (dataApp.aFHABorrCertOccIsAsHomePrev)
            {
                vaBorrower.CertificationOccupancyType = E_VaBorrowerCertificationOccupancyType.C;
            }
            else if (dataApp.aFHABorrCertOccIsAsHomePrevForActiveSpouse)
            {
                vaBorrower.CertificationOccupancyType = E_VaBorrowerCertificationOccupancyType.D;
            }

            return vaBorrower;
        }

        private List<Mismo.Closing2_6.MilitaryService> CreateMilitaryServices(CAppData dataApp)
        {
            var services = new List<Mismo.Closing2_6.MilitaryService>();
            bool isActiveDuty = dataApp.aActiveMilitaryStatTri == E_TriState.Yes;

            services.Add(CreateMilitaryService_ActiveService(dataApp.aVaServ1BranchNum, dataApp.aVaServ1StartD_rep, dataApp.aVaServ1EndD_rep, dataApp.aVaServ1Num, isActiveDuty, 1, dataApp.aMismoId));
            services.Add(CreateMilitaryService_ActiveService(dataApp.aVaServ2BranchNum, dataApp.aVaServ2StartD_rep, dataApp.aVaServ2EndD_rep, dataApp.aVaServ2Num, isActiveDuty, 2, dataApp.aMismoId));
            services.Add(CreateMilitaryService_ActiveService(dataApp.aVaServ3BranchNum, dataApp.aVaServ3StartD_rep, dataApp.aVaServ3EndD_rep, dataApp.aVaServ3Num, isActiveDuty, 3, dataApp.aMismoId));
            services.Add(CreateMilitaryService_ReserveNationalGuard(dataApp.aVaServ4BranchNum, dataApp.aVaServ4StartD_rep, dataApp.aVaServ4EndD_rep, dataApp.aVaServ4Num, isActiveDuty, 4, dataApp.aMismoId));
            services.Add(CreateMilitaryService_ReserveNationalGuard(dataApp.aVaServ5BranchNum, dataApp.aVaServ5StartD_rep, dataApp.aVaServ5EndD_rep, dataApp.aVaServ5Num, isActiveDuty, 5, dataApp.aMismoId));
            services.Add(CreateMilitaryService_ReserveNationalGuard(dataApp.aVaServ6BranchNum, dataApp.aVaServ6StartD_rep, dataApp.aVaServ6EndD_rep, dataApp.aVaServ6Num, isActiveDuty, 6, dataApp.aMismoId));
            services.Add(CreateMilitaryService_ReserveNationalGuard(dataApp.aVaServ7BranchNum, dataApp.aVaServ7StartD_rep, dataApp.aVaServ7EndD_rep, dataApp.aVaServ7Num, isActiveDuty, 7, dataApp.aMismoId));

            if(!string.IsNullOrEmpty(dataApp.aVaServedAltName))
            {
                services.First().ServedAsName = dataApp.aVaServedAltName;
            }

            return services;
        }

        private Mismo.Closing2_6.MilitaryService CreateMilitaryService_ActiveService(string branch, string startD, string endD, string num, bool isActiveDuty, int rowNumber, string borrowerId)
        {
            Mismo.Closing2_6.MilitaryService service = new Mismo.Closing2_6.MilitaryService();

            service.MilitaryBranchType = ToMismo_ServiceBranch(branch);
            if(isActiveDuty)
                service.MilitaryStatusType = E_MilitaryServiceMilitaryStatusType.ActiveDuty;
            service.FromDate = startD;
            service.ToDate = endD;

            service.NumberIdentifier = num;
            service.Id = borrowerId+"_M"+rowNumber; //Create ID to match LOSDATA for linking

            return service;
        }
        private Mismo.Closing2_6.MilitaryService CreateMilitaryService_ReserveNationalGuard(string branch, string startD, string endD, string num, bool isActiveDuty, int rowNumber, string borrowerId)
        {
            Mismo.Closing2_6.MilitaryService service = new Mismo.Closing2_6.MilitaryService();

            service.MilitaryBranchType = ToMismo_ReserveNationalGuard(branch);
            if (isActiveDuty)
                service.MilitaryStatusType = E_MilitaryServiceMilitaryStatusType.ActiveDuty;
            service.FromDate = startD;
            service.ToDate = endD;


            service.NumberIdentifier = num;
            service.Id = borrowerId + "_M" + rowNumber; //Create ID to match LOSDATA for linking

            return service;
        }

        private E_MilitaryServiceMilitaryBranchType ToMismo_ServiceBranch(string branch)
        {
            switch (branch)
            {
                case "Army":
                    return E_MilitaryServiceMilitaryBranchType.Army;
                case "Navy":
                    return E_MilitaryServiceMilitaryBranchType.Navy;
                case "AirForce":
                    return E_MilitaryServiceMilitaryBranchType.AirForce;
                case "Marines":
                    return E_MilitaryServiceMilitaryBranchType.Marines;
                default:
                    return E_MilitaryServiceMilitaryBranchType.Undefined;
            }
        }
        private E_MilitaryServiceMilitaryBranchType ToMismo_ReserveNationalGuard(string branch)
        {
            switch (branch)
            {
                case "AirNationalGuard":
                    return E_MilitaryServiceMilitaryBranchType.AirNationalGuard;
                case "ArmyNationalGuard":
                    return E_MilitaryServiceMilitaryBranchType.ArmyNationalGuard;
                case "ArmyReserves":
                    return E_MilitaryServiceMilitaryBranchType.ArmyReserves;
                case "CoastGuard":
                    return E_MilitaryServiceMilitaryBranchType.CoastGuard;
                case "MarinesReserves":
                    return E_MilitaryServiceMilitaryBranchType.MarinesReserves;
                case "NavyReserves":
                    return E_MilitaryServiceMilitaryBranchType.NavyReserves;
                default:
                    return E_MilitaryServiceMilitaryBranchType.Undefined;
            }
        }

        private E_MilitaryServiceMilitaryStatusType ToMismo(E_aVaMilitaryStatT aVaMilitaryStatT)
        {
            switch (aVaMilitaryStatT)
            {
                case E_aVaMilitaryStatT.LeaveBlank:
                    return E_MilitaryServiceMilitaryStatusType.Undefined;
                case E_aVaMilitaryStatT.SeparatedFromService:
                    return E_MilitaryServiceMilitaryStatusType.Separated;
                case E_aVaMilitaryStatT.InService:
                    return E_MilitaryServiceMilitaryStatusType.ActiveDuty;
                default:
                    throw new UnhandledEnumException(aVaMilitaryStatT);
            }
        }

        private List<PreviousVaLoan> CreatePreviousVaLoans(CAppData dataApp)
        {
            var loans = new List<PreviousVaLoan>();
            IVaPastLoanCollection collection = dataApp.aVaPastLCollection;

            for (int i = 0; i < 3 && i < collection.CountRegular; i++)
            {
                IVaPastLoan record = collection.GetRegularRecordAt(i);
                PreviousVaLoan previousVaLoan = new PreviousVaLoan();
                switch(dataApp.aVaOwnOtherVaLoanT)
                {
                    case E_aVaOwnOtherVaLoanT.Yes:
                        previousVaLoan.CurrentlyOwnPropertyIndicator = E_YNIndicator.Y;
                        break;
                    case E_aVaOwnOtherVaLoanT.No:
                        previousVaLoan.CurrentlyOwnPropertyIndicator = E_YNIndicator.N;
                        break;
                    default:
                        previousVaLoan.CurrentlyOwnPropertyIndicator = E_YNIndicator.Undefined;
                        break;
                }
                previousVaLoan.NoteDate = record.DateOfLoan;
                previousVaLoan.AddressList.Add(new Address() { StreetAddress = record.StAddr, City = record.CityState });
                if (m_dataLoan.sLT == E_sLT.VA && m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                {
                    previousVaLoan.AgencyCaseIdentifier = m_dataLoan.sVaIrrrlsUsedOnlyPdInFullLNum;
                }
                loans.Add(previousVaLoan);
            }

            return loans;
        }
        private FhaBorrower CreateFhaBorrower(CAppData dataApp)
        {
            FhaBorrower fhaBorrower = new FhaBorrower();
            //fhaBorrower.Id = null;

            fhaBorrower.CertificationLeadPaintIndicator = ToMismo(dataApp.aFHABorrCertReceivedLeadPaintPoisonInfoTri);

            fhaBorrower.CertificationOriginalMortgageAmount = dataApp.aFHABorrCertOtherPropOrigMAmt_rep;
            fhaBorrower.CertificationOwn4OrMoreDwellingsIndicator = ToMismo(dataApp.aFHABorrCertOwnMoreThan4DwellingsTri);
            fhaBorrower.CertificationOwnOtherPropertyIndicator = ToMismo(dataApp.aFHABorrCertOwnOrSoldOtherFHAPropTri);
            fhaBorrower.CertificationPropertySoldCity = dataApp.aFHABorrCertOtherPropCity;
            fhaBorrower.CertificationPropertySoldPostalCode = dataApp.aFHABorrCertOtherPropZip;
            fhaBorrower.CertificationPropertySoldState = dataApp.aFHABorrCertOtherPropState;
            fhaBorrower.CertificationPropertySoldStreetAddress = dataApp.aFHABorrCertOtherPropStAddr;
            fhaBorrower.CertificationPropertyToBeSoldIndicator = ToMismo(dataApp.aFHABorrCertOtherPropToBeSoldTri);
            fhaBorrower.CertificationRentalIndicator = ToMismo(dataApp.aFHABorrCertOtherPropCoveredByThisLoanTri);

            fhaBorrower.CertificationSalesPriceAmount = dataApp.aFHABorrCertOtherPropSalesPrice_rep;
            return fhaBorrower;
        }

        private E_YNIndicator ToMismo(E_TriState triState)
        {
            switch (triState)
            {
                case E_TriState.Blank: return E_YNIndicator.Undefined;
                case E_TriState.Yes: return E_YNIndicator.Y;
                case E_TriState.No: return E_YNIndicator.N;
                default:
                    return E_YNIndicator.Undefined;
            }
        }

        private NearestLivingRelative CreateNearestLivingRelative()
        {
            NearestLivingRelative nearestLivingRelative = new NearestLivingRelative();
            //nearestLivingRelative.Id = null;
            nearestLivingRelative.City = m_dataLoan.sFHAPropImprovBorrRelativeCity;
            nearestLivingRelative.Name = m_dataLoan.sFHAPropImprovBorrRelativeNm;
            nearestLivingRelative.PostalCode = m_dataLoan.sFHAPropImprovBorrRelativeZip;
            nearestLivingRelative.RelationshipDescription = m_dataLoan.sFHAPropImprovBorrRelativeRelationship;
            nearestLivingRelative.State = m_dataLoan.sFHAPropImprovBorrRelativeState;
            nearestLivingRelative.StreetAddress = m_dataLoan.sFHAPropImprovBorrRelativeStreetAddress;
            //nearestLivingRelative.StreetAddress2 = null;
            nearestLivingRelative.TelephoneNumber = m_dataLoan.sFHAPropImprovBorrRelativePhone;
            return nearestLivingRelative;
        }

        private PowerOfAttorney CreatePowerOfAttorney(CAppData dataApp)
        {
            string name = dataApp.BorrowerModeT == E_BorrowerModeT.Borrower ? dataApp.aBPowerOfAttorneyNm : dataApp.aCPowerOfAttorneyNm;
            if (string.IsNullOrEmpty(name))
                return null;

            PowerOfAttorney powerOfAttorney = new PowerOfAttorney();
            //powerOfAttorney.Id = null;
            //powerOfAttorney.SigningCapacityTextDescription = null;
            //powerOfAttorney.TitleDescription = null;
            powerOfAttorney.UnparsedName = name;
            return powerOfAttorney;
        }

        private Mismo.Closing2_6.CreditScore CreateCreditScore(string borrowerId, E_CreditScoreCreditRepositorySourceType respository, string score, string aCrOd, string factors, string minimumValue, string maximumValue, string date, IEnumerable<string> additionalFactors)
        {
            if (string.IsNullOrEmpty(score))
                return null;

            Mismo.Closing2_6.CreditScore creditScore = new Mismo.Closing2_6.CreditScore();
            creditScore.CreditScoreId = borrowerId + "_" + respository.ToString();
            //creditScore.CreditReportIdentifier = null;
            creditScore.CreditRepositorySourceType = respository;
            //creditScore.CreditRepositorySourceTypeOtherDescription = null;
            creditScore.Date = aCrOd;
            //creditScore.ExclusionReasonType = null;
            //creditScore.FactaInquiriesIndicator = null;
            //creditScore.ModelNameType = null;
            //creditScore.ModelNameTypeOtherDescription = null;
            creditScore.Value = score;
            if ("" != factors)
            {
                string[] parts = factors.Split('\n');
                foreach (string o in parts)
                {
                    creditScore.FactorList.Add(CreateFactor(o));
                }
            }
            if (additionalFactors != null)
                foreach (string o in additionalFactors)
                {
                    creditScore.FactorList.Add(CreateFactor(o));
                }
            creditScore.MinimumValue = minimumValue;
            creditScore.MaximumValue = maximumValue;
            creditScore.Date = date;
            return creditScore;
        }

        private Factor CreateFactor(string text)
        {
            if (string.IsNullOrEmpty(text))
                return null;

            text = text.TrimEnd('\r', '\n');

            Factor factor = new Factor();
            //factor.Id = null;
            //factor.Code = null;
            factor.Text = text;
            return factor;
        }

        private InvestorFeature CreateInvestorFeature()
        {
            InvestorFeature investorFeature = new InvestorFeature();
            //investorFeature.Id = null;
            //investorFeature.CategoryName = null;
            //investorFeature.Description = null;
            //investorFeature.Identifier = null;
            //investorFeature.Name = null;
            return investorFeature;
        }

        private LoanOriginationSystem CreateLoanOriginationSystem()
        {
            LoanOriginationSystem loanOriginationSystem = new LoanOriginationSystem();
            //loanOriginationSystem.Id = null;
            //loanOriginationSystem.LoanIdentifier = null;
            //loanOriginationSystem.Name = null;
            //loanOriginationSystem.VendorIdentifier = null;
            //loanOriginationSystem.VersionIdentifier = null;
            return loanOriginationSystem;
        }

        private LoanUnderwriting CreateLoanUnderwriting(CAppData dataApp)
        {
            LoanUnderwriting loanUnderwriting = new LoanUnderwriting();
            //loanUnderwriting.LoanUnderwritingId = null;
            //loanUnderwriting.AgencyProgramDescription = null;
            //loanUnderwriting.AutomatedUnderwritingEvaluationStatusDescription = null;
            //loanUnderwriting.AutomatedUnderwritingProcessDescription = null;

            if (m_dataLoan.sLT == E_sLT.VA)
            {
                loanUnderwriting.AutomatedUnderwritingRecommendationDescription = ToMismo(m_dataLoan.sVaRiskT);
                loanUnderwriting.TotalDebtExpenseRatioPercent = dataApp.aVaRatio_rep;
            }
            
            //loanUnderwriting.AutomatedUnderwritingSystemName = null;
            if (m_dataLoan.sFHARatedAcceptedByTotalScorecard)
            {
                loanUnderwriting.AutomatedUnderwritingSystemResultValue = "AA";
                loanUnderwriting.LoanUnderwriterName = m_dataLoan.sFHAAddendumMortgageeName;
            }
            else if (m_dataLoan.sFHARatedReferByTotalScorecard)
            {
                loanUnderwriting.AutomatedUnderwritingSystemResultValue = "Refer";
                loanUnderwriting.LoanUnderwriterName = m_dataLoan.sFHAAddendumUnderwriterName;
            }
            
            //loanUnderwriting.ContractUnderwritingIndicator = null;
            //loanUnderwriting.HousingExpenseRatioPercent = null;
            //loanUnderwriting.LoanManualUnderwritingIndicator = null;
            //loanUnderwriting.LoanProspectorCreditRiskClassificationDescription = null;
            //loanUnderwriting.LoanProspectorDocumentationClassificationDescription = null;
            //loanUnderwriting.LoanProspectorRiskGradeAssignedDescription = null;

            //loanUnderwriting.CaseIdentifier = null;
            //loanUnderwriting.DecisionDatetime = null;
            //loanUnderwriting.InvestorGuidelinesIndicator = null;
            //loanUnderwriting.MethodVersionIdentifier = null;
            //loanUnderwriting.OrganizationName = null;
            //loanUnderwriting.SubmitterType = null;
            //loanUnderwriting.SubmitterTypeOtherDescription = null;
            return loanUnderwriting;
        }

        private string ToMismo(E_sVaRiskT sVaRiskT)
        {
            switch (sVaRiskT)
            {
                case E_sVaRiskT.LeaveBlank:
                    return string.Empty;
                case E_sVaRiskT.Approve:
                    return "Approve";
                case E_sVaRiskT.Refer:
                    return "Refer";
                default:
                    throw new UnhandledEnumException(sVaRiskT);
            }
        }

        private RelatedLoan CreateRelatedLoan()
        {
            RelatedLoan relatedLoan = new RelatedLoan();

            //relatedLoan.MortgageType;
            //relatedLoan.LoanAmortizationType;
            //relatedLoan.OtherAmortizationTypeDescription;
            switch (m_dataLoan.sVaPriorLoanT)
            {
                case E_sVaPriorLoanT.LeaveBlank:
                    break;
                case E_sVaPriorLoanT.FhaFixed:
                    relatedLoan.MortgageType = E_RelatedLoanMortgageType.FHA;
                    relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.Fixed;
                    break;
                case E_sVaPriorLoanT.FhaArm:
                    relatedLoan.MortgageType = E_RelatedLoanMortgageType.FHA;
                    relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.AdjustableRate;
                    break;
                case E_sVaPriorLoanT.ConventionalFixed:
                    relatedLoan.MortgageType = E_RelatedLoanMortgageType.Conventional;
                    relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.Fixed;
                    break;
                case E_sVaPriorLoanT.ConventionalArm:
                    relatedLoan.MortgageType = E_RelatedLoanMortgageType.Conventional;
                    relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.AdjustableRate;
                    break;
                case E_sVaPriorLoanT.ConventionalInterestOnly:
                    relatedLoan.MortgageType = E_RelatedLoanMortgageType.Conventional;
                    relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.OtherAmortizationType;
                    relatedLoan.OtherAmortizationTypeDescription = "Interest Only";
                    break;
                case E_sVaPriorLoanT.VaFixed:
                    relatedLoan.MortgageType = E_RelatedLoanMortgageType.VA;
                    relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.Fixed;
                    break;
                case E_sVaPriorLoanT.VaArm:
                    relatedLoan.MortgageType = E_RelatedLoanMortgageType.VA;
                    relatedLoan.LoanAmortizationType = E_RelatedLoanLoanAmortizationType.AdjustableRate;
                    break;
                case E_sVaPriorLoanT.Other:
                    relatedLoan.MortgageType = E_RelatedLoanMortgageType.Other;
                    break;
                default:
                    throw new UnhandledEnumException(m_dataLoan.sVaPriorLoanT);
            }

            //relatedLoan.RelatedLoanId = null;
            //relatedLoan.BalloonIndicator = null;
            //relatedLoan.HelocMaximumBalanceAmount = null;
            //relatedLoan.InvestorReoPropertyIdentifier = null;
            //relatedLoan.LienHolderCity = null;
            //relatedLoan.LienHolderCountry = null;
            //relatedLoan.LienHolderPostalCode = null;
            //relatedLoan.LienHolderSameAsSubjectLoanIndicator = null;
            //relatedLoan.LienHolderState = null;
            //relatedLoan.LienHolderStreetAddress = null;
            //relatedLoan.LienHolderStreetAddress2 = null;
            //relatedLoan.LienHolderType = null;
            //relatedLoan.LienHolderTypeOtherDescription = null;
            //relatedLoan.LienHolderUnparsedName = null;
            //relatedLoan.LienPriorityType = null;
            //relatedLoan.LienPriorityTypeOtherDescription = null;
            //relatedLoan.LoanAllInPricePercent = null;
            //relatedLoan.LoanAmortizationType = null;
            //relatedLoan.LoanOriginalMaturityTermMonths = null;
            //relatedLoan.MortgageType = null;
            //relatedLoan.NegativeAmortizationType = null;
            //relatedLoan.NoteDate = null;
            //relatedLoan.OriginalLoanAmount = null;
            //relatedLoan.OtherAmortizationTypeDescription = null;
            //relatedLoan.OtherMortgageTypeDescription = null;
            //relatedLoan.RelatedInvestorLoanIdentifier = null;
            //relatedLoan.RelatedLoanFinancingSourceType = null;
            //relatedLoan.RelatedLoanFinancingSourceTypeOtherDescription = null;
            //relatedLoan.RelatedLoanInvestorType = null;
            //relatedLoan.RelatedLoanInvestorTypeOtherDescription = null;
            //relatedLoan.RelatedLoanRelationshipType = null;
            //relatedLoan.RelatedLoanRelationshipTypeOtherDescription = null;
            //relatedLoan.RelatedLoanUpbAmount = null;
            //relatedLoan.ScheduledFirstPaymentDate = null;
            return relatedLoan;
        }

        private UrlaTotal CreateUrlaTotal()
        {
            UrlaTotal urlaTotal = new UrlaTotal();
            //urlaTotal.Id = null;
            //urlaTotal.BorrowerId = null;
            //urlaTotal.UrlaSubtotalLiquidAssetsAmount = null;
            //urlaTotal.AssetsAmount = null;
            //urlaTotal.BaseIncomeAmount = null;
            //urlaTotal.BonusIncomeAmount = null;
            urlaTotal.CashFromToBorrowerAmount = m_dataLoan.sTransNetCash_rep;
            //urlaTotal.CombinedPresentHousingExpenseAmount = null;
            //urlaTotal.CombinedProposedHousingExpenseAmount = null;
            //urlaTotal.CommissionsIncomeAmount = null;
            //urlaTotal.DividendsInterestIncomeAmount = null;
            //urlaTotal.LiabilityMonthlyPaymentsAmount = null;
            //urlaTotal.LiabilityUnpaidBalanceAmount = null;
            //urlaTotal.LotAndImprovementsAmount = null;
            //urlaTotal.MonthlyIncomeAmount = null;
            //urlaTotal.NetRentalIncomeAmount = null;
            //urlaTotal.NetWorthAmount = null;
            //urlaTotal.OtherTypesOfIncomeAmount = null;
            //urlaTotal.OvertimeIncomeAmount = null;
            //urlaTotal.ReoLienInstallmentAmount = null;
            //urlaTotal.ReoLienUpbAmount = null;
            //urlaTotal.ReoMaintenanceExpenseAmount = null;
            //urlaTotal.ReoMarketValueAmount = null;
            //urlaTotal.ReoRentalIncomeGrossAmount = null;
            //urlaTotal.ReoRentalIncomeNetAmount = null;
            urlaTotal.TransactionCostAmount = m_dataLoan.sTotTransC_rep;
            //urlaTotal.HousingExpenseList.Add(CreateHousingExpense());
            return urlaTotal;
        }

        private HousingExpense CreateHousingExpense()
        {
            HousingExpense housingExpense = new HousingExpense();
            //housingExpense.Id = null;
            //housingExpense.HousingExpenseType = null;
            //housingExpense.HousingExpenseTypeOtherDescription = null;
            //housingExpense.PaymentAmount = null;
            return housingExpense;
        }

        private ClosingDocuments CreateClosingDocuments()
        {
            ClosingDocuments closingDocuments = new ClosingDocuments();

            closingDocuments.AllongeToNote = CreateAllongeToNote(); // GF - OPM 97019
            closingDocuments.Beneficiary = CreateBeneficiary();
            closingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.ClosingAgent));
            closingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.Escrow));
            closingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.Title));

            closingDocuments.ClosingInstructionsList.Add(CreateClosingInstructions());

            #region Hudline 802

            if (m_exportTridData)
            {
                //BrokerCompensation
                var compensationFee = (BorrowerClosingCostFee)m_dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sGfeOriginatorCompF, m_dataLoan.BrokerDB.BrokerID, false);
                if (compensationFee != null
                    && m_dataLoan.sGfeCreditLenderPaidItemT != E_CreditLenderPaidItemT.None
                    && m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid)
                {
                    closingDocuments.CompensationList.Add(new Mismo.Closing2_6.Compensation()
                    {
                        Amount = compensationFee.TotalAmount_rep,
                        Type = E_CompensationType.BrokerCompensation,
                        TypeOtherDescription = null,
                        GfeAggregationType = E_CompensationGfeAggregationType.ChosenInterestRateCreditOrCharge,
                        GfeDisclosedAmount = null,
                        PaidToType = E_CompensationPaidToType.Broker,
                        PaidByType = E_CompensationPaidByType.Lender,
                        Percent = compensationFee.Percent_rep
                    });
                }
            }
            //OPM 69510 av 8/26
            else if (mxGfe) // Exporting GFE
            {
                // OPM 70657 m.p. 11/17. Adding compensation elements for line 802.

                // BrokerCompensation
                if (m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF != 0
                    && m_dataLoan.sGfeCreditLenderPaidItemT != E_CreditLenderPaidItemT.None
                    && m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid) // checks if originator compensation is lender paid
                {
                    string brokerCompensationAmount = m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep;
                    //string brokerCompensationPercent = m_dataLoan.sGfeOriginatorCompFPc_backCalculated_rep; // back-calculated: sGfeOriginatorCompF / sFinalLAmt
                    string brokerCompensationPercent = null;
                    if (m_dataLoan.sGfeOriginatorCompFBaseT == E_PercentBaseT.TotalLoanAmount
                        && m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFMb > 0)
                    {
                        brokerCompensationPercent = m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFPc_rep;
                    }
                    closingDocuments.CompensationList.Add(new Mismo.Closing2_6.Compensation()
                    {
                        Amount = brokerCompensationAmount,
                        Type = E_CompensationType.BrokerCompensation,
                        TypeOtherDescription = null,
                        GfeAggregationType = E_CompensationGfeAggregationType.ChosenInterestRateCreditOrCharge,
                        GfeDisclosedAmount = null,
                        PaidToType = E_CompensationPaidToType.Broker,
                        PaidByType = E_CompensationPaidByType.Lender, // This will always be lender for the 802
                        Percent = brokerCompensationPercent
                    });
                }
                // YieldSpreadDifferential - becomes YIELD SPREAD PREMIUM on DM
                string yieldSpreadDiffAmount;
                if (m_dataLoan.sGfeLenderCreditYieldSpreadDifferentialAmount != 0)
                {
                    yieldSpreadDiffAmount = m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditYieldSpreadDifferentialAmount_rep;
                    closingDocuments.CompensationList.Add(new Mismo.Closing2_6.Compensation()
                    {
                        Id = "LQBGFE802Compensation",
                        Amount = yieldSpreadDiffAmount,
                        Type = E_CompensationType.YieldSpreadDifferential,
                        TypeOtherDescription = null,
                        GfeAggregationType = E_CompensationGfeAggregationType.ChosenInterestRateCreditOrCharge,
                        GfeDisclosedAmount = null,
                        PaidToType = E_CompensationPaidToType.Broker,
                        PaidByType = E_CompensationPaidByType.Lender
                        //Percent = m_dataLoan.sGfeDiscountPointFPc_rep
                    });
                    m_setConformingYear = true;
                    m_yieldSpreadDiffAmt = yieldSpreadDiffAmount;
                }
            }
            else // Exporting settlement charges
            {
                decimal sLDiscnt = m_dataLoan.sSettlementLDiscnt;

                // BrokerCompensation
                if (m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF != 0
                    && m_dataLoan.sSettlementCreditLenderPaidItemT != E_CreditLenderPaidItemT.None
                    && m_dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid) // checks if originator compensation is lender paid
                {
                    string brokerCompensationAmount = m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep;
                    //string brokerCompensationPercent = m_dataLoan.sSettlementOriginatorCompFPc_backCalculated_rep; // back-calculated: sSettlementOriginatorCompF / sFinalLAmt
                    string brokerCompensationPercent = null;
                    if (m_dataLoan.sGfeOriginatorCompFBaseT == E_PercentBaseT.TotalLoanAmount
                        && m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFMb > 0)
                    {
                        brokerCompensationPercent = m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompFPc_rep;
                    }
                    closingDocuments.CompensationList.Add(new Mismo.Closing2_6.Compensation()
                    {
                        Amount = brokerCompensationAmount,
                        Type = E_CompensationType.BrokerCompensation,
                        TypeOtherDescription = null,
                        GfeAggregationType = E_CompensationGfeAggregationType.ChosenInterestRateCreditOrCharge,
                        GfeDisclosedAmount = miGfe ? m_dataLoanWithGfeArchiveApplied.sGfeOriginatorCompF_rep : null, // OPM 75442 m.p.: include the $ amount totals of the corresponding elements on the GFE in the GFEDisclosedAmount attributes
                        PaidToType = E_CompensationPaidToType.Broker,
                        PaidByType = E_CompensationPaidByType.Lender, // This will always be lender for the 802
                        Percent = brokerCompensationPercent
                    });
                }

                // YieldSpreadDifferential
                string yieldSpreadDiffAmount;
                if (m_dataLoan.sSettlementLenderCreditYieldSpreadDifferentialAmount != 0)
                {
                    yieldSpreadDiffAmount = m_dataLoan.sSettlementLenderCreditYieldSpreadDifferentialAmount_rep;
                    closingDocuments.CompensationList.Add(new Mismo.Closing2_6.Compensation()
                    {
                        Amount = yieldSpreadDiffAmount,
                        Type = E_CompensationType.YieldSpreadDifferential,
                        TypeOtherDescription = null,
                        GfeAggregationType = E_CompensationGfeAggregationType.ChosenInterestRateCreditOrCharge,
                        GfeDisclosedAmount = miGfe ? m_dataLoanWithGfeArchiveApplied.sGfeLenderCreditYieldSpreadDifferentialAmount_rep : null, // OPM 75442 m.p.: include the $ amount totals of the corresponding elements on the GFE in the GFEDisclosedAmount attributes
                        PaidToType = E_CompensationPaidToType.Broker,
                        PaidByType = E_CompensationPaidByType.Lender
                        //Percent = m_dataLoan.sSettlementDiscountPointFPc_rep
                    });
                    m_setConformingYear = true;
                    m_yieldSpreadDiffAmt = yieldSpreadDiffAmount;
                }
            }

            #endregion
            //closingDocuments.CosignerList.Add(CreateCosigner());
            if (miGfe)
            {
                closingDocuments.EscrowAccountDetailList.Add(CreateEscrowAccountDetail());
            }
            closingDocuments.Execution = CreateExecution();
            //closingDocuments.Investor = CreateInvestor();
            closingDocuments.Lender = CreateLender();
            closingDocuments.LenderBranch = CreateLenderBranch();
            closingDocuments.LoanDetails = CreateLoanDetails();
            closingDocuments.LossPayeeList.Add(CreateLossPayee());

            //have to use create new because fail get agent of role returns the loan officer automatically =/ 
            var mortgageBroker = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.CreateNewDoNotFallBack);
            //if it doesnt exist and  sGfeIsTPOTransaction is set look for a agent loan officer

            if (mortgageBroker.IsNewRecord && m_dataLoan.sGfeIsTPOTransaction)
            {
                mortgageBroker = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            }

            //if somehow mortgageBroker was set by a valid object always export it.
            if (mortgageBroker.IsValid && mortgageBroker.IsNewRecord == false)
            {
                closingDocuments.MortgageBroker = CreateMortgageBroker(mortgageBroker);
            }

            //closingDocuments.PaymentDetails = CreatePaymentDetails();
            if (m_dataLoan.sRefPdOffAmt1003Lckd)
            {
                if (m_dataLoan.sRefPdOffAmt1003_rep != "")
                {
                    closingDocuments.PayoffList.Add(CreatePayoff(m_dataLoan.sRefPdOffAmt1003_rep, ""));
                }
            }
            else
            {
                // 1/27/2006 dd - Export payoff base on liabilities.
                ILiaCollection aLiaCollection = m_dataLoan.GetAppData(0).aLiaCollection;
                int count = aLiaCollection.CountRegular;
                for (int i = 0; i < count; i++)
                {
                    ILiabilityRegular record = aLiaCollection.GetRegularRecordAt(i);
                    if (record.WillBePdOff && record.Bal_rep != "0.00")
                    {
                        closingDocuments.PayoffList.Add(CreatePayoff(record.Bal_rep, record.ComNm));
                    }
                }

            }

            closingDocuments.RecordableDocumentList.Add(CreateRecordableDocument());
            
            if (m_exportSource == E_SettlementChargesExportSource.SETTLEMENT)
                PopulateRespaHudDetailList(closingDocuments.RespaHudDetailList);
            
            
            closingDocuments.RespaServicingData = CreateRespaServicingData();
            closingDocuments.RespaSummary = CreateRespaSummary();
            PopulateClosingDocumentsSellerList(closingDocuments.SellerList);
            //closingDocuments.ServicerList.Add(CreateServicer());
            closingDocuments.BuilderList.Add(CreateBuilder());
            //closingDocuments.ClosingCostList.Add(CreateClosingCost());
            closingDocuments.TrustList.Add(CreateTrust());
            return closingDocuments;
        }

        private void PopulateClosingDocumentsSellerList(List<Seller> sellerList)
        {
            if (m_dataLoan.sSellerCollection.IsEmpty()) return;
            foreach (var seller in m_dataLoan.sSellerCollection.ListOfSellers)
            {
                sellerList.Add(CreateSeller(seller));
            }
        }

        private AllongeToNote CreateAllongeToNote()
        {
            AllongeToNote allongeToNote = new AllongeToNote();
            //allongeToNote.Id = null;
            //allongeToNote.Date = null;
            //allongeToNote.ExecutedByDescription = null;
            //allongeToNote.InFavorOfDescription = null;
            allongeToNote.PayToTheOrderOfDescription = m_dataLoan.sDocMagicTransferToInvestorCode; // GF - OPM 97019
            //allongeToNote.Type = null;
            //allongeToNote.TypeOtherDescription = null;
            //allongeToNote.WithoutRecourseDescription = null;
            //allongeToNote.AuthorizedRepresentative = CreateAuthorizedRepresentative();
            return allongeToNote;
        }

        private Beneficiary CreateBeneficiary()
        {
            DocMagicAlternateLender altLender = new DocMagicAlternateLender(m_dataLoan.sBrokerId, m_dataLoan.sDocMagicAlternateLenderId);
            Beneficiary beneficiary = new Beneficiary();
            //beneficiary.Id = null;
            beneficiary.NonPersonEntityIndicator = altLender.BeneficiaryNonPersonEntityIndicator ? E_YNIndicator.Y : E_YNIndicator.N;
            beneficiary.City = altLender.BeneficiaryCity;
            //beneficiary.Country = null;
            //beneficiary.County = null;
            beneficiary.PostalCode = altLender.BeneficiaryZip;
            beneficiary.State = altLender.BeneficiaryState;
            beneficiary.StreetAddress = altLender.BeneficiaryAddress;
            //beneficiary.StreetAddress2 = null;
            beneficiary.UnparsedName = altLender.BeneficiaryName;
            //beneficiary.ContactDetail = CreateContactDetail();
            //beneficiary.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return beneficiary;
        }

        private ClosingAgent CreateClosingAgent(E_AgentRoleT agentRole)
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }
            ClosingAgent closingAgent = new ClosingAgent();
            //closingAgent.Id = null;
            //closingAgent.AuthorizedToSignIndicator = null;
            //closingAgent.NonPersonEntityIndicator = null;
            closingAgent.City = agent.City;
            closingAgent.State = agent.State;
            //closingAgent.Country = null;
            //closingAgent.County = null;
            //closingAgent.Identifier = null;
            closingAgent.OrderNumberIdentifier = agent.CaseNum;
            closingAgent.PostalCode = agent.Zip;
            closingAgent.StreetAddress = agent.StreetAddr;
            //closingAgent.StreetAddress2 = null;
            //closingAgent.TitleDescription = null;
            closingAgent.Type = ToMismoClosingAgentType(agentRole);
            //closingAgent.TypeOtherDescription = null;
            closingAgent.UnparsedName = agent.CompanyName;
            ContactDetail contactDetail = new ContactDetail();
            contactDetail.Name = agent.AgentName;
            contactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Phone, Value = agent.Phone });
            contactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Fax, Value = agent.FaxNum });
            contactDetail.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Email, Value = agent.EmailAddr });

            closingAgent.ContactDetail = contactDetail;

            //closingAgent.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return closingAgent;
        }

        private E_ClosingAgentType ToMismoClosingAgentType(E_AgentRoleT agentRole)
        {
            switch (agentRole)
            {
                case E_AgentRoleT.ClosingAgent: return E_ClosingAgentType.ClosingAgent;
                case E_AgentRoleT.Escrow: return E_ClosingAgentType.EscrowCompany;
                case E_AgentRoleT.Title: return E_ClosingAgentType.TitleCompany;
                default:
                    return E_ClosingAgentType.Undefined;

            }
        }

        private ClosingInstructions CreateClosingInstructions()
        {
            ClosingInstructions closingInstructions = new ClosingInstructions();

            #region Conditions
            var brokerId = m_dataLoan.sBrokerId;
            BrokerDB brokerDB = m_dataLoan.BrokerDB;
            if (brokerDB.IsUseNewTaskSystem)
            {
                List<Task> tasks = Task.GetActiveConditionsByLoanId(brokerId, slid, false);
                foreach (Task task in tasks)
                {
                    if (task.TaskStatus == E_TaskStatus.Closed)
                    {
                        continue; //skip complete condition
                    }
                    closingInstructions.ConditionList.Add(CreateCondition(task.TaskSubject));
                }
            }
            else
            {
                LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();

                if (brokerDB.HasLenderDefaultFeatures)
                {
                    ldSet.Retrieve(m_dataLoan.sLId, false, false, false);

                    foreach (CLoanConditionObsolete condition in ldSet)
                    {
                        if (condition.CondStatus == E_CondStatus.Done)
                            continue; // Skip complete condition.

                        closingInstructions.ConditionList.Add(CreateCondition(condition.CondDesc));
                    }
                }
                else
                {
                    int count = m_dataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete condition = m_dataLoan.GetCondFieldsObsolete(i);
                        if (condition.IsDone)
                            continue;
                        closingInstructions.ConditionList.Add(CreateCondition(condition.CondDesc));
                    }
                }
            }

            #endregion
            //closingInstructions.Id = null;
            //closingInstructions.FundingCutoffTime = null;
            //closingInstructions.HoursDocumentsNeededPriorToDisbursementCount = null;
            //closingInstructions.LeadBasedPaintCertificationRequiredIndicator = null;
            closingInstructions.PreliminaryTitleReportDate = m_dataLoan.sPrelimRprtDocumentD_rep;
            //closingInstructions.SpecialFloodHazardAreaIndicator = null;
            //closingInstructions.SpecialFloodHazardAreaIndictor = null;
            closingInstructions.TitleReportItemsDescription = m_dataLoan.sTitleReportItemsDescription;
            closingInstructions.TitleReportRequiredEndorsementsDescription = m_dataLoan.sRequiredEndorsements;
            //closingInstructions.ConsolidatedClosingConditionsDescription = null;
            closingInstructions.PropertyTaxMessageDescription = m_dataLoan.sPropertyTaxMessageDescription;
            //closingInstructions.TermiteReportRequiredIndicator = null;
            return closingInstructions;
        }

        private Condition CreateCondition(string desc)
        {
            if (string.IsNullOrEmpty(desc))
                return null;

            Condition condition = new Condition();
            //condition.Id = null;
            condition.Description = Tools.ReplaceInvalidUnicodeChars(desc);
            //condition.MetIndicator = null;
            //condition.SatisfactionTimeframeType = null;
            //condition.SatisfactionTimeframeTypeOtherDescription = null;
            //condition.SatisfactionApprovedByName = null;
            //condition.SatisfactionDate = null;
            //condition.SatisfactionResponsiblePartyType = null;
            //condition.SatisfactionResponsiblePartyTypeOtherDescription = null;
            //condition.SequenceIdentifier = null;
            //condition.WaivedIndicator = null;
            return condition;
        }

        private Mismo.Closing2_6.Compensation CreateCompensation(
            string amount,
            E_CompensationPaidByType paidBy,
            E_CompensationPaidToType paidTo,
            string percent,
            E_CompensationGfeAggregationType gfeAggregationType,
            string gfeDisclosedAmount,
            E_CompensationType type,
            string typeDescription,
            string typeOtherDescription)
        {
            Mismo.Closing2_6.Compensation compensation = new Mismo.Closing2_6.Compensation();
            //compensation.Id = null;
            compensation.Amount = amount;
            compensation.PaidByType = paidBy;
            compensation.PaidToType = paidTo;
            compensation.Percent = percent;
            compensation.GfeAggregationType = gfeAggregationType;
            compensation.GfeDisclosedAmount = gfeDisclosedAmount;
            compensation.Type = type;
            compensation.TypeOtherDescription = typeOtherDescription;
            return compensation;
        }

        private Cosigner CreateCosigner()
        {
            Cosigner cosigner = new Cosigner();
            //cosigner.Id = null;
            //cosigner.TitleDescription = null;
            //cosigner.UnparsedName = null;
            return cosigner;
        }

        private EscrowAccountDetail CreateEscrowAccountDetail()
        {
            EscrowAccountDetail escrowAccountDetail = new EscrowAccountDetail();
            //escrowAccountDetail.Id = null;
            //escrowAccountDetail.TotalMonthlyPitiAmount = null;
            //escrowAccountDetail.InitialBalanceAmount = null;
            //escrowAccountDetail.MinimumBalanceAmount = null;
            //escrowAccountDetail.EscrowAccountActivityList.Add(CreateEscrowAccountActivity());
            escrowAccountDetail.GfeDisclosedInitialEscrowBalanceAmount = m_dataLoanWithGfeArchiveApplied.sGfeInitialImpoundDeposit_rep;

            // OPM 88370 - Sync LQB GFE Block 9 with DocMagic - MP
            // If there is a value in 1004 or 1005
            if (m_dataLoan.sRealETxRsrv != 0.0M || // 1004
                m_dataLoan.sSchoolTxRsrv != 0.0M) // 1005
            {
                escrowAccountDetail.InitialEscrowDepositIncludesAllPropertyTaxesIndicator = ToMismo(true);
            }

            // If there is a value in 1002, 1003, or 1006
            if (m_dataLoan.sHazInsRsrv != 0.0M || // 1002
                m_dataLoan.sMInsRsrv != 0.0M || // 1003
                m_dataLoan.sFloodInsRsrv != 0.0M) // 1006
            {
                escrowAccountDetail.InitialEscrowDepositIncludesAllInsuranceIndicator = ToMismo(true);
            }

            // When there is a value in either 1008 or 1009
            if (m_dataLoan.s1006Rsrv != 0.0M || // The 1008
                m_dataLoan.s1007Rsrv != 0.0M || // 1009
                m_dataLoan.sU3Rsrv != 0.0M ||   // 1010
                m_dataLoan.sU4Rsrv != 0.0M)     // 1011
            {
                var otherDescriptionList = new List<string>();
                if (!string.IsNullOrEmpty(m_dataLoan.s1006ProHExpDesc))
                {
                    otherDescriptionList.Add(m_dataLoan.s1006ProHExpDesc);
                }
                if (!string.IsNullOrEmpty(m_dataLoan.s1007ProHExpDesc))
                {
                    otherDescriptionList.Add(m_dataLoan.s1007ProHExpDesc);
                }
                if (!string.IsNullOrEmpty(m_dataLoan.sU3RsrvDesc))
                {
                    otherDescriptionList.Add(m_dataLoan.sU3RsrvDesc);
                }
                if (!string.IsNullOrEmpty(m_dataLoan.sU4RsrvDesc))
                {
                    otherDescriptionList.Add(m_dataLoan.sU4RsrvDesc);
                }
                escrowAccountDetail.InitialEscrowDepositIncludesOtherDescription = string.Join("/", otherDescriptionList.ToArray());
            }

            return escrowAccountDetail;
        }

        private EscrowAccountActivity CreateEscrowAccountActivity()
        {
            EscrowAccountActivity escrowAccountActivity = new EscrowAccountActivity();
            //escrowAccountActivity.Id = null;
            //escrowAccountActivity.CurrentBalanceAmount = null;
            //escrowAccountActivity.DisbursementMonth = null;
            //escrowAccountActivity.DisbursementSequenceIdentifier = null;
            //escrowAccountActivity.DisbursementYear = null;
            //escrowAccountActivity.PaymentDescriptionType = null;
            //escrowAccountActivity.PaymentDescriptionTypeOtherDescription = null;
            //escrowAccountActivity.PaymentFromEscrowAccountAmount = null;
            //escrowAccountActivity.PaymentToEscrowAccountAmount = null;
            return escrowAccountActivity;
        }

        private Execution CreateExecution()
        {
            Execution execution = new Execution();
            //execution.Id = null;
            //execution.City = null;
            execution.County = m_dataLoan.sSpCounty;
            execution.Date = m_dataLoan.sDocMagicSigningD_rep;
            //execution.State = null;
            return execution;
        }

        private Investor CreateInvestor()
        {
            Investor investor = new Investor();
            //investor.Id = null;
            //investor.NonPersonEntityIndicator = null;
            //investor.City = null;
            //investor.Country = null;
            //investor.County = null;
            //investor.PostalCode = null;
            //investor.State = null;
            //investor.StreetAddress = null;
            //investor.StreetAddress2 = null;
            //investor.UnparsedName = null;
            //investor.ContactDetail = CreateContactDetail();
            //investor.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //investor.RegulatoryAgencyList.Add(CreateRegulatoryAgency());
            return investor;
        }

        private RegulatoryAgency CreateRegulatoryAgency()
        {
            RegulatoryAgency regulatoryAgency = new RegulatoryAgency();
            //regulatoryAgency.Id = null;
            //regulatoryAgency.City = null;
            //regulatoryAgency.Country = null;
            //regulatoryAgency.County = null;
            //regulatoryAgency.PostalCode = null;
            //regulatoryAgency.State = null;
            //regulatoryAgency.StreetAddress = null;
            //regulatoryAgency.StreetAddress2 = null;
            //regulatoryAgency.Type = null;
            //regulatoryAgency.TypeOtherDescription = null;
            //regulatoryAgency.UnparsedName = null;
            //regulatoryAgency.ContactDetail = CreateContactDetail();
            return regulatoryAgency;
        }

        private Lender CreateLender()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            DocMagicAlternateLender altLender = new DocMagicAlternateLender(m_dataLoan.sBrokerId, m_dataLoan.sDocMagicAlternateLenderId);

            Lender lender = new Lender();
            //lender.Id = null;
            lender.NonPersonEntityIndicator = altLender.LenderNonPersonEntityIndicator ? E_YNIndicator.Y : E_YNIndicator.N;
            lender.City = altLender.LenderCity;
            //lender.Country = null;
            //lender.County = null;
            //lender.DocumentsOrderedByName = null;
            //lender.FunderName = null;
            lender.PostalCode = altLender.LenderZip;
            lender.State = altLender.LenderState;
            lender.StreetAddress = altLender.LenderAddress;
            //lender.StreetAddress2 = null;
            lender.UnparsedName = altLender.LenderName;
            lender.ContactDetail = new ContactDetail() { Name = m_dataLoan.sEmployeeLoanRepName };
            //lender.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //lender.RegulatoryAgencyList.Add(CreateRegulatoryAgency());
            return lender;
        }

        private LenderBranch CreateLenderBranch()
        {
            BranchDB branch = new BranchDB(m_dataLoan.sBranchId, m_dataLoan.sBrokerId);
            branch.Retrieve();

            LenderBranch lenderBranch = new LenderBranch();
            //lenderBranch.Id = null;
            lenderBranch.City = branch.Address.City;
            //lenderBranch.Country = branch.Address.Country;
            //lenderBranch.County = branch.Address.County;
            lenderBranch.PostalCode = branch.Address.Zipcode;
            lenderBranch.State = branch.Address.State;
            lenderBranch.StreetAddress = branch.Address.StreetAddress;
            //lenderBranch.StreetAddress2 = null;
            lenderBranch.UnparsedName = m_dataLoan.BranchNm;
            lenderBranch.ContactDetail = CreateContactDetail(branch);
            return lenderBranch;
        }

        /// <summary>
        /// Contact detail for the lender branch
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        private ContactDetail CreateContactDetail(BranchDB branch)
        {
            var contact = new ContactDetail();

            if (!string.IsNullOrEmpty(branch.Phone))
                contact.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Phone, Value = branch.Phone });

            if (!string.IsNullOrEmpty(branch.Fax))
                contact.ContactPointList.Add(new Mismo.Closing2_6.ContactPoint() { Type = Mismo.Closing2_6.E_ContactPointType.Fax, Value = branch.Fax });

            return contact;
        }

        private LoanDetails CreateLoanDetails()
        {
            LoanDetails loanDetails = new LoanDetails();
            //loanDetails.Id = null;
            //loanDetails.ClosingDate = null;
            loanDetails.DisbursementDate = m_dataLoan.sDocMagicDisbursementD_rep;
            //loanDetails.DocumentOrderClassificationType = null;
            loanDetails.DocumentPreparationDate = m_dataLoan.sDocMagicDocumentD_rep;
            loanDetails.TruthInLendingDisclosureDate = m_dataLoan.sTilPrepareDate_rep;
            //loanDetails.FundByDate = null;
            loanDetails.LockExpirationDate = m_dataLoan.sRLckdExpiredD_rep;
            //loanDetails.OriginalLtvRatioPercent = null;
            loanDetails.RescissionDate = m_dataLoan.sDocMagicCancelD_rep;

            if (m_use2015DataLayer)
            {
                loanDetails.InterimInterestList.Add(CreateInterimInterest());
            }
            {
                if (mxGfe || miGfe)
                {
                    loanDetails.InterimInterestList.Add(CreateGfeInterimInterest());
                }
                if (!mxGfe)
                {
                    loanDetails.InterimInterestList.Add(CreateHud1InterimInterest());
                }
            }

            //loanDetails.RequestToRescind = CreateRequestToRescind();
            loanDetails.GfeDetail = CreateGfeDetail();
            return loanDetails;
        }

        private GfeDetail CreateGfeDetail()
        {
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            GfeDetail gfeDetail = new GfeDetail();
            gfeDetail.GfeInterestRateAvailableThroughDate = m_dataLoan.sIsRateLocked ? m_dataLoan.sRLckdExpiredD_rep_WithTime : m_dataLoan.sGfeNoteIRAvailTillD_rep_WithTime;
            gfeDetail.GfeSettlementChargesAvailableThroughDate = m_dataLoan.sDocMagicEstAvailableThroughD_rep;
            gfeDetail.GfeRateLockPeriodDaysCount = m_dataLoan.sGfeRateLockPeriod_rep;
            gfeDetail.GfeRateLockMinimumDaysPriorToSettlementCount = m_dataLoan.sGfeLockPeriodBeforeSettlement_rep;
            gfeDetail.GfeDisclosureDate = m_dataLoan.sDocMagicGFED_rep;
            gfeDetail.GfeRedisclosureReasonDescription = m_dataLoanWithGfeArchiveApplied.sCircumstanceChangeExplanation;
            gfeDetail.GfeComparisonList.Add(CreateGfeComparison(
                E_GfeComparisonType.SameLoanWithLowerInterestRate,
                m_dataLoan.sGfeTradeOffLowerRateNoteIR_rep,
                m_dataLoan.sGfeTradeOffLowerRateClosingCost_rep));
            gfeDetail.GfeComparisonList.Add(CreateGfeComparison(
                E_GfeComparisonType.SameLoanWithLowerSettlementCharges,
                m_dataLoan.sGfeTradeOffLowerCCNoteIR_rep,
                m_dataLoan.sGfeTradeOffLowerCCClosingCost_rep));
            return gfeDetail;
        }

        private GfeComparison CreateGfeComparison(E_GfeComparisonType type, string interestRatePercent, string settlementChargesAmount)
        {
            GfeComparison gfeComparison = new GfeComparison();
            gfeComparison.InterestRatePercent = interestRatePercent;
            gfeComparison.SettlementChargesAmount = settlementChargesAmount;
            gfeComparison.Type = type;

            if (type == E_GfeComparisonType.SameLoanWithLowerSettlementCharges)
            {
                gfeComparison.OriginalLoanAmount = m_dataLoan.sGfeTradeOffLowerCCLoanAmt_rep;
                gfeComparison.MonthlyPaymentAmount = m_dataLoan.sGfeTradeOffLowerCCMPmtAndMIns_rep;
            }
            else if (type == E_GfeComparisonType.SameLoanWithLowerInterestRate)
            {
                gfeComparison.OriginalLoanAmount = m_dataLoan.sGfeTradeOffLowerRateLoanAmt_rep;
                gfeComparison.MonthlyPaymentAmount = m_dataLoan.sGfeTradeOffLowerRateMPmtAndMIns_rep;
            }
            
            return gfeComparison;
        }

        private InterimInterest CreateGfeInterimInterest()
        {
            InterimInterest interimInterest = new InterimInterest();
            //interimInterest.Id = null;
            //interimInterest.PaidFromDate = null;

            string paidNumberOfDays = m_dataLoanWithGfeArchiveApplied.sIPiaDy_rep;
            if (string.IsNullOrEmpty(paidNumberOfDays))
            {
                paidNumberOfDays = "0";
            }
            interimInterest.PaidNumberOfDays = paidNumberOfDays;
            
            //interimInterest.PaidThroughDate = null;
            if (m_dataLoan.sDaysInYr_rep == "365")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._365;
            }
            else if (m_dataLoan.sDaysInYr_rep == "360")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._360;
            }

            interimInterest.DisclosureType = E_InterimInterestDisclosureType.GFE;

            //interimInterest.PerDiemPaymentOptionType = null;
            //interimInterest.PerDiemPaymentOptionTypeOtherDescription = null;
            //interimInterest.SinglePerDiemAmount = null;
            interimInterest.TotalPerDiemAmount = m_dataLoanWithGfeArchiveApplied.sIPia_rep;
            return interimInterest;
        }



        private InterimInterest CreateHud1InterimInterest()
        {
            InterimInterest interimInterest = new InterimInterest();
            //interimInterest.Id = null;
            //interimInterest.PaidFromDate = null;
            string paidNumberOfDays = m_dataLoan.sSettlementIPiaDy_rep;
            if (string.IsNullOrEmpty(paidNumberOfDays))
            {
                paidNumberOfDays = "0";
            }
            interimInterest.PaidNumberOfDays = paidNumberOfDays;
            //interimInterest.PaidThroughDate = null;

            if (m_dataLoan.sDaysInYr_rep == "365")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._365;
            }
            else if (m_dataLoan.sDaysInYr_rep == "360")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._360;
            }
            interimInterest.DisclosureType = E_InterimInterestDisclosureType.HUD1;

            //interimInterest.PerDiemPaymentOptionType = null;
            //interimInterest.PerDiemPaymentOptionTypeOtherDescription = null;
            //interimInterest.SinglePerDiemAmount = null;
            //interimInterest.TotalPerDiemAmount = m_dataLoan.sSettlementIPia_rep;   this is ignored in hud1 
            return interimInterest;
        }

        /// <summary>
        /// Creates an interim interest using fields from the 2015 datalayer.
        /// </summary>
        /// <returns>An <see cref="InterimInterest"/> object.</returns>
        private InterimInterest CreateInterimInterest()
        {
            InterimInterest interimInterest = new InterimInterest();
            //interimInterest.Id = null;
            //interimInterest.PaidFromDate = null;

            string paidNumberOfDays = m_dataLoan.sSettlementIPiaDy_rep;
            if (string.IsNullOrEmpty(paidNumberOfDays))
            {
                paidNumberOfDays = "0";
            }
            interimInterest.PaidNumberOfDays = paidNumberOfDays;
            //interimInterest.PaidThroughDate = null;

            if (m_dataLoan.sDaysInYr_rep == "365")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._365;
            }
            else if (m_dataLoan.sDaysInYr_rep == "360")
            {
                interimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._360;
            }
            interimInterest.DisclosureType = E_InterimInterestDisclosureType.Other;

            //interimInterest.PerDiemPaymentOptionType = null;
            //interimInterest.PerDiemPaymentOptionTypeOtherDescription = null;
            //interimInterest.SinglePerDiemAmount = null;
            //interimInterest.TotalPerDiemAmount = m_dataLoan.sSettlementIPia_rep;
            return interimInterest;
        }

        private RequestToRescind CreateRequestToRescind()
        {
            RequestToRescind requestToRescind = new RequestToRescind();
            //requestToRescind.Id = null;
            //requestToRescind.NotificationCity = null;
            //requestToRescind.NotificationCountry = null;
            //requestToRescind.NotificationPostalCode = null;
            //requestToRescind.NotificationState = null;
            //requestToRescind.NotificationStreetAddress = null;
            //requestToRescind.NotificationStreetAddress2 = null;
            //requestToRescind.NotificationUnparsedName = null;
            //requestToRescind.TransactionDate = null;
            return requestToRescind;
        }

        private LossPayee CreateLossPayee()
        {
            DocMagicAlternateLender altLender = new DocMagicAlternateLender(m_dataLoan.sBrokerId, m_dataLoan.sDocMagicAlternateLenderId);
            LossPayee lossPayee = new LossPayee();
            //lossPayee.Id = null;
            //lossPayee.NonPersonEntityIndicator = null;
            lossPayee.City = altLender.LossPayeeCity;
            //lossPayee.Country = null;
            //lossPayee.County = null;
            lossPayee.PostalCode = altLender.LossPayeeZip;
            lossPayee.State = altLender.LossPayeeState;
            lossPayee.StreetAddress = altLender.LossPayeeAddress;
            //lossPayee.StreetAddress2 = null;
            //lossPayee.Type = null;
            //lossPayee.TypeOtherDescription = null;
            lossPayee.UnparsedName = altLender.LossPayeeName;
            //lossPayee.ContactDetail = CreateContactDetail();
            //lossPayee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return lossPayee;
        }

        private MortgageBroker CreateMortgageBroker(CAgentFields agent)
        {
            if (agent.IsValid == false)
            {
                return null;
            }
            MortgageBroker mortgageBroker = new MortgageBroker();
            //mortgageBroker.Id = null;
            //mortgageBroker.NonPersonEntityIndicator = null;
            mortgageBroker.City = agent.City;
            //mortgageBroker.Country = null;
            //mortgageBroker.County = null;
            mortgageBroker.LicenseNumberIdentifier = agent.LicenseNumOfCompany;
            mortgageBroker.PostalCode = agent.Zip;
            mortgageBroker.State = agent.State;
            mortgageBroker.StreetAddress = agent.StreetAddr;
            //mortgageBroker.StreetAddress2 = null;
            mortgageBroker.UnparsedName = agent.CompanyName;
            mortgageBroker.ContactDetail = CreateContactDetail(agent);
            //mortgageBroker.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //mortgageBroker.RegulatoryAgencyList.Add(CreateRegulatoryAgency());
            return mortgageBroker;
        }

        private PaymentDetails CreatePaymentDetails()
        {
            PaymentDetails paymentDetails = new PaymentDetails();
            //paymentDetails.AmortizationScheduleList.Add(CreateAmortizationSchedule());
            //paymentDetails.PaymentScheduleList.Add(CreatePaymentSchedule());
            return paymentDetails;
        }

        private AmortizationSchedule CreateAmortizationSchedule()
        {
            AmortizationSchedule amortizationSchedule = new AmortizationSchedule();
            //amortizationSchedule.Id = null;
            //amortizationSchedule.EndingBalanceAmount = null;
            //amortizationSchedule.InterestRatePercent = null;
            //amortizationSchedule.LoanToValuePercent = null;
            //amortizationSchedule.MiPaymentAmount = null;
            //amortizationSchedule.PaymentAmount = null;
            //amortizationSchedule.PaymentDueDate = null;
            //amortizationSchedule.PaymentNumber = null;
            //amortizationSchedule.PortionOfPaymentDistributedToInterestAmount = null;
            //amortizationSchedule.PortionOfPaymentDistributedToPrincipalAmount = null;
            return amortizationSchedule;
        }

        private PaymentSchedule CreatePaymentSchedule()
        {
            PaymentSchedule paymentSchedule = new PaymentSchedule();
            //paymentSchedule.Id = null;
            //paymentSchedule.PaymentAmount = null;
            //paymentSchedule.PaymentSequenceIdentifier = null;
            //paymentSchedule.PaymentStartDate = null;
            //paymentSchedule.PaymentVaryingToAmount = null;
            //paymentSchedule.TotalNumberOfPaymentsCount = null;
            return paymentSchedule;
        }

        private Payoff CreatePayoff(string amount, string companyName)
        {
            Payoff payoff = new Payoff();
            //payoff.Id = null;
            //payoff.AccountNumberIdentifier = null;
            payoff.Amount = amount;
            //payoff.PerDiemAmount = null;
            //payoff.SequenceIdentifier = null;
            //payoff.SpecifiedHudLineNumber = hudLine;
            //payoff.ThroughDate = null;
            payoff.Payee = new Payee() { UnparsedName = companyName };
            return payoff;
        }

        private Payee CreatePayee()
        {
            Payee payee = new Payee();
            //payee.Id = null;
            //payee.NonPersonEntityIndicator = null;
            //payee.City = null;
            //payee.Country = null;
            //payee.County = null;
            //payee.PostalCode = null;
            //payee.State = null;
            //payee.StreetAddress = null;
            //payee.StreetAddress2 = null;
            //payee.UnparsedName = null;
            //payee.ContactDetail = CreateContactDetail();
            //payee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return payee;
        }

        private RecordableDocument CreateRecordableDocument()
        {
            RecordableDocument recordableDocument = new RecordableDocument();
            //recordableDocument.Id = null;
            //recordableDocument.SequenceIdentifier = null;
            //recordableDocument.Type = null;
            //recordableDocument.TypeOtherDescription = null;
            //recordableDocument.AssignFrom = CreateAssignFrom();
            //recordableDocument.AssignTo = CreateAssignTo();
            //recordableDocument.Default = CreateDefault();
            //recordableDocument.NotaryList.Add(CreateNotary());
            //recordableDocument.PreparedBy = CreatePreparedBy();
            //recordableDocument.AssociatedDocument = CreateAssociatedDocument();
            recordableDocument.ReturnToList.Add(CreateReturnTo());
            //recordableDocument.Riders = CreateRiders();
            recordableDocument.SecurityInstrument = CreateSecurityInstrument();
            recordableDocument.TrusteeList.Add(CreateTrustee());
            //recordableDocument.WitnessList.Add(CreateWitness());
            return recordableDocument;
        }

        private AssignFrom CreateAssignFrom()
        {
            AssignFrom assignFrom = new AssignFrom();
            //assignFrom.Id = null;
            //assignFrom.NonPersonEntityIndicator = null;
            //assignFrom.City = null;
            //assignFrom.Country = null;
            //assignFrom.County = null;
            //assignFrom.CountyFipsCode = null;
            //assignFrom.PostalCode = null;
            //assignFrom.SigningOfficialName = null;
            //assignFrom.SigningOfficialTitleDescription = null;
            //assignFrom.State = null;
            //assignFrom.StreetAddress = null;
            //assignFrom.StreetAddress2 = null;
            //assignFrom.UnparsedName = null;
            //assignFrom.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return assignFrom;
        }

        private AssignTo CreateAssignTo()
        {
            AssignTo assignTo = new AssignTo();
            //assignTo.Id = null;
            //assignTo.NonPersonEntityIndicator = null;
            //assignTo.City = null;
            //assignTo.Country = null;
            //assignTo.County = null;
            //assignTo.CountyFipsCode = null;
            //assignTo.PostalCode = null;
            //assignTo.State = null;
            //assignTo.StreetAddress = null;
            //assignTo.StreetAddress2 = null;
            //assignTo.UnparsedName = null;
            //assignTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return assignTo;
        }

        private Default CreateDefault()
        {
            Default _default = new Default();
            //_default.Id = null;
            //_default.AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = null;
            //_default.ApplicationFeesAmount = null;
            //_default.ClosingPreparationFeesAmount = null;
            //_default.LendingInstitutionPostOfficeBoxAddress = null;
            return _default;
        }

        private Notary CreateNotary()
        {
            Notary notary = new Notary();
            //notary.Id = null;
            //notary.AppearanceDate = null;
            //notary.AppearedBeforeNamesDescription = null;
            //notary.AppearedBeforeTitlesDescription = null;
            //notary.City = null;
            //notary.CommissionBondNumberIdentifier = null;
            //notary.CommissionCity = null;
            //notary.CommissionCounty = null;
            //notary.CommissionExpirationDate = null;
            //notary.CommissionNumberIdentifier = null;
            //notary.CommissionState = null;
            //notary.County = null;
            //notary.PostalCode = null;
            //notary.State = null;
            //notary.StreetAddress = null;
            //notary.StreetAddress2 = null;
            //notary.TitleDescription = null;
            //notary.UnparsedName = null;
            //notary.CertificateList.Add(CreateCertificate());
            //notary.DataVersionList.Add(CreateDataVersion());
            return notary;
        }

        private Certificate CreateCertificate()
        {
            Certificate certificate = new Certificate();
            //certificate.Id = null;
            //certificate.SignerCompanyName = null;
            //certificate.SignerFirstName = null;
            //certificate.SignerLastName = null;
            //certificate.SignerMiddleName = null;
            //certificate.SignerNameSuffix = null;
            //certificate.SignerTitleDescription = null;
            //certificate.SignerUnparsedName = null;
            //certificate.SigningCounty = null;
            //certificate.SigningDate = null;
            //certificate.SigningState = null;
            //certificate.SignerIdentification = CreateSignerIdentification();
            return certificate;
        }

        private SignerIdentification CreateSignerIdentification()
        {
            SignerIdentification signerIdentification = new SignerIdentification();
            //signerIdentification.Id = null;
            //signerIdentification.Description = null;
            //signerIdentification.Type = null;
            return signerIdentification;
        }

        private PreparedBy CreatePreparedBy()
        {
            PreparedBy preparedBy = new PreparedBy();
            //preparedBy.Id = null;
            //preparedBy.NonPersonEntityIndicator = null;
            //preparedBy.City = null;
            //preparedBy.Country = null;
            //preparedBy.CountryCode = null;
            //preparedBy.County = null;
            //preparedBy.CountyFipsCode = null;
            //preparedBy.ElectronicRoutingAddress = null;
            //preparedBy.ElectronicRoutingMethodType = null;
            //preparedBy.PostalCode = null;
            //preparedBy.State = null;
            //preparedBy.StateFipsCode = null;
            //preparedBy.StreetAddress = null;
            //preparedBy.StreetAddress2 = null;
            //preparedBy.TelephoneNumber = null;
            //preparedBy.TitleDescription = null;
            //preparedBy.UnparsedName = null;
            //preparedBy.ContactDetail = CreateContactDetail();
            return preparedBy;
        }

        private AssociatedDocument CreateAssociatedDocument()
        {
            AssociatedDocument associatedDocument = new AssociatedDocument();
            //associatedDocument.Id = null;
            //associatedDocument.BookNumber = null;
            //associatedDocument.BookType = null;
            //associatedDocument.BookTypeOtherDescription = null;
            //associatedDocument.CountyOfRecordationName = null;
            //associatedDocument.InstrumentNumber = null;
            //associatedDocument.Number = null;
            //associatedDocument.OfficeOfRecordationName = null;
            //associatedDocument.PageNumber = null;
            //associatedDocument.RecordingDate = null;
            //associatedDocument.RecordingJurisdictionName = null;
            //associatedDocument.StateOfRecordationName = null;
            //associatedDocument.TitleDescription = null;
            //associatedDocument.Type = null;
            //associatedDocument.TypeOtherDescription = null;
            //associatedDocument.VolumeNumber = null;
            return associatedDocument;
        }

        private ReturnTo CreateReturnTo()
        {
            DocMagicAlternateLender altLender = new DocMagicAlternateLender(m_dataLoan.sBrokerId, m_dataLoan.sDocMagicAlternateLenderId);
            ReturnTo returnTo = new ReturnTo();
            //returnTo.Id = null;
            //returnTo.NonPersonEntityIndicator = null;
            returnTo.City = altLender.WhenRecodedMailToCity;
            //returnTo.Country = null;
            //returnTo.CountryCode = null;
            //returnTo.County = null;
            //returnTo.CountyFipsCode = null;
            //returnTo.ElectronicRoutingAddress = null;
            //returnTo.ElectronicRoutingMethodType = null;
            returnTo.PostalCode = altLender.WhenRecodedMailToZip;
            returnTo.State = altLender.WhenRecodedMailToState;
            //returnTo.StateFipsCode = null;
            returnTo.StreetAddress = altLender.WhenRecodedMailToAddress;
            //returnTo.StreetAddress2 = null;
            //returnTo.TitleDescription = null;
            returnTo.UnparsedName = altLender.WhenRecodedMailToName;
            //returnTo.ContactDetail = CreateContactDetail();
            //returnTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return returnTo;
        }

        private Riders CreateRiders()
        {
            Riders riders = new Riders();
            //riders.Id = null;
            //riders.AdjustableRateRiderIndicator = null;
            //riders.BalloonRiderIndicator = null;
            //riders.BiweeklyPaymentRiderIndicator = null;
            //riders.CondominiumRiderIndicator = null;
            //riders.GraduatedPaymentRiderIndicator = null;
            //riders.GrowingEquityRiderIndicator = null;
            //riders.NonOwnerOccupancyRiderIndicator = null;
            //riders.OneToFourFamilyRiderIndicator = null;
            //riders.OtherRiderDescription = null;
            //riders.OtherRiderIndicator = null;
            //riders.PlannedUnitDevelopmentRiderIndicator = null;
            //riders.RateImprovementRiderIndicator = null;
            //riders.RehabilitationLoanRiderIndicator = null;
            //riders.SecondHomeRiderIndicator = null;
            //riders.VaRiderIndicator = null;
            return riders;
        }

        private SecurityInstrument CreateSecurityInstrument()
        {
            SecurityInstrument securityInstrument = new SecurityInstrument();
            //securityInstrument.Id = null;
            //securityInstrument.PropertyLongLegalDescriptionPageNumberDescription = null;
            //securityInstrument.PropertyLongLegalPageNumberDescription = null;
            //securityInstrument.RecordedDocumentIdentifier = null;
            //securityInstrument.AssumptionFeeAmount = null;
            //securityInstrument.AttorneyFeeMinimumAmount = null;
            securityInstrument.AttorneyFeePercent = m_dataLoan.sSecInstrAttorneyFeesPc_rep;
            //securityInstrument.CertifyingAttorneyName = null;
            //securityInstrument.MaximumPrincipalIndebtednessAmount = null;
            //securityInstrument.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = null;
            //securityInstrument.NoteHolderName = null;
            //securityInstrument.NoticeOfConfidentialtyRightsDescription = null;
            //securityInstrument.OtherFeesAmount = null;
            //securityInstrument.OtherFeesDescription = null;
            securityInstrument.OweltyOfPartitionIndicator = ToMismo(m_dataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.OweltyOfPartition);
            //securityInstrument.PersonAuthorizedToReleaseLienName = null;
            //securityInstrument.PersonAuthorizedToReleaseLienPhoneNumber = null;
            //securityInstrument.PersonAuthorizedToReleaseLienTitle = null;
            securityInstrument.PurchaseMoneyIndicator = ToMismo(m_dataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.PurchaseMoney);
            //securityInstrument.RealPropertyImprovedOrToBeImprovedIndicator = null;
            //securityInstrument.RealPropertyImprovementsNotCoveredIndicator = null;
            //securityInstrument.RecordingRequestedByName = null;
            securityInstrument.RenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator = 
                ToMismo(m_dataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.RenewalAndExtensionOfLiens);
            //securityInstrument.SignerForRegistersOfficeName = null;
            //securityInstrument.TaxSerialNumberIdentifier = null;
            //securityInstrument.TrusteeFeePercent = null;
            //securityInstrument.VendorsLienDescription = null;
            //securityInstrument.VendorsLienIndicator = null;
            securityInstrument.VestingDescription = m_dataLoan.sVestingToRead;
            //securityInstrument.TaxablePartyList.Add(CreateTaxableParty());
            return securityInstrument;
        }

        private TaxableParty CreateTaxableParty()
        {
            TaxableParty taxableParty = new TaxableParty();
            //taxableParty.Id = null;
            //taxableParty.City = null;
            //taxableParty.Country = null;
            //taxableParty.CountryCode = null;
            //taxableParty.County = null;
            //taxableParty.PostalCode = null;
            //taxableParty.SequenceIdentifier = null;
            //taxableParty.State = null;
            //taxableParty.StreetAddress = null;
            //taxableParty.StreetAddress2 = null;
            //taxableParty.TitleDescription = null;
            //taxableParty.UnparsedName = null;
            //taxableParty.PreferredResponseList.Add(CreatePreferredResponse());
            //taxableParty.ContactDetail = CreateContactDetail();
            return taxableParty;
        }

        private PreferredResponse CreatePreferredResponse()
        {
            PreferredResponse preferredResponse = new PreferredResponse();
            //preferredResponse.Id = null;
            //preferredResponse.MimeType = null;
            //preferredResponse.Destination = null;
            //preferredResponse.Format = null;
            //preferredResponse.FormatOtherDescription = null;
            //preferredResponse.Method = null;
            //preferredResponse.MethodOther = null;
            //preferredResponse.UseEmbeddedFileIndicator = null;
            //preferredResponse.VersionIdentifier = null;
            return preferredResponse;
        }

        private Trustee CreateTrustee()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Trustee, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }

            Trustee trustee = new Trustee();
            //trustee.Id = null;
            //trustee.NonPersonEntityIndicator = null;
            trustee.City = agent.City;
            //trustee.Country = null;
            //trustee.County = null;
            trustee.PostalCode = agent.Zip;
            trustee.State = agent.State;
            trustee.StreetAddress = agent.StreetAddr;
            //trustee.StreetAddress2 = null;
            //trustee.Type = null;
            //trustee.TypeOtherDescription = null;
            trustee.UnparsedName = agent.CompanyName;
            trustee.ContactDetail = CreateContactDetail(agent);
            //trustee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return trustee;
        }

        private Witness CreateWitness()
        {
            Witness witness = new Witness();
            //witness.Id = null;
            //witness.SequenceIdentifier = null;
            //witness.UnparsedName = null;
            return witness;
        }

        private void PopulateRespaHudDetailList(List<RespaHudDetail> ls)
        {
            // Gross amount due
            // 101 Contract sales price
            // Unmapped

            // 102 Personal property
            ls.Add(CreateRespaHudDetail(
                "102",
                "Personal property",
                m_dataLoan.sGrossDueFromBorrPersonalProperty_rep,
                null,
                null,
                true,
                false));

            // 103 Settlement charges to borrower
            // Unmapped

            // 104 - Do not send 104: reserved for payoffs OPM 80612, m.p.
            //ls.Add(CreateRespaHudDetail(
            //    "104",
            //    m_dataLoan.sGrossDueFromBorrU1FDesc,
            //    m_dataLoan.sGrossDueFromBorrU1F_rep,
            //    null,
            //    null,
            //    true,
            //    false));

            // 105
            ls.Add(CreateRespaHudDetail(
                "105",
                m_dataLoan.sGrossDueFromBorrU2FDesc,
                m_dataLoan.sGrossDueFromBorrU2F_rep,
                null,
                null,
                true,
                false));

            // 401 Contract sales price
            // Unmapped
            
            // 402 Personal property
            ls.Add(CreateRespaHudDetail(
                "402",
                "Personal property",
                m_dataLoan.sGrossDueToSellerPersonalProperty_rep,
                null,
                null,
                false,
                true));

            // 403
            ls.Add(CreateRespaHudDetail(
                "403",
                m_dataLoan.sGrossDueToSellerU1FDesc,
                m_dataLoan.sGrossDueToSellerU1F_rep,
                null,
                null,
                false,
                true));

            // 404
            ls.Add(CreateRespaHudDetail(
                "404",
                m_dataLoan.sGrossDueToSellerU2FDesc,
                m_dataLoan.sGrossDueToSellerU2F_rep,
                null,
                null,
                false,
                true));

            // 405
            ls.Add(CreateRespaHudDetail(
                "405",
                m_dataLoan.sGrossDueToSellerU3FDesc,
                m_dataLoan.sGrossDueToSellerU3F_rep,
                null,
                null,
                false,
                true));

            // Adjustments for items paid by seller in advance
            // 106/406 City/town taxes
            ls.Add(CreateRespaHudDetail(
                "106",
                "City/town taxes",
                m_dataLoan.sPdBySellerCityTaxF_rep,
                m_dataLoan.sPdBySellerCityTaxFStartD_rep,
                m_dataLoan.sPdBySellerCityTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "406",
                "City/town taxes",
                m_dataLoan.sPdBySellerCityTaxF_rep,
                m_dataLoan.sPdBySellerCityTaxFStartD_rep,
                m_dataLoan.sPdBySellerCityTaxFEndD_rep,
                false,
                true));

            // 107/407 County taxes
            ls.Add(CreateRespaHudDetail(
                "107",
                "County taxes",
                m_dataLoan.sPdBySellerCountyTaxF_rep,
                m_dataLoan.sPdBySellerCountyTaxFStartD_rep,
                m_dataLoan.sPdBySellerCountyTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "407",
                "County taxes",
                m_dataLoan.sPdBySellerCountyTaxF_rep,
                m_dataLoan.sPdBySellerCountyTaxFStartD_rep,
                m_dataLoan.sPdBySellerCountyTaxFEndD_rep,
                false,
                true));

            // 108/408 Assessments
            ls.Add(CreateRespaHudDetail(
                "108",
                "Assessments",
                m_dataLoan.sPdBySellerAssessmentsTaxF_rep,
                m_dataLoan.sPdBySellerAssessmentsTaxFStartD_rep,
                m_dataLoan.sPdBySellerAssessmentsTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "408",
                "Assessments",
                m_dataLoan.sPdBySellerAssessmentsTaxF_rep,
                m_dataLoan.sPdBySellerAssessmentsTaxFStartD_rep,
                m_dataLoan.sPdBySellerAssessmentsTaxFEndD_rep,
                false,
                true));

            // For 109-112/409-412 and 213-219/513-519, append "{startDate} to {endDate}" as part of the description,
            // and do not include the HUD1LineItemFromDate and HUD1LineItemToDate attributes for now
            // This function will take in the start date and the end date and return a string: "{startDate} to {endDate}" if
            // both are nonempty, "{date}" if only one is nonempty, "" if neither are nonempty.
            Func<CDateTime, CDateTime, string> JoinDates = (start, end) => string.Join(" to ", (new string[] { start.ToString("MM/dd/yy"), end.ToString("MM/dd/yy")}).Where((x) => !string.IsNullOrEmpty(x)).ToArray());

            // 109/409
            var itemsPdBySellerInAdvance = new[]
            {
                new { Hudline = "09", Desc = m_dataLoan.sPdBySellerU1FDesc, Amt = m_dataLoan.sPdBySellerU1F_rep, StartD = m_dataLoan.sPdBySellerU1FStartD, EndD = m_dataLoan.sPdBySellerU1FEndD },
                new { Hudline = "10", Desc = m_dataLoan.sPdBySellerU2FDesc, Amt = m_dataLoan.sPdBySellerU2F_rep, StartD = m_dataLoan.sPdBySellerU2FStartD, EndD = m_dataLoan.sPdBySellerU2FEndD },
                new { Hudline = "11", Desc = m_dataLoan.sPdBySellerU3FDesc, Amt = m_dataLoan.sPdBySellerU3F_rep, StartD = m_dataLoan.sPdBySellerU3FStartD, EndD = m_dataLoan.sPdBySellerU3FEndD },
                new { Hudline = "12", Desc = m_dataLoan.sPdBySellerU4FDesc, Amt = m_dataLoan.sPdBySellerU4F_rep, StartD = m_dataLoan.sPdBySellerU4FStartD, EndD = m_dataLoan.sPdBySellerU4FEndD },
            };

            // Add the lines which have a description
            foreach (var item in itemsPdBySellerInAdvance)
            {
                if (string.IsNullOrEmpty(item.Desc))
                {
                    continue;
                }

                ls.Add(CreateRespaHudDetail(
                    "1" + item.Hudline,
                    item.Desc + " " + JoinDates(item.StartD, item.EndD),
                    item.Amt,
                    null,
                    null,
                    true,
                    false));

                ls.Add(CreateRespaHudDetail(
                    "4" + item.Hudline,
                    item.Desc + " " + JoinDates(item.StartD, item.EndD),
                    item.Amt,
                    null,
                    null,
                    false,
                    true));
            }

            // 200. Amount Paid by or in Behalf of Borrower
            // Unmapped

            // 201 Deposit or earnest money
            ls.Add(CreateRespaHudDetail(
                "201",
                "Deposit or Earnest money",
                m_dataLoan.sTotCashDeposit_rep,
                null,
                null,
                true,
                false));

            // 202 Principal amount of new loans
            // Unmapped

            // 20X Custom lines
            var itemsPdByBorrower = new []
            {
                new { Hudline = m_dataLoan.sPdByBorrowerU1FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU1FDesc, Amt = m_dataLoan.sPdByBorrowerU1F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU2FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU2FDesc, Amt = m_dataLoan.sPdByBorrowerU2F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU3FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU3FDesc, Amt = m_dataLoan.sPdByBorrowerU3F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU4FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU4FDesc, Amt = m_dataLoan.sPdByBorrowerU4F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU5FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU5FDesc, Amt = m_dataLoan.sPdByBorrowerU5F_rep },
                new { Hudline = m_dataLoan.sPdByBorrowerU6FHudline_rep, Desc = m_dataLoan.sPdByBorrowerU6FDesc, Amt = m_dataLoan.sPdByBorrowerU6F_rep },
            };

            // Add the lines which have a description
            foreach (var item in itemsPdByBorrower)
            {
                if (string.IsNullOrEmpty(item.Desc))
                {
                    continue;
                }

                ls.Add(CreateRespaHudDetail(
                    item.Hudline,
                    item.Desc,
                    item.Amt,
                    null,
                    null,
                    true,
                    false));
            }

            // 500 Reductions in Amount due to Seller
            // 501 Excess deposit
            ls.Add(CreateRespaHudDetail(
                "501",
                "Excess deposit",
                m_dataLoan.sReductionsDueToSellerExcessDeposit_rep,
                null,
                null,
                false,
                true));

            // 502 Settlement charges to seller (line 1400)
            // Unmapped

            // 504 Payoff of first mortgage loan
            ls.Add(CreateRespaHudDetail(
                "504",
                "Payoff of first mortgage loan",
                m_dataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep,
                null,
                null,
                false,
                true));

            // 505 Payoff of second mortgage loan
            ls.Add(CreateRespaHudDetail(
                "505",
                "Payoff of second mortgage loan",
                m_dataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep,
                null,
                null,
                false,
                true));

            // 50X Custom lines
            var itemsReducedDueToSeller = new[]
            {
                new { Hudline = m_dataLoan.sReductionsDueToSellerU1FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU1FDesc, Amt = m_dataLoan.sReductionsDueToSellerU1F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU2FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU2FDesc, Amt = m_dataLoan.sReductionsDueToSellerU2F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU3FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU3FDesc, Amt = m_dataLoan.sReductionsDueToSellerU3F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU4FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU4FDesc, Amt = m_dataLoan.sReductionsDueToSellerU4F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU5FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU5FDesc, Amt = m_dataLoan.sReductionsDueToSellerU5F_rep },
                new { Hudline = m_dataLoan.sReductionsDueToSellerU6FHudline_rep, Desc = m_dataLoan.sReductionsDueToSellerU6FDesc, Amt = m_dataLoan.sReductionsDueToSellerU6F_rep },
            };

            // Add the lines which have a description
            foreach (var item in itemsReducedDueToSeller)
            {
                if (string.IsNullOrEmpty(item.Desc))
                {
                    continue;
                }

                ls.Add(CreateRespaHudDetail(
                    item.Hudline,
                    item.Desc,
                    item.Amt,
                    null,
                    null,
                    false,
                    true));
            }

            // Adjustments for items unpaid by seller
            // 210/510 City/town taxes
            ls.Add(CreateRespaHudDetail(
                "210",
                "City/town taxes",
                m_dataLoan.sUnPdBySellerCityTaxF_rep,
                m_dataLoan.sUnPdBySellerCityTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerCityTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "510",
                "City/town taxes",
                m_dataLoan.sUnPdBySellerCityTaxF_rep,
                m_dataLoan.sUnPdBySellerCityTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerCityTaxFEndD_rep,
                false,
                true));

            // 211/511 County taxes
            ls.Add(CreateRespaHudDetail(
                "211",
                "County taxes",
                m_dataLoan.sUnPdBySellerCountyTaxF_rep,
                m_dataLoan.sUnPdBySellerCountyTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerCountyTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "511",
                "County taxes",
                m_dataLoan.sUnPdBySellerCountyTaxF_rep,
                m_dataLoan.sUnPdBySellerCountyTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerCountyTaxFEndD_rep,
                false,
                true));

            // 212/512 Assessments
            ls.Add(CreateRespaHudDetail(
                "212",
                "Assessments",
                m_dataLoan.sUnPdBySellerAssessmentsTaxF_rep,
                m_dataLoan.sUnPdBySellerAssessmentsTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerAssessmentsTaxFEndD_rep,
                true,
                false));

            ls.Add(CreateRespaHudDetail(
                "512",
                "Assessments",
                m_dataLoan.sUnPdBySellerAssessmentsTaxF_rep,
                m_dataLoan.sUnPdBySellerAssessmentsTaxFStartD_rep,
                m_dataLoan.sUnPdBySellerAssessmentsTaxFEndD_rep,
                false,
                true));

            // 213/513
            // 214/514
            // 215/515
            // 216/516
            // 217/517
            // 218/518
            // 219/519
            var itemsUnPdBySeller = new[]
            {
                new { Hudline = "13", Desc = m_dataLoan.sUnPdBySellerU1FDesc, Amt = m_dataLoan.sUnPdBySellerU1F_rep, StartD = m_dataLoan.sUnPdBySellerU1FStartD, EndD = m_dataLoan.sUnPdBySellerU1FEndD },
                new { Hudline = "14", Desc = m_dataLoan.sUnPdBySellerU2FDesc, Amt = m_dataLoan.sUnPdBySellerU2F_rep, StartD = m_dataLoan.sUnPdBySellerU2FStartD, EndD = m_dataLoan.sUnPdBySellerU2FEndD },
                new { Hudline = "15", Desc = m_dataLoan.sUnPdBySellerU3FDesc, Amt = m_dataLoan.sUnPdBySellerU3F_rep, StartD = m_dataLoan.sUnPdBySellerU3FStartD, EndD = m_dataLoan.sUnPdBySellerU3FEndD },
                new { Hudline = "16", Desc = m_dataLoan.sUnPdBySellerU4FDesc, Amt = m_dataLoan.sUnPdBySellerU4F_rep, StartD = m_dataLoan.sUnPdBySellerU4FStartD, EndD = m_dataLoan.sUnPdBySellerU4FEndD },
                new { Hudline = "17", Desc = m_dataLoan.sUnPdBySellerU5FDesc, Amt = m_dataLoan.sUnPdBySellerU5F_rep, StartD = m_dataLoan.sUnPdBySellerU5FStartD, EndD = m_dataLoan.sUnPdBySellerU5FEndD },
                new { Hudline = "18", Desc = m_dataLoan.sUnPdBySellerU6FDesc, Amt = m_dataLoan.sUnPdBySellerU6F_rep, StartD = m_dataLoan.sUnPdBySellerU6FStartD, EndD = m_dataLoan.sUnPdBySellerU6FEndD },
                new { Hudline = "19", Desc = m_dataLoan.sUnPdBySellerU7FDesc, Amt = m_dataLoan.sUnPdBySellerU7F_rep, StartD = m_dataLoan.sUnPdBySellerU7FStartD, EndD = m_dataLoan.sUnPdBySellerU7FEndD },
            };

            // Add the lines which have a description
            foreach (var item in itemsUnPdBySeller)
            {
                if (string.IsNullOrEmpty(item.Desc))
                {
                    continue;
                }

                ls.Add(CreateRespaHudDetail(
                    "2" + item.Hudline,
                    item.Desc + " " + JoinDates(item.StartD, item.EndD),
                    item.Amt,
                    null,
                    null,
                    true,
                    false));

                ls.Add(CreateRespaHudDetail(
                    "5" + item.Hudline,
                    item.Desc + " " + JoinDates(item.StartD, item.EndD),
                    item.Amt,
                    null,
                    null,
                    false,
                    true));
            }

        }

        private RespaHudDetail CreateRespaHudDetail(
            string _specifiedHudLineNumber,
            string _lineItemDescription,
            string _lineItemAmount,
            string _HUD1LineItemFromDate,
            string _HUD1LineItemToDate,
            bool _HUD1CashToOrFromBorrowerIndicator,
            bool _HUD1CashToOrFromSellerIndicator)
        {
            if (string.IsNullOrEmpty(_lineItemAmount) || _lineItemAmount == "0.00")
                return null;

            RespaHudDetail respaHudDetail = new RespaHudDetail();
            respaHudDetail.SpecifiedHudLineNumber = _specifiedHudLineNumber;
            respaHudDetail.LineItemDescription = _lineItemDescription;
            respaHudDetail.LineItemAmount = _lineItemAmount;
            //respaHudDetail.LineItemPaidByType = null;
            //respaHudDetail.LineItemPaidByTypeOtherDescription = null;
            respaHudDetail.Hud1LineItemFromDate = _HUD1LineItemFromDate;
            respaHudDetail.Hud1LineItemToDate = _HUD1LineItemToDate;
            respaHudDetail.Hud1CashToOrFromBorrowerIndicator = ToYNIndicator(_HUD1CashToOrFromBorrowerIndicator);
            respaHudDetail.Hud1CashToOrFromSellerIndicator = ToYNIndicator(_HUD1CashToOrFromSellerIndicator);

            //respaHudDetail.Id = null;
            //respaHudDetail.Hud1ConventionalInsuredIndicator = null;
            //respaHudDetail.Hud1FileNumberIdentifier = null;
            //respaHudDetail.Hud1LenderUnparsedAddress = null;
            //respaHudDetail.Hud1LenderUnparsedName = null;
            //respaHudDetail.Hud1SettlementAgentUnparsedAddress = null;
            //respaHudDetail.Hud1SettlementAgentUnparsedName = null;
            //respaHudDetail.Hud1SettlementDate = null;
            
            return respaHudDetail;
        }

        private RespaServicingData CreateRespaServicingData()
        {
            RespaServicingData respaServicingData = new RespaServicingData();
            //respaServicingData.Id = null;
            //respaServicingData.AreAbleToServiceIndicator = null;
            //respaServicingData.AssignSellOrTransferSomeServicingIndicator = ToYNIndicator(m_dataLoan.sAppliedProgramTransfered); // OBSOLETE
            //respaServicingData.ExpectToAssignSellOrTransferPercent = null;
            //respaServicingData.ExpectToAssignSellOrTransferPercentOfServicingIndicator = null;
            //respaServicingData.ExpectToRetainAllServicingIndicator = null;
            //respaServicingData.ExpectToSellAllServicingIndicator = null;
            //respaServicingData.FirstTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear1; // OBSOLETE
            //respaServicingData.FirstTransferYearValue = m_dataLoan.sFirstLienLoanTransferRecordYear1Percent; // OBSOLETE
            //respaServicingData.HaveNotDecidedToServiceIndicator = null;
            //respaServicingData.HaveNotServicedInPastThreeYearsIndicator = ToYNIndicator(m_dataLoan.sDontServiceInPast3Yrs); // OBSOLETE
            //respaServicingData.HavePreviouslyAssignedServicingIndicator = ToYNIndicator(m_dataLoan.sFirstLienLoanPreviouslyTransfered); // OBSOLETE
            //respaServicingData.SecondTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear2; // OBSOLETE
            //respaServicingData.SecondTransferYearValue = m_dataLoan.sFirstLienLoanTransferRecordYear2Percent; // OBSOLETE
            //respaServicingData.ThirdTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear3; // OBSOLETE
            //respaServicingData.ThirdTransferYearValue = m_dataLoan.sFirstLienLoanTransferRecordYear3Percent; // OBSOLETE
            //respaServicingData.ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator = null;
            //respaServicingData.ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator = null;
            //respaServicingData.ThisIsOurRecordOfTransferringServicingIndicator = ToYNIndicator(m_dataLoan.sFirstLienLoanDeclareServiceRecord); // OBSOLETE
            //respaServicingData.TwelveMonthPeriodTransferPercent = null;
            //respaServicingData.WillNotServiceIndicator = null;
            //respaServicingData.WillServiceIndicator = null;
            respaServicingData.MayAssignServicingIndicator = ToMismo(m_dataLoan.sMayAssignSellTransfer);

            if (m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                // The DoNotServiceIndicator depends on whether there is possible intent to service
                // or whether the loan will explicitly not be serviced by the lender. We take both
                // the Servicing Disclosure and LE datapoints into account, with the LE taking precedence
                // if it conflicts with the Servicing Disclosure. (OPM 246737) 
                respaServicingData.DoNotServiceIndicator = ToMismo(
                    !m_dataLoan.sMayAssignSellTransfer &&
                    !m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan &&
                    (!m_dataLoan.sDontService ||
                    !m_dataLoan.sServiceDontIntendToTransfer));
                respaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator = ToMismo(!m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan);
                respaServicingData.WillServiceIndicator = ToMismo(m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan);
            }
            else
            {
                if (m_dataLoan.sMayAssignSellTransfer || m_dataLoan.sDontService || m_dataLoan.sServiceDontIntendToTransfer)
                {
                    respaServicingData.DoNotServiceIndicator = ToMismo(m_dataLoan.sDontService);
                }

                if (m_dataLoan.sDontService || m_dataLoan.sServiceDontIntendToTransfer)
                {
                    respaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator = ToMismo(m_dataLoan.sDontService);
                    respaServicingData.WillServiceIndicator = ToMismo(m_dataLoan.sServiceDontIntendToTransfer);
                }
            }
            return respaServicingData;
        }

        private RespaSummary CreateRespaSummary()
        {
            RespaSummary respaSummary = new RespaSummary();
            //respaSummary.Id = null;
            //respaSummary.Apr = null;
            if (m_dataLoan.sIsPropertyBeingSoldByCreditor)
                respaSummary.DisclosedTotalSalesPriceAmount = m_dataLoan.sPurchPrice_rep;
            //respaSummary.TotalAprFeesAmount = null;
            //respaSummary.TotalAmountFinancedAmount = null;
            //respaSummary.TotalDepositedReservesAmount = null;
            //respaSummary.TotalFilingRecordingFeeAmount = null;
            //respaSummary.TotalFinanceChargeAmount = null;
            //respaSummary.TotalNetBorrowerFeesAmount = null;
            //respaSummary.TotalNetProceedsForFundingAmount = null;
            //respaSummary.TotalNetSellerFeesAmount = null;
            //respaSummary.TotalNonAprFeesAmount = null;
            //respaSummary.TotalOfAllPaymentsAmount = null;
            //respaSummary.TotalPaidToOthersAmount = null;
            //respaSummary.TotalPrepaidFinanceChargeAmount = null;
            //respaSummary.TotalFeesPaidToList.Add(CreateTotalFeesPaidTo());
            //respaSummary.TotalFeesPaidByList.Add(CreateTotalFeesPaidBy());
            return respaSummary;
        }

        private TotalFeesPaidTo CreateTotalFeesPaidTo()
        {
            TotalFeesPaidTo totalFeesPaidTo = new TotalFeesPaidTo();
            //totalFeesPaidTo.Id = null;
            //totalFeesPaidTo.Type = null;
            //totalFeesPaidTo.TypeAmount = null;
            //totalFeesPaidTo.TypeOtherDescription = null;
            return totalFeesPaidTo;
        }

        private TotalFeesPaidBy CreateTotalFeesPaidBy()
        {
            TotalFeesPaidBy totalFeesPaidBy = new TotalFeesPaidBy();
            //totalFeesPaidBy.Id = null;
            //totalFeesPaidBy.Type = null;
            //totalFeesPaidBy.TypeAmount = null;
            //totalFeesPaidBy.TypeOtherDescription = null;
            return totalFeesPaidBy;
        }

        private Seller CreateSeller(DataAccess.Sellers.Seller sellerInput)
        {
            Seller seller = new Seller();
            //seller.Id = null;
            seller.NonPersonEntityIndicator = ToMismo(sellerInput.Type == E_SellerType.Entity);
            seller.City = sellerInput.Address.City;
            //seller.Country = null;
            //seller.FirstName = null;
            //seller.Identifier = null;
            //seller.LastName = null;
            //seller.MiddleName = null;
            //seller.NameSuffix = null;
            seller.PostalCode = sellerInput.Address.PostalCode;
            //seller.SequenceIdentifier = null;
            seller.State = sellerInput.Address.State;
            seller.StreetAddress = sellerInput.Address.StreetAddress;
            //seller.StreetAddress2 = null;
            seller.UnparsedName = sellerInput.Name;
            //seller.ContactDetail = CreateContactDetail();
            seller.NonPersonEntityDetail = CreateNonPersonEntityDetail(sellerInput.EntityType);
            //seller.PowerOfAttorney = CreatePowerOfAttorney();
            return seller;
        }

        private NonPersonEntityDetail CreateNonPersonEntityDetail(E_SellerEntityType sellerEntityType)
        {
            if (sellerEntityType == E_SellerEntityType.Undefined) return null;
            NonPersonEntityDetail detail = new NonPersonEntityDetail();
            detail.OrganizationType = ToMismo(sellerEntityType);
            if (detail.OrganizationType == E_NonPersonEntityDetailOrganizationType.Other)
            {
                detail.OrganizationTypeOtherDescription = sellerEntityType.ToString();
            }
            return detail;
        }

        private E_NonPersonEntityDetailOrganizationType ToMismo(E_SellerEntityType sellerEntityType)
        {
            switch (sellerEntityType)
            {
                case E_SellerEntityType.Corporation: return E_NonPersonEntityDetailOrganizationType.Corporation;
                case E_SellerEntityType.GeneralPartnership: return E_NonPersonEntityDetailOrganizationType.Partnership;
                case E_SellerEntityType.LimitedLiabilityCompany: return E_NonPersonEntityDetailOrganizationType.LimitedLiabilityCompany;
                case E_SellerEntityType.LimitedPartnership: return E_NonPersonEntityDetailOrganizationType.LimitedPartnership;
                case E_SellerEntityType.SoleProprietor: return E_NonPersonEntityDetailOrganizationType.SoleProprietorship;
                case E_SellerEntityType.Company:
                case E_SellerEntityType.LimitedLiabilityCorporation: return E_NonPersonEntityDetailOrganizationType.Other;
                default:
                    return E_NonPersonEntityDetailOrganizationType.Undefined;
            }
        }

        private Servicer CreateServicer()
        {
            Servicer servicer = new Servicer();
            //servicer.Id = null;
            //servicer.NonPersonEntityIndicator = null;
            //servicer.City = null;
            //servicer.Country = null;
            //servicer.DaysOfTheWeekDescription = null;
            //servicer.DirectInquiryToDescription = null;
            //servicer.HoursOfTheDayDescription = null;
            //servicer.InquiryTelephoneNumber = null;
            //servicer.Name = null;
            //servicer.PaymentAcceptanceEndDate = null;
            //servicer.PaymentAcceptanceStartDate = null;
            //servicer.PostalCode = null;
            //servicer.State = null;
            //servicer.StreetAddress = null;
            //servicer.StreetAddress2 = null;
            //servicer.TransferEffectiveDate = null;
            //servicer.Type = null;
            //servicer.TypeOtherDescription = null;
            //servicer.ContactDetail = CreateContactDetail();
            //servicer.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            //servicer.QualifiedWrittenRequestMailTo = CreateQualifiedWrittenRequestMailTo();
            return servicer;
        }

        private QualifiedWrittenRequestMailTo CreateQualifiedWrittenRequestMailTo()
        {
            QualifiedWrittenRequestMailTo qualifiedWrittenRequestMailTo = new QualifiedWrittenRequestMailTo();
            //qualifiedWrittenRequestMailTo.Id = null;
            //qualifiedWrittenRequestMailTo.City = null;
            //qualifiedWrittenRequestMailTo.Country = null;
            //qualifiedWrittenRequestMailTo.PostalCode = null;
            //qualifiedWrittenRequestMailTo.State = null;
            //qualifiedWrittenRequestMailTo.StreetAddress = null;
            //qualifiedWrittenRequestMailTo.StreetAddress2 = null;
            return qualifiedWrittenRequestMailTo;
        }

        private Builder CreateBuilder()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Builder, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }
            Builder builder = new Builder();
            //builder.Id = null;
            //builder.NonPersonEntityIndicator = null;
            builder.City = agent.City;
            //builder.Country = null;
            //builder.County = null;
            builder.LIcenseIdentifier = agent.LicenseNumOfCompany;
            //builder.LicenseState = null;
            builder.PostalCode = agent.Zip;
            builder.City = agent.City;
            builder.State = agent.State;
            builder.StreetAddress = agent.StreetAddr;
            //builder.StreetAddress2 = null;
            builder.UnparsedName = agent.CompanyName;
            builder.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            builder.ContactDetail.Name = agent.AgentName;
            return builder;
        }

        private Mismo.Closing2_6.ClosingCost CreateClosingCost()
        {
            Mismo.Closing2_6.ClosingCost closingCost = new Mismo.Closing2_6.ClosingCost();
            //closingCost.Id = null;
            //closingCost.ContributionAmount = null;
            //closingCost.FinancedIndicator = null;
            //closingCost.FundsType = null;
            //closingCost.FundsTypeOtherDescription = null;
            //closingCost.SourceType = null;
            //closingCost.SourceTypeOtherDescription = null;
            return closingCost;
        }

        private Trust CreateTrust()
        {
            Trust trust = new Trust();
            //trust.Id = null;
            trust.NonPersonEntityIndicator = E_YNIndicator.Y;
            trust.EstablishedDate = m_dataLoan.sTrustAgreementD_rep;
            trust.Name = m_dataLoan.sTrustName;
            //trust.State = null;
            //trust.NonObligatedIndicator = null;
            trust.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            trust.NonPersonEntityDetail.TaxIdentificationNumberIdentifier = m_dataLoan.sTrustId;
            //trust.BeneficiaryList.Add(CreateBeneficiary());



            var trustCollection = m_dataLoan.sTrustCollection;
            foreach (var trustee in trustCollection.Trustees)
            {
                if (string.IsNullOrWhiteSpace(trustee.Name))
                {
                    continue;
                }

                var temp = new Trustee();
                temp.NonPersonEntityIndicator = E_YNIndicator.Y;
                temp.UnparsedName = trustee.Name;

                if (trustee.CanSignLoanDocs)
                {
                    temp.ContactDetail = new ContactDetail();
                    temp.ContactDetail.ContactPointList.Add(
                        CreateContactPoint(E_ContactPointRoleType.Work, Mismo.Closing2_6.E_ContactPointType.Phone, trustee.Phone));
                    temp.StreetAddress = trustee.Address.StreetAddress;
                    temp.City = trustee.Address.City;
                    temp.State = trustee.Address.State;
                    temp.PostalCode = trustee.Address.PostalCode;
                }

                if (trustee.CanSignCertOfTrust)
                {
                    var authorizedRep = new AuthorizedRepresentative();
                    authorizedRep.AuthorizedToSignIndicator = E_YNIndicator.Y;
                    authorizedRep.UnparsedName = trustee.Name;

                    trust.NonPersonEntityDetail.AuthorizedRepresentativeList.Add(authorizedRep);
                }
                trust.TrusteeList.Add(temp);
            }

            foreach (var grantor in trustCollection.Trustors)
            {
                if (string.IsNullOrWhiteSpace(grantor.Name))
                {
                    continue;
                }

                var temp = CreateGrantor();
                temp.UnparsedName = grantor.Name;
                trust.GrantorList.Add(temp);
            }
            return trust;
        }

        private Grantor CreateGrantor()
        {
            Grantor grantor = new Grantor();
            //grantor.Id = null;
            //grantor.MaritalStatusType = null;
            //grantor.NonPersonEntityIndicator = null;
            //grantor.CapacityDescription = null;
            //grantor.City = null;
            //grantor.Country = null;
            //grantor.County = null;
            //grantor.FirstName = null;
            //grantor.LastName = null;
            //grantor.MiddleName = null;
            //grantor.NameSuffix = null;
            //grantor.PostalCode = null;
            //grantor.SequenceIdentifier = null;
            //grantor.State = null;
            //grantor.StreetAddress = null;
            //grantor.StreetAddress2 = null;
            grantor.UnparsedName = null;
            //grantor.AliasList.Add(CreateAlias());
            return grantor;
        }

        private EscrowAccountSummary CreateEscrowAccountSummary()
        {
            EscrowAccountSummary escrowAccountSummary = new EscrowAccountSummary();
            
            if (mxGfe || m_use2015DataLayer)
            {
                escrowAccountSummary.EscrowAggregateAccountingAdjustmentAmount = m_dataLoanWithGfeArchiveApplied.sAggregateAdjRsrv_rep;
            }
            else
            {
                escrowAccountSummary.EscrowAggregateAccountingAdjustmentAmount = m_dataLoan.sSettlementAggregateAdjRsrv_rep;
            }

            //escrowAccountSummary.EscrowCushionNumberOfMonthsCount = null;
            return escrowAccountSummary;
        }

        private E_BorrowerMaritalStatusType ToMismo(E_aBMaritalStatT aBMaritalStatT)
        {
            switch (aBMaritalStatT)
            {
                case E_aBMaritalStatT.Married: return E_BorrowerMaritalStatusType.Married;
                case E_aBMaritalStatT.NotMarried: return E_BorrowerMaritalStatusType.Unmarried;
                case E_aBMaritalStatT.Separated: return E_BorrowerMaritalStatusType.Separated;
                case E_aBMaritalStatT.LeaveBlank: return E_BorrowerMaritalStatusType.NotProvided;
                default:
                    return E_BorrowerMaritalStatusType.Undefined;
            }
        }

        private E_BorrowerPrintPositionType ToMismo(E_BorrowerModeT BorrowerModeT)
        {
            switch (BorrowerModeT)
            {
                case E_BorrowerModeT.Borrower: return E_BorrowerPrintPositionType.Borrower;
                case E_BorrowerModeT.Coborrower: return E_BorrowerPrintPositionType.CoBorrower;
                default:
                    return E_BorrowerPrintPositionType.Undefined;
            }
        }

        private E_MortgageTermsMortgageType ToMismo(E_sLT sLT, string sLpTemplateNm, bool sIsLineOfCredit)
        {
            if (sIsLineOfCredit)
            {
                return E_MortgageTermsMortgageType.HELOC;
            }
            switch (sLT)
            {
                case E_sLT.Conventional: return E_MortgageTermsMortgageType.Conventional;
                case E_sLT.FHA: return E_MortgageTermsMortgageType.FHA;
                case E_sLT.VA: return E_MortgageTermsMortgageType.VA;
                case E_sLT.UsdaRural: return E_MortgageTermsMortgageType.FarmersHomeAdministration;
                case E_sLT.Other:
                    if (!string.IsNullOrEmpty(sLpTemplateNm) && sLpTemplateNm.ToUpper().Contains("HELOC"))
                        return E_MortgageTermsMortgageType.HELOC;
                    else
                        return E_MortgageTermsMortgageType.Other;
                default:
                    return E_MortgageTermsMortgageType.Undefined;
            }
        }

        private E_MortgageTermsLoanAmortizationType ToMismo(E_sFinMethT sFinMethT)
        {
            switch (sFinMethT)
            {
                case E_sFinMethT.Fixed: return E_MortgageTermsLoanAmortizationType.Fixed;
                case E_sFinMethT.ARM: return E_MortgageTermsLoanAmortizationType.AdjustableRate;
                case E_sFinMethT.Graduated: return E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage;
                default:
                    return E_MortgageTermsLoanAmortizationType.Undefined;
            }
        }

        private E_LoanFeaturesLienPriorityType ToMismo(E_sLienPosT sLienPosT)
        {
            switch (sLienPosT)
            {
                case E_sLienPosT.First: return E_LoanFeaturesLienPriorityType.FirstLien;
                case E_sLienPosT.Second: return E_LoanFeaturesLienPriorityType.SecondLien;
                default:
                    return E_LoanFeaturesLienPriorityType.Undefined;
            }
        }
        private E_LoanFeaturesGsePropertyType ToMismo(E_sGseSpT sGseSpT)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.LeaveBlank: return E_LoanFeaturesGsePropertyType.Undefined;
                case E_sGseSpT.Attached: return E_LoanFeaturesGsePropertyType.Attached;
                case E_sGseSpT.Condominium: return E_LoanFeaturesGsePropertyType.Condominium;
                case E_sGseSpT.Cooperative: return E_LoanFeaturesGsePropertyType.Cooperative;
                case E_sGseSpT.Detached: return E_LoanFeaturesGsePropertyType.Detached;
                case E_sGseSpT.DetachedCondominium: return E_LoanFeaturesGsePropertyType.DetachedCondominium;
                case E_sGseSpT.HighRiseCondominium: return E_LoanFeaturesGsePropertyType.HighRiseCondominium;
                case E_sGseSpT.ManufacturedHomeCondominium: return E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominium;
                case E_sGseSpT.ManufacturedHomeMultiwide: return E_LoanFeaturesGsePropertyType.ManufacturedHousingMultiWide;
                case E_sGseSpT.ManufacturedHousing: return E_LoanFeaturesGsePropertyType.ManufacturedHousing;
                case E_sGseSpT.ManufacturedHousingSingleWide: return E_LoanFeaturesGsePropertyType.ManufacturedHousingSingleWide;
                case E_sGseSpT.Modular: return E_LoanFeaturesGsePropertyType.Modular;
                case E_sGseSpT.PUD: return E_LoanFeaturesGsePropertyType.PUD;
                default:
                    return E_LoanFeaturesGsePropertyType.Undefined;
            }
        }
        private E_YNIndicator ToYNIndicator(bool value)
        {
            return value ? E_YNIndicator.Y : E_YNIndicator.N;
        }
        private E_InterviewerInformationApplicationTakenMethodType ToMismo(E_aIntrvwrMethodT aIntrvwrMethodT)
        {
            switch (aIntrvwrMethodT)
            {
                case E_aIntrvwrMethodT.LeaveBlank: return E_InterviewerInformationApplicationTakenMethodType.Undefined;
                case E_aIntrvwrMethodT.FaceToFace: return E_InterviewerInformationApplicationTakenMethodType.FaceToFace;
                case E_aIntrvwrMethodT.ByMail: return E_InterviewerInformationApplicationTakenMethodType.Mail;
                case E_aIntrvwrMethodT.ByTelephone: return E_InterviewerInformationApplicationTakenMethodType.Telephone;
                case E_aIntrvwrMethodT.Internet: return E_InterviewerInformationApplicationTakenMethodType.Internet;
                default:
                    return E_InterviewerInformationApplicationTakenMethodType.Undefined;
            }
        }

    }

}