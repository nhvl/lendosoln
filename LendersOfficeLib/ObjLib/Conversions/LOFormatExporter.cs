///
/// Author: David Dao
///
namespace LendersOffice.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Xsl;
    using DataAccess;
    using DataAccess.HomeownerCounselingOrganizations;
    using DataAccess.Trust;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.DistributeUnderwriting;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using ObjLib.DocumentConditionAssociation;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using ObjLib.DocMagicLib;
    using DataAccess.FannieMae;
    using Integration.Irs4506T;
    using ObjLib.Appraisal;
    using System.Collections.ObjectModel;
    using LendersOffice.CreditReport;

    public static class LOFormatExporter
    {
        private const int STARTING_RECORD_INDEX = 1; //Used for Record index attribute. Start with 1 to conform with W3C convention.
        private const string BORROWERCLOSINGCOSTSET = "sClosingCostSet";
        private const string SELLERCLOSINGCOSTSET = "sSellerResponsibleClosingCostSet";

        /// <summary>
        /// List of bool fields removed from the loan and app switch statements. For these fields, the bool value was converted to a string using
        /// <see cref="WriteFieldValue(XmlWriter, string, bool)"/>. This function does not take into account the FormatTarget.
        /// </summary>
        private static readonly HashSet<string> DefaultFormatTargetBoolFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
        {
            "slamtlckd",
            "sisionlyforsubfin",
            "sisofinnew",
            "sprodisspinruralarea",
            "sprodiscondotel",
            "sprodisnonwarrantableproj",
            "shas1sttimebuyer",
            "sprodimpound",
            "sstatuslckd",
            "swillescrowbewaived",
            "sprofitancillaryfeelckd",
            "sapprfpaid",
            "scrfpaid",
            "sprocfpaid",
            "saprincludesreqdeposit",
            "sasteriskestimate",
            "sffufmip1003lckd",
            "shasdemandfeature",
            "shasvarrfeature",
            "sifpurchfloodinsfrcreditor",
            "sifpurchinsfrcreditor",
            "sldiscnt1003lckd",
            "socredit1lckd",
            "socredit2lckd",
            "sonlylatepmtestimate",
            "sprominsmidptcancel",
            "srefpdoffamt1003lckd",
            "sreqcreditdisabilityins",
            "sreqcreditlifeins",
            "sreqfloodins",
            "ssecuritycurrentown",
            "ssecuritypurch",
            "stotccpbslocked",
            "stotestcc1003lckd",
            "stotestpp1003lckd",
            "smultiapps",
            "sballoonpmt",
            "sbiweeklypmt",
            "sismanualuw",
            "sradjworstindex",
            "sisselfemployed",
            "sprodcrmanualbk13has",
            "sprodcrmanualbk7has",
            "sprodcrmanualforeclosurehas",
            "sprodhashousinghistory",
            "sisoptionarmsubmitted",
            "soptionarmminpayisioonly",
            "sisfullamortafterrecast",
            "sisionlype",
            "shasrequestedrate",
            "sallowresubmission",
            "sprodincludenormalproc",
            "sprodincludemycommunityproc",
            "sprodincludehomepossibleproc",
            "sprodincludefhatotalproc",
            "sprodincludevaproc",
            "scustomfield1bit",
            "scustomfield2bit",
            "scustomfield3bit",
            "scustomfield4bit",
            "scustomfield5bit",
            "scustomfield6bit",
            "scustomfield7bit",
            "scustomfield8bit",
            "scustomfield9bit",
            "scustomfield10bit",
            "scustomfield11bit",
            "scustomfield12bit",
            "scustomfield13bit",
            "scustomfield14bit",
            "scustomfield15bit",
            "scustomfield16bit",
            "scustomfield17bit",
            "scustomfield18bit",
            "scustomfield19bit",
            "scustomfield20bit",
            "scustomfield21bit",
            "scustomfield22bit",
            "scustomfield23bit",
            "scustomfield24bit",
            "scustomfield25bit",
            "scustomfield26bit",
            "scustomfield27bit",
            "scustomfield28bit",
            "scustomfield29bit",
            "scustomfield30bit",
            "scustomfield31bit",
            "scustomfield32bit",
            "scustomfield33bit",
            "scustomfield34bit",
            "scustomfield35bit",
            "scustomfield36bit",
            "scustomfield37bit",
            "scustomfield38bit",
            "scustomfield39bit",
            "scustomfield40bit",
            "scustomfield41bit",
            "scustomfield42bit",
            "scustomfield43bit",
            "scustomfield44bit",
            "scustomfield45bit",
            "scustomfield46bit",
            "scustomfield47bit",
            "scustomfield48bit",
            "scustomfield49bit",
            "scustomfield50bit",
            "scustomfield51bit",
            "scustomfield52bit",
            "scustomfield53bit",
            "scustomfield54bit",
            "scustomfield55bit",
            "scustomfield56bit",
            "scustomfield57bit",
            "scustomfield58bit",
            "scustomfield59bit",
            "scustomfield60bit",
            "sprodisdurefiplus",
            "sffufmipisbeingfinanced",
            "sfloodcertificationisinspecialarea",
            "sfloodcertificationiscbraoropa",
            "sfloodcertificationislolupgraded",
            "sisionly",
            // sMersOriginatingOrgIdLckd and sMersTosDLckd were set in the switch statement with this specific casing.
            // However, since we call ToLower on the field id in the xml, they never actually get hit. So
            // they've been going through PageDataUtilities this entire time. Which means that they don't ignore the FormatTarget
            //"sMersOriginatingOrgIdLckd", 
            //"sMersTosDLckd", 
            "abaddrmailusepresentaddr",
            "acaddrmailusepresentaddr",
            "anetrenti1003lckd",
            "ao1iforc",
            "ao2iforc",
            "ao3iforc",
            "abnofurnish",
            "acnofurnish",
            "abisamericanindian",
            "acisamericanindian",
            "abisasian",
            "acisasian",
            "abisblack",
            "acisblack",
            "abispacificislander",
            "acispacificislander",
            "abiswhite",
            "aciswhite",
            "aspouseiexcl",
            "aasstliacompletednotjointly",
            "adenialbankruptcy",
            "adenialbyfedhomeloanmortcorp",
            "adenialbyfednationalmortassoc",
            "adenialbyhud",
            "adenialbyother",
            "adenialbyva",
            "adenialcreditappincomplete",
            "adenialdeliquentcreditobligations",
            "adenialexcessiveobligations",
            "adenialgarnishment",
            "adenialinadequatecollateral",
            "adenialinfofromconsumerreportagency",
            "adenialinsufficientcreditfile",
            "adenialinsufficientcreditref",
            "adenialinsufficientfundstoclose",
            "adenialinsufficientincome",
            "adenialinsufficientincomeformortgagepmt",
            "adenialinsufficientpropdata",
            "adeniallackofcashreserves",
            "adeniallenofemployment",
            "adenialnocreditfile",
            "adenialnotgrantcredittoanyapponsuchtermsandconds",
            "adenialotherreason1",
            "adenialotherreason2",
            "adenialshortresidenceperiod",
            "adenialtempresidence",
            "adenialtemporaryemployment",
            "adenialunableverifycreditref",
            "adenialunableverifyemployment",
            "adenialunableverifyincome",
            "adenialunableverifyresidence",
            "adenialunacceptableappraisal",
            "adenialunacceptableleasehold",
            "adenialunacceptablepmtrecord",
            "adenialunacceptableprop",
            "adenialwithdrawnbyapp",
            "aisborrspouseprimarywageearner",
        };

        public static string Export(Guid sLId, AbstractUserPrincipal principal, string requestData, bool isClosingDisclosure = false)
        {
            var bypassFieldSecurity = false;
            return Export(sLId, principal, requestData, default(FormatTarget), bypassFieldSecurity, isClosingDisclosure);
        }

        public static string Export(
            Guid sLId, 
            AbstractUserPrincipal principal, 
            string requestData, 
            FormatTarget format, 
            bool bypassFieldSecurity, 
            bool isClosingDisclosure = false,
            bool useTempArchiveForTridFile = false, 
            bool excludeZeroAdjustmentForDocGenInLoXml = false, 
            // this parameter if specified will take precedence over any attribute specified in the LoXml, so if you want it up to the user, leave it null.
            E_BorrowerModeT? borrowerMode = null)
        {
            XmlDocument requestDoc = Tools.CreateXmlDoc(requestData);

            // 7/18/2014 ir - OPM 187540, Allow web service user to specify FormatTarget as an attribute in LOXmlFormat tag
            if (requestDoc.DocumentElement.HasAttribute("FormatTarget"))
            {
                string formatOverride = requestDoc.DocumentElement.GetAttribute("FormatTarget");
                format = (FormatTarget)Enum.Parse(typeof(FormatTarget), formatOverride, true);
            }

            // Gather all field id in loan and applicant element.
            List<string> loanFieldList = new List<string>();
            List<string> applicantFieldList = new List<string>();
            List<string> applicantCollectionList = new List<string>();

            foreach (XmlElement el in requestDoc.SelectNodes("//loan/field"))
            {
                string id = el.GetAttribute("id");
                if (id.StartsWith("GetPreparerOfForm", StringComparison.OrdinalIgnoreCase))
                {
                    loanFieldList.Add("sfGetPreparerOfForm");
                }
                else if (id.StartsWith("GetAgentOfRole", StringComparison.OrdinalIgnoreCase))
                {
                    loanFieldList.Add("sfGetAgentOfRole");
                }
                else if (id.StartsWith("GetInvestorInfo", StringComparison.OrdinalIgnoreCase))
                {
                    loanFieldList.Add("sfGetInvestorInfo");
                }
                else if (id.StartsWith("GetSubservicerInfo", StringComparison.OrdinalIgnoreCase))
                {
                    loanFieldList.Add("sfGetSubservicerInfo");
                }
                else
                {
                    string loanField = DependencyUtils.NormalizeMemberName(el.GetAttribute("id"));
                    loanFieldList.Add(loanField);
                }
            }

            // we could make this mode be application specific, but currently we just aggregate all fields anyway.
            if (borrowerMode == null)
            {
                foreach (XmlElement el in requestDoc.SelectNodes("//loan/application"))
                {
                    if (el.HasAttribute("borrowerMode"))
                    {
                        borrowerMode = (E_BorrowerModeT)Enum.Parse(typeof(E_BorrowerModeT), el.GetAttribute("borrowerMode"));
                        break;
                    }
                }
            }

            foreach (XmlElement el in requestDoc.SelectNodes("//loan/applicant/field"))
            {
                string applicantField = DependencyUtils.NormalizeMemberName(el.GetAttribute("id"));
                applicantFieldList.Add(applicantField);
            }

            foreach (XmlElement el in requestDoc.SelectNodes("//loan/applicant/collection"))
            {
                applicantCollectionList.Add(el.GetAttribute("id"));
            }

            List<string> dependencyFieldList = CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(LOFormatExporter)).ToList();
            dependencyFieldList.Add("sfGetPreparerOfForm");
            dependencyFieldList.AddRange(loanFieldList);
            dependencyFieldList.AddRange(applicantFieldList);

            CSelectStatementProvider stmtProvider = CSelectStatementProvider.GetProviderFor(E_ReadDBScenarioType.InputTargetFields_NeedTargetFields, dependencyFieldList);

            CPageData dataLoan;

            if (bypassFieldSecurity)
            {
                dataLoan = new CFullAccessPageData(sLId, "LOFormatExporter", stmtProvider);
            }
            else
            {
                dataLoan = new CPageData(sLId, "LOFormatExporter", stmtProvider);
            }

            dataLoan.InitLoad();

            dataLoan.SetFormatTarget(format);
            dataLoan.ByPassFieldSecurityCheck = bypassFieldSecurity;

            if (useTempArchiveForTridFile 
                && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID
                && dataLoan.sHasLoanEstimateArchiveInPendingStatus)
            {
                TemporaryArchive.PostDocumentGenerationAction action = dataLoan.GetPostDocumentGenerationAction();
                if (action == TemporaryArchive.PostDocumentGenerationAction.MarkPendingLoanEstimateIncludedInClosingDisclosure)
                {
                    dataLoan.UpdateArchiveStatus(
                        dataLoan.sLoanEstimateArchiveInPendingStatus.Id,
                        ClosingCostArchive.E_ClosingCostArchiveStatus.IncludedInClosingDisclosure);
                }
                else if (action != TemporaryArchive.PostDocumentGenerationAction.MarkPendingLoanEstimateInvalid)
                {
                    dataLoan.UpdateArchiveStatus(
                        dataLoan.sLoanEstimateArchiveInPendingStatus.Id,
                        ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed);
                }
            }

            using (MemoryStream memoryStream = new MemoryStream())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Encoding = Encoding.ASCII;
                //settings.Indent = true; // 9/12/2012 dd - Temporary disable indent because the current iPad/iPhone app parse the XML by hand.
                settings.OmitXmlDeclaration = true;
                settings.ConformanceLevel = ConformanceLevel.Fragment;
                XmlWriter xmlWriter = XmlWriter.Create(memoryStream, settings);
                xmlWriter.WriteStartElement("LOXmlFormat");
                xmlWriter.WriteAttributeString("version", LendersOffice.Constants.ConstAppDavid.LOXmlFormatVersion);

                xmlWriter.WriteStartElement("loan");

                XmlNodeList list = requestDoc.SelectNodes("//loan/field");

                foreach (XmlElement el in list)
                {
                    WriteLoanFields(el.GetAttribute("id"), dataLoan, xmlWriter, principal);
                }

                WriteApplicants(dataLoan, xmlWriter, applicantFieldList, applicantCollectionList, borrowerMode);

                XmlNodeList collectionList = requestDoc.SelectNodes("//loan/collection");
                foreach (XmlElement el in collectionList)
                {
                    string id = el.GetAttribute("id");
                    switch (id.ToLower())
                    {
                        case "sconddataset":
                            WriteConditions(dataLoan, principal, xmlWriter);
                            break;
                        case "sconddataset_all":
                            WriteAllConditions(dataLoan, principal, xmlWriter);
                            break;
                        case "sbrokerlockadjustments":
                            // 10/3/2011 dd - This will return only adjustments visible to LO.
                            WriteBrokerAdjustment(dataLoan, principal, xmlWriter);
                            break;
                        case "sallbrokerlockadjustments":
                            // 10/3/2011 dd - Return all adjustments. Only user with right permission will able to view data.
                            WriteAllBrokerAdjustment(dataLoan, principal, xmlWriter);
                            break;
                        case "strustcollection":
                            //OPM 117751: trust collections, for DocuTech.
                            WriteTrustCollection(dataLoan, xmlWriter);
                            break;
                        case "changeofcircumstancearchives":
                            WriteCoCArchives(dataLoan, xmlWriter);
                            break;
                        case "settlementserviceproviders":
                            WriteSettlementServiceProviders(dataLoan, xmlWriter);
                            break;
                        case "shomeownercounselingorganizationcollection":
                            //OPM 145024: Homeowner Counseling Organizations (10)
                            WriteHomeownerCounselingOrganizationCollection(dataLoan, xmlWriter);
                            break;
                        case "savailableclosingcostset":
                            WriteAvailableClosingCostSet(dataLoan, BORROWERCLOSINGCOSTSET, xmlWriter);
                            break;
                        case "savailablesellerresponsibleclosingcostset":
                            WriteAvailableClosingCostSet(dataLoan, SELLERCLOSINGCOSTSET, xmlWriter);
                            break;
                        case "sclosingcostset":
                            // OPM 198230: Expose new CFPB data for integrated disclosure.
                            WriteClosingCostSet(dataLoan, BORROWERCLOSINGCOSTSET, xmlWriter, isClosingDisclosure);
                            break;
                        case "ssellerresponsibleclosingcostset":
                            WriteClosingCostSet(dataLoan, SELLERCLOSINGCOSTSET, xmlWriter, isClosingDisclosure);
                            break;
                        case "sloanestimatedatesinfo":
                            WriteLoanEstimateDatesInfo(dataLoan, xmlWriter);
                            break;
                        case "sclosingdisclosuredatesinfo":
                            WriteClosingDisclosureDatesInfo(dataLoan, xmlWriter);
                            break;
                        case "shousingexpenses":
                            WriteHousingExpenses(dataLoan, xmlWriter);
                            break;
                        case "sdudwnpmtsrccollection":
                            WriteDUDownPaymentSources(dataLoan.sDUDwnPmtSrc, xmlWriter);
                            break;
                        case "sagentdataset":
                            WriteAgentDataSet(dataLoan, xmlWriter);
                            break;
                        case "closingcostarchives":
                            WriteClosingCostArchives(dataLoan, xmlWriter, format, useTempArchiveForTridFile, principal);
                            break;
                        case "disclosurearchivemetadata":
                            WriteClosingCostArchiveMetadata(dataLoan, xmlWriter);
                            break;
                        case "sadjustmentlist":
                            WriteAdjustmentList(dataLoan.sAdjustmentList, xmlWriter, excludeZeroAdjustmentForDocGenInLoXml);
                            break;
                        case "sprorationlist":
                            WriteProrationList(dataLoan.sProrationList, xmlWriter, el.GetAttribute("systemProrations").Equals("true", StringComparison.OrdinalIgnoreCase));
                            break;
                        case "salternatelender":
                            WriteAlternateLender(principal.BrokerId, dataLoan.sDocMagicAlternateLenderId, xmlWriter);
                            break;
                        case "sduthirdpartyproviders":
                            WriteDuThirdPartyProviders(dataLoan.sDuThirdPartyProviders.GetProviders(), xmlWriter);
                            break;
                        case "documentpackage":
                            // Document packages get processed by Loan.Load and should be ignored here.
                            break;
                        case "sselectedproductcodefilter":
                            WriteProductCodeSet(principal, dataLoan.BrokerDB, "sSelectedProductCodeFilter", dataLoan.sSelectedProductCodeFilter, xmlWriter);
                            break;
                        case "savailableproductcodefilter":
                            WriteProductCodeSet(principal, dataLoan.BrokerDB, "sAvailableProductCodeFilter", dataLoan.sAvailableProductCodeFilter, xmlWriter);
                            break;
                        case "4506torderlist":
                            Write4506tOrders(dataLoan, xmlWriter);
                            break;
                        case "strackingappraisalorders":
                            WriteAppraisalOrderTrackingCollection(dataLoan, principal, xmlWriter);
                            break;
                        case "stasklist":
                            WriteTaskList(dataLoan, principal, xmlWriter);
                            break;
                        default:
                            Tools.LogWarning("LOFormatExport:: " + id + " is invalid collection id.");
                            break;
                    }
                }

                xmlWriter.WriteEndElement(); //< /loan>
                xmlWriter.WriteEndElement(); // </LOXmlFormat>
                xmlWriter.Flush();
                return System.Text.ASCIIEncoding.ASCII.GetString(memoryStream.GetBuffer(), 0, (int)memoryStream.Position);
            }
        }

        /// <summary>
        /// Retrieves the loan identifiers from an LOXML list of loans for batch exporting.
        /// </summary>
        /// <param name="loanList">An LOXML list of loans where each loan holds an sLId, sLNm, or both.</param>
        /// <param name="principal">The user creating a batch export.</param>
        /// <returns>A list of IDs for each loan in the list.</returns>
        public static List<Guid> GetLoanIdentifiersFromBatch(string loanList, AbstractUserPrincipal principal)
        {
            XmlDocument requestDoc = Tools.CreateXmlDoc(loanList);
            var identifiers = new List<Guid>();

            foreach (XmlElement loan in requestDoc.SelectNodes("//loan"))
            {
                Guid loanIdentifier = Guid.Empty;
                string loanNumber = string.Empty;

                foreach (XmlElement field in loan.SelectNodes("field"))
                {
                    string fieldName = field.GetAttribute("id").TrimWhitespaceAndBOM();
                    if (fieldName.Equals("sLId", StringComparison.OrdinalIgnoreCase))
                    {
                        loanIdentifier = new Guid(field.InnerText.TrimWhitespaceAndBOM());
                        break;
                    }
                    else if (fieldName.Equals("sLNm", StringComparison.OrdinalIgnoreCase))
                    {
                        loanNumber = field.InnerText.TrimWhitespaceAndBOM();
                    }
                }

                // Do not hit the database to retrieve the loan ID unless there was none listed.
                if (Guid.Equals(loanIdentifier, Guid.Empty))
                {
                    loanIdentifier = Tools.GetLoanIdByLoanName(principal.BrokerId, loanNumber);
                    if (Guid.Equals(loanIdentifier, Guid.Empty))
                    {
                        throw new InvalidDataException("Loan number: '" + loanNumber + "' is invalid");
                    }
                }

                identifiers.Add(loanIdentifier);
            }

            return identifiers;
        }

        /// <summary>
        /// Parses a Loan.Load XML query for a Document Package definition. If one is found, it is deserialized into a
        /// DocumentPackage object with the specified parameters. If one is not found or any parameters are missing, a
        /// default package is created instead.
        /// </summary>
        /// <param name="xmlQuery">An LOXML query.</param>
        /// <param name="exportIsLoxmlFormat">Indicates whether the Loan.Load call is requesting the LOXML format.</param>
        /// <returns>A DocumentPackage object.</returns>
        public static ParseResult<VendorConfig.DocumentPackage> GetDocumentPackageFromLoXml(string xmlQuery, bool exportIsLoxmlFormat)
        {
            string name = "Default Document Package";
            VendorConfig.PackageType type = exportIsLoxmlFormat ? VendorConfig.PackageType.Undefined : VendorConfig.PackageType.Closing;
            bool hasGfe = false;
            bool hasTil = false;
            bool hasLe = false;
            bool hasCd = !exportIsLoxmlFormat;
            bool triggersPtmBilling = false;

            var defaultResult = ParseResult<VendorConfig.DocumentPackage>.Success(xmlQuery, new VendorConfig.DocumentPackage(name, type, hasGfe, hasTil, hasLe, hasCd, triggersPtmBilling));
            // Empty or whitespace string => use all default values.
            if (string.IsNullOrWhiteSpace(xmlQuery))
            {
                return defaultResult;
            }

            List<string> parsingErrors = new List<string>();
            XmlDocument requestDoc = Tools.CreateXmlDoc(xmlQuery);
            if (requestDoc.SelectNodes("//loan/collection").Count == 0)
            {
                return defaultResult;
            }

            // Get the items from the first record under the DocumentPackage collection.
            var packageData = requestDoc.SelectNodes("//loan/collection[@id=\"DocumentPackage\"]/record[1]/field");
            if (packageData.Count == 0)
            {
                // This likely means the xml was misspelled or unintentionally empty.
                parsingErrors.Add("No document package field values were found. Please make sure to follow the \"//loan/collection[@id=\"DocumentPackage\"]/record/field\" format or leave the XML query blank for default values.");
                return ParseResult<VendorConfig.DocumentPackage>.Failure(xmlQuery, string.Join("; ", parsingErrors));
            }

            foreach (XmlElement element in packageData)
            {
                string parameterName = null;
                try
                {
                    parameterName = element.GetAttribute("id").TrimWhitespaceAndBOM();
                    switch (parameterName.ToLower())
                    {
                        case "name":
                            name = element.InnerXml;
                            break;
                        case "type":
                            // Since PackageType is a flags enum, we can't simply use Enum.IsDefined.
                            // See http://stackoverflow.com/questions/4950001/enum-isdefined-with-flagged-enums
                            var enumValue = Enum.Parse(typeof(VendorConfig.PackageType), element.InnerXml);
                            int integerValue;

                            if (!int.TryParse(enumValue.ToString(), out integerValue))
                            {
                                type = (VendorConfig.PackageType)enumValue;
                            }

                            break;
                        case "hasgfe":
                            hasGfe = Convert.ToBoolean(element.InnerXml);
                            break;
                        case "hastil":
                            hasTil = Convert.ToBoolean(element.InnerXml);
                            break;
                        case "hasloanestimate":
                            hasLe = Convert.ToBoolean(element.InnerXml);
                            break;
                        case "hasclosingdisclosure":
                            hasCd = Convert.ToBoolean(element.InnerXml);
                            break;
                        case "triggersptmbilling":
                            // Though this parameter does exist, we do not allow it to be set via LOXML.
                            break;
                        default:
                            parsingErrors.Add($"Unknown document package field id: \"{parameterName}\".");
                            break;
                    }
                }
                catch (SystemException ex) when (ex is FormatException || ex is ArgumentException)
                {
                    parsingErrors.Add($"Error parsing parameter \"{parameterName}\".");
                }
            }

            if (parsingErrors.Any())
            {
                return ParseResult<VendorConfig.DocumentPackage>.Failure(xmlQuery, string.Join("; ", parsingErrors));
            }
            else
            {
                return ParseResult<VendorConfig.DocumentPackage>.Success(xmlQuery, new VendorConfig.DocumentPackage(name, type, hasGfe, hasTil, hasLe, hasCd, triggersPtmBilling));
            }
        }

        private static void WriteAllBrokerAdjustment(CPageData dataLoan, AbstractUserPrincipal principal, XmlWriter xmlWriter)
        {
            if (principal.HasPermission(Permission.AllowLockDeskRead) == false)
            {
                throw new AccessDenied("Unable to access sAllBrokerLockAdjustments");
            }

            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sAllBrokerLockAdjustments");
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (var adjustment in dataLoan.sBrokerLockAdjustments)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "Description", adjustment.Description);
                WriteFieldValue(xmlWriter, "Point", adjustment.Fee);
                WriteFieldValue(xmlWriter, "Rate", adjustment.Rate);
                WriteFieldValue(xmlWriter, "Margin", adjustment.Margin);
                WriteFieldValue(xmlWriter, "IsHidden", adjustment.IsHidden);
                WriteFieldValue(xmlWriter, "IsLender", adjustment.IsLenderAdjustment);
                WriteFieldValue(xmlWriter, "IsPersist", adjustment.IsPersist);
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndElement();
        }

        private static void WriteBrokerAdjustment(CPageData dataLoan, AbstractUserPrincipal principal, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sBrokerLockAdjustments");
            bool oldValue = dataLoan.ByPassFieldSecurityCheck;
            // 6/27/2011 dd - Temporary disable check when getting adjustment.
            dataLoan.ByPassFieldSecurityCheck = true;
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (var adjustment in dataLoan.sBrokerLockAdjustments)
            {
                if (adjustment.IsHidden)
                {
                    continue; // We do not export hidden adjustment.
                }

                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "Description", adjustment.Description);
                WriteFieldValue(xmlWriter, "Point", adjustment.Fee);
                WriteFieldValue(xmlWriter, "Rate", adjustment.Rate);
                WriteFieldValue(xmlWriter, "Margin", adjustment.Margin);
                xmlWriter.WriteEndElement();
            }

            dataLoan.ByPassFieldSecurityCheck = oldValue;
            xmlWriter.WriteEndElement();
        }

        #region Export Conditions
        private static void WriteConditions(CPageData dataLoan, AbstractUserPrincipal principal, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sCondDataSet");

            var brokerId = (principal == null || principal.BrokerId == Guid.Empty) ?
                dataLoan.sBrokerId : principal.BrokerId;

            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            if (brokerDB.HasLenderDefaultFeatures)
            {
                if (brokerDB.IsUseNewTaskSystem)
                {
                    ConditionOrderManager orderManager = ConditionOrderManager.Load(dataLoan.sConditionsSortOrder);
                    List<Task> conditions = Task.GetConditionsWithAssociatedDocsByLoanId(principal, dataLoan.sLId, false, false, orderManager);
                    WriteTaskConditions(conditions, xmlWriter, principal);
                }
                else
                {
                    LoanConditionSetObsolete conditionList = new LoanConditionSetObsolete();
                    conditionList.Retrieve(dataLoan.sLId, true, false, false);
                    WriteConditions(conditionList, xmlWriter);
                }
            }
            else
            {
                WriteXmlConditions(dataLoan, principal, xmlWriter);
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        private static void WriteTaskConditions(List<Task> conditions, XmlWriter xmlWriter, AbstractUserPrincipal principal)
        {
            WriteTaskConditions(conditions, xmlWriter, false, principal);
        }

        private static void WriteTaskConditions(List<Task> conditions, XmlWriter xmlWriter, bool includeFirstComment, AbstractUserPrincipal principal)
        {
            Guid loanId;
            if (conditions.Count > 0)
            {
                loanId = conditions.First().LoanId;
            }
            else
            {
                return;
            }

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            Lazy<Dictionary<Guid, EDocument>> readableEdocs = new Lazy<Dictionary<Guid, EDocument>>(() => repo.GetDocumentsByLoanId(loanId).ToDictionary((edoc) => edoc.DocumentId));
            Lazy<Dictionary<int, DocType>> docTypes = new Lazy<Dictionary<int, DocType>>(() => EDocumentDocType.GetDocTypesByBroker(principal.BrokerId, false).ToDictionary((docType) => docType.Id));

            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (Task condition in conditions)
            {
                // 7/10/2013 dd - NOTE: When adding new property to this list please notify Sean Robinson <seanr@ffcmortgage.com> (See OPM 128861)
                // Apparently, he rely on the order to be correct.
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "ID", condition.TaskId);

                if (condition.TaskStatus == E_TaskStatus.Closed)
                {
                    WriteFieldValue(xmlWriter, "DoneDate", condition.TaskClosedDate.Value.ToShortDateString());
                }

                WriteFieldValue(xmlWriter, "AssignedTo", condition.AssignedUserFullName);
                WriteFieldValue(xmlWriter, "Category", condition.CondCategoryId_rep);
                WriteFieldValue(xmlWriter, "CondDesc", condition.TaskSubject);
                WriteFieldValue(xmlWriter, "CondRowId", condition.CondRowId.ToString());
                WriteFieldValue(xmlWriter, "DueDate", condition.TaskDueDate_rep);
                WriteFieldValue(xmlWriter, "FollowUpDate", condition.TaskFollowUpDate_rep);
                WriteFieldValue(xmlWriter, "Notes", condition.CondInternalNotes);
                WriteFieldValue(xmlWriter, "Owner", condition.OwnerFullName);
                WriteFieldValue(xmlWriter, "PermissionLevel", condition.PermissionLevelName);
                WriteFieldValue(xmlWriter, "Status", condition.TaskStatus_rep);
                WriteFieldValue(xmlWriter, "ToBeAssigned", GetRoleNameFromID(condition.TaskToBeAssignedRoleId));
                WriteFieldValue(xmlWriter, "ToBeOwnedBy", GetRoleNameFromID(condition.TaskToBeOwnerRoleId));

                if (includeFirstComment)
                {
                    WriteFieldValue(xmlWriter, "FirstComment", condition.TaskHistoryFirstComment);
                }

                WriteConditionDocumentInfo(xmlWriter, condition, principal, loanId, readableEdocs, docTypes);

                xmlWriter.WriteEndElement(); // </record>
            }
        }

        /// <summary>
        /// Writes documents and related metadata associated with a condition.
        /// </summary>
        /// <param name="xmlWriter">An XML writer.</param>
        /// <param name="condition">A condition.</param>
        /// <param name="principal">The user principal.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="readableEdocs">A collection of documents readable by the user.</param>
        /// <param name="docTypes">A collection of doctypes.</param>
        private static void WriteConditionDocumentInfo(XmlWriter xmlWriter, Task condition, AbstractUserPrincipal principal, Guid loanId, Lazy<Dictionary<Guid, EDocument>> readableEdocs, Lazy<Dictionary<int, DocType>> docTypes)
        {
            bool isPUserCanAccessAssociatedDocs = string.Equals(principal.Type, "p", StringComparison.OrdinalIgnoreCase);
            bool isBUserCanAccessAssociatedDocs = string.Equals(principal.Type, "b", StringComparison.OrdinalIgnoreCase) && principal.HasPermission(Permission.CanViewEDocs);

            // At the moment, there is only one required doc type per condition but there seems to be plans in the future to allow multiple,
            // so we're going to use a collection.
            WriteRequireDocTypeForCondition(xmlWriter, condition, docTypes.Value);

            if (isPUserCanAccessAssociatedDocs)
            {
                // P users can always look at the associated docs.
                WriteAssociatedDocsForCondition(xmlWriter, condition, readableEdocs.Value, bypassPermissionCheck: true);
            }
            else if (isBUserCanAccessAssociatedDocs)
            {
                // B users must have the Can View Edocs user permission which is checked above.
                // In addition, they must be able to view the document itself in order for it to be outputted. This is checked in the function.
                WriteAssociatedDocsForCondition(xmlWriter, condition, readableEdocs.Value, bypassPermissionCheck: false);
            }
        }

        private static void WriteRequireDocTypeForCondition(XmlWriter xmlWriter, Task condition, Dictionary<int, DocType> brokerDocTypes)
        {
            if (!condition.CondRequiredDocTypeId.HasValue ||
                brokerDocTypes == null ||
                !brokerDocTypes.ContainsKey(condition.CondRequiredDocTypeId.Value))
            {
                return;
            }

            int reqDocCount = 1;
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "RequiredDoc");

            xmlWriter.WriteStartElement("record");
            xmlWriter.WriteAttributeString("index", reqDocCount.ToString());

            DocType requiredDocType = brokerDocTypes[condition.CondRequiredDocTypeId.Value];
            WriteFieldValue(xmlWriter, "ReqDocFolderName", requiredDocType.Folder.FolderNm);
            WriteFieldValue(xmlWriter, "ReqDocTypeName", requiredDocType.DocTypeName);
            WriteFieldValue(xmlWriter, "ReqDocTypeId", requiredDocType.DocTypeId);

            xmlWriter.WriteEndElement(); // </record>

            xmlWriter.WriteEndElement(); // </collection>
        }

        private static void WriteAssociatedDocsForCondition(XmlWriter xmlWriter, Task condition, Dictionary<Guid, EDocument> readableEdocs, bool bypassPermissionCheck)
        {
            List<DocumentConditionAssociation> readableAssociatedDocs = new List<DocumentConditionAssociation>();
            foreach (var doc in condition.AssociatedDocs)
            {
                if (bypassPermissionCheck ||
                    (readableEdocs != null && readableEdocs.ContainsKey(doc.DocumentId)))
                {
                   readableAssociatedDocs.Add(doc);
                }
            }

            if (readableAssociatedDocs.Count > 0)
            {
                xmlWriter.WriteStartElement("collection");
                xmlWriter.WriteAttributeString("id", "AssociatedDoc");

                int assocDocCount = 1;
                foreach (var associatedDoc in readableAssociatedDocs)
                {
                    xmlWriter.WriteStartElement("record");
                    xmlWriter.WriteAttributeString("index", (assocDocCount++).ToString());
                    WriteFieldValue(xmlWriter, "AssociatedDocId", associatedDoc.DocumentId.ToString());
                    WriteFieldValue(xmlWriter, "AssociatedDocType", associatedDoc.DocumentTypeName);
                    xmlWriter.WriteEndElement(); // </record>
                }

                xmlWriter.WriteEndElement(); // </collection>
            }
        }

        private static void WriteConditions(LoanConditionSetObsolete conditionList, XmlWriter xmlWriter)
        {
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (CLoanConditionObsolete condition in conditionList)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "IsRequired", true);
                if (condition.CondStatus == E_CondStatus.Done)
                {
                    WriteFieldValue(xmlWriter, "DoneDate", condition.CondStatusDate.ToShortDateString());
                }
                WriteFieldValue(xmlWriter, "Category", condition.CondCategoryDesc);
                WriteFieldValue(xmlWriter, "CondDesc", condition.CondDesc);
                WriteFieldValue(xmlWriter, "Notes", condition.CondNotes);

                xmlWriter.WriteEndElement(); // </record>
            }
        }

        private static string GetRoleNameFromID(Guid? roleID)
        {
            string sRoleName = "";
            if (roleID != null)
            {
                if (roleID == CEmployeeFields.s_AccountantId)
                {
                    sRoleName = "ROLE_ACCOUNTANT";
                }
                else if (roleID == CEmployeeFields.s_AdministratorRoleId)
                {
                    sRoleName = "ROLE_ADMINISTRATOR";
                }
                else if (roleID == CEmployeeFields.s_BrokerProcessorId)
                {
                    sRoleName = "ROLE_BROKER_PROCESSOR";
                }
                else if (roleID == CEmployeeFields.s_CallCenterAgentRoleId)
                {
                    sRoleName = "ROLE_CALL_CENTER_AGENT";
                }
                else if (roleID == CEmployeeFields.s_CloserId)
                {
                    sRoleName = "ROLE_CLOSER";
                }
                else if (roleID == CEmployeeFields.s_CollateralAgentId)
                {
                    sRoleName = "ROLE_COLLATERAL_AGENT";
                }
                else if (roleID == CEmployeeFields.s_ConsumerId)
                {
                    sRoleName = "ROLE_CONSUMER";
                }
                else if (roleID == CEmployeeFields.s_DocDrawerId)
                {
                    sRoleName = "ROLE_DOCDRAWER";
                }
                else if (roleID == CEmployeeFields.s_FunderRoleId)
                {
                    sRoleName = "ROLE_FUNDER";
                }
                else if (roleID == CEmployeeFields.s_InsuringId)
                {
                    sRoleName = "ROLE_INSURING";
                }
                else if (roleID == CEmployeeFields.s_LenderAccountExecRoleId)
                {
                    sRoleName = "ROLE_LENDER_ACCOUNT_EXECUTIVE";
                }
                else if (roleID == CEmployeeFields.s_LoanOpenerId)
                {
                    sRoleName = "ROLE_LOAN_OPENER";
                }
                else if (roleID == CEmployeeFields.s_LoanRepRoleId)
                {
                    sRoleName = "ROLE_LOAN_OFFICER";
                }
                else if (roleID == CEmployeeFields.s_LockDeskRoleId)
                {
                    sRoleName = "ROLE_LOCK_DESK";
                }
                else if (roleID == CEmployeeFields.s_ManagerRoleId)
                {
                    sRoleName = "ROLE_MANAGER";
                }
                else if (roleID == CEmployeeFields.s_PostCloserId)
                {
                    sRoleName = "ROLE_POSTCLOSER";
                }
                else if (roleID == CEmployeeFields.s_ProcessorRoleId)
                {
                    sRoleName = "ROLE_PROCESSOR";
                }
                else if (roleID == CEmployeeFields.s_RealEstateAgentRoleId)
                {
                    sRoleName = "ROLE_REAL_ESTATE_AGENT";
                }
                else if (roleID == CEmployeeFields.s_ShipperId)
                {
                    sRoleName = "ROLE_SHIPPER";
                }
                else if (roleID == CEmployeeFields.s_UnderwriterRoleId)
                {
                    sRoleName = "ROLE_UNDERWRITER";
                }
                else if (roleID == CEmployeeFields.s_ExternalSecondaryId)
                {
                    sRoleName = "ROLE_EXTERNAL_SECONDARY";
                }
                else if (roleID == CEmployeeFields.s_ExternalPostCloserId)
                {
                    sRoleName = "ROLE_EXTERNAL_POST_CLOSER";
                }
            }
            return sRoleName;
        }

        private static void WriteXmlConditions(CPageData dataLoan, AbstractUserPrincipal principal, XmlWriter xmlWriter)
        {
            int count = dataLoan.GetCondRecordCount();
            int recordIndex1 = STARTING_RECORD_INDEX;
            for (int i = 0; i < count; i++)
            {
                CCondFieldsObsolete field = dataLoan.GetCondFieldsObsolete(i);

                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "IsRequired", field.IsRequired);
                if (field.IsDone)
                {
                    WriteFieldValue(xmlWriter, "DoneDate", field.DoneDate_rep);
                }
                WriteFieldValue(xmlWriter, "Category", field.PriorToEventDesc);
                WriteFieldValue(xmlWriter, "CondDesc", field.CondDesc);

                xmlWriter.WriteEndElement(); // </record>
            }
        }

        /// <summary>
        /// Only implemented for Task-based conditions
        /// </summary>
        private static void WriteAllConditions(CPageData dataLoan, AbstractUserPrincipal principal, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sCondDataSet");

            BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

            if (!brokerDB.IsUseNewTaskSystem)
            {
                throw new CBaseException(ErrorMessages.Generic, new NotImplementedException());
            }

            ConditionOrderManager manager = ConditionOrderManager.Load(dataLoan.sConditionsSortOrder);

            List<Task> conditions = Task.GetConditionsWithAssociatedDocsByLoanId(principal, dataLoan.sLId, true, true, manager);
            WriteTaskConditions(conditions, xmlWriter, true, principal); // Include FirstComment for UDN Service 172633

            xmlWriter.WriteEndElement(); // </collection>
        }
        #endregion

        /// <summary>
        /// Writes a collection of DO/DU down payment sources to xmlwriter object.
        /// </summary>
        /// <param name="sDUDwnPmtSrc">Collection of down payment sources for DO/DU.</param>
        /// <param name="xmlWriter">Xmlwriter object to which the collection is written.</param>
        private static void WriteDUDownPaymentSources(DownPaymentGiftSet sDUDwnPmtSrc, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sDUDwnPmtSrcCollection");
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (DownPaymentGift downPayment in sDUDwnPmtSrc)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "Amount", downPayment.Amount_rep);
                WriteFieldValue(xmlWriter, "Source", downPayment.Source);
                WriteFieldValue(xmlWriter, "Explanation", downPayment.Explanation);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }

        private static void WriteLoanFields(string id, CPageData dataLoan, XmlWriter xmlWriter, AbstractUserPrincipal principal)
        {
            var format = dataLoan.m_convertLos.FormatTargetCurrent;
            id = id.TrimWhitespaceAndBOM();
            switch (id.ToLower())
            {
                case "sstatust_rep": WriteFieldValue(xmlWriter, id, dataLoan.sStatusT_rep); break;
                case "semployeecallcenteragentphone": WriteFieldValue(xmlWriter, id, dataLoan.sEmployeeCallCenterPhone); break;
                case "s1006prohexpdesc": WriteFieldValue(xmlWriter, id, dataLoan.s1006ProHExpDesc); break;
                case "sbranchname": WriteFieldValue(xmlWriter, id, dataLoan.BranchNm); break;
                case "sunderwritingdtime": WriteFieldValue(xmlWriter, id, dataLoan.sUnderwritingDTime_rep); break; 
                case "scloseddtime": WriteFieldValue(xmlWriter, id, dataLoan.sClosedDTime_rep); break; 
                case "sbranchaddr":
	                var branch = new BranchDB(dataLoan.sBranchId, dataLoan.sBrokerId);
	                branch.Retrieve();
	                WriteFieldValue(xmlWriter, "Branch", branch.Address);
	                break;
                case "sifpurchpropinsfrcreditor": WriteFieldValue(xmlWriter, id, dataLoan.sIfPurchPropInsFrCreditor, format); break;
                case "slatechargepc":
	                // 1/17/2014 gf - Critical OPM 149935. DT needs this to not include '%'.
	                var convertLos = dataLoan.m_convertLos;
	                string sLateChargePc_rep = convertLos.ToRateString(convertLos.ToRate(dataLoan.sLateChargePc));
	                WriteFieldValue(xmlWriter, id, sLateChargePc_rep);
	                break;
                //OPM 122349: For the generic document vendor framework, DocuTech was having issues when we export this as N
                //because in our system false represents "undefined", and in their system N represents "no".
                //Export false as blank instead.
                case "sreqpropins":
	                var exportFalseAsBlank = true;
	                WriteFieldValue(xmlWriter, id, dataLoan.sReqPropIns, format, exportFalseAsBlank); break;
                case "svarrnotes": WriteFieldValue(xmlWriter, id, dataLoan.sVarRNotes); break;
                case "sdwnpmtsrcexplain": WriteFieldValue(xmlWriter, id, dataLoan.sDwnPmtSrcExplain); break;
                case "solpurposedesc": WriteFieldValue(xmlWriter, id, dataLoan.sOLPurposeDesc); break;
                case "sapprdriveby": WriteFieldValue(xmlWriter, id, dataLoan.sApprDriveBy, format); break;
                case "sapprfull": WriteFieldValue(xmlWriter, id, dataLoan.sApprFull, format); break;
                case "sbuydown": WriteFieldValue(xmlWriter, id, dataLoan.sBuydown, format); break;
                case "siscommlen": WriteFieldValue(xmlWriter, id, dataLoan.sIsCommLen, format); break;
                case "sisduuw": WriteFieldValue(xmlWriter, id, dataLoan.sIsDuUw, format); break;
                case "sishownershipedcertinfile": WriteFieldValue(xmlWriter, id, dataLoan.sIsHOwnershipEdCertInFile, format); break;
                case "sislpuw": WriteFieldValue(xmlWriter, id, dataLoan.sIsLpUw, format); break;
                case "sismorigbroker": WriteFieldValue(xmlWriter, id, dataLoan.sIsMOrigBroker, format); break;
                case "sismorigcorrespondent": WriteFieldValue(xmlWriter, id, dataLoan.sIsMOrigCorrespondent, format); break;
                case "sisotheruw": WriteFieldValue(xmlWriter, id, dataLoan.sIsOtherUw, format); break;
                case "sisspreviewnoappr": WriteFieldValue(xmlWriter, id, dataLoan.sIsSpReviewNoAppr, format); break;
                case "sisoptionarm": WriteFieldValue(xmlWriter, id, dataLoan.sIsOptionArm, format); break;
                case "spmlcerthtml": WriteFieldValue(xmlWriter, id, GeneratePmlCertificate(dataLoan.sPmlCertXmlContent.Value, principal)); break;
                case "stotalscorecertificatehtml": WriteFieldValue(xmlWriter, id, dataLoan.sTotalScoreCertificateHtml.Value); break;
                case "slinkedloaninfolinkedlnm":
	                if (dataLoan.sLinkedLoanInfo.IsLoanLinked)
	                {
		                WriteFieldValue(xmlWriter, id, dataLoan.sLinkedLoanInfo.LinkedLNm);
	                }
	                else
	                {
		                WriteFieldValue(xmlWriter, id, "");
	                }
	                break;
                case "pricingresult": WriteFieldValue(xmlWriter, id, DistributeUnderwritingEngine.GetPricingResultInXml(principal, dataLoan.sLId, false, false,false).OuterXml); break;
                case "unmergepricingresult": WriteFieldValue(xmlWriter, id, DistributeUnderwritingEngine.GetPricingResultInXml(principal, dataLoan.sLId, true, false, false).OuterXml); break;
                case "pricingresultwithcompensationinfo": WriteFieldValue(xmlWriter, id, DistributeUnderwritingEngine.GetPricingResultInXml(principal, dataLoan.sLId, false, true, false).OuterXml); break;
                case "unmergepricingresultwithcompensationinfo": WriteFieldValue(xmlWriter, id, DistributeUnderwritingEngine.GetPricingResultInXml(principal, dataLoan.sLId, true, true, false).OuterXml); break;
                case "pricingresultv2": WriteFieldValue(xmlWriter, id, DistributeUnderwritingEngine.GetPricingResultInXml(principal, dataLoan.sLId, false, false, true).OuterXml); break;
                case "sratelockstatust_rep": WriteFieldValue(xmlWriter, id, dataLoan.sRateLockStatusT_rep); break;
                case "screditreportused": WriteFieldValue(xmlWriter, id, dataLoan.sCreditReportUsed, format); break;
                case "sinvestorlockadjustments": WriteFieldValue(xmlWriter, id, dataLoan.sInvestorLockAdjustXmlContent); break;
                case "sloanofficeremployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentLoanOfficerEmployeeId); break;
                case "sprocessoremployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentProcessorEmployeeId); break;
                case "smanageremployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentManagerEmployeeId); break;
                case "sunderwriteremployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentUnderwriterEmployeeId); break;
                case "sshipperemployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentShipperEmployeeId); break;
                case "sfunderemployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentFunderEmployeeId); break;
                case "spostcloseremployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentPostCloserEmployeeId); break;
                case "sinsuringemployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentInsuringEmployeeId); break;
                case "scollateralagentemployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentCollateralAgentEmployeeId); break;
                case "sdocdraweremployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentDocDrawerEmployeeId); break;
                case "scallcenteragentemployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentCallCenterAgentEmployeeId); break;
                case "sloanopeneremployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentLoanOpenerEmployeeId); break;
                case "slenderaccountexecemployeeid": WriteFieldValue(xmlWriter, id, dataLoan.sAgentLenderAgentEmployeeId); break;
                case "sfloodcertid": WriteFieldValue(xmlWriter, id, dataLoan.sFloodCertId); break;
                case "sMersOriginatingOrgIdLckd": WriteFieldValue(xmlWriter, id, dataLoan.sMersOriginatingOrgIdLckd); break;
                case "sMersTosDLckd": WriteFieldValue(xmlWriter, id, dataLoan.sMersTosDLckd); break;
                case "sappraisalvendornm": WriteFieldValue(xmlWriter, id, (dataLoan.sAppraisalVendorId != Guid.Empty ? AppraisalVendorConfig.Retrieve(dataLoan.sAppraisalVendorId).VendorName : "")); break;
                case "smortgagepoolid": WriteFieldValue(xmlWriter, id, dataLoan.sMortgagePoolId_rep); break;
                case "spoolid":
	                {
		                if (dataLoan.sMortgagePoolId.HasValue)
		                {
			                LendersOffice.ObjLib.MortgagePool.MortgagePool mp = new LendersOffice.ObjLib.MortgagePool.MortgagePool(dataLoan.sMortgagePoolId.Value);
			                WriteFieldValue(xmlWriter, id, mp.InternalId);
		                }
		                else
		                {
			                WriteFieldValue(xmlWriter, id, string.Empty);
		                }

		                break;
	                }
                case "sgfeestscavailtilld_withtime": WriteFieldValue(xmlWriter, id, dataLoan.sGfeEstScAvailTillD_rep_WithTime); break;
                case "stridloanestimatenoteiravailtilld_withtime": WriteFieldValue(xmlWriter, id, dataLoan.sTRIDLoanEstimateNoteIRAvailTillD_rep_WithTime); break;
                // BEGIN compatibility shim for special FHA preparers
                // Uncomment the logging once the doc vendors are migrated. If no logs are generated afterwards,
                // remove the shim.
                case "getpreparerofform[fhaddendummortgagee].preparername":
                    WriteFieldValue(xmlWriter, id, dataLoan.sFHAAddendumMortgageeName);
                    ////Tools.LogWarning("Used Load shim for sFHAAddendumMortgageeName");
                    break;
                case "getpreparerofform[fhaddendummortgagee].title":
                    WriteFieldValue(xmlWriter, id, dataLoan.sFHAAddendumMortgageeTitle);
                    ////Tools.LogWarning("Used Load shim for sFHAAddendumMortgageeTitle");
                    break;
                case "getpreparerofform[fhaaddendumunderwriter].preparername":
                    WriteFieldValue(xmlWriter, id, dataLoan.sFHAAddendumUnderwriterName);
                    ////Tools.LogWarning("Used Load shim for sFHAAddendumUnderwriterName");
                    break;
                case "getpreparerofform[fhaaddendumunderwriter].licensenumofagent":
                    WriteFieldValue(xmlWriter, id, dataLoan.sFHAAddendumUnderwriterChumsId);
                    ////Tools.LogWarning("Used Load shim for sFHAAddendumUnderwriterChumsId");
                    break;
                case "getpreparerofform[fha92900ltunderwriter].licensenumofagent":
                    WriteFieldValue(xmlWriter, id, dataLoan.sFHA92900LtUnderwriterChumsId);
                    ////Tools.LogWarning("Used Load shim for sFHA92900LtUnderwriterChumsId");
                    break;
                // END compatibility shim for special FHA preparers
                default:
                    WriteFieldValueDynamically(xmlWriter, id, dataLoan, dataLoan.GetAppData(0));
                    break;
            }
        }

        private static void WriteFieldValue(XmlWriter xmlWriter, string param, CommonLib.Address a)
        {
            WriteFieldValue(xmlWriter, "s" + param + "Address", a.StreetAddress);
            WriteFieldValue(xmlWriter, "s" + param + "City", a.City);
            WriteFieldValue(xmlWriter, "s" + param + "State", a.State);
            WriteFieldValue(xmlWriter, "s" + param + "Zip", a.Zipcode);
        }

        private static void WriteInvalidField(XmlWriter xmlWriter, string id)
        {
            WriteFieldValue(xmlWriter, id, "INVALID FIELD ID OR FIELD DOES NOT SUPPORT EXPORT");
        }

        private static string GeneratePmlCertificate(string sPmlCertXmlContent, AbstractUserPrincipal principal)
        {
            if (sPmlCertXmlContent == null || "" == sPmlCertXmlContent)
            {
                sPmlCertXmlContent = "<NoData/>";
            }

            string xsltLocation = LendersOffice.ObjLib.Resource.ResourceManager.Instance.GetResourcePath(LendersOffice.ObjLib.Resource.ResourceType.PmlCertificateXslt, principal.BrokerId);
            Guid pmlLenderSiteId = RetrievePmlLenderSiteId(principal.BrokerId);

            XsltArgumentList xsltParams = new XsltArgumentList();
            xsltParams.AddParam("VirtualRoot", "", "https://secure.pricemyloan.com"); // Hardcode to secure.pricemyloan.com url.
            xsltParams.AddParam("LenderPmlSiteId", "", pmlLenderSiteId.ToString());

            string html = "";
            try
            {
                html = XslTransformHelper.Transform(xsltLocation, sPmlCertXmlContent, xsltParams);
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
            }

            return html;
        }

        private static Guid RetrievePmlLenderSiteId(Guid brokerID)
        {
            SqlParameter[] parameters = { new SqlParameter("@BrokerID", brokerID) };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerID, "RetrieveLenderSiteIDByBrokerID", parameters))
            {
                if (reader.Read())
                {
                    return (Guid)reader["BrokerPmlSiteID"];
                }
            }

            return Guid.Empty;
        }

        private static void WriteFieldValue(XmlWriter xmlWriter, string id, Enum value)
        {
            WriteFieldValue(xmlWriter, id, value.ToString("D"));
        }

        private static void WriteFieldValue(XmlWriter xmlWriter, string id, bool value)
        {
            WriteFieldValue(xmlWriter, id, value, default(FormatTarget));
        }

        private static void WriteFieldValue(XmlWriter xmlWriter, string id, bool value, FormatTarget t)
        {
            var exportFalseAsBlank = false;
            WriteFieldValue(xmlWriter, id, value, t, exportFalseAsBlank);
        }

        private static void WriteFieldValue(XmlWriter xmlWriter, string id, bool value, FormatTarget t, bool exportFalseAsBlank)
        {
            string True = "True";
            string False = "False";

            if (t == FormatTarget.MismoClosing)
            {
                True = "Y";
                False = "N";

                if (exportFalseAsBlank) False = "";
            }

            WriteFieldValue(xmlWriter, id, value ? True : False);
        }

        private static void WriteFieldValue(XmlWriter xmlWriter, string id, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = Tools.SantizeXmlString(value);
            }
            xmlWriter.WriteStartElement("field");
            xmlWriter.WriteAttributeString("id", id);
            xmlWriter.WriteString(value);
            xmlWriter.WriteEndElement();
        }

        private static void WriteCData(XmlWriter xmlWriter, string id, string value)
        {
            xmlWriter.WriteStartElement("field");
            xmlWriter.WriteAttributeString("id", id);
            xmlWriter.WriteCData(value);

            xmlWriter.WriteEndElement();
        }

        private static void WriteApplicants(CPageData dataLoan, XmlWriter xmlWriter, List<string> applicantFieldList, List<string> applicantCollectionList, E_BorrowerModeT? borrowerMode)
        {
            if (applicantFieldList.Count == 0 && applicantCollectionList.Count == 0)
            {
                return; // Nothing to export.
            }
            int nAppCount = dataLoan.nApps;
            for (int i = 0; i < nAppCount; i++)
            {
                WriteIndividualApplicant(dataLoan, dataLoan.GetAppData(i), xmlWriter, applicantFieldList, applicantCollectionList, borrowerMode);
            }
        }

        private static void WriteIndividualApplicant(CPageData dataLoan, CAppData dataApp, XmlWriter xmlWriter, List<string> applicantFieldList, List<string> applicantCollectionList, E_BorrowerModeT? borrowerMode)
        {
            xmlWriter.WriteStartElement("applicant");
            xmlWriter.WriteAttributeString("id", dataApp.aBSsn.Replace("-", ""));

            if (borrowerMode.HasValue)
            {
                dataApp.BorrowerModeT = borrowerMode.Value;
            }

            var alimony = dataApp.aLiaCollection.GetAlimony(true);
            var childSupport = dataApp.aLiaCollection.GetChildSupport(true);
            var jobExpense1 = dataApp.aLiaCollection.GetJobRelated1(true);
            var jobExpense2 = dataApp.aLiaCollection.GetJobRelated2(true);
            IPrimaryEmploymentRecord aBPrimary = dataApp.aBEmpCollection.GetPrimaryEmp(true);
            IPrimaryEmploymentRecord aCPrimary = dataApp.aCEmpCollection.GetPrimaryEmp(true);

            var cashDeposit1 = dataApp.aAssetCollection.GetCashDeposit1(true);
            var cashDeposit2 = dataApp.aAssetCollection.GetCashDeposit2(true);
            var lifeInsurance = dataApp.aAssetCollection.GetLifeInsurance(true);
            var retirement = dataApp.aAssetCollection.GetRetirement(true);
            var business = dataApp.aAssetCollection.GetBusinessWorth(true);

            #region //loan/applicant/field
            foreach (string id in applicantFieldList)
            {
                switch (id.ToLower().TrimWhitespaceAndBOM())
                {
                    case "abgender": WriteFieldValue(xmlWriter, id, dataApp.aBGender); break;
                    case "acgender": WriteFieldValue(xmlWriter, id, dataApp.aCGender); break;

                    case "aalimonynm": WriteFieldValue(xmlWriter, id, alimony.OwedTo); break;
                    case "aalimonyexcl": WriteFieldValue(xmlWriter, id, alimony.NotUsedInRatio); break;
                    case "aalimonypmt": WriteFieldValue(xmlWriter, id, alimony.Pmt_rep); break;
                    case "aalimonyremainmons": WriteFieldValue(xmlWriter, id, alimony.RemainMons_rep); break;
                    case "achildsupportnm": WriteFieldValue(xmlWriter, id, childSupport.OwedTo); break;
                    case "achildsupportexcl": WriteFieldValue(xmlWriter, id, childSupport.NotUsedInRatio); break;
                    case "achildsupportpmt": WriteFieldValue(xmlWriter, id, childSupport.Pmt_rep); break;
                    case "achildsupportremainmons": WriteFieldValue(xmlWriter, id, childSupport.RemainMons_rep); break;
                    case "ajobrelated1excl": WriteFieldValue(xmlWriter, id, jobExpense1.NotUsedInRatio); break;
                    case "ajobrelated1expensedesc": WriteFieldValue(xmlWriter, id, jobExpense1.ExpenseDesc); break;
                    case "ajobrelated1pmt": WriteFieldValue(xmlWriter, id, jobExpense1.Pmt_rep); break;
                    case "ajobrelated2excl": WriteFieldValue(xmlWriter, id, jobExpense2.NotUsedInRatio); break;
                    case "ajobrelated2expensedesc": WriteFieldValue(xmlWriter, id, jobExpense2.ExpenseDesc); break;
                    case "ajobrelated2pmt": WriteFieldValue(xmlWriter, id, jobExpense2.Pmt_rep); break;
                    case "aaltnm1accnum": WriteFieldValue(xmlWriter, id, dataApp.aAltNm1AccNum.Value); break;
                    case "aaltnm2accnum": WriteFieldValue(xmlWriter, id, dataApp.aAltNm2AccNum.Value); break;
                    case "abaliases":
                    case "acaliases":
	                    WriteAliases(xmlWriter, id, dataApp); break;
                    case "abprimaryisselfemplmt": WriteFieldValue(xmlWriter, id, aBPrimary.IsSelfEmplmt); break;
                    case "abprimaryemplrnm": WriteFieldValue(xmlWriter, id, aBPrimary.EmplrNm); break;
                    case "abprimaryemplraddr": WriteFieldValue(xmlWriter, id, aBPrimary.EmplrAddr); break;
                    case "abprimaryemplrcity": WriteFieldValue(xmlWriter, id, aBPrimary.EmplrCity); break;
                    case "abprimaryemplrstate": WriteFieldValue(xmlWriter, id, aBPrimary.EmplrState); break;
                    case "abprimaryemplrzip": WriteFieldValue(xmlWriter, id, aBPrimary.EmplrZip); break;
                    case "abprimaryemplmtlen": WriteFieldValue(xmlWriter, id, aBPrimary.EmplmtLen_rep); break;
                    case "abprimaryproflen": WriteFieldValue(xmlWriter, id, aBPrimary.ProfLen_rep); break;
                    case "abprimaryjobtitle": WriteFieldValue(xmlWriter, id, aBPrimary.JobTitle); break;
                    case "abprimaryemplrbusphone": WriteFieldValue(xmlWriter, id, aBPrimary.EmplrBusPhone); break;
                    case "abprimaryemplrfax": WriteFieldValue(xmlWriter, id, aBPrimary.EmplrFax); break;
                    case "abprimaryverifsentd": WriteFieldValue(xmlWriter, id, aBPrimary.VerifSentD_rep); break;
                    case "abprimaryverifreorderedd": WriteFieldValue(xmlWriter, id, aBPrimary.VerifReorderedD_rep); break;
                    case "abprimaryverifrecvd": WriteFieldValue(xmlWriter, id, aBPrimary.VerifRecvD_rep); break;
                    case "abprimaryverifexpd": WriteFieldValue(xmlWriter, id, aBPrimary.VerifExpD_rep); break;
                    case "abprimaryempltstartd": WriteFieldValue(xmlWriter, id, aBPrimary.EmpltStartD_rep); break;
                    case "abprimaryprofstartd": WriteFieldValue(xmlWriter, id, aBPrimary.ProfStartD_rep); break;

                    case "acprimaryisselfemplmt": WriteFieldValue(xmlWriter, id, aCPrimary.IsSelfEmplmt); break;
                    case "acprimaryemplrnm": WriteFieldValue(xmlWriter, id, aCPrimary.EmplrNm); break;
                    case "acprimaryemplraddr": WriteFieldValue(xmlWriter, id, aCPrimary.EmplrAddr); break;
                    case "acprimaryemplrcity": WriteFieldValue(xmlWriter, id, aCPrimary.EmplrCity); break;
                    case "acprimaryemplrstate": WriteFieldValue(xmlWriter, id, aCPrimary.EmplrState); break;
                    case "acprimaryemplrzip": WriteFieldValue(xmlWriter, id, aCPrimary.EmplrZip); break;
                    case "acprimaryemplmtlen": WriteFieldValue(xmlWriter, id, aCPrimary.EmplmtLen_rep); break;
                    case "acprimaryproflen": WriteFieldValue(xmlWriter, id, aCPrimary.ProfLen_rep); break;
                    case "acprimaryjobtitle": WriteFieldValue(xmlWriter, id, aCPrimary.JobTitle); break;
                    case "acprimaryemplrbusphone": WriteFieldValue(xmlWriter, id, aCPrimary.EmplrBusPhone); break;
                    case "acprimaryemplrfax": WriteFieldValue(xmlWriter, id, aCPrimary.EmplrFax); break;
                    case "acprimaryverifsentd": WriteFieldValue(xmlWriter, id, aCPrimary.VerifSentD_rep); break;
                    case "acprimaryverifreorderedd": WriteFieldValue(xmlWriter, id, aCPrimary.VerifReorderedD_rep); break;
                    case "acprimaryverifrecvd": WriteFieldValue(xmlWriter, id, aCPrimary.VerifRecvD_rep); break;
                    case "acprimaryverifexpd": WriteFieldValue(xmlWriter, id, aCPrimary.VerifExpD_rep); break;
                    case "acprimaryempltstartd": WriteFieldValue(xmlWriter, id, aCPrimary.EmpltStartD_rep); break;
                    case "acprimaryprofstartd": WriteFieldValue(xmlWriter, id, aCPrimary.ProfStartD_rep); break;

                    case "acreditreportrawxml": WriteFieldValue(xmlWriter, id, dataApp.aCreditReportRawXml.Value); break;
                    case "acreditreportviewhtml":
	                    if (dataApp.CreditReportView.Value != null)
	                    {
		                    WriteFieldValue(xmlWriter, id, dataApp.CreditReportView.Value.HasHtml ? dataApp.CreditReportView.Value.HtmlContent : string.Empty);
	                    }
	                    else
	                    {
		                    WriteFieldValue(xmlWriter, id, string.Empty);
	                    }
	                    break;
                    case "acreditreportviewpdf":
	                    if (dataApp.CreditReportView.Value != null)
	                    {
		                    WriteFieldValue(xmlWriter, id, dataApp.CreditReportView.Value.HasPdf ? dataApp.CreditReportView.Value.PdfContent : string.Empty);
	                    }
	                    else
	                    {
		                    WriteFieldValue(xmlWriter, id, string.Empty);

	                    }
	                    break;

                    case "acashdeposit1_desc": WriteFieldValue(xmlWriter, id, cashDeposit1.Desc); break;
                    case "acashdeposit1_val": WriteFieldValue(xmlWriter, id, cashDeposit1.Val_rep); break;
                    case "acashdeposit2_desc": WriteFieldValue(xmlWriter, id, cashDeposit2.Desc); break;
                    case "acashdeposit2_val": WriteFieldValue(xmlWriter, id, cashDeposit2.Val_rep); break;
                    case "aassetlifeins_faceval": WriteFieldValue(xmlWriter, id, lifeInsurance.FaceVal_rep); break;
                    case "aassetlifeins_val": WriteFieldValue(xmlWriter, id, lifeInsurance.Val_rep); break;
                    case "aassetretirement_val": WriteFieldValue(xmlWriter, id, retirement.Val_rep); break;
                    case "aassetbusiness_val": WriteFieldValue(xmlWriter, id, business.Val_rep); break;
                    case "avapastlcollection": WriteVAPastLoans(xmlWriter, dataApp); break;
                    case "mclcreditdataforudn": WriteMclCreditDataForUDN(xmlWriter, dataLoan, dataApp); break;
                    // 2018-10 tj - opm 475507 - These legacy fields are still being called via web services; we only support them via a pretend shim
                    case "ao1iforc": WriteFieldValue(xmlWriter, id, dataApp.aOtherIncomeList[0].IsForCoBorrower); break;
                    case "ao1idesc": WriteFieldValue(xmlWriter, id, dataApp.aOtherIncomeList[0].Desc); break;
                    case "ao1i": WriteFieldValue(xmlWriter, id, dataApp.m_convertLos.ToMoneyString(dataApp.aOtherIncomeList[0].Amount, FormatDirection.ToRep)); break;
                    case "ao2iforc": WriteFieldValue(xmlWriter, id, dataApp.aOtherIncomeList[1].IsForCoBorrower); break;
                    case "ao2idesc": WriteFieldValue(xmlWriter, id, dataApp.aOtherIncomeList[1].Desc); break;
                    case "ao2i": WriteFieldValue(xmlWriter, id, dataApp.m_convertLos.ToMoneyString(dataApp.aOtherIncomeList[1].Amount, FormatDirection.ToRep)); break;
                    case "ao3iforc": WriteFieldValue(xmlWriter, id, dataApp.aOtherIncomeList[2].IsForCoBorrower); break;
                    case "ao3idesc": WriteFieldValue(xmlWriter, id, dataApp.aOtherIncomeList[2].Desc); break;
                    case "ao3i": WriteFieldValue(xmlWriter, id, dataApp.m_convertLos.ToMoneyString(dataApp.aOtherIncomeList[2].Amount, FormatDirection.ToRep)); break;
                    default:
                        WriteFieldValueDynamically(xmlWriter, id, dataLoan, dataApp);
                        break;
                }
            }
            #endregion

            #region //loan/applicant/collection
            foreach (string id in applicantCollectionList)
            {
                switch (id.ToLower())
                {
                    case "aliacollection":
                        WriteApplicantLiabilities(dataApp, xmlWriter, dataLoan.GetFormatTarget());
                        break;
                    case "aassetcollection":
                        WriteApplicantAssets(dataApp, xmlWriter);
                        break;
                    case "abempcollection":
                        WriteApplicantEmployment(dataApp, xmlWriter, true);
                        break;
                    case "acempcollection":
                        WriteApplicantEmployment(dataApp, xmlWriter, false);
                        break;
                    case "arecollection":
                        WriteApplicantReo(dataApp, xmlWriter);
                        break;
                    case "aotherincomecollection":
                        WriteOtherIncome(dataApp, xmlWriter);
                        break;
                    case "amortgagelates":
                        WriteMortgageLates(dataApp, xmlWriter);
                        break;
                    default:
                        WriteInvalidField(xmlWriter, id);
                        break;
                }
            }
            #endregion
            xmlWriter.WriteEndElement(); // </applicant>
        }

        private static void WriteAliases(XmlWriter xmlWriter, string id, CAppData dataApp)
        {
            var aliases = id.ToLower().StartsWith("ab") ? dataApp.aBAliases : dataApp.aCAliases;
            var prefix = id.Substring(0, "axalias".Length);

            for (var i = 0; i < aliases.Count(); i++)
            {
                var element = prefix + (i + 1);
                aliases.ElementAt(i);
                WriteFieldValue(xmlWriter, element, aliases.ElementAt(i));
            }
        }

        private static void WriteVAPastLoans(XmlWriter xmlWriter, CAppData dataApp)
        {
            IVaPastLoanCollection collection = dataApp.aVaPastLCollection;
            int i = 0;
            while (i < collection.CountRegular)
            {
                IVaPastLoan vaPastL = collection.GetRegularRecordAt(i++);
                WriteFieldValue(xmlWriter, "aVaPastL" + i.ToString() + "DateOfLoan", vaPastL.DateOfLoan);
                WriteFieldValue(xmlWriter, "aVaPastL" + i.ToString() + "StAddr", vaPastL.StAddr);
                WriteFieldValue(xmlWriter, "aVaPastL" + i.ToString() + "CityState", vaPastL.CityState);
            }
        }

        /// <summary>
        /// For Undisclosed Debt Notification service - OPM 149195
        /// </summary>
        private static void WriteMclCreditDataForUDN(XmlWriter xmlWriter, CPageData dataLoan, CAppData dataApp)
        {
            //Load Credit Order - We have to be sure that it is from MCL, hence why we don't just use LoanApp.CreditReportData
            string ExternalFileId = null;
            Guid DbFileKey = Guid.Empty;
            Guid CompanyId = Guid.Empty;
            #region SQL RetrieveCreditReport
            SqlParameter[] parameters = {
                                            new SqlParameter("@ApplicationID", dataApp.aAppId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(dataLoan.sBrokerId, "RetrieveCreditReport", parameters))
            {
                if (reader.Read())
                {
                    ExternalFileId = (string)reader["ExternalFileId"];
                    DbFileKey = (Guid)reader["DbFileKey"];
                    CompanyId = (Guid)reader["ComId"];
                }
            }
            #endregion
            bool orderFound = DbFileKey != Guid.Empty && CRAIsMcl(CompanyId);
            WriteFieldValue(xmlWriter, "MclCreditReport_Found", orderFound);
            if (orderFound)
            {
                WriteFieldValue(xmlWriter, "MclCreditReport_ExternalFileId", ExternalFileId);
                WriteFieldValue(xmlWriter, "MclCreditReport_ComId", CompanyId.ToString());

                var debugInfo = new CreditReport.CreditReportDebugInfo(dataLoan.sLId, "");
                debugInfo.FileDbKey = DbFileKey.ToString();
                XmlDocument xmlDoc = CreditReport.CreditReportFactory.LoadFromFileDB(DbFileKey.ToString(), debugInfo);
                var creditReportProxy = CreditReport.CreditReportFactory.ConstructCreditReportFromXmlDoc(xmlDoc, debugInfo);
                if (creditReportProxy != null)
                {
                    if (creditReportProxy.BorrowerInfo != null)
                        WriteFieldValue(xmlWriter, "MclCreditReport_BorrowerSSN", creditReportProxy.BorrowerInfo.Ssn);
                    if (creditReportProxy.CoborrowerInfo != null)
                        WriteFieldValue(xmlWriter, "MclCreditReport_CoBorrowerSSN", creditReportProxy.CoborrowerInfo.Ssn);
                }
            }
        }

        private static bool CRAIsMcl(Guid craCompanyId)
        {
            var cra = CreditReport.MasterCRAList.FindById(craCompanyId, false);
            return cra != null && cra.Protocol == CreditReport.CreditReportProtocol.Mcl;
        }

        private static void WriteFieldValueDynamically(XmlWriter xmlWriter, string id, CPageData dataLoan, CAppData dataApp)
        {
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            id = id.TrimWhitespaceAndBOM();
            if (PageDataUtilities.ContainsField(id))
            {
                try
                {
                    string v = PageDataUtilities.GetValue(dataLoan, dataApp, id);
                    if (PageDataUtilities.ContainsBooleanField(id))
                    {
                        // 2/25/2011 dd - We use True or False for LOFormatExport instead of Yes/No
                        string True = "True";
                        string False = "False";

                        if (dataLoan.m_convertLos.FormatTargetCurrent == FormatTarget.MismoClosing && !DefaultFormatTargetBoolFields.Contains(id))
                        {
                            True = "Y";
                            False = "N";
                        }

                        if (v == "Yes")
                        {
                            v = True;
                        }
                        else
                        {
                            v = False;
                        }
                    }
                    WriteFieldValue(xmlWriter, id, v);
                }
                catch (CBaseException exc)
                {
                    Tools.LogError("Error " + id, exc);
                    WriteInvalidField(xmlWriter, id);
                }
            }
            else
            {
                WriteInvalidField(xmlWriter, id);
            }
        }

        private static void WriteAppraisalOrderTrackingCollection(CPageData dataLoan, AbstractUserPrincipal principal, XmlWriter w)
        {
            w.WriteStartElement("collection");
            w.WriteAttributeString("id", "sTrackingAppraisalOrders");

            var viewLoader = new AppraisalOrderViewLoader(dataLoan.sLId, dataLoan.sBrokerId, principal);
            var orderViews = viewLoader.Retrieve().Where(p => p.IsValid);
            int recordIndex1 = STARTING_RECORD_INDEX;

            foreach (var view in orderViews)
            {
                w.WriteStartElement("record");
                w.WriteAttributeString("index", recordIndex1++.ToString());
                w.WriteAttributeString("type", "AppraisalOrder");
                WriteFieldValue(w, "Id", view.AppraisalOrderId.ToString());
                WriteFieldValue(w, "ValuationMethod", view.ValuationMethod);
                WriteFieldValue(w, "DeliveryMethod", view.DeliveryMethod);
                WriteFieldValue(w, "AppraisalFormType", view.AppraisalFormType);
                WriteFieldValue(w, "ProductName", view.ProductName?.ToString());
                WriteFieldValue(w, "OrderNumber", view.OrderNumber?.ToString());
                WriteFieldValue(w, "FhaDocFileId", view.FhaDocFileId?.ToString());
                WriteFieldValue(w, "UcdpAppraisalId", view.UcdpAppraisalId?.ToString());
                WriteFieldValue(w, "Notes", view.Notes?.ToString());
                WriteFieldValue(w, "OrderedDate", view.OrderedDate_rep);
                WriteFieldValue(w, "NeededDate", view.NeededDate_rep);
                WriteFieldValue(w, "ExpirationDate", view.ExpirationDate_rep);
                WriteFieldValue(w, "ReceivedDate", view.ReceivedDate_rep);
                WriteFieldValue(w, "SentToBorrowerDate", view.SentToBorrowerDate_rep);
                WriteFieldValue(w, "BorrowerReceivedDate", view.BorrowerReceivedDate_rep);
                WriteFieldValue(w, "RevisionDate", view.RevisionDate_rep);
                WriteFieldValue(w, "ValuationEffectiveDate", view.ValuationEffectiveDate_rep);
                WriteFieldValue(w, "SubmittedToFhaDate", view.SubmittedToFhaDate_rep);
                WriteFieldValue(w, "SubmittedToUcdpDate", view.SubmittedToUcdpDate_rep);
                WriteFieldValue(w, "CuRiskScore", view.CuRiskScore_rep);
                WriteFieldValue(w, "OvervaluationRiskT", view.OvervaluationRiskT);
                WriteFieldValue(w, "PropertyEligibilityRiskT", view.PropertyEligibilityRiskT);
                WriteFieldValue(w, "AppraisalQualityRiskT", view.AppraisalQualityRiskT);
                WriteFieldValue(w, "Status", view.Status);
                WriteFieldValue(w, "StatusDate", view.StatusDate_rep);
                WriteFieldValue(w, "ForReo", view.HasLinkedReo);
                if (view.HasLinkedReo)
                {
                    WriteFieldValue(w, "ReoId", view.LinkedReoId?.ToString());
                }

                if (view.UploadedDocuments.Count > 0)
                {
                    w.WriteStartElement("collection");
                    w.WriteAttributeString("id", "Documents");

                    int docIndex = STARTING_RECORD_INDEX;
                    foreach (var doc in view.UploadedDocuments)
                    {
                        w.WriteStartElement("record");
                        w.WriteAttributeString("index", docIndex++.ToString());
                        w.WriteAttributeString("type", "AppraisalOrderEDoc");

                        WriteFieldValue(w, "FileName", doc.FileName);
                        if (doc.EDocId.HasValue)
                        {
                            WriteFieldValue(w, "EDocId", doc.EDocId.ToString());
                        }

                        w.WriteEndElement(); //</record>
                    }

                    w.WriteEndElement(); //</collection>
                }

                w.WriteEndElement(); // </record>
            }

            w.WriteEndElement();
        }

        /// <summary>
        /// Writes the collection of tasks accessible by the current user.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="principal">The user principal.</param>
        /// <param name="w">An XML writer.</param>
        private static void WriteTaskList(CPageData dataLoan, AbstractUserPrincipal principal, XmlWriter w)
        {
            if (!principal.BrokerDB.IsUseNewTaskSystem)
            {
                string message = "The LOXml collection sTaskList is only available under the new task system.";
                throw new CBaseException(message, message);
            }

            w.WriteStartElement("collection");
            w.WriteAttributeString("id", "sTaskList");

            var tasks = Task.GetTasksByEmployeeAccess(principal.BrokerId, dataLoan.sLId, principal.UserId);
            int recordIndex = STARTING_RECORD_INDEX;

            EDocumentRepository repo = EDocumentRepository.GetUserRepository();
            Lazy<Dictionary<Guid, EDocument>> readableEdocs = new Lazy<Dictionary<Guid, EDocument>>(() => repo.GetDocumentsByLoanId(dataLoan.sLId).ToDictionary((edoc) => edoc.DocumentId));
            Lazy<Dictionary<int, DocType>> docTypes = new Lazy<Dictionary<int, DocType>>(() => EDocumentDocType.GetDocTypesByBroker(principal.BrokerId, false).ToDictionary((docType) => docType.Id));

            foreach (var task in tasks)
            {
                var isHiddenCondition = task.TaskIsCondition && task.CondIsHidden;

                if (isHiddenCondition && !Task.CanUserViewHiddenInformation(principal))
                {
                    continue;
                }

                w.WriteStartElement("record");
                w.WriteAttributeString("index", recordIndex++.ToString());
                w.WriteAttributeString("type", "Task");

                WriteFieldValue(w, "TaskId", task.TaskId);
                WriteFieldValue(w, "LoanId", task.LoanId.ToString());
                WriteFieldValue(w, "LoanNumber", task.LoanNumCached);
                WriteFieldValue(w, "TaskIsCondition", task.TaskIsCondition);
                WriteFieldValue(w, "TaskSubject", task.TaskSubject);
                WriteFieldValue(w, "BorrowerFullName", task.BorrowerNmCached);
                WriteFieldValue(w, "TaskOwnerFullName", task.OwnerFullName);
                WriteFieldValue(w, "TaskOwnerUsername", EmployeeDB.RetrieveUserLoginByUserId(principal.BrokerId, task.TaskOwnerUserId));
                WriteFieldValue(w, "TaskOwnerUserType", Tools.IsPmlUser(task.TaskOwnerUserId, principal.BrokerId) ? "P" : "B");
                WriteFieldValue(w, "TaskAssignedUserFullName", task.AssignedUserFullName);
                WriteFieldValue(w, "TaskAssignedUserUsername", EmployeeDB.RetrieveUserLoginByUserId(principal.BrokerId, task.TaskAssignedUserId));
                WriteFieldValue(w, "TaskAssignedUserUserType", Tools.IsPmlUser(task.TaskAssignedUserId, principal.BrokerId) ? "P" : "B");
                WriteFieldValue(w, "TaskStatus", task.TaskStatus_rep);
                
                if (task.TaskStatus == E_TaskStatus.Closed)
                {
                    WriteFieldValue(w, "TaskClosedDate", task.TaskClosedDate_rep);
                }

                WriteFieldValue(w, "TaskDueDate", task.TaskDueDate_rep);
                WriteFieldValue(w, "TaskDueDateLocked", task.TaskDueDateLocked);
                WriteFieldValue(w, "DueDateDescription", task.DueDateDescription(sanitize: true));
                WriteFieldValue(w, "TaskFollowUpDate", task.TaskFollowUpDate_rep);
                WriteFieldValue(w, "TaskPermissionLevelId", task.TaskPermissionLevelId_rep);
                WriteFieldValue(w, "PermissionLevelName", task.PermissionLevelName);
                WriteFieldValue(w, "PermissionLevelDescription", task.PermissionLevelDescription);

                WriteFieldValue(w, "IsResolutionBlocked", task.IsResolutionBlocked());
                WriteFieldValue(w, "ResolutionBlockTriggerName", task.ResolutionBlockTriggerName);
                WriteFieldValue(w, "ResolutionDenialMessage", task.ResolutionDenialMessage);
                WriteFieldValue(w, "ResolutionDateSetterFieldId", task.ResolutionDateSetterFieldId);
                WriteFieldValue(w, "ResolutionDateSetterFieldDescription", task.GetResolutionDateSetterFieldDescription());

                // A non-breaking space gets inserted in the HTML somewhere in the task system guts,
                // and the XmlWriter will choke on it if it's not stripped out.
                WriteCData(w, "TaskHistoryHtml", task.TaskHistoryHtml.Replace("\u00A0", " "));

                if (task.TaskIsCondition)
                {
                    WriteFieldValue(w, "CondIsHidden", task.CondIsHidden);
                    WriteFieldValue(w, "CondCategoryId", task.CondCategoryId.ToString());

                    if (task.CondCategoryId.HasValue)
                    {
                        var category = ConditionCategory.GetCategory(principal.BrokerId, task.CondCategoryId.Value);
                        WriteFieldValue(w, "CondCategory", category.Category);
                    }

                    WriteFieldValue(w, "CondInternalNotes", task.CondInternalNotes);
                    WriteFieldValue(w, "CondIsDeleted", task.CondIsDeleted);

                    if (task.CondIsDeleted)
                    {
                        WriteFieldValue(w, "CondDeletedDate", task.CondDeletedDate_rep);
                        WriteFieldValue(w, "CondDeletedBy", task.CondDeletedByUserNm);
                    }

                    WriteConditionDocumentInfo(w, task, principal, dataLoan.sLId, readableEdocs, docTypes);
                }

                w.WriteEndElement(); // </record>
            }

            w.WriteEndElement(); // </collection>
        }

        private static void WriteTrustCollection(CPageData dataLoan, XmlWriter w)
        {
            w.WriteStartElement("collection");
            w.WriteAttributeString("id", "sTrustCollection");

            TrustCollection tc = dataLoan.sTrustCollection;
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (var trustor in tc.Trustors)
            {
                w.WriteStartElement("record");
                w.WriteAttributeString("index", recordIndex1++.ToString());
                w.WriteAttributeString("type", "Trustor");
                WriteFieldValue(w, "Name", trustor.Name);
                w.WriteEndElement();
            }
            int trusteeIndex = 0;
            foreach (var trustee in tc.Trustees)
            {
                w.WriteStartElement("record");
                w.WriteAttributeString("index", recordIndex1++.ToString());
                w.WriteAttributeString("type", "Trustee");
                WriteFieldValue(w, "TrusteeID", (++trusteeIndex).ToString());
                WriteFieldValue(w, "Name", trustee.Name);
                WriteFieldValue(w, "Phone", trustee.Phone);
                WriteFieldValue(w, "CanSignCertOfTrust", trustee.CanSignCertOfTrust, dataLoan.GetFormatTarget());
                WriteFieldValue(w, "CanSignLoanDocs", trustee.CanSignLoanDocs, dataLoan.GetFormatTarget());
                WriteFieldValue(w, "SignatureVerbiage", trustee.SignatureVerbiage);

                WriteFieldValue(w, "StreetAddress", trustee.Address.StreetAddress);
                WriteFieldValue(w, "City", trustee.Address.City);
                WriteFieldValue(w, "PostalCode", trustee.Address.PostalCode);
                WriteFieldValue(w, "State", trustee.Address.State);

                w.WriteEndElement();
            }

            w.WriteEndElement();

        }

        /// <summary>
        /// Writes a set of archived data. For now it just retrieves the last disclosed archive, but it
        /// is implemented as a collection so we can add more archives in the future.
        /// This method is only valid for non-Legacy loans.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="xmlWriter">An XML writer.</param>
        /// <param name="format">The target format for archive data.</param>
        private static void WriteClosingCostArchives(CPageData dataLoan, XmlWriter xmlWriter, FormatTarget format, bool useTempArchiveForTridFile, AbstractUserPrincipal principal)
        {
            if (dataLoan.sIsLegacyClosingCostVersion || false == dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                return;
            }

            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "ClosingCostArchives");

            xmlWriter.WriteStartElement("record");
            xmlWriter.WriteAttributeString("type", "ClosingCostArchive");

            if (useTempArchiveForTridFile && dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                dataLoan.ApplyTemporaryArchive(principal);
            }
            else
            {
                dataLoan.ApplyClosingCostArchive(dataLoan.sLastDisclosedClosingCostArchive);
            }

            WriteFieldValue(xmlWriter, "sTRIDLoanEstimateLenderCredits", dataLoan.sTRIDLoanEstimateLenderCredits_rep);
            WriteFieldValue(xmlWriter, "sTRIDClosingDisclosureGeneralLenderCredits", dataLoan.sTRIDClosingDisclosureGeneralLenderCredits_rep);
            WriteFieldValue(xmlWriter, "sToleranceCure_Neg", dataLoan.sToleranceCure_Neg_rep);
            WriteFieldValue(xmlWriter, "sLenderPaidFeesAmt_Neg", dataLoan.sLenderPaidFeesAmt_Neg_rep);
            WriteFieldValue(xmlWriter, "sLenderGeneralCreditAmt_Neg", dataLoan.sLenderGeneralCreditAmt_Neg_rep);
            WriteFieldValue(xmlWriter, "sTRIDLoanEstimateGeneralLenderCredits", dataLoan.sTRIDLoanEstimateGeneralLenderCredits_rep);
            WriteFieldValue(xmlWriter, "sTRIDClosingDisclosureGeneralLenderCreditsLessToleranceCures", dataLoan.sTRIDClosingDisclosureGeneralLenderCreditsLessToleranceCures_rep);
            WriteFieldValue(xmlWriter, "sGfeLenderCreditFProps_Apr", dataLoan.sGfeLenderCreditFProps_Apr);
            WriteFieldValue(xmlWriter, "sLenderCustomCredit1DiscloseLocationT", dataLoan.sLenderCustomCredit1DiscloseLocationT);
            WriteFieldValue(xmlWriter, "sLenderCustomCredit1Description", dataLoan.sLenderCustomCredit1Description);
            WriteFieldValue(xmlWriter, "sLenderCustomCredit1Amount", dataLoan.sLenderCustomCredit1Amount_rep);
            WriteFieldValue(xmlWriter, "sLenderCustomCredit1AmountAsCharge", dataLoan.sLenderCustomCredit1AmountAsCharge_rep);
            WriteFieldValue(xmlWriter, "sLenderCustomCredit2DiscloseLocationT", dataLoan.sLenderCustomCredit2DiscloseLocationT);
            WriteFieldValue(xmlWriter, "sLenderCustomCredit2Description", dataLoan.sLenderCustomCredit2Description);
            WriteFieldValue(xmlWriter, "sLenderCustomCredit2Amount", dataLoan.sLenderCustomCredit2Amount_rep);
            WriteFieldValue(xmlWriter, "sLenderCustomCredit2AmountAsCharge", dataLoan.sLenderCustomCredit2AmountAsCharge_rep);
            WriteFieldValue(xmlWriter, "sTRIDClosingDisclosureCashToClose", dataLoan.sTRIDClosingDisclosureCashToClose_rep);

            dataLoan.ClearClosingCostArchive();

            xmlWriter.WriteEndElement(); // </record>
            xmlWriter.WriteEndElement(); // </collection>
        }

        private static void WriteClosingCostArchiveMetadata(CPageData dataLoan, XmlWriter xmlWriter)
        {
            if (dataLoan.sIsLegacyClosingCostVersion || !dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                return;
            }

            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "DisclosureArchiveMetadata");

            int index = 0;
            var tridTargetVersion = dataLoan.sTridTargetRegulationVersionT;
            var loanArchives = dataLoan.sClosingCostArchive.Where(a =>
                a.ClosingCostArchiveType != ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate ||
                ClosingCostArchive.IsDisclosedDataArchive(tridTargetVersion, a));

            foreach (var archive in loanArchives)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", (++index).ToString());
                xmlWriter.WriteAttributeString("type", "DisclosureArchiveMetadata");

                WriteFieldValue(xmlWriter, "Id", archive.Id.ToString());
                WriteFieldValue(xmlWriter, "DateArchived", archive.DateArchived);
                WriteFieldValue(xmlWriter, "ArchiveType", archive.ClosingCostArchiveType);
                WriteFieldValue(xmlWriter, "Status", archive.Status);

                xmlWriter.WriteEndElement(); // </record>
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Writes the set of Loan Estimate disclosure dates to LOXML for exporting.
        /// </summary>
        /// <param name="dataLoan">The dataset for an individual loan.</param>
        /// <param name="xmlWriter">An XML writer.</param>
        private static void WriteLoanEstimateDatesInfo(CPageData dataLoan, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sLoanEstimateDatesInfo");

            var converter = dataLoan.m_convertLos;
            int recordIndex1 = STARTING_RECORD_INDEX;

            Dictionary<Guid, ClosingCostArchive> validArchives = dataLoan.GetValidArchivesForLoanEstimateDates();
            foreach (var disclosureDate in dataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.OrderByDescending(d => d.CreatedDate))
            {
                bool hasValidArchive = disclosureDate.ArchiveId == Guid.Empty || validArchives.ContainsKey(disclosureDate.ArchiveId);

                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                xmlWriter.WriteAttributeString("type", "LoanEstimateDates");

                WriteFieldValue(xmlWriter, "Id", disclosureDate.UniqueId.ToString()); 
                WriteFieldValue(xmlWriter, "CreatedDate", converter.ToDateTimeString(disclosureDate.CreatedDate));
                WriteFieldValue(xmlWriter, "IssuedDate", converter.ToDateTimeString(disclosureDate.IssuedDate));
                WriteFieldValue(xmlWriter, "DeliveryMethod", disclosureDate.DeliveryMethod);
                WriteFieldValue(xmlWriter, "ReceivedDate", converter.ToDateTimeString(disclosureDate.ReceivedDate));
                WriteFieldValue(xmlWriter, "SignedDate", converter.ToDateTimeString(disclosureDate.SignedDate));
                WriteFieldValue(xmlWriter, "ArchiveId", disclosureDate.ArchiveId.ToString());
                WriteFieldValue(xmlWriter, "ArchiveUsed", converter.ToDateTimeString(disclosureDate.ArchiveDate)); // Archive Date
                WriteFieldValue(xmlWriter, "IsInitial", disclosureDate.IsInitial);
                WriteFieldValue(xmlWriter, "LastDisclosedTRIDLoanProductDescription", disclosureDate.LastDisclosedTRIDLoanProductDescription); // Product
                WriteFieldValue(xmlWriter, "TransactionId", disclosureDate.TransactionId);
                WriteFieldValue(xmlWriter, "IsManual", disclosureDate.IsManual);
                WriteFieldValue(xmlWriter, "Valid", hasValidArchive);
                WriteFieldValue(xmlWriter, "DisclosedAprLckd", disclosureDate.DisclosedAprLckd);
                WriteFieldValue(xmlWriter, "DisclosedApr", disclosureDate.DisclosedApr_rep);
                WriteFieldValue(xmlWriter, "LqbApr", disclosureDate.LqbApr_rep);
                WriteFieldValue(xmlWriter, "DocVendorApr", disclosureDate.DocVendorApr_rep);

                WriteDisclosureDatesByConsumerId(xmlWriter, disclosureDate.DisclosureDatesByConsumerId, converter);

                xmlWriter.WriteEndElement(); // </record>
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Writes the set of Closing Disclosure disclosure dates to LOXML for exporting.
        /// </summary>
        /// <param name="dataLoan">The dataset for an individual loan.</param>
        /// <param name="xmlWriter">An XML writer.</param>
        private static void WriteClosingDisclosureDatesInfo(CPageData dataLoan, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sClosingDisclosureDatesInfo");

            var converter = dataLoan.m_convertLos;
            int recordIndex1 = STARTING_RECORD_INDEX;
            Dictionary<Guid, ClosingCostArchive> validArchives = dataLoan.GetValidArchivesForClosingDisclosureDates();
            foreach (var disclosureDate in dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.OrderByDescending(d => d.CreatedDate))
            {
                bool hasValidArchive = disclosureDate.ArchiveId == Guid.Empty || validArchives.ContainsKey(disclosureDate.ArchiveId); 
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                xmlWriter.WriteAttributeString("type", "ClosingDisclosureDates");

                WriteFieldValue(xmlWriter, "Id", disclosureDate.UniqueId.ToString());
                WriteFieldValue(xmlWriter, "CreatedDate", converter.ToDateTimeString(disclosureDate.CreatedDate));
                WriteFieldValue(xmlWriter, "IssuedDate", converter.ToDateTimeString(disclosureDate.IssuedDate));
                WriteFieldValue(xmlWriter, "DeliveryMethod", disclosureDate.DeliveryMethod);
                WriteFieldValue(xmlWriter, "ReceivedDate", converter.ToDateTimeString(disclosureDate.ReceivedDate));
                WriteFieldValue(xmlWriter, "SignedDate", converter.ToDateTimeString(disclosureDate.SignedDate));
                WriteFieldValue(xmlWriter, "ArchiveId", disclosureDate.ArchiveId.ToString());
                WriteFieldValue(xmlWriter, "ArchiveUsed", converter.ToDateTimeString(disclosureDate.ArchiveDate));
                WriteFieldValue(xmlWriter, "IsInitial", disclosureDate.IsInitial);
                WriteFieldValue(xmlWriter, "TransactionId", disclosureDate.TransactionId);
                WriteFieldValue(xmlWriter, "IsManual", disclosureDate.IsManual);
                WriteFieldValue(xmlWriter, "Valid", hasValidArchive);
                WriteFieldValue(xmlWriter, "IsPreview", disclosureDate.IsPreview);
                WriteFieldValue(xmlWriter, "IsFinal", disclosureDate.IsFinal);
                WriteFieldValue(xmlWriter, "IsPostClosing", disclosureDate.IsPostClosing);
                WriteFieldValue(xmlWriter, "LastDisclosedTRIDLoanProductDescription", disclosureDate.LastDisclosedTRIDLoanProductDescription);
                WriteFieldValue(xmlWriter, "UcdDocument", disclosureDate.UcdDocument.ToString());
                WriteFieldValue(xmlWriter, "IsDisclosurePostClosingDueToCureForToleranceViolation", disclosureDate.IsDisclosurePostClosingDueToCureForToleranceViolation);
                WriteFieldValue(xmlWriter, "IsDisclosurePostClosingDueToNonNumericalClericalError", disclosureDate.IsDisclosurePostClosingDueToNonNumericalClericalError);
                WriteFieldValue(xmlWriter, "IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller", disclosureDate.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidBySeller);
                WriteFieldValue(xmlWriter, "IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower", disclosureDate.IsDisclosurePostClosingDueToNumericalChangeInAmountPaidByBorrower);
                WriteFieldValue(xmlWriter, "PostConsummationKnowledgeOfEventDate", converter.ToDateTimeString(disclosureDate.PostConsummationKnowledgeOfEventDate));
                WriteFieldValue(xmlWriter, "PostConsummationRedisclosureReasonDate", converter.ToDateTimeString(disclosureDate.PostConsummationRedisclosureReasonDate));
                WriteFieldValue(xmlWriter, "DisclosedAprLckd", disclosureDate.DisclosedAprLckd);
                WriteFieldValue(xmlWriter, "DisclosedApr", disclosureDate.DisclosedApr_rep);
                WriteFieldValue(xmlWriter, "LqbApr", disclosureDate.LqbApr_rep);
                WriteFieldValue(xmlWriter, "DocVendorApr", disclosureDate.DocVendorApr_rep);
                WriteFieldValue(xmlWriter, "DocCode", disclosureDate.DocCode);

                WriteDisclosureDatesByConsumerId(xmlWriter, disclosureDate.DisclosureDatesByConsumerId, converter);

                xmlWriter.WriteEndElement(); // </record>
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Writes disclosure dates by consumer ID to the specified XML writer.
        /// </summary>
        /// <param name="xmlWriter">
        /// The XML writer.
        /// </param>
        /// <param name="disclosureDatesByConsumerId">
        /// The disclosure dates by consumer ID.
        /// </param>
        /// <param name="converter">
        /// The conversion object.
        /// </param>
        private static void WriteDisclosureDatesByConsumerId(
            XmlWriter xmlWriter,
            ReadOnlyCollection<KeyValuePair<Guid, BorrowerDisclosureDates>> disclosureDatesByConsumerId,
            LosConvert converter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "DisclosureDatesByConsumerId");
            var disclosureDatesIndex = STARTING_RECORD_INDEX;

            foreach (var disclosureDatesPair in disclosureDatesByConsumerId)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", disclosureDatesIndex++.ToString());
                xmlWriter.WriteAttributeString("type", "DisclosureDatesByConsumerId");
                
                var disclosureDates = disclosureDatesPair.Value;

                WriteFieldValue(xmlWriter, "ConsumerId", disclosureDatesPair.Key.ToString());
                WriteFieldValue(xmlWriter, "ConsumerName", disclosureDates.ConsumerName);
                WriteFieldValue(xmlWriter, "ExcludeFromCalculations", disclosureDates.ExcludeFromCalculations);
                WriteFieldValue(xmlWriter, "IssuedDate", converter.ToDateTimeString(disclosureDates.IssuedDate ?? CDateTime.InvalidDateTime));
                WriteFieldValue(xmlWriter, "DeliveryMethod", disclosureDates.DeliveryMethod);
                WriteFieldValue(xmlWriter, "ReceivedDate", converter.ToDateTimeString(disclosureDates.ReceivedDate ?? CDateTime.InvalidDateTime));
                WriteFieldValue(xmlWriter, "SignedDate", converter.ToDateTimeString(disclosureDates.SignedDate ?? CDateTime.InvalidDateTime));

                xmlWriter.WriteEndElement(); // </record>
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Outputs the available closing cost fees that can be added to a loan file.
        /// </summary>
        /// <param name="dataLoan">The dataset for an individual loan.</param>
        /// <param name="collectionId">A string indicating whether borrower responsible or seller responsible closing cost set is being referenced.</param>
        /// <param name="xmlWriter">An XML writer.</param>
        private static void WriteAvailableClosingCostSet(CPageData dataLoan, string collectionId, XmlWriter xmlWriter)
        {
            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            FeeSetupClosingCostSet brokerBaseClosingCostFeeCollection = db.GetUnlinkedClosingCostSet();

            bool isBorrowerClosingCostSet = collectionId.Equals(BORROWERCLOSINGCOSTSET);
            LoanClosingCostSet closingCosts = isBorrowerClosingCostSet ? (LoanClosingCostSet)dataLoan.sClosingCostSet : (LoanClosingCostSet)dataLoan.sSellerResponsibleClosingCostSet;

            Func<BaseClosingCostFee, bool> setFilter = null;
            if (isBorrowerClosingCostSet)
            {
                setFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostSet)closingCosts,
                                                                                     false,
                                                                                     dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                     dataLoan.sDisclosureRegulationT,
                                                                                     dataLoan.sGfeIsTPOTransaction);
            }
            IEnumerable<Guid> loanClosingCostFeeTypeIds = closingCosts.GetFees(setFilter).Select(c => c.ClosingCostFeeTypeId);

            Func<BaseClosingCostFee, bool> selector;
            Func<BaseClosingCostFee, bool> baseSelector = c => c.CanManuallyAddToEditor && !loanClosingCostFeeTypeIds.Contains(c.ClosingCostFeeTypeId);

            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated)
            {
                Func<BaseClosingCostFee, bool> additionalSelector = BuildLegacyButMigratedSelector(closingCosts, setFilter);
                selector = c => baseSelector(c) && additionalSelector(c);
            }
            else
            {
                selector = baseSelector;
            }

            IEnumerable<BaseClosingCostFee> availableFees = brokerBaseClosingCostFeeCollection.GetFees(selector);

            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", isBorrowerClosingCostSet ? "sAvailableClosingCostSet" : "sAvailableSellerResponsibleClosingCostSet");
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (BaseClosingCostFee availableFee in availableFees)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "Beneficiary", availableFee.Beneficiary);
                WriteFieldValue(xmlWriter, "CanShop", availableFee.CanShop);
                WriteFieldValue(xmlWriter, "ClosingCostFeeType", availableFee.ClosingCostFeeTypeId.ToString());
                WriteFieldValue(xmlWriter, "Description", availableFee.Description);
                WriteFieldValue(xmlWriter, "Dflp", availableFee.Dflp);
                WriteFieldValue(xmlWriter, "FormulaT", availableFee.FormulaT);
                WriteFieldValue(xmlWriter, "IntegratedDisclosureSectionT", availableFee.IntegratedDisclosureSectionT);
                WriteFieldValue(xmlWriter, "GfeSectionT", availableFee.GfeSectionT);
                WriteFieldValue(xmlWriter, "HudLine", availableFee.HudLine_rep);
                WriteFieldValue(xmlWriter, "IsAffiliate", availableFee.IsAffiliate);
                WriteFieldValue(xmlWriter, "IsApr", availableFee.IsApr);
                WriteFieldValue(xmlWriter, "IsFhaAllowable", availableFee.IsFhaAllowable);
                WriteFieldValue(xmlWriter, "IsOptional", availableFee.IsOptional);
                WriteFieldValue(xmlWriter, "IsSystemLegacyFee", availableFee.IsSystemLegacyFee);
                WriteFieldValue(xmlWriter, "IsThirdParty", availableFee.IsThirdParty);
                WriteFieldValue(xmlWriter, "IsTitleFee", availableFee.IsTitleFee);
                WriteFieldValue(xmlWriter, "IsVaAllowable", availableFee.IsVaAllowable);
                WriteFieldValue(xmlWriter, "LegacyGfeFieldT", availableFee.LegacyGfeFieldT);
                WriteFieldValue(xmlWriter, "MismoFeeT", availableFee.MismoFeeT);
                xmlWriter.WriteEndElement(); // </record>
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Builds the selector for filtering available closing costs to add in legacy but migrated loan files.
        /// </summary>
        /// <param name="closingCosts">The loan file closing cost set.</param>
        /// <returns>Returns a function used to filter available closing costs.</returns>
        private static Func<BaseClosingCostFee, bool> BuildLegacyButMigratedSelector(LoanClosingCostSet closingCosts, Func<BaseClosingCostFee, bool> filter)
        {
            Dictionary<int, int> customFeesCount = new Dictionary<int, int>(5)
            {
                {800, 0},
                {900, 0},
                {1100, 0},
                {1200, 0},
                {1300, 0}
            };

            HashSet<int> availableCustomFees = new HashSet<int>(BaseClosingCostFeeSection.AllowableCustomFeesForLegacyButMigrated);

            //// If existing fee on loan file has hudline in AllowableCustomFeesForLegacyButMigrated, remove it. Keep track of the number of custom fees by section.
            foreach (LoanClosingCostFee closingCost in closingCosts.GetFees(filter))
            {
                if (closingCost.LegacyGfeFieldT == E_LegacyGfeFieldT.Undefined && StaticMethodsAndExtensions.IsInRange(closingCost.HudLine, 800, 1399))
                {
                    availableCustomFees.Remove(closingCost.HudLine);

                    int key = (closingCost.HudLine - closingCost.HudLine % 100);
                    int count = 0;
                    if (customFeesCount.TryGetValue(key, out count))
                    {
                        customFeesCount[key]++;
                    }
                }
            }

            //// Check the number of 800, 900, 1100, 1200, 1300 section custom fees on the file.
            //// If the section custom fee count is below the maximum, there is room to add custom fees. Include the generic *00 HUD line for the given section.
            //// Else, remove all numbers of the given section from availableCustomFees.
            foreach (int key in customFeesCount.Keys)
            {
                int maxCount = 0;
                if (BaseClosingCostFeeSection.MaxCustomCountPerSection.TryGetValue(key, out maxCount) && customFeesCount[key] < maxCount)
                {
                    availableCustomFees.Add(key);
                }
                else
                {
                    availableCustomFees.RemoveWhere(customFee => customFee - customFee % 100 == key);
                }
            }

            return c => c.LegacyGfeFieldT != E_LegacyGfeFieldT.Undefined || availableCustomFees.Contains(c.HudLine);
        }

        /// <summary>
        /// Writes the set of closing costs to LOXML for exporting.
        /// </summary>
        /// <param name="dataLoan">The dataset for an individual loan.</param>
        /// <param name="collectionId">A string indicating whether borrower responsible or seller responsible closing cost set is being referenced.</param>
        /// <param name="xmlWriter">An XML writer.</param>
        /// <param name="isClosingDisclosure">Indicates whether closing data should be written.</param>
        private static void WriteClosingCostSet(CPageData dataLoan, string collectionId, XmlWriter xmlWriter, bool isClosingDisclosure)
        {
            bool isBorrowerClosingCostSet = collectionId.Equals(BORROWERCLOSINGCOSTSET);
            LoanClosingCostSet closingCosts = isBorrowerClosingCostSet ? (LoanClosingCostSet)dataLoan.sClosingCostSet : (LoanClosingCostSet)dataLoan.sSellerResponsibleClosingCostSet;

            Func<BaseClosingCostFee, bool> setFilter = null;
            if (isBorrowerClosingCostSet)
            {
                setFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter((BorrowerClosingCostSet)closingCosts,
                                                                                     false,
                                                                                     dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                                     dataLoan.sDisclosureRegulationT,
                                                                                     dataLoan.sGfeIsTPOTransaction);
            }

            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", collectionId);
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (LoanClosingCostFee closingCost in closingCosts.GetFees(setFilter))
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                xmlWriter.WriteAttributeString("type", "ClosingCostFee");

                WriteFieldValue(xmlWriter, "BaseAmount", closingCost.BaseAmount_rep);
                WriteFieldValue(xmlWriter, "Beneficiary", closingCost.Beneficiary);
                WriteFieldValue(xmlWriter, "BeneficiaryType", closingCost.BeneficiaryType);
                WriteFieldValue(xmlWriter, "BeneficiaryAgentId", closingCost.BeneficiaryAgentId.ToString());
                WriteFieldValue(xmlWriter, "BeneficiaryDescription", closingCost.BeneficiaryDescription);
                WriteFieldValue(xmlWriter, "CanShop", closingCost.CanShop);
                WriteFieldValue(xmlWriter, "ClosingCostFeeType", closingCost.ClosingCostFeeTypeId.ToString());
                WriteFieldValue(xmlWriter, "Description", closingCost.Description);
                WriteFieldValue(xmlWriter, "Dflp", closingCost.Dflp);
                WriteFieldValue(xmlWriter, "DidShop", closingCost.DidShop);
                if (isBorrowerClosingCostSet)
                {
                    WriteFieldValue(xmlWriter, "FinancedQmAmount", ((BorrowerClosingCostFee)closingCost).FinancedQmAmount_rep);
                }

                WriteFieldValue(xmlWriter, "FormulaT", closingCost.FormulaT);
                WriteFieldValue(xmlWriter, "IntegratedDisclosureSectionT", closingCost.GetTRIDSectionBasedOnDisclosure(isClosingDisclosure || (dataLoan.sInitialClosingDisclosureIssuedD.IsValid && dataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Any(d => d.IsInitial))));
                if (isBorrowerClosingCostSet)
                {
                    WriteFieldValue(xmlWriter, "GfeProviderChoiceT",  ((BorrowerClosingCostFee)closingCost).GfeProviderChoiceT);
                }
                WriteFieldValue(xmlWriter, "GfeResponsiblePartyT", closingCost.GfeResponsiblePartyT);
                WriteFieldValue(xmlWriter, "GfeSectionT", closingCost.GfeSectionT);
                WriteFieldValue(xmlWriter, "HudLine", closingCost.HudLine_rep);
                WriteFieldValue(xmlWriter, "Id", closingCost.UniqueId.ToString());
                WriteFieldValue(xmlWriter, "IsAffiliate", closingCost.IsAffiliate);
                WriteFieldValue(xmlWriter, "IsApr", closingCost.IsApr);
                WriteFieldValue(xmlWriter, "IsFhaAllowable", closingCost.IsFhaAllowable);
                if (isBorrowerClosingCostSet)
                {
                    WriteFieldValue(xmlWriter, "IsIncludedInQm", ((BorrowerClosingCostFee)closingCost).IsIncludedInQm);
                }
                WriteFieldValue(xmlWriter, "IsOptional", closingCost.IsOptional);
                if (isBorrowerClosingCostSet)
                {
                    WriteFieldValue(xmlWriter, "IsShowQmWarning", ((BorrowerClosingCostFee)closingCost).IsShowQmWarning);
                }
                WriteFieldValue(xmlWriter, "IsSystemLegacyFee", closingCost.IsSystemLegacyFee);
                WriteFieldValue(xmlWriter, "IsThirdParty", closingCost.IsThirdParty);
                WriteFieldValue(xmlWriter, "IsTitleFee", closingCost.IsTitleFee);
                WriteFieldValue(xmlWriter, "IsVaAllowable", closingCost.IsVaAllowable);
                WriteFieldValue(xmlWriter, "LegacyGfeFieldT", closingCost.LegacyGfeFieldT);
                WriteFieldValue(xmlWriter, "MismoFeeT", closingCost.MismoFeeT);
                WriteFieldValue(xmlWriter, "NumberOfPeriods", closingCost.NumberOfPeriods_rep);
                WriteFieldValue(xmlWriter, "OriginalDescription", closingCost.OriginalDescription);
                WriteFieldValue(xmlWriter, "Percent", closingCost.Percent_rep);
                WriteFieldValue(xmlWriter, "PercentBaseT", closingCost.PercentBaseT);
                if (isBorrowerClosingCostSet)
                {
                    WriteFieldValue(xmlWriter, "ProviderChosenByLender", ((BorrowerClosingCostFee)closingCost).ProviderChosenByLender);
                    WriteFieldValue(xmlWriter, "QmAmount", ((BorrowerClosingCostFee)closingCost).QmAmount_rep);
                }
                WriteFieldValue(xmlWriter, "TotalAmount", closingCost.TotalAmount_rep);

                xmlWriter.WriteStartElement("collection");
                xmlWriter.WriteAttributeString("id", "Payments");
                int recordIndex2 = STARTING_RECORD_INDEX;
                foreach (LoanClosingCostFeePayment payment in closingCost.Payments)
                {
                    xmlWriter.WriteStartElement("record");
                    xmlWriter.WriteAttributeString("index", recordIndex2++.ToString());
                    xmlWriter.WriteAttributeString("type", "ClosingCostFeePayment");

                    WriteFieldValue(xmlWriter, "Amount", payment.Amount_rep);
                    WriteFieldValue(xmlWriter, "Entity", payment.Entity);
                    WriteFieldValue(xmlWriter, "GfeClosingCostFeePaymentTimingT", payment.GfeClosingCostFeePaymentTimingT);
                    WriteFieldValue(xmlWriter, "Id", payment.Id.ToString());
                    WriteFieldValue(xmlWriter, "IsFinanced", payment.IsFinanced);
                    WriteFieldValue(xmlWriter, "IsMade", payment.IsMade);
                    WriteFieldValue(xmlWriter, "IsSystemGenerated", payment.IsSystemGenerated);
                    WriteFieldValue(xmlWriter, "PaidByT", payment.PaidByT);
                    WriteFieldValue(xmlWriter, "PaymentDate", payment.PaymentDate_rep);
                    WriteFieldValue(xmlWriter, "ResponsiblePartyT", payment.ResponsiblePartyT);

                    xmlWriter.WriteEndElement(); // </record>
                }

                xmlWriter.WriteEndElement(); // </collection>
                xmlWriter.WriteEndElement(); // </record>
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Write fields under sHousingExpenses to LOXML for exporting.
        /// </summary>
        /// <param name="dataLoan">The dataset for an individual loan.</param>
        /// <param name="xmlWriter">An XML writer.</param>
        private static void WriteHousingExpenses(CPageData dataLoan, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sHousingExpenses");

            var converter = dataLoan.m_convertLos;
            int recordIndex1 = STARTING_RECORD_INDEX;
            WriteBaseHousingExpenseCollection(dataLoan.sHousingExpenses.AssignedExpenses, xmlWriter, converter, ref recordIndex1);
            WriteBaseHousingExpenseCollection(dataLoan.sHousingExpenses.UnassignedExpenses, xmlWriter, converter, ref recordIndex1);

            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Write a collection of housing expenses to LOXML for exporting.
        /// </summary>
        /// <param name="expenses">A collection of housing expenses.</param>
        /// <param name="xmlWriter">An XML writer.</param>
        /// <param name="converter">An LOS converter.</param>
        /// <param name="recordIndex">Starting index of collection when invoked.</param>
        private static void WriteBaseHousingExpenseCollection(ICollection<BaseHousingExpense> expenses, XmlWriter xmlWriter, LosConvert converter, ref int recordIndex)
        {
            foreach (var expense in expenses)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex++.ToString());
                xmlWriter.WriteAttributeString("type", "BaseHousingExpense");

                WriteFieldValue(xmlWriter, "AnnualAmt", expense.AnnualAmt_rep);
                WriteFieldValue(xmlWriter, "AnnualAmtCalcBaseAmt", expense.AnnualAmtCalcBaseAmt_rep);
                WriteFieldValue(xmlWriter, "AnnualAmtCalcBasePerc", expense.AnnualAmtCalcBasePerc_rep);
                WriteFieldValue(xmlWriter, "AnnualAmtCalcBaseType", expense.AnnualAmtCalcBaseType);
                WriteFieldValue(xmlWriter, "AnnualAmtCalcType", expense.AnnualAmtCalcType);
                WriteFieldValue(xmlWriter, "CustomExpenseLineNum", expense.CustomExpenseLineNum);
                WriteFieldValue(xmlWriter, "DisbursementRepInterval", expense.DisbursementRepInterval);
                WriteFieldValue(xmlWriter, "HousingExpenseType", expense.HousingExpenseType);

                var prepaidFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionF);
                WriteFieldValue(xmlWriter, "HudLine",  prepaidFee == null ? null : prepaidFee.HudLine_rep);

                WriteFieldValue(xmlWriter, "IsEscrowedAtClosing", expense.IsEscrowedAtClosing);
                WriteFieldValue(xmlWriter, "MonthlyAmtFixedAmt", expense.MonthlyAmtFixedAmt_rep);
                WriteFieldValue(xmlWriter, "MonthlyAmtTotal", expense.MonthlyAmtTotal_rep);
                WriteFieldValue(xmlWriter, "PaidTo", expense.PaidTo);
                WriteFieldValue(xmlWriter, "PolicyActivationD", converter.ToDateTimeVarCharString(expense.PolicyActivationD_rep));
                WriteFieldValue(xmlWriter, "PrepaidAmt", expense.PrepaidAmt_rep);
                WriteFieldValue(xmlWriter, "PrepaidMonths", expense.PrepaidMonths_rep);
                WriteFieldValue(xmlWriter, "ReserveAmt", expense.ReserveAmt_rep);
                WriteFieldValue(xmlWriter, "ReserveMonths", expense.ReserveMonths_rep);
                WriteFieldValue(xmlWriter, "ReserveMonthsLckd", expense.ReserveMonthsLckd);
                WriteFieldValue(xmlWriter, "TaxType", expense.TaxType);

                // For Disbursements, we ignore the keys as those are only used for internal organization.
                WriteHousingExpenseDisbursements(expense.ActualDisbursementsList, "ActualDisbursements", xmlWriter, converter);
                WriteHousingExpenseDisbursements(expense.ProjectedDisbursements, "ProjectedDisbursements", xmlWriter, converter);

                xmlWriter.WriteEndElement(); // </record>
            }
        }

        /// <summary>
        /// Write a collection of housing expense disbursements to LOXML for exporting.
        /// </summary>
        /// <param name="disbursements">A set of disbursements.</param>
        /// <param name="collectionName">The name of the disbursement collection.</param>
        /// <param name="xmlWriter">An XML writer.</param>
        /// <param name="converter">An LOS converter.</param>
        private static void WriteHousingExpenseDisbursements(List<SingleDisbursement> disbursements, string collectionName, XmlWriter xmlWriter, LosConvert converter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", collectionName);
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (var disbursement in disbursements)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                xmlWriter.WriteAttributeString("type", "SingleDisbursement");

                WriteFieldValue(xmlWriter, "BillingPeriodEndD", converter.ToDateTimeVarCharString(disbursement.BillingPeriodEndD_rep));
                WriteFieldValue(xmlWriter, "BillingPeriodStartD", converter.ToDateTimeVarCharString(disbursement.BillingPeriodStartD_rep));
                WriteFieldValue(xmlWriter, "DisbId", disbursement.DisbId.ToString());
                WriteFieldValue(xmlWriter, "DisbursementAmt", disbursement.DisbursementAmt_rep);
                WriteFieldValue(xmlWriter, "DisbursementAmtLckd", disbursement.DisbursementAmtLckd);
                WriteFieldValue(xmlWriter, "DisbursementType", disbursement.DisbursementType);
                WriteFieldValue(xmlWriter, "DueDate", converter.ToDateTimeVarCharString(disbursement.DueDate_rep));
                WriteFieldValue(xmlWriter, "EscAccMonth", disbursement.EscAccMonth.ToString());
                WriteFieldValue(xmlWriter, "ExpenseCalcType", disbursement.ExpenseCalcType);
                WriteFieldValue(xmlWriter, "IsPaidFromEscrowAcc", disbursement.IsPaidFromEscrowAcc);
                WriteFieldValue(xmlWriter, "PaidBy", disbursement.PaidBy);
                WriteFieldValue(xmlWriter, "PaidDate", converter.ToDateTimeVarCharString(disbursement.PaidDate_rep));

                xmlWriter.WriteEndElement(); // </record>
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        private static void WriteCoCArchives(CPageData dataLoan, XmlWriter w)
        {
            if (false == dataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                return;
            }

            w.WriteStartElement("collection");
            w.WriteAttributeString("id", "ChangeOfCircumstanceArchives");

            // 1/16/2014 gf - Critical OPM 149683, parse money and then send to mismo rep.
            var convertLos = dataLoan.m_convertLos;
            Func<string, string> FormatMoney = (str) => convertLos.ToMoneyString(convertLos.ToMoney(str), FormatDirection.ToRep);

            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
            {
                var cocArchives = dataLoan.CoCArchives;
                int recordIndex1 = STARTING_RECORD_INDEX;

                foreach (var cocArchive in cocArchives)
                {
                    w.WriteStartElement("record");
                    w.WriteAttributeString("index", recordIndex1++.ToString());
                    w.WriteAttributeString("type", "ChangeOfCircumstanceArchive");

                    WriteFieldValue(w, "GfePrepareDate", dataLoan.m_convertLos.ToDateTimeVarCharString(cocArchive.GfePrepareDate));
                    WriteFieldValue(w, "DateOfChange", dataLoan.m_convertLos.ToDateTimeVarCharString(cocArchive.sCircumstanceChangeD));
                    WriteFieldValue(w, "GfeRedisclosureDate", dataLoan.m_convertLos.ToDateTimeVarCharString(cocArchive.sGfeRedisclosureD));

                    WriteFieldValue(w, "ChangeExplanation", cocArchive.sCircumstanceChangeExplanation);

                    WriteFieldValue(w, "sCircumstanceChange1Reason", cocArchive.sCircumstanceChange1Reason);
                    WriteFieldValue(w, "sCircumstanceChange2Reason", cocArchive.sCircumstanceChange2Reason);
                    WriteFieldValue(w, "sCircumstanceChange3Reason", cocArchive.sCircumstanceChange3Reason);
                    WriteFieldValue(w, "sCircumstanceChange4Reason", cocArchive.sCircumstanceChange4Reason);
                    WriteFieldValue(w, "sCircumstanceChange5Reason", cocArchive.sCircumstanceChange5Reason);
                    WriteFieldValue(w, "sCircumstanceChange6Reason", cocArchive.sCircumstanceChange6Reason);
                    WriteFieldValue(w, "sCircumstanceChange7Reason", cocArchive.sCircumstanceChange7Reason);
                    WriteFieldValue(w, "sCircumstanceChange8Reason", cocArchive.sCircumstanceChange8Reason);

                    WriteFieldValue(w, "sCircumstanceChange1FeeId", cocArchive.sCircumstanceChange2FeeId);
                    WriteFieldValue(w, "sCircumstanceChange2FeeId", cocArchive.sCircumstanceChange2FeeId);
                    WriteFieldValue(w, "sCircumstanceChange3FeeId", cocArchive.sCircumstanceChange3FeeId);
                    WriteFieldValue(w, "sCircumstanceChange4FeeId", cocArchive.sCircumstanceChange4FeeId);
                    WriteFieldValue(w, "sCircumstanceChange5FeeId", cocArchive.sCircumstanceChange5FeeId);
                    WriteFieldValue(w, "sCircumstanceChange6FeeId", cocArchive.sCircumstanceChange6FeeId);
                    WriteFieldValue(w, "sCircumstanceChange7FeeId", cocArchive.sCircumstanceChange7FeeId);
                    WriteFieldValue(w, "sCircumstanceChange8FeeId", cocArchive.sCircumstanceChange8FeeId);

                    WriteFieldValue(w, "sCircumstanceChange1Amount", FormatMoney(cocArchive.sCircumstanceChange1Amount));
                    WriteFieldValue(w, "sCircumstanceChange2Amount", FormatMoney(cocArchive.sCircumstanceChange2Amount));
                    WriteFieldValue(w, "sCircumstanceChange3Amount", FormatMoney(cocArchive.sCircumstanceChange3Amount));
                    WriteFieldValue(w, "sCircumstanceChange4Amount", FormatMoney(cocArchive.sCircumstanceChange4Amount));
                    WriteFieldValue(w, "sCircumstanceChange5Amount", FormatMoney(cocArchive.sCircumstanceChange5Amount));
                    WriteFieldValue(w, "sCircumstanceChange6Amount", FormatMoney(cocArchive.sCircumstanceChange6Amount));
                    WriteFieldValue(w, "sCircumstanceChange7Amount", FormatMoney(cocArchive.sCircumstanceChange7Amount));
                    WriteFieldValue(w, "sCircumstanceChange8Amount", FormatMoney(cocArchive.sCircumstanceChange8Amount));

                    w.WriteEndElement();
                }
            }
            else
            {
                var cocArchives = dataLoan.sClosingCostCoCArchive;
                int recordIndex = STARTING_RECORD_INDEX;

                foreach (var cocArchive in cocArchives)
                {
                    w.WriteStartElement("record");
                    w.WriteAttributeString("index", recordIndex++.ToString());
                    w.WriteAttributeString("type", "ChangeOfCircumstanceArchive");

                    // Remove the time portion of GfePrepareDate so the data matches the Legacy version.
                    var prepareDate = DateTime.Parse(cocArchive.DateArchived);
                    WriteFieldValue(w, "GfePrepareDate", convertLos.ToDateTimeVarCharString(prepareDate.ToShortDateString()));

                    WriteFieldValue(w, "DateOfChange", convertLos.ToDateTimeVarCharString(cocArchive.CircumstanceChangeD));
                    WriteFieldValue(w, "GfeRedisclosureDate", convertLos.ToDateTimeVarCharString(cocArchive.RedisclosureD));
                    WriteFieldValue(w, "ChangeExplanation", cocArchive.CircumstanceChangeExplanation);

                    int feeIndex = STARTING_RECORD_INDEX;
                    foreach (var fee in cocArchive.Fees)
                    {
                        WriteFieldValue(w, "sCircumstanceChange" + feeIndex.ToString() + "Reason", fee.Description);
                        WriteFieldValue(w, "sCircumstanceChange" + feeIndex.ToString() + "FeeId", fee.FeeId.ToString());
                        WriteFieldValue(w, "sCircumstanceChange" + feeIndex.ToString() + "Amount", FormatMoney(fee.Value.ToString()));
                        feeIndex++;
                    }

                    w.WriteEndElement();
                }
            }

            w.WriteEndElement();
        }

        private static void WriteSettlementServiceProviders(CPageData dataLoan, XmlWriter w)
        {
            w.WriteStartElement("collection");
            w.WriteAttributeString("id", "SettlementServiceProviders");

            int recordIndex1 = STARTING_RECORD_INDEX;
            var settlementServiceProviders = dataLoan.sAllSettlementServiceProviders.ToList();

            if (!dataLoan.sIsLegacyClosingCostVersion)
            {
                settlementServiceProviders.AddRange(
                    dataLoan.sAvailableSettlementServiceProviders.GetAllProviders());
            }

            // 10/3/2015 BS - Case 227808. Remove settlement service provider duplicate
            List<SettlementServiceProvider> noDuplicateSettlementServiceProviders = null;
            if (settlementServiceProviders.Count != 0)
            {
                noDuplicateSettlementServiceProviders = settlementServiceProviders.Distinct(new SettlementServiceProviderComparer()).ToList();
            }
            else
            {
                noDuplicateSettlementServiceProviders = settlementServiceProviders;
            }

            foreach (var ssp in noDuplicateSettlementServiceProviders)
            {
                w.WriteStartElement("record");
                w.WriteAttributeString("index", recordIndex1++.ToString());
                w.WriteAttributeString("type", "SettlementServiceProvider");
                WriteFieldValue(w, "ServiceT", ssp.ServiceT);
                WriteFieldValue(w, "ServiceType", ssp.ServiceTDesc);
                WriteFieldValue(w, "CompanyName", ssp.CompanyName);
                WriteFieldValue(w, "StreetAddress", ssp.StreetAddress);
                WriteFieldValue(w, "City", ssp.City);
                WriteFieldValue(w, "State", ssp.State);
                WriteFieldValue(w, "Zip", ssp.Zip);
                WriteFieldValue(w, "ContactName", ssp.ContactName);
                WriteFieldValue(w, "Phone", ssp.Phone);
                WriteFieldValue(w, "Id", ssp.Id.ToString());

                w.WriteEndElement();
            }

            w.WriteEndElement();
        }

        private static void WriteHomeownerCounselingOrganizationCollection(CPageData dataLoan, XmlWriter w)
        {

            w.WriteStartElement("collection");
            w.WriteAttributeString("id", "sHomeownerCounselingOrganizationCollection");

            HomeownerCounselingOrganizationCollection orgs = dataLoan.sHomeownerCounselingOrganizationCollection;
            while (orgs.Count < 10)
                orgs.Add(new HomeownerCounselingOrganization());
            while (orgs.Count > 10)
                orgs.RemoveAt(10);
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (var organization in orgs)
            {
                w.WriteStartElement("record");
                w.WriteAttributeString("index", recordIndex1++.ToString());
                w.WriteAttributeString("type", "HomeownerCounselingOrganization");
                WriteFieldValue(w, "Id", organization.Id.ToString());
                WriteFieldValue(w, "Name", organization.Name);
                WriteFieldValue(w, "Phone", organization.Phone);
                WriteFieldValue(w, "Email", organization.Email);
                WriteFieldValue(w, "Website", organization.Website);
                WriteFieldValue(w, "Services", organization.Services);
                WriteFieldValue(w, "Languages", organization.Languages);

                WriteFieldValue(w, "StreetAddress", organization.Address.StreetAddress);
                WriteFieldValue(w, "StreetAddress2", organization.Address.StreetAddress2);
                WriteFieldValue(w, "City", organization.Address.City);
                WriteFieldValue(w, "State", organization.Address.State);
                WriteFieldValue(w, "Zipcode", organization.Address.Zipcode);

                WriteFieldValue(w, "Distance", organization.Distance);
                w.WriteEndElement();
            }
            w.WriteEndElement();
        }

        /// <summary>
        /// Writes AdjustmentList object to LOXml format.
        /// </summary>
        /// <param name="adjustmentList">The AdjustmentList object, which contains a collection of Closing Disclosure adjustment items.</param>
        /// <param name="xmlWriter">Xmlwriter object to which the collection is written.</param>
        private static void WriteAdjustmentList(AdjustmentList adjustmentList, XmlWriter xmlWriter, bool excludeZeroAdjustmentForDocGenInLoXml = false)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sAdjustmentList");

            //// IR: 9/30/2015 - OPM 227461: System-Generated $0 adjustment is tantamount to no adjustment for purposes of LOXml.
            //// SellerCredit deserializes to $0 adjustment by default and should not be included in LOXml in this case.
            var adjustments = adjustmentList.GetEnumerable(includeZeroAmountSellerCredit: false, includeAssetsAsAdjustments: true);

            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (Adjustment adjustment in adjustments)
            {
                if (excludeZeroAdjustmentForDocGenInLoXml)
                {
                    if (adjustment.Amount == 0M && adjustment.BorrowerAmount == 0M && adjustment.SellerAmount == 0M)
                    {
                        continue;
                    }
                }

                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());

                if (adjustment.Id.HasValue)
                {
                    WriteFieldValue(xmlWriter, "Id", adjustment.Id.Value.ToString());
                }
                WriteFieldValue(xmlWriter, "AdjustmentType", adjustment.AdjustmentType);
                WriteFieldValue(xmlWriter, "Amount", adjustment.Amount_rep);
                //// WriteFieldValue(xmlWriter, "AmountToBorrower", adjustment.AmountToBorrower); - IR: 9/30/2015 This is not used outside of Adjustment class
                WriteFieldValue(xmlWriter, "BorrowerAmount", adjustment.BorrowerAmount_rep);
                WriteFieldValue(xmlWriter, "Description", adjustment.Description);
                WriteFieldValue(xmlWriter, "IsPaidFromBorrower", adjustment.IsPaidFromBorrower);
                WriteFieldValue(xmlWriter, "IsPaidFromSeller", adjustment.IsPaidFromSeller);
                WriteFieldValue(xmlWriter, "IsPaidToBorrower", adjustment.IsPaidToBorrower);
                WriteFieldValue(xmlWriter, "IsPaidToSeller", adjustment.IsPaidToSeller);
                WriteFieldValue(xmlWriter, "IsPopulateToLineLOn1003", adjustment.IsPopulateToLineLOn1003);
                //// WriteFieldValue(xmlWriter, "IsPopulateToLineLOn1003Disabled", adjustment.IsPopulateToLineLOn1003Disabled); - IR: 9/30/2015 This is not used outside of Adjustment class
                WriteFieldValue(xmlWriter, "PaidFromParty", adjustment.PaidFromParty);
                WriteFieldValue(xmlWriter, "PaidToParty", adjustment.PaidToParty);
                WriteFieldValue(xmlWriter, "POC", adjustment.POC);
                WriteFieldValue(xmlWriter, "PopulateTo", adjustment.PopulateTo);
                WriteFieldValue(xmlWriter, "SellerAmount", adjustment.SellerAmount_rep);

                xmlWriter.WriteEndElement(); //// </record>
            }

            xmlWriter.WriteEndElement(); //// </collection>
        }

        /// <summary>
        /// Writes the product code filters to xml.
        /// </summary>
        /// <param name="collectionName">The principal of the user.</param>
        /// <param name="brokerDb">The broker.</param>
        /// <param name="productCodeSet">The product code set to write.</param>
        /// <param name="xmlWriter">The xml writer.</param>
        private static void WriteProductCodeSet(AbstractUserPrincipal principal, BrokerDB brokerDb, string collectionName, Dictionary<string, bool> productCodeSet, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", collectionName);

            if (!brokerDb.IsAdvancedFilterOptionsForPricingEnabled(principal))
            {
                WriteFieldValue(xmlWriter, "Error", "Advanced filtering options are not enabled.");
            }
            else
            {
                for (int i = 0; i < productCodeSet.Count; i++)
                {
                    xmlWriter.WriteStartElement("record");
                    xmlWriter.WriteAttributeString("index", (i + 1).ToString());

                    var pair = productCodeSet.ElementAt(i);
                    WriteFieldValue(xmlWriter, "ProductCodeName", pair.Key);
                    WriteFieldValue(xmlWriter, "IsVisible", pair.Value);

                    xmlWriter.WriteEndElement(); // record
                }
            }

            xmlWriter.WriteEndElement(); // collection;
        }

        /// <summary>
        /// Writes ProrationList object to LOXml format.
        /// </summary>
        /// <param name="prorationList">The ProrationList object, which contains a collection of Closing Disclosure proration items.</param>
        /// <param name="xmlWriter">Xmlwriter object to which the collection is written.</param>
        private static void WriteProrationList(ProrationList prorationList, XmlWriter xmlWriter, bool includeAllSystemProrations = false)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sProrationList");

            //// IR: 9/30/2015 - OPM 227461: System-Generated $0 proration is tantamount to no proration for purposes of LOXml.
            //// CityTownTaxes, CountyTaxes, Assessments deserialize to $0 prorations by default and should not be included in LOXml in this case.
            var prorations = prorationList.GetEnumerable(includeAllSystemProrations);

            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (Proration proration in prorations)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());

                if (proration.Id.HasValue)
                {
                    WriteFieldValue(xmlWriter, "Id", proration.Id.Value.ToString());
                }
                WriteFieldValue(xmlWriter, "Amount", proration.Amount_rep);
                WriteFieldValue(xmlWriter, "AmountPaidFromBorrowerToSeller", proration.AmountPaidFromBorrowerToSeller_rep);
                WriteFieldValue(xmlWriter, "AmountPaidFromSellerToBorrower", proration.AmountPaidFromSellerToBorrower_rep);
                //// WriteFieldValue(xmlWriter, "AmountToBorrower", proration.AmountToBorrower); - IR: 9/30/2015 This is not used outside of Adjustment class
                WriteFieldValue(xmlWriter, "ClosingDate", proration.ClosingDate_rep);
                WriteFieldValue(xmlWriter, "Description", proration.Description);
                WriteFieldValue(xmlWriter, "IsPaidFromBorrowerToSeller", proration.IsPaidFromBorrowerToSeller);
                WriteFieldValue(xmlWriter, "IsPaidFromSellerToBorrower", proration.IsPaidFromSellerToBorrower);
                WriteFieldValue(xmlWriter, "IsSystemProration", proration.IsSystemProration);
                WriteFieldValue(xmlWriter, "PaidFromDate", proration.PaidFromDate_rep);
                WriteFieldValue(xmlWriter, "PaidFromParty", proration.PaidFromParty);
                WriteFieldValue(xmlWriter, "PaidThroughDate", proration.PaidThroughDate_rep);
                WriteFieldValue(xmlWriter, "PaidToDate", proration.PaidToDate_rep);
                WriteFieldValue(xmlWriter, "PaidToParty", proration.PaidToParty);
                WriteFieldValue(xmlWriter, "ProrationType", proration.ProrationType);

                xmlWriter.WriteEndElement(); //// </record>
            }

            xmlWriter.WriteEndElement(); //// </collection>
        }

        private static void WriteApplicantLiabilities(CAppData dataApp, XmlWriter xmlWriter, FormatTarget format)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "aLiaCollection");

            ILiaCollection aLiaCollection = dataApp.aLiaCollection;

            int count = dataApp.aLiaCollection.CountRegular;
            int recordIndex1 = STARTING_RECORD_INDEX;
            for (int i = 0; i < count; i++)
            {
                ILiabilityRegular field = dataApp.aLiaCollection.GetRegularRecordAt(i);
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "OwnerT", field.OwnerT);
                WriteFieldValue(xmlWriter, "DebtT", field.DebtT);
                WriteFieldValue(xmlWriter, "ComNm", field.ComNm);
                WriteFieldValue(xmlWriter, "ComAddr", field.ComAddr);
                WriteFieldValue(xmlWriter, "ComCity", field.ComCity);
                WriteFieldValue(xmlWriter, "ComState", field.ComState);
                WriteFieldValue(xmlWriter, "ComZip", field.ComZip);
                WriteFieldValue(xmlWriter, "ComPhone", field.ComPhone);
                WriteFieldValue(xmlWriter, "ComFax", field.ComFax);
                WriteFieldValue(xmlWriter, "Desc", field.Desc);
                WriteFieldValue(xmlWriter, "AccNm", field.AccNm);
                WriteFieldValue(xmlWriter, "AccNum", field.AccNum.Value);
                WriteFieldValue(xmlWriter, "Bal", field.Bal_rep);
                WriteFieldValue(xmlWriter, "Pmt", field.Pmt_rep);
                WriteFieldValue(xmlWriter, "RemainMons", field.RemainMons_rep);
                WriteFieldValue(xmlWriter, "Rate", field.R_rep);
                WriteFieldValue(xmlWriter, "OrigTerm", field.OrigTerm_rep);
                WriteFieldValue(xmlWriter, "Due", field.Due_rep);
                WriteFieldValue(xmlWriter, "Late30", field.Late30_rep);
                WriteFieldValue(xmlWriter, "Late60", field.Late60_rep);
                WriteFieldValue(xmlWriter, "Late90Plus", field.Late90Plus_rep);
                WriteFieldValue(xmlWriter, "WillBePdOff", field.WillBePdOff, format);
                WriteFieldValue(xmlWriter, "UsedInRatio", !field.NotUsedInRatio, format);
                WriteFieldValue(xmlWriter, "IsPiggyBack", field.IsPiggyBack, format);
                WriteFieldValue(xmlWriter, "IncInReposession", field.IncInReposession, format);
                WriteFieldValue(xmlWriter, "IncInBankruptcy", field.IncInBankruptcy, format);
                WriteFieldValue(xmlWriter, "IncInForeclosure", field.IncInForeclosure, format);
                WriteFieldValue(xmlWriter, "ExcFromUnderwriting", field.ExcFromUnderwriting, format);
                WriteFieldValue(xmlWriter, "VerifSentD", field.VerifSentD_rep);
                WriteFieldValue(xmlWriter, "VerifReorderedD", field.VerifReorderedD_rep);
                WriteFieldValue(xmlWriter, "VerifRecvD", field.VerifRecvD_rep);
                WriteFieldValue(xmlWriter, "VerifExpD", field.VerifExpD_rep);
                WriteFieldValue(xmlWriter, "MatchedReRecordId", field.MatchedReRecordId.ToString());
                WriteFieldValue(xmlWriter, "IsSubjectProperty1stMortgage", field.IsSubjectProperty1stMortgage, format);
                WriteFieldValue(xmlWriter, "IsSubjectPropertyMortgage", field.IsSubjectPropertyMortgage, format);
                WriteFieldValue(xmlWriter, "PayoffAmt", field.PayoffAmt_rep);
                WriteFieldValue(xmlWriter, "PayoffAmtLckd", field.PayoffAmtLckd, format);
                WriteFieldValue(xmlWriter, "PayoffTiming", field.PayoffTiming);
                WriteFieldValue(xmlWriter, "PayoffTimingLckd", field.PayoffTimingLckd, format);
                WriteFieldValue(xmlWriter, "AutoYearMake", field.AutoYearMake);
                WriteFieldValue(xmlWriter, "IsForAuto", field.IsForAuto, format);
                WriteFieldValue(xmlWriter, "IsMortFHAInsured", field.IsMortFHAInsured, format);
                WriteFieldValue(xmlWriter, "OrigDebtAmt", field.OrigDebtAmt_rep);
                xmlWriter.WriteEndElement(); // </record>
            }
            xmlWriter.WriteEndElement(); // </collection>
        }

        private static void WriteApplicantReo(CAppData dataApp, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "aReCollection");
            var reCollection = dataApp.aReCollection;

            int count = reCollection.CountRegular;
            int recordIndex1 = STARTING_RECORD_INDEX;
            for (int i = 0; i < count; i++)
            {
                var field = reCollection.GetRegularRecordAt(i);
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "Addr", field.Addr);
                WriteFieldValue(xmlWriter, "City", field.City);
                WriteFieldValue(xmlWriter, "GrossRentI", field.GrossRentI_rep);
                WriteFieldValue(xmlWriter, "HExp", field.HExp_rep);
                WriteFieldValue(xmlWriter, "IsSubjectProp", field.IsSubjectProp);
                WriteFieldValue(xmlWriter, "MAmt", field.MAmt_rep);
                WriteFieldValue(xmlWriter, "MPmt", field.MPmt_rep);
                WriteFieldValue(xmlWriter, "State", field.State);
                WriteFieldValue(xmlWriter, "StatT", field.StatT);
                WriteFieldValue(xmlWriter, "Type", field.Type);
                WriteFieldValue(xmlWriter, "Val", field.Val_rep);
                WriteFieldValue(xmlWriter, "Zip", field.Zip);
                WriteFieldValue(xmlWriter, "OccR", field.OccR_rep);
                WriteFieldValue(xmlWriter, "ReOwnerT", field.ReOwnerT);
                WriteFieldValue(xmlWriter, "NetRentI", field.NetRentI_rep);
                WriteFieldValue(xmlWriter, "IsForceCalcNetRentalI", field.IsForceCalcNetRentalI);
                WriteFieldValue(xmlWriter, "IsPrimaryResidence", field.IsPrimaryResidence);
                WriteFieldValue(xmlWriter, "RecordId", field.RecordId.ToString());
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
        }
        private static void WriteApplicantAssets(CAppData dataApp, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "aAssetCollection");

            IAssetCollection aAssetCollection = dataApp.aAssetCollection;
            int count = aAssetCollection.CountRegular;
            int recordIndex1 = STARTING_RECORD_INDEX;
            for (int i = 0; i < count; i++)
            {
                var field = aAssetCollection.GetRegularRecordAt(i);
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "AccNm", field.AccNm);
                WriteFieldValue(xmlWriter, "AccNum", field.AccNum.Value);
                WriteFieldValue(xmlWriter, "AssetT", field.AssetT);
                WriteFieldValue(xmlWriter, "Attention", field.Attention);
                WriteFieldValue(xmlWriter, "City", field.City);
                WriteFieldValue(xmlWriter, "ComNm", field.ComNm);
                WriteFieldValue(xmlWriter, "DepartmentName", field.DepartmentName);
                WriteFieldValue(xmlWriter, "Desc", field.Desc);
                if (field.AssetT == E_AssetRegularT.GiftFunds)
                {
                    WriteFieldValue(xmlWriter, "GiftSource", field.GiftSource);
                }
                WriteFieldValue(xmlWriter, "OwnerT", field.OwnerT);
                WriteFieldValue(xmlWriter, "StAddr", field.StAddr);
                WriteFieldValue(xmlWriter, "State", field.State);
                WriteFieldValue(xmlWriter, "Val", field.Val_rep);
                WriteFieldValue(xmlWriter, "VerifExpD", field.VerifExpD_rep);
                WriteFieldValue(xmlWriter, "VerifRecvD", field.VerifRecvD_rep);
                WriteFieldValue(xmlWriter, "VerifReorderedD", field.VerifReorderedD_rep);
                WriteFieldValue(xmlWriter, "VerifSentD", field.VerifSentD_rep);
                WriteFieldValue(xmlWriter, "Zip", field.Zip);
                xmlWriter.WriteEndElement(); // </record>
            }
            xmlWriter.WriteEndElement(); // </collection>
        }
        private static void WriteApplicantEmployment(CAppData dataApp, XmlWriter xmlWriter, bool isBorrower)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", isBorrower ? "aBEmpCollection" : "aCEmpCollection");

            IEmpCollection empCollection = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
            int count = empCollection.CountRegular;
            int recordIndex1 = STARTING_RECORD_INDEX;
            for (int i = 0; i < count; i++)
            {
                IRegularEmploymentRecord field = empCollection.GetRegularRecordAt(i);
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "IsSelfEmplmt", field.IsSelfEmplmt);
                WriteFieldValue(xmlWriter, "EmplrNm", field.EmplrNm);
                WriteFieldValue(xmlWriter, "EmplmtStartD", field.EmplmtStartD_rep);
                WriteFieldValue(xmlWriter, "EmplmtEndD", field.EmplmtEndD_rep);
                WriteFieldValue(xmlWriter, "EmplrAddr", field.EmplrAddr);
                WriteFieldValue(xmlWriter, "MonI", field.MonI_rep);
                WriteFieldValue(xmlWriter, "EmplrCity", field.EmplrCity);
                WriteFieldValue(xmlWriter, "EmplrState", field.EmplrState);
                WriteFieldValue(xmlWriter, "EmplrZip", field.EmplrZip);
                WriteFieldValue(xmlWriter, "JobTitle", field.JobTitle);
                WriteFieldValue(xmlWriter, "EmplrBusPhone", field.EmplrBusPhone);
                WriteFieldValue(xmlWriter, "EmplrFax", field.EmplrFax);
                WriteFieldValue(xmlWriter, "VerifSentD", field.VerifSentD_rep);
                WriteFieldValue(xmlWriter, "VerifReorderedD", field.VerifReorderedD_rep);
                WriteFieldValue(xmlWriter, "VerifRecvD", field.VerifRecvD_rep);
                WriteFieldValue(xmlWriter, "VerifExpD", field.VerifExpD_rep);
                WriteFieldValue(xmlWriter, "IsCurrent", field.IsCurrent);
                xmlWriter.WriteEndElement(); // </record>
            }
            xmlWriter.WriteEndElement(); // </collection>
        }

        private static void WriteOtherIncome(CAppData dataApp, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "aOtherIncomeCollection");
            int recordIndex1 = STARTING_RECORD_INDEX;
            foreach (var income in dataApp.aOtherIncomeList)
            {
                xmlWriter.WriteStartElement("record");
                xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                WriteFieldValue(xmlWriter, "IsForCoborrower", income.IsForCoBorrower ? "True" : "False");
                WriteFieldValue(xmlWriter, "IncomeDesc", income.Desc);
                WriteFieldValue(xmlWriter, "MonthlyAmt", dataApp.m_convertLos.ToMoneyString(income.Amount,FormatDirection.ToRep));

                xmlWriter.WriteEndElement(); // </record>
            }
            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Writes mortgage late information from the credit report to the output as a collection.
        /// </summary>
        /// <param name="dataApp">The application to pull the credit report from.</param>
        /// <param name="xmlWriter">The xml writer for the output.</param>
        private static void WriteMortgageLates(CAppData dataApp, XmlWriter xmlWriter)
        {
            var creditReport = dataApp.CreditReportData.Value;
            if (creditReport == null)
            {
                xmlWriter.WriteStartElement("collection");
                xmlWriter.WriteAttributeString("id", "aMortgageLates");
                WriteFieldValue(xmlWriter, "Error", "No credit report on file.");
                xmlWriter.WriteEndElement();
                return;
            }

            var nMonths = new int[] { 12, 24 };
            var lateMonths = new E_AdverseType[] {
                E_AdverseType.Late30,
                E_AdverseType.Late60,
                E_AdverseType.Late90,
                E_AdverseType.AllLate
            };
            var rollingOptions = new bool[] { false, true };
            var collection = new System.Xml.Linq.XElement("collection",
                new System.Xml.Linq.XAttribute("id", "aMortgageLates"));

            foreach (int months in nMonths)
            {
                foreach (E_AdverseType lateMonth in lateMonths)
                {
                    foreach (bool rolling in rollingOptions)
                    {
                        // Doesn't make sense to roll over all lates, need a specific term
                        if (lateMonth == E_AdverseType.AllLate && rolling)
                        {
                            continue;
                        }

                        collection.Add(new System.Xml.Linq.XElement("record",
                            new System.Xml.Linq.XElement("field",
                                new System.Xml.Linq.XAttribute("id", "PastMonths"),
                                months),
                            new System.Xml.Linq.XElement("field",
                                new System.Xml.Linq.XAttribute("id", "LateDays"),
                                lateMonth.ToString("G").Replace("Late", string.Empty)),
                            new System.Xml.Linq.XElement("field",
                                new System.Xml.Linq.XAttribute("id", "RollingLates"),
                                rolling),
                            new System.Xml.Linq.XElement("field",
                                new System.Xml.Linq.XAttribute("id", "MortgageLateCount"),
                                creditReport.GetNumberOfLates(months, E_DebtRegularT.Mortgage, lateMonth, rolling))
                            ));
                    }
                }
            }
            collection.WriteTo(xmlWriter);
        }

        private static void WriteAgentDataSet(CPageData dataLoan, XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("collection");
            xmlWriter.WriteAttributeString("id", "sAgentDataSet");
            int recordIndex1 = STARTING_RECORD_INDEX;

            int agentRecordCount = dataLoan.GetAgentRecordCount();
            for (int i = 0; i < agentRecordCount; i++)
            {
                CAgentFields agent = dataLoan.GetAgentFields(i);
                if (agent.IsValid)
                {
                    xmlWriter.WriteStartElement("record");
                    xmlWriter.WriteAttributeString("index", recordIndex1++.ToString());
                    WriteFieldValue(xmlWriter, "AgentRoleT", agent.AgentRoleT);
                    WriteFieldValue(xmlWriter, "AgentRoleTDesc", agent.AgentRoleTDesc);
                    WriteFieldValue(xmlWriter, "OtherAgentRoleTDesc", agent.OtherAgentRoleTDesc);
                    WriteFieldValue(xmlWriter, "AgentName", agent.AgentName);
                    WriteFieldValue(xmlWriter, "EmployeeIDInCompany", agent.EmployeeIDInCompany);
                    WriteFieldValue(xmlWriter, "CompanyId", agent.CompanyId);
                    WriteFieldValue(xmlWriter, "TaxId", agent.TaxId);
                    WriteFieldValue(xmlWriter, "ChumsId", agent.ChumsId);
                    WriteFieldValue(xmlWriter, "LicenseNumOfAgent", agent.LicenseNumOfAgent);
                    WriteFieldValue(xmlWriter, "LicenseNumOfCompany", agent.LicenseNumOfCompany);
                    WriteFieldValue(xmlWriter, "DepartmentName", agent.DepartmentName);
                    WriteFieldValue(xmlWriter, "CaseNum", agent.CaseNum);
                    WriteFieldValue(xmlWriter, "CompanyName", agent.CompanyName);
                    WriteFieldValue(xmlWriter, "StreetAddr", agent.StreetAddr);
                    WriteFieldValue(xmlWriter, "City", agent.City);
                    WriteFieldValue(xmlWriter, "State", agent.State);
                    WriteFieldValue(xmlWriter, "Zip", agent.Zip);
                    WriteFieldValue(xmlWriter, "County", agent.County);
                    WriteFieldValue(xmlWriter, "Phone", agent.Phone);
                    WriteFieldValue(xmlWriter, "CellPhone", agent.CellPhone);
                    WriteFieldValue(xmlWriter, "PagerNum", agent.PagerNum);
                    WriteFieldValue(xmlWriter, "FaxNum", agent.FaxNum);
                    WriteFieldValue(xmlWriter, "EmailAddr", agent.EmailAddr);
                    WriteFieldValue(xmlWriter, "Notes", agent.Notes);
                    WriteFieldValue(xmlWriter, "IsUseOriginatorCompensationAmount", agent.IsUseOriginatorCompensationAmount);
                    WriteFieldValue(xmlWriter, "CommissionMinBase", agent.CommissionMinBase_rep);
                    WriteFieldValue(xmlWriter, "CommissionPointOfLoanAmount", agent.CommissionPointOfLoanAmount_rep);
                    WriteFieldValue(xmlWriter, "CommissionPointOfGrossProfit", agent.CommissionPointOfGrossProfit_rep);
                    WriteFieldValue(xmlWriter, "Commission", agent.Commission_rep);
                    WriteFieldValue(xmlWriter, "IsCommissionPaid", agent.IsCommissionPaid);
                    WriteFieldValue(xmlWriter, "CommissionPaid", agent.CommissionPaid_rep);
                    WriteFieldValue(xmlWriter, "PaymentNotes", agent.PaymentNotes);
                    WriteFieldValue(xmlWriter, "InvestorSoldDate", agent.InvestorSoldDate_rep);
                    WriteFieldValue(xmlWriter, "InvestorBasisPoints", agent.InvestorBasisPoints_rep);
                    WriteFieldValue(xmlWriter, "IsListedInGFEProviderForm", agent.IsListedInGFEProviderForm);
                    WriteFieldValue(xmlWriter, "IsLenderAssociation", agent.IsLenderAssociation);
                    WriteFieldValue(xmlWriter, "IsLenderAffiliate", agent.IsLenderAffiliate);
                    WriteFieldValue(xmlWriter, "IsLenderRelative", agent.IsLenderRelative);
                    WriteFieldValue(xmlWriter, "HasLenderRelationship", agent.HasLenderRelationship);
                    WriteFieldValue(xmlWriter, "HasLenderAccountLast12Months", agent.HasLenderAccountLast12Months);
                    WriteFieldValue(xmlWriter, "IsUsedRepeatlyByLenderLast12Months", agent.IsUsedRepeatlyByLenderLast12Months);
                    WriteFieldValue(xmlWriter, "ProviderItemNumber", agent.ProviderItemNumber);
                    WriteFieldValue(xmlWriter, "PhoneOfCompany", agent.PhoneOfCompany);
                    WriteFieldValue(xmlWriter, "FaxOfCompany", agent.FaxOfCompany);
                    WriteFieldValue(xmlWriter, "IsNotifyWhenLoanStatusChange", agent.IsNotifyWhenLoanStatusChange);
                    WriteFieldValue(xmlWriter, "LoanOriginatorIdentifier", agent.LoanOriginatorIdentifier);
                    WriteFieldValue(xmlWriter, "CompanyLoanOriginatorIdentifier", agent.CompanyLoanOriginatorIdentifier);
                    WriteFieldValue(xmlWriter, "BranchName", agent.BranchName);
                    WriteFieldValue(xmlWriter, "PayToBankName", agent.PayToBankName);
                    WriteFieldValue(xmlWriter, "PayToBankCityState", agent.PayToBankCityState);
                    WriteFieldValue(xmlWriter, "PayToABANumber", agent.PayToABANumber.Value);
                    WriteFieldValue(xmlWriter, "PayToAccountNumber", agent.PayToAccountNumber.Value);
                    WriteFieldValue(xmlWriter, "PayToAccountName", agent.PayToAccountName);
                    WriteFieldValue(xmlWriter, "FurtherCreditToAccountNumber", agent.FurtherCreditToAccountNumber.Value);
                    WriteFieldValue(xmlWriter, "FurtherCreditToAccountName", agent.FurtherCreditToAccountName);
                    WriteFieldValue(xmlWriter, "IsLender", agent.IsLender);
                    WriteFieldValue(xmlWriter, "IsOriginator", agent.IsOriginator);
                    WriteFieldValue(xmlWriter, "IsOriginatorAffiliate", agent.IsOriginatorAffiliate);
                    WriteFieldValue(xmlWriter, "LicenseNumOfAgentIsExpired", agent.LicenseNumOfAgentIsExpired);
                    WriteFieldValue(xmlWriter, "LicenseNumOfCompanyIsExpired", agent.LicenseNumOfCompanyIsExpired);
                    WriteFieldValue(xmlWriter, "RecordId", agent.RecordId.ToString());
                    xmlWriter.WriteEndElement(); // </record>
                }
            }

            xmlWriter.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Writes alternate lender info to the payload.
        /// </summary>
        /// <param name="brokerId">The broker ID.</param>
        /// <param name="altLenderId">The alternate lender ID.</param>
        /// <param name="writer">An XML writer.</param>
        private static void WriteAlternateLender(Guid brokerId, Guid altLenderId, XmlWriter writer)
        {
            writer.WriteStartElement("collection");
            writer.WriteAttributeString("id", "sAlternateLender");

            if (altLenderId != Guid.Empty)
            {
                var altLender = new DocMagicAlternateLender(brokerId, altLenderId);

                writer.WriteStartElement("record");
                writer.WriteAttributeString("type", "Lender");
                WriteFieldValue(writer, "Name", altLender.LenderName);
                WriteFieldValue(writer, "Address", altLender.LenderAddress);
                WriteFieldValue(writer, "City", altLender.LenderCity);
                WriteFieldValue(writer, "State", altLender.LenderState);
                WriteFieldValue(writer, "Zip", altLender.LenderZip);
                WriteFieldValue(writer, "NonPersonEntity", altLender.LenderNonPersonEntityIndicator);
                writer.WriteEndElement(); // <record>

                writer.WriteStartElement("record");
                writer.WriteAttributeString("type", "Beneficiary");
                WriteFieldValue(writer, "Name", altLender.BeneficiaryName);
                WriteFieldValue(writer, "Address", altLender.BeneficiaryAddress);
                WriteFieldValue(writer, "City", altLender.BeneficiaryCity);
                WriteFieldValue(writer, "State", altLender.BeneficiaryState);
                WriteFieldValue(writer, "Zip", altLender.BeneficiaryZip);
                WriteFieldValue(writer, "NonPersonEntity", altLender.BeneficiaryNonPersonEntityIndicator);
                writer.WriteEndElement(); // <record>

                writer.WriteStartElement("record");
                writer.WriteAttributeString("type", "LossPayee");
                WriteFieldValue(writer, "Name", altLender.LossPayeeName);
                WriteFieldValue(writer, "Address", altLender.LossPayeeAddress);
                WriteFieldValue(writer, "City", altLender.LossPayeeCity);
                WriteFieldValue(writer, "State", altLender.LossPayeeState);
                WriteFieldValue(writer, "Zip", altLender.LossPayeeZip);
                writer.WriteEndElement(); // <record>

                writer.WriteStartElement("record");
                writer.WriteAttributeString("type", "MakePaymentsTo");
                WriteFieldValue(writer, "Name", altLender.MakePaymentsToName);
                WriteFieldValue(writer, "Address", altLender.MakePaymentsToAddress);
                WriteFieldValue(writer, "City", altLender.MakePaymentsToCity);
                WriteFieldValue(writer, "State", altLender.MakePaymentsToState);
                WriteFieldValue(writer, "Zip", altLender.MakePaymentsToZip);
                writer.WriteEndElement(); // <record>

                writer.WriteStartElement("record");
                writer.WriteAttributeString("type", "WhenRecordedMailTo");
                WriteFieldValue(writer, "Name", altLender.WhenRecodedMailToName);
                WriteFieldValue(writer, "Address", altLender.WhenRecodedMailToAddress);
                WriteFieldValue(writer, "City", altLender.WhenRecodedMailToCity);
                WriteFieldValue(writer, "State", altLender.WhenRecodedMailToState);
                WriteFieldValue(writer, "Zip", altLender.WhenRecodedMailToZip);
                writer.WriteEndElement(); // <record>
            }

            writer.WriteEndElement(); // </collection>
        }

        private static void WriteDuThirdPartyProviders(List<FannieMaeThirdPartyProvider> list, XmlWriter writer)
        {
            writer.WriteStartElement("collection");
            writer.WriteAttributeString("id", "sDuThirdPartyProviders");
            foreach (FannieMaeThirdPartyProvider provider in list)
            {
                writer.WriteStartElement("record");
                WriteFieldValue(writer, "FNMAServiceProviderName", provider.Name);
                WriteFieldValue(writer, "ReferenceNumber", provider.ReferenceNumber);
				writer.WriteEndElement(); // </record>
            }

            writer.WriteEndElement(); // </collection>
        }

        /// <summary>
        /// Writes a set of 4506-T orders associated with a loan.
        /// </summary>
        /// <param name="dataLoan">A loan file.</param>
        /// <param name="writer">An XML writer.</param>
        private static void Write4506tOrders(CPageData dataLoan, XmlWriter writer)
        {
            writer.WriteStartElement("collection");
            writer.WriteAttributeString("id", "4506tOrderList");

            var orders = Irs4506TOrderInfo.RetrieveByLoanId(dataLoan.sLId);
            var vendors = Irs4506TVendorConfiguration.ListActiveVendor();
            var convertor = dataLoan.m_convertLos;

            foreach (var order in orders)
            {
                var vendor = vendors.First(v => v.VendorId == order.VendorId);

                writer.WriteStartElement("record");
                writer.WriteAttributeString("type", "4506tOrder");
                WriteFieldValue(writer, "VendorName", vendor.VendorName);
                WriteFieldValue(writer, "ApplicationName", order.ApplicationName);
                WriteFieldValue(writer, "ApplicationId", order.aAppId.ToString());
                WriteFieldValue(writer, "OrderDate", convertor.ToDateTimeString(order.OrderDate, displayTime: true));
                WriteFieldValue(writer, "OrderNumber", order.OrderNumber);
                WriteFieldValue(writer, "Status", order.StatusT.ToString());
                WriteFieldValue(writer, "StatusDate", convertor.ToDateTimeString(order.StatusDate, displayTime: true));
                writer.WriteEndElement(); // </record>
            }

            writer.WriteEndElement(); // </collection>
        }
    }
}
