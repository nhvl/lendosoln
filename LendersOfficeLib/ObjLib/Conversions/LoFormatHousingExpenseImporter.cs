﻿namespace LendersOffice.Conversions
{
    using System;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// Handles the importing of the housing expense collection from LOXML format into the <see cref="CPageData"/> loan object's relevant housing expenses.
    /// </summary>
    public class LoFormatHousingExpenseImporter
    {
        /// <summary>
        /// The loan into which the housing expense info will be imported.
        /// </summary>
        private CPageData dataLoan;

        /// <summary>
        /// The XML containing housing expense info to be imported.
        /// </summary>
        private XmlElement housingExpenseCollectionXml;

        /// <summary>
        /// A log object that handles the formatting of errors and warning messages.
        /// </summary>
        private LoFormatHousingExpenseMessages messages;

        /// <summary>
        /// The user requesting the import.
        /// </summary>
        private AbstractUserPrincipal principal;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoFormatHousingExpenseImporter"/> class.
        /// </summary>
        /// <param name="loan">The loan for which the housing expenses are being imported.</param>
        /// <param name="requestingUser">The user requesting the import.</param>
        /// <param name="housingExpenseCollectionXml">The collection xml element containing housing expense information to import.</param>
        /// <param name="warningMessages">The StringBuilder that accumulates warning messages in parsing the input.</param>
        public LoFormatHousingExpenseImporter(CPageData loan, AbstractUserPrincipal requestingUser, XmlElement housingExpenseCollectionXml, StringBuilder warningMessages)
        {
            this.dataLoan = loan;
            this.principal = requestingUser;
            this.housingExpenseCollectionXml = housingExpenseCollectionXml;
            this.messages = new LoFormatHousingExpenseMessages(warningMessages, this.dataLoan.sLNm);
        }

        /// <summary>
        /// Defines a method signature that matches the basic "TryParse" pattern.
        /// </summary>
        /// <typeparam name="T">The type to parse. Usually a struct.</typeparam>
        /// <param name="value">The string value to parse into an instance of type <typeparamref name="T"/>.</param>
        /// <param name="parsedValue">The output value parsed from the string.</param>
        /// <returns>Whether or not the parse was successful.</returns>
        private delegate bool TryParseDelegate<T>(string value, out T parsedValue);

        /// <summary>
        /// Entry point for this class. Sets the housing expenses of this object's referenced loan based on the input XML.
        /// </summary>
        public void SetHousingExpenses()
        {
            if (!this.dataLoan.sIsHousingExpenseMigrated)
            {
                this.messages.AddHousingExpenseMessage(message: $"sHousingExpenses collection was provided, but the loan does not have migrated housing expenses. Skipping housing expenses.", warningLevel: "ERROR");
                return;
            }

            XmlNodeList expenseRecords = this.housingExpenseCollectionXml.SelectNodes("record");

            foreach (XmlElement expenseRecord in expenseRecords)
            {
                this.CreateHousingExpense(expenseRecord);
            }
        }

        /// <summary>
        /// Creates a housing expense from the given XML and adds it to the correct expense in the internal <see cref="CPageData"/>.
        /// </summary>
        /// <param name="housingExpenseRecordElement">The housing expense "record" XML element.</param>
        private void CreateHousingExpense(XmlElement housingExpenseRecordElement)
        {
            if (housingExpenseRecordElement == null)
            {
                return;
            }

            int index;
            if (int.TryParse(housingExpenseRecordElement.GetAttribute("index"), out index))
            {
                this.messages.ExpenseRecordIndex = index;
            }
            
            // Parse the expense type and line number so that we can retrieve the appropriate expense from the loan file.
            E_HousingExpenseTypeT? housingExpenseType = TryParseEnumField<E_HousingExpenseTypeT>(housingExpenseRecordElement, "HousingExpenseType");
            if (housingExpenseType == null)
            {
                this.messages.AddHousingExpenseMessage("Field HousingExpenseType is required for a record in the sHousingExpenses collection. Skipping record.");
                return;
            }

            this.messages.ExpenseType = housingExpenseType.ToString();
            E_CustomExpenseLineNumberT lineNumber = TryParseEnumField<E_CustomExpenseLineNumberT>(housingExpenseRecordElement, "CustomExpenseLineNum") ?? E_CustomExpenseLineNumberT.None;

            if (housingExpenseType == E_HousingExpenseTypeT.Unassigned && lineNumber == E_CustomExpenseLineNumberT.None)
            {
                this.messages.AddHousingExpenseMessage("HousingExpenseType= Unassigned and LineNumber= None. An unassigned housing expense must have a line number specified. Skipping record.");
                return;
            }

            BaseHousingExpense expense = this.dataLoan.GetExpense(housingExpenseType.Value, lineNumber);
            expense.SetCustomLineNumber(lineNumber);
            this.messages.ExpenseType = expense.CalculatedHousingExpenseType.ToString();

            // The Annual Amount Calculation Type determines which fields to use for calculations. Fields specific to the other calc type will be ignored.
            E_AnnualAmtCalcTypeT? annualAmtCalcType = TryParseEnumField<E_AnnualAmtCalcTypeT>(housingExpenseRecordElement, "AnnualAmtCalcType");
            if (annualAmtCalcType != null)
            {
                expense.AnnualAmtCalcType = annualAmtCalcType.Value;
            }

            // "Calculated" mode for annual amount
            if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.LoanValues)
            {
                decimal? annualAmtCalcBasePerc = TryParseField<decimal>(housingExpenseRecordElement, "AnnualAmtCalcBasePerc", decimal.TryParse);
                if (annualAmtCalcBasePerc != null)
                {
                    expense.AnnualAmtCalcBasePerc = annualAmtCalcBasePerc.Value;
                }

                E_PercentBaseT? annualAmtCalcBaseType = TryParseEnumField<E_PercentBaseT>(housingExpenseRecordElement, "AnnualAmtCalcBaseType");
                if (annualAmtCalcBaseType != null)
                {
                    var allowedValues = new E_PercentBaseT[] { E_PercentBaseT.LoanAmount, E_PercentBaseT.SalesPrice, E_PercentBaseT.AppraisalValue, E_PercentBaseT.TotalLoanAmount };
                    if (allowedValues.Contains(annualAmtCalcBaseType.Value))
                    {
                        expense.AnnualAmtCalcBaseType = annualAmtCalcBaseType.Value;
                    }
                    else
                    {
                        this.messages.AddHousingExpenseMessage($"Value= {housingExpenseRecordElement.SelectSingleNode($".//field[@id=\"AnnualAmtCalcBaseType\"]").InnerText} is invalid for field AnnualAmtCalcBaseType.");
                    }
                }

                E_DisbursementRepIntervalT? disbursementRepInterval = TryParseEnumField<E_DisbursementRepIntervalT>(housingExpenseRecordElement, "DisbursementRepInterval");
                if (disbursementRepInterval != null)
                {
                    expense.DisbursementRepInterval = disbursementRepInterval.Value;
                }

                decimal? monthlyAmtFixedAmt = TryParseField<decimal>(housingExpenseRecordElement, "MonthlyAmtFixedAmt", decimal.TryParse);
                if (monthlyAmtFixedAmt != null)
                {
                    expense.MonthlyAmtFixedAmt = monthlyAmtFixedAmt.Value;
                }

                int? prepaidMonths = TryParseField<int>(housingExpenseRecordElement, "PrepaidMonths", int.TryParse);
                if (prepaidMonths != null)
                {
                    expense.PrepaidMonths = prepaidMonths.Value;
                }

                if (expense.DisbursementRepInterval == E_DisbursementRepIntervalT.Annual)
                {
                    XmlElement disbursementScheduleElement = housingExpenseRecordElement.SelectSingleNode("collection[@id=\"DisbursementScheduleMonths\"]") as XmlElement;

                    if (disbursementScheduleElement != null)
                    {
                        string[] months = new string[]
                        {
                            "January",
                            "February",
                            "March",
                            "April",
                            "May",
                            "June",
                            "July",
                            "August",
                            "September",
                            "October",
                            "November",
                            "December"
                        };

                        for (int i = 0; i < months.Length; i++)
                        {
                            int? monthCount = TryParseField<int>(disbursementScheduleElement, months[i], int.TryParse);
                            if (monthCount != null)
                            {
                                // DisbursementScheduleMonths[0] is CushionMonths
                                expense.DisbursementScheduleMonths[i + 1] = monthCount.Value;
                            }
                        }
                    }
                }
            }
            else if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                this.ExecuteDisbursementCollectionActionsFromXml(expense, (XmlElement)housingExpenseRecordElement.SelectSingleNode("collection[@id=\"ActualDisbursements\"]"));
            }

            XmlNode expenseDescriptionNode = housingExpenseRecordElement.SelectSingleNode("field[@id=\"Description\"]");
            if (expenseDescriptionNode != null)
            {
                expense.ExpenseDescription = expenseDescriptionNode.InnerText;
            }

            bool? escrowedAtClosing = TryParseField<bool>(housingExpenseRecordElement, "IsEscrowedAtClosing", bool.TryParse);
            if (escrowedAtClosing != null)
            {
                expense.IsEscrowedAtClosing = escrowedAtClosing.Value ? E_TriState.Yes : E_TriState.No;
            }

            bool? isPrepaid = TryParseField<bool>(housingExpenseRecordElement, "IsPrepaid", bool.TryParse);
            if (isPrepaid != null)
            {
                expense.IsPrepaid = isPrepaid.Value;
            }

            bool? reserveMonthsLckd = TryParseField<bool>(housingExpenseRecordElement, "ReserveMonthsLckd", bool.TryParse);
            if (reserveMonthsLckd != null)
            {
                expense.ReserveMonthsLckd = reserveMonthsLckd.Value;
            }

            int? reserveMonths = TryParseField<int>(housingExpenseRecordElement, "ReserveMonths", int.TryParse);
            if (reserveMonths != null)
            {
                expense.ReserveMonths = reserveMonths.Value;
            }

            int? cushionMonths = TryParseField<int>(housingExpenseRecordElement, "CushionMonths", int.TryParse);
            if (cushionMonths != null)
            {
                expense.DisbursementScheduleMonths[0] = cushionMonths.Value;
            }

            E_TaxTableTaxT? taxType = TryParseEnumField<E_TaxTableTaxT>(housingExpenseRecordElement, "TaxType");
            if (taxType != null)
            {
                expense.TaxType = taxType.Value;
            }

            if (this.dataLoan.sAggEscrowCalcModeT == E_AggregateEscrowCalculationModeT.Legacy && expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                this.messages.AddHousingExpenseMessage("The calculation source for the housing expense is set to Disbursements, but the loan file's Aggregate Escrow/Impound calculation mode (sAggEscrowCalcModeT) is set to Legacy.  The Legacy calculation mode may not properly reflect updates to the disbursements. Please switch to the new calculation mode to reflect updated disbursement data.");
            }

            if (this.dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.LegacyButMigrated && expense.CustomExpenseLineNum == E_CustomExpenseLineNumberT.Any)
            {
                this.messages.AddHousingExpenseMessage("There are housing expenses marked as escrowed that are not assigned to specific HUD-1 lines and the file is in Legacy but Migrated closing cost mode. Escrowed housing expenses are required to be assigned to HUD-1 lines to correctly calculate in Legacy but Migrated mode.", "ESCROW WARNING");
            }
        }

        /// <summary>
        /// Executes the given action to add, edit, or delete a disbursement on a housing expense.
        /// Actions are determined as follows:
        ///     "Add" or ("0" with Disbursement Id not specified) - Add a new disbursement to the collection.
        ///     "Edit" or ("0" with Disbursement Id not specified) - Edit an existing disbursement in the collection.
        ///     "Delete" or "1" - Delete an existing disbursement in the collection.
        /// </summary>
        /// <param name="expense">The expense that houses the disbursement collection.</param>
        /// <param name="actualDisbursement">The disbursement XML element that needs to be parsed for disbursement info. This is passed through to <see cref="LoadDisbursementDataFromXml(XmlNode, SingleDisbursement)"/>.</param>
        /// <param name="action">The action string provided for the collection. The expected values are "Add", "Edit", "Delete", "0", and "1".</param>
        /// <param name="disbId">The disbursement Id as parsed from XML.</param>
        /// <param name="disbIdPresent">Whether or not the disbursement ID field was present in the XML. This affects what kind of error messages are given.</param>
        private void ExecuteDisbursementAction(BaseHousingExpense expense, XmlElement actualDisbursement, string action, Guid? disbId, bool disbIdPresent)
        {
            SingleDisbursement disbursement;
            if (action.Equals("Delete", StringComparison.OrdinalIgnoreCase) || action.Equals("1", StringComparison.OrdinalIgnoreCase))
            {
                if (!disbIdPresent)
                {
                    this.messages.AddHousingExpenseMessage("Could not find Disbursement ID (id=DisbId) field in ActualDisbursements collection. This is required to be able to delete. Skipping.");
                }
                else if (disbId != null && !expense.RemoveDisbursement(disbId.Value))
                {
                    this.messages.AddHousingExpenseMessage($"Trying to delete Disbursement ID (DisbId)= {disbId} was not successful. The disbursement was not found.");
                }
            }
            else if (action.Equals("Edit", StringComparison.OrdinalIgnoreCase) || (action.Equals("0", StringComparison.OrdinalIgnoreCase) && disbIdPresent))
            {
                if (!disbIdPresent)
                {
                    this.messages.AddHousingExpenseMessage("Could not find Disbursement ID (id=DisbId) field in ActualDisbursements collection. This is required to be able to edit. Skipping.");
                }
                else if (disbId != null && expense.ActualDisbursements.TryGetValue(disbId.Value, out disbursement))
                {
                    this.LoadDisbursementDataFromXml(actualDisbursement, disbursement);
                }
                else
                {
                    this.messages.AddHousingExpenseMessage($"Disbursement ID (DisbId)= {disbId} was not found to edit. Skipping.");
                }
            }
            else if (action.Equals("Add", StringComparison.OrdinalIgnoreCase) || (action.Equals("0", StringComparison.OrdinalIgnoreCase) && !disbIdPresent))
            {
                disbursement = new SingleDisbursement();
                disbursement.DisbursementType = E_DisbursementTypeT.Actual;
                expense.AddDisbursement(disbursement);
                this.LoadDisbursementDataFromXml(actualDisbursement, disbursement);
            }
            else
            {
                // Unrecognized action
                this.messages.AddHousingExpenseMessage($"Unrecognized ActualDisbursement action: {action}. Valid actions are Add, Edit, and Delete. Skipping.");
            }
        }

        /// <summary>
        /// Carries out the disbursement collection actions in the XML to add, edit or delete the <paramref name="expense"/>'s <see cref="BaseHousingExpense.ActualDisbursementsList"/>.
        /// </summary>
        /// <param name="expense">The housing expense to modify the disbursement collection of.</param>
        /// <param name="disbursementCollectionElement">The XML element containing the disbursement actions to carry out on the disbursement list.</param>
        private void ExecuteDisbursementCollectionActionsFromXml(BaseHousingExpense expense, XmlElement disbursementCollectionElement)
        {
            if (disbursementCollectionElement == null)
            {
                return;
            }

            foreach (XmlElement actualDisbursement in disbursementCollectionElement.SelectNodes("record").Cast<XmlElement>().OrderBy(disbursementRecord => disbursementRecord.GetAttribute("index")))
            {
                string action = actualDisbursement.GetAttribute("action");
                Guid? disbId = TryParseField<Guid>(actualDisbursement, "DisbId", Guid.TryParse);
                bool disbIdPresent = actualDisbursement.SelectSingleNode("field[@id=\"DisbId\"]") != null;

                if (string.IsNullOrEmpty(action))
                {
                    this.messages.AddHousingExpenseMessage("Action not provided for a record in the ActualDisbursements collection. Action is required. Skipping.");
                    continue;
                }

                this.ExecuteDisbursementAction(expense, actualDisbursement, action, disbId, disbIdPresent);
            }
        }

        /// <summary>
        /// Loads disbursement data from LOXML into a specified <see cref="SingleDisbursement"/> object.
        /// </summary>
        /// <param name="actualDisbursement">The disbursement data XML node.</param>
        /// <param name="disbursement">The disbursement object to load data into.</param>
        private void LoadDisbursementDataFromXml(XmlNode actualDisbursement, SingleDisbursement disbursement)
        {
            E_DisbPaidDateType? disbPaidDateType = this.TryParseEnumField<E_DisbPaidDateType>(actualDisbursement, "DisbPaidDateType");
            if (disbPaidDateType != null)
            {
                disbursement.DisbPaidDateType = disbPaidDateType.Value;
            }

            DateTime? dueDate = this.TryParseField<DateTime>(actualDisbursement, "DueDate", DateTime.TryParse);
            if (dueDate != null)
            {
                disbursement.DueDate = dueDate.Value;
            }

            decimal? disbursementAmt = this.TryParseField<decimal>(actualDisbursement, "DisbursementAmt", decimal.TryParse);
            if (disbursementAmt != null)
            {
                disbursement.DisbursementAmt = disbursementAmt.Value;
            }

            int? coveredMonths = this.TryParseField<int>(actualDisbursement, "CoveredMonths", int.TryParse);
            if (coveredMonths != null)
            {
                disbursement.CoveredMonths = coveredMonths.Value;
            }

            DateTime? paidDate = this.TryParseField<DateTime>(actualDisbursement, "PaidDate", DateTime.TryParse);
            if (paidDate != null)
            {
                disbursement.PaidDate = paidDate.Value;
            }

            E_DisbursementPaidByT? paidByType = this.TryParseEnumField<E_DisbursementPaidByT>(actualDisbursement, "PaidBy");
            if (paidByType != null)
            {
                disbursement.PaidBy = paidByType.Value;
            }

            DateTime? billingPeriodStartD = this.TryParseField<DateTime>(actualDisbursement, "BillingPeriodStartD", DateTime.TryParse);
            if (billingPeriodStartD == null)
            {
                disbursement.BillingPeriodStartD = billingPeriodStartD.Value;
            }

            DateTime? billingPeriodEndD = this.TryParseField<DateTime>(actualDisbursement, "BillingPeriodEndD", DateTime.TryParse);
            if (billingPeriodEndD == null)
            {
                disbursement.BillingPeriodEndD = billingPeriodEndD.Value;
            }
        }

        /// <summary>
        /// Tries to parse an Enum value from an XmlNode. Returns null if unsuccessful. Logs parsing errors to messages.
        /// </summary>
        /// <typeparam name="TEnum">The type of Enum to try to parse from the field text.</typeparam>
        /// <param name="record">The XmlNode representing the record. There should be only one field with id=<paramref name="fieldName"/> within this node.</param>
        /// <param name="fieldName">The field ID to look for the value of.</param>
        /// <returns>The field value, or null if it was not parsed successfully.</returns>
        private TEnum? TryParseEnumField<TEnum>(XmlNode record, string fieldName) where TEnum : struct, IComparable
        {
            TEnum? enumValue = TryParseField<TEnum>(record, fieldName, Enum.TryParse);

            if (enumValue == null)
            {
                return null;
            }

            if (!Enum.IsDefined(typeof(TEnum), enumValue))
            {
                this.messages.AddHousingExpenseMessage($"Value= {record.SelectSingleNode($".//field[@id=\"{fieldName}\"]").InnerText} is invalid for field {fieldName}.");
                return null;
            }

            return enumValue;
        }

        /// <summary>
        /// Tries to parse a field value from an LOXml-formatted XmlNode. Returns null if unsuccessful. Logs parsing errors to messages.
        /// </summary>
        /// <typeparam name="TStruct">The type to try to parse from the field text.</typeparam>
        /// <param name="record">The XmlNode representing the record. There should be only one field with id=<paramref name="fieldName"/> within this node.</param>
        /// <param name="fieldName">The field ID to look for the value of.</param>
        /// <param name="tryParseMethod">A TryParse method used to parse the value of the field. For example, <see cref="int.TryParse(string, out int)"/>.</param>
        /// <returns>The field value, or null if it was not parsed successfully.</returns>
        private TStruct? TryParseField<TStruct>(XmlNode record, string fieldName, TryParseDelegate<TStruct> tryParseMethod) where TStruct : struct
        {
            if (record == null)
            {
                this.messages.AddHousingExpenseMessage($"Something went wrong trying to parse field {fieldName}. Record was not found.");
                return null;
            }

            XmlNode fieldNode = record.SelectSingleNode($".//field[@id=\"{fieldName}\"]");
            if (fieldNode == null)
            {
                return null;
            }

            TStruct value;
            if (!tryParseMethod(fieldNode.InnerText, out value))
            {
                this.messages.AddHousingExpenseMessage($"Could not parse value= {fieldNode.InnerText} for field {fieldName}.");
                return null;
            }

            return value;
        }

        /// <summary>
        /// A log object that handles the formatting of errors and warning messages for <see cref="LoFormatHousingExpenseImporter"/>.
        /// </summary>
        private class LoFormatHousingExpenseMessages
        {
            /// <summary>
            /// The underlying object storing the messages generated by this class.
            /// </summary>
            private StringBuilder messages;

            /// <summary>
            /// Initializes a new instance of the <see cref="LoFormatHousingExpenseMessages"/> class.
            /// </summary>
            /// <param name="logTarget">The underlying <see cref="StringBuilder"/> to write to.</param>
            /// <param name="loanNumber">The loan number of the loan being logged for (optional).</param>
            public LoFormatHousingExpenseMessages(StringBuilder logTarget, string loanNumber = null)
            {
                this.messages = logTarget;
                this.LoanNumber = loanNumber;
                this.ExpenseType = string.Empty;
                this.ExpenseRecordIndex = null;
            }

            /// <summary>
            /// Gets or sets the expense type in the context of the parent importer, added to messages.
            /// </summary>
            public string ExpenseType { get; set; }

            /// <summary>
            /// Gets or sets the loan number in the context of the parent importer, added to messages.
            /// </summary>
            public string LoanNumber { get; set; }

            /// <summary>
            /// Gets or sets the index of the current housing expense record.
            /// </summary>
            public int? ExpenseRecordIndex { get; set; }

            /// <summary>
            /// Adds a new message to the logging target with the current context information (<see cref="LoanNumber"/> and <see cref="ExpenseType"/>) and warning level.
            /// </summary>
            /// <param name="message">The message to add.</param>
            /// <param name="warningLevel">The warning level (usually WARNING or ERROR) to add before the message. Default is WARNING.</param>
            public void AddHousingExpenseMessage(string message, string warningLevel = "WARNING")
            {
                this.AddHousingExpenseMessage(message, this.LoanNumber, this.ExpenseRecordIndex, this.ExpenseType, warningLevel);
            }

            /// <summary>
            /// Appends a new message to the logging target as a new line.
            /// </summary>
            /// <param name="message">The new message to add.</param>
            public void AddMessage(string message)
            {
                this.messages.AppendLine(message);
            }

            /// <summary>
            /// Adds a new message to the logging target with custom context information (loan number and expense type) and warning level.
            /// </summary>
            /// <param name="message">The message to add.</param>
            /// <param name="loanNumber">The loan number for context of where the message occurred.</param>
            /// <param name="index">The index of the record where the message occurred.</param>
            /// <param name="calculatedExpenseType">The expense type for context of where the message occurred.</param>
            /// <param name="warningLevel">The warning level (usually WARNING or ERROR) to add before the message. Default is WARNING.</param>
            private void AddHousingExpenseMessage(string message, string loanNumber = null, int? index = null, string calculatedExpenseType = null, string warningLevel = "WARNING")
            {
                this.AddMessage(
                    $"{(string.IsNullOrEmpty(warningLevel) ? string.Empty : warningLevel + ": ")}" +
                    $"{(string.IsNullOrEmpty(loanNumber) ? string.Empty : $"[sLNm]= {loanNumber} ")}" +
                    $"{(string.IsNullOrEmpty(calculatedExpenseType) ? string.Empty : $"[Housing Expense]= {calculatedExpenseType} ")}" +
                    $"{(index == null ? string.Empty : $"[index]= {index} ")}-- {message}");
            }
        }
    }
}
