using System;
using System.Collections;
using System.Xml;

using LendersOffice.Common;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Constants;
using System.Collections.Generic;
using LqbGrammar.DataTypes;

namespace LendersOffice.Conversions.Mismo23
{
	public class LoanMismo23Importer
	{
        private CPageData  m_dataLoan;
        private Hashtable m_dataAppHash = new Hashtable();
        private bool hasSubjectPropertyNetCashFlow;
        private decimal totalSubjectPropertyNetCashFlow;

		public LoanMismo23Importer()
		{
		}

        /// <summary>
        /// dataLoan object must be in InitSave() state. And need to invoke Save() after this method return.
        /// </summary>
        /// <param name="dataLoan"></param>
        /// <param name="doc"></param>
        public void Import(CPageData dataLoan, XmlDocument doc) 
        {
            m_dataLoan = dataLoan;
            ReadLoanApplication(doc);
        }

        /// <summary>
        /// Import MISMO xml into LoanID. To create new loan, pass Guid.Empty as LoanID. If srcLoanID is not valid then exception will get throw.
        /// </summary>
        /// <param name="srcLoanID"></param>
        /// <param name="mismoXml"></param>
        public CPageData  Import(Guid srcLoanID, XmlDocument doc, AbstractUserPrincipal principal, E_IntegrationT type) 
        {
            CLoanFileCreator creator = null;

            if (Guid.Empty == srcLoanID)
            {
                creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.MismoImport);
                srcLoanID = creator.BeginCreateImportBaseLoanFile(
                    sourceFileId: Guid.Empty,
                    setInitialEmployeeRoles: true,
                    addEmployeeOfficialAgent: true,
                    assignEmployeesFromRelationships: true,
                    branchIdToUse: Guid.Empty);
            }
            m_dataLoan = new CLoanMismo23ImporterData(srcLoanID);
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            m_dataLoan.SetFormatTarget(FormatTarget.LON_Mismo);

            ReadLoanApplication(doc);

            // 08/07/03-Binh-Added IntegrationTypeSet so that change msgs will be
            // forwarded to a queue for processing
            m_dataLoan.IntegrationTypeAdd(type) ;

            m_dataLoan.Save();

            /// Officially declare that this loan is valid. Otherwise it won't
            /// show up on the Search page
            if( null != creator )
                creator.CommitFileCreation(false);

            return m_dataLoan;

        }

        private void ReadLoanApplication(XmlDocument doc) 
        {
            this.hasSubjectPropertyNetCashFlow = false;
            this.totalSubjectPropertyNetCashFlow = 0;

            XmlElement xeLoanApp = (XmlElement) doc.SelectSingleNode("//LOAN_APPLICATION");
            if (xeLoanApp == null)
                throw new CBaseException(ErrorMessages.InvalidMismoFormat, "Invalid MISMO format");

            // <xs:element ref="BORROWER" maxOccurs="unbounded" />
            ReadBorrowers(xeLoanApp.SelectNodes("BORROWER"));

            if (this.m_dataLoan.sIsIncomeCollectionEnabled)
            {
                this.m_dataLoan.ImportGrossRent(this.hasSubjectPropertyNetCashFlow, this.totalSubjectPropertyNetCashFlow);
            }
            
            //<xs:element ref="ADDITIONAL_CASE_DATA" minOccurs="0" />
            ReadAdditionalCaseData((XmlElement) xeLoanApp.SelectSingleNode("ADDITIONAL_CASE_DATA"));

            //  <xs:element ref="ASSET" minOccurs="0" maxOccurs="unbounded" /> 
            ReadAssets(xeLoanApp.SelectNodes("ASSET"));
            
            //  <xs:element ref="DOWN_PAYMENT" minOccurs="0" maxOccurs="unbounded" /> 
            ReadDownPayment((XmlElement) xeLoanApp.SelectSingleNode("DOWN_PAYMENT"));

            //  <xs:element ref="GOVERNMENT_LOAN" minOccurs="0" />             
            ReadGovernmentLoan((XmlElement) xeLoanApp.SelectSingleNode("GOVERNMENT_LOAN"));
          
            //  <xs:element ref="INTERVIEWER_INFORMATION" minOccurs="0" />  
            ReadInterviewerInformation((XmlElement) xeLoanApp.SelectSingleNode("INTERVIEWER_INFORMATION"));
            
            //  <xs:element ref="LIABILITY" minOccurs="0" maxOccurs="unbounded" /> 
            ReadLiabilities(xeLoanApp.SelectNodes("LIABILITY"));

            //  <xs:element ref="LOAN_PRODUCT_DATA" minOccurs="0" />
            ReadLoanProductData((XmlElement) xeLoanApp.SelectSingleNode("LOAN_PRODUCT_DATA"));
            
            //  <xs:element ref="MORTGAGE_TERMS" minOccurs="0" /> 
            ReadMortgageTerms((XmlElement) xeLoanApp.SelectSingleNode("MORTGAGE_TERMS"));
            
            //  <xs:element ref="PROPERTY" minOccurs="0" /> 
            ReadProperty((XmlElement) xeLoanApp.SelectSingleNode("PROPERTY"));
            
            //  <xs:element ref="PROPOSED_HOUSING_EXPENSE" minOccurs="0" maxOccurs="unbounded" /> 
            ReadProposedHousingExpense(xeLoanApp.SelectNodes("PROPOSED_HOUSING_EXPENSE"));
            
            //  <xs:element ref="REO_PROPERTY" minOccurs="0" maxOccurs="unbounded" /> 
            ReadREOProperties(xeLoanApp.SelectNodes("REO_PROPERTY"));
            
            //  <xs:element ref="TITLE_HOLDER" minOccurs="0" maxOccurs="unbounded" /> 
            ReadTitleHolder(xeLoanApp.SelectNodes("TITLE_HOLDER"));

            // <xs:element ref="TRANSACTION_DETAIL" minOccurs="0" />
            ReadTransactionDetail((XmlElement) xeLoanApp.SelectSingleNode("TRANSACTION_DETAIL"));

            //  <xs:element ref="_DATA_INFORMATION" minOccurs="0" /> 
            //  <xs:element ref="AFFORDABLE_LENDING" minOccurs="0" /> 
            //  <xs:element ref="GOVERNMENT_REPORTING" minOccurs="0" /> 
            //  <xs:element ref="LOAN_PURPOSE" minOccurs="0" /> 
            ReadLoanPurposeData((XmlElement) xeLoanApp.SelectSingleNode("LOAN_PURPOSE"));
            //  <xs:element ref="LOAN_QUALIFICATION" minOccurs="0" /> 
            //  <xs:element ref="TRANSACTION_DETAIL" minOccurs="0" /> 
            //  <xs:element ref="ESCROW" minOccurs="0" maxOccurs="unbounded" /> 
            //  <xs:element ref="RESPA_FEE" minOccurs="0" maxOccurs="unbounded" /> 
            //  <xs:element ref="MI_DATA" minOccurs="0" /> 
            //  <xs:element ref="DISCLOSURE_DATA" minOccurs="0" /> 

        }
        private CAppData FindAppByID(string id) 
        {
            return (CAppData) m_dataAppHash[id];
        }
        private void ReadBorrowers(XmlNodeList nodeList) 
        {
            foreach (XmlElement el in nodeList) 
            {
                CAppData dataApp = FindAppByID(el.GetAttribute("BorrowerID"));
                bool isCoborrower = el.GetAttribute("_PrintPositionType") == "CoBorrower";

                if (null == dataApp) 
                {
                    // 1/25/2005 dd - Unable to locate dataApp from hash, try to search by SSN. Only useful for import existing.
                    dataApp = FindAppBySsn(el.GetAttribute("_SSN"));

                    if (null == dataApp) 
                    {
                        m_dataLoan.Save();
                        dataApp = m_dataLoan.GetAppData(m_dataLoan.AddNewApp());
                        m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    } 
                    else 
                    {
                        if (!m_dataAppHash.Contains(el.GetAttribute("BorrowerID"))) 
                        {
                            m_dataAppHash.Add(el.GetAttribute("BorrowerID"), dataApp);
                        }
                        if (!m_dataAppHash.Contains(el.GetAttribute("JointAssetBorrowerID"))) 
                        {
                            m_dataAppHash.Add(el.GetAttribute("JointAssetBorrowerID"), dataApp);
                        }
                    }

                }

                #region BORROWER_INFO


                if (isCoborrower) 
                {
                    dataApp.aCFirstNm       = el.GetAttribute("_FirstName");
                    dataApp.aCMidNm         = el.GetAttribute("_MiddleName");
                    dataApp.aCLastNm        = el.GetAttribute("_LastName");
                    dataApp.aCSuffix        = el.GetAttribute("_NameSuffix");
                    dataApp.aCAge_rep       = el.GetAttribute("_AgeAtApplicationYears");
                    dataApp.aCHPhone        = el.GetAttribute("_HomeTelephoneNumber");
                    dataApp.aCSsn           = el.GetAttribute("_SSN");
                    dataApp.aCDependNum_rep = el.GetAttribute("DependentCount");
                    dataApp.aCSchoolYrs_rep = el.GetAttribute("SchoolingYears");
                    dataApp.aCMaritalStatT  = (E_aCMaritalStatT) MaritalStatusMap.FromMismo(el.GetAttribute("MaritalStatusType"));
                    try 
                    {
                        string s = el.GetAttribute("_BirthDate");
                        DateTime dt = DateTime.ParseExact(s, new string[] {"yyyyMMdd", "yyyy-MM-dd", "yyyy-MM", "yyyy-MM-ddTHH:mm:ss"},null, System.Globalization.DateTimeStyles.None);
                        dataApp.aCDob_rep       = dt.ToString("MM-dd-yyyy");
                    } 
                    catch {}

                } 
                else 
                {
                    dataApp.aBFirstNm       = el.GetAttribute("_FirstName");
                    dataApp.aBMidNm         = el.GetAttribute("_MiddleName");
                    dataApp.aBLastNm        = el.GetAttribute("_LastName");
                    dataApp.aBSuffix        = el.GetAttribute("_NameSuffix");
                    dataApp.aBAge_rep       = el.GetAttribute("_AgeAtApplicationYears");
                    dataApp.aBHPhone        = el.GetAttribute("_HomeTelephoneNumber");
                    dataApp.aBSsn           = el.GetAttribute("_SSN");
                    dataApp.aBDependNum_rep = el.GetAttribute("DependentCount");
                    dataApp.aBSchoolYrs_rep = el.GetAttribute("SchoolingYears");
                    dataApp.aBMaritalStatT  = MaritalStatusMap.FromMismo(el.GetAttribute("MaritalStatusType"));
                    try 
                    {
                        string s = el.GetAttribute("_BirthDate");
                        DateTime dt = DateTime.ParseExact(s, new string[] {"yyyyMMdd", "yyyy-MM-dd", "yyyy-MM", "yyyy-MM-ddTHH:mm:ss"},null, System.Globalization.DateTimeStyles.None);
                        dataApp.aBDob_rep       = dt.ToString("MM-dd-yyyy");
                    } 
                    catch {}
                }
                dataApp.aAsstLiaCompletedNotJointly = el.GetAttribute("JointAssetLiabilityReportingType") == "NotJointly";
                #endregion

                #region _MAIL_TO
                XmlElement elMailTo = (XmlElement) el.SelectSingleNode("_MAIL_TO");
                if (null != elMailTo) 
                {
                    if (isCoborrower) 
                    {
                        dataApp.aCAddrMailUsePresentAddr = false;
                        dataApp.aCAddrMail               = elMailTo.GetAttribute("_StreetAddress");
                        dataApp.aCCityMail               = elMailTo.GetAttribute("_City");
                        dataApp.aCStateMail              = elMailTo.GetAttribute("_State");
                        dataApp.aCZipMail                = elMailTo.GetAttribute("_PostalCode");

                    } 
                    else 
                    {
                        dataApp.aBAddrMailUsePresentAddr = false;
                        dataApp.aBAddrMail               = elMailTo.GetAttribute("_StreetAddress");
                        dataApp.aBCityMail               = elMailTo.GetAttribute("_City");
                        dataApp.aBStateMail              = elMailTo.GetAttribute("_State");
                        dataApp.aBZipMail                = elMailTo.GetAttribute("_PostalCode");
                    }
                } 
                else 
                {
                    if (isCoborrower)
                        dataApp.aCAddrMailUsePresentAddr = true;
                    else
                        dataApp.aBAddrMailUsePresentAddr = true;

                }
                #endregion

                #region _RESIDENCE
                XmlElement elResidence = (XmlElement) el.SelectSingleNode("_RESIDENCE[@BorrowerResidencyType='Current']");
                if (null != elResidence) 
                {   
                    if (isCoborrower) 
                    {
                        dataApp.aCAddr    = elResidence.GetAttribute("_StreetAddress");
                        dataApp.aCCity    = elResidence.GetAttribute("_City");
                        dataApp.aCState   = elResidence.GetAttribute("_State");
                        dataApp.aCZip     = elResidence.GetAttribute("_PostalCode");
                        dataApp.aCAddrT   = (E_aCAddrT) AddressTypeMap.FromMismo(elResidence.GetAttribute("BorrowerResidencyBasisType"));
                        dataApp.aCAddrYrs = elResidence.GetAttribute("BorrowerResidencyDurationYears");
                        // 4/27/2004 dd - BorrowerResidencyDurationMonths NOT SUPPORTED

                    } 
                    else 
                    {
                        dataApp.aBAddr    = elResidence.GetAttribute("_StreetAddress");
                        dataApp.aBCity    = elResidence.GetAttribute("_City");
                        dataApp.aBState   = elResidence.GetAttribute("_State");
                        dataApp.aBZip     = elResidence.GetAttribute("_PostalCode");
                        dataApp.aBAddrT   = (E_aBAddrT) AddressTypeMap.FromMismo(elResidence.GetAttribute("BorrowerResidencyBasisType"));
                        dataApp.aBAddrYrs = elResidence.GetAttribute("BorrowerResidencyDurationYears");
                        // 4/27/2004 dd - BorrowerResidencyDurationMonths NOT SUPPORTED
                    }
                }
                elResidence = (XmlElement) el.SelectSingleNode("_RESIDENCE[@BorrowerResidencyType='Prior'][1]");
                if (null != elResidence) 
                {
                    if (isCoborrower) 
                    {
                        dataApp.aCPrev1Addr    = elResidence.GetAttribute("_StreetAddress");
                        dataApp.aCPrev1City    = elResidence.GetAttribute("_City");
                        dataApp.aCPrev1State   = elResidence.GetAttribute("_State");
                        dataApp.aCPrev1Zip     = elResidence.GetAttribute("_PostalCode");
                        dataApp.aCPrev1AddrT   = (E_aCPrev1AddrT) AddressTypeMap.FromMismo(elResidence.GetAttribute("BorrowerResidencyBasisType"));
                        dataApp.aCPrev1AddrYrs = elResidence.GetAttribute("BorrowerResidencyDurationYears");
                        // 4/27/2004 dd - BorrowerResidencyDurationMonths NOT SUPPORTED

                    } 
                    else 
                    {
                        dataApp.aBPrev1Addr    = elResidence.GetAttribute("_StreetAddress");
                        dataApp.aBPrev1City    = elResidence.GetAttribute("_City");
                        dataApp.aBPrev1State   = elResidence.GetAttribute("_State");
                        dataApp.aBPrev1Zip     = elResidence.GetAttribute("_PostalCode");
                        dataApp.aBPrev1AddrT   = (E_aBPrev1AddrT) AddressTypeMap.FromMismo(elResidence.GetAttribute("BorrowerResidencyBasisType"));
                        dataApp.aBPrev1AddrYrs = elResidence.GetAttribute("BorrowerResidencyDurationYears");
                        // 4/27/2004 dd - BorrowerResidencyDurationMonths NOT SUPPORTED
                    }
                }
                elResidence = (XmlElement) el.SelectSingleNode("_RESIDENCE[@BorrowerResidencyType='Prior'][2]");
                if (null != elResidence) 
                {
                    if (isCoborrower) 
                    {
                        dataApp.aCPrev2Addr    = elResidence.GetAttribute("_StreetAddress");
                        dataApp.aCPrev2City    = elResidence.GetAttribute("_City");
                        dataApp.aCPrev2State   = elResidence.GetAttribute("_State");
                        dataApp.aCPrev2Zip     = elResidence.GetAttribute("_PostalCode");
                        dataApp.aCPrev2AddrT   = (E_aCPrev2AddrT) AddressTypeMap.FromMismo(elResidence.GetAttribute("BorrowerResidencyBasisType"));
                        dataApp.aCPrev2AddrYrs = elResidence.GetAttribute("BorrowerResidencyDurationYears");
                        // 4/27/2004 dd - BorrowerResidencyDurationMonths NOT SUPPORTED

                    } 
                    else 
                    {
                        dataApp.aBPrev2Addr    = elResidence.GetAttribute("_StreetAddress");
                        dataApp.aBPrev2City    = elResidence.GetAttribute("_City");
                        dataApp.aBPrev2State   = elResidence.GetAttribute("_State");
                        dataApp.aBPrev2Zip     = elResidence.GetAttribute("_PostalCode");
                        dataApp.aBPrev2AddrT   = (E_aBPrev2AddrT) AddressTypeMap.FromMismo(elResidence.GetAttribute("BorrowerResidencyBasisType"));
                        dataApp.aBPrev2AddrYrs = elResidence.GetAttribute("BorrowerResidencyDurationYears");
                        // 4/27/2004 dd - BorrowerResidencyDurationMonths NOT SUPPORTED
                    }
                }
                #endregion

                #region CURRENT_INCOME
                List<OtherIncome> otherIncomeList = new List<OtherIncome>();
                XmlNodeList listIncome = el.SelectNodes("CURRENT_INCOME");

                var consumerId = isCoborrower
                    ? dataApp.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>()
                    : dataApp.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();

                if (m_dataLoan.sIsIncomeCollectionEnabled)
                {
                    m_dataLoan.ClearPrimaryIncomeSourcesForConsumer(consumerId);
                }

                foreach (XmlElement elIncome in listIncome) 
                {
                    var monthlyAmount = Money.Create(m_dataLoan.m_convertLos.ToMoney(elIncome.GetAttribute("_MonthlyTotalAmount")));

                    switch (elIncome.GetAttribute("IncomeType")) 
                    {
                        case "Base":
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                if (isCoborrower)
                                    dataApp.aCBaseI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                                else
                                    dataApp.aBBaseI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.BaseIncome,
                                        MonthlyAmountData = monthlyAmount
                                    });
                            }
                           break; 
                        case "Overtime":
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                if (isCoborrower)
                                    dataApp.aCOvertimeI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                                else
                                    dataApp.aBOvertimeI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.Overtime,
                                        MonthlyAmountData = monthlyAmount
                                    });
                            }
                            break; 
                        case "Bonus":
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                if (isCoborrower)
                                    dataApp.aCBonusesI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                                else
                                    dataApp.aBBonusesI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.Bonuses,
                                        MonthlyAmountData = monthlyAmount
                                    });
                            }
                            break; 
                        case "Commissions": 
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                if (isCoborrower)
                                    dataApp.aCCommisionI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                                else
                                    dataApp.aBCommisionI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.Commission,
                                        MonthlyAmountData = monthlyAmount
                                    });
                            }
                            break; 
                        case "DividendsInterest": 
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                if (isCoborrower)
                                    dataApp.aCDividendI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                                else
                                    dataApp.aBDividendI_rep = elIncome.GetAttribute("_MonthlyTotalAmount");
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.DividendsOrInterest,
                                        MonthlyAmountData = monthlyAmount
                                    });
                            }
                            break; 
                        case "NetRentalIncome": 
                            dataApp.aNetRentI1003Lckd = true;
                            if (isCoborrower) 
                                dataApp.aCNetRentI1003_rep = elIncome.GetAttribute("_MonthlyTotalAmount"); 
                            else 
                                dataApp.aBNetRentI1003_rep = elIncome.GetAttribute("_MonthlyTotalAmount"); 
                            break; 
                        default:
                            // 4/27/2004 dd - Group everything else to other Monthly Income.
                            if (m_dataLoan.sIsIncomeCollectionEnabled 
                                && elIncome.GetAttribute("IncomeType").Equals("SubjectPropertyNetCashFlow", StringComparison.OrdinalIgnoreCase))
                            {
                                this.m_dataLoan.sSpCountRentalIForPrimaryResidToo = true;
                                this.hasSubjectPropertyNetCashFlow = true;
                                this.totalSubjectPropertyNetCashFlow += m_dataLoan.m_convertLos.ToMoney(elIncome.GetAttribute("_MonthlyTotalAmount"));
                            }
                            else
                            {
                                otherIncomeList.Add(new OtherIncome()
                                {
                                    Amount = m_dataLoan.m_convertLos.ToMoney(elIncome.GetAttribute("_MonthlyTotalAmount")),
                                    Desc = GetOtherMonthlyIncomeDescription(elIncome.GetAttribute("IncomeType")),
                                    IsForCoBorrower = isCoborrower
                                });
                            }
                            break;
                    } // switch (elIncome.GetAttribute("IncomeType")) 
                }

                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                {
                    dataApp.aOtherIncomeList = otherIncomeList;
                }
                else
                {
                    var migratedOtherIncome = IncomeCollectionMigration.GetMigratedOtherIncome(otherIncomeList, dataApp.aBConsumerId, dataApp.aCConsumerId);
                    foreach (var consumerIdIncomeSourcePair in migratedOtherIncome)
                    {
                        m_dataLoan.AddIncomeSource(consumerIdIncomeSourcePair.Item1, consumerIdIncomeSourcePair.Item2);
                    }
                }
                #endregion

                #region DECLARATION

                XmlElement elDeclaration = (XmlElement) el.SelectSingleNode("DECLARATION");
                if (null != elDeclaration) 
                {
                    if (isCoborrower) 
                    {
                        switch (elDeclaration.GetAttribute("CitizenshipResidencyType")) 
                        {
                            case "USCitizen":
                                dataApp.aCDecCitizen = "Y";
                                dataApp.aCDecResidency = "N";
                                break;
                            case "PermanentResidentAlien":
                                dataApp.aCDecCitizen = "N";
                                dataApp.aCDecResidency = "Y";
                                break;
                            case "NonPermanentResidentAlien":
                            case "NonResidentAlien":
                            case "Unknown":
                                dataApp.aCDecCitizen = "N";
                                dataApp.aCDecResidency = "N";
                                break;
                        }
                        dataApp.aCDecAlimony             = elDeclaration.GetAttribute("AlimonyChildSupportObligationIndicator");
                        dataApp.aCDecBankrupt            = elDeclaration.GetAttribute("BankruptcyIndicator");
                        dataApp.aCDecBorrowing           = elDeclaration.GetAttribute("BorrowedDownPaymentIndicator");
                        dataApp.aCDecEndorser            = elDeclaration.GetAttribute("CoMakerEndorserOfNoteIndicator");
                        dataApp.aCDecPastOwnership       = elDeclaration.GetAttribute("HomeownerPastThreeYearsType") == "Yes" ? "Y" : "N";
                        dataApp.aCDecOcc                 = elDeclaration.GetAttribute("IntentToOccupyType") == "Yes" ? "Y" : "N";
                        dataApp.aCDecObligated           = elDeclaration.GetAttribute("LoanForeclosureOrJudgementIndicator");
                        dataApp.aCDecJudgment            = elDeclaration.GetAttribute("OutstandingJudgementsIndicator");
                        dataApp.aCDecLawsuit             = elDeclaration.GetAttribute("PartyToLawsuitIndicator");
                        dataApp.aCDecDelinquent          = elDeclaration.GetAttribute("PresentlyDelinquentIndicator");
                        dataApp.aCDecForeclosure         = elDeclaration.GetAttribute("PropertyForeclosedPastSevenYearsIndicator");
                        dataApp.aCDecPastOwnedPropTitleT = (E_aCDecPastOwnedPropTitleT) PriorPropTitleTypeMap.FromMismo(elDeclaration.GetAttribute("PriorPropertyTitleType"));
                        dataApp.aCDecPastOwnedPropT      = (E_aCDecPastOwnedPropT) PriorPropUsageTypeMap.FromMismo(elDeclaration.GetAttribute("PriorPropertyUsageType"));
                        //elDeclaration.GetAttribute("BorrowerFirstTimeHomebuyerIndicator"); // 4/27/2004 dd - DON'T HAVE IT

                    }  // if (isCoborrower) 
                    else 
                    {
                        switch (elDeclaration.GetAttribute("CitizenshipResidencyType")) 
                        {
                            case "USCitizen":
                                dataApp.aBDecCitizen = "Y";
                                dataApp.aBDecResidency = "N";
                                break;
                            case "PermanentResidentAlien":
                                dataApp.aBDecCitizen = "N";
                                dataApp.aBDecResidency = "Y";
                                break;
                            case "NonPermanentResidentAlien":
                            case "NonResidentAlien":
                            case "Unknown":
                                dataApp.aBDecCitizen = "N";
                                dataApp.aBDecResidency = "N";
                                break;
                        }
                        dataApp.aBDecAlimony             = elDeclaration.GetAttribute("AlimonyChildSupportObligationIndicator");
                        dataApp.aBDecBankrupt            = elDeclaration.GetAttribute("BankruptcyIndicator");
                        dataApp.aBDecBorrowing           = elDeclaration.GetAttribute("BorrowedDownPaymentIndicator");
                        dataApp.aBDecEndorser            = elDeclaration.GetAttribute("CoMakerEndorserOfNoteIndicator");
                        dataApp.aBDecPastOwnership       = elDeclaration.GetAttribute("HomeownerPastThreeYearsType") == "Yes" ? "Y" : "N";
                        dataApp.aBDecOcc                 = elDeclaration.GetAttribute("IntentToOccupyType") == "Yes" ? "Y" : "N";
                        dataApp.aBDecObligated           = elDeclaration.GetAttribute("LoanForeclosureOrJudgementIndicator");
                        dataApp.aBDecJudgment            = elDeclaration.GetAttribute("OutstandingJudgementsIndicator");
                        dataApp.aBDecLawsuit             = elDeclaration.GetAttribute("PartyToLawsuitIndicator");
                        dataApp.aBDecDelinquent          = elDeclaration.GetAttribute("PresentlyDelinquentIndicator");
                        dataApp.aBDecForeclosure         = elDeclaration.GetAttribute("PropertyForeclosedPastSevenYearsIndicator");
                        dataApp.aBDecPastOwnedPropTitleT = (E_aBDecPastOwnedPropTitleT) PriorPropTitleTypeMap.FromMismo(elDeclaration.GetAttribute("PriorPropertyTitleType"));
                        dataApp.aBDecPastOwnedPropT      = (E_aBDecPastOwnedPropT) PriorPropUsageTypeMap.FromMismo(elDeclaration.GetAttribute("PriorPropertyUsageType"));
                        //elDeclaration.GetAttribute("BorrowerFirstTimeHomebuyerIndicator"); // 4/27/2004 dd - DON'T HAVE IT

                    }
                }
                #endregion

                #region DEPENDENT
                XmlNodeList  elDependentList = el.SelectNodes("DEPENDENT");
                bool isFirstNode = true;
                foreach (XmlElement elDependent in elDependentList) 
                {
                    string age = elDependent.GetAttribute("_AgeYears");
                    if (isCoborrower)
                        dataApp.aCDependAges += (isFirstNode ? "" : ",") + age;
                    else
                        dataApp.aBDependAges += (isFirstNode ? "" : ",") + age;
                    isFirstNode = false;

                }
                #endregion

                #region EMPLOYER
                IEmpCollection empColl = isCoborrower ? dataApp.aCEmpCollection : dataApp.aBEmpCollection;
                XmlNodeList employerList = el.SelectNodes("EMPLOYER");
                foreach (XmlElement elEmployer in employerList) 
                {
                    bool isPrimary = elEmployer.GetAttribute("EmploymentPrimaryIndicator") == "Y";
                    if (isPrimary) 
                    {
                        IPrimaryEmploymentRecord record = empColl.GetPrimaryEmp(true);
                        record.EmplrNm = elEmployer.GetAttribute("_Name");
                        record.EmplrAddr = elEmployer.GetAttribute("_StreetAddress");
                        record.EmplrCity = elEmployer.GetAttribute("_City");
                        record.EmplrState = elEmployer.GetAttribute("_State");
                        record.EmplrZip = elEmployer.GetAttribute("_PostalCode");
                        record.EmplrBusPhone = elEmployer.GetAttribute("_TelephoneNumber");
                        record.ProfLen_rep = elEmployer.GetAttribute("CurrentEmploymentTimeInLineOfWorkYears");
                        record.EmplmtLen_rep = elEmployer.GetAttribute("CurrentEmploymentYearsOnJob"); // 4/27/2004 dd - Ignore Month for now.
                        record.IsSelfEmplmt = elEmployer.GetAttribute("EmploymentBorrowerSelfEmployedIndicator") == "Y";
                        record.JobTitle = elEmployer.GetAttribute("EmploymentPositionDescription");
                    } 
                    else 
                    {
                        IRegularEmploymentRecord record = FindRegularEmployment(empColl, elEmployer);
                        if (null == record) 
                            record = empColl.AddRegularRecord();
                        record.EmplrNm = elEmployer.GetAttribute("_Name");
                        record.EmplrAddr = elEmployer.GetAttribute("_StreetAddress");
                        record.EmplrCity = elEmployer.GetAttribute("_City");
                        record.EmplrState = elEmployer.GetAttribute("_State");
                        record.EmplrZip = elEmployer.GetAttribute("_PostalCode");
                        record.EmplrBusPhone = elEmployer.GetAttribute("_TelephoneNumber");
                        record.IsSelfEmplmt = elEmployer.GetAttribute("EmploymentBorrowerSelfEmployedIndicator") == "Y";
                        record.JobTitle = elEmployer.GetAttribute("EmploymentPositionDescription");
                        record.MonI_rep = elEmployer.GetAttribute("IncomeEmploymentMonthlyAmount");
                        record.EmplmtStartD_rep = elEmployer.GetAttribute("PreviousEmploymentStartDate");
                        record.EmplmtEndD_rep = elEmployer.GetAttribute("PreviousEmploymentEndDate");
                    }
                }
                #endregion

                #region GOVERNMENT_MONITORING
                XmlElement elGovernment = (XmlElement) el.SelectSingleNode("GOVERNMENT_MONITORING");
                if (null != elGovernment) 
                {

                    if (isCoborrower) 
                    {
                        dataApp.aCGender = GenderTypeMap.FromMismo(elGovernment.GetAttribute("GenderType"));
                        dataApp.aCRaceT = (E_aCRaceT) RaceTypeMap.FromMismo(elGovernment.GetAttribute("RaceNationalOriginType"));
                        dataApp.aCORaceDesc = elGovernment.GetAttribute("OtherRaceNationalOriginDescription");
                        dataApp.aCHispanicT = HispanicTypeMap.FromMismo(elGovernment.GetAttribute("HMDAEthnicityType"));

                        XmlNodeList hmdaRaceList = elGovernment.SelectNodes("HMDA_RACE");
                        foreach (XmlElement elHmdaRace in hmdaRaceList) 
                        {
                            switch (elHmdaRace.GetAttribute("_Type")) 
                            {
                                case "AmericanIndianOrAlaskaNative":
                                    dataApp.aCIsAmericanIndian = true;
                                    break;
                                case "Asian":
                                    dataApp.aCIsAsian = true;
                                    break;
                                case "BlackOrAfricanAmerican":
                                    dataApp.aCIsBlack = true;
                                    break;
                                case "NativeHawaiianOrOtherPacificIslander":
                                    dataApp.aCIsPacificIslander = true;
                                    break;
                                case "White":
                                    dataApp.aCIsWhite = true;
                                    break;
                            } // switch (elHmdaRace.GetAttribute("_Type")) 
                        } // foreach (XmlElement elHmdaRace in hmdaRaceList) 
                    }
                    else 
                    {
                        dataApp.aBGender = GenderTypeMap.FromMismo(elGovernment.GetAttribute("GenderType"));
                        dataApp.aBRaceT = (E_aBRaceT) RaceTypeMap.FromMismo(elGovernment.GetAttribute("RaceNationalOriginType"));
                        dataApp.aBORaceDesc = elGovernment.GetAttribute("OtherRaceNationalOriginDescription");
                        dataApp.aBHispanicT = HispanicTypeMap.FromMismo(elGovernment.GetAttribute("HMDAEthnicityType"));

                        XmlNodeList hmdaRaceList = elGovernment.SelectNodes("HMDA_RACE");
                        foreach (XmlElement elHmdaRace in hmdaRaceList) 
                        {
                            switch (elHmdaRace.GetAttribute("_Type")) 
                            {
                                case "AmericanIndianOrAlaskaNative":
                                    dataApp.aBIsAmericanIndian = true;
                                    break;
                                case "Asian":
                                    dataApp.aBIsAsian = true;
                                    break;
                                case "BlackOrAfricanAmerican":
                                    dataApp.aBIsBlack = true;
                                    break;
                                case "NativeHawaiianOrOtherPacificIslander":
                                    dataApp.aBIsPacificIslander = true;
                                    break;
                                case "White":
                                    dataApp.aBIsWhite = true;
                                    break;
                            } // switch (elHmdaRace.GetAttribute("_Type")) 
                        } // foreach (XmlElement elHmdaRace in hmdaRaceList) 
                    }
                }
                #endregion

                #region PRESENT_HOUSING_EXPENSE
                XmlNodeList housingExpenseList = el.SelectNodes("PRESENT_HOUSING_EXPENSE");
                foreach (XmlElement elHousingExpense in housingExpenseList) 
                {
                    switch (elHousingExpense.GetAttribute("HousingExpenseType")) 
                    {
                        case "FirstMortgagePrincipalAndInterest":
                            dataApp.aPres1stM_rep = elHousingExpense.GetAttribute("_PaymentAmount") ;
                            break ;
                        case "HazardInsurance":
                            dataApp.aPresHazIns_rep = elHousingExpense.GetAttribute("_PaymentAmount") ;
                            break ;
                        case "HomeownersAssociationDuesAndCondominiumFees":
                            dataApp.aPresHoAssocDues_rep = elHousingExpense.GetAttribute("_PaymentAmount") ;
                            break ;
                        case "MI":
                            dataApp.aPresMIns_rep = elHousingExpense.GetAttribute("_PaymentAmount") ;
                            break ;
                        case "OtherHousingExpense":
                            dataApp.aPresOHExp_rep = elHousingExpense.GetAttribute("_PaymentAmount") ;
                            break ;
                        case "OtherMortgageLoanPrincipalAndInterest":
                            dataApp.aPresOFin_rep = elHousingExpense.GetAttribute("_PaymentAmount") ;
                            break ;
                        case "RealEstateTax":
                            dataApp.aPresRealETx_rep = elHousingExpense.GetAttribute("_PaymentAmount") ;
                            break ;
                        case "Rent":
                            dataApp.aPresRent_rep = elHousingExpense.GetAttribute("_PaymentAmount") ;
                            break ;
                    }
                }

                #endregion

                #region SUMMARY
                #endregion

                #region CONTACT_POINT
                XmlNodeList listContactPoint = el.SelectNodes("CONTACT_POINT");
                foreach (XmlElement elContactPoint in listContactPoint) 
                {
                    string roleType = elContactPoint.GetAttribute("_RoleType");
                    string type = elContactPoint.GetAttribute("_Type");
                    string value = elContactPoint.GetAttribute("_Value");
                    if (roleType == "Home" && type == "Email") 
                    {
                        if (isCoborrower)
                            dataApp.aCEmail = value;
                        else
                            dataApp.aBEmail = value;
                    } 
                    else if (roleType == "Mobile" && type == "Phone") 
                    {
                        if (isCoborrower)
                            dataApp.aCCellPhone = value;
                        else
                            dataApp.aBCellPhone = value;
                    }
                    else if (roleType == "Work" && type == "Phone") 
                    {
                        if (isCoborrower)
                            dataApp.aCBusPhone = value;
                        else
                            dataApp.aBBusPhone = value;
                    }

                }
                #endregion

            }
        }
        private void ReadAdditionalCaseData(XmlElement el) 
        {
            if (null == el)
                return;

            XmlElement elTransData = (XmlElement) el.SelectSingleNode("TRANSMITTAL_DATA");
            if (null != elTransData) 
            {

                m_dataLoan.sApprVal_rep = elTransData.GetAttribute("PropertyAppraisedValueAmount");
                m_dataLoan.sSpMarketVal_rep = elTransData.GetAttribute("PropertyEstimatedValueAmount");
            }
        }
        private void ReadAssets(XmlNodeList nodeList)
        {
            foreach(XmlElement el in nodeList)
            {
                // find applicant
                CAppData dataApp = FindAppByID(el.GetAttribute("BorrowerID"));
                if (null == dataApp)
                {
                    Tools.LogWarning("Asset Drop. Cannot find applicant: " + el.GetAttribute("BorrowerID"));
                    continue ;
                }

                IAssetCollection assetColl = dataApp.aAssetCollection;

                E_AssetT assetT = AssetTypeMap.FromMismo(el.GetAttribute("_Type")) ;

                IAsset fields;
                if( CAssetFields.IsTypeOfSpecialRecord( assetT ) )
                {
                    switch( (E_AssetSpecialT) assetT )
                    {
                        case E_AssetSpecialT.LifeInsurance:
                            var lifeIns = assetColl.GetLifeInsurance( true );
                            lifeIns.FaceVal_rep = el.GetAttribute("LifeInsuranceFaceValueAmount") ;
                            fields = lifeIns;
                            break;
                        case E_AssetSpecialT.CashDeposit:
                            // Get the first empty cash deposit slot.
                            var cashdep = assetColl.GetCashDeposit1( false );
                            if( null == cashdep )
                                cashdep = assetColl.GetCashDeposit1( true );
                            else
                            {
                                cashdep = assetColl.GetCashDeposit2( false );
                                if( null == cashdep )
                                    cashdep = assetColl.GetCashDeposit2( true );
                                else
                                {
                                    Tools.LogWarning( "Mismo21.LoanMismo21Reader cannot accept more than 2 cash deposit entries, ignoring the entry." );
                                    continue;
                                }
                            }
                            fields = cashdep;
                            break;
                        case E_AssetSpecialT.Retirement:
                            var retirement = assetColl.GetRetirement( true );
                            fields = retirement;
                            break;
                        case E_AssetSpecialT.Business:
                            var business = assetColl.GetBusinessWorth( true );
                            fields = business;
                            break;
                        default:
                            Tools.LogBug( assetT + " is invalid type, ignoring the entry." );
                            continue;
                    }

                }
                else
                {
                    E_AssetRegularT regularT = (E_AssetRegularT) assetT;
                    // find the duplicate asset (if it exists)
                    var reg = FindRegularAsset(assetColl, el) ;
                    if( null == reg )
                        reg = dataApp.aAssetCollection.AddRegularRecord();
                    reg.AssetT = regularT;
                    reg.AccNm  = el.GetAttribute("_HolderName") ;
                    reg.AccNum = el.GetAttribute("_AccountIdentifier") ;
                    reg.StAddr = el.GetAttribute("_HolderStreetAddress") ;
                    reg.City   = el.GetAttribute( "_HolderCity") ;
                    reg.State  = el.GetAttribute( "_HolderState") ;
                    reg.Zip    = el.GetAttribute( "_HolderPostalCode") ;
                    switch( regularT )
                    {
                        case E_AssetRegularT.OtherIlliquidAsset:
                        case E_AssetRegularT.OtherLiquidAsset:
                            reg.OtherTypeDesc = el.GetAttribute("OtherAssetTypeDescription") ;
                            break;
                    }
                    fields = reg;
                }
				
				
                fields.Val_rep = el.GetAttribute("_CashOrMarketValueAmount") ;
                fields.Desc    = el.GetAttribute("AutomobileMakeDescription") ;
                //WE DON'T HAVE THIS FIELD EXPLICITLY
                //writer.WriteAttributeString("AutomobileModelYear", xxxxx) ;
				
                if (fields.Desc.Length == 0)
                    fields.Desc = el.GetAttribute("OtherAssetTypeDescription") ;
					
                //WE DON'T HAVE THIS FIELD.
                //writer.WriteAttributeString("StockBondMutualFundShareCount", fields.) ;
            }
        }
        private void ReadDownPayment(XmlElement el) 
        {
            if (null == el) return;

            m_dataLoan.sDwnPmtSrc = DownPaymentMap.FromMismo(el.GetAttribute("_Type"));
            m_dataLoan.sDwnPmtSrcExplain = el.GetAttribute("_SourceDescription");
            m_dataLoan.sEquityCalc_rep = el.GetAttribute("_Amount");
        }
        private void ReadGovernmentLoan(XmlElement el) 
        {
            if (null == el) return;

            // 4/28/2004 dd - TODO
        }
        private void ReadInterviewerInformation(XmlElement el) 
        {
            if (null == el) return;

            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
            interviewer.PreparerName = el.GetAttribute("InterviewersName");
            interviewer.Phone        = el.GetAttribute("InterviewersTelephoneNumber");
            interviewer.CompanyName  = el.GetAttribute("InterviewersEmployerName");
            interviewer.StreetAddr   = el.GetAttribute("InterviewersEmployerStreetAddress");
            interviewer.City         = el.GetAttribute("InterviewersEmployerCity");
            interviewer.State        = el.GetAttribute("InterviewersEmployerState");
            interviewer.Zip          = el.GetAttribute("InterviewersEmployerPostalCode");
            interviewer.Update();

            // 4/28/2004 dd - Apply interviewer method to all application.
            int nApps = m_dataLoan.nApps;
            for (int i = 0; i < nApps; i++)
            {
                m_dataLoan.GetAppData(i).aBInterviewMethodT = InterviewerMethodMap.FromMismo(el.GetAttribute("ApplicationTakenMethodType"));
                m_dataLoan.GetAppData(i).aCInterviewMethodT = InterviewerMethodMap.FromMismo(el.GetAttribute("ApplicationTakenMethodType"));
            }


        }
        private void ReadLiabilities(XmlNodeList nodeList) 
        {
            foreach (XmlElement el in nodeList) 
            {
                // find applicant
                CAppData dataApp = FindAppByID(el.GetAttribute("BorrowerID"));
                if (null == dataApp)
                {
                    Tools.LogWarning("Liability Drop. Cannot find applicant: " + el.GetAttribute("BorrowerID"));
                    continue ;
                }

                ILiaCollection liaColl = dataApp.aLiaCollection;
                E_DebtT debtT = LiabilityTypeMap.FromMismo(el.GetAttribute("_Type"));

                ILiability fields = null;
                if (CLiaFields.IsTypeOfSpecialRecord(debtT)) 
                {
                    E_DebtSpecialT debtSpecialT = (E_DebtSpecialT) debtT;
                    switch (debtSpecialT) 
                    {
                        case E_DebtSpecialT.Alimony:
                            ILiabilityAlimony alimony = liaColl.GetAlimony(true);
                            alimony.OwedTo = el.GetAttribute("AlimonyOwedToName");
                            fields = alimony;
                            break;
                        case E_DebtSpecialT.JobRelatedExpense:
                            ILiabilityJobExpense jobExp = liaColl.CreateNewJobExpenseAtFirstAvailSlot();
                            if (null == jobExp) 
                            {
                                Tools.LogWarning("Mismo23Reader cannot accept more than 2 job releated expense entries, ingoring entry.");
                                continue;
                            }
                            jobExp.ExpenseDesc = el.GetAttribute("_HolderName");
                            fields = jobExp;
                            break;
                        case E_DebtSpecialT.ChildSupport:
                            // Child support has no special attributes.
                            fields = liaColl.GetChildSupport(true);
                            break;
                        default:
                            Tools.LogWarning(debtSpecialT + " is invalid liability type.");
                            continue;

                    }
                }  // if (CLiaFields.IsTypeOfSpecialRecord(debtT)) 
                else 
                {
                    ILiabilityRegular liaReg = FindRegularLiability(liaColl, el);
                    if (null == liaReg) 
                    {
                        liaReg = liaColl.AddRegularRecord();
                    }
                    liaReg.ComNm = el.GetAttribute("_HolderName");
                    liaReg.ComAddr = el.GetAttribute("_HolderStreetAddress");
                    liaReg.ComCity = el.GetAttribute("_HolderCity");
                    liaReg.ComState = el.GetAttribute("_HolderState");
                    liaReg.ComZip = el.GetAttribute("_HolderPostalCode");
                    liaReg.AccNum = el.GetAttribute("_AccountIdentifier");
                    liaReg.Bal_rep = el.GetAttribute("_UnpaidBalanceAmount");
                    liaReg.IsPiggyBack = el.GetAttribute("SubjectLoanResubordinationIndicator") == "Y";
                    fields = liaReg;

                }
                if (null != fields) 
                {
                    // 1/25/2005 dd - TODO: BorrowerID is not always SSN
//                    if (el.GetAttribute("BorrowerID").Replace("-", "") == dataApp.aBSsn.Replace("-", ""))
//                        fields.OwnerT = E_LiaOwnerT.Borrower;
//                    else
//                        fields.OwnerT = E_LiaOwnerT.CoBorrower;

                    // 4/28/2004 dd - TODO: Handle REO_ID

                    fields.NotUsedInRatio = el.GetAttribute("_ExclusionIndicator") == "Y";
                    fields.Pmt_rep = el.GetAttribute("_MonthlyPaymentAmount");
                    fields.WillBePdOff = el.GetAttribute("_PayoffStatusIndicator") == "Y";
                    fields.RemainMons_rep = el.GetAttribute("_RemainingTermMonths");
                    fields.Update();
                }

            }
        }
        private void ReadLoanProductData(XmlElement el) 
        {
            if (null == el) return ;

            XmlElement elArm = (XmlElement) el.SelectSingleNode("ARM") ;
            if (null != elArm)
            {
                m_dataLoan.sRAdjIndexR_rep = elArm.GetAttribute("_IndexCurrentValuePercent") ;
                m_dataLoan.sRAdjMarginR_rep = elArm.GetAttribute("_IndexMarginPercent") ;
                // X writer.WriteAttributeString("_IndexType", xxxx) ; We don't have this.
                m_dataLoan.sQualIRLckd = true;
                m_dataLoan.sQualIR_rep = elArm.GetAttribute("_QualifyingRatePercent") ;
                // X writer.WriteAttributeString("PaymentAdjustmentLifetimeCapAmount", xxxx) ; We don't have this.
                m_dataLoan.sPmtAdjMaxBalPc_rep = elArm.GetAttribute("PaymentAdjustmentLifetimeCapPercent") ;
                m_dataLoan.sRAdjLifeCapR_rep = elArm.GetAttribute("RateAdjustmentLifetimeCapPercent") ;
                // X writer.WriteAttributeString("_ConversionOptionIndicator", xxxx) ;	I don't know what this is, I don't think we have the field.
            }
			
            //writer.WriteStartElement("BUYDOWN") ;
            // X writer.WriteAttributeString("_BaseDateType", xxxx) ; we don't have this
            // T writer.WriteAttributeString("_ChangeFrequencyMonths", xxxx) ; sBuydwnMon1, not sure.
            // X writer.WriteAttributeString("_ContributorType", xxxx) ; We don't have this
            // X writer.WriteAttributeString("_ContributorTypeOtherDescription", xxxx) ; We don't have this.
            // writer.WriteAttributeString("_DurationMonths", xxxx) ; sBuydwnMon1, not sure
            // T writer.WriteAttributeString("_IncreaseRatePercent", xxxx) ; sBuydwnR1, not sure.
            // X writer.WriteAttributeString("_LenderFundingIndicator", xxxx) ; We don't have this
            // T writer.WriteAttributeString("_OriginalBalanceAmount", xxxx) ; sFinalLAmt, not sure.
            // X writer.WriteAttributeString("_SubsidyCalculationType", xxxx) ; we don't have this
            // X writer.WriteAttributeString("_PermanentIndicator", xxxx) ; we don't have this.

            // X writer.WriteStartElement("_SUBSIDY_SCHEDULE") ; we don't have these.
            //writer.WriteAttributeString("_PeriodIdentifier", xxxx) ;
            //writer.WriteAttributeString("_PeriodicPaymentEffectiveDate", xxxx) ;
            //writer.WriteAttributeString("_PeriodicPaymentSubsidyAmount", xxxx) ;
            //writer.WriteAttributeString("_PeriodicTerm", xxxx) ;
            //writer.WriteEndElement() ; // _SUBSIDY_SCHEDULE
            //writer.WriteEndElement() ; // BUYDOWN

            XmlElement elFeature = (XmlElement) el.SelectSingleNode("LOAN_FEATURES") ;
            if (null != elFeature)
            {
                m_dataLoan.sGseSpT = GSEPropertyTypeMap.FromMismo(elFeature.GetAttribute("GSEPropertyType"));
                m_dataLoan.sSpT = PropertyTypeMap.FromMismo(elFeature.GetAttribute("GSEPropertyType")) ;
                m_dataLoan.sLienPosT = LienPositionMap.FromMismo(elFeature.GetAttribute("LienPriorityType")) ;
                m_dataLoan.sAssumeLT = AssumabilityMap.FromMismo(elFeature.GetAttribute("AssumabilityIndicator")) ;
                m_dataLoan.sDue_rep = elFeature.GetAttribute("BalloonLoanMaturityTermMonths") ; 
                m_dataLoan.sAggregateAdjRsrv_rep = elFeature.GetAttribute("EscrowAggregateAccountingAdjustmentAmount") ;

                // -- THINH: todo: needs population
                // X writer.WriteAttributeString("CounselingConfirmationIndicator", xxxx) ; We don't have this
                // X writer.WriteAttributeString("CounselingConfirmationType", xxxx) ; we don't have this
                // X writer.WriteAttributeString("DownPaymentOptionType", xxxx) ; we don't have this
                // X writer.WriteAttributeString("EscrowWaiverIndicator", xxxx) ; we don't have this
                // X writer.WriteAttributeString("EscrowCushionNumberOfMonthsCount", xxxx) ; we don't have this
                // X writer.WriteAttributeString("FREOfferingIdentifier", xxxx) ; we don't have this
                // X writer.WriteAttributeString("FNMProductPlanIndentifier", xxxx) ; we don't have this
                // X writer.WriteAttributeString("HELOCMaximumBalanceAmount", xxxx) ; we don't have this
                // X writer.WriteAttributeString("LoanDocumentationType", xxxx) ; we don't have this
                // X writer.WriteAttributeString("LoanRepaymentType", xxxx) ; we don't have this
                m_dataLoan.sEstCloseD_rep = elFeature.GetAttribute( "LoanScheduledClosingDate") ;
                m_dataLoan.sEstCloseDLckd = true;
                // X writer.WriteAttributeString("MICoveragePercent", xxxx) ; we don't have this
                // X writer.WriteAttributeString("MICompanyNameType", xxxx) ; we don't have this
                m_dataLoan.sPmtAdjMaxBalPc_rep = elFeature.GetAttribute("NegativeAmortizationLimitPercent") ;
                //writer.WriteAttributeString("PaymentFrequencyType", "Monthly") ;
                m_dataLoan.sPrepmtPenaltyT = PrepayPenaltyMap.FromMismo(elFeature.GetAttribute("PrepaymentPenaltyIndicator")) ;
                // X writer.WriteAttributeString("PrepaymentRestrictionIndicator", xxxx) ; don't know what this is, sPrepmtRefundT? Also see what we export for PrepaymentFinanceChargeRefundableIndicator
                m_dataLoan.sLpTemplateNm = elFeature.GetAttribute("ProductName") ; 
                m_dataLoan.sSchedDueD1_rep = elFeature.GetAttribute("ScheduledFirstPaymentDate") ;
                // 9/26/2013 gf - opm 130112 1st pmt date is now a calculated
                // field. Lock to maintain imported date.
                m_dataLoan.sSchedDueD1Lckd = true;
                m_dataLoan.sHasDemandFeature = elFeature.GetAttribute("DemandFeatureIndicator") == "Y" ;
                m_dataLoan.sIPiaDy_rep =elFeature.GetAttribute("EstimatedPrepaidDays") ;
                // X writer.WriteAttributeString("EstimatedPrepaidDaysPaidByType", xxxx) ; We don't have this.
                // X writer.WriteAttributeString("EstimatedPrepaidDaysPaidByOtherTypeDescription", xxxx) ; we don't have this
                // X writer.WriteAttributeString("InitialPaymentRatePercent", xxxx) ; Not sure what this is.
                m_dataLoan.sPrepmtRefundT = PrepayRefundChargeMap.FromMismo(elFeature.GetAttribute("PrepaymentFinanceChargeRefundableIndicator")) ;
                // X writer.WriteAttributeString("RefundableApplicationFeeIndicator", xxxx) ; don't know what this is
                // X writer.WriteAttributeString("RequiredDepositIndicator", xxxx) ; don't have this
                m_dataLoan.sTerm_rep = elFeature.GetAttribute("OriginalBalloonTermMonths") ;
            }

            /* -- todo
            writer.WriteStartElement("PAYMENT_ADJUSTMENT") ;
            writer.WriteAttributeString("FirstPaymentAdjustmentMonths", loanData.sPmtAdjCapMon_rep) ; 
            // X writer.WriteAttributeString("_Amount", xxxx) ; We don't have this
            // T writer.WriteAttributeString("_CalculationType", xxxx) ; "AddPercentToCurrentPaymentAmount" is the closest one but our pmt adjustment is for controling the payment increase resulted of adj rate.
            // T writer.WriteAttributeString("_DurationMonths", xxxx) ; sPmtAdjRecastPeriodMon, not sure.
            // T writer.WriteAttributeString("_Percent", xxxx) ; we don't have this, maybe just 0
            // X writer.WriteAttributeString("_PeriodicCapAmount", xxxx) ; We don't have this
            //writer.WriteAttributeString("_PeriodicCapPercent", loanData.sPmtAdjCapR_rep) ;
            // X writer.WriteAttributeString("_PeriodNumber", xxxx) ;don't know what this is.
            // T writer.WriteAttributeString("SubsequentPaymentAdjustmentMonths", xxxx) ;sPmtAdjCapMon, what out for importing, used in more than one place
            writer.WriteEndElement() ; // PAYMENT_ADJUSTMENT
            */

            /* todo
            writer.WriteStartElement("RATE_ADJUSTMENT") ;
            // T writer.WriteAttributeString("FirstRateAdjustmentMonths", xxxx) ;
            // X writer.WriteAttributeString("_CalculationType", xxxx) ; We always use IndexPlusMargin
            // X writer.WriteAttributeString("_DurationMonths", xxxx) ; sRAdjCapMon , not sure
            // T writer.WriteAttributeString("_Percent", xxxx) ;0
            // X writer.WriteAttributeString("_PeriodNumber", xxxx) ; don't know what this is.
            // writer.WriteAttributeString("_SubsequentCapPercent", xxxx) ;we don't have this
            // X writer.WriteAttributeString("SubsequentRateAdjustmentMonths", xxxx) ;sRAdjCapMon, used multiple places
            // T writer.WriteAttributeString("_InitialCapPercent", xxxx) ; sRAdj1stCapR
            writer.WriteEndElement() ; // RATE_ADJUSTMENT
            */

        }
        private void ReadMortgageTerms(XmlElement el) 
        {
            if (null == el) return;

            m_dataLoan.sAgencyCaseNum = el.GetAttribute("AgencyCaseIdentifier");
            m_dataLoan.sFinMethT = FinanceMethodMap.FromMismo(el.GetAttribute("LoanAmortizationType"));
            if (m_dataLoan.sFinMethT == E_sFinMethT.ARM)
                m_dataLoan.sFinMethDesc = el.GetAttribute("ARMTypeDescription");
            else
                m_dataLoan.sFinMethDesc = el.GetAttribute("OtherAmortizationTypeDescription");

            m_dataLoan.sLT = LoanTypeMap.FromMismo(el.GetAttribute("MortgageType"));
            m_dataLoan.sLTODesc = el.GetAttribute("OtherMortgageTypeDescription");
            m_dataLoan.sLenderCaseNum = el.GetAttribute("LenderCaseIdentifier");
            m_dataLoan.sTerm_rep = el.GetAttribute("LoanAmortizationTermMonths");
            m_dataLoan.sNoteIR_rep = el.GetAttribute("RequestedInterestRatePercent");
            m_dataLoan.sEstCloseD_rep = el.GetAttribute("LoanEstimatedClosingDate");
            m_dataLoan.sEstCloseDLckd = true;
            m_dataLoan.sLAmtCalc_rep = el.GetAttribute("BaseLoanAmount");
            m_dataLoan.sLAmtLckd = true;
            // 4/28/2004 dd - Don't kow what is the different between BorrowerRequestedLoanAmount and BaseLoanAmount
            //el.GetAttribute("BorrowerRequestedLoanAmount");

        }
        private void ReadProperty(XmlElement el) 
        {
            if (null == el)
                return;
            
            m_dataLoan.sSpAddr       = el.GetAttribute("_StreetAddress");
            m_dataLoan.sSpCity       = el.GetAttribute("_City");
            m_dataLoan.sSpState      = el.GetAttribute("_State");
            m_dataLoan.sSpZip        = el.GetAttribute("_PostalCode");
            m_dataLoan.sUnitsNum_rep = el.GetAttribute("_FinancedNumberOfUnits");
            m_dataLoan.sYrBuilt      = el.GetAttribute("_StructureBuiltYear");
            m_dataLoan.sSpCounty     = el.GetAttribute("_County");
            XmlElement legalDesc = (XmlElement) el.SelectSingleNode("_LEGAL_DESCRIPTION");
            if (null != legalDesc) 
            {
                m_dataLoan.sSpLegalDesc = legalDesc.GetAttribute("_TextDescription");
            }

        }
        private void ReadProposedHousingExpense(XmlNodeList nodeList) 
        {
            foreach (XmlElement el in nodeList) 
            {
                switch (el.GetAttribute("HousingExpenseType")) 
                {
                    case "FirstMortgagePrincipalAndInterest":
                        // 4/28/2004 dd - ReadOnly
                        break;
                    case "GroundRent":
                        break;
                    case "HazardInsurance":
                        m_dataLoan.sProHazInsMb_rep = el.GetAttribute("_PaymentAmount");
                        break;
                    case "HomeownersAssociationDuesAndCondominiumFees":
                        m_dataLoan.sProHoAssocDues_rep = el.GetAttribute("_PaymentAmount");
                        break;
                    case "MI":
                        //m_dataLoan.sProMInsMb_rep = el.GetAttribute("_PaymentAmount");
                        string payment = el.GetAttribute("_PaymentAmount");
                        if (payment != m_dataLoan.sProMIns_rep)
                        {
                            m_dataLoan.sProMIns_rep = payment;
                            m_dataLoan.sProMInsLckd = true;
                        }
                        break;
                    case "OtherHousingExpense":
                        m_dataLoan.sProOHExp_rep = el.GetAttribute("_PaymentAmount");
                        break;
                    case "OtherMortgageLoanPrincipalAndInterest":
                        // 4/28/2004 dd - Readonly
                        break;
                    case "RealEstateTax":
                        m_dataLoan.sProRealETxMb_rep = el.GetAttribute("_PaymentAmount");
                        break;
                }
            }
        }
        private void ReadREOProperties(XmlNodeList nodeList) 
        {
            foreach (XmlElement el in nodeList) 
            {
                // find applicant
                CAppData dataApp = FindAppByID(el.GetAttribute("BorrowerID")) ;
                if (null == dataApp)
                {
                    Tools.LogWarning("REO drop. Cannot find applicant: " + el.GetAttribute("BorrowerID")) ;
                    continue ;
                }

                E_ReoStatusT statusT = ReoDispositionMap.FromMismo(el.GetAttribute("_DispositionStatusType"));
                var reColl = dataApp.aReCollection;

                var fields = FindREO(reColl, el);
                if (null == fields)
                    fields = reColl.AddRecord(statusT);

                // 1/25/2005 dd - TODO: BorrowerID is not always SSN
//                if (el.GetAttribute("BorrowerID").Replace("-", "") == dataApp.aCSsn.Replace("-", ""))
//                    fields.ReOwnerT = E_ReOwnerT.CoBorrower;
//                else
//                    fields.ReOwnerT = E_ReOwnerT.Borrower;

                // 4/28/2004 dd - TODO: Map REO to Liability.   LiabilityID

                fields.Addr            = el.GetAttribute("_StreetAddress");
                fields.City            = el.GetAttribute("_City");
                fields.State           = el.GetAttribute("_State");
                fields.Zip             = el.GetAttribute("_PostalCode");
                fields.TypeT            = REOGsePropertyTypeMap.FromMismo(el.GetAttribute("_GSEPropertyType"));
                fields.StatT           = statusT;
                fields.MPmt_rep        = el.GetAttribute("_LienInstallmentAmount");
                fields.MAmt_rep        = el.GetAttribute("_LienUPBAmount");
                fields.HExp_rep        = el.GetAttribute("_MaintenanceExpenseAmount");
                fields.Val_rep         = el.GetAttribute("_MarketValueAmount");
                fields.GrossRentI_rep  = el.GetAttribute("_RentalIncomeGrossAmount");
                if (el.GetAttribute("_SubjectIndicator") == "Y") 
                {
                    fields.IsSubjectProp = true;
                    m_dataLoan.sSpAddr = fields.Addr;
                    m_dataLoan.sSpCity = fields.City;
                    m_dataLoan.sSpAddr = fields.State;
                    m_dataLoan.sSpZip = fields.Zip;

                }
                fields.Update();



            }
        }
        private void ReadTitleHolder(XmlNodeList nodeList) 
        {
            int index = 0;
            CAppData dataApp = m_dataLoan.GetAppData(0); // 4/28/2004 dd - Get primary app for now.
            foreach (XmlElement el in nodeList) 
            {
                if (index == 0) 
                {
                    dataApp.aTitleNm1 = el.GetAttribute("_Name");
                    index++;
                } 
                else 
                {
                    dataApp.aTitleNm2 += el.GetAttribute("_Name") + " ";
                }
            }
        }
        private void ReadTransactionDetail(XmlElement el) 
        {
            if (null == el) return;

            m_dataLoan.sAltCost_rep = el.GetAttribute("AlterationsImprovementsAndRepairsAmount");
            m_dataLoan.sLDiscnt1003_rep = el.GetAttribute("BorrowerPaidDiscountPointsTotalAmount");
            m_dataLoan.sLDiscnt1003Lckd = true;

            m_dataLoan.sTotEstCcNoDiscnt1003_rep = el.GetAttribute("EstimatedClosingCostsAmount");
            m_dataLoan.sTotEstCc1003Lckd = true;

            m_dataLoan.sFfUfmip1003_rep = el.GetAttribute("MIAndFundingFeeTotalAmount") ;
            m_dataLoan.sFfUfmip1003Lckd = true;

            m_dataLoan.sTotEstPp1003_rep = el.GetAttribute("PrepaidItemsEstimatedAmount");
            m_dataLoan.sTotEstPp1003Lckd = true;

            m_dataLoan.sPurchPrice_rep = el.GetAttribute("PurchasePriceAmount");

            m_dataLoan.sRefPdOffAmt1003_rep = el.GetAttribute("RefinanceIncludingDebtsToBePaidOffAmount");
            m_dataLoan.sRefPdOffAmt1003Lckd = true;

            m_dataLoan.sFHASalesConcessions_rep = el.GetAttribute("SalesConcessionAmount") ;
            m_dataLoan.sTotCcPbs_rep = el.GetAttribute("SellerPaidClosingCostsAmount") ;
            m_dataLoan.sTotCcPbsLocked = true;

            m_dataLoan.sLoads1003LineLFromAdjustments = false; // opm 212045
            int index = 0;
            XmlNodeList creditList = el.SelectNodes("PURCHASE_CREDIT");
            foreach (XmlElement elCredit in creditList) 
            {
                if (index == 0) 
                {
                    m_dataLoan.sOCredit1Desc = PurchaseCreditMap.FromMismo(elCredit.GetAttribute("_Type"), elCredit.GetAttribute("_SourceType"));
                    m_dataLoan.sOCredit1Amt_rep = elCredit.GetAttribute("_Amount");
                } 
                else if (index == 1) 
                {
                    m_dataLoan.sOCredit2Desc = PurchaseCreditMap.FromMismo(elCredit.GetAttribute("_Type"), elCredit.GetAttribute("_SourceType"));
                    m_dataLoan.sOCredit2Amt_rep = elCredit.GetAttribute("_Amount");

                } 
                else if (index == 2) 
                {
                    m_dataLoan.sOCredit3Desc = PurchaseCreditMap.FromMismo(elCredit.GetAttribute("_Type"), elCredit.GetAttribute("_SourceType"));
                    m_dataLoan.sOCredit3Amt_rep = elCredit.GetAttribute("_Amount");

                }
                else if (index == 3) 
                {
                    m_dataLoan.sOCredit4Desc = PurchaseCreditMap.FromMismo(elCredit.GetAttribute("_Type"), elCredit.GetAttribute("_SourceType"));
                    m_dataLoan.sOCredit4Amt_rep = elCredit.GetAttribute("_Amount");

                } 
                else 
                {
                    break;
                }
                index++;
            }
        }

        private void ReadLoanPurposeData(XmlElement el)
        {
            if (null == el) return ;
            //TODO: Review this as title manner is no longer per loan in LendersOffice.
            m_dataLoan.GetAppData(0).aManner = el.GetAttribute("GSETitleMannerHeldDescription") ; 
            m_dataLoan.sLPurposeT = LoanPurposeTypeMap.FromMismo(el.GetAttribute("_Type")) ;
            m_dataLoan.sOLPurposeDesc = el.GetAttribute("OtherLoanPurposeDescription") ;
			
            m_dataLoan.sLeaseHoldExpireD_rep = el.GetAttribute("PropertyLeaseholdExpirationDate") ;
            m_dataLoan.sEstateHeldT = EstateHeldTypeMap.FromMismo(el.GetAttribute("PropertyRightsType")) ;
            m_dataLoan.GetAppData(0).aOccT = OccupancyTypeMap.FromMismo(el.GetAttribute("PropertyUsageType")) ;

            XmlElement xeRefi = (XmlElement) el.SelectSingleNode("CONSTRUCTION_REFINANCE_DATA") ;
            if (null != xeRefi)
            {
                switch(m_dataLoan.sLPurposeT)
                {
                    case E_sLPurposeT.VaIrrrl:
                    case E_sLPurposeT.FhaStreamlinedRefinance:
                    case E_sLPurposeT.Refin:
                    case E_sLPurposeT.RefinCashout:
                        m_dataLoan.sSpAcqYr = xeRefi.GetAttribute("PropertyAcquiredYear") ;
                        m_dataLoan.sSpOrigC_rep = xeRefi.GetAttribute("PropertyOriginalCostAmount") ;
                        m_dataLoan.sSpLien_rep = xeRefi.GetAttribute("PropertyExistingLienAmount") ;
                        m_dataLoan.sRefPurpose = GSERefiPurposeMap.FromMismo(xeRefi.GetAttribute("GSERefinancePurposeType")) ;
                        m_dataLoan.sProdCashoutAmt_rep = xeRefi.GetAttribute( "FRECashOutAmount") ;
                        m_dataLoan.sSpImprovC_rep = xeRefi.GetAttribute("RefinanceImprovementCostsAmount") ;
                    switch (xeRefi.GetAttribute("RefinanceImprovementsType"))
                    {
                        case "Made":
                            m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.Made ;
                            break ;
                        case "ToBeMade":
                            m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.ToBeMade ;
                            break ;
                        default:
                            m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.LeaveBlank ;
                            break ;
                    }
                        m_dataLoan.sSpImprovDesc = xeRefi.GetAttribute("RefinanceProposedImprovementsDescription") ;
                        break ;
                    case E_sLPurposeT.Construct:
                    case E_sLPurposeT.ConstructPerm:
                        m_dataLoan.sLotAcqYr = xeRefi.GetAttribute("PropertyAcquiredYear") ;
                        m_dataLoan.sLotOrigC_rep = xeRefi.GetAttribute("LandOriginalCostAmount") ;
                        m_dataLoan.sLotLien_rep = xeRefi.GetAttribute( "PropertyExistingLienAmount") ;
                        m_dataLoan.sLotVal_rep = xeRefi.GetAttribute("LandEstimatedValueAmount") ;
                        m_dataLoan.sLotImprovC_rep = xeRefi.GetAttribute("ConstructionImprovementCostsAmount") ;
                        // X THINH: todo: not available.  We don't have these.
                        //writer.WriteAttributeString("ConstructionPeriodInterestRatePercent", xxxx) ;
                        //writer.WriteAttributeString("ConstructionPeriodNumberOfMonthsCount", xxxx) ;
                        break ;
                    default:
                        break ;
                }
            }
        }

        private IRegularEmploymentRecord FindRegularEmployment(IEmpCollection empColl, XmlElement el)
        {

            var subcoll = empColl.GetSubcollection( true, E_EmpGroupT.Regular );
            foreach (IRegularEmploymentRecord record in subcoll)
            {
                // determine if there's a match
                if (record.EmplrNm.Length > 0 && record.EmplrNm == el.GetAttribute("_Name") && 
                    record.JobTitle == el.GetAttribute("EmploymentPositionDescription"))
                    return record ;
            }

            return null ;	// nothing found
        }

        private IAssetRegular FindRegularAsset(IAssetCollection assetColl, XmlElement el)
        {
            // find duplicate assets
            E_AssetT assetT = AssetTypeMap.FromMismo(el.GetAttribute("_Type")) ;
            var subcoll = assetColl.GetSubcollection( true, E_AssetGroupT.Regular );
            foreach( var item in subcoll )
            {
                var fields = (IAssetRegular)item;
                if (fields.AssetT == (E_AssetRegularT) assetT)
                {
                    string s = el.GetAttribute("OtherAssetTypeDescription") ;
                    if (s.Length > 0 && s == fields.Desc)
                        return fields ;
                    s = el.GetAttribute("_AccountIdentifier") ;
                    if (s.Length > 0 && s == fields.AccNum.Value)
                        return fields ;
                    s = el.GetAttribute("AutomobileMakeDescription") ;
                    if (s.Length > 0 && s == fields.Desc)
                        return fields ;
                }
            }
            return null;

        }
        private ILiabilityRegular FindRegularLiability(ILiaCollection liaColl, XmlElement el) 
        {
            E_DebtT debtT = LiabilityTypeMap.FromMismo(el.GetAttribute("_Type"));

            var subcoll = liaColl.GetSubcollection(true, E_DebtGroupT.Regular);
            foreach (ILiabilityRegular reg in subcoll) 
            {
                if (reg.RecordId.ToString() == el.GetAttribute("_ID"))
                    return reg;
                if (reg.AccNum.Value.Length > 0 && reg.AccNum.Value == el.GetAttribute("_AccountIdentifier"))
                    return reg;
            }
            return null;

        }
        private IRealEstateOwned FindREO(IReCollection reColl, XmlElement el) 
        {
            var subColl = reColl.GetSubcollection(true, E_ReoGroupT.All);
            foreach (var item in subColl) 
            {
                var fields = (IRealEstateOwned)item;

                if (fields.RecordId.ToString() == el.GetAttribute("REO_ID"))
                    return fields;

                if (fields.Addr.Length > 0 && fields.Addr == el.GetAttribute("_StreetAddress"))
                    return fields;

            }
            return null;
        }

        private static string GetOtherMonthlyIncomeDescription(string desc)
        {
            E_aOIDescT descriptionType;
            switch (desc)
            {
                case "AlimonyChildSupport":
                    descriptionType = E_aOIDescT.AlimonyChildSupport;
                    break;
                case "AutomobileExpenseAccount":
                    descriptionType = E_aOIDescT.AutomobileExpenseAccount;
                    break;
                case "FosterCare":
                    descriptionType = E_aOIDescT.FosterCare;
                    break;
                case "NotesReceivableInstallment":
                    descriptionType = E_aOIDescT.NotesReceivableInstallment;
                    break;
                case "Pension":
                    descriptionType = E_aOIDescT.PensionRetirement;
                    break;
                case "SocialSecurity":
                    descriptionType = E_aOIDescT.SocialSecurityDisability;
                    break;
                case "SubjectPropertyNetCashFlow":
                    descriptionType = E_aOIDescT.SubjPropNetCashFlow;
                    break;
                case "Trust":
                    descriptionType = E_aOIDescT.Trust;
                    break;
                case "Unemployment":
                    descriptionType = E_aOIDescT.UnemploymentWelfare;
                    break;
                case "PublicAssistance":
                    descriptionType = E_aOIDescT.PublicAssistance;
                    break;
                case "VABenefitsNonEducational":
                    descriptionType = E_aOIDescT.VABenefitsNonEducation;
                    break;
                case "MilitaryBasePay":
                    descriptionType = E_aOIDescT.MilitaryBasePay;
                    break;
                case "MilitaryRationsAllowance":
                    descriptionType = E_aOIDescT.MilitaryRationsAllowance;
                    break;
                case "MilitaryFlightPay":
                    descriptionType = E_aOIDescT.MilitaryFlightPay;
                    break;
                case "MilitaryHazardPay":
                    descriptionType = E_aOIDescT.MilitaryHazardPay;
                    break;
                case "MilitaryClothesAllowance":
                    descriptionType = E_aOIDescT.MilitaryClothesAllowance;
                    break;
                case "MilitaryQuartersAllowance":
                    descriptionType = E_aOIDescT.MilitaryQuartersAllowance;
                    break;
                case "MilitaryPropPay":
                    descriptionType = E_aOIDescT.MilitaryPropPay;
                    break;
                case "MilitaryOverseasPay":
                    descriptionType = E_aOIDescT.MilitaryOverseasPay;
                    break;
                case "MilitaryCombatPay":
                    descriptionType = E_aOIDescT.MilitaryCombatPay;
                    break;
                case "MilitaryVariableHousingAllowance":
                    descriptionType = E_aOIDescT.MilitaryVariableHousingAllowance;
                    break;
                case "HousingChoiceVoucherProgramSection8":
                    descriptionType = E_aOIDescT.HousingChoiceVoucher;
                    break;
                case "NonBorrowerHouseholdIncome":
                    descriptionType = E_aOIDescT.NonBorrowerHouseholdIncome;
                    break;
                case "AccessoryUnitIncome":
                    descriptionType = E_aOIDescT.AccessoryUnitIncome;
                    break;
                case "OtherTypesOfIncome":
                    descriptionType = E_aOIDescT.Other;
                    break;
                default:
                    return desc;
            }

            return OtherIncome.GetDescription(descriptionType);
        }

        private CAppData FindAppBySsn(string ssn) 
        {
            ssn = ssn.Replace("-", "").TrimWhitespaceAndBOM(); // 4/27/2004 dd - Remove any dash in SSN
//            joinSsn = joinSsn.Replace("-", "");

            for (int i = 0; i < m_dataLoan.nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                string aBSsn = dataApp.aBSsn.Replace("-", "").TrimWhitespaceAndBOM();
                string aCSsn = dataApp.aCSsn.Replace("-", "").TrimWhitespaceAndBOM();
                if (ssn == aBSsn || ssn == aCSsn)
                    return dataApp;

                else if (m_dataLoan.nApps == 1) 
                {
                    /// handle case where this is the first app being inserted into 
                    /// the system since LO automatically creates a new App for you 
                    /// already
                    if (aBSsn == "" && aCSsn == "")
                        return dataApp ;
                }
            }
            return null;

        }
	}
}
