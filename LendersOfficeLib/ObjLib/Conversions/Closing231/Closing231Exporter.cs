/// Author: David Dao
namespace LendersOffice.Conversions.Closing231
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;

    public class Closing231Exporter
    {
        private CPageData m_dataLoan = null;
        private CPageData m_dataLoanWithGfeArchiveApplied = null;
        private string m_lenderLoanIdentifier;

        private static CPageData GetPageData(Guid slid)
        {
            return new CPageData(slid, "Closing231Exporter",
             CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(Closing231Exporter)).Union(
             CPageBase.TEMP_GFEArchiveFieldNamesFrom2013Q3Release)
             );
        }

        public Closing231Exporter(Guid sLId, string lenderLoanIdentifier)
        {
            m_dataLoanWithGfeArchiveApplied = GetPageData(sLId);
            m_dataLoanWithGfeArchiveApplied.InitLoad();

            if (m_dataLoanWithGfeArchiveApplied.BrokerDB.IsGFEandCoCVersioningEnabled && m_dataLoanWithGfeArchiveApplied.LastDisclosedGFEArchive != null)
            {
                m_dataLoanWithGfeArchiveApplied.ApplyGFEArchive(m_dataLoanWithGfeArchiveApplied.LastDisclosedGFEArchive);
            }
            m_dataLoanWithGfeArchiveApplied.SetFormatTarget(FormatTarget.MismoClosing);

            m_dataLoan = GetPageData(sLId);
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            m_lenderLoanIdentifier = lenderLoanIdentifier;

        }

        public string Export() 
        {
            byte[] bytes = null;

            Mismo.Closing.XELoan loan = CreateLoan();
            int contentLength = 0;
            using (MemoryStream stream = new MemoryStream(5000)) 
            {
                XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);
                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                
                loan.GenerateXml(writer);

                writer.Flush();
                contentLength = (int) stream.Position;

                bytes = stream.GetBuffer();
            }
            if (null != bytes) 
            {
                return System.Text.Encoding.ASCII.GetString(bytes, 0, contentLength);
            }
            else 
            {
                return null;
            }
            
        }
        private Mismo.Closing.XELoan CreateLoan() 
        {
            Mismo.Closing.XELoan loan = new Mismo.Closing.XELoan();

            loan.SetApplication(CreateApplication());
            loan.SetClosingDocuments(CreateClosingDocuments());

            return loan;
        }
        private Mismo.Closing.XEClosingDocuments CreateClosingDocuments() 
        {
            Mismo.Closing.XEClosingDocuments closingDocuments = new Mismo.Closing.XEClosingDocuments();
            closingDocuments.AddClosingAgent(CreateClosingAgent(E_AgentRoleT.ClosingAgent));
            closingDocuments.AddClosingAgent(CreateClosingAgent(E_AgentRoleT.Escrow));
            closingDocuments.AddClosingAgent(CreateClosingAgent(E_AgentRoleT.Title));

            closingDocuments.AddClosingInstructions(CreateClosingInstructions());
            closingDocuments.SetInvestor(CreateInvestor());
            closingDocuments.SetLender(CreateLender());
            closingDocuments.SetLoanDetails(CreateLoanDetails());
            closingDocuments.SetRespaServicingData(CreateRespaServicingData());
            closingDocuments.SetMortgageBroker(CreateMortgageBroker());
            closingDocuments.AddCompensation(CreateCompensation());
            closingDocuments.AddRecordableDocument(CreateRecordableDocument());

//            closingDocuments.SetAllongeToNote();
//            closingDocuments.SetBeneficiary();
//            closingDocuments.AddCosigner();
//            closingDocuments.AddEscrowAccountDetail();
//            closingDocuments.SetExecution();
//            closingDocuments.SetLenderBranch();
//            closingDocuments.AddLossPayee();
//            closingDocuments.SetPaymentDetails();
//            closingDocuments.AddPayoff();
//            closingDocuments.AddRespaHudDetail();
//            closingDocuments.SetRespaSummary();
//            closingDocuments.AddSeller();
//            closingDocuments.AddServicer();
            return closingDocuments;

        }

        
        private Mismo.Closing.XERiders CreateRiders() 
        {
            Mismo.Closing.XERiders riders = new Mismo.Closing.XERiders();

            E_aOccT aOccT = m_dataLoan.GetAppData(0).aOccT;

            riders.NonOwnerOccupancyRiderIndicator = ToMismo(aOccT != E_aOccT.PrimaryResidence);

            return riders;
        }
        private Mismo.Closing.XERecordableDocument CreateRecordableDocument() 
        {
            Mismo.Closing.XERecordableDocument recordableDocument = new Mismo.Closing.XERecordableDocument();
            recordableDocument.SetRiders(CreateRiders());

            return recordableDocument;
        }
        private Mismo.Closing.XECompensation CreateCompensation() 
        {
            // 9/26/2007 dd - I assume broker comp 1 is for SRP.
            Mismo.Closing.XECompensation compensation = new Mismo.Closing.XECompensation();
            compensation.PaidToType = Mismo.Closing.E_CompensationPaidToType.Broker;
            compensation.Type = Mismo.Closing.E_CompensationType.ServiceReleasePremium;
            compensation.Amount = m_dataLoan.sBrokComp1_rep;
            return compensation;

        }
        private Mismo.Closing.XEMortgageBroker CreateMortgageBroker() 
        {
            // 9/26/2007 dd - We get broker information through 1003 interviewer section. If 1003 interviewer section is blank then we retrieve from Broker agent.
            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm( E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject );
            if (!interviewer.IsEmpty) 
            {
                Mismo.Closing.XEMortgageBroker mortgageBroker = new Mismo.Closing.XEMortgageBroker();
                mortgageBroker.UnparsedName = interviewer.CompanyName;
                mortgageBroker.StreetAddress = interviewer.StreetAddr;
                mortgageBroker.City = interviewer.City;
                mortgageBroker.State = interviewer.State;
                mortgageBroker.PostalCode = interviewer.Zip;
                mortgageBroker.LicenseNumberIdentifier = interviewer.LicenseNumOfCompany;

                return mortgageBroker;
            } 
            else 
            {
                CAgentFields broker = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Broker, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (broker.IsValid) 
                {
                    Mismo.Closing.XEMortgageBroker mortgageBroker = new Mismo.Closing.XEMortgageBroker();
                    mortgageBroker.UnparsedName = broker.CompanyName;
                    mortgageBroker.StreetAddress = broker.StreetAddr;
                    mortgageBroker.City = broker.City;
                    mortgageBroker.State = broker.State;
                    mortgageBroker.PostalCode = broker.Zip;
                    mortgageBroker.LicenseNumberIdentifier = broker.LicenseNumOfCompany;
                    
                    Mismo.MismoXmlElement.XEContactDetail contactDetail = new Mismo.MismoXmlElement.XEContactDetail();
                    contactDetail.Name = broker.AgentName;
                    contactDetail.AddWorkPhone(broker.Phone);
                    mortgageBroker.SetContactDetail(contactDetail);

                    return mortgageBroker;
                }
            }

            return null;
        }
        private Mismo.MismoXmlElement.XERespaServicingData CreateRespaServicingData() 
        {
            Mismo.MismoXmlElement.XERespaServicingData respaServicingData = new Mismo.MismoXmlElement.XERespaServicingData();
            respaServicingData.FirstTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear1;
            respaServicingData.FirstTransferYearValue = m_dataLoan.sFirstLienLoanTransferRecordYear1Percent;
            respaServicingData.SecondTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear2;
            respaServicingData.SecondTransferYearValue = m_dataLoan.sFirstLienLoanTransferRecordYear2Percent;
            respaServicingData.ThirdTransferYear = m_dataLoan.sFirstLienLoanTransferRecordYear3;
            respaServicingData.ThirdTransferYearValue = m_dataLoan.sFirstLienLoanTransferRecordYear3Percent;
            respaServicingData.HavePreviouslyAssignedServicingIndicator = ToMismo(m_dataLoan.sFirstLienLoanPreviouslyTransfered);
            respaServicingData.ThisIsOurRecordOfTransferringServicingIndicator = ToMismo(m_dataLoan.sFirstLienLoanDeclareServiceRecord);
            respaServicingData.HaveNotServicedInPastThreeYearsIndicator = ToMismo(m_dataLoan.sDontServiceInPast3Yrs);
            respaServicingData.AssignSellOrTransferSomeServicingIndicator = ToMismo(m_dataLoan.sAppliedProgramTransfered);
            respaServicingData.ExpectToAssignSellOrTransferPercent = m_dataLoan.sAppliedProgramTransferedPercent_rep;
            respaServicingData.MayAssignServicingIndicator = ToMismo(m_dataLoan.sMayAssignSellTransfer);

            if (m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                // The DoNotServiceIndicator depends on whether there is possible intent to service
                // or whether the loan will explicitly not be serviced by the lender. We take both
                // the Servicing Disclosure and LE datapoints into account, with the LE taking precedence
                // if it conflicts with the Servicing Disclosure. (OPM 246737) 
                respaServicingData.DoNotServiceIndicator = ToMismo(
                    !m_dataLoan.sMayAssignSellTransfer &&
                    !m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan &&
                    (!m_dataLoan.sDontService || 
                    !m_dataLoan.sServiceDontIntendToTransfer));
                respaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator = ToMismo(!m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan);
                respaServicingData.WillServiceIndicator = ToMismo(m_dataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan);
            }
            else
            {
                if (m_dataLoan.sMayAssignSellTransfer || m_dataLoan.sDontService || m_dataLoan.sServiceDontIntendToTransfer)
                {
                    respaServicingData.DoNotServiceIndicator = ToMismo(m_dataLoan.sDontService);
                }

                if (m_dataLoan.sDontService || m_dataLoan.sServiceDontIntendToTransfer)
                {
                    respaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator = ToMismo(m_dataLoan.sDontService);
                    respaServicingData.WillServiceIndicator = ToMismo(m_dataLoan.sServiceDontIntendToTransfer);
                }
            }

            switch (m_dataLoan.sAppliedProgramTransferedT) 
            {
                case E_sAppliedProgramTransferedT.RetainAll:
                    respaServicingData.ExpectToRetainAllServicingIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
                case E_sAppliedProgramTransferedT.RetainNone:
                    respaServicingData.ExpectToSellAllServicingIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
                case E_sAppliedProgramTransferedT.RetainSome:
                    respaServicingData.ExpectToAssignSellOrTransferPercentOfServicingIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
            }
            switch (m_dataLoan.sFirstLienLoanServiceEstimateT) 
            {
                case E_sFirstLienLoanServiceEstimateT.Does:
                    respaServicingData.ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
                case E_sFirstLienLoanServiceEstimateT.Doesnot:
                    respaServicingData.ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator = Mismo.Common.E_YesNoIndicator.No;
                    break;                    
            }


            switch (m_dataLoan.sFirstLienLoanDeclareServiceRecordIncludeT) 
            {
                case E_sFirstLienLoanDeclareServiceRecordIncludeT.Does:
                    respaServicingData.ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
                case E_sFirstLienLoanDeclareServiceRecordIncludeT.Doesnot:
                    respaServicingData.ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator = Mismo.Common.E_YesNoIndicator.No;
                    break;
            }

            switch (m_dataLoan.sAbleToServiceAndDecisionT) 
            {
                case E_sAbleToServiceAndDecisionT.Blank:
                    break;
                case E_sAbleToServiceAndDecisionT.HaventDecide:
                    respaServicingData.AreAbleToServiceIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    respaServicingData.HaveNotDecidedToServiceIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
                case E_sAbleToServiceAndDecisionT.Will:
                    respaServicingData.AreAbleToServiceIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
                case E_sAbleToServiceAndDecisionT.Willnot:
                    respaServicingData.AreAbleToServiceIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    respaServicingData.WillNotServiceIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
            }

            switch (m_dataLoan.sTransferPercentFirstLienLoanT) 
            {
                case E_sTransferPercentFirstLienLoanT._25:
                    respaServicingData.TwelveMonthPeriodTransferPercent = "25";
                    break;
                case E_sTransferPercentFirstLienLoanT._50:
                    respaServicingData.TwelveMonthPeriodTransferPercent = "50";
                    break;
                case E_sTransferPercentFirstLienLoanT._75:
                    respaServicingData.TwelveMonthPeriodTransferPercent = "75";
                    break;
                case E_sTransferPercentFirstLienLoanT._100:
                    respaServicingData.TwelveMonthPeriodTransferPercent = "100";
                    break;
            }

            return respaServicingData;
        }
        private Mismo.Closing.XELoanDetails CreateLoanDetails() 
        {
            Mismo.Closing.XELoanDetails loanDetails = new Mismo.Closing.XELoanDetails();
            loanDetails.ClosingDate = m_dataLoan.sClosedD_rep;
            loanDetails.FundByDate = m_dataLoan.sFundD_rep;
            loanDetails.LockExpirationDate = m_dataLoan.sRLckdExpiredD_rep;
            //            loanDetails.OriginalLTVRatioPercent;
            //            loanDetails.DisbursementDate;
            loanDetails.DocumentOrderClassificationType = Mismo.Closing.E_LoanDetailsDocumentOrderClassificationType.Preliminary;
            loanDetails.DocumentPreparationDate = m_dataLoan.sDocsD_rep;

            //            loanDetails.RescissionDate;
            //            loanDetails.SetInterimInterest();
            //            loanDetails.SetRequestToRescind();

            return loanDetails;
        }
        private Mismo.Closing.XELender CreateLender() 
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid) 
            {
                return null;
            }

            Mismo.Closing.XELender lender = new Mismo.Closing.XELender();
            lender.UnparsedName = agent.CompanyName;
            lender.StreetAddress = agent.StreetAddr;
            lender.City = agent.City;
            lender.State = agent.State;
            lender.PostalCode = agent.Zip;

            return lender;

        }
        private Mismo.Closing.XEInvestor CreateInvestor() 
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Investor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid) 
            {
                return null;
            }

            Mismo.Closing.XEInvestor investor = new Mismo.Closing.XEInvestor();
            investor.UnparsedName = agent.CompanyName;
            investor.StreetAddress = agent.StreetAddr;
            investor.City = agent.City;
            investor.State = agent.State;
            investor.PostalCode = agent.Zip;

            return investor;
        }
        private Mismo.Closing.XEClosingInstructions CreateClosingInstructions() 
        {
            Mismo.Closing.XEClosingInstructions closingInstructions = new Mismo.Closing.XEClosingInstructions();

            StringBuilder sb = new StringBuilder();            
            #region Conditions

            Guid brokerId = ((AbstractUserPrincipal) System.Threading.Thread.CurrentPrincipal).BrokerId;

            
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);

            if (brokerDB.IsUseNewTaskSystem)
            {
                List<Task> tasks = Task.GetActiveConditionsByLoanId(brokerDB.BrokerID, m_dataLoan.sLId, false);
                foreach (Task task in tasks)
                {

                    if (task.TaskStatus == E_TaskStatus.Closed)
                        continue; // Skip complete condition.

                    sb.AppendFormat("{0}{1}", task.TaskSubject, Environment.NewLine);
                    closingInstructions.AddCondition(CreateCondition(task.TaskSubject));

                }
            }
            else
            {
                LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();
                if (brokerDB.HasLenderDefaultFeatures)
                {
                    ldSet.Retrieve(m_dataLoan.sLId, false, false, false);

                    foreach (CLoanConditionObsolete condition in ldSet)
                    {
                        if (condition.CondStatus == E_CondStatus.Done)
                            continue; // Skip complete condition.

                        sb.AppendFormat("{0}{1}", condition.CondDesc, Environment.NewLine);
                        closingInstructions.AddCondition(CreateCondition(condition.CondDesc));
                    }

                }
                else
                {
                    int count = m_dataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete condition = m_dataLoan.GetCondFieldsObsolete(i);
                        if (condition.IsDone)
                            continue;
                        sb.AppendFormat("{0}{1}", condition.CondDesc, Environment.NewLine);
                        closingInstructions.AddCondition(CreateCondition(condition.CondDesc));
                    }
                }
            }

            #endregion

            closingInstructions.ConsolidatedClosingConditionsDescription = sb.ToString(); // Online Documents use this attribute.
//            closingInstructions.SpecialFloodHazardAreaIndicator;
//            closingInstructions.LeadBasedPaintCertificationRequiredIndicator;
//            closingInstructions.PropertyTaxMessageDescription;
//            closingInstructions.PreliminaryTitleReportDate;
//            closingInstructions.TitleReportItemsDescription;
//            closingInstructions.TitleReportRequiredEndorsementsDescription;
//            closingInstructions.TermiteReportRequiredIndicator;
//            closingInstructions.HoursDocumentsNeededPriorToDisbursementCount;
//            closingInstructions.FundingCutoffTime;

            return closingInstructions;
        }
        private Mismo.Closing.XECondition CreateCondition(string desc) 
        {
            Mismo.Closing.XECondition condition = new Mismo.Closing.XECondition();
            condition.Description = desc;
            return condition;
        }
        private Mismo.Closing.XEClosingAgent CreateClosingAgent(E_AgentRoleT agentRole) 
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid) 
            {
                return null;
            }

            Mismo.Closing.XEClosingAgent closingAgent = new Mismo.Closing.XEClosingAgent();
            closingAgent.Type = ToMismoClosingAgentType(agentRole);
            closingAgent.UnparsedName = agent.CompanyName;
            closingAgent.StreetAddress = agent.StreetAddr;
            closingAgent.City = agent.City;
            closingAgent.State = agent.State;
            closingAgent.PostalCode = agent.Zip;

            Mismo.MismoXmlElement.XEContactDetail contactDetail = new Mismo.MismoXmlElement.XEContactDetail();
            contactDetail.Name = agent.AgentName;
            contactDetail.AddWorkPhone(agent.Phone);
            contactDetail.AddWorkFax(agent.FaxNum);

            closingAgent.SetContactDetail(contactDetail);

            return closingAgent;
            
        }
        private Mismo.Closing.XEApplication CreateApplication() 
        {
            Mismo.Closing.XEApplication application = new Mismo.Closing.XEApplication();

            application.SetDataInformation(CreateDataInformation());
            application.SetAdditionalCaseData(CreateAdditionalCaseData());
            application.AddDownPayment(CreateDownpayment());

            int nApps = m_dataLoan.nApps;
            int borrowerSequenceIdentifier = 1;
            for (int borrowerIndex = 0; borrowerIndex < nApps; borrowerIndex++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(borrowerIndex);
                bool hasBorrowerInfo = dataApp.aBLastNm != "" || dataApp.aBFirstNm != "" || dataApp.aBSsn != "";
                bool hasCoborrowerInfo = dataApp.aCLastNm != "" || dataApp.aCFirstNm != "" || dataApp.aCSsn != "";

                if (!hasBorrowerInfo && !hasCoborrowerInfo)
                    continue; // Skip if there is no borrower & no coborrower.

                #region Add Assets
                IAssetCollection assetCollection = dataApp.aAssetCollection;
                application.AddAsset(CreateAsset(assetCollection.GetBusinessWorth(false), borrowerIndex)); // Add Business Networthed.
                application.AddAsset(CreateAsset(assetCollection.GetLifeInsurance(false), borrowerIndex)); // Add Life Insurance
                application.AddAsset(CreateAsset(assetCollection.GetRetirement(false), borrowerIndex));
                application.AddAsset(CreateAsset(assetCollection.GetCashDeposit1(false), borrowerIndex));
                application.AddAsset(CreateAsset(assetCollection.GetCashDeposit2(false), borrowerIndex));
                int assetRecordCount = assetCollection.CountRegular;
                for (int recordIndex = 0; recordIndex < assetRecordCount; recordIndex++) 
                {
                    application.AddAsset(CreateAsset(assetCollection.GetRegularRecordAt(recordIndex), borrowerIndex));
                }
                #endregion

                #region Add Liabilities
                ILiaCollection liaCollection = dataApp.aLiaCollection;
                application.AddLiability(CreateLiability(liaCollection.GetAlimony(false), borrowerIndex));
                application.AddLiability(CreateLiability(liaCollection.GetChildSupport(false), borrowerIndex));
                application.AddLiability(CreateLiability(liaCollection.GetJobRelated1(false), borrowerIndex));
                application.AddLiability(CreateLiability(liaCollection.GetJobRelated2(false), borrowerIndex));
                int liaRecordCount = liaCollection.CountRegular;
                for (int recordIndex = 0; recordIndex < liaRecordCount; recordIndex++) 
                {
                    application.AddLiability(CreateLiability(liaCollection.GetRegularRecordAt(recordIndex), borrowerIndex));
                }
                #endregion

                #region Add REO Properties
                var reCollection = dataApp.aReCollection;
                int reRecordCount = reCollection.CountRegular;
                for (int recordIndex = 0; recordIndex < reRecordCount; recordIndex++) 
                {
                    application.AddReoProperty(CreateReoProperty(dataApp, reCollection.GetRegularRecordAt(recordIndex), borrowerIndex));
                }
                #endregion

                if (hasBorrowerInfo)
                    application.AddBorrower(CreateBorrower(dataApp, true, hasCoborrowerInfo, borrowerIndex, borrowerSequenceIdentifier++));
                if (hasCoborrowerInfo)
                    application.AddBorrower(CreateBorrower(dataApp, false, hasBorrowerInfo, borrowerIndex, borrowerSequenceIdentifier++));

            }

            application.SetGovernmentLoan(CreateGovernmentLoan());
            application.SetGovernmentReporting(CreateGovernmentReporting());
            application.SetInterviewerInformation(CreateInterviewerInformation());
            application.SetLoanProductData(CreateLoanProductData());
            application.SetLoanPurpose(CreateLoanPurpose());
            application.SetLoanQualification(CreateLoanQualification());
            application.SetMortgageTerms(CreateMortgageTerms());
            application.SetProperty(CreateProperty());

            #region Proposed Housing Expense
            application.AddProposedHousingExpense(CreateProposedHousingExpense(Mismo.MismoXmlElement.E_ProposedHousingExpenseType.MI, m_dataLoan.sProMIns_rep));
            application.AddProposedHousingExpense(CreateProposedHousingExpense(Mismo.MismoXmlElement.E_ProposedHousingExpenseType.HazardInsurance, m_dataLoan.sProHazIns_rep));
            application.AddProposedHousingExpense(CreateProposedHousingExpense(Mismo.MismoXmlElement.E_ProposedHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, m_dataLoan.sProSecondMPmt_rep));
            application.AddProposedHousingExpense(CreateProposedHousingExpense(Mismo.MismoXmlElement.E_ProposedHousingExpenseType.OtherHousingExpense, m_dataLoan.sProOHExp_rep));
            application.AddProposedHousingExpense(CreateProposedHousingExpense(Mismo.MismoXmlElement.E_ProposedHousingExpenseType.RealEstateTax, m_dataLoan.sProRealETx_rep));
            application.AddProposedHousingExpense(CreateProposedHousingExpense(Mismo.MismoXmlElement.E_ProposedHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, m_dataLoan.sProHoAssocDues_rep));
            application.AddProposedHousingExpense(CreateProposedHousingExpense(Mismo.MismoXmlElement.E_ProposedHousingExpenseType.FirstMortgagePrincipalAndInterest, m_dataLoan.sProFirstMPmt_rep));

            #endregion

            #region Respa Fee
            // In order to map RESPA fee correctly to Online Documents, the required APR and HUD# to match exactly with their system.
            //Y,801,LOAN ORIGINATION FEE
            //Y,802,LOAN DISCOUNT FEE
            //N,803,APPRAISAL FEE
            //N,804,CREDIT REPORT FEE
            //Y,,PROCESSING FEE
            //?,,DOCUMENT PREP. FEE
            //Y,,ADMINISTRATION FEE
            //N,1106,NOTARY FEE
            //N,1201,RECORDING FEE
            //Y,808,TAX SERVICE FEE
            //Y,1101,ESCROW FEE
            //N,1108,TITLE INSURANCE FEE
            //Y,,UNDERWRITING FEE
            //Y,,WIRE FEE
            //Y,,COURIER FEE
            //?,1107,ATTORNEY FEE
            //Y,,FEDERAL EXPRESS FEE
            //N,1301,SURVEY FEE
            //N,,FLOOD CERTIFICATION

            //            application.AddRespaFee();
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.LoanOriginationFee, 
                "801", "", false, m_dataLoanWithGfeArchiveApplied.sLOrigF_rep, m_dataLoanWithGfeArchiveApplied.sLOrigFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.LoanDiscountPoints, 
                "802", "", false, m_dataLoanWithGfeArchiveApplied.sLDiscnt_rep, m_dataLoan.sLDiscntProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.AppraisalFee, 
                "803", "", m_dataLoan.sApprFPaid, m_dataLoanWithGfeArchiveApplied.sApprF_rep, m_dataLoanWithGfeArchiveApplied.sApprFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.CreditReportFee, 
                "804", "", m_dataLoan.sCrFPaid, m_dataLoanWithGfeArchiveApplied.sCrF_rep, m_dataLoanWithGfeArchiveApplied.sCrFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.InspectionFee, 
                "805", "", false, m_dataLoanWithGfeArchiveApplied.sInspectF_rep, m_dataLoanWithGfeArchiveApplied.sInspectFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.MortgageBrokerFee, 
                "" /* 808 will be conflict with Tax Service Fee */, "", false, m_dataLoanWithGfeArchiveApplied.sMBrokF_rep, m_dataLoanWithGfeArchiveApplied.sMBrokFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.TaxRelatedServiceFee, 
                "808" /* Online Documents use HUD 808 */, "", false, m_dataLoanWithGfeArchiveApplied.sTxServF_rep, m_dataLoanWithGfeArchiveApplied.sTxServFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.ProcessingFee, 
                "" /* Online Documents does not accept hud# */, "", m_dataLoan.sProcFPaid, m_dataLoanWithGfeArchiveApplied.sProcF_rep, m_dataLoanWithGfeArchiveApplied.sProcFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.UnderwritingFee, 
                "" /* Online Documents does not accept hud# */, "", false, m_dataLoanWithGfeArchiveApplied.sUwF_rep, m_dataLoanWithGfeArchiveApplied.sUwFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.Other, 
                "" /* Online Documents does not accept hud# */, "WIRE FEE", false, m_dataLoanWithGfeArchiveApplied.sWireF_rep, m_dataLoanWithGfeArchiveApplied.sWireFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.s800U1FCode, m_dataLoanWithGfeArchiveApplied.s800U1FDesc, false, m_dataLoanWithGfeArchiveApplied.s800U1F_rep, m_dataLoanWithGfeArchiveApplied.s800U1FProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.s800U2FCode, m_dataLoanWithGfeArchiveApplied.s800U2FDesc, false, m_dataLoanWithGfeArchiveApplied.s800U2F_rep, m_dataLoanWithGfeArchiveApplied.s800U2FProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.s800U3FCode, m_dataLoanWithGfeArchiveApplied.s800U3FDesc, false, m_dataLoanWithGfeArchiveApplied.s800U3F_rep, m_dataLoanWithGfeArchiveApplied.s800U3FProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.s800U4FCode, m_dataLoanWithGfeArchiveApplied.s800U4FDesc, false, m_dataLoanWithGfeArchiveApplied.s800U4F_rep, m_dataLoanWithGfeArchiveApplied.s800U4FProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._800_LoanFees, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.s800U5FCode, m_dataLoanWithGfeArchiveApplied.s800U5FDesc, false, m_dataLoanWithGfeArchiveApplied.s800U5F_rep, m_dataLoanWithGfeArchiveApplied.s800U5FProps));

            
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, Mismo.Closing.E_RespaFeeType.Undefined, 
                "901", "", false, m_dataLoan.sIPia_rep, m_dataLoan.sIPiaProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, Mismo.Closing.E_RespaFeeType.Other, 
                "902", "MORTGAGE INSURANCE PREMIUM", false, m_dataLoan.sMipPia_rep, m_dataLoanWithGfeArchiveApplied.sMipPiaProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, Mismo.Closing.E_RespaFeeType.Undefined, 
                "903", "", false, m_dataLoan.sHazInsPia_rep, m_dataLoan.sHazInsPiaProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, Mismo.Closing.E_RespaFeeType.Undefined, 
                "904", m_dataLoanWithGfeArchiveApplied.s904PiaDesc, false, m_dataLoanWithGfeArchiveApplied.s904Pia_rep, m_dataLoanWithGfeArchiveApplied.s904PiaProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, Mismo.Closing.E_RespaFeeType.Undefined, 
                "905", "", false, m_dataLoanWithGfeArchiveApplied.sVaFf_rep, m_dataLoanWithGfeArchiveApplied.sVaFfProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._900_LenderRequiredPaidInAdvance, Mismo.Closing.E_RespaFeeType.Undefined, 
                m_dataLoan.s900U1PiaCode, m_dataLoanWithGfeArchiveApplied.s900U1PiaDesc, false, m_dataLoanWithGfeArchiveApplied.s900U1Pia_rep, m_dataLoanWithGfeArchiveApplied.s900U1PiaProps));

            // 9/26/2007 dd - This section does not belong to RESPA FEE
//            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, Mismo.Closing.E_RespaFeeType.Undefined, 
//                "1001", "", false, m_dataLoan.sHazInsRsrv_rep, m_dataLoan.sHazInsRsrvProps));
//            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, Mismo.Closing.E_RespaFeeType.Undefined, 
//                "1002", "", false, m_dataLoan.sMInsRsrv_rep, m_dataLoan.sMInsRsrvProps));
//            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, Mismo.Closing.E_RespaFeeType.Undefined, 
//                "1003", "", false, m_dataLoan.sSchoolTxRsrv_rep, m_dataLoan.sSchoolTxRsrvProps));
//            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, Mismo.Closing.E_RespaFeeType.Undefined, 
//                "1004", "", false, m_dataLoan.sRealETxRsrv_rep, m_dataLoan.sRealETxRsrvProps));
//            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, Mismo.Closing.E_RespaFeeType.Undefined, 
//                "1005", "", false, m_dataLoan.sFloodInsRsrv_rep, m_dataLoan.sFloodInsRsrvProps));
//            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, Mismo.Closing.E_RespaFeeType.Undefined, 
//                "1006", m_dataLoan.s1006ProHExpDesc, false, m_dataLoan.s1006Rsrv_rep, m_dataLoan.s1006RsrvProps));
//            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, Mismo.Closing.E_RespaFeeType.Undefined, 
//                "1007", m_dataLoan.s1007ProHExpDesc, false, m_dataLoan.s1007Rsrv_rep, m_dataLoan.s1007RsrvProps));
//            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1000_ReservesDepositedWithLender, Mismo.Closing.E_RespaFeeType.Undefined, 
//                "1008", "", false, m_dataLoan.sAggregateAdjRsrv_rep, m_dataLoan.sAggregateAdjRsrvProps));


            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.SettlementOrClosingFee, 
                "1101", "", false, m_dataLoanWithGfeArchiveApplied.sEscrowF_rep, m_dataLoanWithGfeArchiveApplied.sEscrowFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.DocumentPreparationFee, 
                ""  /* Online Documents does not accept hud# */, "", false, m_dataLoanWithGfeArchiveApplied.sDocPrepF_rep, m_dataLoanWithGfeArchiveApplied.sDocPrepFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.NotaryFee, 
                "1106", "", false, m_dataLoanWithGfeArchiveApplied.sNotaryF_rep, m_dataLoanWithGfeArchiveApplied.sNotaryFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.AttorneyFee, 
                "1107", "", false, m_dataLoanWithGfeArchiveApplied.sAttorneyF_rep, m_dataLoanWithGfeArchiveApplied.sAttorneyFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.TitleInsuranceFee, 
                "1108", "", false, m_dataLoanWithGfeArchiveApplied.sTitleInsF_rep, m_dataLoanWithGfeArchiveApplied.sTitleInsFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU1TcCode, m_dataLoanWithGfeArchiveApplied.sU1TcDesc, false, m_dataLoanWithGfeArchiveApplied.sU1Tc_rep, m_dataLoanWithGfeArchiveApplied.sU1TcProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU2TcCode, m_dataLoanWithGfeArchiveApplied.sU2TcDesc, false, m_dataLoanWithGfeArchiveApplied.sU1Tc_rep, m_dataLoanWithGfeArchiveApplied.sU2TcProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU3TcCode, m_dataLoanWithGfeArchiveApplied.sU3TcDesc, false, m_dataLoanWithGfeArchiveApplied.sU1Tc_rep, m_dataLoanWithGfeArchiveApplied.sU3TcProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1100_TitleCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU4TcCode, m_dataLoanWithGfeArchiveApplied.sU4TcDesc, false, m_dataLoanWithGfeArchiveApplied.sU1Tc_rep, m_dataLoanWithGfeArchiveApplied.sU4TcProps));

            // 1201/1202 Recording Fee
            if (m_dataLoanWithGfeArchiveApplied.sRecFLckd)
            {
                application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.Undefined,
                    "1201", "", false, m_dataLoanWithGfeArchiveApplied.sRecF_rep, m_dataLoanWithGfeArchiveApplied.sRecFProps));
            }
            else
            {
                application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.DeedRecordingFee,
                    "1202", "", false, m_dataLoanWithGfeArchiveApplied.sRecDeed_rep, m_dataLoanWithGfeArchiveApplied.sRecFProps));

                application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.MortgageRecordingFee,
                    "1202", "", false, m_dataLoanWithGfeArchiveApplied.sRecMortgage_rep, m_dataLoanWithGfeArchiveApplied.sRecFProps));

                application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.ReleaseRecordingFee,
                    "1202", "", false, m_dataLoanWithGfeArchiveApplied.sRecRelease_rep, m_dataLoanWithGfeArchiveApplied.sRecFProps));
            }

            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.CityCountyMortgageTaxStampFee, 
                "1202", "", false, m_dataLoanWithGfeArchiveApplied.sCountyRtc_rep, m_dataLoanWithGfeArchiveApplied.sCountyRtcProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.StateMortgageTaxStampFee, 
                "1203", "", false, m_dataLoanWithGfeArchiveApplied.sStateRtc_rep, m_dataLoanWithGfeArchiveApplied.sStateRtcProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU1GovRtcCode, m_dataLoanWithGfeArchiveApplied.sU1GovRtcDesc, false, m_dataLoanWithGfeArchiveApplied.sU1GovRtc_rep, m_dataLoanWithGfeArchiveApplied.sU1GovRtcProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU2GovRtcCode, m_dataLoanWithGfeArchiveApplied.sU2GovRtcDesc, false, m_dataLoanWithGfeArchiveApplied.sU1GovRtc_rep, m_dataLoanWithGfeArchiveApplied.sU2GovRtcProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1200_RecordingAndTransferCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU3GovRtcCode, m_dataLoanWithGfeArchiveApplied.sU3GovRtcDesc, false, m_dataLoanWithGfeArchiveApplied.sU1GovRtc_rep, m_dataLoanWithGfeArchiveApplied.sU3GovRtcProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, Mismo.Closing.E_RespaFeeType.PestInspectionFee, 
                "1302", "", false, m_dataLoanWithGfeArchiveApplied.sPestInspectF_rep, m_dataLoanWithGfeArchiveApplied.sPestInspectFProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU1ScCode, m_dataLoanWithGfeArchiveApplied.sU1ScDesc, false, m_dataLoanWithGfeArchiveApplied.sU1Sc_rep, m_dataLoanWithGfeArchiveApplied.sU1ScProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU2ScCode, m_dataLoanWithGfeArchiveApplied.sU2ScDesc, false, m_dataLoanWithGfeArchiveApplied.sU2Sc_rep, m_dataLoanWithGfeArchiveApplied.sU2ScProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU3ScCode, m_dataLoanWithGfeArchiveApplied.sU3ScDesc, false, m_dataLoanWithGfeArchiveApplied.sU3Sc_rep, m_dataLoanWithGfeArchiveApplied.sU3ScProps));
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU4ScCode, m_dataLoanWithGfeArchiveApplied.sU4ScDesc, false, m_dataLoanWithGfeArchiveApplied.sU4Sc_rep, m_dataLoanWithGfeArchiveApplied.sU4ScProps));            
            application.AddRespaFee(CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType._1300_AdditionalSettlementCharges, Mismo.Closing.E_RespaFeeType.Other, 
                m_dataLoan.sU5ScCode, m_dataLoanWithGfeArchiveApplied.sU5ScDesc, false, m_dataLoanWithGfeArchiveApplied.sU5Sc_rep, m_dataLoanWithGfeArchiveApplied.sU5ScProps));

            #endregion
            application.SetTransactionDetail(CreateTransactionDetail());

//            Mismo.Closing.XEEscrowAccountSummary escrowAccountSummary = new Mismo.Closing.XEEscrowAccountSummary();
//            escrowAccountSummary.EscrowAggregateAccountingAdjustmentAmount = m_dataLoan.sAggregateAdjRsrv_rep;
//            escrowAccountSummary.EscrowCushionNumberOfMonthsCount = "2";
//            application.SetEscrowAccountSummary(escrowAccountSummary);
            application.AddEscrow(CreateEscrow("1001", "", m_dataLoan.sProHazIns_rep, m_dataLoan.sProHazIns, m_dataLoan.sHazInsRsrvMon_rep));
            application.AddEscrow(CreateEscrow("1003", "", m_dataLoan.sProSchoolTx_rep, m_dataLoan.sProSchoolTx, m_dataLoan.sSchoolTxRsrvMon_rep));
            application.AddEscrow(CreateEscrow("1004", "", m_dataLoan.sProRealETx_rep, m_dataLoan.sProRealETx, m_dataLoan.sRealETxRsrvMon_rep));
            application.AddEscrow(CreateEscrow("1005", "", m_dataLoan.sProFloodIns_rep, m_dataLoan.sProFloodIns, m_dataLoan.sFloodInsRsrvMon_rep));
            application.AddEscrow(CreateEscrow("1006", m_dataLoan.s1006ProHExpDesc, m_dataLoan.s1006ProHExp_rep, m_dataLoan.s1006ProHExp, m_dataLoan.s1006RsrvMon_rep));
            application.AddEscrow(CreateEscrow("1007", m_dataLoan.s1007ProHExpDesc, m_dataLoan.s1007ProHExp_rep, m_dataLoan.s1007ProHExp, m_dataLoan.s1007RsrvMon_rep));
            // 2/25/14 gf - opm 126772, don't export the additional lines if they're not enabled.
            // I don't think exporting them would have any negative affects, as the values would
            // all be blank or 0. However, I wasn't 100% sure, and this will keep behavior consistent 
            // with the other exports.
            if (m_dataLoan.BrokerDB.EnableAdditionalSection1000CustomFees)
            {
                application.AddEscrow(CreateEscrow("1008", m_dataLoan.sU3RsrvDesc, m_dataLoan.sProU3Rsrv_rep, m_dataLoan.sProU3Rsrv, m_dataLoan.sU3RsrvMon_rep));
                application.AddEscrow(CreateEscrow("1009", m_dataLoan.sU4RsrvDesc, m_dataLoan.sProU4Rsrv_rep, m_dataLoan.sProU4Rsrv, m_dataLoan.sU4RsrvMon_rep));
            }

            //            application.SetMers(CreateMers());
            //            application.SetMiData(CreateMiData());
            //application.SetAffordableLending(CreateAffordableLending());
            //            application.AddTitleHolder();
            //            application.SetEscrowAccountSummary()

            return application;
        }

        private Mismo.Closing.XEEscrow CreateEscrow(string hudLineNumber, string otherDesc, string monthlyPaymentAmount_rep, decimal monthlyPaymentAmount, string numOfMonths) 
        {
            int colIndex = -1;

            Mismo.Closing.E_EscrowItemType itemType = Mismo.Closing.E_EscrowItemType.Undefined;

            switch (hudLineNumber) 
            {
                case "1001":
                    itemType = Mismo.Closing.E_EscrowItemType.HazardInsurance;
                    colIndex = 1; // HAZARD
                    break;
                case "1003":
                    itemType = Mismo.Closing.E_EscrowItemType.SchoolPropertyTax;
                    colIndex = 4; // SCHOOL TAX
                    break;
                case "1004":
                    itemType = Mismo.Closing.E_EscrowItemType.CountyPropertyTax;
                    colIndex = 0; // PROPERTY TAX
                    break;
                case "1005":
                    itemType = Mismo.Closing.E_EscrowItemType.FloodInsurance;
                    colIndex = 3; // FLOOD
                    break;
                case "1006":
                    itemType = Mismo.Closing.E_EscrowItemType.Other;
                    colIndex = 5; // OTHER 1
                    break;
                case "1007":
                    itemType = Mismo.Closing.E_EscrowItemType.Other;
                    colIndex = 6; // OTHER 2
                    break;
                case "1008":
                    itemType = Mismo.Closing.E_EscrowItemType.Other;
                    colIndex = 7; // OTHER 3
                    break;
                case "1009":
                    itemType = Mismo.Closing.E_EscrowItemType.Other;
                    colIndex = 8; // OTHER 4
                    break;
            }

            Mismo.Closing.XEEscrow escrow = new Mismo.Closing.XEEscrow();
            escrow.ItemTypeOtherDescription = otherDesc;
            escrow.MonthlyPaymentAmount = monthlyPaymentAmount_rep;
            escrow.SpecifiedHUD1LineNumber = hudLineNumber;
            escrow.CollectedNumberOfMonthsCount = numOfMonths;
            escrow.ItemType = itemType;
            escrow.AnnualPaymentAmount = m_dataLoan.m_convertLos.ToMoneyString(monthlyPaymentAmount * 12, FormatDirection.ToRep);
            escrow.PaidByType = Mismo.Closing.E_EscrowPaidByType.Buyer;


            bool hasSchedDueD1 = false;
            DateTime sSchedDueD1 = DateTime.MinValue;

            try 
            {
                sSchedDueD1 = m_dataLoan.sSchedDueD1.DateTimeForComputation;
                hasSchedDueD1 = true;
            } 
            catch {}
            if (hasSchedDueD1) 
            {
                int[,] sInitialEscrowAcc = m_dataLoan.sInitialEscrowAcc;
                int index = 1;
                for (int offset = 0; offset < 12; offset++) 
                {
                    DateTime dueDate = sSchedDueD1.AddMonths(offset);
                    int nMonths = sInitialEscrowAcc[dueDate.Month, colIndex];
                    if (nMonths > 0) 
                    {

                        decimal amount = (decimal) nMonths * monthlyPaymentAmount;
                        escrow.AddPayments(CreatePayments(m_dataLoan.m_convertLos.ToDateTimeString(dueDate), m_dataLoan.m_convertLos.ToMoneyString(amount, FormatDirection.ToRep), index.ToString()));
                        index++;
                    }
                }
            }
//            escrow.AddPayments(CreatePayments());
//            escrow.PaidByType;
//            escrow.PaymentFrequencyType;
//            escrow.PaymentFrequencyTypeOtherDescription;
//            escrow.PremiumAmount;
//            escrow.PremiumDurationMonthsCount;
//            escrow.PremiumPaidByType;
//            escrow.PremiumPaymentType;


            return escrow;
        }

        private Mismo.MismoXmlElement.XEPayments CreatePayments(string dueDate, string paymentAmount, string sequenceIdentifier) 
        {
            Mismo.MismoXmlElement.XEPayments payments = new Mismo.MismoXmlElement.XEPayments();
            payments.DueDate = dueDate;
            payments.PaymentAmount = paymentAmount;
            payments.SequenceIdentifier = sequenceIdentifier;
            return payments;
        }
        private Mismo.MismoXmlElement.XETransactionDetail CreateTransactionDetail() 
        {
            Mismo.MismoXmlElement.XETransactionDetail transactionDetail = new Mismo.MismoXmlElement.XETransactionDetail();
            transactionDetail.AddPurchaseCredit(CreatePurchaseCredit(m_dataLoan.sOCredit1Desc, m_dataLoan.sOCredit1Amt_rep));
            transactionDetail.AddPurchaseCredit(CreatePurchaseCredit(m_dataLoan.sOCredit2Desc, m_dataLoan.sOCredit2Amt_rep));
            transactionDetail.AddPurchaseCredit(CreatePurchaseCredit(m_dataLoan.sOCredit3Desc, m_dataLoan.sOCredit3Amt_rep));
            transactionDetail.AddPurchaseCredit(CreatePurchaseCredit(m_dataLoan.sOCredit4Desc, m_dataLoan.sOCredit4Amt_rep));

            
            transactionDetail.AlterationsImprovementsAndRepairsAmount = m_dataLoan.sAltCost_rep;
            transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount = m_dataLoan.sRefPdOffAmt1003_rep;
            transactionDetail.SubordinateLienAmount = m_dataLoan.sONewFinBal_rep;
            transactionDetail.PurchasePriceAmount = m_dataLoan.sPurchPrice_rep;

            transactionDetail.BorrowerPaidDiscountPointsTotalAmount = m_dataLoan.sLDiscnt1003_rep;
            transactionDetail.EstimatedClosingCostsAmount = m_dataLoan.sTotEstCcNoDiscnt1003_rep;
            transactionDetail.MIAndFundingFeeFinancedAmount = m_dataLoanWithGfeArchiveApplied.sFfUfmipFinanced_rep;
            transactionDetail.MIAndFundingFeeTotalAmount = m_dataLoanWithGfeArchiveApplied.sFfUfmip1003_rep;
            transactionDetail.PrepaidItemsEstimatedAmount = m_dataLoan.sTotEstPp1003_rep;
            transactionDetail.SellerPaidClosingCostsAmount = m_dataLoan.sTotCcPbs_rep;

//            transactionDetail.FREReserveAmount;
//            transactionDetail.FREReservesAmount;
//            transactionDetail.SalesConcessionAmount;
//            transactionDetail.SubordinateLienHELOCAmount;

            return transactionDetail;
        }
        private Mismo.MismoXmlElement.XEPurchaseCredit CreatePurchaseCredit(string desc, string amount) 
        {
            if (null == amount || "" == amount || "0.00" == amount || null == desc)
                return null;

            Mismo.MismoXmlElement.XEPurchaseCredit purchaseCredit = new Mismo.MismoXmlElement.XEPurchaseCredit();
            switch (desc.ToLower()) 
            {
                case "cash deposit on sales contract":
                    purchaseCredit.Type = Mismo.MismoXmlElement.E_PurchaseCreditType.EarnestMoney;
                    break;
                case "employer assisted housing":
                    purchaseCredit.Type = Mismo.MismoXmlElement.E_PurchaseCreditType.EmployerAssistedHousing;
                    break;
                case "lease purchase fund":
                    purchaseCredit.Type = Mismo.MismoXmlElement.E_PurchaseCreditType.LeasePurchaseFund;
                    break;
                case "relocation funds":
                    purchaseCredit.Type = Mismo.MismoXmlElement.E_PurchaseCreditType.RelocationFunds;
                    break;
                case "seller credit":
                    purchaseCredit.SourceType = Mismo.MismoXmlElement.E_PurchaseCreditSourceType.PropertySeller;
                    purchaseCredit.Type = Mismo.MismoXmlElement.E_PurchaseCreditType.Other;
                    break;
                case "lender credit":
                    purchaseCredit.SourceType = Mismo.MismoXmlElement.E_PurchaseCreditSourceType.Lender;
                    purchaseCredit.Type = Mismo.MismoXmlElement.E_PurchaseCreditType.Other;
                    break;

                default:
                    purchaseCredit.Type = Mismo.MismoXmlElement.E_PurchaseCreditType.Other;
                    break;
            }
            purchaseCredit.Amount = amount;
            return purchaseCredit;

        }
        private Mismo.MismoXmlElement.XEProposedHousingExpense CreateProposedHousingExpense(Mismo.MismoXmlElement.E_ProposedHousingExpenseType type, string amount) 
        {
            if (null == amount || ""== amount || "0.00" == amount)
                return null;

            Mismo.MismoXmlElement.XEProposedHousingExpense proposedHousingExpense = new Mismo.MismoXmlElement.XEProposedHousingExpense();
            proposedHousingExpense.HousingExpenseType = type;
            proposedHousingExpense.PaymentAmount = amount;

            return proposedHousingExpense;

        }

        private Mismo.Closing.XEPayment CreatePayment(bool isPaid, string amount, int props) 
        {
            Mismo.Closing.XEPayment payment = new Mismo.Closing.XEPayment();
            payment.IncludedInAPRIndicator = ToMismo(LosConvert.GfeItemProps_Apr(props));
            payment.PaidOutsideOfClosingIndicator = ToMismo(LosConvert.GfeItemProps_Poc(props));

            payment.AllowableFHAClosingCostIndicator = ToMismo(LosConvert.GfeItemProps_FhaAllow(props));
            payment.Amount = amount;

            int paidBy = LosConvert.GfeItemProps_Payer(props);
            if (paidBy == 0 || paidBy == 1) 
            {
                payment.PaidByType = Mismo.Closing.E_PaymentPaidByType.Buyer;
            }
            else if (paidBy == 2) 
            {
                payment.PaidByType = Mismo.Closing.E_PaymentPaidByType.Seller;
            }
            else if (paidBy == 3) 
            {
                payment.PaidByType = Mismo.Closing.E_PaymentPaidByType.Lender; 
            }
            else if (paidBy == 4) 
            {
                payment.PaidByType = Mismo.Closing.E_PaymentPaidByType.ThirdParty;
                payment.PaidByTypeThirdPartyName = "Broker";
            }
//            payment.CollectedByType;

//            payment.NetDueAmount;
            
//            payment.Percent;
//            payment.ProcessType;
//            payment.Section32Indicator;

            return payment;

        }

        private Mismo.Closing.XERespaFee CreateRespaFee(Mismo.Closing.E_RespaFeeRespaSectionClassificationType classification, Mismo.Closing.E_RespaFeeType type,
            string hudLineNumber, string description, bool isPaid, string amount, int props) 
        {
            if ("0.00" == amount)
                return null;

            Mismo.Closing.XERespaFee respaFee = new Mismo.Closing.XERespaFee();
            respaFee.SpecifiedHUDLineNumber = hudLineNumber;
            respaFee.Type = type;
            respaFee.TypeOtherDescription = description;
            respaFee.RespaSectionClassificationType = classification;

            respaFee.AddPayment(CreatePayment(isPaid, amount, props));
            if (LosConvert.GfeItemProps_ToBr(props))
                respaFee.PaidToType = Mismo.Closing.E_RespaFeePaidToType.Broker;
            else
                respaFee.PaidToType = Mismo.Closing.E_RespaFeePaidToType.Lender;

//            respaFee.AddRequiredServiceProvider();
//            respaFee.SetPaidTo();

            
//            respaFee.PaidToTypeOtherDescription;
//            respaFee.RequiredProviderOfServiceIndicator;
//            respaFee.ResponsiblePartyType;

            return respaFee;
        }

        private Mismo.Closing.XEProperty CreateProperty() 
        {
            Mismo.Closing.XEProperty property = new Mismo.Closing.XEProperty();

            property.City = m_dataLoan.sSpCity;
            property.State = m_dataLoan.sSpState;
            property.FinancedNumberOfUnits = m_dataLoan.sUnitsNum_rep;
            property.StreetAddress = m_dataLoan.sSpAddr;
            property.PostalCode = m_dataLoan.sSpZip;
            property.County = m_dataLoan.sSpCounty;
            property.StructureBuiltYear = m_dataLoan.sYrBuilt;
            property.AddLegalDescription(CreateLegalDescription());

//            property.AcquiredDate;
//            property.AcreageNumber;
//            property.AddValuation();
//            property.AssessorsParcelIdentifier;
//            property.AssessorsSecondParcelIdentifier;
//            property.BuildingStatusType;
//            property.Country;
//            property.PlannedUnitDevelopmentIndicator;
//            property.SetDetails();
//            property.SetHomeownersAssociation();
//            property.SetParsedStreetAddress();
//            property.StreetAddress2;

            return property;
        }
        private Mismo.Closing.XELegalDescription CreateLegalDescription() 
        {
            Mismo.Closing.XELegalDescription legalDescription = new Mismo.Closing.XELegalDescription();
            legalDescription.TextDescription = m_dataLoan.sSpLegalDesc;
            return legalDescription;
        }
        private Mismo.Closing.XEMortgageTerms CreateMortgageTerms() 
        {
            Mismo.Closing.XEMortgageTerms mortgageTerms = new Mismo.Closing.XEMortgageTerms();
            mortgageTerms.AgencyCaseIdentifier = m_dataLoan.sAgencyCaseNum;
            mortgageTerms.ArmTypeDescription = m_dataLoan.sFinMethDesc;
            mortgageTerms.BaseLoanAmount= m_dataLoan.sLAmt1003_rep;
            mortgageTerms.BorrowerRequestedLoanAmount = m_dataLoan.sFinalLAmt_rep;
            mortgageTerms.LenderCaseIdentifier = m_dataLoan.sLenderCaseNum;
            mortgageTerms.LendersContactPrimaryTelephoneNumber = m_dataLoan.GetAgentOfRole( E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject ).Phone;
            mortgageTerms.LoanAmortizationTermMonths = m_dataLoan.sTerm_rep;
            mortgageTerms.OtherAmortizationTypeDescription = m_dataLoan.sFinMethDesc;
            mortgageTerms.OtherMortgageTypeDescription = m_dataLoan.sLTODesc;
            mortgageTerms.RequestedInterestRatePercent = m_dataLoan.sNoteIR_rep;
            mortgageTerms.NoteRatePercent = m_dataLoan.sNoteIR_rep;

            mortgageTerms.LoanAmortizationType = ToMismo(m_dataLoan.sFinMethT);
            mortgageTerms.LoanEstimatedClosingDate = m_dataLoan.sEstCloseD_rep;
            mortgageTerms.MortgageType = ToMismo(m_dataLoan.sLT);
            mortgageTerms.LenderLoanIdentifier = m_lenderLoanIdentifier;

            mortgageTerms.OriginalLoanAmount = m_dataLoan.sFinalLAmt_rep;
//            mortgageTerms.PaymentRemittanceDay;


            return mortgageTerms;
        }
        private Mismo.MismoXmlElement.XELoanQualification CreateLoanQualification() 
        {
            Mismo.MismoXmlElement.XELoanQualification loanQualification = new Mismo.MismoXmlElement.XELoanQualification();
            loanQualification.AdditionalBorrowerAssetsConsideredIndicator = ToMismo(m_dataLoan.sMultiApps);
            loanQualification.AdditionalBorrowerAssetsNotConsideredIndicator = ToMismo(m_dataLoan.GetAppData(0).aSpouseIExcl);
            return loanQualification;
        }

        private Mismo.Closing.XELoanProductData CreateLoanProductData() 
        {
            Mismo.Closing.XELoanProductData loanProductData = new Mismo.Closing.XELoanProductData();

            loanProductData.SetArm(CreateArm());
            loanProductData.SetLoanFeatures(CreateLoanFeatures());
            loanProductData.AddPaymentAdjustment(CreatePaymentAdjustment());
            loanProductData.AddRateAdjustment(CreateRateAdjustment());
            loanProductData.SetInterestOnly(CreateInterestOnly());

            if (m_dataLoan.sHasTempBuydown)
            {
                List<Mismo.Closing.XEBuyDown> buydownList = GenerateCondensedBuydown();

                foreach (var buydown in buydownList)
                {
                    loanProductData.AddBuyDown(buydown);
                }
            }

            // loanProductData.SetHeloc();
            // loanProductData.AddPrepaymentPenalty();
            return loanProductData;
        }
        private Mismo.Closing.XERateAdjustment CreateRateAdjustment() 
        {
            Mismo.Closing.XERateAdjustment rateAdjustment = new Mismo.Closing.XERateAdjustment();
            rateAdjustment.SubsequentCapPercent = m_dataLoan.sRAdjCapR_rep;
            rateAdjustment.SubsequentRateAdjustmentMonths = m_dataLoan.sRAdjCapMon_rep;
            rateAdjustment.FirstChangeCapRate = m_dataLoan.sR1stCapR_rep;
            rateAdjustment.FirstRateAdjustmentMonths = m_dataLoan.sRAdj1stCapMon_rep;
//
//            rateAdjustment.CalculationType;
//            rateAdjustment.DurationMonths;
//            rateAdjustment.FirstChangeFloorPercent;
//            rateAdjustment.FirstRateAdjustmentDate;
//            rateAdjustment.InitialCapPercent;
//            rateAdjustment.Percent;
//            rateAdjustment.PeriodNumber;
            return rateAdjustment;
        }
        private Mismo.Closing.XEPaymentAdjustment CreatePaymentAdjustment() 
        {
            Mismo.Closing.XEPaymentAdjustment paymentAdjustment = new Mismo.Closing.XEPaymentAdjustment();
            paymentAdjustment.PeriodNumber = m_dataLoan.sPmtAdjCapMon_rep;
            paymentAdjustment.PeriodicCapPercent = m_dataLoan.sPmtAdjCapR_rep;
//
//            paymentAdjustment.FirstPaymentAdjustmentDate;
//            paymentAdjustment.Amount;
//            paymentAdjustment.CalculationType;
//            paymentAdjustment.DurationMonths;
//            paymentAdjustment.Percent;
//            paymentAdjustment.PeriodicCapAmount;
//            paymentAdjustment.SubsequentPaymentAdjustmentMonths;
//            paymentAdjustment.FirstPaymentAdjustmentDate;
//            paymentAdjustment.LastPaymentAdjustmentDate;


            return paymentAdjustment;

        }
        private Mismo.Closing.XELoanFeatures CreateLoanFeatures() 
        {
            Mismo.Closing.XELoanFeatures loanFeatures = new Mismo.Closing.XELoanFeatures();

            loanFeatures.BalloonIndicator = ToMismo(m_dataLoan.sBalloonPmt);
            loanFeatures.BalloonLoanMaturityTermMonths = m_dataLoan.sDue_rep;
            loanFeatures.DemandFeatureIndicator = ToMismo(m_dataLoan.sHasDemandFeature);
            loanFeatures.EstimatedPrepaidDays = m_dataLoan.sIPiaDy_rep;
            loanFeatures.InterestOnlyTerm = m_dataLoan.sIOnlyMon_rep;
            loanFeatures.LoanScheduledClosingDate = m_dataLoan.sEstCloseD_rep;
            loanFeatures.NegativeAmortizationLimitPercent = m_dataLoan.sPmtAdjMaxBalPc_rep;
            loanFeatures.OriginalBalloonTermMonths = m_dataLoan.sTerm_rep;
            loanFeatures.PaymentFrequencyType = Mismo.Closing.E_LoanFeaturesPaymentFrequencyType.Monthly;
            loanFeatures.ScheduledFirstPaymentDate = m_dataLoan.sSchedDueD1_rep;
            loanFeatures.ProductName = m_dataLoan.sLpTemplateNm;
            loanFeatures.EscrowWaiverIndicator = ToMismo(m_dataLoan.sWillEscrowBeWaived);
            loanFeatures.LoanOriginalMaturityTermMonths = m_dataLoan.sDue_rep;
            loanFeatures.OriginalPrincipalAndInterestPaymentAmount = m_dataLoan.sProThisMPmt_rep;

            switch (m_dataLoan.sAssumeLT) 
            {
                case E_sAssumeLT.May:
                    loanFeatures.AssumabilityIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    loanFeatures.ConditionsToAssumabilityIndicator = Mismo.Common.E_YesNoIndicator.No;
                    break;
                case E_sAssumeLT.MayNot:
                    loanFeatures.AssumabilityIndicator = Mismo.Common.E_YesNoIndicator.No;
                    break;
                case E_sAssumeLT.MaySubjectToCondition:
                    loanFeatures.AssumabilityIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    loanFeatures.ConditionsToAssumabilityIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
            }
            loanFeatures.LienPriorityType = ToMismo(m_dataLoan.sLienPosT);
            switch (m_dataLoan.sPrepmtPenaltyT) 
            {
                case E_sPrepmtPenaltyT.May:
                    loanFeatures.PrepaymentPenaltyIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
                case E_sPrepmtPenaltyT.WillNot:
                    loanFeatures.PrepaymentPenaltyIndicator = Mismo.Common.E_YesNoIndicator.No;
                    break;
                default:
                    loanFeatures.PrepaymentPenaltyIndicator = Mismo.Common.E_YesNoIndicator.Undefined;
                    break;
            }
            switch (m_dataLoan.sPrepmtRefundT) 
            {
                case E_sPrepmtRefundT.May:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = Mismo.Common.E_YesNoIndicator.Yes;
                    break;
                case E_sPrepmtRefundT.WillNot:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = Mismo.Common.E_YesNoIndicator.No;
                    break;
                default:
                    loanFeatures.PrepaymentFinanceChargeRefundableIndicator = Mismo.Common.E_YesNoIndicator.Undefined;
                    break;

            }
            loanFeatures.RequiredDepositIndicator = ToMismo(m_dataLoan.sAprIncludesReqDeposit);

            loanFeatures.GsePropertyType = ToMismo(m_dataLoan.sGseSpT);
//            loanFeatures.AddLateCharge();
//            loanFeatures.AddNotePayTo();


//            loanFeatures.BuydownTemporarySubsidyIndicator;
//            loanFeatures.ConformingIndicator;
//            loanFeatures.CounselingConfirmationIndicator;
//            loanFeatures.CounselingConfirmationType;
//            loanFeatures.DownPaymentOptionType;
            

//            loanFeatures.EstimatedPrepaidDaysPaidByOtherTypeDescription;
////            loanFeatures.EstimatedPrepaidDaysPaidByType;
//            loanFeatures.FnmProductPlanIdentifier;
//            loanFeatures.FreOfferingIdentifier;
//            loanFeatures.FullPrepaymentPenaltyOptionType;
//            loanFeatures.GraduatedPaymentMultiplierFactor;
//            loanFeatures.GrowingEquityLoanPayoffYearsCount;

//            loanFeatures.HelocInitialAdvanceAmount;
//            loanFeatures.HelocMaximumBalanceAmount;
//            loanFeatures.InitialPaymentRatePercent;

//            loanFeatures.LenderSelfInsuredIndicator;
            
//            loanFeatures.LoanClosingStatusType;
//            loanFeatures.LoanDocumentationType;
//            loanFeatures.LoanMaturityDate;
//            loanFeatures.LoanRepaymentType;
//            loanFeatures.MiCertificationStatusType;
//            loanFeatures.MiCompanyNameType;
//            loanFeatures.MiCoveragePercent;
//            loanFeatures.NameDocumentsDrawnInType;
//            loanFeatures.PaymentFrequencyTypeOtherDescription;
//            loanFeatures.PrepaymentPenaltyTermMonths;
//            loanFeatures.PrepaymentRestrictionIndicator;
//            loanFeatures.ProductDescription;

//            loanFeatures.RefundableApplicationFeeIndicator;

//            loanFeatures.ServicingTransferStatusType;
//            loanFeatures.TimelyPaymentRateReductionIndicator;
//            loanFeatures.TimelyPaymentRateReductionPercent;
//

            return loanFeatures;
        }
        private Mismo.Closing.XEArm CreateArm() 
        {
            Mismo.Closing.XEArm arm = new Mismo.Closing.XEArm();
            arm.IndexMarginPercent = m_dataLoan.sRAdjMarginR_rep;
            arm.LifetimeFloorPercent = m_dataLoan.sRAdjFloorR_rep;
            arm.QualifyingRatePercent = m_dataLoan.sQualIR_rep;
            arm.RateAdjustmentLifetimeCapPercent = m_dataLoan.sRAdjLifeCapR_rep;
            //            arm.PaymentAdjustmentLifetimeCapAmount;
            //            arm.PaymentAdjustmentLifetimeCapPercent;
            //            arm.IndexCurrentValuePercent;
            //            arm.IndexType;
            //            arm.InterestRateRoundingFactor;
            //            arm.InterestRateRoundingType;
            //            arm.LifetimeCapRate;

            //            arm.ConversionOptionIndicator;
            //            arm.FnmTreasuryYieldForCurrentIndexDivisorNumber;
            //            arm.FnmTreasuryYieldForIndexDivisorNumber;
            //            arm.SetConversionOption();

            return arm;
        }
        private Mismo.Closing.XEInterestOnly CreateInterestOnly() 
        {
            if (m_dataLoan.sIOnlyMon_rep == "0")
                return null; // Not an interest only loan.

            Mismo.Closing.XEInterestOnly interestOnly = new Mismo.Closing.XEInterestOnly();
            interestOnly.TermMonthsCount = m_dataLoan.sIOnlyMon_rep;
            interestOnly.MonthlyPaymentAmount = m_dataLoan.sProThisMPmt_rep;
            return interestOnly;
        }
        private Mismo.MismoXmlElement.XEInterviewerInformation CreateInterviewerInformation() 
        {
            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm( E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject );


            Mismo.MismoXmlElement.XEInterviewerInformation interviewerInformation = new Mismo.MismoXmlElement.XEInterviewerInformation();
            interviewerInformation.ApplicationTakenMethodType = ToMismo(m_dataLoan.GetAppData(0).aIntrvwrMethodT);
            interviewerInformation.InterviewersEmployerCity = interviewer.City;
            interviewerInformation.InterviewersEmployerName = interviewer.CompanyName;
            interviewerInformation.InterviewersEmployerPostalCode = interviewer.Zip;
            interviewerInformation.InterviewersEmployerState = interviewer.State;
            interviewerInformation.InterviewersEmployerStreetAddress = interviewer.StreetAddr;
            interviewerInformation.InterviewersName = interviewer.PreparerName;
            interviewerInformation.InterviewersTelephoneNumber = interviewer.Phone;
            //            interviewerInformation.InterviewersApplicationSignedDate;

            return interviewerInformation;
        }
        private Mismo.MismoXmlElement.XEGovernmentReporting CreateGovernmentReporting() 
        {
            Mismo.MismoXmlElement.XEGovernmentReporting governmentReporting = new Mismo.MismoXmlElement.XEGovernmentReporting();
            governmentReporting.HmdaHoepaLoanStatusIndicator = ToMismo(m_dataLoan.sHmdaReportAsHoepaLoan);
            governmentReporting.HmdaPreapprovalType = ToMismo(m_dataLoan.sHmdaPreapprovalT);
            switch (m_dataLoan.sLPurposeT) 
            {
                case E_sLPurposeT.Purchase: 
                    governmentReporting.HmdaPurposeOfLoanType = Mismo.MismoXmlElement.E_HmdaPurposeOfLoanType.HomePurchase;
                    break;
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    if (m_dataLoan.sHmdaReportAsHomeImprov) 
                    {
                        governmentReporting.HmdaPurposeOfLoanType = Mismo.MismoXmlElement.E_HmdaPurposeOfLoanType.HomeImprovement;
                    } 
                    else 
                    {
                        governmentReporting.HmdaPurposeOfLoanType = Mismo.MismoXmlElement.E_HmdaPurposeOfLoanType.Refinancing;
                    }
                    break;
                default:
                    governmentReporting.HmdaPurposeOfLoanType = Mismo.MismoXmlElement.E_HmdaPurposeOfLoanType.Refinancing;
                    break;
            }
            

            governmentReporting.HmdaRateSpreadPercent = m_dataLoan.sHmdaAprRateSpread;
            
            return governmentReporting;
        }
        private Mismo.MismoXmlElement.XEGovernmentLoan CreateGovernmentLoan() 
        {
            if (m_dataLoan.sLT != E_sLT.FHA && m_dataLoan.sLT != E_sLT.VA)
                return null;

            Mismo.MismoXmlElement.XEGovernmentLoan governmentLoan = new Mismo.MismoXmlElement.XEGovernmentLoan();
            if (m_dataLoan.sLT == E_sLT.FHA) 
            {
                governmentLoan.SetFhaLoan(CreateFhaLoan());
            } 
            else if (m_dataLoan.sLT == E_sLT.VA) 
            {
                governmentLoan.SetVaLoan(CreateVaLoan());
            }
            governmentLoan.SetFhaVaLoan(CreateFhaVaLoan());

            return governmentLoan;
        }
        private Mismo.MismoXmlElement.XEFhaLoan CreateFhaLoan() 
        {
            Mismo.MismoXmlElement.XEFhaLoan fhaLoan = new Mismo.MismoXmlElement.XEFhaLoan();
            fhaLoan.FhaEnergyRelatedRepairsOrImprovementsAmount = m_dataLoan.sFHAEnergyEffImprov_rep;
            fhaLoan.FhaMIPremiumRefundAmount = m_dataLoan.sFHA203kFHAMipRefund_rep;
            fhaLoan.LenderIdentifier = m_dataLoan.sFHALenderIdCode;
            fhaLoan.SectionOfActType = ToMismoLoanSectionOfAct(m_dataLoan.sFHAHousingActSection); 
            fhaLoan.SponsorIdentifier = m_dataLoan.sFHASponsorAgentIdCode;

            //            fhaLoan.BorrowerFinancedFhaDiscountPointsAmount;
            //            fhaLoan.FhaAlimonyLiabilityTreatmentType;
            //            fhaLoan.FhaCoverageRenewalRatePercent;
            //            fhaLoan.FhaGeneralServicesAdminstrationCodeIdentifier;
            //            fhaLoan.FhaLimitedDenialParticipationIdentifier;
            //            fhaLoan.FhaRefinanceInterestOnExistingLienAmount;
            //            fhaLoan.FhaRefinanceOriginalExistingFhaCaseIdentifier;
            //            fhaLoan.FhaRefinanceOriginalExistingUpFrontMIPAmount;
            //            fhaLoan.FhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier;
            //            fhaLoan.FhaUpfrontMIPremiumPercent;
            //            fhaLoan.HudAdequateAvailableAssetsIndicator;
            //            fhaLoan.HudAdequateEffectiveIncomeIndicator;
            //            
            //            fhaLoan.HudCreditCharacteristicsIndicator;
            //            fhaLoan.HudStableEffectiveIncomeIndicator;
            return fhaLoan;
        }
        private Mismo.MismoXmlElement.XEVaLoan CreateVaLoan() 
        {
            CAppData dataApp = m_dataLoan.GetAppData(0);
            Mismo.MismoXmlElement.XEVaLoan vaLoan = new Mismo.MismoXmlElement.XEVaLoan();
            vaLoan.VaEntitlementAmount = dataApp.aVaEntitleAmt_rep;
            vaLoan.VaEntitlementCodeIdentifier = dataApp.aVaEntitleCode;
            vaLoan.VaMaintenanceExpenseMonthlyAmount = m_dataLoan.sVaProMaintenancePmt_rep;
            vaLoan.VaResidualIncomeAmount = dataApp.aVaFamilySupportBal_rep;
            vaLoan.VaUtilityExpenseMonthlyAmount = m_dataLoan.sVaProUtilityPmt_rep;

//            vaLoan.BorrowerFundingFeePercent;
//            vaLoan.VaBorrowerCoBorrowerMarriedIndicator;
//            vaLoan.VaHouseholdSizeCount;

            return vaLoan;
        }
        private Mismo.MismoXmlElement.XEFhaVaLoan CreateFhaVaLoan() 
        {
            return null;
        }
        private Mismo.Closing.XEBorrower CreateBorrower(CAppData dataApp, bool isBorrower, bool hasSpouse, int borrowerIndex, int borrowerSequenceIdentifier) 
        {
            Mismo.Closing.XEBorrower borrower = new Mismo.Closing.XEBorrower();
            string borrowerId = (isBorrower ? "B" : "C") + borrowerIndex; // Id is in this format B1, C1 or B2, C2
            string coborrowerId = null;
            if (hasSpouse) 
            {
                coborrowerId = (isBorrower ? "C" : "B") + borrowerIndex; // Id is in this format B1, C1 or B2, C2
            }

            borrower.BorrowerID = borrowerId;
            borrower.JointAssetBorrowerID = coborrowerId;
            borrower.FirstName = isBorrower ? dataApp.aBFirstNm : dataApp.aCFirstNm;
            borrower.MiddleName = isBorrower ? dataApp.aBMidNm : dataApp.aCMidNm;
            borrower.LastName = isBorrower ? dataApp.aBLastNm : dataApp.aCLastNm;
            borrower.NameSuffix = isBorrower ? dataApp.aBSuffix : dataApp.aCSuffix;
            borrower.AgeAtApplicationYears = isBorrower ? dataApp.aBAge_rep : dataApp.aCAge_rep;
            borrower.BirthDate = isBorrower ? dataApp.aBDob_rep : dataApp.aCDob_rep;
            borrower.HomeTelephoneNumber = isBorrower ? dataApp.aBHPhone : dataApp.aCHPhone;
            borrower.PrintPositionType = isBorrower ? Mismo.Closing.E_BorrowerPrintPositionType.Borrower : Mismo.Closing.E_BorrowerPrintPositionType.CoBorrower;
            borrower.Ssn = isBorrower ? dataApp.aBSsn : dataApp.aCSsn;
            borrower.DependentCount = isBorrower ? dataApp.aBDependNum_rep : dataApp.aCDependNum_rep;
            borrower.JointAssetLiabilityReportingType = dataApp.aAsstLiaCompletedNotJointly ? Mismo.Closing.E_BorrowerJointAssetLiabilityReportingType.NotJointly : Mismo.Closing.E_BorrowerJointAssetLiabilityReportingType.Jointly;
            borrower.MaritalStatusType = isBorrower ? ToMismo(dataApp.aBMaritalStatT) : ToMismo(dataApp.aCMaritalStatT);
            borrower.SchoolingYears = isBorrower ? dataApp.aBSchoolYrs_rep : dataApp.aCSchoolYrs_rep;
            //            borrower.AddAlias();
            borrower.SetMailTo(CreateMailTo(dataApp, isBorrower));

            if (isBorrower) 
            {
                #region Residence
                borrower.AddResidence(CreateResidence(dataApp.aBAddr, dataApp.aBCity, dataApp.aBState, dataApp.aBZip, dataApp.aBAddrT, dataApp.aBAddrYrs, true));
                borrower.AddResidence(CreateResidence(dataApp.aBPrev1Addr, dataApp.aBPrev1City, dataApp.aBPrev1State, dataApp.aBPrev1Zip, (E_aBAddrT) dataApp.aBPrev1AddrT, dataApp.aBPrev1AddrYrs, false));
                borrower.AddResidence(CreateResidence(dataApp.aBPrev2Addr, dataApp.aBPrev2City, dataApp.aBPrev2State, dataApp.aBPrev2Zip, (E_aBAddrT) dataApp.aBPrev2AddrT, dataApp.aBPrev2AddrYrs, false));
                #endregion

                #region Current Income
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.Base, dataApp.aBBaseI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.Overtime, dataApp.aBOvertimeI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.Bonus, dataApp.aBBonusesI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.Commissions, dataApp.aBCommisionI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.DividendsInterest, dataApp.aBDividendI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.NetRentalIncome, dataApp.aBNetRentI1003_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.OtherTypesOfIncome, dataApp.aBTotOI_rep));
                #endregion

            }
            else 
            {
                #region Residence
                borrower.AddResidence(CreateResidence(dataApp.aCAddr, dataApp.aCCity, dataApp.aCState, dataApp.aCZip, (E_aBAddrT) dataApp.aCAddrT, dataApp.aCAddrYrs, true));
                borrower.AddResidence(CreateResidence(dataApp.aCPrev1Addr, dataApp.aCPrev1City, dataApp.aCPrev1State, dataApp.aCPrev1Zip, (E_aBAddrT) dataApp.aCPrev1AddrT, dataApp.aCPrev1AddrYrs, false));
                borrower.AddResidence(CreateResidence(dataApp.aCPrev2Addr, dataApp.aCPrev2City, dataApp.aCPrev2State, dataApp.aCPrev2Zip, (E_aBAddrT) dataApp.aCPrev2AddrT, dataApp.aCPrev2AddrYrs, false));
                #endregion

                #region Current Income
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.Base, dataApp.aCBaseI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.Overtime, dataApp.aCOvertimeI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.Bonus, dataApp.aCBonusesI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.Commissions, dataApp.aCCommisionI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.DividendsInterest, dataApp.aCDividendI_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.NetRentalIncome, dataApp.aCNetRentI1003_rep));
                borrower.AddCurrentIncome(CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType.OtherTypesOfIncome, dataApp.aCTotOI_rep));
                #endregion
            }

            borrower.SetDeclaration(CreateDeclaration(dataApp, isBorrower));

            string[] dependentAges = isBorrower ? dataApp.aBDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':' ) : dataApp.aCDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':' );
            foreach (string s in dependentAges) 
            {
                borrower.AddDependent(CreateDependent(s));
            }
            #region Employer
            IEmpCollection empCollection = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
            borrower.AddEmployer(CreateEmployer(empCollection.GetPrimaryEmp(false)));

            foreach (IRegularEmploymentRecord record in empCollection.GetSubcollection(true, E_EmpGroupT.Previous)) 
            {
                borrower.AddEmployer(CreateEmployer(record));
            }
            #endregion

            borrower.SetGovernmentMonitoring(CreateGovernmentMonitoring(dataApp, isBorrower));

            if (isBorrower) 
            {
                borrower.AddPresentHousingExpense(CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType.Rent, dataApp.aPresRent_rep));
                borrower.AddPresentHousingExpense(CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType.FirstMortgagePrincipalAndInterest, dataApp.aPres1stM_rep));
                borrower.AddPresentHousingExpense(CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, dataApp.aPresOFin_rep));
                borrower.AddPresentHousingExpense(CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType.HazardInsurance, dataApp.aPresHazIns_rep));
                borrower.AddPresentHousingExpense(CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType.RealEstateTax, dataApp.aPresRealETx_rep));
                borrower.AddPresentHousingExpense(CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType.MI, dataApp.aPresMIns_rep));
                borrower.AddPresentHousingExpense(CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, dataApp.aPresHoAssocDues_rep));
                borrower.AddPresentHousingExpense(CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType.OtherHousingExpense, dataApp.aPresOHExp_rep));

            }
            if (isBorrower && m_dataLoan.sLT == E_sLT.FHA) 
            {
                borrower.SetFhaBorrower(CreateFhaBorrower(dataApp));
            }
            borrower.AddContactPoint(CreateContactPoint(Mismo.MismoXmlElement.E_XEContactPointRoleType.Work, Mismo.MismoXmlElement.E_XEContactPointType.Phone, isBorrower ? dataApp.aBBusPhone : dataApp.aCBusPhone));
            borrower.AddContactPoint(CreateContactPoint(Mismo.MismoXmlElement.E_XEContactPointRoleType.Mobile, Mismo.MismoXmlElement.E_XEContactPointType.Phone, isBorrower ? dataApp.aBCellPhone : dataApp.aCCellPhone));
            borrower.AddContactPoint(CreateContactPoint(Mismo.MismoXmlElement.E_XEContactPointRoleType.Home, Mismo.MismoXmlElement.E_XEContactPointType.Fax, isBorrower ? dataApp.aBFax : dataApp.aCFax));
            borrower.AddContactPoint(CreateContactPoint(Mismo.MismoXmlElement.E_XEContactPointRoleType.Home, Mismo.MismoXmlElement.E_XEContactPointType.Email, isBorrower ? dataApp.aBEmail : dataApp.aCEmail));
            borrower.SequenceIdentifier = borrowerSequenceIdentifier.ToString();

            //            borrower.SetFhaVaBorrower();
            //            borrower.AddSummary();
            //            borrower.SetVaBorrower();

            //            borrower.SetNearestLivingRelative();
            //            borrower.AddNonPersonEntityDetail();
            //            borrower.SetPowerOfAttorney();
            //            borrower.AddContactPoint();


            //            borrower.ApplicationSignedDate;
            //            borrower.CreditReportIdentifier;
            //            borrower.BorrowerNonObligatedIndicator;
            //            borrower.NonPersonEntityIndicator;
            //            borrower.RelationshipTitleType;
            //            borrower.RelationshipTitleTypeOtherDescription;
            //            borrower.UnparsedName;

            return borrower;
        }
        private Mismo.MismoXmlElement.XEContactPoint CreateContactPoint(Mismo.MismoXmlElement.E_XEContactPointRoleType roleType, Mismo.MismoXmlElement.E_XEContactPointType type, string value) 
        {
            if (null == value || "" == value)
                return null;

            Mismo.MismoXmlElement.XEContactPoint contactPoint = new Mismo.MismoXmlElement.XEContactPoint();
            contactPoint.Type = type;
            contactPoint.RoleType = roleType;
            contactPoint.Value = value;
            return contactPoint;
        }
        private Mismo.MismoXmlElement.XEPresentHousingExpense CreatePresentHousingExpense(Mismo.MismoXmlElement.E_PresentHousingExpenseType type, string amount) 
        {
            if ("" == amount || "0.00" == amount || "$0.00" == amount)
                return null;

            Mismo.MismoXmlElement.XEPresentHousingExpense presentHousingExpense = new Mismo.MismoXmlElement.XEPresentHousingExpense();
            presentHousingExpense.HousingExpenseType = type;
            presentHousingExpense.PaymentAmount = amount;
            return presentHousingExpense;
        }
        private Mismo.MismoXmlElement.XEFhaBorrower CreateFhaBorrower(CAppData dataApp) 
        {
            Mismo.MismoXmlElement.XEFhaBorrower fhaBorrower = new Mismo.MismoXmlElement.XEFhaBorrower();

            fhaBorrower.CertificationOriginalMortgageAmount = dataApp.aFHABorrCertOtherPropOrigMAmt_rep;
            fhaBorrower.CertificationOwn4OrMoreDwellingsIndicator = ToMismo(dataApp.aFHABorrCertOwnMoreThan4DwellingsTri);
            fhaBorrower.CertificationOwnOtherPropertyIndicator = ToMismo(dataApp.aFHABorrCertOwnOrSoldOtherFHAPropTri);
            fhaBorrower.CertificationPropertySoldCity = dataApp.aFHABorrCertOtherPropCity;
            fhaBorrower.CertificationPropertySoldPostalCode = dataApp.aFHABorrCertOtherPropZip;
            fhaBorrower.CertificationPropertySoldState = dataApp.aFHABorrCertOtherPropState;
            fhaBorrower.CertificationPropertySoldStreetAddress = dataApp.aFHABorrCertOtherPropStAddr;
            fhaBorrower.CertificationPropertyToBeSoldIndicator = ToMismo(dataApp.aFHABorrCertOtherPropToBeSoldTri);
            fhaBorrower.CertificationSalesPriceAmount = dataApp.aFHABorrCertOtherPropSalesPrice_rep;
            //            fhaBorrower.CertificationLeadPaintIndicator;
            //            fhaBorrower.CertificationRentalIndicator;

            return fhaBorrower;
        }
        private Mismo.MismoXmlElement.XEGovernmentMonitoring CreateGovernmentMonitoring(CAppData dataApp, bool isBorrower) 
        {
            Mismo.MismoXmlElement.XEGovernmentMonitoring governmentMonitoring = new Mismo.MismoXmlElement.XEGovernmentMonitoring();

            governmentMonitoring.HmdaEthnicityType = ToMismo(isBorrower ? dataApp.aBHispanicTFallback : dataApp.aCHispanicTFallback);
            governmentMonitoring.GenderType = ToMismo(isBorrower ? dataApp.aBGenderFallback : dataApp.aCGenderFallback);
            governmentMonitoring.RaceNationalOriginRefusalIndicator = ToMismo(isBorrower ? dataApp.aBNoFurnish : dataApp.aCNoFurnish);
                
            if ((isBorrower && dataApp.aBIsAmericanIndian) || (!isBorrower && dataApp.aCIsAmericanIndian))
                governmentMonitoring.AddHmdaRace(CreateHmdaRace(Mismo.MismoXmlElement.E_HmdaRaceType.AmericanIndianOrAlaskaNative));
                
            if ((isBorrower && dataApp.aBIsAsian) || (!isBorrower && dataApp.aCIsAsian))
                governmentMonitoring.AddHmdaRace(CreateHmdaRace(Mismo.MismoXmlElement.E_HmdaRaceType.Asian));

            if ((isBorrower && dataApp.aBIsBlack) || (!isBorrower && dataApp.aCIsBlack))
                governmentMonitoring.AddHmdaRace(CreateHmdaRace(Mismo.MismoXmlElement.E_HmdaRaceType.BlackOrAfricanAmerican));

            if ((isBorrower && dataApp.aBIsPacificIslander) || (!isBorrower && dataApp.aCIsPacificIslander))
                governmentMonitoring.AddHmdaRace(CreateHmdaRace(Mismo.MismoXmlElement.E_HmdaRaceType.NativeHawaiianOrOtherPacificIslander));

            if ((isBorrower && dataApp.aBIsWhite) || (!isBorrower && dataApp.aCIsWhite))
                governmentMonitoring.AddHmdaRace(CreateHmdaRace(Mismo.MismoXmlElement.E_HmdaRaceType.White));

            //            governmentMonitoring.OtherRaceNationalOriginDescription;
            //            governmentMonitoring.RaceNationalOriginType;

            return governmentMonitoring;
        }
        private Mismo.MismoXmlElement.XEHmdaRace CreateHmdaRace(Mismo.MismoXmlElement.E_HmdaRaceType type) 
        {
            Mismo.MismoXmlElement.XEHmdaRace hmdaRace = new Mismo.MismoXmlElement.XEHmdaRace();
            hmdaRace.Type = type;
            return hmdaRace;
        }
        private Mismo.MismoXmlElement.XEEmployer CreateEmployer(IPrimaryEmploymentRecord record)
        {
            if (null == record)
                return null;

            Mismo.MismoXmlElement.XEEmployer employer = new Mismo.MismoXmlElement.XEEmployer();
            employer.Name = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.PostalCode = record.EmplrZip;
            employer.TelephoneNumber = record.EmplrBusPhone;
            employer.CurrentEmploymentMonthsOnJob =  record.EmplmtLenInMonths_rep;
            employer.CurrentEmploymentYearsOnJob = record.EmplmtLenInYrs_rep;
            employer.CurrentEmploymentTimeInLineOfWorkYears = record.ProfLen_rep;
            employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            employer.EmploymentCurrentIndicator = Mismo.Common.E_YesNoIndicator.Yes;
            employer.EmploymentPositionDescription = record.JobTitle;
            employer.EmploymentPrimaryIndicator = Mismo.Common.E_YesNoIndicator.Yes;
            //            employer.IncomeEmploymentMonthlyAmount;
            //            employer.PreviousEmploymentEndDate;
            //            employer.PreviousEmploymentStartDate;
            return employer;
        }
        private Mismo.MismoXmlElement.XEEmployer CreateEmployer(IRegularEmploymentRecord record) 
        {
            if (null == record)
                return null;

            Mismo.MismoXmlElement.XEEmployer employer = new Mismo.MismoXmlElement.XEEmployer();
            employer.Name = record.EmplrNm;
            employer.StreetAddress = record.EmplrAddr;
            employer.City = record.EmplrCity;
            employer.State = record.EmplrState;
            employer.PostalCode = record.EmplrZip;
            employer.TelephoneNumber = record.EmplrBusPhone;
            employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            employer.EmploymentPositionDescription = record.JobTitle;
            employer.EmploymentPrimaryIndicator = ToMismo(record.IsPrimaryEmp);
            employer.IncomeEmploymentMonthlyAmount = record.MonI_rep;
            employer.PreviousEmploymentEndDate = record.EmplmtEndD_rep;
            employer.PreviousEmploymentStartDate = record.EmplmtStartD_rep;
            //            employer.EmploymentCurrentIndicator = Mismo.Common.E_YesNoIndicator.No;
            //            employer.CurrentEmploymentMonthsOnJob =  record.EmplmtLenInMonths_rep;
            //            employer.CurrentEmploymentYearsOnJob = record.EmplmtLenInYrs_rep;
            //            employer.CurrentEmploymentTimeInLineOfWorkYears = record.ProfLen_rep;

            return employer;

        }
        private Mismo.MismoXmlElement.XEDependent CreateDependent(string s) 
        {
            Mismo.MismoXmlElement.XEDependent dependent = new Mismo.MismoXmlElement.XEDependent();
            dependent.AgeYears = s;
            return dependent;
        }
        private Mismo.MismoXmlElement.XEDeclaration CreateDeclaration(CAppData dataApp, bool isBorrower) 
        {
            Mismo.MismoXmlElement.XEDeclaration declaration = new Mismo.MismoXmlElement.XEDeclaration();

            declaration.AlimonyChildSupportObligationIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecAlimony : dataApp.aCDecAlimony);
            declaration.BankruptcyIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecBankrupt : dataApp.aCDecBankrupt);
            declaration.BorrowedDownPaymentIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecBorrowing : dataApp.aCDecBorrowing);
            declaration.CoMakerEndorserOfNoteIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecEndorser : dataApp.aCDecEndorser);
            declaration.LoanForeclosureOrJudgementIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecObligated : dataApp.aCDecObligated);
            declaration.OutstandingJudgementsIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecJudgment : dataApp.aCDecJudgment);
            declaration.PartyToLawsuitIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecLawsuit : dataApp.aCDecLawsuit);
            declaration.PresentlyDelinquentIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecDelinquent : dataApp.aCDecDelinquent);
            declaration.PropertyForeclosedPastSevenYearsIndicator = ToMismoFromDeclaration(isBorrower ? dataApp.aBDecForeclosure : dataApp.aCDecForeclosure);
            declaration.BorrowerFirstTimeHomebuyerIndicator = ToMismo(m_dataLoan.sHas1stTimeBuyer);

            declaration.PriorPropertyTitleType = ToMismo(isBorrower ? dataApp.aBDecPastOwnedPropTitleT : (E_aBDecPastOwnedPropTitleT) dataApp.aCDecPastOwnedPropTitleT);
            declaration.PriorPropertyUsageType = ToMismo(isBorrower ? dataApp.aBDecPastOwnedPropT : (E_aBDecPastOwnedPropT) dataApp.aCDecPastOwnedPropT);

            string aDecCitizen = isBorrower ? dataApp.aBDecCitizen : dataApp.aCDecCitizen;
            string aDecResidency = isBorrower ? dataApp.aBDecResidency : dataApp.aCDecResidency;
            string aDecForeignNational = isBorrower ? dataApp.aBDecForeignNational : "";
            declaration.CitizenshipResidencyType = ToMismoDeclarationCitizenship(aDecCitizen, aDecResidency, aDecForeignNational);

            string aBDecPastOwnership = isBorrower ? dataApp.aBDecPastOwnership : dataApp.aCDecPastOwnership;
            switch (aBDecPastOwnership.ToUpper()) 
            {
                case "Y":
                    declaration.HomeownerPastThreeYearsType = Mismo.MismoXmlElement.E_DeclarationHomeownerPastThreeYearsType.Yes;
                    break;
                case "N":
                    declaration.HomeownerPastThreeYearsType = Mismo.MismoXmlElement.E_DeclarationHomeownerPastThreeYearsType.No;
                    break;
                default:
                    declaration.HomeownerPastThreeYearsType = Mismo.MismoXmlElement.E_DeclarationHomeownerPastThreeYearsType.Undefined;
                    break;

            }

            string aDecOcc = isBorrower ? dataApp.aBDecOcc : dataApp.aCDecOcc;
            switch (aDecOcc.ToUpper()) 
            {
                case "Y":
                    declaration.IntentToOccupyType = Mismo.MismoXmlElement.E_DeclarationIntentToOccupyType.Yes;
                    break;
                case "N":
                    declaration.IntentToOccupyType = Mismo.MismoXmlElement.E_DeclarationIntentToOccupyType.No;
                    break;
                default:
                    declaration.IntentToOccupyType = Mismo.MismoXmlElement.E_DeclarationIntentToOccupyType.Undefined;
                    break;
            }

            return declaration;
        }
        private Mismo.MismoXmlElement.XECurrentIncome CreateCurrentIncome(Mismo.MismoXmlElement.E_XECurrentIncomeType incomeType, string monthlyTotalAmount) 
        {
            if (monthlyTotalAmount == "")
                return null;

            Mismo.MismoXmlElement.XECurrentIncome currentIncome = new Mismo.MismoXmlElement.XECurrentIncome();
            currentIncome.IncomeType = incomeType;
            currentIncome.MonthlyTotalAmount = monthlyTotalAmount;
            
            return currentIncome;
        }
        private Mismo.Closing.XEResidence CreateResidence(string addr, string city, string st, string zip, E_aBAddrT addrT, string addrYrs, bool isCurrent) 
        {
            if (addr.TrimWhitespaceAndBOM() == "" && city.TrimWhitespaceAndBOM() == "" && zip.TrimWhitespaceAndBOM() == "" && st.TrimWhitespaceAndBOM() == "")
                return null; // No street address ignore.

            YearMonthParser parser = new YearMonthParser();
            parser.Parse(addrYrs);

            Mismo.Closing.XEResidence residence = new Mismo.Closing.XEResidence();
            residence.BorrowerResidencyBasisType = ToMismo(addrT);
            residence.BorrowerResidencyDurationMonths = parser.NumberOfMonths.ToString();
            residence.BorrowerResidencyDurationYears = parser.NumberOfYears.ToString();
            residence.BorrowerResidencyType = isCurrent ? Mismo.Closing.E_ResidenceBorrowerResidencyType.Current : Mismo.Closing.E_ResidenceBorrowerResidencyType.Prior;
            residence.City = city;
            //            residence.Country;
            residence.PostalCode = zip;
            residence.State = st;
            residence.StreetAddress = addr;
            return residence;

        }
        private Mismo.Closing.XEMailTo CreateMailTo(CAppData dataApp, bool isBorrower) 
        {
            Mismo.Closing.XEMailTo mailTo = new Mismo.Closing.XEMailTo();

            bool aAddrMailUsePresentAddr = isBorrower ? dataApp.aBAddrMailUsePresentAddr : dataApp.aCAddrMailUsePresentAddr;
            mailTo.AddressSameAsPropertyIndicator = ToMismo(aAddrMailUsePresentAddr);
            mailTo.City = isBorrower ? dataApp.aBCityMail : dataApp.aCCityMail;
            mailTo.PostalCode = isBorrower ? dataApp.aBZipMail : dataApp.aCZipMail;
            mailTo.State = isBorrower ? dataApp.aBStateMail : dataApp.aCStateMail;
            mailTo.StreetAddress = isBorrower ? dataApp.aBAddrMail : dataApp.aCAddrMail;
            //            mailTo.StreetAddress2;
            //            mailTo.Country;

            return mailTo;
        }
        private Mismo.MismoXmlElement.XEDataInformation CreateDataInformation() 
        {
            return null;
        }

        private Mismo.MismoXmlElement.XEReoProperty CreateReoProperty(CAppData dataApp, IRealEstateOwned field, int borrowerIndex) 
        {
            Mismo.MismoXmlElement.XEReoProperty reoProperty = new Mismo.MismoXmlElement.XEReoProperty();

            reoProperty.ReoId = "REO_" + field.RecordId.ToString("N");
            reoProperty.BorrowerId = field.ReOwnerT == E_ReOwnerT.CoBorrower ? "C" + borrowerIndex : "B" + borrowerIndex;

            Guid[] matchedLiabilities = dataApp.FindMatchedLiabilities(field.RecordId);
            string str = "";
            foreach (Guid id in matchedLiabilities) 
            {
                str += "LIA_" + id.ToString("N")  + " ";
            }
            if (matchedLiabilities.Length > 0) 
            {
                reoProperty.LiabilityId = str.TrimWhitespaceAndBOM();
            }
            reoProperty.StreetAddress = field.Addr;
            reoProperty.City = field.City;
            reoProperty.State = field.State;
            reoProperty.PostalCode = field.Zip;
            reoProperty.GsePropertyType = ToMismoReoType(field.TypeT);
            reoProperty.DispositionStatusType = ToMismo(field.StatT);
            reoProperty.LienInstallmentAmount = field.MPmt_rep;
            reoProperty.LienUPBAmount = field.MAmt_rep;
            reoProperty.MaintenanceExpenseAmount = field.HExp_rep;
            reoProperty.MarketValueAmount = field.Val_rep;
            reoProperty.RentalIncomeGrossAmount = field.GrossRentI_rep;
            reoProperty.RentalIncomeNetAmount = field.NetRentI_rep;
            reoProperty.SubjectIndicator = ToMismo(field.IsSubjectProp);
            //            reoProperty.LienUPBAmount;
            //            reoProperty.CurrentResidenceIndicator;

            return reoProperty;
        }

        private Mismo.MismoXmlElement.XELoanPurpose CreateLoanPurpose() 
        {
            Mismo.MismoXmlElement.XELoanPurpose loanPurpose = new Mismo.MismoXmlElement.XELoanPurpose();

            CAppData dataApp = m_dataLoan.GetAppData(0);

            loanPurpose.GseTitleMannerHeldDescription   = dataApp.aManner;
            loanPurpose.OtherLoanPurposeDescription     = m_dataLoan.sOLPurposeDesc;
            loanPurpose.PropertyLeaseholdExpirationDate = m_dataLoan.sLeaseHoldExpireD_rep;
            loanPurpose.PropertyRightsType              = ToMismo(m_dataLoan.sEstateHeldT);
            loanPurpose.PropertyUsageType               = ToMismo(dataApp.aOccT);
            loanPurpose.Type                            = ToMismo(m_dataLoan.sLPurposeT);
            
            Mismo.MismoXmlElement.XEConstructionRefinanceData constructionRefinanceData = CreateConstructionRefinanceData();
            loanPurpose.SetConstructionRefinanceData(constructionRefinanceData);

            return loanPurpose;
        }

        private Mismo.MismoXmlElement.XEConstructionRefinanceData CreateConstructionRefinanceData() 
        {
            Mismo.MismoXmlElement.XEConstructionRefinanceData constructionRefinanceData = null;
            switch (m_dataLoan.sLPurposeT) 
            {
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    constructionRefinanceData = new Mismo.MismoXmlElement.XEConstructionRefinanceData();
                    constructionRefinanceData.GSERefinancePurposeType = ToMismoGseRefinancePurposeType(m_dataLoan.sRefPurpose);
                    constructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sSpAcqYr;
                    constructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sSpLien_rep;
                    constructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sSpOrigC_rep;
                    constructionRefinanceData.RefinanceImprovementCostsAmount = m_dataLoan.sSpImprovC_rep;
                    constructionRefinanceData.RefinanceImprovementsType = ToMismo(m_dataLoan.sSpImprovTimeFrameT);
                    constructionRefinanceData.RefinanceProposedImprovementsDescription = m_dataLoan.sSpImprovDesc;
                    break;
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    constructionRefinanceData = new Mismo.MismoXmlElement.XEConstructionRefinanceData();
                    constructionRefinanceData.ConstructionImprovementCostsAmount = m_dataLoan.sLotImprovC_rep;
                    constructionRefinanceData.ConstructionPurposeType = m_dataLoan.sLPurposeT == E_sLPurposeT.Construct ? Mismo.MismoXmlElement.E_ConstructionPurposeType.ConstructionOnly : Mismo.MismoXmlElement.E_ConstructionPurposeType.ConstructionToPermanent;
                    constructionRefinanceData.LandEstimatedValueAmount = m_dataLoan.sLotVal_rep;
                    constructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sLotAcqYr;
                    constructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sLotLien_rep;
                    constructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sLotOrigC_rep;
                    break;
            }

            return constructionRefinanceData;
        }

        private Mismo.MismoXmlElement.XEAdditionalCaseData CreateAdditionalCaseData() 
        {
            Mismo.MismoXmlElement.XEAdditionalCaseData additionalCaseData = new Mismo.MismoXmlElement.XEAdditionalCaseData();

            additionalCaseData.SetTransmittalData(CreateTransmittalData());

            return additionalCaseData;
        }
        private Mismo.MismoXmlElement.XETransmittalData CreateTransmittalData() 
        {
            Mismo.MismoXmlElement.XETransmittalData transmittalData = new Mismo.MismoXmlElement.XETransmittalData();

            transmittalData.LoanOriginationSystemLoanIdentifier = m_dataLoan.sLNm;
            transmittalData.PropertyAppraisedValueAmount = m_dataLoan.sApprVal_rep;
            transmittalData.CurrentFirstMortgageHolderType = ToMismo(m_dataLoan.s1stMOwnerT);
            transmittalData.RateLockPeriodDays = m_dataLoan.sRLckdDays_rep;
            transmittalData.BuydownRatePercent = m_dataLoan.sBuydownResultIR_rep;
            //            transmittalData.RateLockRequestedExtensionDays;

            //            transmittalData.ArmsLengthIndicator;
            //            transmittalData.BelowMarketSubordinateFinancingIndicator;
            //            transmittalData.CaseStateType;
            //            transmittalData.CommitmentReferenceIdentifier;
            //            transmittalData.ConcurrentOriginationIndicator;
            //            transmittalData.ConcurrentOriginationLenderIndicator;
            //            transmittalData.CreditReportAuthorizationIndicator;
            //            transmittalData.InvestorInstitutionIdentifier;
            //            transmittalData.InvestorLoanIdentifier;
            //            transmittalData.LenderBranchIdentifier;
            //            transmittalData.LenderRegistrationIdentifier;
            //            transmittalData.PropertyEstimatedValueAmount;
            //            transmittalData.RateLockType;
            return transmittalData;
        }
        private Mismo.MismoXmlElement.XEDownPayment CreateDownpayment() 
        {
            Mismo.MismoXmlElement.XEDownPayment downPayment = null;

            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase) 
            {
                // 9/18/2007 dd - Downpayment only valid for loan purpose is purchase.
                downPayment = new Mismo.MismoXmlElement.XEDownPayment();
                downPayment.Amount = m_dataLoan.sEquityCalc_rep;
                downPayment.SourceDescription = m_dataLoan.sDwnPmtSrcExplain;
                downPayment.Type = ToMismoDownPaymentType(m_dataLoan.sDwnPmtSrc);
            }
            return downPayment;
        }
        private Mismo.MismoXmlElement.XEAsset CreateAsset(IAssetBusiness business, int borrowerIndex) 
        {
            if (null == business)
                return null;

            Mismo.MismoXmlElement.XEAsset asset = new Mismo.MismoXmlElement.XEAsset();

            asset.BorrowerID = "B" + borrowerIndex;
            asset.Type = Mismo.MismoXmlElement.E_XEAssetType.NetWorthOfBusinessOwned;
            asset.CashOrMarketValueAmount = business.Val_rep;
            return asset;
        }
        private Mismo.MismoXmlElement.XEAsset CreateAsset(IAssetRetirement retirement, int borrowerIndex) 
        {
            if (null == retirement)
                return null;

            Mismo.MismoXmlElement.XEAsset asset = new Mismo.MismoXmlElement.XEAsset();

            asset.BorrowerID = "B" + borrowerIndex;
            asset.Type = Mismo.MismoXmlElement.E_XEAssetType.RetirementFund;
            asset.CashOrMarketValueAmount = retirement.Val_rep;
            return asset;
        }
        private Mismo.MismoXmlElement.XEAsset CreateAsset(IAssetLifeInsurance lifeIns, int borrowerIndex) 
        {
            if (null == lifeIns)
                return null;

            Mismo.MismoXmlElement.XEAsset asset = new Mismo.MismoXmlElement.XEAsset();

            asset.BorrowerID = "B" + borrowerIndex;
            asset.Type = Mismo.MismoXmlElement.E_XEAssetType.LifeInsurance;
            asset.CashOrMarketValueAmount = lifeIns.Val_rep;
            asset.LifeInsuranceFaceValueAmount = lifeIns.FaceVal_rep;
            return asset;
        }
        private Mismo.MismoXmlElement.XEAsset CreateAsset(IAssetCashDeposit cashDeposit, int borrowerIndex) 
        {
            if (null == cashDeposit)
                return null;

            Mismo.MismoXmlElement.XEAsset asset = new Mismo.MismoXmlElement.XEAsset();

            asset.BorrowerID = "B" + borrowerIndex;
            asset.Type = Mismo.MismoXmlElement.E_XEAssetType.EarnestMoneyCashDepositTowardPurchase;
            asset.CashOrMarketValueAmount = cashDeposit.Val_rep;
            asset.HolderName = cashDeposit.Desc;
            return asset;
        }
        private Mismo.MismoXmlElement.XEAsset CreateAsset(IAssetRegular assetField, int borrowerIndex) 
        {
            if (null == assetField)
                return null;

            Mismo.MismoXmlElement.XEAsset asset = new Mismo.MismoXmlElement.XEAsset();
            
            asset.BorrowerID = assetField.OwnerT == E_AssetOwnerT.CoBorrower ?  "C" + borrowerIndex : "B" + borrowerIndex;
            asset.AccountIdentifier = assetField.AccNum.Value;
            asset.CashOrMarketValueAmount = assetField.Val_rep;
            asset.Type = ToMismo(assetField.AssetT);
            asset.HolderName = assetField.ComNm;
            asset.HolderStreetAddress = assetField.StAddr;
            asset.HolderCity = assetField.City;
            asset.HolderState = assetField.State;
            asset.HolderPostalCode = assetField.Zip;
            asset.OtherAssetTypeDescription = asset.OtherAssetTypeDescription;
            if (assetField.AssetT == E_AssetRegularT.Auto) 
            {
                asset.AutomobileMakeDescription = assetField.Desc;
            }
            //            asset.VerifiedIndicator;
            //            asset.AutomobileMakeDescription =;
            //            asset.AutomobileModelYear;
            //            asset.LifeInsuranceFaceValueAmount;
            //            asset.StockBondMutualFundShareCount;
            return asset;
        }
        private Mismo.MismoXmlElement.XELiability CreateLiability(ILiabilityAlimony alimony, int borrowerIndex) 
        {
            if (null == alimony)
                return null;

            Mismo.MismoXmlElement.XELiability liability = new Mismo.MismoXmlElement.XELiability();
            liability.Id = "LIA_" + alimony.RecordId.ToString("N");
            liability.BorrowerId = "B" + borrowerIndex;
            liability.Type = Mismo.MismoXmlElement.E_LiabilityType.Alimony;
            liability.AlimonyOwedToName = alimony.OwedTo;
            liability.RemainingTermMonths = alimony.RemainMons_rep;
            liability.MonthlyPaymentAmount = alimony.Pmt_rep;
            return liability;
        }
        private Mismo.MismoXmlElement.XELiability CreateLiability(ILiabilityChildSupport childSupport, int borrowerIndex)
        {
            if (null == childSupport)
                return null;

            Mismo.MismoXmlElement.XELiability liability = new Mismo.MismoXmlElement.XELiability();
            liability.Id = "LIA_" + childSupport.RecordId.ToString("N");
            liability.BorrowerId = "B" + borrowerIndex;
            liability.Type = Mismo.MismoXmlElement.E_LiabilityType.ChildSupport;
            liability.RemainingTermMonths = childSupport.RemainMons_rep;
            liability.MonthlyPaymentAmount = childSupport.Pmt_rep;
            return liability;
        }
        private Mismo.MismoXmlElement.XELiability CreateLiability(ILiabilityJobExpense jobExpense, int borrowerIndex) 
        {
            if (null == jobExpense)
                return null;

            Mismo.MismoXmlElement.XELiability liability = new Mismo.MismoXmlElement.XELiability();
            liability.Id = "LIA_" + jobExpense.RecordId.ToString("N");
            liability.BorrowerId = "B" + borrowerIndex;
            liability.Type = Mismo.MismoXmlElement.E_LiabilityType.JobRelatedExpenses;
            liability.HolderName = jobExpense.ExpenseDesc;
            liability.MonthlyPaymentAmount = jobExpense.Pmt_rep;
            return liability;
        }
        private Mismo.MismoXmlElement.XELiability CreateLiability(ILiabilityRegular field, int borrowerIndex) 
        {
            if (null == field)
                return null;

            Mismo.MismoXmlElement.XELiability liability = new Mismo.MismoXmlElement.XELiability();
            liability.Id = "LIA_" + field.RecordId.ToString("N");
            liability.BorrowerId = field.OwnerT == E_LiaOwnerT.CoBorrower ? "C" + borrowerIndex : "B" + borrowerIndex;
            if (field.DebtT == E_DebtRegularT.Mortgage && field.MatchedReRecordId != Guid.Empty) 
            {
                liability.ReoId = "REO_" + field.MatchedReRecordId.ToString("N");
            }
            liability.HolderStreetAddress = field.ComAddr;
            liability.HolderCity = field.ComCity;
            liability.HolderState = field.ComState;
            liability.HolderPostalCode = field.ComZip;
            liability.AccountIdentifier = field.AccNum.Value;
            liability.HolderName = field.ComNm;
            liability.MonthlyPaymentAmount = field.Pmt_rep;
            liability.PayoffStatusIndicator = ToMismo(field.WillBePdOff);
            liability.RemainingTermMonths = field.RemainMons_rep;
            liability.Type = ToMismo(field.DebtT);
            liability.UnpaidBalanceAmount = field.Bal_rep;
            liability.ExclusionIndicator = ToMismo(field.NotUsedInRatio);

            //            liability.PayoffWithCurrentAssetsIndicator;
            //            liability.AlimonyOwedToName;
            //            liability.SubjectLoanResubordinationIndicator;
            return liability;
        }
        /// <summary>
        /// Generates new Condensed Buydown List. Case 238101 
        /// </summary>
        /// <param name="dataLoan">loan data</param>
        /// <returns>A list of Buydown object populated with calculated increasedRate, changeFrequencyMonths, and durationMonths</returns>
        private List<Mismo.Closing.XEBuyDown> GenerateCondensedBuydown()
        {
            var buydownRows = new[]
                    {
                        //// R = Rate Reduction, Mon = Term (mths)
                        new { R = m_dataLoan.sBuydwnR1, R_rep = m_dataLoan.sBuydwnR1_rep, Mon = m_dataLoan.sBuydwnMon1, Mon_rep = m_dataLoan.sBuydwnMon1_rep, Num = "1"},
                        new { R = m_dataLoan.sBuydwnR2, R_rep = m_dataLoan.sBuydwnR2_rep, Mon = m_dataLoan.sBuydwnMon2, Mon_rep = m_dataLoan.sBuydwnMon2_rep, Num = "2"},
                        new { R = m_dataLoan.sBuydwnR3, R_rep = m_dataLoan.sBuydwnR3_rep, Mon = m_dataLoan.sBuydwnMon3, Mon_rep = m_dataLoan.sBuydwnMon3_rep, Num = "3"},
                        new { R = m_dataLoan.sBuydwnR4, R_rep = m_dataLoan.sBuydwnR4_rep, Mon = m_dataLoan.sBuydwnMon4, Mon_rep = m_dataLoan.sBuydwnMon4_rep, Num = "4"},
                        new { R = m_dataLoan.sBuydwnR5, R_rep = m_dataLoan.sBuydwnR5_rep, Mon = m_dataLoan.sBuydwnMon5, Mon_rep = m_dataLoan.sBuydwnMon5_rep, Num = "5"},
                    };

            //// Get nonempty rows. A row is nonzero if both fields have a nonzero value.
            var buydownRowsFiltered = buydownRows.Where((row) => row.R != 0 && row.Mon != 0);

            Mismo.Closing.XEBuyDown buydownResult = new Mismo.Closing.XEBuyDown();
            List<Mismo.Closing.XEBuyDown> buydownResultList = new List<Mismo.Closing.XEBuyDown>();

            decimal increaseRatePercent = 0.0M;
            int changeFrequencyMonths = 0;
            int durationMonths = 0;

            if (buydownRowsFiltered.Count() > 1)
            {
                increaseRatePercent = buydownRowsFiltered.First().R - buydownRowsFiltered.ElementAt(1).R;
                changeFrequencyMonths = buydownRowsFiltered.First().Mon;
                durationMonths = buydownRowsFiltered.First().Mon;

                for (int i = 1; i < buydownRowsFiltered.Count(); i++)
                {
                    if (i != buydownRowsFiltered.Count() - 1)
                    {
                        if ((increaseRatePercent != (buydownRowsFiltered.ElementAt(i).R - buydownRowsFiltered.ElementAt(i + 1).R))
                            || (changeFrequencyMonths != buydownRowsFiltered.ElementAt(i).Mon))
                        {
                            buydownResult.IncreaseRatePercent = (m_dataLoan.m_convertLos.ToRateStringNoPercent(increaseRatePercent).Equals("")) ? "0.000" : m_dataLoan.m_convertLos.ToRateStringNoPercent(increaseRatePercent);
                            buydownResult.ChangeFrequencyMonths = (m_dataLoan.m_convertLos.ToCountString(changeFrequencyMonths).Equals("")) ? "0" : m_dataLoan.m_convertLos.ToCountString(changeFrequencyMonths);
                            buydownResult.DurationMonths = (m_dataLoan.m_convertLos.ToCountString(durationMonths).Equals("")) ? "0" : m_dataLoan.m_convertLos.ToCountString(durationMonths);
                            buydownResultList.Add(buydownResult);

                            buydownResult = new Mismo.Closing.XEBuyDown();
                            increaseRatePercent = buydownRowsFiltered.ElementAt(i).R - buydownRowsFiltered.ElementAt(i + 1).R;
                            changeFrequencyMonths = buydownRowsFiltered.ElementAt(i).Mon;
                            durationMonths = buydownRowsFiltered.ElementAt(i).Mon;
                        }
                        else
                        {
                            durationMonths += buydownRowsFiltered.ElementAt(i).Mon;
                            buydownResult.DurationMonths = m_dataLoan.m_convertLos.ToCountString(durationMonths);
                        }
                    }
                    else
                    {
                        if (increaseRatePercent != buydownRowsFiltered.ElementAt(i).R
                            || changeFrequencyMonths != buydownRowsFiltered.ElementAt(i).Mon)
                        {
                            buydownResult.IncreaseRatePercent = (m_dataLoan.m_convertLos.ToRateStringNoPercent(increaseRatePercent).Equals("")) ? "0.000" : m_dataLoan.m_convertLos.ToRateStringNoPercent(increaseRatePercent);
                            buydownResult.ChangeFrequencyMonths = (m_dataLoan.m_convertLos.ToCountString(changeFrequencyMonths).Equals("")) ? "0" : m_dataLoan.m_convertLos.ToCountString(changeFrequencyMonths);
                            buydownResult.DurationMonths = (m_dataLoan.m_convertLos.ToCountString(durationMonths).Equals("")) ? "0" : m_dataLoan.m_convertLos.ToCountString(durationMonths);
                            buydownResultList.Add(buydownResult);

                            buydownResult = new Mismo.Closing.XEBuyDown();
                            buydownResult.IncreaseRatePercent = buydownRowsFiltered.ElementAt(i).R_rep;
                            buydownResult.ChangeFrequencyMonths = buydownRowsFiltered.ElementAt(i).Mon_rep;
                            buydownResult.DurationMonths = buydownRowsFiltered.ElementAt(i).Mon_rep;
                        }
                        else
                        {
                            durationMonths += buydownRowsFiltered.ElementAt(i).Mon;
                            buydownResult.IncreaseRatePercent = m_dataLoan.m_convertLos.ToRateStringNoPercent(increaseRatePercent);
                            buydownResult.ChangeFrequencyMonths = m_dataLoan.m_convertLos.ToCountString(changeFrequencyMonths);
                            buydownResult.DurationMonths = m_dataLoan.m_convertLos.ToCountString(durationMonths);
                        }
                        buydownResultList.Add(buydownResult);
                    }
                }
            }
            else if (buydownRowsFiltered.Count() == 1)
            {
                buydownResult.IncreaseRatePercent = buydownRowsFiltered.First().R_rep;
                buydownResult.ChangeFrequencyMonths = buydownRowsFiltered.First().Mon_rep;
                buydownResult.DurationMonths = buydownRowsFiltered.First().Mon_rep;

                buydownResultList.Add(buydownResult);
            }

            return buydownResultList;
        }
        #region Mismo Type mapping
        private Mismo.Closing.E_ClosingAgentType ToMismoClosingAgentType(E_AgentRoleT agentRoleT) 
        {
            
            switch (agentRoleT) 
            {
                case E_AgentRoleT.ClosingAgent: return Mismo.Closing.E_ClosingAgentType.ClosingAgent;
                case E_AgentRoleT.Escrow: return Mismo.Closing.E_ClosingAgentType.EscrowCompany;
                case E_AgentRoleT.Title: return Mismo.Closing.E_ClosingAgentType.TitleCompany;
                default:
                    return Mismo.Closing.E_ClosingAgentType.Undefined;
                    
            }
        }
        private Mismo.Closing.E_MortgageTermsMortgageType ToMismo(E_sLT sLT) 
        {
            switch (sLT) 
            {
                case E_sLT.Conventional: return Mismo.Closing.E_MortgageTermsMortgageType.Conventional;
                case E_sLT.FHA: return Mismo.Closing.E_MortgageTermsMortgageType.FHA;
                case E_sLT.Other: return Mismo.Closing.E_MortgageTermsMortgageType.Other;
                case E_sLT.UsdaRural: return Mismo.Closing.E_MortgageTermsMortgageType.FarmersHomeAdministration;
                case E_sLT.VA: return Mismo.Closing.E_MortgageTermsMortgageType.VA;
                default:
                    LogInvalidEnum("E_sLT", sLT);
                    return Mismo.Closing.E_MortgageTermsMortgageType.Undefined;
            }
        }
        private Mismo.Closing.E_MortgageTermsLoanAmortizationType ToMismo(E_sFinMethT method) 
        {
            switch (method) 
            {
                case E_sFinMethT.ARM: return Mismo.Closing.E_MortgageTermsLoanAmortizationType.AdjustableRate;
                case E_sFinMethT.Fixed: return Mismo.Closing.E_MortgageTermsLoanAmortizationType.Fixed;
                case E_sFinMethT.Graduated: return Mismo.Closing.E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage;
                default:
                    LogInvalidEnum("E_sFinMethT", method);
                    return Mismo.Closing.E_MortgageTermsLoanAmortizationType.Undefined;

            }
        }
        private Mismo.Closing.E_LoanFeaturesGsePropertyType ToMismo(E_sGseSpT sGseSpT) 
        {
            switch (sGseSpT) 
            {
                case E_sGseSpT.Attached: return Mismo.Closing.E_LoanFeaturesGsePropertyType.Attached;
                case E_sGseSpT.Condominium: return Mismo.Closing.E_LoanFeaturesGsePropertyType.Condominium;
                case E_sGseSpT.Cooperative: return Mismo.Closing.E_LoanFeaturesGsePropertyType.Cooperative;
                case E_sGseSpT.Detached: return Mismo.Closing.E_LoanFeaturesGsePropertyType.Detached;
                case E_sGseSpT.DetachedCondominium: return Mismo.Closing.E_LoanFeaturesGsePropertyType.DetachedCondominium;
                case E_sGseSpT.HighRiseCondominium: return Mismo.Closing.E_LoanFeaturesGsePropertyType.HighRiseCondominium;
                case E_sGseSpT.LeaveBlank: return Mismo.Closing.E_LoanFeaturesGsePropertyType.Undefined;
                case E_sGseSpT.ManufacturedHomeCondominium: return Mismo.Closing.E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominium;
                case E_sGseSpT.ManufacturedHomeMultiwide: return Mismo.Closing.E_LoanFeaturesGsePropertyType.ManufacturedHousingMultiWide;
                case E_sGseSpT.ManufacturedHousing: return Mismo.Closing.E_LoanFeaturesGsePropertyType.ManufacturedHousing;
                case E_sGseSpT.ManufacturedHousingSingleWide: return Mismo.Closing.E_LoanFeaturesGsePropertyType.ManufacturedHousingSingleWide;
                case E_sGseSpT.Modular: return Mismo.Closing.E_LoanFeaturesGsePropertyType.Modular;
                case E_sGseSpT.PUD: return Mismo.Closing.E_LoanFeaturesGsePropertyType.PUD;
                default:
                    LogInvalidEnum("E_sGseSpT", sGseSpT);
                    return Mismo.Closing.E_LoanFeaturesGsePropertyType.Undefined;
            }
        }

        private Mismo.Closing.E_LoanFeaturesLienPriorityType ToMismo(E_sLienPosT lienPosT) 
        {
            switch (lienPosT) 
            {
                case E_sLienPosT.First: return Mismo.Closing.E_LoanFeaturesLienPriorityType.FirstLien;
                case E_sLienPosT.Second: return Mismo.Closing.E_LoanFeaturesLienPriorityType.SecondLien;
                default:
                    LogInvalidEnum("E_sLienPosT", lienPosT);
                    return Mismo.Closing.E_LoanFeaturesLienPriorityType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_ApplicationTakenMethodType ToMismo(E_aIntrvwrMethodT method) 
        {
            switch (method) 
            {
                case E_aIntrvwrMethodT.ByMail: return Mismo.MismoXmlElement.E_ApplicationTakenMethodType.Mail;
                case E_aIntrvwrMethodT.ByTelephone: return Mismo.MismoXmlElement.E_ApplicationTakenMethodType.Telephone;
                case E_aIntrvwrMethodT.FaceToFace: return Mismo.MismoXmlElement.E_ApplicationTakenMethodType.FaceToFace;
                case E_aIntrvwrMethodT.Internet: return Mismo.MismoXmlElement.E_ApplicationTakenMethodType.Internet;
                case E_aIntrvwrMethodT.LeaveBlank: return Mismo.MismoXmlElement.E_ApplicationTakenMethodType.Undefined;
                default:
                    LogInvalidEnum("E_aIntrvwrMethodT", method);
                    return Mismo.MismoXmlElement.E_ApplicationTakenMethodType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_HmdaPreapprovalType ToMismo(E_sHmdaPreapprovalT hmdaPreapprovalT) 
        {
            switch (hmdaPreapprovalT) 
            {
                case E_sHmdaPreapprovalT.LeaveBlank: return Mismo.MismoXmlElement.E_HmdaPreapprovalType.Undefined;
                case E_sHmdaPreapprovalT.NotApplicable: return Mismo.MismoXmlElement.E_HmdaPreapprovalType.NotApplicable;
                case E_sHmdaPreapprovalT.PreapprovalNotRequested: return Mismo.MismoXmlElement.E_HmdaPreapprovalType.PreapprovalWasNotRequested;
                case E_sHmdaPreapprovalT.PreapprovalRequested: return Mismo.MismoXmlElement.E_HmdaPreapprovalType.PreapprovalWasRequested;
                default:
                    LogInvalidEnum("E_sHmdaPreapprovalT", hmdaPreapprovalT);
                    return Mismo.MismoXmlElement.E_HmdaPreapprovalType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_FhaLoanSectionOfActType ToMismoLoanSectionOfAct(string type) 
        {
            switch (type.ToLower()) 
            {
                case "203(b)": return Mismo.MismoXmlElement.E_FhaLoanSectionOfActType._203B;
                case "203(b)2": return Mismo.MismoXmlElement.E_FhaLoanSectionOfActType._203B2;
                case "203(b)/251": return Mismo.MismoXmlElement.E_FhaLoanSectionOfActType._203B251;
                case "203(k)": return Mismo.MismoXmlElement.E_FhaLoanSectionOfActType._203K;
                case "203(k)/251": return Mismo.MismoXmlElement.E_FhaLoanSectionOfActType._203K251;
                case "234(c)": return Mismo.MismoXmlElement.E_FhaLoanSectionOfActType._234C;
                case "234(c)/251": return Mismo.MismoXmlElement.E_FhaLoanSectionOfActType._234C251;
                default: return Mismo.MismoXmlElement.E_FhaLoanSectionOfActType.Undefined;
            }
        }
        private Mismo.Common.E_YesNoIndicator ToMismo(E_TriState value) 
        {
            switch (value) 
            {
                case E_TriState.Yes: return Mismo.Common.E_YesNoIndicator.Yes;
                case E_TriState.No: return Mismo.Common.E_YesNoIndicator.No;
                case E_TriState.Blank:
                    return Mismo.Common.E_YesNoIndicator.Undefined;
                default:
                    LogInvalidEnum("E_TriState", value);
                    return Mismo.Common.E_YesNoIndicator.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_GovernmentMonitoringGenderType ToMismo(E_GenderT genderT) 
        {
            switch (genderT) 
            {
                case E_GenderT.Female: return Mismo.MismoXmlElement.E_GovernmentMonitoringGenderType.Female;
                case E_GenderT.Male: return Mismo.MismoXmlElement.E_GovernmentMonitoringGenderType.Male;
                case E_GenderT.LeaveBlank: return Mismo.MismoXmlElement.E_GovernmentMonitoringGenderType.Undefined;
                case E_GenderT.NA: return Mismo.MismoXmlElement.E_GovernmentMonitoringGenderType.NotApplicable;
                case E_GenderT.Unfurnished: return Mismo.MismoXmlElement.E_GovernmentMonitoringGenderType.InformationNotProvidedUnknown;
                default:
                    LogInvalidEnum("E_GenderT", genderT);
                    return Mismo.MismoXmlElement.E_GovernmentMonitoringGenderType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_GovernmentMonitoringHmdaEthnicityType ToMismo(E_aHispanicT aHispanicT) 
        {
            switch (aHispanicT) 
            {
                case E_aHispanicT.Hispanic: return Mismo.MismoXmlElement.E_GovernmentMonitoringHmdaEthnicityType.HispanicOrLatino;
                case E_aHispanicT.LeaveBlank: return Mismo.MismoXmlElement.E_GovernmentMonitoringHmdaEthnicityType.Undefined;
                case E_aHispanicT.NotHispanic: return Mismo.MismoXmlElement.E_GovernmentMonitoringHmdaEthnicityType.NotHispanicOrLatino;
                default:
                    LogInvalidEnum("E_aHispanicT", aHispanicT);
                    return Mismo.MismoXmlElement.E_GovernmentMonitoringHmdaEthnicityType.Undefined;
            }
            
        }
        private Mismo.MismoXmlElement.E_DeclarationCitizenshipResidencyType ToMismoDeclarationCitizenship(string aBDecCitizen, string aBDecResidency, string aBDecForeignNational) 
        {
            Mismo.MismoXmlElement.E_DeclarationCitizenshipResidencyType ret = Mismo.MismoXmlElement.E_DeclarationCitizenshipResidencyType.Undefined;

            if (aBDecCitizen.ToUpper() == "Y") 
            {
                ret = Mismo.MismoXmlElement.E_DeclarationCitizenshipResidencyType.USCitizen;
            } 
            else 
            {
                switch (aBDecResidency.ToUpper()) 
                {
                    case "Y":
                        ret = Mismo.MismoXmlElement.E_DeclarationCitizenshipResidencyType.PermanentResidentAlien;
                        break;
                    case "N":
                        if (aBDecForeignNational.ToUpper() == "Y")
                            ret = Mismo.MismoXmlElement.E_DeclarationCitizenshipResidencyType.NonResidentAlien;
                        else
                            ret = Mismo.MismoXmlElement.E_DeclarationCitizenshipResidencyType.NonPermanentResidentAlien;
                        break;
                }
            }
            return ret;
        }
        private Mismo.MismoXmlElement.E_DeclarationPriorPropertyTitleType ToMismo(E_aBDecPastOwnedPropTitleT type) 
        {
            switch (type) 
            {
                case E_aBDecPastOwnedPropTitleT.Empty: return Mismo.MismoXmlElement.E_DeclarationPriorPropertyTitleType.Undefined;
                case E_aBDecPastOwnedPropTitleT.O: return Mismo.MismoXmlElement.E_DeclarationPriorPropertyTitleType.JointWithOtherThanSpouse;
                case E_aBDecPastOwnedPropTitleT.S: return Mismo.MismoXmlElement.E_DeclarationPriorPropertyTitleType.Sole;
                case E_aBDecPastOwnedPropTitleT.SP: return Mismo.MismoXmlElement.E_DeclarationPriorPropertyTitleType.JointWithSpouse;
                default:
                    LogInvalidEnum("E_aBDecPastOwnedPropTitleT", type);
                    return Mismo.MismoXmlElement.E_DeclarationPriorPropertyTitleType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_DeclarationPriorPropertyUsageType ToMismo(E_aBDecPastOwnedPropT type) 
        {
            switch (type) 
            {
                case E_aBDecPastOwnedPropT.Empty: return Mismo.MismoXmlElement.E_DeclarationPriorPropertyUsageType.Undefined;
                case E_aBDecPastOwnedPropT.IP: return Mismo.MismoXmlElement.E_DeclarationPriorPropertyUsageType.Investment;
                case E_aBDecPastOwnedPropT.PR: return Mismo.MismoXmlElement.E_DeclarationPriorPropertyUsageType.PrimaryResidence;
                case E_aBDecPastOwnedPropT.SH: return Mismo.MismoXmlElement.E_DeclarationPriorPropertyUsageType.SecondaryResidence;
                default:
                    LogInvalidEnum("E_aBDecPastOwnedPropT", type);
                    return Mismo.MismoXmlElement.E_DeclarationPriorPropertyUsageType.Undefined;
            }
        }
        private Mismo.Common.E_YesNoIndicator ToMismoFromDeclaration(string str) 
        {
            switch (str.ToUpper()) 
            {
                case "Y": return Mismo.Common.E_YesNoIndicator.Yes;
                case "N": return Mismo.Common.E_YesNoIndicator.No;
                default: return Mismo.Common.E_YesNoIndicator.Undefined;
            }
        }
        private Mismo.Closing.E_ResidenceBorrowerResidencyBasisType ToMismo(E_aBAddrT addrT) 
        {
            switch (addrT) 
            {
                case E_aBAddrT.Own: return Mismo.Closing.E_ResidenceBorrowerResidencyBasisType.Own;
                case E_aBAddrT.Rent: return Mismo.Closing.E_ResidenceBorrowerResidencyBasisType.Rent;
                case E_aBAddrT.LeaveBlank: return Mismo.Closing.E_ResidenceBorrowerResidencyBasisType.Undefined;
                default:
                    LogInvalidEnum("E_aBAddrT", addrT);
                    return Mismo.Closing.E_ResidenceBorrowerResidencyBasisType.Undefined;
            }
        }
        private Mismo.Closing.E_BorrowerMaritalStatusType ToMismo(E_aBMaritalStatT maritalStatT) 
        {
            switch (maritalStatT) 
            {
                case E_aBMaritalStatT.LeaveBlank: return Mismo.Closing.E_BorrowerMaritalStatusType.NotProvided;
                case E_aBMaritalStatT.Married: return Mismo.Closing.E_BorrowerMaritalStatusType.Married;
                case E_aBMaritalStatT.NotMarried: return Mismo.Closing.E_BorrowerMaritalStatusType.Unmarried;
                case E_aBMaritalStatT.Separated: return Mismo.Closing.E_BorrowerMaritalStatusType.Separated;
                default:
                    LogInvalidEnum("E_aBMaritalStatT", maritalStatT);
                    return Mismo.Closing.E_BorrowerMaritalStatusType.Undefined;
            }
        }
        private Mismo.Closing.E_BorrowerMaritalStatusType ToMismo(E_aCMaritalStatT maritalStatT) 
        {
            switch (maritalStatT) 
            {
                case E_aCMaritalStatT.LeaveBlank: return Mismo.Closing.E_BorrowerMaritalStatusType.NotProvided;
                case E_aCMaritalStatT.Married: return Mismo.Closing.E_BorrowerMaritalStatusType.Married;
                case E_aCMaritalStatT.NotMarried: return Mismo.Closing.E_BorrowerMaritalStatusType.Unmarried;
                case E_aCMaritalStatT.Separated: return Mismo.Closing.E_BorrowerMaritalStatusType.Separated;
                default:
                    LogInvalidEnum("E_aCMaritalStatT", maritalStatT);
                    return Mismo.Closing.E_BorrowerMaritalStatusType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_ReoPropertyDispositionStatusType ToMismo(E_ReoStatusT statusT) 
        {
            switch (statusT) 
            {
                case E_ReoStatusT.PendingSale: return Mismo.MismoXmlElement.E_ReoPropertyDispositionStatusType.PendingSale;
                case E_ReoStatusT.Rental: return Mismo.MismoXmlElement.E_ReoPropertyDispositionStatusType.RetainForRental;
                case E_ReoStatusT.Residence: return Mismo.MismoXmlElement.E_ReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence;
                case E_ReoStatusT.Sale: return Mismo.MismoXmlElement.E_ReoPropertyDispositionStatusType.Sold;
                default:
                    LogInvalidEnum("E_ReoStatusT", statusT);
                    return Mismo.MismoXmlElement.E_ReoPropertyDispositionStatusType.Undefined;

            }
        }
        private Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType ToMismoReoType(E_ReoTypeT type) 
        {
            switch (type) 
            {
                case E_ReoTypeT._2_4Plx: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.TwoToFourUnitProperty;
                case E_ReoTypeT.ComNR: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.CommercialNonResidential;
                case E_ReoTypeT.ComR: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.HomeAndBusinessCombined;
                case E_ReoTypeT.Condo: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.Condominium;
                case E_ReoTypeT.Coop: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.Cooperative;
                case E_ReoTypeT.Farm: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.Farm;
                case E_ReoTypeT.Land: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.Land;
                case E_ReoTypeT.Mixed: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.MixedUseResidential;
                case E_ReoTypeT.Mobil: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.ManufacturedMobileHome;
                case E_ReoTypeT.Multi: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits;
                case E_ReoTypeT.SFR: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.SingleFamily;
                case E_ReoTypeT.Town: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.Townhouse;
                case E_ReoTypeT.Other: return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.Undefined;
                default:
                    return Mismo.MismoXmlElement.E_ReoPropertyGsePropertyType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_XEAssetType ToMismo(E_AssetRegularT assetT) 
        {
            switch (assetT) 
            {
                case E_AssetRegularT.Auto:               return Mismo.MismoXmlElement.E_XEAssetType.Automobile;
                case E_AssetRegularT.Bonds:              return Mismo.MismoXmlElement.E_XEAssetType.Bond;
                case E_AssetRegularT.Checking:           return Mismo.MismoXmlElement.E_XEAssetType.CheckingAccount;
                case E_AssetRegularT.GiftFunds:          return Mismo.MismoXmlElement.E_XEAssetType.GiftsNotDeposited;
                case E_AssetRegularT.OtherIlliquidAsset: return Mismo.MismoXmlElement.E_XEAssetType.OtherNonLiquidAssets;
                case E_AssetRegularT.OtherLiquidAsset:   return Mismo.MismoXmlElement.E_XEAssetType.OtherLiquidAssets;
                case E_AssetRegularT.Savings:            return Mismo.MismoXmlElement.E_XEAssetType.SavingsAccount;
                case E_AssetRegularT.Stocks:             return Mismo.MismoXmlElement.E_XEAssetType.Stock;
                case E_AssetRegularT.MoneyMarketFund:    return Mismo.MismoXmlElement.E_XEAssetType.MoneyMarketFund;
                case E_AssetRegularT.TrustFunds:         return Mismo.MismoXmlElement.E_XEAssetType.TrustAccount;
                case E_AssetRegularT.BridgeLoanNotDeposited: return Mismo.MismoXmlElement.E_XEAssetType.BridgeLoanNotDeposited;
                case E_AssetRegularT.CertificateOfDeposit: return Mismo.MismoXmlElement.E_XEAssetType.CertificateOfDepositTimeDeposit;
                case E_AssetRegularT.SecuredBorrowedFundsNotDeposit: return Mismo.MismoXmlElement.E_XEAssetType.SecuredBorrowedFundsNotDeposited;
                default:
                    LogInvalidEnum("E_AssetRegularT", assetT);
                    return Mismo.MismoXmlElement.E_XEAssetType.Undefined;

            }
        }
        private Mismo.MismoXmlElement.E_LiabilityType ToMismo(E_DebtRegularT debtT) 
        {
            switch (debtT) 
            {
                case E_DebtRegularT.Installment: return Mismo.MismoXmlElement.E_LiabilityType.Installment;
                case E_DebtRegularT.Mortgage: return Mismo.MismoXmlElement.E_LiabilityType.MortgageLoan;
                case E_DebtRegularT.Open: return Mismo.MismoXmlElement.E_LiabilityType.Open30DayChargeAccount;
                case E_DebtRegularT.Other: return Mismo.MismoXmlElement.E_LiabilityType.OtherLiability;
                case E_DebtRegularT.Revolving: return Mismo.MismoXmlElement.E_LiabilityType.Revolving;
                default:
                    LogInvalidEnum("E_DebtRegularT", debtT);
                    return Mismo.MismoXmlElement.E_LiabilityType.Undefined;
            }
        }
        private Mismo.Common.E_YesNoIndicator ToMismo(bool value) 
        {
            return value ? Mismo.Common.E_YesNoIndicator.Yes : Mismo.Common.E_YesNoIndicator.No;
        }
        private Mismo.MismoXmlElement.E_DownPaymentType ToMismoDownPaymentType(string src) 
        {
            switch (src.ToLower()) 
            {
                case "checking/savings":             return Mismo.MismoXmlElement.E_DownPaymentType.CheckingSavings;
                case "gift funds":                   return Mismo.MismoXmlElement.E_DownPaymentType.GiftFunds;
                case "stocks & bonds":               return Mismo.MismoXmlElement.E_DownPaymentType.StocksAndBonds;
                case "lot equity":                   return Mismo.MismoXmlElement.E_DownPaymentType.LotEquity;
                case "bridge loan":                  return Mismo.MismoXmlElement.E_DownPaymentType.BridgeLoan;
                case "trust funds":                  return Mismo.MismoXmlElement.E_DownPaymentType.TrustFunds;
                case "retirement funds":             return Mismo.MismoXmlElement.E_DownPaymentType.RetirementFunds;
                case "life insurance cash value":    return Mismo.MismoXmlElement.E_DownPaymentType.LifeInsuranceCashValue;
                case "sale of chattel":              return Mismo.MismoXmlElement.E_DownPaymentType.SaleOfChattel;
                case "trade equity":                 return Mismo.MismoXmlElement.E_DownPaymentType.TradeEquity;
                case "sweat equity":                 return Mismo.MismoXmlElement.E_DownPaymentType.SweatEquity;
                case "cash on hand":                 return Mismo.MismoXmlElement.E_DownPaymentType.CashOnHand;
                case "deposit on sales contract":    return Mismo.MismoXmlElement.E_DownPaymentType.DepositOnSalesContract;
                case "equity from pending sale":     return Mismo.MismoXmlElement.E_DownPaymentType.EquityOnPendingSale;
                case "equity from subject property": return Mismo.MismoXmlElement.E_DownPaymentType.EquityOnSubjectProperty;
                case "equity on sold property":      return Mismo.MismoXmlElement.E_DownPaymentType.EquityOnSoldProperty;
                case "other type of down payment":   return Mismo.MismoXmlElement.E_DownPaymentType.OtherTypeOfDownPayment;
                case "rent with option to purchase": return Mismo.MismoXmlElement.E_DownPaymentType.RentWithOptionToPurchase;
                case "secured borrowed funds":       return Mismo.MismoXmlElement.E_DownPaymentType.SecuredBorrowedFunds;
                case "unsecured borrowed funds":     return Mismo.MismoXmlElement.E_DownPaymentType.UnsecuredBorrowedFunds;
                
                default:
                    return Mismo.MismoXmlElement.E_DownPaymentType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_TransmittalDataCurrentFirstMortgageHolderType ToMismo(E_s1stMOwnerT s1stMOwnerT) 
        {
            if (m_dataLoan.sLienPosT == E_sLienPosT.First)
                return Mismo.MismoXmlElement.E_TransmittalDataCurrentFirstMortgageHolderType.Undefined;
            switch (s1stMOwnerT) 
            {
                case E_s1stMOwnerT.FannieMae:     return Mismo.MismoXmlElement.E_TransmittalDataCurrentFirstMortgageHolderType.FNM;
                case E_s1stMOwnerT.FreddieMac:    return Mismo.MismoXmlElement.E_TransmittalDataCurrentFirstMortgageHolderType.FRE;
                case E_s1stMOwnerT.SellerOrOther: return Mismo.MismoXmlElement.E_TransmittalDataCurrentFirstMortgageHolderType.Other;
                case E_s1stMOwnerT.LeaveBlank:    return Mismo.MismoXmlElement.E_TransmittalDataCurrentFirstMortgageHolderType.Unknown;
                default:
                    LogInvalidEnum("s1stMOwnerT", s1stMOwnerT);
                    return Mismo.MismoXmlElement.E_TransmittalDataCurrentFirstMortgageHolderType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_GSERefinancePurposeType ToMismoGseRefinancePurposeType(string desc) 
        {
            switch (desc.ToLower()) 
            {
                case "no cash-out rate/term":       return Mismo.MismoXmlElement.E_GSERefinancePurposeType.ChangeInRateTerm;
                case "limited cash-out rate/term":  return Mismo.MismoXmlElement.E_GSERefinancePurposeType.CashOutLimited;
                case "cash-out/home improvement":   return Mismo.MismoXmlElement.E_GSERefinancePurposeType.CashOutHomeImprovement;
                case "cash-out/debt consolidation": return Mismo.MismoXmlElement.E_GSERefinancePurposeType.CashOutDebtConsolidation;
                case "cash-out/other":              return Mismo.MismoXmlElement.E_GSERefinancePurposeType.CashOutOther;
                default:
                    if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                        return Mismo.MismoXmlElement.E_GSERefinancePurposeType.NoCashOutOther;
                    else if (m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
                        return Mismo.MismoXmlElement.E_GSERefinancePurposeType.CashOutOther;
                    else
                        return Mismo.MismoXmlElement.E_GSERefinancePurposeType.Undefined;

            }
        }
        private Mismo.MismoXmlElement.E_RefinanceImprovementsType ToMismo(E_sSpImprovTimeFrameT sSpImprovTimeFrameT) 
        {
            switch (sSpImprovTimeFrameT) 
            {
                case E_sSpImprovTimeFrameT.LeaveBlank: return Mismo.MismoXmlElement.E_RefinanceImprovementsType.Unknown;
                case E_sSpImprovTimeFrameT.Made: return Mismo.MismoXmlElement.E_RefinanceImprovementsType.Made;
                case E_sSpImprovTimeFrameT.ToBeMade: return Mismo.MismoXmlElement.E_RefinanceImprovementsType.ToBeMade;
                default:
                    LogInvalidEnum("sSpImprovTimeFrameT", sSpImprovTimeFrameT);
                    return Mismo.MismoXmlElement.E_RefinanceImprovementsType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_LoanPurposeType ToMismo(E_sLPurposeT sLPurposeT) 
        {
            switch (sLPurposeT) 
            {
                case E_sLPurposeT.Construct:     return Mismo.MismoXmlElement.E_LoanPurposeType.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm: return Mismo.MismoXmlElement.E_LoanPurposeType.ConstructionToPermanent;
                case E_sLPurposeT.Other:         return Mismo.MismoXmlElement.E_LoanPurposeType.Other;
                case E_sLPurposeT.Purchase:      return Mismo.MismoXmlElement.E_LoanPurposeType.Purchase;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    return Mismo.MismoXmlElement.E_LoanPurposeType.Refinance;
                default:
                    LogInvalidEnum("sLPurposeT", sLPurposeT);
                    return Mismo.MismoXmlElement.E_LoanPurposeType.Undefined;

            }
        }
        private Mismo.MismoXmlElement.E_LoanPurposePropertyRightsType ToMismo(E_sEstateHeldT sEstateHeldT) 
        {
            switch (sEstateHeldT) 
            {
                case E_sEstateHeldT.FeeSimple: return Mismo.MismoXmlElement.E_LoanPurposePropertyRightsType.FeeSimple;
                case E_sEstateHeldT.LeaseHold: return Mismo.MismoXmlElement.E_LoanPurposePropertyRightsType.Leasehold;
                default:
                    LogInvalidEnum("sEstateHeldT", sEstateHeldT);
                    return Mismo.MismoXmlElement.E_LoanPurposePropertyRightsType.Undefined;
            }
        }
        private Mismo.MismoXmlElement.E_LoanPurposePropertyUsageType ToMismo(E_aOccT aOccT) 
        {
            switch (aOccT) 
            {
                case E_aOccT.PrimaryResidence: return Mismo.MismoXmlElement.E_LoanPurposePropertyUsageType.PrimaryResidence;
                case E_aOccT.SecondaryResidence: return Mismo.MismoXmlElement.E_LoanPurposePropertyUsageType.SecondHome;
                case E_aOccT.Investment: return Mismo.MismoXmlElement.E_LoanPurposePropertyUsageType.Investor;
                default:
                    LogInvalidEnum("aOccT", aOccT);
                    return Mismo.MismoXmlElement.E_LoanPurposePropertyUsageType.Undefined;
            }
        }
        #endregion
        private void LogInvalidEnum(string type, Enum value) 
        {
            Tools.LogBug("Unhandle enum value='" + value + "' for " + type);
        }
        
    }
}
