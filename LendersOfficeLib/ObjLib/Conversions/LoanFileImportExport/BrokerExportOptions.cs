﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;

    /// <summary>
    /// A class encapsulating the different parameters for export.
    /// </summary>
    [Serializable]
    public class BrokerExportOptions
    {
        // tdsk: update this to have an isvalid, and validate on his with SpecificExportType prior to running anything.
        // tdsk: change SpecificExportType, SelectedDB to an enums.
        // tdsk: utilize EmailToNotifyOnFinish, and make sure it's a validated email tied to the user.

        /// <summary>
        /// Gets or sets the id of the broker to use for the export.
        /// </summary>
        /// <value>The id of the broker to use for the export.</value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we want the result to be compressed.  Especially true if we want to include files.
        /// </summary>
        /// <value>True if we want the result compressed.</value>
        public bool Compress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating which db we want relative to the stage.  Currently MAIN_DB or RATE_SHEET.
        /// </summary>
        /// <value>The db we want.</value>
        public string SelectedDB { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we want the tool to log additional debug info during this export.
        /// </summary>
        /// <value>True iff we want the tool to log additional info.</value>
        public bool LogDebugInfo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we want the tool to include files from tin the export.
        /// </summary>
        /// <value>True iff we want the tool to include files.</value>
        public bool IncludeFileDbEntries { get; set; }

        /// <summary>
        /// Gets or sets the type of export we want.
        /// </summary>
        /// <value>The type of export we want to do.</value>
        public ReplicationExportType ExportType { get; set; }

        /// <summary>
        /// Gets or sets the specific type of export we want if the export type is broker.<para></para>
        /// This can be "NO_USER", "MAIN_DB_PRICING", "ALL_BRANCH_USERS", "LOAN_TEMPLATES":"SYS_TABLES", "RATE_SHEET", "ONE_BRANCH_USERS".
        /// </summary>
        /// <value>The specific type of export.</value>
        public string SpecificExportType { get; set; }

        /// <summary>
        /// Gets or sets the name of the branch that we want the users of if we are exporting broker users of one branch.
        /// </summary>
        /// <value>The name of the branch to export the users from.</value>
        public string BranchNameToGetUsers { get; set; }

        /// <summary>
        /// Gets or sets the extra options associated with custom export.  Null is ok if not custom export.
        /// </summary>
        /// <value>The extra options associated with custom export.</value>
        public CustomBrokerExportOptions CustomOptions { get; set; }

        /// <summary>
        /// Gets or sets the name of the email to notify once the export is finished, or null if not interested.<para></para>
        /// Does not currently notify.
        /// </summary>
        /// <value>The email to notify when finished.</value>
        public string EmailToNotifyOnFinish { get; set; }
    }
}