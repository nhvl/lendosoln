﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using Common;
    using Constants;
    using DataAccess;
    using DataAccess.TempFile;
    using LendersOffice.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// A class for importing/exporting xml of loans from one stage to another. <para></para>
    /// It's pretty much just a copy of the LoanOverwriteImportExport page but with a class surface and some added security / context precautions. <para></para>
    /// tdsk: After this has been succesfully adopted, delete the corresponding methods in the LoanOverwriteImportExport page.
    /// </summary>
    public class LoanReplicator
    {
        /// <summary>
        /// The filename of the white list. Need further transformation to get the real address according to the setting of server. <para/>
        /// </summary>
        private const string SenderWhitelistFileName = "senderWhiteList.csv.config";

        /// <summary>
        /// Gets the full address of the white list. <para/>
        /// </summary>
        /// <value>The full address of the whitelist.</value>
        private static string SenderWhiteListFullName
        {
            get
            {
                return Path.Combine(SenderWhiteListDirectory, SenderWhitelistFileName);
            }
        }

        /// <summary>
        /// Gets the directory where the whitelist resides. <para/>
        /// </summary>
        /// <value>The directory where the whitelist resides. </value>
        private static string SenderWhiteListDirectory
        {
            get
            {
                return Tools.GetServerMapPath("~/");
            }
        }

        /// <summary>
        /// Imports the loan with the specified options.
        /// </summary>
        /// <param name="principal">The person wanting to perform the loan import.</param>
        /// <param name="options">The options they specified for importing the loan.</param>
        /// <returns>An object with information about the result of the import.</returns>
        public static LoanImportResult ImportLoan(AbstractUserPrincipal principal, LoanImportOptions options)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            List<string> reasonsUserCantImportLoan;
            if (!CanUserImportLoan(principal, options, out reasonsUserCantImportLoan))
            {
                if (principal is InternalUserPrincipal || principal is SystemUserPrincipal)
                {
                    return new LoanImportResult()
                    {
                        Errors = reasonsUserCantImportLoan
                    };
                }
                else
                {
                    Tools.LogInfo("User was blocked from importing loan because: " + string.Join(", ", reasonsUserCantImportLoan));

                    return new LoanImportResult()
                    {
                        Errors = new List<string> { "You are not allowed to import a loan." }
                    };
                }
            }

            Guid loanIdToOverwrite;

            if (!options.LoanIdToOverwrite.HasValue)
            {
                // tdsk: this needs to be created with some representative principal for that broker.
                var creator = CLoanFileCreator.GetCreator(principal, LendersOffice.Audit.E_LoanCreationSource.UserCreateFromBlank);
                loanIdToOverwrite = creator.CreateBlankLoanFile();
                creator.CommitFileCreation(false);
            }
            else
            {
                loanIdToOverwrite = options.LoanIdToOverwrite.Value;
            }

            List<string> errorList = new List<string>();
            int justinOneSqlQueryTimeOutInSeconds = ConstStage.JustinOneSqlQueryTimeOutInSeconds;
            LoanOverwriteTool.ImportOneXmlFile_LoanUpdate(
                ref errorList,
                ShareImportExport.ExportTableNames,
                TempFileUtils.Name2Path(options.FileNameToImport),
                loanIdToOverwrite.ToString(),
                principal.UserId.ToString(),
                justinOneSqlQueryTimeOutInSeconds,
                options.DefaultOtherBrokerFields);

            var result = new LoanImportResult()
            {
                Errors = errorList,
                CreatedLoanId = options.LoanIdToOverwrite.HasValue ? (Guid?)null : loanIdToOverwrite
            };

            return result;
        }

        /// <summary>
        /// Exports the loan with the specified options.
        /// </summary>
        /// <param name="principal">The person wanting to perform the loan export.</param>
        /// <param name="options">The options they specified for exporting the loan.</param>
        /// <returns>An object with information about the result of the export.</returns>
        public static LoanExportResult ExportLoan(AbstractUserPrincipal principal, LoanExportOptions options)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            List<string> reasonsUserCantExportLoan;
            if (!CanUserExportLoan(principal, out reasonsUserCantExportLoan))
            {
                if (principal is SystemUserPrincipal || principal is InternalUserPrincipal)
                {
                    return new LoanExportResult()
                    {
                        Errors = reasonsUserCantExportLoan
                    };
                }
                else
                {
                    Tools.LogInfo("User was blocked from exporting loan because: " + string.Join(", ", reasonsUserCantExportLoan));

                    return new LoanExportResult()
                    {
                        Errors = new List<string>() { "You are not allowed to export loans." }
                    };
                }
            }

            Dictionary<string, WhitelistData> whitelistDictionary;
            
            IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer;

            var result = LoanReplicator.ExportLoanWithMissingFields(
                options,
                out whitelistDictionary,
                out fileDBKeysForDirectTransfer);

            if (result.TempFileName != null)
            {
                TempFileAccessManager.AssertThatCurrentUserCreatedFile(principal, Path.GetFileName(result.TempFileName));
            }

            return result;
        }

        /// <summary>
        /// Gets whether or not the supplied principal can export the loan.
        /// </summary>
        /// <param name="principal">The principal asking to export the loan.</param>
        /// <param name="reasons">The reasons the user cant export the loan, or empty list if they can.</param>
        /// <returns>True iff the user can export the loan.</returns>
        private static bool CanUserExportLoan(AbstractUserPrincipal principal, out List<string> reasons)
        {
            reasons = new List<string>();
            string reason;
            bool canUserPossiblyExport = CanUserPossiblyDoX(principal, "export loans", out reason);

            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                var isLoauthRequest = Tools.RequestIsOnLoauth();
                if (!(isLoauthRequest.HasValue && isLoauthRequest.Value))
                {
                    reasons.Add("You may not export from production unless using LoAuth.");
                }
            }

            if (!canUserPossiblyExport)
            {
                reasons.Add(reason);
            }

            if (!reasons.Any())
            {
                return true;
            }
            
            return false;
        }

        /// <summary>
        /// Gets whether or not the supplied principal can import the loan.
        /// </summary>
        /// <param name="principal">The principal asking to import the loan.</param>
        /// <param name="options">The options the user is using to import the loan.</param>
        /// <param name="reasons">The reasons the user cant import the loan, or empty list if they can.</param>
        /// <returns>True iff the user can import the loan.</returns>
        private static bool CanUserImportLoan(AbstractUserPrincipal principal, LoanImportOptions options, out List<string> reasons)
        {
            reasons = new List<string>();

            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                reasons.Add("Cannot import loans to production.");
            }

            if (principal is BrokerUserPrincipal && !TempFileAccessManager.DoesFileBelongToCurrentUser(principal, options.FileNameToImport))
            {
                reasons.Add("You did not create this file so you may not import it.");
            }

            if (!(principal is BrokerUserPrincipal || options.LoanIdToOverwrite.HasValue))
            {
                reasons.Add("Must be a broker user or specify a loan id to overwrite.");
            }

            string reason;
            if (!CanUserPossiblyDoX(principal, "import loans", out reason))
            {
                reasons.Add(reason);
            }

            if (!reasons.Any())
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets if it's possible that they can do the thing.  <para></para>
        /// False means definitely they shouldn't, true means there may be other reasons they can't perform the operation.
        /// </summary>
        /// <param name="principal">The principal asking to perform the operation.</param>
        /// <param name="operationName">The name of the operation the user wants to perform.</param>
        /// <param name="reasonCant">The reason the user can't do the thing they requested to do, or null if no apparent reason.</param>
        /// <returns>True iff it's possible the user can do the thing, but there may be other reasons they can't.</returns>
        private static bool CanUserPossiblyDoX(AbstractUserPrincipal principal, string operationName, out string reasonCant)
        {   
            if (principal is SystemUserPrincipal)
            {
                reasonCant = null;
                return true;
            }

            if (HttpContext.Current == null)
            {
                reasonCant = $"Only system user principals are allowed to automate {operationName}.";
                return false;
            }

            if (principal.ApplicationType == E_ApplicationT.LendersOffice)
            {
                if (ConstStage.LoginNamesRequiredOfBUsersForLoanReplicateServiceMethods.Contains(principal.LoginNm, StringComparer.OrdinalIgnoreCase)
                && ConstStage.IpsRequiredOfBUsersForLoanReplicateServiceMethods.Contains(RequestHelper.ClientIP, StringComparer.OrdinalIgnoreCase))
                {
                    reasonCant = null;
                    return true;
                }

                if (principal.CanUserViewLoanEditorTestPage(out reasonCant))
                {
                    return true;
                }
            }

            reasonCant = $"You are not authorized to {operationName}.";
            return false;
        }

        /// <summary>
        /// Exports the loan and may include fields that are missing from the whitelist relative to the stage's database.
        /// </summary>
        /// <param name="options">The options the user wants to use during the export.</param>
        /// <param name="whitelistDictionary">The dictionary of whitelist info to be obtained from the whitelist file.</param>
        /// <param name="fileDBKeysForDirectTransfer">Any additional file db keys that need export.</param>
        /// <returns>An object containing information about the result of the export.</returns>
        private static LoanExportResult ExportLoanWithMissingFields(
            LoanExportOptions options, 
            out Dictionary<string, WhitelistData> whitelistDictionary,
            out IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer)
        {
            var errors = new List<string>();
            var warnings = new List<string>();
            Dictionary<string, string> reconstructMap;
            bool needReview;
            bool needUpdateWhiteList;
            var result = new LoanExportResult();

            if (HttpContext.Current != null)
            {
                HttpContext.Current.Server.ScriptTimeout = ConstStage.JustinBrokerExportTimeOutInSeconds;
            }            

            List<List<string>> connBrokerIdCustomerCodeListList = null;
            List<List<string>> listOfLoanTableRecordStrings = null;
            LoanReplicator.GetTableEntriesAndBrokerConnectionsForLoan(out connBrokerIdCustomerCodeListList, out listOfLoanTableRecordStrings, options.LoanId.ToString());

            string filename = TableReplicateTool.ExportTablesBySpAndEntryList(
                ref errors,
                ref warnings,
                out whitelistDictionary,
                out reconstructMap,
                out needReview,
                out needUpdateWhiteList,
                out fileDBKeysForDirectTransfer,
                LoanReplicator.SenderWhiteListFullName,
                LoanReplicator.SenderWhiteListDirectory,
                "MAIN_DB",
                options.IncludeFileDbEntries,
                new HashSet<string>(ShareImportExport.ExportTableNames),
                isLoanOverwrite: true,
                listOfListsOfBrokerConnectionInfoStrings: connBrokerIdCustomerCodeListList,
                listOfListsOfTableRecordStrings: listOfLoanTableRecordStrings,
                needDebugInfo: true);

            result.Errors = errors;
            result.Warnings = warnings;
            result.NeedToReviewTheWhitelist = needReview;
            result.NeedToUpdateTheWhitelist = needUpdateWhiteList;
            result.TempFileName = Path.GetFileName(filename);

            if (errors.Any())
            {
                Tools.LogInfo("loanoverwriteimportexport_export_errors: " + string.Join(Environment.NewLine, errors));
            }

            if (warnings.Any())
            {
                Tools.LogInfo("loanoverwriteimportexport_export_warnings: " + string.Join(Environment.NewLine, warnings));
            }

            return result;
        }

        /// <summary>
        /// Generates list of loan-table strings (entry list) and broker connection info strings (combo list) for loan export.
        /// </summary>
        /// <param name="connBrokerIdCustomerCodeListList">A list of combo lists of (broker id, customer code) pairs separated by the '|' vertical line symbol, the outer list of length 1.</param>
        /// <param name="listOfLoanTableRecordStrings">A list of entry lists, where each entry is a list like tablename|slid|slidvalue, and the outer list is length 1.</param>
        /// <param name="loanId">The loan id of current loan.</param>
        private static void GetTableEntriesAndBrokerConnectionsForLoan(out List<List<string>> connBrokerIdCustomerCodeListList, out List<List<string>> listOfLoanTableRecordStrings, string loanId)
        {
            connBrokerIdCustomerCodeListList = new List<List<string>>();
            listOfLoanTableRecordStrings = new List<List<string>>();
            Guid connInfoBrokerId = Guid.Empty;
            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(new Guid(loanId), out connInfoBrokerId);
            string customerCode = ShareImportExport.GetCustomerCodeByBrokerId(connInfoBrokerId);
            int exportTableCnt = ShareImportExport.ExportTableNames.Count();

            List<string> appIdList = ShareImportExport.GetAppIdListByLoanId(connInfoBrokerId, loanId);
            List<string> curComboList = new List<string>();
            List<string> curEntryList = new List<string>();

            string combo = connInfoBrokerId + "|" + customerCode;
            for (int i = 0; i < exportTableCnt; i++)
            {
                string loanTableRecordString = string.Empty;
                if (ShareImportExport.ExportTableNames[i].StartsWith("APPLICATION_"))
                {
                    foreach (string appId in appIdList)
                    {
                        loanTableRecordString = ShareImportExport.ExportTableNames[i] + "|aAppId|" + appId;
                        curComboList.Add(combo);
                        curEntryList.Add(loanTableRecordString);
                    }
                }
                else
                {
                    loanTableRecordString = ShareImportExport.ExportTableNames[i] + "|sLId|" + loanId;
                    curComboList.Add(combo);
                    curEntryList.Add(loanTableRecordString);
                }
            }

            connBrokerIdCustomerCodeListList.Add(curComboList);
            listOfLoanTableRecordStrings.Add(curEntryList);
        }
    }
}
