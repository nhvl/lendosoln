﻿// <copyright file="FileDBEntryInfoForExport.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   11/17/2015
// </summary>
namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using DataAccess;

    /// <summary>
    /// In addition to having the minimum needed to specify the file, <para></para>
    /// this may contain additional info like the FiledBKeyType.
    /// </summary>
    public class FileDBEntryInfoForExport
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FileDBEntryInfoForExport"/> class.
        /// </summary>
        /// <param name="entry">The file entry info.</param>
        /// <param name="keyType">The file key type.</param>
        public FileDBEntryInfoForExport(FileDBEntryInfo entry, E_FileDBKeyType keyType)
        {
            this.FileDBEntry = entry;
            this.FileDBKeyType = keyType;
        }

        /// <summary>
        /// Gets the FileDBEntryInfo associated with the file.
        /// </summary>
        /// <value>The FileDBEntryInfo associated with the file.</value>
        public FileDBEntryInfo FileDBEntry { get; }

        /// <summary>
        /// Gets the type of file.
        /// </summary>
        /// <value>The type of file i.e. E_FileDBKeyType.</value>
        public E_FileDBKeyType FileDBKeyType { get; }

        /// <summary>
        /// Deep equality.
        /// </summary>
        /// <param name="obj">The other object we compare to.</param>
        /// <returns>True iff the other object is deeply equal.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is FileDBEntryInfoForExport))
            {
                return false;
            }

            return this.Equals((FileDBEntryInfoForExport)obj);
        }

        /// <summary>
        /// Deep equality.
        /// </summary>
        /// <param name="other">The other object we compare to.</param>
        /// <returns>True iff the other object is deeply equal.</returns>
        public bool Equals(FileDBEntryInfoForExport other)
        {
            if (other == null)
            {
                return false;
            }

            return
                this.FileDBKeyType == other.FileDBKeyType &&
                ((this.FileDBEntry == null && other.FileDBEntry == null) || (this.FileDBEntry != null && this.FileDBEntry.Equals(other)));
        }

        /// <summary>
        /// A hashcode for this instance.
        /// </summary>
        /// <returns>A hashcode using tuple's GetHashCode method.</returns>
        public override int GetHashCode()
        {
            return Tuple.Create(this.FileDBEntry, this.FileDBKeyType).GetHashCode();
        }
    }
}
