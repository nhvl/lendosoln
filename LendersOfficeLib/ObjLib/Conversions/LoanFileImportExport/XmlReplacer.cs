﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.XPath;

    /// <summary>
    /// A class to assist with replacing values in xml by their xpath. <para></para>
    /// NOTE: It currently accepts XDocuments, so the xml should be small and well-formed or we'll need another implementation.
    /// </summary>
    public class XmlReplacer
    {
        /// <summary>
        /// Replaces the text values in the xml that are xpath matches with the specified replacement. <para></para>
        /// NOTE: <para></para>
        ///  - Since this uses XDocument 1) the xml should be well-formed and 2) it should not be large. <para></para>
        ///  - This affects the XDocument in place.
        /// </summary>
        /// <param name="document">The xml loaded as an xdocument. <see cref="XDocument.Parse(string)"/>.</param>
        /// <param name="compiledXPath">The compiled xpath that points to the node containing the text to replace.  <see cref="XPathExpression.Compile(string)"/>.</param>
        /// <param name="replacementByValue">The dictionary of replacements for the matches.</param>
        /// <param name="todoWhenValueNotInReplacementDictionary">What to do when the match is not in the dictionary of replacements.</param>
        public static void ReplaceXmlTextValues(
            XDocument document,
            XPathExpression compiledXPath,
            Dictionary<string, string> replacementByValue,
            Action<string> todoWhenValueNotInReplacementDictionary)
        {
            if (replacementByValue == null)
            {
                throw new ArgumentNullException(nameof(replacementByValue));
            }

            Action<XText> replaceText = (textNode) =>
            {
                XmlReplacer.Replace(textNode, replacementByValue, todoWhenValueNotInReplacementDictionary);
            };

            WalkXmlTextValues(document, compiledXPath, replaceText);            
        }

        /// <summary>
        /// Replaces the attribute values in the xml that are xpath matches with the specified replacement. <para></para>
        /// NOTE: <para></para>
        ///  - Since this uses XDocument 1) the xml should be well-formed and 2) it should not be large. <para></para>
        ///  - This affects the XDocument in place.
        /// </summary>
        /// <param name="document">The xml loaded as an xdocument. <see cref="XDocument.Parse(string)"/>.</param>
        /// <param name="compiledXPath">The compiled xpath that points to the node containing the attribute to replace.  <see cref="XPathExpression.Compile(string)"/>.</param>
        /// <param name="attributeName">The name of the attribute whose value we are going to replace.</param>
        /// <param name="replacementByValue">The dictionary of replacements for the attribute values.</param>
        /// <param name="todoWhenValueNotInReplacementDictionary">What to do when the attribute match is not in the dictionary of replacements.</param>
        public static void ReplaceXmlAttributeValues(
           XDocument document,
           XPathExpression compiledXPath,
           string attributeName,
           Dictionary<string, string> replacementByValue,
           Action<string> todoWhenValueNotInReplacementDictionary)
        {
            if (replacementByValue == null)
            {
                throw new ArgumentNullException(nameof(replacementByValue));
            }

            Action<XAttribute> replaceAttributeValues = (attribute) =>
            {
                XmlReplacer.Replace(attribute, replacementByValue, todoWhenValueNotInReplacementDictionary);
            };

            WalkXmlAttributeValues(document, compiledXPath, attributeName, replaceAttributeValues);
        }

        /// <summary>
        /// Get the text values associated with the xml and xpath.
        /// </summary>
        /// <param name="document">The xml loaded as an xdocument. <see cref="XDocument.Parse(string)"/>.</param>
        /// <param name="compiledXPath">The compiled xpath that points to the node containing the attribute to replace.  <see cref="XPathExpression.Compile(string)"/>.</param>
        /// <returns>The unique set of text values associated with the xml and xpath.</returns>
        public static HashSet<string> GetXmlTextValues(
            XDocument document,
            XPathExpression compiledXPath)
        {
            var matchingTextValues = new HashSet<string>();

            Action<XText> gatherTextValues = (textNode) =>
            {
                var origVal = textNode.Value;

                matchingTextValues.Add(origVal);
            };            

            WalkXmlTextValues(document, compiledXPath, gatherTextValues);
            return matchingTextValues;
        }

        /// <summary>
        /// Get the attribute values associated with the xml and xpath.
        /// </summary>
        /// <param name="document">The xml loaded as an xdocument. <see cref="XDocument.Parse(string)"/>.</param>
        /// <param name="compiledXPath">The compiled xpath that points to the node containing the attribute to replace.  <see cref="XPathExpression.Compile(string)"/>.</param>
        /// <param name="attributeName">The name of the attribute of interest for the nodes of interest.</param>
        /// <returns>The unique set of attribute values associated with the xml, xpath, and attribute name.</returns>
        public static HashSet<string> GetXmlAttributeValues(
            XDocument document,
            XPathExpression compiledXPath,
            string attributeName)
        {
            var matchingAttributeValues = new HashSet<string>();

            Action<XAttribute> gatherAttributeValues = (attribute) =>
            {
                string origVal = attribute.Value;
                matchingAttributeValues.Add(origVal);
            };

            WalkXmlAttributeValues(document, compiledXPath, attributeName, gatherAttributeValues);

            return matchingAttributeValues;
        }

        /// <summary>
        /// Walks along the xml finding text of the nodes matching the xpath, then performs the specified action on said text node.
        /// </summary>
        /// <param name="document">The xml loaded as an xdocument. <see cref="XDocument.Parse(string)"/>.</param>
        /// <param name="compiledXPath">The compiled xpath that points to the node containing the text of interest.  <see cref="XPathExpression.Compile(string)"/>.</param>
        /// <param name="useNode">An action to perform on the matching text node on finding it.</param>
        private static void WalkXmlTextValues(
            XDocument document,
            XPathExpression compiledXPath,
            Action<XText> useNode)
        {
            if (document == null)
            {
                throw new ArgumentNullException(nameof(document));
            }

            if (compiledXPath == null)
            {
                throw new ArgumentNullException(nameof(compiledXPath));
            }

            if (useNode == null)
            {
                throw new ArgumentNullException(nameof(useNode));
            }

            var nodes = document.XPathSelectElements(compiledXPath.Expression);
            foreach (XElement node in nodes)
            {
                foreach (XText textNode in node.Nodes().OfType<XText>())
                {
                    useNode(textNode);
                }
            }
        }

        /// <summary>
        /// Walks along the xml finding the attribute of interest of the nodes of interest, performing the specified action on said attribute.
        /// </summary>
        /// <param name="document">The xml loaded as an xdocument. <see cref="XDocument.Parse(string)"/>.</param>
        /// <param name="compiledXPath">The compiled xpath that points to the node containing the attribute of interest.  <see cref="XPathExpression.Compile(string)"/>.</param>
        /// <param name="attributeName">The name of the attribute of interest for the nodes of interest.</param>
        /// <param name="useAttribute">An action to perform on the matching attribute of the nodes specified by the xpath.</param>
        private static void WalkXmlAttributeValues(
            XDocument document,
            XPathExpression compiledXPath,
            string attributeName,
            Action<XAttribute> useAttribute)
        {
            if (document == null)
            {
                throw new ArgumentNullException(nameof(document));
            }

            if (compiledXPath == null)
            {
                throw new ArgumentNullException(nameof(compiledXPath));
            }

            if (attributeName == null)
            {
                throw new ArgumentNullException(nameof(attributeName));
            }

            if (useAttribute == null)
            {
                throw new ArgumentNullException(nameof(useAttribute));
            }

            var nodes = document.XPathSelectElements(compiledXPath.Expression);
            foreach (XElement node in nodes)
            {
                XAttribute attribute = node.Attribute(attributeName);

                if (attribute == null)
                {
                    throw new ArgumentException($"xpath '{compiledXPath.Expression}' pointed to node that did not contain attribute with name '{attributeName}'");
                }

                useAttribute(attribute);
            }
        }

        /// <summary>
        /// Replaces the value of the xml node with the replacement in the dictionary, if it exists. <para></para>
        /// Assumes node and replacementByValue are not null. <para></para>
        /// </summary>
        /// <param name="node">The node whose value we are replacing.</param>
        /// <param name="replacementByValue">The dictionary containing replacement values by node values.</param>
        /// <param name="todoWhenValueNotInReplacementDictionary">The method to run when the replacement dictionary does not contain the value.</param>
        private static void Replace(XText node, Dictionary<string, string> replacementByValue, Action<string> todoWhenValueNotInReplacementDictionary)
        {
            string origVal = node.Value;

            if (replacementByValue.ContainsKey(origVal))
            {
                node.Value = replacementByValue[origVal];
            }
            else
            {
                if (todoWhenValueNotInReplacementDictionary != null)
                {
                    todoWhenValueNotInReplacementDictionary(origVal);
                }
            }
        }

        /// <summary>
        /// Replaces the value of the xml node with the replacement in the dictionary, if it exists. <para></para>
        /// Assumes node and replacementByValue are not null. <para></para>
        /// </summary>
        /// <param name="node">The node whose value we are replacing.</param>
        /// <param name="replacementByValue">The dictionary containing replacement values by node values.</param>
        /// <param name="todoWhenValueNotInReplacementDictionary">The method to run when the replacement dictionary does not contain the value.</param>
        private static void Replace(XAttribute node, Dictionary<string, string> replacementByValue, Action<string> todoWhenValueNotInReplacementDictionary)
        {
            string origVal = node.Value;

            if (replacementByValue.ContainsKey(origVal))
            {
                node.Value = replacementByValue[origVal];
            }
            else
            {
                if (todoWhenValueNotInReplacementDictionary != null)
                {
                    todoWhenValueNotInReplacementDictionary(origVal);
                }
            }
        }
    }
}
