﻿// -----------------------------------------------------------------------
//  <copyright file="TableReplicateTool.cs" company="Meridianlink">
//      Copyright (c) Sprocket Enterprises. All rights reserved.
//  </copyright>
//  <author>Zijing Jia</author>
//  <summary>Table replicate tool.</summary>
// -----------------------------------------------------------------------

namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Adapter;
    using Common;
    using Common.SerializationTypes;
    using Constants;
    using CreditReport;
    using DataAccess;
    using Db;
    using Drivers.Gateways;
    using Drivers.NetFramework;
    using LqbGrammar;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers;

    /// <summary>
    /// A toolkit for table replication.
    /// </summary>
    public class TableReplicateTool
    {
        /// <summary>
        /// The file name with implicit references to identity keys. Need further transformation to get the real address according to the setting of server. <para/>
        /// </summary>
        private const string ImplicitIdentityKeyFileName = "implicitIdentityKeyRefs.csv.config";

        /// <summary>
        /// Gets the full address of the file with implicit references to identity keys. <para/>
        /// </summary>
        /// <value>The full address of the file with implicit references to identity keys.</value>
        private static string ImplicitIdentityKeyFullFileName
        {
            get
            {
                return (System.Web.HttpContext.Current != null) ? Tools.GetServerMapPath("~/" + TableReplicateTool.ImplicitIdentityKeyFileName) : Path.Combine(Directory.GetCurrentDirectory(), ImplicitIdentityKeyFileName);
            }
        }

        /// <summary>
        /// An additional function for xml export tool that will export the connection information, such as broker id and customer code into the xml.
        /// </summary>
        /// <param name="errorList">List of errors.</param>
        /// <param name="writer">An xml writer to write to the xml.</param>
        /// <param name="connInfoComboSet">A list that stores broker id and customer code.</param>
        public static void ExportDbConnInfo(ref List<string> errorList, XmlWriter writer, HashSet<string> connInfoComboSet)
        {
            writer.WriteStartElement("ConnectionInfo");
            foreach (string connInfoCombo in connInfoComboSet)
            {
                writer.WriteElementString("BrokerIdCustomerCodeCombo", connInfoCombo);
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Convert the file data into byte array.
        /// </summary>
        /// <param name="filePath">The physic address of current file.</param>
        /// <returns>Byte data of current file.</returns>
        public static byte[] GetFileAsByte(string filePath)
        {
            return BinaryFileHelper.ReadAllBytes(filePath);
        }

        /// <summary>
        /// Get loan id by application id.
        /// </summary>
        /// <param name="connInfoBrokerId">A broker id to track data source.</param>
        /// <param name="appId">This is application id.</param>
        /// <returns>The loan id in string format.</returns>
        public static string GetLoanIdByAppId(Guid connInfoBrokerId, string appId)
        {
            string loanId = null;
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("aAppId", appId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(connInfoBrokerId, "Justin_Testing_GetLoanIdByAppId", parameters))
            {
                if (reader.Read())
                {
                    loanId = Convert.ToString(reader["sLId"]).TrimWhitespaceAndBOM();
                }
            }

            return loanId;
        }

        /// <summary>
        /// Get the complete foreign key, reference key and corresponding table relationship.
        /// </summary>
        /// <param name="connSrc">The data source for query.</param>
        /// <returns>Foreign key relationship table. Each child list represents one query row, including foreign key table, foreign key, reference table, reference key.</returns>
        public static List<List<string>> GetFkRelationshipTable(DataSrc connSrc)
        {
            List<List<string>> fkeyRelationshipTable = new List<List<string>>();
            using (var reader = StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_GetAllForeignReferenceKeyAndTables"))
            {
                while (reader.Read())
                {
                    List<string> sublist = new List<string>(4);
                    sublist.Add(Convert.ToString(reader["FK_TABLE_NAME"]).TrimWhitespaceAndBOM());
                    sublist.Add(Convert.ToString(reader["FK_COLUMN_NAME"]).TrimWhitespaceAndBOM());
                    sublist.Add(Convert.ToString(reader["REFERENCED_TABLE_NAME"]).TrimWhitespaceAndBOM());
                    sublist.Add(Convert.ToString(reader["REFERENCED_COLUMN_NAME"]).TrimWhitespaceAndBOM());
                    fkeyRelationshipTable.Add(sublist);
                }
            }

            // hardcode
            fkeyRelationshipTable.Add(new string[] { "LOAN_FILE_E", "sInvestorRolodexId", "INVESTOR_ROLODEX", "InvestorRolodexId" }.ToList());
            fkeyRelationshipTable.Add(new string[] { "BROKER_USER", "PmlBrokerId", "PML_BROKER", "PmlBrokerId" }.ToList());

            return fkeyRelationshipTable;
        }

        /// <summary>
        /// Get the reference table as key. The foreign key table as the value of the dictionary.
        /// </summary>
        /// <param name="fkeyRelationShipTable">A table store the major information about all reference and foreign key.</param>
        /// <returns>Return the dictionary for reference key and foreign key relationship with the corresponding table names.</returns>
        public static Dictionary<string, List<string>> GetRefDictionary(List<List<string>> fkeyRelationShipTable)
        {
            Dictionary<string, List<string>> refDictionary = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
            foreach (List<string> entry in fkeyRelationShipTable)
            {
                if (!refDictionary.ContainsKey(entry[2]))
                {
                    refDictionary.Add(entry[2], new List<string>());
                }

                refDictionary[entry[2]].Add(entry[0]);
            }

            return refDictionary;
        }

        /// <summary>
        /// Dictionary format: <code>foreignKeyTableName|foreignKeyColumnName -> list: [ refKeyTableName|refKeyColumnName ] [ refKeyTableName|refKeyColumnName ]</code>.
        /// </summary>
        /// <param name="fkeyRelationShipTable">A table store the major information about all reference and foreign key.</param> 
        /// <returns>Return the dictionary for foreign key and reference key relationship with the corresponding tables.</returns>
        public static Dictionary<string, List<string>> GetFkRealNmDictionary(List<List<string>> fkeyRelationShipTable)
        {
            Dictionary<string, List<string>> fkeyRealNmDictionary = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
            foreach (List<string> entry in fkeyRelationShipTable)
            {
                string foreignKeyCombo = entry[0] + "|" + entry[1];
                string refKeyCombo = entry[2] + "|" + entry[3];

                if (!fkeyRealNmDictionary.ContainsKey(foreignKeyCombo))
                {
                    fkeyRealNmDictionary.Add(foreignKeyCombo, new List<string>(new string[] { refKeyCombo }));
                }
                else
                {
                    List<string> refKeyComboList = fkeyRealNmDictionary[foreignKeyCombo];
                    if (!refKeyComboList.Contains(refKeyCombo))
                    {
                        refKeyComboList.Add(refKeyCombo);
                    }
                }
            }

            return fkeyRealNmDictionary;
        }

        /// <summary>
        /// Get the dictionary that stores the relation of reference key columns and the foreign key columns that refer to that reference key column.
        /// <code>
        /// tablePairMappingKeyPairList: [refKeyTableName|foreignKeyTableName] -> [Rk1|Fk1, Fk2, Fk3][Rk2|Fk4].
        /// Fk1, Fk2, Fk3, Fk4 all belong to the same foreignKeyTableName.
        /// </code>. 
        /// Multiple foreign key in one foreign key table might refer to the same reference key of one Table.
        /// Another data structure. User can use the reference table and foreign key table to find the corresponding foreign key and reference key.
        /// </summary>
        /// <param name="fkeyRelationShipTable">A table store the major information about all reference and foreign key.</param> 
        /// <returns>Dictionary that map reference table name plus foreign key table name to a list of reference and foreign keys.</returns>
        public static Dictionary<string, List<string>> GetTablePairMappingKeyPairList(List<List<string>> fkeyRelationShipTable)
        {
            Dictionary<string, List<string>> tablePairMappingKeyPairList = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
            foreach (List<string> entry in fkeyRelationShipTable)
            {
                string foreignKeyTable = entry[0];
                string foreignKeyNm = entry[1];
                string refKeyTable = entry[2];
                string refKeyNm = entry[3];
                string tableCombo = refKeyTable + "|" + foreignKeyTable;
                string refKeyForeignKeyCombo = refKeyNm + "|" + foreignKeyNm;

                if (!tablePairMappingKeyPairList.ContainsKey(tableCombo))
                {
                    List<string> foreignKeyRefKeyList = new List<string>(new string[] { refKeyForeignKeyCombo });
                    tablePairMappingKeyPairList.Add(tableCombo, foreignKeyRefKeyList);
                }
                else
                {
                    List<string> foreignKeyRefKeyList = tablePairMappingKeyPairList[tableCombo];
                    int ind = foreignKeyRefKeyList.FindIndex(
                        0,
                        delegate(string node)
                        {
                            return node.StartsWith(refKeyNm);
                        });

                    // two foreign keys from the same table refer to the same reference key, defaultBranchId, curBranchId
                    if (ind >= 0 && foreignKeyRefKeyList[ind].Contains("," + foreignKeyNm + ",") == false
                        && foreignKeyRefKeyList[ind].Contains("|" + foreignKeyNm + ",") == false
                        && foreignKeyRefKeyList[ind].EndsWith("," + foreignKeyNm) == false)
                    {
                        string refKeyWithFkList = foreignKeyRefKeyList[ind] + "," + foreignKeyNm;
                        foreignKeyRefKeyList[ind] = refKeyWithFkList;
                    }
                    else
                    {
                        // the foreign key mapping doesn't exist
                        foreignKeyRefKeyList.Add(refKeyForeignKeyCombo);
                    }
                }
            }

            return tablePairMappingKeyPairList;
        }

        /// <summary>
        /// Structure: tableNm -> all its foreign key list.
        /// </summary>
        /// <param name="fkeyRelationShipTable">A table store the major information about all reference and foreign key.</param> 
        /// <returns>Return the dictionary of foreign key.</returns>
        public static Dictionary<string, List<string>> GetForeignKeyDictionary(List<List<string>> fkeyRelationShipTable)
        {
            Dictionary<string, List<string>> foreignKeyDictionary = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
            foreach (List<string> entry in fkeyRelationShipTable)
            {
                string foreignKeyTable = entry[0].ToUpper();
                string foreignKeyColNm = entry[1];
                if (!foreignKeyDictionary.ContainsKey(foreignKeyTable))
                {
                    foreignKeyDictionary.Add(foreignKeyTable, new List<string>());
                }

                List<string> curList = foreignKeyDictionary[foreignKeyTable];
                if (!curList.Contains(foreignKeyColNm))
                {
                    curList.Add(foreignKeyColNm);
                }
            }

            return foreignKeyDictionary;
        }

        /// <summary>
        /// A generated ranking. The lower the value, the earlier the table should be inserted.
        /// </summary>
        /// <returns>Return the dictionary that maps table name to hardcode ranking.</returns>
        public static Dictionary<string, int> HardCodeRankingForInsertionSeq()
        {
            Dictionary<string, int> tableRankMapping = new Dictionary<string, int>();
            tableRankMapping.Add("ACTION_EVENT", 1);
            tableRankMapping.Add("AGENT", 1);
            tableRankMapping.Add("ALL_USER", 0);
            tableRankMapping.Add("APPLICATION_A", 1);
            tableRankMapping.Add("APPLICATION_B", 2);
            tableRankMapping.Add("APPRAISAL_ORDER_INFO", 3);
            tableRankMapping.Add("APPRAISAL_VENDOR_BROKER_ENABLED", 1);
            tableRankMapping.Add("APPRAISAL_VENDOR_CONFIGURATION", 0);
            tableRankMapping.Add("APPRAISAL_VENDOR_EMPLOYEE_LOGIN", 3);
            tableRankMapping.Add("ARM_INDEX", 1);
            tableRankMapping.Add("AUDIT_TRAIL_DAILY", 2);
            tableRankMapping.Add("BILLING_PER_TRANSACTION", 3);
            tableRankMapping.Add("BRANCH", 1);
            tableRankMapping.Add("BROKER", 0);
            tableRankMapping.Add("BROKER_USER", 3);
            tableRankMapping.Add("BROKER_XSLT_REPORT_MAP", 1);
            tableRankMapping.Add("CC_TEMPLATE", 1);
            tableRankMapping.Add("CC_TEMPLATE_SYSTEM", 4);
            tableRankMapping.Add("CC_TEMPLATE_SYSTEM_RELEASE_AUDIT", 1);
            tableRankMapping.Add("CONDITION", 3);
            tableRankMapping.Add("CONDITION_CATEGORY", 2);
            tableRankMapping.Add("CONDITION_CHOICE", 3);
            tableRankMapping.Add("CONFIG_DRAFT", 1);
            tableRankMapping.Add("CONFIG_RELEASED", 1);
            tableRankMapping.Add("CONFORMING_LOAN_LIMITS", 1);
            tableRankMapping.Add("CONSUMER_PORTAL", 4);
            tableRankMapping.Add("CONSUMER_PORTAL_USER", 1);
            tableRankMapping.Add("CONSUMER_PORTAL_USER_X_LOANAPP", 2);
            tableRankMapping.Add("CP_CONSUMER", 1);
            tableRankMapping.Add("CP_CONSUMER_ACTION_REQUEST", 3);
            tableRankMapping.Add("CP_CONSUMER_ACTION_RESPONSE", 4);
            tableRankMapping.Add("CP_CONSUMER_X_LOANID", 3);
            tableRankMapping.Add("CP_SHARE_DOCUMENT", 2);
            tableRankMapping.Add("CREDIT_REPORT_ACCOUNT_PROXY", 1);
            tableRankMapping.Add("CreditReportProtocol", 1);
            tableRankMapping.Add("CUSTOM_FORM_FIELD_CATEGORY", 0);
            tableRankMapping.Add("CUSTOM_FORM_FIELD_LIST", 1);
            tableRankMapping.Add("CUSTOM_LETTER", 1);
            tableRankMapping.Add("DAILY_REPORTS", 1);
            tableRankMapping.Add("DATA_RETRIEVAL_PARTNER", 0);
            tableRankMapping.Add("DATA_RETRIEVAL_PARTNER_BROKER", 1);
            tableRankMapping.Add("DISCUSSION_LOG", 1);
            tableRankMapping.Add("DISCUSSION_NOTIFICATION", 2);
            tableRankMapping.Add("DOCMAGIC_ALTERNATE_LENDER", 1);
            tableRankMapping.Add("DOCMAGIC_PENDING_ESIGN_REQUEST", 3);
            tableRankMapping.Add("DOCUMENT_VENDOR_BRANCH_CREDENTIALS", 2);
            tableRankMapping.Add("DOCUMENT_VENDOR_BROKER_SETTINGS", 1);
            tableRankMapping.Add("DOCUMENT_VENDOR_USER_ENABLED_CREDENTIALS", 1);
            tableRankMapping.Add("DROPBOX_FILES", 1);
            tableRankMapping.Add("EDOCS_AUDIT_HISTORY", 2);
            tableRankMapping.Add("EDOCS_BILLING", 2);
            tableRankMapping.Add("EDOCS_BILLING_EVENT", 3);
            tableRankMapping.Add("EDOCS_BILLING_SUMMARY", 3);
            tableRankMapping.Add("EDOCS_DOCMAGIC_DOCTYPE", 0);
            tableRankMapping.Add("EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE", 1);
            tableRankMapping.Add("EDOCS_DOCMAGIC_PDF_STATUS", 3);
            tableRankMapping.Add("EDOCS_DOCUMENT", 1);
            tableRankMapping.Add("EDOCS_DOCUMENT_TYPE", 0);
            tableRankMapping.Add("EDOCS_DOCUMENT_TYPE_X_SHIPPING_TEMPLATE", 1);
            tableRankMapping.Add("EDOCS_DOCUTECH_DOCTYPE", 0);
            tableRankMapping.Add("EDOCS_DOCUTECH_DOCTYPE_X_DOCTYPE", 1);
            tableRankMapping.Add("EDOCS_GENERIC", 3);
            tableRankMapping.Add("EDOCS_IDS_DOCTYPE", 0);
            tableRankMapping.Add("EDOCS_IDS_DOCTYPE_X_DOCTYPE", 1);
            tableRankMapping.Add("EDOCS_PNG_ENTRY", 2);
            tableRankMapping.Add("EDOCS_SHIPPING_TEMPLATE", 0);
            tableRankMapping.Add("EMPLOYEE", 2);
            tableRankMapping.Add("EMPLOYEE_FAVORITE_REPORTS", 3);
            tableRankMapping.Add("FEATURE", 0);
            tableRankMapping.Add("FEATURE_SUBSCRIPTION", 1);
            tableRankMapping.Add("FEE_SERVICE_REVISION", 1);
            tableRankMapping.Add("FHA_LOAN_LIMITS", 1);
            tableRankMapping.Add("FIPS_COUNTY", 0);
            tableRankMapping.Add("FIRST_CLOSE_TITLE_RESPONSE", 1);
            tableRankMapping.Add("GDMS_ORDER", 3);
            tableRankMapping.Add("GROUP", 1);
            tableRankMapping.Add("GROUP_x_BRANCH", 2);
            tableRankMapping.Add("GROUP_x_EMPLOYEE", 3);
            tableRankMapping.Add("INTEGRATION_FILE_MODIFIED", 3);
            tableRankMapping.Add("INTERNAL_USER", 1);
            tableRankMapping.Add("INVESTOR_ROLODEX", 1);
            tableRankMapping.Add("IRS_4506T_ORDER_INFO", 3);
            tableRankMapping.Add("IRS_4506T_VENDOR_BROKER_SETTINGS", 1);
            tableRankMapping.Add("IRS_4506T_VENDOR_CONFIGURATION", 0);
            tableRankMapping.Add("IRS_4506T_VENDOR_EMPLOYEE_INFO", 4);
            tableRankMapping.Add("LENDER_LOCK_POLICY", 1);
            tableRankMapping.Add("LENDER_LOCK_POLICY_CUSTOM_WARNING_MSGS", 2);
            tableRankMapping.Add("LENDER_LOCK_POLICY_HOLIDAY", 1);
            tableRankMapping.Add("Lender_TPO_LandingPageConfig", 1);
            tableRankMapping.Add("LO_SIGNATURE_TEMPLATE", 1);
            tableRankMapping.Add("LOAN_CACHE_UPDATE_REQUEST", 1);
            tableRankMapping.Add("LOAN_FILE_A", 0);
            tableRankMapping.Add("LOAN_FILE_ACTIVITY", 3);
            tableRankMapping.Add("LOAN_FILE_B", 1);
            tableRankMapping.Add("LOAN_FILE_C", 1);
            tableRankMapping.Add("LOAN_FILE_CACHE", 2);
            tableRankMapping.Add("LOAN_FILE_CACHE_2", 1);
            tableRankMapping.Add("LOAN_FILE_CACHE_3", 1);
            tableRankMapping.Add("LOAN_FILE_CACHE_4", 1);
            tableRankMapping.Add("LOAN_FILE_D", 1);
            tableRankMapping.Add("LOAN_FILE_E", 2);
            tableRankMapping.Add("LOAN_FILE_F", 2);
            tableRankMapping.Add("LOAN_LINK", 2);
            tableRankMapping.Add("LOAN_USER_ASSIGNMENT", 3);
            tableRankMapping.Add("LOCKDESK_CLOSURE", 1);
            tableRankMapping.Add("LPE_PRICE_GROUP", 1);
            tableRankMapping.Add("LPE_PRICE_GROUP_PRODUCT", 2);
            tableRankMapping.Add("MBS_TRADE", 1);
            tableRankMapping.Add("MBS_TRANSACTION", 2);
            tableRankMapping.Add("MONTHLY_REPORTS", 1);
            tableRankMapping.Add("MORTGAGE_POOL", 1);
            tableRankMapping.Add("PML_BROKER", 1);
            tableRankMapping.Add("Q_MESSAGE", 1);
            tableRankMapping.Add("Q_TYPE", 0);
            tableRankMapping.Add("RATE_MONITOR", 3);
            tableRankMapping.Add("REPORT_QUERY", 0);
            tableRankMapping.Add("REPORT_SCHEDULE", 1);
            tableRankMapping.Add("RESERVE_STRING_LIST", 1);
            tableRankMapping.Add("ROLE", 0);
            tableRankMapping.Add("ROLE_ASSIGNMENT", 3);
            tableRankMapping.Add("ROLE_DEFAULT_PERMISSIONS", 1);
            tableRankMapping.Add("RS_EXPIRATION_CUSTOM_WARNING_MSGS", 1);
            tableRankMapping.Add("SCHEDULED_REPORT_USER_INFO", 0);
            tableRankMapping.Add("SERVICE_COMPANY", 0);
            tableRankMapping.Add("SERVICE_FILE", 3);
            tableRankMapping.Add("TASK", 4);
            tableRankMapping.Add("TASK_PERMISSION_LEVEL", 1);
            tableRankMapping.Add("TASK_SUBSCRIPTION", 4);
            tableRankMapping.Add("TASK_x_EDOCS_DOCUMENT", 2);
            tableRankMapping.Add("TASK_x_TASK_TRIGGER_TEMPLATE", 1);
            tableRankMapping.Add("TITLE_VENDOR", 0);
            tableRankMapping.Add("TITLE_VENDOR_POLICY", 1);
            tableRankMapping.Add("TITLE_VENDOR_RESPONSE", 1);
            tableRankMapping.Add("TITLE_VENDOR_X_BROKER", 1);
            tableRankMapping.Add("TRACK", 3);
            tableRankMapping.Add("TRUST_ACCOUNT", 1);
            tableRankMapping.Add("USAGE_EVENT", 1);
            tableRankMapping.Add("WAREHOUSE_LENDER_ROLODEX", 1);
            tableRankMapping.Add("WEEKLY_REPORTS", 1);
            tableRankMapping.Add("XSLT_EXPORT_RESULT_STATUS", 1);
            return tableRankMapping;
        }

        private static readonly HashSet<string> SharedTableNames
            = new HashSet<string>(SharedTableInfo.Names, StringComparer.OrdinalIgnoreCase);

        /// <summary>
        /// The target of this function is to find out if the key value is valid when the value is not null nor empty.
        /// One foreign key might actually refer to multiple reference columns, though it is more reasonable for it to refer only one reference key.
        /// As long as there is one reference key exists for that foreign key value, we will consider the reference is correct. 
        /// If none, it means the foreign key points to something that is not existed. There might be problem then.
        /// <code>ComboList: [table|key][table|key][table|key]</code>
        /// </summary>
        /// <param name="connSrc">The database source for query.</param>
        /// <param name="guidBrokerId">The broker id for database query.</param>
        /// <param name="comboList">A list of reference table and key.</param>
        /// <param name="keyValue">The foreign key value.</param>
        /// <returns>Return true if the reference key exists. Else return false.</returns>
        public static bool CheckIfReferenceKeyListExists(DataSrc connSrc, Guid guidBrokerId, List<string> comboList, object keyValue)
        {
            if (connSrc == DataSrc.LOShare)
            {
                connSrc = DataSrc.LOShareROnly;
            }

            foreach (string combo in comboList)
            {
                string[] pair = combo.Split('|');
                string tableName = pair[0];
                string foreignKeyColumnName = pair[1];

                bool bOK = RegularExpressionHelper.IsMatch(RegularExpressionString.DBTableName, tableName);
                if (!bOK)
                {
                    throw new ApplicationException("Bad table name");
                }

                bOK = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, foreignKeyColumnName);
                if (!bOK)
                {
                    throw new ApplicationException("Bad column name");
                }

                SqlParameter[] para = new SqlParameter[] { new SqlParameter(foreignKeyColumnName, keyValue) };
                string query = "select " + foreignKeyColumnName + " from [" + tableName + "] where " + foreignKeyColumnName + " = @" + foreignKeyColumnName;

                bool hasData = false;
                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        hasData = true;
                    }
                };

                if (!TableReplicateTool.SharedTableNames.Contains(tableName) && MainDbDataSources.Contains(connSrc))
                {
                    DBSelectUtility.ProcessDBData(guidBrokerId, query, null, para, readHandler);
                }
                else
                {
                    DBSelectUtility.ProcessDBData(connSrc, query, null, para, readHandler);
                }

                if (hasData)
                {
                    return true;
                }
            }

            return false;
        }

        private static HashSet<DataSrc> MainDbDataSources = new HashSet<DataSrc>(new DataSrc[] { DataSrc.LOShare, DataSrc.LOShareROnly });

        /// <summary>
        /// Generate insertion script for one table entry by its "sqlParameter" list. Only the must-insert column are included in this insertion.
        /// </summary>
        /// <param name="errorList">Record the list of errors.</param>
        /// <param name="sqlScript">Result of generated sql script.</param> 
        /// <param name="testSqlScript">The script that contains the field value, directly runnable at sql studio.</param> 
        /// <param name="listOfParametersForTable">Parameter list of current table entry.</param> 
        /// <param name="columnInfoByColumnName">The column information of current table.</param> 
        /// <param name="tableName">Current table name.</param> 
        /// <param name="ignoreIdentityCol">The identity key name, which will not be included in the insertion script.</param> 
        public static void GetInsertScript_MinimumPara(
            ref List<string> errorList,
            ref string sqlScript,
            ref string testSqlScript,
            List<SqlParameter> listOfParametersForTable,
            Dictionary<string, ColumnInfo> columnInfoByColumnName,
            string tableName,
            string ignoreIdentityCol)
        {
            var headerSql = new StringBuilder("Insert into [" + tableName + "] ( ");
            var bodySql = new StringBuilder();
            var sqlToBeLogged = new StringBuilder();
            int numParameters = listOfParametersForTable.Count;

            if (ignoreIdentityCol != null && numParameters == 1)
            {
                sqlScript = "Insert into [" + tableName + "] default values";
                testSqlScript = "Insert into [" + tableName + "] default values";
                return;
            }

            for (int parameterIndex = 0; parameterIndex < numParameters; parameterIndex++)
            {
                var parameter = listOfParametersForTable[parameterIndex];
                var parameterName = parameter.ToString().TrimStart('@');

                if (string.Equals(parameterName, ignoreIdentityCol, StringComparison.OrdinalIgnoreCase))
                {
                    continue;
                }

                if(!columnInfoByColumnName.ContainsKey(parameterName))
                {
                    Tools.LogWarning($"Column {parameterName} of table {tableName} is present in the {nameof(listOfParametersForTable)} parameter but not in " +
                        $"the {nameof(columnInfoByColumnName)} parameter (the db) of the {nameof(GetInsertScript_MinimumPara)} method.");
                    continue;
                }

                string testParameterValue = (parameter.Value == DBNull.Value) ? "NULL" : parameter.Value.ToString();

                if (   columnInfoByColumnName[parameterName].DataType.Contains("binary") )
                {
                    if(!IsValidBinaryData(testParameterValue))
                    {
                        throw new GenericUserErrorMessageException($"The binary data for parameterName {parameterName} is not valid.");
                    }

                    // the reason to append the binary value itself instead of a reference to a sqlParameter is that
                    // it's not clear how to convert say 0x00 to a sql parameter.
                    bodySql.Append(testParameterValue + ",");
                    sqlToBeLogged.Append(testParameterValue + ",");
                }
                else
                {
                    bodySql.Append("@" + parameterName + ",");
                    sqlToBeLogged.Append("'" + testParameterValue + "',");
                }
                
                headerSql.Append(parameterName + ",");
            }

            sqlScript = headerSql.ToString().TrimEnd(',') + ")\nVALUES ( " + bodySql.ToString().TrimEnd(',') + ")";
            testSqlScript = "(TEST - real one uses sql parameters): " + headerSql.ToString().TrimEnd(',') + ")\nVALUES ( " + sqlToBeLogged.ToString().TrimEnd(',') + ")";
            if (bodySql.Length == 0)
            {
                errorList.Add("Empty insertion statement: " + sqlScript);
                return;
            }
        }

        private static bool IsValidBinaryData(string binaryString)
        {
            var factory = GenericLocator<IRegularExpressionDriverFactory>.Factory;
            var driver = factory.Create();
            return driver.IsMatch(RegularExpressionString.DbBinaryDataInStringForm, binaryString);
        }

        /// <summary>
        /// Generate update script for one table entry by its "sqlParameter" list.
        /// </summary>
        /// <param name="errorList">Record the list of errors.</param>
        /// <param name="sqlScript">Result of generated sql script.</param> 
        /// <param name="sqlUpdateScriptForLogging">The script that contains the field value, directly runnable at sql studio.</param> 
        /// <param name="listOfParametersForTable">Parameter list of current table entry.</param> 
        /// <param name="columnInfoByColumnName">The column information of current table.</param> 
        /// <param name="tableName">Current table name.</param> 
        /// <param name="listOfPrimaryKeyNamesAndValues">A list stores both primary key names and values consecutively.</param> 
        public static void GetUpdateScript_ReplicateTool(
            ref List<string> errorList,
            ref string sqlScript,
            ref string sqlUpdateScriptForLogging,
            List<SqlParameter> listOfParametersForTable,
            Dictionary<string, ColumnInfo> columnInfoByColumnName,
            string tableName,
            List<string> listOfPrimaryKeyNamesAndValues)
        {
            StringBuilder headSql = new StringBuilder("UPDATE [" + tableName + "] SET ");
            StringBuilder testHeadSql = new StringBuilder("UPDATE [" + tableName + "] SET ");
            StringBuilder tailSql = new StringBuilder(" WHERE ");
            StringBuilder testTailSql = new StringBuilder(" WHERE ");
            int numPrimaryKeyColumns = listOfPrimaryKeyNamesAndValues.Count();
            for (int i = 0; i < numPrimaryKeyColumns; i += 2)
            {
                var primaryKeyColumnName = listOfPrimaryKeyNamesAndValues[i];
                var primaryKeyColumnValue = listOfPrimaryKeyNamesAndValues[i + 1];

                tailSql.Append(primaryKeyColumnName + " = @" + primaryKeyColumnName + " and ");
                testTailSql.Append(primaryKeyColumnName + " = '" + primaryKeyColumnValue + "' and ");
            }

            string tailSqlStr = tailSql.ToString().Substring(0, tailSql.Length - 5);
            int numParameters = listOfParametersForTable.Count;
            for (int i = 0; i < numParameters; i++)
            {
                var parameter = listOfParametersForTable[i];
                string parameterName = parameter.ToString();

                if (!columnInfoByColumnName.ContainsKey(parameterName))
                {
                    Tools.LogWarning($"Column {parameterName} of table {tableName} is present in the {nameof(listOfParametersForTable)} parameter but not in " +
                        $"the {nameof(columnInfoByColumnName)} parameter (the db) of the {nameof(GetUpdateScript_ReplicateTool)} method.");
                    continue;
                }

                var columnInfo = columnInfoByColumnName[parameterName];

                if (columnInfo.IsComputed || columnInfo.DataType == "timestamp")
                {
                    // these are auto calculated, so we can't update them.
                    continue;
                }

                string testParameterValue = (parameter.Value == DBNull.Value) ? "NULL" : parameter.Value.ToString();
                if (columnInfo.DataType.Contains("binary"))
                {
                    if (!IsValidBinaryData(testParameterValue))
                    {
                        throw new GenericUserErrorMessageException($"The binary data for parameterName {parameterName} is not valid.");
                    }

                    // the reason to append the binary value itself instead of a reference to a sqlParameter is that
                    // it's not clear how to convert set say 0x00 to a sql parameter.
                    headSql.Append(parameterName + " = " + testParameterValue + ",");
                    testHeadSql.Append(parameterName + " = " + testParameterValue + ",\n");
                }
                else
                {
                    headSql.Append(parameterName + " = @" + parameterName + ",");
                    testHeadSql.Append(parameterName + " = '" + testParameterValue + "',\n");
                }
            }

            sqlScript = headSql.ToString().TrimEnd(',') + tailSqlStr;
            sqlUpdateScriptForLogging = testHeadSql.ToString().TrimEnd(',') + testTailSql;
            if (headSql.Length <= 12 + tableName.Length)
            {
                errorList.Add("Empty update statement: " + sqlScript);
                return;
            }

            // if any primary key is missing in the sql parameter list, add them into it.
            for (int i = 0; i < numPrimaryKeyColumns; i += 2)
            {
                int ind = listOfParametersForTable.FindIndex(delegate(SqlParameter para)
                {
                    return para.ToString().Equals(listOfPrimaryKeyNamesAndValues[i]);
                });
                if (ind < 0)
                {
                    listOfParametersForTable.Add(new SqlParameter(listOfPrimaryKeyNamesAndValues[i], listOfPrimaryKeyNamesAndValues[i + 1]));
                }
            }
        }

        /// <summary>
        /// Execute a list of sql query with parameters in one transaction. It has specialized function part for identity key.
        /// It will records the inserted identity key during insertion and record it in the identity key dictionary.
        /// Only when the transaction runs successfully, then it will write the identity key dictionary into the identity key mapping in database.
        /// Notice that only the main database has broker while data like pricing, rate can only be found by data source. That's the reason I require both broker id and data source.
        /// </summary>
        /// <param name="errorList">The list stores errors.</param>
        /// <param name="identityKeyOldNewMapping">A dictionary records the identity key old value and new value.</param>
        /// <param name="connSrc">The connection data source for database query.</param>
        /// <param name="connBrokerId">The broker id for database query.</param>
        /// <param name="tableIdentityKeyDictionary">Dictionary that maps table name to identity column name.</param>
        /// <param name="autoIncreFkTableTrioListMapping">Dictionary that records foreign key that refers to identity key:<code>foreign_key_table -> identity_key_table|identity_key|foreign_key</code>.</param>
        /// <param name="sqlCmdList">A list of sql commands.</param>
        /// <param name="sqlParaListList">A list of list of sql parameters corresponding to each command in the sql command list.</param>
        /// <param name="tableNmList">A list of table names in the same sequence of the sql command list.</param>
        /// <param name="defaultForeignKeyValDictionary">A dictionary that records foreign key default value.<code>tableNm|Fk -> defaultValue</code></param>
        /// <param name="mainTestScriptList">Record the list of executable sql scripts for error log.</param>
        /// <param name="justinOneSqlQueryTimeOutInSeconds">Time out for query.</param>
        /// <param name="needDebugInfo">If true, it will show the log information in the Paul Bunyan message viewer. It has many message. </param>
        /// <param name="debugRandCode">A random code to help search the error message in log.</param>
        /// <param name="implicitIdentityKeyReferencesByReferencingTableName">A list of references to identity keys that are implicit rather than explicit.</param>
        public static void ExecSqlInOneTransaction_IdentityKey(
            ref List<string> errorList,
            ref Dictionary<string, string> identityKeyOldNewMapping,
            DataSrc connSrc,
            Guid connBrokerId,
            Dictionary<string, string> tableIdentityKeyDictionary,
            Dictionary<string, List<string>> autoIncreFkTableTrioListMapping,
            List<string> sqlCmdList,
            List<List<SqlParameter>> sqlParaListList,
            List<string> tableNmList,
            Dictionary<string, string> defaultForeignKeyValDictionary,
            List<string> mainTestScriptList,
            int? justinOneSqlQueryTimeOutInSeconds,
            bool needDebugInfo,
            string debugRandCode,
            ILookup<string, ValidatedImplicitIdentityKeyReference> implicitIdentityKeyReferencesByReferencingTableName)
        {
            bool isException = true;
            DbTransaction tx = null;
            int i = 0;
            int numOfCmd = sqlCmdList.Count();
            if (numOfCmd == 0)
            {
                return;
            }
            
            try
            {
                using (DbConnection conn = (connSrc == DataSrc.LOShare) ? DbAccessUtils.GetConnection(connBrokerId) : DbAccessUtils.GetConnection(connSrc))
                {
                    conn.OpenWithRetry();
                    var command = conn.CreateCommand();
                    tx = conn.BeginTransaction(IsolationLevel.RepeatableRead);

                    if (needDebugInfo)
                    {
                        StringBuilder parasDebugInfo = new StringBuilder();
                        foreach (SqlParameter para in sqlParaListList[0])
                        {
                            parasDebugInfo.Append(para.ToString() + " = " + para.Value + ";  ");
                        }

                        Tools.LogInfo(debugRandCode + "Transaction has [" + numOfCmd + "] commands. 1st cmd: " + tableNmList[0] + ", " + parasDebugInfo.ToString());
                    }

                    for (i = 0; i < numOfCmd; i++)
                    {
                        string tableName = tableNmList[i];
                        // this is the fkTable that refer the identity key.
                        if (autoIncreFkTableTrioListMapping.ContainsKey(tableName))
                        {
                            List<string> foreignKeyTrioList = autoIncreFkTableTrioListMapping[tableName];
                            foreach (string trioStr in foreignKeyTrioList)
                            {
                                string oldTrioKey;
                                string[] trio = trioStr.Split('|'); // rkTable|rk|fk
                                int ind = sqlParaListList[i].FindIndex(delegate(SqlParameter para)
                                {
                                    return para.ToString().Equals(trio[2]);
                                });

                                if (ind < 0)
                                {
                                    continue;
                                }

                                string oldValue = Convert.ToString(sqlParaListList[i][ind].Value);
                                oldTrioKey = trio[0] + "|" + trio[1] + "|" + oldValue;
                                //// if the identity key value is null or empty or negative or default value, skip check
                                if (oldValue == "null" ||
                                    oldValue == null ||
                                    oldValue == string.Empty ||
                                    oldValue.TrimStart().StartsWith("-") ||  //// consider a negative identity key value is kind of impossible normally
                                    (defaultForeignKeyValDictionary.ContainsKey(tableName + "|" + trio[2]) &&
                                    defaultForeignKeyValDictionary[tableName + "|" + trio[2]] == oldValue))
                                {
                                    continue;  // cautious! this is probably wrong.
                                }
                                else
                                {
                                    if (!identityKeyOldNewMapping.ContainsKey(oldTrioKey))
                                    {
                                        errorList.Add("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before import table " + tableName + ": " + oldTrioKey);
                                        Tools.LogError("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before import table " + tableName + ": " + oldTrioKey);
                                        tx.Rollback();
                                        tx = null;
                                        return;
                                    }

                                    sqlParaListList[i][ind].Value = identityKeyOldNewMapping[oldTrioKey];
                                }
                            }
                        }

                        if (implicitIdentityKeyReferencesByReferencingTableName.Contains(tableName))
                        {
                            var implicitIdentityKeyReferencesForTable = implicitIdentityKeyReferencesByReferencingTableName[tableName];
                            foreach (var implicitReferenceForTable in implicitIdentityKeyReferencesForTable)
                            {
                                string identityKeyTableName = implicitReferenceForTable.IdentityKeyTableName;
                                string identityKeyColumnName = implicitReferenceForTable.IdentityKeyColumName;
                                string referencingColumnName = implicitReferenceForTable.ReferencingColumnName;

                                int ind = sqlParaListList[i].FindIndex(delegate (SqlParameter para)
                                {
                                    return para.ToString().Equals(referencingColumnName);
                                });

                                if (ind < 0)
                                {
                                    continue;
                                }

                                string oldValue = Convert.ToString(sqlParaListList[i][ind].Value);

                                switch (implicitReferenceForTable.Type)
                                {
                                    case ImplicitReferenceType.XPathToText:
                                    case ImplicitReferenceType.XPathAndAttribute:
                                        if (!ReplaceXmlValueWithIdentityKeySubstitutions(errorList, tableName, oldValue, identityKeyOldNewMapping, implicitReferenceForTable, sqlParaListList[i][ind]))
                                        {
                                            tx.Rollback();
                                            tx = null;
                                            return;
                                        }
                                        break;
                                    case ImplicitReferenceType.DbToDb:
                                        {
                                            string oldTrioKey = identityKeyTableName.ToUpper() + "|" + identityKeyColumnName + "|" + oldValue;
                                            //// if the identity key value is null or empty or negative or default value, skip check

                                            if (!identityKeyOldNewMapping.ContainsKey(oldTrioKey))
                                            {
                                                errorList.Add("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before import table " + tableName + ": " + oldTrioKey);
                                                Tools.LogError("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before import table " + tableName + ": " + oldTrioKey);
                                                tx.Rollback();
                                                tx = null;
                                                return;
                                            }

                                            sqlParaListList[i][ind].Value = identityKeyOldNewMapping[oldTrioKey];
                                        }
                                        break;
                                }
                            }
                        }

                        if (tableIdentityKeyDictionary.ContainsKey(tableName))
                        {
                            // This is the identity key table
                            string identityKey = tableIdentityKeyDictionary[tableName];
                            int ind = sqlParaListList[i].FindIndex(delegate(SqlParameter para)
                            {
                                return para.ToString().Equals(identityKey);
                            });

                            if (ind >= 0)
                            {
                                string oldValue = Convert.ToString(sqlParaListList[i][ind].Value);
                                string oldTrioKey = tableName + "|" + identityKey + "|" + oldValue;
                                if (identityKeyOldNewMapping.ContainsKey(oldTrioKey))
                                {
                                    sqlParaListList[i][ind].Value = identityKeyOldNewMapping[oldTrioKey];
                                }
                                else if (sqlCmdList[i].ToLower().Contains("update"))
                                {
                                    errorList.Add("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before other import: " + oldTrioKey);
                                    tx.Rollback();
                                    tx = null;
                                    return;
                                }
                            }
                        }

                        try
                        {
                            using (var reader = ShareImportExport.ExecuteReaderDangerously(conn, tx, command, justinOneSqlQueryTimeOutInSeconds, sqlCmdList[i], sqlParaListList[i].ToArray()))
                            {
                                bool hasLine = reader.Read();
                                if (hasLine && tableIdentityKeyDictionary.ContainsKey(tableName))
                                {
                                    string identityKey = Convert.ToString(reader[0]).TrimWhitespaceAndBOM();
                                    string colName = reader.GetName(0).TrimWhitespaceAndBOM();
                                    SqlParameter identityPara = sqlParaListList[i].Find(delegate(SqlParameter para)
                                    {
                                        return para.ToString().Equals(colName);
                                    });

                                    string oldKey = tableName + "|" + colName + "|" + identityPara.Value;
                                    if (!identityKeyOldNewMapping.ContainsKey(oldKey))
                                    {
                                        identityKeyOldNewMapping.Add(oldKey, identityKey); // ikTable|ikNm|ikOldVal => ikNewVal
                                    }
                                    else
                                    {
                                        identityKeyOldNewMapping[oldKey] = identityKey;
                                    }
                                }
                            }
                        }
                        catch (SqlException e)
                        {
                            // Sometimes we try to insert into an employee's favorite reports a report that belongs to another broker
                            // Usually one of our own, and if that's the case we shouldn't rollback the transaction.
                            if (string.Compare(tableName, "Employee_Favorite_Reports", true) == 0 
                                && e.Message.Contains("The INSERT statement conflicted with the FOREIGN KEY constraint \"fk_EMPLOYEE_FAVORITE_REPORTS_REPORT_QUERY\""))
                            {
                                string part = string.Join(", " + Environment.NewLine, sqlParaListList[i].Select(p => p.AsSimpleFormatString()).ToArray());
                                var msg = "Probably we tried to give an employee a favorite report from another broker, like one of our own.\n" + sqlCmdList[i] + "\n" + part; 

                                Tools.LogWarning(msg, e);
                                command.Parameters.Clear();
                                continue;
                            }
                            else
                            {
                                throw;
                            }
                        }

                        command.Parameters.Clear();
                    }

                    tx.Commit();
                    tx = null;
                }

                if (needDebugInfo)
                {
                    Tools.LogInfo(debugRandCode + "Broker Import Transaction finish successfully.");
                }

                isException = false; //// for all exception, not just sqlException
            }
            catch (SqlException exc)
            {
                Tools.LogError("Justin Exception Message:\r\n" + exc.ToString());
                if (tx != null)
                {
                    Tools.LogWarning("Try to roll back the code in transaction at TableReplicateTool.cs");
                    tx.Rollback();
                    tx = null;
                }

                throw;
            }
            finally
            {
                if (isException)
                {
                    if (i >= mainTestScriptList.Count())
                    {
                        Tools.LogError("Finish all sql command and encounter error at last moment. \n\nLast Command: sql[" + (i - 1) + "] " + mainTestScriptList[i - 1] + "\n\n");
                    }
                    else
                    {
                        Tools.LogError("Fail at sql[" + i + "]: \n" + mainTestScriptList[i]);
                    }
                }                
            }
        }

        /// <summary>
        /// Replaces the old value with any identity keys it refers to.
        /// </summary>
        /// <param name="errors">Any errors that occur along the way.</param>
        /// <param name="tableName">The table where the value is housed.</param>
        /// <param name="oldValue">The old xml value.</param>
        /// <param name="identityKeyOldNewMapping">The mapping from exported values to imported values, from the database.</param>
        /// <param name="implicitReferenceForTable">The information about the reference in the value to the identity key.</param>
        /// <param name="parameter">The parameter to update.</param>
        /// <returns>True on success, false on fail.</returns>
        private static bool ReplaceXmlValueWithIdentityKeySubstitutions(
            List<string> errors,
            string tableName,
            string oldValue,
            Dictionary<string, string> identityKeyOldNewMapping,
            ValidatedImplicitIdentityKeyReference implicitReferenceForTable,
            SqlParameter parameter)
        {
            string identityKeyTableName = implicitReferenceForTable.IdentityKeyTableName;
            string identityKeyColumnName = implicitReferenceForTable.IdentityKeyColumName;
            string referencingColumnName = implicitReferenceForTable.ReferencingColumnName;

            Dictionary<string, string> newValueByOldValue = new Dictionary<string, string>();
            XDocument d = XDocument.Parse(oldValue);
            var missingIdentityKeyValues = new HashSet<string>();
            IEnumerable<string> values;
            switch (implicitReferenceForTable.Type)
            {
                case ImplicitReferenceType.XPathToText:
                    values = XmlReplacer.GetXmlTextValues(d, implicitReferenceForTable.XPath);
                    break;
                case ImplicitReferenceType.XPathAndAttribute:
                    values = XmlReplacer.GetXmlAttributeValues(d, implicitReferenceForTable.XPath, implicitReferenceForTable.AttributeName);
                    break;
                default:
                    throw new UnhandledEnumException(implicitReferenceForTable.Type);
            }

            foreach (string textValue in values)
            {
                string oldTrioKey = identityKeyTableName.ToUpper() + "|" + identityKeyColumnName + "|" + textValue;
                if (!identityKeyOldNewMapping.ContainsKey(oldTrioKey))
                {
                    missingIdentityKeyValues.Add(oldTrioKey);
                }
                else
                {
                    newValueByOldValue.Add(textValue, identityKeyOldNewMapping[oldTrioKey]);
                }
            }

            if (missingIdentityKeyValues.Any())
            {
                foreach (var missingValue in missingIdentityKeyValues)
                {
                    errors.Add("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before import table " + tableName + ": " + missingValue);
                    Tools.LogError("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before import table " + tableName + ": " + missingValue);
                }
                return false;
            }

            var extraErrors = new List<string>();
            
            switch (implicitReferenceForTable.Type)
            {
                case ImplicitReferenceType.XPathToText:
                    XmlReplacer.ReplaceXmlTextValues(d, implicitReferenceForTable.XPath, newValueByOldValue, (absent) =>
                    {
                        extraErrors.Add("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before import table " + tableName + ": " + absent);
                        extraErrors.Add($"{nameof(XmlReplacer.ReplaceXmlTextValues)} method is got text values not found with {nameof(XmlReplacer.GetXmlTextValues)}.");
                    });
                    break;
                case ImplicitReferenceType.XPathAndAttribute:
                    XmlReplacer.ReplaceXmlAttributeValues(d, implicitReferenceForTable.XPath, implicitReferenceForTable.AttributeName, newValueByOldValue, (absent) =>
                    {
                        extraErrors.Add("Please make sure to insert the identity key mapping entry in table DATA_IMPORT_IDENTITY_COLUMN_MAP before import table " + tableName + ": " + absent);
                        extraErrors.Add($"{nameof(XmlReplacer.ReplaceXmlAttributeValues)} method is got text values not found with {nameof(XmlReplacer.GetXmlAttributeValues)}.");
                    });
                    break;
                default:
                    throw new UnhandledEnumException(implicitReferenceForTable.Type);
            }

            if (extraErrors.Any())
            {
                errors.AddRange(extraErrors);
                foreach (string error in extraErrors)
                {
                    Tools.LogError(error);
                }
                return false;
            }

            parameter.Value = d.ToString(SaveOptions.DisableFormatting);
            return true;
        }


        /// <summary>
        /// A function for exporting one column into xml.
        /// </summary>
        /// <param name="errorList">Record the errors during the running.</param>
        /// <param name="whitelistDictionary">A dictionary maps table name to a white list data object.</param>
        /// <param name="needUpdateWhitelist">Check if need to update white list. For now, update only if it finds new column.</param>
        /// <param name="exportXmlWriter">The xml writer for writing xml into file.</param>
        /// <param name="tableNm">The name of table.</param>
        /// <param name="tableMap">A dictionary that maps columns to column information.</param>
        /// <param name="startTag">The beginning tag of this column in xml.</param>
        /// <param name="reader">The sql reader that reads the current column now.</param>
        /// <param name="readerInd">The index of sql data reader.</param>
        /// <param name="isErrorFk">When it is true, it means the foreign key, though has value, points to no entry. Such foreign key should not be exported.</param>
        /// <param name="originalValue">The original value of this column in database.</param>
        /// <param name="passwordHashAndSalt">A size two array that stores password hash value and password salt value so as to used as a default value for consumer portal columns that need to have both random.</param>
        /// <param name="needDebugInfo">If true, it will show the log information in the Paul Bunyan message viewer. It has many message. </param>
        public static void ExportOneColumn(
            ref List<string> errorList,
            ref Dictionary<string, WhitelistData> whitelistDictionary,
            ref bool needUpdateWhitelist,
            XmlWriter exportXmlWriter,
            string tableNm,
            Dictionary<string, ColumnInfo> tableMap,
            string startTag,
            DbDataReader reader,
            int readerInd,
            bool isErrorFk,
            //// bool isWaived,
            object originalValue,
            string[] passwordHashAndSalt,
            bool needDebugInfo)
        {
            WhitelistData whitelist = whitelistDictionary[tableNm];
            string colName = reader.GetName(readerInd);
            string colRawVal = reader[readerInd].ToString();
            HashSet<string> exportColSet = whitelist.ExportSet;
            HashSet<string> reviewColSet = whitelist.ReviewSet;
            Dictionary<string, string> protectColDictionary = whitelist.ProtectedDictionary;
            bool isExportableCol = exportColSet.Contains(colName);
            bool isProtectedCol = protectColDictionary.ContainsKey(colName);
            bool isNoExportNoImportCol = whitelist.NoImportExportSet.Contains(colName);
            bool isNotAllowUpdate = whitelist.NoUpdateSet.Contains(colName);
            bool isOnlyUpdateProtected = whitelist.UpdateProtectDictionary.ContainsKey(colName);
            string protectedValue = isProtectedCol ? protectColDictionary[colName] : null;

            if (isNoExportNoImportCol)
            {
                return;
            }
            else if (isProtectedCol && protectColDictionary[colName].Contains("ParseXml") && colRawVal != string.Empty && colRawVal != null)
            {
                string parseTableNm = tableNm + "|" + "ParseXml|" + colName;
                string[] protectedTokens = isProtectedCol ? protectColDictionary[colName].Split('|') : null;
                string escapeMethod = protectedTokens[1];
                string filteredContent = ShareImportExport.FilterXmlByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, parseTableNm, colRawVal, escapeMethod, needDebugInfo);
                exportXmlWriter.WriteStartElement(startTag);
                exportXmlWriter.WriteAttributeString("n", colName);
                //// string colValue = ShareImportExport.ClearNullCharacter(filteredContent);
                string colValue = filteredContent;
                exportXmlWriter.WriteAttributeString("v", colValue);
                exportXmlWriter.WriteEndElement();
                return;
            }
            else if (isProtectedCol && protectColDictionary[colName].Contains("ParseJson") && colRawVal != string.Empty && colRawVal != null)
            {
                string parseTableNm = tableNm + "|" + "ParseJson|" + colName;
                string[] protectedTokens = isProtectedCol ? protectColDictionary[colName].Split('|') : null;
                string escapeMethod = protectedTokens[1];
                string filteredContent = ShareImportExport.FilterJsonByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, parseTableNm, colRawVal, escapeMethod, needDebugInfo);
                exportXmlWriter.WriteStartElement(startTag);
                exportXmlWriter.WriteAttributeString("n", colName);
                //// string colValue = ShareImportExport.ClearNullCharacter(filteredContent);
                string colValue = filteredContent;
                exportXmlWriter.WriteAttributeString("v", colValue);
                exportXmlWriter.WriteEndElement();
            }
            else if (isProtectedCol && tableMap[colName].Nullable && (reader[readerInd].ToString() == string.Empty || reader[readerInd] == DBNull.Value || reader[readerInd] == null))
            {
                return;
            }
            else if (tableMap[colName].IsComputed)
            {
                return;
            }
            else
            {
                exportXmlWriter.WriteStartElement(startTag);
                exportXmlWriter.WriteAttributeString("n", colName);

                string colValue = null;
                if (isNoExportNoImportCol == false && isProtectedCol == false && isExportableCol == false && isNotAllowUpdate == false && isOnlyUpdateProtected == false)
                {
                    colValue = tableMap[colName].Nullable ? "NULL" : ShareImportExport.GetDefaultDatabaseValByType(tableMap[colName].DataType, colName, tableNm).ToString();
                    exportXmlWriter.WriteAttributeString("v", colValue);
                    if (!reviewColSet.Contains(colName))
                    {
                        reviewColSet.Add(colName);
                        needUpdateWhitelist = true;
                    }
                }
                else
                {
                    colValue = ShareImportExport.ExportDataConvert(tableMap[colName].DataType, reader[readerInd]);
                }

                if (isProtectedCol)
                {
                    if (reader[readerInd].ToString() == string.Empty || reader[readerInd] == DBNull.Value || reader[readerInd] == null)
                    {
                        if (tableMap[colName].Nullable == false)
                        {
                            exportXmlWriter.WriteAttributeString("v", string.Empty);
                            exportXmlWriter.WriteAttributeString("protectThis", "1");
                            exportXmlWriter.WriteAttributeString("OriginalEmptyOrNullVal", string.Empty);
                        }
                    }
                    else
                    {
                        string resultVal = string.Empty;
                        string defaultVal = protectColDictionary[colName];
                        if (defaultVal.StartsWith("CONSUMER_PORTAL_TEMP_PASSWORD_"))
                        {
                            if (defaultVal.StartsWith("CONSUMER_PORTAL_TEMP_PASSWORD_HASH{"))
                            {
                                if (passwordHashAndSalt[0] != string.Empty)
                                {
                                    resultVal = passwordHashAndSalt[0];
                                    passwordHashAndSalt[0] = string.Empty;
                                }
                                else
                                {
                                    string pwdSaltVal;
                                    WhitelistData.ConvertWhitelistPasswordCode(out resultVal, out pwdSaltVal, defaultVal, new string[] { });
                                    passwordHashAndSalt[0] = string.Empty;
                                    passwordHashAndSalt[1] = pwdSaltVal;
                                }
                            }
                            else if (defaultVal.StartsWith("CONSUMER_PORTAL_TEMP_PASSWORD_SALT{"))
                            {
                                if (passwordHashAndSalt[1] != string.Empty)
                                {
                                    resultVal = passwordHashAndSalt[1];
                                    passwordHashAndSalt[1] = string.Empty;
                                }
                                else
                                {
                                    string pwdHashVal;
                                    WhitelistData.ConvertWhitelistPasswordCode(out pwdHashVal, out resultVal, defaultVal, new string[] { });
                                    passwordHashAndSalt[0] = pwdHashVal;
                                    passwordHashAndSalt[1] = string.Empty;
                                }
                            }
                        }
                        else
                        {
                            string unusedSaltVal;
                            WhitelistData.ConvertWhitelistPasswordCode(out resultVal, out unusedSaltVal, defaultVal, new string[] { });
                        }

                        ////string defaultVal = WhitelistData.ConvertWhitelistSpecialCode(protectColDictionary[colName], new string[] { });
                        exportXmlWriter.WriteAttributeString("v", resultVal);
                        exportXmlWriter.WriteAttributeString("protectThis", "1");
                    }
                }
                else if (isExportableCol)
                {
                    if (isErrorFk || reader.IsDBNull(readerInd))
                    {
                        if (((ColumnInfo)tableMap[colName]).Nullable)
                        {
                            exportXmlWriter.WriteAttributeString("v", "null");
                        }
                        else
                        {
                            exportXmlWriter.WriteAttributeString("v", string.Empty);
                        }
                    }
                    else if (((ColumnInfo)tableMap[colName]).DataType == "uniqueidentifier" && colValue == string.Empty)
                    {
                        exportXmlWriter.WriteAttributeString("v", "00000000-0000-0000-0000-000000000000");
                    }
                    else
                    {
                        exportXmlWriter.WriteAttributeString("v", colValue);
                    }

                    if (isErrorFk)
                    {
                        exportXmlWriter.WriteAttributeString("errorFk", Convert.ToString(originalValue));
                    }
                }

                ColumnInfo curColumnInfo = (ColumnInfo)tableMap[colName];
                if (curColumnInfo.Nullable)
                {
                    exportXmlWriter.WriteAttributeString("isNullable", "1");
                }

                if (curColumnInfo.IsComputed || curColumnInfo.DataType == "timestamp")
                {
                    exportXmlWriter.WriteAttributeString("isReadonly", "1");
                }

                if (whitelist.NoUpdateSet.Contains(colName))
                {
                    exportXmlWriter.WriteAttributeString("noUpdate", "1");
                }
                else if (whitelist.UpdateProtectDictionary.ContainsKey(colName))
                {
                    string resultVal = string.Empty;
                    string defaultVal = whitelist.UpdateProtectDictionary[colName];
                    if (defaultVal.StartsWith("CONSUMER_PORTAL_TEMP_PASSWORD_"))
                    {
                        if (defaultVal.StartsWith("CONSUMER_PORTAL_TEMP_PASSWORD_HASH{"))
                        {
                            if (passwordHashAndSalt[0] != string.Empty)
                            {
                                resultVal = passwordHashAndSalt[0];
                                passwordHashAndSalt[0] = string.Empty;
                            }
                            else
                            {
                                string pwdSaltVal;
                                WhitelistData.ConvertWhitelistPasswordCode(out resultVal, out pwdSaltVal, defaultVal, new string[] { });
                                passwordHashAndSalt[0] = string.Empty;
                                passwordHashAndSalt[1] = pwdSaltVal;
                            }
                        }
                        else if (defaultVal.StartsWith("CONSUMER_PORTAL_TEMP_PASSWORD_SALT{"))
                        {
                            if (passwordHashAndSalt[1] != string.Empty)
                            {
                                resultVal = passwordHashAndSalt[1];
                                passwordHashAndSalt[1] = string.Empty;
                            }
                            else
                            {
                                string pwdHashVal;
                                WhitelistData.ConvertWhitelistPasswordCode(out pwdHashVal, out resultVal, defaultVal, new string[] { });
                                passwordHashAndSalt[0] = pwdHashVal;
                                passwordHashAndSalt[1] = string.Empty;
                            }
                        }
                    }
                    else
                    {
                        string unusedSaltVal;
                        WhitelistData.ConvertWhitelistPasswordCode(out resultVal, out unusedSaltVal, defaultVal, new string[] { });
                    }

                    ////string defaultVal = WhitelistData.ConvertWhitelistSpecialCode(whitelist.UpdateProtectDictionary[colName], new string[] { });
                    exportXmlWriter.WriteAttributeString("updateSubstitution", resultVal);
                }

                exportXmlWriter.WriteEndElement();
            }
        }

        /// <summary>
        /// A function for exporting one column into xml.
        /// </summary>
        /// <param name="errorList">Record the errors during the running.</param>
        /// <param name="whitelist">The white list data object.</param>
        /// <param name="exportXmlWriter">The xml writer for writing xml into file.</param>
        /// <param name="tableNm">The name of table.</param>
        /// <param name="tableMap">A dictionary that maps columns to column information.</param>
        /// <param name="startTag">The beginning tag of this column in xml.</param>
        /// <param name="columnName">The column name of this column.</param>
        /// <param name="designatedValue">The value you want to assign to this exported column.</param>
        /// <param name="isErrorFk">When it is true, it means the foreign key, though has value, points to no entry. Such foreign key should not be exported.</param>
        /// <param name="isWaived">Check if this column should be waived from exporting the real value.</param>
        /// <param name="originalValue">The original value of this column in database.</param>
        /*public static void ExportOneColumnByDesignatedValue(
            ref List<string> errorList,
            ref WhitelistData whitelist,
            XmlWriter exportXmlWriter,
            string tableNm,
            Dictionary<string, ColumnInfo> tableMap,
            string startTag,
            string columnName,
            string designatedValue,
            bool isErrorFk,
            bool isWaived,
            object originalValue)
        {
            bool isExportableCol = whitelist.ExportSet.Contains(columnName);
            bool isProtectedCol = whitelist.ProtectedDictionary.ContainsKey(columnName);
            bool isNoExportNoImportCol = whitelist.NoImportExportSet.Contains(columnName);
            bool isNotAllowUpdate = whitelist.NoUpdateSet.Contains(columnName);
            bool isOnlyUpdateProtected = whitelist.UpdateProtectDictionary.ContainsKey(columnName);

            const string XmlPattern = @"&lt;.+&gt;";
            string colName = columnName; // no data type, so set it null

            exportXmlWriter.WriteStartElement(startTag);
            exportXmlWriter.WriteAttributeString("n", colName);

            string colValue = null;
            if (isNoExportNoImportCol)
            {
                return;
            }
            else if (isNoExportNoImportCol == false && isProtectedCol == false && isExportableCol == false && isNotAllowUpdate == false && isOnlyUpdateProtected == false)
            {
                colValue = tableMap[columnName].Nullable ? "NULL" : ShareImportExport.GetDefaultDatabaseValByType(tableMap[columnName].DataType, columnName, tableNm).ToString();
                exportXmlWriter.WriteAttributeString("v", colValue);
                if (!whitelist.ReviewSet.Contains(columnName))
                {
                    whitelist.ReviewSet.Add(columnName);
                }
            }
            else
            {
                colValue = ShareImportExport.ExportDataConvert(tableMap[columnName].DataType, designatedValue);
            }

            bool isXmlContent = false;
            if (isErrorFk || designatedValue.ToLower() == "null" || (((ColumnInfo)tableMap[colName]).DataType == "uniqueidentifier" && colValue.TrimWhitespaceAndBOM() == string.Empty))
            {
                if (((ColumnInfo)tableMap[colName]).Nullable)
                {
                    exportXmlWriter.WriteAttributeString("v", "null");
                }
                else
                {
                    exportXmlWriter.WriteAttributeString("v", string.Empty);
                }
            }
            else if (System.Text.RegularExpressions.Regex.IsMatch(colValue, XmlPattern))
            {
                isXmlContent = true;
            }
            else if (isWaived)
            {
                exportXmlWriter.WriteAttributeString("v", "null");
                //// exportXmlWriter.WriteAttributeString("originalVal", Convert.ToString(originalValue));
            }
            else
            {
                exportXmlWriter.WriteAttributeString("v", colValue);
            }

            ColumnInfo curColumnInfo = (ColumnInfo)tableMap[colName];
            if (curColumnInfo.Nullable)
            {
                exportXmlWriter.WriteAttributeString("isNullable", "1");
            }

            if (curColumnInfo.IsComputed || curColumnInfo.DataType == "timestamp")
            {
                exportXmlWriter.WriteAttributeString("isReadonly", "1");
            }

            if (whitelist.NoUpdateSet.Contains(colName))
            {
                exportXmlWriter.WriteAttributeString("noUpdate", "1");
            }
            else if (whitelist.UpdateProtectDictionary.ContainsKey(colName))
            {
                exportXmlWriter.WriteAttributeString("updateSubstitution", whitelist.UpdateProtectDictionary[colName]);
            }

            if (isErrorFk)
            {
                exportXmlWriter.WriteAttributeString("errorFk", Convert.ToString(originalValue));
            }

            if (isXmlContent)
            {
                colValue = ShareImportExport.ClearNullCharacter(colValue);
                exportXmlWriter.WriteCData(colValue);
            }

            exportXmlWriter.WriteEndElement();
        }*/

        /// <summary>
        /// Create sql parameters if the broker doesn't have database connection string.
        /// </summary>
        /// <param name="errorList">Record any error.</param>
        /// <param name="connInfoParaList">A list of sql parameters that store the connection information.</param>
        /// <param name="brokerId">The id of broker.</param>
        /// <param name="customerCode">The customer code.</param>
        public static void GetParameters_DB_Connection(ref List<string> errorList, out List<SqlParameter> connInfoParaList, string brokerId, string customerCode)
        {
            connInfoParaList = new List<SqlParameter>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_CheckIfTableExists", new SqlParameter("@tableName", "DB_CONNECTION")))
            {
                if (!reader.Read())
                {
                    return;
                }
            }

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_CheckIfTableExists", new SqlParameter("@tableName", "DB_CONNECTION_x_BROKER")))
            {
                if (!reader.Read())
                {
                    return;
                }
            }

            string databaseConnId = null;
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "DB_CONNECTION_RetrieveByBrokerId", new SqlParameter("@BrokerId", brokerId)))
            {
                if (reader.Read())
                {
                    return;
                }
            }

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetDefault_DBConnection"))
            {
                if (reader.Read())
                {
                    databaseConnId = Convert.ToString(reader["id"]).TrimWhitespaceAndBOM();
                }
                else
                {
                    reader.NextResult();
                    if (reader.Read())
                    {
                        databaseConnId = Convert.ToString(reader["id"]).TrimWhitespaceAndBOM();
                    }
                }
            }

            if (databaseConnId == null)
            {
                errorList.Add("There is no row in DB_CONNECTION for import: brokerId " + brokerId + ". Have the DB_CONNECTION been set up yet?");
                Tools.LogError("There is no row in DB_CONNECTION for import: brokerId " + brokerId + ". Have the DB_CONNECTION been set up yet?");
                return;
            }
            else
            {
                connInfoParaList.Add(new SqlParameter("@DbConnectionId", databaseConnId));
                connInfoParaList.Add(new SqlParameter("@BrokerId", brokerId));
                connInfoParaList.Add(new SqlParameter("@CustomerCode", customerCode));
            }
        }

        /// <summary>
        /// Import the database connection information by xml elements.
        /// </summary>
        /// <param name="errorList">List records errors.</param>
        /// <param name="connInfoComboElements">The xml elements that store connection information.</param>
        public static void ImportDbConnectionInfoByXmlElements(ref List<string> errorList, IEnumerable<XElement> connInfoComboElements)
        {
            if (connInfoComboElements == null || !connInfoComboElements.Any())
            {
                return;
            }

            foreach (XElement connInfoComboElement in connInfoComboElements)
            {
                string[] tokens = connInfoComboElement.Value.Split('|');
                string brokerId = tokens[0];
                string customerCode = tokens[1];
                List<SqlParameter> connInfoParaList = new List<SqlParameter>();
                TableReplicateTool.GetParameters_DB_Connection(ref errorList, out connInfoParaList, brokerId, customerCode);
                bool isDBConnByBrokerIdExist = false;

                SqlParameter[] parameters = 
                {
                    new SqlParameter("@BrokerId", brokerId)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "DB_CONNECTION_RetrieveByBrokerId", parameters))
                {
                    if (reader.Read())
                    {
                        isDBConnByBrokerIdExist = true;
                    }
                }

                if (connInfoParaList.Any())
                {
                    if (isDBConnByBrokerIdExist == false)
                    {
                        StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DB_CONNECTION_x_BROKER_Add", 0, connInfoParaList.ToArray());
                    }
                    else
                    {
                        StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DB_CONNECTION_x_BROKER_Update", 0, connInfoParaList.ToArray());
                    }
                }
            }
        }

        /// <summary>
        /// Fetch the sql parameters from xml element.
        /// When check exist, remember to do the identity key mapping first. 
        /// 1. it is identity key table;  2. its foreign key is an identity key, and this foreign key is a primary key at the same time.
        /// We cannot update the primary key list to be the actual identity value. The reason is that in the execTransaction, it will check again and if we change the value, it might find other key values.
        /// </summary>
        /// <param name="errorList">List that stores error messages.</param>
        /// <param name="insertParameters">List of parameters for inserts.</param>
        /// <param name="updateParameters">List of parameters for updates.</param>
        /// <param name="primaryKeyNameAndValueList">List of primary key: <code>list: pk1Nm|pk1Val|pk2Nm|pk2Val</code></param>
        /// <param name="connectionSource">The data source for query.</param>
        /// <param name="connectionBrokerId">The broker id for query.</param>
        /// <param name="columnInfoByColumnName">A dictionary that maps columns to column information.</param>
        /// <param name="tableElement">The xml element of the whole table.</param>
        /// <param name="identityKeyTargetValueBySourceTableColumnAndValue">A dictionary records the identity key old value and new value. <para/>Structure: table_name|column_name|source_value -> thisDbs_value.</param>
        /// <param name="identityKeyColumnNameByTableName">Dictionary maps table name to identity key.</param>
        /// <param name="referencedIdentityKeyTriosByTableName">A dictionary. For a given table name gives the list of identity_key_table_name|identity_key_name|referencing_column_name that the table refers to.</param>
        /// <param name="defaultExternalBrokerReferenceFields">If true it will default fields like sInvestorRolodexId to some default,
        /// since otherwise they could just fail.</param>
        public static void GetParameters_OneXmlTable_FullReplicate_v2(
            ref List<string> errorList,
            ref List<SqlParameter> insertParameters,
            ref List<SqlParameter> updateParameters,
            ref List<string> primaryKeyNameAndValueList,
            DataSrc connectionSource,
            Guid connectionBrokerId,
            Dictionary<string, ColumnInfo> columnInfoByColumnName,
            XElement tableElement,
            Dictionary<string, string> identityKeyTargetValueBySourceTableColumnAndValue,
            Dictionary<string, string> identityKeyColumnNameByTableName,
            Dictionary<string, List<string>> referencedIdentityKeyTriosByTableName,
            bool defaultExternalBrokerReferenceFields)
        {
            string tableName = tableElement.Attribute("n").Value;
            insertParameters = new List<SqlParameter>();
            updateParameters = new List<SqlParameter>();
            primaryKeyNameAndValueList = new List<string>();
            List<string> priKeyWhereList = new List<string>();
            HashSet<string> primaryKeyColumnNames = new HashSet<string>();
            IEnumerable<XAttribute> tableElementAttributes = tableElement.Attributes();

            bool ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBTableName, tableName);
            if (!ok)
            {
                throw new ApplicationException("Bad table name");
            }

            // Extract the primary key names and values.
            foreach (XAttribute attribute in tableElementAttributes)
            {
                if (attribute.Name.ToString().StartsWith("pk"))
                {
                    priKeyWhereList.Add(attribute.Value);
                    primaryKeyNameAndValueList.Add(attribute.Value);
                    primaryKeyColumnNames.Add(attribute.Value);
                }
                else if (attribute.Name.ToString().StartsWith("val"))
                {
                    priKeyWhereList.Add(attribute.Value);
                    primaryKeyNameAndValueList.Add(attribute.Value);
                }
            }

            bool entryExistsInDbMappingOrTable = false;
            bool tableHasIdentity = identityKeyColumnNameByTableName.ContainsKey(tableName);
            bool tableHasColumnThatIsBothPrimaryAndIdentity = tableHasIdentity && primaryKeyColumnNames.Contains(identityKeyColumnNameByTableName[tableName]);
            bool tableRefersToAnIdentityKey = referencedIdentityKeyTriosByTableName.ContainsKey(tableName);
            string columnNameThatsBothPrimaryAndIdentity = tableHasColumnThatIsBothPrimaryAndIdentity ? identityKeyColumnNameByTableName[tableName] : null;

            if (tableHasColumnThatIsBothPrimaryAndIdentity)
            {
                // If tableHasColumnThatIsBothPrimaryAndIdentity, look for the entry in the db's identity key mapping table and set entryExistsInDbMappingOrTable accordingly.

                IEnumerable<XElement> identityKeyElements = from tag in tableElement.Elements() where tag.Attribute("n").Value == columnNameThatsBothPrimaryAndIdentity select tag;
                if (identityKeyElements.Any() == false)
                {
                    var msg = $"Xml for table {tableName} is missing element for column {columnNameThatsBothPrimaryAndIdentity} that is both an identity and primary key.";
                    errorList.Add(msg);
                    Tools.LogError(msg);
                    return;
                }

                if (identityKeyElements.Count() > 1)
                {
                    Tools.LogWarning($"The xml for table {tableName} contained multiple elements with name {columnNameThatsBothPrimaryAndIdentity}.  We used the first value.");
                }

                string oldValue = identityKeyElements.First().Attribute("v").Value;
                if (identityKeyTargetValueBySourceTableColumnAndValue.ContainsKey(tableName + "|" + columnNameThatsBothPrimaryAndIdentity + "|" + oldValue))
                {
                    // tdsk: need to double check that it actually exists in the db.  if it doesn't, remove it from the database mapping.
                    entryExistsInDbMappingOrTable = true;
                }                
            }
            else
            {
                // If not tableHasPrimaryKeyColumnThatIsIdentity, check that the table exists in the db.

                // If one of the primary key columns refers to an identity key of another table,
                // our (primary key) where condition should use the mapped (current db) identity key to check for its existence.
                if (tableRefersToAnIdentityKey)
                {
                    var referencedIdentityKeyTrios = referencedIdentityKeyTriosByTableName[tableName];
                    foreach (string trio in referencedIdentityKeyTrios)
                    {
                        string columnNameThatRefersToIdentityKey = trio.Substring(trio.LastIndexOf("|") + 1);
                        if (!primaryKeyColumnNames.Contains(columnNameThatRefersToIdentityKey))
                        {
                            continue;
                        }

                        // Apply to title_vendor_x_broker and other's whose identity fk is also primary key but not identity key table.
                        int ind = priKeyWhereList.IndexOf(columnNameThatRefersToIdentityKey);
                        if (ind < 0)
                        {
                            continue;
                        }

                        var sourceTrioKey = trio.Substring(0, trio.LastIndexOf("|") + 1) + priKeyWhereList[ind + 1];
                        if (identityKeyTargetValueBySourceTableColumnAndValue.ContainsKey(sourceTrioKey))
                        {
                            priKeyWhereList[ind + 1] = identityKeyTargetValueBySourceTableColumnAndValue[sourceTrioKey];
                        }
                    }
                }

                // tdsk: what if the xml doesn't supply enough primary keys / primary key values?  The where clause better have it anyway.
                var whereClauses = new List<string>();
                var primaryKeyParameters = new List<SqlParameter>();
                for (int i = 0; i < priKeyWhereList.Count(); i += 2)
                {
                    ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, priKeyWhereList[i]);
                    if (!ok)
                    {
                        throw new ApplicationException("Bad column name");
                    }

                    whereClauses.Add(priKeyWhereList[i] + " = @" + priKeyWhereList[i]);
                    primaryKeyParameters.Add(new SqlParameter("@" + priKeyWhereList[i], priKeyWhereList[i + 1]));
                }

                // tdsk: change '*' to just one column, but also make sure that the table has some sort of primary key.
                string checkExistSql = $"select top 1 * from [{tableName}] where " + string.Join(" and ", whereClauses);

                entryExistsInDbMappingOrTable = CheckExist(connectionSource, connectionBrokerId, entryExistsInDbMappingOrTable, primaryKeyParameters, checkExistSql);   
            }

            if (entryExistsInDbMappingOrTable)
            {
                // Either it exists as a non-identity table, or it's a table with an identity key that was found in the identity key mapping table.
                // Since entry exists, update the update parameters (all but primary key)

                // This list consists of elements that are nonnullable keys per the xml, but doesn't include the primary keys. 
                // You're left with nonnullable foreign keys, but not the foreign keys that are primary.
                List<XElement> nonNullableKeyButNotPkElements = new List<XElement>();
                IEnumerable<XElement> nonNullableKeyElements = tableElement.Elements("tag_noNullKey");
                foreach (XElement nonNullableKeyElement in nonNullableKeyElements)
                {
                    string keyName = nonNullableKeyElement.Attribute("n").Value;
                    if (!primaryKeyColumnNames.Contains(keyName))
                    {
                        nonNullableKeyButNotPkElements.Add(nonNullableKeyElement);
                    }
                }

                GetParametersByXelementList(ref updateParameters, nonNullableKeyButNotPkElements, columnInfoByColumnName, null, true, defaultExternalBrokerReferenceFields);
                GetParametersByXelementList(ref updateParameters, tableElement.Elements("tag_noNullCol"), columnInfoByColumnName, null, true, defaultExternalBrokerReferenceFields);
                GetParametersByXelementList(ref updateParameters, tableElement.Elements("tag_nullableCol"), columnInfoByColumnName, null, true, defaultExternalBrokerReferenceFields);
                GetParametersByXelementList(ref updateParameters, tableElement.Elements("tag_nullableKey"), columnInfoByColumnName, null, true, defaultExternalBrokerReferenceFields);
            }
            else
            {
                // when the entry doesn't exist, update the insert parameters, but set the initial insert of nullable Key elements to null (primary key won't be nullable)
                // and update the nullable Keys instead.

                GetParametersByXelementList(ref insertParameters, tableElement.Elements("tag_noNullKey"), columnInfoByColumnName, null, false, defaultExternalBrokerReferenceFields);
                GetParametersByXelementList(ref insertParameters, tableElement.Elements("tag_noNullCol"), columnInfoByColumnName, null, false, defaultExternalBrokerReferenceFields);
                GetParametersByXelementList(ref updateParameters, tableElement.Elements("tag_nullableKey"), columnInfoByColumnName, null, false, defaultExternalBrokerReferenceFields);
                GetParametersByXelementList(ref insertParameters, tableElement.Elements("tag_nullableCol"), columnInfoByColumnName, null, false, defaultExternalBrokerReferenceFields);

                // tdsk: consider scrapping this.  this may be the reason that templates come across as valid even when invalid.
                if (tableName == "LOAN_FILE_A")
                {
                    int ind = insertParameters.FindIndex(para => para.ToString().Equals("IsValid"));
                    insertParameters[ind].Value = "False";
                    updateParameters.Add(new SqlParameter("IsValid", "True"));
                }
            }
        }

        /// <summary>
        /// Check whether a table exists with the input column data.
        /// </summary>
        /// <param name="connSrc">DataSrc target for the DB.</param>
        /// <param name="connBrokerId">BrokerId used to target the DB.</param>
        /// <param name="entryExist">Value of the entry list going in.</param>
        /// <param name="paraList">The list of sql parameters.</param>
        /// <param name="checkExistSql">The sql query.</param>
        /// <returns>True if the row exists in the DB.</returns>
        public static bool CheckExist(DataSrc connSrc, Guid connBrokerId, bool entryExist, List<SqlParameter> paraList, string checkExistSql)
        {
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                if (reader.Read())
                {
                    entryExist = true;
                }
            };

            if (connSrc == DataSrc.LOShare)
            {
                DBSelectUtility.ProcessDBData(connBrokerId, checkExistSql, null, paraList, readHandler);
            }
            else
            {
                DBSelectUtility.ProcessDBData(connSrc, checkExistSql, null, paraList, readHandler);
            }

            return entryExist;
        }

        /// <summary>
        /// Put the xml data into the parameter list.
        /// </summary>
        /// <param name="listOfParameters">List of sql parameters.</param>
        /// <param name="elements">List of xml column elements.</param>
        /// <param name="columnInfoByColumnNameForTable">Get the dictionary of column information of current table.</param>
        /// <param name="columnNamesToExclude">Exclude certain columns from insertion/update.</param>
        /// <param name="isThisUpdate">If true, this function is called for updating. Else, it is for insertion.</param>
        /// <param name="defaultExternalBrokerReferenceFields">If true it will default fields like sInvestorRolodexId to some default,
        /// since otherwise they could just fail.</param>
        public static void GetParametersByXelementList(
            ref List<SqlParameter> listOfParameters,
            IEnumerable<XElement> elements,
            ////WhitelistData whitelist,
            Dictionary<string, ColumnInfo> columnInfoByColumnNameForTable,
            HashSet<string> columnNamesToExclude,
            bool isThisUpdate,
            bool defaultExternalBrokerReferenceFields)
        {
            int debug_int = 0;
            Dictionary<string, SqlParameter> parameterByParameterName = new Dictionary<string, SqlParameter>();

            foreach (XElement el in elements)
            {
                bool isNullable = false, skipCurrentElement = false, isNotAllowUpdate = false, isUpdateProtected = false;
                string currentFieldName = null, currentFieldValue = null, updateSubstitution = null;

                string columnName = el.Attribute("n").Value;
                if (!columnInfoByColumnNameForTable.ContainsKey(columnName))
                {
                    // If the column name does not exist in the dictionary then the column doesn't exist in current database. Skip importing this column.
                    continue;
                }
                else if (columnNamesToExclude != null && columnNamesToExclude.Contains(columnName))
                {
                    continue;
                }

                var columnInfo = columnInfoByColumnNameForTable[columnName];
                isNullable = columnInfo.Nullable;
                SqlDbType columnSqlDbType;
                if(!columnInfo.TryGetSqlDbType(out columnSqlDbType))
                {                    
                    Tools.LogError($"Could not get the sql db type for the column {columnName}.");
                    return;
                }

                //if (el.Name.ToString().ToLower().Contains("nullable")) // SK - not sure why this was here.  we should trust the db not the xml.
                //{
                //    isNullable = true;
                //}

                debug_int++;
                IEnumerable<XAttribute> attributes = el.Attributes();
                foreach (XAttribute attribute in attributes)
                {
                    switch (attribute.Name.ToString())
                    {
                        case "n":
                            currentFieldName = attribute.Value;
                            break;

                        case "v":
                            ////curFieldValue = ShareImportExport.UnEscapeXmlCharacter(attri.Value);
                            currentFieldValue = attribute.Value;
                            break;

                        case "isNullable":
                            //// isNullable = true; // SK - not sure why this was here.  we should trust the db not the xml.
                            break;

                        case "isReadonly":
                            skipCurrentElement = true;
                            break;

                        case "originalVal":
                            break;

                        case "noUpdate":
                            isNotAllowUpdate = true;
                            break;

                        case "protectThis":
                            break;

                        case "updateSubstitution":
                            isUpdateProtected = true;
                            if (isThisUpdate)
                            {
                                ////updateSubstitution = ShareImportExport.UnEscapeXmlCharacter(attri.Value);
                                updateSubstitution = attribute.Value;
                            }

                            break;

                        case "OriginalEmptyOrNullVal":
                            break;

                        default:
                            Tools.LogError("Unknown attribute: " + attribute.Name.ToString());
                            throw new Exception("Unknown attribute: " + attribute.Name.ToString());
                    }

                    if (skipCurrentElement || (isNotAllowUpdate && isThisUpdate))
                    {
                        break;
                    }
                }

                if (skipCurrentElement || (isThisUpdate && isNotAllowUpdate))
                {
                    skipCurrentElement = false;
                    continue;
                }

                if (isUpdateProtected && isThisUpdate)
                {
                    currentFieldValue = updateSubstitution;
                }

                SqlParameter parameter = null;
                if (el.FirstNode != null && el.FirstNode.NodeType == XmlNodeType.CDATA)
                {
                    ////curFieldValue = ShareImportExport.UnEscapeXmlCharacter(el.Value.Trim('\0').TrimWhitespaceAndBOM());
                    parameter = new SqlParameter(currentFieldName, el.Value.Trim('\0'));
                }
                else if ((!skipCurrentElement) && (currentFieldValue != null))
                {
                    // "curFieldValue == null" means the value of xml is empty
                    if (currentFieldValue.ToLower() == "null")
                    {
                        if (isNullable)
                        {
                            //if (columnSqlDbType == SqlDbType.Binary || columnSqlDbType == SqlDbType.VarBinary)
                            //{
                            //    parameter = new SqlParameter(currentFieldName, columnSqlDbType);
                            //    parameter.Value = System.Data.SqlTypes.SqlBinary.Null;
                            //}
                            //else
                            //{
                                parameter = new SqlParameter(currentFieldName, columnSqlDbType)
                                {
                                    Value = DBNull.Value
                                };
                            //}
                        }
                        else
                        {
                            Tools.LogWarning($"Xml contained 'null' for {currentFieldName} which is not nullable.  Setting it to empty string instead.");
                            parameter = new SqlParameter(currentFieldName, string.Empty); //columnSqlDbType)
                            //{
                            //    Value = string.Empty
                            //};
                        }
                    }
                    else
                    {
                        if (defaultExternalBrokerReferenceFields && LoanOverwriteTool.FieldDefaultsForFieldsReferringToExternalBrokerByFieldName.ContainsKey(currentFieldName))
                        {
                            var replacementValue = LoanOverwriteTool.FieldDefaultsForFieldsReferringToExternalBrokerByFieldName[currentFieldName];
                            parameter = new SqlParameter(currentFieldName, replacementValue); //columnSqlDbType)
                            //{
                            //    Value = replacementValue
                            //};
                        }
                        else
                        {
                            parameter = new SqlParameter(currentFieldName, currentFieldValue);//columnSqlDbType)
                            //{
                            //    Value = currentFieldValue
                            //};
                        }
                    }
                }

                if (!parameterByParameterName.ContainsKey(parameter.ToString()))
                {
                    parameterByParameterName.Add(parameter.ToString(), parameter);
                }
                else
                {
                    Tools.LogError($"Xml contained duplicate field name {parameter.ToString()}");
                    parameterByParameterName[parameter.ToString()].Value = parameter.Value;
                }
            }

            listOfParameters.AddRange(parameterByParameterName.Values);
        }

        /// <summary>
        /// Delete the applications of the loan in database if application id of current loan doesn't exist in the xml.
        /// </summary>
        /// <param name="loanId">This is loan id.</param>
        /// <param name="xmlAppIdSet">The set of application id found in xml file.</param>
        /// <param name="dbaseAppIdSet">The set of application id from database by the current loan id.</param>
        /// <param name="userId">The id of user who calls this function.</param>
        public static void DeleteNonMatchApp(string loanId, HashSet<string> xmlAppIdSet, HashSet<string> dbaseAppIdSet, string userId)
        {
            foreach (string dbaseAppId in dbaseAppIdSet)
            {
                if (!xmlAppIdSet.Contains(dbaseAppId))
                {
                    ShareImportExport.DeleteApp(dbaseAppId, userId);
                }
            }
        }

        /// <summary>
        /// Get all tables that has a column ended with broker id by hardcode.
        /// The attribute of null-able is necessary for safe insertion. So we cannot force the null-able column to be none null-able. Instead we make this checking to ensure the correct logic.
        /// Basically, this is used to ensure the broker id looking columns are not null.
        /// </summary>
        /// <returns>A dictionary map table name to column ended with broker id string.</returns>
        public static Dictionary<string, List<string>> GetHardcodeTableWithBrokeIdColumnDictionary()
        {
            Dictionary<string, List<string>> noneNullTableColDictionary = new Dictionary<string, List<string>>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetTableWithBrokerIdColumn"))
            {
                while (reader.Read())
                {
                    string tableNm = reader["TABLE_NAME"].ToString().TrimWhitespaceAndBOM();
                    string colNm = reader["COLUMN_NAME"].ToString().TrimWhitespaceAndBOM();
                    if (!noneNullTableColDictionary.ContainsKey(tableNm))
                    {
                        noneNullTableColDictionary.Add(tableNm, new List<string>());
                    }

                    noneNullTableColDictionary[tableNm].Add(colNm);
                }
            }

            //// hardcode
            if (!noneNullTableColDictionary.ContainsKey("EDOCS_DOCUMENT_TYPE"))
            {
                noneNullTableColDictionary.Add("EDOCS_DOCUMENT_TYPE", new List<string>());
            }

            noneNullTableColDictionary["EDOCS_DOCUMENT_TYPE"].Add("FolderId");
            return noneNullTableColDictionary;
        }

        /// <summary>
        /// Check if the current xml is ok to be imported.
        /// </summary>
        /// <param name="errorList">List stores error.</param>
        /// <param name="tableList">List of xml elements of tables.</param>
        /// <returns>True: this xml can be imported. False: This xml is not qualified to be imported.</returns>
        public static bool AllowImport_HardcodeCheck(ref List<string> errorList, IEnumerable<XElement> tableList)
        {
            bool isPass = true;
            Dictionary<string, List<string>> colLikeBrokerIdDictionary = GetHardcodeTableWithBrokeIdColumnDictionary();
            foreach (XElement tableXml in tableList)
            {
                int xmlColCnt = tableXml.IsEmpty ? 0 : tableXml.Elements().Count();
                if (xmlColCnt == 0)
                {
                    continue;
                }

                string tableNm = tableXml.Attribute("n").Value;
                if (colLikeBrokerIdDictionary.ContainsKey(tableNm))
                {
                    List<string> colNmList = colLikeBrokerIdDictionary[tableNm];
                    foreach (string colNm in colNmList)
                    {
                        bool isThisColPass = true;
                        IEnumerable<XElement> noneNullColList = from noneNullCol in tableXml.Elements() where noneNullCol.Attribute("n").Value == colNm select noneNullCol;
                        if (noneNullColList == null || !noneNullColList.Any())
                        {
                            isPass = false;
                            isThisColPass = false;
                        }
                        else
                        {
                            foreach (XElement noneNullCol in noneNullColList)
                            {
                                string colVal = noneNullCol.Attribute("v").Value;
                                if (colVal == null || colVal == string.Empty)
                                {
                                    isPass = false;
                                    isThisColPass = false;
                                }
                            }
                        }

                        if (!isThisColPass)
                        {
                            errorList.Add("Table " + tableNm + " column " + colNm + " is not allowed to be empty.");
                        }
                    }
                }
            }

            return isPass;
        }

        /// <summary>
        /// For identity table, we might be able to tell if it is existed already by some non-identity columns based on unique index 
        /// constraint or intuition over certain non identity columns. If based on this standard, the identity entry exists. Even if 
        /// their identity key value is different or there is no such record in the identity key mapping table, we can still add it to it. 
        /// Two Parts:
        /// 1. Collect non-identity column values from imported xml file.
        /// 2. Collect current database's identity key value by the non identity column values from xml file. And use that identity key and
        /// xml identity key value to build the key mapping. (If you can find the identity entry by designated non identity column, that means the entry is identical to the one in xml.)
        /// Notice: if not written in the stored procedure, the identity table will not be checked or recollected in this function.
        /// Notice: the first column is identity column.
        /// </summary>
        /// <param name="errorList">Record error in list.</param>
        /// <param name="connSrc">Data source for query.</param>
        /// <param name="connBrokerId">Broker id for database query.</param>
        /// <param name="tableXml">One table entry's xml element.</param>
        /// <param name="identityKeyOldNewMapping">A dictionary map from remote database identity key value to current database one.</param>
        public static void ReCollectOneTableIdentityKeyMappingByNonIdentityColAtSp(
            ref List<string> errorList,
            DataSrc connSrc,
            Guid connBrokerId,
            XElement tableXml,
            Dictionary<string, string> identityKeyOldNewMapping)
        {
            string tableNm = tableXml.Attribute("n").Value;
            List<string> nonIdentityColList = new List<string>();
            int isByTableName = 1;
            int colCnt = 0;
            var firstRoundParameters = new SqlParameter[] 
            {
                new SqlParameter("@IsByTableName", isByTableName),
                new SqlParameter("@UserOption", "CheckIdentityEntryExist"),
                new SqlParameter("@InputTableName", tableNm)
            };
            using (var reader = (connSrc == DataSrc.LOShare) ?
                StoredProcedureHelper.ExecuteReader(connBrokerId, "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers", firstRoundParameters) :
                StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers", firstRoundParameters))
            {
                if (reader.Read())
                {
                    colCnt = reader.FieldCount;
                    for (int colInd = 0; colInd < colCnt; colInd++)
                    {
                        nonIdentityColList.Add(reader[colInd].ToString().TrimWhitespaceAndBOM());
                    }
                }
                else
                {
                    return;
                }
            }

            string identityColNm = string.Empty;
            string xmlIdentityVal = string.Empty;
            List<SqlParameter> secondRoundParameters = new List<SqlParameter>();
            secondRoundParameters.Add(new SqlParameter("@IsByTableName", isByTableName));
            secondRoundParameters.Add(new SqlParameter("@UserOption", "CheckIdentityEntryExist"));
            secondRoundParameters.Add(new SqlParameter("@InputTableName", tableNm));
            for (int i = 0; i < colCnt; i++)
            {
                string colNm = nonIdentityColList[i];
                IEnumerable<string> foundCols = from colElement in tableXml.Elements() where colElement.Attribute("n").Value == colNm select colElement.Attribute("v").Value;
                if (foundCols != null && foundCols.Any())
                {
                    if (i > 0)
                    {
                        secondRoundParameters.Add(new SqlParameter("@varcharKey" + i, foundCols.First()));
                    }
                    else
                    {
                        identityColNm = colNm;
                        xmlIdentityVal = foundCols.First();
                    }
                }
                else
                {
                    errorList.Add("Fail in Identity Entry Existence Check: Cannot find column " + colNm + " in xml. Please either allow the column to be exported or change stored procedure to skip the checking by this column.");
                    Tools.LogError("Fail in Identity Entry Existence Check: Cannot find column " + colNm + " in xml. Please either allow the column to be exported or change stored procedure to skip the checking by this column.");
                    return;
                }
            }

            using (var reader = (connSrc == DataSrc.LOShare) ?
                StoredProcedureHelper.ExecuteReader(connBrokerId, "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers", secondRoundParameters) :
                StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers", secondRoundParameters))
            {
                reader.NextResult();
                // There should be only one entry found by the non identity column since they act like primary key.
                if (reader.Read())
                {
                    string curDbIdentityColVal = reader[0].ToString();
                    string tripleKey = tableNm + "|" + identityColNm + "|" + xmlIdentityVal;
                    if (!identityKeyOldNewMapping.ContainsKey(tripleKey))
                    {
                        // It is the first time import this entry by the import tool but the entry already exists in DB. So, just add it to identity key mapping.
                        identityKeyOldNewMapping.Add(tripleKey, curDbIdentityColVal);
                    }
                    else
                    {
                        string identityKeyMappingColVal = identityKeyOldNewMapping[tripleKey];
                        if (identityKeyMappingColVal != curDbIdentityColVal)
                        {
                            errorList.Add("The xml identity table " + tripleKey + "should map to current database identity key value [" + curDbIdentityColVal + "]. But the identity key mapping already maps [" + identityKeyMappingColVal + "] to value [" + curDbIdentityColVal + "].");
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// The missing identity key mapping will be added here. So, no more need to do this part of job at get parameter function.
        /// There is no possibility for mismatch of identity key table since we can do the identity key map.
        /// But for non-identity key table, there might be conflicts over non-primary-key unique index.
        /// Remove the hardcode checking at get parameter function.
        /// This function will first use the unique index to query the primary key values. Then it will compare those value in the xml.
        /// If all primary key values match, it means the same entry in xml already exists in database and so it won't violate the unique index constraint.
        /// </summary>
        /// <param name="errorList">Record list of errors.</param>
        /// <param name="connSrc">The data source of the tables in xml element.</param>
        /// <param name="connBrokerId">The broker id for query. The broker id used to find where this table is.</param>
        /// <param name="tableXml">The xml element of this table.</param>
        /// <param name="tableUniqueIdxColDictionary">The dictionary maps table to columns group by unique index.</param>
        /// <param name="tableIdentityKeyDictionary">Map table name to identity key name.</param>
        /// <param name="allTablesColumnInfo">The column information of all tables. It is used here to check if a column is computed.</param>
        /// <returns>If true, it means no mismatch. If false, it means there is mismatch.</returns>
        public static bool CheckUniqueIndexIfMatch_NonIdentityTable(
            ref List<string> errorList,
            DataSrc connSrc,
            Guid connBrokerId,
            XElement tableXml,
            Dictionary<string, List<List<string>>> tableUniqueIdxColDictionary,
            Dictionary<string, string> tableIdentityKeyDictionary,
            Dictionary<string, Dictionary<string, ColumnInfo>> allTablesColumnInfo)
        {
            string tableNm = tableXml.Attribute("n").Value;
            if (!tableUniqueIdxColDictionary.ContainsKey(tableNm))
            {
                // This table has no unique index. So no need to check.
                return true;
            }

            if (tableIdentityKeyDictionary.ContainsKey(tableNm))
            {
                // as per JJ's comment on this method, we don't need to check identity key tables since we can do the identity key map.
                return true; 
            }

            bool ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBTableName, tableNm);
            if (!ok)
            {
                throw new ApplicationException("Bad table name");
            }

            List<List<string>> curTableIdxList = tableUniqueIdxColDictionary[tableNm];
            int debug_int = 0;
            
            // Query the primary key by the unique index columns. If primary key exists, check if they match.
            Dictionary<string, string> xmlPrimaryKeyValueByName;
            GetSubPrimaryKeysForTableElement(out xmlPrimaryKeyValueByName, tableXml);

            foreach (List<string> colListPerIdx in curTableIdxList)
            {
                debug_int++;
                var uniqueIdxMsg = new StringBuilder();
                var whereClauses = new List<string>();
                var sqlParameters = new List<SqlParameter>();

                foreach (string colNm in colListPerIdx)
                {
                    if (allTablesColumnInfo[tableNm][colNm].IsComputed)
                    {
                        continue;
                    }

                    ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, colNm);
                    if (!ok)
                    {
                        throw new ApplicationException("Bad column name");
                    }

                    IEnumerable<XElement> values = from e in tableXml.Elements() where e.Attribute("n").Value == colNm select e;

                    if (values != null && values.Any() && values.First() != null)
                    {
                        var parameterName = "@" + colNm;

                        if (values.First().Attribute("v") != null)
                        {
                            string colVal = values.First().Attribute("v").Value;
                            whereClauses.Add(colNm + " = " + parameterName);
                            sqlParameters.Add(new SqlParameter(parameterName, colVal));
                            uniqueIdxMsg.Append(colNm + "[" + colVal + "],");
                        }
                        else if (values.First().Value != null)
                        {
                            string colVal = values.First().Value;
                            whereClauses.Add(colNm + " = " + parameterName);
                            sqlParameters.Add(new SqlParameter(parameterName, colVal));
                            uniqueIdxMsg.Append(colNm + "[" + colVal + "],");
                        }
                    }
                    else
                    {
                        var firstPartOfErrorMessage = "The xml for table " + tableNm + " is missing an element for column " + colNm + " which is a member of the table's unique indexes.";
                        errorList.Add(firstPartOfErrorMessage + "  Details in PB log.");
                        Tools.LogError(firstPartOfErrorMessage + " The following is the xml: " + tableXml.ToString());
                    }
                }

                if (!whereClauses.Any())
                {
                    continue;
                }

                StringBuilder dbasePrimaryKeyInOneString = new StringBuilder();
                string queryUniqueIdxVal = "SELECT " + string.Join(", ", xmlPrimaryKeyValueByName.Keys.ToArray()) + " FROM [" + tableNm + "] WHERE " + string.Join(" AND ", whereClauses.ToArray());

                string errorFound = null;
                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    if (reader.Read())
                    {
                        bool isIndexValInUseAlready = false;
                        foreach (string primaryKey in xmlPrimaryKeyValueByName.Keys)
                        {
                            string dbasePkVal = Convert.ToString(reader[primaryKey]).TrimWhitespaceAndBOM();
                            dbasePrimaryKeyInOneString.Append(primaryKey + "[" + dbasePkVal + "] ");
                            if (dbasePkVal != xmlPrimaryKeyValueByName[primaryKey])
                            {
                                isIndexValInUseAlready = true;
                            }
                        }

                        if (isIndexValInUseAlready)
                        {
                            errorFound = uniqueIdxMsg.ToString().TrimEnd(',') +
                                " required to be unique by table " + tableNm +
                                " coming from xml " + string.Join(string.Empty, xmlPrimaryKeyValueByName.Select(kvp => string.Format("{0}[{1}]", kvp.Key, kvp.Value)).ToArray()) +
                                " is already in use by table row " + dbasePrimaryKeyInOneString + ". Please choose different value for this unique index.";
                        }
                    }
                    else
                    {
                        // Cannot find the primary key value by the unique index. That is such index value is not in-use. So you can use it.
                    }
                };

                if (errorFound != null)
                {
                    errorList.Add(errorFound);
                }

                if (connSrc == DataSrc.LOShare)
                {
                    DBSelectUtility.ProcessDBData(connBrokerId, queryUniqueIdxVal, null, sqlParameters, readHandler);

                    if (errorFound != null)
                    {
                        errorList.Add(errorFound);
                    }
                }
                else
                {
                    DBSelectUtility.ProcessDBData(connSrc, queryUniqueIdxVal, null, sqlParameters, readHandler);

                    if (errorFound != null)
                    {
                        errorList.Add(errorFound);
                    }
                }
            }            

            return true;
        }

        /*/// <summary>
        /// Import all the table entries in the xml file into database.
        /// Non-foreignKey field will be inserted: set in export.
        /// </summary>
        /// <param name="errorList">List that records error messages.</param>
        /// <param name="xmlPath">The address of xml file.</param>
        /// <param name="whitelistDictionary">Dictionary maps table name to white list data object.</param>
        /// <param name="option">Option can be loan or broker. If is loan, it needs to export file database data.</param>
        /// <param name="userId">The id of user who calls this function.</param>
        public static void ImportOneXmlFile_Old(ref List<string> errorList, string xmlPath, Dictionary<string, WhitelistData> whitelistDictionary, string option, string userId)
        {
            int debug_ind = 0;
            if (errorList.Any())
            {
                return;
            }

            XDocument xdoc = XDocument.Load(xmlPath);
            IEnumerable<XElement> tableList = xdoc.Root.Elements("table");
            if (!AllowImport_HardcodeCheck(ref errorList, tableList))
            {
                return;
            }

            XElement filedbElement = xdoc.Root.Element("filedb");
            XAttribute stageSiteSourceNameAttr = xdoc.Root.Attribute("stageSiteSourceName");
            XAttribute databaseSourceNameAttr = xdoc.Root.Attribute("databaseSourceName");
            DataSrc databaseConnSrc = (xdoc.Root.Attribute("dbConnSrc").Value == "RATE_SHEET")? DataSrc.RateSheet : DataSrc.LOMain;
            
            if (stageSiteSourceNameAttr == null || databaseSourceNameAttr == null)
            {
                errorList.Add("stageSiteSourceName or databaseSourceName is missing at main tag Export. Do you try to import loan-overwrite xml through loan-replicate tool?");
                return;
            }

            ////Add Db_Connection_X_broker
            IEnumerable<XElement> connInfoElementList = xdoc.Root.Elements("ConnectionInfo");
            if (connInfoElementList != null)
            {
                XElement connInfoElement = connInfoElementList.First();
                if (connInfoElement != null)
                {
                    IEnumerable<XElement> brokerIdCustomerCodeComboList = connInfoElement.Elements("BrokerIdCustomerCodeCombo");
                    TableReplicateTool.ImportDbConnectionInfoByXmlElements(ref errorList, brokerIdCustomerCodeComboList);
                }
            }

            int capacity = tableList.Count();
            List<string> insertTableNmList = new List<string>(capacity);
            List<string> updateTableNmList = new List<string>(capacity);
            List<string> insertTableScriptList = new List<string>(capacity);
            List<string> updateTableScriptList = new List<string>(capacity);
            List<string> testInsertTableScriptList = new List<string>(capacity);
            List<string> testUpdateTableScriptList = new List<string>(capacity);
            List<List<SqlParameter>> insertParaListList = new List<List<SqlParameter>>();
            List<List<SqlParameter>> updateParaListList = new List<List<SqlParameter>>();
            Dictionary<string, Dictionary<string, ColumnInfo>> allTablesColumnInfo = new Dictionary<string, Dictionary<string, ColumnInfo>>();
            List<List<SqlParameter>> notNullParaListList = new List<List<SqlParameter>>();
            List<List<SqlParameter>> nullableParaListList = new List<List<SqlParameter>>();
            List<List<string>> priKeyListList = new List<List<string>>();
            Dictionary<string, string> tableIdentityKeyDictionary = ShareImportExport.GetIdentityTableKeyDictionary(databaseConnSrc); // rkTable as key
            Dictionary<string, List<string>> autoIncreFkTableTrioListMapping = ShareImportExport.HardCodeAutoIncreTableTrioList(); // fkTable as key
            Dictionary<string, string> identityKeyOldNewMapping = ShareImportExport.ReadIkOldNewMappingFromDB(stageSiteSourceNameAttr.Value, databaseSourceNameAttr.Value);
            Dictionary<string, string> oldIdKeyMapping = new Dictionary<string, string>(identityKeyOldNewMapping);
            Dictionary<string, string> defaultForeignKeyValDictionary = ShareImportExport.GetDefaultForeignKeyValDictionary();
            Dictionary<string, List<List<string>>> tableUniqueIdxColDictionary = ShareImportExport.GetUniqueIdxColDictionary();

            foreach (XElement table in tableList)
            {
                debug_ind++;

                int xmlColCnt = table.IsEmpty ? 0 : table.Elements().Count();
                if (xmlColCnt == 0)
                {
                    continue;
                }

                CheckUniqueIndexIfMatch_NonIdentityTable(ref errorList, table, tableUniqueIdxColDictionary, identityKeyOldNewMapping, tableIdentityKeyDictionary);
                if (errorList.Any())
                {
                    return;
                }

                List<SqlParameter> notNullParaList = new List<SqlParameter>(), nullableParaList = new List<SqlParameter>();
                List<string> priKeyList = new List<string>();
                string tableNm = table.Attribute("n").Value;
                if (!allTablesColumnInfo.ContainsKey(tableNm))
                {
                    Dictionary<string, ColumnInfo> curTableColumnInfo = ShareImportExport.GetColumnInfo(tableNm);
                    allTablesColumnInfo.Add(tableNm, curTableColumnInfo);
                }

                if (whitelistDictionary.ContainsKey(tableNm))
                {
                    if (whitelistDictionary[tableNm].ReviewSet.Any())
                    {
                        errorList.Add("Please review new column by export whitelist and move the column into <export> or <protected>.");
                        return;
                    }
                }
                else
                {
                    errorList.Add("table->" + tableNm + " is not found in whitelist.");
                }

                GetParameters_OneXmlTable_FullReplicate_v2(
                    ref errorList,
                    ref notNullParaList,
                    ref nullableParaList,
                    ref priKeyList,
                    allTablesColumnInfo[tableNm],
                    whitelistDictionary[tableNm],
                    table,
                    identityKeyOldNewMapping,
                    tableIdentityKeyDictionary,
                    autoIncreFkTableTrioListMapping); // nullableParaList includes update-only tags

                if (notNullParaList.Any())
                {
                    insertTableNmList.Add(tableNm);
                    notNullParaListList.Add(notNullParaList);
                }

                if (nullableParaList.Any())
                {
                    priKeyListList.Add(priKeyList);
                    updateTableNmList.Add(tableNm);
                    nullableParaListList.Add(nullableParaList);
                }

                if (tableNm == "BROKER")
                {
                    List<SqlParameter> databaseConnParaList = new List<SqlParameter>();
                    int indOfCustomerCode = priKeyList.FindIndex(x => x == "CustomerCode");
                    string cutomerCode = priKeyList[indOfCustomerCode + 1];
                    int indOfBrokerId = priKeyList.FindIndex(x => x == "BrokerId");
                    string brokerId = priKeyList[indOfBrokerId + 1];

                    if (errorList.Any())
                    {
                        return;
                    }
                    else if (databaseConnParaList.Any())
                    {
                        insertTableNmList.Add("DB_CONNECTION_x_BROKER");
                        notNullParaListList.Add(databaseConnParaList);
                    }
                }
            }

            if (errorList.Any())
            {
                return;
            }

            // the identity key will be ignored in the script generation
            for (int i = 0; i < insertTableNmList.Count; i++)
            {
                debug_ind++;
                string tableNm = insertTableNmList[i];
                List<SqlParameter> notNullParaList = notNullParaListList[i];
                string insertSql = string.Empty, testSql = string.Empty;
                string ignoreCol = tableIdentityKeyDictionary.ContainsKey(tableNm) ? tableIdentityKeyDictionary[tableNm] : null;
                GetInsertScript_MinimumPara(ref errorList, ref insertSql, ref testSql, notNullParaList, allTablesColumnInfo[tableNm], tableNm, ignoreCol);
                if (tableIdentityKeyDictionary.ContainsKey(tableNm))
                {
                    insertSql += " \nselect IDENT_CURRENT('" + tableNm + "') as " + tableIdentityKeyDictionary[tableNm]; // fetch the new identity key value
                }

                insertTableScriptList.Add(insertSql);
                insertParaListList.Add(notNullParaList); // use another listList to preserve the original one. This one got changed for identity key.
                testInsertTableScriptList.Add(testSql);
            }

            for (int i = 0; i < updateTableNmList.Count; i++)
            {
                debug_ind++;
                string tableNm = updateTableNmList[i];
                List<string> priKeyList = priKeyListList[i];
                List<SqlParameter> nullableParaList = nullableParaListList[i];
                string updateSql = string.Empty, testSql = string.Empty;
                GetUpdateScript_ReplicateTool(ref errorList, ref updateSql, ref testSql, nullableParaList, allTablesColumnInfo[tableNm], tableNm, priKeyList);
                updateTableScriptList.Add(updateSql);
                updateParaListList.Add(nullableParaList);
                testUpdateTableScriptList.Add(testSql);
            }

            List<string> mainScriptList = ShareImportExport.MergeTwoLists<string>(insertTableScriptList, updateTableScriptList);
            List<string> mainTestScriptList = ShareImportExport.MergeTwoLists<string>(testInsertTableScriptList, testUpdateTableScriptList);
            List<List<SqlParameter>> mainParaListList = ShareImportExport.MergeTwoLists<List<SqlParameter>>(insertParaListList, updateParaListList);
            List<string> mainTableNmList = ShareImportExport.MergeTwoLists<string>(insertTableNmList, updateTableNmList);

            if (option == "Loan" && mainTableNmList.Contains("LOAN_FILE_A") && filedbElement == null)
            {
                errorList.Add("Mistake in the xml file: The loan xml is missing filedb elements.");
                return;
            }

            ExecSqlInOneTransaction_IdentityKey(ref errorList, ref identityKeyOldNewMapping, string.Empty, tableIdentityKeyDictionary, autoIncreFkTableTrioListMapping, mainScriptList, mainParaListList, mainTableNmList, defaultForeignKeyValDictionary, mainTestScriptList, false);

            if (errorList.Any())
            {
                return;
            }

            string stageSiteSourceName = xdoc.Root.Attribute("stageSiteSourceName").Value;
            string databaseSourceName = xdoc.Root.Attribute("databaseSourceName").Value;
            ShareImportExport.WriteIkOldNewMappingBackToDB(ref oldIdKeyMapping, identityKeyOldNewMapping, stageSiteSourceName, databaseSourceName);

            //// Update filedb data
            if (option == "Loan" && mainTableNmList.Contains("LOAN_FILE_A"))
            {
                if (filedbElement == null)
                {
                    errorList.Add("Mistake in the xml file: The loan xml is missing filedb elements.");
                    return;
                }

                ShareImportExport.ImportFiledb(ref errorList, filedbElement);
            }
        }
        */

        /// <summary>
        /// A helper function for find next entry list function. It pushes the found entry into the result set. The set is to avoid duplicates.
        /// It also deal with some special cases of columns that usually restricted by trigger but not registered as primary key.
        /// When the table is loan or application, it will make sure they all be pushed so that none will miss.
        /// </summary>
        /// <param name="errorList">List stores the errors.</param>
        /// <param name="allFoundEntrySet">A hash set that stores the pushed entries.</param>
        /// <param name="queryResultEntries">A list stores query result. That is, a list of entries.</param>
        /// <param name="connDbSrc">Data source for all queries.</param>
        /// <param name="prevConnInfoBrokerId">The connection information to locate data source.</param>
        /// <param name="query">A query that finds the primary key value of the current table.</param>
        /// <param name="paras">Parameter array for the primary key query.</param>
        /// <param name="tableNm">The name of current table.</param>
        /// <param name="fieldNames">All the primary keys in the query, separating by comma.</param>
        public static void PushQueryResult(ref List<string> errorList, ref HashSet<string> allFoundEntrySet, ref List<string> queryResultEntries, DataSrc connDbSrc, Guid prevConnInfoBrokerId, string query, SqlParameter[] paras, string tableNm, string fieldNames)
        {
            string[] fields = fieldNames.Replace("T2.", string.Empty).Split(',');

            List<string> entryStrList = new List<string>();
            string firstKeyVal = null;
            bool isFirst = true;

            Action<IDataReader> readHandler = delegate (IDataReader reader)
            {
                while (reader.Read())
                {
                    StringBuilder oneEntry = new StringBuilder();
                    if (isFirst)
                    {
                        isFirst = false;
                        firstKeyVal = reader[fields[0]].ToString().TrimWhitespaceAndBOM();
                    }

                    foreach (string field in fields)
                    {
                        oneEntry.Append(field + "|" + reader[field].ToString().TrimWhitespaceAndBOM() + "|");
                    }

                    if (oneEntry.Length > 0)
                    {
                        entryStrList.Add(tableNm + "|" + oneEntry.Remove(oneEntry.Length - 1, 1).ToString());
                    }
                }
            };

            if (connDbSrc == DataSrc.LOShare)
            {
                DBSelectUtility.ProcessDBData(prevConnInfoBrokerId, query, null, paras, readHandler);
            }
            else
            {
                DBSelectUtility.ProcessDBData(connDbSrc, query, null, paras, readHandler);
            }

            if (isFirst)
            {
                return; // the query has no result
            }

            bool isLoan = tableNm.StartsWith("LOAN_FILE_");
            bool isApp = tableNm.StartsWith("APPLICATION_");
            if (isLoan || isApp)
            {
                // note the order of the keys must match Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers <your_table> when @isByTableName = 1
                AddExtraTablesThatReferToLoan(errorList, allFoundEntrySet, queryResultEntries, prevConnInfoBrokerId, firstKeyVal, isLoan);
            }
            else if (tableNm == "EMPLOYEE")
            {
                AddExtraTablesThatReferToEmployee(allFoundEntrySet, queryResultEntries, prevConnInfoBrokerId, firstKeyVal);
            }
            else if (tableNm == "BROKER")
            {
                AddExtraTablesThatReferToBroker(allFoundEntrySet, queryResultEntries, prevConnInfoBrokerId, firstKeyVal);
            }
            //else if(tableNm == "BRANCH")
            //{
            //    // e.g. group_x_branch
            //    AddExtraTablesThatReferToBranch(allFoundEntrySet, queryResultEntries, prevConnInfoBrokerId, firstKeyVal);
            //}
            //else if (tableNm == "PML_BROKER")
            //{
            //    // e.g. group_x_pml_broker
            //    AddExtraTablesThatReferToPmlBroker(allFoundEntrySet, queryResultEntries, prevConnInfoBrokerId, firstKeyVal);
            //}


            foreach (string oneEntryStr in entryStrList)
            {
                if (!allFoundEntrySet.Contains(oneEntryStr))
                {
                    queryResultEntries.Add(oneEntryStr);
                    allFoundEntrySet.Add(oneEntryStr);
                }
            }
        }

        private static void AddExtraTablesThatReferToEmployee(HashSet<string> allFoundEntrySet, List<string> queryResultEntries, Guid prevConnInfoBrokerId, string firstKeyVal)
        {
            /*
                e.g. role_assignment, group_x_employee, team_user_assignment
            */
            string curEmpEntry = "EMPLOYEE|EmployeeId|" + firstKeyVal;
            if (!allFoundEntrySet.Contains(curEmpEntry))
            {
                queryResultEntries.Add(curEmpEntry);
                allFoundEntrySet.Add(curEmpEntry);
            }

            // string roleAssignSql = "select RoleId from ROLE_ASSIGNMENT where EmployeeId = '" + firstKeyVal + "'";
            SqlParameter[] curParas = new SqlParameter[] { new SqlParameter("@EmployeeId", firstKeyVal) };
            using (var reader = StoredProcedureHelper.ExecuteReader(prevConnInfoBrokerId, "ROLE_ASSIGNMENT_ListByEmployeeId", curParas))
            {
                while (reader.Read())
                {
                    string subEntry = "ROLE_ASSIGNMENT|EmployeeId|" + firstKeyVal + "|RoleId|" + Convert.ToString(reader["RoleId"]).TrimWhitespaceAndBOM();
                    if (!allFoundEntrySet.Contains(subEntry))
                    {
                        queryResultEntries.Add(subEntry);
                        allFoundEntrySet.Add(subEntry);
                    }
                }
            }

            curParas = new SqlParameter[] { new SqlParameter("@EmployeeId", firstKeyVal) };
            using (var reader = StoredProcedureHelper.ExecuteReader(prevConnInfoBrokerId, "TEAM_USER_ASSIGNMENT_List", curParas))
            {
                while (reader.Read())
                {
                    string subEntry = string.Join("|", new string[] { "TEAM_USER_ASSIGNMENT", "EmployeeId", firstKeyVal, "TeamId", Convert.ToString(reader["TeamId"]) });
                    if (!allFoundEntrySet.Contains(subEntry))
                    {
                        queryResultEntries.Add(subEntry);
                        allFoundEntrySet.Add(subEntry);
                    }
                }
            }

            curParas = new SqlParameter[] { new SqlParameter("@EmployeeId", firstKeyVal), new SqlParameter("@BrokerId", prevConnInfoBrokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(prevConnInfoBrokerId, "GetGroupMembershipsForEmployee", curParas))
            {
                while (reader.Read())
                {
                    if ((int)reader["IsInGroup"] == 0)
                    {
                        continue; // non-employee group, ideally need another sproc.
                    }

                    string subEntry = string.Join("|", new string[] { "GROUP_X_EMPLOYEE", "EmployeeId", firstKeyVal, "GroupId", Convert.ToString(reader["GroupId"]) });
                    if (!allFoundEntrySet.Contains(subEntry))
                    {
                        queryResultEntries.Add(subEntry);
                        allFoundEntrySet.Add(subEntry);
                    }
                }
            }
        }

        private static void AddExtraTablesThatReferToLoan(List<string> errorList, HashSet<string> allFoundEntrySet, List<string> queryResultEntries, Guid prevConnInfoBrokerId, string firstKeyVal, bool isLoan)
        {
            //? what about rolodex?
            /*
                All lff, lfc tables, trust account, loan_user_assignments, team_loan_assignments, apps
            */
            string[] loanBundleList = new string[] { "LOAN_FILE_A", "LOAN_FILE_B", "LOAN_FILE_C", "LOAN_FILE_D", "LOAN_FILE_E", "LOAN_FILE_F", "LOAN_FILE_CACHE", "LOAN_FILE_CACHE_2", "LOAN_FILE_CACHE_3", "LOAN_FILE_CACHE_4", "TRUST_ACCOUNT" };

            string missingLoanId = isLoan ? firstKeyVal : TableReplicateTool.GetLoanIdByAppId(prevConnInfoBrokerId, firstKeyVal);

            foreach (string loanTable in loanBundleList)
            {
                string subEntry = loanTable + "|sLId|" + missingLoanId;
                if (loanTable == "LOAN_USER_ASSIGNMENT")
                {
                    continue;
                }

                if (!allFoundEntrySet.Contains(subEntry))
                {
                    queryResultEntries.Add(subEntry);
                    allFoundEntrySet.Add(subEntry);
                }
            }

            // string loanUserAssignmentSql = "select RoleId from LOAN_USER_ASSIGNMENT where sLId = '" + missingLoanId + "'";
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@loanId", missingLoanId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(prevConnInfoBrokerId, "ListLoanAssignmentsByLoanId", parameters))
            {
                while (reader.Read())
                {
                    string subEntry = "LOAN_USER_ASSIGNMENT|EmployeeId|" + Convert.ToString(reader["EmployeeId"]).TrimWhitespaceAndBOM() + "|RoleId|" + Convert.ToString(reader["RoleId"]).TrimWhitespaceAndBOM() + "|sLId|" + missingLoanId;
                    if (!allFoundEntrySet.Contains(subEntry))
                    {
                        queryResultEntries.Add(subEntry);
                        allFoundEntrySet.Add(subEntry);
                    }
                }
            }

            parameters = new SqlParameter[] { new SqlParameter("@LoanId", missingLoanId), new SqlParameter("@BrokerId", prevConnInfoBrokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(prevConnInfoBrokerId, "TEAM_ListTeamsByLoan", parameters))
            {
                while (reader.Read())
                {
                    string subEntry = string.Join("|", new string[] { "TEAM_LOAN_ASSIGNMENT", "LoanId", Convert.ToString(missingLoanId), "TeamId", Convert.ToString(reader["Id"]) });
                    if (!allFoundEntrySet.Contains(subEntry))
                    {
                        queryResultEntries.Add(subEntry);
                        allFoundEntrySet.Add(subEntry);
                    }
                }
            }

            // string appSql = "select aAppId from application_a where sLId = '" + missingLoanId + "'";
            bool hasApp = false;
            List<string> appIdList = ShareImportExport.GetAppIdListByLoanId(prevConnInfoBrokerId, missingLoanId);
            foreach (string appId in appIdList)
            {
                hasApp = true;
                string entryA = "APPLICATION_A|aAppId|" + appId.ToLower();
                string entryB = "APPLICATION_B|aAppId|" + appId.ToLower();
                if (!allFoundEntrySet.Contains(entryA))
                {
                    queryResultEntries.Add(entryA);
                    allFoundEntrySet.Add(entryA);
                }

                if (!allFoundEntrySet.Contains(entryB))
                {
                    queryResultEntries.Add(entryB);
                    allFoundEntrySet.Add(entryB);
                }
            }

            if (!hasApp)
            {
                string errorMsg = "application is missing in loan slid = " + missingLoanId;
                if (!errorList.Contains(errorMsg))
                {
                    errorList.Add(errorMsg);
                    Tools.LogError(errorMsg);
                }
            }
        }

        private static void AddExtraTablesThatReferToBroker(HashSet<string> allFoundEntrySet, List<string> queryResultEntries, Guid prevConnInfoBrokerId, string firstKeyVal)
        {
            /*
                Config_released, ??
            */
            var curParas = new SqlParameter[] { new SqlParameter("@OrgId", firstKeyVal) };
            using (var reader = StoredProcedureHelper.ExecuteReader(prevConnInfoBrokerId, "CONFIG_RELEASED_GetLatestId", curParas))
            {
                while (reader.Read())
                {
                    string subEntry = string.Join("|", new string[] { "CONFIG_RELEASED", "ConfigId", Convert.ToString(reader["ConfigId"]), "OrgId", firstKeyVal });
                    if (!allFoundEntrySet.Contains(subEntry))
                    {
                        queryResultEntries.Add(subEntry);
                        allFoundEntrySet.Add(subEntry);
                    }
                }
            }

            curParas = new SqlParameter[] { new SqlParameter("@BrokerId", firstKeyVal) };
            using (var reader = StoredProcedureHelper.ExecuteReader(prevConnInfoBrokerId, "Feature_Subscription_ListByBrokerId", curParas))
            {
                while (reader.Read())
                {
                    string subEntry = string.Join("|", new string[] { "FEATURE_SUBSCRIPTION", "BrokerId", firstKeyVal, "FeatureId", Convert.ToString(reader["FeatureId"]) });
                    if (!allFoundEntrySet.Contains(subEntry))
                    {
                        queryResultEntries.Add(subEntry);
                        allFoundEntrySet.Add(subEntry);
                    }
                }
            }
        }

        /// <summary>
        /// Using the current list of tables and its primary keys to find all foreign keys of those tables. 
        /// Then use the foreign keys to find the reference tables and the primary keys to form the entry list as the result.
        /// The current server will send the entry to the remote server by service to check if the table entries are missing there.
        /// Don't forget to check duplicate entry for the return list.
        /// two base lists are needed. One is the result list, to remove duplicate before adding new entry.
        /// The other one is the major list that record all the previous sent lists so that it won't send those lists again. Avoid dead loop.
        /// It is possible to send multiple keys with same key names and same table but diff value, that is diff entries of the same table.
        /// <Updates>I add database connection information to it. I assume that if one table row belongs to one broker, it will not associate with another broker.
        /// This is important because every related entry found will reuse the database connection the previous entry use.
        /// </Updates>
        /// <code>prevMissingEntryList: [tableNm][keyNm][keyVal][keyNm][keyVal]</code>
        /// </summary>
        /// <param name="nextConnInfoComboList">Show the connection information corresponding to the next entry list.</param>
        /// <param name="errorList">The list stores error message.</param>
        /// <param name="allSentEntrySet">A hash set that records all previously sent entries.</param>
        /// <param name="connDbSrc">The data source of all input entries.</param>
        /// <param name="prevConnInfoComboList">A list stores connection information corresponding to previous entry list.</param>
        /// <param name="prevMissingEntryList">Previously sent entry list. Use this entry list to find the next entry list.</param>
        /// <param name="foreignKeyDictionary">A dictionary that maps table name to a list of foreign keys.</param>
        /// <param name="primaryKeyDictionary">A dictionary that maps table name to a list of primary keys.</param>
        /// <param name="fkeyRealNmDictionary">Dictionary format: <code>foreignKeyTableName|foreignKeyColumnName -> list: [ refKeyTableName|refKeyColumnName ] [ refKeyTableName|refKeyColumnName ]</code> .</param>
        /// <param name="tablePairMappingKeyPairList"><code>tablePairMappingKeyPairList: [refKeyTableName|foreignKeyTableName] -> [Rk1|Fk1, Fk2, Fk3][Rk2|Fk4]. Fk1, Fk2, Fk3, Fk4 all belong to the same foreignKeyTableName.</code> </param>
        /// <param name="missingfTableEntryFilepath">The searched entry results will be stored in a temp file. This is only for debugging.</param>
        /// <param name="whitelistDictionary">The white list.</param>
        /// <returns>The next entry list.</returns>
        public static List<string> FindNextEntryList(
            out List<string> nextConnInfoComboList,
            ref List<string> errorList,
            ref HashSet<string> allSentEntrySet,
            DataSrc connDbSrc,
            List<string> prevConnInfoComboList,
            List<string> prevMissingEntryList,
            Dictionary<string, List<string>> foreignKeyDictionary,
            Dictionary<string, List<string>> primaryKeyDictionary,
            Dictionary<string, List<string>> fkeyRealNmDictionary,
            Dictionary<string, List<string>> tablePairMappingKeyPairList,
            string missingfTableEntryFilepath,
            Dictionary<string, WhitelistData> whitelistDictionary)
        {
            List<string> nextFoundPkEntries = new List<string>();  // string s = tableNm|primaryKeyNm|primaryKeyVal|primaryKeyNm|primaryKeyVal|primaryKeyNm|primaryKeyVal
            HashSet<string> visitedQuery = new HashSet<string>();
            nextConnInfoComboList = new List<string>();
            int debug_cnt = 0;
            int preEntryListSize = prevMissingEntryList.Count();
            for (int preInd = 0; preInd < preEntryListSize; preInd++)
            {
                debug_cnt += 1;
                string prevMissingEntry = prevMissingEntryList[preInd];
                string[] preConnInfoTokens = prevConnInfoComboList[preInd].Split('|');
                Guid prevConnInfoBrokerId = new Guid(preConnInfoTokens[0]);
                string prevConnInfoCutomerCode = preConnInfoTokens[1];
                string[] priKeyList = prevMissingEntry.Split('|');
                string tableNm = priKeyList[0], whereStr = string.Empty;
                int length = priKeyList.Length;
                if (!foreignKeyDictionary.ContainsKey(tableNm))
                {
                    continue;
                }
                else if (!whitelistDictionary.ContainsKey(tableNm))
                {
                    whitelistDictionary.Add(tableNm, new WhitelistData());
                    Tools.LogError("The whole table " + tableNm + " need reviewed in the whitelist.");
                    whitelistDictionary[tableNm].ReviewSet = ShareImportExport.GetColumnNamesForTable(tableNm);
                    continue;
                }

                if (!RegularExpressionHelper.IsMatch(RegularExpressionString.DBTableName, tableNm))
                {
                    throw new ApplicationException("Bad table name");
                }

                List<string> fkeyListOfCurTable = foreignKeyDictionary[tableNm];
                StringBuilder where = new StringBuilder();
                List<SqlParameter> paraList = new List<SqlParameter>();
                for (int prkeyInd = 1; prkeyInd < length; prkeyInd += 2)
                {
                    if (priKeyList[prkeyInd + 1] == "NULL")
                    {
                        continue;
                    }

                    bool ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, priKeyList[prkeyInd]);
                    if (!ok)
                    {
                        throw new ApplicationException("Bad column name");
                    }

                    where.Append("T1." + priKeyList[prkeyInd] + " = @" + priKeyList[prkeyInd] + " and ");
                    paraList.Add(new SqlParameter("@" + priKeyList[prkeyInd], priKeyList[prkeyInd + 1]));
                }

                whereStr = where.ToString().Substring(0, where.Length - 5);

                // each fk might point to a different ref table, or point to the same table but different entry
                foreach (string fk in fkeyListOfCurTable)
                {
                    //// some fields are avoided to be export/import so that it will not relate other loan
                    //// if (findNextWaiveSet.Contains(tableNm + "|" + fk))
                    if (whitelistDictionary[tableNm].ReviewSet.Contains(fk) || whitelistDictionary[tableNm].ProtectedDictionary.ContainsKey(fk) || whitelistDictionary[tableNm].NoImportExportSet.Contains(fk))
                    {
                        continue;
                    }

                    bool ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, fk);
                    if (!ok)
                    {
                        throw new ApplicationException("Bad column name");
                    }

                    //// by the foreign key from the tableNm, we can find a list of reference key that it might refer to. For example, brokerId in condition_category is a foreign key that refer to broker id in task_permission_level and also broker
                    List<string> refKeyForeignKeyComboList = fkeyRealNmDictionary[tableNm + "|" + fk];
                    foreach (string refKeyForeignKeyCombo in refKeyForeignKeyComboList)
                    {
                        List<StringBuilder> joinFkList = new List<StringBuilder>();
                        joinFkList.Add(new StringBuilder(string.Empty));
                        string[] refKeyForeignKeyPair = refKeyForeignKeyCombo.Split('|');

                        ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBTableName, refKeyForeignKeyPair[0]);
                        if (!ok)
                        {
                            throw new ApplicationException("Bad table name");
                        }

                        ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBTableName, refKeyForeignKeyPair[1]);
                        if (!ok)
                        {
                            throw new ApplicationException("Bad column name");
                        }

                        List<string> rkfkMappingList = tablePairMappingKeyPairList[refKeyForeignKeyPair[0] + "|" + tableNm];

                        StringBuilder select = new StringBuilder(string.Empty);
                        List<string> selectList = primaryKeyDictionary[refKeyForeignKeyPair[0]];
                        foreach (string selectPk in selectList)
                        {
                            ok = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, selectPk);
                            if (!ok)
                            {
                                throw new ApplicationException("Bad column name");
                            }

                            select.Append("T2." + selectPk + ",");
                        }

                        string joinFk = "T1." + fk + " = T2." + refKeyForeignKeyPair[1] + " and ";
                        //// use the primary key query to generate the entry as results.
                        string priKeyQuery = "SELECT DISTINCT " + select.ToString().TrimEnd(',') + " \nfrom [" +
                            tableNm + "] as T1,[" + refKeyForeignKeyPair[0] + "] as T2 \n" + "WHERE " + joinFk.ToString() + whereStr;

                        if (visitedQuery.Contains(priKeyQuery + prevMissingEntry))
                        {
                            continue;
                        }
                        else
                        {
                            visitedQuery.Add(priKeyQuery + prevMissingEntry);
                        }

                        List<SqlParameter> curParaList = new List<SqlParameter>();
                        foreach (SqlParameter para in paraList)
                        {
                            curParaList.Add(new SqlParameter(para.ToString(), para.Value));
                        }

                        PushQueryResult(ref errorList, ref allSentEntrySet, ref nextFoundPkEntries, connDbSrc, prevConnInfoBrokerId, priKeyQuery, curParaList.ToArray(), refKeyForeignKeyPair[0], select.ToString().TrimEnd(','));

                        int size = nextFoundPkEntries.Count();
                        for (int foundEntryInd = 0; foundEntryInd < size; foundEntryInd++)
                        {
                            nextConnInfoComboList.Add(prevConnInfoBrokerId + "|" + prevConnInfoCutomerCode);
                        }
                    }
                }
            }

            return nextFoundPkEntries;
        }

        /// <summary>
        /// We avoid find related table of certain foreign key to avoid some problems, e.g., exporting multiple loans.
        /// Format of hash set: <code>ForeignTableName|ForeignKeyName</code>
        /// </summary>
        /// <returns>The hash set of table plus columns entry.</returns>
        public static HashSet<string> HardCodeWaiveFindNextTableSet()
        {
            //// some fields like QuickPricerTemplateId, if you want to avoid it exported, you must also disable the column IsQuickPricerEnable

            HashSet<string> waiveSet = new HashSet<string>();
            ////waiveSet.Add("CONSUMER_PORTAL|DefaultLoanTemplateId");
            ////waiveSet.Add("BROKER|QuickPricerTemplateId");
            ////waiveSet.Add("BROKER|PmlLoanTemplateId");
            //// waiveSet.Add("ROLE|RoleDefaultPipelineReportId"); //if waived, this will break the role table.
            return waiveSet;
        }

        /// <summary>
        /// Add protected or not allowed exported fields into the waive set.
        /// </summary>
        /// <param name="waiveSet">The column that will not be considered to be exported.</param>
        /// <param name="whitelistDictionary">The dictionary records all white lists.</param>
        public static void AddProtectFieldOfWhitelistIntoWaiveFindNextSet(ref HashSet<string> waiveSet, Dictionary<string, WhitelistData> whitelistDictionary)
        {
            Dictionary<string, WhitelistData>.KeyCollection tables = whitelistDictionary.Keys;
            foreach (string tableNm in tables)
            {
                Dictionary<string, string>.KeyCollection protectedFields = whitelistDictionary[tableNm].ProtectedDictionary.Keys;
                foreach (string protectedField in protectedFields)
                {
                    if (!waiveSet.Contains(tableNm + "|" + protectedField))
                    {
                        waiveSet.Add(tableNm + "|" + protectedField);
                    }
                }

                HashSet<string> notExportSet = whitelistDictionary[tableNm].NoImportExportSet;
                foreach (string notExportCol in notExportSet)
                {
                    if (!waiveSet.Contains(tableNm + "|" + notExportCol))
                    {
                        waiveSet.Add(tableNm + "|" + notExportCol);
                    }
                }
            }
        }

        /// <summary>
        /// Get the export table sequence by using the tableRankMapping to do hash sort.
        /// This one do not make changes by names but return index of the table list.
        /// </summary>
        /// <param name="tableRankMapping">A dictionary maps table name to rank index.</param>
        /// <param name="curTableList">List of table names.</param>
        /// <returns>The list of index of the sorted table names.</returns>
        public static List<int> GetInsertSeqLevelMapping_EasyUse(Dictionary<string, int> tableRankMapping, List<string> curTableList)
        {
            List<int> orderedListIndex = new List<int>();
            Dictionary<int, List<int>> insertSeqDictionary = new Dictionary<int, List<int>>(); // the seq in the same lvl does not matter
            List<int> noneSeqList = new List<int>(); //// To store the tables that doesn't care about the import sequence.
            orderedListIndex = new List<int>();
            for (int i = 0; i < curTableList.Count; i++)
            {
                string tableNm = curTableList[i];
                if (!tableRankMapping.ContainsKey(tableNm))
                {
                    noneSeqList.Add(i);
                    continue;
                }
                else
                {
                    int rank = tableRankMapping[tableNm];
                    List<int> curLvlSet = null;
                    if (!insertSeqDictionary.ContainsKey(rank))
                    {
                        curLvlSet = new List<int>();
                        insertSeqDictionary.Add(rank, curLvlSet);
                    }
                    else
                    {
                        curLvlSet = insertSeqDictionary[rank];
                    }

                    curLvlSet.Add(i);
                }
            }

            IEnumerable<int> orderedKeys = insertSeqDictionary.Keys.OrderBy(key => key);
            foreach (int key in orderedKeys)
            {
                List<int> curSet = insertSeqDictionary[key];
                orderedListIndex.AddRange(curSet);
            }

            orderedListIndex.AddRange(noneSeqList);
            return orderedListIndex;
        }

        /// <summary>
        /// Split a primary key entry into table name, primary key name list, primary key value list.
        /// Primary key entry format: <code>tableName|primaryKeyName1|KeyValue1|primaryKeyName2|KeyValue2|...</code>.
        /// </summary>
        /// <param name="tableNm">The table name of current entry.</param>
        /// <param name="priKeyNmList">The primary key name list.</param>
        /// <param name="priKeyValList">The primary key value list.</param>
        /// <param name="primaryKeyEntry">The original primary key entry.</param>
        public static void SplitEntryValues(ref string tableNm, ref List<string> priKeyNmList, ref List<string> priKeyValList, string primaryKeyEntry)
        {
            string[] tokens = primaryKeyEntry.Split('|');
            tableNm = tokens[0];
            int size = tokens.Count();
            priKeyNmList = new List<string>();
            priKeyValList = new List<string>();
            for (int i = 1; i < size; i += 2)
            {
                priKeyNmList.Add(tokens[i]);
                priKeyValList.Add(tokens[i + 1]);
            }
        }

        /// <summary>
        /// Find the related table by the provided table list for start.
        /// </summary>
        /// <param name="errorList">Record the list of errors.</param>
        /// <param name="missingConnInfoComboList">Record the connection information corresponding to the missing broker id list.</param>
        /// <param name="missingTableNmList">The names of tables related to the starter tables.</param>
        /// <param name="missingTableEntryList">The entries correspond to the missing table name list.</param>
        /// <param name="allSentFindSet">Store all the found table entries to avoid duplicate entries.</param>
        /// <param name="starterDataSrc">The only data source for all start entries.</param>
        /// <param name="starterConnInfoComboList">A list of broker id and customer id that use to track the data source of the starter tables.</param>
        /// <param name="starterTableNmList">The names of tables to start for searching related tables.</param>
        /// <param name="starterTableEntryList">The entries correspond to the starter name list.</param>
        /// <param name="primaryKeyDictionary">Map table name to a list of primary key.</param>
        /// <param name="foreignKeyDictionary">Map table name to a list of foreign key.</param>
        /// <param name="fkeyRealNmDictionary">Map the combination of table name and foreign key to its reference table and reference key name.</param>
        /// <param name="tableMappingKeyList">Dictionary that stores the relation of reference key columns and the foreign key columns that refer to that reference key column.</param>
        /// <param name="whitelistDictionary">The white list dictionary.</param>
        /// <param name="missingTableEntryFilepath">A file path that record the search result for test purpose.</param>
        /// <param name="needExportRelatedTable">If yes, it will search related tables. If no, it will only return the starter tables.</param>
        public static void FindAllRelatedTableEntry_NoService(
            ref List<string> errorList,
            ref List<string> missingConnInfoComboList,
            ref List<string> missingTableNmList,
            ref List<string> missingTableEntryList,
            ref HashSet<string> allSentFindSet,
            DataSrc starterDataSrc,
            List<string> starterConnInfoComboList,
            List<string> starterTableNmList,
            List<string> starterTableEntryList,
            Dictionary<string, List<string>> primaryKeyDictionary,
            Dictionary<string, List<string>> foreignKeyDictionary,
            Dictionary<string, List<string>> fkeyRealNmDictionary,
            Dictionary<string, List<string>> tableMappingKeyList,
            Dictionary<string, WhitelistData> whitelistDictionary,
            string missingTableEntryFilepath,
            bool needExportRelatedTable)
        {
            if (!needExportRelatedTable)
            {
                missingConnInfoComboList.AddRange(starterConnInfoComboList);
                missingTableNmList.AddRange(starterTableNmList);
                missingTableEntryList.AddRange(starterTableEntryList);
                return;
            }

            ////HashSet<string> allSentFindSet = new HashSet<string>(starterTableEntryList);
            List<string> nextEntryList = starterTableEntryList;
            List<string> nextConnInfoComboList = starterConnInfoComboList;
            for (int searchRoundCntInd = 0; true; searchRoundCntInd++)
            {
                List<string> foundEntryList = new List<string>();
                List<string> foundConnInfoComboList = new List<string>();
                foundEntryList.AddRange(nextEntryList);
                foundConnInfoComboList.AddRange(nextConnInfoComboList);

                //// writer.WriteLine("Round " + i);
                //// ComboList: [brokerId];[tableNm|pkNm1|pkVal1|pkNm2|pkVal2|...]
                foreach (string entry in foundEntryList)
                {
                    int ind = entry.IndexOf('|');
                    string tableNm = entry.Substring(0, ind);
                    missingTableEntryList.Add(entry);
                    missingTableNmList.Add(tableNm);
                }

                missingConnInfoComboList.AddRange(foundConnInfoComboList);

                if (foundEntryList.Any() == false)
                {
                    break;
                }

                //// writer.WriteLine("\n\n\n");
                nextEntryList = FindNextEntryList(
                    out nextConnInfoComboList,
                    ref errorList,
                    ref allSentFindSet,
                    starterDataSrc,
                    foundConnInfoComboList,
                    foundEntryList,
                    foreignKeyDictionary,
                    primaryKeyDictionary,
                    fkeyRealNmDictionary,
                    tableMappingKeyList,
                    missingTableEntryFilepath,
                    whitelistDictionary);
            }
        }

        /// <summary>
        /// This is for broker export. This table is not related to broker id. But broker editor use the table to provide options. So, it is necessary to be exported.
        /// Since it is not related to broker id. It doesn't have to be exported for every broker export. Do it once a while should be ok.
        /// </summary>
        /// <param name="tableNmList">This is an extra need to put the name of each entry into a list so that the later function can use.</param>
        /// <returns>It returns a list of entry that can be directly used as starter table entries.</returns>
        /*public static List<string> GetEntry_DOCUMENT_VENDOR_CONFIGURATION(ref List<string> tableNmList)
        {
            List<string> docuVendorConfigEntryList = new List<string>();
            using (var reader = StoredProcedureHelper.ExecuteReader("DOCUMENT_VENDOR_AvailableVendors"))
            {
                while (reader.Read())
                {
                    docuVendorConfigEntryList.Add("DOCUMENT_VENDOR_CONFIGURATION|VendorId|" + Convert.ToString(reader["VendorId"]).TrimWhitespaceAndBOM());
                    tableNmList.Add("DOCUMENT_VENDOR_CONFIGURATION");
                }
            }

            return docuVendorConfigEntryList;
        }

        /// <summary>
        /// This is for broker export. This table is not related to broker id. But broker editor use the table to provide options. So, it is necessary to be exported.
        /// Since it is not related to broker id. It doesn't have to be exported for every broker export. Do it once a while should be ok.
        /// </summary>
        /// <param name="tableNmList">This is an extra need to put the name of each entry into a list so that the later function can use.</param>
        /// <returns>It returns a list of entry that can be directly used as starter table entries.</returns>
        public static List<string> GetEntry_APPRAISAL_VENDOR_CONFIGURATION(ref List<string> tableNmList)
        {
            List<string> apprVendorConfigEntryList = new List<string>();
            using (var reader = StoredProcedureHelper.ExecuteReader("APPRAISAL_VENDOR_AvailableVendors"))
            {
                while (reader.Read())
                {
                    apprVendorConfigEntryList.Add("APPRAISAL_VENDOR_CONFIGURATION|VendorId|" + Convert.ToString(reader["VendorId"]).TrimWhitespaceAndBOM());
                    tableNmList.Add("APPRAISAL_VENDOR_CONFIGURATION");
                }
            }

            return apprVendorConfigEntryList;
        }*/

        /// <summary>
        /// This is for broker export. This table is not related to broker id. But broker editor use the table to provide options. So, it is necessary to be exported.
        /// Since it is not related to broker id. It doesn't have to be exported for every broker export. Do it once a while should be ok.
        /// </summary>
        /// <param name="tableNmList">This is an extra need to put the name of each entry into a list so that the later function can use.</param>
        /// <returns>It returns a list of entry that can be directly used as starter table entries.</returns>
        /*public static List<string> GetEntry_IRS_4506T_VENDOR_CONFIGURATION(ref List<string> tableNmList)
        {
            List<string> irs4506TVendorConfigEntryList = new List<string>();
            using (var reader = StoredProcedureHelper.ExecuteReader("IRS_4506T_VENDOR_CONFIGURATION_ListActive"))
            {
                while (reader.Read())
                {
                    irs4506TVendorConfigEntryList.Add("IRS_4506T_VENDOR_CONFIGURATION|VendorId|" + Convert.ToString(reader["VendorId"]).TrimWhitespaceAndBOM());
                    tableNmList.Add("IRS_4506T_VENDOR_CONFIGURATION");
                }
            }

            return irs4506TVendorConfigEntryList;
        }*/

        /// <summary>
        /// This is for broker export. This table is not related to broker id. But broker editor use the table to provide options. So, it is necessary to be exported.
        /// Since it is not related to broker id. It doesn't have to be exported for every broker export. Do it once a while should be ok.
        /// </summary>
        /// <param name="tableNmList">This is an extra need to put the name of each entry into a list so that the later function can use.</param>
        /// <returns>It returns a list of entry that can be directly used as starter table entries.</returns>
        /*public static List<string> GetEntry_DATA_RETRIEVAL_PARTNER_BROKER(ref List<string> tableNmList)
        {
            List<string> parterBrokerEntryList = new List<string>();
            using (var reader = StoredProcedureHelper.ExecuteReader("DATA_RETRIEVAL_PARTNER_List"))
            {
                while (reader.Read())
                {
                    string partnerId = Convert.ToString(reader["PartnerId"]).TrimWhitespaceAndBOM();
                    parterBrokerEntryList.Add("DATA_RETRIEVAL_PARTNER_BROKER|PartnerId|" + partnerId);
                    parterBrokerEntryList.Add("DATA_RETRIEVAL_PARTNER|PartnerId|" + partnerId);
                    tableNmList.Add("DATA_RETRIEVAL_PARTNER_BROKER");
                    tableNmList.Add("DATA_RETRIEVAL_PARTNER");
                }
            }

            return parterBrokerEntryList;
        }*/

        /// <summary>
        /// This is for broker export. This table is related to broker id. Broker editor use the table to provide options. 
        /// It is necessary to be exported every time a broker export request is called.
        /// </summary>
        /// <param name="tableNmList">This is an extra need to put the name of each entry into a list so that the later function can use.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>It returns a list of entry that can be directly used as starter table entries.</returns>
        public static List<string> GetEntry_CREDIT_REPORT_ACCOUNT_PROXY(ref List<string> tableNmList, string brokerId)
        {
            List<string> creditReportAccountEntryList = new List<string>();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(new Guid(brokerId), "RetrieveCreditProxyByBrokerID", parameters))
            {
                while (reader.Read())
                {
                    string proxyId = Convert.ToString(reader["CrAccProxyId"]).TrimWhitespaceAndBOM();
                    creditReportAccountEntryList.Add("CREDIT_REPORT_ACCOUNT_PROXY|CrAccProxyId|" + proxyId);
                    tableNmList.Add("CREDIT_REPORT_ACCOUNT_PROXY");
                }
            }

            return creditReportAccountEntryList;
        }

        /// <summary>
        /// This is for broker export. This function calls all the broker id related function above to gather the broker id related entries. 
        /// It is necessary to be exported every time a broker export request is called.
        /// </summary>
        /// <param name="starterTableEntryList">This is an extra list to store the result entry list for later usage.</param>
        /// <param name="starterTableNmList">This is an extra list to store the table name list for later usage.</param>
        /// <param name="brokerId">Provide the current broker id.</param>
        public static void StarterTableList_AddOtherTables_BrokerIdRelated(ref List<string> starterTableEntryList, ref List<string> starterTableNmList, string brokerId)
        {
            ////starterTableEntryList.AddRange(TableReplicateTool.GetEntry_CREDIT_REPORT_ACCOUNT_PROXY(ref starterTableNmList, brokerId));
            AddStarterTableListByBrokerId_AllBrokerRelatedTables(ref starterTableEntryList, ref starterTableNmList, brokerId);
            var list = MasterCRAList.RetrieveAvailableCras(new Guid(brokerId));
            foreach (CRA item in list)
            {
                starterTableEntryList.Add("SERVICE_COMPANY|ComId|" + item.ID.ToString());
                starterTableNmList.Add("SERVICE_COMPANY");
            }
        }

        /// <summary>
        /// This function gather the above broker id non-related table entries.
        /// This function can be called once a while when you feel the original tables might be updated.
        /// </summary>
        /// <param name="starterTableEntryList">This is an extra list to store the result entry list for later usage.</param>
        /// <param name="starterTableNmList">This is an extra list to store the table name list for later usage.</param>
        /*public static void StarterTableList_AddOtherTables_BrokerIdUnrelated(ref List<string> starterTableEntryList, ref List<string> starterTableNmList)
        {
            starterTableEntryList.AddRange(TableReplicateTool.GetEntry_DOCUMENT_VENDOR_CONFIGURATION(ref starterTableNmList));
            starterTableEntryList.AddRange(TableReplicateTool.GetEntry_APPRAISAL_VENDOR_CONFIGURATION(ref starterTableNmList));
            starterTableEntryList.AddRange(TableReplicateTool.GetEntry_DATA_RETRIEVAL_PARTNER_BROKER(ref starterTableNmList));
            starterTableEntryList.AddRange(TableReplicateTool.GetEntry_IRS_4506T_VENDOR_CONFIGURATION(ref starterTableNmList));
        }*/

        /// <summary>
        /// This function get all entries of license related to broker id. This is later added. But this is probably more important than the above two functions.
        /// Need to be called during each broker export.
        /// </summary>
        /// <param name="starterTableEntryList">This is an extra list to store the result entry list for later usage.</param>
        /// <param name="starterTableNmList">This is an extra list to store the table name list for later usage.</param>
        /// <param name="brokerId">Provide the current broker id.</param>
        public static void AddStarterTableListByBrokerid_LICENSE(ref List<string> starterTableEntryList, ref List<string> starterTableNmList, string brokerId)
        {
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(new Guid(brokerId), "ListAllLicense", parameters))
            {
                while (reader.Read())
                {
                    starterTableEntryList.Add("LICENSE|LicenseId|" + Convert.ToString(reader["LicenseId"]).TrimWhitespaceAndBOM() + "|LicenseOwnerBrokerId|" + Convert.ToString(reader["LicenseOwnerBrokerId"]).TrimWhitespaceAndBOM());
                    starterTableNmList.Add("LICENSE");
                }
            }
        }

        /// <summary>
        /// Add broker related table entries into the starter table list.
        /// </summary>
        /// <param name="starterTableEntryList">The table entry list to start with the search of related tables.</param>
        /// <param name="starterTableNmList">The table name list correspond to the entry list.</param>
        /// <param name="brokerId">The broker id.</param>
        public static void AddStarterTableListByBrokerId_AllBrokerRelatedTables(ref List<string> starterTableEntryList, ref List<string> starterTableNmList, string brokerId)
        {
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(new Guid(brokerId), "Justin_Testing_GetAllBrokerRelatedTablesByBrokerId", parameters))
            {
                bool hasNextQuery = true;
                while (hasNextQuery)
                {
                    while (reader.Read())
                    {
                        string tableNm = Convert.ToString(reader[0]).TrimWhitespaceAndBOM();
                        int numOfCol = reader.FieldCount;
                        string entry = tableNm;
                        for (int i = 1; i < numOfCol; i++)
                        {
                            string colNm = reader.GetName(i);
                            entry += "|" + colNm + "|" + Convert.ToString(reader[colNm]).TrimWhitespaceAndBOM();
                        }

                        starterTableEntryList.Add(entry);
                        starterTableNmList.Add(tableNm);
                    }

                    hasNextQuery = reader.NextResult();
                }
            }
        }

        /// <summary>
        /// Add user related table entries into the starter table list.
        /// </summary>
        /// <param name="starterTableEntryList">The table entry list to start with the search of related tables.</param>
        /// <param name="starterTableNmList">The table name list correspond to the entry list.</param>
        /// <param name="empUserIdComboList">A list of combination of employee id and user id.</param>
        /// <param name="connInfoBrokerId">A broker id use to track the data source of this starter table.</param>
        public static void AddStarterTableListByEmpUserIdComboList_AllUserRelatedTables(ref List<string> starterTableEntryList, ref List<string> starterTableNmList, List<string> empUserIdComboList, Guid connInfoBrokerId)
        {
            HashSet<string> visitedEntrySet = new HashSet<string>();
            foreach (string empUserIdCombo in empUserIdComboList)
            {
                string[] tokens = empUserIdCombo.Split('|');
                object empObj = (tokens[0] == string.Empty) ? (object)DBNull.Value : tokens[0];
                object userObj = (tokens[1] == string.Empty) ? (object)DBNull.Value : tokens[1];
                ////SqlParameter empPara = new SqlParameter("@EmployeeId", empObj);
                ////SqlParameter userPara = new SqlParameter("@UserId", userObj);
                SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@EmployeeId", empObj), new SqlParameter("@UserId", userObj) };
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfoBrokerId, "Justin_Testing_GetAllUserRelatedTablesByEmployeeIdAndUserId", parameters))
                {
                    bool hasNextQuery = true;
                    while (hasNextQuery)
                    {
                        while (reader.Read())
                        {
                            string tableNm = Convert.ToString(reader[0]).TrimWhitespaceAndBOM();
                            int numOfCol = reader.FieldCount;
                            string entry = tableNm;
                            for (int i = 1; i < numOfCol; i++)
                            {
                                string colNm = reader.GetName(i);
                                entry += "|" + colNm + "|" + Convert.ToString(reader[colNm]).TrimWhitespaceAndBOM();
                            }

                            if (!visitedEntrySet.Contains(entry))
                            {
                                visitedEntrySet.Add(entry);
                                starterTableEntryList.Add(entry);
                                starterTableNmList.Add(tableNm);
                            }
                        }

                        hasNextQuery = reader.NextResult();
                    }
                }
            }
        }

        /// <summary>
        /// Export one xml table by reading database.
        /// </summary>
        /// <param name="errorList">List that stores error messages.</param>
        /// <param name="whitelistDictionary">The dictionary of white list data objects.</param>
        /// <param name="needUpdateWhitelist">Check if need updating white list. For now, it needs to update only when there is newly found column.</param>
        /// <param name="isGroupEnd">A boolean flag to check if this entry is the last one in the group, that is one sql transaction.</param>
        /// <param name="exportXmlWriter">The writer to write xml into the file for export.</param>
        /// <param name="reader">A reader that read the table data from database.</param>
        /// <param name="connSrc">Data source of this table.</param>
        /// <param name="connInfoBrokerId">Connection information for this table, only if this table is from main database.</param>
        /// <param name="tableName">The current table name.</param>
        /// <param name="tableMap">A dictionary that maps columns to column information.</param>
        /// <param name="primaryKeyNameList">Primary key name list.</param>
        /// <param name="primaryKeyValueList">Primary key value list.</param>
        /// <param name="foreignKeyRealNmDictionary">Records foreign key and its reference key names.<code>foreignKeyTableName|foreignKeyColumnName -> list: [ refKeyTableName|refKeyColumnName ] [ refKeyTableName|refKeyColumnName ]</code>.</param>
        /// <param name="defaultForeignKeyValDictionary">A dictionary that records foreign key default value.<code>tableNm|Fk -> defaultValue</code></param>
        /// <param name="needDebugInfo">If true, it will show the log information in the Paul Bunyan message viewer. It has many message. </param>
        public static void ExportOneXmlTableBySp(
            ref List<string> errorList,
            ref Dictionary<string, WhitelistData> whitelistDictionary,
            ref bool needUpdateWhitelist,
            ref bool isGroupEnd,
            XmlWriter exportXmlWriter,
            DbDataReader reader,
            DataSrc connSrc,
            Guid connInfoBrokerId,
            string tableName,
            Dictionary<string, ColumnInfo> tableMap,
            List<string> primaryKeyNameList,
            List<string> primaryKeyValueList,
            Dictionary<string, List<string>> foreignKeyRealNmDictionary,
            ////Dictionary<string, string> identityDictionary,
            ////HashSet<string> findNextWaiveSet,
            Dictionary<string, string> defaultForeignKeyValDictionary,
            bool needDebugInfo)
        {
            string stageSiteSourceName = ConstAppDavid.CurrentServerLocation.ToString();
            Dictionary<string, ColumnInfo>.KeyCollection columnNames = tableMap.Keys;
            HashSet<string> notNullColList = new HashSet<string>();
            HashSet<string> nullableColList = new HashSet<string>(); // for non-fk list
            HashSet<string> notNullKeyList = new HashSet<string>(); // for pk and fk
            HashSet<string> nullableKeyList = new HashSet<string>(); // for fk
            WhitelistData whitelist = whitelistDictionary[tableName];
            string[] passwordHashAndSalt = new string[2];

            foreach (string colNm in columnNames)
            {
                bool isPk = primaryKeyNameList.Contains(colNm);
                ColumnInfo colInfo = tableMap[colNm];
                if (isPk)
                {
                    if (!notNullKeyList.Contains(colNm))
                    {
                        notNullKeyList.Add(colNm);
                    }
                    else
                    {
                        Tools.LogError("error duplicate pk in notNullKeyList: " + colNm);
                    }
                }
                else if (colInfo.IsForeignKey == false && colInfo.Nullable == false)
                {
                    if (!notNullColList.Contains(colNm))
                    {
                        notNullColList.Add(colNm);
                    }
                    else
                    {
                        Tools.LogError("error duplicate in notNullColSet: " + colNm);
                    }
                }
                else if (colInfo.IsForeignKey == false && colInfo.Nullable == true)
                {
                    if (!nullableColList.Contains(colNm))
                    {
                        nullableColList.Add(colNm);
                    }
                    else
                    {
                        Tools.LogError("error duplicate in nullableColSet: " + colNm);
                    }
                }
                else if (colInfo.IsForeignKey == true && colInfo.Nullable == false)
                {
                    if (!notNullKeyList.Contains(colNm))
                    {
                        notNullKeyList.Add(colNm);
                    }
                    else
                    {
                        Tools.LogError("error duplicate fk in notNullKeyList: " + colNm);
                    }
                }
                else if (colInfo.IsForeignKey == true && colInfo.Nullable == true)
                {
                    if (!nullableKeyList.Contains(colNm))
                    {
                        nullableKeyList.Add(colNm);
                    }
                    else
                    {
                        Tools.LogError("error duplicate in nullableKeyList: " + colNm);
                    }
                }
            }

            int i;
            /*StringBuilder sql = new StringBuilder("SELECT * FROM [" + tableName + "] WHERE ");
            List<SqlParameter> paraList = new List<SqlParameter>();
            for (i = 0; i < priKeyNmList.Count; i++)
            {
                sql.Append(priKeyNmList[i] + " = @" + priKeyNmList[i] + " and ");
                paraList.Add(new SqlParameter("@" + priKeyNmList[i], priKeyValList[i]));
            }*/

            //// using (var reader = DataAccess.DbAccessUtils.ExecuteReaderDangerously(sql.ToString().Substring(0, sql.Length - 5), paraList.ToArray()))
            {
                //// while (reader.Read())
                {
                    isGroupEnd = (Convert.ToString(reader["IsEndOfGroup"]).TrimWhitespaceAndBOM() == "1") ? true : false;
                    //// Dictionary<string, string> skipColValMap = new Dictionary<string, string>();

                    exportXmlWriter.WriteStartElement("table");
                    exportXmlWriter.WriteAttributeString("n", tableName);

                    for (i = 0; i < primaryKeyNameList.Count; i++)
                    {
                        exportXmlWriter.WriteAttributeString("pk_" + i, primaryKeyNameList[i]);
                        exportXmlWriter.WriteAttributeString("val_" + i, primaryKeyValueList[i]);
                    }

                    // LO_FORM table has a special constraint on brokerid. It can be fixed through stored procedure. Do it after most parts are stable.
                    if (tableName == "LO_FORM" && (bool)reader["IsStandardForm"] == false)
                    {
                        notNullKeyList.Add("BrokerId");
                        nullableKeyList.Remove("BrokerId");
                    }

                    HashSet<string> visitedColSet = new HashSet<string>();
                    int size = reader.FieldCount;
                    for (int ind = 0; ind < size; ind++)
                    {
                        string columnName = reader.GetName(ind);
                        if (needDebugInfo)
                        {
                            Tools.LogInfo("JustinDebug[" + (ShareImportExport.DebugCount++) + "]: Try to export table[" + tableName + "] and column[" + columnName + "]");
                        }

                        if (whitelist.NoImportExportSet.Contains(columnName) || columnName == "TableName" || columnName == "IsEndOfGroup")
                        {
                            continue;
                        }

                        if (visitedColSet.Contains(columnName))
                        {
                            continue;
                        }
                        else
                        {
                            visitedColSet.Add(columnName);
                        }

                        bool isProtected = whitelist.ProtectedDictionary.ContainsKey(columnName);
                        string originValue = Convert.ToString(reader[ind]).TrimWhitespaceAndBOM();

                        if (!tableMap.ContainsKey(columnName))
                        {
                            //// errorList.Add("Table column info mapping: table [" + tableName + "] missing column [" + columnName + "].");
                            //// Tools.LogError("Table column info mapping: table [" + tableName + "] missing column [" + columnName + "].");
                            continue;
                        }

                        // Check if referenced table is reviewed in whitelist
                        if (tableMap[columnName].IsForeignKey && isProtected == false && whitelist.ExportSet.Contains(columnName) && originValue != null && originValue != string.Empty)
                        {
                            List<string> refComboList = foreignKeyRealNmDictionary[tableName + "|" + columnName];
                            foreach (string refCombo in refComboList)
                            {
                                string[] tokens = refCombo.Split('|');
                                string refTable = tokens[0];

                                if (!whitelistDictionary.ContainsKey(refTable))
                                {
                                    whitelistDictionary.Add(refTable, new WhitelistData());
                                    whitelistDictionary[refTable].MissingSet = ShareImportExport.GetColumnNamesForTable(refTable);
                                }

                                WhitelistData refWhitelist = whitelistDictionary[refTable];
                                if (refWhitelist.IsAnyColumnReviewed_NonRealTime == false)
                                {
                                    bool hasAny = refWhitelist.ExportSet.Any() || refWhitelist.NoImportExportSet.Any() || refWhitelist.NoUpdateSet.Any() || refWhitelist.ProtectedDictionary.Any() || refWhitelist.UpdateProtectDictionary.Any();
                                    refWhitelist.IsAnyColumnReviewed_NonRealTime = hasAny;
                                }

                                if (refWhitelist.IsAnyColumnReviewed_NonRealTime == false)
                                {
                                    var msg = $"The table {tableName} has column {columnName} that refers (FKs) to a not-yet-reviewed table {refTable}.  Please review {refTable} in the whitelist.";
                                    errorList.Add(msg);
                                    Tools.LogError(msg);
                                }
                            }
                        }

                        // probably has primary key
                        if (notNullKeyList.Contains(columnName))
                        {
                            bool isErrorFk = false;
                            if (tableMap[columnName].IsForeignKey && isProtected == false && whitelist.ExportSet.Contains(columnName) && originValue != null && originValue != string.Empty)
                            {
                                bool hasDefaultFkVal = defaultForeignKeyValDictionary.ContainsKey(tableName + "|" + columnName);
                                string foreignKeyDefaultValue = hasDefaultFkVal ? defaultForeignKeyValDictionary[tableName + "|" + columnName] : null;
                                if (whitelist.ProtectedDictionary.ContainsKey(columnName) == false && (hasDefaultFkVal == false || (hasDefaultFkVal && originValue != foreignKeyDefaultValue)))
                                {
                                    bool isExist = CheckIfReferenceKeyListExists(connSrc, connInfoBrokerId, foreignKeyRealNmDictionary[tableName + "|" + columnName], originValue);
                                    if (!isExist)
                                    {
                                        errorList.Add("Foreign key " + columnName + "[" + originValue + "] in table " + tableName + " point to a non-exist table. ");
                                        Tools.LogError("Foreign key " + columnName + "[" + originValue + "] in table " + tableName + " point to a non-exist table.");
                                        continue;
                                    }
                                }
                            }

                            ExportOneColumn(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, exportXmlWriter, tableName, tableMap, "tag_noNullKey", reader, ind, isErrorFk, originValue, passwordHashAndSalt, needDebugInfo);
                        }
                        else if (nullableKeyList.Contains(columnName))
                        {
                            //// The checking is tricky. Right now, it is only for identity key whose value is -1 and is kind of hardcode.
                            bool isErrorFk = false;
                            if (tableMap[columnName].IsForeignKey && isProtected == false && originValue != string.Empty && originValue != null)
                            {
                                bool hasDefaultFkVal = defaultForeignKeyValDictionary.ContainsKey(tableName + "|" + columnName);
                                string foreignKeyDefaultValue = hasDefaultFkVal ? defaultForeignKeyValDictionary[tableName + "|" + columnName] : null;
                                if (whitelist.ProtectedDictionary.ContainsKey(columnName) == false && (hasDefaultFkVal == false || (hasDefaultFkVal && originValue != foreignKeyDefaultValue)))
                                {
                                    bool isExist = CheckIfReferenceKeyListExists(connSrc, connInfoBrokerId, foreignKeyRealNmDictionary[tableName + "|" + columnName], originValue);
                                    if (!isExist)
                                    {
                                        errorList.Add("Foreign key " + columnName + "[" + originValue + "] in table " + tableName + " point to a non-exist table. ");
                                        Tools.LogError("Foreign key " + columnName + "[" + originValue + "] in table " + tableName + " point to a non-exist table.");
                                        continue;
                                    }
                                }
                            }

                            // bool isWaived = findNextWaiveSet.Contains(tableName + "|" + columnName) ? true : false;
                            ExportOneColumn(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, exportXmlWriter, tableName, tableMap, "tag_nullableKey", reader, ind, isErrorFk, originValue, passwordHashAndSalt, needDebugInfo);
                        }
                        else if (notNullColList.Contains(columnName))
                        {
                            /*if (skipColValMap.ContainsKey(columnName))
                            {
                                ExportOneColumnByDesignatedValue(ref errorList, ref whitelist, exportXmlWriter, tableName, tableMap, "tag_noNullCol", columnName, skipColValMap[columnName], false, false, skipColValMap[columnName]);
                            }
                            else*/
                            {
                                ExportOneColumn(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, exportXmlWriter, tableName, tableMap, "tag_noNullCol", reader, ind, false, originValue, passwordHashAndSalt, needDebugInfo);
                            }
                        }
                        else if (nullableColList.Contains(columnName))
                        {
                            /*if (skipColValMap.ContainsKey(columnName))
                            {
                                ExportOneColumnByDesignatedValue(ref errorList, ref whitelist, exportXmlWriter, tableName, tableMap, "tag_nullableCol", columnName, skipColValMap[columnName], false, false, skipColValMap[columnName]);
                            }
                            else*/
                            {
                                ExportOneColumn(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, exportXmlWriter, tableName, tableMap, "tag_nullableCol", reader, ind, false, originValue, passwordHashAndSalt, needDebugInfo);
                            }
                        }
                    }

                    exportXmlWriter.WriteEndElement();
                }
            }
        }

        /// <summary>
        /// Export all starter tables and tables that related to the starter tables By stored procedure.
        /// </summary>
        /// <param name="errorList">List stores errors.</param>
        /// <param name="warningList">List stores warnings.</param>
        /// <param name="fileDBKeysForDirectTransfer">The FileDB entries that should be transferred directly outsied the xml file.</param>
        /// <param name="whitelistPath">The physics address where white list is stored.</param>
        /// <param name="mainDirectory">A directory for storing the file of missing entry, which is for testing purpose.</param>
        /// <param name="brokerId">The id of broker.</param>
        /// <param name="includeFiledb">Decide whether to include file database.</param>
        /// <param name="userOption">The option for exporting user. 0: Export no user. 1: Export the users belonged to one designated branch by branch name. 
        /// 3: Export all users of this broker. 4: Export system tables. 5. Export pricing tables in main database. 6. Export pricing tables in rate sheet database.
        /// </param>
        /// <param name="branchName">In use only if user want to export only that branch's employee accounts.</param>
        /// <param name="justinOneSqlQueryTimeOutInSeconds">Time out for query.</param>
        /// <param name="needDebugInfo">Determine whether to show the debug information or not.</param>
        /// <returns>The address of the exported xml file.</returns>
        public static string ExportAllBrokerRelatedTablesBySp(
            ref List<string> errorList,
            ref List<string> warningList,
            out IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer,
            string whitelistPath,
            string mainDirectory,
            ////bool isSystemTableExport,
            string brokerId,
            bool includeFiledb,
            string userOption,
            string branchName,
            int? justinOneSqlQueryTimeOutInSeconds,
            bool needDebugInfo)
        {
            var fileDBKeysForDirectTransferSet = new HashSet<FileDBEntryInfoForExport>();
            fileDBKeysForDirectTransfer = fileDBKeysForDirectTransferSet;
            ShareImportExport.DebugCount = 0;
            Dictionary<string, WhitelistData> whitelistDictionary = null;
            Dictionary<string, string> reconstructMap = null;
            ShareImportExport.ReadCsvWhiteList(ref errorList, out reconstructMap, out whitelistDictionary, whitelistPath, false);
            if (errorList.Any())
            {
                return null;
            }

            var loanIDsForFullExport = new HashSet<Guid>();
            var additionalFileDBEntries = new HashSet<FileDBEntryInfoForExport>();

            List<string> nonNullInsertList = new List<string>(); // record the tables that are to be inserted first since their fk are non-nullable
            List<string> nullableInsertList = new List<string>(); // record tables that can be inserted later since their foreign keys are nullable.

            string databaseSourceName = null;
            DataSrc connSrc = (userOption == "RATE_SHEET") ? DataSrc.RateSheet : DataSrc.LOShare;
            if(userOption != "RATE_SHEET")
            {
                var brokerGuid = new Guid(brokerId);
                var connectionInfo = DbConnectionInfo.GetConnectionInfo(brokerGuid);
                databaseSourceName = connectionInfo.InitialCatalog;
            }

            List<List<string>> fkeyRelationshipTable = TableReplicateTool.GetFkRelationshipTable(connSrc);
            Dictionary<string, List<string>> primaryKeyDictionary = ShareImportExport.GetPrimaryKeyDictionary(connSrc);
            Dictionary<string, string> defaultForeignKeyValDictionary = ShareImportExport.GetDefaultForeignKeyValDictionary();
            Dictionary<string, List<string>> foreignKeyDictionary = TableReplicateTool.GetForeignKeyDictionary(fkeyRelationshipTable);
            Dictionary<string, List<string>> fkeyRealNmDictionary = TableReplicateTool.GetFkRealNmDictionary(fkeyRelationshipTable);
            ////Dictionary<string, List<string>> tableMappingKeyList = TableReplicateTool.GetTablePairMappingKeyPairList(fkeyRelationshipTable);
            ////Dictionary<string, string> identityDictionary = ShareImportExport.HardcodeIdentityTableKeyDictionary();
            ////HashSet<string> findNextWaiveSet = TableReplicateTool.HardCodeWaiveFindNextTableSet();
            ////TableReplicateTool.AddProtectFieldOfWhitelistIntoWaiveFindNextSet(ref findNextWaiveSet, whitelistDictionary);
            Dictionary<string, List<List<string>>> tableUniqueIdxColDictionary = ShareImportExport.GetUniqueIdxColDictionary(connSrc);
            ////Dictionary<string, List<string>> complexChkConstraintColTableSet = ShareImportExport.GetCheckConstraintColTableComboSet_QuickCheck();
            Dictionary<string, Dictionary<string, ColumnInfo>> allTableColInfoMap = new Dictionary<string, Dictionary<string, ColumnInfo>>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, List<string>> specChkConstraintColTableComboSet = ShareImportExport.GetCheckConstraintColTableComboSet_QuickCheck(connSrc);

            string stageSiteSourceName = ConstAppDavid.CurrentServerLocation.ToString();
            string customerCode = null;
            Guid connInfoBrokerId = Guid.Empty;
            if (userOption != "RATE_SHEET")
            {
                if (needDebugInfo)
                {
                    Tools.LogInfo("JustinJ Export: Collect connection info of broker id.");
                }

                connInfoBrokerId = new Guid(brokerId);
                SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BrokerId", connInfoBrokerId) };
                using (var reader = StoredProcedureHelper.ExecuteReader(connInfoBrokerId, "BROKER_RetrieveLiteInfo", parameters))
                {
                    if (reader.Read())
                    {
                        customerCode = Convert.ToString(reader["CustomerCode"]);
                    }
                }
            }

            string connInfoCombo = connInfoBrokerId + "|" + customerCode;

            int debug_int = 0;
            bool needReview = false;
            bool needUpdateWhitelist = false;
            bool hasNotNullForeignKeyToReview = false;
            HashSet<string> reviewTableSet = new HashSet<string>();
            StringBuilder reviewNotNullColMsg = new StringBuilder("The following columns must be reviewed from whitelist because they might be not-null-foreign-key, unique index or has special value range.");

            //// get columninfo (reason: avoid do sql query inside sql query below.)
            HashSet<string> allExportedTableNmSet = new HashSet<string>();
            int isByTableName = 0;

            SqlParameter[] paras = new SqlParameter[]
            {
                new SqlParameter("@UserOption", userOption),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@BranchName", branchName),
                new SqlParameter("@IsByTableName", isByTableName),
                new SqlParameter("@InputTableName", string.Empty)
            };

            if (needDebugInfo)
            {
                Tools.LogInfo("JustinJ Export: Collect all to-be-exported table names.");
            }

            using (var reader = (userOption == "RATE_SHEET")
                ? StoredProcedureHelper.ExecuteReader(
                        DataSrc.RateSheet,
                        "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                        justinOneSqlQueryTimeOutInSeconds,
                        paras)
                : (userOption == "SYS_TABLES")
                        ? StoredProcedureHelper.ExecuteReader(
                                connInfoBrokerId,
                                "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                                paras)
                       : StoredProcedureHelper.ExecuteReader(
                                connInfoBrokerId,
                                "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                                justinOneSqlQueryTimeOutInSeconds,
                                paras))
            {
                bool hasNextQuery = true;
                while (hasNextQuery)
                {
                    if (reader.Read())
                    {
                        string tableNm = Convert.ToString(reader["TableName"]).TrimWhitespaceAndBOM();
                        if (tableNm != "GROUP_END" && allExportedTableNmSet.Contains(tableNm) == false)
                        {
                            allExportedTableNmSet.Add(tableNm);
                        }

                        if (needDebugInfo)
                        {
                            Tools.LogInfo("JustinJ Export: Get tableNm[" + tableNm + "]");
                        }
                    }

                    hasNextQuery = reader.NextResult();
                }
            }

            if (needDebugInfo)
            {
                Tools.LogInfo("JustinJ Export: Collect tables' column info.");
            }

            foreach (string tableNm in allExportedTableNmSet)
            {
                if (!allTableColInfoMap.ContainsKey(tableNm))
                {
                    allTableColInfoMap.Add(tableNm, ShareImportExport.GetColumnInfoByColumnNameMap(connSrc, tableNm));
                }

                if (!whitelistDictionary.ContainsKey(tableNm))
                {
                    whitelistDictionary.Add(tableNm, new WhitelistData());
                    whitelistDictionary[tableNm].MissingSet = ShareImportExport.GetColumnNamesForTable(tableNm);
                }
            }

            string uniqueFileName = TempFileUtils.NewTempFilePath() + ".xml";
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "    ";

            if (needDebugInfo)
            {
                Tools.LogInfo("JustinJ Export: begin export.");
            }

            int debug_groupInd = 0;
            using (XmlWriter exportXmlWriter = XmlWriter.Create(uniqueFileName, settings))
            {
                exportXmlWriter.WriteStartElement("Export");
                exportXmlWriter.WriteAttributeString("stageSiteSourceName", stageSiteSourceName);
                exportXmlWriter.WriteAttributeString("databaseSourceName", databaseSourceName);
                exportXmlWriter.WriteAttributeString("dbConnSrc", (userOption == "RATE_SHEET") ? "RATE_SHEET" : "MAIN_DB");
                exportXmlWriter.WriteStartElement("Group");
                exportXmlWriter.WriteAttributeString("connBrokerIdCustomCodeCombo", connInfoCombo);

                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@UserOption", userOption),
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@BranchName", branchName),
                    new SqlParameter("@IsByTableName", "0"),
                    new SqlParameter("@InputTableName", string.Empty)
                };

                using (var reader = (userOption == "RATE_SHEET")
                    ? StoredProcedureHelper.ExecuteReader(
                            DataSrc.RateSheet,
                            "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                            justinOneSqlQueryTimeOutInSeconds,
                            parameters)
                    : (userOption == "SYS_TABLES")
                           ? StoredProcedureHelper.ExecuteReader(
                                    connInfoBrokerId,
                                    "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                                    parameters)
                          : StoredProcedureHelper.ExecuteReader(
                                    connInfoBrokerId,
                                    "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                                    justinOneSqlQueryTimeOutInSeconds,
                                    parameters))
                {
                    HashSet<string> visitedEntrySet = new HashSet<string>();
                    bool hasNextQuery = true;
                    bool isGroupEnd = false;
                    bool isEmptyGroup = true;
                    while (hasNextQuery)
                    {
                        debug_int++;
                        string tableNm = string.Empty;
                        int rowCnt = 0;
                        while (reader.Read())
                        {
                            isEmptyGroup = false;
                            tableNm = Convert.ToString(reader["TableName"]).TrimWhitespaceAndBOM();
                            int rowsPerGroup = Convert.ToInt32(reader["IsEndOfGroup"]) - 1;
                            string entry = tableNm;
                            if (tableNm == "GROUP_END")
                            {
                                debug_groupInd += 1;
                                isGroupEnd = true;
                                break;
                            }

                            List<string> priKeyNmList = primaryKeyDictionary[tableNm];
                            List<string> priKeyValList = new List<string>();
                            foreach (string prikeyNm in priKeyNmList)
                            {
                                string priKeyVal = Convert.ToString(reader[prikeyNm]).TrimWhitespaceAndBOM();
                                priKeyValList.Add(priKeyVal);
                                entry += "|" + prikeyNm + "|" + priKeyVal;
                            }

                            entry += "|" + reader.FieldCount;

                            if (needDebugInfo)
                            {
                                Tools.LogInfo("JustinDebug[" + (ShareImportExport.DebugCount++) + "]: Export Group[" + debug_groupInd + "] table entry: " + entry);
                            }

                            if (visitedEntrySet.Contains(entry))
                            {
                                continue;
                            }
                            else
                            {
                                visitedEntrySet.Add(entry);
                            }

                            WhitelistData curTableWhitelist = null;
                            if (whitelistDictionary.ContainsKey(tableNm))
                            {
                                curTableWhitelist = whitelistDictionary[tableNm];
                            }

                            Dictionary<string, ColumnInfo> curTableMap = allTableColInfoMap[tableNm];
                            if (whitelistDictionary.ContainsKey(tableNm) == false || (!(curTableWhitelist.ExportSet.Any() || curTableWhitelist.NoImportExportSet.Any() || curTableWhitelist.NoUpdateSet.Any() || curTableWhitelist.ProtectedDictionary.Any() || curTableWhitelist.UpdateProtectDictionary.Any())))
                            {
                                Tools.LogError("The whole table " + tableNm + " need reviewed in the whitelist.");
                                needReview = true;
                                if (!reviewTableSet.Contains(tableNm))
                                {
                                    reviewTableSet.Add(tableNm);
                                }

                                break;
                            }

                            if (tableNm == "LOAN_FILE_B")
                            {
                                var curLoanIdString = reader["slid"].ToString();
                                var curLoanId = new Guid(curLoanIdString);
                                var curClosingCostXml = reader["sClosingCostArchiveXmlContent"].ToString();
                                var closingCostArchives = new List<ClosingCostArchive>();
                                loanIDsForFullExport.Add(curLoanId);

                                if (!string.IsNullOrEmpty(curClosingCostXml))
                                {
                                    closingCostArchives = (List<ClosingCostArchive>)SerializationHelper.XmlDeserialize(curClosingCostXml, typeof(List<ClosingCostArchive>));
                                }

                                var closingCostArchiveCount = closingCostArchives.Count;
                                foreach (var archive in closingCostArchives)
                                {
                                    var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                        entry: new FileDBEntryInfo(E_FileDB.Normal, archive.FileDBKey),
                                        keyType: E_FileDBKeyType.Normal_ClosingCostArchive
                                    );
                                    
                                    additionalFileDBEntries.Add(fileDBEntryForExport);
                                }

                                var sLoanDataMigrationAuditHistorySubKey = (Guid?)reader["sLoanDataMigrationAuditHistoryKey"];
                                if (sLoanDataMigrationAuditHistorySubKey.HasValue && sLoanDataMigrationAuditHistorySubKey.Value != Guid.Empty)
                                {
                                    var key = Migration.LoanDataMigrationResult.GetFileDbKey(curLoanId, sLoanDataMigrationAuditHistorySubKey.Value);
                                    var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                       entry: new FileDBEntryInfo(E_FileDB.Normal, key),
                                       keyType: E_FileDBKeyType.Normal_Loan_MigrationAuditHistoryKey
                                    );

                                    additionalFileDBEntries.Add(fileDBEntryForExport);
                                }
                            }
                            else if (tableNm == "FEE_SERVICE_REVISION")
                            {
                                var curBrokerID = reader["BrokerID"].ToString();
                                var fileDBKey = reader["FileDBKey"].ToString();

                                if (!string.IsNullOrEmpty(fileDBKey))
                                {
                                    var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                        entry: new FileDBEntryInfo(E_FileDB.Normal, fileDBKey),
                                        keyType: E_FileDBKeyType.Normal_FeeServiceRevision_File
                                    );                                  

                                    fileDBKeysForDirectTransferSet.Add(fileDBEntryForExport);
                                }
                            }
                            else if (tableNm == "LPE_PRICE_GROUP")
                            {
                                var curBrokerID = reader["BrokerID"].ToString();
                                var fileDBKey = reader["ContentKey"].ToString();

                                if (!string.IsNullOrEmpty(fileDBKey))
                                {
                                    var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                        entry: new FileDBEntryInfo(E_FileDB.Normal, fileDBKey),
                                        keyType: E_FileDBKeyType.Normal_LpePriceGroup_Content
                                    );

                                    fileDBKeysForDirectTransferSet.Add(fileDBEntryForExport);
                                }
                            }
                            else if (tableNm == "LO_FORM")
                            {
                                var curBrokerID = reader["BrokerID"].ToString();
                                var fileDBKey = reader["FormId"].ToString();

                                if (!string.IsNullOrEmpty(fileDBKey))
                                {
                                    var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                        entry: new FileDBEntryInfo(E_FileDB.EDMS, fileDBKey),
                                        keyType: E_FileDBKeyType.Edms_CustomPdf_Form
                                    );

                                    fileDBKeysForDirectTransferSet.Add(fileDBEntryForExport);
                                }
                            }
                            else if(tableNm == "CUSTOM_LETTER")
                            {
                                var curBrokerID = reader["BrokerId"].ToString();
                                var fileDBKey = reader["FileDbKey"].ToString();

                                if (!string.IsNullOrEmpty(fileDBKey))
                                {
                                    var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                        entry: new FileDBEntryInfo(E_FileDB.Normal, fileDBKey),
                                        keyType: E_FileDBKeyType.Normal_CustomNonPdf_Form
                                    );

                                    fileDBKeysForDirectTransferSet.Add(fileDBEntryForExport);
                                }
                            }

                            TableReplicateTool.ExportOneXmlTableBySp(
                                ref errorList,
                                ref whitelistDictionary,
                                ref needUpdateWhitelist,
                                ref isGroupEnd,
                                exportXmlWriter,
                                reader,
                                connSrc,
                                connInfoBrokerId,
                                tableNm,
                                curTableMap,
                                priKeyNmList,
                                priKeyValList,
                                fkeyRealNmDictionary,
                                ////identityDictionary,
                                ////findNextWaiveSet,
                                defaultForeignKeyValDictionary,
                                needDebugInfo);

                            if (curTableWhitelist.ReviewSet.Any())
                            {
                                needReview = true;
                                if (!reviewTableSet.Contains(tableNm))
                                {
                                    reviewTableSet.Add(tableNm);
                                }

                                HashSet<string> reviewNotNullFkList = new HashSet<string>();
                                if (foreignKeyDictionary.ContainsKey(tableNm))
                                {
                                    foreach (string col in curTableWhitelist.ReviewSet)
                                    {
                                        if (foreignKeyDictionary[tableNm].Contains(col) && curTableMap[col].Nullable == false)
                                        {
                                            hasNotNullForeignKeyToReview = true;
                                            if (!reviewNotNullFkList.Contains(col))
                                            {
                                                reviewNotNullFkList.Add(col);
                                            }
                                        }
                                    }
                                }

                                // Check unique index.
                                if (!hasNotNullForeignKeyToReview && tableUniqueIdxColDictionary.ContainsKey(tableNm))
                                {
                                    List<List<string>> idxListList = tableUniqueIdxColDictionary[tableNm];
                                    foreach (List<string> idxList in idxListList)
                                    {
                                        foreach (string col in idxList)
                                        {
                                            if (whitelistDictionary[tableNm].ReviewSet.Contains(col) && curTableMap[col].Nullable == false)
                                            {
                                                hasNotNullForeignKeyToReview = true;
                                                if (!reviewNotNullFkList.Contains(col))
                                                {
                                                    reviewNotNullFkList.Add(col);
                                                }
                                            }
                                        }
                                    }
                                }

                                // Check constraint that doesn't have a common default value.
                                if (!hasNotNullForeignKeyToReview)
                                {
                                    if (specChkConstraintColTableComboSet.ContainsKey(tableNm))
                                    {
                                        List<string> colList = specChkConstraintColTableComboSet[tableNm];
                                        foreach (string col in colList)
                                        {
                                            ////fix logic problem. Should be true here.
                                            if (whitelistDictionary[tableNm].ReviewSet.Contains(col) && curTableMap[col].Nullable == true)
                                            {
                                                hasNotNullForeignKeyToReview = true;
                                                if (!reviewNotNullFkList.Contains(col))
                                                {
                                                    reviewNotNullFkList.Add(col);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (reviewNotNullFkList.Any())
                                {
                                    reviewNotNullColMsg.Append("Table " + tableNm + " needs whitelist review of columns: ");
                                    foreach (string col in reviewNotNullFkList)
                                    {
                                        reviewNotNullColMsg.Append(col + ",");
                                    }
                                }
                            }

                            if (rowsPerGroup > 0)
                            {
                                if (rowCnt == rowsPerGroup - 1)
                                {
                                    exportXmlWriter.WriteEndElement();
                                    exportXmlWriter.WriteStartElement("Group");
                                    exportXmlWriter.WriteAttributeString("connBrokerIdCustomCodeCombo", connInfoCombo);
                                    rowCnt = 0;
                                }
                                else
                                {
                                    rowCnt += 1;
                                }
                            }
                        }

                        hasNextQuery = reader.NextResult();
                        if (isGroupEnd)
                        {
                            isGroupEnd = false;
                            if (hasNextQuery && isEmptyGroup == false)
                            {
                                isEmptyGroup = true;
                                exportXmlWriter.WriteEndElement();
                                exportXmlWriter.WriteStartElement("Group");
                                exportXmlWriter.WriteAttributeString("connBrokerIdCustomCodeCombo", connInfoCombo);
                            }
                        }
                    }
                }

                if (includeFiledb)
                {
                    ShareImportExport.ExportFiledb(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, exportXmlWriter, loanIDsForFullExport, needDebugInfo, additionalFileDBEntries);
                }

                exportXmlWriter.WriteEndElement();

                if (needReview || needUpdateWhitelist)
                {
                    // PostActionForReviewWhitelist(ref errorList, ref reconstructMap, needUpdateWhitelist, whitelistPath, whitelistDictionary, reviewTableSet, exportXmlWriter, hasNotNullForeignKeyToReview, uniqueFileName);
                    Tools.LogWarning("Whitelist needs review / updating.");                    
                }

                HashSet<string> connInfoComboSet = new HashSet<string>();
                connInfoComboSet.Add(brokerId + "|" + customerCode);
                ExportDbConnInfo(ref errorList, exportXmlWriter, connInfoComboSet);
            }

            if (needReview)
            {
                if (hasNotNullForeignKeyToReview)
                {
                    errorList.Add(reviewNotNullColMsg.ToString());
                    return null;
                }
                else
                {
                    Tools.LogError("Please review new column by export whitelist and set the column to be Yes(exportable) or No(protected) or NoExportNoImport.");
                    int fileInd = uniqueFileName.LastIndexOf('\\') + 1;
                    string reviewFileName = uniqueFileName.Substring(0, fileInd) + "Check_Missing_Column_" + uniqueFileName.Substring(fileInd);
                    FileOperationHelper.Move(uniqueFileName, reviewFileName);
                    return reviewFileName;
                }
            }

            return uniqueFileName;
        }

        /// <summary>
        /// The actions dealing with missing columns after the xml export. In critical case, the export will be stopped. Otherwise, user can get the xml file.
        /// </summary>
        /// <param name="errorList">Record errors into list.</param>
        /// <param name="reconstructMap">A dictionary uses to reconstruct white list since not all information is stored in the white list dictionary.</param>
        /// <param name="needUpdateWhitelist">Decide whether to update white list or not.</param>
        /// <param name="whitelistPath">The white list address.</param>
        /// <param name="whitelistDictionary">The white list dictionary in use for export.</param>
        /// <param name="reviewTableSet">The list of tables which has columns to be reviewed.</param>
        /// <param name="exportXmlWriter">The xml writer in use for writing the exported xml.</param>
        /// <param name="hasNotNullForeignKeyToReview">A boolean flag to tell if there is any non-null-able foreign key.</param>
        /// <param name="uniqueFileName">The file path of the original exported xml. It might be changed to start with reviewing column to remind users there are columns to be reviewed.</param>
        public static void PostActionForReviewWhitelist(
            ref List<string> errorList,
            ref Dictionary<string, string> reconstructMap,
            bool needUpdateWhitelist,
            string whitelistPath,
            Dictionary<string, WhitelistData> whitelistDictionary,
            HashSet<string> reviewTableSet,
            XmlWriter exportXmlWriter,
            bool hasNotNullForeignKeyToReview,
            string uniqueFileName)
        {
            ShareImportExport.WriteCsvWhistList(ref errorList, ref reconstructMap, whitelistDictionary, whitelistPath, false);

            HashSet<string> usedWhitelistMissingColSet = new HashSet<string>();
            StringBuilder missingColMsg = new StringBuilder("The followings are missing columns:\n");
            foreach (string tableNm in reviewTableSet)
            {
                HashSet<string> reviewSet = whitelistDictionary[tableNm].ReviewSet;
                missingColMsg.Append("Table " + tableNm + ":\n");
                foreach (string colNm in reviewSet)
                {
                    missingColMsg.Append("  " + colNm + "\n");
                }

                missingColMsg.Append("\n");
            }

            string missingColMsgStr = missingColMsg.ToString();
            if (!usedWhitelistMissingColSet.Contains(missingColMsgStr))
            {
                usedWhitelistMissingColSet.Add(missingColMsgStr);
                exportXmlWriter.WriteComment(missingColMsgStr);
            }
        }

        /// <summary>
        /// Though the radio is part of the broker export but its way of exporting is quite different so I separate it from the "export all broker related table by stored procedure".
        /// </summary>
        /// <param name="errorList">List stores errors.</param>
        /// <param name="warningList">List stores warnings.</param>
        /// <param name="whitelistPath">The physics address where white list is stored.</param>
        /// <param name="mainDirectory">A directory for storing the file of missing entry, which is for testing purpose.</param>
        /// <param name="brokerId">The id of broker.</param>
        /// <param name="includeFiledb">To tell if include file database.</param>
        /// <param name="userOption">The option for exporting user. 0 means exporting no user. 1 means exporting the users belonged to one designated branch by branch name. 3 means export all users of this broker.</param>
        /// <param name="needDebugInfo">If true, show debug information in log.</param>
        /// <returns>The address of the exported xml file.</returns>
        public static string ExportLoanTemplatesByBrokerId(
            ref List<string> errorList,
            ref List<string> warningList,
            out IEnumerable<FileDBEntryInfoForExport> fileDbKeyEntriesForDirectTransfer,
            string whitelistPath,
            string mainDirectory,
            string brokerId,
            bool includeFiledb,
            string userOption,
            bool needDebugInfo)
        {
            Guid connInfoBrokerId = new Guid(brokerId);
            string customerCode = null;
            SqlParameter[] brokerParas = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(connInfoBrokerId, "GetBrokerCustomerCodeByBrokerId", brokerParas))
            {
                if (reader.Read())
                {
                    customerCode = Convert.ToString(reader["CustomerCode"]);
                }
            }

            string connInfoCombo = brokerId + "|" + customerCode;
            HashSet<string> loanIdSet = new HashSet<string>();
            HashSet<string> appIdSet = new HashSet<string>();
            List<List<string>> entryListList = new List<List<string>>();

            List<List<string>> connInfoComboListList = new List<List<string>>();
            SqlParameter[] parameters = new SqlParameter[] 
            { 
                new SqlParameter("@UserOption", userOption),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@IsByTableName", "0"),
                new SqlParameter("@InputTableName", string.Empty) 
            };
            using (var reader = StoredProcedureHelper.ExecuteReader(
                connInfoBrokerId,
                "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                311,
                parameters))
            {
                while (reader.Read())
                {
                    string loanId = Convert.ToString(reader["sLId"]).TrimWhitespaceAndBOM().ToUpper();
                    string appId = Convert.ToString(reader["aAppId"]).TrimWhitespaceAndBOM().ToUpper();
                    List<string> entryList = new List<string>();
                    List<string> connInfoComboList = new List<string>();
                    if (!loanIdSet.Contains(loanId))
                    {
                        loanIdSet.Add(loanId);
                        entryList.Add("LOAN_FILE_A|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_B|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_C|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_D|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_E|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_F|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_CACHE|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_CACHE_2|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_CACHE_3|sLId|" + loanId);
                        entryList.Add("LOAN_FILE_CACHE_4|sLId|" + loanId);
                        entryList.Add("TRUST_ACCOUNT|sLId|" + loanId);

                        for (int i = 0; i < entryList.Count; i++)
                        {
                            connInfoComboList.Add(connInfoCombo);
                        }
                    }

                    if (!appIdSet.Contains(appId))
                    {
                        appIdSet.Add(appId);
                        entryList.Add("APPLICATION_A|aAppId|" + appId);
                        entryList.Add("APPLICATION_B|aAppId|" + appId);

                        connInfoComboList.Add(connInfoCombo);
                        connInfoComboList.Add(connInfoCombo);
                    }

                    entryListList.Add(entryList);
                    connInfoComboListList.Add(connInfoComboList);
                }
            }

            HashSet<string> allExportedTableNmSet = new HashSet<string>();
            allExportedTableNmSet.Add("LOAN_FILE_A");
            allExportedTableNmSet.Add("LOAN_FILE_B");
            allExportedTableNmSet.Add("LOAN_FILE_C");
            allExportedTableNmSet.Add("LOAN_FILE_D");
            allExportedTableNmSet.Add("LOAN_FILE_E");
            allExportedTableNmSet.Add("LOAN_FILE_F");
            allExportedTableNmSet.Add("LOAN_FILE_CACHE");
            allExportedTableNmSet.Add("LOAN_FILE_CACHE_2");
            allExportedTableNmSet.Add("LOAN_FILE_CACHE_3");
            allExportedTableNmSet.Add("LOAN_FILE_CACHE_4");
            allExportedTableNmSet.Add("APPLICATION_A");
            allExportedTableNmSet.Add("APPLICATION_B");
            allExportedTableNmSet.Add("TRUST_ACCOUNT");

            bool needUpdateWhiteList;
            bool needReview;
            Dictionary<string, WhitelistData> whitelistDictionary;
            Dictionary<string, string> reconstructMap;

            string fileNm = ExportTablesBySpAndEntryList(
                ref errorList,
                ref warningList,
                out whitelistDictionary,
                out reconstructMap, 
                out needReview, 
                out needUpdateWhiteList,
                out fileDbKeyEntriesForDirectTransfer,
                whitelistPath, 
                mainDirectory, 
                "MAIN_DB", 
                includeFiledb, 
                allExportedTableNmSet, 
                false, 
                connInfoComboListList, 
                entryListList,
                needDebugInfo);

            return fileNm;
        }

        /// <summary>
        /// Export whatever in the entry list. It will not export any extra entry other than the provided ones in the list.
        /// </summary>
        /// <param name="errorList">List records errors.</param>
        /// <param name="warningList">List records warning. I plan to cancel it because I cannot send the export file while giving the warning. Maybe use warning log instead.</param>
        /// <param name="whitelistDictionary">The object representation of the whitelist.</param>
        /// <param name="reconstructMap">Some extra fields not used yet are stored there to rebuild the file.</param>
        /// <param name="needReview">Whether or not the file needs review.</param>
        /// <param name="needUpdateWhitelist">Whether or not the file needs updating.</param>
        /// <param name="whitelistPath">The address of white list.</param>
        /// <param name="mainDirectory">The main directory for any file to record information.</param>
        /// <param name="dataSrcOption">The data source name in text.</param>
        /// <param name="includeFiledb">Decide whether should include file database.</param>
        /// <param name="allExportedTableNmSet">All unique table names involve in export.</param>
        /// <param name="isLoanOverwrite">To check if it should load the white list for loan overwrite tool.</param>
        /// <param name="listOfListsOfBrokerConnectionInfoStrings">A list of lists of broker connection strings corresponding in order to the list of lists of table record strings.</param>
        /// <param name="listOfListsOfTableRecordStrings">This is the list of lists of table record strings (tablename | pk | pk value) that needed to be exported, <para/>
        /// matching the order of with the listOfListsOfBrokerConnectionInfoStrings.</param>
        /// <param name="needDebugInfo">If true, it will print the debug information into the PB log.</param>
        /// <returns>The address of the file.</returns>
        public static string ExportTablesBySpAndEntryList(
            ref List<string> errorList,
            ref List<string> warningList,
            out Dictionary<string, WhitelistData> whitelistDictionary,
            out Dictionary<string, string> reconstructMap,
            out bool needReview,
            out bool needUpdateWhitelist,
            out IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer,
            string whitelistPath,
            string mainDirectory,
            string dataSrcOption,
            bool includeFiledb,
            HashSet<string> allExportedTableNmSet,
            bool isLoanOverwrite,
            List<List<string>> listOfListsOfBrokerConnectionInfoStrings,
            List<List<string>> listOfListsOfTableRecordStrings,
            bool needDebugInfo)
        {
            ShareImportExport.DebugCount = 0;
            whitelistDictionary = null;
            reconstructMap = null;
            needReview = false;
            needUpdateWhitelist = false;
            var fileDBKeysForDirectTransferSet = new HashSet<FileDBEntryInfoForExport>();
            fileDBKeysForDirectTransfer = fileDBKeysForDirectTransferSet;

            if (listOfListsOfTableRecordStrings == null)
            {
                errorList.Add("developer error, listOfListsOfTableRecordStrings is null.");
                return null;
            }
            if (listOfListsOfTableRecordStrings.Count == 0)
            {
                errorList.Add("no entries to export.");
                return null;
            }

            ShareImportExport.ReadCsvWhiteList(ref errorList, out reconstructMap, out whitelistDictionary, whitelistPath, isLoanOverwrite);
            if (errorList.Any())
            {
                return null;
            }

            string databaseSourceName = null;
            if (dataSrcOption == "MAIN_DB")
            {
                var representativeBrokerId = new Guid(listOfListsOfBrokerConnectionInfoStrings[0][0].Split('|')[0]);
                var representativeConnectionInfo = DbConnectionInfo.GetConnectionInfo(representativeBrokerId);
                databaseSourceName = representativeConnectionInfo.InitialCatalog;
                foreach(var brokerCustomerCodeCombo in listOfListsOfBrokerConnectionInfoStrings)
                {
                    var brokerId = new Guid(brokerCustomerCodeCombo[0].Split('|')[0]);
                    var connInfo = DbConnectionInfo.GetConnectionInfo(brokerId);
                    if(connInfo.InitialCatalog != databaseSourceName)
                    {
                        warningList.Add("Exporting from different dbs.");
                    }
                }                
            }
            
            List<string> nonNullInsertList = new List<string>(); // the table names that are to be inserted first since their fk are non-nullable
            List<string> nullableInsertList = new List<string>(); //the table names that can be inserted later since their foreign keys are nullable.

            DataSrc connSrc = (dataSrcOption == "RATE_SHEET") ? DataSrc.RateSheet : DataSrc.LOShare;
            List<List<string>> fkeyRelationshipTable = TableReplicateTool.GetFkRelationshipTable(connSrc);
            Dictionary<string, List<string>> primaryKeyDictionary = ShareImportExport.GetPrimaryKeyDictionary(connSrc);
            Dictionary<string, string> defaultForeignKeyValDictionary = ShareImportExport.GetDefaultForeignKeyValDictionary();
            Dictionary<string, List<string>> foreignKeyDictionary = TableReplicateTool.GetForeignKeyDictionary(fkeyRelationshipTable);
            Dictionary<string, List<string>> fkeyRealNmDictionary = TableReplicateTool.GetFkRealNmDictionary(fkeyRelationshipTable);
            //// Dictionary<string, List<string>> tableMappingKeyList = TableReplicateTool.GetTablePairMappingKeyPairList(fkeyRelationshipTable);
            //// Dictionary<string, string> identityDictionary = ShareImportExport.HardcodeIdentityTableKeyDictionary();
            Dictionary<string, Dictionary<string, ColumnInfo>> columnInfoByColumnNameByTableName = new Dictionary<string, Dictionary<string, ColumnInfo>>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, List<string>> specChkConstraintColTableComboSet = ShareImportExport.GetCheckConstraintColTableComboSet_QuickCheck(connSrc);
            Dictionary<string, List<List<string>>> tableUniqueIdxColDictionary = ShareImportExport.GetUniqueIdxColDictionary(connSrc);
            ////HashSet<string> findNextWaiveSet = TableReplicateTool.HardCodeWaiveFindNextTableSet();
            ////TableReplicateTool.AddProtectFieldOfWhitelistIntoWaiveFindNextSet(ref findNextWaiveSet, whitelistDictionary);
            HashSet<string> connInfoComboSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            ////List<string> filedbAppIdLoanIdComboList = new List<string>();
            string stageSiteSourceName = ConstAppDavid.CurrentServerLocation.ToString();
            int debug_int = 0;
            bool hasNotNullForeignKeyToReview = false;
            HashSet<string> reviewTableSet = new HashSet<string>();
            StringBuilder reviewNotNullColMsg = new StringBuilder("The following columns must be reviewed from whitelist because they might be not-null-foreign-key, unique index or has special value range.");

            foreach (string tableNm in allExportedTableNmSet)
            {
                columnInfoByColumnNameByTableName.Add(tableNm, ShareImportExport.GetColumnInfoByColumnNameMap(connSrc, tableNm));
            }

            string uniqueFileName = TempFileUtils.NewTempFilePath() + ".xml";
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "    ";

            using (XmlWriter exportXmlWriter = XmlWriter.Create(uniqueFileName, settings))
            {
                int loanCnt = 0;
                exportXmlWriter.WriteStartElement("Export");
                exportXmlWriter.WriteAttributeString("stageSiteSourceName", stageSiteSourceName);
                exportXmlWriter.WriteAttributeString("databaseSourceName", databaseSourceName);
                exportXmlWriter.WriteAttributeString("dbConnSrc", (dataSrcOption == "RATE_SHEET") ? "RATE_SHEET" : "MAIN_DB");

                int listListSize = listOfListsOfTableRecordStrings.Count;
                var additionalFileDBEntries = new HashSet<FileDBEntryInfoForExport>();
                var loanIDsToExportFully = new HashSet<Guid>();
                for (int listlistInd = 0; listlistInd < listListSize; listlistInd++)
                {
                    List<string> listOfTableRecordStrings = listOfListsOfTableRecordStrings[listlistInd];
                    List<string> connInfoComboList = listOfListsOfBrokerConnectionInfoStrings[listlistInd];
                    if (loanCnt >= 110)
                    {
                        loanCnt = 0;
                        exportXmlWriter.WriteComment("Justin: file size limit reaches.");
                    }

                    int numTableRecords = listOfTableRecordStrings.Count;
                    if (needDebugInfo)
                    {
                        string debugEntryMessage = numTableRecords > 0 ? listOfTableRecordStrings[0] : string.Empty;
                        Tools.LogInfo("JustinDebug[" + (ShareImportExport.DebugCount++) + "]: Export group[" + listlistInd + "].  1st entry:" + debugEntryMessage);
                    }

                    exportXmlWriter.WriteStartElement("Group");
                    exportXmlWriter.WriteAttributeString("connBrokerIdCustomCodeCombo", connInfoComboList[0]);
                    for (int tableRecordIndex = 0; tableRecordIndex < numTableRecords; tableRecordIndex++)
                    {
                        try
                        {
                            string tableRecordString = listOfTableRecordStrings[tableRecordIndex];
                            string connInfoCombo = connInfoComboList[tableRecordIndex];
                            string[] connInfoTokens = connInfoCombo.Split('|');
                            Guid connInfoBrokerId = new Guid(connInfoTokens[0]);
                            string[] tokens = tableRecordString.Split('|');
                            string inputTableName = tokens[0];
                            Dictionary<string, ColumnInfo> curTableMap = null;
                            if (columnInfoByColumnNameByTableName.ContainsKey(inputTableName))
                            {
                                curTableMap = columnInfoByColumnNameByTableName[inputTableName];
                            }
                            else
                            {
                                errorList.Add("Table " + inputTableName + " is missing from table column info map");
                                return null;
                            }

                            object[] pkey = new object[3] { DBNull.Value, DBNull.Value, DBNull.Value };
                            object[] intKey = new object[3] { DBNull.Value, DBNull.Value, DBNull.Value };
                            object[] bigintKey = new object[3] { DBNull.Value, DBNull.Value, DBNull.Value };
                            object[] datetimeKey = new object[3] { DBNull.Value, DBNull.Value, DBNull.Value };
                            object[] varcharKey = new object[5] { '%', '%', '%', '%', '%' };
                            object[] variantkey = new object[3] { DBNull.Value, DBNull.Value, DBNull.Value };

                            int varcharCnt = 0;
                            int identityKeyVal = 0;
                            for (int i = 1; i < tokens.Length; i += 2)
                            {
                                varcharKey[varcharCnt++] = tokens[i + 1];
                            }

                            SqlParameter[] parameters = new SqlParameter[]
                            {
                            new SqlParameter("@UserOption", dataSrcOption),
                            new SqlParameter("@BrokerId", DBNull.Value),
                            new SqlParameter("@BranchName", string.Empty),
                            new SqlParameter("@IsByTableName", (int)1),
                            new SqlParameter("@InputTableName", inputTableName),
                            new SqlParameter("@pkey1", pkey[0]),
                            new SqlParameter("@pkey2", pkey[1]),
                            new SqlParameter("@pkey3", pkey[2]),
                            new SqlParameter("@variantKey1", variantkey[0]),
                            new SqlParameter("@variantKey2", variantkey[1]),
                            new SqlParameter("@variantKey3", variantkey[2]),
                            new SqlParameter("@intKey1", intKey[0]),
                            new SqlParameter("@intKey2", intKey[1]),
                            new SqlParameter("@intKey3", intKey[2]),
                            new SqlParameter("@bigintKey1", bigintKey[0]),
                            new SqlParameter("@bigintKey2", bigintKey[1]),
                            new SqlParameter("@bigintKey3", bigintKey[2]),
                            new SqlParameter("@datetimeKey1", datetimeKey[0]),
                            new SqlParameter("@datetimeKey2", datetimeKey[1]),
                            new SqlParameter("@datetimeKey3", datetimeKey[2]),
                            new SqlParameter("@varcharKey1", varcharKey[0]),
                            new SqlParameter("@varcharKey2", varcharKey[1]),
                            new SqlParameter("@varcharKey3", varcharKey[2]),
                            new SqlParameter("@varcharKey4", varcharKey[3]),
                            new SqlParameter("@varcharKey5", varcharKey[4]),
                            new SqlParameter("@identityKey", Convert.ToString(identityKeyVal))
                            };

                            using (var reader = (dataSrcOption == "RATE_SHEET") ?
                                StoredProcedureHelper.ExecuteReader(
                                DataSrc.RateSheet,
                                "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                                311,
                                parameters) :
                                StoredProcedureHelper.ExecuteReader(
                                connInfoBrokerId,
                                "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                                311,
                                parameters))
                            {
                                bool hasNextQuery = true;
                                bool isGroupEnd = false;
                                while (hasNextQuery)
                                {
                                    while (reader.Read())
                                    {
                                        debug_int++;
                                        string tableName = Convert.ToString(reader["TableName"]).TrimWhitespaceAndBOM();
                                        if (tableName == "LOAN_FILE_A")
                                        {
                                            loanCnt += 1;
                                        }
                                        else if (tableName == "LOAN_FILE_B")
                                        {
                                            string curLoanIdString = reader["slid"].ToString();
                                            var curLoanId = new Guid(curLoanIdString);
                                            string curClosingCostArchiveXmlContent = reader["sClosingCostArchiveXmlContent"].ToString();

                                            if (!string.IsNullOrEmpty(curClosingCostArchiveXmlContent))
                                            {
                                                List<LendersOffice.Common.SerializationTypes.ClosingCostArchive> closingCostArchives = null;
                                                try
                                                {
                                                    closingCostArchives = (List<LendersOffice.Common.SerializationTypes.ClosingCostArchive>)
                                                                                SerializationHelper.XmlDeserialize(
                                                                                    curClosingCostArchiveXmlContent,
                                                                                    typeof(List<LendersOffice.Common.SerializationTypes.ClosingCostArchive>));
                                                }
                                                catch (GenericUserErrorMessageException e)
                                                {
                                                    errorList.Add("could not parse sClosingCostArchiveXmlContent. see logs.");
                                                    Tools.LogError("could not parse sClosingCostArchiveXmlContent.", e);
                                                }

                                                if (closingCostArchives != null)
                                                {
                                                    foreach (var archive in closingCostArchives)
                                                    {
                                                        // see ClosingCostArchive > FlushToFileDBIfDirty:
                                                        additionalFileDBEntries.Add(
                                                            new FileDBEntryInfoForExport(
                                                                entry: new FileDBEntryInfo(E_FileDB.Normal, archive.FileDBKey),
                                                                keyType: E_FileDBKeyType.Normal_ClosingCostArchive
                                                            ));
                                                    }
                                                }
                                            }

                                            var sLoanDataMigrationAuditHistorySubKey = (Guid?)reader["sLoanDataMigrationAuditHistoryKey"];
                                            if (sLoanDataMigrationAuditHistorySubKey.HasValue && sLoanDataMigrationAuditHistorySubKey.Value != Guid.Empty)
                                            {
                                                var key = Migration.LoanDataMigrationResult.GetFileDbKey(curLoanId, sLoanDataMigrationAuditHistorySubKey.Value);
                                                var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                                   entry: new FileDBEntryInfo(E_FileDB.Normal, key),
                                                   keyType: E_FileDBKeyType.Normal_Loan_MigrationAuditHistoryKey
                                                );

                                                additionalFileDBEntries.Add(fileDBEntryForExport);
                                            }

                                            loanIDsToExportFully.Add(curLoanId);
                                        }
                                        else if (tableName == "FEE_SERVICE_REVISION")
                                        {
                                            var fileDBKey = reader["FileDBKey"].ToString();

                                            if (!string.IsNullOrEmpty(fileDBKey))
                                            {
                                                var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                                    entry: new FileDBEntryInfo(E_FileDB.Normal, fileDBKey),
                                                    keyType: E_FileDBKeyType.Normal_FeeServiceRevision_File
                                                );

                                                fileDBKeysForDirectTransferSet.Add(fileDBEntryForExport);
                                            }
                                        }
                                        else if (tableName == "LPE_PRICE_GROUP")
                                        {
                                            var fileDBKey = reader["ContentKey"].ToString();

                                            if (!string.IsNullOrEmpty(fileDBKey))
                                            {
                                                var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                                    entry: new FileDBEntryInfo(E_FileDB.Normal, fileDBKey),
                                                    keyType: E_FileDBKeyType.Normal_FeeServiceRevision_File
                                                );

                                                fileDBKeysForDirectTransferSet.Add(fileDBEntryForExport);
                                            }
                                        }
                                        else if (tableName == "LO_FORM")
                                        {
                                            var fileDBKey = reader["FormId"].ToString();

                                            if (!string.IsNullOrEmpty(fileDBKey))
                                            {
                                                var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                                    entry: new FileDBEntryInfo(E_FileDB.EDMS, fileDBKey),
                                                    keyType: E_FileDBKeyType.Edms_CustomPdf_Form
                                                );

                                                fileDBKeysForDirectTransferSet.Add(fileDBEntryForExport);
                                            }
                                        }
                                        else if (tableName == "CUSTOM_LETTER")
                                        {
                                            var fileDBKey = reader["FileDbKey"].ToString();

                                            if (!string.IsNullOrEmpty(fileDBKey))
                                            {
                                                var fileDBEntryForExport = new FileDBEntryInfoForExport(
                                                    entry: new FileDBEntryInfo(E_FileDB.Normal, fileDBKey),
                                                    keyType: E_FileDBKeyType.Normal_CustomNonPdf_Form
                                                );

                                                fileDBKeysForDirectTransferSet.Add(fileDBEntryForExport);
                                            }
                                        }

                                        List<string> primaryKeyNameList = primaryKeyDictionary[tableName];
                                        List<string> primaryKeyValList = new List<string>();
                                        foreach (string primaryKeyName in primaryKeyNameList)
                                        {
                                            primaryKeyValList.Add(Convert.ToString(reader[primaryKeyName]).TrimWhitespaceAndBOM());
                                        }

                                        WhitelistData curTableWhitelist = null;
                                        bool whiteListHasTable = whitelistDictionary.ContainsKey(tableName);
                                        if (whiteListHasTable)
                                        {
                                            curTableWhitelist = whitelistDictionary[tableName];
                                        }

                                        if (whiteListHasTable && !curTableWhitelist.IsAnyColumnReviewed_NonRealTime)
                                        {
                                            bool hasAny = curTableWhitelist.ExportSet.Any() || curTableWhitelist.NoImportExportSet.Any() || curTableWhitelist.NoUpdateSet.Any() || curTableWhitelist.ProtectedDictionary.Any() || curTableWhitelist.UpdateProtectDictionary.Any();
                                            curTableWhitelist.IsAnyColumnReviewed_NonRealTime = hasAny;
                                        }

                                        if (!whiteListHasTable || !whitelistDictionary[tableName].IsAnyColumnReviewed_NonRealTime)
                                        {
                                            Tools.LogError("The whole table " + tableName + " is missing from the whitelist.  Review it.");
                                            needReview = true;
                                            if (!whiteListHasTable)
                                            {
                                                whitelistDictionary.Add(tableName, new WhitelistData());
                                                whitelistDictionary[tableName].MissingSet = ShareImportExport.GetColumnNamesForTable(tableName);
                                            }

                                            if (!reviewTableSet.Contains(tableName))
                                            {
                                                reviewTableSet.Add(tableName);
                                            }

                                            break;
                                        }

                                        if (!connInfoComboSet.Contains(connInfoCombo))
                                        {
                                            connInfoComboSet.Add(connInfoCombo);
                                        }

                                        TableReplicateTool.ExportOneXmlTableBySp(
                                            ref errorList,
                                            ref whitelistDictionary,
                                            ref needUpdateWhitelist,
                                            ref isGroupEnd,
                                            exportXmlWriter,
                                            reader,
                                            connSrc,
                                            connInfoBrokerId,
                                            tableName,
                                            curTableMap,
                                            primaryKeyNameList,
                                            primaryKeyValList,
                                            fkeyRealNmDictionary,
                                            ////identityDictionary,
                                            ////findNextWaiveSet,
                                            defaultForeignKeyValDictionary,
                                            needDebugInfo);

                                        if (curTableWhitelist.IsAnyColumnReviewed_NonRealTime && curTableWhitelist.ReviewSet.Any())
                                        {
                                            needReview = true;
                                            if (!reviewTableSet.Contains(tableName))
                                            {
                                                reviewTableSet.Add(tableName);
                                            }

                                            HashSet<string> reviewNotNullFkList = new HashSet<string>();
                                            if (foreignKeyDictionary.ContainsKey(tableName))
                                            {
                                                foreach (string col in curTableWhitelist.ReviewSet)
                                                {
                                                    if (foreignKeyDictionary[tableName].Contains(col) && curTableMap[col].Nullable == false)
                                                    {
                                                        hasNotNullForeignKeyToReview = true;
                                                        if (!reviewNotNullFkList.Contains(col))
                                                        {
                                                            reviewNotNullFkList.Add(col);
                                                        }
                                                    }
                                                }
                                            }

                                            if (!hasNotNullForeignKeyToReview && tableUniqueIdxColDictionary.ContainsKey(tableName))
                                            {
                                                List<List<string>> idxListList = tableUniqueIdxColDictionary[tableName];
                                                foreach (List<string> idxList in idxListList)
                                                {
                                                    foreach (string col in idxList)
                                                    {
                                                        if (whitelistDictionary[tableName].ReviewSet.Contains(col) && curTableMap[col].Nullable == false)
                                                        {
                                                            hasNotNullForeignKeyToReview = true;
                                                            if (!reviewNotNullFkList.Contains(col))
                                                            {
                                                                reviewNotNullFkList.Add(col);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (!hasNotNullForeignKeyToReview)
                                            {
                                                if (specChkConstraintColTableComboSet.ContainsKey(tableName))
                                                {
                                                    List<string> colList = specChkConstraintColTableComboSet[tableName];
                                                    foreach (string col in colList)
                                                    {
                                                        if (whitelistDictionary[tableName].ReviewSet.Contains(col) && curTableMap[col].Nullable == true)
                                                        {
                                                            hasNotNullForeignKeyToReview = true;
                                                            if (!reviewNotNullFkList.Contains(col))
                                                            {
                                                                reviewNotNullFkList.Add(col);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (reviewNotNullFkList.Any())
                                            {
                                                reviewNotNullColMsg.Append("Table " + tableName + " needs whitelist review of columns: ");
                                                foreach (string col in reviewNotNullFkList)
                                                {
                                                    reviewNotNullColMsg.Append(col + ",");
                                                }

                                                // reviewNotNullColMsg.AppendLine("\n");
                                            }
                                        }
                                    }

                                    hasNextQuery = reader.NextResult();
                                    if (isGroupEnd)
                                    {
                                        isGroupEnd = false;
                                        if (hasNextQuery)
                                        {
                                            exportXmlWriter.WriteEndElement();
                                            exportXmlWriter.WriteStartElement("Group");
                                            exportXmlWriter.WriteAttributeString("connBrokerIdCustomCodeCombo", connInfoComboList[0]);
                                        }
                                    }
                                }
                            }
                        }
                        catch (SqlException sq)
                        {
                            errorList.Add(sq.Message);
                        }
                    }

                    exportXmlWriter.WriteEndElement();
                }

                if (includeFiledb)
                {
                    ShareImportExport.ExportFiledb(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, exportXmlWriter, loanIDsToExportFully, needDebugInfo, additionalFileDBEntries);
                }

                if (needReview || needUpdateWhitelist)
                {
                    // PostActionForReviewWhitelist(ref errorList, ref reconstructMap, needUpdateWhitelist, whitelistPath, whitelistDictionary, reviewTableSet, exportXmlWriter, hasNotNullForeignKeyToReview, uniqueFileName);
                    Tools.LogWarning("Whitelist needs review / updating.");
                }

                ExportDbConnInfo(ref errorList, exportXmlWriter, connInfoComboSet);
            }

            if (needReview)
            {
                if (hasNotNullForeignKeyToReview)
                {
                    errorList.Add(reviewNotNullColMsg.ToString());
                    return null;
                }
                else
                {
                    Tools.LogError("Please review new column by export whitelist and set the column to be Yes(exportable) or No(protected) or NoExportNoImport.");
                    int fileInd = uniqueFileName.LastIndexOf('\\') + 1;
                    string reviewFileName = uniqueFileName.Substring(0, fileInd) + "Check_Missing_Column_" + uniqueFileName.Substring(fileInd);
                    FileOperationHelper.Move(uniqueFileName, reviewFileName);
                    return reviewFileName;
                }
            }

            return uniqueFileName;
        }

        /// <summary>
        /// Import all the table entries in the xml file into database.
        /// Non-foreignKey field will be inserted: set in export.
        /// </summary>
        /// <param name="errorList">List that records error messages.</param>
        /// <param name="warningList">List warning but will not block the import process.</param>
        /// <param name="xmlPath">The address of xml file.</param>
        /// <param name="whitelistDictionary">Dictionary maps table name to white list data object.</param>
        /// <param name="fileInfos">FileInfos that may be imported if the FileDB keys are in the xml.</param>
        /// <param name="option">Option can be loan or broker. If is loan, it needs to export file database data.</param>
        /// <param name="userId">The id of user who calls this function.</param>
        /// <param name="needCombineSysConfigXml">Decide whether to combine the system default configure xml or not.</param>
        /// <param name="justinOneSqlQueryTimeOutInSeconds">A time out setting for each heavy query inside the function.</param>
        /// <param name="needDebugInfo">If true, it will show the log information in the Paul Bunyan message viewer. It has many message. </param>
        /// <param name="defaultExternalBrokerReferenceFields">If true it will default fields like sInvestorRolodexId to some default,
        /// since otherwise they could just fail.</param>
        public static void ImportOneXmlFileBySp(
            ref List<string> errorList,
            ref List<string> warningList,
            string xmlPath,
            Dictionary<string, WhitelistData> whitelistDictionary,
            IEnumerable<FileInfo> fileInfos,
            string option,
            string userId,
            bool needCombineSysConfigXml,
            int? justinOneSqlQueryTimeOutInSeconds,
            bool needDebugInfo,
            bool defaultExternalBrokerReferenceFields)
        {
            string debugRandCode = DateTime.Now.ToString() + "|Justin0007:\r\n";
            int debug_ind = 0;

            if (errorList.Any())
            {
                if (needDebugInfo)
                {
                    Tools.LogInfo("ImportOneXmlFileBySp - returning early because there were already errors." + string.Join(Environment.NewLine, errorList));
                }

                return;
            }

            var currentStageSiteSource = ConstAppDavid.CurrentServerLocation;
            if (currentStageSiteSource == ServerLocation.Production)
            {
                errorList.Add(@"Error: You are not allowed to import brokers into production servers!");

                if (needDebugInfo)
                {
                    Tools.LogInfo("ImportOneXmlFileBySp - returning early because you are not allowed to import brokers into product server");
                }

                return;
            }

            string stageSiteSourceNameAttr = null;
            string databaseSourceNameAttr = null;
            DataSrc connectionSource = DataSrc.LOShare;
            XElement filedbElement = null;
            using (XmlReader reader = XmlReader.Create(xmlPath))
            {
                if (reader.ReadToFollowing("Export"))
                {
                    stageSiteSourceNameAttr = reader.GetAttribute("stageSiteSourceName");
                    databaseSourceNameAttr = reader.GetAttribute("databaseSourceName");
                    connectionSource = (reader.GetAttribute("dbConnSrc") == "RATE_SHEET") ? DataSrc.RateSheet : DataSrc.LOShare;
                }

                if (reader.ReadToFollowing("FILEDB"))
                {
                    filedbElement = (XElement)XElement.ReadFrom(reader);
                }
            }

            if (stageSiteSourceNameAttr == null || databaseSourceNameAttr == null)
            {
                var msg = "stageSiteSourceName or databaseSourceName is missing at main tag Export. Do you try to import loan-overwrite xml through loan-replicate tool?";
                errorList.Add(msg);
                if (needDebugInfo)
                {
                    Tools.LogInfo("ImportOneXmlFileBySp - returning early because: " + msg);
                }

                return;
            }

            ILookup<int, string> errorsWithImplicitReferences;

            Dictionary<string, Dictionary<string, ColumnInfo>> allTablesColumnInfo = new Dictionary<string, Dictionary<string, ColumnInfo>>();
            Dictionary<string, string> identityKeyColumnNameByTableName = ShareImportExport.GetIdentityTableKeyDictionary(connectionSource); // rkTable as key
            Dictionary<string, List<string>> identityKeyRefereeTrioListByTableName = ShareImportExport.GetIdentityKeyRefereeTrioListByTableName(connectionSource); // fkTable as key
            Dictionary<string, string> identityKeyTargetValueBySourceTableColumnAndValue = ShareImportExport.ReadIdentityKeyMappingFromSourceToDestDb(stageSiteSourceNameAttr, databaseSourceNameAttr);
            Dictionary<string, string> oldIdentityKeyTargetValueBySourceTableColumnAndValue = new Dictionary<string, string>(identityKeyTargetValueBySourceTableColumnAndValue);
            Dictionary<string, string> defaultForeignKeyValByTableAndColumnName = ShareImportExport.GetDefaultForeignKeyValDictionary();
            Dictionary<string, List<List<string>>> tableUniqueIdxColDictionary = ShareImportExport.GetUniqueIdxColDictionary(connectionSource);
            List<ValidatedImplicitIdentityKeyReference> implicitIdentityKeyReferences = ShareImportExport.ReadImplicitIdentityKeyReferencesFile(TableReplicateTool.ImplicitIdentityKeyFullFileName, out errorsWithImplicitReferences);
            var implicitIdentityKeyReferencesByReferencingTableName = implicitIdentityKeyReferences.ToLookup(v => v.ReferencingTableName, StringComparer.OrdinalIgnoreCase);

            using (XmlReader reader = XmlReader.Create(xmlPath))
            {
                // Add Db_Connection_X_broker
                if (reader.ReadToFollowing("ConnectionInfo"))
                {
                    XElement connInfoElement = (XElement)XElement.ReadFrom(reader);
                    if (connInfoElement != null)
                    {
                        IEnumerable<XElement> brokerIdCustomerCodeComboList = connInfoElement.Elements("BrokerIdCustomerCodeCombo");
                        TableReplicateTool.ImportDbConnectionInfoByXmlElements(ref errorList, brokerIdCustomerCodeComboList);
                    }
                }
            }

            var filedbEntryInfosForImport = new HashSet<FileDBEntryInfo>();

            using (XmlReader xmlReader = XmlReader.Create(xmlPath))
            {
                Guid brokerIdForConnection = Guid.Empty;
                string connCustomerCode = string.Empty;

                while (xmlReader.ReadToFollowing("Group"))
                {
                    if (xmlReader.HasAttributes && xmlReader.MoveToAttribute("connBrokerIdCustomCodeCombo"))
                    {
                        string connBrokerIdCustomCodeCombo = xmlReader.Value;
                        string[] tokens = connBrokerIdCustomCodeCombo.Split('|');
                        brokerIdForConnection = new Guid(tokens[0]);
                        connCustomerCode = tokens[1];
                        xmlReader.MoveToElement();
                    }

                    HashSet<string> sharedDbUserIdLoginNmComboSet = new HashSet<string>();
                    while (xmlReader.ReadToDescendant("table"))
                    {
                        HashSet<Guid> allCurGroupLoanIdSet = new HashSet<Guid>();
                        List<XElement> tableList = new List<XElement>();
                        do
                        {
                            tableList.Add((XElement)XElement.ReadFrom(xmlReader));
                        }
                        while (xmlReader.ReadToNextSibling("table"));

                        int capacity = tableList.Count;

                        if (capacity == 0)
                        {
                            continue;
                        }
                        else if (!AllowImport_HardcodeCheck(ref errorList, tableList))
                        {
                            return;
                        }

                        List<List<string>> listOfPrimaryKeyNameAndValueLists = new List<List<string>>();
                        List<string> tableNamesForInsert = new List<string>(capacity);
                        List<string> tableNamesForUpdate = new List<string>(capacity);
                        List<string> insertTableScriptList = new List<string>(capacity);
                        List<string> updateTableScriptList = new List<string>(capacity);
                        List<string> testInsertTableScriptList = new List<string>(capacity);
                        List<string> testUpdateTableScriptList = new List<string>(capacity);
                        List<List<SqlParameter>> insertParaListList = new List<List<SqlParameter>>();
                        List<List<SqlParameter>> updateParaListList = new List<List<SqlParameter>>();
                        List<List<SqlParameter>> listOfListsOfParametersForInsert = new List<List<SqlParameter>>();
                        List<List<SqlParameter>> listOfListOfParametersForUpdate = new List<List<SqlParameter>>();

                        foreach (XElement tableElement in tableList)
                        {
                            debug_ind++;
                            string tableName = tableElement.Attribute("n").Value;
                            if (tableName == "CONFIG_RELEASED" && needCombineSysConfigXml)
                            {
                                if (brokerIdForConnection.ToString() == "11111111-1111-1111-1111-111111111111")
                                {
                                    continue;
                                }
                            }

                            int xmlColCnt = tableElement.IsEmpty ? 0 : tableElement.Elements().Count();
                            if (xmlColCnt == 0 || tableName == "GROUP_END")
                            {
                                continue;
                            }

                            if (!allTablesColumnInfo.ContainsKey(tableName))
                            {
                                Dictionary<string, ColumnInfo> curTableColumnInfo = ShareImportExport.GetColumnInfoByColumnNameMap(connectionSource, tableName);
                                allTablesColumnInfo.Add(tableName, curTableColumnInfo);
                            }

                            int previousErrorListCount = errorList.Count;
                            ReCollectOneTableIdentityKeyMappingByNonIdentityColAtSp(ref errorList, connectionSource, brokerIdForConnection, tableElement, identityKeyTargetValueBySourceTableColumnAndValue);
                            CheckUniqueIndexIfMatch_NonIdentityTable(ref errorList, connectionSource, brokerIdForConnection, tableElement, tableUniqueIdxColDictionary, identityKeyColumnNameByTableName, allTablesColumnInfo);
                            
                            if (errorList.Count > previousErrorListCount)
                            {
                                if (needDebugInfo)
                                {
                                    Tools.LogInfo(string.Format("continuing because errorList.Count {0} > previousErrorListCount {1}", errorList.Count, previousErrorListCount));
                                }

                                // continuing even if there are errors so we can give more errors to the user.
                                continue;
                            }

                            if (tableName == "LOAN_FILE_A")
                            {
                                Guid loanId = new Guid(tableElement.Attribute("val_0").Value.TrimWhitespaceAndBOM().ToLower());
                                if (!allCurGroupLoanIdSet.Contains(loanId))
                                {
                                    allCurGroupLoanIdSet.Add(loanId);
                                }
                            }

                            var parametersForInsert = new List<SqlParameter>();
                            var parametersForUpdate = new List<SqlParameter>();
                            var primaryKeyNameAndValueList = new List<string>();

                            GetParameters_OneXmlTable_FullReplicate_v2(
                                ref errorList,
                                ref parametersForInsert,
                                ref parametersForUpdate,
                                ref primaryKeyNameAndValueList,
                                connectionSource,
                                brokerIdForConnection,
                                allTablesColumnInfo[tableName],
                                tableElement,
                                identityKeyTargetValueBySourceTableColumnAndValue,
                                identityKeyColumnNameByTableName,
                                identityKeyRefereeTrioListByTableName,
                                defaultExternalBrokerReferenceFields); 

                            if (parametersForInsert.Any())
                            {
                                tableNamesForInsert.Add(tableName);
                                listOfListsOfParametersForInsert.Add(parametersForInsert);
                            }

                            if (parametersForUpdate.Any())
                            {
                                listOfPrimaryKeyNameAndValueLists.Add(primaryKeyNameAndValueList);
                                tableNamesForUpdate.Add(tableName);
                                listOfListOfParametersForUpdate.Add(parametersForUpdate);
                            }

                            if (tableName == "BROKER")
                            {
                                List<SqlParameter> dbConnectionParameters = new List<SqlParameter>();
                                int indOfCustomerCode = primaryKeyNameAndValueList.FindIndex(x => x == "CustomerCode");
                                string cutomerCode = primaryKeyNameAndValueList[indOfCustomerCode + 1];
                                int indOfBrokerId = primaryKeyNameAndValueList.FindIndex(x => x == "BrokerId");
                                string brokerId = primaryKeyNameAndValueList[indOfBrokerId + 1];
                                GetParameters_DB_Connection(ref errorList, out dbConnectionParameters, brokerId, cutomerCode);

                                if (errorList.Any())
                                {
                                    return;
                                }

                                if (dbConnectionParameters.Any())
                                {
                                    tableNamesForInsert.Add("DB_CONNECTION_x_BROKER");
                                    listOfListsOfParametersForInsert.Add(dbConnectionParameters);
                                }
                            }
                            else if (tableName == "ALL_USER")
                            {
                                var userIDAttributeName = "UserId";
                                var loginNmAttributeName = "LoginNm";
                                var attributesByName = GetAttributeValuesByName(tableElement, new string[] { userIDAttributeName, loginNmAttributeName });
                                string curUserId = null;
                                string curLoginNm = null;

                                if (attributesByName.ContainsKey(userIDAttributeName))
                                {
                                    curUserId = attributesByName[userIDAttributeName];
                                }

                                if (attributesByName.ContainsKey(loginNmAttributeName))
                                {
                                    curLoginNm = attributesByName[loginNmAttributeName];
                                }

                                if (curUserId == null)
                                {
                                    errorList.Add("table ALL_User missing userid.\r\n" + tableElement.ToString());
                                    Tools.LogError("table ALL_User missing loginNm.\r\n" + tableElement.ToString());
                                }

                                if (curLoginNm == null)
                                {
                                    errorList.Add("table ALL_User missing loginNm.\r\n" + tableElement.ToString());
                                    Tools.LogError("table ALL_User missing loginNm.\r\n" + tableElement.ToString());
                                }

                                if (curUserId != null && curLoginNm != null && sharedDbUserIdLoginNmComboSet.Contains(curUserId + "|" + curLoginNm) == false)
                                {
                                    sharedDbUserIdLoginNmComboSet.Add(curUserId + "|" + curLoginNm);
                                }
                            }
                            else if (tableName == "FEE_SERVICE_REVISION")
                            {
                                var filedbEntryInfo = GetFileDbEntryInfo(tableElement, tableName, "FileDbKey", E_FileDB.Normal, errorList, needDebugInfo: needDebugInfo, isFileKeyNullable: false);
                                filedbEntryInfosForImport.AddIfNotNull(filedbEntryInfo);
                            }
                            else if(tableName == "LPE_PRICE_GROUP")
                            {
                                var filedbEntryInfo = GetFileDbEntryInfo(tableElement, tableName, "ContentKey", E_FileDB.Normal, errorList, needDebugInfo: needDebugInfo, isFileKeyNullable: true);
                                filedbEntryInfosForImport.AddIfNotNull(filedbEntryInfo);                                
                            }
                            else if (tableName == "LO_FORM")
                            {
                                var filedbEntryInfo = GetFileDbEntryInfo(tableElement, tableName, "FormId", E_FileDB.EDMS, errorList, needDebugInfo: needDebugInfo, isFileKeyNullable: false);
                                filedbEntryInfosForImport.AddIfNotNull(filedbEntryInfo);
                            }
                            else if (tableName == "CUSTOM_LETTER")
                            {
                                // tdsk: merge with ExtractFileDbKey (the issue is it can be Guid.Empty.)
                                if (needDebugInfo)
                                {
                                    Tools.LogInfo("Loading from CUSTOM_LETTER xml table entry.");
                                }

                                var fileDBKeyAttributeName = "FileDBKey";
                                var attributesByName = GetAttributeValuesByName(tableElement, new string[] { fileDBKeyAttributeName });
                                string fileDBKey = null;

                                if (attributesByName.ContainsKey(fileDBKeyAttributeName))
                                {
                                    var attributeValue = attributesByName[fileDBKeyAttributeName];
                                    Guid fileDbKeyAsGuid;
                                    if (Guid.TryParse(attributeValue, out fileDbKeyAsGuid))
                                    {
                                        if(fileDbKeyAsGuid != Guid.Empty)
                                        {
                                            fileDBKey = fileDbKeyAsGuid.ToString();
                                        }
                                    }
                                }

                                if (fileDBKey != null)
                                {
                                    var fileDBEntryInfo = new FileDBEntryInfo(E_FileDB.Normal, fileDBKey);

                                    filedbEntryInfosForImport.Add(fileDBEntryInfo);

                                    if (needDebugInfo)
                                    {
                                        Tools.LogInfo("Adding the following key to filedbEntryInfosForImport: " + fileDBKey);
                                    }
                                }
                            }
                        }

                        if (errorList.Any())
                        {
                            if (needDebugInfo)
                            {
                                Tools.LogInfo("returning early because there were errors: " + string.Join(Environment.NewLine, errorList));
                            }

                            return;
                        }

                        // the identity key will be ignored in the script generation
                        for (int i = 0; i < tableNamesForInsert.Count; i++)
                        {
                            debug_ind++;
                            string tableNm = tableNamesForInsert[i];
                            List<SqlParameter> insertParameters = listOfListsOfParametersForInsert[i];
                            string insertSql = string.Empty, testSql = string.Empty;
                            string ignoreCol = identityKeyColumnNameByTableName.ContainsKey(tableNm) ? identityKeyColumnNameByTableName[tableNm] : null;
                            GetInsertScript_MinimumPara(ref errorList, ref insertSql, ref testSql, insertParameters, allTablesColumnInfo[tableNm], tableNm, ignoreCol);
                            if (identityKeyColumnNameByTableName.ContainsKey(tableNm))
                            {
                                insertSql += " \nselect IDENT_CURRENT('" + tableNm + "') as " + identityKeyColumnNameByTableName[tableNm]; // fetch the new identity key value
                            }

                            insertTableScriptList.Add(insertSql);
                            insertParaListList.Add(insertParameters); // use another listList to preserve the original one. This one got changed for identity key.
                            testInsertTableScriptList.Add(testSql);
                        }

                        for (int i = 0; i < tableNamesForUpdate.Count; i++)
                        {
                            debug_ind++;
                            string tableNm = tableNamesForUpdate[i];
                            List<string> priKeyList = listOfPrimaryKeyNameAndValueLists[i];
                            List<SqlParameter> updateParameters = listOfListOfParametersForUpdate[i];
                            string updateSql = string.Empty, testSql = string.Empty;
                            GetUpdateScript_ReplicateTool(ref errorList, ref updateSql, ref testSql, updateParameters, allTablesColumnInfo[tableNm], tableNm, priKeyList);
                            updateTableScriptList.Add(updateSql);
                            updateParaListList.Add(updateParameters);
                            testUpdateTableScriptList.Add(testSql);
                        }

                        if (needDebugInfo)
                        {
                            Tools.LogInfo(debugRandCode + "#" + debug_ind + ": Finish Getting [" + (tableNamesForUpdate.Count > 0 ? tableNamesForUpdate[0] : "Zero Table In this Group") + "]'s parameter and its scripts and also the rest tables' similar info of the same group.");
                        }

                        List<string> mainScriptList = ShareImportExport.MergeTwoLists<string>(insertTableScriptList, updateTableScriptList);
                        List<string> mainTestScriptList = ShareImportExport.MergeTwoLists<string>(testInsertTableScriptList, testUpdateTableScriptList);
                        List<List<SqlParameter>> mainParaListList = ShareImportExport.MergeTwoLists<List<SqlParameter>>(insertParaListList, updateParaListList);
                        List<string> mainTableNmList = ShareImportExport.MergeTwoLists<string>(tableNamesForInsert, tableNamesForUpdate);

                        if (option == "Loan" && mainTableNmList.Contains("LOAN_FILE_A") && filedbElement == null)
                        {
                            var msg = "Mistake in the xml file: The loan xml is missing filedb elements.";
                            errorList.Add(msg);
                            if (needDebugInfo)
                            {
                                Tools.LogInfo("returning early because there were errors: " + msg);
                            }

                            return;
                        }

                        ExecSqlInOneTransaction_IdentityKey(
                            ref errorList,
                            ref identityKeyTargetValueBySourceTableColumnAndValue, 
                            connectionSource, 
                            brokerIdForConnection, 
                            identityKeyColumnNameByTableName, 
                            identityKeyRefereeTrioListByTableName, 
                            mainScriptList, 
                            mainParaListList, 
                            mainTableNmList, 
                            defaultForeignKeyValByTableAndColumnName,
                            mainTestScriptList, 
                            justinOneSqlQueryTimeOutInSeconds, 
                            needDebugInfo,
                            debugRandCode,
                            implicitIdentityKeyReferencesByReferencingTableName);

                        if (errorList.Any())
                        {
                            if (needDebugInfo)
                            {
                                Tools.LogInfo("returning early because there were errors: " + string.Join(Environment.NewLine, errorList));
                            }

                            return;
                        }

                        // tdsk: need to fix this to only be for 'B' users, not 'P'.
                        foreach (string userInfoCombo in sharedDbUserIdLoginNmComboSet)
                        {
                            string[] tokens = userInfoCombo.Split('|');
                            string otherBrokerId = null;
                            string otherUserId = null;
                            SqlParameter[] paras = { new SqlParameter("@LoginNm", tokens[1]) };
                            using (var sqlReader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, "ALL_USER_SHARED_DB_RetrieveByLoginNm", 0, paras))
                            {
                                if (sqlReader.Read())
                                {
                                    otherBrokerId = sqlReader["BrokerId"].ToString();
                                    otherUserId = sqlReader["UserId"].ToString();
                                }
                            }

                            if (otherBrokerId != null && otherUserId != null && otherUserId != tokens[0])
                            {
                                warningList.Add("Cannot import ALL_USER_SHARED_DB_Sync: LoginNm[" + tokens[1] + "] acquires by all_user[" + tokens[0] + "] is already used by all_user[" + otherUserId + "]");
                                Tools.LogError("Cannot import ALL_USER_SHARED_DB_Sync: LoginNm[" + tokens[1] + "] acquires by all_user[" + tokens[0] + "] is already used by all_user[" + otherUserId + "]");
                            }
                            else
                            {
                                SqlParameter[] parameters = 
                                {
                                    new SqlParameter("@LoginNm", tokens[1]),
                                    new SqlParameter("@UserId", tokens[0]),
                                    new SqlParameter("@BrokerId", brokerIdForConnection)
                                };

                                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "ALL_USER_SHARED_DB_Sync", 0, parameters);
                            }
                        }

                        ShareImportExport.WriteIkOldNewMappingBackToDB(ref oldIdentityKeyTargetValueBySourceTableColumnAndValue, identityKeyTargetValueBySourceTableColumnAndValue, stageSiteSourceNameAttr, databaseSourceNameAttr);
                        foreach (Guid loanId in allCurGroupLoanIdSet)
                        {
                            if (needDebugInfo)
                            {
                                Tools.LogInfo(debugRandCode + "start to update cache for loan [" + loanId + "]");
                            }

                            Tools.UpdateCacheTable(loanId, null);
                        }
                    }
                }
            }
            
            //// Update filedb data
            if (filedbElement != null)
            {
                if (needDebugInfo)
                {
                    Tools.LogInfo("About to call ShareImportExport.ImportFiledb.");
                    Tools.LogInfo("ShareImportExport.ImportFiledb filedbEntryInfosForImport: " + string.Join(Environment.NewLine, filedbEntryInfosForImport.Select(e => e.Key)));
                    Tools.LogInfo("ShareImportExport.ImportFiledb fileInfos: " + string.Join(Environment.NewLine, fileInfos.Select(fi => fi.FullName)));
                }

                ShareImportExport.ImportFiledb(ref errorList, filedbElement, filedbEntryInfosForImport, fileInfos);

                if (needDebugInfo)
                {
                    if (errorList.Any())
                    {
                        Tools.LogInfo("ShareImportExport.ImportFiledb resulted in errors: " + string.Join(Environment.NewLine, errorList));
                    }
                }
            }
            else
            {
                if (needDebugInfo)
                {
                    Tools.LogInfo("FileDB element was null.");
                }
            }

            if (needDebugInfo)
            {
                Tools.LogInfo(debugRandCode + "Great success! The file is imported.");
            }
        }

        /// <summary>
        /// Extracts the filedbkey from the given element and creates the appropriate FileDBEntryInfo, or returns null.
        /// </summary>
        /// <returns></returns>
        private static FileDBEntryInfo GetFileDbEntryInfo(XElement tableElement, string tableName, string fileDBKeyAttributeName, E_FileDB fileDbType, List<string> errorList, bool needDebugInfo, bool isFileKeyNullable)
        {
            // tdsk: merge this logic with the above, as well as with that in ShareImportExport
            if (needDebugInfo)
            {
                Tools.LogInfo($"Loading from {tableName} xml table entry.");
            }
            
            var attributesByName = GetAttributeValuesByName(tableElement, new string[] { fileDBKeyAttributeName });
            string fileDBKey = null;

            if (attributesByName.ContainsKey(fileDBKeyAttributeName))
            {
                if (isFileKeyNullable)
                {
                    var attributeValue = attributesByName[fileDBKeyAttributeName];
                    if (!string.Equals(attributeValue, "NULL", StringComparison.OrdinalIgnoreCase))
                    {
                        fileDBKey = attributeValue;
                    }
                }
                else
                {
                    fileDBKey = attributesByName[fileDBKeyAttributeName];
                }
            }

            if (fileDBKey != null)
            {
                var fileDBEntryInfo = new FileDBEntryInfo(fileDbType, fileDBKey);

                if (needDebugInfo)
                {
                    Tools.LogInfo("Adding the following key to filedbEntryInfosForImport: " + fileDBKey);
                }

                return fileDBEntryInfo;
            }
            else
            {
                if (!isFileKeyNullable)
                {
                    var msg = "table " + tableName + " is missing attribute " + fileDBKeyAttributeName + "." + Environment.NewLine + tableElement.ToString();
                    errorList.Add(msg);
                    Tools.LogError(msg);
                }

                return null;
            }
        }

        /// <summary>
        /// Given an xml import table element, return a dictionary of the attributes by name corresponding to the named attributes given.
        /// </summary>
        /// <param name="tableElement">The table element to find the attributes of.</param>
        /// <param name="attibuteNamesOfInterest">The names of the attributes we want.</param>
        /// <returns>A dictionary of attibute values by name.</returns>
        private static Dictionary<string, string> GetAttributeValuesByName(XElement tableElement, IEnumerable<string> attibuteNamesOfInterest)
        {
            IEnumerable<XElement> colElements = tableElement.Elements();

            var attributeNamesSet = new HashSet<string>(attibuteNamesOfInterest);
            var attributeValueByName = new Dictionary<string, string>();
            
            foreach (XElement colElement in colElements)
            {
                XAttribute colNmAttr = colElement.Attribute("n");
                if (colNmAttr == null)
                {
                    continue;
                }

                if (!attributeNamesSet.Contains(colNmAttr.Value))
                {
                    continue;
                }

                XAttribute colValAttr = colElement.Attribute("v");
                if (colValAttr != null)
                {
                    attributeValueByName.Add(colNmAttr.Value, colValAttr.Value);
                }
            }

            return attributeValueByName;
        }

        /// <summary>
        /// Gets the primary keys and values associated with the table element, ordered.
        /// </summary>
        /// <param name="xmlPrimaryKeyValueByName">The primary key values by primary key column name.</param>
        /// <param name="tableXml">The xml element containing the table row and its primary key column names and values.</param>
        private static void GetSubPrimaryKeysForTableElement(out Dictionary<string, string> xmlPrimaryKeyValueByName, XElement tableXml)
        {
            xmlPrimaryKeyValueByName = new Dictionary<string, string>();
            List<XAttribute> tableElementAttributeList = tableXml.Attributes().ToList();

            for (int i = 0; i < tableElementAttributeList.Count; i++)
            {
                if (!tableElementAttributeList[i].Name.ToString().StartsWith("val"))
                {
                    continue;
                }

                // the XML attributes are arranged so the sub primary key name precedes the sub primary key value.
                var subPrimaryKeyValue = tableElementAttributeList[i].Value;
                var subPrimaryKeyName = tableElementAttributeList[i - 1].Value;

                if (!xmlPrimaryKeyValueByName.ContainsKey(subPrimaryKeyName))
                {
                    xmlPrimaryKeyValueByName.Add(subPrimaryKeyName, subPrimaryKeyValue);
                }
            }
        }
    }
}