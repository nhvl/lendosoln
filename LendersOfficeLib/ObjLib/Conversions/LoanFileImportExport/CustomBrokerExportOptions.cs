﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;

    /// <summary>
    /// A class encapsulating the extra parameters for custom export beyond <see cref="BrokerExportOptions"/>
    /// </summary>
    [Serializable]
    public class CustomBrokerExportOptions
    {
        /// <summary>
        /// Gets or sets a value of the tab or pipe separated and semicolon/line separated entries for export.
        /// </summary>
        /// <default>Empty string.</default>
        /// <value>The text for the custom export.</value>
        public string Text { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets a value indicating whether or not we want the tool to include related tables.
        /// </summary>
        /// <default>true</default>
        /// <value>True iff we want the tool to include files.</value>
        public bool IncludeRelatedTables { get; set; } = true;

        /// <summary>
        /// Gets or sets a value indicating whether or not we want the tool to include db entries outside of this broker. <para></para>
        /// May be useful to set to false if we want say shared db entries, but otherwise it is safer to set this to true.
        /// </summary>
        /// <default>true</default>
        /// <value>True iff we want the tool to be able to export from other brokers.</value>
        public bool UseOnlyThisBrokersDb { get; set; } = true;
    }
}
