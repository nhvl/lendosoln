﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A class encapsulating the result of an import of a loan.
    /// </summary>
    public class LoanImportResult
    {
        /// <summary>
        /// Gets or sets the id of the loan that was created during import. <para></para>
        /// Null if only overwriting, not null if the tool had to create a loan and overwrite that.
        /// </summary>
        /// <value>Null if only overwriting, not null if the tool had to create a loan and overwrite that.</value>
        public Guid? CreatedLoanId { get; set; }

        /// <summary>
        /// Gets or sets the list of errors associated with the import. <para></para>
        /// Empty if successful.<para></para>
        /// This can be considered sensitive info.
        /// </summary>
        /// <value>Empty if successful, not empty if not.</value>
        public List<string> Errors { get; set; } = new List<string>();
    }
}
