﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    /// <summary>
    /// The type of implicit reference from a string to an identity key.
    /// </summary>
    public enum ImplicitReferenceType
    {
        /// <summary>
        /// When the xpath refers to a node containing the text of interest.
        /// </summary>
        XPathToText = 0,

        /// <summary>
        /// When the xpath refers to a node with an attribute of interest.
        /// </summary>
        XPathAndAttribute = 1,

        /// <summary>
        /// When the reference is between separate databases.
        /// </summary>
        DbToDb = 2
    }
}
