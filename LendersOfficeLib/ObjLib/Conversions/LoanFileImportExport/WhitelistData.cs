﻿//-----------------------------------------------------------------------
// <copyright file="WhitelistData.cs" company="Meridianlink">
//     Copyright (c) Sprocket Enterprises. All rights reserved.
// </copyright>
// <author>Zijing Jia</author>
// <summary>Store white list data type.</summary>
//-----------------------------------------------------------------------

namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Security.Cryptography;
    using System.Text;
    using Common;
    using global::CommonProjectLib.Common.Lib;
    using DataAccess;

    /// <summary>
    /// Get all the white list related tag: export, protect, review, noImportExport.
    /// For columns that are allowed to be export during insertion but not allow export in update, that column should be stored both 
    /// in export set and no update set.
    /// The whitelist support user to enter special code "HASH{}" and "RANDOM(size)" to generate hash and random value within allowed
    /// range of characters(based on password's rule but more narrow than that.) 
    /// However, due to the nature of "random", we cannot just do it once for all but have to do it every time it uses a value. So,
    /// The only way is that when a function calls a default value with "RANDOM{size}" in it, it should replace this code with a real
    /// random value. Same thing to hash since we might need HASH{RANDOM{size}}.
    /// </summary>
    public class WhitelistData
    {
        /// <summary>
        /// Initializes a new instance of the WhitelistData class.
        /// </summary>
        public WhitelistData()
        {
            this.ExportSet = new HashSet<string>();
            this.ReviewSet = new HashSet<string>();
            this.NoImportExportSet = new HashSet<string>();
            this.ProtectedDictionary = new FriendlyDictionary<string, string>();
            this.NoUpdateSet = new HashSet<string>();
            this.UpdateProtectDictionary = new FriendlyDictionary<string, string>();
            this.MissingSet = new HashSet<string>();
            this.IsAnyColumnReviewed_NonRealTime = false;
        }

        /// <summary>
        /// Gets or sets table name of current white list data.
        /// </summary>
        /// <value>Table name string.</value>
        public string TableName { get; set; }

        /// <summary>
        /// Gets or sets the names of the (columns) / (class properties) that the whitelist is missing. <para/>
        /// Note that these are coming from a combination of the whitelist + the database.
        /// </summary>
        /// <value>The names of the columns that the whitelist doesn't have.</value>
        public HashSet<string> MissingSet { get; set; }

        /// <summary>
        /// Gets or sets the exportable column name set.
        /// </summary>
        /// <value>Exportable column name set.</value>
        public HashSet<string> ExportSet { get; set; }

        /// <summary>
        /// Gets or sets the review column name set.
        /// </summary>
        /// <value>Column name set that require to be reviewed.</value>
        public HashSet<string> ReviewSet { get; set; }

        /// <summary>
        /// Gets or sets the column name set that are not allowed to be exported/imported.
        /// </summary>
        /// <value>Column name set that require to be reviewed.</value>
        public HashSet<string> NoImportExportSet { get; set; }

        /// <summary>
        /// Gets or sets the protected column name->default value dictionary.
        /// </summary>
        /// <value>Protected column name To default value of that column.</value>
        public FriendlyDictionary<string, string> ProtectedDictionary { get; set; }

        /// <summary>
        /// Gets or sets the column name set which will not be updated.
        /// </summary>
        /// <value>Column names that are not updated.</value>
        public HashSet<string> NoUpdateSet { get; set; }

        /// <summary>
        /// Gets or sets the protected columns' column name and default value dictionary.
        /// </summary>
        /// <value>Map protected column name to its substitution value.</value>
        public FriendlyDictionary<string, string> UpdateProtectDictionary { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether any column is reviewed in table.  If false, the whole table in the whitelist is suspect and needs review.
        /// </summary>
        /// <value>True: there is at least one column reviewed and it is not stored in review set but in other structure.</value>
        public bool IsAnyColumnReviewed_NonRealTime { get; set; }

        /// <summary>
        /// It's a simple version. It can only parse "HASH{value}", "RANDOM{size}" or "HASH{RANDOM{size}}". 
        /// DefaultVal that follows other pattern will be returned as it was.
        /// </summary>
        /// <param name="passwordHash">The result according to the white list special code.</param>
        /// <param name="passwordSalt">The password salt. If not set in white list special code, then its value will be null.</param>
        /// <param name="defaultVal">The content user writes at the default value column in the white list.</param>
        /// <param name="passwordForbids">A list of string not allowed to be used in password, such as first name, last name, login name.</param>
        public static void ConvertWhitelistPasswordCode(out string passwordHash, out string passwordSalt, string defaultVal, string[] passwordForbids)
        {
            string codePara = "0";
            if (defaultVal.StartsWith("HASH{RANDOM{"))
            {
                codePara = defaultVal.Substring(12).TrimEnd('}');
                string randVal = GenerateRandomWord(Convert.ToInt32(codePara), passwordForbids);
                passwordHash = Tools.HashOfPassword(randVal);
                passwordSalt = null;
            }
            else if (defaultVal.StartsWith("HASH{"))
            {
                codePara = defaultVal.Substring(5).TrimEnd('}');
                passwordHash = Tools.HashOfPassword(codePara);
                passwordSalt = null;
            }
            else if (defaultVal.StartsWith("RANDOM{"))
            {
                codePara = defaultVal.Substring(7).TrimEnd('}');
                passwordHash = GenerateRandomWord(Convert.ToInt32(codePara), passwordForbids);
                passwordSalt = null;
            }
            else if (defaultVal.StartsWith("CONSUMER_PORTAL_TEMP_PASSWORD_HASH{"))
            {
                string password = EncryptionHelper.GetPasswordAndPBKDF2HashSalt(out passwordHash, out passwordSalt);
            }
            else if (defaultVal.StartsWith("CONSUMER_PORTAL_TEMP_PASSWORD_SALT{"))
            {
                string password = EncryptionHelper.GetPasswordAndPBKDF2HashSalt(out passwordHash, out passwordSalt);
            }
            else
            {
                passwordHash = defaultVal;
                passwordSalt = null;
            }
        }

        /// <summary>
        /// For the special code "RANDOM".
        /// </summary>
        /// <param name="lengthOfPwd">The length of password.</param>
        /// <param name="randwordForbids">A list of string that are forbidden.</param>
        /// <returns>Return a random string.</returns>
        public static string GenerateRandomWord(int lengthOfPwd, string[] randwordForbids)
        {
            string charSrc = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$*&|"; 
            int legnthOfSrc = charSrc.Length;
            StringBuilder randword = new StringBuilder();
            RNGCryptoServiceProvider rngCsp = new RNGCryptoServiceProvider();
            bool hasNumeric = false;
            bool hasAlphabetic = false;
            bool hasSpecialChar = false;

            for (int nextIndex = 0, numOfTries = 0; nextIndex < lengthOfPwd && numOfTries < 100; numOfTries++)
            {
                byte[] randomNum = new byte[1];
                char randomChar = (char)0;
                if (nextIndex >= lengthOfPwd - 3)
                {
                    if (!hasAlphabetic)
                    {
                        int randIndex = Convert.ToInt32(randomNum[0]) % 52;
                        randomChar = charSrc[randIndex];
                    }
                    else if (!hasNumeric)
                    {
                        int randIndex = Convert.ToInt32(randomNum[0]) % 10;
                        randomChar = (char)('0' + randIndex);
                    }
                    else if (!hasSpecialChar)
                    {
                        int randIndex = (Convert.ToInt32(randomNum[0]) % 7) + 62;
                        randomChar = charSrc[randIndex];
                    }
                    else
                    {
                        rngCsp.GetBytes(randomNum);
                        int randIndex = Convert.ToInt32(randomNum[0]) % legnthOfSrc;
                        randomChar = charSrc[randIndex];
                    }
                }
                else
                {
                    rngCsp.GetBytes(randomNum);
                    int randIndex = Convert.ToInt32(randomNum[0]) % legnthOfSrc;
                    randomChar = charSrc[randIndex];
                }

                //// cannot contain 3 identical characters or consecutive alphabetical or numeric characters
                if (nextIndex >= 2)
                {
                    if (randword[nextIndex - 2] == randword[nextIndex - 1] && randword[nextIndex - 1] == randomChar)
                    {
                        continue;
                    }
                    else if (randword[nextIndex - 2] + 1 == randword[nextIndex - 1] && randword[nextIndex - 1] + 1 == randomChar)
                    {
                        continue;
                    }
                    else if (randword[nextIndex - 2] - 1 == randword[nextIndex - 1] && randword[nextIndex - 1] - 1 == randomChar)
                    {
                        continue;
                    }
                }

                //// cannot contain first name, last name, or login name. 
                bool containsForbidWord = false;
                string tryRandword = randword.ToString() + randomChar;
                foreach (string forbidStr in randwordForbids)
                {
                    if (tryRandword.IndexOf(forbidStr, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        containsForbidWord = true;
                        break;
                    }
                }

                if (containsForbidWord)
                {
                    continue;
                }

                if (randomChar >= '0' && randomChar <= '9')
                {
                    hasNumeric = true;
                }
                else if (randomChar >= 'a' && randomChar <= 'z')
                {
                    hasAlphabetic = true;
                }
                else if (randomChar >= 'A' && randomChar <= 'Z')
                {
                    hasAlphabetic = true;
                }

                randword.Append(randomChar);
                nextIndex += 1;
            }

            return randword.ToString();
        }
    }
}
