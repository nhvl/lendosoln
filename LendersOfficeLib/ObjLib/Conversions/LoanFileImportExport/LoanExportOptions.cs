﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    
    /// <summary>
    /// A class encapsulating the options for exporting a loan.
    /// </summary>
    [Serializable]
    public class LoanExportOptions
    {
        /// <summary>
        /// Gets or sets the id of the loan to export.
        /// </summary>
        /// <value>The id of the loan to export.</value>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to include filedb entries associated with the loan.
        /// </summary>
        /// <value>True iff we want to include file db entries.</value>
        public bool IncludeFileDbEntries { get; set; } = true;
    }
}
