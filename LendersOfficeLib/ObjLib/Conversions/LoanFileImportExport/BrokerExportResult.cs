﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A class encapsulating the result of a broker export.
    /// </summary>
    [Serializable]
    public class BrokerExportResult
    {
        /// <summary>
        /// Gets a value indicating whether or not the import succeeded.
        /// </summary>
        /// <value>True iff the export succeeded.</value>
        public bool Succeeded
        {
            get
            {
                return !this.ErrorMessages.Any();
            }
        }

        /// <summary>
        /// Gets or sets the file name relative to the temporary directory.
        /// </summary>
        /// <value>The file name in the temp directory where the result xml/zip resides.</value>
        public string TempFileName { get; set; }

        /// <summary>
        /// Gets or sets the type of content of the result.
        /// </summary>
        /// <value>The type of content of the result, can be xml or zip etc.</value>
        public string ContentType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the result is compressed.
        /// </summary>
        /// <value>Whether or not the result is compressed.</value>
        public bool IsCompressed { get; set; }

        /// <summary>
        /// Gets or sets a list of warning messages.  These did not block the import.
        /// </summary>
        /// <value>The list of warning messages that occurred that didn't block the import but that the user should be aware of.</value>
        public List<string> WarningMessages { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets a list of error messages.  Each one individually blocked the import.
        /// </summary>
        /// <value>The list of error messages that occurred that prevented the import.</value>
        public List<string> ErrorMessages { get; set; } = new List<string>();
    }
}
