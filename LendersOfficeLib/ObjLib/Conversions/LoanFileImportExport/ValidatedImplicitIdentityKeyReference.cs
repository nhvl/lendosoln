﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.XPath;
    using DataAccess;
    using Drivers.NetFramework;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Represents the validated relevant information for an implicit reference from one table to another.<para></para>
    /// The object validates <see cref="UnvalidatedImplicitIdentityKeyReference"/>.<para></para>
    /// An example might be FEE_SERVICE_TEMPLATE FeeServiceRevision is xml that contains attributes and text elements that refer to the identity key of the Region table. <para></para>
    /// Another example is a conversation log db Category has a DefaultPermissionLevelId that refers to a main db CONVOLOG_PERMISSION_LEVEL id.<para></para>
    /// It has several related private methods for doing validation.
    /// </summary>
    public class ValidatedImplicitIdentityKeyReference
    {
        /// <summary>
        /// Gets the data source where resides the table that implicitly refers to the identity key.
        /// </summary>
        public DataSrc ReferencingDataSource { get; private set; }

        /// <summary>
        /// Gets the name of the table that implicitly refers to the identity key.
        /// </summary>
        public string ReferencingTableName { get; private set; }

        /// <summary>
        /// Gets the name of the column that implicitly refers to the identity key.
        /// </summary>
        public string ReferencingColumnName { get; private set; }

        /// <summary>
        /// Gets the data source where resides the table with the identity key being referenced.
        /// </summary>
        public DataSrc IdentityKeyDataSource { get; private set; }

        /// <summary>
        /// Gets the name of the table where resides the identity key being referenced.
        /// </summary>
        public string IdentityKeyTableName { get; private set; }

        /// <summary>
        /// Gets the name of the column of the identity key being referenced.
        /// </summary>
        public string IdentityKeyColumName { get; private set; }

        /// <summary>
        /// Gets the type of implicit reference.
        /// </summary>
        public ImplicitReferenceType Type { get; private set; }

        /// <summary>
        /// Gets the XPathExpression to the node where the reference to the identity key is, if applicable.
        /// </summary>
        public XPathExpression XPath { get; private set; }

        /// <summary>
        /// Gets the name of the attribute where the reference to the identity key is, if applicable.
        /// </summary>
        public string AttributeName { get; private set; }

        /// <summary>
        /// Validates the unvalidated reference and yields a valid object or a list of reasons it's not valid.
        /// </summary>
        /// <param name="unvalidatedReference">The reference that hasn't yet been validated.</param>
        /// <param name="validReference">The validated reference, or null if not valid.</param>
        /// <returns>Reasons that the unvalidated reference is not valid, or null if it's valid.</returns>
        public static IEnumerable<string> GetValidInstanceOrReasonsInvalid(
            UnvalidatedImplicitIdentityKeyReference unvalidatedReference,
            out ValidatedImplicitIdentityKeyReference validReference)
        {
            validReference = null;
            List<string> errors = new List<string>();

            if (unvalidatedReference == null)
            {
                errors.Add(nameof(unvalidatedReference) + " was null.");
                return errors;
            }

            var candidateValidReference = new ValidatedImplicitIdentityKeyReference();

            SetValidEnumOrUpdateErrors<DataSrc>(nameof(unvalidatedReference.ReferencingDataSource), unvalidatedReference.ReferencingDataSource, errors, (d) => candidateValidReference.ReferencingDataSource = d);
            SetValidEnumOrUpdateErrors<DataSrc>(nameof(unvalidatedReference.IdentityKeyDataSource), unvalidatedReference.IdentityKeyDataSource, errors, (d) => candidateValidReference.IdentityKeyDataSource = d);
            SetValidDbTableNameOrUpdateErrors(nameof(unvalidatedReference.IdentityKeyTableName), unvalidatedReference.IdentityKeyTableName, errors, (s) => candidateValidReference.IdentityKeyTableName = s);
            SetValidDbTableNameOrUpdateErrors(nameof(unvalidatedReference.ReferencingTableName), unvalidatedReference.ReferencingTableName, errors, (s) => candidateValidReference.ReferencingTableName = s);
            SetValidDbColumnNameOrUpdateErrors(nameof(unvalidatedReference.ReferencingColumnName), unvalidatedReference.ReferencingColumnName, errors, (s) => candidateValidReference.ReferencingColumnName = s);
            SetValidDbColumnNameOrUpdateErrors(nameof(unvalidatedReference.IdentityKeyColumName), unvalidatedReference.IdentityKeyColumName, errors, (s) => candidateValidReference.IdentityKeyColumName = s);
            SetValidEnumOrUpdateErrors<ImplicitReferenceType>(nameof(unvalidatedReference.Type), unvalidatedReference.Type, errors, (t) => candidateValidReference.Type = t);

            if (candidateValidReference.Type == ImplicitReferenceType.XPathToText || candidateValidReference.Type == ImplicitReferenceType.XPathAndAttribute)
            {
                SetValidXPathOrUpdateErrors(nameof(unvalidatedReference.XPath), unvalidatedReference.XPath, errors, (x) => candidateValidReference.XPath = x);
            }

            if (candidateValidReference.Type == ImplicitReferenceType.XPathAndAttribute)
            {
                SetValidAttributeNameOrUpdateErrors(nameof(unvalidatedReference.AttributeName), unvalidatedReference.AttributeName, errors, (s) => candidateValidReference.AttributeName = unvalidatedReference.AttributeName);
            }

            if (!errors.Any())
            {
                validReference = candidateValidReference;
            }

            return errors;
        }

        /// <summary>
        /// Validates the xpath and allows updating the valid instance's XPathExpression if it is valid, otherwise updates the errors with reasons it's not valid.
        /// </summary>
        /// <param name="name">The name of the property being validated.</param>
        /// <param name="candidate">The candidate path that may or may not be valid.</param>
        /// <param name="errors">The errors associated with validation, that may be updated.</param>
        /// <param name="update">The method to run with the valid XPathExpression if the xpath is valid.  This may set the valid instance's property.</param>
        private static void SetValidXPathOrUpdateErrors(string name, string candidate, List<string> errors, Action<XPathExpression> update)
        {
            if (string.IsNullOrEmpty(candidate))
            {
                errors.Add(name + " is null or empty.");
                return;
            }

            XPathExpression x;
            try
            {
                x = XPathExpression.Compile(candidate);
                update(x);
            }
            catch (ArgumentException)
            {
                errors.Add($"{name} '{candidate}' is an invalid argument to XPathExpression.Compile");
            }
            catch (XPathException)
            {
                errors.Add($"{name} '{candidate}' is yields an XPathException when calling XPathExpression.Compile");
            }
        }

        /// <summary>
        /// Validates the xml attribute name and allows updating the valid instance's attribute if it is valid, otherwise updates the errors with reasons it's not valid.
        /// </summary>
        /// <param name="name">The name of the property being validated.</param>
        /// <param name="candidate">The candidate xml attribute name that may or may not be valid.</param>
        /// <param name="errors">The errors associated with validation, that may be updated.</param>
        /// <param name="update">The method to run with the valid attribute name if it is valid. This may set the valid instance's property.</param>
        private static void SetValidAttributeNameOrUpdateErrors(string name, string candidate, List<string> errors, Action<string> update)
        {
            if (string.IsNullOrEmpty(candidate))
            {
                errors.Add(name + $" is null or empty but the type was {ImplicitReferenceType.XPathAndAttribute.ToString("G")}.");
                return;
            }

            if (IsValidXmlAttributeName(candidate))
            {
                update(candidate);
            }
            else
            {
                errors.Add($"{name} '{candidate}' is not a valid attributeName.");
            }
        }

        /// <summary>
        /// Checks if the xml attribute name is valid. <para></para>
        /// Note: There is probably a better way but due to limited time constraints, capturing the error is the only way I could think to check.
        /// </summary>
        /// <param name="attributeName">The candidate attribute name that may or may not be valid.</param>
        /// <returns>Whether or not the candidate is a valid xml attribute name.</returns>
        private static bool IsValidXmlAttributeName(string attributeName)
        {
            try
            {
                var d = new System.Xml.XmlDocument();
                d.LoadXml($@"<a {attributeName}=""X""/>");
                return true;
            }
            catch (System.Xml.XmlException)
            {
                return false;
            }
        }

        /// <summary>
        /// Validates the candidate and allows updating the valid instance's property if it is valid, otherwise updates the errors with reasons it's not valid.
        /// </summary>
        /// <typeparam name="EnumType">The type of enum to convert the candidate to.</typeparam>
        /// <param name="name">The name of the property being validated.</param>
        /// <param name="candidate">The candidate that may or may not be a valid type of the provided enum type.</param>
        /// <param name="errors">The errors associated with validation, that may be updated.</param>
        /// <param name="update">The method to run with the valid enum type if it is valid. This may set the valid instance's property.</param>
        private static void SetValidEnumOrUpdateErrors<EnumType>(string name, string candidate, List<string> errors, Action<EnumType> update) where EnumType : struct
        {
            if (string.IsNullOrEmpty(candidate))
            {
                errors.Add(name + " is null or empty.");
                return;
            }

            EnumType dataSrc;
            if (!Enum.TryParse<EnumType>(candidate, out dataSrc))
            {
                errors.Add($"{name} '{candidate}' is not a known {typeof(EnumType)}.");
                return;
            }

            update(dataSrc);
        }

        /// <summary>
        /// Validates the table name and allows updating the valid instance's property if it is valid, otherwise updates the errors with reasons it's not valid.
        /// </summary>
        /// <param name="name">The name of the property being validated.</param>
        /// <param name="candidate">The candidate table name that may or may not be valid.</param>
        /// <param name="errors">The errors associated with validation, that may be updated.</param>
        /// <param name="update">The method to run with the valid table name if it is valid. This may set the valid instance's property.</param>
        private static void SetValidDbTableNameOrUpdateErrors(string name, string candidate, List<string> errors, Action<string> update)
        {
            if (string.IsNullOrEmpty(candidate))
            {
                errors.Add(name + " is null or empty.");
                return;
            }

            bool isOk = RegularExpressionHelper.IsMatch(RegularExpressionString.DBTableName, candidate);
            if (!isOk)
            {
                errors.Add($"{name} '{candidate}' is not a valid db table name.");
                return;
            }

            update(candidate);
        }

        /// <summary>
        /// Validates the column name and allows updating the valid instance's property if it is valid, otherwise updates the errors with reasons it's not valid.
        /// </summary>
        /// <param name="name">The name of the property being validated.</param>
        /// <param name="candidate">The candidate column name that may or may not be valid.</param>
        /// <param name="errors">The errors associated with validation, that may be updated.</param>
        /// <param name="update">The method to run with the valid column name if it is valid. This may set the valid instance's property.</param>
        private static void SetValidDbColumnNameOrUpdateErrors(string name, string candidate, List<string> errors, Action<string> update)
        {
            if (string.IsNullOrEmpty(candidate))
            {
                errors.Add(name + " is null or empty.");
                return;
            }

            bool isOk = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, candidate);
            if (!isOk)
            {
                errors.Add($"{name} '{candidate}' is not a valid db column name.");
                return;
            }

            update(candidate);
        }
    }
}
