﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Xml;
    using System.Xml.Linq;
    using Admin;
    using Common;
    using global::ConfigSystem;
    using global::ConfigSystem.Engine;
    using DataAccess;
    using DataAccess.TempFile;
    using Drivers.Gateways;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.Drivers.SecurityEventLogging;

    /// <summary>
    /// A class for importing/exporting xml of brokers / table records from one stage to another. <para></para>
    /// </summary>
    public class BrokerReplicator
    {
        // tdsk: Replace SenderWhitelistFileName, SenderWhiteListFullName with a filedb key(s) so all servers on a stage would share it, perhaps with a history.

        /// <summary>
        /// The filename of the white list. Need further transformation to get the real address according to the setting of server. <para/>
        /// </summary>
        private const string SenderWhitelistFileName = "senderWhiteList.csv.config";

        /// <summary>
        /// Gets the full address of the white list. <para/>
        /// </summary>
        /// <value>The full address of the whitelist.</value>
        private static string SenderWhiteListFullName
        {
            get
            {
                return (HttpContext.Current != null) ? Tools.GetServerMapPath("~/" + BrokerReplicator.SenderWhitelistFileName) : Path.Combine(Directory.GetCurrentDirectory(), SenderWhitelistFileName);
            }
        }
        
        /// <summary>
        /// Function to import the table xml called by page file button.<para></para>
        /// Requirements: <para></para>
        ///     - not on production <para></para>
        /// Side effects:<para></para>
        ///     - if it's on a http request, it will set the timeout to a long time (ConstStage.JustinBrokerImportTimeOutInSeconds) <para></para>
        /// </summary>
        /// <param name="principal">The principal that is executing the request.</param>
        /// <param name="options">The options to be used while importing the xml.</param>
        /// <returns>An enumeration of reasons it didn't work, or an empty enumeration.</returns>
        public static BrokerImportResult ImportBrokerXml(AbstractUserPrincipal principal, BrokerImportOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if (options.SourceFileNameIncludingExtension == null)
            {
                throw new ArgumentNullException(nameof(options.SourceFileNameIncludingExtension));
            }

            var result = new BrokerImportResult();
            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                // tdsk - have this check the database, not by context name.
                result.ErrorMessages.Add("Cannot import into production.");
                return result;
            }
            
            if (!CanImport(principal))
            {
                result.ErrorMessages.Add("You may only import brokers as a LoAdmin user or as a system user, or with special permission.");
                return result;
            }

            if (principal is BrokerUserPrincipal && !TempFileAccessManager.DoesFileBelongToCurrentUser(principal, options.FileNameToImport))
            {
                result.ErrorMessages.Add("You did not create this file so you may not import it.");
                return result;
            }

            // ensure the file is in the temp directory.
            options.FileNameToImport = TempFileUtils.Name2Path(options.FileNameToImport);

            if (HttpContext.Current != null)
            {
                HttpContext.Current.Server.ScriptTimeout = ConstStage.JustinBrokerImportTimeOutInSeconds;
            }
            
            if (!options.SourceFileNameIncludingExtension.ToLower().EndsWith(".zip"))
            {
                if (options.LogDebugInfo)
                {
                    Tools.LogInfo($"srcFileName was {options.SourceFileNameIncludingExtension} which is not a '.zip' file so didn't decompress.");
                }

                result = ImportXml(principal, options);
                return result;
            }

            if (options.LogDebugInfo)
            {
                Tools.LogInfo($"srcFileName was {options.SourceFileNameIncludingExtension} which is a '.zip' file so decompressing.");
            }

            var targetDirectoryFullName = Path.Combine(ConstApp.TempFolder, Path.GetFileNameWithoutExtension(options.SourceFileNameIncludingExtension));
            var fileOnServerInfo = new FileInfo(options.FileNameToImport);
            var targetDirectoryInfo = new DirectoryInfo(targetDirectoryFullName);

            if (options.LogDebugInfo)
            {
                Tools.LogInfo($"decompressing with fileOnServerInfo:'{fileOnServerInfo.FullName}' and targetDirectoryInfo:'{targetDirectoryInfo.FullName}'");
            }

            CompressionHelper.DecompressIOCompressedFile(fileOnServerInfo, targetDirectoryInfo, deleteExisting: true);

            var allDecompressedFiles = Directory.EnumerateFiles(targetDirectoryFullName, "*").Select(s => new FileInfo(s));

            if (options.LogDebugInfo)
            {
                Tools.LogInfo("decompression contained the following files: " 
                    + string.Join(", " + Environment.NewLine, allDecompressedFiles.Select(a => a.FullName)));
            }

            foreach (var fileFullname in Directory.EnumerateFiles(targetDirectoryFullName, "*.xml", SearchOption.AllDirectories))
            {
                try
                {
                    var oneResult = ImportXml(principal, options, allDecompressedFiles, fileFullname);

                    result.ErrorMessages.AddRange(oneResult.ErrorMessages);
                    result.WarningMessages.AddRange(oneResult.WarningMessages);
                }
                catch (SqlException se)
                {
                    result.ErrorMessages.Add(se.Message);
                }
                catch (XmlException xe)
                {
                    result.ErrorMessages.Add(xe.Message);
                }
            }

            // tdsk: make sure that just prior to returning a result (anywhere in this method), an email gets sent if the email option isn't null.
            return result;
        }

        /// <summary>
        /// Exports the xml of the broker with the specified options.
        /// </summary>
        /// <param name="principal">The principal that is executing the request.</param>
        /// <param name="options">The options to use whilst exporting.</param>
        /// <returns>An export result including the name of the file where the xml is.</returns>
        public static BrokerExportResult ExportBrokerXml(AbstractUserPrincipal principal, BrokerExportOptions options)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if (options.ExportType == ReplicationExportType.Custom)
            {
                if (options.CustomOptions == null)
                {
                    throw new ArgumentException($"attempted to custom export with null CustomOptions.");
                }

                switch (options.SelectedDB)
                {
                    case "RATE_SHEET":
                    case "MAIN_DB":
                        break;
                    default:
                        throw new ArgumentException($"Unexpected selected db: '{options.SelectedDB}'for custom export.");
                }
            }

            var result = new BrokerExportResult();

            if (!ServerIsOkForExport())
            {
                string msg = "Please do not export production data from secure url but from loauth url.";
                result.ErrorMessages.Add(msg);
                return result;
            }

            // tdsk: if the user can't export we may wish to notify ourselves that an attempt was made without access.
            if (!CanExport(principal))
            {
                result.ErrorMessages.Add("You may only export brokers as a LoAdmin user or as a system user, or with special permission.");
                return result;
            }

            if (HttpContext.Current != null)
            {
                HttpContext.Current.Server.ScriptTimeout = ConstStage.JustinBrokerExportTimeOutInSeconds;
            }

            IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer = new List<FileDBEntryInfoForExport>();

            if (options.ExportType == ReplicationExportType.Broker)
            {
                result = GetMainXmlFile_BySp(options, out fileDBKeysForDirectTransfer);
            }
            else if (options.ExportType == ReplicationExportType.Custom)
            { 
                List<string> errorList = new List<string>();
                List<string> starterTableNmList = new List<string>();
                List<string> starterTableEntryList = new List<string>();
                List<string> starterConnBrokerIdCustomerCodeComboList = new List<string>();
                DataSrc starterDataSrc;
                result = GetInputEntries(principal, options, ref errorList, out starterDataSrc, out starterConnBrokerIdCustomerCodeComboList, out starterTableNmList, out starterTableEntryList);

                if (errorList.Any())
                {
                    result.ErrorMessages.AddRange(errorList);
                    return result;
                }

                result = GetEntryXmlFile_BySp(
                    options,
                    out fileDBKeysForDirectTransfer,
                    starterConnBrokerIdCustomerCodeComboList,
                    options.IncludeFileDbEntries,
                    starterDataSrc,
                    starterTableNmList,
                    starterTableEntryList,
                    needExportRelatedTable: options.CustomOptions.IncludeRelatedTables);

                if (result.TempFileName == null)
                {
                    result.ErrorMessages.Add("File not created.");
                    return result;
                }
            }

            if (!result.Succeeded)
            {
                return result;
            }

            if (!options.IncludeFileDbEntries)
            {
                fileDBKeysForDirectTransfer = new List<FileDBEntryInfoForExport>();
            }

            bool doCompression = options.Compress;

            if (fileDBKeysForDirectTransfer.Any())
            {
                doCompression = true;
            }

            result.ContentType = MIMEContentType.XML;
            if (doCompression)
            {
                Compress(options, result, fileDBKeysForDirectTransfer);
            }

            if (result.Succeeded)
            {
                // ensures this is just the relative filename.
                result.TempFileName = Path.GetFileName(result.TempFileName);

                if (principal is BrokerUserPrincipal)
                {
                    TempFileAccessManager.AssertThatCurrentUserCreatedFile(principal, result.TempFileName);
                }
            }

            return result;
        }

        /// <summary>
        /// Use this to combine system default configure release workflow from current database and current imported xml <para></para>
        /// so that the system workflow can be compatible to both imported broker's workflow and also current database broker's workflow.
        /// </summary>
        /// <param name="principal">The principal that is requresting to import the system system config.</param>
        /// <param name="errorList">A list record errors.</param>
        /// <param name="filepath">The file path of the imported xml.</param>
        public static void ImportSystemSystemConfigFromXml(AbstractUserPrincipal principal, List<string> errorList, string filepath)
        {
            if ((principal == null) || !(principal is InternalUserPrincipal || principal is SystemUserPrincipal))
            {
                throw new AccessDenied("You must be a loadmin or system user to be able to import system config.");
            }

            XElement root = XElement.Load(filepath);
            IEnumerable<XElement> systemGroups = from curGroup in root.Elements("Group") where curGroup.Attribute("connBrokerIdCustomCodeCombo").Value.StartsWith("11111111-1111-1111-1111-111111111111") select curGroup;
            XElement releaseSystemConfigElement = null;

            // Since we only need the latest default config_released entry, when encountering the first one, we will stop searching.
            foreach (XElement systemGroup in systemGroups)
            {
                releaseSystemConfigElement = (from table in systemGroup.Elements("table") where table.Attribute("n").Value == "CONFIG_RELEASED" select table).First();
                if (releaseSystemConfigElement != null)
                {
                    break;
                }
            }

            Guid systemOrganizationId = new Guid("11111111-1111-1111-1111-111111111111");
            string combinedSysConfigStr;
            DateTime versionDate;

            bool systemConfigWasPresentInBothXmlAndDb = ShareImportExport.CombineConfigReleaseFromXmlAndDb(out combinedSysConfigStr, out versionDate, errorList, systemOrganizationId, systemOrganizationId, releaseSystemConfigElement);
            if (!systemConfigWasPresentInBothXmlAndDb)
            {
                return;
            }

            // tdsk: consider adding validation as well prior to db save, though that seems against the grain of this tool generally, and would validate all brokers xml.
            SystemConfig config = new SystemConfig(combinedSysConfigStr);
            byte[] bytesOfCompiledCode = ExecutingEngineCompiler.Compile(config, versionDate);
            var modifyingUserId = principal.UserId;
            var parameters = new List<SqlParameter>(5);
            parameters.Add(new SqlParameter("ConfigurationXmlContent", combinedSysConfigStr));
            parameters.Add(new SqlParameter("OrgId", systemOrganizationId));
            parameters.Add(new SqlParameter("ModifyingUserId", modifyingUserId));
            SqlParameter hexPara = new SqlParameter("CompiledConfiguration", bytesOfCompiledCode);
            parameters.Add(hexPara);
            parameters.Add(new SqlParameter("ReleaseDate", versionDate));
            StoredProcedureHelper.ExecuteNonQuery(systemOrganizationId, "CONFIG_SaveRelease", 0, parameters);
        }

        /// <summary>
        /// A function for getting compiled code of configure release xml content.
        /// </summary>
        /// <param name="systemConfigXml">The original configure release xml content.</param>
        /// <param name="versionDate">The date of version of the xml file.</param>
        /// <returns>The compiled hex value of xml content.</returns>
        public static string GetCompiledCodeHexStr(string systemConfigXml, DateTime versionDate)
        {
            SystemConfig config = new SystemConfig(systemConfigXml);
            byte[] compiledBytes = ExecutingEngineCompiler.Compile(config, versionDate);
            string hexVal = BitConverter.ToString(compiledBytes).Replace("-", string.Empty);
            return "0x" + hexVal;
        }

        /// <summary>
        /// Determines if the user can export.
        /// </summary>
        /// <param name="principal">The principal in question.</param>
        /// <returns>True iff the user is internal, system, or in stage config.</returns>
        private static bool CanExport(AbstractUserPrincipal principal)
        {
            if (principal is InternalUserPrincipal)
            {
                InternalUserPermissions permissions = new InternalUserPermissions();
                permissions.Opts = principal.Permissions;
                if (!permissions.Can(E_InternalUserPermissions.ExportBroker))
                {
                    return false;
                }
            }

            return CanX(principal, ConstStage.LoginNamesRequiredOfBUsersForBrokerReplicateServiceMethods, ConstStage.IpsRequiredOfBUsersForBrokerReplicateServiceMethods);
        }

        /// <summary>
        /// Determines if the user can import.
        /// </summary>
        /// <param name="principal">The principal in question.</param>
        /// <returns>True iff the user is internal, system, or in stage config.</returns>
        private static bool CanImport(AbstractUserPrincipal principal)
        {
            if (principal is InternalUserPrincipal)
            {
                InternalUserPermissions permissions = new InternalUserPermissions();
                permissions.Opts = principal.Permissions;
                if (!permissions.Can(E_InternalUserPermissions.ImportBroker))
                {
                    return false;
                }
            }

            return CanX(principal, ConstStage.LoginNamesRequiredOfBUsersForBrokerReplicateServiceMethods, ConstStage.IpsRequiredOfBUsersForBrokerReplicateServiceMethods)
                && ConstAppDavid.CurrentServerLocation != ServerLocation.Production;
        }

        /// <summary>
        /// Tells if the user has permission to do the operation given their type, login name, and ip.
        /// </summary>
        /// <param name="principal">The principal we're authorizing.</param>
        /// <param name="loginNamesRequiredOfBUsers">The login names required of B users to do the operation.</param>
        /// <param name="requiredIpAddressesOfBUsers">The ips required of B users to do the operation.</param>
        /// <returns>Whether or not the user is authorized to do the thing.</returns>
        private static bool CanX(AbstractUserPrincipal principal, IEnumerable<string> loginNamesRequiredOfBUsers, IEnumerable<string> requiredIpAddressesOfBUsers)
        {
            if (loginNamesRequiredOfBUsers == null)
            {
                throw new ArgumentNullException(nameof(loginNamesRequiredOfBUsers));
            }

            if (requiredIpAddressesOfBUsers == null)
            {
                throw new ArgumentNullException(nameof(requiredIpAddressesOfBUsers));
            }

            if (!ServerIsOkForExport())
            {
                return false;
            }

            if (principal == null)
            {
                return false;
            }

            if (principal is InternalUserPrincipal || principal is SystemUserPrincipal)
            {
                return true;
            }

            if (HttpContext.Current == null)
            {
                return false;
            }

            if (principal is BrokerUserPrincipal
               && principal.ApplicationType == E_ApplicationT.LendersOffice
               && loginNamesRequiredOfBUsers.Contains(principal.LoginNm, StringComparer.OrdinalIgnoreCase)
               && requiredIpAddressesOfBUsers.Contains(RequestHelper.ClientIP))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// If the server is production it's not ok to export, unless it's loauth.  The reasons are: <para></para>
        /// 1) Whitelist is up to date on loauth, but not on production servers.<para></para>
        /// 2) It won't tie up production server UI threads when on Loauth.<para></para>
        /// </summary>
        /// <returns>Whether or not it's ok to export from the server.</returns>
        private static bool ServerIsOkForExport()
        {
            var stage = ConstAppDavid.CurrentServerLocation;
            if (stage != ServerLocation.Production)
            {
                return true;
            }

            var requestIsLoauth = Tools.RequestIsOnLoauth() ?? false;
            if (requestIsLoauth)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// The actual one that do the writing of exported file for broker and its related tables.
        /// </summary>
        /// <param name="options">The options used during the export.</param>
        /// <param name="fileDBKeysForDirectTransfer">The file db entries to directly add to the exported zip.</param>
        /// <returns>The file address of broker export.</returns>
        private static BrokerExportResult GetMainXmlFile_BySp(BrokerExportOptions options, out IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer)
        {
            var result = new BrokerExportResult();
            string mainDirectory = (HttpContext.Current != null) ? Tools.GetServerMapPath(@"~/") : Directory.GetCurrentDirectory();
            string whitelistPath = SenderWhiteListFullName;

            List<string> errorList = new List<string>();
            List<string> warningList = new List<string>();

            int justinOneSqlQueryTimeOutInSeconds = ConstStage.JustinOneSqlQueryTimeOutInSeconds;

            string userOption = options.SpecificExportType;
            string branchName = options.BranchNameToGetUsers;
            bool includeFiledb = options.IncludeFileDbEntries;
            if (userOption == "LOAN_TEMPLATES")
            {
                fileDBKeysForDirectTransfer = new List<FileDBEntryInfoForExport>();
                result.TempFileName = TableReplicateTool.ExportLoanTemplatesByBrokerId(
                    ref errorList,
                    ref warningList,
                    out fileDBKeysForDirectTransfer,
                    whitelistPath,
                    mainDirectory,
                    options.BrokerId.ToString(),
                    includeFiledb,
                    userOption,
                    options.LogDebugInfo);
            }
            else
            {
                result.TempFileName = TableReplicateTool.ExportAllBrokerRelatedTablesBySp(
                    ref errorList,
                    ref warningList,
                    out fileDBKeysForDirectTransfer,
                    whitelistPath,
                    mainDirectory,
                    options.BrokerId.ToString(),
                    options.IncludeFileDbEntries,
                    userOption,
                    branchName,
                    justinOneSqlQueryTimeOutInSeconds,
                    options.LogDebugInfo);
            }

            result.ErrorMessages.AddRange(errorList);
            result.WarningMessages.AddRange(warningList);

            return result;
        }

        /// <summary>
        /// This function generates an xml file that contains the exported entries. <para/>
        /// Related tables are tables that the tables foreign key to, or that we have determined to be related.
        /// </summary>
        /// <param name="options">The options used in the export.</param>
        /// <param name="fileDbKeyEntriesForDirectTransfer">Additional filedb entries for direct export.</param>
        /// <param name="starterConnBrokerIdCustomerCodeComboList">A list of connection information to find the data source.</param>
        /// <param name="includeFiledb">A boolean value decide whether to include file db entries.</param>
        /// <param name="starterDataSrc">A data source for the entries. All entries must belong to the same data source.</param>
        /// <param name="starterTableNmList">The starter table name list.</param>
        /// <param name="starterTableEntryList">The starter table entry list.</param>
        /// <param name="needExportRelatedTable">True iff exporting related tables.</param>
        /// <returns>Return the result including any errors or warning as well as the path to the xml file that contains the exported entries.</returns>
        private static BrokerExportResult GetEntryXmlFile_BySp(
            BrokerExportOptions options,
            out IEnumerable<FileDBEntryInfoForExport> fileDbKeyEntriesForDirectTransfer,
            List<string> starterConnBrokerIdCustomerCodeComboList,
            bool includeFiledb,
            DataSrc starterDataSrc,
            List<string> starterTableNmList,
            List<string> starterTableEntryList,
            bool needExportRelatedTable)
        {
            var result = new BrokerExportResult();
            string mainDirectory = Tools.GetServerMapPath(@"~/");
            string whitelistPath = BrokerReplicator.SenderWhiteListFullName;

            List<string> errorList = new List<string>();
            List<string> warningList = new List<string>();
            fileDbKeyEntriesForDirectTransfer = null;

            List<List<string>> missingConnBrokerIdCustomerCodeComboListList = new List<List<string>>();
            HashSet<string> allExportedTableNmSet = new HashSet<string>();
            List<List<string>> allMissingEntryListList = GetRelatedTableEntries(
                ref errorList,
                ref missingConnBrokerIdCustomerCodeComboListList,
                out allExportedTableNmSet,
                starterDataSrc,
                starterConnBrokerIdCustomerCodeComboList,
                starterTableNmList,
                starterTableEntryList,
                needExportRelatedTable,
                options.LogDebugInfo);

            bool needUpdateWhiteList;
            bool needReview;
            Dictionary<string, WhitelistData> whitelistDictionary;
            Dictionary<string, string> reconstructMap;

            if (starterTableEntryList.Count == 0)
            {
                var msg = "No entries found at this broker.";
                result.ErrorMessages.Add(msg);
                return result;
            }

            result.TempFileName = TableReplicateTool.ExportTablesBySpAndEntryList(
                ref errorList,
                ref warningList,
                out whitelistDictionary,
                out reconstructMap,
                out needReview,
                out needUpdateWhiteList,
                out fileDbKeyEntriesForDirectTransfer,
                whitelistPath,
                mainDirectory,
                options.SelectedDB,
                includeFiledb,
                allExportedTableNmSet,
                isLoanOverwrite: false,
                listOfListsOfBrokerConnectionInfoStrings: missingConnBrokerIdCustomerCodeComboListList,
                listOfListsOfTableRecordStrings: allMissingEntryListList,
                needDebugInfo: options.LogDebugInfo);

            if (errorList.Any())
            {
                result.ErrorMessages.AddRange(errorList);
            }

            if (warningList.Any())
            {
                result.WarningMessages.AddRange(warningList);
            }

            return result;
        }

        /// <summary>
        /// Search and generated a complete list of entries by searching the related tables of the starter table list.
        /// </summary>
        /// <param name="errorList">Record errors into the list.</param>
        /// <param name="relatedConnBrokerIdCustomerCodeComboListList">A list of connection information to find the data source. This is one of the result you would want for related entries.</param>
        /// <param name="allTableNmSet">Record every unique table name in the entries.</param>
        /// <param name="starterDataSrc">A data source for the entries. All entries must belong to the same data source.</param>
        /// <param name="starterConnBrokerIdCustomerCodeComboList">A list of connection information to find the data source, a list that corresponding to each entry in the start entry list.</param>
        /// <param name="starterTableNmList">The names of a list of tables to start with.</param>
        /// <param name="starterTableEntryList">Entries correspond to the table name list.</param>
        /// <param name="needExportRelatedTable">If yes, it will search related tables. If no, it will only export the starter tables.</param>
        /// <param name="showDebugInfo">A boolean value decide whether to print out progress message or not.</param>
        /// <returns>Return the related table entries list.</returns>
        private static List<List<string>> GetRelatedTableEntries(ref List<string> errorList, ref List<List<string>> relatedConnBrokerIdCustomerCodeComboListList, out HashSet<string> allTableNmSet, DataSrc starterDataSrc, List<string> starterConnBrokerIdCustomerCodeComboList, List<string> starterTableNmList, List<string> starterTableEntryList, bool needExportRelatedTable, bool showDebugInfo)
        {
            allTableNmSet = new HashSet<string>();
            List<List<string>> missingTableNmListList = new List<List<string>>();
            List<List<string>> missingTableEntryListList = new List<List<string>>();
            if (!needExportRelatedTable)
            {
                string currConnInfo = string.Empty;
                int currConnInfoInd = 0;
                int starterConnCnt = starterConnBrokerIdCustomerCodeComboList.Count();
                for (int i = 0; i < starterConnCnt; i++)
                {
                    string tableNm = starterTableEntryList[i].Substring(0, starterTableEntryList[i].IndexOf('|'));
                    if (!allTableNmSet.Contains(tableNm))
                    {
                        allTableNmSet.Add(tableNm);
                    }

                    if (currConnInfo != starterConnBrokerIdCustomerCodeComboList[i])
                    {
                        currConnInfo = starterConnBrokerIdCustomerCodeComboList[i];
                        relatedConnBrokerIdCustomerCodeComboListList.Add(new List<string>());
                        missingTableEntryListList.Add(new List<string>());
                        currConnInfoInd = relatedConnBrokerIdCustomerCodeComboListList.Count() - 1;
                    }

                    relatedConnBrokerIdCustomerCodeComboListList[currConnInfoInd].Add(currConnInfo);
                    missingTableEntryListList[currConnInfoInd].Add(starterTableEntryList[i]);
                }

                return missingTableEntryListList;
            }

            Dictionary<string, WhitelistData> whitelistDictionary = null;
            Dictionary<string, string> m_reconstructMap = null;
            if (errorList.Any() == false)
            {
                string whitelistPath = BrokerReplicator.SenderWhiteListFullName;
                ShareImportExport.ReadCsvWhiteList(ref errorList, out m_reconstructMap, out whitelistDictionary, whitelistPath, false);
            }

            if (errorList.Any() == false)
            {
                List<List<string>> fkeyRelationshipTable = TableReplicateTool.GetFkRelationshipTable(starterDataSrc);
                Dictionary<string, List<string>> primaryKeyDictionary = ShareImportExport.GetPrimaryKeyDictionary(starterDataSrc);
                Dictionary<string, string> defaultForeignKeyValDictionary = ShareImportExport.GetDefaultForeignKeyValDictionary();
                Dictionary<string, List<string>> foreignKeyDictionary = TableReplicateTool.GetForeignKeyDictionary(fkeyRelationshipTable);
                Dictionary<string, List<string>> fkeyRealNmDictionary = TableReplicateTool.GetFkRealNmDictionary(fkeyRelationshipTable);
                Dictionary<string, List<string>> tableMappingKeyList = TableReplicateTool.GetTablePairMappingKeyPairList(fkeyRelationshipTable);
                ////Dictionary<string, string> identityDictionary = ShareImportExport.HardcodeIdentityTableKeyDictionary();
                ////HashSet<string> findNextWaiveSet = TableReplicateTool.HardCodeWaiveFindNextTableSet();
                ////TableReplicateTool.AddProtectFieldOfWhitelistIntoWaiveFindNextSet(ref findNextWaiveSet, whitelistDictionary);
                string missingTableEntryFilepath = Tools.GetServerMapPath(@"~/") + "missTableEntry.txt";
                HashSet<string> allSentFindSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
                Dictionary<string, int> tableRankMapping = TableReplicateTool.HardCodeRankingForInsertionSeq();

                int size = starterTableEntryList.Count();
                for (int i = 0; i < size; i++)
                {
                    if (allSentFindSet.Contains(starterTableEntryList[i]))
                    {
                        continue;
                    }
                    else
                    {
                        allSentFindSet.Add(starterTableEntryList[i]);
                    }

                    List<string> missingTableNmList = new List<string>();
                    List<string> missingTableEntryList = new List<string>();
                    List<string> missingConnBrokerIdCustomerCodeComboList = new List<string>();
                    List<string> oneStarterTableNm = new List<string>();
                    List<string> oneStarterTableEntry = new List<string>();
                    List<string> oneConnInfoCombo = new List<string>();

                    oneStarterTableNm.Add(starterTableNmList[i]);
                    oneStarterTableEntry.Add(starterTableEntryList[i]);
                    oneConnInfoCombo.Add(starterConnBrokerIdCustomerCodeComboList[i]);

                    if (showDebugInfo)
                    {
                        Tools.LogInfo("Start to find related table for entry[" + i + "]");
                    }

                    TableReplicateTool.FindAllRelatedTableEntry_NoService(
                        ref errorList,
                        ref missingConnBrokerIdCustomerCodeComboList,
                        ref missingTableNmList,
                        ref missingTableEntryList,
                        ref allSentFindSet,
                        starterDataSrc,
                        oneConnInfoCombo,
                        oneStarterTableNm,
                        oneStarterTableEntry,
                        primaryKeyDictionary,
                        foreignKeyDictionary,
                        fkeyRealNmDictionary,
                        tableMappingKeyList,
                        whitelistDictionary,
                        missingTableEntryFilepath,
                        needExportRelatedTable);

                    List<int> orderedIndex = TableReplicateTool.GetInsertSeqLevelMapping_EasyUse(tableRankMapping, missingTableNmList);
                    List<string> sortedTableNmList = new List<string>();
                    List<string> srotedTableEntryList = new List<string>();
                    for (int ind = 0; ind < orderedIndex.Count(); ind++)
                    {
                        sortedTableNmList.Add(missingTableNmList[orderedIndex[ind]]);
                        srotedTableEntryList.Add(missingTableEntryList[orderedIndex[ind]]);
                    }

                    missingTableNmListList.Add(sortedTableNmList);
                    missingTableEntryListList.Add(srotedTableEntryList);
                    relatedConnBrokerIdCustomerCodeComboListList.Add(missingConnBrokerIdCustomerCodeComboList);
                }
            }

            foreach (List<string> missingTableNmList in missingTableNmListList)
            {
                foreach (string tableNm in missingTableNmList)
                {
                    if (!allTableNmSet.Contains(tableNm))
                    {
                        allTableNmSet.Add(tableNm);
                    }
                }
            }

            return missingTableEntryListList;
        }

        /// <summary>
        /// Generate a list of table names, and full entries (all primary keys) and associated broker id and customer code pairs.
        /// If UseOnlyThisBrokersDb is checked, it skips any entries from other brokers.
        /// tdsk: 
        ///     - Instead of returning separate lists, return one list of objects that encapsulates all the things.
        ///     - Instead of storing things as pipe separated strings, return objects.
        /// </summary>
        /// <param name="principal">The user attempting to get the input entries.</param>
        /// <param name="options">The options specified for the export.</param>
        /// <param name="errorList">Record the list of errors.</param>
        /// <param name="starterDataSrc">A data source for the entries. All entries must belong to the same data source.</param>
        /// <param name="starterConnBrokerIdCustomerCodeComboList">A list of connection information to find the data source.</param>
        /// <param name="starterTableNmList">A list of table names to start with the export.</param>
        /// <param name="starterTableEntryList">A list of entries correspond to the table name list.</param>
        /// <returns>The result of exporting the input entries.</returns>
        private static BrokerExportResult GetInputEntries(AbstractUserPrincipal principal, BrokerExportOptions options, ref List<string> errorList, out DataSrc starterDataSrc, out List<string> starterConnBrokerIdCustomerCodeComboList, out List<string> starterTableNmList, out List<string> starterTableEntryList)
        {
            var result = new BrokerExportResult();

            starterDataSrc = DataSrc.LOShare;
            starterTableNmList = new List<string>();
            starterTableEntryList = new List<string>();
            starterConnBrokerIdCustomerCodeComboList = new List<string>();

            if (options.CustomOptions.Text.Length == 0)
            {
                string errorMsg = "Please enter your table entry to export.";
                result.ErrorMessages.Add(errorMsg);
                return result;
            }

            // unfiltered here means the list could have entries from all brokers.
            var unfilteredStarterTableNmList = new List<string>();
            var unfilteredStarterTableEntryList = new List<string>();

            DataSrc dataSource = options.SelectedDB == "RATE_SHEET" ? DataSrc.RateSheet : DataSrc.LOShare;
            starterDataSrc = dataSource;
            Dictionary<string, List<string>> primaryKeyColumnNamesByTableName = ShareImportExport.GetPrimaryKeyDictionary(dataSource);
            Guid? brokerIdToFilterIn = options.CustomOptions.UseOnlyThisBrokersDb ? options.BrokerId : (Guid?)null;

            // By entry we mean an inputted line separated by semicolons.  It could be one record or multiple depending on how many results are associated with it.
            List<string> inputEntryList = options.CustomOptions.Text.Replace("\r", string.Empty).Replace("\n", string.Empty).Trim('\t').TrimEnd(';').ToUpper().Split(';').ToList();
            HashSet<string> inputEntrySet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            var databaseConnectionInfos = new List<DbConnectionInfo>();
            foreach (string inputEntry in inputEntryList)
            {
                if (inputEntrySet.Contains(inputEntry))
                {
                    continue;
                }

                inputEntrySet.Add(inputEntry);
                string formattedEntry = inputEntry.Trim('\t');
                formattedEntry = formattedEntry.Replace('\t', '|');
                ShareImportExport.TryGetConnInfoAndStandardEntryByOneInputEntryAndDataSrc(
                    ref errorList,
                    ref databaseConnectionInfos,
                    ref unfilteredStarterTableEntryList,
                    ref unfilteredStarterTableNmList,
                    dataSource,
                    primaryKeyColumnNamesByTableName,
                    formattedEntry,
                    brokerIdToFilterIn);
            }

            if (errorList.Any())
            {
                result.ErrorMessages.AddRange(errorList);
                return result;
            }

            int numEntries = unfilteredStarterTableEntryList.Count;
            if (dataSource != DataSrc.LOShare)
            {
                for (int i = 0; i < numEntries; i++)
                {
                    starterConnBrokerIdCustomerCodeComboList.Add("|");
                }

                starterTableEntryList = unfilteredStarterTableEntryList;
                starterTableNmList = unfilteredStarterTableNmList;
                return result;
            }

            // Get the associated broker id and customer code with each record. Filter out other brokers if the user requested that.
            starterTableEntryList = new List<string>(unfilteredStarterTableEntryList.Count);
            starterTableNmList = new List<string>(unfilteredStarterTableNmList.Count);
            for (int i = 0; i < numEntries; i++)
            {
                Guid connBrokerId;
                string customerCode;
                string entry = unfilteredStarterTableEntryList[i];
                ShareImportExport.TryGetBrokerIdAndCustomerCodeByEntryAndDbConnInfo(
                    ref errorList,
                    out connBrokerId,
                    out customerCode,
                    databaseConnectionInfos[i],
                    entry);

                if (brokerIdToFilterIn.HasValue)
                {
                    if (connBrokerId != brokerIdToFilterIn.Value)
                    {
                        continue;
                    }
                }

                starterTableEntryList.Add(entry);
                starterTableNmList.Add(unfilteredStarterTableNmList[i]);
                starterConnBrokerIdCustomerCodeComboList.Add(connBrokerId.ToString() + "|" + customerCode);
            }

            return result;
        }

        /// <summary>
        /// May compress the file.  The method of export is left up to the consumer. <para></para>
        /// If the user doesn't request to compress, but there are filedb entries for the export, it will still create a folder and compress it.
        /// </summary>
        /// <param name="options">The options used during the export.</param>
        /// <param name="result">The result of the export.  It may be affected if a compression takes place.</param>
        /// <param name="fileDBKeysForDirectTransfer">The files to be directly exported from filedb.  If there are any values, it will compress regardless of user preference.</param>
        private static void Compress(BrokerExportOptions options, BrokerExportResult result, IEnumerable<FileDBEntryInfoForExport> fileDBKeysForDirectTransfer)
        {
            var fi = new FileInfo(result.TempFileName);
            var targetFullFileName = Path.Combine(ConstApp.TempFolder, GetMeaningfulFileName(options) + ".zip");
            var targetFileInfo = new FileInfo(targetFullFileName);
            
            var fileDBEntriesThatExist = new List<FileDBEntryInfo>();
            foreach (var fileInfo in fileDBKeysForDirectTransfer)
            {
                var entry = fileInfo.FileDBEntry;
                if (!FileDBTools.DoesFileExist(entry.FileDBType, entry.Key))
                {
                    result.WarningMessages.Add($"No file existing for type: {entry.FileDBType} key: {entry.Key}");
                }
                else
                {
                    fileDBEntriesThatExist.Add(entry);
                }
            }

            CompressionHelper.CompressWithIOCompression(
                fileDBEntriesThatExist,
                new FileInfo[] { fi },
                targetFileInfo,
                deleteFileIfExists: true);

            result.TempFileName = targetFullFileName;
            result.ContentType = MIMEContentType.ZIP;
            result.IsCompressed = true;
        }

        /// <summary>
        /// Function to import the xml from a file on disk.
        /// </summary>
        /// <param name="principal">The principal we are importing for.</param>
        /// <param name="options">The options to be used while importing the xml.</param>
        /// <param name="fileInfos">The file infos that may or may not be imported depending on the imported xml file.  Can be null.</param>
        /// <param name="fullFileNameFromZip">The full file name for this import that came from the extracted zipped folder, or null if not applicable.</param>
        /// <returns>The reasons the import failed.</returns>
        private static BrokerImportResult ImportXml(AbstractUserPrincipal principal, BrokerImportOptions options, IEnumerable<FileInfo> fileInfos = null, string fullFileNameFromZip = null)
        {
            if (options == null)
            {
                throw new ArgumentNullException(nameof(options));
            }

            if (options.CombineConfigReleased)
            {
                throw new NotSupportedException("Combining with system config is no longer supported.  Import system config directly via custom export.");
            }

            var result = new BrokerImportResult();

            // tdsk: don't rely on url to determine the server location, we care the db.
            var stageSiteSource = ConstAppDavid.CurrentServerLocation;
            if (stageSiteSource == ServerLocation.Production)
            {
                result.ErrorMessages.Add("Error: You are not allowed to import brokers into production servers!");
                return result;
            }

            string whitelistAddr = BrokerReplicator.SenderWhiteListFullName;
            if (!FileOperationHelper.Exists(whitelistAddr))
            {
                result.ErrorMessages.Add("Cannot find white list at " + whitelistAddr.Replace("\\", "\\\\"));
                return result;
            }

            var errorMessages = new List<string>();
            var warningMessages = new List<string>();
            Dictionary<string, string> reconstructMap = null;
            Dictionary<string, WhitelistData> whitelistDictionary;
            ShareImportExport.ReadCsvWhiteList(ref errorMessages, out reconstructMap, out whitelistDictionary, whitelistAddr, false);

            int justinOneSqlQueryTimeOutInSeconds = ConstStage.JustinOneSqlQueryTimeOutInSeconds;
            TableReplicateTool.ImportOneXmlFileBySp(
                ref errorMessages,
                ref warningMessages,
                fullFileNameFromZip ?? options.FileNameToImport,
                whitelistDictionary,
                fileInfos,
                null,
                string.Empty,
                options.CombineConfigReleased,
                justinOneSqlQueryTimeOutInSeconds,
                options.LogDebugInfo,
                options.DefaultExternalBrokerField);

            result.WarningMessages.AddRange(warningMessages);
            result.ErrorMessages.AddRange(errorMessages);

            if (options.CombineConfigReleased)
            {
                if (errorMessages.Any())
                {
                    result.WarningMessages.Add("skipping the import of system config because there were errors.");
                }
                else
                {
                    BrokerReplicator.ImportSystemSystemConfigFromXml(principal, errorMessages, options.FileNameToImport);
                    result.WarningMessages.AddRange(errorMessages);
                }
            }

            return result;
        }

        /// <summary>
        /// Gets a more meaningful name for the file to be exported.
        /// </summary>
        /// <param name="options">The options used while exporting the broker.</param>
        /// <returns>A more meaningful string for the filename than a random string.</returns>
        private static string GetMeaningfulFileName(BrokerExportOptions options)
        {
            string prefix = null;
            switch (options.ExportType)
            {
                case ReplicationExportType.Broker:
                    prefix = "brx";
                    break;
                case ReplicationExportType.Custom:
                    prefix = "cux";
                    break;
                default:
                    throw new UnhandledEnumException(options.ExportType);
            }

            var nameParts = new string[]
            {
                prefix,
                GetStageName(),
                GetWhitelistName(),
                GetBrokerName(options),
                GetNameFromOptions(options),
                GetDateAsFileName()
            };

            return string.Join("_", nameParts);
        }

        /// <summary>
        /// Gets the current date in string format for files and folders.
        /// </summary>
        /// <returns>The current date in string format for files and folders.</returns>
        private static string GetDateAsFileName()
        {
            return DateTime.Now.ToString("yy-MM-dd-HH-mm");
        }

        /// <summary>
        /// Gets a name from some of the options for the particular export.
        /// </summary>
        /// <param name="options">The options used while exporting the broker.</param>
        /// <returns>A name from some of the options for the particular export.</returns>
        private static string GetNameFromOptions(BrokerExportOptions options)
        {
            switch (options.ExportType)
            {
                case ReplicationExportType.Broker:
                    return GetNameFromBrokerRadioOptions(options) + "-" + GetNameFromIncludeFile(options);
                case ReplicationExportType.Custom:
                    return GetDBName(options) + "-" + GetNameFromIncludeFile(options);
                default:
                    throw new UnhandledEnumException(options.ExportType);
            }
        }

        /// <summary>
        /// Gets a name for the db we are using when using custom export.
        /// </summary>
        /// <param name="options">The options used while exporting the broker.</param>
        /// <returns>A name for the db we are using when using custom export.</returns>
        private static string GetDBName(BrokerExportOptions options)
        {
            switch (options.SelectedDB)
            {
                case "MAIN_DB":
                    return "main";
                case "RATE_SHEET":
                    return "rs";
                default:
                    throw new ArgumentException("Did not handle options selected db of: " + options.SelectedDB, nameof(options.SelectedDB));
            }
        }

        /// <summary>
        /// Gets a name for whether or not we are including files in the custom export.
        /// </summary>
        /// <param name="options">The options used while exporting the broker.</param>
        /// <returns>A name for whether or not we are including files in the custom export.</returns>
        private static string GetNameFromIncludeFile(BrokerExportOptions options)
        {
            return options.IncludeFileDbEntries ? "if" : "nf";
        }

        /// <summary>
        /// Gets a name for the radio options associated with the broker export.
        /// </summary>
        /// <param name="options">The options used while exporting the broker.</param>
        /// <returns>A name for the radio options associated with the broker export.</returns>
        private static string GetNameFromBrokerRadioOptions(BrokerExportOptions options)
        {
            switch (options.SpecificExportType)
            {
                case "MAIN_DB_PRICING":
                    return "pricing";
                case "NO_USER":
                    return "nousr";
                case "ALL_BRANCH_USERS":
                    return "allusrs";
                case "LOAN_TEMPLATES":
                    return "lts";
                case "SYS_TABLES":
                    return "sys";
                case "RATE_SHEET":
                    return "rs";
                case "ONE_BRANCH_USERS":
                    return "1br-usrs";
                default:
                    throw new ArgumentException(
                            "Did not handle specific export type of : " + options.SpecificExportType, 
                            nameof(options.SpecificExportType));
            }
        }

        /// <summary>
        /// Gets a name for the broker (the customer code), or none if no broker.
        /// </summary>
        /// <param name="options">The options used while exporting the broker.</param>
        /// <returns>A name for the broker.</returns>
        private static string GetBrokerName(BrokerExportOptions options)
        {
            if (options.BrokerId != Guid.Empty)
            {
                var broker = BrokerDB.RetrieveById(options.BrokerId);
                return broker.CustomerCode;
            }
            else
            {
                return "none";
            }
        }

        /// <summary>
        /// Gets a name associated with the current whitelist file.
        /// </summary>
        /// <returns>A name associated with the current whitelist file.</returns>
        private static string GetWhitelistName()
        {
            // tdsk: At somepoint might be nice to replace this with a version / checksum.
            var whiteListFile = new FileInfo(SenderWhiteListFullName);
            var lastUpdateTime = whiteListFile.LastWriteTime;
            return "wl" + lastUpdateTime.ToString("MM-dd-HH-mm");
        }

        /// <summary>
        /// Gets the name associated with the stage.
        /// </summary>
        /// <returns>The name associated with the stage.</returns>
        private static string GetStageName()
        {
            switch (ConstAppDavid.CurrentServerLocation)
            {
                case ServerLocation.Production:
                    return "loauth";
                case ServerLocation.Beta:
                    return "beta";
                case ServerLocation.Alpha:
                    return "alpha";
                case ServerLocation.Development:
                case ServerLocation.LocalHost:
                    return "dev";
                case ServerLocation.Demo:
                    return "demo";
                case ServerLocation.DevCopy:
                    return "devcopy";
                default:
                    throw new UnhandledEnumException(ConstAppDavid.CurrentServerLocation);
            }
        }

        /// <summary>
        /// A small collection of mime content type string for ease of export.
        /// </summary>
        internal static class MIMEContentType
        {
            /// <summary>
            /// Plain text.
            /// </summary>
            internal const string Text = "text/plain";

            /// <summary>
            /// Textual CSV.
            /// </summary>
            internal const string CSV = "text/csv";

            /// <summary>
            /// Textual XML.
            /// </summary>
            internal const string XML = "text/xml";

            /// <summary>
            /// Zipped or compressed file.
            /// </summary>
            internal const string ZIP = "application/zip, application/octet-stream";
        }
    }
}
