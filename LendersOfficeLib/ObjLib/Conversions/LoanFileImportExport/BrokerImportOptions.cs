﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using Drivers.Gateways;

    /// <summary>
    /// The options to be used while importing broker xml. <para></para>
    /// </summary>
    [Serializable]
    public class BrokerImportOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerImportOptions"/> class. <para></para>
        /// DON'T USE THIS - only use for serialization.
        /// </summary>
        public BrokerImportOptions()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerImportOptions"/> class. <para></para>
        /// Ensures that the file name we are importing actually exists on disk.
        /// </summary>
        /// <param name="fileNameToImport">The name of the file to import that should exist on disk.</param>
        public BrokerImportOptions(string fileNameToImport)
        {
            if (fileNameToImport == null)
            {
                throw new ArgumentNullException(nameof(fileNameToImport));
            }

            if (!FileOperationHelper.Exists(fileNameToImport))
            {
                throw new ArgumentException($"The file {fileNameToImport} does not exist.", nameof(fileNameToImport));
            }

            this.FileNameToImport = fileNameToImport;
        }

        /// <summary>
        /// Gets or sets the file name to import.  This should be in the temp folder so it can be cleaned up.<para></para>
        /// This should be set using the constructor.
        /// </summary>
        /// <value>The name of the file to import.</value>
        public string FileNameToImport { get; set; }

        /// <summary>
        /// Gets or sets the name of the file, including extension, from the client. <para></para>
        /// For UI, this is the name of the file on the client's end. <para/>
        /// In console app, this can be the same name as FileNameToImport.  File type is determined using this.
        /// </summary>
        /// <value>The name of the file, including the extension, from the client.  For console app it can match FileNameToImport. </value>
        public string SourceFileNameIncludingExtension { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we want to log extra debug info.
        /// </summary>
        /// <value>True iff we want to log extra debug information.</value>
        public bool LogDebugInfo { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to combine with the system config.  Should not be used currently. <para></para>
        /// </summary>
        /// <value>True iff we want to combine with the system config.</value>
        public bool CombineConfigReleased { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not we want to default away the fields that would otherwise cause the import to fail.<para></para>
        /// </summary>
        /// <value>True iff we want to default any fields that would otherwise cause the import to fail.</value>
        public bool DefaultExternalBrokerField { get; set; }

        /// <summary>
        /// Gets or sets the email to notify after the import finishes.
        /// </summary>
        /// <value>Null if we don't wish to notify anyone.  Otherwise it's the email to notify.</value>
        public string EmailToNotifyOnFinish { get; set; }
    }
}
