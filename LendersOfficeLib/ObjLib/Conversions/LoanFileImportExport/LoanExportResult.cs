﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A class encapsulating the result of a loan export.
    /// </summary>
    [Serializable]
    public class LoanExportResult
    {
        /// <summary>
        /// Gets or sets the name of the file that was created and sits in the temporary directory on the server.
        /// </summary>
        /// <value>The name of the file that was created and sits in the temporary directory on the server.</value>
        public string TempFileName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the user should consider updating the white list.<para></para>
        /// This is weaker than review, in that perhaps only a few columns may be missing here and there.
        /// </summary>
        /// <value>True iff the user should consider updating the whitelist.</value>
        public bool NeedToUpdateTheWhitelist { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not the user should consider reviewing the white list. <para></para>
        /// This is stronger than update, in that a whole table may be missing instead of just a column.
        /// </summary>
        /// <value>True iff the user should consider reviewing the white list.</value>
        public bool NeedToReviewTheWhitelist { get; set; }

        /// <summary>
        /// Gets or sets the list of errors that occurred during the export.
        /// </summary>
        /// <value>Empty means successful.</value>
        public List<string> Errors { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets the list of warnings that occurred during the export.
        /// </summary>
        /// <value>Empty means no warnings, but success only depends on errors.</value>
        public List<string> Warnings { get; set; } = new List<string>();
    }
}
