﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    /// <summary>
    /// The type of export, either broker or custom.
    /// </summary>
    public enum ReplicationExportType
    {
        /// <summary>
        /// For broker exports.
        /// </summary>
        Broker,

        /// <summary>
        /// For custom (user-specified table rows) export.
        /// </summary>
        Custom
    }
}
