﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using DataAccess;
    using Drivers.Gateways;    

    /// <summary>
    /// A class encapsulating the options for importing a loan.
    /// </summary>
    [Serializable]
    public class LoanImportOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanImportOptions"/> class. <para></para>
        /// DON'T USE THIS - only use for serialization.
        /// </summary>
        public LoanImportOptions()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanImportOptions"/> class. <para></para>
        /// Ensures that the file name we are importing actually exists on disk.
        /// </summary>
        /// <param name="fileNameToImport">The name of the file to import that should exist on disk.</param>
        public LoanImportOptions(string fileNameToImport)
        {
            if (fileNameToImport == null)
            {
                throw new ArgumentNullException(nameof(fileNameToImport));
            }

            if (!FileOperationHelper.Exists(TempFileUtils.Name2Path(fileNameToImport)))
            {
                throw new ArgumentException($"The file {fileNameToImport} does not exist.", nameof(fileNameToImport));
            }

            this.FileNameToImport = fileNameToImport;
        }

        /// <summary>
        /// Gets or sets the nullable id of the loan to overwrite. <para></para>
        /// If null, the tool will just create a blank loan and then overwrite that.
        /// </summary>
        /// <value>If null, the tool will just create a blank loan and then overwrite that.</value>
        public Guid? LoanIdToOverwrite { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether or not to default other broker fields. <para></para>
        /// True iff we want to default certain fields if they came from another broker e.g. rolodexid.
        /// </summary>
        /// <value>True iff we want to default certain fields if they came from another broker e.g. rolodexid.</value>
        public bool DefaultOtherBrokerFields { get; set; }

        /// <summary>
        /// Gets or sets the file name to import.
        /// </summary>
        /// <value>The name of the file that should reside in the temp file folder.</value>
        public string FileNameToImport { get; set; }
    }
}
