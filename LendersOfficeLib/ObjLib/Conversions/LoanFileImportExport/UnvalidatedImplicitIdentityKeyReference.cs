﻿namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    /// <summary>
    /// Represents the unvalidated relevant information for an implicit reference from one table to another.<para></para>
    /// The object can be validated by <see cref="ValidatedImplicitIdentityKeyReference"/>.<para></para>
    /// An example might be FEE_SERVICE_TEMPLATE FeeServiceRevision is xml that contains attributes and text elements that refer to the identity key of the Region table. <para></para>
    /// Another example is a conversation log db Category has a DefaultPermissionLevelId that refers to a main db CONVOLOG_PERMISSION_LEVEL id.
    /// </summary>
    public class UnvalidatedImplicitIdentityKeyReference
    {
        /// <summary>
        /// Gets or sets the string representing the data source containing the table that implicitly refers to an identity key.
        /// </summary>
        public string ReferencingDataSource { get; set; }

        /// <summary>
        /// Gets or sets the name of the table that implicitly refers to an identity key.
        /// </summary>
        public string ReferencingTableName { get; set; }

        /// <summary>
        /// Gets or sets the name of the column that implicitly refers an identity key or that contains data that implicitly refers to an identity key.
        /// </summary>
        public string ReferencingColumnName { get; set; }

        /// <summary>
        /// Gets or sets the string representing the data source containing the table of the identity key being implicitly referred to.
        /// </summary>
        public string IdentityKeyDataSource { get; set; }

        /// <summary>
        /// Gets or sets the name of the table containing the identity key that is being implicitly referred to.
        /// </summary>
        public string IdentityKeyTableName { get; set; }

        /// <summary>
        /// Gets or sets the name of the column of the identity key that is being implicitly referred to.
        /// </summary>
        public string IdentityKeyColumName { get; set; }

        /// <summary>
        /// Gets or sets the string representing the type of implicit reference type. <see cref="ImplicitReferenceType"/>.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the string xpath to the node of interest where the reference to the identity key is, if applicable.
        /// </summary>
        public string XPath { get; set; }
        
        /// <summary>
        /// Gets or sets the attribute name where the reference is stored, if applicable.
        /// </summary>
        public string AttributeName { get; set; }
    }
}
