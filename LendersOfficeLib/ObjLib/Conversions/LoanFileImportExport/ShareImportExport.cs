﻿//-----------------------------------------------------------------------
// <copyright file="ShareImportExport.cs" company="Meridianlink">
//     Copyright (c) Sprocket Enterprises. All rights reserved.
// </copyright>
// <author>Zijing Jia</author>
// <summary>Share toolkit for table import export.</summary>
//-----------------------------------------------------------------------

namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using Adapter;
    using DataAccess;
    using Drivers.Gateways;
    using Drivers.NetFramework;
    using LendersOffice.Common;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json;

    /// <summary>
    /// Share toolkit for loan import export.
    /// </summary>
    public class ShareImportExport
    {
        /// <summary>
        /// The file extension for the whitelist.  Currently ".csv".
        /// </summary>
        public const string WhiteListFileExtension = ".csv";

        /// <summary>
        /// List of basic tables that should be exported in the loan overwrite tool export.
        /// </summary>
        public static readonly string[] ExportTableNames = new string[]
        {
            "LOAN_FILE_A", "LOAN_FILE_B", "LOAN_FILE_C", "LOAN_FILE_D", "LOAN_FILE_E", "LOAN_FILE_F", "APPLICATION_A", "APPLICATION_B", "LOAN_FILE_CACHE", "LOAN_FILE_CACHE_2", "LOAN_FILE_CACHE_3", "LOAN_FILE_CACHE_4", "TRUST_ACCOUNT"
        };

        /// <summary>
        /// Gets or sets the value for counting the sequence of debug message.
        /// </summary>
        /// <value>The value for counting the sequence of debug message.</value>
        public static int DebugCount { get; set; }

        /// <summary>
        /// Filter out the invalid character so that the reader can read the main content normally. 
        /// But it will preserve the invalid character at prefix and suffix because sometimes those characters are needed for the system itself.
        /// </summary>
        /// <param name="errorList">List records errors.</param>
        /// <param name="prefix">The prefix that contains invalid characters.</param>
        /// <param name="suffix">The suffix that contains invalid characters.</param>
        /// <param name="filteredContent">The main content without invalid prefix and suffix.</param>
        /// <param name="originalContent">The original content that might have invalid prefix and suffix.</param>
        public static void FilterJsonPreSuffixBadCharacter(ref List<string> errorList, ref string prefix, ref string suffix, ref string filteredContent, string originalContent)
        {
            if (originalContent == string.Empty || originalContent == null)
            {
                prefix = string.Empty;
                suffix = string.Empty;
                filteredContent = originalContent;
                return;
            }

            int prefInd = originalContent.IndexOfAny(new char[] { '[', '{' });
            int sufInd = originalContent.LastIndexOfAny(new char[] { ']', '}' });

            if (prefInd < 0 || sufInd < 0)
            {
                Tools.LogError("JsonPreSuffixBadCharacterFilter found invalid json: " + originalContent);
                prefix = string.Empty;
                suffix = string.Empty;
                filteredContent = originalContent;
                return;
            }

            prefix = originalContent.Substring(0, prefInd);
            suffix = originalContent.Substring(sufInd + 1);
            filteredContent = originalContent.Substring(prefInd, sufInd - prefInd + 1);
        }

        /// <summary>
        /// Filter out the invalid character so that the reader can read the main content normally. 
        /// But it will preserve the invalid character at prefix and suffix because sometimes those characters are needed for the system itself.
        /// </summary>
        /// <param name="errorList">List records errors.</param>
        /// <param name="prefix">The prefix that contains invalid characters.</param>
        /// <param name="suffix">The suffix that contains invalid characters.</param>
        /// <param name="filteredContent">The main content without invalid prefix and suffix.</param>
        /// <param name="originalContent">The original content that might have invalid prefix and suffix.</param>
        public static void FilterXmlPreSuffixBadCharacter(ref List<string> errorList, ref string prefix, ref string suffix, ref string filteredContent, string originalContent)
        {
            int prefInd = originalContent.IndexOfAny(new char[] { '<' });
            int sufInd = originalContent.LastIndexOfAny(new char[] { '>' });

            if (prefInd < 0 || sufInd < 0)
            {
                errorList.Add("XmlPreSuffixBadCharacterFilter found invalid xml: " + originalContent);
                return;
            }

            prefix = originalContent.Substring(0, prefInd);
            suffix = originalContent.Substring(sufInd + 1);
            filteredContent = originalContent.Substring(prefInd, sufInd - prefInd + 1);
        }

        /// <summary>
        /// Convert xml escape character.
        /// </summary>
        /// <param name="value">Original xml value.</param> 
        /// <returns>Converted xml characters.</returns>
        public static string EscapeXmlCharacter(string value)
        {
            int cnt = value.Length;
            StringBuilder result = new StringBuilder((cnt * 2) + 1);
            for (int i = 0; i < cnt; i++)
            {
                char ch = value[i];
                if (ch == '\"')
                {
                    result.Append("&quot;");
                }
                else if (ch == '\'')
                {
                    result.Append("&apos;");
                }
                else if (ch == '<')
                {
                    result.Append("&lt;");
                }
                else if (ch == '>')
                {
                    result.Append("&gt;");
                }
                else if (ch == '&')
                {
                    result.Append("&amp;");
                }
                else
                {
                    result.Append(ch);
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Convert json escape character.
        /// </summary>
        /// <param name="value">Original json value.</param> 
        /// <returns>Converted json characters.</returns>
        public static string EscapeJsonCharacter(string value)
        {
            int cnt = value.Length;
            StringBuilder result = new StringBuilder((cnt * 2) + 1);
            for (int i = 0; i < cnt; i++)
            {
                char ch = value[i];
                if (ch == '\\')
                {
                    result.Append("\\\\");
                }
                else if (ch == '\"')
                {
                    result.Append("\\\"");
                }
                else if (ch == '/')
                {
                    result.Append("\\/");
                }
                else if (ch == '\b')
                {
                    result.Append("\\b");
                }
                else if (ch == '\f')
                {
                    result.Append("\\f");
                }
                else if (ch == '\n')
                {
                    result.Append("\\n");
                }
                else if (ch == '\r')
                {
                    result.Append("\\r");
                }
                else if (ch == '\t')
                {
                    result.Append("\\t");
                }
                else if (ch == '<')
                {
                    result.Append("\\u003c");
                }
                else if (ch == '>')
                {
                    result.Append("\\u003e");
                }
                else
                {
                    result.Append(ch);
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Convert json escape character. These are uncommon ones. In other words, you don't need to escape these in common situations.
        /// </summary>
        /// <param name="value">Original json value.</param> 
        /// <returns>Converted json characters.</returns>
        public static string EscapeJsonCharacter_Optional(string value)
        {
            int cnt = value.Length;
            StringBuilder result = new StringBuilder((cnt * 2) + 1);
            for (int i = 0; i < cnt; i++)
            {
                char ch = value[i];
                if (ch == '\\')
                {
                    result.Append("\\\\");
                }
                else if (ch == '\"')
                {
                    result.Append("\\\"");
                }
                else if (ch == '<')
                {
                    result.Append("\\u003c");
                }
                else if (ch == '>')
                {
                    result.Append("\\u003e");
                }
                else if (ch == '\n')
                {
                    result.Append("\\n");
                }
                else if (ch == '\r')
                {
                    result.Append("\\r");
                }
                else
                {
                    result.Append(ch);
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Convert only json escape characters which must be done. This must be executed right before you send back the parsed json content. 
        /// </summary>
        /// <param name="value">Original json value.</param> 
        /// <returns>Converted json characters.</returns>
        public static string EscapeJsonCharacter_Basic(string value)
        {
            int cnt = value.Length;
            StringBuilder result = new StringBuilder((cnt * 2) + 1);
            for (int i = 0; i < cnt; i++)
            {
                char ch = value[i];
                if (ch == '\\')
                {
                    result.Append("\\\\");
                }
                else if (ch == '\"')
                {
                    result.Append("\\\"");
                }
                else
                {
                    result.Append(ch);
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Revert xml escape character.
        /// </summary>
        /// <param name="value">Converted xml value.</param> 
        /// <returns>Reverted xml characters.</returns>
        public static string UnEscapeXmlCharacter(string value)
        {
            value = value.Replace("&quot;", "\"");
            value = value.Replace("&apos;", "\'");
            value = value.Replace("&lt;", "<");
            value = value.Replace("&gt;", ">");
            value = value.Replace("&#xA;", string.Empty);
            ////I put &amp; the last because if an xml is escaped twice, and if I move this above any other symbol, those xml will only need one unescape rather than twice.
            value = value.Replace("&amp;", "&");
            return value;
        }

        /// <summary>
        /// A final export data converter for special characters.
        /// </summary>
        /// <param name="dataType">The data type of this data like binary.</param> 
        /// <param name="dataValue">Object data value.</param> 
        /// <returns>Converted xml characters.</returns>
        public static string ExportDataConvert(string dataType, object dataValue)
        {
            if (dataType != null && dataType.ToLower().Contains("varbinary"))
            {
                StringBuilder typeValue = new StringBuilder();
                byte[] binArray = (byte[])dataValue;
                typeValue.Append("0x");
                foreach (byte binItem in binArray)
                {
                    typeValue.AppendFormat("{0:x2}", binItem);
                }

                return typeValue.ToString().TrimWhitespaceAndBOM();
            }
            else
            {
                return Convert.ToString(dataValue).TrimWhitespaceAndBOM();
            }
        }

        /// <summary>
        /// Convert hexadecimal string into byte array.
        /// </summary>
        /// <param name="input">The input hexadecimal string, which might start with '0x'.</param>
        /// <returns>The byte array of the hexadecimal string.</returns>
        public static byte[] ConvertHexStringToByteArray(string input)
        {
            if (input.ToLower() == "null")
            {
                return null;
            }

            string hex = input.StartsWith("0x") ? input.Substring(2) : input;
            if (hex.Length % 2 != 0)
            {
                hex = "0" + hex;
            }

            byte[] hexBytes = new byte[hex.Length / 2];
            int bytesLen = hex.Length / 2;
            hexBytes[0] = byte.Parse("0", NumberStyles.HexNumber);
            for (int i = 0; i < bytesLen; i++)
            {
                string byteVal = hex.Substring(i * 2, 2);
                hexBytes[i] = byte.Parse(byteVal, NumberStyles.HexNumber);
            }

            return hexBytes;
        }

        /// <summary>
        /// Clear empty characters from the value for xml.
        /// </summary>
        /// <param name="item">Original xml value.</param> 
        /// <returns>String with no null character.</returns>
        public static string ClearNullCharacter(string item)
        {
            return item.Trim('\0').TrimWhitespaceAndBOM();
        }

        /// <summary>
        /// Clear leading or ending empty characters from the file.
        /// </summary>
        /// <param name="filepath">The full address of file.</param> 
        public static void RemoveLeadingOrEndingSpaceOfFile(string filepath)
        {
            string fileContent = TextFileHelper.ReadFile(filepath);
            string cleanContent = fileContent.Trim('\0').TrimWhitespaceAndBOM();
            TextFileHelper.WriteString(filepath, cleanContent, false);
        }

        /// <summary>
        /// Count the substring in the main string.
        /// </summary>
        /// <param name="text">The main string.</param> 
        /// <param name="pattern">The substring needed to count.</param> 
        /// <returns>Return the count of substring.</returns>
        public static int CountStringOccurrences(string text, string pattern)
        {
            int count = 0;
            int i = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }

            return count;
        }

        /// <summary>
        /// To fetch a default value for certain columns. Usually, with the data type, it is enough to get the value.
        /// But there might be some columns with special restriction. As for checking constraint or unique index, there are already some codes handling that. 
        /// </summary>
        /// <param name="datatype">The major parameter for finding a default value.</param>
        /// <param name="columnNm">Extra parameter that might help in some special cases.</param>
        /// <param name="tableNm">Extra parameter that might help in rare case.</param>
        /// <returns>The default value in object type.</returns>
        public static object GetDefaultDatabaseValByType(string datatype, string columnNm, string tableNm)
        {
            if (datatype == "int" || datatype == "decimal" || datatype == "bigint" || datatype == "smallint" || datatype == "tinyint" || datatype == "money" || datatype == "bit")
            {
                return 0;
            }
            else if (datatype == "uniqueidentifier")
            {
                return "00000000-0000-0000-0000-000000000000";
            }
            else if (datatype == "varbinary")
            {
                return new byte[1];
            }
            else if (datatype == "datetime" || datatype == "smalldatetime")
            {
                return "2013-01-01 01:00:00";
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get asset xml.
        /// </summary>
        /// <param name="loanID">This is loan id.</param> 
        /// <param name="appID">Application id.</param> 
        /// <returns>Asset xml string.</returns>
        public static string GetAssetXmlRecord(string loanID, string appID)
        {
            string xmlContent = string.Empty;
            ////CPageData dataLoan = CPageData.CreateUsingSmartDependency(new Guid(loanID), typeof(ShareImportExport));
            CPageData dataLoan = new CAssetAllAppsData(new Guid(loanID));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(new Guid(appID));
            IAssetCollection recordList = dataApp.aAssetCollection;
            xmlContent = recordList.GetDataSet().GetXml();
            DataView sortedView = recordList.SortedView;
            int cnt = sortedView.Count;

            StringBuilder strWriter = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "    ";

            IAssetRegular record = null;
            using (XmlWriter xmlWriter = XmlWriter.Create(strWriter, settings))
            {
                xmlWriter.WriteStartElement("AssetSchemaDataSet");
                for (int i = 0; i < cnt; i++)
                {
                    xmlWriter.WriteStartElement("AssetXmlContent");
                    record = recordList.GetRegularRecordAt(i);
                    xmlWriter.WriteElementString("RecordId", record.RecordId.ToString());
                    xmlWriter.WriteElementString("OwnerT", record.OwnerT.ToString());
                    xmlWriter.WriteElementString("AssetT", record.AssetT.ToString());
                    xmlWriter.WriteElementString("Val", record.Val_rep);
                    xmlWriter.WriteElementString("Desc", record.Desc);
                    xmlWriter.WriteElementString("AccNum", record.AccNum.Value);
                    xmlWriter.WriteElementString("AccNm", record.AccNm);
                    xmlWriter.WriteElementString("ComNm", record.ComNm);
                    xmlWriter.WriteElementString("StAddr", record.StAddr);
                    xmlWriter.WriteElementString("City", record.City);
                    xmlWriter.WriteElementString("State", record.State);
                    xmlWriter.WriteElementString("Zip", record.Zip);
                    xmlWriter.WriteElementString("VerifReorderedD", record.VerifReorderedD_rep);
                    xmlWriter.WriteElementString("VerifSentD", record.VerifSentD_rep);
                    xmlWriter.WriteElementString("VerifRecvD", record.VerifRecvD_rep);
                    xmlWriter.WriteElementString("VerifExpD", record.VerifExpD_rep);
                    xmlWriter.WriteElementString("OtherTypeDesc", record.OtherTypeDesc);
                    xmlWriter.WriteElementString("DepartmentName", record.DepartmentName);
                    xmlWriter.WriteElementString("OrderRankValue", record.OrderRankValue.ToString());
                    xmlWriter.WriteElementString("GiftSource", record.GiftSource.ToString());
                    xmlWriter.WriteElementString("IsEmptyCreated", record.IsEmptyCreated.ToString());
                    xmlWriter.WriteEndElement();
                }

                xmlWriter.WriteEndElement();
            }

            return strWriter.ToString();
        }

        /// <summary>
        /// Get liability xml.
        /// </summary>
        /// <param name="loanID">This is loan id.</param> 
        /// <param name="appID">Application id.</param> 
        /// <returns>Liability xml string.</returns>
        public static string GetLiaXmlRecord(string loanID, string appID)
        {
            string xmlContent = string.Empty;
            CPageData dataLoan = new CLiaData(new Guid(loanID), false);
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(new Guid(appID));
            ILiaCollection recordList = dataApp.aLiaCollection;
            xmlContent = recordList.GetDataSet().GetXml();
            DataView sortedView = recordList.SortedView;
            int cnt = sortedView.Count;
            ILiabilityRegular record = null;
            StringBuilder strWriter = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "    ";

            using (XmlWriter xmlWriter = XmlWriter.Create(strWriter, settings))
            {
                xmlWriter.WriteStartElement("LiabilitySchemaDataSet");
                for (int i = 0; i < cnt; i++)
                {
                    xmlWriter.WriteStartElement("LiaXmlContent");
                    record = recordList.GetRegularRecordAt(i);
                    xmlWriter.WriteElementString("RecordId", record.RecordId.ToString());
                    xmlWriter.WriteElementString("MatchedReRecordId", record.MatchedReRecordId.ToString());
                    xmlWriter.WriteElementString("OwnerT", record.OwnerT.ToString());
                    xmlWriter.WriteElementString("DebtT", record.DebtT.ToString());
                    xmlWriter.WriteElementString("ComNm", record.ComNm);
                    xmlWriter.WriteElementString("ComAddr", record.ComAddr);
                    xmlWriter.WriteElementString("ComCity", record.ComCity);
                    xmlWriter.WriteElementString("ComState", record.ComState);
                    xmlWriter.WriteElementString("ComZip", record.ComZip);
                    xmlWriter.WriteElementString("ComPhone", record.ComPhone);
                    xmlWriter.WriteElementString("ComFax", record.ComFax);
                    xmlWriter.WriteElementString("AccNum", record.AccNum.Value);
                    xmlWriter.WriteElementString("AccNm", record.AccNm);
                    xmlWriter.WriteElementString("Bal", record.Bal.ToString());
                    xmlWriter.WriteElementString("Pmt", record.Pmt.ToString());
                    xmlWriter.WriteElementString("R", record.R_rep);
                    xmlWriter.WriteElementString("OrigTerm", record.OrigTerm_rep);
                    xmlWriter.WriteElementString("Due", record.Due_rep);
                    xmlWriter.WriteElementString("RemainMons", record.RemainMons_rep);
                    xmlWriter.WriteElementString("WillBePdOff", record.WillBePdOff.ToString());
                    xmlWriter.WriteElementString("NotUsedInRatio", record.NotUsedInRatio.ToString());
                    xmlWriter.WriteElementString("IsPiggyBack", record.IsPiggyBack.ToString());
                    xmlWriter.WriteElementString("Late30", record.Late30_rep);
                    xmlWriter.WriteElementString("Late60", record.Late60_rep);
                    xmlWriter.WriteElementString("Late90Plus", record.Late90Plus_rep);
                    xmlWriter.WriteElementString("IncInReposession", record.IncInReposession.ToString());
                    xmlWriter.WriteElementString("IncInBankruptcy", record.IncInBankruptcy.ToString());
                    xmlWriter.WriteElementString("IncInForeclosure", record.IncInForeclosure.ToString());
                    xmlWriter.WriteElementString("ExcFromUnderwriting", record.ExcFromUnderwriting.ToString());
                    xmlWriter.WriteElementString("VerifSentD", record.VerifSentD_rep);
                    xmlWriter.WriteElementString("VerifRecvD", record.VerifRecvD_rep);
                    xmlWriter.WriteElementString("VerifExpD", record.VerifExpD_rep);
                    xmlWriter.WriteElementString("VerifReorderedD", record.VerifReorderedD_rep);
                    xmlWriter.WriteElementString("Desc", record.Desc);
                    xmlWriter.WriteElementString("OrderRankValue", record.OrderRankValue.ToString());
                    xmlWriter.WriteEndElement();
                }

                xmlWriter.WriteEndElement();
            }

            return strWriter.ToString();
        }

        /// <summary>
        /// Get asset xml.
        /// </summary>
        /// <param name="whitelistDictionary">A white list dictionary.</param>
        /// <param name="loanID">This is loan id.</param> 
        /// <param name="appID">Application id.</param> 
        /// <returns>Asset xml string.</returns>
        public static string GetAssetXmlRecord(ref Dictionary<string, WhitelistData> whitelistDictionary, string loanID, string appID)
        {
            ////CPageData dataLoan = CPageData.CreateUsingSmartDependency(new Guid(loanID), typeof(ShareImportExport));
            CPageData dataLoan = new CAssetAllAppsData(new Guid(loanID));
            dataLoan.InitLoad();
            CAppData dataApp = dataLoan.GetAppData(new Guid(appID));
            string xmlContent = dataApp.aAssetXmlContent.Value;

            string tableNm = "FILEDB|ASSET";
            if (!whitelistDictionary.ContainsKey(tableNm))
            {
                whitelistDictionary.Add(tableNm, new WhitelistData());
            }

            WhitelistData curWhitelist = whitelistDictionary[tableNm];
            XElement xmlElement = XElement.Parse(xmlContent);
            ShareImportExport.FilterFiledbXmlElementByWhitelist(ref curWhitelist, ref xmlElement);
            return xmlElement.ToString();
        }

        /// <summary>
        /// Get the file database record by file database key and special class.
        /// </summary>
        /// <param name="whitelistDictionary">A dictionary maps table name to a white list object.</param>
        /// <param name="filedbOfOneLoan">A large field storage object created by loan id.</param>
        /// <param name="filedbKey">A xml content key from database used to find the file database content.</param>
        /// <param name="tableNm">A table name defined by me at the code so that we can filter it in the white list.</param>
        /// <returns>Return the xml content string.</returns>
        public static string GetFiledbXmlRecord(ref Dictionary<string, WhitelistData> whitelistDictionary, LargeFieldStorage filedbOfOneLoan, Guid filedbKey, string tableNm)
        {
            if (!whitelistDictionary.ContainsKey(tableNm))
            {
                whitelistDictionary.Add(tableNm, new WhitelistData());
            }

            WhitelistData curWhitelist = whitelistDictionary[tableNm];
            string xmlContent = filedbOfOneLoan.GetText(filedbKey);
            if (xmlContent.TrimWhitespaceAndBOM() == string.Empty)
            {
                return xmlContent;
            }

            XElement xmlElement = XElement.Parse(xmlContent);
            ShareImportExport.FilterFiledbXmlElementByWhitelist(ref curWhitelist, ref xmlElement);
            return xmlElement.ToString();
        }

        /// <summary>
        /// Set asset xml.
        /// </summary>
        /// <param name="msgList">Record error message.</param> 
        /// <param name="xmlContent">The input string of asset xml.</param> 
        /// <param name="loanID">This is loan id.</param> 
        /// <param name="appID">Application id.</param> 
        public static void SetAssetXmlRecord(ref List<string> msgList, string xmlContent, string loanID, string appID)
        {
            // CPageData dataLoan = CPageData.CreateUsingSmartDependency(new Guid(loanID), typeof(ShareImportExport));
            CPageData dataLoan = new CAssetAllAppsData(new Guid(loanID));
            dataLoan.InitLoad();
            dataLoan.InitSave(dataLoan.sFileVersion);
            CAppData dataApp = dataLoan.GetAppData(new Guid(appID));
            IAssetCollection recordList = dataApp.aAssetCollection;
            DataView sortedView = recordList.SortedView;
            int cnt = sortedView.Count;
            XmlReader xmlReader = XmlReader.Create(new System.IO.StringReader(xmlContent));
            int curRecordNum = 0;
            string curTagName = string.Empty;
            IAssetRegular record = null;

            int num_AssetXml = CountStringOccurrences(xmlContent, @"<AssetXmlContent>");
            if (num_AssetXml < cnt)
            {
                for (int i = num_AssetXml; i < cnt; i++)
                {
                    record = recordList.GetRegularRecordAt(i);
                    record.IsOnDeathRow = true;
                }
                ////msgList.Add(@"<script language='javascript'>alert('Warning: The program delete extra records from aAssetXmlContent of current loan file');</script>");
            }
            else if (num_AssetXml > cnt)
            {
                for (int i = cnt; i < num_AssetXml; i++)
                {
                    recordList.AddRegularRecord();
                }
                ////msgList.Add(@"<script language='javascript'>alert('Warning: The program adds more records into aLiaXmlContent of current loan file');</script>");
            }

            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == System.Xml.XmlNodeType.Element)
                {
                    curTagName = xmlReader.Name;
                    if (curTagName.Equals("AssetXmlContent"))
                    {
                        record = recordList.GetRegularRecordAt(curRecordNum);
                        curRecordNum += 1;
                    }
                }
                else if (xmlReader.NodeType == System.Xml.XmlNodeType.EndElement)
                {
                }
                else if (xmlReader.NodeType == System.Xml.XmlNodeType.Text)
                {
                    // case "RecordId": record.RecordId = xmlReader.Value; break;
                    if (curTagName == "AccNm")
                    {
                        record.AccNm = xmlReader.Value;
                    }
                    else if (curTagName == "AccNum")
                    {
                        record.AccNum = xmlReader.Value;
                    }
                    else if (curTagName == "AssetT")
                    {
                        record.AssetT = (E_AssetRegularT)Enum.Parse(typeof(E_AssetRegularT), xmlReader.Value);
                    }
                    else if (curTagName == "City")
                    {
                        record.City = xmlReader.Value;
                    }
                    else if (curTagName == "ComNm")
                    {
                        record.ComNm = xmlReader.Value;
                    }
                    else if (curTagName == "DepartmentName")
                    {
                        record.DepartmentName = xmlReader.Value;
                    }
                    else if (curTagName == "Desc")
                    {
                        record.Desc = xmlReader.Value;
                    }
                    else if (curTagName == "GiftSource")
                    {
                        record.GiftSource = (E_GiftFundSourceT)Enum.Parse(typeof(E_GiftFundSourceT), xmlReader.Value);
                    }
                    else if (curTagName == "IsEmptyCreated")
                    {
                        record.IsEmptyCreated = Convert.ToBoolean(xmlReader.Value);
                    }
                    else if (curTagName == "OrderRankValue")
                    {
                        record.OrderRankValue = Convert.ToDouble(xmlReader.Value);
                    }
                    else if (curTagName == "OtherTypeDesc")
                    {
                        record.OtherTypeDesc = xmlReader.Value;
                    }
                    else if (curTagName == "OwnerT")
                    {
                        record.OwnerT = (E_AssetOwnerT)Enum.Parse(typeof(E_AssetOwnerT), xmlReader.Value);
                    }
                    else if (curTagName == "StAddr")
                    {
                        record.StAddr = xmlReader.Value;
                    }
                    else if (curTagName == "State")
                    {
                        record.State = xmlReader.Value;
                    }
                    else if (curTagName == "Val")
                    {
                        record.Val_rep = xmlReader.Value;
                    }
                    else if (curTagName == "VerifExpD")
                    {
                        record.VerifExpD_rep = xmlReader.Value;
                    }
                    else if (curTagName == "VerifRecvD")
                    {
                        record.VerifRecvD_rep = xmlReader.Value;
                    }
                    else if (curTagName == "VerifReorderedD")
                    {
                        record.VerifReorderedD_rep = xmlReader.Value;
                    }
                    else if (curTagName == "VerifSentD")
                    {
                        record.VerifSentD_rep = xmlReader.Value;
                    }
                    else if (curTagName == "Zip")
                    {
                        record.Zip = xmlReader.Value;
                    }
                }
            }

            recordList.Flush();
            dataLoan.Save();
        }

        /// <summary>
        /// Set liability xml.
        /// </summary>
        /// <param name="msgList">Record error message.</param> 
        /// <param name="xmlContent">The input string of liability xml.</param> 
        /// <param name="loanID">This is loan id.</param> 
        /// <param name="appID">Application id.</param> 
        public static void SetLiaXmlRecord(ref List<string> msgList, string xmlContent, string loanID, string appID)
        {
            CPageData dataLoan = new CLiaData(new Guid(loanID), false /*Workflow is not intended to be enforced here*/);
            dataLoan.InitLoad();
            dataLoan.InitSave(dataLoan.sFileVersion);
            CAppData dataApp = null;
            dataApp = dataLoan.GetAppData(new Guid(appID));
            ILiaCollection recordList = dataApp.aLiaCollection;
            DataView sortedView = recordList.SortedView;
            int cnt = sortedView.Count;
            XmlReader xmlReader = XmlReader.Create(new System.IO.StringReader(xmlContent));
            int curRecordNum = 0;
            string curTagName = string.Empty;
            ILiabilityRegular record = null;
            int num_LiaXml = CountStringOccurrences(xmlContent, @"<LiaXmlContent>");
            if (num_LiaXml < cnt)
            {
                for (int i = num_LiaXml; i < cnt; i++)
                {
                    record = recordList.GetRegularRecordAt(i);
                    record.IsOnDeathRow = true;
                }
                ////msgList.Add(@"<script language='javascript'>alert('Warning: The program delete extra records from aLiaXmlContent of current loan file');</script>");
            }
            else if (num_LiaXml > cnt)
            {
                for (int i = cnt; i < num_LiaXml; i++)
                {
                    recordList.AddRegularRecord();
                }
                ////msgList.Add(@"<script language='javascript'>alert('Warning: The program adds more records into aLiaXmlContent of current loan file');</script>");
            }

            while (xmlReader.Read())
            {
                if (xmlReader.NodeType == System.Xml.XmlNodeType.Element)
                {
                    curTagName = xmlReader.Name;
                    if (curTagName.Equals("LiaXmlContent"))
                    {
                        record = recordList.GetRegularRecordAt(curRecordNum);
                        curRecordNum += 1;
                    }
                }
                else if (xmlReader.NodeType == System.Xml.XmlNodeType.EndElement)
                {
                }
                else if (xmlReader.NodeType == System.Xml.XmlNodeType.Text)
                {
                    // case "RecordId": record.RecordId = xmlReader.Value; break;
                    if (curTagName == "MatchedReRecordId")
                    {
                        record.MatchedReRecordId = new Guid(xmlReader.Value);
                    }
                    else if (curTagName == "OwnerT")
                    {
                        record.OwnerT = (E_LiaOwnerT)Enum.Parse(typeof(E_LiaOwnerT), xmlReader.Value);
                    }
                    else if (curTagName == "DebtT")
                    {
                        record.DebtT = (E_DebtRegularT)Enum.Parse(typeof(E_DebtRegularT), xmlReader.Value);
                    }
                    else if (curTagName == "ComNm")
                    {
                        record.ComNm = xmlReader.Value;
                    }
                    else if (curTagName == "ComAddr")
                    {
                        record.ComAddr = xmlReader.Value;
                    }
                    else if (curTagName == "ComCity")
                    {
                        record.ComCity = xmlReader.Value;
                    }
                    else if (curTagName == "ComState")
                    {
                        record.ComState = xmlReader.Value;
                    }
                    else if (curTagName == "ComZip")
                    {
                        record.ComZip = xmlReader.Value;
                    }
                    else if (curTagName == "ComPhone")
                    {
                        record.ComPhone = xmlReader.Value;
                    }
                    else if (curTagName == "ComFax")
                    {
                        record.ComFax = xmlReader.Value;
                    }
                    else if (curTagName == "AccNum")
                    {
                        record.AccNum = xmlReader.Value;
                    }
                    else if (curTagName == "AccNm")
                    {
                        record.AccNm = xmlReader.Value;
                    }
                    else if (curTagName == "Bal")
                    {
                        record.Bal_rep = xmlReader.Value;
                    }
                    else if (curTagName == "Pmt")
                    {
                        record.Pmt_rep = xmlReader.Value;
                    }
                    else if (curTagName == "R")
                    {
                        record.R_rep = xmlReader.Value;
                    }
                    else if (curTagName == "OrigTerm")
                    {
                        record.OrigTerm_rep = xmlReader.Value;
                    }
                    else if (curTagName == "Due")
                    {
                        record.Due_rep = xmlReader.Value;
                    }
                    else if (curTagName == "RemainMons")
                    {
                        record.RemainMons_rep = xmlReader.Value;
                    }
                    else if (curTagName == "WillBePdOff")
                    {
                        record.WillBePdOff = Convert.ToBoolean(xmlReader.Value);
                    }
                    else if (curTagName == "NotUsedInRatio")
                    {
                        record.NotUsedInRatio = Convert.ToBoolean(xmlReader.Value);
                    }
                    else if (curTagName == "IsPiggyBack")
                    {
                        record.IsPiggyBack = Convert.ToBoolean(xmlReader.Value);
                    }
                    else if (curTagName == "Late30")
                    {
                        record.Late30_rep = xmlReader.Value;
                    }
                    else if (curTagName == "Late60")
                    {
                        record.Late60_rep = xmlReader.Value;
                    }
                    else if (curTagName == "Late90Plus")
                    {
                        record.Late90Plus_rep = xmlReader.Value;
                    }
                    else if (curTagName == "IncInReposession")
                    {
                        record.IncInReposession = Convert.ToBoolean(xmlReader.Value);
                    }
                    else if (curTagName == "IncInBankruptcy")
                    {
                        record.IncInBankruptcy = Convert.ToBoolean(xmlReader.Value);
                    }
                    else if (curTagName == "IncInForeclosure")
                    {
                        record.IncInForeclosure = Convert.ToBoolean(xmlReader.Value);
                    }
                    else if (curTagName == "ExcFromUnderwriting")
                    {
                        record.ExcFromUnderwriting = Convert.ToBoolean(xmlReader.Value);
                    }
                    else if (curTagName == "VerifSentD")
                    {
                        record.VerifSentD_rep = xmlReader.Value;
                    }
                    else if (curTagName == "VerifRecvD")
                    {
                        record.VerifRecvD_rep = xmlReader.Value;
                    }
                    else if (curTagName == "VerifExpD")
                    {
                        record.VerifExpD_rep = xmlReader.Value;
                    }
                    else if (curTagName == "VerifReorderedD")
                    {
                        record.VerifReorderedD_rep = xmlReader.Value;
                    }
                    else if (curTagName == "Desc")
                    {
                        record.Desc = xmlReader.Value;
                    }
                    else if (curTagName == "OrderRankValue")
                    {
                        record.OrderRankValue = Convert.ToDouble(xmlReader.Value);
                    }
                }
            }

            recordList.Flush();
            dataLoan.Save();
        }

        /// <summary>
        /// One xml content should contain one liability schema data set.
        /// File database column can only be protected or exportable in white list.
        /// This function is to apply protection over the original file database content.
        /// </summary>
        /// <param name="whitelist">A white list that shows any column needs to be protected.</param>
        /// <param name="xmlElement">One xml element for all liabilities/assets of one application.</param>
        public static void FilterFiledbXmlElementByWhitelist(ref WhitelistData whitelist, ref XElement xmlElement)
        {
            IEnumerable<XElement> xmlContentList = xmlElement.Elements();
            foreach (XElement xmlContent in xmlContentList)
            {
                IEnumerable<XElement> colXmlList = xmlContent.Elements();
                foreach (XElement colXml in colXmlList)
                {
                    string colNm = colXml.Name.ToString();
                    bool isExportableCol = whitelist.ExportSet.Contains(colNm);
                    bool isProtectedCol = whitelist.ProtectedDictionary.ContainsKey(colNm);

                    if (isExportableCol)
                    {
                        continue;
                    }
                    else if (isProtectedCol)
                    {
                        colXml.Value = whitelist.ProtectedDictionary[colNm];
                    }
                    else
                    {
                        colXml.Value = string.Empty;
                        if (!whitelist.ReviewSet.Contains(colNm))
                        {
                            whitelist.MissingSet.Add(colNm);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Use to filter a value based on white list, especially for sub columns within a json or xml content column.
        /// </summary>
        /// <param name="errorList">List records errors.</param>
        /// <param name="whitelistDictionary">A white list dictionary to tell if a column is exportable.</param>
        /// <param name="needUpdateWhitelist">A flag decides whether need to update white list or not.</param>
        /// <param name="filteredVal">The result value filtered by white list.</param>
        /// <param name="isNewCol">A flag to tell if this is a new column. Set false if you don't know.</param>
        /// <param name="tableName">The table name of current column.</param>
        /// <param name="colName">The name of the sub column.</param>
        /// <param name="originalVal">The original value of the sub column.</param>
        /// <param name="needDebugInfo">Decide whether need extra debugging information or not.</param>
        /// <returns>True if the column is exportable. False if the column is not exportable.</returns>
        public static bool FilterValueByWhitelist(
            ref List<string> errorList,
            ref Dictionary<string, WhitelistData> whitelistDictionary,
            ref bool needUpdateWhitelist,
            ref string filteredVal,
            bool isNewCol,
            string tableName,
            string colName,
            string originalVal,
            bool needDebugInfo)
        {
            if (needDebugInfo)
            {
                Tools.LogInfo("JustinDebug[" + (ShareImportExport.DebugCount++) + "] -> FilterValueByWhitelist: table[" + tableName + "] column[" + colName + "] originalVal:\n" + originalVal);
            }

            string trimTableNm = tableName.TrimWhitespaceAndBOM();
            if (trimTableNm == string.Empty || trimTableNm == null)
            {
                return true;
            }

            if (!whitelistDictionary.ContainsKey(tableName))
            {
                whitelistDictionary.Add(tableName, new WhitelistData());
            }

            WhitelistData whitelist = whitelistDictionary[tableName];
            bool isNoExportNoImport = whitelist.NoImportExportSet.Contains(colName);
            bool isProtected = whitelist.ProtectedDictionary.ContainsKey(colName);
            bool isExportable = whitelist.ExportSet.Contains(colName);
            bool isPreview = whitelist.ReviewSet.Contains(colName);
            if (isNewCol ||
                (isPreview == false &&
                isExportable == false &&
                isNoExportNoImport == false &&
                isProtected == false))
            {
                needUpdateWhitelist = true;
                whitelist.MissingSet.Add(colName);
                filteredVal = string.Empty;
            }
            else if (isNoExportNoImport || isPreview)
            {
                filteredVal = string.Empty;
            }
            else if (isProtected)
            {
                string defaultVal = whitelist.ProtectedDictionary[colName];
                if (defaultVal.StartsWith("{ParseJson"))
                {
                    string jsonTableNm = tableName + "|ParseJson|" + colName;
                    string[] tokens = defaultVal.Substring(1, defaultVal.Length - 2).Split('|');
                    filteredVal = ShareImportExport.FilterJsonByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, jsonTableNm, originalVal, tokens[1], needDebugInfo);
                }
                else if (defaultVal.StartsWith("{ParseXml"))
                {
                    string xmlTableNm = tableName + "|ParseXml|" + colName;
                    string[] tokens = defaultVal.Substring(1, defaultVal.Length - 2).Split('|');
                    filteredVal = ShareImportExport.FilterXmlByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, xmlTableNm, originalVal, tokens[1], needDebugInfo);
                }
                else if (defaultVal.StartsWith("{DateFormat:"))
                {
                    if (originalVal == string.Empty || originalVal == null)
                    {
                        filteredVal = originalVal;
                        return true;
                    }

                    string[] tokens = defaultVal.Substring(defaultVal.IndexOf(':') + 1).TrimEnd('}').Split('|');
                    string dateFormatStr = tokens[0];
                    string escapeMethod = tokens[1];
                    if (dateFormatStr == "Microsoft" || dateFormatStr == "ISO")
                    {
                        DateTime dateResult = DateTime.MinValue;
                        bool datePassOrNot = DateTime.TryParse(originalVal, out dateResult);
                        if (datePassOrNot == false)
                        {
                            Tools.LogError("Fail to parse datetime in json table[" + tableName + "] subCol[" + colName + "] \n\ncolVal[" + originalVal + "]");
                            filteredVal = originalVal;
                            return true;
                        }

                        JsonSerializerSettings microsoftDateFormatSettings = SerializationHelper.GetJsonNetSerializerSettings();
                        microsoftDateFormatSettings.DateFormatHandling = (dateFormatStr == "ISO") ? DateFormatHandling.IsoDateFormat : DateFormatHandling.MicrosoftDateFormat;
                        filteredVal = JsonConvert.SerializeObject(dateResult, microsoftDateFormatSettings).Trim('\"');
                        if (escapeMethod != "ESCAPE")
                        {
                            filteredVal = filteredVal.Replace("\\/", "/");
                        }
                    }
                    else if (dateFormatStr == "NormalSerialize")
                    {
                        if (originalVal == string.Empty || originalVal == null)
                        {
                            filteredVal = originalVal;
                        }

                        DateTime baseDate = new DateTime(1970, 1, 1);
                        DateTime currentDate;
                        bool datePassOrNot = DateTime.TryParse(originalVal, out currentDate);
                        if (datePassOrNot == false)
                        {
                            Tools.LogError("Fail to parse datetime in json table[" + tableName + "] subCol[" + colName + "] \n\ncolVal[" + originalVal + "]");
                            filteredVal = originalVal;
                            return true;
                        }

                        TimeSpan ts = new TimeSpan(currentDate.Ticks - baseDate.Ticks);
                        filteredVal = escapeMethod == "ESCAPE" ? "\\/Date(" + ts.TotalMilliseconds + ")\\/" : "/Date(" + ts.TotalMilliseconds + ")/";
                    }
                    else
                    {
                        DateTime dateVal = DateTime.MinValue;
                        DateTime.TryParse(originalVal, out dateVal);
                        filteredVal = dateVal.ToString(dateFormatStr);
                    }
                }
                else
                {
                    filteredVal = defaultVal;
                }
            }
            else if (isExportable)
            {
                filteredVal = originalVal;
            }

            return !(isNoExportNoImport || isPreview);
        }

        /// <summary>
        /// Use white list to filter out sensitive data in json.
        /// </summary>
        /// <param name="errorList">List records errors.</param>
        /// <param name="whitelistDictionary">A dictionary map table name to a white list data.</param>
        /// <param name="needUpdateWhitelist">A flag decides whether need to update white list or not.</param>
        /// <param name="tableName">Current table name.</param>
        /// <param name="oldJson">The original json content.</param>
        /// <param name="escapeMethod">Specify the method to escape json value's character.</param>
        /// <param name="needDebugInfo">If true, it will show the log information in the Paul Bunyan message viewer. It has many message.</param>
        /// <returns>Return the json filtered by white list.</returns>
        public static string FilterJsonByWhitelist(
            ref List<string> errorList, 
            ref Dictionary<string, WhitelistData> whitelistDictionary, 
            ref bool needUpdateWhitelist, 
            string tableName, 
            string oldJson, 
            string escapeMethod,
            bool needDebugInfo)
        {
            if (needDebugInfo)
            {
                Tools.LogInfo("JustinDebug[" + (ShareImportExport.DebugCount++) + "] -> FilterValueByWhitelist: table[" + tableName + "] json[" + oldJson + "] \n");
            }

            string trimTableNm = tableName.TrimWhitespaceAndBOM();
            if (trimTableNm == string.Empty || trimTableNm == null)
            {
                return string.Empty;
            }

            if (!whitelistDictionary.ContainsKey(tableName))
            {
                needUpdateWhitelist = true;
                whitelistDictionary.Add(tableName, new WhitelistData());
            }

            if (oldJson == string.Empty || oldJson == null)
            {
                return oldJson;
            }

            string jsonPrefix = string.Empty;
            string jsonSuffix = string.Empty;
            string jsonMainData = string.Empty;
            ShareImportExport.FilterJsonPreSuffixBadCharacter(ref errorList, ref jsonPrefix, ref jsonSuffix, ref jsonMainData, oldJson);
            if (jsonMainData == null || jsonMainData == string.Empty)
            {
                return jsonMainData;
            }

            WhitelistData whitelist = whitelistDictionary[tableName];
            bool allNonReviewInWhitelist = (whitelist == null) || (whitelist.NoImportExportSet.Count() + whitelist.NoUpdateSet.Count() + whitelist.ProtectedDictionary.Count() + whitelist.ReviewSet.Count() + whitelist.ExportSet.Count() + whitelist.UpdateProtectDictionary.Count() == 0) ? true : false;
            StringBuilder newJsonBuilder = new StringBuilder();
            using (JsonTextReader reader = new JsonTextReader(new StringReader(jsonMainData)))
            {
                using (JsonTextWriter writer = new JsonTextWriter(new StringWriter(newJsonBuilder)))
                {
                    string curPropertyNm = string.Empty;
                    while (reader.Read())
                    {
                        JsonToken tokenType = reader.TokenType;
                        string value = reader.Value == null ? null : reader.Value.ToString();
                        string filteredVal = string.Empty;
                        bool isExportable = false;
                        if (tokenType == JsonToken.Boolean ||
                            tokenType == JsonToken.Bytes ||
                            tokenType == JsonToken.Integer ||
                            tokenType == JsonToken.Float ||
                            tokenType == JsonToken.Date ||
                            tokenType == JsonToken.String)
                        {
                            filteredVal = null;
                            isExportable = FilterValueByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, ref filteredVal, allNonReviewInWhitelist, tableName, curPropertyNm, value, needDebugInfo);
                            if (filteredVal != null)
                            {
                                if (whitelist.ProtectedDictionary.ContainsKey(curPropertyNm) && whitelist.ProtectedDictionary[curPropertyNm].Contains("NO_INHERIT_ESCAPE"))
                                {
                                    //// This means the both the parent column, which is "tableName" and child column, "curPropertyNm" are json/xml content.
                                    //// When specifying "no inherit escape", the escape method in parent node will not apply to the child node. 
                                    //// Otherwise, the child node will first apply its own escape method and then the parent node will apply the parent's escape method over it.
                                }
                                else
                                {
                                    if (escapeMethod == "ESCAPE_JSON")
                                    {
                                        filteredVal = ShareImportExport.EscapeJsonCharacter(filteredVal);
                                    }
                                    else if (escapeMethod == "ESCAPE_JSON_OPTIONAL")
                                    {
                                        filteredVal = ShareImportExport.EscapeJsonCharacter_Optional(filteredVal);
                                    }
                                    else
                                    {
                                        filteredVal = ShareImportExport.EscapeJsonCharacter_Basic(filteredVal);
                                    }
                                }

                                if (escapeMethod == "ESCAPE_XML")
                                {
                                    filteredVal = ShareImportExport.EscapeXmlCharacter(filteredVal);
                                }
                            }
                        }

                        switch (tokenType)
                        {
                            case JsonToken.Null:
                                writer.WriteNull();
                                break;
                            case JsonToken.Boolean:
                                bool boolResult;
                                bool.TryParse(filteredVal, out boolResult);
                                writer.WriteValue(boolResult);
                                break;
                            case JsonToken.Integer:
                                int intResult;
                                int.TryParse(filteredVal, out intResult);
                                writer.WriteValue(intResult);
                                break;
                            case JsonToken.Float:
                                double doubResult;
                                double.TryParse(filteredVal, out doubResult);
                                writer.WriteValue(doubResult);
                                break;
                            case JsonToken.Date:
                            case JsonToken.String:
                                writer.WriteRawValue("\"" + filteredVal + "\"");
                                break;
                            case JsonToken.Bytes:
                                writer.WriteValue(filteredVal);
                                break;
                            case JsonToken.PropertyName:
                                writer.WritePropertyName(value);
                                curPropertyNm = Regex.Replace(reader.Path, @"\[[0-9]+\]", string.Empty);
                                break;
                            case JsonToken.StartConstructor:
                                writer.WriteStartConstructor(value);
                                break;
                            case JsonToken.StartObject:
                                writer.WriteStartObject();
                                break;
                            case JsonToken.StartArray:
                                writer.WriteStartArray();
                                break;
                            case JsonToken.EndConstructor:
                                writer.WriteEndConstructor();
                                break;
                            case JsonToken.EndArray:
                                writer.WriteEndArray();
                                break;
                            case JsonToken.EndObject:
                                writer.WriteEndObject();
                                break;
                            case JsonToken.Comment:
                                writer.WriteComment(filteredVal);
                                break;
                            case JsonToken.Undefined:
                            case JsonToken.None:
                                break;
                            case JsonToken.Raw:
                                writer.WriteRaw(value);
                                break;
                            default:
                                throw new ApplicationException("Unknow json token type:" + tokenType.ToString());
                        }
                    }
                }
            }

            return jsonPrefix + newJsonBuilder.ToString() + jsonSuffix;
        }

        /// <summary>
        /// Most xml content don't follow the predefined format as my exported file does.
        /// So, we need a more generic method to traverse through the xml and filter sensitive data.
        /// Right now, it only deals with element, attribute, c data and comment.
        /// </summary>
        /// <param name="errorList">List records errors.</param>
        /// <param name="whitelistDictionary">A dictionary map table name to a white list data.</param>
        /// <param name="needUpdateWhitelist">A flag decides whether need to update white list or not.</param>
        /// <param name="tableName">Current table name.</param>
        /// <param name="originalXml">The original xml content.</param>
        /// <param name="escapedMethod">The escape method.</param>
        /// <param name="needDebugInfo">Decide whether to show debug information or not.</param>
        /// <returns>The xml content filtered by white list.</returns>
        public static string FilterXmlByWhitelist(
            ref List<string> errorList, 
            ref Dictionary<string, WhitelistData> whitelistDictionary, 
            ref bool needUpdateWhitelist, 
            string tableName, 
            string originalXml, 
            string escapedMethod,
            bool needDebugInfo)
        {
            if (needDebugInfo)
            {
                Tools.LogInfo("JustinDebug[" + (ShareImportExport.DebugCount++) + "] -> FilterXmlByWhitelist: table[" + tableName + "] originalXml:\n" + originalXml);
            }

            string trimTableNm = tableName.TrimWhitespaceAndBOM();
            if (trimTableNm == string.Empty || trimTableNm == null)
            {
                return string.Empty;
            }
            
            if (!whitelistDictionary.ContainsKey(tableName))
            {
                needUpdateWhitelist = true;
                whitelistDictionary.Add(tableName, new WhitelistData());
            }

            if (originalXml == null || originalXml == string.Empty)
            {
                return originalXml;
            }

            string xmlPrefix = string.Empty;
            string xmlSuffix = string.Empty;
            string xmlMainData = string.Empty;
            ShareImportExport.FilterXmlPreSuffixBadCharacter(ref errorList, ref xmlPrefix, ref xmlSuffix, ref xmlMainData, originalXml);
            if (xmlMainData == null || xmlMainData == string.Empty)
            {
                return xmlMainData;
            }

            WhitelistData whitelist = whitelistDictionary[tableName];
            bool allNonReviewInWhitelist = (whitelist == null) || (whitelist.NoImportExportSet.Count() + whitelist.ProtectedDictionary.Count() + whitelist.ReviewSet.Count() + whitelist.ExportSet.Count() == 0) ? true : false;
            StringBuilder newXmlBuilder = new StringBuilder();
            XmlReaderSettings readSettings = new XmlReaderSettings();
            readSettings.LineNumberOffset = 1;
            readSettings.LinePositionOffset = 4;
            readSettings.IgnoreWhitespace = true;

            XmlWriterSettings writeSettings = new XmlWriterSettings();
            writeSettings.Indent = true;
            writeSettings.ConformanceLevel = ConformanceLevel.Auto;
            using (XmlReader reader = XmlReader.Create(new StringReader(xmlMainData), readSettings))
            {
                using (XmlWriter writer = XmlWriter.Create(new StringWriter(newXmlBuilder), writeSettings))
                {
                    Stack<string> elementNmStack = new Stack<string>();

                    bool isCurElementWhitelistCol = true;
                    while (reader.Read())
                    {
                        XmlNodeType nodeType = reader.NodeType;
                        if (nodeType == XmlNodeType.Element)
                        {
                            string filteredVal = null;
                            writer.WriteStartElement(reader.Name);
                            elementNmStack.Push(reader.Name);
                            isCurElementWhitelistCol = true;
                            if (reader.HasAttributes)
                            {
                                int attrSize = reader.AttributeCount;
                                string curColNm = null;
                                for (int i = 0; i < attrSize; i++)
                                {
                                    reader.MoveToAttribute(i);
                                    if (reader.Name.Contains(':'))
                                    {
                                        writer.WriteAttributeString(reader.Prefix, reader.LocalName, null, reader.Value);
                                    }
                                    else if (whitelistDictionary.ContainsKey(tableName) &&
                                        whitelistDictionary[tableName].ProtectedDictionary.ContainsKey(reader.Name) &&
                                        whitelistDictionary[tableName].ProtectedDictionary[reader.Name].StartsWith("{XmlAttr|Col"))
                                    {
                                        if (whitelistDictionary[tableName].ProtectedDictionary[reader.Name] == "{XmlAttr|ColNm}")
                                        {
                                            curColNm = reader.Value;
                                            writer.WriteAttributeString(reader.Name, reader.Value);
                                            ////FilterValueByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, ref filteredVal, allNonReviewInWhitelist, tableName, curColNm, string.Empty);
                                        }
                                        else if (whitelistDictionary[tableName].ProtectedDictionary[reader.Name] == "{XmlAttr|ColVal}")
                                        {
                                            bool isExportable = FilterValueByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, ref filteredVal, allNonReviewInWhitelist, tableName, curColNm, reader.Value, needDebugInfo);
                                            if (isExportable)
                                            {
                                                writer.WriteAttributeString(reader.Name, filteredVal);
                                            }
                                            else
                                            {
                                                writer.WriteAttributeString(reader.Name, string.Empty);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        FilterValueByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, ref filteredVal, allNonReviewInWhitelist, tableName, reader.Name, reader.Value, needDebugInfo);
                                        writer.WriteAttributeString(reader.Name, filteredVal);
                                    }
                                }

                                reader.MoveToElement();
                            }

                            if (reader.IsEmptyElement)
                            {
                                writer.WriteEndElement();
                                string curElementNm = elementNmStack.Pop();
                                if (isCurElementWhitelistCol &&
                                    whitelist.NoImportExportSet.Contains(curElementNm) == false &&
                                    whitelist.ExportSet.Contains(curElementNm) == false &&
                                    whitelist.ProtectedDictionary.ContainsKey(curElementNm) == false &&
                                    whitelist.ReviewSet.Contains(curElementNm) == false)
                                {
                                    whitelist.MissingSet.Add(curElementNm);
                                }
                            }

                            continue;
                        }
                        else
                        {
                            bool skipThisRound = false;
                            string filteredVal = null;
                            string curElementNm = null;

                            switch (nodeType)
                            {
                                case XmlNodeType.CDATA:
                                    isCurElementWhitelistCol = true;
                                    curElementNm = elementNmStack.Peek();
                                    FilterValueByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, ref filteredVal, allNonReviewInWhitelist, tableName, curElementNm, reader.Value, needDebugInfo);
                                    writer.WriteCData(filteredVal);
                                    break;
                                case XmlNodeType.Comment:
                                    writer.WriteComment(reader.Value);
                                    break;
                                case XmlNodeType.Document:
                                    break;
                                case XmlNodeType.DocumentFragment:
                                    break;
                                case XmlNodeType.DocumentType:
                                    break;
                                case XmlNodeType.EndElement:
                                    writer.WriteEndElement();
                                    curElementNm = elementNmStack.Pop();
                                    if (isCurElementWhitelistCol &&
                                        whitelist.NoImportExportSet.Contains(curElementNm) == false &&
                                        whitelist.ExportSet.Contains(curElementNm) == false &&
                                        whitelist.ProtectedDictionary.ContainsKey(curElementNm) == false &&
                                        whitelist.ReviewSet.Contains(curElementNm) == false)
                                    {
                                        whitelist.MissingSet.Add(curElementNm);
                                    }

                                    isCurElementWhitelistCol = false;
                                    break;
                                case XmlNodeType.EndEntity:
                                    break;
                                case XmlNodeType.Entity:
                                    break;
                                case XmlNodeType.EntityReference:
                                    break;
                                case XmlNodeType.Notation:
                                    break;
                                case XmlNodeType.Text:
                                    isCurElementWhitelistCol = true;
                                    curElementNm = elementNmStack.Peek();
                                    FilterValueByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, ref filteredVal, allNonReviewInWhitelist, tableName, curElementNm, reader.Value, needDebugInfo);
                                    writer.WriteValue(filteredVal);
                                    break;
                                case XmlNodeType.None:
                                case XmlNodeType.Whitespace:
                                case XmlNodeType.XmlDeclaration:
                                case XmlNodeType.SignificantWhitespace:
                                case XmlNodeType.ProcessingInstruction:
                                    skipThisRound = true;
                                    break;
                                default:
                                    throw new ApplicationException("Unknow cdata node type:" + nodeType.ToString());
                            }

                            if (skipThisRound)
                            {
                                continue;
                            }
                        }
                    }
                }
            }

            string finalContent = xmlPrefix + newXmlBuilder.ToString() + xmlSuffix;
            if (finalContent != null)
            {
                if (escapedMethod == "ESCAPE_JSON")
                {
                    finalContent = EscapeJsonCharacter(finalContent);
                }
                else if (escapedMethod == "ESCAPE_XML")
                {
                    finalContent = EscapeXmlCharacter(finalContent);
                }
                else if (escapedMethod == "ESCAPE_JSON_OPTIONAL")
                {
                    finalContent = ShareImportExport.EscapeJsonCharacter_Optional(finalContent);
                }
            }

            return finalContent;
        }        

        /// <summary>
        /// Export all fields of one table into a hash set.
        /// </summary>
        /// <param name="tableNm">Table name of all the fields.</param> 
        /// <returns>Hash Set: column name of this table.</returns>
        public static HashSet<string> GetColumnNamesForTable(string tableNm)
        {
            HashSet<string> reviewSet = new HashSet<string>();
            //// string sql = "select distinct column_name from information_schema.columns where table_name = '" + tableNm + "'";
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetColumnNamesByTableName", new SqlParameter("TableName", tableNm)))
            {
                while (reader.Read())
                {
                    string colNm = Convert.ToString(reader["column_name"]).TrimWhitespaceAndBOM();
                    reviewSet.Add(colNm);
                }
            }

            return reviewSet;
        }

        /// <summary>
        /// Generate a default Comma-Separated Values whitelist at a random file address.
        /// </summary>
        /// <param name="tableNmList">List of table names needed in the white list file.</param> 
        /// <returns>The whitelist address.</returns>
        public static string GetDefaultCsvWhitelistByList(List<string> tableNmList)
        {
            string filepath = TempFileUtils.NewTempFilePath() + ".csv";
            string databaseNm = string.Empty;
            using (StreamWriter writer = new StreamWriter(filepath))
            {
                writer.WriteLine("database_name,table_name,column_name,data_type,replicate-insert-protect-lvl(No/NoExportNoImport/Yes),replicate-update-protect-lvl(use the same policy as insertion if blank),substitution-replicate,overwrite-protect-lvl,stuffing,substitution-overwrite,Notes");
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetTableCatalogByTableName", new SqlParameter("@TableName", "LOAN_FILE_A")))
                {
                    if (reader.Read())
                    {
                        databaseNm = Convert.ToString(reader["table_catalog"]).TrimWhitespaceAndBOM();
                    }
                }

                foreach (string tableNm in tableNmList)
                {
                    // string query = "select column_name, DATA_TYPE from information_schema.columns where table_name = '" + tableNm + "'";
                    using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetColumnNameAndDataTypeByTableName", new SqlParameter("@TableName", tableNm)))
                    {
                        while (reader.Read())
                        {
                            string colNm = Convert.ToString(reader["column_name"]).TrimWhitespaceAndBOM();
                            string dataType = Convert.ToString(reader["DATA_TYPE"]).TrimWhitespaceAndBOM();
                            string entry = databaseNm + "," + tableNm + "," + colNm + "," + dataType + ",Yes," + string.Empty + "," + string.Empty + ",Yes," + string.Empty + "," + string.Empty + "," + string.Empty;
                            writer.WriteLine(entry);
                        }
                    }
                }
            }

            return filepath;
        }

        /// <summary>
        /// Fetch the extra information that are not stored for actual usage but recorded in the white list.
        /// </summary>
        /// <param name="reconstructMap">Some columns in white list are not recorded into the dictionary. But in order to keep the information, we use this "reconstructMap".</param> 
        /// <param name="databaseNm">The name of database.</param> 
        /// <param name="dataType">The data type of identity key.</param> 
        /// <param name="backupExport">The exportable checking field for another tool. For example, if current tool that uses white list is loan overwrite. Then this field is for loan-replicate. It is used to restore the whole white list.</param> 
        /// <param name="backupUpdateProtectLvl">The field for storing the protection level during update.</param>
        /// <param name="backupSubsti">The substitution value of the protected field for another tool.</param> 
        /// <param name="note">The note field at white list file.</param> 
        /// <param name="tableNm">Current table name.</param> 
        /// <param name="col">List of table names needed in the white list file.</param> 
        public static void WhiteList_GetReconstructData(
            ref Dictionary<string, string> reconstructMap,
            ref string databaseNm,
            ref string dataType,
            ref string backupExport,
            ref string backupUpdateProtectLvl,
            ref string backupSubsti,
            ref string note,
            string tableNm,
            string col)
        {
            if (!reconstructMap.ContainsKey(tableNm + "," + col))
            {
                if (tableNm.StartsWith("FILEDB|"))
                {
                    databaseNm = "Undefined DB";
                    dataType = col.StartsWith("Is") ? "bit" : "varchar";
                    backupExport = string.Empty;
                    backupUpdateProtectLvl = string.Empty;
                    backupSubsti = string.Empty;
                    note = string.Empty;
                    reconstructMap.Add(tableNm + "," + col, "Undefined Catalog" + "," + dataType + "," + backupExport + "," + backupUpdateProtectLvl + "," + backupSubsti + "," + note);
                }
                else if (tableNm.StartsWith("ParseXml|") || tableNm.StartsWith("ParseJson|"))
                {
                    databaseNm = string.Empty;
                    dataType = col.StartsWith("Is") ? "bit" : "varchar";
                    backupExport = string.Empty;
                    backupUpdateProtectLvl = string.Empty;
                    backupSubsti = string.Empty;
                    note = string.Empty;
                    reconstructMap.Add(tableNm + "," + col, "Undefined Catalog" + "," + dataType + "," + backupExport + "," + backupUpdateProtectLvl + "," + backupSubsti + "," + note);
                }
                else
                {
                    var parameters = new List<SqlParameter>()
                    {
                        new SqlParameter("@TableName", tableNm),
                        new SqlParameter("@ColumnName", col)
                    };

                    using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetCatalogNameAndDataTypeByTableNameAndColumnName", parameters))
                    {
                        if (reader.Read())
                        {
                            databaseNm = Convert.ToString(reader["table_catalog"]).TrimWhitespaceAndBOM();
                            dataType = Convert.ToString(reader["data_type"]).TrimWhitespaceAndBOM();
                            backupExport = string.Empty;
                            backupUpdateProtectLvl = string.Empty;
                            backupSubsti = string.Empty;
                            note = string.Empty;
                            reconstructMap.Add(tableNm + "," + col, databaseNm + "," + dataType + "," + backupExport + "," + backupUpdateProtectLvl + "," + backupSubsti + "," + note);
                        }
                    }
                }
            }
            else
            {
                string[] tokens = reconstructMap[tableNm + "," + col].Split(',');
                databaseNm = tokens[0];
                dataType = tokens[1];
                backupExport = tokens[2];
                backupUpdateProtectLvl = tokens[3];
                backupSubsti = tokens[4];
                note = tokens[5];
            }
        }

        /// <summary>
        /// Get a primary key dictionary of the whole database.
        /// </summary>
        /// <param name="connSrc">Data source to connect to.</param>
        /// <returns>Dictionary: table_name -> primary_key.</returns>
        public static Dictionary<string, List<string>> GetPrimaryKeyDictionary(DataSrc connSrc)
        {
            Dictionary<string, List<string>> primaryKeyDictionary = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
            using (var reader = StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_GetColumnPropertyByAllTable", new SqlParameter("@Property", "IsPrimaryKey")))
            {
                while (reader.Read())
                {
                    string curTableNm = Convert.ToString(reader["table_name"]).TrimWhitespaceAndBOM();
                    string curColNm = Convert.ToString(reader["column_name"]).TrimWhitespaceAndBOM();
                    int isIdentity = Convert.ToInt32(reader["is_identity"]);
                    List<string> tlist = null;
                    if (!primaryKeyDictionary.ContainsKey(curTableNm))
                    {
                        primaryKeyDictionary.Add(curTableNm, new List<string>());
                    }

                    tlist = primaryKeyDictionary[curTableNm];
                    if (!tlist.Contains(curColNm))
                    {
                        if (isIdentity == 0)
                        {
                            tlist.Add(curColNm);
                        }
                        else
                        {
                            // try to put identity key into the first place so that it will be at the first place in the xml. For test purpose
                            tlist.Insert(0, curColNm); 
                        }
                    }
                }
            }

            // hardcode
            if (!primaryKeyDictionary.ContainsKey("BROKER"))
            {
                primaryKeyDictionary.Add("BROKER", new List<string>());
            }

            primaryKeyDictionary["BROKER"].Add("CustomerCode");

            if (!primaryKeyDictionary.ContainsKey("TASK_PERMISSION_LEVEL"))
            {
                primaryKeyDictionary.Add("TASK_PERMISSION_LEVEL", new List<string>());
            }

            primaryKeyDictionary["TASK_PERMISSION_LEVEL"].Add("Name");

            if (!primaryKeyDictionary.ContainsKey("CONDITION_CATEGORY"))
            {
                primaryKeyDictionary.Add("CONDITION_CATEGORY", new List<string>());
            }

            primaryKeyDictionary["CONDITION_CATEGORY"].Add("Category");

            ////hardcode some tables that only have foreign key:
            if (!primaryKeyDictionary.ContainsKey("DAILY_REPORTS"))
            {
                primaryKeyDictionary.Add("DAILY_REPORTS", new List<string>());
            }

            if (!primaryKeyDictionary.ContainsKey("EDOCS_ACTIVE_STANDARD_SHIPPING_TEMPLATE_BY_BROKERID"))
            {
                primaryKeyDictionary.Add("EDOCS_ACTIVE_STANDARD_SHIPPING_TEMPLATE_BY_BROKERID", new List<string>());
            }

            if (!primaryKeyDictionary.ContainsKey("EDOCS_FOLDER_X_ROLE"))
            {
                primaryKeyDictionary.Add("EDOCS_FOLDER_X_ROLE", new List<string>());
            }

            if (!primaryKeyDictionary.ContainsKey("FIRST_AMERICAN_QUOTE_POLICY_AVAILABLE"))
            {
                primaryKeyDictionary.Add("FIRST_AMERICAN_QUOTE_POLICY_AVAILABLE", new List<string>());
            }

            if (!primaryKeyDictionary.ContainsKey("IRS_4506T_VENDOR_EMPLOYEE_INFO"))
            {
                primaryKeyDictionary.Add("IRS_4506T_VENDOR_EMPLOYEE_INFO", new List<string>());
            }

            if (!primaryKeyDictionary.ContainsKey("MONTHLY_REPORTS"))
            {
                primaryKeyDictionary.Add("MONTHLY_REPORTS", new List<string>());
            }

            if (!primaryKeyDictionary.ContainsKey("WEEKLY_REPORTS"))
            {
                primaryKeyDictionary.Add("WEEKLY_REPORTS", new List<string>());
            }

            if (!primaryKeyDictionary.ContainsKey("LICENSE"))
            {
                primaryKeyDictionary.Add("LICENSE", new List<string>());
            }

            primaryKeyDictionary["DAILY_REPORTS"].Add("ReportId");
            primaryKeyDictionary["EDOCS_ACTIVE_STANDARD_SHIPPING_TEMPLATE_BY_BROKERID"].Add("DeployingBrokerId");
            primaryKeyDictionary["EDOCS_ACTIVE_STANDARD_SHIPPING_TEMPLATE_BY_BROKERID"].Add("ShippingTemplateId");
            primaryKeyDictionary["EDOCS_FOLDER_X_ROLE"].Add("FolderId");
            primaryKeyDictionary["EDOCS_FOLDER_X_ROLE"].Add("RoleId");
            primaryKeyDictionary["EDOCS_FOLDER_X_ROLE"].Add("IsPmlUser");
            primaryKeyDictionary["FIRST_AMERICAN_QUOTE_POLICY_AVAILABLE"].Add("BrokerId");
            primaryKeyDictionary["FIRST_AMERICAN_QUOTE_POLICY_AVAILABLE"].Add("PolicyId");
            primaryKeyDictionary["IRS_4506T_VENDOR_EMPLOYEE_INFO"].Add("UserId");
            primaryKeyDictionary["IRS_4506T_VENDOR_EMPLOYEE_INFO"].Add("VendorId");
            primaryKeyDictionary["MONTHLY_REPORTS"].Add("ReportId");
            primaryKeyDictionary["WEEKLY_REPORTS"].Add("ReportId");
            //// primaryKeyDictionary["LICENSE"].Add("LicenseNumber");
            return primaryKeyDictionary;
        }

        /*/// <summary>
        /// Some entries might not be queried through primary keys, for example, select loan file by broker id. In this case, we need to transform the entry so that stored procedure can accept it since we only query by primary keys.
        /// </summary>
        /// <param name="primaryKeyDictionary">The primary key dictionary.</param>
        /// <param name="inputEntry">The input entry, which might represents several table entry.</param>
        /// <returns>The list of formatted entry.</returns>
        public static List<string> GetCorrectEntryListByPrimaryKey(Dictionary<string, List<string>> primaryKeyDictionary, string inputEntry)
        {
            bool needReQueryToGenerateEntriesByPrimaryKeys = false;
            List<string> entryList = new List<string>();
            string[] tokens = inputEntry.Split('|');
            string tableNm = tokens[0];
            List<string> priKeyList = primaryKeyDictionary[tableNm];

            int size = tokens.Count();
            StringBuilder whereCont = new StringBuilder();
            List<SqlParameter> paraList = new List<SqlParameter>();
            for (int i = 1; i < size - 1; i += 2)
            {
                whereCont.Append(tokens[i] + " = @" + tokens[i] + " and ");
                paraList.Add(new SqlParameter("@" + tokens[i], tokens[i + 1]));
            }

            foreach (string priKey in priKeyList)
            {
                if (!tokens.Contains(priKey))
                {
                    needReQueryToGenerateEntriesByPrimaryKeys = true;
                    break;
                }
            }

            if (needReQueryToGenerateEntriesByPrimaryKeys)
            {
                string query = "SELECT * FROM [" + tableNm + "] WHERE " + whereCont.ToString().Substring(0, whereCont.Length - 5);
                using (var reader = DataAccess.DbAccessUtils.ExecuteReaderDangerously(query, paraList.ToArray()))
                {
                    while (reader.Read())
                    {
                        StringBuilder entry = new StringBuilder(tableNm + "|");
                        foreach (string priKey in priKeyList)
                        {
                            entry.Append(priKey + "|" + Convert.ToString(reader[priKey]).TrimWhitespaceAndBOM() + "|");
                        }

                        entryList.Add(entry.ToString().TrimEnd('|'));
                    }
                }
            }
            else
            {
                entryList.Add(inputEntry);
            }

            return entryList;
        }*/

        /// <summary>
        /// Get dictionary of the whole database to fetch foreign key default value. <para></para>
        /// Structure: table_name|foreign_key_name -> default_value.
        /// </summary>
        /// <returns>Dictionary: table_name|foreign_key -> default_value.</returns>
        public static Dictionary<string, string> GetDefaultForeignKeyValDictionary()
        {
            Dictionary<string, string> defaultFkValDictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            defaultFkValDictionary.Add("LOAN_FILE_E|sInvestorRolodexId", "-1");
            return defaultFkValDictionary;
        }

        /// <summary>
        /// Get the list of column name for each unique index of each table.
        /// </summary>
        /// <param name="connSrc">Data source to connect to.</param>
        /// <returns>A dictionary that maps table name to a list of unique indexes while each index contains a list of columns.</returns>
        public static Dictionary<string, List<List<string>>> GetUniqueIdxColDictionary(DataSrc connSrc)
        {
            Dictionary<string, Dictionary<string, List<string>>> tableIdxColDictionary = new Dictionary<string, Dictionary<string, List<string>>>(StringComparer.OrdinalIgnoreCase);
            using (var reader = StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_GetAllUniqueIndexColumnNames"))
            {
                while (reader.Read())
                {
                    string tableName = Convert.ToString(reader["tableName"]).TrimWhitespaceAndBOM();
                    string columnName = Convert.ToString(reader["columnName"]).TrimWhitespaceAndBOM();
                    string idxName = Convert.ToString(reader["indexName"]).TrimWhitespaceAndBOM();
                    if (!tableIdxColDictionary.ContainsKey(tableName))
                    {
                        tableIdxColDictionary.Add(tableName, new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase));
                    }

                    if (!tableIdxColDictionary[tableName].ContainsKey(idxName))
                    {
                        tableIdxColDictionary[tableName].Add(idxName, new List<string>());
                    }

                    tableIdxColDictionary[tableName][idxName].Add(columnName);
                }
            }

            Dictionary<string, List<List<string>>> result = new Dictionary<string, List<List<string>>>();
            Dictionary<string, Dictionary<string, List<string>>>.KeyCollection allTables = tableIdxColDictionary.Keys;
            foreach (string tableNm in allTables)
            {
                result.Add(tableNm, new List<List<string>>());
                Dictionary<string, List<string>>.KeyCollection allIdx = tableIdxColDictionary[tableNm].Keys;
                foreach (string index in allIdx)
                {
                    result[tableNm].Add(tableIdxColDictionary[tableNm][index]);
                }
            }

            return result;
        }

        /// <summary>
        /// Quickly check if any column belongs to unique index.
        /// </summary>
        /// <returns>A combo set of table and column name.</returns>
        public static HashSet<string> GetUniqueIndexColTableComboSet_QuickCheck()
        {
            HashSet<string> comboSet = new HashSet<string>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetAllUniqueIndexColumnNames"))
            {
                while (reader.Read())
                {
                    string tableNm = Convert.ToString(reader["tableName"]).TrimWhitespaceAndBOM();
                    string colNm = Convert.ToString(reader["columnName"]).TrimWhitespaceAndBOM();
                    if (!comboSet.Contains(tableNm + "|" + colNm))
                    {
                        comboSet.Add(tableNm + "|" + colNm);
                    }
                }
            }

            return comboSet;
        }

        /// <summary>
        /// Quickly check if any column belongs to complex check constraint that are difficult to assign default value to that column.
        /// </summary>
        /// <param name="connSrc">Data source to connect to.</param>
        /// <returns>A combo set of table and column name.</returns>
        public static Dictionary<string, List<string>> GetCheckConstraintColTableComboSet_QuickCheck(DataSrc connSrc)
        {
            Dictionary<string, List<string>> chkDic = new Dictionary<string, List<string>>(StringComparer.OrdinalIgnoreCase);
            using (var reader = StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_GetCheckConstraintByPattern", new SqlParameter("@LikePattern", "%] IS NOT NULL)")))
            {
                while (reader.Read())
                {
                    string colNm = reader["columnName"].ToString().TrimWhitespaceAndBOM();
                    string tableNm = reader["tableName"].ToString().TrimWhitespaceAndBOM();
                    if (!chkDic.ContainsKey(tableNm))
                    {
                        chkDic.Add(tableNm, new List<string>());
                    }

                    chkDic[tableNm].Add(colNm);
                }
            }

            return chkDic;
        }

        /// <summary>
        /// Get identity key dictionary of the whole database. Data: identity_table -> identity_key.
        /// </summary>
        /// <param name="dataSrc">Data source to connect to.</param>
        /// <returns>Identity key dictionary.</returns>
        public static Dictionary<string, string> GetIdentityTableKeyDictionary(DataSrc dataSrc)
        {
            Dictionary<string, string> identityTableKeyDictionary = new Dictionary<string, string>();
            
            using (var reader = StoredProcedureHelper.ExecuteReader(dataSrc, "Justin_Testing_GetIdentityColumns"))
            {
                while (reader.Read())
                {
                    string tableNm = reader["table_name"].ToString();
                    string colNm = reader["column_name"].ToString();
                    if (!identityTableKeyDictionary.ContainsKey(tableNm))
                    {
                        identityTableKeyDictionary.Add(tableNm, colNm);
                    }
                }
            }

            return identityTableKeyDictionary;
        }

        /// <summary>
        /// Given a table name, give the list of identities that it foreign keys to. <para></para>
        /// Structure: referencing_table_name -> list of referenced_identity_key_table_name|referenced_identity_key_column_name|referencing_column_name.
        /// </summary>
        /// <param name="connectionSource">Data source to connect to.</param>
        /// <returns>Dictionary: referencing_table_name -> list of referenced_identity_key_table_name|referenced_identity_key_column_name|referencing_column_name.</returns>
        public static Dictionary<string, List<string>> GetIdentityKeyRefereeTrioListByTableName(DataSrc connectionSource)
        {
            Dictionary<string, List<string>> identityKeyRefereesByTableName = new Dictionary<string, List<string>>();

            // Do the hardcode first.
            identityKeyRefereesByTableName.Add("BROKER", new List<string>());
            identityKeyRefereesByTableName.Add("LOAN_FILE_C", new List<string>());
            identityKeyRefereesByTableName.Add("LOAN_FILE_E", new List<string>());
            identityKeyRefereesByTableName.Add("Q_MESSAGE", new List<string>());

            identityKeyRefereesByTableName["BROKER"].Add("TITLE_VENDOR|TitleVendorId|ClosingCostTitleVendorId");
            identityKeyRefereesByTableName["LOAN_FILE_C"].Add("WAREHOUSE_LENDER_ROLODEX|WarehouseLenderRolodexId|sWarehouseLenderRolodexId");
            identityKeyRefereesByTableName["LOAN_FILE_E"].Add("INVESTOR_ROLODEX|InvestorRolodexId|sInvestorRolodexId");
            identityKeyRefereesByTableName["Q_MESSAGE"].Add("Q_TYPE|QueueId|QueueId");

            // Then do the dynamic one.
            using (var reader = StoredProcedureHelper.ExecuteReader(connectionSource, "Justin_Testing_GetAllForeignReferenceKeyAndTables", new SqlParameter("@IsIdentity", 1)))
            {
                while (reader.Read())
                {
                    string rkeyTable = reader["REFERENCED_TABLE_NAME"].ToString();
                    string rkeyCol = reader["REFERENCED_COLUMN_NAME"].ToString();
                    string fkeyTable = reader["FK_TABLE_NAME"].ToString();
                    string fkeyCol = reader["FK_COLUMN_NAME"].ToString();
                    if (!identityKeyRefereesByTableName.ContainsKey(fkeyTable))
                    {
                        identityKeyRefereesByTableName.Add(fkeyTable, new List<string>());
                    }

                    identityKeyRefereesByTableName[fkeyTable].Add(rkeyTable + "|" + rkeyCol + "|" + fkeyCol);
                }
            }

            return identityKeyRefereesByTableName;
        }

        /// <summary>
        /// Import file_database data related to loan: liability xml and asset xml.
        /// </summary>
        /// <param name="errorList">Recorded any errors happen during the import.</param> 
        /// <param name="filedbXmlContent">This includes liability xml and asset xml.</param> 
        public static void ImportFiledb_old(ref List<string> errorList, XElement filedbXmlContent)
        {
            Dictionary<string, LargeFieldStorage> filedbPerLoanDictionary = new Dictionary<string, LargeFieldStorage>();
            IEnumerable<XElement> filedbElements = filedbXmlContent.Elements();
            Dictionary<string, string> appIdFiledbDictionary = new Dictionary<string, string>();
            foreach (XElement filedbElement in filedbElements)
            {
                string xmlFiledbKey = filedbElement.Attribute("filedbKey").Value;

                string loanId = filedbElement.Attribute("slid").Value;
                string xmlAppId = filedbElement.Attribute("aAppId").Value;
                string tagNm = filedbElement.Name.ToString();
                string cdataXml = filedbElement.Value;
                if (!filedbPerLoanDictionary.ContainsKey(loanId))
                {
                    filedbPerLoanDictionary.Add(loanId, new LargeFieldStorage(new Guid(loanId)));
                    using (var sqlReader = StoredProcedureHelper.ExecuteReader(
                        DataSrc.LOShare,
                        "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                        new SqlParameter("@IsByTableName", 1),
                        new SqlParameter("@UserOption", "MAIN_DB"),
                        new SqlParameter("@InputTableName", "APPLICATION_B_byLoanId"),
                        new SqlParameter("@varcharKey1", loanId)))
                    {
                        while (sqlReader.Read())
                        {
                            string sqlAppId = sqlReader["aAppId"].ToString();
                            string sqlLiabilityKey = sqlReader["aLiaXmlContent"].ToString();
                            string sqlAssetKey = sqlReader["aAssetXmlContent"].ToString();
                            if (!appIdFiledbDictionary.ContainsKey(sqlAppId))
                            {
                                appIdFiledbDictionary.Add(sqlAppId, sqlLiabilityKey + "|" + sqlAssetKey);
                            }
                        }
                    }
                }

                if (appIdFiledbDictionary.ContainsKey(xmlAppId))
                {
                    string[] tokens = appIdFiledbDictionary[xmlAppId].Split('|');
                    int filedbInd = tagNm == "aLiaXmlContent" ? 0 : 1;
                    if (tokens[filedbInd] == Guid.Empty.ToString())
                    {
                        Guid newFiledbKeyGuid = Guid.NewGuid();
                        tokens[filedbInd] = newFiledbKeyGuid.ToString();
                        StoredProcedureHelper.ExecuteNonQuery(
                            DataSrc.LOShare,
                            "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers",
                            0,
                            new SqlParameter("@IsByTableName", 1),
                            new SqlParameter("@UserOption", "UpdateFiledbKeyInApplication"),
                            new SqlParameter("@varcharKey1", tokens[0]),
                            new SqlParameter("@varcharKey2", tokens[1]),
                            new SqlParameter("@varcharKey3", xmlAppId));
                    }

                    LargeFieldStorage curLoanFiledb = filedbPerLoanDictionary[loanId];
                    curLoanFiledb.SetText(new Guid(tokens[filedbInd]), cdataXml);
                }
                else if (xmlAppId != Guid.Empty.ToString())
                {
                    errorList.Add("Justin: Unrecognized aAppId[" + xmlAppId + "] in xml filedb part.");
                    Tools.LogError("Justin: Unrecognized aAppId[" + xmlAppId + "] in xml filedb part.");
                    continue;
                }
            }

            Dictionary<string, LargeFieldStorage>.ValueCollection filedbCollection = filedbPerLoanDictionary.Values;
            foreach (LargeFieldStorage filedb in filedbCollection)
            {
                filedb.Flush();
            }
        }

        /// <summary>
        /// Import FileDB by the loan id and app id.
        /// </summary>
        /// <param name="errorList">List records errors in the transaction and before transaction.</param> 
        /// <param name="filedbXmlContent">Xml of file database, including assets and liability.</param> 
        /// <param name="loanId">The id of loan.</param> 
        /// <param name="xmlAppIdMapToDbAppId">Dictionary maps the application id in xml to the one in database.</param> 
        public static void ImportFiledbToOneLoan(ref List<string> errorList, XElement filedbXmlContent, string loanId, Dictionary<string, string> xmlAppIdMapToDbAppId)
        {
            IEnumerable<XElement> nodes = filedbXmlContent.Elements();
            int debugInd = 0;
            XElement testnode = null;
            foreach (XElement node in nodes)
            {
                testnode = node;
                ++debugInd;
                string tagNm = node.Name.ToString();
                string cdataXml = node.Value;
                string xmlAppId = node.Attribute("aAppId").Value;
                string dbaseAppId = xmlAppIdMapToDbAppId[xmlAppId];
                if (tagNm == "aLiaXmlContent")
                {
                    ShareImportExport.SetLiaXmlRecord(ref errorList, cdataXml, loanId, dbaseAppId);
                }
                else if (tagNm == "aAssetXmlContent")
                {
                    ShareImportExport.SetAssetXmlRecord(ref errorList, cdataXml, loanId, dbaseAppId);
                }
            }
        }

        /// <summary>
        /// New version white list reader using new data structure "WhitelistData". Read from Comma-separated values file.
        /// </summary>
        /// <param name="errorList">Recorded any errors happen during the import.</param> 
        /// <param name="reconstructMap">Some extra fields not used yet are stored there to rebuild the file.</param> 
        /// <param name="whitelistDictionary">Store white list data.</param> 
        /// <param name="filepath">The path to store the white list "Comma-separated values" file.</param> 
        /// <param name="isLoanOverwrite">If the whitelist is for loan overwrite, choose true. If for loan replicate, set false.</param> 
        public static void ReadCsvWhiteList(
            ref List<string> errorList,
            out Dictionary<string, string> reconstructMap,
            out Dictionary<string, WhitelistData> whitelistDictionary,
            string filepath,
            bool isLoanOverwrite)
        {
            if (FileOperationHelper.Exists(filepath) == false)
            {
                errorList.Add("The whitelist is not found at " + filepath.Replace("\\", "\\\\"));
                reconstructMap = null;
                whitelistDictionary = null;
                return;
            }

            reconstructMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            whitelistDictionary = new Dictionary<string, WhitelistData>(StringComparer.OrdinalIgnoreCase);
            int curExportInd = isLoanOverwrite ? 7 : 4;
            int backupExpInd = isLoanOverwrite ? 4 : 7;
            int curSubstituteInd = isLoanOverwrite ? 9 : 6;
            int backupSubstituteInd = isLoanOverwrite ? 6 : 9;
            int curUpdateLvlInd = isLoanOverwrite ? 8 : 5;
            int backupUpdateLvlInd = isLoanOverwrite ? 5 : 8;

            using (var fs = new FileStream(filepath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (StreamReader reader = new StreamReader(fs))
            {
                string line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    if (!line.Contains(','))
                    {
                        continue; ////blank line. But not work for ",,,,,,"
                    }

                    string[] tokens = line.Split(',');
                    if (tokens[0] == "database_name")
                    {
                        continue;
                    }

                    var tableName = tokens[1];
                    var columnName = tokens[2];

                    if (!whitelistDictionary.ContainsKey(tableName))
                    {
                        whitelistDictionary.Add(tableName, new WhitelistData());
                    }

                    if (tokens[curExportInd] == "Yes")
                    {
                        ////export
                        if (whitelistDictionary[tableName].ExportSet.Contains(columnName) == false)
                        {
                            whitelistDictionary[tableName].ExportSet.Add(columnName);
                        }
                    }
                    else if (tokens[curExportInd] == "No")
                    {
                        ////protect
                        if (whitelistDictionary[tableName].ProtectedDictionary.ContainsKey(columnName) == false)
                        {
                            whitelistDictionary[tableName].ProtectedDictionary.Add(columnName, tokens[curSubstituteInd]);
                        }
                    }
                    else if (tokens[curExportInd] == "NoExportNoImport")
                    {
                        //// NoExportNoImport
                        if (whitelistDictionary[tableName].NoImportExportSet.Contains(columnName) == false)
                        {
                            whitelistDictionary[tableName].NoImportExportSet.Add(columnName);
                        }
                    }
                    else if (!whitelistDictionary[tableName].ReviewSet.Contains(columnName))
                    {
                        ////Review
                        whitelistDictionary[tableName].ReviewSet.Add(columnName);
                    }

                    if (!isLoanOverwrite)
                    {
                        if (tokens[curExportInd] != "No" && tokens[curUpdateLvlInd] == "No" && whitelistDictionary[tableName].UpdateProtectDictionary.ContainsKey(columnName) == false)
                        {
                            whitelistDictionary[tableName].UpdateProtectDictionary.Add(columnName, tokens[curSubstituteInd]);
                        }
                        else if (tokens[curUpdateLvlInd] == "NoExportNoImport" && whitelistDictionary[tableName].NoUpdateSet.Contains(columnName) == false)
                        {
                            whitelistDictionary[tableName].NoUpdateSet.Add(columnName);
                        }
                    }

                    if (!reconstructMap.ContainsKey(tableName + "," + columnName))
                    {
                        reconstructMap.Add(tableName + "," + columnName, tokens[0] + "," + tokens[3] + "," + tokens[backupExpInd] + "," + tokens[backupUpdateLvlInd] + "," + tokens[backupSubstituteInd] + "," + tokens[10]);
                    }
                }
            }
        }

        /// <summary>
        /// Reads the csv file containing implicit references to identity keys and reports errors and line numbers, if any. <para></para>
        /// Otherwise returns the list of reference objects for each row.
        /// </summary>
        /// <param name="filePath">The path to the csv file containing the references.</param>
        /// <param name="errors">The errors by line number, or null if none.</param>
        /// <returns>The list of reference objects for each row, or null if there are errors.</returns>
        public static List<ValidatedImplicitIdentityKeyReference> ReadImplicitIdentityKeyReferencesFile(
            string filePath,
            out ILookup<int, string> errors)
        {
            ILookup<int, string> parseErrors;
            var dt = Common.TextImport.DataParsing.FileToDataTable(filePath, true, null, out parseErrors);

            if (parseErrors.Any())
            {
                errors = parseErrors;
                return null;
            }

            var implicitReferences = new List<ValidatedImplicitIdentityKeyReference>();
            var validationErrorsByRowNumber = new Dictionary<int, IEnumerable<string>>();
            int rowNumber = 1;
            foreach (DataRow row in dt.Rows)
            {
                UnvalidatedImplicitIdentityKeyReference u = new UnvalidatedImplicitIdentityKeyReference()
                {
                    ReferencingDataSource = (string)row["ReferencingDataSource"],
                    ReferencingTableName = (string)row["ReferencingTableName"],
                    ReferencingColumnName = (string)row["ReferencingColumnName"],
                    IdentityKeyDataSource = (string)row["IdentityKeyDataSource"],
                    IdentityKeyTableName = (string)row["IdentityKeyTableName"],
                    IdentityKeyColumName = (string)row["IdentityKeyColumnName"],
                    Type = (string)row["Type"],
                    XPath = (string)row["XPath"],
                    AttributeName = (string)row["AttributeName"]
                };
                ValidatedImplicitIdentityKeyReference v;
                var validationErrors = ValidatedImplicitIdentityKeyReference.GetValidInstanceOrReasonsInvalid(u, out v);
                if (validationErrors.Any())
                {
                    validationErrorsByRowNumber.Add(rowNumber, validationErrors);
                }
                else
                {
                    implicitReferences.Add(v);
                }

                rowNumber++;
            }

            if (validationErrorsByRowNumber.Any())
            {
                errors = validationErrorsByRowNumber
                            .SelectMany(pair => pair.Value, (pair, Value) => new { pair.Key, Value })
                            .ToLookup(pair => pair.Key, pair => pair.Value);
                return null;
            }

            errors = null;
            return implicitReferences;
        }

        /// <summary>
        /// New version white list writer using new data structure "WhitelistData". This write function should only call at export side.
        /// </summary>
        /// <param name="errorList">Recorded any errors happen during the import.</param> 
        /// <param name="reconstructMap">Some extra fields not used yet are stored there to rebuild the file.</param> 
        /// <param name="whitelistDictionary">Store white list data.</param> 
        /// <param name="filepath">The path to store the white list "Comma-separated values" file.</param> 
        /// <param name="isLoanOverwrite">If the whitelist is for loan overwrite, choose true. If for loan replicate, set false.</param> 
        public static void WriteCsvWhistList(
            ref List<string> errorList,
            ref Dictionary<string, string> reconstructMap,
            Dictionary<string, WhitelistData> whitelistDictionary,
            string filepath,
            bool isLoanOverwrite)
        {
            int curExportInd = isLoanOverwrite ? 7 : 4;
            int backupExpInd = isLoanOverwrite ? 4 : 7;
            int curSubstituteInd = isLoanOverwrite ? 9 : 6;
            int backupSubstituteInd = isLoanOverwrite ? 6 : 9;
            int curUpdateLvlInd = isLoanOverwrite ? 8 : 5;
            int backupUpdateLvlInd = isLoanOverwrite ? 5 : 8;
            string delimiter = ",";
            using (StreamWriter writer = new StreamWriter(filepath))
            {
                writer.WriteLine("database_name,table_name,column_name,data_type,replicate-insert-protect-lvl(No/NoExportNoImport/Yes),replicate-update-protect-lvl(use the same policy as insertion if blank),substitution-replicate,overwrite-protect-lvl,stuffing,substitution-overwrite,Notes");
                Dictionary<string, WhitelistData>.KeyCollection keys = whitelistDictionary.Keys;
                string databaseNm = string.Empty, dataType = string.Empty, note = string.Empty, backupExport = string.Empty, backupSubsti = string.Empty, backupUpdateProtectLvl = string.Empty;
                foreach (string tableNm in keys)
                {
                    HashSet<string> missingColumnNames = whitelistDictionary[tableNm].MissingSet;
                    foreach (string col in missingColumnNames)
                    {
                        WhiteList_GetReconstructData(ref reconstructMap, ref databaseNm, ref dataType, ref backupExport, ref backupUpdateProtectLvl, ref backupSubsti, ref note, tableNm, col);
                        string[] row = new string[] { databaseNm, tableNm, col, dataType, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, "Missing" };
                        row[backupExpInd] = backupExport;
                        row[backupSubstituteInd] = backupSubsti;
                        row[backupUpdateLvlInd] = backupUpdateProtectLvl;
                        writer.WriteLine(string.Join(delimiter, row));
                    }
                }

                foreach (string tableNm in keys)
                {
                    HashSet<string> columnsToReview = whitelistDictionary[tableNm].ReviewSet;
                    
                    foreach (string col in columnsToReview)
                    {
                        WhiteList_GetReconstructData(ref reconstructMap, ref databaseNm, ref dataType, ref backupExport, ref backupUpdateProtectLvl, ref backupSubsti, ref note, tableNm, col);
                        string[] row = new string[] { databaseNm, tableNm, col, dataType, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, note };
                        row[backupExpInd] = backupExport;
                        row[backupSubstituteInd] = backupSubsti;
                        row[backupUpdateLvlInd] = backupUpdateProtectLvl;
                        writer.WriteLine(string.Join(delimiter, row));
                    }
                }

                foreach (string tableNm in keys)
                {
                    HashSet<string> noneImportExportSet = whitelistDictionary[tableNm].NoImportExportSet;
                    foreach (string expFieldNm in noneImportExportSet)
                    {
                        WhiteList_GetReconstructData(ref reconstructMap, ref databaseNm, ref dataType, ref backupExport, ref backupUpdateProtectLvl, ref backupSubsti, ref note, tableNm, expFieldNm);
                        string[] row = new string[] { databaseNm, tableNm, expFieldNm, dataType, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, note };
                        row[backupExpInd] = backupExport;
                        row[backupSubstituteInd] = backupSubsti;
                        row[curExportInd] = "NoExportNoImport";
                        row[backupUpdateLvlInd] = backupUpdateProtectLvl;
                        writer.WriteLine(string.Join(delimiter, row));
                    }
                }

                foreach (string tableNm in keys)
                {
                    Dictionary<string, string>.KeyCollection protectFieldList = whitelistDictionary[tableNm].ProtectedDictionary.Keys;
                    foreach (string proField in protectFieldList)
                    {
                        WhiteList_GetReconstructData(ref reconstructMap, ref databaseNm, ref dataType, ref backupExport, ref backupUpdateProtectLvl, ref backupSubsti, ref note, tableNm, proField);
                        string[] row = new string[] { databaseNm, tableNm, proField, dataType, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, note };
                        row[backupExpInd] = backupExport;
                        row[backupSubstituteInd] = backupSubsti;
                        row[curExportInd] = "No";
                        row[curSubstituteInd] = whitelistDictionary[tableNm].ProtectedDictionary[proField];
                        row[backupUpdateLvlInd] = backupUpdateProtectLvl;
                        if (!isLoanOverwrite)
                        {
                            if (whitelistDictionary[tableNm].NoUpdateSet.Contains(proField))
                            {
                                row[curUpdateLvlInd] = "NoExportNoImport";
                            }
                            else
                            {
                                row[curUpdateLvlInd] = "No";
                            }
                        }

                        writer.WriteLine(string.Join(delimiter, row));
                    }
                }

                foreach (string tableNm in keys)
                {
                    HashSet<string> exportList = whitelistDictionary[tableNm].ExportSet;
                    foreach (string expFieldNm in exportList)
                    {
                        WhiteList_GetReconstructData(ref reconstructMap, ref databaseNm, ref dataType, ref backupExport, ref backupUpdateProtectLvl, ref backupSubsti, ref note, tableNm, expFieldNm);
                        string[] row = new string[] { databaseNm, tableNm, expFieldNm, dataType, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, note };
                        row[backupExpInd] = backupExport;
                        row[backupSubstituteInd] = backupSubsti;
                        row[curExportInd] = "Yes";
                        row[backupUpdateLvlInd] = backupUpdateProtectLvl;
                        if (!isLoanOverwrite)
                        {
                            if (whitelistDictionary[tableNm].NoUpdateSet.Contains(expFieldNm))
                            {
                                row[curUpdateLvlInd] = "NoExportNoImport";
                            }
                            else if (whitelistDictionary[tableNm].UpdateProtectDictionary.ContainsKey(expFieldNm))
                            {
                                row[curUpdateLvlInd] = "No";
                                row[curSubstituteInd] = whitelistDictionary[tableNm].UpdateProtectDictionary[expFieldNm];
                            }
                            else
                            {
                                row[curUpdateLvlInd] = "Yes";
                            }
                        }

                        writer.WriteLine(string.Join(delimiter, row));
                    }
                }
            }
        }

        /// <summary>
        /// Check if nothing in database needs to be reviewed in white list and nothing in white list needs to be reviewed.
        /// </summary>
        /// <param name="errorList">The list records error.</param>
        /// <param name="whitelistDictionary">The resulting dictionary holding new info like the review set.</param>
        /// <param name="reconstructMap">Some extra fields not used yet are stored there to rebuild the file.</param> 
        /// <param name="whitelistPath">The address of the original whitelist.</param>
        /// <returns>If true, means nothing in whitelist needs to be reviewed. Otherwise, you need to print the message in error list.</returns>
        public static bool CheckWhitelistStatusForAllTables(
            ref List<string> errorList, 
            out Dictionary<string, WhitelistData> whitelistDictionary, 
            out Dictionary<string, string> reconstructMap, 
            string whitelistPath)
        {
            HashSet<string> tableSet = new HashSet<string>();
            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetAllNonViewTableNames"))
            {
                while (reader.Read())
                {
                    string tableNm = reader["TABLE_NAME"].ToString().TrimWhitespaceAndBOM();
                    if (!tableSet.Contains(tableNm))
                    {
                        tableSet.Add(tableNm);
                    }
                }
            }

            whitelistDictionary = null;
            reconstructMap = null;
            ShareImportExport.ReadCsvWhiteList(ref errorList, out reconstructMap, out whitelistDictionary, whitelistPath, false);
            bool needReview = false;
            foreach (string tableNm in tableSet)
            {
                if (!whitelistDictionary.ContainsKey(tableNm))
                {
                    whitelistDictionary.Add(tableNm, new WhitelistData());
                    whitelistDictionary[tableNm].MissingSet = ShareImportExport.GetColumnNamesForTable(tableNm);
                }

                WhitelistData curTableWhitelist = whitelistDictionary[tableNm];
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Justin_Testing_GetColumnNamesByTableName", new SqlParameter("@TableName", tableNm)))
                {
                    while (reader.Read())
                    {
                        string colNm = reader["column_name"].ToString().TrimWhitespaceAndBOM();
                        if (curTableWhitelist.ExportSet.Contains(colNm) == false && curTableWhitelist.NoImportExportSet.Contains(colNm) == false
                            && curTableWhitelist.NoUpdateSet.Contains(colNm) == false && curTableWhitelist.ProtectedDictionary.ContainsKey(colNm) == false
                            && curTableWhitelist.UpdateProtectDictionary.ContainsKey(colNm) == false)
                        {
                            if (curTableWhitelist.ReviewSet.Contains(colNm) == false)
                            {
                                curTableWhitelist.MissingSet.Add(colNm);
                            }

                            needReview = true;
                        }
                    }
                }
            }

            return !needReview;
        }

        /// <summary>
        /// Get the column information stored in dictionary.
        /// A special setting: when it is identity key, set read-only to be false.
        /// </summary>
        /// <param name="connSrc">Data source to connect to.</param>
        /// <param name="tableName">Current table name.</param> 
        /// <returns>Dictionary: column name -> "columnInfo".</returns>
        public static Dictionary<string, ColumnInfo> GetColumnInfoByColumnNameMap(DataSrc connSrc, string tableName)
        {
            var columnInfoByColumnName = new Dictionary<string, ColumnInfo>(StringComparer.OrdinalIgnoreCase);
            var identityColumnNames = new List<string>();
            int debug_cnt = 0;

            using (var reader = StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_GetColumnInfoByTableName", new SqlParameter("@TableName", tableName)))
            {
                columnInfoByColumnName = new Dictionary<string, ColumnInfo>(reader.FieldCount);
                while (reader.Read())
                {
                    string curColNm = Convert.ToString(reader["column_name"]).TrimWhitespaceAndBOM();
                    if (!columnInfoByColumnName.ContainsKey(curColNm))
                    {
                        columnInfoByColumnName.Add(curColNm, new ColumnInfo());
                    }

                    var columnInfo = columnInfoByColumnName[curColNm];

                    columnInfo.ColumnSize = Convert.ToInt64(reader["max_length"]);
                    columnInfo.DataType = Convert.ToString(reader["data_type"]).TrimWhitespaceAndBOM();
                    columnInfo.IsIdentity = Convert.ToBoolean(reader["is_identity"]);
                    columnInfo.Nullable = Convert.ToBoolean(reader["is_nullable"]);
                    columnInfo.IsComputed = Convert.ToBoolean(reader["is_computed"]);
                    debug_cnt++;
                }

                reader.NextResult();
                while (reader.Read())
                {
                    // For columns that are claimed to be nullable but has not-null check constraint.
                    string curColNm = Convert.ToString(reader["columnName"]).TrimWhitespaceAndBOM();
                    columnInfoByColumnName[curColNm].Nullable = false;
                }
            }

            SqlParameter[] fkeyParas = { new SqlParameter("@Property", "IsForeignKey"), new SqlParameter("@TableName", tableName) };
            using (var reader = StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_GetColumnPropertyByTableName", fkeyParas))
            {
                while (reader.Read())
                {
                    columnInfoByColumnName[Convert.ToString(reader["column_name"]).TrimWhitespaceAndBOM()].IsForeignKey = true;
                }
            }

            SqlParameter[] pkeyParas = { new SqlParameter("@Property", "IsPrimaryKey"), new SqlParameter("@TableName", tableName) };
            using (var reader = StoredProcedureHelper.ExecuteReader(connSrc, "Justin_Testing_GetColumnPropertyByTableName", pkeyParas))
            {
                while (reader.Read())
                {
                    columnInfoByColumnName[Convert.ToString(reader["column_name"]).TrimWhitespaceAndBOM()].IsPrimaryKey = true;
                }
            }

            //// hardcode
            /*if (tableName == "LOAN_FILE_E")
            {
                curTableMap["sInvestorRolodexId"].IsForeignKey = true;
                curTableMap["sInvestorRolodexId"].IsAutoIncrement = true;
            }*/

            if (tableName == "LENDER_LOCK_POLICY_HOLIDAY")
            {
                columnInfoByColumnName["LockDeskCloseTime"].Nullable = false;
                columnInfoByColumnName["LockDeskOpenTime"].Nullable = false;
            }            

            return columnInfoByColumnName;
        }

        /// <summary>
        /// The reason for this is that I have to do a dynamic query to find out the correct connection information for entry by any column.
        /// The problem is that there is no available danger reader with sql parameter. To write the actual column value into the query string
        /// would be a real risky way and I don't want that since user will enter the column value. So, I temporarily write my own reader and 
        /// in case we can add one or has other solution, we can drop this one.
        /// </summary>
        /// <param name="connInfo">Sql connection.</param> 
        /// <param name="query">Sql query.</param> 
        /// <param name="parameters">Sql parameters.</param> 
        /// <returns>Sql data reader.</returns>
        public static DbDataReader ExecuteReaderDangerously(DbConnectionInfo connInfo, string query, params SqlParameter[] parameters)
        {
            var conn = connInfo.GetConnection();
            var command = conn.CreateCommand();
            command.CommandText = query;
            command.CommandType = CommandType.Text;

            if (parameters != null)
            {
                command.Parameters.AddRange(parameters);
            }

            conn.OpenWithRetry();
            try
            {
                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
                return reader;
            }
            catch (Exception exc)
            {
                conn.Close();
                StringBuilder paraErrorMsg = new StringBuilder();
                foreach (SqlParameter para in parameters)
                {
                    paraErrorMsg.Append("@" + para.ToString() + " = '" + para.Value + "', \n");
                }

                Tools.LogError("Error for query:\r\n" + query + "\r\n" + paraErrorMsg.ToString() + "\r\nException:" + exc.ToString());
                throw;
            }
        }

        /// <summary>
        /// A custom sql reader execute that provide "DbConnection" so that it can be used for transaction.
        /// </summary>
        /// <param name="conn">Sql connection.</param> 
        /// <param name="stran">Sql transaction.</param> 
        /// <param name="cmd">Sql command.</param>
        /// <param name="justinOneSqlQueryTimeOutInSeconds">Time out for one sql query.</param>
        /// <param name="query">Sql query.</param> 
        /// <param name="parameters">Sql parameters.</param> 
        /// <returns>Sql data reader.</returns>
        public static DbDataReader ExecuteReaderDangerously(DbConnection conn, DbTransaction stran, DbCommand cmd, int? justinOneSqlQueryTimeOutInSeconds, string query, params SqlParameter[] parameters)
        {
            cmd.Connection = conn;
            cmd.Transaction = stran;
            cmd.CommandText = query;
            if (justinOneSqlQueryTimeOutInSeconds.HasValue)
            {
                cmd.CommandTimeout = justinOneSqlQueryTimeOutInSeconds.Value;
            }

            if (parameters != null)
            {
                cmd.Parameters.AddRange(parameters);
            }

            try
            {
                var reader = cmd.ExecuteReader();
                return reader;
            }
            catch (SqlException exc)
            {
                Tools.LogError(exc);
                throw;
            }
        }

        /// <summary>
        /// A special query designed for transaction. I put the assignment of sql command's transaction, connection outside of this function because that can be done once for all.
        /// </summary>
        /// <param name="conn">Sql connection object.</param>
        /// <param name="command">Sql command object.</param>
        /// <param name="justinOneSqlQueryTimeOutInSeconds">A setting for sql query time out.</param>
        /// <param name="procedureName">The name of stored procedure.</param>
        /// <param name="parameters">The list of parameters for stored procedure.</param>
        /// <returns>Return the lines affected by sql query.</returns>
        public static int ExecuteNonQueryBySp(DbConnection conn, DbCommand command, int? justinOneSqlQueryTimeOutInSeconds, string procedureName, IEnumerable<SqlParameter> parameters)
        {
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = procedureName;
            if (parameters != null)
            {
                foreach (SqlParameter p in parameters)
                {
                    command.Parameters.Add((SqlParameter)((ICloneable)p).Clone());
                }
            }

            try
            {
                int ret = int.MinValue;
                ret = command.ExecuteNonQuery();

                if (parameters != null)
                {
                    // 9/7/2006 dd - Since we are cloning the SqlParameters, we need to set the value of the new SqlParamter to the original argument.
                    foreach (SqlParameter p in parameters)
                    {
                        p.Value = command.Parameters[p.ParameterName].Value;
                    }
                }

                return ret;
            }
            finally
            {
                command.Parameters.Clear();
            }
        }

        /// <summary>
        /// Read identity key mapping from main database. <para></para>
        /// Structure: identity_table_name|identity_key_name|source_value -> thisDbs_value.
        /// </summary>
        /// <param name="stageSiteSourceName">Show the stage. It could be production, development.</param>
        /// <param name="catalogSourceName">Show the source. It could be lender office.</param>
        /// <returns>Dictionary: "identity_table_name|identity_key_name|old_value -> new_value".</returns>
        public static Dictionary<string, string> ReadIdentityKeyMappingFromSourceToDestDb(string stageSiteSourceName, string catalogSourceName)
        {
            Dictionary<string, string> idkOldNewMapping = new Dictionary<string, string>();
            var parameters = new List<SqlParameter>()
            {
                new SqlParameter("@StageSiteSourceName", SqlDbType.Char, 100) { Value = stageSiteSourceName },
                new SqlParameter("@DatabaseSourceName", SqlDbType.Char, 100) { Value = catalogSourceName },
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShare, "Justin_Testing_IdentityColumnMap_SelectForSource", parameters))
            {
                while (reader.Read())
                {
                    var tableName = reader["TableName"].ToString().TrimWhitespaceAndBOM();
                    var columName = reader["ColumnName"].ToString().TrimWhitespaceAndBOM();
                    var sourceDbValue = reader["SourceDbValue"].ToString().TrimWhitespaceAndBOM();
                    var thisDbValue = reader["ThisDbValue"].ToString().TrimWhitespaceAndBOM();
                    var fullSourceInfo = string.Join("|", new string[] { tableName, columName, sourceDbValue });

                    idkOldNewMapping.Add(fullSourceInfo, thisDbValue);                    
                }
            }

            return idkOldNewMapping;
        }

        /// <summary>
        /// Write only the new identity key mapping back into database by comparing the old dictionary. Only call this function once per import of one xml file. 
        /// </summary>
        /// <param name="oldDictionary">Original identity key mapping dictionary.</param> 
        /// <param name="idkOldNewMapping">Latest identity key mapping dictionary: "identity_table|identity_key_name|old_value -> new_value".</param> 
        /// <param name="stageSiteSourceName">The stage site: 'Test', 'Production', 'LoanPQ'.</param> 
        /// <param name="databaseSourceName">Database source: 'Main', 'RateSheet', 'Transient'.</param> 
        public static void WriteIkOldNewMappingBackToDB(
            ref Dictionary<string, string> oldDictionary,
            Dictionary<string, string> idkOldNewMapping,
            string stageSiteSourceName,
            string databaseSourceName)
        {
            Dictionary<string, string>.KeyCollection keys = idkOldNewMapping.Keys;
            foreach (string key in keys)
            {
                string[] tokens = key.Split('|');
                string tableNm = tokens[0];
                string colNm = tokens[1];
                string oldVal = tokens[2];

                if (oldDictionary.ContainsKey(key) && idkOldNewMapping[key] != oldDictionary[key])
                {
                    continue;
                }

                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@StageSiteSourceName", stageSiteSourceName),
                    new SqlParameter("@DatabaseSourceName", databaseSourceName),
                    new SqlParameter("@TableName", tableNm),
                    new SqlParameter("@ColumnName", colNm),
                    new SqlParameter("@ColumnType", "varchar"),
                    new SqlParameter("@SourceDbValue", oldVal),
                    new SqlParameter("@ThisDbValue", idkOldNewMapping[key])
                };

                if (!oldDictionary.ContainsKey(key))
                {
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "Justin_Testing_IdentityColumnMap_Insertion", 1, parameters);
                    oldDictionary.Add(key, idkOldNewMapping[key]);
                }
                else if (idkOldNewMapping[key] != oldDictionary[key])
                {
                    StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "Justin_Testing_IdentityColumnMap_Update", 1, parameters);
                    oldDictionary[key] = idkOldNewMapping[key];
                }
            }
        }

        /// <summary>
        /// Merge two list into one list. 
        /// </summary>
        /// <typeparam name="T">The first generic type parameter.</typeparam>
        /// <param name="list1">First list to be combined.</param> 
        /// <param name="list2">Second list to be combined.</param> 
        /// <returns>Return merged list.</returns>
        public static List<T> MergeTwoLists<T>(List<T> list1, List<T> list2)
        {
            List<T> list = new List<T>();
            list.AddRange(list1);
            list.AddRange(list2);
            return list;
        }

        /// <summary>
        /// Delete one application by its application id.
        /// </summary>
        /// <param name="appId">This is application id.</param> 
        /// <param name="userId">The id of login user who tries to delete the application.</param> 
        public static void DeleteApp(string appId, string userId)
        {
            StoredProcedureHelper.ExecuteNonQuery_OBSOLETE_DO_NOT_USE("DeleteApplication", 1, new SqlParameter("@AppId", appId), new SqlParameter("@UserId", userId));
        }

        /// <summary>
        /// Create an application with its primary key being preset to the designated one. It will fail if the application id already exists.
        /// </summary>
        /// <param name="appId">The application id you want this app to have.</param>
        /// <param name="loanId">The loan id of the loan you want this application to be inserted into.</param>
        public static void AddApp(string appId, string loanId)
        {
            StoredProcedureHelper.ExecuteNonQuery_OBSOLETE_DO_NOT_USE("Justin_Testing_CreateNewApplicationByAppId", 1, new SqlParameter("@LoanId", loanId), new SqlParameter("@AppId", appId));
        }

        /// <summary>
        /// Get all the application id based on the sequence of primary rank index.
        /// </summary>
        /// <param name="connInfoBrokerId">The connection of this loan.</param>
        /// <param name="loanId">This is loan id.</param> 
        /// <returns>A list of application id.</returns>
        public static List<string> GetAppIdListByLoanId(Guid connInfoBrokerId, string loanId)
        {
            List<string> appIdList = new List<string>();
            HashSet<string> appIdSet = new HashSet<string>();
            SqlParameter[] paras = new SqlParameter[] { new SqlParameter("@LoanId", loanId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(connInfoBrokerId, "ListAppsByLoanId", paras))
            {
                while (reader.Read())
                {
                    string appId = Convert.ToString(reader["aAppId"]).TrimWhitespaceAndBOM();
                    if (!appIdSet.Contains(appId))
                    {
                        appIdList.Add(appId);
                        appIdSet.Add(appId);
                    }
                }
            }

            return appIdList;
        }

        /// <summary>
        /// Get all the application id based on the sequence of primary rank index.
        /// </summary>
        /// <param name="connInfoBrokerId">The connection information of this loan.</param>
        /// <param name="loanId">This is loan id.</param> 
        /// <returns>A list of application id.</returns>
        public static HashSet<string> GetAppIdSetByLoanId(Guid connInfoBrokerId, string loanId)
        {
            HashSet<string> appIdList = new HashSet<string>();
            SqlParameter[] paras = new SqlParameter[] { new SqlParameter("@LoanId", loanId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(connInfoBrokerId, "ListAppsByLoanId", paras))
            {
                while (reader.Read())
                {
                    string appId = Convert.ToString(reader["aAppId"]).TrimWhitespaceAndBOM();
                    if (!appIdList.Contains(appId))
                    {
                        appIdList.Add(appId.ToLower());
                    }
                }
            }

            return appIdList;
        }

        /// <summary>
        /// Get broker id by using loan id.
        /// This is expensive since it will go through all data source to see if the loan can be found there. Try to avoid use this.
        /// </summary>
        /// <param name="loanId">This is loan id.</param> 
        /// <returns>The broker id of current loan.</returns>
        /*public static Guid GetBrokerIdByLoanId(Guid loanId)
        {
            Guid brokerId = new Guid();
            DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

            return brokerId;
        }*/

        /// <summary>
        /// Get customerCode by using broker id.
        /// </summary>
        /// <param name="guidBrokerId">This is broker id.</param> 
        /// <returns>The customerCode of current broker.</returns>
        public static string GetCustomerCodeByBrokerId(Guid guidBrokerId)
        {
            string customerCode = string.Empty;
            SqlParameter[] paras = new SqlParameter[] { new SqlParameter("@BrokerId", guidBrokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(guidBrokerId, "GetBrokerCustomerCodeByBrokerId", paras))
            {
                if (reader.Read())
                {
                    customerCode = Convert.ToString(reader["CustomerCode"]).TrimWhitespaceAndBOM();
                }
            }

            return customerCode;
        }

        /// <summary>
        /// Get branch id list by using broker id.
        /// </summary>
        /// <param name="guidBrokerId">This is broker id.</param> 
        /// <returns>The list of branch id that belong to current broker.</returns>
        public static List<string> GetBranchIdListByBrokerId(Guid guidBrokerId)
        {
            List<string> branchIdList = new List<string>();
            SqlParameter[] paras = new SqlParameter[] { new SqlParameter("@BrokerId", guidBrokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(guidBrokerId, "ListBranchByBrokerID", paras))
            {
                while (reader.Read())
                {
                    branchIdList.Add(Convert.ToString(reader[0]).TrimWhitespaceAndBOM());
                }
            }

            return branchIdList;
        }

        /// <summary>
        /// Get branch id list by using broker id.
        /// </summary>
        /// <param name="guidBrokerId">This is broker id.</param> 
        /// <param name="branchName">The name of branch.</param>
        /// <returns>The list of branch id that belong to current broker and with the name.</returns>
        public static List<string> GetBranchIdListByBranchNameAndBrokerId(Guid guidBrokerId, string branchName)
        {
            List<string> branchIdList = new List<string>();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BranchNm", branchName), new SqlParameter("@BrokerId", guidBrokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(guidBrokerId, "Justin_Testing_GetBranchByBranchNameAndBrokerId", parameters))
            {
                while (reader.Read())
                {
                    branchIdList.Add(Convert.ToString(reader["BranchId"]).TrimWhitespaceAndBOM());
                }
            }

            return branchIdList;
        }

        /// <summary>
        /// It uses broker id to find all employee. And then generate a list of entries.
        /// </summary>
        /// <param name="guidBrokerId">The broker id.</param>
        /// <returns>List of user information entries.</returns>
        public static List<string> GetAllUserEntryByBrokerId(Guid guidBrokerId)
        {
            List<string> userEntryList = new List<string>();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BrokerID", guidBrokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(guidBrokerId, "INTERNAL_USER_ONLY_ListUsersBelongingToBroker", parameters))
            {
                while (reader.Read())
                {
                    string userId = Convert.ToString(reader["userid"]).TrimWhitespaceAndBOM();
                    string employeeId = Convert.ToString(reader["employeeid"]).TrimWhitespaceAndBOM();
                    if (userId != null && userId != string.Empty)
                    {
                        userEntryList.Add("BROKER_USER|UserId|" + userId);
                    }

                    if (employeeId != null && employeeId != string.Empty)
                    {
                        userEntryList.Add("EMPLOYEE|EmployeeId|" + employeeId);
                    }
                }
            }

            return userEntryList;
        }

        /// <summary>
        /// Get the employee id and user id by branch id. Each entry is composed by one employee id and one user id.
        /// </summary>
        /// <param name="guidBrokerId">Broker id in guid format.</param>
        /// <param name="branchId">The id of branch.</param>
        /// <returns>The list of combo entries.</returns>
        public static List<string> GetEmpUserIdComboListByBranchId(Guid guidBrokerId, string branchId)
        {
            List<string> empUserIdComboList = new List<string>();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BranchID", branchId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(guidBrokerId, "Justin_Testing_ListUsersBelongingToBranch", parameters))
            {
                while (reader.Read())
                {
                    string userId = Convert.ToString(reader["userid"]).TrimWhitespaceAndBOM();
                    string employeeId = Convert.ToString(reader["employeeid"]).TrimWhitespaceAndBOM();
                    empUserIdComboList.Add(employeeId + "|" + userId);
                }
            }

            return empUserIdComboList;
        }

        /// <summary>
        /// It uses combo of employee and user id list to find all employees and users. And then generate a list of entries.
        /// </summary>
        /// <param name="empUserIdComboList">The combo list of employee and user id.</param>
        /// <returns>List of user information entries.</returns>
        public static List<string> GetAllUserEntryByEmpUserIdComboList(List<string> empUserIdComboList)
        {
            List<string> userEntryList = new List<string>();
            foreach (string empUserIdCombo in empUserIdComboList)
            {
                string[] tokens = empUserIdCombo.Split('|');
                string employeeId = tokens[0];
                string userId = tokens[1];
                if (userId != null && userId != string.Empty)
                {
                    userEntryList.Add("BROKER_USER|UserId|" + userId);
                }

                if (employeeId != null && employeeId != string.Empty)
                {
                    userEntryList.Add("EMPLOYEE|EmployeeId|" + employeeId);
                }
            }

            return userEntryList;
        }

        /// <summary>
        /// It uses branch id to find all employee. And then generate a list of entries.
        /// </summary>
        /// <param name="guidBrokerId">Broker id in guid format.</param>
        /// <param name="branchId">The branch id.</param>
        /// <returns>List of user information entries.</returns>
        public static List<string> GetAllUserEntryByBranchId(Guid guidBrokerId, string branchId)
        {
            List<string> userEntryList = new List<string>();
            SqlParameter[] parameters = new SqlParameter[] { new SqlParameter("@BranchID", branchId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(guidBrokerId, "Justin_Testing_ListUsersBelongingToBranch", parameters))
            {
                while (reader.Read())
                {
                    string userId = Convert.ToString(reader["userid"]).TrimWhitespaceAndBOM();
                    string employeeId = Convert.ToString(reader["employeeid"]).TrimWhitespaceAndBOM();
                    if (reader.IsDBNull(reader.GetOrdinal("userid")) == false && userId != null && userId != string.Empty)
                    {
                        userEntryList.Add("BROKER_USER|UserId|" + userId);
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("employeeid")) == false && employeeId != null && employeeId != string.Empty)
                    {
                        userEntryList.Add("EMPLOYEE|EmployeeId|" + employeeId);
                    }
                }
            }

            return userEntryList;
        }

        /// <summary>
        /// Method extracted to make unit testing easy.
        /// </summary>
        /// <param name="tokens">The tokens.</param>
        /// <param name="size">The size of the tokens.</param>
        /// <param name="tableNm">The table name.</param>
        /// <returns>Initial part of the sql query under construction.</returns>
        public static StringBuilder GetQueryFromTableAndTokens(string[] tokens, int size, string tableNm)
        {
            bool bOK = RegularExpressionHelper.IsMatch(RegularExpressionString.DBTableName, tableNm);
            if (!bOK)
            {
                throw new ApplicationException("Bad table name");
            }

            var query = new StringBuilder("SELECT * FROM [" + tableNm + "] WHERE "); // Have to use "... from [tableName] ... " if tableName = Group ...
            for (int i = 1; i < size - 1; i += 2)
            {
                bOK = RegularExpressionHelper.IsMatch(RegularExpressionString.DBColumnName, tokens[i]);
                if (!bOK)
                {
                    throw new ApplicationException("Bad column name");
                }

                query.Append(tokens[i] + " = @" + tokens[i] + " and ");
            }

            return query;
        }

        /// <summary>
        /// This function take any entry and check if it exists in any of data sources. 
        /// If it does, it return true and output the database connection information object.
        /// Else, it return false and output null.
        /// 1/26/2015: You cannot just retrieve broker id by any entry. You can only retrieve them with the correct primary key set on it.
        /// But it is impossible to convert any entry into a standard format entry before knowing the connection information first.
        /// So, I change this function to use connection information to get the corresponding broker id for that entry.
        /// Also, you should not call this table if entry is not from loMain data source. It could be in any database belonged to broker.
        /// </summary>
        /// <param name="errorList">Record errors in list.</param>
        /// <param name="connInfoBrokerId">The object stores the connection data.</param>
        /// <param name="customerCode">The customer code of broker.</param>
        /// <param name="curConnInfo">Database connection information.</param>
        /// <param name="entry">The entry of table.</param>
        /// <returns>Return true if find one. Else return false.</returns>
        public static bool TryGetBrokerIdAndCustomerCodeByEntryAndDbConnInfo(ref List<string> errorList, out Guid connInfoBrokerId, out string customerCode, DbConnectionInfo curConnInfo, string entry)
        {
            connInfoBrokerId = Guid.Empty;
            customerCode = string.Empty;
            string[] tokens = entry.Split('|');
            int size = tokens.Count();
            string tableNm = tokens[0];

            int isByTableName = 1;
            List<SqlParameter> paraList = new List<SqlParameter>();
            paraList.Add(new SqlParameter("@UserOption", "MAIN_DB"));
            paraList.Add(new SqlParameter("@BrokerId", Guid.Empty));
            paraList.Add(new SqlParameter("@IsByTableName", isByTableName));
            paraList.Add(new SqlParameter("@InputTableName", tableNm));

            for (int i = 1; i < size - 1; i += 2)
            {
                paraList.Add(new SqlParameter("@varcharKey" + ((i + 1) / 2), tokens[i + 1]));
            }

            var sprocName = "Justin_Testing_Another_GetBrokerBranchUserRelatedTablesAndOthers";
            try
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(curConnInfo, sprocName, paraList))
                {
                    if (reader.Read())
                    {
                        connInfoBrokerId = (Guid)reader["BrokerId"];
                    }
                    else
                    {
                        var msg = "Please make sure table " + tableNm + " has connBrokerid in " + sprocName + ". Or maybe using a wrong varchar key for the query?";
                        if (!errorList.Contains(msg))
                        {
                            errorList.Add(msg);
                            Tools.LogError(msg);
                        }

                        return false;
                    }
                }
            }
            catch (SqlException sq)
            {
                if (!errorList.Contains(sq.Message))
                {
                    errorList.Add(sq.Message);
                    Tools.LogError(sq);
                }

                return false;
            }

            // use the connectionInfo to get the first broker's id as the id for this entry.
            SqlParameter[] paras = new SqlParameter[] { new SqlParameter("@BrokerId", connInfoBrokerId) };
            using (var reader = StoredProcedureHelper.ExecuteReader(curConnInfo, "BROKER_RetrieveLiteInfo", paras))
            {
                if (reader.Read())
                {
                    customerCode = Convert.ToString(reader["CustomerCode"]);
                }
                else
                {
                    errorList.Add("Broker [" + connInfoBrokerId + "] is missing customerCode.");
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 3/16/2015 - A new way to export file database: User can use white list to select which type of files to be exported.
        /// User also need to use the default value to choose the parser for that content.
        /// If without parser, it will just export the blank content.
        /// </summary>
        /// <param name="errorList">Record errors in list.</param>
        /// <param name="whitelistDictionary">A white list dictionary.</param>
        /// <param name="needUpdateWhitelist">Check if need to update whitelist after all the export actions.</param>
        /// <param name="exportXmlWriter">An xml writer passes in to write file database block xml.</param>
        /// <param name="loanIdList">A list of loan id to export all the associated filedbkeys of.</param>
        /// <param name="needDebugInfo">If true, it will show the log information in the Paul Bunyan message viewer. It has many message. </param>
        /// <param name="additionalFileDBEntries">Any filedb entries that came from the database specifically.  e.g. closing cost archive entries.</param>
        public static void ExportFiledb(
            ref List<string> errorList, 
            ref Dictionary<string, WhitelistData> whitelistDictionary, 
            ref bool needUpdateWhitelist, 
            XmlWriter exportXmlWriter, 
            IEnumerable<Guid> loanIdList,
            bool needDebugInfo,
            IEnumerable<FileDBEntryInfoForExport> additionalFileDBEntries)
        {
            string tableNm = "FILEDB|LOAN";
            if (!whitelistDictionary.ContainsKey(tableNm))
            {
                whitelistDictionary.Add(tableNm, new WhitelistData());
                needUpdateWhitelist = true;
            }

            exportXmlWriter.WriteStartElement("FILEDB");

            {
                var filedbKeyTypes = (E_FileDBKeyType[])Enum.GetValues(typeof(E_FileDBKeyType));
                foreach (E_FileDBKeyType filedbKeyType in filedbKeyTypes)
                {
                    var filedbKeyTypeName = filedbKeyType.ToString("g");

                    bool isReview = whitelistDictionary[tableNm].ReviewSet.Contains(filedbKeyTypeName);
                    bool isExportable = whitelistDictionary[tableNm].ExportSet.Contains(filedbKeyTypeName); //// This should always be false.
                    bool isProtect = whitelistDictionary[tableNm].ProtectedDictionary.ContainsKey(filedbKeyTypeName);
                    bool isNoExportImport = whitelistDictionary[tableNm].NoImportExportSet.Contains(filedbKeyTypeName);
                    if (!(isProtect || isReview || isNoExportImport || isExportable))
                    {
                        whitelistDictionary[tableNm].MissingSet.Add(filedbKeyTypeName);
                        needUpdateWhitelist = true;
                    }
                }
            }

            Dictionary<string, string>.KeyCollection whitelistFiledbKeyTypeNames = whitelistDictionary[tableNm].ProtectedDictionary.Keys;
            foreach (string filedbKeyTypeName in whitelistFiledbKeyTypeNames)
            {
                var filedbKeyType = (E_FileDBKeyType)Enum.Parse(typeof(E_FileDBKeyType), filedbKeyTypeName);
                
                if (!Tools.SpecificallyLoanFileDBKeyTypes.Contains(filedbKeyType))
                {
                    // here we only export the filedb types that are in the protected list and specifically tied to the loan as opposed to
                    // being tied to a closing cost archive, say.
                    continue;
                }
                
                string[] tokens = whitelistDictionary[tableNm].ProtectedDictionary[filedbKeyTypeName].Trim('{', '}').Split('|');
                string fileFormat = tokens[1].ToUpper();
                string encodingNm = tokens[2].ToUpper();
                string escapeMethod = tokens[3].ToUpper();
                foreach (var loanId in loanIdList)
                {
                    FileDBEntryInfo filedbEntry = Tools.GetFileDBEntryInfoForLoan(filedbKeyType, loanId);
                    WriteFileDBEntryToExportXML(
                        ref errorList,
                        ref whitelistDictionary,
                        ref needUpdateWhitelist,
                        exportXmlWriter,
                        filedbEntry,
                        filedbKeyType,
                        encodingNm,
                        fileFormat,
                        escapeMethod,
                        needDebugInfo);
                }
            }
            
            foreach (var fileDBEntry in additionalFileDBEntries)
            {
                string filedbKeyTypeName = fileDBEntry.FileDBKeyType.ToString("g");
                string[] tokens = whitelistDictionary[tableNm].ProtectedDictionary[filedbKeyTypeName].Trim('{', '}').Split('|');
                string fileFormat = tokens[1].ToUpper();
                string encodingNm = tokens[2].ToUpper();
                string escapeMethod = tokens[3].ToUpper();

                WriteFileDBEntryToExportXML(
                    ref errorList,
                    ref whitelistDictionary,
                    ref needUpdateWhitelist,
                    exportXmlWriter,
                    fileDBEntry.FileDBEntry,
                    fileDBEntry.FileDBKeyType,
                    encodingNm,
                    fileFormat,
                    escapeMethod,
                    needDebugInfo);
            }
            
            exportXmlWriter.WriteEndElement();
        }

        /// <summary>
        /// A hardcode function for exporting liability and asset.
        /// </summary>
        /// <param name="whitelistDictionary">A white list dictionary.</param>
        /// <param name="needUpdateWhitelist">Check if need to update whitelist after all the export actions.</param>
        /// <param name="tableName">The name of table.</param>
        /// <param name="originalXml">The original xml content.</param>
        /// <returns>Return the whole file database string content that is filtered by the current whitelist.</returns>
        public static string FilterLiaAssetByWhitelistHardcode(ref Dictionary<string, WhitelistData> whitelistDictionary, ref bool needUpdateWhitelist, string tableName, string originalXml)
        {
            if (originalXml == null || originalXml == string.Empty)
            {
                return originalXml;
            }

            string trimTableNm = tableName.TrimWhitespaceAndBOM();
            if (trimTableNm == string.Empty || trimTableNm == null)
            {
                return string.Empty;
            }

            if (!whitelistDictionary.ContainsKey(tableName))
            {
                needUpdateWhitelist = true;
                whitelistDictionary.Add(tableName, new WhitelistData());
            }

            if (!whitelistDictionary.ContainsKey(tableName + "|Liability"))
            {
                needUpdateWhitelist = true;
                whitelistDictionary.Add(tableName + "|Liability", new WhitelistData());
            }

            if (!whitelistDictionary.ContainsKey(tableName + "|Asset"))
            {
                needUpdateWhitelist = true;
                whitelistDictionary.Add(tableName + "|Asset", new WhitelistData());
            }

            WhitelistData whitelist = whitelistDictionary[tableName];
            WhitelistData liaWhitelist = whitelistDictionary[tableName + "|Liability"];
            WhitelistData assetWhitelist = whitelistDictionary[tableName + "|Asset"];
            XElement root = XElement.Parse(originalXml);
            IEnumerable<XElement> sectionList = root.Descendants("Section");
            foreach (XElement section in sectionList)
            {
                List<XElement> liaAssetList = section.Elements().ToList();
                int cnt = liaAssetList.Count();
                for (int i = 0; i < cnt; i++)
                {
                    XElement liaAsset = liaAssetList[i];
                    if (liaAsset.Name == "LiabilitySchemaDataSet")
                    {
                        ShareImportExport.FilterFiledbXmlElementByWhitelist(ref liaWhitelist, ref liaAsset);
                        section.ReplaceNodes(liaAsset.ToString());
                    }
                    else if (liaAsset.Name == "XmlTable")
                    {
                        ShareImportExport.FilterFiledbXmlElementByWhitelist(ref assetWhitelist, ref liaAsset);
                        section.ReplaceNodes(liaAsset.ToString());
                    }
                }
            }

            return root.ToString();
        }

        /// <summary>
        /// Import file_database data related to loan. It will fetch file_database key from xml.
        /// </summary>
        /// <param name="errorList">Recorded any errors happen during the import.</param> 
        /// <param name="filedbXmlContent">An xml element of the whole file database block.</param> 
        /// <param name="fileDBEntriesForImport">Additional filedb entries for direct import.  It is assumed these are present in the zip file, and in fileInfos.</param>
        /// <param name="fileInfos">The infos of files that could be imported.  Only the ones with names matching the fileDBEntriesForImport keys will get imported.</param>
        public static void ImportFiledb(ref List<string> errorList, XElement filedbXmlContent, IEnumerable<FileDBEntryInfo> fileDBEntriesForImport, IEnumerable<FileInfo> fileInfos)
        {
            if (fileDBEntriesForImport == null)
            {
                fileDBEntriesForImport = new List<FileDBEntryInfo>();
            }

            if (fileInfos == null)
            {
                fileInfos = new List<FileInfo>();
            }

            var fileInfoByName = fileInfos.ToDictionary((fi) => fi.Name);

            IEnumerable<XElement> filedbList = filedbXmlContent.Elements();
            foreach (XElement oneFiledb in filedbList)
            {
                string filedbKey = oneFiledb.Attribute("filedbKey").Value;
                string encodingNm = oneFiledb.Attribute("encoding").Value;
                string database = oneFiledb.Attribute("database").Value;
                
                var fileDB = (E_FileDB)Enum.Parse(typeof(E_FileDB), database);

                Encoding encoding = Encoding.GetEncoding(encodingNm);
                string innerContent = oneFiledb.Value;
                byte[] data = encoding.GetBytes(innerContent);
                
                FileDBTools.WriteData(fileDB, filedbKey, data);
            }

            foreach (var fileDBEntry in fileDBEntriesForImport)
            {
                if (!fileInfoByName.ContainsKey(fileDBEntry.Key))
                {
                    errorList.Add("imported file was missing file with key: " + fileDBEntry.Key);
                    continue;
                }

                FileDBTools.WriteFile(fileDBEntry.FileDBType, fileDBEntry.Key, fileInfoByName[fileDBEntry.Key].FullName);
            }
        }

        /// <summary>
        /// Import file_database data related to loan. It will generate file_database key based on the loan id parameter.
        /// </summary>
        /// <param name="errorList">Recorded any errors happen during the import.</param> 
        /// <param name="filedbXmlContent">An xml element of the whole file database block.</param> 
        /// <param name="loanId">Loan id used to generate file database key.</param>
        public static void ImportFiledb(ref List<string> errorList, XElement filedbXmlContent, string loanId)
        {
            IEnumerable<XElement> filedbList = filedbXmlContent.Elements();
            foreach (XElement oneFiledb in filedbList)
            {
                string filedbTypeName = oneFiledb.Name.ToString();
                string encodingNm = oneFiledb.Attribute("encoding").Value;
                string database = oneFiledb.Attribute("database").Value;
                Encoding encoding = Encoding.GetEncoding(encodingNm);
                E_FileDBKeyType filedbKeyType = (E_FileDBKeyType)Enum.Parse(typeof(E_FileDBKeyType), filedbTypeName);
                
                FileDBEntryInfo filedbEntry;
                if (Tools.SpecificallyLoanFileDBKeyTypes.Contains(filedbKeyType))
                {
                    filedbEntry = Tools.GetFileDBEntryInfoForLoan(filedbKeyType, new Guid(loanId));
                }
                else
                {
                    string filedbKey = oneFiledb.Attribute("filedbKey").Value;
                    switch (filedbKeyType)
                    {
                        case E_FileDBKeyType.Normal_ClosingCostArchive:
                        case E_FileDBKeyType.Normal_FeeServiceRevision_File:
                        case E_FileDBKeyType.Normal_Loan_MigrationAuditHistoryKey:
                        case E_FileDBKeyType.Normal_LpePriceGroup_Content:
                        case E_FileDBKeyType.Edms_CustomPdf_Form:
                        case E_FileDBKeyType.Normal_CustomNonPdf_Form:
                            filedbEntry = new FileDBEntryInfo(E_FileDB.Normal, filedbKey);
                            break;
                        default:
                            throw new UnhandledEnumException(filedbKeyType);
                    }
                }

                string innerContent = oneFiledb.Value;
                byte[] data = encoding.GetBytes(innerContent);
                FileDBTools.WriteData(filedbEntry.FileDBType, filedbEntry.Key, data);
            }
        }

        /// <summary>
        /// The main function that merges system default config xml from the database with the system config in the xml file being imported.
        /// </summary>
        /// <param name="combinedSystemConfigString">The combined configuration xml content.</param>
        /// <param name="versionDate">The date of this xml. By default, use the time when it is merged.</param>
        /// <param name="errorList">A list record errors.</param>
        /// <param name="connBrokerId">The broker id set for connection string for database query.</param>
        /// <param name="orgId">The organization id of the merged configure xml. Both database and imported xml file's configure xml should use this id.</param>
        /// <param name="configReleaseXElement">The configure xml element of imported xml file.</param>
        /// <returns>Return true if successfully found the xml for both and combined them, but doesn't save.</returns>
        public static bool CombineConfigReleaseFromXmlAndDb(
            out string combinedSystemConfigString,
            out DateTime versionDate,
            List<string> errorList,
            Guid connBrokerId,
            Guid orgId,
            XElement configReleaseXElement)
        {
            versionDate = DateTime.Now;
            combinedSystemConfigString = string.Empty;
            string databaseConfigReleasedContent = null;
            string combinedXmlCompiledCode = string.Empty;
            string xmlConfigReleasedContent = (from col in configReleaseXElement.Elements() where col.Attribute("n").Value == "ConfigurationXmlContent" select col.Attribute("v").Value).First();

            if (xmlConfigReleasedContent == null)
            {
                errorList.Add($"Could not find config_released element with id {orgId} and ConfigurationXmlContent attribute in current xml file.");
                return false;
            }

            List<SqlParameter> databaseParaList = new List<SqlParameter>();
            databaseParaList.Add(new SqlParameter("OrgId", orgId));
            using (var sqlReader = StoredProcedureHelper.ExecuteReader(connBrokerId, "CONFIG_LoadLatestReleases", databaseParaList))
            {
                if (sqlReader.Read())
                {
                    databaseConfigReleasedContent = Convert.ToString(sqlReader["ConfigurationXmlContent"]);
                }
                else
                {
                    errorList.Add($"Could not find config_released with id {orgId} in current database.");
                    return false;
                }
            }

            var combinedSystemConfig = CombineSystemConfigs(xmlConfigReleasedContent, databaseConfigReleasedContent);
            combinedSystemConfigString = combinedSystemConfig.ToString();

            return true;
        }

        /// <summary>
        /// Takes xml from two system configs and combines them. <para></para>
        /// Watch out: system configs tend to be more than 85KB, so using this eats LOH memory. <para></para>
        /// Also note: The combination is not currently symmetric, so combining with the arguments reversed can give a different result.
        /// </summary>
        /// <param name="systemConfigXml1">The xml of the first system config.</param>
        /// <param name="systemConfigXml2">The xml of the second system config.</param>
        /// <returns>An XElement containing the combined xml for the two system configs.</returns>
        public static XElement CombineSystemConfigs(string systemConfigXml1, string systemConfigXml2)
        {
            var reasonsInputInvalid = new List<string>();
            if (string.IsNullOrEmpty(systemConfigXml1))
            {
                reasonsInputInvalid.Add($"{nameof(systemConfigXml1)} is null or empty.");
            }

            if (string.IsNullOrEmpty(systemConfigXml2))
            {
                reasonsInputInvalid.Add($"{nameof(systemConfigXml2)} is null or empty.");
            }

            if (reasonsInputInvalid.Any())
            {
                throw new ArgumentException(string.Join(Environment.NewLine, reasonsInputInvalid));
            }

            XElement sysConfig1 = XElement.Parse(systemConfigXml1);
            XElement sysConfig2 = XElement.Parse(systemConfigXml2);
            XElement combinedSysConfig = new XElement("SystemConfig");
            XElement combinedConditionSet = CombineChildrenOfTwoElementById(sysConfig1.Element("ConditionSet"), sysConfig2.Element("ConditionSet"));
            XElement combinedConstraintSet = CombineChildrenOfTwoElementById(sysConfig1.Element("ConstraintSet"), sysConfig2.Element("ConstraintSet"));
            XElement combinedCustomVariableSet = CombineChildrenOfTwoElementById(sysConfig1.Element("CustomVariableSet"), sysConfig2.Element("CustomVariableSet"));
            combinedSysConfig.Add(combinedConditionSet);
            combinedSysConfig.Add(combinedConstraintSet);
            combinedSysConfig.Add(combinedCustomVariableSet);

            return combinedSysConfig;
        }

        /// <summary>
        /// User may enter an entry with any column. This function is to generate the standard entry which contains all primary keys
        /// used in the main stored procedure.
        /// </summary>
        /// <param name="errorList">Record errors in list.</param>
        /// <param name="connInfoList">The connection information list.</param>
        /// <param name="standardEntryList">All entries in standard format, that is by primary key.</param>
        /// <param name="standardTableNmList">Table names for all entries.</param>
        /// <param name="dataSrc">Data source of entries. All entries must be from the same source.</param>
        /// <param name="primaryKeyColumnNamesByTableName">Primary key dictionary.</param>
        /// <param name="oneInputEntry">One entry that can be composed by any columns.</param>
        /// <param name="brokerId">The id of the broker so all entries will be in the same database, or null to skip this.</param>
        /// <returns>Return the number of found connection information that the entry related to.</returns>
        public static int TryGetConnInfoAndStandardEntryByOneInputEntryAndDataSrc(
            ref List<string> errorList,
            ref List<DbConnectionInfo> connInfoList,
            ref List<string> standardEntryList,
            ref List<string> standardTableNmList,
            DataSrc dataSrc,
            Dictionary<string, List<string>> primaryKeyColumnNamesByTableName,
            string oneInputEntry,
            Guid? brokerId)
        {
            int foundConnCnt = 0;
            IEnumerable<DbConnectionInfo> allDbConnInfoList = DbConnectionInfo.ListAll();
            string[] tokens = oneInputEntry.Split('|');
            int numTokens = tokens.Count();
            string tableName = tokens[0];
            if (primaryKeyColumnNamesByTableName.ContainsKey(tableName) == false)
            {
                errorList.Add($"Table {tableName} lacks primary key columns in data source ${dataSrc} from GetPrimaryKeyDictionary.");
                return 0;
            }

            var query = GetQueryFromTableAndTokens(tokens, numTokens, tableName);
            string queryStr = query.ToString().Substring(0, query.Length - 5);

            var primaryKeyColumnNames = primaryKeyColumnNamesByTableName[tableName];
            if (dataSrc == DataSrc.LOShare)
            {
                if (brokerId.HasValue)
                {
                    var brokerDbConnection = DbConnectionInfo.GetConnectionInfo(brokerId.Value);
                    List<SqlParameter> sqlParameters = new List<SqlParameter>();
                    for (int i = 1; i < numTokens - 1; i += 2)
                    {
                        sqlParameters.Add(new SqlParameter("@" + tokens[i], tokens[i + 1]));
                    }

                    foundConnCnt = HandleLOMain(connInfoList, standardEntryList, standardTableNmList, foundConnCnt, tableName, primaryKeyColumnNames, brokerDbConnection, sqlParameters, queryStr);
                }
                else
                {
                    foreach (DbConnectionInfo oneDbConnInfo in allDbConnInfoList)
                    {
                        List<SqlParameter> sqlParameters = new List<SqlParameter>();
                        for (int i = 1; i < numTokens - 1; i += 2)
                        {
                            sqlParameters.Add(new SqlParameter("@" + tokens[i], tokens[i + 1]));
                        }

                        foundConnCnt = HandleLOMain(connInfoList, standardEntryList, standardTableNmList, foundConnCnt, tableName, primaryKeyColumnNames, oneDbConnInfo, sqlParameters, queryStr);
                    }
                }
            }
            else
            {
                List<SqlParameter> sqlParameters = new List<SqlParameter>();
                for (int i = 1; i < numTokens - 1; i += 2)
                {
                    sqlParameters.Add(new SqlParameter("@" + tokens[i], tokens[i + 1]));
                }

                foundConnCnt = HandleDataSrc(connInfoList, standardEntryList, standardTableNmList, dataSrc, foundConnCnt, tableName, primaryKeyColumnNames, sqlParameters, queryStr);
            }

            return foundConnCnt;
        }

        /// <summary>
        /// Applies where children are like "Value" (of Parameter) or "System Operation" (of ApplicableSystemOperationSet). <para></para>
        /// Warning: This is not very correct for "reproduction" purposes.  <para></para>
        /// 1) The parameter combine should depend on the function (in, not in, etc.) <para></para>
        /// 2) If we combine ApplicableSystemOperationSet for a condition, it may over allow compared to what either config saw.<para></para>
        /// 3) If we combine ApplicableSystemOperationSet for a constraint, it may over block compared to what either config saw.<para></para>
        /// Strangely enough, this one pulls the name and attributes from the first element, not the second element as in other similar methods.
        /// </summary>
        /// <param name="element1">First: Can be "Parameter", "Applicable SystemOperation Set".</param>
        /// <param name="element2">Second: Can be "Parameter", "Applicable SystemOperation Set".</param>
        /// <returns>The combined result.</returns>
        protected static XElement CombineChildrenOfTwoElementByValue(XElement element1, XElement element2)
        {
            XElement combinedElement = new XElement(element1.Name);
            combinedElement.Add(element1.Attributes());

            IEnumerable<XElement> children1 = element1.Elements();
            HashSet<string> childValueSet = new HashSet<string>();
            foreach (XElement child in children1)
            {
                if (!childValueSet.Contains(child.Value))
                {
                    childValueSet.Add(child.Value);
                    combinedElement.Add(child);
                }
            }

            IEnumerable<XElement> children2 = element2.Elements();
            foreach (XElement child in children2)
            {
                if (!childValueSet.Contains(child.Value))
                {
                    childValueSet.Add(child.Value);
                    combinedElement.Add(child);
                }
            }

            return combinedElement;
        }

        /// <summary>
        /// This function expects system config set elements and combines them. <para></para>
        /// It combines elements by id. If the id is in one or the other, it will be present in the combination. <para></para>
        /// If the id is in both xml elements, it tries to merge them. <para></para>
        /// When in doubt, it "trusts" the xml from the second argument more.  <para></para>
        /// (e.g. the name, attributes, notes, and failure message come exclusively from the second xml) <para></para>
        /// The child elements ParameterSet and ApplicableSystemOperationSet are only merged if both xml have those elements.
        /// </summary>
        /// <param name="set1Element">First set: Can be Condition Set, Constraint Set, Custom Variable Set.</param>
        /// <param name="set2Element">Second set: Can be Condition Set, Constraint Set, Custom Variable Set.</param>
        /// <returns>The combined result.</returns>
        protected static XElement CombineChildrenOfTwoElementById(XElement set1Element, XElement set2Element)
        {
            XElement combinedSetXml = new XElement(set1Element.Name);
            
            // The variable naming is based on conditions so we have an example to keep in mind, but the logic works for constraint and custom variable sets too.
            Dictionary<string, XElement> conditionElementById = new Dictionary<string, XElement>();
            
            // first the first ones, then the second ones.  Merged ones will be one of the two.
            List<string> idsInFirstThenSecondOrder = new List<string>();
            IEnumerable<XElement> conditions1 = set1Element.Elements();
            foreach (XElement condition in conditions1)
            {
                string id = condition.Attribute("id").Value;
                if (!conditionElementById.ContainsKey(id))
                {
                    conditionElementById.Add(id, condition);
                    idsInFirstThenSecondOrder.Add(id);
                }
            }

            IEnumerable<XElement> conditions2 = set2Element.Elements();
            foreach (XElement condition2 in conditions2)
            {
                string id = condition2.Attribute("id").Value;
                if (!conditionElementById.ContainsKey(id))
                {
                    conditionElementById.Add(id, condition2);
                    idsInFirstThenSecondOrder.Add(id);
                    continue;
                }

                // merge if the same id is present in both.
                // the combined condition uses the name and attributes from the second condition only.
                XElement combinedCondition = new XElement(condition2.Name);
                IEnumerable<XAttribute> attrList = condition2.Attributes();
                foreach (XAttribute attr in attrList)
                {
                    combinedCondition.Add(new XAttribute(attr));
                }

                var condition1 = conditionElementById[id];

                // if both conditions have a ParameterSet element, merge their parameters by name.
                XElement parameterSet2 = condition2.Element("ParameterSet");
                if (parameterSet2 != null)
                {
                    XElement parameterSet1 = condition1.Element("ParameterSet");
                    if (parameterSet1 != null)
                    {
                        XElement combinedSet = CombineChildrenOfTwoParameterSetByName(parameterSet1, parameterSet1);
                        combinedCondition.Add(combinedSet);
                    }
                }

                // if both conditions have an ApplicableSystemOperationSet element, merge their applicable system operations by value.
                XElement appSysOpSet2 = condition2.Element("ApplicableSystemOperationSet");
                if (appSysOpSet2 != null)
                {
                    XElement appSysOpSet1 = condition1.Element("ApplicableSystemOperationSet");
                    if (appSysOpSet1 != null)
                    {
                        XElement combinedSet = CombineChildrenOfTwoElementByValue(appSysOpSet1, appSysOpSet2);
                        combinedCondition.Add(combinedSet);
                    }
                }

                // pull the notes and condition failure message from only the second condition.
                XElement conditionFailureMessage = condition2.Element("ConditionFailureMessage");
                if (conditionFailureMessage != null)
                {
                    combinedCondition.Add(conditionFailureMessage);
                }

                XElement notes2 = condition2.Element("Notes");
                if (notes2 != null)
                {
                    combinedCondition.Add(notes2);
                }

                conditionElementById[id] = combinedCondition;
            }

            foreach (string id in idsInFirstThenSecondOrder)
            {
                combinedSetXml.Add(conditionElementById[id]);
            }

            return combinedSetXml;
        }

        /// <summary>
        /// This function combines two sets of parameters into one based on the name. <para></para>
        /// Note: This is probably a poor way to combine sets of parameters, since it's not clear what the goal for the user is, and it just merges them.
        /// </summary>
        /// <param name="parameterSet1Element">First parameter set within the configure released xml content.</param>
        /// <param name="parameterSet2Element">Second parameter set within the configure released xml content.</param>
        /// <returns>The combined result.</returns>
        protected static XElement CombineChildrenOfTwoParameterSetByName(XElement parameterSet1Element, XElement parameterSet2Element)
        {
            // tdsk: it's not clear what the result of combining should be. It's probably best to not combine at all.
            // the result of the combine is that for a given pair of sets of parameters, we union by name, but also merge any parameters with the same name (poorly).
            Dictionary<string, XElement> parameterElementByName = new Dictionary<string, XElement>();
            List<string> namesOrderedByFirstThenSecond = new List<string>();
            XElement combinedParameterSetElement = new XElement("ParameterSet");

            IEnumerable<XElement> set1ParameterElements = parameterSet1Element.Elements("Parameter");
            foreach (XElement parameterElement1 in set1ParameterElements)
            {
                string name1 = parameterElement1.Attribute("name").Value;
                if (!parameterElementByName.ContainsKey(name1))
                {
                    parameterElementByName.Add(name1, parameterElement1);
                    namesOrderedByFirstThenSecond.Add(name1);
                }
            }

            IEnumerable<XElement> set2ParameterElements = parameterSet2Element.Elements("Parameter");
            foreach (XElement parameterElement2 in set2ParameterElements)
            {
                string name = parameterElement2.Attribute("name").Value;
                if (!parameterElementByName.ContainsKey(name))
                {
                    parameterElementByName.Add(name, parameterElement2);
                    namesOrderedByFirstThenSecond.Add(name);
                }
                else
                {
                    // merge the two parameter elements that share the name.
                    var parameterElement1 = parameterElementByName[name];
                    XElement combinedParameterElement = CombineChildrenOfTwoElementByValue(parameterElement1, parameterElement2);
                    parameterElementByName[name] = combinedParameterElement;
                }
            }

            foreach (string parameterName in namesOrderedByFirstThenSecond)
            {
                combinedParameterSetElement.Add(parameterElementByName[parameterName]);
            }

            return combinedParameterSetElement;
        }

        /// <summary>
        /// Process data from connection info.
        /// </summary>
        /// <param name="connInfoList">The connection info list.</param>
        /// <param name="standardEntryList">The standard entry list.</param>
        /// <param name="standardTableNmList">The standard table name list.</param>
        /// <param name="foundConnCnt">The input found count.</param>
        /// <param name="tableNm">The table name.</param>
        /// <param name="priKeyList">The priKey list.</param>
        /// <param name="oneDbConnInfo">The connection info to use as DB target.</param>
        /// <param name="paraList">The parameter list.</param>
        /// <param name="queryStr">The query string.</param>
        /// <returns>The output found count.</returns>
        private static int HandleLOMain(List<DbConnectionInfo> connInfoList, List<string> standardEntryList, List<string> standardTableNmList, int foundConnCnt, string tableNm, List<string> priKeyList, DbConnectionInfo oneDbConnInfo, List<SqlParameter> paraList, string queryStr)
        {
            var stdEList = standardEntryList;
            var stdTList = standardTableNmList;
            var connList = connInfoList;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                bool firstRow = true;
                while (reader.Read())
                {
                    if (firstRow)
                    {
                        firstRow = false;
                        foundConnCnt++;
                    }

                    StringBuilder oneEntry = new StringBuilder(tableNm + "|");
                    foreach (string priKey in priKeyList)
                    {
                        oneEntry.Append(priKey + "|" + Convert.ToString(reader[priKey]).TrimWhitespaceAndBOM() + "|");
                    }

                    stdEList.Add(oneEntry.ToString().TrimEnd('|'));
                    stdTList.Add(tableNm);
                    connList.Add(oneDbConnInfo);
                }
            };

            DBSelectUtility.ProcessDBData(oneDbConnInfo, queryStr, null, paraList, readHandler);
            return foundConnCnt;
        }

        /// <summary>
        /// Process data from data source.
        /// </summary>
        /// <param name="connInfoList">The connection info list.</param>
        /// <param name="standardEntryList">The standard entry list.</param>
        /// <param name="standardTableNmList">The standard table name list.</param>
        /// <param name="dataSrc">The data source to use as DB target.</param>
        /// <param name="foundConnCnt">The input found count.</param>
        /// <param name="tableNm">The table name.</param>
        /// <param name="priKeyList">The priKey list.</param>
        /// <param name="paraList">The parameter list.</param>
        /// <param name="queryStr">The query string.</param>
        /// <returns>The output found count.</returns>
        private static int HandleDataSrc(List<DbConnectionInfo> connInfoList, List<string> standardEntryList, List<string> standardTableNmList, DataSrc dataSrc, int foundConnCnt, string tableNm, List<string> priKeyList, List<SqlParameter> paraList, string queryStr)
        {
            var stdEList = standardEntryList;
            var stdTList = standardTableNmList;
            var connList = connInfoList;
            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                bool firstRow = true;
                while (reader.Read())
                {
                    if (firstRow)
                    {
                        firstRow = false;
                        foundConnCnt++;
                    }

                    StringBuilder oneEntry = new StringBuilder(tableNm + "|");
                    foreach (string priKey in priKeyList)
                    {
                        oneEntry.Append(priKey + "|" + Convert.ToString(reader[priKey]).TrimWhitespaceAndBOM() + "|");
                    }

                    stdEList.Add(oneEntry.ToString().TrimEnd('|'));
                    stdTList.Add(tableNm);
                    connList.Add(null);
                }
            };

            DBSelectUtility.ProcessDBData(dataSrc, queryStr, null, paraList, readHandler);
            return foundConnCnt;
        }

        /// <summary>
        /// Writes from filedb to the xml being exported.
        /// </summary>
        /// <param name="errorList">Record errors in list.</param>
        /// <param name="whitelistDictionary">A white list dictionary.</param>
        /// <param name="needUpdateWhitelist">Check if need to update whitelist after all the export actions.</param>
        /// <param name="exportXmlWriter">An xml writer passes in to write file database block xml.</param>
        /// <param name="filedbEntry">Where the file is stored.</param>
        /// <param name="filedbKeyType">The type of file this represents.</param>
        /// <param name="encodingNm">The encoding that was used when serializing the object for the file.</param>
        /// <param name="fileFormat">The format that was used when serializing the object for the file.</param>
        /// <param name="escapeMethod">The method to be used for escaping characters.</param>
        /// <param name="needDebugInfo">If true, it will show the log information in the Paul Bunyan message viewer. It has many message. </param>
        private static void WriteFileDBEntryToExportXML(
            ref List<string> errorList,
            ref Dictionary<string, WhitelistData> whitelistDictionary,
            ref bool needUpdateWhitelist,
            XmlWriter exportXmlWriter,
            FileDBEntryInfo filedbEntry,
            E_FileDBKeyType filedbKeyType,
            string encodingNm,
            string fileFormat,
            string escapeMethod,
            bool needDebugInfo)
        {
            Encoding encoding = Encoding.GetEncoding(encodingNm);
            var filedbKeyTypeName = filedbKeyType.ToString("g");

            string rawData = string.Empty;
            bool doesFileExist = FileDBTools.DoesFileExist(filedbEntry.FileDBType, filedbEntry.Key);
            if (!doesFileExist)
            {
                return;
            }

            rawData = encoding.GetString(FileDBTools.ReadData(filedbEntry.FileDBType, filedbEntry.Key));
            string filterData = string.Empty;
            exportXmlWriter.WriteStartElement(filedbKeyTypeName);
            exportXmlWriter.WriteAttributeString("filedbKey", filedbEntry.Key);
            exportXmlWriter.WriteAttributeString("encoding", encodingNm);
            exportXmlWriter.WriteAttributeString("database", filedbEntry.FileDBType.ToString());

            if (filedbKeyTypeName == "Normal_Loan")
            {
                rawData = ShareImportExport.UnEscapeXmlCharacter(rawData);
                int avoidBomInd = rawData.IndexOf('<');
                string rawDataWithNoBom = rawData.Substring(avoidBomInd);
                filterData = ShareImportExport.FilterLiaAssetByWhitelistHardcode(ref whitelistDictionary, ref needUpdateWhitelist, filedbKeyTypeName, rawDataWithNoBom);
            }
            else if (fileFormat == "XML")
            {
                ////int avoidBomInd = rawData.IndexOf('<');
                ////string rawDataWithNoBom = rawData.Substring(avoidBomInd);
                rawData = ShareImportExport.UnEscapeXmlCharacter(rawData);
                filterData = ShareImportExport.FilterXmlByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, filedbKeyTypeName, rawData, escapeMethod, needDebugInfo);
            }
            else if (fileFormat == "JSON")
            {
                ////int avoidBomInd = rawData.IndexOf('[');
                ////string rawDataWithNoBom = rawData.Substring(avoidBomInd);
                filterData = ShareImportExport.FilterJsonByWhitelist(ref errorList, ref whitelistDictionary, ref needUpdateWhitelist, filedbKeyTypeName, rawData, escapeMethod, needDebugInfo);
            }
            else
            {
                throw new ArgumentException(fileFormat + " is not a handled file format.");
            }

            exportXmlWriter.WriteCData(filterData);
            exportXmlWriter.WriteEndElement();
        }
    }
}
