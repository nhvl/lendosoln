﻿//-----------------------------------------------------------------------
// <copyright file="LoanOverwriteTool.cs" company="Meridianlink">
//     Copyright (c) Sprocket Enterprises. All rights reserved.
// </copyright>
// <author>Zijing Jia</author>
// <summary>Loan overwrite tool.</summary>
//-----------------------------------------------------------------------

namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Adapter;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;

    /// <summary>
    /// Toolkit for loan overwrite tool.
    /// </summary>
    public class LoanOverwriteTool
    {
        /// <summary>
        /// When importing a loan from another broker into a different broker, we can't keep some fields.  <para></para>
        /// Here we keep track of what we should default those fields to.
        /// </summary>
        public static readonly System.Collections.ObjectModel.ReadOnlyDictionary<string, object> FieldDefaultsForFieldsReferringToExternalBrokerByFieldName = new System.Collections.ObjectModel.ReadOnlyDictionary<string, object>(new Dictionary<string, object>()
        {
            { "sInvestorRolodexId", -1 },
            { "sWarehouseLenderRolodexId", -1 },
            { "sSubservicerRolodexId", -1 }
        });

        /// <summary>
        /// For matching CData's in XML.
        /// </summary>
        private const string CDataPattern = @"&lt;.+&gt;";

        /// <summary>
        /// Export one column of table into xml for simple export only.
        /// </summary>
        /// <param name="whitelistDictionary">Dictionary that map table name to a set of white list.</param> 
        /// <param name="tableMap">Map column name to its column information.</param> 
        /// <param name="exportXmlWriter">Writer to export xml.</param> 
        /// <param name="reader">Reader that read by sql to fetch field value.</param> 
        /// <param name="startTag">The xml tag for this column.</param> 
        /// <param name="tableNm">Table this column belongs to.</param> 
        /// <param name="colName">The name of this column.</param> 
        public static void ExportOneColumn_Simple(
            ref Dictionary<string, WhitelistData> whitelistDictionary,
            Dictionary<string, ColumnInfo> tableMap,
            XmlWriter exportXmlWriter,
            DbDataReader reader,
            string startTag,
            string tableNm,
            string colName)
        {
            bool isExport = whitelistDictionary[tableNm].ExportSet.Contains(colName);
            bool isReview = whitelistDictionary[tableNm].ReviewSet.Contains(colName);
            bool isNoExport = whitelistDictionary[tableNm].NoImportExportSet.Contains(colName);
            bool isProtect = whitelistDictionary[tableNm].ProtectedDictionary.ContainsKey(colName);

            if (isNoExport)
            {
                return;
            }

            // !isNoExport
            if (!(isProtect || isExport || isReview)) 
            {
                whitelistDictionary[tableNm].MissingSet.Add(colName);
                return;
            }

            // !isNoExport && (isProtect || isExport || isReview)
            exportXmlWriter.WriteStartElement(startTag);
            exportXmlWriter.WriteAttributeString("n", colName);
            if (isProtect)
            {
                exportXmlWriter.WriteAttributeString("v", whitelistDictionary[tableNm].ProtectedDictionary[colName]);
            }
            else if (isExport)
            {
                string colValue = ShareImportExport.ExportDataConvert(tableMap[colName].DataType, reader[colName]);
                bool isCData = false;
                if (reader.IsDBNull(reader.GetOrdinal(colName)) || (((ColumnInfo)tableMap[colName]).DataType == "uniqueidentifier" && colValue.TrimWhitespaceAndBOM() == string.Empty))
                {
                    if (((ColumnInfo)tableMap[colName]).Nullable)
                    {
                        exportXmlWriter.WriteAttributeString("v", "null");
                    }
                    else
                    {
                        exportXmlWriter.WriteAttributeString("v", string.Empty);
                    }
                }
                else if (System.Text.RegularExpressions.Regex.IsMatch(colValue, CDataPattern))
                {
                    isCData = true;
                }
                else
                {
                    exportXmlWriter.WriteAttributeString("v", colValue);
                }

                ColumnInfo curColInfo = (ColumnInfo)tableMap[colName];
                if (curColInfo.Nullable)
                {
                    exportXmlWriter.WriteAttributeString("isNullable", "1");
                }

                if (curColInfo.IsComputed || curColInfo.DataType == "timestamp" || curColInfo.IsPrimaryKey || curColInfo.IsIdentity || curColInfo.IsForeignKey || colName == "sLNm")
                {
                    exportXmlWriter.WriteAttributeString("isReadonly", "1");
                }

                if (isCData)
                {
                    colValue = ShareImportExport.ClearNullCharacter(colValue);
                    exportXmlWriter.WriteCData(colValue);
                }
            }

            exportXmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Execute update scripts in one transaction.
        /// </summary>
        /// <param name="errorList">List records errors in the transaction and before transaction.</param> 
        /// <param name="connInfo">The connection information used for query.</param>
        /// <param name="storedProcedureNmList">A list of stored procedure names.</param>
        /// <param name="sqlParaListList">A list of sql parameter lists.</param>
        /// <param name="justinOneSqlQueryTimeOutInSeconds">The sql time out setting for sql query.</param>
        /// <param name="needDebugInfo">Check if need debug information. Not in use yet.</param>
        public static void ExecSqlInOneTransaction_LoanUpdate(
            ref List<string> errorList,
            DbConnectionInfo connInfo,
            List<string> storedProcedureNmList,
            List<List<SqlParameter>> sqlParaListList,
            int? justinOneSqlQueryTimeOutInSeconds,
            bool needDebugInfo)
        {
            bool isException = true;
            DbCommand cmd = null;
            DbTransaction tx = null;
            int i = 0;
            try
            {
                using (DbConnection conn = connInfo.GetConnection())
                {
                    conn.OpenWithRetry();
                    tx = conn.BeginTransaction(IsolationLevel.RepeatableRead);
                    cmd = conn.CreateCommand();
                    cmd.Transaction = tx;
                    int numOfSp = storedProcedureNmList.Count();
                    for (i = 0; i < numOfSp; i++)
                    {
                        if (needDebugInfo)
                        {
                            Tools.LogInfo("Justin Debug: execute sp " + storedProcedureNmList[i]);
                        }

                        ShareImportExport.ExecuteNonQueryBySp(conn, cmd, justinOneSqlQueryTimeOutInSeconds, storedProcedureNmList[i], sqlParaListList[i]);
                    }

                    tx.Commit();
                    tx = null;
                }

                isException = false;
            }
            catch (SqlException exc)
            {
                Tools.LogError(exc.ToString() + "   \n\n" + exc.StackTrace);
                if (tx != null)
                {
                    tx.Rollback();
                    tx = null;
                    Tools.LogWarning("roll back the code in transaction at LoanOverwriteTool.cs");
                }

                throw exc;
            }
            finally
            {
                if (isException)
                {
                    if (i >= storedProcedureNmList.Count())
                    {
                        Tools.LogError("Finish all sql command and encounter error at last moment. \n\nLast Command: sql[" + (i - 1) + "] " + storedProcedureNmList[i - 1] + "\n\n");
                    }
                    else
                    {
                        StringBuilder allParaStr = new StringBuilder();
                        foreach (SqlParameter para in sqlParaListList[i])
                        {
                            allParaStr.Append("@" + para.ToString() + "='" + para.SqlValue + "',\n");
                        }

                        Tools.LogError("Fail at sql[" + i + "]: \nExec " + storedProcedureNmList[i] + "\n" + allParaStr.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// If this is production server, certain fields must be set to designated values to enable the xml import.
        /// </summary>
        /// <param name="loanId">This is loan id.</param> 
        /// <returns>True: you can import xml. False: this is product server and you haven't set the fields into nuke value to enable the import.</returns>
        public static bool CheckNukeFieldsForProdServer(string loanId)
        {
            string stageSiteSourceName = ConstAppDavid.CurrentServerLocation.ToString();
            if (stageSiteSourceName == "Production")
            {
                SqlParameter[] parameters = 
                                            {
                                                new SqlParameter("@LoanId", loanId)
                                            };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader_OBSOLETE_DO_NOT_USE("Justin_Testing_CheckIfTableEligibleForImport", parameters))
                {
                    if (!reader.Read())
                    {
                        return false;
                    }

                    reader.NextResult();
                    if (!reader.Read())
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Generate update script for one table entry by its "sqlParameter" list.
        /// </summary>
        /// <param name="sqlScript">Result of generated sql script.</param> 
        /// <param name="testSqlScript">The script that contains the field value, directly runnable at sql studio.</param> 
        /// <param name="oneTableParaList">Parameter list of current table entry.</param> 
        /// <param name="oneTableColumnInfo">The column information of current table.</param> 
        /// <param name="tableNm">Current table name.</param> 
        /// <param name="prkNmValList">A list stores both primary key names and values consecutively.</param> 
        public static void GetUpdateScript_LoanOverwrite(
            ref string sqlScript,
            ref string testSqlScript,
            List<SqlParameter> oneTableParaList,
            Dictionary<string, ColumnInfo> oneTableColumnInfo,
            string tableNm,
            List<string> prkNmValList)
        {
            StringBuilder headSql = new StringBuilder("UPDATE " + tableNm + " SET ");
            StringBuilder testHeadSql = new StringBuilder("UPDATE " + tableNm + " SET ");
            StringBuilder tailSql = new StringBuilder(" WHERE ");
            StringBuilder testTailSql = new StringBuilder(" WHERE ");
            int prkListSize = prkNmValList.Count();
            for (int i = 0; i < prkListSize; i += 2)
            {
                tailSql.Append(prkNmValList[i] + " = @" + prkNmValList[i] + " and ");
                testTailSql.Append(prkNmValList[i] + " = '" + prkNmValList[i + 1] + "' and ");
            }

            string tailSqlStr = tailSql.ToString().Substring(0, tailSql.Length - 5);
            int paraSize = oneTableParaList.Count();
            for (int i = 0; i < paraSize; i++)
            {
                string paraNm = oneTableParaList[i].ToString();
                string paraVl = oneTableParaList[i].Value.ToString();
                ColumnInfo curColInfo = oneTableColumnInfo[paraNm];
                if (curColInfo.IsComputed || curColInfo.DataType == "timestamp" || curColInfo.IsIdentity || curColInfo.IsPrimaryKey || curColInfo.IsForeignKey)
                {
                    continue;
                }
                else if (oneTableColumnInfo[paraNm].DataType.Contains("binary"))
                {
                    headSql.Append(paraNm + " = " + paraVl + ",");
                    testHeadSql.Append(paraNm + " = " + paraVl + ",\n");
                }
                else
                {
                    headSql.Append(paraNm + " = @" + paraNm + ",");
                    testHeadSql.Append(paraNm + " = '" + paraVl + "',\n");
                }
            }

            sqlScript = headSql.ToString().TrimEnd(',') + tailSqlStr;
            testSqlScript = testHeadSql.ToString().TrimEnd(',') + testTailSql;

            for (int i = 0; i < prkListSize; i += 2)
            {
                int ind = oneTableParaList.FindIndex(delegate(SqlParameter para)
                {
                    return para.ToString().Equals(prkNmValList[i]);
                });
                if (ind < 0)
                {
                    oneTableParaList.Add(new SqlParameter(prkNmValList[i], prkNmValList[i + 1]));
                }
            }
        }        

        /// <summary>
        /// Only import non-key value; For the loan export; when columns marked as "no touch", they won't be imported.
        /// </summary>
        /// <param name="errorList">List records errors in the import.</param> 
        /// <param name="importedTableNmList">The list of imported tables.</param> 
        /// <param name="xmlPath">File path of the import xml file.</param> 
        /// <param name="loanId">The id of current loan.</param> 
        /// <param name="userId">The id of user who calls this function.</param>
        /// <param name="justinOneSqlQueryTimeOutInSeconds">The time out setting per sql query.</param> 
        /// <param name="defaultExternalBrokerReferenceFields">If true it will default fields like sInvestorRolodexId to some default,
        /// since otherwise they could just fail.</param>
        public static void ImportOneXmlFile_LoanUpdate(
            ref List<string> errorList,
            string[] importedTableNmList,
            string xmlPath,
            string loanId,
            string userId,
            int? justinOneSqlQueryTimeOutInSeconds,
            bool defaultExternalBrokerReferenceFields)
        {
            Guid connInfoBrokerId = Guid.Empty;
            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(new Guid(loanId), out connInfoBrokerId);

            ServerLocation currentStageSiteSource = ConstAppDavid.CurrentServerLocation;
            if (currentStageSiteSource == ServerLocation.Production)
            {
                errorList.Add(@"Warning: You are not allowed to import brokers into product server!");
                return;
            }

            int debug_ind = 0;
            if (errorList.Count() > 0)
            {
                return;
            }

            XDocument xdoc = XDocument.Load(xmlPath);
            XElement filedbElement = xdoc.Root.Element("FILEDB");
            IEnumerable<XElement> groupList = xdoc.Root.Elements("Group");

            foreach (XElement group in groupList)
            {
                IEnumerable<XElement> tableList = group.Elements("table");
                int capacity = tableList.Count();
                List<string> updateStoredProcedureNmList = new List<string>(capacity);
                List<List<SqlParameter>> updateParaListList = new List<List<SqlParameter>>();
                Dictionary<string, Dictionary<string, ColumnInfo>> allTablesColumnInfo = new Dictionary<string, Dictionary<string, ColumnInfo>>();
                Dictionary<string, string> appIdMappingXmlToDb = new Dictionary<string, string>(); //// xml app id Map To database app id
                List<string> appIdList = ShareImportExport.GetAppIdListByLoanId(connInfoBrokerId, loanId);
                MatchApplicationManually(ref errorList, appIdList, group, loanId, userId); //// must match first, then get the list since this function might delete or create app
                appIdList = ShareImportExport.GetAppIdListByLoanId(connInfoBrokerId, loanId);

                HashSet<string> excludeColNmList = new HashSet<string>();
                excludeColNmList.Add("sLId");
                excludeColNmList.Add("aAppId");

                int appCnt = 0;
                foreach (XElement table in tableList)
                {
                    debug_ind++;
                    string tableNm = table.Attribute("n").Value;
                    string primaryKey = table.Attribute("val_0").Value;
                    if (!importedTableNmList.Contains(tableNm))
                    {
                        Tools.LogWarning("Loan Overwrite: The table " + tableNm + " does not belong to loan and is excluded from import.");
                        continue;
                    }

                    HashSet<string> allowedParaNmSet = new HashSet<string>();
                    List<SqlParameter> paraForSpList = new List<SqlParameter>();
                    paraForSpList.Add(new SqlParameter("spName", "Justin_Testing_Update_" + tableNm));
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfoBrokerId, "Justin_Testing_GetParameterNamesByStoredProcedureName", paraForSpList))
                    {
                        while (reader.Read())
                        {
                            string paraNm = reader["name"].ToString().TrimStart('@');
                            if (!allowedParaNmSet.Contains(paraNm))
                            {
                                allowedParaNmSet.Add(paraNm);
                            }
                        }
                    }

                    List<XElement> allowedParaElementList = new List<XElement>();
                    IEnumerable<XElement> allParaElements = table.Elements();
                    foreach (XElement paraElement in allParaElements)
                    {
                        string colNm = paraElement.Attribute("n").Value;
                        if (allowedParaNmSet.Contains(colNm))
                        {
                            allowedParaElementList.Add(paraElement);
                        }
                    }

                    List<SqlParameter> curTableParaList = new List<SqlParameter>();
                    if (!allTablesColumnInfo.ContainsKey(tableNm))
                    {
                        Dictionary<string, ColumnInfo> curTableColumnInfo = ShareImportExport.GetColumnInfoByColumnNameMap(DataSrc.LOShare, tableNm);
                        allTablesColumnInfo.Add(tableNm, curTableColumnInfo);
                    }

                    if (tableNm.StartsWith("APPLICATION_"))
                    {
                        string xmlAppId = primaryKey;
                        if (!appIdMappingXmlToDb.ContainsKey(xmlAppId))
                        {
                            appIdMappingXmlToDb.Add(xmlAppId, appIdList[appCnt++]);
                        }
                    }

                    TableReplicateTool.GetParametersByXelementList(
                        ref curTableParaList,
                        allowedParaElementList,
                        allTablesColumnInfo[tableNm],
                        excludeColNmList,
                        true, 
                        defaultExternalBrokerReferenceFields);

                    if (curTableParaList.Any())
                    {
                        updateStoredProcedureNmList.Add("Justin_Testing_Update_" + tableNm);
                        curTableParaList.Add(new SqlParameter("sLId", loanId));
                        if (tableNm.StartsWith("APPLICATION"))
                        {
                            curTableParaList.Add(new SqlParameter("aAppId", appIdMappingXmlToDb[primaryKey]));
                        }

                        updateParaListList.Add(curTableParaList);
                    }
                }

                ExecSqlInOneTransaction_LoanUpdate(ref errorList, connInfo, updateStoredProcedureNmList, updateParaListList, justinOneSqlQueryTimeOutInSeconds, true);

                if (errorList.Count() > 0)
                {
                    return;
                }

                Tools.UpdateCacheTable(new Guid(loanId), null);
            }

            if (filedbElement != null)
            {
                ShareImportExport.ImportFiledb(ref errorList, filedbElement, loanId);
            }
        }

        /// <summary>
        /// Insert a blank application. After using stored procedure, primary rank index no longer has any effect.
        /// </summary>
        /// <param name="loanId">This is loan id.</param> 
        /// <param name="appId">This is application id.</param> 
        /// <param name="primaryRankIndex">This is primary rank index.</param> 
        public static void InsertApp(string loanId, string appId, int primaryRankIndex)
        {
            StoredProcedureHelper.ExecuteNonQuery_OBSOLETE_DO_NOT_USE("CreateNewApplication", 0, new SqlParameter("@LoanId", loanId), new SqlParameter("@AppId", appId));
        }

        /// <summary>
        /// Make sure the current loan's number of applications equal to the one in the xml. 
        /// </summary>
        /// <param name="returnMsgList">Store the error message during processing.</param> 
        /// <param name="seqAppIdList">List of application id in the sequence of primary rank index.</param> 
        /// <param name="xmlContent">The xml element of the whole file.</param> 
        /// <param name="loanId">This is loan id.</param> 
        /// <param name="userId">The id of user who calls this function.</param>
        public static void MatchApplicationManually(ref List<string> returnMsgList, List<string> seqAppIdList, XElement xmlContent, string loanId, string userId)
        {
            int xmlAppNum = (from app in xmlContent.Elements("table") where app.Attribute("n").Value == "APPLICATION_A" select app).Count();
            int dbaseAppNum = seqAppIdList.Count();

            if (xmlAppNum > dbaseAppNum)
            {
                //// add applications
                int offset = xmlAppNum - dbaseAppNum;
                for (int i = 0; i < offset; i++)
                {
                    string newAppId = Guid.NewGuid().ToString();
                    InsertApp(loanId, newAppId, dbaseAppNum + i);
                    //// seqAppIdList.Add(newAppId);
                }
            }
            else if (xmlAppNum < dbaseAppNum)
            {
                //// delete applications with highest primaryRankIndex.
                int offset = dbaseAppNum - xmlAppNum;
                List<string> deletedAppList = new List<string>();
                for (int i = 0; i < offset; i++)
                {
                    deletedAppList.Add(seqAppIdList[dbaseAppNum - 1 - i]);
                }

                foreach (string curAppId in deletedAppList)
                {
                    ShareImportExport.DeleteApp(curAppId, userId);
                    seqAppIdList.Remove(curAppId);
                }
            }
        }
    }
}
