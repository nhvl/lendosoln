﻿//-----------------------------------------------------------------------
// <copyright file="ColumnInfo.cs" company="Meridianlink">
//     Copyright (c) Sprocket Enterprises. All rights reserved.
// </copyright>
// <author>Zijing Jia</author>
// <summary>Store the information of one column.</summary>
//-----------------------------------------------------------------------

namespace LendersOffice.ObjLib.Conversions.LoanFileImportExport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Store column information.
    /// </summary>
    public class ColumnInfo
    {
        /// <summary>
        /// Contains the <see cref="System.Data.SqlDbType"/> by its lowercase name, which is what appears to come form the data_type column of information schema.
        /// </summary>
        private static Dictionary<string, System.Data.SqlDbType> SqlDbTypeByLowerCaseName;

        /// <summary>
        /// Initializes SqlDbTypeByLowerCaseName.
        /// </summary>
        static ColumnInfo()
        {
            SqlDbTypeByLowerCaseName = new Dictionary<string, System.Data.SqlDbType>();
            foreach (System.Data.SqlDbType value in Enum.GetValues(typeof(System.Data.SqlDbType)))
            {
                SqlDbTypeByLowerCaseName.Add(value.ToString().ToLower(), value);
            }
        }

        /// <summary>
        /// Initializes a new instance of the ColumnInfo class.
        /// </summary>
        public ColumnInfo()
        {
            this.Nullable = false;
            this.IsPrimaryKey = false;
            this.IsForeignKey = false;
            this.IsIdentity = false;
            this.IsComputed = false;
        }

        /// <summary>
        /// Gets or sets data type of current column.
        /// </summary>
        /// <value>Data type of current column.</value>
        public string DataType { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether this column is null-able or not.
        /// </summary>
        /// <value>If current column is null-able.</value>
        public bool Nullable { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this column is primary key or not.
        /// </summary>
        /// <value>If current column is read-only, it cannot be imported. If false, it means the column is importable.</value>
        public bool IsPrimaryKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this column is a foreign key or not.
        /// </summary>
        /// <value>Boolean: is foreign key or not.</value>
        public bool IsForeignKey { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this column is computed or not.
        /// </summary>
        /// <value>Boolean: is computed column or not.</value>
        public bool IsComputed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this column is an identity key column or not.
        /// </summary>
        /// <value>Check if current column is identity key.</value>
        public bool IsIdentity { get; set; }

        /// <summary>
        /// Gets or sets the character size limit of current column. Only apply for char array database-type.
        /// </summary>
        /// <value>Character size limit of current column.</value>
        public long ColumnSize { get; set; }

        /// <summary>
        /// Tries to get the corresponding <see cref="System.Data.SqlDbType"/> of the column.
        /// </summary>
        /// <param name="sqlDbType">The <see cref="System.Data.SqlDbType"/> of the column, if found.</param>
        /// <returns>True iff it found the corresponding <see cref="System.Data.SqlDbType"/>.</returns>
        public bool TryGetSqlDbType(out System.Data.SqlDbType sqlDbType)
        {
            if (SqlDbTypeByLowerCaseName.ContainsKey(DataType))
            {
                sqlDbType = SqlDbTypeByLowerCaseName[DataType];
                return true;
            }

            sqlDbType = System.Data.SqlDbType.VarChar;
            return false;
        }
    }
}
