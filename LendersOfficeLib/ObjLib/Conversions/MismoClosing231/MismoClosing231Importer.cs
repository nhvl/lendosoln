﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using DataAccess;
using Mismo.Closing2_4;
using XmlSerializableCommon;
using LendersOffice.Constants;
using System.Linq;
using LendersOffice.Security;
using System.Text;
using LqbGrammar.DataTypes;
using LendersOffice.Common;

namespace LendersOffice.Conversions.MismoClosing231
{
    public class MismoClosing231Importer
    {
        private bool DoAppMergeLogic = false;
        private bool hasSubjectPropertyNetCashFlow;
        private decimal totalSubjectPropertyNetCashFlow;

        private CPageData CreatePageData(Guid sLId)
        {
            return CPageData.CreateUsingSmartDependency(sLId, typeof(MismoClosing231Importer));
        }

        #region Variables
        private Dictionary<string, string> m_borrowerId_Ssn_Hash = null;
        private Dictionary<string, ILiabilityRegular> m_liability_Hash = null;
        private CPageData m_dataLoan = null;
        private string m_sCondoStories = "";
        #endregion
        public bool IsSyncLoanOriginationSystemLoanIdentifierWithLoanNumber { get; set; }
        private void Reset()
        {
            m_borrowerId_Ssn_Hash = new Dictionary<string, string>();
            m_liability_Hash = new Dictionary<string, ILiabilityRegular>();
            this.hasSubjectPropertyNetCashFlow = false;
            this.totalSubjectPropertyNetCashFlow = 0;
        }
        public void Import(Guid sLId, string xml)
        {
            Reset();
            XmlReader reader = XmlReader.Create(new StringReader(xml));
            Loan loan = new Loan();
            loan.ReadXml(reader);

            m_dataLoan = CreatePageData(sLId);
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);


            //Do not delete the apps for remn do a merge
            DoAppMergeLogic = PrincipalFactory.CurrentPrincipal.BrokerId == new Guid("baf07293-f626-44c6-9750-706199a7c0b6");
            //DoAppMergeLogic = PrincipalFactory.CurrentPrincipal.BrokerId == new Guid("013ae5a4-5201-4464-94d0-109ba654957f"); //BB local Encompass test lender
            if (DoAppMergeLogic)
            {
                //clear out these fields for them we may need to do this alt he time
                m_dataLoan.sOCredit1Desc = "";
                m_dataLoan.sOCredit2Desc = "";
                m_dataLoan.sOCredit3Desc = "";
                m_dataLoan.sOCredit4Desc = "";

                m_dataLoan.sOCredit1Amt_rep = "";
                m_dataLoan.sOCredit1Amt_rep = "";
                m_dataLoan.sOCredit2Amt_rep = "";
                m_dataLoan.sOCredit3Amt_rep = "";

                m_dataLoan.sOCredit1Lckd = false;

                m_dataLoan.sOCredit4Amt_rep = "";
                m_dataLoan.sSpCountRentalIForPrimaryResidToo = false; // BB OPM 105695
                m_dataLoan.sProOHExpLckd = false; // BB OPM 107547
                m_dataLoan.sProOHExp_rep = "";
                m_dataLoan.sProOHExpDesc = "";
                ClearIncomeInfoForAllApps();
            }
            if (DoAppMergeLogic == false || CanMergeApps(loan.Application.BorrowerList) == false)
            {
                DeleteAllBorrowerInformation();
            }
            Parse(loan);

            m_dataLoan.Save();
        }
        private void Parse(Loan loan)
        {
            //loan.MismoVersionIdentifier;
            Parse(loan.Application);
            Parse(loan.ClosingDocuments);
        }
        private void Parse(ClosingDocuments closingDocuments)
        {
            Parse(closingDocuments.AllongeToNote);
            Parse(closingDocuments.Beneficiary);
            Parse(closingDocuments.ClosingAgentList);
            Parse(closingDocuments.ClosingInstructionsList);
            Parse(closingDocuments.CompensationList);
            Parse(closingDocuments.CosignerList);
            Parse(closingDocuments.EscrowAccountDetailList);
            Parse(closingDocuments.Execution);
            Parse(closingDocuments.Investor);
            Parse(closingDocuments.Lender);
            Parse(closingDocuments.LenderBranch);
            Parse(closingDocuments.LoanDetails);
            Parse(closingDocuments.LossPayeeList);
            Parse(closingDocuments.MortgageBroker);
            Parse(closingDocuments.PaymentDetails);
            Parse(closingDocuments.PayoffList);
            Parse(closingDocuments.RecordableDocumentList);
            Parse(closingDocuments.RespaHudDetailList);
            Parse(closingDocuments.RespaServicingData);
            Parse(closingDocuments.RespaSummary);
            Parse(closingDocuments.SellerList);
            Parse(closingDocuments.ServicerList);
            Parse(closingDocuments.BuilderList);
            Parse(closingDocuments.ClosingCostList);
            Parse(closingDocuments.TrustList);
        }
        private void Parse(List<Trust> trustList)
        {
            foreach (var trust in trustList)
            {
                //trust.Id;
                //trust.NonPersonEntityIndicator;
                //trust.EstablishedDate;
                //trust.Name;
                //trust.State;
                //trust.NonObligatedIndicator;
                Parse(trust.NonPersonEntityDetail);
                Parse(trust.BeneficiaryList);
                Parse(trust.TrusteeList);
                Parse(trust.GrantorList);
            }
        }
        private void Parse(List<Grantor> grantorList)
        {
            foreach (var grantor in grantorList)
            {
                //grantor.Id;
                //grantor.MaritalStatusType;
                //grantor.NonPersonEntityIndicator;
                //grantor.CapacityDescription;
                //grantor.City;
                //grantor.Country;
                //grantor.County;
                //grantor.FirstName;
                //grantor.LastName;
                //grantor.MiddleName;
                //grantor.NameSuffix;
                //grantor.PostalCode;
                //grantor.SequenceIdentifier;
                //grantor.State;
                //grantor.StreetAddress;
                //grantor.StreetAddress2;
                //grantor.UnparsedName;
                Parse(grantor.AliasList);
            }
        }
        private void Parse(List<Alias> aliasList)
        {
            foreach (var alias in aliasList)
            {
                //alias.Id;
                //alias.AccountIdentifier;
                //alias.CreditorName;
                //alias.FirstName;
                //alias.LastName;
                //alias.MiddleName;
                //alias.NameSuffix;
                //alias.SequenceIdentifier;
                //alias.Type;
                //alias.TypeOtherDescription;
                //alias.UnparsedName;
            }
        }
        private void Parse(List<Trustee> trusteeList)
        {
            foreach (var trustee in trusteeList)
            {
                //trustee.Id;
                //trustee.NonPersonEntityIndicator;
                //trustee.City;
                //trustee.Country;
                //trustee.County;
                //trustee.PostalCode;
                //trustee.State;
                //trustee.StreetAddress;
                //trustee.StreetAddress2;
                //trustee.Type;
                //trustee.TypeOtherDescription;
                //trustee.UnparsedName;
                Parse(trustee.ContactDetail);
                Parse(trustee.NonPersonEntityDetail);
            }
        }
        private void Parse(NonPersonEntityDetail nonPersonEntityDetail)
        {
            //nonPersonEntityDetail.Id;
            //nonPersonEntityDetail.MersOrganizationIdentifier;
            //nonPersonEntityDetail.OrganizationLicensingTypeDescription;
            //nonPersonEntityDetail.OrganizationType;
            //nonPersonEntityDetail.OrganizationTypeOtherDescription;
            //nonPersonEntityDetail.OrganizedUnderTheLawsOfJurisdictionName;
            //nonPersonEntityDetail.SuccessorClauseTextDescription;
            //nonPersonEntityDetail.TaxIdentificationNumberIdentifier;
            Parse(nonPersonEntityDetail.AuthorizedRepresentativeList);
        }
        private void Parse(List<AuthorizedRepresentative> authorizedRepresentativeList)
        {
            foreach (var authorizedRepresentative in authorizedRepresentativeList)
            {
                //authorizedRepresentative.Id;
                //authorizedRepresentative.AuthorizedToSignIndicator;
                //authorizedRepresentative.TitleDescription;
                //authorizedRepresentative.UnparsedName;
                Parse(authorizedRepresentative.ContactDetail);
            }
        }
        private void Parse(ContactDetail contactDetail)
        {
            //contactDetail.Id;
            //contactDetail.FirstName;
            //contactDetail.Identifier;
            //contactDetail.LastName;
            //contactDetail.MiddleName;
            //contactDetail.Name;
            //contactDetail.NameSuffix;
            //contactDetail.SequenceIdentifier;
            Parse(contactDetail.ContactPointList);
        }
        private void Parse(List<ContactPoint> contactPointList)
        {
            foreach (var contactPoint in contactPointList)
            {
                //contactPoint.Id;
                //contactPoint.PreferenceIndicator;
                //contactPoint.RoleType;
                //contactPoint.RoleTypeOtherDescription;
                //contactPoint.Type;
                //contactPoint.TypeOtherDescription;
                //contactPoint.Value;
            }
        }
        private void Parse(List<Beneficiary> beneficiaryList)
        {
            foreach (var beneficiary in beneficiaryList)
            {
                //beneficiary.Id;
                //beneficiary.NonPersonEntityIndicator;
                //beneficiary.City;
                //beneficiary.Country;
                //beneficiary.County;
                //beneficiary.PostalCode;
                //beneficiary.State;
                //beneficiary.StreetAddress;
                //beneficiary.StreetAddress2;
                //beneficiary.UnparsedName;
                Parse(beneficiary.ContactDetail);
                Parse(beneficiary.NonPersonEntityDetail);
            }
        }
        private void Parse(List<ClosingCost> closingCostList)
        {
            foreach (var closingCost in closingCostList)
            {
                //closingCost.Id;
                //closingCost.ContributionAmount;
                //closingCost.FinancedIndicator;
                //closingCost.FundsType;
                //closingCost.FundsTypeOtherDescription;
                //closingCost.SourceType;
                //closingCost.SourceTypeOtherDescription;
            }
        }
        private void Parse(List<Builder> builderList)
        {
            foreach (var builder in builderList)
            {
                //builder.Id;
                //builder.NonPersonEntityIndicator;
                //builder.City;
                //builder.Country;
                //builder.County;
                //builder.LIcenseIdentifier;
                //builder.LicenseState;
                //builder.PostalCode;
                //builder.State;
                //builder.StreetAddress;
                //builder.StreetAddress2;
                //builder.UnparsedName;
                Parse(builder.NonPersonEntityDetail);
            }
        }
        private void Parse(List<Servicer> servicerList)
        {
            foreach (var servicer in servicerList)
            {
                //servicer.Id;
                //servicer.NonPersonEntityIndicator;
                //servicer.City;
                //servicer.Country;
                //servicer.DaysOfTheWeekDescription;
                //servicer.DirectInquiryToDescription;
                //servicer.HoursOfTheDayDescription;
                //servicer.InquiryTelephoneNumber;
                //servicer.Name;
                //servicer.PaymentAcceptanceEndDate;
                //servicer.PaymentAcceptanceStartDate;
                //servicer.PostalCode;
                //servicer.State;
                //servicer.StreetAddress;
                //servicer.StreetAddress2;
                //servicer.TransferEffectiveDate;
                //servicer.Type;
                //servicer.TypeOtherDescription;
                Parse(servicer.ContactDetail);
                Parse(servicer.NonPersonEntityDetail);
                Parse(servicer.QualifiedWrittenRequestMailTo);
            }
        }
        private void Parse(QualifiedWrittenRequestMailTo qualifiedWrittenRequestMailTo)
        {
            //qualifiedWrittenRequestMailTo.Id;
            //qualifiedWrittenRequestMailTo.City;
            //qualifiedWrittenRequestMailTo.Country;
            //qualifiedWrittenRequestMailTo.PostalCode;
            //qualifiedWrittenRequestMailTo.State;
            //qualifiedWrittenRequestMailTo.StreetAddress;
            //qualifiedWrittenRequestMailTo.StreetAddress2;
        }
        private void Parse(List<Seller> sellerList)
        {
            foreach (var seller in sellerList)
            {
                //seller.Id;
                //seller.NonPersonEntityIndicator;
                //seller.City;
                //seller.Country;
                //seller.FirstName;
                //seller.Identifier;
                //seller.LastName;
                //seller.MiddleName;
                //seller.NameSuffix;
                //seller.PostalCode;
                //seller.SequenceIdentifier;
                //seller.State;
                //seller.StreetAddress;
                //seller.StreetAddress2;
                //seller.UnparsedName;
                Parse(seller.ContactDetail);
                Parse(seller.NonPersonEntityDetail);
                Parse(seller.PowerOfAttorney);
            }
        }
        private void Parse(PowerOfAttorney powerOfAttorney)
        {
            //powerOfAttorney.Id;
            //powerOfAttorney.SigningCapacityTextDescription;
            //powerOfAttorney.TitleDescription;
            //powerOfAttorney.UnparsedName;
        }
        private void Parse(RespaSummary respaSummary)
        {
            //respaSummary.Id;
            //respaSummary.Apr;
            //respaSummary.DisclosedTotalSalesPriceAmount;
            //respaSummary.TotalAprFeesAmount;
            //respaSummary.TotalAmountFinancedAmount;
            //respaSummary.TotalDepositedReservesAmount;
            //respaSummary.TotalFilingRecordingFeeAmount;
            //respaSummary.TotalFinanceChargeAmount;
            //respaSummary.TotalNetBorrowerFeesAmount;
            //respaSummary.TotalNetProceedsForFundingAmount;
            //respaSummary.TotalNetSellerFeesAmount;
            //respaSummary.TotalNonAprFeesAmount;
            //respaSummary.TotalOfAllPaymentsAmount;
            //respaSummary.TotalPaidToOthersAmount;
            //respaSummary.TotalPrepaidFinanceChargeAmount;
            Parse(respaSummary.TotalFeesPaidToList);
            Parse(respaSummary.TotalFeesPaidByList);
        }
        private void Parse(List<TotalFeesPaidBy> totalFeesPaidByList)
        {
            foreach (var totalFeesPaidBy in totalFeesPaidByList)
            {
                //totalFeesPaidBy.Id;
                //totalFeesPaidBy.Type;
                //totalFeesPaidBy.TypeAmount;
                //totalFeesPaidBy.TypeOtherDescription;
            }
        }
        private void Parse(List<TotalFeesPaidTo> totalFeesPaidToList)
        {
            foreach (var totalFeesPaidTo in totalFeesPaidToList)
            {
                //totalFeesPaidTo.Id;
                //totalFeesPaidTo.Type;
                //totalFeesPaidTo.TypeAmount;
                //totalFeesPaidTo.TypeOtherDescription;
            }
        }
        private void Parse(RespaServicingData respaServicingData)
        {
            //respaServicingData.Id;
            //respaServicingData.AreAbleToServiceIndicator;
            //respaServicingData.AssignSellOrTransferSomeServicingIndicator;
            //respaServicingData.DoNotServiceIndicator;
            //respaServicingData.ExpectToAssignSellOrTransferPercent;
            //respaServicingData.ExpectToAssignSellOrTransferPercentOfServicingIndicator;
            //respaServicingData.ExpectToRetainAllServicingIndicator;
            //respaServicingData.ExpectToSellAllServicingIndicator;
            //respaServicingData.FirstTransferYear;
            //respaServicingData.FirstTransferYearValue;
            //respaServicingData.HaveNotDecidedToServiceIndicator;
            //respaServicingData.HaveNotServicedInPastThreeYearsIndicator;
            //respaServicingData.HavePreviouslyAssignedServicingIndicator;
            //respaServicingData.MayAssignServicingIndicator;
            //respaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator;
            //respaServicingData.SecondTransferYear;
            //respaServicingData.SecondTransferYearValue;
            //respaServicingData.ThirdTransferYear;
            //respaServicingData.ThirdTransferYearValue;
            //respaServicingData.ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator;
            //respaServicingData.ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator;
            //respaServicingData.ThisIsOurRecordOfTransferringServicingIndicator;
            //respaServicingData.TwelveMonthPeriodTransferPercent;
            //respaServicingData.WillNotServiceIndicator;
            //respaServicingData.WillServiceIndicator;
        }
        private void Parse(List<RespaHudDetail> respaHudDetailList)
        {
            foreach (var respaHudDetail in respaHudDetailList)
            {
                //respaHudDetail.Id;
                //respaHudDetail.Hud1CashToOrFromBorrowerIndicator;
                //respaHudDetail.Hud1CashToOrFromSellerIndicator;
                //respaHudDetail.Hud1ConventionalInsuredIndicator;
                //respaHudDetail.Hud1FileNumberIdentifier;
                //respaHudDetail.Hud1LenderUnparsedAddress;
                //respaHudDetail.Hud1LenderUnparsedName;
                //respaHudDetail.Hud1LineItemFromDate;
                //respaHudDetail.Hud1LineItemToDate;
                //respaHudDetail.Hud1SettlementAgentUnparsedAddress;
                //respaHudDetail.Hud1SettlementAgentUnparsedName;
                //respaHudDetail.Hud1SettlementDate;
                //respaHudDetail.LineItemAmount;
                //respaHudDetail.LineItemDescription;
                //respaHudDetail.LineItemPaidByType;
                //respaHudDetail.SpecifiedHudLineNumber;
            }
        }
        private void Parse(List<RecordableDocument> recordableDocumentList)
        {
            foreach (var recordableDocument in recordableDocumentList)
            {
                //recordableDocument.Id;
                //recordableDocument.SequenceIdentifier;
                //recordableDocument.Type;
                //recordableDocument.TypeOtherDescription;
                Parse(recordableDocument.AssignFrom);
                Parse(recordableDocument.AssignTo);
                Parse(recordableDocument.Default);
                Parse(recordableDocument.NotaryList);
                Parse(recordableDocument.PreparedBy);
                Parse(recordableDocument.AssociatedDocument);
                Parse(recordableDocument.ReturnToList);
                Parse(recordableDocument.Riders);
                Parse(recordableDocument.SecurityInstrument);
                Parse(recordableDocument.TrusteeList);
                Parse(recordableDocument.WitnessList);
            }
        }
        private void Parse(List<Witness> witnessList)
        {
            foreach (var witness in witnessList)
            {
                //witness.Id;
                //witness.SequenceIdentifier;
                //witness.UnparsedName;
            }
        }
        private void Parse(SecurityInstrument securityInstrument)
        {
            //securityInstrument.Id;
            //securityInstrument.PropertyLongLegalDescriptionPageNumberDescription;
            //securityInstrument.PropertyLongLegalPageNumberDescription;
            //securityInstrument.RecordedDocumentIdentifier;
            //securityInstrument.AssumptionFeeAmount;
            //securityInstrument.AttorneyFeeMinimumAmount;
            //securityInstrument.AttorneyFeePercent;
            //securityInstrument.CertifyingAttorneyName;
            //securityInstrument.MaximumPrincipalIndebtednessAmount;
            //securityInstrument.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator;
            //securityInstrument.NoteHolderName;
            //securityInstrument.NoticeOfConfidentialtyRightsDescription;
            //securityInstrument.OtherFeesAmount;
            //securityInstrument.OtherFeesDescription;
            //securityInstrument.OweltyOfPartitionIndicator;
            //securityInstrument.PersonAuthorizedToReleaseLienName;
            //securityInstrument.PersonAuthorizedToReleaseLienPhoneNumber;
            //securityInstrument.PersonAuthorizedToReleaseLienTitle;
            //securityInstrument.PurchaseMoneyIndicator;
            //securityInstrument.RealPropertyImprovedOrToBeImprovedIndicator;
            //securityInstrument.RealPropertyImprovementsNotCoveredIndicator;
            //securityInstrument.RecordingRequestedByName;
            //securityInstrument.RenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator;
            //securityInstrument.SignerForRegistersOfficeName;
            //securityInstrument.TaxSerialNumberIdentifier;
            //securityInstrument.TrusteeFeePercent;
            //securityInstrument.VendorsLienDescription;
            //securityInstrument.VendorsLienIndicator;
            //securityInstrument.VestingDescription;
            Parse(securityInstrument.TaxablePartyList);
        }
        private void Parse(List<TaxableParty> taxablePartyList)
        {
            foreach (var taxableParty in taxablePartyList)
            {
                //taxableParty.Id;
                //taxableParty.City;
                //taxableParty.Country;
                //taxableParty.CountryCode;
                //taxableParty.County;
                //taxableParty.PostalCode;
                //taxableParty.SequenceIdentifier;
                //taxableParty.State;
                //taxableParty.StreetAddress;
                //taxableParty.StreetAddress2;
                //taxableParty.TitleDescription;
                //taxableParty.UnparsedName;
                Parse(taxableParty.PreferredResponseList);
                Parse(taxableParty.ContactDetail);
            }
        }
        private void Parse(List<PreferredResponse> preferredResponseList)
        {
            foreach (var preferredResponse in preferredResponseList)
            {
                //preferredResponse.Id;
                //preferredResponse.MimeType;
                //preferredResponse.Destination;
                //preferredResponse.Format;
                //preferredResponse.FormatOtherDescription;
                //preferredResponse.Method;
                //preferredResponse.MethodOther;
                //preferredResponse.UseEmbeddedFileIndicator;
                //preferredResponse.VersionIdentifier;
            }
        }
        private void Parse(Riders riders)
        {
            //riders.Id;
            //riders.AdjustableRateRiderIndicator;
            //riders.BalloonRiderIndicator;
            //riders.BiweeklyPaymentRiderIndicator;
            //riders.CondominiumRiderIndicator;
            //riders.GraduatedPaymentRiderIndicator;
            //riders.GrowingEquityRiderIndicator;
            //riders.NonOwnerOccupancyRiderIndicator;
            //riders.OneToFourFamilyRiderIndicator;
            //riders.OtherRiderDescription;
            //riders.OtherRiderIndicator;
            //riders.PlannedUnitDevelopmentRiderIndicator;
            //riders.RateImprovementRiderIndicator;
            //riders.RehabilitationLoanRiderIndicator;
            //riders.SecondHomeRiderIndicator;
            //riders.VaRiderIndicator;
        }
        private void Parse(List<ReturnTo> returnToList)
        {
            foreach (var returnTo in returnToList)
            {
                //returnTo.Id;
                //returnTo.NonPersonEntityIndicator;
                //returnTo.City;
                //returnTo.Country;
                //returnTo.CountryCode;
                //returnTo.County;
                //returnTo.CountyFipsCode;
                //returnTo.ElectronicRoutingAddress;
                //returnTo.ElectronicRoutingMethodType;
                //returnTo.PostalCode;
                //returnTo.State;
                //returnTo.StateFipsCode;
                //returnTo.StreetAddress;
                //returnTo.StreetAddress2;
                //returnTo.TitleDescription;
                //returnTo.UnparsedName;
                Parse(returnTo.ContactDetail);
                Parse(returnTo.NonPersonEntityDetail);
            }
        }
        private void Parse(AssociatedDocument associatedDocument)
        {
            //associatedDocument.Id;
            //associatedDocument.BookNumber;
            //associatedDocument.BookType;
            //associatedDocument.BookTypeOtherDescription;
            //associatedDocument.CountyOfRecordationName;
            //associatedDocument.InstrumentNumber;
            //associatedDocument.Number;
            //associatedDocument.OfficeOfRecordationName;
            //associatedDocument.PageNumber;
            //associatedDocument.RecordingDate;
            //associatedDocument.RecordingJurisdictionName;
            //associatedDocument.StateOfRecordationName;
            //associatedDocument.TitleDescription;
            //associatedDocument.Type;
            //associatedDocument.TypeOtherDescription;
            //associatedDocument.VolumeNumber;
        }
        private void Parse(PreparedBy preparedBy)
        {
            //preparedBy.Id;
            //preparedBy.NonPersonEntityIndicator;
            //preparedBy.City;
            //preparedBy.Country;
            //preparedBy.CountryCode;
            //preparedBy.County;
            //preparedBy.CountyFipsCode;
            //preparedBy.ElectronicRoutingAddress;
            //preparedBy.ElectronicRoutingMethodType;
            //preparedBy.PostalCode;
            //preparedBy.State;
            //preparedBy.StateFipsCode;
            //preparedBy.StreetAddress;
            //preparedBy.StreetAddress2;
            //preparedBy.TelephoneNumber;
            //preparedBy.TitleDescription;
            //preparedBy.UnparsedName;
            Parse(preparedBy.ContactDetail);
        }
        private void Parse(List<Notary> notaryList)
        {
            foreach (var notary in notaryList)
            {
                //notary.Id;
                //notary.AppearanceDate;
                //notary.AppearedBeforeNamesDescription;
                //notary.AppearedBeforeTitlesDescription;
                //notary.City;
                //notary.CommissionBondNumberIdentifier;
                //notary.CommissionCity;
                //notary.CommissionCounty;
                //notary.CommissionExpirationDate;
                //notary.CommissionNumberIdentifier;
                //notary.CommissionState;
                //notary.County;
                //notary.PostalCode;
                //notary.State;
                //notary.StreetAddress;
                //notary.StreetAddress2;
                //notary.TitleDescription;
                //notary.UnparsedName;
                Parse(notary.CertificateList);
                Parse(notary.DataVersionList);
            }
        }
        private void Parse(List<DataVersion> dataVersionList)
        {
            foreach (var dataVersion in dataVersionList)
            {
                //dataVersion.Id;
                //dataVersion.Name;
                //dataVersion.Number;
            }
        }
        private void Parse(List<Certificate> certificateList)
        {
            foreach (var certificate in certificateList)
            {
                //certificate.Id;
                //certificate.SignerCompanyName;
                //certificate.SignerFirstName;
                //certificate.SignerLastName;
                //certificate.SignerMiddleName;
                //certificate.SignerNameSuffix;
                //certificate.SignerTitleDescription;
                //certificate.SignerUnparsedName;
                //certificate.SigningCounty;
                //certificate.SigningDate;
                //certificate.SigningState;
                Parse(certificate.SignerIdentification);
            }
        }
        private void Parse(SignerIdentification signerIdentification)
        {
            //signerIdentification.Id;
            //signerIdentification.Description;
            //signerIdentification.Type;
        }
        private void Parse(Default _default)
        {
            //_default.Id;
            //_default.AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator;
            //_default.ApplicationFeesAmount;
            //_default.ClosingPreparationFeesAmount;
            //_default.LendingInstitutionPostOfficeBoxAddress;
        }
        private void Parse(AssignTo assignTo)
        {
            //assignTo.Id;
            //assignTo.NonPersonEntityIndicator;
            //assignTo.City;
            //assignTo.Country;
            //assignTo.County;
            //assignTo.CountyFipsCode;
            //assignTo.PostalCode;
            //assignTo.State;
            //assignTo.StreetAddress;
            //assignTo.StreetAddress2;
            //assignTo.UnparsedName;
            Parse(assignTo.NonPersonEntityDetail);
        }
        private void Parse(AssignFrom assignFrom)
        {
            //assignFrom.Id;
            //assignFrom.NonPersonEntityIndicator;
            //assignFrom.City;
            //assignFrom.Country;
            //assignFrom.County;
            //assignFrom.CountyFipsCode;
            //assignFrom.PostalCode;
            //assignFrom.SigningOfficialName;
            //assignFrom.SigningOfficialTitleDescription;
            //assignFrom.State;
            //assignFrom.StreetAddress;
            //assignFrom.StreetAddress2;
            //assignFrom.UnparsedName;
            Parse(assignFrom.NonPersonEntityDetail);
        }
        private void Parse(List<Payoff> payoffList)
        {
            foreach (var payoff in payoffList)
            {
                //payoff.Id;
                //payoff.AccountNumberIdentifier;
                //payoff.Amount;
                //payoff.PerDiemAmount;
                //payoff.SequenceIdentifier;
                //payoff.SpecifiedHudLineNumber;
                //payoff.ThroughDate;
                Parse(payoff.Payee);
            }
        }
        private void Parse(Payee payee)
        {
            //payee.Id;
            //payee.NonPersonEntityIndicator;
            //payee.City;
            //payee.Country;
            //payee.County;
            //payee.PostalCode;
            //payee.State;
            //payee.StreetAddress;
            //payee.StreetAddress2;
            //payee.UnparsedName;
            Parse(payee.ContactDetail);
            Parse(payee.NonPersonEntityDetail);
        }
        private void Parse(PaymentDetails paymentDetails)
        {
            Parse(paymentDetails.AmortizationScheduleList);
            Parse(paymentDetails.PaymentScheduleList);
        }
        private void Parse(List<PaymentSchedule> paymentScheduleList)
        {
            foreach (var paymentSchedule in paymentScheduleList)
            {
                //paymentSchedule.Id;
                //paymentSchedule.PaymentAmount;
                //paymentSchedule.PaymentSequenceIdentifier;
                //paymentSchedule.PaymentStartDate;
                //paymentSchedule.PaymentVaryingToAmount;
                //paymentSchedule.TotalNumberOfPaymentsCount;
            }
        }
        private void Parse(List<AmortizationSchedule> amortizationScheduleList)
        {
            foreach (var amortizationSchedule in amortizationScheduleList)
            {
                //amortizationSchedule.Id;
                //amortizationSchedule.EndingBalanceAmount;
                //amortizationSchedule.InterestRatePercent;
                //amortizationSchedule.LoanToValuePercent;
                //amortizationSchedule.MiPaymentAmount;
                //amortizationSchedule.PaymentAmount;
                //amortizationSchedule.PaymentDueDate;
                //amortizationSchedule.PaymentNumber;
                //amortizationSchedule.PortionOfPaymentDistributedToInterestAmount;
                //amortizationSchedule.PortionOfPaymentDistributedToPrincipalAmount;
            }
        }
        private void Parse(MortgageBroker mortgageBroker)
        {
            //mortgageBroker.Id;
            //mortgageBroker.NonPersonEntityIndicator;
            //mortgageBroker.City;
            //mortgageBroker.Country;
            //mortgageBroker.County;
            //mortgageBroker.LicenseNumberIdentifier;
            //mortgageBroker.PostalCode;
            //mortgageBroker.State;
            //mortgageBroker.StreetAddress;
            //mortgageBroker.StreetAddress2;
            //mortgageBroker.UnparsedName;
            Parse(mortgageBroker.ContactDetail);
            Parse(mortgageBroker.NonPersonEntityDetail);
            Parse(mortgageBroker.RegulatoryAgencyList);
        }
        private void Parse(List<RegulatoryAgency> regulatoryAgencyList)
        {
            foreach (var regulatoryAgency in regulatoryAgencyList)
            {
                //regulatoryAgency.Id;
                //regulatoryAgency.City;
                //regulatoryAgency.Country;
                //regulatoryAgency.County;
                //regulatoryAgency.PostalCode;
                //regulatoryAgency.State;
                //regulatoryAgency.StreetAddress;
                //regulatoryAgency.StreetAddress2;
                //regulatoryAgency.Type;
                //regulatoryAgency.TypeOtherDescription;
                //regulatoryAgency.UnparsedName;
                Parse(regulatoryAgency.ContactDetail);
            }
        }
        private void Parse(List<LossPayee> lossPayeeList)
        {
            foreach (var lossPayee in lossPayeeList)
            {
                //lossPayee.Id;
                //lossPayee.NonPersonEntityIndicator;
                //lossPayee.City;
                //lossPayee.Country;
                //lossPayee.County;
                //lossPayee.PostalCode;
                //lossPayee.State;
                //lossPayee.StreetAddress;
                //lossPayee.StreetAddress2;
                //lossPayee.Type;
                //lossPayee.TypeOtherDescription;
                //lossPayee.UnparsedName;
                Parse(lossPayee.ContactDetail);
                Parse(lossPayee.NonPersonEntityDetail);
            }
        }
        private void Parse(LoanDetails loanDetails)
        {
            //loanDetails.Id;
            //loanDetails.ClosingDate;
            //loanDetails.DisbursementDate;
            //loanDetails.DocumentOrderClassificationType;
            //loanDetails.DocumentPreparationDate;
            //loanDetails.FundByDate;
            //loanDetails.LockExpirationDate;
            //loanDetails.OriginalLtvRatioPercent;
            //loanDetails.RescissionDate;
            Parse(loanDetails.InterimInterest);
            Parse(loanDetails.RequestToRescind);
        }
        private void Parse(RequestToRescind requestToRescind)
        {
            //requestToRescind.Id;
            //requestToRescind.NotificationCity;
            //requestToRescind.NotificationCountry;
            //requestToRescind.NotificationPostalCode;
            //requestToRescind.NotificationState;
            //requestToRescind.NotificationStreetAddress;
            //requestToRescind.NotificationStreetAddress2;
            //requestToRescind.NotificationUnparsedName;
            //requestToRescind.TransactionDate;
        }
        private void Parse(InterimInterest interimInterest)
        {
            //interimInterest.Id;
            //interimInterest.PaidFromDate;
            //interimInterest.PaidNumberOfDays;
            //interimInterest.PaidThroughDate;
            //interimInterest.PerDiemCalculationMethodType;
            //interimInterest.PerDiemPaymentOptionType;
            //interimInterest.PerDiemPaymentOptionTypeOtherDescription;
            //interimInterest.SinglePerDiemAmount;
            //interimInterest.TotalPerDiemAmount;
        }
        private void Parse(LenderBranch lenderBranch)
        {
            //lenderBranch.Id;
            //lenderBranch.City;
            //lenderBranch.Country;
            //lenderBranch.County;
            //lenderBranch.PostalCode;
            //lenderBranch.State;
            //lenderBranch.StreetAddress;
            //lenderBranch.StreetAddress2;
            //lenderBranch.UnparsedName;
            Parse(lenderBranch.ContactDetail);
        }
        private void Parse(Lender lender)
        {
            //lender.Id;
            //lender.NonPersonEntityIndicator;
            //lender.City;
            //lender.Country;
            //lender.County;
            //lender.DocumentsOrderedByName;
            //lender.FunderName;
            //lender.PostalCode;
            //lender.State;
            //lender.StreetAddress;
            //lender.StreetAddress2;
            //lender.UnparsedName;
            Parse(lender.ContactDetail);
            Parse(lender.NonPersonEntityDetail);
            Parse(lender.RegulatoryAgencyList);
        }
        private void Parse(Investor investor)
        {
            //investor.Id;
            //investor.NonPersonEntityIndicator;
            //investor.City;
            //investor.Country;
            //investor.County;
            //investor.PostalCode;
            //investor.State;
            //investor.StreetAddress;
            //investor.StreetAddress2;
            //investor.UnparsedName;
            Parse(investor.ContactDetail);
            Parse(investor.NonPersonEntityDetail);
            Parse(investor.RegulatoryAgencyList);
        }
        private void Parse(Execution execution)
        {
            //execution.Id;
            //execution.City;
            //execution.County;
            //execution.Date;
            //execution.State;
        }
        private void Parse(List<EscrowAccountDetail> escrowAccountDetailList)
        {
            foreach (var escrowAccountDetail in escrowAccountDetailList)
            {
                //escrowAccountDetail.Id;
                //escrowAccountDetail.TotalMonthlyPitiAmount;
                //escrowAccountDetail.InitialBalanceAmount;
                //escrowAccountDetail.MinimumBalanceAmount;
                Parse(escrowAccountDetail.EscrowAccountActivityList);
            }
        }
        private void Parse(List<EscrowAccountActivity> escrowAccountActivityList)
        {
            foreach (var escrowAccountActivity in escrowAccountActivityList)
            {
                //escrowAccountActivity.Id;
                //escrowAccountActivity.CurrentBalanceAmount;
                //escrowAccountActivity.DisbursementMonth;
                //escrowAccountActivity.DisbursementSequenceIdentifier;
                //escrowAccountActivity.DisbursementYear;
                //escrowAccountActivity.PaymentDescriptionType;
                //escrowAccountActivity.PaymentDescriptionTypeOtherDescription;
                //escrowAccountActivity.PaymentFromEscrowAccountAmount;
                //escrowAccountActivity.PaymentToEscrowAccountAmount;
            }
        }
        private void Parse(List<Cosigner> cosignerList)
        {
            foreach (var cosigner in cosignerList)
            {
                //cosigner.Id;
                //cosigner.TitleDescription;
                //cosigner.UnparsedName;
            }
        }
        private void Parse(List<Compensation> compensationList)
        {
            foreach (var compensation in compensationList)
            {
                //compensation.Id;
                //compensation.Amount;
                //compensation.PaidByType;
                //compensation.PaidToType;
                //compensation.Percent;
                //compensation.Type;
                //compensation.TypeOtherDescription;
            }
        }
        private void Parse(List<ClosingInstructions> closingInstructionsList)
        {
            foreach (var closingInstructions in closingInstructionsList)
            {
                //closingInstructions.Id;
                //closingInstructions.FundingCutoffTime;
                //closingInstructions.HoursDocumentsNeededPriorToDisbursementCount;
                //closingInstructions.LeadBasedPaintCertificationRequiredIndicator;
                //closingInstructions.PreliminaryTitleReportDate;
                //closingInstructions.SpecialFloodHazardAreaIndicator;
                //closingInstructions.SpecialFloodHazardAreaIndictor;
                //closingInstructions.TitleReportItemsDescription;
                //closingInstructions.TitleReportRequiredEndorsementsDescription;
                //closingInstructions.ConsolidatedClosingConditionsDescription;
                //closingInstructions.PropertyTaxMessageDescription;
                //closingInstructions.TermiteReportRequiredIndicator;
                Parse(closingInstructions.ConditionList);
            }
        }
        private void Parse(List<Condition> conditionList)
        {
            foreach (var condition in conditionList)
            {
                //condition.Id;
                //condition.Description;
                //condition.MetIndicator;
                //condition.SatisfactionTimeframeType;
                //condition.SatisfactionTimeframeTypeOtherDescription;
                //condition.SatisfactionApprovedByName;
                //condition.SatisfactionDate;
                //condition.SatisfactionResponsiblePartyType;
                //condition.SatisfactionResponsiblePartyTypeOtherDescription;
                //condition.SequenceIdentifier;
                //condition.WaivedIndicator;
            }
        }
        private void Parse(List<ClosingAgent> closingAgentList)
        {
            foreach (var closingAgent in closingAgentList)
            {
                //closingAgent.Id;
                //closingAgent.AuthorizedToSignIndicator;
                //closingAgent.NonPersonEntityIndicator;
                //closingAgent.City;
                //closingAgent.Country;
                //closingAgent.County;
                //closingAgent.Identifier;
                //closingAgent.OrderNumberIdentifier;
                //closingAgent.PostalCode;
                //closingAgent.State;
                //closingAgent.StreetAddress;
                //closingAgent.StreetAddress2;
                //closingAgent.TitleDescription;
                //closingAgent.Type;
                //closingAgent.TypeOtherDescription;
                //closingAgent.UnparsedName;
                Parse(closingAgent.ContactDetail);
                Parse(closingAgent.NonPersonEntityDetail);
            }
        }
        private void Parse(Beneficiary beneficiary)
        {
            //beneficiary.Id;
            //beneficiary.NonPersonEntityIndicator;
            //beneficiary.City;
            //beneficiary.Country;
            //beneficiary.County;
            //beneficiary.PostalCode;
            //beneficiary.State;
            //beneficiary.StreetAddress;
            //beneficiary.StreetAddress2;
            //beneficiary.UnparsedName;
            Parse(beneficiary.ContactDetail);
            Parse(beneficiary.NonPersonEntityDetail);
        }
        private void Parse(AllongeToNote allongeToNote)
        {
            //allongeToNote.Id;
            //allongeToNote.Date;
            //allongeToNote.ExecutedByDescription;
            //allongeToNote.InFavorOfDescription;
            //allongeToNote.PayToTheOrderOfDescription;
            //allongeToNote.Type;
            //allongeToNote.TypeOtherDescription;
            //allongeToNote.WithoutRecourseDescription;
            Parse(allongeToNote.AuthorizedRepresentative);
        }
        private void Parse(AuthorizedRepresentative authorizedRepresentative)
        {
            //authorizedRepresentative.Id;
            //authorizedRepresentative.AuthorizedToSignIndicator;
            //authorizedRepresentative.TitleDescription;
            //authorizedRepresentative.UnparsedName;
            Parse(authorizedRepresentative.ContactDetail);
        }
        private void Parse(Application application)
        {
            //application.Id;
            Parse(application.LoanPurpose); // Need to be parse first in order for sLPurposeT set correctly.

            Parse(application.BorrowerList); // 7/8/2009 dd - Must process BorrowerList first before asset, liabilities and reos

            Parse(application.DownPaymentList);

            Parse(application.Property);
            Parse(application.LoanQualification);

            Parse(application.MortgageTerms);
            Parse(application.LoanProductData);
            Parse(application.TransactionDetail);
            Parse(application.ProposedHousingExpenseList);

            // Clear out existing collection: asset, liabilities, REOs
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                dataApp.aAssetCollection.ClearAll();
                dataApp.aReCollection.ClearAll();
                dataApp.aLiaCollection.ClearAll();
            }

            Parse(application.LiabilityList);
            Parse(application.AssetList, application.DownPaymentList);
            Parse(application.ReoPropertyList);


            Parse(application.DataInformation);
            Parse(application.AdditionalCaseData);
            Parse(application.AffordableLending);
            Parse(application.EscrowList);
            Parse(application.EscrowAccountSummary);

            if ((m_dataLoan.sLT == E_sLT.FHA) || (m_dataLoan.sLT == E_sLT.VA))
                Parse(application.GovernmentLoan);
            
            Parse(application.GovernmentReporting);
            Parse(application.InterviewerInformation);
            Parse(application.Mers);
            Parse(application.MiData);
            Parse(application.RespaFeeList);
            Parse(application.TitleHolderList);
            Parse(application.InvestorFeatureList);
            Parse(application.LoanOriginationSystem);
            Parse(application.LoanUnderwritingList);
            Parse(application.RelatedLoanList);
            Parse(application.UrlaTotalList);

            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                m_dataLoan.GetAppData(i).Flush(); // Save all liabilities, assets and reos to db.
            }
        }
        private void Parse(List<UrlaTotal> urlaTotalList)
        {
            if (urlaTotalList == null)
            {
                return;
            }
            foreach (var urlaTotal in urlaTotalList)
            {
                //urlaTotal.Id;
                //urlaTotal.BorrowerId;
                //urlaTotal.UrlaSubtotalLiquidAssetsAmount;
                //urlaTotal.AssetsAmount;
                //urlaTotal.BaseIncomeAmount;
                //urlaTotal.BonusIncomeAmount;
                //urlaTotal.CashFromToBorrowerAmount;
                //urlaTotal.CombinedPresentHousingExpenseAmount;
                //urlaTotal.CombinedProposedHousingExpenseAmount;
                //urlaTotal.CommissionsIncomeAmount;
                //urlaTotal.DividendsInterestIncomeAmount;
                //urlaTotal.LiabilityMonthlyPaymentsAmount;
                //urlaTotal.LiabilityUnpaidBalanceAmount;
                //urlaTotal.LotAndImprovementsAmount;
                //urlaTotal.MonthlyIncomeAmount;
                //urlaTotal.NetRentalIncomeAmount;
                //urlaTotal.NetWorthAmount;
                //urlaTotal.OtherTypesOfIncomeAmount;
                //urlaTotal.OvertimeIncomeAmount;
                //urlaTotal.ReoLienInstallmentAmount;
                //urlaTotal.ReoLienUpbAmount;
                //urlaTotal.ReoMaintenanceExpenseAmount;
                //urlaTotal.ReoMarketValueAmount;
                //urlaTotal.ReoRentalIncomeGrossAmount;
                //urlaTotal.ReoRentalIncomeNetAmount;
                //urlaTotal.TransactionCostAmount;
                Parse(urlaTotal.HousingExpenseList);
            }
        }
        private void Parse(List<HousingExpense> housingExpenseList)
        {
            foreach (var housingExpense in housingExpenseList)
            {
                //housingExpense.Id;
                //housingExpense.HousingExpenseType;
                //housingExpense.HousingExpenseTypeOtherDescription;
                //housingExpense.PaymentAmount;
            }
        }
        private void Parse(List<RelatedLoan> relatedLoanList)
        {
            if (relatedLoanList == null)
            {
                return;
            }
            foreach (var relatedLoan in relatedLoanList)
            {
                //relatedLoan.RelatedLoanId;
                //relatedLoan.BalloonIndicator;
                //relatedLoan.HelocMaximumBalanceAmount;
                //relatedLoan.InvestorReoPropertyIdentifier;
                //relatedLoan.LienHolderCity;
                //relatedLoan.LienHolderCountry;
                //relatedLoan.LienHolderPostalCode;
                //relatedLoan.LienHolderSameAsSubjectLoanIndicator;
                //relatedLoan.LienHolderState;
                //relatedLoan.LienHolderStreetAddress;
                //relatedLoan.LienHolderStreetAddress2;
                //relatedLoan.LienHolderType;
                //relatedLoan.LienHolderTypeOtherDescription;
                //relatedLoan.LienHolderUnparsedName;
                //relatedLoan.LienPriorityType;
                //relatedLoan.LienPriorityTypeOtherDescription;
                //relatedLoan.LoanAllInPricePercent;
                //relatedLoan.LoanAmortizationType;
                //relatedLoan.LoanOriginalMaturityTermMonths;
                //relatedLoan.MortgageType;
                //relatedLoan.NegativeAmortizationType;
                //relatedLoan.NoteDate;
                //relatedLoan.OriginalLoanAmount;
                //relatedLoan.OtherAmortizationTypeDescription;
                //relatedLoan.OtherMortgageTypeDescription;
                //relatedLoan.RelatedInvestorLoanIdentifier;
                //relatedLoan.RelatedLoanFinancingSourceType;
                //relatedLoan.RelatedLoanFinancingSourceTypeOtherDescription;
                //relatedLoan.RelatedLoanInvestorType;
                //relatedLoan.RelatedLoanInvestorTypeOtherDescription;
                //relatedLoan.RelatedLoanRelationshipType;
                //relatedLoan.RelatedLoanRelationshipTypeOtherDescription;
                //relatedLoan.RelatedLoanUpbAmount;
                //relatedLoan.ScheduledFirstPaymentDate;
            }
        }
        private void Parse(List<LoanUnderwriting> loanUnderwritingList)
        {
            if (loanUnderwritingList == null)
            {
                return;
            }
            foreach (var loanUnderwriting in loanUnderwritingList)
            {
                //loanUnderwriting.LoanUnderwritingId;
                //loanUnderwriting.AgencyProgramDescription;
                //loanUnderwriting.AutomatedUnderwritingEvaluationStatusDescription;
                //loanUnderwriting.AutomatedUnderwritingProcessDescription;
                //loanUnderwriting.AutomatedUnderwritingRecommendationDescription;
                //loanUnderwriting.AutomatedUnderwritingSystemName;
                //loanUnderwriting.AutomatedUnderwritingSystemResultValue;
                //loanUnderwriting.ContractUnderwritingIndicator;
                //loanUnderwriting.HousingExpenseRatioPercent;
                //loanUnderwriting.LoanManualUnderwritingIndicator;
                //loanUnderwriting.LoanProspectorCreditRiskClassificationDescription;
                //loanUnderwriting.LoanProspectorDocumentationClassificationDescription;
                //loanUnderwriting.LoanProspectorRiskGradeAssignedDescription;
                //loanUnderwriting.TotalDebtExpenseRatioPercent;
                //loanUnderwriting.CaseIdentifier;
                //loanUnderwriting.DecisionDatetime;
                //loanUnderwriting.InvestorGuidelinesIndicator;
                //loanUnderwriting.MethodVersionIdentifier;
                //loanUnderwriting.OrganizationName;
                //loanUnderwriting.SubmitterType;
                //loanUnderwriting.SubmitterTypeOtherDescription;
            }
        }
        private void Parse(LoanOriginationSystem loanOriginationSystem)
        {
            //loanOriginationSystem.Id;
            //loanOriginationSystem.LoanIdentifier;
            //loanOriginationSystem.Name;
            //loanOriginationSystem.VendorIdentifier;
            //loanOriginationSystem.VersionIdentifier;
        }
        private void Parse(List<InvestorFeature> investorFeatureList)
        {
            if (investorFeatureList == null)
            {
                return;
            }
            foreach (var investorFeature in investorFeatureList)
            {
                //investorFeature.Id;
                //investorFeature.CategoryName;
                //investorFeature.Description;
                //investorFeature.Identifier;
                //investorFeature.Name;
            }
        }
        private void PopulateBorrowerIdAndSsnDictionary(List<Borrower> borrowerList)
        {
            foreach (var borrower in borrowerList)
            {
                if(string.IsNullOrEmpty(borrower.Ssn))
                {
                    throw new BlankSSNException();
                }
                m_borrowerId_Ssn_Hash.Add(borrower.BorrowerId, borrower.Ssn);
            }
        }

        private bool CanMergeApps(List<Borrower> borrowerList)
        {
            Dictionary<string, Borrower> borrowersById = new Dictionary<string,Borrower>(StringComparer.OrdinalIgnoreCase);
            Dictionary<string, Borrower> coborrowersById = new Dictionary<string, Borrower>(StringComparer.OrdinalIgnoreCase);
            HashSet<string> ssns = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            StringBuilder sb = new StringBuilder();

            foreach (var borrower in borrowerList)
            {
                sb.AppendLine(borrower.BorrowerId + " " + borrower.JointAssetBorrowerId + " " + borrower.Ssn);

                if (borrower.PrintPositionType == E_BorrowerPrintPositionType.Borrower)
                {
                    borrowersById.Add(borrower.BorrowerId, borrower);
                }
                else
                {
                    if (string.IsNullOrEmpty(borrower.JointAssetBorrowerId))
                    {
                        Tools.LogError("CanMergeApps JointAssetBorrowerId is missing. " + sb.ToString());
                        return false;
                    }
                    coborrowersById.Add(borrower.BorrowerId, borrower);
                }

                if ( ssns.Contains(borrower.Ssn))
                {
                    Tools.LogError("CanMergeApps Duplicate borrows found. " + sb.ToString());
                    return false;
                }
                ssns.Add(borrower.Ssn);
            }

            Dictionary<string, string> borrowerAndCoborrowerSsns = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

            #region populate borrowerAndCoborrowerSsns
            foreach (var entry in borrowersById)
            {
                borrowerAndCoborrowerSsns.Add(entry.Value.Ssn, "");
            }

            foreach (var coborrower in coborrowersById)
            {
                var borrower = borrowersById[coborrower.Value.JointAssetBorrowerId];
                borrowerAndCoborrowerSsns[borrower.Ssn] = coborrower.Value.Ssn;
            }
            #endregion

            int nApps = m_dataLoan.nApps;
            sb.AppendLine("APP COUNT :" + nApps);

            if (nApps != borrowerAndCoborrowerSsns.Count)
            {
                Tools.LogError("CanMergeApps App count doesnt match ssn count" + sb.ToString());
                return false;
            }
            sb.AppendLine("END BORROWER CHECKS");

            for (int i = 0; i < nApps; i++)
            {
                CAppData data = m_dataLoan.GetAppData(i);
                sb.AppendLine(data.aBSsn + " " + data.aCSsn);

                if (false == borrowerAndCoborrowerSsns.ContainsKey(data.aBSsn))
                {
                    Tools.LogError("CanMergeApps SSN Did Not Exist " + sb.ToString());
                    return false;
                }

                if (borrowerAndCoborrowerSsns[data.aBSsn] != data.aCSsn)
                {
                    Tools.LogError("Coborrower did not match " + sb.ToString());
                    return false;
                }
            }

            return true;
        }

        private void ClearIncomeInfoForAllApps()
        {
            int nApps = m_dataLoan.nApps;

            if (!m_dataLoan.sIsIncomeCollectionEnabled)
            {
                for (int i = 0; i < nApps; i++)
                {
                    CAppData dataApp = m_dataLoan.GetAppData(i);

                    dataApp.aOtherIncomeList = new List<OtherIncome>();

                    dataApp.aBBaseI_rep = "";
                    dataApp.aCBaseI_rep = "";
                    dataApp.aBOvertimeI_rep = "";
                    dataApp.aCOvertimeI_rep = "";
                    dataApp.aBBonusesI_rep = "";
                    dataApp.aCBonusesI_rep = "";
                    dataApp.aBCommisionI_rep = "";
                    dataApp.aCCommisionI_rep = "";
                    dataApp.aBDividendI_rep = "";
                    dataApp.aCDividendI_rep = "";
                }
            }
            else
            {
                m_dataLoan.ClearIncomeSources();
            }
        }

        private void DeleteAllBorrowerInformation()
        {
            int nApps = m_dataLoan.nApps;
            for (int i = nApps - 1; i > 0; i--)
            {
                m_dataLoan.DelApp(i);
                m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            }
            CAppData dataApp = m_dataLoan.GetAppData(0);

            this.ClearIncomeInfoForAllApps();

            dataApp.DelMarriedCobor(); // 12/2/2010 dd - Remove spouse info for primary app.
            dataApp.aBSsn = ""; // 12/2/2010 dd - Since we cannot remove primary borrower, we just clear out ssn.

            // 11/05/2012 BB - any credit data on file from a prior execution will be replaced with credit data from Encompass
            if (dataApp.aIsCreditReportOnFile) 
                LendersOffice.CreditReport.CreditReportServer.DeleteCreditReport(m_dataLoan.sBrokerId, dataApp.aAppId); 
        }
        private void Parse(List<Borrower> borrowerList)
        {
            if (borrowerList == null)
            {
                return;
            }
            PopulateBorrowerIdAndSsnDictionary(borrowerList);

            bool isFirstApp = true;
            foreach (var borrower in borrowerList)
            {
                CAppData dataApp = m_dataLoan.FindDataAppBySsn(borrower.Ssn);
                if (null == dataApp)
                {
                    if (!string.IsNullOrEmpty(borrower.JointAssetBorrowerId))
                    {
                        string spouseSsn = "";
                        if (m_borrowerId_Ssn_Hash.TryGetValue(borrower.JointAssetBorrowerId, out spouseSsn))
                        {
                            dataApp = m_dataLoan.FindDataAppBySsn(spouseSsn);
                        }
                    }

                    // the borrower was not found... 
                    if (null == dataApp)
                    {
                        if (isFirstApp)
                        {
                            // 7/8/2009 dd - Could not locate applicant, since this is the first borrower info use primary app.
                            dataApp = m_dataLoan.GetAppData(0);
                        }
                        else
                        {
                            m_dataLoan.Save();
                            int iApp = m_dataLoan.AddNewApp();
                            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                            dataApp = m_dataLoan.GetAppData(iApp);
                        }
                    }
                }
                isFirstApp = false;

                Parse(borrower, dataApp);
            }

            if (this.m_dataLoan.sIsIncomeCollectionEnabled)
            {
                this.m_dataLoan.ImportGrossRent(this.hasSubjectPropertyNetCashFlow, this.totalSubjectPropertyNetCashFlow);
            }
        }

        private void Parse(Borrower borrower, CAppData dataApp)
        {
            switch (borrower.PrintPositionType)
            {
                case E_BorrowerPrintPositionType.Borrower:
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    break;
                case E_BorrowerPrintPositionType.CoBorrower:
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    break;
                default:
                    throw new UnhandledEnumException(borrower.PrintPositionType);
            }
            dataApp.aDob_rep = borrower.BirthDate;
            dataApp.aBDependNum_rep = borrower.DependentCount;
            dataApp.aSchoolYrs_rep = borrower.SchoolingYears;
            dataApp.aFirstNm = borrower.FirstName;
            dataApp.aHPhone = borrower.HomeTelephoneNumber;
            dataApp.aLastNm = borrower.LastName;
            dataApp.aMidNm = borrower.MiddleName;
            dataApp.aSuffix = borrower.NameSuffix;
            dataApp.aSsn = borrower.Ssn;
            dataApp.aMaritalStatT = Parse(borrower.MaritalStatusType);

            Parse(borrower.CurrentIncomeList, dataApp);

            //borrower.BorrowerId;
            //borrower.JointAssetBorrowerId;
            //borrower.BorrowerIsCosignerIndicator;
            //borrower.BorrowerNonObligatedIndicator;
            //borrower.BorrowerNonTitleSpouseIndicator;
            //borrower.CreditReportIdentifier;
            //borrower.JointAssetLiabilityReportingType;
            //borrower.NonPersonEntityIndicator;
            //borrower.UrlaBorrowerTotalMonthlyIncomeAmount;
            //borrower.UrlaBorrowerTotalOtherIncomeAmount;
            //borrower.AgeAtApplicationYears;
            //borrower.ApplicationSignedDate;

            //borrower.RelationshipTitleType;
            //borrower.RelationshipTitleTypeOtherDescription;
            //borrower.SequenceIdentifier;
            //borrower.UnparsedName;
            Parse(borrower.AliasList);
            Parse(borrower.MailTo, dataApp);
            Parse(borrower.ResidenceList, dataApp);
            Parse(borrower.Declaration, dataApp);
            Parse(borrower.DependentList, dataApp);
            Parse(borrower.EmployerList, dataApp);
            Parse(borrower.FhaVaBorrower, dataApp);
            Parse(borrower.GovernmentMonitoring, dataApp);
            Parse(borrower.PresentHousingExpenseList, dataApp);
            Parse(borrower.VaBorrower, dataApp);
            Parse(borrower.FhaBorrower, dataApp);
            Parse(borrower.SummaryList);
            Parse(borrower.NearestLivingRelative);
            Parse(borrower.NonPersonEntityDetail);
            Parse(borrower.PowerOfAttorney);
            Parse(borrower.ContactPointList);
            Parse(borrower.CreditScoreList);
        }

        private E_aBMaritalStatT Parse(E_BorrowerMaritalStatusType borrowerMaritalStatusType)
        {
            switch (borrowerMaritalStatusType)
            {
                case E_BorrowerMaritalStatusType.Married:
                    return E_aBMaritalStatT.Married;
                case E_BorrowerMaritalStatusType.Separated:
                    return E_aBMaritalStatT.Separated;
                case E_BorrowerMaritalStatusType.Unmarried:
                    return E_aBMaritalStatT.NotMarried;
                case E_BorrowerMaritalStatusType.NotProvided:
                case E_BorrowerMaritalStatusType.Undefined:
                case E_BorrowerMaritalStatusType.Unknown:
                    return E_aBMaritalStatT.LeaveBlank;
                default:
                    throw new UnhandledEnumException(borrowerMaritalStatusType);
            }
        }
        private void Parse(List<CreditScore> creditScoreList)
        {
            foreach (var creditScore in creditScoreList)
            {
                //creditScore.CreditScoreId;
                //creditScore.CreditReportIdentifier;
                //creditScore.CreditRepositorySourceType;
                //creditScore.CreditRepositorySourceTypeOtherDescription;
                //creditScore.Date;
                //creditScore.ExclusionReasonType;
                //creditScore.FactaInquiriesIndicator;
                //creditScore.ModelNameType;
                //creditScore.ModelNameTypeOtherDescription;
                //creditScore.Value;
                Parse(creditScore.FactorList);
            }
        }
        private void Parse(List<Factor> factorList)
        {
            foreach (var factor in factorList)
            {
                //factor.Id;
                //factor.Code;
                //factor.Text;
            }
        }
        private void Parse(NearestLivingRelative nearestLivingRelative)
        {
            //nearestLivingRelative.Id;
            //nearestLivingRelative.City;
            //nearestLivingRelative.Name;
            //nearestLivingRelative.PostalCode;
            //nearestLivingRelative.RelationshipDescription;
            //nearestLivingRelative.State;
            //nearestLivingRelative.StreetAddress;
            //nearestLivingRelative.StreetAddress2;
            //nearestLivingRelative.TelephoneNumber;
        }
        private void Parse(FhaBorrower fhaBorrower, CAppData dataApp)
        {
            //fhaBorrower.Id;
            //fhaBorrower.CertificationLeadPaintIndicator;
            //fhaBorrower.CertificationOriginalMortgageAmount;
            //fhaBorrower.CertificationOwn4OrMoreDwellingsIndicator;
            //fhaBorrower.CertificationOwnOtherPropertyIndicator;
            //fhaBorrower.CertificationPropertySoldCity;
            //fhaBorrower.CertificationPropertySoldPostalCode;
            //fhaBorrower.CertificationPropertySoldState;
            //fhaBorrower.CertificationPropertySoldStreetAddress;
            //fhaBorrower.CertificationPropertyToBeSoldIndicator;
            //fhaBorrower.CertificationRentalIndicator;
            //fhaBorrower.CertificationSalesPriceAmount;
        }
        private void Parse(VaBorrower vaBorrower, CAppData dataApp)
        {
            //vaBorrower.Id;
            //vaBorrower.VaCoBorrowerNonTaxableIncomeAmount;
            //vaBorrower.VaCoBorrowerTaxableIncomeAmount;
            //vaBorrower.VaFederalTaxAmount;
            //vaBorrower.VaLocalTaxAmount;
            //vaBorrower.VaPrimaryBorrowerNonTaxableIncomeAmount;
            //vaBorrower.VaPrimaryBorrowerTaxableIncomeAmount;
            //vaBorrower.VaSocialSecurityTaxAmount;
            //vaBorrower.VaStateTaxAmount;
            //vaBorrower.CertificationOccupancyType;
        }
        private void Parse(List<Summary> summaryList)
        {
            foreach (var summary in summaryList)
            {
                //summary.Id;
                //summary.Amount;
                //summary.AmountType;
                //summary.AmountTypeOtherDescription;
            }
        }
        private void Parse(List<PresentHousingExpense> presentHousingExpenseList, CAppData dataApp)
        {
            decimal dOtherHousingExpense = 0m;

            foreach (var presentHousingExpense in presentHousingExpenseList)
            {
                //presentHousingExpense.Id;
                switch(presentHousingExpense.HousingExpenseType)
                {
                    case E_PresentHousingExpenseHousingExpenseType.FirstMortgagePITI:
                    case E_PresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest:
                        dataApp.aPres1stM_rep = presentHousingExpense.PaymentAmount;
                        break;
                    case E_PresentHousingExpenseHousingExpenseType.GroundRent:
                        dataApp.aPresRent_rep = presentHousingExpense.PaymentAmount;
                        break;
                    case E_PresentHousingExpenseHousingExpenseType.HazardInsurance:
                        dataApp.aPresHazIns_rep = presentHousingExpense.PaymentAmount;
                        break;
                    case E_PresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees:
                        dataApp.aPresHoAssocDues_rep = presentHousingExpense.PaymentAmount;
                        break;
                    case E_PresentHousingExpenseHousingExpenseType.MI:
                        dataApp.aPresMIns_rep = presentHousingExpense.PaymentAmount;
                        break;
                    case E_PresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest:
                    case E_PresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalInterestTaxesAndInsurance:
                        dataApp.aPresOFin_rep = presentHousingExpense.PaymentAmount;
                        break;
                    case E_PresentHousingExpenseHousingExpenseType.RealEstateTax:
                        dataApp.aPresRealETx_rep = presentHousingExpense.PaymentAmount;
                        break;
                    case E_PresentHousingExpenseHousingExpenseType.Rent:
                        dataApp.aPresRent_rep = presentHousingExpense.PaymentAmount;
                        break;

                    case E_PresentHousingExpenseHousingExpenseType.LeaseholdPayments:
                    case E_PresentHousingExpenseHousingExpenseType.MaintenanceAndMiscellaneous:
                    case E_PresentHousingExpenseHousingExpenseType.OtherHousingExpense:
                    case E_PresentHousingExpenseHousingExpenseType.Undefined:
                    case E_PresentHousingExpenseHousingExpenseType.Utilities:
                        dOtherHousingExpense += dataApp.LoanData.m_convertLos.ToMoney(presentHousingExpense.PaymentAmount);
                        break;
                    default:
                        throw new UnhandledEnumException(presentHousingExpense.HousingExpenseType);
                }

                if (dOtherHousingExpense > 0)
                {
                    dataApp.aPresOHExp_rep = dOtherHousingExpense.ToString();
                    dataApp.aPresOHExpDesc = "Other Housing Expenses";
                }

                // On app 2-N, if the borrower will not occupy per the declarations then mark occupancy as investment 
                //  so that the borrower's present housing expense will be included in the ratios
                if((dataApp.aAppId != m_dataLoan.GetAppData(0).aAppId) && 
                    (string.Equals(dataApp.aDecOcc, "N", StringComparison.OrdinalIgnoreCase)) &&
                    dataApp.aOccT == E_aOccT.PrimaryResidence)
                {
                    dataApp.aOccT = E_aOccT.Investment;
                }

                //presentHousingExpense.HousingExpenseTypeOtherDescription;
            }
        }
        private void Parse(GovernmentMonitoring governmentMonitoring, CAppData dataApp)
        {
            //governmentMonitoring.Id;
            //governmentMonitoring.GenderType;
            //governmentMonitoring.HmdaEthnicityType;
            //governmentMonitoring.OtherRaceNationalOriginDescription;
            //governmentMonitoring.RaceNationalOriginRefusalIndicator;
            //governmentMonitoring.RaceNationalOriginType;
            Parse(governmentMonitoring.HmdaRaceList);
        }
        private void Parse(List<HmdaRace> hmdaRaceList)
        {
            foreach (var hmdaRace in hmdaRaceList)
            {
                //hmdaRace.Id;
                //hmdaRace.Type;
            }
        }
        private void Parse(FhaVaBorrower fhaVaBorrower, CAppData dataApp)
        {
            //fhaVaBorrower.Id;
            //fhaVaBorrower.CaivrsIdentifier;
            //fhaVaBorrower.FnmBankruptcyCount;
            //fhaVaBorrower.FnmBorrowerCreditRating;
            //fhaVaBorrower.FnmCreditReportScoreType;
            //fhaVaBorrower.FnmForeclosureCount;
            //fhaVaBorrower.VeteranStatusIndicator;
            //fhaVaBorrower.CertificationSalesPriceExceedsAppraisedValueType;
        }
        private void Parse(List<Employer> employerList, CAppData dataApp)
        {
            IEmpCollection empCollection = dataApp.aEmpCollection;
            empCollection.ClearAll();
            foreach (var employer in employerList)
            {
                if (employer.EmploymentPrimaryIndicator == E_YNIndicator.Y)
                {
                    IPrimaryEmploymentRecord record = empCollection.GetPrimaryEmp(true);
                    record.EmplrNm = employer.Name;
                    record.EmplrAddr = employer.StreetAddress;
                    record.EmplrCity = employer.City;
                    record.EmplrState = employer.State;
                    record.EmplrZip = employer.PostalCode;
                    record.EmplrBusPhone = employer.TelephoneNumber;

                    YearMonthParser yearMonthParser = new YearMonthParser();
                    yearMonthParser.ParseStr(employer.CurrentEmploymentYearsOnJob, employer.CurrentEmploymentMonthsOnJob);
                    record.EmplmtLen_rep = yearMonthParser.YearsInDecimal.ToString();

                    record.ProfLen_rep = employer.CurrentEmploymentTimeInLineOfWorkYears;
                    record.IsSelfEmplmt = employer.EmploymentBorrowerSelfEmployedIndicator == E_YNIndicator.Y;
                    record.JobTitle = employer.EmploymentPositionDescription;
                    record.Update();
                }
                else
                {
                    IRegularEmploymentRecord record = empCollection.AddRegularRecord();
                    record.EmplrNm = employer.Name;
                    record.EmplrAddr = employer.StreetAddress;
                    record.EmplrCity = employer.City;
                    record.EmplrState = employer.State;
                    record.EmplrZip = employer.PostalCode;
                    record.EmplrBusPhone = employer.TelephoneNumber;
                    record.IsSelfEmplmt = employer.EmploymentBorrowerSelfEmployedIndicator == E_YNIndicator.Y;
                    record.JobTitle = employer.EmploymentPositionDescription;
                    record.MonI_rep = employer.IncomeEmploymentMonthlyAmount;
                    record.EmplmtEndD_rep = employer.PreviousEmploymentEndDate;
                    record.EmplmtStartD_rep = employer.PreviousEmploymentStartDate;
                    record.IsCurrent = employer.EmploymentCurrentIndicator == E_YNIndicator.Y;
                    record.Update();
                }

            }
        }
        private void Parse(List<Dependent> dependentList, CAppData dataApp)
        {
            foreach (var dependent in dependentList)
            {
                //dependent.Id;
                //dependent.AgeYears;
            }
        }
        private string ParseDeclaration(E_YNIndicator indicator)
        {
            switch (indicator)
            {
                case E_YNIndicator.Y: return "Y";
                case E_YNIndicator.N: return "N";
                default: return "";
            }
        }
        private void Parse(Declaration declaration, CAppData dataApp)
        {
            dataApp.aDecAlimony = ParseDeclaration(declaration.AlimonyChildSupportObligationIndicator);
            dataApp.aDecBankrupt = ParseDeclaration(declaration.BankruptcyIndicator);
            dataApp.aDecBorrowing = ParseDeclaration(declaration.BorrowedDownPaymentIndicator);
            dataApp.aDecEndorser = ParseDeclaration(declaration.CoMakerEndorserOfNoteIndicator);
            dataApp.aDecObligated = ParseDeclaration(declaration.LoanForeclosureOrJudgementIndicator);
            dataApp.aDecJudgment = ParseDeclaration(declaration.OutstandingJudgementsIndicator);
            dataApp.aDecLawsuit = ParseDeclaration(declaration.PartyToLawsuitIndicator);
            dataApp.aDecDelinquent = ParseDeclaration(declaration.PresentlyDelinquentIndicator);
            dataApp.aDecForeclosure = ParseDeclaration(declaration.PropertyForeclosedPastSevenYearsIndicator);
            switch (declaration.PriorPropertyTitleType)
            {
                case E_DeclarationPriorPropertyTitleType.Sole:
                    dataApp.aDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.S;
                    break;
                case E_DeclarationPriorPropertyTitleType.JointWithOtherThanSpouse:
                    dataApp.aDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.O;
                    break;
                case E_DeclarationPriorPropertyTitleType.JointWithSpouse:
                    dataApp.aDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.SP;
                    break;
                default:
                    dataApp.aDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.Empty;
                    break;
            }
            switch (declaration.PriorPropertyUsageType)
            {
                case E_DeclarationPriorPropertyUsageType.Investment:
                    dataApp.aDecPastOwnedPropT = E_aBDecPastOwnedPropT.IP;
                    break;
                case E_DeclarationPriorPropertyUsageType.PrimaryResidence:
                    dataApp.aDecPastOwnedPropT = E_aBDecPastOwnedPropT.PR;
                    break;
                case E_DeclarationPriorPropertyUsageType.SecondaryResidence:
                    dataApp.aDecPastOwnedPropT = E_aBDecPastOwnedPropT.SH;
                    break;
                default:
                    dataApp.aDecPastOwnedPropT = E_aBDecPastOwnedPropT.Empty;
                    break;
            }
            switch (declaration.HomeownerPastThreeYearsType)
            {
                case E_DeclarationHomeownerPastThreeYearsType.Yes:
                    dataApp.aDecPastOwnership = "Y";
                    break;
                case E_DeclarationHomeownerPastThreeYearsType.No:
                    dataApp.aDecPastOwnership = "N";
                    break;
                default:
                    dataApp.aDecPastOwnership = "";
                    break;
            }
            switch (declaration.IntentToOccupyType)
            {
                case E_DeclarationIntentToOccupyType.Yes:
                    dataApp.aDecOcc = "Y";
                    break;
                case E_DeclarationIntentToOccupyType.No:
                    dataApp.aDecOcc = "N";
                    break;
                default:
                    dataApp.aDecOcc = "";
                    break;
            }
            switch (declaration.CitizenshipResidencyType)
            {
                case E_DeclarationCitizenshipResidencyType.NonPermanentResidentAlien:
                    dataApp.aDecCitizen = "N";
                    dataApp.aDecResidency = "N";
                    if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
                    {
                        dataApp.aBDecForeignNational = "N";
                    }
                    break;
                case E_DeclarationCitizenshipResidencyType.NonResidentAlien:
                    dataApp.aDecCitizen = "N";
                    dataApp.aDecResidency = "N";
                    if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
                    {
                        dataApp.aBDecForeignNational = "Y";
                    }
                    break;
                case E_DeclarationCitizenshipResidencyType.PermanentResidentAlien:
                    dataApp.aDecCitizen = "N";
                    dataApp.aDecResidency = "Y";
                    break;

                case E_DeclarationCitizenshipResidencyType.USCitizen:
                    dataApp.aDecCitizen = "Y";
                    dataApp.aDecResidency = "N";
                    break;
                default:
                    dataApp.aDecCitizen = "";
                    dataApp.aDecResidency = "";
                    if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
                    {
                        dataApp.aBDecForeignNational = "";
                    }
                    break;
            }
            //declaration.Id;
            switch (declaration.BorrowerFirstTimeHomebuyerIndicator)
            {
                case E_YNIndicator.Y:
                    dataApp.aHas1stTimeBuyerTri = E_TriState.Yes;
                    break;
                case E_YNIndicator.N:
                    dataApp.aHas1stTimeBuyerTri = E_TriState.No;
                    break;
                default:
                    dataApp.aHas1stTimeBuyerTri = E_TriState.Blank;
                    break;
            }
            Parse(declaration.ExplanationList);
        }
        private void Parse(List<Explanation> explanationList)
        {
            foreach (var explanation in explanationList)
            {
                //explanation.Id;
                //explanation.Description;
                //explanation.Type;
            }
        }

        public static string Parse(E_CurrentIncomeIncomeType type)
        {
            E_aOIDescT descriptionType;
            switch (type)
            {
                case E_CurrentIncomeIncomeType.AlimonyChildSupport:
                    descriptionType = E_aOIDescT.AlimonyChildSupport;
                    break;
                case E_CurrentIncomeIncomeType.AutomobileExpenseAccount:
                    descriptionType = E_aOIDescT.AutomobileExpenseAccount;
                    break;
                case E_CurrentIncomeIncomeType.FosterCare:
                    descriptionType = E_aOIDescT.FosterCare;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryBasePay:
                    descriptionType = E_aOIDescT.MilitaryBasePay;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryClothesAllowance:
                    descriptionType = E_aOIDescT.MilitaryClothesAllowance;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryCombatPay:
                    descriptionType = E_aOIDescT.MilitaryCombatPay;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryFlightPay:
                    descriptionType = E_aOIDescT.MilitaryFlightPay;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryHazardPay:
                    descriptionType = E_aOIDescT.MilitaryHazardPay;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryOverseasPay:
                    descriptionType = E_aOIDescT.MilitaryOverseasPay;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryPropPay:
                    descriptionType = E_aOIDescT.MilitaryPropPay;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryQuartersAllowance:
                    descriptionType = E_aOIDescT.MilitaryQuartersAllowance;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryRationsAllowance:
                    descriptionType = E_aOIDescT.MilitaryRationsAllowance;
                    break;
                case E_CurrentIncomeIncomeType.MilitaryVariableHousingAllowance:
                    descriptionType = E_aOIDescT.MilitaryVariableHousingAllowance;
                    break;
                case E_CurrentIncomeIncomeType.NotesReceivableInstallment:
                    descriptionType = E_aOIDescT.NotesReceivableInstallment;
                    break;
                case E_CurrentIncomeIncomeType.Pension:
                    descriptionType = E_aOIDescT.PensionRetirement;
                    break;
                case E_CurrentIncomeIncomeType.MortgageDifferential:
                    descriptionType = E_aOIDescT.RealEstateMortgageDifferential;
                    break;
                case E_CurrentIncomeIncomeType.SocialSecurity:
                    descriptionType = E_aOIDescT.SocialSecurityDisability;
                    break;
                case E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow:
                    descriptionType = E_aOIDescT.SubjPropNetCashFlow;
                    break;
                case E_CurrentIncomeIncomeType.Trust:
                    descriptionType = E_aOIDescT.Trust;
                    break;
                case E_CurrentIncomeIncomeType.Unemployment:
                    descriptionType = E_aOIDescT.UnemploymentWelfare;
                    break;
                case E_CurrentIncomeIncomeType.VABenefitsNonEducational:
                    descriptionType = E_aOIDescT.VABenefitsNonEducation;
                    break;
                case E_CurrentIncomeIncomeType.OtherTypesOfIncome:
                default:
                    descriptionType = E_aOIDescT.Other;
                    break;
            }

            return OtherIncome.GetDescription(descriptionType);
        }

        private void Parse(List<CurrentIncome> currentIncomeList, CAppData dataApp)
        {
            var consumerId = dataApp.aConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            List<OtherIncome> aOtherIncomeList;
            if (!m_dataLoan.sIsIncomeCollectionEnabled)
            {
                aOtherIncomeList = dataApp.aOtherIncomeList;
            }
            else
            {
                aOtherIncomeList = new List<OtherIncome>();
            }

            foreach (var currentIncome in currentIncomeList)
            {
                if (currentIncome.MonthlyTotalAmount != "")
                {
                    var monthlyTotalAmount = Money.Create(m_dataLoan.m_convertLos.ToMoney(currentIncome.MonthlyTotalAmount));
                    switch (currentIncome.IncomeType)
                    {
                        case E_CurrentIncomeIncomeType.Base:
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                dataApp.aBaseI_rep = currentIncome.MonthlyTotalAmount;
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.BaseIncome,
                                        MonthlyAmountData = monthlyTotalAmount
                                    });
                            }
                            break;
                        case E_CurrentIncomeIncomeType.Overtime:
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                dataApp.aOvertimeI_rep = currentIncome.MonthlyTotalAmount;
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.Overtime,
                                        MonthlyAmountData = monthlyTotalAmount
                                    });
                            }
                            break;
                        case E_CurrentIncomeIncomeType.Bonus:
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                dataApp.aBonusesI_rep = currentIncome.MonthlyTotalAmount;
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.Bonuses,
                                        MonthlyAmountData = monthlyTotalAmount
                                    });
                            }
                            break;
                        case E_CurrentIncomeIncomeType.Commissions:
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                dataApp.aCommisionI_rep = currentIncome.MonthlyTotalAmount;
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.Commission,
                                        MonthlyAmountData = monthlyTotalAmount
                                    });
                            }
                            break;
                        case E_CurrentIncomeIncomeType.DividendsInterest:
                            if (!m_dataLoan.sIsIncomeCollectionEnabled)
                            {
                                dataApp.aDividendI_rep = currentIncome.MonthlyTotalAmount;
                            }
                            else
                            {
                                m_dataLoan.AddIncomeSource(
                                    consumerId,
                                    new LendingQB.Core.Data.IncomeSource
                                    {
                                        IncomeType = IncomeType.DividendsOrInterest,
                                        MonthlyAmountData = monthlyTotalAmount
                                    });
                            }
                            break;
                        case E_CurrentIncomeIncomeType.NetRentalIncome:
                            dataApp.aNetRentI1003Lckd = true;
                            dataApp.aNetRentI1003_rep = currentIncome.MonthlyTotalAmount;
                            break;

                        case E_CurrentIncomeIncomeType.AlimonyChildSupport:
                        case E_CurrentIncomeIncomeType.AutomobileExpenseAccount:
                        case E_CurrentIncomeIncomeType.BoarderIncome:
                        case E_CurrentIncomeIncomeType.BorrowerEstimatedTotalMonthlyIncome:
                        case E_CurrentIncomeIncomeType.ContractBasis:
                        case E_CurrentIncomeIncomeType.FosterCare:
                        case E_CurrentIncomeIncomeType.MilitaryBasePay:
                        case E_CurrentIncomeIncomeType.MilitaryClothesAllowance:
                        case E_CurrentIncomeIncomeType.MilitaryCombatPay:
                        case E_CurrentIncomeIncomeType.MilitaryFlightPay:
                        case E_CurrentIncomeIncomeType.MilitaryHazardPay:
                        case E_CurrentIncomeIncomeType.MilitaryOverseasPay:
                        case E_CurrentIncomeIncomeType.MilitaryPropPay:
                        case E_CurrentIncomeIncomeType.MilitaryQuartersAllowance:
                        case E_CurrentIncomeIncomeType.MilitaryRationsAllowance:
                        case E_CurrentIncomeIncomeType.MilitaryVariableHousingAllowance:
                        case E_CurrentIncomeIncomeType.MortgageCreditCertificate:
                        case E_CurrentIncomeIncomeType.MortgageDifferential:
                        case E_CurrentIncomeIncomeType.NotesReceivableInstallment:
                        case E_CurrentIncomeIncomeType.OtherTypesOfIncome:
                        case E_CurrentIncomeIncomeType.Pension:
                        case E_CurrentIncomeIncomeType.ProposedGrossRentForSubjectProperty:
                        case E_CurrentIncomeIncomeType.PublicAssistance:
                        case E_CurrentIncomeIncomeType.RealEstateOwnedGrossRentalIncome:
                        case E_CurrentIncomeIncomeType.SocialSecurity:
                        case E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow:
                        case E_CurrentIncomeIncomeType.TrailingCoBorrowerIncome:
                        case E_CurrentIncomeIncomeType.Trust:
                        case E_CurrentIncomeIncomeType.Undefined:
                        case E_CurrentIncomeIncomeType.Unemployment:
                        case E_CurrentIncomeIncomeType.VABenefitsNonEducational:
                        case E_CurrentIncomeIncomeType.WorkersCompensation:

                            if (m_dataLoan.sIsIncomeCollectionEnabled && currentIncome.IncomeType == E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow)
                            {
                                this.m_dataLoan.sSpCountRentalIForPrimaryResidToo = true;
                                this.hasSubjectPropertyNetCashFlow = true;
                                this.totalSubjectPropertyNetCashFlow += dataApp.LoanData.m_convertLos.ToMoney(currentIncome.MonthlyTotalAmount);
                            }
                            else
                            {
                                aOtherIncomeList.Add(new OtherIncome()
                                {
                                    Amount = dataApp.LoanData.m_convertLos.ToMoney(currentIncome.MonthlyTotalAmount),
                                    Desc = Parse(currentIncome.IncomeType),
                                    IsForCoBorrower = dataApp.BorrowerModeT != E_BorrowerModeT.Borrower
                                });
                            }
                            break;
                        default:
                            break;
                    }
                }
                //currentIncome.Id;
                //currentIncome.IncomeFederalTaxExemptIndicator;
                //currentIncome.IncomeType;
                //currentIncome.IncomeTypeOtherDescription;
                //currentIncome.MonthlyTotalAmount;
            }

            if (!m_dataLoan.sIsIncomeCollectionEnabled)
            {
                dataApp.aOtherIncomeList = aOtherIncomeList; //this is needed for flush
            }
            else
            {
                var migratedOtherIncome = IncomeCollectionMigration.GetMigratedOtherIncome(aOtherIncomeList, dataApp.aBConsumerId, dataApp.aCConsumerId);
                foreach (var consumerIdIncomeSourcePair in migratedOtherIncome)
                {
                    m_dataLoan.AddIncomeSource(consumerIdIncomeSourcePair.Item1, consumerIdIncomeSourcePair.Item2);
                }
            }
        }
        private void Parse(List<Residence> residenceList, CAppData dataApp)
        {
            if (residenceList == null)
            {
                return;
            }
            int aPrevAddrIndex = 0;
            YearMonthParser yearMonthParser = new YearMonthParser();

            foreach (var residence in residenceList)
            {
                yearMonthParser.ParseStr(residence.BorrowerResidencyDurationYears, residence.BorrowerResidencyDurationMonths);

                        if (residence.BorrowerResidencyType == E_ResidenceBorrowerResidencyType.Current) 
                        {
                            dataApp.aAddr = residence.StreetAddress;
                            dataApp.aCity = residence.City;
                            dataApp.aState = residence.State;
                            dataApp.aZip = residence.PostalCode;
                            dataApp.aAddrYrs = yearMonthParser.YearsInDecimal.ToString();
                            switch (residence.BorrowerResidencyBasisType) 
                            {
                                case E_ResidenceBorrowerResidencyBasisType.Own:
                                    dataApp.aAddrT = E_aBAddrT.Own;
                                    break;
                                case E_ResidenceBorrowerResidencyBasisType.Rent:
                                    dataApp.aAddrT = E_aBAddrT.Rent;
                                    break;
                                case E_ResidenceBorrowerResidencyBasisType.LivingRentFree:
                                    dataApp.aAddrT = E_aBAddrT.LivingRentFree;
                                    break;
                                default:
                                    dataApp.aAddrT = E_aBAddrT.LeaveBlank;
                                    break;

                            }
                        }
                        else if (residence.BorrowerResidencyType == E_ResidenceBorrowerResidencyType.Prior)
                        {
                            if (aPrevAddrIndex == 0)
                            {
                                dataApp.aPrev1Addr = residence.StreetAddress;
                                dataApp.aPrev1City = residence.City;
                                dataApp.aPrev1State = residence.State;
                                dataApp.aPrev1Zip = residence.PostalCode;
                                switch (residence.BorrowerResidencyBasisType)
                                {
                                    case E_ResidenceBorrowerResidencyBasisType.Own:
                                        dataApp.aPrev1AddrT = E_aBPrev1AddrT.Own;
                                        break;
                                    case E_ResidenceBorrowerResidencyBasisType.Rent:
                                        dataApp.aPrev1AddrT = E_aBPrev1AddrT.Rent;
                                        break;
                                    case E_ResidenceBorrowerResidencyBasisType.LivingRentFree:
                                        dataApp.aPrev1AddrT = E_aBPrev1AddrT.LivingRentFree;
                                        break;
                                    default:
                                        dataApp.aPrev1AddrT = E_aBPrev1AddrT.LeaveBlank;
                                        break;
                                }

                                dataApp.aPrev1AddrYrs = yearMonthParser.YearsInDecimal.ToString();

                            }
                            else if (aPrevAddrIndex == 1)
                            {
                                dataApp.aPrev2Addr = residence.StreetAddress;
                                dataApp.aPrev2City = residence.City;
                                dataApp.aPrev2State = residence.State;
                                dataApp.aPrev2Zip = residence.PostalCode;
                                switch (residence.BorrowerResidencyBasisType)
                                {
                                    case E_ResidenceBorrowerResidencyBasisType.Own:
                                        dataApp.aPrev2AddrT = E_aBPrev2AddrT.Own;
                                        break;
                                    case E_ResidenceBorrowerResidencyBasisType.Rent:
                                        dataApp.aPrev2AddrT = E_aBPrev2AddrT.Rent;
                                        break;
                                    case E_ResidenceBorrowerResidencyBasisType.LivingRentFree:
                                        dataApp.aPrev2AddrT = E_aBPrev2AddrT.LivingRentFree;
                                        break;
                                    default:
                                        dataApp.aPrev2AddrT = E_aBPrev2AddrT.LeaveBlank;
                                        break;
                                }

                                dataApp.aPrev2AddrYrs = yearMonthParser.YearsInDecimal.ToString();

                            }
                            aPrevAddrIndex++;
                        }

                Parse(residence.Landlord);
            }
        }
        
        private void Parse(Landlord landlord)
        {
            //landlord.Id;
            //landlord.City;
            //landlord.Name;
            //landlord.PostalCode;
            //landlord.State;
            //landlord.StreetAddress;
            //landlord.StreetAddress2;
            Parse(landlord.ContactDetail);
        }
        private void Parse(MailTo mailTo, CAppData dataApp)
        {

            //mailTo.Id;
            if (mailTo.AddressSameAsPropertyIndicator == E_YNIndicator.Y)
            {
                dataApp.aAddrMailSourceT = E_aAddrMailSourceT.PresentAddress;
            }
            else
            {
                dataApp.aAddrMailSourceT = E_aAddrMailSourceT.Other;
                dataApp.aCityMail = mailTo.City;
                dataApp.aZipMail = mailTo.PostalCode;
                dataApp.aStateMail = mailTo.State;
                dataApp.aAddrMail = mailTo.StreetAddress;
            }
            //mailTo.Country;
            //mailTo.StreetAddress2;
        }
        private void Parse(TransactionDetail transactionDetail)
        {
            m_dataLoan.sAltCost_rep = transactionDetail.AlterationsImprovementsAndRepairsAmount;
            m_dataLoan.sLDiscnt1003Lckd = true;
            m_dataLoan.sLDiscnt1003_rep = transactionDetail.BorrowerPaidDiscountPointsTotalAmount;
            m_dataLoan.sTotEstCc1003Lckd = true;
            m_dataLoan.sTotEstCcNoDiscnt1003_rep = transactionDetail.EstimatedClosingCostsAmount;
            // Upfront MI items are imported from the app/MIData element
            //m_dataLoan.sFfUfmipFinanced_rep = transactionDetail.MiAndFundingFeeFinancedAmount;
            // 3/15/2012 dd - For Encompass Import.
            m_dataLoan.sFfUfmip1003Lckd = true;
            m_dataLoan.sFfUfmip1003_rep = transactionDetail.MiAndFundingFeeTotalAmount;
            m_dataLoan.sTotEstPp1003_rep = transactionDetail.PrepaidItemsEstimatedAmount;
            m_dataLoan.sTotEstPp1003Lckd = true;
            m_dataLoan.sPurchPrice_rep = transactionDetail.PurchasePriceAmount;
            m_dataLoan.sRefPdOffAmt1003Lckd = true;
            m_dataLoan.sRefPdOffAmt1003_rep = transactionDetail.RefinanceIncludingDebtsToBePaidOffAmount;
            m_dataLoan.sTotCcPbsLocked = true;
            m_dataLoan.sTotCcPbs_rep = transactionDetail.SellerPaidClosingCostsAmount;
            m_dataLoan.sFredieReservesAmt_rep = transactionDetail.FreReservesAmount;
            m_dataLoan.sFHASalesConcessions_rep = transactionDetail.SalesConcessionAmount;

            if (transactionDetail.SubordinateLienAmount != "")
            {
                m_dataLoan.sIsOFinNew = true;
                if (m_dataLoan.sLienPosT == E_sLienPosT.First)
                {
                    m_dataLoan.sConcurSubFin_rep = transactionDetail.SubordinateLienAmount;
                }
                else if (m_dataLoan.sLienPosT == E_sLienPosT.Second)
                {
                    m_dataLoan.s1stMtgOrigLAmt_rep = transactionDetail.SubordinateLienAmount;
                }
            }
            m_dataLoan.sHelocBal_rep = transactionDetail.SubordinateLienHelocAmount;

            //transactionDetail.Id;
            //transactionDetail.FreReserveAmount;
            //transactionDetail.FreReservesAmount;
            //transactionDetail.SubordinateLienPurposeType;
            //transactionDetail.SubordinateLienPurposeTypeOtherDescription;
            Parse(transactionDetail.PurchaseCreditList);
        }
        private void Parse(List<PurchaseCredit> purchaseCreditList)
        {
            int purchaseCreditIndex = 0;

            foreach (var purchaseCredit in purchaseCreditList)
            {
                if (purchaseCreditIndex == 0)
                {
                    m_dataLoan.sOCredit1Lckd = true;
                    m_dataLoan.sOCredit1Desc = Parse(purchaseCredit.Type);
                    m_dataLoan.sOCredit1Amt_rep = purchaseCredit.Amount;
                }
                else if (purchaseCreditIndex == 1)
                {
                    //m_dataLoan.sOCredit2Lckd = true;
                    m_dataLoan.sOCredit2Desc = Parse(purchaseCredit.Type);
                    m_dataLoan.sOCredit2Amt_rep = purchaseCredit.Amount;
                }
                else if (purchaseCreditIndex == 2)
                {
                    m_dataLoan.sOCredit3Desc = Parse(purchaseCredit.Type);
                    m_dataLoan.sOCredit3Amt_rep = purchaseCredit.Amount;
                }
                else if (purchaseCreditIndex == 3)
                {
                    m_dataLoan.sOCredit4Desc = Parse(purchaseCredit.Type);
                    m_dataLoan.sOCredit4Amt_rep = purchaseCredit.Amount;
                }
                purchaseCreditIndex++;
            }
        }
        private string Parse(E_PurchaseCreditType type)
        {
            switch (type)
            {
                case E_PurchaseCreditType.EarnestMoney:
                    return "cash deposit on sales contract";
                case E_PurchaseCreditType.EmployerAssistedHousing:
                    return "employer assisted housing";
                case E_PurchaseCreditType.LeasePurchaseFund:
                    return "lease purchase fund";
                case E_PurchaseCreditType.RelocationFunds:
                    return "relocation funds";
                default:
                    return "";
            }

        }
        private void Parse(List<TitleHolder> titleHolderList)
        {
            if (titleHolderList == null)
            {
                return;
            }
            foreach (var titleHolder in titleHolderList)
            {
                //titleHolder.Id;
                //titleHolder.LandTrustType;
                //titleHolder.LandTrustTypeOtherDescription;
                //titleHolder.Name;
            }
        }
        private void Parse(List<RespaFee> respaFeeList)
        {
            if (respaFeeList == null)
            {
                return;
            }
            foreach (var respaFee in respaFeeList)
            {
                //respaFee.Id;
                //respaFee.RespaSectionClassificationType;
                //respaFee.ItemDescription;
                //respaFee.PaidToType;
                //respaFee.PaidToTypeOtherDescription;
                //respaFee.PercentBasisType;
                //respaFee.PercentBasisTypeOtherDescription;
                //respaFee.RequiredProviderOfServiceIndicator;
                //respaFee.ResponsiblePartyType;
                //respaFee.SpecifiedFixedAmount;
                //respaFee.SpecifiedHudLineNumber;
                //respaFee.TotalAmount;
                //respaFee.TotalPercent;
                //respaFee.Type;
                //respaFee.TypeOtherDescription;
                Parse(respaFee.PaymentList);
                Parse(respaFee.RequiredServiceProvider);
                Parse(respaFee.PaidTo);
            }
        }
        private void Parse(PaidTo paidTo)
        {
            //paidTo.Id;
            //paidTo.NonPersonEntityIndicator;
            //paidTo.City;
            //paidTo.Country;
            //paidTo.County;
            //paidTo.Name;
            //paidTo.PostalCode;
            //paidTo.State;
            //paidTo.StreetAddress;
            //paidTo.StreetAddress2;
            Parse(paidTo.ContactDetail);
            Parse(paidTo.NonPersonEntityDetail);
        }
        private void Parse(RequiredServiceProvider requiredServiceProvider)
        {
            //requiredServiceProvider.Id;
            //requiredServiceProvider.City;
            //requiredServiceProvider.Name;
            //requiredServiceProvider.NatureOfRelationshipDescription;
            //requiredServiceProvider.PostalCode;
            //requiredServiceProvider.ReferenceIdentifier;
            //requiredServiceProvider.State;
            //requiredServiceProvider.StreetAddress;
            //requiredServiceProvider.StreetAddress2;
            //requiredServiceProvider.TelephoneNumber;
        }
        private void Parse(List<Payment> paymentList)
        {
            foreach (var payment in paymentList)
            {
                //payment.Id;
                //payment.AllowableFhaClosingCostIndicator;
                //payment.Amount;
                //payment.CollectedByType;
                //payment.IncludedInAprIndicator;
                //payment.IncludedInStateHighCostIndicator;
                //payment.NetDueAmount;
                //payment.PaidByType;
                //payment.PaidByTypeThirdPartyName;
                //payment.PaidOutsideOfClosingIndicator;
                //payment.Percent;
                //payment.ProcessType;
                //payment.Section32Indicator;
            }
        }
        private void Parse(List<ReoProperty> reoPropertyList)
        {
            if (reoPropertyList == null)
            {
                return;
            }
            foreach (var reoProperty in reoPropertyList)
            {
                string[] id = reoProperty.BorrowerId.Split(' '); // 7/29/2009 dd - Borrower Id can be in a joint format i.e: BOR1 COB1. For joint just get the first id.

                CAppData dataApp = m_dataLoan.FindDataAppBySsn(m_borrowerId_Ssn_Hash[id[0]]);

                Parse(reoProperty, dataApp.aReCollection);
            }
        }
        private void Parse(ReoProperty reoProperty, IReCollection aReCollection)
        {
            var reFields = aReCollection.AddRegularRecord();

            string[] matchedLiabilities = reoProperty.LiabilityId.Split(' ');
            foreach (string liaId in matchedLiabilities)
            {
                if (!string.IsNullOrEmpty(liaId))
                {
                    ILiabilityRegular liaFields = null;
                    if(m_liability_Hash.TryGetValue(liaId, out liaFields))
                    {
                        liaFields.MatchedReRecordId = reFields.RecordId;
                        liaFields.Update();
                    }
                }
            }
            reFields.OccR_rep = "100";
            reFields.Addr = reoProperty.StreetAddress;
            reFields.City = reoProperty.City;
            reFields.State = reoProperty.State;
            reFields.Zip = reoProperty.PostalCode;
            reFields.TypeT = Parse(reoProperty.GsePropertyType);
            reFields.StatT = Parse(reoProperty.DispositionStatusType);
            reFields.MPmt_rep = reoProperty.LienInstallmentAmount;
            reFields.MAmt_rep = reoProperty.LienUpbAmount;
            reFields.HExp_rep = reoProperty.MaintenanceExpenseAmount;
            reFields.Val_rep = reoProperty.MarketValueAmount;
            reFields.GrossRentI_rep = reoProperty.RentalIncomeGrossAmount;
            //BB 11/14/12 iOPM 105603: lock the net rental income if provided; catches negative rental income for DTI
            if (m_dataLoan.m_convertLos.ToMoney(reoProperty.RentalIncomeNetAmount) != 0)
            {
                reFields.NetRentILckd = true;
                reFields.NetRentI_rep = reoProperty.RentalIncomeNetAmount;
            }
            reFields.IsSubjectProp = reoProperty.SubjectIndicator == E_YNIndicator.Y;

            reFields.Update();

        }
        private E_ReoStatusT Parse(E_ReoPropertyDispositionStatusType type)
        {
            switch (type)
            {
                case E_ReoPropertyDispositionStatusType.PendingSale: return E_ReoStatusT.PendingSale;
                case E_ReoPropertyDispositionStatusType.RetainForRental: return E_ReoStatusT.Rental;
                case E_ReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence: return E_ReoStatusT.Residence;
                case E_ReoPropertyDispositionStatusType.Sold: return E_ReoStatusT.Sale;
                default:
                    return E_ReoStatusT.Residence;
            }
        }

        private E_ReoTypeT Parse(E_ReoPropertyGsePropertyType type)
        {
            switch (type)
            {
                case E_ReoPropertyGsePropertyType.TwoToFourUnitProperty: return E_ReoTypeT._2_4Plx;
                case E_ReoPropertyGsePropertyType.CommercialNonResidential: return E_ReoTypeT.ComNR;
                case E_ReoPropertyGsePropertyType.HomeAndBusinessCombined: return E_ReoTypeT.ComR;
                case E_ReoPropertyGsePropertyType.Condominium: return E_ReoTypeT.Condo;
                case E_ReoPropertyGsePropertyType.Cooperative: return E_ReoTypeT.Coop;
                case E_ReoPropertyGsePropertyType.Farm: return E_ReoTypeT.Farm;
                case E_ReoPropertyGsePropertyType.Land: return E_ReoTypeT.Land;
                case E_ReoPropertyGsePropertyType.MixedUseResidential: return E_ReoTypeT.Mixed;
                case E_ReoPropertyGsePropertyType.ManufacturedMobileHome: return E_ReoTypeT.Mobil;
                case E_ReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits: return E_ReoTypeT.Multi;
                case E_ReoPropertyGsePropertyType.SingleFamily: return E_ReoTypeT.SFR;
                case E_ReoPropertyGsePropertyType.Townhouse: return E_ReoTypeT.Town;
                case E_ReoPropertyGsePropertyType.Undefined: return E_ReoTypeT.LeaveBlank;
                default:
                    return E_ReoTypeT.LeaveBlank;
            }
        }

        private void Parse(List<ProposedHousingExpense> proposedHousingExpenseList)
        {
            if (null == proposedHousingExpenseList)
            {
                return;
            }
            foreach (var proposedHousingExpense in proposedHousingExpenseList)
            {
                switch (proposedHousingExpense.HousingExpenseType)
                {
                    case E_ProposedHousingExpenseHousingExpenseType.MI:
                        if (m_dataLoan.sProMIns_rep != proposedHousingExpense.PaymentAmount)
                        {
                            m_dataLoan.sProMInsLckd = true;
                            m_dataLoan.sProMIns_rep = proposedHousingExpense.PaymentAmount;
                        }
                        break;
                    case E_ProposedHousingExpenseHousingExpenseType.HazardInsurance:
                        m_dataLoan.sProHazInsR_rep = "0";
                        m_dataLoan.sProHazInsMb_rep = proposedHousingExpense.PaymentAmount;
                        break;
                    // case E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest:
                    case E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense:
                        m_dataLoan.sProOHExpLckd = true;
                        m_dataLoan.sProOHExp_rep = proposedHousingExpense.PaymentAmount;
                        m_dataLoan.sProOHExpDesc = proposedHousingExpense.HousingExpenseTypeOtherDescription;
                        break;
                    case E_ProposedHousingExpenseHousingExpenseType.RealEstateTax:
                        m_dataLoan.sProRealETxMb_rep = proposedHousingExpense.PaymentAmount;
                        m_dataLoan.sProRealETxR_rep = "0";
                        break;
                    case E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees:
                        m_dataLoan.sProHoAssocDues_rep = proposedHousingExpense.PaymentAmount;
                        break;
                    // case E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest:
                    default:
                        break;
                }
            }
        }
        private void Parse(Property property)
        {
            m_dataLoan.sSpCity = property.City;
            m_dataLoan.sUnitsNum_rep = property.FinancedNumberOfUnits;
            m_dataLoan.sSpZip = property.PostalCode;
            m_dataLoan.sSpState = property.State;
            m_dataLoan.sSpAddr = property.StreetAddress;
            m_dataLoan.sYrBuilt = property.StructureBuiltYear;
            m_dataLoan.sSpCounty = property.County;
            if (property.PlannedUnitDevelopmentIndicator != E_YNIndicator.Undefined)
            {
                m_dataLoan.sSpIsInPud = property.PlannedUnitDevelopmentIndicator == E_YNIndicator.Y;
            }
            if (property.BuildingStatusType != E_PropertyBuildingStatusType.Undefined)
            {
                m_dataLoan.sBuildingStatusT = Parse(property.BuildingStatusType);
            }

            //property.Id;
            //property.AssessorsParcelIdentifier;
            //property.AssessorsSecondParcelIdentifier;
            //property.BuildingStatusTypeOtherDescription;
            //property.CurrentVacancyStatusType;
            //property.GrossLivingAreaSquareFeetCount;
            //property.ManufacturedHomeManufactureYear;
            //property.NativeAmericanLandsType;
            //property.NativeAmericanLandsTypeOtherDescription;
            //property.UniqueDwellingType;
            //property.UniqueDwellingTypeOtherDescription;
            //property.AcquiredDate;
            //property.AcreageNumber;
            //property.CommunityLandTrustIndicator;
            //property.ConditionDescription;
            //property.Country;
            //property.InclusionaryZoningIndicator;
            //property.NeighborhoodLocationType;
            //property.NeighborhoodLocationTypeOtherDescription;
            //property.OwnershipType;
            //property.OwnershipTypeOtherDescription;
            //property.PreviouslyOccupiedIndicator;
            //property.StreetAddress2;
            //property.ZoningCategoryType;
            //property.ZoningCategoryTypeOtherDescription;
            Parse(property.LegalDescriptionList);
            Parse(property.ParsedStreetAddress);
            Parse(property.ValuationList);
            Parse(property.Details);
            Parse(property.HomeownersAssociation);
            Parse(property.Project);
            Parse(property.CategoryList);
            if (m_dataLoan.sGseSpT == E_sGseSpT.Condominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium)
            {
                m_dataLoan.sProdCondoStories_rep = property.StoriesCount;
            }
            m_sCondoStories = property.StoriesCount;

            Parse(property.DwellingUnitList);
            Parse(property.ManufacturedHomeList);
            Parse(property.PlattedLandList);
            Parse(property.UnplattedLand);
            Parse(property.FloodDeterminationList);
        }
        private E_sBuildingStatusT Parse(E_PropertyBuildingStatusType type)
        {
            switch (type)
            {
                case E_PropertyBuildingStatusType.Existing:
                    return E_sBuildingStatusT.Existing;
                case E_PropertyBuildingStatusType.Proposed:
                    return E_sBuildingStatusT.Proposed;
                case E_PropertyBuildingStatusType.SubjectToAlterationImprovementRepairAndRehabilitation:
                    return E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab;
                case E_PropertyBuildingStatusType.UnderConstruction:
                    return E_sBuildingStatusT.UnderConstruction;
                default:
                    return E_sBuildingStatusT.LeaveBlank;
            }
        }

        private void Parse(List<FloodDetermination> floodDeterminationList)
        {
            foreach (var floodDetermination in floodDeterminationList)
            {
                //floodDetermination.FloodDeterminationId;
                //floodDetermination.FloodCertificationIdentifier;
                //floodDetermination.FloodContractFeeAmount;
                //floodDetermination.FloodDeterminationLifeofLoanIndicator;
                //floodDetermination.FloodPartialIndicator;
                //floodDetermination.FloodProductCertifyDate;
                //floodDetermination.NfipCommunityIdentifier;
                //floodDetermination.NfipCommunityName;
                //floodDetermination.NfipCommunityParticipationStatusType;
                //floodDetermination.NfipCommunityParticipationStatusTypeOtherDescription;
                //floodDetermination.NfipFloodZoneIdentifier;
                //floodDetermination.NfipMapIdentifier;
                //floodDetermination.NfipMapPanelDate;
                //floodDetermination.NfipMapPanelIdentifier;
                //floodDetermination.SpecialFloodHazardAreaIndicator;
            }
        }
        private void Parse(UnplattedLand unplattedLand)
        {
            //unplattedLand.Id;
            //unplattedLand.PropertyRangeIdentifier;
            //unplattedLand.PropertySectionIdentifier;
            //unplattedLand.PropertyTownshipIdentifier;
            //unplattedLand.AbstractNumberIdentifier;
            //unplattedLand.BaseIdentifier;
            //unplattedLand.DescriptionType;
            //unplattedLand.DescriptionTypeOtherDescription;
            //unplattedLand.LandGrantIdentifier;
            //unplattedLand.MeridianIdentifier;
            //unplattedLand.MetesAndBoundsRemainingDescription;
            //unplattedLand.QuarterSectionIdentifier;
            //unplattedLand.SequenceIdentifier;
        }
        private void Parse(List<PlattedLand> plattedLandList)
        {
            foreach (var plattedLand in plattedLandList)
            {
                //plattedLand.Id;
                //plattedLand.PlatName;
                //plattedLand.PropertyBlockIdentifier;
                //plattedLand.PropertyLotIdentifier;
                //plattedLand.PropertySectionIdentifier;
                //plattedLand.PropertySubdivisionIdentifier;
                //plattedLand.PropertyTractIdentifier;
                //plattedLand.RecordedDocumentBook;
                //plattedLand.RecordedDocumentPage;
                //plattedLand.AdditionalParcelDescription;
                //plattedLand.AdditionalParcelIdentifier;
                //plattedLand.AppurtenanceDescription;
                //plattedLand.AppurtenanceIdentifier;
                //plattedLand.BuildingIdentifier;
                //plattedLand.PlatCodeIdentifier;
                //plattedLand.PlatInstrumentIdentifier;
                //plattedLand.SequenceIdentifier;
                //plattedLand.Type;
                //plattedLand.TypeOtherDescription;
                //plattedLand.UnitNumberIdentifier;
            }
        }
        private void Parse(List<ManufacturedHome> manufacturedHomeList)
        {
            foreach (var manufacturedHome in manufacturedHomeList)
            {
                //manufacturedHome.Id;
                //manufacturedHome.LengthFeetCount;
                //manufacturedHome.WidthFeetCount;
                //manufacturedHome.AttachedToFoundationIndicator;
                //manufacturedHome.ConditionDescriptionType;
                //manufacturedHome.HudCertificationLabelIdentifier;
                //manufacturedHome.MakeIdentifier;
                //manufacturedHome.ModelIdentifier;
                //manufacturedHome.SerialNumberIdentifier;
                //manufacturedHome.WidthType;
            }
        }
        private void Parse(List<DwellingUnit> dwellingUnitList)
        {
            foreach (var dwellingUnit in dwellingUnitList)
            {
                //dwellingUnit.Id;
                //dwellingUnit.BedroomCount;
                //dwellingUnit.PropertyRehabilitationCompletionDate;
                //dwellingUnit.EligibleRentAmount;
                //dwellingUnit.LeaseProvidedIndicator;
            }
        }
        private void Parse(List<Category> categoryList)
        {
            foreach (var category in categoryList)
            {
                m_dataLoan.sGseSpT = Parse(category.Type);
                //category.Id;
                //category.TypeOtherDescription;
            }
        }
        private void Parse(Project project)
        {
            //project.Id;
            //project.LivingUnitCount;
            //project.ClassificationType;
            //project.ClassificationTypeOtherDescription;
            //project.DesignType;
            //project.DesignTypeOtherDescription;
        }
        private E_sGseSpT Parse(E_CategoryType type)
        {
            switch (type)
            {
                case E_CategoryType.Attached: return E_sGseSpT.Attached;
                case E_CategoryType.Detached: return E_sGseSpT.Detached;
                case E_CategoryType.HighRise: return E_sGseSpT.HighRiseCondominium;
                case E_CategoryType.LowRise: return E_sGseSpT.Condominium;
                case E_CategoryType.Manufactured: return E_sGseSpT.ManufacturedHousing;
                case E_CategoryType.ManufacturedMultiWide: return E_sGseSpT.ManufacturedHomeMultiwide;
                case E_CategoryType.ManufacturedSingleWide: return E_sGseSpT.ManufacturedHousingSingleWide;
                case E_CategoryType.Modular: return E_sGseSpT.Modular;
                default:
                    return E_sGseSpT.LeaveBlank;
            }
        }
        private void Parse(HomeownersAssociation homeownersAssociation)
        {
            //homeownersAssociation.Id;
            //homeownersAssociation.City;
            //homeownersAssociation.Country;
            //homeownersAssociation.County;
            //homeownersAssociation.Name;
            //homeownersAssociation.PostalCode;
            //homeownersAssociation.State;
            //homeownersAssociation.StreetAddress;
            //homeownersAssociation.StreetAddress2;
            Parse(homeownersAssociation.ContactDetail);
        }
        private void Parse(Details details)
        {
            //details.Id;
            //details.CondominiumIndicator;
            //details.CondominiumPudDeclarationsDescription;
            //details.JudicialDistrictName;
            //details.JudicialDivisionName;
            //details.ManufacturedHomeIndicator;
            //details.NfipCommunityIdentifier;
            //details.NfipCommunityName;
            //details.NfipCommunityParticipationStatusType;
            //details.NfipCommunityParticipationStatusTypeOtherDescription;
            //details.NfipFloodZoneIdentifier;
            //details.OneToFourFamilyIndicator;
            //details.ProjectName;
            //details.ProjectTotalSharesCount;
            //details.PropertyUnincorporatedAreaName;
            //details.RecordingJurisdictionName;
            //details.RecordingJurisdictionType;
            //details.RecordingJurisdictionTypeOtherDescription;
        }
        private void Parse(List<Valuation> valuationList)
        {
            foreach (var valuation in valuationList)
            {
                //valuation.Id;
                //valuation.AppraisalFormType;
                //valuation.AppraisalFormTypeOtherDescription;
                //valuation.AppraisalFormVersionIdentifier;
                //valuation.AppraisalInspectionType;
                //valuation.AppraisalInspectionTypeOtherDescription;
                //valuation.MethodType;
                //valuation.MethodTypeOtherDescription;
                Parse(valuation.AppraiserList);
            }
        }
        private void Parse(List<Appraiser> appraiserList)
        {
            foreach (var appraiser in appraiserList)
            {
                //appraiser.Id;
                //appraiser.CompanyName;
                //appraiser.LicenseIdentifier;
                //appraiser.LicenseState;
                //appraiser.Name;
            }
        }
        private void Parse(ParsedStreetAddress parsedStreetAddress)
        {
            //parsedStreetAddress.Id;
            //parsedStreetAddress.ApartmentOrUnit;
            //parsedStreetAddress.BuildingNumber;
            //parsedStreetAddress.DirectionPrefix;
            //parsedStreetAddress.DirectionSuffix;
            //parsedStreetAddress.HouseNumber;
            //parsedStreetAddress.MilitaryApoFpo;
            //parsedStreetAddress.PostOfficeBox;
            //parsedStreetAddress.RuralRoute;
            //parsedStreetAddress.StreetName;
            //parsedStreetAddress.StreetSuffix;
        }
        private void Parse(List<LegalDescription> legalDescriptionList)
        {
            string legalDesc = "";
            foreach (var legalDescription in legalDescriptionList)
            {
                if (null != legalDescription)
                    legalDesc += legalDescription.TextDescription;
            }
            m_dataLoan.sSpLegalDesc = legalDesc;
        }
        private void Parse(PlattedLand plattedLand)
        {
            //plattedLand.Id;
            //plattedLand.PlatName;
            //plattedLand.PropertyBlockIdentifier;
            //plattedLand.PropertyLotIdentifier;
            //plattedLand.PropertySectionIdentifier;
            //plattedLand.PropertySubdivisionIdentifier;
            //plattedLand.PropertyTractIdentifier;
            //plattedLand.RecordedDocumentBook;
            //plattedLand.RecordedDocumentPage;
            //plattedLand.AdditionalParcelDescription;
            //plattedLand.AdditionalParcelIdentifier;
            //plattedLand.AppurtenanceDescription;
            //plattedLand.AppurtenanceIdentifier;
            //plattedLand.BuildingIdentifier;
            //plattedLand.PlatCodeIdentifier;
            //plattedLand.PlatInstrumentIdentifier;
            //plattedLand.SequenceIdentifier;
            //plattedLand.Type;
            //plattedLand.TypeOtherDescription;
            //plattedLand.UnitNumberIdentifier;
        }
        private void Parse(MortgageTerms mortgageTerms)
        {
            m_dataLoan.sAgencyCaseNum = mortgageTerms.AgencyCaseIdentifier;
            m_dataLoan.sLAmtLckd = true;
            m_dataLoan.sLAmtCalc_rep = mortgageTerms.BaseLoanAmount;
            m_dataLoan.sLenderCaseNum = mortgageTerms.LenderCaseIdentifier;
            m_dataLoan.sTerm_rep = mortgageTerms.LoanAmortizationTermMonths;
            m_dataLoan.sNoteIR_rep = mortgageTerms.RequestedInterestRatePercent;

            switch (mortgageTerms.LoanAmortizationType)
            {
                case E_MortgageTermsLoanAmortizationType.Fixed:
                    m_dataLoan.sFinMethT = E_sFinMethT.Fixed;
                    break;
                case E_MortgageTermsLoanAmortizationType.AdjustableRate:
                    m_dataLoan.sFinMethT = E_sFinMethT.ARM;
                    break;
                case E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage:
                    m_dataLoan.sFinMethT = E_sFinMethT.Graduated;
                    break;
                default:
                    m_dataLoan.sFinMethT = E_sFinMethT.Fixed;
                    break;
            }
            switch (mortgageTerms.MortgageType)
            {
                case E_MortgageTermsMortgageType.Conventional:
                    m_dataLoan.sLT = E_sLT.Conventional;
                    break;
                case E_MortgageTermsMortgageType.FHA:
                    m_dataLoan.sLT = E_sLT.FHA;
                    break;
                case E_MortgageTermsMortgageType.VA:
                    m_dataLoan.sLT = E_sLT.VA;
                    break;
                case E_MortgageTermsMortgageType.FarmersHomeAdministration:
                    m_dataLoan.sLT = E_sLT.UsdaRural;
                    break;
                default:
                    m_dataLoan.sLT = E_sLT.Other;
                    m_dataLoan.sLTODesc = mortgageTerms.OtherMortgageTypeDescription;
                    break;
            }

            //mortgageTerms.Id;
            //mortgageTerms.ArmTypeDescription;
            //mortgageTerms.BorrowerRequestedLoanAmount;
            //mortgageTerms.LenderLoanIdentifier;
            //mortgageTerms.LendersContactPrimaryTelephoneNumber;
            //mortgageTerms.LoanEstimatedClosingDate;
            //mortgageTerms.NoteRatePercent;
            //mortgageTerms.OriginalLoanAmount;
            //mortgageTerms.OtherAmortizationTypeDescription;
            //mortgageTerms.OtherMortgageTypeDescription;
            //mortgageTerms.PaymentRemittanceDay;
        }
        private void Parse(MiData miData)
        {
            //miData.Id;
            //miData.BorrowerMiTerminationDate;
            //miData.MiCertificateIdentifier;
            //miData.MiCollectedNumberOfMonthsCount;
            //miData.MiCompanyName;
            //miData.MiCushionNumberOfMonthsCount;
            //miData.MiDurationType;
            //miData.MiEscrowIncludedInAggregateIndicator;
            m_dataLoan.sFfUfmip1003Lckd = true;
            if (miData.MiSourceType == E_MiDataMiSourceType.FHA)
            {
                m_dataLoan.sFfUfmip1003_rep = miData.MiFhaUpfrontPremiumAmount;
            }
            else
            {
                m_dataLoan.sFfUfmip1003_rep = miData.MiInitialPremiumAmount;
            }
            m_dataLoan.sFfUfMipIsBeingFinanced = (miData.MiPremiumFinancedIndicator == E_YNIndicator.Y);
            //miData.MiInitialPremiumAtClosingType;
            m_dataLoan.sMipPiaMon_rep = miData.MiInitialPremiumRateDurationMonths;

            // UFMIP rate is read from the Government_Loan element for govie loans
            if (!((m_dataLoan.sLT == E_sLT.FHA) || (m_dataLoan.sLT == E_sLT.VA)))
                m_dataLoan.sFfUfmipR_rep = miData.MiInitialPremiumRatePercent;

            m_dataLoan.SetsUfCashPdViaEncompassImport(miData.MiPremiumFromClosingAmount); // OPM 32441
            if (m_dataLoan.sLT == E_sLT.FHA)
                m_dataLoan.sProdIsFhaMipFinanced = m_dataLoan.sFfUfMipIsBeingFinanced;

            m_dataLoan.sProdMIOptionT = Parse(miData.MiPremiumPaymentType);
            //miData.MiPremiumRefundableType;
            //miData.MiPremiumTermMonths;
            //miData.MiRenewalCalculationType;
            //miData.MiScheduledTerminationDate;
            m_dataLoan.sProMInsCancelLtv_rep = miData.MiLtvCutoffPercent;
            //miData.MiLtvCutoffType;
            //miData.ScheduledAmortizationMidpointDate;
            Parse(miData.MiPremiumTax);
            Parse(miData.MiRenewalPremiumList);
            Parse(miData.PaidTo);
        }
        private E_sProdMIOptionT Parse(E_MiDataMiPremiumPaymentType miPaymentType)
        {
            switch (miPaymentType)
            {
                case E_MiDataMiPremiumPaymentType.BorrowerPaid: return E_sProdMIOptionT.BorrowerPdPmi;
                case E_MiDataMiPremiumPaymentType.BothBorrowerAndLenderPaid: return E_sProdMIOptionT.BorrowerPdPmi;
                case E_MiDataMiPremiumPaymentType.Financed: return E_sProdMIOptionT.BorrowerPdPmi;
                case E_MiDataMiPremiumPaymentType.LenderPaid: return E_sProdMIOptionT.NoPmi;
                case E_MiDataMiPremiumPaymentType.PaidFromEscrow: return E_sProdMIOptionT.BorrowerPdPmi;
                case E_MiDataMiPremiumPaymentType.Prepaid: return E_sProdMIOptionT.BorrowerPdPmi;
                case E_MiDataMiPremiumPaymentType.Undefined: return E_sProdMIOptionT.LeaveBlank;
                default: return E_sProdMIOptionT.LeaveBlank;
            }
        }
        private void Parse(List<MiRenewalPremium> miRenewalPremiumList)
        {
            foreach (var miRenewalPremium in miRenewalPremiumList)
            {
                //miRenewalPremium.Id;
                switch(miRenewalPremium.Sequence)
                {
                    case E_MiRenewalPremiumSequence.First:
                        m_dataLoan.sProMInsLckd = true;
                        m_dataLoan.sProMIns_rep = miRenewalPremium.MonthlyPaymentAmount;
                        m_dataLoan.sProMInsR_rep = miRenewalPremium.Rate;
                        m_dataLoan.sProMInsMon_rep = miRenewalPremium.RateDurationMonths;
                        break;
                    case E_MiRenewalPremiumSequence.Second:
                        m_dataLoan.sProMInsR2_rep = miRenewalPremium.Rate;
                        m_dataLoan.sProMIns2Mon_rep = miRenewalPremium.RateDurationMonths;
                        break;
                    case E_MiRenewalPremiumSequence.Third:
                    case E_MiRenewalPremiumSequence.Fourth:
                    case E_MiRenewalPremiumSequence.Fifth:
                    case E_MiRenewalPremiumSequence.Undefined:
                    default:
                        break;
                }
                //miRenewalPremium.MonthlyPaymentRoundingType;
            }
        }
        private void Parse(MiPremiumTax miPremiumTax)
        {
            //miPremiumTax.Id;
            //miPremiumTax.CodeAmount;
            //miPremiumTax.CodePercent;
            //miPremiumTax.CodeType;
        }
        private void Parse(Mers mers)
        {
            //mers.Id;
            //mers.MersMortgageeOfRecordIndicator;
            //mers.MersOriginalMortgageeOfRecordIndicator;
            //mers.MersRegistrationDate;
            //mers.MersRegistrationIndicator;
            //mers.MersRegistrationStatusType;
            //mers.MersRegistrationStatusTypeOtherDescription;
            //mers.MersTaxNumberIdentifier;
            //mers.MersMinNumber;
        }
        private void Parse(LoanQualification loanQualification)
        {
            //loanQualification.Id;
            m_dataLoan.sMultiApps = loanQualification.AdditionalBorrowerAssetsConsideredIndicator == E_YNIndicator.Y;

            m_dataLoan.GetAppData(0).aSpouseIExcl = loanQualification.AdditionalBorrowerAssetsNotConsideredIndicator == E_YNIndicator.Y;
        }
        private void Parse(LoanPurpose loanPurpose)
        {
            m_dataLoan.GetAppData(0).aManner = loanPurpose.GseTitleMannerHeldDescription;
            m_dataLoan.sOLPurposeDesc = loanPurpose.OtherLoanPurposeDescription;
            m_dataLoan.sLeaseHoldExpireD_rep = loanPurpose.PropertyLeaseholdExpirationDate;
            m_dataLoan.sLPurposeT = Parse(loanPurpose.Type);

            m_dataLoan.sEstateHeldT = Parse(loanPurpose.PropertyRightsType);
            m_dataLoan.GetAppData(0).aOccT = Parse(loanPurpose.PropertyUsageType);

            //loanPurpose.Id;
            //loanPurpose.PropertyRightsTypeOtherDescription;
            //loanPurpose.PropertyUsageTypeOtherDescription;
            Parse(loanPurpose.ConstructionRefinanceData);
        }
        private E_sEstateHeldT Parse(E_LoanPurposePropertyRightsType propertyRightsType)
        {
            switch (propertyRightsType)
            {
                case E_LoanPurposePropertyRightsType.FeeSimple:
                    return E_sEstateHeldT.FeeSimple;
                case E_LoanPurposePropertyRightsType.Leasehold:
                    return E_sEstateHeldT.LeaseHold;
                default:
                    return E_sEstateHeldT.FeeSimple; // 7/8/2009 dd - Default to FeeSimple
            }
        }
        private E_aOccT Parse(E_LoanPurposePropertyUsageType propertyUsageType)
        {
            switch (propertyUsageType)
            {
                case E_LoanPurposePropertyUsageType.Investment:
                    return E_aOccT.Investment;
                case E_LoanPurposePropertyUsageType.PrimaryResidence:
                    return E_aOccT.PrimaryResidence;
                case E_LoanPurposePropertyUsageType.SecondHome:
                    return E_aOccT.SecondaryResidence;
                default:
                    return E_aOccT.PrimaryResidence;
            }
        }
        private E_sLPurposeT Parse(E_LoanPurposeType loanPurposeType)
        {
            switch (loanPurposeType)
            {
                case E_LoanPurposeType.ConstructionOnly:
                    return E_sLPurposeT.Construct;
                case E_LoanPurposeType.ConstructionToPermanent:
                    return E_sLPurposeT.ConstructPerm;
                case E_LoanPurposeType.Other:
                    return E_sLPurposeT.Other;
                case E_LoanPurposeType.Purchase:
                    return E_sLPurposeT.Purchase;
                case E_LoanPurposeType.Refinance:
                    return E_sLPurposeT.Refin;
                case E_LoanPurposeType.Undefined:
                case E_LoanPurposeType.Unknown:
                default:
                    return E_sLPurposeT.Other;
            }

        }
        private void Parse(ConstructionRefinanceData constructionRefinanceData)
        {
            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                m_dataLoan.sSpOrigC_rep = constructionRefinanceData.PropertyOriginalCostAmount;
                m_dataLoan.sSpAcqYr = constructionRefinanceData.PropertyAcquiredYear;
                m_dataLoan.sSpLien_rep = constructionRefinanceData.PropertyExistingLienAmount;
                m_dataLoan.sSpImprovC_rep = constructionRefinanceData.RefinanceImprovementCostsAmount;
                m_dataLoan.sSpImprovDesc = constructionRefinanceData.RefinanceProposedImprovementsDescription;
                if (constructionRefinanceData.GseRefinancePurposeType == E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation ||
                    constructionRefinanceData.GseRefinancePurposeType == E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement ||
                    constructionRefinanceData.GseRefinancePurposeType == E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited ||
                    constructionRefinanceData.GseRefinancePurposeType == E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther)
                {
                    m_dataLoan.sLPurposeT = E_sLPurposeT.RefinCashout;
                }
                m_dataLoan.sProdCashoutAmt_rep = constructionRefinanceData.FreCashOutAmount;

                m_dataLoan.sPayingOffSubordinate = constructionRefinanceData.SecondaryFinancingRefinanceIndicator == E_YNIndicator.Y;
                switch (constructionRefinanceData.RefinanceImprovementsType)
                {
                    case E_ConstructionRefinanceDataRefinanceImprovementsType.Made:
                        m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.Made;
                        break;
                    case E_ConstructionRefinanceDataRefinanceImprovementsType.ToBeMade:
                        m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.ToBeMade;
                        break;
                    default:
                        m_dataLoan.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.LeaveBlank;
                        break;
                }
                m_dataLoan.sGseRefPurposeT = Parse(constructionRefinanceData.GseRefinancePurposeType);
                switch (constructionRefinanceData.GseRefinancePurposeType)
                {
                    case E_ConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm:
                        m_dataLoan.sRefPurpose = "no cash-out rate/term";
                        break;
                    case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited:
                        m_dataLoan.sRefPurpose = "limited cash-out rate/term";
                        break;
                    case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement:
                        m_dataLoan.sRefPurpose = "cash-out/home improvement";
                        break;
                    case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation:
                        m_dataLoan.sRefPurpose = "cash-out/debt consolidation";
                        break;
                    case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther:
                        m_dataLoan.sRefPurpose = "cash-out/other";
                        break;
                    case E_ConstructionRefinanceDataGseRefinancePurposeType.Other:
                        m_dataLoan.sRefPurpose = constructionRefinanceData.GseRefinancePurposeTypeOtherDescription;
                        break;

                }

            }
            else if (m_dataLoan.sLPurposeT == E_sLPurposeT.Construct || m_dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                m_dataLoan.sLotImprovC_rep = constructionRefinanceData.ConstructionImprovementCostsAmount;
                m_dataLoan.sLotVal_rep = constructionRefinanceData.LandEstimatedValueAmount;
                m_dataLoan.sLotOrigC_rep = constructionRefinanceData.PropertyOriginalCostAmount;
                m_dataLoan.sLotAcqYr = constructionRefinanceData.PropertyAcquiredYear;
                m_dataLoan.sLotLien_rep = constructionRefinanceData.PropertyExistingLienAmount;
            }
        }
        private E_sGseRefPurposeT Parse(E_ConstructionRefinanceDataGseRefinancePurposeType type)
        {
            switch (type)
            {
                case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation: return E_sGseRefPurposeT.CashOutDebtConsolidation;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement: return E_sGseRefPurposeT.CashOutHomeImprovement;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited: return E_sGseRefPurposeT.CashOutLimited;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther: return E_sGseRefPurposeT.CashOutOther;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm: return E_sGseRefPurposeT.ChangeInRateTerm;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.Undefined: return E_sGseRefPurposeT.LeaveBlank;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFHAStreamlinedRefinance: return E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutFREOwnedRefinance: return E_sGseRefPurposeT.NoCashOutFREOwnedRefinance;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther: return E_sGseRefPurposeT.NoCashOutOther;
                case E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutStreamlinedRefinance: return E_sGseRefPurposeT.NoCashOutStreamlinedRefinance;
                default:
                    return E_sGseRefPurposeT.LeaveBlank;
            }
        }

        private void Parse(LoanProductData loanProductData)
        {
            //loanProductData.Id;
            Parse(loanProductData.Arm);
            Parse(loanProductData.BuydownList);
            Parse(loanProductData.LoanFeatures);
            Parse(loanProductData.PaymentAdjustmentList);
            Parse(loanProductData.RateAdjustmentList);
            Parse(loanProductData.Heloc);
            Parse(loanProductData.InterestOnly);
            Parse(loanProductData.PrepaymentPenaltyList);
            Parse(loanProductData.InterestCalculationRuleList);
        }
        private void Parse(List<InterestCalculationRule> interestCalculationRuleList)
        {
            foreach (var interestCalculationRule in interestCalculationRuleList)
            {
                //interestCalculationRule.Id;
                //interestCalculationRule.InterestCalculationBasisDaysInPeriodType;
                //interestCalculationRule.InterestCalculationBasisDaysInPeriodTypeOtherDescription;
                //interestCalculationRule.InterestCalculationBasisDaysInYearCount;
                //interestCalculationRule.InterestCalculationBasisType;
                //interestCalculationRule.InterestCalculationBasisTypeOtherDescription;
                //interestCalculationRule.InterestCalculationEffectiveDate;
                //interestCalculationRule.InterestCalculationEffectiveMonthsCount;
                //interestCalculationRule.InterestCalculationExpirationDate;
                //interestCalculationRule.InterestCalculationPeriodAdjustmentIndicator;
                //interestCalculationRule.InterestCalculationPeriodType;
                //interestCalculationRule.InterestCalculationPurposeType;
                //interestCalculationRule.InterestCalculationPurposeTypeOtherDescription;
                //interestCalculationRule.InterestCalculationType;
                //interestCalculationRule.InterestCalculationTypeOtherDescription;
                //interestCalculationRule.InterestInAdvanceIndicator;
                //interestCalculationRule.LoanInterestAccrualStartDate;
            }
        }
        private void Parse(List<PrepaymentPenalty> prepaymentPenaltyList)
        {
            foreach (var prepaymentPenalty in prepaymentPenaltyList)
            {
                //prepaymentPenalty.Id;
                //prepaymentPenalty.PenaltyFixedAmount;
                //prepaymentPenalty.Percent;
                //prepaymentPenalty.PeriodSequenceIdentifier;
                //prepaymentPenalty.TermMonths;
                //prepaymentPenalty.TextDescription;
            }
        }
        private void Parse(InterestOnly interestOnly)
        {
            //interestOnly.Id;
            //interestOnly.MonthlyPaymentAmount;
            //interestOnly.TermMonthsCount;
        }
        private void Parse(Heloc heloc)
        {
            //heloc.Id;
            //heloc.AnnualFeeAmount;
            //heloc.CreditCardAccountIdentifier;
            //heloc.CreditCardIndicator;
            //heloc.DailyPeriodicInterestRateCalculationType;
            //heloc.DailyPeriodicInterestRatePercent;
            //heloc.DrawPeriodMonthsCount;
            //heloc.FirstLienBookNumber;
            //heloc.FirstLienDate;
            //heloc.FirstLienHolderName;
            //heloc.FirstLienIndicator;
            //heloc.FirstLienInstrumentNumber;
            //heloc.FirstLienPageNumber;
            //heloc.FirstLienPrincipalBalanceAmount;
            //heloc.FirstLienRecordedDate;
            //heloc.InitialAdvanceAmount;
            //heloc.MaximumAprRate;
            //heloc.MinimumAdvanceAmount;
            //heloc.MinimumPaymentAmount;
            //heloc.MinimumPaymentPercent;
            //heloc.RepayPeriodMonthsCount;
            //heloc.ReturnedCheckChargeAmount;
            //heloc.StopPaymentChargeAmount;
            //heloc.TeaserTermEndDate;
            //heloc.TeaserTermMonthsCount;
            //heloc.TerminationFeeAmount;
            //heloc.TerminationPeriodMonthsCount;
        }
        private void Parse(List<RateAdjustment> rateAdjustmentList)
        {
            foreach (var rateAdjustment in rateAdjustmentList)
            {
                //rateAdjustment.Id;
                //rateAdjustment.FirstRateAdjustmentDate;
                //rateAdjustment.FirstRateAdjustmentMonths;
                //rateAdjustment.SubsequentRateAdjustmentMonths;
                //rateAdjustment.CalculationType;
                //rateAdjustment.CalculationTypeOtherDescription;
                //rateAdjustment.DurationMonths;
                //rateAdjustment.FirstChangeCapRate;
                //rateAdjustment.FirstChangeFloorPercent;
                //rateAdjustment.FirstChangeFloorRate;
                //rateAdjustment.InitialCapPercent;
                //rateAdjustment.Percent;
                //rateAdjustment.PeriodNumber;
                //rateAdjustment.SubsequentCapPercent;
            }
        }
        private void Parse(List<PaymentAdjustment> paymentAdjustmentList)
        {
            foreach (var paymentAdjustment in paymentAdjustmentList)
            {
                //paymentAdjustment.Id;
                //paymentAdjustment.FirstPaymentAdjustmentDate;
                //paymentAdjustment.FirstPaymentAdjustmentMonths;
                //paymentAdjustment.LastPaymentAdjustmentDate;
                //paymentAdjustment.SubsequentPaymentAdjustmentMonths;
                //paymentAdjustment.Amount;
                //paymentAdjustment.CalculationType;
                //paymentAdjustment.CalculationTypeOtherDescription;
                //paymentAdjustment.DurationMonths;
                //paymentAdjustment.Percent;
                //paymentAdjustment.PeriodNumber;
                //paymentAdjustment.PeriodicCapAmount;
                //paymentAdjustment.PeriodicCapPercent;
            }
        }
        private void Parse(LoanFeatures loanFeatures)
        {
            m_dataLoan.sDue_rep = loanFeatures.BalloonLoanMaturityTermMonths;
            m_dataLoan.sHelocCreditLimit_rep = loanFeatures.HelocMaximumBalanceAmount;
            switch (loanFeatures.LienPriorityType)
            {
                case E_LoanFeaturesLienPriorityType.FirstLien:
                    m_dataLoan.sLienPosT = E_sLienPosT.First;
                    if (loanFeatures.PrepaymentPenaltyIndicator == E_YNIndicator.Y)
                    {
                        m_dataLoan.sProdPpmtPenaltyMon_rep = loanFeatures.PrepaymentPenaltyTermMonths;
                    }
                    break;
                case E_LoanFeaturesLienPriorityType.SecondLien:
                    m_dataLoan.sLienPosT = E_sLienPosT.Second;
                    if (loanFeatures.PrepaymentPenaltyIndicator == E_YNIndicator.Y)
                    {
                        m_dataLoan.sProdPpmtPenaltyMon2ndLien_rep = loanFeatures.PrepaymentPenaltyTermMonths;
                    }
                    break;
                case E_LoanFeaturesLienPriorityType.Undefined:
                    m_dataLoan.sLienPosT = E_sLienPosT.First;
                    break;
                default:
                    throw new UnhandledEnumException(loanFeatures.LienPriorityType);
            }
            m_dataLoan.sPmtAdjMaxBalPc_rep = loanFeatures.NegativeAmortizationLimitPercent;
            switch (loanFeatures.NegativeAmortizationType)
            {
                case E_LoanFeaturesNegativeAmortizationType.NoNegativeAmortization:
                    m_dataLoan.sNegAmortT = E_sNegAmortT.NoNegAmort;
                    break;
                case E_LoanFeaturesNegativeAmortizationType.PotentialNegativeAmortization:
                    m_dataLoan.sNegAmortT = E_sNegAmortT.PotentialNegAmort;
                    break;

                case E_LoanFeaturesNegativeAmortizationType.ScheduledNegativeAmortization:
                    m_dataLoan.sNegAmortT = E_sNegAmortT.ScheduledNegAmort;
                    break;
                case E_LoanFeaturesNegativeAmortizationType.Undefined:
                default:
                    m_dataLoan.sNegAmortT = E_sNegAmortT.LeaveBlank;
                    break;

            }
            m_dataLoan.sBuydown = loanFeatures.BuydownTemporarySubsidyIndicator == E_YNIndicator.Y;
            m_dataLoan.sLpTemplateNm = loanFeatures.ProductDescription;
            m_dataLoan.sFredAffordProgId = DataAccess.Tools.LoanProspectorIdTosFredAffordProdId(loanFeatures.FreOfferingIdentifier);

            //loanFeatures.Id;
            //loanFeatures.AssumabilityIndicator;
            //loanFeatures.BalloonIndicator;
            //loanFeatures.ConditionsToAssumabilityIndicator;
            //loanFeatures.ConformingIndicator;
            //loanFeatures.CounselingConfirmationIndicator;
            //loanFeatures.CounselingConfirmationType;
            //loanFeatures.DemandFeatureIndicator;
            //loanFeatures.DownPaymentOptionType;
            //loanFeatures.DownPaymentOptionTypeOtherDescription;
            m_dataLoan.sProdImpound = !(loanFeatures.EscrowWaiverIndicator == E_YNIndicator.Y);
            //loanFeatures.EstimatedPrepaidDays;
            //loanFeatures.EstimatedPrepaidDaysPaidByOtherTypeDescription;
            //loanFeatures.EstimatedPrepaidDaysPaidByType;
            //loanFeatures.FnmProductPlanIdentifier;
            //loanFeatures.FnmProductPlanIndentifier;
            //loanFeatures.FnmProjectClassificationType;
            //loanFeatures.FnmProjectClassificationTypeOtherDescription;
            //loanFeatures.FreProjectClassificationType;
            //loanFeatures.FreProjectClassificationTypeOtherDescription;
            //loanFeatures.FullPrepaymentPenaltyOptionType;
            //loanFeatures.GseProjectClassificationType;
            m_dataLoan.sGseSpT = Parse(loanFeatures.GsePropertyType);
            if (m_dataLoan.sGseSpT == E_sGseSpT.Condominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium ||
                m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium)
            {
                m_dataLoan.sProdCondoStories_rep = m_sCondoStories;
            }

            //loanFeatures.GraduatedPaymentMultiplierFactor;
            //loanFeatures.GrowingEquityLoanPayoffYearsCount;
            //loanFeatures.HelocInitialAdvanceAmount;
            //loanFeatures.InitialPaymentRatePercent;
            //loanFeatures.InitialPaymentDiscountPercent;
            m_dataLoan.sIOnlyMon_rep = loanFeatures.InterestOnlyTerm;
            //loanFeatures.LenderSelfInsuredIndicator;
            //loanFeatures.LienPriorityTypeOtherDescription;
            //loanFeatures.LoanClosingStatusType;
            m_dataLoan.sProdDocT = Parse(loanFeatures.LoanDocumentationType);
            if (m_dataLoan.sProdDocT == E_sProdDocT.Streamline
                && m_dataLoan.sLPurposeT == E_sLPurposeT.Refin)
            {
                if(m_dataLoan.sLT == E_sLT.FHA)
                    m_dataLoan.sLPurposeT = E_sLPurposeT.FhaStreamlinedRefinance;
                else if(m_dataLoan.sLT == E_sLT.VA)
                    m_dataLoan.sLPurposeT = E_sLPurposeT.VaIrrrl;
            }
            //loanFeatures.LoanDocumentationTypeOtherDescription;
            //loanFeatures.LoanMaturityDate;
            //loanFeatures.LoanOriginalMaturityTermMonths;
            //loanFeatures.LoanRepaymentType;
            //loanFeatures.LoanRepaymentTypeOtherDescription;
            //loanFeatures.LoanScheduledClosingDate;
            //loanFeatures.MiCertificationStatusType;
            //loanFeatures.MiCertificationStatusTypeOtherDescription;
            //loanFeatures.MiCompanyNameType;
            //loanFeatures.MiCompanyNameTypeOtherDescription;
            //loanFeatures.MiCoveragePercent;
            //loanFeatures.NameDocumentsDrawnInType;
            //loanFeatures.NameDocumentsDrawnInTypeOtherDescription;
            //loanFeatures.NegativeAmortizationLimitMonthsCount;
            //loanFeatures.OriginalBalloonTermMonths;
            //loanFeatures.OriginalPrincipalAndInterestPaymentAmount;
            //loanFeatures.PaymentFrequencyType;
            //loanFeatures.PaymentFrequencyTypeOtherDescription;
            //loanFeatures.PrepaymentFinanceChargeRefundableIndicator;
            //loanFeatures.PrepaymentRestrictionIndicator;
            //loanFeatures.ProductIdentifier;
            //loanFeatures.ProductName;
            //loanFeatures.RefundableApplicationFeeIndicator;
            //loanFeatures.RequiredDepositIndicator;
            //loanFeatures.ScheduledFirstPaymentDate;
            //loanFeatures.ServicingTransferStatusType;
            //loanFeatures.TimelyPaymentRateReductionIndicator;
            //loanFeatures.TimelyPaymentRateReductionPercent;
            Parse(loanFeatures.LateCharge);
            Parse(loanFeatures.NotePayTo);
        }
        private E_sGseSpT Parse(E_LoanFeaturesGsePropertyType propertyType)
        {
            switch (propertyType)
            {
                case E_LoanFeaturesGsePropertyType.Attached: return E_sGseSpT.Attached;
                case E_LoanFeaturesGsePropertyType.Condominium: return E_sGseSpT.Condominium;
                case E_LoanFeaturesGsePropertyType.Cooperative: return E_sGseSpT.Cooperative;
                case E_LoanFeaturesGsePropertyType.Detached: return E_sGseSpT.Detached;
                case E_LoanFeaturesGsePropertyType.DetachedCondominium: return E_sGseSpT.DetachedCondominium;
                case E_LoanFeaturesGsePropertyType.HighRiseCondominium: return E_sGseSpT.HighRiseCondominium;
                case E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominium: return E_sGseSpT.ManufacturedHomeCondominium;
                case E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominiumOrPUDOrCooperative: return E_sGseSpT.LeaveBlank;
                case E_LoanFeaturesGsePropertyType.ManufacturedHousing: return E_sGseSpT.ManufacturedHousing;
                case E_LoanFeaturesGsePropertyType.ManufacturedHousingDoubleWide: return E_sGseSpT.ManufacturedHousing;
                case E_LoanFeaturesGsePropertyType.ManufacturedHousingMultiWide: return E_sGseSpT.ManufacturedHomeMultiwide;
                case E_LoanFeaturesGsePropertyType.ManufacturedHousingSingleWide: return E_sGseSpT.ManufacturedHousingSingleWide;
                case E_LoanFeaturesGsePropertyType.Modular: return E_sGseSpT.Modular;
                case E_LoanFeaturesGsePropertyType.PUD: return E_sGseSpT.PUD;
                case E_LoanFeaturesGsePropertyType.Undefined: return E_sGseSpT.LeaveBlank;
                default: return E_sGseSpT.LeaveBlank;
            }
        }
        private E_sProdDocT Parse(E_LoanFeaturesLoanDocumentationType docType)
        {
            switch (docType)
            {
                case E_LoanFeaturesLoanDocumentationType.Alternative: return E_sProdDocT.Alt;
                case E_LoanFeaturesLoanDocumentationType.FullDocumentation: return E_sProdDocT.Full;
                case E_LoanFeaturesLoanDocumentationType.NoDepositVerification: return E_sProdDocT.VINA;
                case E_LoanFeaturesLoanDocumentationType.NoDepositVerificationEmploymentVerificationOrIncomeVerification: return E_sProdDocT.NINANE;
                case E_LoanFeaturesLoanDocumentationType.NoDocumentation: return E_sProdDocT.NINANE;
                case E_LoanFeaturesLoanDocumentationType.NoEmploymentVerificationOrIncomeVerification: return E_sProdDocT.NIVANE;
                case E_LoanFeaturesLoanDocumentationType.NoIncomeNoEmploymentNoAssetsOn1003: return E_sProdDocT.NINANE;
                case E_LoanFeaturesLoanDocumentationType.NoIncomeOn1003: return E_sProdDocT.NIVA;
                case E_LoanFeaturesLoanDocumentationType.NoRatio: return E_sProdDocT.NIVA;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedAssets: return E_sProdDocT.VISA;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncome: return E_sProdDocT.SIVA;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeEmploymentOrAssets: return E_sProdDocT.SISA;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssests: return E_sProdDocT.SISA;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets: return E_sProdDocT.SISA;
                case E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrEmployment: return E_sProdDocT.SIVA;
                case E_LoanFeaturesLoanDocumentationType.OnePaystub: return E_sProdDocT.VINA;
                case E_LoanFeaturesLoanDocumentationType.OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040: return E_sProdDocT.VINA;
                case E_LoanFeaturesLoanDocumentationType.OnePaystubAndVerbalVerificationOfEmployment: return E_sProdDocT.VINA;
                case E_LoanFeaturesLoanDocumentationType.Other: return E_sProdDocT.NINANE;
                case E_LoanFeaturesLoanDocumentationType.Reduced: return E_sProdDocT.NINANE;
                case E_LoanFeaturesLoanDocumentationType.StreamlineRefinance: return E_sProdDocT.Streamline;
                case E_LoanFeaturesLoanDocumentationType.Undefined: return E_sProdDocT.NINANE;
                case E_LoanFeaturesLoanDocumentationType.VerbalVerificationOfEmployment: return E_sProdDocT.Full;
                default: return E_sProdDocT.Full;
            }
        }
        private void Parse(NotePayTo notePayTo)
        {
            //notePayTo.Id;
            //notePayTo.City;
            //notePayTo.Country;
            //notePayTo.PostalCode;
            //notePayTo.State;
            //notePayTo.StreetAddress;
            //notePayTo.StreetAddress2;
            //notePayTo.UnparsedName;
        }
        private void Parse(LateCharge lateCharge)
        {
            //lateCharge.Id;
            //lateCharge.Amount;
            //lateCharge.GracePeriod;
            //lateCharge.LoanPaymentAmount;
            //lateCharge.MaximumAmount;
            //lateCharge.MinimumAmount;
            //lateCharge.Rate;
            //lateCharge.Type;
        }
        private void Parse(List<Buydown> buydownList)
        {
            foreach (var buydown in buydownList)
            {
                //buydown.Id;
                //buydown.BaseDateType;
                //buydown.BaseDateTypeOtherDescription;
                //buydown.ChangeFrequencyMonths;
                //buydown.ContributorType;
                //buydown.ContributorTypeOtherDescription;
                //buydown.DurationMonths;
                //buydown.IncreaseRatePercent;
                //buydown.LenderFundingIndicator;
                //buydown.OriginalBalanceAmount;
                //buydown.PermanentIndicator;
                //buydown.SubsidyCalculationType;
                //buydown.TotalSubsidyAmount;
                Parse(buydown.ContributorList);
                Parse(buydown.SubsidyScheduleList);
            }
        }
        private void Parse(List<SubsidySchedule> subsidyScheduleList)
        {
            foreach (var subsidySchedule in subsidyScheduleList)
            {
                //subsidySchedule.Id;
                //subsidySchedule.AdjustmentPercent;
                //subsidySchedule.PeriodIdentifier;
                //subsidySchedule.PeriodicPaymentEffectiveDate;
                //subsidySchedule.PeriodicPaymentSubsidyAmount;
                //subsidySchedule.PeriodicTerm;
            }
        }
        private void Parse(List<Contributor> contributorList)
        {
            foreach (var contributor in contributorList)
            {
                //contributor.Id;
                //contributor.Amount;
                //contributor.Percent;
                //contributor.RoleType;
                //contributor.RoleTypeOtherDescription;
                //contributor.UnparsedName;
            }
        }
        private void Parse(Arm arm)
        {
            //arm.Id;
            //arm.FnmTreasuryYieldForCurrentIndexDivisorNumber;
            //arm.FnmTreasuryYieldForIndexDivisorNumber;
            //arm.PaymentAdjustmentLifetimeCapAmount;
            //arm.PaymentAdjustmentLifetimeCapPercent;
            //arm.RateAdjustmentLifetimeCapPercent;
            //arm.ConversionOptionIndicator;
            //arm.IndexCurrentValuePercent;
            //arm.IndexMarginPercent;
            //arm.IndexType;
            //arm.IndexTypeOtherDescription;
            //arm.InterestRateRoundingFactor;
            //arm.InterestRateRoundingType;
            //arm.LifetimeCapRate;
            //arm.LifetimeFloorPercent;
            //arm.QualifyingRatePercent;
            Parse(arm.ConversionOption);
        }
        private void Parse(ConversionOption conversionOption)
        {
            //conversionOption.Id;
            //conversionOption.ConversionOptionPeriodFeePercent;
            //conversionOption.EndingChangeDatePeriodDescription;
            //conversionOption.FeeAmount;
            //conversionOption.NoteTermGreaterThanFifteenYearsAdditionalPercent;
            //conversionOption.NoteTermLessThanFifteenYearsAdditionalPercent;
            //conversionOption.PeriodEndDate;
            //conversionOption.PeriodStartDate;
            //conversionOption.StartingChangeDatePeriodDescription;
        }
        private void Parse(List<Liability> liabilityList)
        {
            if (null == liabilityList)
            {
                return;
            }
            foreach (var liability in liabilityList)
            {
                string[] id = liability.BorrowerId.Split(' '); // 7/29/2009 dd - Borrower Id can be in a joint format i.e: BOR1 COB1. For joint just get the first id.
                CAppData dataApp = m_dataLoan.FindDataAppBySsn(m_borrowerId_Ssn_Hash[id[0]]);
                if (null != dataApp)
                {
                    Parse(liability, dataApp.aLiaCollection, id.Length > 1, dataApp.BorrowerModeT == E_BorrowerModeT.Borrower);
                }
            }
        }
        private void Parse(Liability liability, ILiaCollection aLiaCollection, bool bIsJoint, bool bIsBorrower)
        {
            if (liability.Type == E_LiabilityType.JobRelatedExpenses)
            {
                var jobExpense = aLiaCollection.GetJobRelated1(true);
                if (jobExpense.Pmt_rep != "" && jobExpense.Pmt_rep != "0.00")
                {
                    jobExpense = aLiaCollection.GetJobRelated2(true);
                }
                jobExpense.ExpenseDesc = liability.HolderName;
                jobExpense.Pmt_rep = liability.MonthlyPaymentAmount;
                jobExpense.OwnerT = LiabilityOwnerType(bIsJoint, bIsBorrower);
                jobExpense.Update();
            }
            else if (liability.Type == E_LiabilityType.Alimony)
            {
                var alimony = aLiaCollection.GetAlimony(true);
                alimony.RemainMons_rep = liability.RemainingTermMonths;
                alimony.Pmt_rep = liability.MonthlyPaymentAmount;
                alimony.OwedTo = liability.AlimonyOwedToName;
                alimony.NotUsedInRatio = liability.ExclusionIndicator == E_YNIndicator.Y ;
                alimony.OwnerT = LiabilityOwnerType(bIsJoint, bIsBorrower);
                alimony.Update();
            }
            else if (liability.Type == E_LiabilityType.ChildSupport)
            {
                var childSupport = aLiaCollection.GetChildSupport(true);
                childSupport.RemainMons_rep = liability.RemainingTermMonths;
                childSupport.Pmt_rep = liability.MonthlyPaymentAmount;
                childSupport.NotUsedInRatio = liability.ExclusionIndicator == E_YNIndicator.Y;
                childSupport.OwnerT = LiabilityOwnerType(bIsJoint, bIsBorrower);
                childSupport.Update();
            }
            else
            {
                var liaRegular = aLiaCollection.AddRegularRecord();
                liaRegular.ComAddr = liability.HolderStreetAddress;
                liaRegular.ComCity = liability.HolderCity;
                liaRegular.ComState = liability.HolderState;
                liaRegular.ComZip = liability.HolderPostalCode;
                liaRegular.AccNum = liability.AccountIdentifier;
                liaRegular.ComNm = liability.HolderName;
                liaRegular.Pmt_rep = liability.MonthlyPaymentAmount;
                liaRegular.WillBePdOff = liability.PayoffStatusIndicator == E_YNIndicator.Y;
                liaRegular.RemainMons_rep = liability.RemainingTermMonths;
                liaRegular.Bal_rep = liability.UnpaidBalanceAmount;
                liaRegular.NotUsedInRatio = ExcludeFromRatio(liability.PayoffStatusIndicator, liability.ExclusionIndicator); //iOPM 76028
                liaRegular.DebtT = Parse(liability.Type);
                liaRegular.OwnerT = LiabilityOwnerType(bIsJoint, bIsBorrower);
                liaRegular.Update();

                m_liability_Hash.Add(liability.Id, liaRegular);
            }
        }
        private bool ExcludeFromRatio(E_YNIndicator ynPaidOff, E_YNIndicator ynExclude)
        {
            return ynPaidOff == E_YNIndicator.Y || ynExclude == E_YNIndicator.Y;
        }
        private E_LiaOwnerT LiabilityOwnerType(bool bIsJoint, bool bIsBorrower)
        {
            E_LiaOwnerT liaOwner = E_LiaOwnerT.Borrower;
            if (bIsJoint)
            {
                liaOwner = E_LiaOwnerT.Joint;
            }
            else if(!bIsBorrower)
            {
                liaOwner = E_LiaOwnerT.CoBorrower;
            }
            return liaOwner;
        }
        private E_DebtRegularT Parse(E_LiabilityType type)
        {
            switch (type)
            {
                case E_LiabilityType.Installment:
                    return E_DebtRegularT.Installment;
                case E_LiabilityType.MortgageLoan:
                    return E_DebtRegularT.Mortgage;
                case E_LiabilityType.Revolving:
                    return E_DebtRegularT.Revolving;
                case E_LiabilityType.Open30DayChargeAccount:
                    return E_DebtRegularT.Open;
                default:
                    return E_DebtRegularT.Other;
            }
        }
        private void Parse(InterviewerInformation interviewerInformation)
        {
            //interviewerInformation.Id;
            //interviewerInformation.ApplicationTakenMethodType;
            //interviewerInformation.InterviewerApplicationSignedDate;
            //interviewerInformation.InterviewersEmployerCity;
            //interviewerInformation.InterviewersEmployerName;
            //interviewerInformation.InterviewersEmployerPostalCode;
            //interviewerInformation.InterviewersEmployerState;
            //interviewerInformation.InterviewersEmployerStreetAddress;
            //interviewerInformation.InterviewersEmployerStreetAddress2;
            //interviewerInformation.InterviewersName;
            //interviewerInformation.InterviewersTelephoneNumber;
        }
        private void Parse(GovernmentReporting governmentReporting)
        {
            //governmentReporting.Id;
            //governmentReporting.HmdaPreapprovalType;
            //governmentReporting.HmdaPurposeOfLoanType;
            //governmentReporting.HmdaRateSpreadPercent;
            //governmentReporting.HmdaHoepaLoanStatusIndicator;
        }
        private void Parse(GovernmentLoan governmentLoan)
        {
            //governmentLoan.Id;
            if (m_dataLoan.sLT == E_sLT.FHA)
                Parse(governmentLoan.FhaLoan);
            
            Parse(governmentLoan.FhaVaLoan);

            if (m_dataLoan.sLT == E_sLT.VA)
                Parse(governmentLoan.VaLoan);
        }
        private void Parse(VaLoan vaLoan)
        {
            //vaLoan.Id;
            m_dataLoan.sFfUfmipR_rep = vaLoan.BorrowerFundingFeePercent;
            //vaLoan.VaBorrowerCoBorrowerMarriedIndicator;
            //vaLoan.VaEntitlementAmount;
            //vaLoan.VaEntitlementCodeIdentifier;
            //vaLoan.VaHouseholdSizeCount;
            //vaLoan.VaMaintenanceExpenseMonthlyAmount;
            //vaLoan.VaResidualIncomeAmount;
            //vaLoan.VaUtilityExpenseMonthlyAmount;
        }
        private void Parse(FhaVaLoan fhaVaLoan)
        {
            //fhaVaLoan.Id;
            //fhaVaLoan.BorrowerPaidFhaVaClosingCostsAmount;
            //fhaVaLoan.BorrowerPaidFhaVaClosingCostsPercent;
            //fhaVaLoan.GovernmentMortgageCreditCertificateAmount;
            //fhaVaLoan.GovernmentRefinanceType;
            //fhaVaLoan.GovernmentRefinanceTypeOtherDescription;
            //fhaVaLoan.OtherPartyPaidFhaVaClosingCostsAmount;
            //fhaVaLoan.OtherPartyPaidFhaVaClosingCostsPercent;
            //fhaVaLoan.PropertyEnergyEfficientHomeIndicator;
            //fhaVaLoan.SellerPaidFhaVaClosingCostsPercent;
            //fhaVaLoan.OriginatorIdentifier;
        }
        private void Parse(FhaLoan fhaLoan)
        {
            //fhaLoan.Id;
            //fhaLoan.BorrowerFinancedFhaDiscountPointsAmount;
            //fhaLoan.BorrowerHomeInspectionChosenIndicator;
            //fhaLoan.DaysToFhaMiEligibilityCount;
            //fhaLoan.FhaAlimonyLiabilityTreatmentType;
            //fhaLoan.FhaCoverageRenewalRatePercent;
            //fhaLoan.FhaEnergyRelatedRepairsOrImprovementsAmount;
            //fhaLoan.FhaGeneralServicesAdministrationCodeIdentifier;
            //fhaLoan.FhaGeneralServicesAdminstrationCodeIdentifier;
            //fhaLoan.FhaLimitedDenialParticipationIdentifier;
            //fhaLoan.FhaNonOwnerOccupancyRiderRule248Indicator;
            //fhaLoan.FhaRefinanceInterestOnExistingLienAmount;
            //fhaLoan.FhaRefinanceOriginalExistingFhaCaseIdentifier;
            //fhaLoan.FhaRefinanceOriginalExistingUpFrontMipAmount;
            //fhaLoan.FhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier;
            m_dataLoan.sFfUfmipR_rep = fhaLoan.FhaUpfrontMiPremiumPercent;
            //fhaLoan.FhaMiPremiumRefundAmount;
            //fhaLoan.HudAdequateAvailableAssetsIndicator;
            //fhaLoan.HudAdequateEffectiveIncomeIndicator;
            //fhaLoan.HudCreditCharacteristicsIndicator;
            //fhaLoan.HudStableEffectiveIncomeIndicator;
            Parse(fhaLoan.SectionOfActType);
            //fhaLoan.SectionOfActTypeOtherDescription;
            //fhaLoan.SoldUnderHudSingleFamilyPropertyDispositionProgramIndicator;
            m_dataLoan.sFHALenderIdCode = fhaLoan.LenderIdentifier;
            m_dataLoan.sFHASponsorAgentIdCode = fhaLoan.SponsorIdentifier;
        }
        private void Parse(E_FhaLoanSectionOfActType SOAtype)
        {
            //http://www.hud.gov/offices/hsg/sfh/f17c/f17adpcurrent_xmlhlp.cfm
            //https://www.ginniemae.gov/edi/adpcodes.asp?Section=Issuers
            //Note: the SOA enum does not include section 257, E_sTotalScoreFhaProductT.HOPEForHomeowners
            E_sTotalScoreFhaProductT type = E_sTotalScoreFhaProductT.LeaveBlank;
            switch (SOAtype)
            { 
                case E_FhaLoanSectionOfActType._203B:
                case E_FhaLoanSectionOfActType._203B2:
                case E_FhaLoanSectionOfActType._203B241:
                case E_FhaLoanSectionOfActType._203B251:
                case E_FhaLoanSectionOfActType._234C:
                case E_FhaLoanSectionOfActType._234C251:
                case E_FhaLoanSectionOfActType._251:
                    type = E_sTotalScoreFhaProductT.Standard;
                    break;
                case E_FhaLoanSectionOfActType._203K:
                case E_FhaLoanSectionOfActType._203K241:
                case E_FhaLoanSectionOfActType._203K251:
                    type = E_sTotalScoreFhaProductT.Rehabilitation;
                    break;
                case E_FhaLoanSectionOfActType._203H:
                    type = E_sTotalScoreFhaProductT.DisasterVictims;
                    break;
                case E_FhaLoanSectionOfActType._184:
                case E_FhaLoanSectionOfActType._201S:
                case E_FhaLoanSectionOfActType._201SD:
                case E_FhaLoanSectionOfActType._201U:
                case E_FhaLoanSectionOfActType._201UD:
                case E_FhaLoanSectionOfActType._203I:
                case E_FhaLoanSectionOfActType._213:
                case E_FhaLoanSectionOfActType._220:
                case E_FhaLoanSectionOfActType._221:
                case E_FhaLoanSectionOfActType._221D2:
                case E_FhaLoanSectionOfActType._221D2251:
                case E_FhaLoanSectionOfActType._222:
                case E_FhaLoanSectionOfActType._223E:
                case E_FhaLoanSectionOfActType._233:
                case E_FhaLoanSectionOfActType._235:
                case E_FhaLoanSectionOfActType._237:
                case E_FhaLoanSectionOfActType._240:
                case E_FhaLoanSectionOfActType._245:
                case E_FhaLoanSectionOfActType._247:
                case E_FhaLoanSectionOfActType._248:
                case E_FhaLoanSectionOfActType._255:
                case E_FhaLoanSectionOfActType._26101:
                case E_FhaLoanSectionOfActType._26102:
                case E_FhaLoanSectionOfActType._26201:
                case E_FhaLoanSectionOfActType._26202:
                case E_FhaLoanSectionOfActType._265:
                case E_FhaLoanSectionOfActType._27001:
                case E_FhaLoanSectionOfActType._27002:
                case E_FhaLoanSectionOfActType._27003:
                case E_FhaLoanSectionOfActType._27004:
                case E_FhaLoanSectionOfActType._27005:
                case E_FhaLoanSectionOfActType._27101:
                case E_FhaLoanSectionOfActType._27102:
                case E_FhaLoanSectionOfActType._27103:
                case E_FhaLoanSectionOfActType._27104:
                case E_FhaLoanSectionOfActType._27105:
                case E_FhaLoanSectionOfActType._3703:
                case E_FhaLoanSectionOfActType._3703D:
                case E_FhaLoanSectionOfActType._3703D2:
                case E_FhaLoanSectionOfActType._3710:
                case E_FhaLoanSectionOfActType._3711:
                case E_FhaLoanSectionOfActType._502:
                case E_FhaLoanSectionOfActType._729:
                case E_FhaLoanSectionOfActType.Other:
                    type = E_sTotalScoreFhaProductT.Other;
                    break;
                case E_FhaLoanSectionOfActType.Undefined:
                default:
                    type = E_sTotalScoreFhaProductT.LeaveBlank;
                    break;
            }
            m_dataLoan.sTotalScoreFhaProductT = type;

            if(type != E_sTotalScoreFhaProductT.LeaveBlank)
                m_dataLoan.sFHAHousingActSection = SOAtype.ToString().Replace("_", "");
        }
        private void Parse(EscrowAccountSummary escrowAccountSummary)
        {
            //escrowAccountSummary.EscrowAggregateAccountingAdjustmentAmount;
            //escrowAccountSummary.EscrowCushionNumberOfMonthsCount;
        }
        private void Parse(List<Escrow> escrowList)
        {
            if (escrowList == null)
            {
                return;
            }
            foreach (var escrow in escrowList)
            {
                //escrow.Id;
                //escrow.AnnualPaymentAmount;
                //escrow.CollectedNumberOfMonthsCount;
                //escrow.DueDate;
                //escrow.InsurancePolicyIdentifier;
                //escrow.ItemType;
                //escrow.ItemTypeOtherDescription;
                //escrow.MonthlyPaymentAmount;
                //escrow.MonthlyPaymentRoundingType;
                //escrow.PaidByType;
                //escrow.PaymentFrequencyType;
                //escrow.PaymentFrequencyTypeOtherDescription;
                //escrow.PremiumAmount;
                //escrow.PremiumDurationMonthsCount;
                //escrow.PremiumPaidByType;
                //escrow.PremiumPaymentType;
                //escrow.SpecifiedHud1LineNumber;
                Parse(escrow.AccountSummary);
                Parse(escrow.PaymentsList);
                Parse(escrow.PaidTo);
            }
        }
        private void Parse(List<Payments> paymentsList)
        {
            foreach (var payments in paymentsList)
            {
                //payments.Id;
                //payments.DueDate;
                //payments.PaymentAmount;
                //payments.SequenceIdentifier;
            }
        }
        private void Parse(AccountSummary accountSummary)
        {
            //accountSummary.Id;
            //accountSummary.EscrowAggregateAccountingAdjustmentAmount;
            //accountSummary.EscrowCushionNumberOfMonthsCount;
        }
        private void Parse(List<DownPayment> downPaymentList)
        {
            if (E_sLPurposeT.Purchase != m_dataLoan.sLPurposeT)
            {
                return;
            }
            // SK - use max - this LINQ method has disadvantage of evaluating ToMoney twice per element, 
            // but that shouldn't be a problem. Cleaner than the alternative without LINQ.
            var maxDownPayment = downPaymentList.Aggregate(
                (agg, next) =>
                    (m_dataLoan.m_convertLos.ToMoney(next.Amount) > m_dataLoan.m_convertLos.ToMoney(agg.Amount) 
                    ? next : agg
                ));


            m_dataLoan.sEquityCalc_rep = maxDownPayment.Amount;
            m_dataLoan.sDwnPmtSrcExplain = maxDownPayment.SourceDescription;
            m_dataLoan.sDwnPmtSrc = Parse(maxDownPayment.Type);
        }
        private string Parse(E_DownPaymentType type)
        {
            switch (type)
            {
                case E_DownPaymentType.CheckingSavings:
                    return "checking/savings";
                case E_DownPaymentType.GiftFunds:
                    return "gift funds";
                case E_DownPaymentType.StocksAndBonds:
                    return "stocks & bonds";
                case E_DownPaymentType.LotEquity:
                    return "lot equity";
                case E_DownPaymentType.BridgeLoan:
                    return "bridge loan";
                case E_DownPaymentType.TrustFunds:
                    return "trust funds";
                case E_DownPaymentType.RetirementFunds:
                    return "retirement funds";
                case E_DownPaymentType.LifeInsuranceCashValue:
                    return "life insurance cash value";
                case E_DownPaymentType.SaleOfChattel:
                    return "sale of chattel";
                case E_DownPaymentType.TradeEquity:
                    return "trade equity";
                case E_DownPaymentType.SweatEquity:
                    return "sweat equity";
                case E_DownPaymentType.CashOnHand:
                    return "cash on hand";
                case E_DownPaymentType.DepositOnSalesContract:
                    return "deposit on sales contract";
                case E_DownPaymentType.EquityOnPendingSale:
                    return "equity from pending sale";
                case E_DownPaymentType.EquityOnSubjectProperty:
                    return "equity from subject property";
                case E_DownPaymentType.EquityOnSoldProperty:
                    return "equity on sold property";
                case E_DownPaymentType.OtherTypeOfDownPayment:
                    return "other type of down payment";
                case E_DownPaymentType.RentWithOptionToPurchase:
                    return "rent with option to purchase";
                case E_DownPaymentType.SecuredBorrowedFunds:
                    return "secured borrowed funds";
                case E_DownPaymentType.UnsecuredBorrowedFunds:
                    return "unsecured borrowed funds";
                case E_DownPaymentType.CashOrOtherEquity:
                    return "cash or other equity";
                case E_DownPaymentType.Contribution:
                    return "contribution";
                case E_DownPaymentType.CreditCard:
                    return "credit card";
                case E_DownPaymentType.ForgivableSecuredLoan:
                    return "forgivable secured loan";
                case E_DownPaymentType.HousingRelocation:
                    return "housing relocation";
                case E_DownPaymentType.MortgageCreditCertificates:
                    return "mortgage credit certificates";
                case E_DownPaymentType.PledgedCollateral:
                    return "pledged collateral";
                case E_DownPaymentType.PremiumFunds:
                    return "premium funds";
                case E_DownPaymentType.SalesPriceAdjustment:
                    return "sales price adjustment";
                case E_DownPaymentType.SecondaryFinancing:
                    return "secondary financing";
                case E_DownPaymentType.FHAGiftSourceEmployer:
                    return "fha - gift - source employer";
                case E_DownPaymentType.FHAGiftSourceGovernmentAssistance:
                    return "fha - gift - source government assistance";
                case E_DownPaymentType.FHAGiftSourceNA:
                    return "fha – gift - source n/a";
                case E_DownPaymentType.FHAGiftSourceNonprofitReligiousCommunityNonSellerFunded:
                    return "fha - gift - source nonprofit/religious/community - non-seller funded";
                case E_DownPaymentType.FHAGiftSourceNonprofitReligiousCommunitySellerFunded:
                    return "fha - gift - source nonprofit/religious/community - seller funded";
                case E_DownPaymentType.FHAGiftSourceRelative:
                    return "fha - gift - source relative";
                case E_DownPaymentType.Undefined:
                default:
                    return string.Empty;
            }
        }
        private void Parse(List<Asset> assetList, List<DownPayment> downpayments)
        {
            if (assetList == null)
            {
                return;
            }
            var parsedAppSSNs = new HashSet<string>(); // ssns of the borrowers whose applications are "parsed."
            foreach (var asset in assetList)
            {
                string[] id = asset.BorrowerId.Split(' '); // 7/29/2009 dd - Borrower Id can be in a joint format i.e: BOR1 COB1. For joint just get the first id.

                CAppData dataApp = m_dataLoan.FindDataAppBySsn(m_borrowerId_Ssn_Hash[id[0]]);
                if (dataApp != null)
                {
                    Parse(asset, dataApp.aAssetCollection);
                    parsedAppSSNs.Add(m_borrowerId_Ssn_Hash[id[0]]);
                }                
            }

            SetGiftSourceTypeOfFirstBlankGiftAsset(downpayments, parsedAppSSNs);            
        }
        /// <summary>
        /// Set the gift source of the first asset with source type "gift funds" and a blank gift source
        /// to be the associated source of the only gift downpayment. (if there is 0 or more than one gift downpayment, do nothing.)
        /// as per spec opm 90538 https://opm/default.asp?90538#BugEvent.701229
        /// </summary>
        /// <param name="downpayments"></param>
        /// <param name="parsedAppSSNs">ssns of the borrowers whose applications are "parsed."</param>
        private void SetGiftSourceTypeOfFirstBlankGiftAsset(List<DownPayment> downpayments, HashSet<string> parsedAppSSNs)
        {
            // 9/4/2012 SK opm 90538 
            DownPayment downPaymentGift = null;
            bool foundExactlyOneGifts = false;
            foreach (var downpayment in downpayments)
            {
                if (IsGiftDownpaymentType(downpayment.Type))
                {
                    if (foundExactlyOneGifts)
                    {
                        foundExactlyOneGifts = false;
                        break;
                    }
                    downPaymentGift = downpayment;
                    foundExactlyOneGifts = true;
                }
            }
            if (foundExactlyOneGifts)
            {
                IAssetRegular maxRegAsset = null;

                foreach (var appID in parsedAppSSNs)
                {
                    CAppData dataApp = m_dataLoan.FindDataAppBySsn(appID);
                    var regAssets = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.GiftFunds);
                    foreach (var regAssetXmlRecord in regAssets)
                    {
                        var regAsset = (IAssetRegular)regAssetXmlRecord;
                        //if (regAsset.GiftSource == E_GiftFundSourceT.Blank) // clobbering.
                        //{
                            if (maxRegAsset == null)
                            {
                                maxRegAsset = regAsset;
                            }
                            else
                            {
                                if (maxRegAsset.Val < regAsset.Val)
                                {
                                    maxRegAsset = regAsset;
                                }
                            }
                        //}
                    }
                }
                if (maxRegAsset != null)
                {
                    maxRegAsset.GiftSource = ConvertToGiftSource(downPaymentGift.Type);
                    maxRegAsset.Update();
                    return;
                }
            }
        }
        private void Parse(Asset asset, IAssetCollection aAssetCollection)
        {
            if (asset.Type == E_AssetType.NetWorthOfBusinessOwned)
            {
                var assetBusiness = aAssetCollection.GetBusinessWorth(true);
                assetBusiness.Val_rep = asset.CashOrMarketValueAmount;
                assetBusiness.Update();
            }
            else if (asset.Type == E_AssetType.EarnestMoneyCashDepositTowardPurchase)
            {
                var cashDeposit = aAssetCollection.GetCashDeposit1(true);
                if (cashDeposit.Val_rep != "" && cashDeposit.Val_rep != "0.00")
                {
                    // First entry already populate, use the second field.
                    cashDeposit = aAssetCollection.GetCashDeposit2(true);
                }
                cashDeposit.Val_rep = asset.CashOrMarketValueAmount;
                cashDeposit.Desc = asset.HolderName;
                cashDeposit.Update();
            }
            else if (asset.Type == E_AssetType.RetirementFund)
            {
                var assetRetirement = aAssetCollection.GetRetirement(true);
                assetRetirement.Val_rep = asset.CashOrMarketValueAmount;
                assetRetirement.Update();
            }
            else if (asset.Type == E_AssetType.LifeInsurance)
            {
                var assetLifeIns = aAssetCollection.GetLifeInsurance(true);
                assetLifeIns.Val_rep = asset.CashOrMarketValueAmount;
                assetLifeIns.FaceVal_rep = asset.LifeInsuranceFaceValueAmount;
                assetLifeIns.Update();
            }
            else
            {
                var assetRegular = aAssetCollection.AddRegularRecord();
                assetRegular.AccNum = asset.AccountIdentifier;
                assetRegular.Val_rep = asset.CashOrMarketValueAmount;
                assetRegular.ComNm = asset.HolderName;
                assetRegular.StAddr = asset.HolderStreetAddress;
                assetRegular.City = asset.HolderCity;
                assetRegular.State = asset.HolderState;
                assetRegular.Zip = asset.HolderPostalCode;
                assetRegular.OtherTypeDesc = asset.OtherAssetTypeDescription;
                assetRegular.AssetT = Parse(asset.Type);
                if (asset.Type == E_AssetType.Automobile)
                {
                    assetRegular.Desc = asset.AutomobileMakeDescription;
                }
                assetRegular.Update();
            }

        }
        private bool IsGiftDownpaymentType(E_DownPaymentType e_DownPaymentType)
        {
            switch (e_DownPaymentType)
            {
                case E_DownPaymentType.FHAGiftSourceEmployer:
                case E_DownPaymentType.FHAGiftSourceGovernmentAssistance:
                case E_DownPaymentType.FHAGiftSourceNA:
                case E_DownPaymentType.FHAGiftSourceNonprofitReligiousCommunityNonSellerFunded:
                case E_DownPaymentType.FHAGiftSourceNonprofitReligiousCommunitySellerFunded:
                case E_DownPaymentType.FHAGiftSourceRelative:
                    return true;
                default:
                    return false;
            }
        }

        private E_GiftFundSourceT ConvertToGiftSource(E_DownPaymentType e_DownPaymentType)
        {
            switch (e_DownPaymentType)
            {
                case E_DownPaymentType.FHAGiftSourceEmployer:
                    return E_GiftFundSourceT.Employer;
                case E_DownPaymentType.FHAGiftSourceGovernmentAssistance:
                    return E_GiftFundSourceT.Government;
                case E_DownPaymentType.FHAGiftSourceNA:
                    return E_GiftFundSourceT.Other;
                case E_DownPaymentType.FHAGiftSourceNonprofitReligiousCommunityNonSellerFunded:
                case E_DownPaymentType.FHAGiftSourceNonprofitReligiousCommunitySellerFunded:
                    return E_GiftFundSourceT.Nonprofit;
                case E_DownPaymentType.FHAGiftSourceRelative:
                    return E_GiftFundSourceT.Relative;
                default:
                    return E_GiftFundSourceT.Blank;
            }
        }
        private E_AssetRegularT Parse(E_AssetType type)
        {
            switch (type)
            {
                case E_AssetType.Automobile: return E_AssetRegularT.Auto;
                case E_AssetType.Bond: return E_AssetRegularT.Bonds;
                case E_AssetType.CheckingAccount: return E_AssetRegularT.Checking;
                case E_AssetType.GiftsTotal: return E_AssetRegularT.GiftFunds;
                case E_AssetType.OtherNonLiquidAssets: return E_AssetRegularT.OtherIlliquidAsset;
                case E_AssetType.OtherLiquidAssets: return E_AssetRegularT.OtherLiquidAsset;
                case E_AssetType.SavingsAccount: return E_AssetRegularT.Savings;
                case E_AssetType.Stock: return E_AssetRegularT.Stocks;
                case E_AssetType.PendingNetSaleProceedsFromRealEstateAssets: return E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets;
                default:
                    return E_AssetRegularT.OtherLiquidAsset;
            }
        }
        private void Parse(AffordableLending affordableLending)
        {
            //affordableLending.Id;
            //affordableLending.FnmCommunityLendingProductName;
            //affordableLending.FnmCommunityLendingProductType;
            //affordableLending.FnmCommunityLendingProductTypeOtherDescription;
            //affordableLending.FnmCommunitySecondsIndicator;
            //affordableLending.FnmNeighborsMortgageEligibilityIndicator;
            //affordableLending.FreAffordableProgramIdentifier;
            //affordableLending.HudIncomeLimitAdjustmentFactor;
            //affordableLending.HudLendingIncomeLimitAmount;
            //affordableLending.HudMedianIncomeAmount;
            //affordableLending.MsaIdentifier;
        }
        private void Parse(AdditionalCaseData additionalCaseData)
        {
            //additionalCaseData.Id;
            Parse(additionalCaseData.MortgageScoreList);
            Parse(additionalCaseData.TransmittalData);
        }
        private void Parse(TransmittalData transmittalData)
        {
            //transmittalData.Id;
            //transmittalData.ArmsLengthIndicator;
            //transmittalData.BelowMarketSubordinateFinancingIndicator;
            //transmittalData.BuydownRatePercent;
            //transmittalData.CaseStateType;
            //transmittalData.CaseStateTypeOtherDescription;
            //transmittalData.CommitmentReferenceIdentifier;
            //transmittalData.ConcurrentOriginationIndicator;
            //transmittalData.ConcurrentOriginationLenderIndicator;
            //transmittalData.CreditReportAuthorizationIndicator;
            //transmittalData.CurrentFirstMortgageHolderType;
            //transmittalData.CurrentFirstMortgageHolderTypeOtherDescription;
            //transmittalData.InvestorInstitutionIdentifier;
            //transmittalData.InvestorLoanIdentifier;
            //transmittalData.LenderBranchIdentifier;
            //transmittalData.LenderRegistrationIdentifier;
            if (IsSyncLoanOriginationSystemLoanIdentifierWithLoanNumber)
            {
                // 6/14/2012 dd - For REMN (PML0200) we want to sync Encompass loan number with our loan number.
                if (string.IsNullOrEmpty(transmittalData.LoanOriginationSystemLoanIdentifier) == false)
                {
                    if (m_dataLoan.sOldLNm != m_dataLoan.sLNm)
                    {
                        m_dataLoan.sOldLNm = m_dataLoan.sLNm; //however they want to release back to broker save this back.
                    }
                    m_dataLoan.SetsLNmWithPermissionBypass(transmittalData.LoanOriginationSystemLoanIdentifier);
                }
            }
            //transmittalData.LoanOriginatorType;
            //transmittalData.LoanOriginatorTypeOtherDescription;
            //transmittalData.PropertiesFinancedByLenderCount;
            m_dataLoan.sApprVal_rep = transmittalData.PropertyAppraisedValueAmount;
            //transmittalData.PropertyEstimatedValueAmount;
            m_dataLoan.sProdRLckdDays_rep = transmittalData.RateLockPeriodDays;
            //transmittalData.RateLockRequestedExtensionDays;
            //transmittalData.RateLockType;
        }
        private void Parse(List<MortgageScore> mortgageScoreList)
        {
            foreach (var mortgageScore in mortgageScoreList)
            {
                //mortgageScore.Id;
                //mortgageScore.Date;
                //mortgageScore.Type;
                //mortgageScore.TypeOtherDescription;
                //mortgageScore.Value;
            }
        }
        private void Parse(DataInformation dataInformation)
        {
            //dataInformation.Id;
            Parse(dataInformation.DataVersionList);
        }

    }
}
