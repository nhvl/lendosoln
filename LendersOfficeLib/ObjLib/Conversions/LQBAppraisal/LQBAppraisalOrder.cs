﻿namespace LendersOffice.ObjLib.Conversions.LQBAppraisal
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using System.Xml.XPath;

    using DataAccess;
    using EDocs;
    using LendersOffice.Common;
    using LendersOffice.Integration.Appraisals;
    using LendersOffice.ObjLib.Appraisal;
    using LendersOffice.Security;
    using global::LQBAppraisal.AppraisalStatusUpdate;
    using global::LQBAppraisal.LQBAppraisalRequest;

    public class LQBAppraisalOrder : AppraisalOrderBase
    {
        private static LosConvert convert = new LosConvert();
        private static DateTime INVALID_DATE = new DateTime(1901, 1, 1);

        public Guid VendorId = Guid.Empty;
        public string Status = string.Empty;

        private DateTime m_statusDate = INVALID_DATE;
        private AppraisalOrder m_appraisalOrder = null;
        private OrderDataUpdate m_orderDataUpdate = null;
        private List<AppraisalDocument> m_uploadedFiles = null;
        private List<OrderStatusHistoryItem> m_orderHistory = null;
        private List<Guid> m_embeddedDocIds = null;
        private string m_orderNumber = string.Empty;

        public class AppraisalDocument
        {
            internal UploadedDocument Document;
            //"_" is an invalid character in Base64, so this is guaranteed to never be in a real Base64 string.
            private const string FileDBPrefix = "APPRAISALDOCUMENT_FILEDBKEY_";
            public AppraisalDocument(XElement xe, XmlSerializer docSer)
            {
                using (var xr = xe.CreateReader())
                {

                    SetDoc((UploadedDocument)docSer.Deserialize(xr));
                }
            }

            public AppraisalDocument(UploadedDocument doc)
            {
                SetDoc(doc);
            }

            private void SetDoc(UploadedDocument doc)
            {
                Document = doc;
                StoreDocumentToFileDB(doc);
            }


            public string FileDBKey
            {
                get
                {
                    return Document.Content;
                }
            }

            public string DocumentName
            {
                get
                {
                    return Document.DocumentName;
                }
            }

            public Guid? EdocId
            {
                get
                {
                    return Document.EDocId;
                }
            }

            public E_UploadedDocumentDocumentFormat DocumentFormat
            {
                get
                {
                    return Document.DocumentFormat;
                }
            }

            private void StoreDocumentToFileDB(UploadedDocument doc)
            {
                //Don't need to do anything if it's already been processed, or there isn't anything here
                if (string.IsNullOrEmpty(doc.Content.TrimWhitespaceAndBOM())) return;
                if (doc.Content.StartsWith(FileDBPrefix, StringComparison.CurrentCultureIgnoreCase)) return;

                //Otherwise save the actual file to FileDB, then replace the content with the FileDB key.
                Guid docKeyId = Guid.NewGuid();
                string file = Utilities.Base64StringToFile(doc.Content, "_Appraisal_" + docKeyId);
                string docFileDBKey = FileDBPrefix + docKeyId;

                FileDBTools.WriteFile(E_FileDB.Normal, docFileDBKey, file);
                doc.Content = docFileDBKey;
            }

            public void UseFile(Action<FileInfo> toDo)
            {
                FileDBTools.UseFile(E_FileDB.Normal, Document.Content, toDo);
            }

            public T UseFile<T>(Func<FileInfo, T> toDo)
            {
                return FileDBTools.UseFile<T>(E_FileDB.Normal, Document.Content, toDo);
            }

            /// <summary>
            /// Sets the Edoc id.
            /// </summary>
            /// <param name="edocId">The edoc id.</param>
            public void SetEdocId(Guid? edocId)
            {
                if (this.Document != null)
                {
                    this.Document.EDocId = edocId;
                }
            }

            internal static List<AppraisalDocument> DeserializeList(string xml)
            {
                //For some reason, deserializing with the XmlSerializer made with 
                //TypeOf(List<UploadedDocument>) causes out of memory errors. Doing it this way works.
                var doc = XDocument.Parse(xml);
                var nodes = doc.XPathSelectElements("//UploadedDocument[@DocumentName]");
                return nodes.Select(d => new AppraisalDocument(d, new XmlSerializer(typeof(UploadedDocument))))
                            .ToList();
            }
        }

        public AppraisalDocument AddDocument(UploadedDocument d)
        {
            var appraisalDoc = new AppraisalDocument(d);
            UploadedFiles.Add(appraisalDoc);
            return appraisalDoc;
        }

        /// <summary>
        /// Gets the type of appraisal order this object is.
        /// </summary>
        /// <value>Appraisal order type.</value>
        public override AppraisalOrderType AppraisalOrderType
        {
            get
            {
                return AppraisalOrderType.Lqb;
            }

            protected set
            {
            }
        }

        public override string OrderNumber
        {
            get
            {
                if (string.IsNullOrEmpty(m_orderNumber))
                    return AppraisalOrder.OrderNumber;
                return m_orderNumber;
            }
            set { m_orderNumber = value; }
        }

        public AppraisalOrder AppraisalOrder
        {
            get
            {
                if (m_appraisalOrder == null)
                    m_appraisalOrder = new AppraisalOrder();
                return m_appraisalOrder;
            }
            set { m_appraisalOrder = value; }
        }

        public OrderDataUpdate OrderDataUpdate
        {
            get
            {
                if (m_orderDataUpdate == null)
                    m_orderDataUpdate = new OrderDataUpdate();
                return m_orderDataUpdate;
            }
            //set { m_orderDataUpdate = value; }
            //use PushUpdate(OrderDataUpdate) instead
        }

        public List<AppraisalDocument> UploadedFiles
        {
            get
            {
                if (m_uploadedFiles == null)
                    m_uploadedFiles = new List<AppraisalDocument>();
                return m_uploadedFiles;
            }
            set { m_uploadedFiles = value; }
        }

        public List<OrderStatusHistoryItem> OrderHistory
        {
            get
            {
                if (m_orderHistory == null)
                    m_orderHistory = new List<OrderStatusHistoryItem>();
                return m_orderHistory;
            }
            set { m_orderHistory = value; }
        }

        public override string ProductName
        {
            get
            {
                return string.Join(", ", this.AppraisalOrder.OrderInfo.OrderedProductList.Select(prod => prod.ProductName));
            }

            set
            {
            }
        }

        public override CDateTime NeededDate
        {
            get
            {
                DateTime neededDate;
                if (DateTime.TryParseExact(this.AppraisalOrder.OrderInfo.AppraisalNeededDate, "dd-MM-yyyy", null, System.Globalization.DateTimeStyles.None, out neededDate))
                {
                    return CDateTime.Create(neededDate);
                }

                return CDateTime.InvalidWrapValue;
            }

            set
            {
                this.AppraisalOrder.OrderInfo.AppraisalNeededDate = value.ToString("dd-MM-yyyy");
            }
        }

        public override string FhaDocFileId
        {
            get
            {
                return this.OrderDataUpdate.OrderFields.FHADocID;
            }

            set
            {
                this.OrderDataUpdate.OrderFields.FHADocID = value;
            }
        }

        public override string UcdpAppraisalId
        {
            get
            {
                return this.OrderDataUpdate.OrderFields.UCDPDocID;
            }

            set
            {
                this.OrderDataUpdate.OrderFields.UCDPDocID = value;
            }
        }

        public override CDateTime SubmittedToFhaDate
        {
            get
            {
                DateTime submittedToFhaDate;
                if (DateTime.TryParseExact(this.OrderDataUpdate.OrderFields.DateSubmittedToFHA, "dd-MM-yyyy", null, System.Globalization.DateTimeStyles.None, out submittedToFhaDate))
                {
                    return CDateTime.Create(submittedToFhaDate);
                }

                return CDateTime.InvalidWrapValue;
            }

            set
            {
                this.OrderDataUpdate.OrderFields.DateSubmittedToFHA = value.ToString("dd-MM-yyyy");
            }
        }

        public override CDateTime SubmittedToUcdpDate
        {
            get
            {
                DateTime submittedToUcdpDate;
                if (DateTime.TryParseExact(this.OrderDataUpdate.OrderFields.DateSubmittedToUCDP, "dd-MM-yyyy", null, System.Globalization.DateTimeStyles.None, out submittedToUcdpDate))
                {
                    return CDateTime.Create(submittedToUcdpDate);
                }

                return CDateTime.InvalidWrapValue;
            }

            set
            {
                this.OrderDataUpdate.OrderFields.DateSubmittedToUCDP = value.ToString("dd-MM-yyyy");
            }
        }

        public override decimal CuRiskScore
        {
            get { return this.Converter.ToDecimal(this.OrderDataUpdate.OrderFields.CURiskScore); }
            set { this.OrderDataUpdate.OrderFields.CURiskScore = this.Converter.ToDecimalString_sSpCuScore(value); }
        }

        public override E_CuRiskT OvervaluationRiskT
        {
            get { return ConvertStringToCollateralUnderwriterRiskType(this.OrderDataUpdate.OrderFields.OvervaluationRiskIndicator); }
            set { this.OrderDataUpdate.OrderFields.OvervaluationRiskIndicator = ConvertCollateralUnderwriterRiskTypeToString(value); }
        }

        public override E_CuRiskT PropertyEligibilityRiskT
        {
            get { return ConvertStringToCollateralUnderwriterRiskType(this.OrderDataUpdate.OrderFields.PropertyEligibilitiyRiskIndicator); }
            set { this.OrderDataUpdate.OrderFields.PropertyEligibilitiyRiskIndicator = ConvertCollateralUnderwriterRiskTypeToString(value); }
        }

        public override E_CuRiskT AppraisalQualityRiskT
        {
            get { return ConvertStringToCollateralUnderwriterRiskType(this.OrderDataUpdate.OrderFields.AppraisalQualityRiskIndicator); }
            set { this.OrderDataUpdate.OrderFields.AppraisalQualityRiskIndicator = ConvertCollateralUnderwriterRiskTypeToString(value); }
        }

        public DateTime? StatusDate
        {
            get
            {
                if (m_statusDate == INVALID_DATE)
                    return null;
                return m_statusDate;
            }
            set
            {
                if (value == null)
                    m_statusDate = INVALID_DATE;
                else
                    m_statusDate = (DateTime)value;
            }
        }

        public string StatusDate_rep
        {
            get
            {
                if (m_statusDate == INVALID_DATE)
                {
                    return string.Empty;
                }
                return m_statusDate.ToShortDateString();
            }
        }

        public List<Guid> EmbeddedDocumentIds
        {
            get
            {
                if (m_embeddedDocIds == null)
                    m_embeddedDocIds = new List<Guid>();
                return m_embeddedDocIds;
            }
            set { m_embeddedDocIds = value; }
        }

        /// <summary>
        /// Gets the edoc ids of all associated edocs. Note that this pulls from both the attached files and the files received via the 
        /// appraisal framework web hook.
        /// </summary>
        protected override IEnumerable<Guid> AssociatedEdocIds
        {
            get
            {
                List<Guid> edocIds = new List<Guid>(this.EmbeddedDocumentIds.CoalesceWithEmpty());
                foreach (var uploadedFile in this.UploadedFiles.CoalesceWithEmpty())
                {
                    if (uploadedFile.EdocId.HasValue)
                    {
                        edocIds.Add(uploadedFile.EdocId.Value);
                    }
                }

                return edocIds;
            }
        }
        /// <summary>
        /// Converts this appraisal order into an appraisal order view.
        /// </summary>
        /// <param name="principal">The principal of the user generating the views.</param>
        /// <returns>Appraisal order view representation of the order.</returns>
        public override AppraisalOrderView ToView(AbstractUserPrincipal principal)
        {
            AppraisalOrderView view = new AppraisalOrderView(this);

            view.LoadingId = this.OrderNumber;
            view.VendorId = this.VendorId;
            view.VendorName = AppraisalVendorConfig.Retrieve(this.VendorId).VendorName;
            view.Status = string.IsNullOrEmpty(this.Status) ? "Unassigned" : this.Status;
            view.StatusDate = this.StatusDate.HasValue ? CDateTime.Create(this.StatusDate.Value) : CDateTime.InvalidWrapValue;

            var pdf = new LendersOffice.Pdf.CLQBAppraisalOrderPDF();
            view.ViewOrderUrl = $"{Tools.VRoot}/pdf/{pdf.Name}.aspx?loanid={this.LoanId}&ordernumber={HttpUtility.UrlEncode(this.OrderNumber)}";

            // Get Document Views
            view.UploadedDocuments = GetDocumentViews(principal);

            return view;
        }

        /// <summary>
        /// Generates a list of appraisal document views for use in the appraisal tracking page UI.
        /// </summary>
        /// <param name="principal">The principal of the user creating the views.</param>
        /// <returns>List of AppraisalDocumentView objects.</returns>
        private List<AppraisalDocumentView> GetDocumentViews(AbstractUserPrincipal principal)
        {
            List<AppraisalDocumentView> docViews = new List<AppraisalDocumentView>();

            // These are the files that come back to us. They're saved both in FileDb and in Edocs(if it's a PDF or XML file).
            foreach (LQBAppraisalOrder.AppraisalDocument doc in this.UploadedFiles)
            {
                AppraisalDocumentView docView = new AppraisalDocumentView();
                docView.FileName = doc.DocumentName;
                docView.ViewUrl = string.Format("javascript:openFileDbDoc('{0}');", doc.FileDBKey);
                docView.EDocId = doc.EdocId;
                docViews.Add(docView);
            }

            // These are the docs attached to the order.
            docViews.AddRange(this.GetDocumentViewsForEdocs(principal, this.EmbeddedDocumentIds));
            return docViews;
        }

        private static XmlSerializer orderSer;
        private static XmlSerializer dataSer;
        private static XmlSerializer docSer = new XmlSerializer(typeof(List<UploadedDocument>));
        private static XmlSerializer statusSer = new XmlSerializer(typeof(List<OrderStatusHistoryItem>));
        private static XmlSerializer docIdSer;

        public LQBAppraisalOrder(Guid loanId, Guid brokerId, Guid vendorId, string orderNumber)
            : base(loanId, brokerId)
        {
            this.VendorId = vendorId;
            this.OrderNumber = orderNumber;
        }

        private LQBAppraisalOrder(IDataRecord row)
            : base(row)
        {
            VendorId = (Guid)row["VendorId"];
            OrderNumber = row["OrderNumber"].ToString();
            Status = row["Status"].ToString();
            StatusDate = (DateTime)row["StatusDate"];

            DateTime dbDate = (DateTime)row["OrderedDate"];
            OrderedDate = CDateTime.Create(dbDate);

            string xml = row["AppraisalOrderXmlContent"].ToString();
            using (var sr = new StringReader(xml))
            {
                AppraisalOrder = (AppraisalOrder)orderSer.Deserialize(sr);

                AppraisalOrder.Billing.CCInfo.CCExpMonth = string.Empty;
                AppraisalOrder.Billing.CCInfo.CCExpYear = string.Empty;
                AppraisalOrder.Billing.CCInfo.CCNumber = string.Empty;
                AppraisalOrder.Billing.CCInfo.CCSecurityCode = string.Empty;
                AppraisalOrder.Billing.CCInfo.CCType = string.Empty;
            }

            xml = row["OrderDataUpdateXmlContent"].ToString();
            using (var sr = new StringReader(xml))
            {
                m_orderDataUpdate = (OrderDataUpdate)dataSer.Deserialize(sr);
            }

            xml = row["UploadedFilesXmlContent"].ToString();
            UploadedFiles = AppraisalDocument.DeserializeList(xml);

            xml = row["OrderHistoryXmlContent"].ToString();
            using (var sr = new StringReader(xml))
            {
                OrderHistory = (List<OrderStatusHistoryItem>)statusSer.Deserialize(sr);
            }

            xml = row["EmbeddedDocumentIdsXmlContent"].ToString();
            if (!string.IsNullOrEmpty(xml))
            {
                using (var sr = new StringReader(xml))
                {
                    EmbeddedDocumentIds = (List<Guid>)docIdSer.Deserialize(sr);
                }
            }
        }

        public static LQBAppraisalOrder Load(string loanId, string orderNumber)
        {
            if (orderSer == null) orderSer = new XmlSerializer(typeof(AppraisalOrder));
            if (dataSer == null) dataSer = new XmlSerializer(typeof(OrderDataUpdate));
            if (docIdSer == null) docIdSer = new XmlSerializer(typeof(List<Guid>));
            Guid sLId = new Guid(loanId);
            LQBAppraisalOrder order = null;

            Guid brokerId = Guid.Empty;

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(sLId, out brokerId);
            SqlParameter[] parameters = {
                                                new SqlParameter("sLId", sLId), 
                                                new SqlParameter("OrderNumber", orderNumber)
                                            };

            using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetAppraisalOrderById", parameters))
            {
                if (reader.Read())
                {
                    order = new LQBAppraisalOrder(reader);
                }
            }

            return order;
        }

        // Load method that takes an outside CStoredProcedureExec, for use within a transaction scope.
        public static LQBAppraisalOrder Load(string loanId, string orderNumber, CStoredProcedureExec exec)
        {
            if (orderSer == null) orderSer = new XmlSerializer(typeof(AppraisalOrder));
            if (dataSer == null) dataSer = new XmlSerializer(typeof(OrderDataUpdate));
            if (docIdSer == null) docIdSer = new XmlSerializer(typeof(List<Guid>));
            Guid sLId = new Guid(loanId);
            LQBAppraisalOrder order = null;

            SqlParameter[] parameters = {
                                                new SqlParameter("sLId", sLId),
                                                new SqlParameter("OrderNumber", orderNumber)
                                            };

            using (var reader = exec.ExecuteReader("GetAppraisalOrderById", parameters))
            {
                if (reader.Read())
                {
                    order = new LQBAppraisalOrder(reader);
                }
            }

            return order;
        }

        public static List<LQBAppraisalOrder> GetOrdersForLoan(Guid loanId)
        {
            if (orderSer == null) orderSer = new XmlSerializer(typeof(AppraisalOrder));
            if (dataSer == null) dataSer = new XmlSerializer(typeof(OrderDataUpdate));
            if (docIdSer == null) docIdSer = new XmlSerializer(typeof(List<Guid>));

            Guid brokerId = Guid.Empty;

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(loanId, out brokerId);

            List<LQBAppraisalOrder> orders = new List<LQBAppraisalOrder>();

            SqlParameter[] parameters = {
                                            new SqlParameter("sLId", loanId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "GetAppraisalOrders", parameters))
            {
                while (reader.Read())
                {
                    orders.Add(new LQBAppraisalOrder(reader));
                }
            }

            return orders;
        }

        /// <summary>
        /// Saves appraisal order to database and clears out AppraisalOrder.EmbeddedDocList's embeddedDoc.Content to free up memory.
        /// Do not call before AppraisalVendorServer.Submit(LQBAppraisalRequest request, string url)
        /// </summary>
        public void Save()
        {
            // OPM 246937 - Verify that appraisal vendor exists before saving order.
            AppraisalVendorConfig.Retrieve(VendorId);   // Throws if vendor does not exist.

            List<SqlParameter> parameters = GetParameters();

            Guid brokerId = Guid.Empty;

            DbConnectionInfo connInfo = DbConnectionInfo.GetConnectionInfoByLoanId(this.LoanId, out brokerId);

            StoredProcedureHelper.ExecuteNonQuery(connInfo, "SaveAppraisalOrder", 3, parameters);
        }

        /// <summary>
        /// Saves appraisal order to database and clears out AppraisalOrder.EmbeddedDocList's embeddedDoc.Content to free up memory.
        /// Do not call before AppraisalVendorServer.Submit(LQBAppraisalRequest request, string url)
        /// </summary>
        /// <param name="exec">Used to execute stored procedures within a transaction scope.</param>
        public void Save(CStoredProcedureExec exec)
        {
            // OPM 246937 - Verify that appraisal vendor exists before saving order.
            AppraisalVendorConfig.Retrieve(VendorId);   // Throws if vendor does not exist.

            List<SqlParameter> parameters = GetParameters();

            exec.ExecuteNonQuery("SaveAppraisalOrder", 3, parameters.ToArray());
        }

        /// <summary>
        /// Gets a list of parameters for saving order to DB.
        /// </summary>
        /// <returns>List of parameter.</returns>
        private List<SqlParameter> GetParameters()
        {

            if (this.AppraisalOrder.Billing != null && this.AppraisalOrder.Billing.CCInfo != null)
            {
                this.AppraisalOrder.Billing.CCInfo = null;
            }

            AppraisalOrder.EmbeddedDocList.ForEach(embeddedDoc => embeddedDoc.Content = "[base64 Encoded file data]"); // OPM 145156 - Out Of Memory Exception thrown by Xml Serialization
            List<SqlParameter> parameters = this.GetBaseParameters();
            parameters.Add(new SqlParameter("VendorId", VendorId));
            parameters.Add(new SqlParameter("Status", Status));
            parameters.Add(new SqlParameter("StatusDate", m_statusDate));
            parameters.Add(new SqlParameter("AppraisalOrderXmlContent", XmlSerialize(AppraisalOrder)));
            parameters.Add(new SqlParameter("OrderDataUpdateXmlContent", XmlSerialize(OrderDataUpdate)));
            parameters.Add(new SqlParameter("UploadedFilesXmlContent", XmlSerialize(UploadedFiles)));
            parameters.Add(new SqlParameter("OrderHistoryXmlContent", XmlSerialize(OrderHistory)));
            parameters.Add(new SqlParameter("EmbeddedDocumentIdsXmlContent", XmlSerialize(EmbeddedDocumentIds)));
            return parameters;
        }

        private static string XmlSerialize(Object obj)
        {
            using (StringWriter8 sw = new StringWriter8())
            {
                XmlSerializer xs = new XmlSerializer(obj.GetType());
                xs.Serialize(sw, obj);
                sw.Flush();
                return sw.ToString();
            }
        }

        private static string XmlSerialize(List<AppraisalDocument> l)
        {
            using (StringWriter8 sw = new StringWriter8())
            {
                XmlSerializer xs = new XmlSerializer(typeof(List<UploadedDocument>));
                var docs = l.Select(d => d.Document).ToList();
                xs.Serialize(sw, docs);

                return sw.ToString();
            }
        }

        /// <summary>
        /// Converts a <see cref="string"/> to a <see cref="E_CuRiskT"/> value.
        /// </summary>
        /// <param name="indicatorValue">The <see cref="string"/> to convert.</param>
        /// <returns><see cref="E_CuRiskT.HeightenedRisk"/> for <c>"Y"</c>, <see cref="E_CuRiskT.NotHeightenedRisk"/> for <c>"N"</c>, and <see cref="E_CuRiskT.LeaveBlank"/> for any other value.</returns>
        private static E_CuRiskT ConvertStringToCollateralUnderwriterRiskType(string indicatorValue)
        {
            if (StringComparer.OrdinalIgnoreCase.Equals(indicatorValue, "Y"))
            {
                return E_CuRiskT.HeightenedRisk;
            }
            else if (StringComparer.OrdinalIgnoreCase.Equals(indicatorValue, "N"))
            {
                return E_CuRiskT.NotHeightenedRisk;
            }

            return E_CuRiskT.LeaveBlank;
        }

        /// <summary>
        /// Convert a <see cref="E_CuRiskT"/> value to a <see cref="string"/>.
        /// </summary>
        /// <param name="riskType">The <see cref="E_CuRiskT"/> to convert.</param>
        /// <returns><c>"Y"</c> for <see cref="E_CuRiskT.HeightenedRisk"/>, <c>"N"</c> for <see cref="E_CuRiskT.NotHeightenedRisk"/>, and <see cref="string.Empty"/> otherwise.</returns>
        private static string ConvertCollateralUnderwriterRiskTypeToString(E_CuRiskT riskType)
        {
            switch (riskType)
            {
                case E_CuRiskT.HeightenedRisk: return "Y";
                case E_CuRiskT.NotHeightenedRisk: return "N";
                case E_CuRiskT.LeaveBlank: return string.Empty;
                default:
                    throw new UnhandledEnumException(riskType);
            }
        }

        public void PushUpdate(OrderDataUpdate update)
        {
            DateTime now = DateTime.Now;
            MergeAssignedAppraiser(OrderDataUpdate.AssignedAppraiser, update.AssignedAppraiser);
            MergeOrderFields(OrderDataUpdate.OrderFields, update.OrderFields);
            MergeAMCContactInfo(OrderDataUpdate.AMCContactInfo, update.AMCContactInfo);

            if (!string.IsNullOrEmpty(update.OrderFields.Status))
            {
                var item = new OrderStatusHistoryItem();
                item.DateTimeStamp = now;
                item.StatusName = update.OrderFields.Status;
                item.StatusDescription = update.OrderFields.StatusComments;

                Status = update.OrderFields.Status;
                StatusDate = now;
                OrderHistory.Add(item);
            }
        }

        private void MergeAMCContactInfo(AMCContactInfo old, AMCContactInfo update)
        {
            if (!string.IsNullOrEmpty(update.ContactEmail)) old.ContactEmail = update.ContactEmail;
            if (!string.IsNullOrEmpty(update.ContactName)) old.ContactName = update.ContactName;
            if (!string.IsNullOrEmpty(update.ContactPhone)) old.ContactPhone = update.ContactPhone;
        }

        private void MergeAssignedAppraiser(AssignedAppraiser old, AssignedAppraiser update)
        {
            MergeAppraiserInfo(old.AppraiserInfo, update.AppraiserInfo);
            MergeAppraiserCompanyInfo(old.AppraiserCompanyInfo, update.AppraiserCompanyInfo);
            MergeAppraiserLicenses(old.AppraiserLicenseList, update.AppraiserLicenseList);
        }

        private void MergeAppraiserInfo(AppraiserInfo old, AppraiserInfo update)
        {
            if (!string.IsNullOrEmpty(update.ApprEmail)) old.ApprEmail = update.ApprEmail;
            if (!string.IsNullOrEmpty(update.ApprFax)) old.ApprFax = update.ApprFax;
            if (!string.IsNullOrEmpty(update.ApprName)) old.ApprName = update.ApprName;
            if (!string.IsNullOrEmpty(update.ApprPhone)) old.ApprPhone = update.ApprPhone;
        }

        private void MergeAppraiserCompanyInfo(AppraiserCompanyInfo old, AppraiserCompanyInfo update)
        {
            if (!string.IsNullOrEmpty(update.ApprAddress)) old.ApprAddress = update.ApprAddress;
            if (!string.IsNullOrEmpty(update.ApprCity)) old.ApprCity = update.ApprCity;
            if (!string.IsNullOrEmpty(update.ApprCompanyFax)) old.ApprCompanyFax = update.ApprCompanyFax;
            if (!string.IsNullOrEmpty(update.ApprCompanyName)) old.ApprCompanyName = update.ApprCompanyName;
            if (!string.IsNullOrEmpty(update.ApprCompanyPhone)) old.ApprCompanyPhone = update.ApprCompanyPhone;
            if (!string.IsNullOrEmpty(update.ApprState)) old.ApprState = update.ApprState;
            if (!string.IsNullOrEmpty(update.ApprZIP)) old.ApprZIP = update.ApprZIP;
        }

        private void MergeAppraiserLicenses(List<AppraiserLicense> old, List<AppraiserLicense> update)
        {
            if (update.Count > 0)
            {
                old.Clear();
                old.AddRange(update);
            }
        }

        private void MergeOrderFields(OrderFields old, OrderFields update)
        {
            if (!string.IsNullOrEmpty(update.AppraiserLastModifiedDate)) old.AppraiserLastModifiedDate = update.AppraiserLastModifiedDate;
            if (!string.IsNullOrEmpty(update.DateAssigned)) old.DateAssigned = update.DateAssigned;
            if (!string.IsNullOrEmpty(update.DateCancelled)) old.DateCancelled = update.DateCancelled;
            if (!string.IsNullOrEmpty(update.DateCompleted)) old.DateCompleted = update.DateCompleted;
            if (!string.IsNullOrEmpty(update.DateEstCompletion)) old.DateEstCompletion = update.DateEstCompletion;
            if (!string.IsNullOrEmpty(update.DateInReview)) old.DateInReview = update.DateInReview;
            if (!string.IsNullOrEmpty(update.DateInspected)) old.DateInspected = update.DateInspected;
            if (!string.IsNullOrEmpty(update.DateInspectionScheduled)) old.DateInspectionScheduled = update.DateInspectionScheduled;
            if (!string.IsNullOrEmpty(update.EstimatedValue)) old.EstimatedValue = update.EstimatedValue;
            if (!string.IsNullOrEmpty(update.InspectionEndDate)) old.InspectionEndDate = update.InspectionEndDate;
            if (!string.IsNullOrEmpty(update.InspectionStartDate)) old.InspectionStartDate = update.InspectionStartDate;
            if (!string.IsNullOrEmpty(update.InspectionTime)) old.InspectionTime = update.InspectionTime;
            if (!string.IsNullOrEmpty(update.InvoiceDate)) old.InvoiceDate = update.InvoiceDate;
            if (!string.IsNullOrEmpty(update.ReviewersComments)) old.ReviewersComments = update.ReviewersComments;
            if (!string.IsNullOrEmpty(update.Status)) old.Status = update.Status;
            if (!string.IsNullOrEmpty(update.StatusComments)) old.StatusComments = update.StatusComments;
            if (!string.IsNullOrEmpty(update.StatusHistory)) old.StatusHistory = update.StatusHistory;
            if (!string.IsNullOrEmpty(update.UCDPDocID)) old.UCDPDocID = update.UCDPDocID;
            if (!string.IsNullOrEmpty(update.FHADocID)) old.FHADocID = update.FHADocID;
            if (!string.IsNullOrEmpty(update.DateSubmittedToUCDP)) old.DateSubmittedToUCDP = update.DateSubmittedToUCDP;
            if (!string.IsNullOrEmpty(update.DateSubmittedToFHA)) old.DateSubmittedToFHA = update.DateSubmittedToFHA;
            if (!string.IsNullOrEmpty(update.CURiskScore)) old.CURiskScore = update.CURiskScore;
            if (!string.IsNullOrEmpty(update.OvervaluationRiskIndicator)) old.OvervaluationRiskIndicator = update.OvervaluationRiskIndicator;
            if (!string.IsNullOrEmpty(update.PropertyEligibilitiyRiskIndicator)) old.PropertyEligibilitiyRiskIndicator = update.PropertyEligibilitiyRiskIndicator;
            if (!string.IsNullOrEmpty(update.AppraisalQualityRiskIndicator)) old.AppraisalQualityRiskIndicator = update.AppraisalQualityRiskIndicator;
        }
    }
    public class OrderStatusHistoryItem
    {
        public DateTime DateTimeStamp { get; set; }
        public string StatusName { get; set; }
        public string StatusDescription { get; set; }
    }
}
