﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace LendersOffice.ObjLib.Conversions.LQBAppraisal
{
    public class LQBAppraisalStatusUpdateResponse
    {
        public enum StatusType
        {
            OK,
            ERROR
        }

        public byte[] ToXml()
        {
            // Serialize to XML
            XmlSerializer serializer = new XmlSerializer(typeof(LQBAppraisalStatusUpdateResponse));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            byte[] bytes = null;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                XmlWriterSettings writerSettings = new XmlWriterSettings();
                writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM
                writerSettings.OmitXmlDeclaration = true;
                using (XmlWriter tw = XmlWriter.Create(stream, writerSettings))
                {
                    serializer.Serialize(tw, this, ns);
                }
                bytes = stream.ToArray();
            }

            return bytes;
        }

        public StatusType Status;
        public string Message = "";
    }
}
