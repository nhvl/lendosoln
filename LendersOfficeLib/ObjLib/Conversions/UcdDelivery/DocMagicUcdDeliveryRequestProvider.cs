﻿namespace LendersOffice.Conversions.UcdDelivery
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Constants;
    using DocMagic.Common;
    using DocMagic.DsiDocRequest;
    using Integration.Templates;
    using Integration.UcdDelivery;
    using Integration.UcdDelivery.DocMagic;
    using Templates;
    using XmlSerializableCommon;

    /// <summary>
    /// Uses the DocMagic data classes to generate a payload for their UCD Data Service.
    /// </summary>
    public class DocMagicUcdDeliveryRequestProvider : IRequestProvider
    {
        /// <summary>
        /// The data to populate the request.
        /// </summary>
        private DocMagicUcdDeliveryRequestData requestData;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocMagicUcdDeliveryRequestProvider"/> class.
        /// </summary>
        /// <param name="requestData">The data to populate the request.</param>
        public DocMagicUcdDeliveryRequestProvider(DocMagicUcdDeliveryRequestData requestData)
        {
            this.requestData = requestData;
        }

        /// <summary>
        /// Audits the generated request payload.
        /// </summary>
        /// <returns>An integration audit result.</returns>
        public IntegrationAuditResult AuditRequest()
        {
            throw new NotImplementedException("No data audit currently takes place for the DocMagic UCD integration.");
        }

        /// <summary>
        /// Serializes the DocMagic request to a string.
        /// </summary>
        /// <returns>The request payload.</returns>
        public string SerializeRequest()
        {
            var request = this.CreateDsiDocumentServerRequest();

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = new UTF8Encoding(encoderShouldEmitUTF8Identifier: false);
            settings.OmitXmlDeclaration = true;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    request.WriteXml(writer);
                }

                byte[] bytes = stream.GetBuffer();
                return Encoding.UTF8.GetString(bytes, 0, (int)stream.Position);
            }
        }

        /// <summary>
        /// Generates a DocMagic request using the given data.
        /// </summary>
        /// <returns>A DocMagic request root element.</returns>
        public DsiDocumentServerRequest GenerateRequest()
        {
            return this.CreateDsiDocumentServerRequest();
        }

        /// <summary>
        /// Logs a request to PB.
        /// </summary>
        /// <param name="request">The payload string.</param>
        public void LogRequest(string request = null)
        {
            if (string.IsNullOrEmpty(request))
            {
                return;
            }

            XElement root = XElement.Parse(request);
            var descendants = root.Descendants();

            var passwords = descendants.Where(el => el.Name.LocalName.Contains("Password"));
            foreach (var password in passwords)
            {
                password.Value = ConstAppDavid.FakePasswordDisplay;
            }

            UcdDeliveryUtilities.LogPayload(root.ToString(SaveOptions.DisableFormatting), UcdDeliveryUtilities.DocMagicRequestLogHeader);
        }

        /// <summary>
        /// Creates a container holding the user's GSE authentication credentials.
        /// </summary>
        /// <returns>A <see cref="Credentials"/> container.</returns>
        private Credentials CreateCredentials()
        {
            var credentials = new Credentials();
            credentials.LoginAccountIdentifier = this.requestData.GseUserName;
            credentials.LoginAccountPassword = this.requestData.GsePassword;

            return credentials;
        }

        /// <summary>
        /// Creates a container holding customer information.
        /// </summary>
        /// <returns>A <see cref="CustomerInformation"/> container.</returns>
        private CustomerInformation CreateCustomerInformation()
        {
            var customerInformation = new CustomerInformation();
            customerInformation.CustomerId = this.requestData.DmCustomerId;
            customerInformation.UserName = this.requestData.DmUsername;
            customerInformation.Password = this.requestData.DmPassword;

            return customerInformation;
        }

        /// <summary>
        /// Creates a container indicating the source of the payload.
        /// </summary>
        /// <returns>A <see cref="DataSource"/> container.</returns>
        private DataSource CreateDataSource()
        {
            var dataSource = new DataSource();
            dataSource.DsName = "LENDINGQB";
            dataSource.TransactionId = this.requestData.TransactionId.ToString();

            return dataSource;
        }

        /// <summary>
        /// Creates a container holding data about the request.
        /// </summary>
        /// <returns>A <see cref="DataServiceRequest"/> container.</returns>
        private DataServiceRequest CreateDataServiceRequest()
        {
            var request = new DataServiceRequest();
            request.DataServiceType = E_DataServiceType.UCD;
            request.DataServiceUcd = this.CreateDataServiceUcd();

            return request;
        }

        /// <summary>
        /// Creates a container holding information to call the UCD data service.
        /// </summary>
        /// <returns>A <see cref="DataServiceUCD"/> container.</returns>
        private DataServiceUCD CreateDataServiceUcd()
        {
            var dataServiceUcd = new DataServiceUCD();
            dataServiceUcd.DataServiceActionType = this.requestData.Action == UcdDeliveryAction.GenerateAndDeliver
                ? E_DataServiceActionType.CreateAndDeliver
                : E_DataServiceActionType.Create;
            dataServiceUcd.RequestedContentEncodingType = E_EncodingType.None;
            dataServiceUcd.DocCode = this.requestData.DocCode;

            if (this.requestData.Action == UcdDeliveryAction.GenerateAndDeliver)
            {
                dataServiceUcd.DataServiceActionType = E_DataServiceActionType.CreateAndDeliver;
                
                if (this.requestData.DeliveryTarget == UcdDeliveredToEntity.FannieMae)
                {
                    dataServiceUcd.InvestorType = E_InvestorType.FNM;
                }
                else if (this.requestData.DeliveryTarget == UcdDeliveredToEntity.FreddieMac)
                {
                    dataServiceUcd.InvestorType = E_InvestorType.FRE;
                }

                dataServiceUcd.InvestorDelivery = this.CreateInvestorDelivery();
            }
            else
            {
                dataServiceUcd.DataServiceActionType = E_DataServiceActionType.Create;
                dataServiceUcd.InvestorType = E_InvestorType.None;
            }

            return dataServiceUcd;
        }

        /// <summary>
        /// Creates a root container for a DocMagic request.
        /// </summary>
        /// <returns>A <see cref="DsiDocumentServerRequest"/> container.</returns>
        private DsiDocumentServerRequest CreateDsiDocumentServerRequest()
        {
            var request = new DsiDocumentServerRequest();
            request.RequestOriginator = E_DsiDocumentServerRequestRequestOriginator.ThirdPartyServer;

            request.DataSource = this.CreateDataSource();
            request.CustomerInformation = this.CreateCustomerInformation();
            request.DataServiceRequest = this.CreateDataServiceRequest();

            return request;
        }

        /// <summary>
        /// Creates a container holding information on the investor to deliver the UCD to.
        /// </summary>
        /// <returns>A <see cref="InvestorDelivery"/> container.</returns>
        private InvestorDelivery CreateInvestorDelivery()
        {
            var investorDelivery = new InvestorDelivery();
            investorDelivery.IncludeDeliveryContentInResponseIndicator = E_YNIndicator.Y;
            investorDelivery.Credentials = this.CreateCredentials();

            if (this.requestData.DeliveryTarget == UcdDeliveredToEntity.FreddieMac)
            {
                investorDelivery.UCDDeliveryParticipants = this.CreateUcdDeliveryParticipants();
            }

            return investorDelivery;
        }

        /// <summary>
        /// Creates a container indicating a UCD delivery participant.
        /// </summary>
        /// <returns>A <see cref="UCDDeliveryParticipant"/> container.</returns>
        private UCDDeliveryParticipant CreateUcdDeliveryParticipant()
        {
            var participant = new UCDDeliveryParticipant();

            var freddieMacRelationship = this.requestData.Principal.BrokerDB.RelationshipWithFreddieMac;
            if (freddieMacRelationship == Admin.LenderRelationshipWithFreddieMac.Correspondent)
            {
                participant.ParticipantType = E_ParticipantType.Correspondent;
            }
            else if (freddieMacRelationship == Admin.LenderRelationshipWithFreddieMac.Seller)
            {
                participant.ParticipantType = E_ParticipantType.Seller;
            }
            
            participant.ParticipantIdentifier = this.requestData.GseCustomerId;

            return participant;
        }

        /// <summary>
        /// Creates a container holding a set of UCD delivery participants.
        /// </summary>
        /// <returns>A <see cref="UCDDeliveryParticipants"/> container.</returns>
        private UCDDeliveryParticipants CreateUcdDeliveryParticipants()
        {
            var participants = new UCDDeliveryParticipants();
            participants.UCDDeliveryParticipantList.Add(this.CreateUcdDeliveryParticipant());

            return participants;
        }
    }
}
