﻿namespace LendersOffice.ObjLib.Conversions.UcdDelivery.FannieMae
{
    using System;
    using System.IO;
    using EDocs;
    using Integration.Templates;
    using Integration.UcdDelivery;
    using Integration.UcdDelivery.FannieMae;
    using LendersOffice.Conversions.Templates;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// The request provider for Fannie Mae UCD integration.
    /// </summary>
    public class FannieMaeUcdDeliveryRequestProvider : IRequestProvider
    {
        /// <summary>
        /// The Fannie Mae UCD integration request data.
        /// </summary>
        private FannieMaeUcdDeliveryRequestData requestData;

        /// <summary>
        /// The payload for the Fannie Mae UCD integration.
        /// </summary>
        private Lazy<string> payload;

        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDeliveryRequestProvider"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        public FannieMaeUcdDeliveryRequestProvider(FannieMaeUcdDeliveryRequestData requestData)
        {
            this.requestData = requestData;
            this.payload = new Lazy<string>(this.GetPayload);
        }

        /// <summary>
        /// Audits the data payload.
        /// </summary>
        /// <returns>The audit results.</returns>
        public IntegrationAuditResult AuditRequest()
        {
            throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("No data audit currently takes place for the Fannie Mae UCD integration."));
        }

        /// <summary>
        /// Logs the data payload, masking out any large or sensitive information.
        /// </summary>
        /// <param name="request">The payload string, if needed.</param>
        public void LogRequest(string request = null)
        {
            if (this.requestData.Action == UcdDeliveryAction.DeliverExisting)
            {
                UcdDeliveryUtilities.LogPayload($"UCD file Edoc id: {this.requestData.UcdEdocId.ToString()}", UcdDeliveryUtilities.FannieMaeRequestLogHeader);
            }
            else if (this.requestData.Action == UcdDeliveryAction.GetResults)
            {
                UcdDeliveryUtilities.LogPayload($"GET request. No payload.", UcdDeliveryUtilities.FannieMaeRequestLogHeader);
            }
        }

        /// <summary>
        /// Serializes the request data into a string.
        /// </summary>
        /// <returns>The request data as a string.</returns>
        public string SerializeRequest()
        {
            return this.payload.Value;
        }

        /// <summary>
        /// Gets the payload for the request.
        /// </summary>
        /// <returns>The payload as a string.</returns>
        private string GetPayload()
        {
            if (this.requestData.Action == UcdDeliveryAction.DeliverExisting)
            {
                // The body for a delivery is the UCD xml itself.
                var ucdEdocId = this.requestData.UcdEdocId;
                EDocumentRepository repository = EDocumentRepository.GetUserRepository(this.requestData.Principal);
                var doc = repository.GetGenericDocumentById(ucdEdocId.Value);
                return File.ReadAllText(doc.GetContentPath());
            }
            else if (this.requestData.Action == UcdDeliveryAction.GetResults)
            {
                // Get requests don't have a body, just a bunch of headers and some query parameters.
                return null;
            }
            else
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid action: {this.requestData.Action}"));
            }
        }
    }
}
