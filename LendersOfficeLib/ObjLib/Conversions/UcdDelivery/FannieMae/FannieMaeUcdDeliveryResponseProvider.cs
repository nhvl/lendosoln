﻿namespace LendersOffice.ObjLib.Conversions.UcdDelivery.FannieMae
{
    using System;
    using System.Xml;
    using Integration.UcdDelivery;
    using Integration.UcdDelivery.FannieMae;
    using LendersOffice.Conversions.Templates;

    /// <summary>
    /// Response provider for Fannie Mae UCD findings XML.
    /// </summary>
    public class FannieMaeUcdDeliveryResponseProvider : AbstractResponseProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDeliveryResponseProvider"/> class.
        /// </summary>
        /// <param name="responsePayload">The response payload.</param>
        public FannieMaeUcdDeliveryResponseProvider(string responsePayload)
            : base(responsePayload, null)
        {
        }

        /// <summary>
        /// Gets the response data.
        /// </summary>
        /// <value>The response data.</value>
        public new FannieMaeUcdDeliveryResponseData ResponseData => (FannieMaeUcdDeliveryResponseData)base.ResponseData;

        /// <summary>
        /// Logs the response.
        /// </summary>
        public override void LogResponse()
        {
            XmlDocument findingsXml = new XmlDocument();
            try
            {
                findingsXml.LoadXml(this.ResponsePayload);
            }
            catch (XmlException)
            {
                UcdDeliveryUtilities.LogPayload(this.ResponsePayload, UcdDeliveryUtilities.FannieMaeResponseLogHeader);
                return;
            }

            XmlNamespaceManager manager = new XmlNamespaceManager(findingsXml.NameTable);
            manager.AddNamespace("ns", FannieMaeUcdDelivery.FannieMaeNamespace);
            var findingsHtmlNode = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Casefiles/ns:Casefile/ns:Datasets/ns:Dataset/ns:CasefileAttachments/ns:CasefileAttachment[ns:CasefileAttachmentType/text()='HTML']/ns:CasefileAttachmentContent", manager);
            if (findingsHtmlNode != null)
            {
                findingsHtmlNode.InnerText = "[Redacted for space]";
            }

            UcdDeliveryUtilities.LogPayload(findingsXml.OuterXml, UcdDeliveryUtilities.FannieMaeResponseLogHeader);
        }

        /// <summary>
        /// Parses the Fannie Mae UCD findings XML.
        /// </summary>
        public override void ParseResponse()
        {
            XmlDocument findingsXml = new XmlDocument();
            try
            {
                findingsXml.LoadXml(this.ResponsePayload);
            }
            catch (XmlException)
            {
                this.Errors.Add("Unable to retrieve results.");
                return;
            }

            XmlNamespaceManager manager = new XmlNamespaceManager(findingsXml.NameTable);
            manager.AddNamespace("ns", FannieMaeUcdDelivery.FannieMaeNamespace);

            // Check for errors first
            var batchStatus = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Batch/ns:BatchStatus", manager)?.InnerText;
            if (!string.IsNullOrEmpty(batchStatus) && !batchStatus.Equals("Complete", StringComparison.OrdinalIgnoreCase))
            {
                var batchError = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Batch/ns:BatchMessages/ns:msgText", manager)?.InnerText;
                this.Errors.Add(batchError);
                return;
            }

            var casefileId = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Casefiles/ns:Casefile/ns:FannieMaeCasefileIdentifier", manager)?.InnerText;
            var batchId = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Batch/ns:BatchId", manager)?.InnerText;
            var statusString = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Casefiles/ns:Casefile/ns:Datasets/ns:Dataset[@DataSetType='UniformClosingDataset']/ns:FannieMaeCasefileStatusType", manager)?.InnerText;
            FannieMaeUcdDeliveryStatus status;
            if (!string.IsNullOrEmpty(statusString) && statusString.Equals("Successful", StringComparison.OrdinalIgnoreCase))
            {
                status = FannieMaeUcdDeliveryStatus.Successful;
            }
            else
            {
                status = FannieMaeUcdDeliveryStatus.NotSuccessful;
            }

            if (casefileId.Equals("Invalid", StringComparison.OrdinalIgnoreCase) && status == FannieMaeUcdDeliveryStatus.NotSuccessful)
            {
                var errorMsg = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Casefiles/ns:Casefile/ns:Datasets/ns:Dataset[@DataSetType='UniformClosingDataset']/ns:CasefileFindings/ns:CasefileFinding/ns:CasefileFindingsMessageText", manager)?.InnerText;
                this.Errors.Add(errorMsg);
                return;
            }

            // If you're going to edit this, please edit FannieMaeUcdDelivery.FindingsHtml as well.
            var findingsHtml = findingsXml.SelectSingleNode("//ns:LenderFeedback/ns:Casefiles/ns:Casefile/ns:Datasets/ns:Dataset/ns:CasefileAttachments/ns:CasefileAttachment[ns:CasefileAttachmentType/text()='HTML']/ns:CasefileAttachmentContent", manager)?.InnerText;
            base.ResponseData = new FannieMaeUcdDeliveryResponseData(
                UcdDeliveryResultStatus.Delivered,
                status,
                findingsHtml,
                casefileId,
                generationResults: null,
                deliveryResults: new UcdDeliveryResults(this.ResponsePayload, batchId));
        }
    }
}
