﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" exclude-result-prefixes="exslt msxsl" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:sch="http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas" xmlns:typ1="http://lclaloanresults.freddiemac.com/schema/types" xmlns:env="http://schemas.xmlsoap.org/soap/envelope/" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:param name="VirtualRoot" />

<xsl:template match="/">
<html>
<head>

<title>Loan Closing Advisor -System Error</title>
<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/style.css')}" media="screen,print" />
<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/additonal-style.css')}" media="screen,print" />
	
<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/lcla-global.css')}" media="screen,print" />
<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/lcla_loan_eval_rslts.css')}" media="screen,print" />

</head>

<body class="bg-greybg main-nav-fixed">

<nav class="main-nav fixed">
					<ul class="main-nav-list" style="margin: 0px;">
						<li>
							<a href="#">
							
								<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-app-lcla-inv.svg')}" />Loan Closing Advisor<span class="servicemark" />

					</a>
						</li>
					</ul>
</nav>


  <section class="container" style="padding-top:50px">
    <div class="row">
			<section class="col-12">
				<div class="row">
					<xsl:choose>
					
					<xsl:when test="env:Envelope/env:Body/env:Fault/faultstring ='Schema Validation Error'">
						<div class="floating-header col-12">	<i class="ic-error" style="padding-right:15px; font-size:39px;vertical-align:text-bottom" /> <span>MISMO Schema Error</span></div>
					</xsl:when>
					<xsl:otherwise>
						<div class="floating-header col-12"><i class="ic-error" style="padding-right:15px; font-size:39px; vertical-align:text-bottom" />  <span>System Fault</span></div>
					</xsl:otherwise>
					</xsl:choose>
				</div>
				<div class="row">
					<section class="container content first bg-white">
						<div class="row">
							<span class="body-gray">
									<xsl:if test="sch:LCLALoanEvaluationServiceFault/sch:EvaluationResults/typ1:ErrorDetails/typ1:ErrorDetail/typ1:ErrorMessage != ''">
										<xsl:value-of select="sch:LCLALoanEvaluationServiceFault/sch:EvaluationResults/typ1:ErrorDetails/typ1:ErrorDetail/typ1:ErrorMessage" />
									</xsl:if>
									<xsl:if test="env:Envelope/env:Body/env:Fault/detail/env:WSGFault != ''">
										<xsl:value-of select="env:Envelope/env:Body/env:Fault/detail/env:WSGFault" />
									</xsl:if>
							</span>
						</div>
					</section>
				</div>
			</section>
		</div>
	
  </section>

  
</body>
</html>
	</xsl:template>
</xsl:stylesheet>