﻿namespace LendersOffice.Integration.UcdDelivery.FreddieMac
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Linq;
    using Admin;
    using EDocs;
    using LendersOffice.Conversions.Templates;
    using LendersOffice.Integration.Templates;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Converts Loan Closing Advisor (LCLA) request data into the SOAP string format sent to Freddie Mac's LCLA web service.
    /// </summary>
    public class LoanClosingAdvisorRequestProvider : IRequestProvider
    {
        /// <summary>
        /// Maps the data layer <see cref="LenderRelationshipWithFreddieMac"/> type to the exporter type <see cref="CustomerIdentifierType"/>.
        /// </summary>
        public static readonly IReadOnlyDictionary<LenderRelationshipWithFreddieMac, CustomerIdentifierType?> LenderRelationshipToCustomerIdentifierType =
            new Dictionary<LenderRelationshipWithFreddieMac, CustomerIdentifierType?>()
            {
                { LenderRelationshipWithFreddieMac.Undefined, null },
                { LenderRelationshipWithFreddieMac.Seller, CustomerIdentifierType.Seller },
                { LenderRelationshipWithFreddieMac.Correspondent, CustomerIdentifierType.Correspondent }
            };

        /// <summary>
        /// Vendor identifier to send. This is the identifier Freddie has assigned to us.
        /// </summary>
        private const string VendorIdentifier = "000184";

        /// <summary>
        /// Vendor name to send.
        /// </summary>
        private const string VendorName = "MeridianLink";

        /// <summary>
        /// Software name to send.
        /// </summary>
        private const string VendorSoftware = "LendingQB";

        /// <summary>
        /// XML namespace for SOAP.
        /// </summary>
        private static XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";

        /// <summary>
        /// XML namespace for SOAP authentication header.
        /// </summary>
        private static XNamespace wsse = "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

        /// <summary>
        /// XML namespace for Freddie Mac Loan Evaluation Service schema.
        /// </summary>
        private static XNamespace sch = "http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas";

        /// <summary>
        /// XML namespace for Freddie Mac.
        /// </summary>
        private static XNamespace gse = "http://www.efreddiemac.com";

        /// <summary>
        /// XML namespace for MISMO.
        /// </summary>
        private static XNamespace mismo = "http://www.mismo.org/residential/2009/schemas";

        /// <summary>
        /// Software version.
        /// </summary>
        private static int vendorSoftwareVersion = DateTime.Now.Year;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanClosingAdvisorRequestProvider"/> class.
        /// </summary>
        /// <param name="request">The request data to use for building the request structure.</param>
        public LoanClosingAdvisorRequestProvider(FreddieMacUcdDeliveryRequestData request = null)
        {
            this.Request = request;
        }

        /// <summary>
        /// Gets or sets the request object that will be used to populate the request.
        /// </summary>
        public FreddieMacUcdDeliveryRequestData Request { get; set; }

        /// <summary>
        /// Audits the <see cref="Request"/> object to ensure serialization will be successful.
        /// </summary>
        /// <returns>The integration audit result, including any audit errors.</returns>
        public IntegrationAuditResult AuditRequest()
        {
            throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("No data audit currently takes place for the Freddie Mac UCD (Loan Closing Advisor) integration."));
        }

        /// <summary>
        /// Logs the request.
        /// </summary>
        /// <param name="request">The request.</param>
        public void LogRequest(string request = null)
        {
            UcdDeliveryUtilities.LogPayload(CreateXml(this.Request.LoanClosingAdvisorRequest, forLogging: true).ToString(SaveOptions.DisableFormatting), UcdDeliveryUtilities.FreddieMacRequestLogHeader);
        }

        /// <summary>
        /// Serializes the request to an XML string ready for including in an HTTP request.
        /// </summary>
        /// <returns>The serialized XML string.</returns>
        public string SerializeRequest()
        {
            return CreateXml(this.Request.LoanClosingAdvisorRequest).ToString(SaveOptions.DisableFormatting);
        }

        /// <summary>
        /// Creates the XML content of the MISMO message to be sent to Freddie.
        /// </summary>
        /// <param name="request">The request data to send.</param>
        /// <param name="forLogging">Whether or not the XML will be logged.</param>
        /// <returns>A new XElement to serialize as the SOAP request body.</returns>
        public static XElement CreateXml(LoanClosingAdvisorLoanEvaluationRequest request, bool forLogging = false)
        {
            XElement envelope = new XElement(
                soapenv + "Envelope",
                new XAttribute(XNamespace.Xmlns + nameof(soapenv), soapenv.NamespaceName),
                CreateHeader(request.FreddieUserName, request.FreddiePassword, forLogging),
                CreateBody(request.CorrelationId, request.RelationshipWithFreddie, request.SellerIdentifier, request.UcdFile.Value, forLogging));
            return envelope;
        }

        /// <summary>
        /// Creates the XML header of the SOAP request.
        /// </summary>
        /// <param name="userName">The Freddie Mac username to include.</param>
        /// <param name="password">The Freddie Mac password to include.</param>
        /// <param name="forLogging">Whether or not the XML will be logged.</param>
        /// <returns>An XElement representing the SOAP header.</returns>
        private static XElement CreateHeader(string userName, string password, bool forLogging)
        {
            XElement header = new XElement(
                soapenv + "Header",
                new XAttribute(XNamespace.Xmlns + nameof(wsse), wsse.NamespaceName),
                new XElement(
                    wsse + "Security",
                    new XElement(
                        wsse + "UsernameToken",
                        new XElement(wsse + "Username", forLogging ? (object)new XComment("Removed for logging.") : userName),
                        new XElement(wsse + "Password", forLogging ? (object)new XComment("Removed for logging.") : password))));
            return header;
        }

        /// <summary>
        /// Creates the XML body of the SOAP request.
        /// </summary>
        /// <param name="correlationId">The correlation ID to identify the request transaction with Freddie Mac.</param>
        /// <param name="relationshipWithFreddie">The type of customer identifier to send to Freddie (Seller/Correspondent).</param>
        /// <param name="sellerIdentifier">The seller identifier with Freddie.</param>
        /// <param name="ucdFile">The UCD file MISMO content.</param>
        /// <param name="forLogging">Whether or not the XML will be logged.</param>
        /// <returns>An XElement representing the SOAP body.</returns>
        private static XElement CreateBody(string correlationId, LenderRelationshipWithFreddieMac relationshipWithFreddie, string sellerIdentifier, string ucdFile, bool forLogging)
        {
            XElement ucdMismo;
            try
            {
                ucdMismo = XElement.Parse(ucdFile);
                foreach (var attribute in ucdMismo.Attributes().Where(att => att.IsNamespaceDeclaration && att.Value == mismo))
                {
                    attribute.Remove();
                }
            }
            catch (XmlException)
            {
                return null;
            }

            if (forLogging)
            {
                var embeddedXmlElement = ucdMismo.Descendants(mismo + "EmbeddedContentXML").FirstOrDefault();
                embeddedXmlElement?.RemoveNodes();
                embeddedXmlElement?.Add(new XComment("Removed embedded file for logging size."));
            }

            CustomerIdentifierType? customerIdType = LenderRelationshipToCustomerIdentifierType[relationshipWithFreddie];

            XElement lclaLoanEvaluationServiceRequest = new XElement(
                sch + "LCLALoanEvaluationServiceRequest",
                new XAttribute(XNamespace.Xmlns + nameof(sch), sch.NamespaceName),
                new XAttribute(XNamespace.Xmlns + nameof(gse), gse.NamespaceName),
                new XAttribute(XName.Get("xmlns", string.Empty), mismo.NamespaceName),
                new XElement(sch + "CorrelationID", correlationId),
                new XElement(
                    sch + "CustomerIdentifications",
                    new XElement(
                        sch + "CustomerIdentification",
                        customerIdType == null ? null : new XElement(sch + "CustomerIdentifierType", customerIdType?.ToString("G")),
                        new XElement(sch + "CustomerIdentifier", sellerIdentifier))),
                new XElement(sch + "RequestVersion", "2.1"),
                new XElement(sch + "ResponseVersion", "2.1"),
                new XElement(
                    sch + "VendorIdentification",
                    new XElement(sch + "VendorIdentifier", VendorIdentifier),
                    new XElement(sch + "VendorName", VendorName),
                    new XElement(sch + "VendorSoftware", VendorSoftware),
                    new XElement(sch + "VendorSoftwareVersion", vendorSoftwareVersion)),
                ucdMismo);
            return new XElement(soapenv + "Body", lclaLoanEvaluationServiceRequest);
        }

        /// <summary>
        /// Loads the UCD Document from EDocs for including in the delivery.
        /// </summary>
        /// <param name="ucdEdocId">The document ID for the UCD doc.</param>
        /// <param name="user">The user account to load the document with.</param>
        /// <returns>The UDC document contents as a string.</returns>
        private static string LoadUcdDocument(Guid ucdEdocId, EDocumentRepository repository)
        {
            var doc = repository.GetGenericDocumentById(ucdEdocId);
            return File.ReadAllText(doc.GetContentPath());
        }
    }
}
