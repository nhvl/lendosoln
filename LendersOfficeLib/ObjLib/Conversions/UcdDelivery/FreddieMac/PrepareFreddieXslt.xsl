<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:custom="http://localhost" xmlns:axsl="http://www.w3.org/1999/XSL/TransformAlias" xmlns:amsxsl="http://www.w3.org/1999/XSL/MSTransformAlias" version="1.0" exclude-result-prefixes="custom">
  <xsl:param name="ResourcesDirectoryPathRelativeToVRoot" select="'/pml_shared/FreddieMacUcd/'"/>
  <xsl:output method="xml" indent="yes" />
  <xsl:namespace-alias stylesheet-prefix="axsl" result-prefix="xsl" />
  <xsl:namespace-alias stylesheet-prefix="amsxsl" result-prefix="msxsl" />
  <xsl:variable name="quot">
    <xsl:text>'</xsl:text>
  </xsl:variable>
  <msxsl:script language="CSharp" implements-prefix="custom">
    public static string Replace(string input, string oldValue, string newValue)
    {
      return input.Replace(oldValue, newValue);
    }
  </msxsl:script>

  <!-- stylesheet needs a param VirtualRoot for external resources -->
  <xsl:template match="xsl:stylesheet">
    <xsl:copy>
      <xsl:apply-templates select="@*" />
      <xsl:apply-templates select="node()[name() = 'xsl:import']"/><!--'xsl:import' element children must precede all other children of the 'xsl:stylesheet' element.-->
      <xsl:if test="not(xsl:include)"><!-- 'xsl:include' will typically be the file that actually needs VirtualRoot, and the param will get defined there. -->
        <axsl:param name="VirtualRoot"></axsl:param>
      </xsl:if>
      <xsl:apply-templates select="node()[name() != 'xsl:import']" />
    </xsl:copy>
  </xsl:template>

  <!-- The script tag containing JScript this['node-set'] = function is not valid in XSLT 1.0 -->
  <xsl:template match="msxsl:script">
    <xsl:if test="not(@language='JScript' and @implements-prefix='exslt' and starts-with(normalize-space(text()), concat('this[',$quot,'node-set',$quot,'] = function')))">
      <xsl:message terminate="yes">Unexpected script block! <xsl:value-of select="."/></xsl:message>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="@*">
    <xsl:choose>
      <!-- Links to resources need to go to VirtualRoot/pml_shared -->
      <xsl:when test="starts-with(., 'resources/') and (name(.) = 'href' or name(.) = 'src')">
        <xsl:attribute name="{name(.)}">
          <xsl:value-of select="concat('{concat($VirtualRoot,',$quot,$ResourcesDirectoryPathRelativeToVRoot,.,$quot,')}')"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:when test="contains(., 'resources/')">
        <xsl:message terminate="yes">Unhandled resources link! <xsl:value-of select="."/></xsl:message>
      </xsl:when>
      <xsl:when test="starts-with(., 'exslt:node-set(')">
        <xsl:attribute name="{name(.)}">
          <xsl:value-of select="custom:Replace(.,'exslt:','msxsl:')"/>
        </xsl:attribute>
      </xsl:when>
      <xsl:when test="contains(., 'exslt') and not(name(.)='exclude-result-prefixes')">
        <xsl:message terminate="yes">Unhandled exslt reference! <xsl:value-of select="."/></xsl:message>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Everything else gets copied normally -->
  <xsl:template match="node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>