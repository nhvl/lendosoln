@echo off
SET SourceDir=\\Megatron\lendersoffice\Integration\Freddie Mac\LoanClosingAdvisor\LCLA_S2S_Spec\v1.0.0_4\StyleSheets
robocopy "%SourceDir%\resources" resources /MIR
copy "%SourceDir%\*.xsl"

rename resources\scripts\*.txt *.js

REM C:\LendingQB\XslExport\Tools\ExcelToXml.exe is just for the XslTransform.exe input.xml style.xsl syntax
forfiles /m *v1.0.xsl /c "cmd /c C:\LendingQB\XslExport\Tools\ExcelToXml.exe @file PrepareFreddieXslt.xsl & move @file.xml @file"

forfiles /s /p resources /m Thumbs.db /c "cmd.exe /C del /a:H @path"
REM 5 directories up to C:\LendOSoln
robocopy resources ..\..\..\..\..\PML\pml_shared\FreddieMacUcd\resources /MIR
rmdir resources /s /q

PAUSE
