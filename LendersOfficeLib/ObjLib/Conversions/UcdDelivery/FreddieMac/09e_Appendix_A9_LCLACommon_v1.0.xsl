﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:sch="http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas" xmlns:lclaln="http://lclaloanresults.freddiemac.com/schema/types" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:param name="VirtualRoot" xmlns:msxsl="urn:schemas-microsoft-com:xslt" />
<xsl:template name="date">
    <xsl:param name="yyyy-mm-dd" />
		<xsl:if test="$yyyy-mm-dd !=''">
		<xsl:if test="string-length($yyyy-mm-dd) &gt;9">
		
			<xsl:variable name="yyyy" select="substring-before($yyyy-mm-dd, '-')" />
			<xsl:variable name="mm-dd" select="substring-after($yyyy-mm-dd, '-')" />
			<xsl:variable name="dd" select="substring-after($mm-dd, '-')" />
			<xsl:variable name="mm" select="substring-before($mm-dd, '-')" />
			<xsl:value-of select="concat($mm,'/',$dd,'/',$yyyy)" />
			</xsl:if>
	</xsl:if>

  </xsl:template> 
  
  
  <xsl:template name="loantermformat">
   <xsl:param name="loantermvalue" />
    
	<xsl:variable name="loanTermP" select="substring-after($loantermvalue,'P')" />
	<xsl:variable name="loanterm" select="substring-before($loanTermP,'M')" />
    <xsl:variable name="years" select="floor($loanterm div 12)" />
	<xsl:variable name="months" select="$loanterm mod 12" />
	
	<xsl:if test="$years = 1 and $months = 0">
		<xsl:value-of select="concat($years,' Year')" />
	</xsl:if>
	
	<xsl:if test="$years = 1 and $months = 1">
		<xsl:value-of select="concat($years,' Year, ' ,$months, ' Month' )" />
	</xsl:if>
	<xsl:if test="$years = 1 and $months &gt; 1">
		<xsl:value-of select="concat($years,' Year, ' ,$months, ' Months')" />
	</xsl:if>
	
	<xsl:if test="$years &gt; 1 and  $months = 0">
		<xsl:value-of select="concat($years,' Years ')" />
	</xsl:if>
	
	<xsl:if test="$years &gt; 1 and $months &gt; 1">
		<xsl:value-of select="concat($years,' Years, ' ,$months, ' Months')" />
	</xsl:if>
	
	<xsl:if test="$years &gt; 1 and $months = 1">
		<xsl:value-of select="concat($years,' Years, ' ,$months, ' Month')" />
	</xsl:if> 
      
	  <xsl:if test="$years &lt; 1 and $months &gt; 1">
		<xsl:value-of select="concat($months, ' Months')" />
	</xsl:if>
	
	<xsl:if test="$years &lt; 1 and $months = 1">
		<xsl:value-of select="concat($months, ' Month')" />
	</xsl:if>
    
  </xsl:template>
	
  </xsl:stylesheet>