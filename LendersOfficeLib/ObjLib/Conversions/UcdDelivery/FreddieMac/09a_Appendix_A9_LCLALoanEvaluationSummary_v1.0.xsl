﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" exclude-result-prefixes="exslt msxsl" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:sch="http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas" xmlns:lclaln="http://lclaloanresults.freddiemac.com/schema/types" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:import href="09e_Appendix_A9_LCLACommon_v1.0.xsl" />
  <xsl:param name="VirtualRoot" />
	
	

	<xsl:template match="sch:LCLALoanEvaluationServiceResponse">


		<html>
			<head>
				<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/style.css')}" media="screen,print" />
				<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/additonal-style.css')}" media="screen,print" />
				<script type="text/javascript" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/scripts/jquery-1.11.1.js')}" />
				<script type="text/javascript" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/scripts/jquery-ui.min.js')}" />
				<script type="text/javascript" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/scripts/messageToggle.js')}" />
				<script type="text/javascript" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/scripts/lcla-global.js')}" />
				<script type="text/javascript" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/scripts/loanEvalSummary.js')}" />
				<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/lcla-global.css')}" />
				<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/lcla_loan_eval_rslts.css')}" />
				<script type="text/javascript">
					function printpage() {
					window.print();
					}
					$(document)
					.ready(
					function() {
					var fileSubmissionDt =
					document.getElementById('fileSubDtJs').innerHTML;
					if(fileSubmissionDt != null){
					var d = new Date(fileSubmissionDt);
					if
					(!isNaN(d.getTime())) {
					document.getElementById('fileSubDtJs').innerHTML=
					(dateformat.format(d,'Y-m-d G:i:s A'));
					}else{
					document.getElementById('fileSubDtJs').innerHTML=fileSubmissionDt;
					}
					}
					});
				</script>
			</head>
			<body class="bg-greybg">
				<xsl:variable name="evalMsgCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])      +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])- count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:MessageCode='MSGGI17']           )" />
				<nav class="main-nav fixed">
					<ul class="main-nav-list" style="margin: 0px;">
						<li>
							<a href="#">
								<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-app-lcla-inv.svg')}" />
								Loan Closing Advisor
								<span class="servicemark" />

							</a>
						</li>
					</ul>
				</nav>

				<xsl:variable name="borrowerPartyRoles">
					<borrowers>
						<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
							<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL[lclaln:PartyRoleType='Borrower']">
								<borrower>
									<firstname>
										<xsl:value-of select="lclaln:INDIVIDUAL/lclaln:NAME/lclaln:FirstName" />
									</firstname>
									<lastname>
										<xsl:value-of select="lclaln:INDIVIDUAL/lclaln:NAME/lclaln:LastName" />
									</lastname>
									<fullname>
										<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
									</fullname>

									<addressline>
										<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:AddressLineText" />
									</addressline>
									<city>
										<xsl:if test="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:CityName!= ''">
											<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:CityName" />
										</xsl:if>
									</city>
									<state>
										<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:StateCode" />
									</state>
									<postalcode>
										<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:PostalCode" />
									</postalcode>

								</borrower>
							</xsl:if>
						</xsl:for-each>
					</borrowers>
				</xsl:variable>
				<section class="container">
					<div class="row" style="margin-top: 129px;">
						<div class="floating-header col-10 right">
							<span class="title" id="pageTitle">
								Loan Evaluation Summary</span>
						</div>
					</div>
					<div class="row">
						<nav class="col-2" style="margin-top: 129px;">
							<ul class="sidebar-list fixed col-2">
								<li>
									<div class="body-gray">
										Transaction ID:
										<xsl:value-of select="sch:EvaluationResults/@TransactionID" />
									</div>
									<br />
									<div class="row padding10" id="borrowerName">
										<span class="h4-label">Borrower Name</span>
										<br />
										<xsl:variable name="BorrowerPartyNodes" select="msxsl:node-set($borrowerPartyRoles)/borrowers/borrower" />
										<xsl:if test="count($BorrowerPartyNodes) &gt; 0">

											<div class="messageToggle">
												<xsl:for-each select="$BorrowerPartyNodes">
													<xsl:variable name="i" select="position()" />
													<xsl:if test="$i &lt; 3">
														<xsl:if test="$i = 2">
															<span class="body-navy">
																<xsl:text> and   </xsl:text>
															</span>
														</xsl:if>

														<span class="body-navy">
															<xsl:value-of select="firstname" />
															<xsl:text> </xsl:text>
															<xsl:value-of select="lastname" />
														</span>

														<span class="body-navy">
															<xsl:value-of select="fullname" />
														</span>
													</xsl:if>
												</xsl:for-each>
											</div>
										</xsl:if>
									</div>
									<br />
									<div class="row padding10" id="lenderloanId">
										<span class="h4-label">Lender Loan Identifier</span>
										<div class="word-wrap">
											<span class="body-navy">
												<xsl:value-of select="sch:EvaluationResults/lclaln:LOAN_IDENTIFIERS/lclaln:LOAN_IDENTIFIER[lclaln:LoanIdentifierType='LenderLoan']/lclaln:LoanIdentifier" />
											</span>
										</div>
									</div>
									<br />
									<div class="row padding10" id="batcId">
										<div class="col-6">
											<span class="h4-label">Batch ID</span>
											<div class="word-wrap">
												<span class="body-navy">
													<xsl:value-of select="sch:EvaluationResults/@BatchID" />
												</span>
											</div>
										</div>
										<div class="col-6">
											<span class=" h4-label">User ID </span>
											<br />
											<div class="word-wrap">
												<span class="body-navy col-10">
													<xsl:value-of select="sch:EvaluationResults/@UserID" />
												</span>
											</div>
										</div>
									</div>
									<br />
									<div class="row padding10" id="cdtype">
										<span class="h4-label">Closing Disclosure Type </span>
										<br />
										<div class="word-wrap">
											<span class="body-navy">
												<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureType" />
											</span>
										</div>
									</div>
									<div class="row padding10">
										<span class="h4-label">Submission Date/Time</span>
										<br />
										<div>
											<span id="fileSubDtJs" class="body-navy">
												<xsl:value-of select="sch:EvaluationResults/@SubmissionDateTime" />
											</span>
										</div>
									</div>
								</li>
								<li>
									<a href="javascript:void(0)" onclick="leftNavLinks('parentNavLink')" class="local-nav-link" id="parentNavLink">Loan Evaluation Summary</a>
									<ul class="sidebar-sublist">
										<li id="evalResultsSubList">
											<a href="javascript:void(0)" onclick="leftNavLinks('evalResultsSubLink')" id="evalResultsSubLink">Evaluation Results</a>
										</li>
										<li id="closingInfoSubList">
											<a href="javascript:void(0)" onclick="leftNavLinks('closingInfoSubLink')" id="closingInfoSubLink">Closing Information</a>
										</li>
										<li id="transInfoResultsSubList">
											<a href="javascript:void(0)" onclick="leftNavLinks('transInfoResultsSubLink')" id="transInfoResultsSubLink">Transaction Information</a>
										</li>
										<li id="loanInfoSubList">
											<a href="javascript:void(0)" onclick="leftNavLinks('loanInfoSubLink')" id="loanInfoSubLink">Loan Information</a>
										</li>
										<li id="evalMessagesSubList">
											<a href="javascript:void(0)" onclick="leftNavLinks('evalMessagesSubLink')" id="evalMessagesSubLink">
												Evaluation Messages
												<span style="padding-left:15px;">
													<xsl:value-of select="$evalMsgCount" />
												</span>
											</a>
										</li>
									</ul>
								</li>
							</ul>
						</nav>
						<section class="col-10 right">
							<div class="bg-greybg ">
								<div id="evalRsltsSec" class="row">
									<section class="container content first bg-white col-10">
										<h1 class="header-contained">Evaluation Results</h1>
										<hr style="color:#AAB7C2;" />
										<div class="row" style="padding-bottom:20px;">
											<div class="col-4">
												<div class="h4-label">GENERAL INFO</div>
												<div class="row">
													<xsl:variable name="geninfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
													<xsl:variable name="geninfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])- count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:MessageCode='MSGGI17']           )" />
													<xsl:variable name="geninfoGreenCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
													<xsl:choose>
														<xsl:when test="$geninfoGreenCount&gt; 0">
															<i class="ic-shield-green float-left green-color" alt="Green" />
														</xsl:when>
														<xsl:otherwise>
															<xsl:if test="$geninfoRedCount&gt; 0">
																<i class="ic-shield-red float-left red-color" alt="Red" />
																<span class="body-navy rslts-cnt-paddRight">
																	<xsl:value-of select="$geninfoRedCount" />
																</span>
															</xsl:if>
															<xsl:if test="$geninfoYellowCount&gt; 0">
																<i class="ic-shield-yellow float-left yellow-color" alt="Yellow" />
																<span class="body-navy rslts-cnt-paddRight">
																	<xsl:value-of select="$geninfoYellowCount" />
																</span>
															</xsl:if>
														</xsl:otherwise>
													</xsl:choose>
												</div>
											</div>
											<div class="col-4">
												<div class="h4-label">DATA QUALITY</div>
												<div class="row">
													<xsl:variable name="dqinfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
													<xsl:variable name="dqinfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
													<xsl:variable name="dqinfoGreenCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
													<xsl:choose>
														<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Conclusion='Green'">
															<i class="ic-shield-green float-left green-color" alt="Green" />
														</xsl:when>
														<xsl:otherwise>
															<xsl:if test="$dqinfoRedCount &gt; 0">
																<i class="ic-shield-red float-left red-color" alt="Red" />
																<span class="body-navy rslts-cnt-paddRight">
																	<xsl:value-of select="$dqinfoRedCount" />
																</span>
															</xsl:if>
															<xsl:if test="$dqinfoYellowCount &gt; 0">
																<i class="ic-shield-yellow float-left yellow-color" alt="Yellow" />
																<span class="body-navy rslts-cnt-paddRight">
																	<xsl:value-of select="$dqinfoYellowCount" />
																</span>
															</xsl:if>
														</xsl:otherwise>
													</xsl:choose>
												</div>
											</div>
											<div class="col-4">
												<div class="h4-label">ELIGIBILITY</div>
												<div class="row">
													<xsl:variable name="eginfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
													<xsl:variable name="eginfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
													<xsl:variable name="eginfoGreenCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
													<xsl:choose>
														<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Conclusion='Green'">
															<i class="ic-shield-green float-left green-color" alt="Green" />
														</xsl:when>
														<xsl:otherwise>
															<xsl:if test="$eginfoRedCount &gt; 0">
																<i class="ic-shield-red float-left red-color" alt="Red" />
																<span class="body-navy rslts-cnt-paddRight">
																	<xsl:value-of select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
																</span>
															</xsl:if>
															<xsl:if test="$eginfoYellowCount &gt; 0">
																<i class="ic-shield-yellow float-left yellow-color" alt="Yellow" />
																<span class="body-navy rslts-cnt-paddRight">
																	<xsl:value-of select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
																</span>
															</xsl:if>
														</xsl:otherwise>
													</xsl:choose>
												</div>
											</div>
										</div>
										<div class="row" style="padding-bottom:0px;">
											<div class="col-4">
												<a class="button btn-secondary-sm" id="giLink" href="javascript:void(0)">RESULTS</a>
											</div>
											<div class="col-4">
												<a class="button btn-secondary-sm" id="dqLink" href="javascript:void(0)">RESULTS</a>
											</div>
											<div class="col-4">
												<a class="button btn-secondary-sm" id="eligLink" href="javascript:void(0)">RESULTS</a>
											</div>
										</div>
										<div id="infoTabAnchor" />
									</section>
								</div>
							</div>
							<div class="row">
								<div id="infoTab" class="col-12 tabs-container">
									<ul class="tabs-list" id="evalInfoTabs">
										<li class="active">
											<a id="#closingInfo" href="javascript:void(0);">Closing Information</a>
										</li>
										<li class="">
											<a id="#transInfo" href="javascript:void(0);">Transaction Information</a>
										</li>
										<li>
											<a id="#loanInfo" href="javascript:void(0);">Loan Information</a>
										</li>
									</ul>
									<div id="closingInfoTab" class="content bg-white active">
										<div class="row">
											<div class="col-3">
												<div class="h4-label">DATE ISSUED</div>
												<div>
													<span id="dateIssued" class="body-navy">
														<xsl:copy>
															<xsl:call-template name="date">
																<xsl:with-param name="yyyy-mm-dd" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:INTEGRATED_DISCLOSURE/lclaln:INTEGRATED_DISCLOSURE_DETAIL/lclaln:IntegratedDisclosureIssuedDate" />
															</xsl:call-template>
														</xsl:copy>
													</span>
												</div>
											</div>
											<div class="col-3">
												<div class="h4-label">CLOSING DATE</div>
												<div>
													<span id="closingDate" class="body-navy">
														<xsl:copy>
															<xsl:call-template name="date">
																<xsl:with-param name="yyyy-mm-dd" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:ClosingDate" />
															</xsl:call-template>
														</xsl:copy>
													</span>
												</div>
											</div>
											<div class="col-3">
												<div class="h4-label">DISBURSEMENT DATE</div>
												<div>
													<span id="disbursementDate" class="body-navy">

														<xsl:copy>
															<xsl:call-template name="date">
																<xsl:with-param name="yyyy-mm-dd" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:DisbursementDate" />
															</xsl:call-template>
														</xsl:copy>
													</span>
												</div>
											</div>
											<div class="col-3">
												<div class="h4-label">SETTLEMENT AGENT</div>
												<div class="word-wrap">
													<span class="body-navy">
														<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
															<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='ClosingAgent'">
																<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
															</xsl:if>
														</xsl:for-each>
													</span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-3">
												<div class="h4-label">FILE #</div>
												<div class="word-wrap">
													<span class="body-navy">
														<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:ClosingAgentOrderNumberIdentifier" />
													</span>
												</div>
											</div>
											<div class="col-3">
												<div class="h4-label">PROPERTY</div>
												<div class="body-navy ellipses" title="17 Cherry Hill Court">
													<span>
														<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:AddressLineText" />
													</span>
												</div>
												<div class="body-navy word-wrap">
													<span>
														<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:CityName != ''">
															<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:CityName" />
															,
															<xsl:text> </xsl:text>
														</xsl:if>
														<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:StateCode" />
														<xsl:text> </xsl:text>
														<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:PostalCode" />
													</span>
												</div>
											</div>
											<div class="col-3">
												<xsl:choose>
													<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType != 'Refinance'">

														<div class="h4-label">SALE PRICE</div>

														<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:SalePrice">

															<div class="body-navy word-wrap">
																<xsl:text>$</xsl:text>
																<xsl:value-of select="format-number(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:SalePrice,&quot;###,###.##&quot;)" />
															</div>
														</xsl:if>
													</xsl:when>
													<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType = 'Refinance'">
														<div class="h4-label">APPRAISED PROP. VALUE</div>

														<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:AppraisedValue">

															<div class="body-navy word-wrap">
																<xsl:text>$</xsl:text>
																<xsl:value-of select="format-number(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:AppraisedValue,'###,###.##')" />
															</div>
														</xsl:if>
													</xsl:when>

												</xsl:choose>
											</div>
										</div>
									</div>
									<div id="transInfoTab" class="content bg-white">
										<div class="row">
											<xsl:variable name="BorrowerPartyNodes" select="msxsl:node-set($borrowerPartyRoles)/borrowers/borrower" />
											<xsl:if test="count($BorrowerPartyNodes) &gt; 0">
												<div class="col-4">
													<div class="h4-label">BORROWER</div>
													<div class="messageToggle">
														<xsl:for-each select="$BorrowerPartyNodes">
															<xsl:variable name="i" select="position()" />
															<xsl:if test="$i &lt; 3">
																<xsl:if test="$i = 2">
																	<span class="body-navy">
																		<xsl:text> and   </xsl:text>
																	</span>
																</xsl:if>

																<span class="body-navy">
																	<xsl:value-of select="firstname" />
																	<xsl:text> </xsl:text>
																	<xsl:value-of select="lastname" />
																</span>

																<span class="body-navy">
																	<xsl:value-of select="fullname" />
																</span>
															</xsl:if>
														</xsl:for-each>
													</div>
													<xsl:for-each select="$BorrowerPartyNodes">

														<xsl:if test="position() = 1">

															<div class="word-wrap">
																<span class="body-navy">

																	<xsl:value-of select="addressline" />
																</span>
															</div>
															<div class="word-wrap">
																<span class="body-navy">


																	<xsl:if test="city!= ''">
																		<xsl:value-of select="city" />
																		<xsl:text>, </xsl:text>
																	</xsl:if>
																	<xsl:value-of select="state" />
																	<xsl:text> </xsl:text>
																	<xsl:value-of select="postalcode" />
																</span>
															</div>
														</xsl:if>
													</xsl:for-each>
												</div>

											</xsl:if>

											<xsl:variable name="SellersPartyRoles">
												<sellers>
													<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
														<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL[lclaln:PartyRoleType='PropertySeller']">
															<seller>
																<firstname>
																	<xsl:value-of select="lclaln:INDIVIDUAL/lclaln:NAME/lclaln:FirstName" />
																</firstname>
																<lastname>
																	<xsl:value-of select="lclaln:INDIVIDUAL/lclaln:NAME/lclaln:LastName" />
																</lastname>
																<fullname>
																	<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
																</fullname>

																<addressline>
																	<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:AddressLineText" />
																</addressline>
																<city>
																	<xsl:if test="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:CityName!= ''">
																		<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:CityName" />
																		
																		<xsl:text> </xsl:text>
																	</xsl:if>
																</city>
																<state>
																	<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:StateCode" />
																</state>
																<postalcode>
																	<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:PostalCode" />
																</postalcode>

															</seller>
														</xsl:if>
													</xsl:for-each>
												</sellers>
											</xsl:variable>
											<xsl:variable name="SellerPartyNodes" select="msxsl:node-set($SellersPartyRoles)/sellers/seller" />
											<xsl:if test="count($SellerPartyNodes) &gt; 0  and sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType !='Refinance'">
												<div class="col-4">
													<div class="h4-label">SELLER</div>
													<div class="messageToggle">
														<xsl:for-each select="$SellerPartyNodes">
															<xsl:variable name="i" select="position()" />
															<xsl:if test="$i &lt; 3">
																<xsl:if test="$i = 2">
																	<span class="body-navy">
																		<xsl:text> and   </xsl:text>
																	</span>
																</xsl:if>

																<span class="body-navy">
																	<xsl:value-of select="firstname" />
																	<xsl:text> </xsl:text>
																	<xsl:value-of select="lastname" />
																</span>

																<span class="body-navy">
																	<xsl:value-of select="fullname" />
																</span>
															</xsl:if>
														</xsl:for-each>
													</div>
													<xsl:for-each select="$SellerPartyNodes">

														<xsl:if test="position() = 1">

															<div class="word-wrap">
																<span class="body-navy">
																	<xsl:value-of select="addressline" />
																</span>
															</div>
															<div class="word-wrap">
																<span class="body-navy">


																	<xsl:if test="city!= ''">
																		<xsl:value-of select="city" />
																		,
																		<xsl:text> </xsl:text>
																	</xsl:if>
																	<xsl:value-of select="state" />
																	<xsl:text> </xsl:text>
																	<xsl:value-of select="postalcode" />
																</span>
															</div>
														</xsl:if>
													</xsl:for-each>
												</div>

											</xsl:if>
											<div class="col-4">
												<div class="h4-label">LENDER</div>
												<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
													<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='NotePayTo'">
														<div class="word-wrap">
															<span class="body-navy">
																<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
															</span>
														</div>
													</xsl:if>
												</xsl:for-each>
											</div>
										</div>
									</div>
									<div id="loanInfoTab" class="content bg-white">
										<div class="row">
											<div class="col-3">
												<div class="h4-label">LOAN TERM</div>
												<div class="word-wrap messageToggle">
													<span class="body-navy">
														<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm !='' ">
															<xsl:copy>
																<xsl:call-template name="loantermformat">

																	<xsl:with-param name="loantermvalue" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm" />
																</xsl:call-template>
															</xsl:copy>

														</xsl:if>
														<xsl:if test="(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm !='')and (sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum !='')">
															<span> - </span>
														</xsl:if>

														<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum !='' ">
															<xsl:copy>
																<xsl:call-template name="loantermformat">

																	<xsl:with-param name="loantermvalue" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum" />
																</xsl:call-template>
															</xsl:copy>
															<!--xsl:with-param name="loanterm" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:MATURITY/lclaln:MATURITY_RULE/lclaln:LoanMaturityPeriodCount"/ -->

														</xsl:if>
													</span>

												</div>
											</div>
											<div class="col-3">
												<div class="h4-label">PURPOSE</div>
												<div class="word-wrap">
													<span class="body-navy">
														<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType" />
													</span>
												</div>
											</div>
											<div class="col-3">
												<div class="h4-label">PRODUCT</div>
												<div class="word-wrap">
													<span class="body-navy">
														<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:AMORTIZATION/lclaln:AMORTIZATION_RULE/lclaln:AmortizationType" />

													</span>
												</div>
											</div>
											<div class="col-3">
												<div class="h4-label">LOAN TYPE</div>
												<div class="word-wrap">
													<span class="body-navy">
														<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedMortgageTerm/lclaln:LoanType" />
													</span>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-3">
												<div class="h4-label">LOAN ID #</div>
												<div class="word-wrap">
													<span class="body-navy">
														<xsl:value-of select="sch:EvaluationResults/lclaln:LOAN_IDENTIFIERS/lclaln:LOAN_IDENTIFIER[lclaln:LoanIdentifierType='LenderLoan']/lclaln:LoanIdentifier" />
													</span>
												</div>
											</div>
											<div class="col-3">
												<div class="h4-label">MIC #</div>
												<div class="word-wrap">
													<span class="body-navy">
														<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:MI_DATA/lclaln:MI_DATA_DETAIL/lclaln:MICertificateIdentifier" />
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="evalMsgsCount" class="row">
								<div class="col-12" style="padding-bottom:40px">
									<div class="row section-header">
										<span class="evalMsgSecHeader">Evaluation Messages</span>
										<span class="evalMsgCount">
											<xsl:value-of select="$evalMsgCount" />
										</span>
									</div>
								</div>
								<div id="msgsTabAnchor" />
							</div>
							<div class="row">
								<div id="msgsTab" class="col-12 tabs-container">
									<ul class="tabs-list" id="evalMsgsTab">
										<li class="active">
											<a id="#generalMsgs" href="javascript:void(0)">General Info</a>
										</li>
										<li class="">
											<a id="#dataQualityMsgs" href="javascript:void(0)">Data Quality</a>
										</li>
										<li class="">
											<a id="#eligibilityMsgs" href="javascript:void(0)">Eligibility</a>
										</li>
										<li class="right">
											<div class="row">
												<div style="position:relative;">
													<span class="h4-label">MESSAGE VIEW</span>
												</div>
												<div style="margin-top:1px">
													<button id="businessMsgs" class="msgToggleBtn msgToggleActive" style="border-right:0px;">BUSINESS</button>
													<button id="technicalMsgs" style="border-radius: 0px 5px 5px 0px;" class="msgToggleBtn">TECHNICAL</button>
												</div>
											</div>
										</li>
									</ul>
									<div id="generalMsgsTab" class="content bg-white active">
										<div class="row">
											<div class="col-12">
												<div class="row active">
													<xsl:variable name="giinfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])- count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:MessageCode='MSGGI17']           )" />
													<xsl:variable name="giinfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
													<div class="row" id="sec_giTable">
														<div class="row loanSection" id="giRowDD" style="padding-bottom:15px;margin-bottom:15px;" onclick="toggleSection(event,'giTable')">
															<div class="col-10">
																<div class="row">
																	<h1 class="header-contained" style="margin:0px;">Results</h1>
																</div>
															</div>
															<div class="col-2">
																<div class="row">
																	<xsl:choose>
																		<xsl:when test="$giinfoRedCount&gt;0 and $giinfoYellowCount = 0">
																			<div class="col-10 icon-padding">
																				<div class="filledRCircle right">
																					<span>
																						<xsl:value-of select="$giinfoRedCount" />
																					</span>
																				</div>
																			</div>
																		</xsl:when>
																		<xsl:when test="$giinfoRedCount=0 and $giinfoYellowCount&gt;0">
																			<div class="col-10 icon-padding">
																				<div class="filledYCircle right">
																					<span class="">
																						<xsl:value-of select="$giinfoYellowCount" />
																					</span>
																				</div>
																			</div>
																		</xsl:when>
																		<xsl:when test="$giinfoRedCount&gt;0 and $giinfoYellowCount&gt;0">
																			<div class="col-6 icon-padding">
																				<div class="filledRCircle right">
																					<span>
																						<xsl:value-of select="$giinfoRedCount" />
																					</span>
																				</div>
																			</div>
																			<div class="col-4 icon-padding">
																				<div class="filledYCircle right">
																					<span>
																						<xsl:value-of select="$giinfoYellowCount" />
																					</span>
																				</div>
																			</div>
																		</xsl:when>
																		<xsl:otherwise>
																			<div class="col-10 icon-padding">
																				<div class="filledGCircle right" />
																			</div>
																		</xsl:otherwise>
																	</xsl:choose>
																	<div class="col-2 right">
																		<span>
																			<a style="text-decoration:none" id="giDD" href="javascript:void(0)">
																				<i class="ic-dropdown  ic-dd-rotate-180 parentDDIcon" style="font-size: 24px; color: #006693;" />
																			</a>
																		</span>
																	</div>
																</div>
															</div>
														</div>

														<xsl:variable name="GIRedMessages">
															<messages>
																<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Red']">
																	<message>
																		<code>
																			<xsl:value-of select="lclaln:MessageCode" />
																		</code>
																		<businessMessage>
																			<xsl:value-of select="lclaln:MessageText" />
																		</businessMessage>
																		<technicalMessage>
																			<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																		</technicalMessage>
																	</message>
																</xsl:for-each>
															</messages>
														</xsl:variable>
														<xsl:variable name="GIYellowMessages">
															<messages>
																<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Yellow']">
																	<xsl:if test="lclaln:MessageCode != 'MSGGI17'">
																		<message>
																			<code>
																				<xsl:value-of select="lclaln:MessageCode" />
																			</code>
																			<businessMessage>
																				<xsl:value-of select="lclaln:MessageText" />
																			</businessMessage>
																			<technicalMessage>
																				<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																			</technicalMessage>
																		</message>
																	</xsl:if>
																</xsl:for-each>
															</messages>
														</xsl:variable>
														<xsl:variable name="GIGreenMessages">
															<messages>
																<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Green']">
																	<message>
																		<code>
																			<xsl:value-of select="lclaln:MessageCode" />
																		</code>
																		<businessMessage>
																			<xsl:value-of select="lclaln:MessageText" />
																		</businessMessage>
																		<technicalMessage>
																			<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																		</technicalMessage>
																	</message>
																</xsl:for-each>
															</messages>
														</xsl:variable>
														<xsl:variable name="giinfoRedNodes" select="msxsl:node-set($GIRedMessages)/messages/message" />
														<xsl:variable name="giinfoYellowNodes" select="msxsl:node-set($GIYellowMessages)/messages/message" />
														<xsl:variable name="giinfoGreenNodes" select="msxsl:node-set($GIGreenMessages)/messages/message" />
														<div class="row" id="giRow">
															<xsl:if test="count($giinfoGreenNodes)&gt;0">
																<div class="row section-sub-headerDiv" style="padding-bottom:10px; padding-top:0px">
																	<div id="Green" class="col-12 section-sub-header">
																		<div class="col-12">
																			<div class="col-2">
																				<span class="sectionStatus">Green</span>

																				<span>
																					<i class="ic-pass green-color" style="font-size:24px;" />
																				</span>
																			</div>
																			<div class="col-10">
																				<span class="right">
																					<a style="text-decoration:none;margin-top:5px;" id="Green" href="javascript:void(0);">
																						<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" style="padding-right:15px; " />
																					</a>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="msgTablepadding">
																	<table class="table msgTablePaddingBottom">
																		<thead class="table-header">
																			<tr>
																				<th>MESSAGE CODE</th>
																				<th>MESSAGES</th>
																			</tr>
																		</thead>
																		<tbody>
																			<xsl:for-each select="$giinfoGreenNodes">
																				<xsl:variable name="technicalMessage" select="technicalMessage" />
																				<tr id="row_criticalSec_Green">
																					<td>
																						<xsl:value-of select="code" />
																					</td>
																					<td colspan="2">
																						<div class="messageToggleContainer">
																							<div class="col-11">
																								<div class="messageToggle">
																									<xsl:value-of select="businessMessage" />
																								</div>
																								<xsl:if test="$technicalMessage !=''">
																									<div class="messageToggle techMessageToggle  messageToggleHidden">
																										<xsl:value-of select="technicalMessage" />
																									</div>
																								</xsl:if>
																							</div>
																							<div class="col-1">
																								<xsl:if test="$technicalMessage !=''">
																									<a class="messageToggleSwitch right" href="javascript:void(0)">
																										<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																									</a>
																								</xsl:if>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</xsl:for-each>
																		</tbody>
																	</table>
																</div>


															</xsl:if>
															<xsl:if test="count($giinfoRedNodes)&gt;0">
																<div class="row section-sub-headerDiv" style="padding-bottom:10px; padding-top:0px">
																	<div id="Red" class="col-12 section-sub-header">
																		<div class="col-12">
																			<div class="col-2">
																				<span class="sectionStatus">Red</span>

																				<span>
																					<i class="ic-critical red-color" style="font-size:24px;" />
																				</span>
																			</div>
																			<div class="col-10">
																				<span class="right">
																					<a style="text-decoration:none;margin-top:5px;" id="Red" href="javascript:void(0);">
																						<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" style="padding-right:15px; " />
																					</a>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="msgTablepadding">
																	<table class="table msgTablePaddingBottom">
																		<thead class="table-header">
																			<tr>
																				<th>MESSAGE CODE</th>
																				<th>MESSAGES</th>
																			</tr>
																		</thead>
																		<tbody>
																			<xsl:for-each select="$giinfoRedNodes">
																				<xsl:variable name="technicalMessage" select="technicalMessage" />
																				<tr id="row_criticalSec_Red">
																					<td>
																						<xsl:value-of select="code" />
																					</td>
																					<td colspan="2">
																						<div class="messageToggleContainer">
																							<div class="col-11">
																								<div class="messageToggle">
																									<xsl:value-of select="businessMessage" />
																								</div>
																								<xsl:if test="$technicalMessage !=''">
																									<div class="messageToggle techMessageToggle  messageToggleHidden">
																										<xsl:value-of select="technicalMessage" />
																									</div>
																								</xsl:if>
																							</div>
																							<div class="col-1">
																								<xsl:if test="$technicalMessage !=''">
																									<a class="messageToggleSwitch right" href="javascript:void(0)">
																										<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																									</a>
																								</xsl:if>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</xsl:for-each>
																		</tbody>
																	</table>
																</div>

															</xsl:if>
															<xsl:if test="count($giinfoYellowNodes)&gt;0">
																<div class="row section-sub-headerDiv" style="padding-bottom:10px; padding-top:0px">
																	<div id="Yellow" class="col-12 section-sub-header">
																		<div class="col-12">
																			<div class="col-2">
																				<span class="sectionStatus">Yellow</span>

																				<span>
																					<i class="ic-error yellow-color" style="font-size:24px;" />
																				</span>
																			</div>
																			<div class="col-10">
																				<span class="right">
																					<a style="text-decoration:none;margin-top:5px;" id="Yellow" href="javascript:void(0);">
																						<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" style="padding-right:15px; " />
																					</a>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="msgTablepadding">
																	<table class="table msgTablePaddingBottom">
																		<thead class="table-header">
																			<tr>
																				<th>MESSAGE CODE</th>
																				<th>MESSAGES</th>
																			</tr>
																		</thead>
																		<tbody>
																			<xsl:for-each select="$giinfoYellowNodes">
																				<xsl:variable name="technicalMessage" select="technicalMessage" />
																				<tr id="row_criticalSec_Yellow">
																					<td>
																						<xsl:value-of select="code" />
																					</td>
																					<td colspan="2">
																						<div class="messageToggleContainer">
																							<div class="col-11">
																								<div class="messageToggle">
																									<xsl:value-of select="businessMessage" />
																								</div>
																								<xsl:if test="$technicalMessage !=''">
																									<div class="messageToggle techMessageToggle  messageToggleHidden">
																										<xsl:value-of select="technicalMessage" />
																									</div>
																								</xsl:if>
																							</div>
																							<div class="col-1">
																								<xsl:if test="$technicalMessage !=''">
																									<a class="messageToggleSwitch right" href="javascript:void(0)">
																										<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																									</a>
																								</xsl:if>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</xsl:for-each>
																		</tbody>
																	</table>
																</div>
															</xsl:if>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<xsl:variable name="dqinfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
									<xsl:variable name="dqinfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
									<xsl:variable name="dqinfoGreenCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
									<span style="display:none;" id="dqCount">
										<xsl:value-of select="$dqinfoRedCount+$dqinfoRedCount+$dqinfoGreenCount" />
									</span>
									<div id="dataQualityMsgsTab" class="content bg-white">
										<div class="row">
											<div class="col-12">
												<div class="row" style="text-align:right;">
													<a id="showSections" class="button showHideBtn showAllInActive" href="javascript:showCollapseSections('show')">
													</a>
													<a id="hideSections" class="button showHideBtn hideAllActive" href="javascript:showCollapseSections('hide')" />
												</div>
												<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message">
													<xsl:variable name="i" select="position()" />
													<xsl:variable name="sectionName" select="lclaln:Section/lclaln:SectionName" />
													<xsl:variable name="redMessages">
														<messages>
															<xsl:for-each select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Red']">
																<message>
																	<code>
																		<xsl:value-of select="lclaln:MessageCode" />
																	</code>
																	<businessMessage>
																		<xsl:value-of select="lclaln:MessageText" />
																	</businessMessage>
																	<technicalMessage>
																		<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																	</technicalMessage>
																</message>
															</xsl:for-each>
														</messages>
													</xsl:variable>
													<xsl:variable name="yellowMessages">
														<messages>
															<xsl:for-each select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Yellow']">
																<message>
																	<code>
																		<xsl:value-of select="lclaln:MessageCode" />
																	</code>
																	<businessMessage>
																		<xsl:value-of select="lclaln:MessageText" />
																	</businessMessage>
																	<technicalMessage>
																		<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																	</technicalMessage>
																</message>
															</xsl:for-each>
														</messages>
													</xsl:variable>
													<xsl:variable name="greenMessages">
														<messages>
															<xsl:for-each select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Green']">
																<message>
																	<code>
																		<xsl:value-of select="lclaln:MessageCode" />
																	</code>
																	<businessMessage>
																		<xsl:value-of select="lclaln:MessageText" />
																	</businessMessage>
																	<technicalMessage>
																		<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																	</technicalMessage>
																</message>
															</xsl:for-each>
														</messages>
													</xsl:variable>
													<div id="sec_{$i}">
														<xsl:if test="lclaln:Section/lclaln:SectionName != following::*[1]/lclaln:Section/lclaln:SectionName">
															<xsl:variable name="j" select="position()" />
															<xsl:variable name="sectionDetail" select="lclaln:Section/lclaln:SectionName" />
															<xsl:variable name="sectionValue" select="lclaln:Section/lclaln:SectionValue" />
															<xsl:variable name="dqinfoRedNodes" select="msxsl:node-set($redMessages)/messages/message" />
															<xsl:variable name="dqinfoYellowNodes" select="msxsl:node-set($yellowMessages)/messages/message" />
															<xsl:variable name="dqinfoGreenNodes" select="msxsl:node-set($greenMessages)/messages/message" />
															<div id="dqRowDD_{$i}" class="row loanSection" style="padding-bottom:15px;margin-bottom:15px;" onclick="toggleSection(event,{$i})">
																<div class="col-10">
																	<div>
																		SECTION
																		<xsl:value-of select="$sectionValue" />
																		-
																		<xsl:value-of select="$sectionDetail" />

																	</div>
																</div>
																<div class="col-2">
																	<div class="row">

																		<xsl:variable name="dqinfoSectionRedCount" select="count($dqinfoRedNodes)" />
																		<xsl:variable name="dqinfoSectionYellowCount" select="count($dqinfoYellowNodes)" />
																		<xsl:variable name="dqinfoSectionGreenCount" select="count($dqinfoGreenNodes)" />

																		<xsl:choose>
																			<xsl:when test="$dqinfoSectionRedCount&gt;0 and $dqinfoSectionYellowCount = 0">
																				<div class="col-10 icon-padding">
																					<div class="filledRCircle right">
																						<span>
																							<xsl:value-of select="$dqinfoSectionRedCount" />
																						</span>
																					</div>
																				</div>
																			</xsl:when>
																			<xsl:when test="$dqinfoSectionRedCount=0 and $dqinfoSectionYellowCount&gt;0">
																				<div class="col-10 icon-padding">
																					<div class="filledYCircle right">
																						<span class="">
																							<xsl:value-of select="$dqinfoSectionYellowCount" />
																						</span>
																					</div>
																				</div>
																			</xsl:when>
																			<xsl:when test="$dqinfoSectionRedCount&gt;0 and $dqinfoSectionYellowCount&gt;0">
																				<div class="col-6 icon-padding">
																					<div class="filledRCircle right">
																						<span>
																							<xsl:value-of select="$dqinfoSectionRedCount" />
																						</span>
																					</div>
																				</div>
																				<div class="col-4 icon-padding">
																					<div class="filledYCircle right">
																						<span>
																							<xsl:value-of select="$dqinfoSectionYellowCount" />
																						</span>
																					</div>
																				</div>
																			</xsl:when>
																			<xsl:otherwise>
																				<div class="col-10 icon-padding">
																					<div class="filledGCircle right">
																					</div>

																				</div>
																			</xsl:otherwise>
																		</xsl:choose>

																		<div class="col-2 right">
																			<span>
																				<a style="text-decoration:none;margin-top:5px;" id="{$sectionDetail}" href="javascript:void(0)">
																					<i class="ic-dropdown  ic-dd-rotate-180 parentDDIcon" style="font-size: 24px; color: #006693;" />
																				</a>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row" style="padding-bottom:0px;">
																<div class="col-12">
																	<div class="row" style="padding-bottom:0px;">
																		<xsl:variable name="dqinfoSectionGreenCount1" select="count($dqinfoGreenNodes)" />
																		<xsl:variable name="dqinfoSectionRedCount1" select="count($dqinfoRedNodes)" />
																		<xsl:variable name="dqinfoSectionYellowCount1" select="count($dqinfoYellowNodes)" />

																		<xsl:choose>

																			<xsl:when test="$dqinfoSectionGreenCount1&gt; 0">
																				<div class="row section-sub-headerDiv" style="padding-bottom:10px;" id="Green">

																					<div id="Green_colorCount_{$sectionValue}" class=" col-12 section-sub-header">
																						<div class="col-2">

																							<span class="sectionStatus">
																								<xsl:value-of select="'Green'" />
																							</span>
																							<span>
																								<i class="ic-pass green-color" style="font-size:24px;" />
																							</span>

																						</div>
																						<div class="col-10">
																							<span class="right">
																								<a style="text-decoration:none;margin-top:5px;" id="Green" href="javascript:void(0);">
																									<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" />
																								</a>
																							</span>
																						</div>
																					</div>

																				</div>
																				<div class="msgTablepadding">
																					<table class="table msgTablePaddingBottom">
																						<thead class="table-header">
																							<tr>
																								<th>MESSAGE CODE</th>
																								<th>MESSAGES</th>
																							</tr>
																						</thead>
																						<tbody>
																							<xsl:for-each select="msxsl:node-set($greenMessages)/messages/message">

																								<xsl:variable name="technicalMessage" select="technicalMessage" />
																								<tr>
																									<td>
																										<xsl:value-of select="code" />
																									</td>
																									<td colspan="2">
																										<div class="messageToggleContainer">
																											<div class="col-11">
																												<div class="messageToggle">
																													<xsl:value-of select="businessMessage" />
																												</div>
																												<xsl:if test="$technicalMessage !=''">
																													<div class="messageToggle techMessageToggle  messageToggleHidden">
																														<xsl:value-of select="$technicalMessage" />
																													</div>
																												</xsl:if>
																											</div>
																											<div class="col-1">
																												<xsl:if test="$technicalMessage !=''">
																													<a class="messageToggleSwitch right" href="javascript:void(0)">
																														<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																													</a>
																												</xsl:if>
																											</div>
																										</div>
																									</td>
																								</tr>
																							</xsl:for-each>
																						</tbody>
																					</table>
																				</div>
																			</xsl:when>


																			<xsl:otherwise>

																				<xsl:if test="$dqinfoSectionRedCount1&gt;0">
																					<div class="row section-sub-headerDiv" style="padding-bottom:10px;" id="Red">

																						<div id="Red_colorCount_{$sectionValue}" class=" col-12 section-sub-header">
																							<div class="col-2">

																								<span class="sectionStatus">
																									<xsl:value-of select="'Red'" />
																								</span>
																								<span>
																									<i class="ic-critical red-color" style="font-size:24px;" />
																								</span>

																							</div>
																							<div class="col-10">
																								<span class="right">
																									<a style="text-decoration:none;margin-top:5px;" id="Red" href="javascript:void(0);">
																										<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" />
																									</a>
																								</span>
																							</div>
																						</div>

																					</div>
																					<div class="msgTablepadding">
																						<table class="table msgTablePaddingBottom">
																							<thead class="table-header">
																								<tr>
																									<th>MESSAGE CODE</th>
																									<th>MESSAGES</th>
																								</tr>
																							</thead>
																							<tbody>
																								<xsl:for-each select="msxsl:node-set($redMessages)/messages/message">

																									<xsl:variable name="technicalMessage" select="technicalMessage" />
																									<tr>
																										<td>
																											<xsl:value-of select="code" />
																										</td>
																										<td colspan="2">
																											<div class="messageToggleContainer">
																												<div class="col-11">
																													<div class="messageToggle">
																														<xsl:value-of select="businessMessage" />
																													</div>
																													<xsl:if test="$technicalMessage !=''">
																														<div class="messageToggle techMessageToggle  messageToggleHidden">
																															<xsl:value-of select="$technicalMessage" />
																														</div>
																													</xsl:if>
																												</div>
																												<div class="col-1">
																													<xsl:if test="$technicalMessage !=''">
																														<a style="text-decoration:none;margin-top:5px;" class="messageToggleSwitch right" href="javascript:void(0)">
																															<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																														</a>
																													</xsl:if>
																												</div>
																											</div>
																										</td>
																									</tr>
																								</xsl:for-each>
																							</tbody>
																						</table>
																					</div>
																				</xsl:if>
																				<xsl:if test="$dqinfoSectionYellowCount1&gt;0">
																					<div class="row section-sub-headerDiv" style="padding-bottom:10px;">

																						<div id="Yellow_colorCount_{$sectionValue}" class=" col-12 section-sub-header">
																							<div class="col-2">

																								<span class="sectionStatus">
																									<xsl:value-of select="'Yellow'" />
																								</span>
																								<span>
																									<i class="ic-error yellow-color" style="font-size:24px;" />
																								</span>

																							</div>
																							<div class="col-10">
																								<span class="right">
																									<a style="text-decoration:none;margin-top:5px;" id="Yellow" href="javascript:void(0);">
																										<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" />
																									</a>
																								</span>
																							</div>
																						</div>

																					</div>
																					<div class="msgTablepadding">
																						<table class="table msgTablePaddingBottom">
																							<thead class="table-header">
																								<tr>
																									<th>MESSAGE CODE</th>
																									<th>MESSAGES</th>
																								</tr>
																							</thead>
																							<tbody>
																								<xsl:for-each select="msxsl:node-set($yellowMessages)/messages/message">
																									<xsl:variable name="technicalMessage" select="technicalMessage" />
																									<tr>
																										<td>
																											<xsl:value-of select="code" />
																										</td>
																										<td colspan="2">
																											<div class="messageToggleContainer">
																												<div class="col-11">
																													<div class="messageToggle">
																														<xsl:value-of select="businessMessage" />
																													</div>
																													<xsl:if test="$technicalMessage !=''">
																														<div class="messageToggle techMessageToggle  messageToggleHidden">
																															<xsl:value-of select="$technicalMessage" />
																														</div>
																													</xsl:if>
																												</div>
																												<div class="col-1">
																													<xsl:if test="$technicalMessage !=''">
																														<a style="text-decoration:none;margin-top:5px;" class="messageToggleSwitch right" href="javascript:void(0)">
																															<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																														</a>
																													</xsl:if>
																												</div>
																											</div>
																										</td>
																									</tr>
																								</xsl:for-each>
																							</tbody>
																						</table>
																					</div>
																				</xsl:if>
																			</xsl:otherwise>
																		</xsl:choose>

																	</div>
																</div>
															</div>
														</xsl:if>
														<xsl:if test="lclaln:Section/lclaln:SectionValue = 'GSE' and preceding-sibling::*[1]/lclaln:Section/lclaln:SectionValue !='GSE'">
															<xsl:variable name="j" select="position()" />
															<xsl:variable name="sectionDetail" select="lclaln:Section/lclaln:SectionName" />
															<xsl:variable name="sectionValue" select="lclaln:Section/lclaln:SectionValue" />
															<xsl:variable name="dqinfoRedNodes" select="msxsl:node-set($redMessages)/messages/message" />
															<xsl:variable name="dqinfoYellowNodes" select="msxsl:node-set($yellowMessages)/messages/message" />
															<xsl:variable name="dqinfoGreenNodes" select="msxsl:node-set($greenMessages)/messages/message" />

															<div id="dqRowDD_{$i}" class="row loanSection" style="padding-bottom:15px;margin-bottom:15px;" onclick="toggleSection(event,{$i})">
																<div class="col-10">
																	<div>
																		<xsl:value-of select="$sectionValue" />
																		-
																		<xsl:value-of select="$sectionDetail" />

																	</div>
																</div>
																<div class="col-2">
																	<div class="row">

																		<xsl:variable name="dqinfoSectionRedCount" select="count($dqinfoRedNodes)" />
																		<xsl:variable name="dqinfoSectionYellowCount" select="count($dqinfoYellowNodes)" />
																		<xsl:variable name="dqinfoSectionGreenCount" select="count($dqinfoGreenNodes)" />

																		<xsl:choose>
																			<xsl:when test="$dqinfoSectionRedCount&gt;0 and $dqinfoSectionYellowCount = 0">
																				<div class="col-10 icon-padding">
																					<div class="filledRCircle right">
																						<span>
																							<xsl:value-of select="$dqinfoSectionRedCount" />
																						</span>
																					</div>
																				</div>
																			</xsl:when>
																			<xsl:when test="$dqinfoSectionRedCount=0 and $dqinfoSectionYellowCount&gt;0">
																				<div class="col-10 icon-padding">
																					<div class="filledYCircle right">
																						<span class="">
																							<xsl:value-of select="$dqinfoSectionYellowCount" />
																						</span>
																					</div>
																				</div>
																			</xsl:when>
																			<xsl:when test="$dqinfoSectionRedCount&gt;0 and $dqinfoSectionYellowCount&gt;0">
																				<div class="col-6 icon-padding">
																					<div class="filledRCircle right">
																						<span>
																							<xsl:value-of select="$dqinfoSectionRedCount" />
																						</span>
																					</div>
																				</div>
																				<div class="col-4 icon-padding">
																					<div class="filledYCircle right">
																						<span>
																							<xsl:value-of select="$dqinfoSectionYellowCount" />
																						</span>
																					</div>
																				</div>
																			</xsl:when>
																			<xsl:otherwise>
																				<div class="col-10 icon-padding">
																					<div class="filledGCircle right">
																					</div>

																				</div>
																			</xsl:otherwise>
																		</xsl:choose>

																		<div class="col-2 right">
																			<span>
																				<a style="text-decoration:none;margin-top:5px;" id="{$sectionDetail}" href="javascript:void(0)">
																					<i class="ic-dropdown  ic-dd-rotate-180 parentDDIcon" style="font-size: 24px; color: #006693;" />
																				</a>
																			</span>
																		</div>
																	</div>
																</div>
															</div>
															<div class="row" style="padding-bottom:0px;">
																<div class="col-12">
																	<div class="row" style="padding-bottom:0px;">
																		<xsl:variable name="dqinfoSectionGreenCount1" select="count($dqinfoGreenNodes)" />
																		<xsl:variable name="dqinfoSectionRedCount1" select="count($dqinfoRedNodes)" />
																		<xsl:variable name="dqinfoSectionYellowCount1" select="count($dqinfoYellowNodes)" />

																		<xsl:choose>

																			<xsl:when test="$dqinfoSectionGreenCount1&gt; 0">
																				<div class="row section-sub-headerDiv" style="padding-bottom:10px;" id="Green">

																					<div id="Green_colorCount_{$sectionValue}" class=" col-12 section-sub-header">
																						<div class="col-2">

																							<span class="sectionStatus">
																								<xsl:value-of select="'Green'" />
																							</span>
																							<span>
																								<i class="ic-pass green-color" style="font-size:24px;" />
																							</span>

																						</div>
																						<div class="col-10">
																							<span class="right">
																								<a style="text-decoration:none;margin-top:5px;" id="Green" href="javascript:void(0);">
																									<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" />
																								</a>
																							</span>
																						</div>
																					</div>

																				</div>
																				<div class="msgTablepadding">
																					<table class="table msgTablePaddingBottom">
																						<thead class="table-header">
																							<tr>
																								<th>MESSAGE CODE</th>
																								<th>MESSAGES</th>
																							</tr>
																						</thead>
																						<tbody>
																							<xsl:for-each select="msxsl:node-set($greenMessages)/messages/message">

																								<xsl:variable name="technicalMessage" select="technicalMessage" />
																								<tr>
																									<td>
																										<xsl:value-of select="code" />
																									</td>
																									<td colspan="2">
																										<div class="messageToggleContainer">
																											<div class="col-11">
																												<div class="messageToggle">
																													<xsl:value-of select="businessMessage" />
																												</div>
																												<xsl:if test="$technicalMessage !=''">
																													<div class="messageToggle techMessageToggle  messageToggleHidden">
																														<xsl:value-of select="$technicalMessage" />
																													</div>
																												</xsl:if>
																											</div>
																											<div class="col-1">
																												<xsl:if test="$technicalMessage !=''">
																													<a class="messageToggleSwitch right" href="javascript:void(0)">
																														<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																													</a>
																												</xsl:if>
																											</div>
																										</div>
																									</td>
																								</tr>
																							</xsl:for-each>
																						</tbody>
																					</table>
																				</div>
																			</xsl:when>


																			<xsl:otherwise>

																				<xsl:if test="$dqinfoSectionRedCount1&gt;0">
																					<div class="row section-sub-headerDiv" style="padding-bottom:10px;" id="Red">

																						<div id="Red_colorCount_{$sectionValue}" class=" col-12 section-sub-header">
																							<div class="col-2">

																								<span class="sectionStatus">
																									<xsl:value-of select="'Red'" />
																								</span>
																								<span>
																									<i class="ic-critical red-color" style="font-size:24px;" />
																								</span>

																							</div>
																							<div class="col-10">
																								<span class="right">
																									<a style="text-decoration:none;margin-top:5px;" id="Red" href="javascript:void(0);">
																										<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" />
																									</a>
																								</span>
																							</div>
																						</div>

																					</div>
																					<div class="msgTablepadding">
																						<table class="table msgTablePaddingBottom">
																							<thead class="table-header">
																								<tr>
																									<th>MESSAGE CODE</th>
																									<th>MESSAGES</th>
																								</tr>
																							</thead>
																							<tbody>
																								<xsl:for-each select="msxsl:node-set($redMessages)/messages/message">

																									<xsl:variable name="technicalMessage" select="technicalMessage" />
																									<tr>
																										<td>
																											<xsl:value-of select="code" />
																										</td>
																										<td colspan="2">
																											<div class="messageToggleContainer">
																												<div class="col-11">
																													<div class="messageToggle">
																														<xsl:value-of select="businessMessage" />
																													</div>
																													<xsl:if test="$technicalMessage !=''">
																														<div class="messageToggle techMessageToggle  messageToggleHidden">
																															<xsl:value-of select="$technicalMessage" />
																														</div>
																													</xsl:if>
																												</div>
																												<div class="col-1">
																													<xsl:if test="$technicalMessage !=''">
																														<a style="text-decoration:none;margin-top:5px;" class="messageToggleSwitch right" href="javascript:void(0)">
																															<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																														</a>
																													</xsl:if>
																												</div>
																											</div>
																										</td>
																									</tr>
																								</xsl:for-each>
																							</tbody>
																						</table>
																					</div>
																				</xsl:if>
																				<xsl:if test="$dqinfoSectionYellowCount1&gt;0">
																					<div class="row section-sub-headerDiv" style="padding-bottom:10px;">

																						<div id="Yellow_colorCount_{$sectionValue}" class=" col-12 section-sub-header">
																							<div class="col-2">

																								<span class="sectionStatus">
																									<xsl:value-of select="'Yellow'" />
																								</span>
																								<span>
																									<i class="ic-error yellow-color" style="font-size:24px;" />
																								</span>

																							</div>
																							<div class="col-10">
																								<span class="right">
																									<a style="text-decoration:none;margin-top:5px;" id="Yellow" href="javascript:void(0);">
																										<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" />
																									</a>
																								</span>
																							</div>
																						</div>

																					</div>
																					<div class="msgTablepadding">
																						<table class="table msgTablePaddingBottom">
																							<thead class="table-header">
																								<tr>
																									<th>MESSAGE CODE</th>
																									<th>MESSAGES</th>
																								</tr>
																							</thead>
																							<tbody>
																								<xsl:for-each select="msxsl:node-set($yellowMessages)/messages/message">
																									<xsl:variable name="technicalMessage" select="technicalMessage" />
																									<tr>
																										<td>
																											<xsl:value-of select="code" />
																										</td>
																										<td colspan="2">
																											<div class="messageToggleContainer">
																												<div class="col-11">
																													<div class="messageToggle">
																														<xsl:value-of select="businessMessage" />
																													</div>
																													<xsl:if test="$technicalMessage !=''">
																														<div class="messageToggle techMessageToggle  messageToggleHidden">
																															<xsl:value-of select="$technicalMessage" />
																														</div>
																													</xsl:if>
																												</div>
																												<div class="col-1">
																													<xsl:if test="$technicalMessage !=''">
																														<a style="text-decoration:none;margin-top:5px;" class="messageToggleSwitch right" href="javascript:void(0)">
																															<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																														</a>
																													</xsl:if>
																												</div>
																											</div>
																										</td>
																									</tr>
																								</xsl:for-each>
																							</tbody>
																						</table>
																					</div>
																				</xsl:if>
																			</xsl:otherwise>
																		</xsl:choose>

																	</div>
																</div>
															</div>
														</xsl:if>
													</div>
												</xsl:for-each>
											</div>
										</div>
									</div>
									<xsl:variable name="eginfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
									<xsl:variable name="eginfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
									<xsl:variable name="eginfoGreenCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
									<span style="display:none;" id="egCount">
										<xsl:value-of select="$eginfoYellowCount+$eginfoRedCount+$eginfoGreenCount" />
									</span>

									<div id="eligibilityMsgsTab" class="content bg-white">
										<xsl:if test="$eginfoYellowCount &gt;0 or $eginfoRedCount&gt;0 or $eginfoGreenCount&gt;0">
											<div class="row">
												<div class="col-12">
													<div class="row" id="sec_eligTable">
														<div class="row loanSection" style="padding-bottom:15px;margin-bottom:15px;" id="eligRowDD" onclick="toggleSection(event,'eligTable')">
															<div class="col-10">
																<div class="row">
																	<h1 class="header-contained" style="margin:0px;">Results</h1>
																</div>
															</div>
															<div class="col-2">
																<div class="row">
																	<xsl:choose>
																		<xsl:when test="$eginfoRedCount&gt;0 and $eginfoYellowCount = 0">
																			<div class="col-10 icon-padding">
																				<div class="filledRCircle right">
																					<span>
																						<xsl:value-of select="$eginfoRedCount" />
																					</span>
																				</div>
																			</div>
																		</xsl:when>
																		<xsl:when test="$eginfoRedCount=0 and $eginfoYellowCount&gt;0">
																			<div class="col-10 icon-padding">
																				<div class="filledYCircle right">
																					<span class="">
																						<xsl:value-of select="$eginfoYellowCount" />
																					</span>
																				</div>
																			</div>
																		</xsl:when>
																		<xsl:when test="$eginfoRedCount&gt;0 and $eginfoYellowCount&gt;0">
																			<div class="col-6 icon-padding">
																				<div class="filledRCircle right">
																					<span>
																						<xsl:value-of select="$eginfoRedCount" />
																					</span>
																				</div>
																			</div>
																			<div class="col-4 icon-padding">
																				<div class="filledYCircle right">
																					<span>
																						<xsl:value-of select="$eginfoYellowCount" />
																					</span>
																				</div>
																			</div>
																		</xsl:when>
																		<xsl:otherwise>
																			<div class="col-10 icon-padding">
																				<div class="filledGCircle right" />
																			</div>
																		</xsl:otherwise>
																	</xsl:choose>
																	<div class="col-2 right">
																		<span>
																			<a style="text-decoration:none;margin-top:5px;" id="giDD" href="javascript:void(0)">
																				<i class="ic-dropdown  ic-dd-rotate-180 parentDDIcon" style="font-size: 24px; color: #006693;" />
																			</a>
																		</span>
																	</div>
																</div>
															</div>
														</div>
														<xsl:variable name="EGRedMessages">
															<messages>
																<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Red']">
																	<message>
																		<code>
																			<xsl:value-of select="lclaln:MessageCode" />
																		</code>
																		<businessMessage>
																			<xsl:value-of select="lclaln:MessageText" />
																		</businessMessage>
																		<technicalMessage>
																			<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																		</technicalMessage>
																	</message>
																</xsl:for-each>
															</messages>
														</xsl:variable>
														<xsl:variable name="EGYellowMessages">
															<messages>
																<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Yellow']">
																	<message>
																		<code>
																			<xsl:value-of select="lclaln:MessageCode" />
																		</code>
																		<businessMessage>
																			<xsl:value-of select="lclaln:MessageText" />
																		</businessMessage>
																		<technicalMessage>
																			<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																		</technicalMessage>
																	</message>
																</xsl:for-each>
															</messages>
														</xsl:variable>
														<xsl:variable name="EGGreenMessages">
															<messages>
																<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Green']">
																	<message>
																		<code>
																			<xsl:value-of select="lclaln:MessageCode" />
																		</code>
																		<businessMessage>
																			<xsl:value-of select="lclaln:MessageText" />
																		</businessMessage>
																		<technicalMessage>
																			<xsl:value-of select="lclaln:MessageTextOtherDescription" />
																		</technicalMessage>
																	</message>
																</xsl:for-each>
															</messages>
														</xsl:variable>
														<xsl:variable name="egRedNodes" select="msxsl:node-set($EGRedMessages)/messages/message" />
														<xsl:variable name="egYellowNodes" select="msxsl:node-set($EGYellowMessages)/messages/message" />
														<xsl:variable name="egGreenNodes" select="msxsl:node-set($EGGreenMessages)/messages/message" />


														<div id="eligTable" class="row">

															<xsl:if test="count($egGreenNodes)&gt;0">
																<div class="row section-sub-headerDiv" style="padding-bottom:10px; padding-top:0px">
																	<div id="Green" class="col-12 section-sub-header">
																		<div class="col-12">
																			<div class="col-2">
																				<span class="sectionStatus">Green</span>

																				<span>
																					<i class="ic-pass green-color" style="font-size:24px;" />
																				</span>
																			</div>
																			<div class="col-10">
																				<span class="right">
																					<a style="text-decoration:none;margin-top:5px;" id="Green" href="javascript:void(0);">
																						<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" style="padding-right:15px; " />
																					</a>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="msgTablepadding">
																	<table class="table msgTablePaddingBottom">
																		<thead class="table-header">
																			<tr>
																				<th>MESSAGE CODE</th>
																				<th>MESSAGES</th>
																			</tr>
																		</thead>
																		<tbody>
																			<xsl:for-each select="$egGreenNodes">
																				<xsl:variable name="technicalMessage" select="technicalMessage" />
																				<tr id="row_criticalSec_Green">
																					<td>
																						<xsl:value-of select="code" />
																					</td>
																					<td colspan="2">
																						<div class="messageToggleContainer">
																							<div class="col-11">
																								<div class="messageToggle">
																									<xsl:value-of select="businessMessage" />
																								</div>
																								<xsl:if test="$technicalMessage !=''">
																									<div class="messageToggle techMessageToggle  messageToggleHidden">
																										<xsl:value-of select="technicalMessage" />
																									</div>
																								</xsl:if>
																							</div>
																							<div class="col-1">
																								<xsl:if test="$technicalMessage !=''">
																									<a class="messageToggleSwitch right" href="javascript:void(0)">
																										<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																									</a>
																								</xsl:if>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</xsl:for-each>
																		</tbody>
																	</table>
																</div>


															</xsl:if>
															<xsl:if test="count($egRedNodes)&gt;0">
																<div class="row section-sub-headerDiv" style="padding-bottom:10px; padding-top:0px">
																	<div id="Red" class="col-12 section-sub-header">
																		<div class="col-12">
																			<div class="col-2">
																				<span class="sectionStatus">Red</span>

																				<span>
																					<i class="ic-critical red-color" style="font-size:24px;" />
																				</span>
																			</div>
																			<div class="col-10">
																				<span class="right">
																					<a style="text-decoration:none;margin-top:5px;" id="Red" href="javascript:void(0);">
																						<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" style="padding-right:15px; " />
																					</a>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="msgTablepadding">
																	<table class="table msgTablePaddingBottom">
																		<thead class="table-header">
																			<tr>
																				<th>MESSAGE CODE</th>
																				<th>MESSAGES</th>
																			</tr>
																		</thead>
																		<tbody>
																			<xsl:for-each select="$egRedNodes">
																				<xsl:variable name="technicalMessage" select="technicalMessage" />
																				<tr id="row_criticalSec_Red">
																					<td>
																						<xsl:value-of select="code" />
																					</td>
																					<td colspan="2">
																						<div class="messageToggleContainer">
																							<div class="col-11">
																								<div class="messageToggle">
																									<xsl:value-of select="businessMessage" />
																								</div>
																								<xsl:if test="$technicalMessage !=''">
																									<div class="messageToggle techMessageToggle  messageToggleHidden">
																										<xsl:value-of select="technicalMessage" />
																									</div>
																								</xsl:if>
																							</div>
																							<div class="col-1">
																								<xsl:if test="$technicalMessage !=''">
																									<a class="messageToggleSwitch right" href="javascript:void(0)">
																										<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																									</a>
																								</xsl:if>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</xsl:for-each>
																		</tbody>
																	</table>
																</div>

															</xsl:if>
															<xsl:if test="count($egYellowNodes)&gt;0">
																<div class="row section-sub-headerDiv" style="padding-bottom:10px; padding-top:0px">
																	<div id="Yellow" class="col-12 section-sub-header">
																		<div class="col-12">
																			<div class="col-2">
																				<span class="sectionStatus">Yellow</span>

																				<span>
																					<i class="ic-error yellow-color" style="font-size:24px;" />
																				</span>
																			</div>
																			<div class="col-10">
																				<span class="right">
																					<a style="text-decoration:none;margin-top:5px;" id="Yellow" href="javascript:void(0);">
																						<i class="ic-dropdown  ic-dd-rotate-180 childDDIcon" style="padding-right:15px; " />
																					</a>
																				</span>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="msgTablepadding">
																	<table class="table msgTablePaddingBottom">
																		<thead class="table-header">
																			<tr>
																				<th>MESSAGE CODE</th>
																				<th>MESSAGES</th>
																			</tr>
																		</thead>
																		<tbody>
																			<xsl:for-each select="$egYellowNodes">
																				<xsl:variable name="technicalMessage" select="technicalMessage" />
																				<tr id="row_criticalSec_Yellow">
																					<td>
																						<xsl:value-of select="code" />
																					</td>
																					<td colspan="2">
																						<div class="messageToggleContainer">
																							<div class="col-11">
																								<div class="messageToggle">
																									<xsl:value-of select="businessMessage" />
																								</div>
																								<xsl:if test="$technicalMessage !=''">
																									<div class="messageToggle techMessageToggle  messageToggleHidden">
																										<xsl:value-of select="technicalMessage" />
																									</div>
																								</xsl:if>
																							</div>
																							<div class="col-1">
																								<xsl:if test="$technicalMessage !=''">
																									<a class="messageToggleSwitch right" href="javascript:void(0)">
																										<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-toggle-icon20x30.png')}" />
																									</a>
																								</xsl:if>
																							</div>
																						</div>
																					</td>
																				</tr>
																			</xsl:for-each>
																		</tbody>
																	</table>
																</div>
															</xsl:if>

														</div>

													</div>
												</div>
											</div>
										</xsl:if>
									</div>

								</div>
							</div>
						</section>
					</div>
				</section>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>