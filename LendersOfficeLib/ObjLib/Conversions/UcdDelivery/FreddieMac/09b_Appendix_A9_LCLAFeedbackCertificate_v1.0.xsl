﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" exclude-result-prefixes="exslt msxsl" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:sch="http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas" xmlns:lclaln="http://lclaloanresults.freddiemac.com/schema/types" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:import href="09e_Appendix_A9_LCLACommon_v1.0.xsl" />
  <xsl:param name="VirtualRoot" />
    
 
	<xsl:template match="sch:LCLALoanEvaluationServiceResponse">
<html>
<head>
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/style.css')}" media="screen,print" />
    <link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/lcla-global.css')}" media="screen,print" />
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/css/lcla_loan_eval_rslts.css')}" media="screen,print" />
	<script type="text/javascript" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/scripts/jquery-1.11.1.js')}" />
    <script type="text/javascript" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/scripts/lcla-global.js')}" />
    <title>Loan Closing Advisor Feedback Certificate </title>
 
 <style>
	@media print {
	  #printWrapper, #footerWrapper {
	    display:none !important;
	  }
	  
	  pre {page-break-inside:auto !important;}
	  .main-nav { position:relative !important}
	  .container { padding-top:20px; margin:0 !important}
	  .printArea { width:95% !important}
	  .panel-top, .panel-padding { padding-right:0 !important; padding-left:0 !important;}
	  .panel { border:0 !important} 
	  a[href]:after { content:none !important}
	  .nofixhdr { position:relative !important; width:0;}
	  .noprintpad { padding-top:0px !important}
	  body { width:1040px !important}
	}
	


</style>
   
   <script type="text/javascript">
	function printpage() {
		window.print();
	  }
	  $(document)
				.ready(
						function() {
							var fileSubmissionDt = document.getElementById('fileSubDtJs1').innerHTML;
							
							var d = new Date(fileSubmissionDt);
							if (!isNaN(d.getTime())) {
								 document.getElementById('fileSubDtJs').innerHTML= (dateformat.format(d,'Y-m-d G:i:s A'));
								 document.getElementById('fileSubDtJs1').innerHTML= (dateformat.format(d,'Y-m-d G:i:s A'));
							}else{
								document.getElementById('fileSubDtJs').innerHTML=fileSubmissionDt;	
								document.getElementById('fileSubDtJs1').innerHTML= fileSubmissionDt;
							}
							
											  	
});
</script>
 
</head>	
 

<body style="margin:40px 0px; min-width:1000px;">
	<section class="container content bg-white">
		<section class="col-12">
			<section class="container content" style="margin: 0px; padding: 0px;">
				<div id="printWrapper"> 
		            <div class="row"> 
		               <div class="col-12">
							<button class="btn-primary-lg right" id="print" style="margin-right: 0px;" onclick="printpage();">PRINT</button>
						</div>
					</div>
					<hr />
				</div>
				<section class="row">
						<div class="col-12 printArea" style="padding-bottom: 20px;">
								<div class="col-6" style="margin-top: 8px;">
								 	<img class="logo" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/lgo-freddiemac.svg')}" />
								</div>
								<div class="col-6 right" style="text-align: right;">
								 	<img style="height:32px; width:32px;padding-right:10px" class="logo1" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/resources/Asset/ic-app-lcla.svg')}" />
								 	<span class="floating-header" style="vertical-align: middle;">Loan Closing Advisor<span class="servicemark" /></span>
								</div>
						</div>
				</section>
			  </section>
			  <xsl:variable name="generalInfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
	<section class="container content first bg-white col-12" style="border-top-color:#009ce0;margin-top:0px; padding-bottom:0px; margin-bottom: 40px;">
		<div class="row center" style="margin-bottom: 0px;  margin-left:50px; margin-right:50px; ">
			<i class="ic-certificate active" style="verticle-align:middle;" />
			<span class="floating-header" style=" padding-left:20px; vertical-align: top;">Loan Closing Advisor Feedback Certificate </span>
		</div>
	</section>
	<section class="container content  bg-white col-12" style="margin-top:0px; padding-top:10px !important;padding-bottom:0px; margin-bottom: 60px;">
		<div class="row center" style="margin-bottom: 0px;  margin-left:50px; margin-right:50px; vertical-align: middle;">
		
			<span style="vertical-align: top;">
			<span class="callout-md" style="color: #009CE0;">UCD Requirement:</span>
			 <xsl:choose>
									<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator != '' and (sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator ='true'           or sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator ='1')">
								
										<i class="ic-confirmation green-color" style="padding:0px 20px !important;font-size:30px; vertical-align: baseline;" />
										<span class="callout-md green-color" style="vertical-align: baseline;"><xsl:value-of select="'Satisfied'" />
										
									</span>
									
									</xsl:when>
									<xsl:otherwise>
										 <xsl:choose>
											<xsl:when test="$generalInfoRedCount &gt; 1  or $generalInfoRedCount = 1">
											<i class="ic-error yellow-color" style="padding:0px 20px !important; font-size: 30px; vertical-align: baseline;" />
												<span class="callout-md yellow-color" style="vertical-align: baseline;"><xsl:value-of select="'Not Satisfied'" />
												</span>
											</xsl:when>
											<xsl:otherwise>
											<i class="ic-confirmation green-color" style="padding:0px 20px !important;font-size:30px; vertical-align: baseline;" />
												<span class="callout-md green-color" style="vertical-align: baseline;"><xsl:value-of select="'Satisfied' " />
												</span>
												<span style="padding-left:2px; font-size:18px" class=""> <xsl:value-of select="' (Without a Closing Disclosure PDF)'" /></span>
											</xsl:otherwise>
										 </xsl:choose>
									</xsl:otherwise>
								</xsl:choose>
							</span>	
		</div>
	</section>	
		
	<xsl:variable name="borrowerPartyRoles">
										<borrowers>
											<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
												<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL[lclaln:PartyRoleType='Borrower']">
												<borrower>
													<firstname>
														<xsl:value-of select="lclaln:INDIVIDUAL/lclaln:NAME/lclaln:FirstName" />
													</firstname>
													<lastname>
														<xsl:value-of select="lclaln:INDIVIDUAL/lclaln:NAME/lclaln:LastName" />
													</lastname>
													<fullname>
														<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
													</fullname>
												
													<addressline>
														<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:AddressLineText" />
													</addressline>
													<city>
														<xsl:if test="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:CityName!= ''">
																<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:CityName" />
															</xsl:if>
													</city>
													<state>
														<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:StateCode" />
													</state>
													<postalcode>
														<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:PostalCode" />
													</postalcode>
													
												</borrower>
												</xsl:if>
											</xsl:for-each>
										</borrowers>
						</xsl:variable>
	
	<section class="col-12  printArea" style="margin-left: 0px;">
			<div class="col-6">
				<div class="header-contained line" style="padding-bottom: 15px; margin-bottom: 40px;">Details</div>
				<div class="row">
					<div class="col-12" style="padding-bottom:40px;">
					<xsl:variable name="BorrowerPartyNodes" select="msxsl:node-set($borrowerPartyRoles)/borrowers/borrower" />
						<xsl:if test="count($BorrowerPartyNodes) &gt; 0">
										<div class="col-4">
											<div class="h4-label">BORROWER</div>
												<div class="messageToggle">
													<xsl:for-each select="$BorrowerPartyNodes">
														<xsl:variable name="i" select="position()" />
															<xsl:if test="$i &lt; 3">
														<xsl:if test="$i = 2">
															<span class="body-navy"><xsl:text> and   </xsl:text></span>
														</xsl:if>
																													
														<span class="body-navy">
																<xsl:value-of select="firstname" />
															<xsl:text> </xsl:text>
																<xsl:value-of select="lastname" />
														</span>
			 
														<span class="body-navy">
															<xsl:value-of select="fullname" />
														</span>
														</xsl:if>
													</xsl:for-each>
												</div>
												<xsl:for-each select="$BorrowerPartyNodes">
														
												<xsl:if test="position() = 1">
													
															<div class="word-wrap">
																<span class="body-navy">
																
																	<xsl:value-of select="addressline" />
																</span>
															</div>
															<div class="word-wrap">	
																<span class="body-navy">
																
																
																	<xsl:if test="city!= ''">
																		<xsl:value-of select="city" /><xsl:text>, </xsl:text>
																	</xsl:if>
																	<xsl:value-of select="state" />
																	<xsl:text> </xsl:text>
																	<xsl:value-of select="postalcode" />
																</span>
															</div>
												</xsl:if>
													</xsl:for-each>
											</div>
										
										</xsl:if>
					
				       <div class="col-4">
			 		    	<div class="h4-label">PROPERTY ADDRESS</div>
							<xsl:variable name="propertyAddressLine" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:AddressLineText" />
			 		    	<div class="word-wrap messageToggle">
			 		    		<span class="body-navy" title="{$propertyAddressLine}"><xsl:value-of select="$propertyAddressLine" /></span>
							</div>
							<div class="word-wrap messageToggle">		 		    		
				       			<span class="body-navy">
									<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:CityName" />
									<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:CityName != '' ">
										<xsl:text>, </xsl:text>
									</xsl:if>
									<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:StateCode" /><xsl:text> </xsl:text>
									<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:PostalCode" />
								</span>
			 		    	</div>
					   </div>
					   <div class="col-4">
							<div class="h4-label ">CLOSING DISCLOSURE TYPE </div>
		 		    		<div class="word-wrap">						
		 		    			<span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureType" /></span>
		 		    		</div>
	 		    		</div>
					</div>
				</div>
				<div class="row" style="padding-bottom:40px;">
					<div class="col-12">
						<div class="col-4">
				     		<div class="h4-label">CLOSING DATE</div> 
				     		<div class="word-wrap">
				     			  
	                             <span class="body-navy">
									<xsl:copy>
									  <xsl:call-template name="date">
										<xsl:with-param name="yyyy-mm-dd" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:ClosingDate" />
									  </xsl:call-template>
									</xsl:copy>
									</span>
	                     
				     		</div> 
				      </div>
				      <div class="col-4">
							<div class="h4-label">DISBURSEMENT DATE</div>
							<div class="word-wrap">
					       		
		                        <span class="body-navy">
								<xsl:copy>
									  <xsl:call-template name="date">
										<xsl:with-param name="yyyy-mm-dd" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:DisbursementDate" />
									  </xsl:call-template>
									</xsl:copy>
								</span>
	                        </div>	
				      </div>
				       <div class="col-4">
			 		    	<xsl:choose>
								<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType != 'Refinance'">
									<div class="h4-label">SALE PRICE</div>
									<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:SalePrice">
																			
										<div class="body-navy word-wrap">
											<xsl:text>$</xsl:text>
											<xsl:value-of select="format-number(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:SalePrice,&quot;###,###.##&quot;)" />
										</div>
									</xsl:if>		
								</xsl:when>
								<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType = 'Refinance'">
									<div class="h4-label">APPRAISED PROP. VALUE</div>
													
									<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:AppraisedValue">
													
													<div class="body-navy word-wrap">
														<xsl:text>$</xsl:text>
														<xsl:value-of select="format-number(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:AppraisedValue,'###,###.##')" />
													</div>
									</xsl:if>		
								</xsl:when>
												
							</xsl:choose>
					   </div>
					</div>
				</div>
				<div class="row" style="padding-bottom:40px;">
					<div class="col-12">
						 <div class="col-4">
							<div class="h4-label">LENDER</div>
							<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
													<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='NotePayTo'">
														<div class="word-wrap">
															<span class="body-navy">
																<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
															</span>
														</div>
													</xsl:if>
												</xsl:for-each>
	 		    		</div>
	 		    		 <div class="col-4">
							<div class="h4-label">LENDER LOAN ID</div>
							<div class="word-wrap">
							  <span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LOAN_IDENTIFIERS/lclaln:LOAN_IDENTIFIER[lclaln:LoanIdentifierType='LenderLoan']/lclaln:LoanIdentifier" /></span>
		 		    		</div>
	 		    		</div>				       
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="header-contained line" style="padding-bottom: 15px; margin-bottom: 40px;">Evaluation Results</div>
				 <div class="row">
					<div class="col-12" style="padding-bottom:40px;">
					 	<div class="col-4">
					 		<div class="h4-label">GENERAL INFO</div>
					 	</div>
					 	<div class="col-4">
						 	 <xsl:variable name="geninfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
													<xsl:variable name="geninfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])- count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:MessageCode='MSGGI17']           )              " />
													<xsl:variable name="geninfoGreenCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
													<xsl:choose>
														<xsl:when test="$geninfoGreenCount&gt; 0">
															<i class="ic-shield-green float-left green-color" alt="Green" />
														</xsl:when>
														<xsl:otherwise>
															<xsl:if test="$geninfoRedCount&gt; 0">
																<i class="ic-shield-red float-left red-color" alt="Red" />
																<span class="body-navy rslts-cnt-paddRight">
																	<xsl:value-of select="$geninfoRedCount" />
																</span>
															</xsl:if>
															<xsl:if test="$geninfoYellowCount&gt; 0">
																<i class="ic-shield-yellow float-left yellow-color" alt="Yellow" />
																<span class="body-navy rslts-cnt-paddRight">
																	<xsl:value-of select="$geninfoYellowCount" />
																</span>
															</xsl:if>
														</xsl:otherwise>
							</xsl:choose>
					 		 </div>
					 	</div>
						
				<xsl:if test="$generalInfoRedCount &lt; 1">		
				 	<div class="row">
					<div class="col-12" style="padding-bottom:40px;">
					 	<div class="col-4">
					 		<div class="h4-label">DATA QUALITY</div>
					 	</div>
					 	<div class="col-4">
							<xsl:variable name="dqinfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
							<xsl:variable name="dqinfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
							<xsl:variable name="dqinfoGreenCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
							<xsl:choose>
								<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Conclusion='Green'">
									<i class="ic-shield-green float-left green-color" alt="Green" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:if test="$dqinfoRedCount &gt; 0">
										<i class="ic-shield-red float-left red-color" alt="Red" />
										<span class="body-navy rslts-cnt-paddRight">
											<xsl:value-of select="$dqinfoRedCount" />
										</span>
									</xsl:if>
									<xsl:if test="$dqinfoYellowCount &gt; 0">
										<i class="ic-shield-yellow float-left yellow-color" alt="Yellow" />
										<span class="body-navy rslts-cnt-paddRight">
											<xsl:value-of select="$dqinfoYellowCount" />
										</span>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
					 	</div>
					 	</div>
					 	</div>
					 	<div class="row">
							<div class="col-12" style="padding-bottom:40px;">
					 	<div class="col-4">
					 		<div class="h4-label">ELIGIBILITY</div>
					 	</div>
					 	<div class="col-4">
							<xsl:variable name="eginfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
							<xsl:variable name="eginfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
							<xsl:variable name="eginfoGreenCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
							<xsl:choose>
								<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Conclusion='Green'">
									<i class="ic-shield-green float-left green-color" alt="Green" />
								</xsl:when>
								<xsl:otherwise>
									<xsl:if test="$eginfoRedCount &gt; 0">
										<i class="ic-shield-red float-left red-color" alt="Red" />
										<span class="body-navy rslts-cnt-paddRight">
											<xsl:value-of select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
										</span>
									</xsl:if>
									<xsl:if test="$eginfoYellowCount &gt; 0">
										<i class="ic-shield-yellow float-left yellow-color" alt="Yellow" />
										<span class="body-navy rslts-cnt-paddRight">
											<xsl:value-of select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
										</span>
									</xsl:if>
								</xsl:otherwise>
							</xsl:choose>
					 	</div>
					</div>
					</div>
				</xsl:if>	
				</div>
			</div>
	</section>
	<xsl:if test="$generalInfoRedCount &gt; 1  or $generalInfoRedCount = 1">		
	<section class="col-12 printArea" style="margin-left: 0px;">
		<div class="row">
		<div class="col-12" style="padding-bottom:40px;">
			<span class="body-navy red-color" style="font-size:16px;">This feedback certificate is invalid due to General Info errors. Please correct and resubmit.</span>
			<span id="fileSubDtJs" style="display:none;" class="body-navy" />
		</div>
		</div>
	</section>
	</xsl:if>
	<xsl:if test="$generalInfoRedCount &lt; 1">		
	<section class="col-12 printArea" style="margin-left: 0px;">
			<div class="col-6">
				<div class="header-contained line" style="padding-bottom: 15px; margin-bottom: 40px;">Loan Information</div>
				<div class="row">
					<div class="col-12" style="padding-bottom:40px;">
					
						<div class="col-4" style="padding-bottom:40px;">
				     		<div class="h4-label">LOAN TYPE</div> 
				     		 <div class="word-wrap"> 
		 		        		<span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedMortgageTerm/lclaln:LoanType" /></span>
		                     </div>    
				      </div>
				      <div class="col-4">
			 		    	<div class="h4-label">LOAN TERM</div>
			 		    	<div class="word-wrap messageToggle">
								<span class="body-navy">
								
								 <xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm !='' ">
										 <xsl:copy>
									  <xsl:call-template name="loantermformat">
									
										<xsl:with-param name="loantermvalue" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm" />
									  </xsl:call-template>
									</xsl:copy>
								
								</xsl:if>
								
								 <xsl:if test="(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm !='')and (sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum !='')">	
									<span> - </span>	
								</xsl:if>	
							  <xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum !='' ">
										 <xsl:copy>
									  <xsl:call-template name="loantermformat">
									
										<xsl:with-param name="loantermvalue" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum" />
									  </xsl:call-template>
									</xsl:copy>
								
								
								</xsl:if>
														
														
								</span>
							</div>
					  </div>
					  <div class="col-4">
			 		    	<div class="h4-label">PRODUCT</div>
			 		    	<div class="word-wrap">
								<span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:AMORTIZATION/lclaln:AMORTIZATION_RULE/lclaln:AmortizationType" /></span>	 		    	
							</div>
					  </div>
					  <div class="row">
						<div class="col-12" style="padding-bottom:0px;" />
					 </div>
					  <div class="row">
						<div class="col-12" style="padding-bottom:40px;">
						  <div class="col-4">
					     		<div class="h4-label">PURPOSE</div>
					       		<div class="word-wrap">
					       			<span class="body-navy">
									<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType" /></span>
					       		</div> 
					      </div>
					      
					      <div class="col-4">
				     			<div class="h4-label">MIC#</div>
				     			<div class="word-wrap">
						       		<span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:MI_DATA/lclaln:MI_DATA_DETAIL/lclaln:MICertificateIdentifier" /></span>
						       	</div>
						  </div>
						  
						  <div class="col-4">
				     			<div class="h4-label">APPRAISAL IDENTIFIER</div>
				     			<div class="word-wrap">
				     				<span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:PROPERTY_VALUATIONS/lclaln:PROPERTY_VALUATION/lclaln:PROPERTY_VALUATION_DETAIL/lclaln:AppraisalIdentifier" /></span>
				     			</div>
						  </div>
					  </div>
				  </div>
						  
				  <div class="row">
					<div class="col-12" style="padding-bottom:40px;">
						
					  	<div class="col-4">
			     			<div class="h4-label">AUS#</div>
			     			<div class="word-wrap">
		                            <span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:UNDERWRITING/lclaln:AUTOMATED_UNDERWRITINGS/lclaln:AUTOMATED_UNDERWRITING/lclaln:AutomatedUnderwritingCaseIdentifier" /></span>		                       </div>
					      </div>
					      
				      <div class="col-4">
			     			<div class="h4-label">MERS MIN</div>
			     			<div class="word-wrap">
	                        	<span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LOAN_IDENTIFIERS/lclaln:LOAN_IDENTIFIER[lclaln:LoanIdentifierType='MERS_MIN']/lclaln:LoanIdentifier" /></span>
	                        </div>
	 				  </div>
	 				 </div>
	 				 </div>
				</div>	
			</div>
		</div>
		
		<div class="col-6">
				<div class="header-contained line" style="padding-bottom: 15px; margin-bottom: 40px;">Transaction Information</div>
				<div class="row">
					<div class="col-12" style="padding-bottom:40px;">
					 	
					 	<div class="col-4">
							<xsl:choose>
							<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY/lclaln:ROLES/lclaln:ROLE[lclaln:ROLE_DETAIL/lclaln:PartyRoleTypeOtherDescription='Correspondent']/lclaln:PARTY_ROLE_IDENTIFIERS/lclaln:PARTY_ROLE_IDENTIFIER/lclaln:PartyRoleIdentifier != ''">
							<div class="h4-label">Correspondent ID</div>
					 		<div class="word-wrap">
								 <span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY/lclaln:ROLES/lclaln:ROLE[lclaln:ROLE_DETAIL/lclaln:PartyRoleTypeOtherDescription='Correspondent']/lclaln:PARTY_ROLE_IDENTIFIERS/lclaln:PARTY_ROLE_IDENTIFIER/lclaln:PartyRoleIdentifier" /></span>
							</div>
							</xsl:when>
							<xsl:otherwise>
								<div class="h4-label">SELLER NUMBER</div>
								<div class="word-wrap">
									 <span class="body-navy"><xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY/lclaln:ROLES/lclaln:ROLE[lclaln:ROLE_DETAIL/lclaln:PartyRoleTypeOtherDescription='Seller']/lclaln:PARTY_ROLE_IDENTIFIERS/lclaln:PARTY_ROLE_IDENTIFIER/lclaln:PartyRoleIdentifier" /></span>
								</div>
							</xsl:otherwise>
							</xsl:choose>	
					 	</div>
					 	
					 	 <div class="col-4">
							<div class="h4-label">BATCH ID</div>
							<div class="word-wrap">
							  <span class="body-navy"><xsl:value-of select="sch:EvaluationResults/@BatchID" /></span>
		 		    		</div>
	 		    		</div>
	 		    		
	 		    		 <div class="col-4">
							<div class="h4-label">TRANSACTION ID</div>
							<div class="word-wrap">
							  <span class="body-navy"><xsl:value-of select="sch:EvaluationResults/@TransactionID" /></span>
		 		    		</div>
	 		    		</div>
	 		    	</div>
	 		    </div>
	 		    		
	 		    <div class="row">
					<div class="col-12" style="padding-bottom:40px;">
					
					<div class="col-4">
				       	<div class="h4-label">CLOSING DISCLOSURE <br /> EMBEDDED PDF</div>
				       		<div class="word-wrap">
								<xsl:choose>
									<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator != '' and (sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator ='true'           or sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator ='1')">
									<span class="body-navy"><xsl:value-of select="'Yes'" /></span>
									</xsl:when>
									<xsl:otherwise>
											<span class="body-navy"><xsl:value-of select="'No'" /></span>
									</xsl:otherwise>
								</xsl:choose>
				       		</div>
					   </div>
							
					    <div class="col-4">
						   <div class="h4-label">SUBMISSION NUMBER</div>
					 		  <div class="word-wrap">
					       		 <span class="body-navy"><xsl:value-of select="sch:EvaluationResults/@SubmissionNumber" /></span>
					 		 </div>
					   </div>
						
						<div class="col-4">
					 		<div class="h4-label">DATE/TIME EVALUATED</div>
						 	<div class="word-wrap">
						 		<span id="fileSubDtJs" class="body-navy"><xsl:value-of select="sch:EvaluationResults/@SubmissionDateTime" /></span>
						 	</div>
					 	</div>
					 	
					</div>
				</div>
			</div>
		
		
			
		</section>
		</xsl:if>
	</section>

	<div id="footerWrapper" style="padding-left: 0px;">
		<div class="col-12" style="padding-top:200px;">
			<div class="col-8">
			 	<span class="body-gray">Loan Closing Advisor Feedback Certificate - </span>
			 	<span id="fileSubDtJs1" class="body-gray"><xsl:value-of select="sch:EvaluationResults/@SubmissionDateTime" /></span>
			</div>
			<div class="col-2 right" style="text-align: right;">
			 	<span class="body-gray" style="vertical-align: right;">Page 1 of 1 </span>
			</div>
		</div>
	</div>
	</section>
	</body>
</html>
	</xsl:template>
</xsl:stylesheet>