﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:sch="http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas" xmlns:lclaln="http://lclaloanresults.freddiemac.com/schema/types" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:import href="09e_Appendix_A9_LCLACommon.xsl" />
  <xsl:param name="VirtualRoot" />


<!-- Version 2.1 -->

<xsl:template match="sch:LCLALoanEvaluationServiceResponse">
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/style.css')}" media="screen,print" />
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/css/lcla_style.css')}" />
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/css/lcla-global.css')}" />
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/css/lcla_loan_eval_rslts.css')}" />
	
	<title>Loan Closing Advisor - Loan Evaluation Summary</title>
</head>

<body class="bg-greybg main-nav-fixed">
	<xsl:variable name="generalInfoRedCount3" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
	<xsl:variable name="evalMsgCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])   +count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])- count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:MessageCode='MSGGI17'])" />
	<nav class="main-nav fixed">
		<ul class="main-nav-list">
			<li>
				<a href="#">
					<img src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/Asset/ic-app-lcla-inv.svg')}" />
					Loan Closing Advisor
					<span class="servicemark" />
				</a>
			</li>
		</ul>
	</nav>
	
	<section class="container pageContainer">
		<div class="row">
			<nav class="col-2">
				<div class="scrollbox col-2">
					<ul class="sidebar-list scrollbox-content">
						<xsl:variable name="overallStatus" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:OverallUCDRequirementStatus" />
						<xsl:if test="$overallStatus">
							<div class="navBarUcd">
								<span class="h5-grey">Overall UCD Requirement</span>
								<div>
									<xsl:choose>
										<xsl:when test="$overallStatus = 'Satisfied'">
											<span class="callout-xs-green">Satisfied</span>
										</xsl:when>
										<xsl:when test="$overallStatus = 'Satisfied Without a Closing Disclosure PDF'">
											<span class="callout-xs-green">Satisfied</span>
											<br />
											<span class="lead-md-regular">(Without a Closing Disclosure PDF)</span>
										</xsl:when>
										<xsl:when test="$overallStatus = 'Not Satisfied'">
											<span class="callout-xs-yellow">Not Satisfied</span>
										</xsl:when>
										<xsl:otherwise>
											<span class="callout-xs-yellow" />
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</xsl:if>
					
						<li class="leftNavInfo">
							<div class="callout-xs-blue">
								Transaction ID:
								<xsl:value-of select="sch:EvaluationResults/@TransactionID" />
							</div>
							<div class="row padding10" id="ucd">
								<span class="h5-grey">This Submission's UCD Requirement Status</span>
								<div>
									<xsl:choose>
										<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Satisfied'">
											<span class="callout-xs-green">Satisfied</span>
										</xsl:when>
										<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Satisfied Without a Closing Disclosure PDF'">
											<span class="callout-xs-green">Satisfied</span>
											<br />
											<span class="lead-md-regular">(Without a Closing Disclosure PDF)</span>
										</xsl:when>
										<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Not Satisfied'">
											<span class="callout-xs-yellow">Not Satisfied</span>
										</xsl:when>
										<xsl:otherwise>
											<span class="callout-xs-yellow" />
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
							<div class="row padding10" id="borrowerName">
								<span class="h5-grey">Borrower Name</span>
								<br />
								<xsl:variable name="PartyNodes1" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY" />
								<xsl:variable name="BorrowerPartyNodes1" select="$PartyNodes1 [lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='Borrower']" />
								<xsl:if test="count($BorrowerPartyNodes1) &gt; 0">
									<div class="messageToggle">
										<xsl:call-template name="borrowerNames">
											<xsl:with-param name="BorrowerPartyNodes" select="$BorrowerPartyNodes1" />
										</xsl:call-template>
									</div>
								</xsl:if>
							</div>
							<div class="row padding10" id="lenderloanId">
								<span class="h5-grey">Lender Loan Identifier</span>
								<div class="word-wrap">
									<span class="lead-lg-regular">
										<xsl:value-of select="sch:EvaluationResults/lclaln:LOAN_IDENTIFIERS/lclaln:LOAN_IDENTIFIER[lclaln:LoanIdentifierType='LenderLoan']/lclaln:LoanIdentifier" />
									</span>
								</div>
							</div>
							<div class="row padding10" id="batcId">
								<div class="col-6 noPadColumn">
									<span class="h5-grey">Batch ID</span>
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:value-of select="sch:EvaluationResults/@BatchID" />
										</span>
									</div>
								</div>
								<div class="col-6 noPadColumn">
									<span class="h5-grey">User ID</span>
									<br />
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:value-of select="sch:EvaluationResults/@UserID" />
										</span>
									</div>
								</div>
							</div>
							<div class="row padding10" id="cdtype">
								<span class="h5-grey">Closing Disclosure Type</span>
								<br />
								<div class="word-wrap">
									<span class="lead-lg-regular">
										<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureType" />
									</span>
								</div>
							</div>
							<div class="row padding10">
								<span class="h5-grey">Submission Date/Time</span>
								<br />
								<div>
									<span id="fileSubDtJs" class="lead-lg-regular">
										<xsl:call-template name="formatDateTime">
											<xsl:with-param name="dateTime" select="sch:EvaluationResults/@SubmissionDateTime" />
										</xsl:call-template>
									</span>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</nav>
			<section class="col-10 right">
				<div class="row">
					<div class="h1-blue col-padding">
						<span class="title" id="pageTitle">Loan Evaluation Summary</span>
					</div>
					<xsl:variable name="overallStatus" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:OverallUCDRequirementStatus" />
					<xsl:if test="$overallStatus">
						<div class="col-padding right">
							<xsl:if test="$overallStatus = 'Satisfied Without a Closing Disclosure PDF'">
								<xsl:attribute name="class">col-padding right without-pdf</xsl:attribute>
							</xsl:if>
							<span class="h3-grey">Overall UCD Requirement: </span> 
							<xsl:choose>
								<xsl:when test="$overallStatus = 'Satisfied'">
									<span class="callout-md-green">Satisfied</span>
								</xsl:when>
								<xsl:when test="$overallStatus = 'Satisfied Without a Closing Disclosure PDF'">
									<span class="callout-md-green">Satisfied</span>
									<span class="body-lg-regular"> (Without a Closing Disclosure PDF)</span>
								</xsl:when>
								<xsl:when test="$overallStatus = 'Not Satisfied'">
									<span class="callout-md-yellow">Not Satisfied</span>
								</xsl:when>
								<xsl:otherwise>
									<span class="callout-md-yellow" />
								</xsl:otherwise>
							</xsl:choose>
						</div>
					</xsl:if>
				</div>
				<div class="bg-greybg ">
					<div id="evalRsltsSec">
						<section class="container content first bg-white">
							<div class="row noMarginRow">
								<div class="h3-grey left">Evaluation Results</div>
								<div class="right">
									<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Satisfied Without a Closing Disclosure PDF'">
										<xsl:attribute name="class">right without-pdf</xsl:attribute>
									</xsl:if>
									<span class="h3-grey">This Submission's UCD Requirement Status: </span> 
									<xsl:choose>
										<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Satisfied'">
											<span class="callout-sm-green">Satisfied</span>
										</xsl:when>
										<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Satisfied Without a Closing Disclosure PDF'">
											<span class="callout-sm-green">Satisfied</span>
											<span class="body-md-regular"> (Without a Closing Disclosure PDF)</span>
										</xsl:when>
										<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Not Satisfied'">
											<span class="callout-sm-yellow">Not Satisfied</span>
										</xsl:when>
										<xsl:otherwise>
											<span class="callout-sm-yellow" />
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
							<hr id="dividing-line" />
							<div class="row message-summaries">
								<div class="col-4">
									<xsl:variable name="geninfoRedCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
									<xsl:variable name="geninfoYellowCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])- count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:MessageCode='MSGGI17'])" />
									<xsl:variable name="geninfoGreenCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
									<xsl:if test="$geninfoRedCount1 + $geninfoYellowCount1 + $geninfoGreenCount1 != 0">
										<div class="h5-grey msgCountHeader">GENERAL INFO</div>
										<div class="row results-icon">
											<xsl:choose>
												<xsl:when test="$geninfoGreenCount1&gt; 0">
													<i class="ic-shield-green left green-color" alt="Green" />
												</xsl:when>
												<xsl:otherwise>
													<xsl:if test="$geninfoRedCount1&gt; 0">
														<i class="ic-shield-red left red-color" alt="Red" />
														<span class="lead-lg-regular rslts-cnt-paddRight">
															<xsl:value-of select="$geninfoRedCount1" />
														</span>
													</xsl:if>
													<xsl:if test="$geninfoYellowCount1&gt; 0">
														<i class="ic-shield-yellow left yellow-color" alt="Yellow" />
														<span class="lead-lg-regular rslts-cnt-paddRight">
															<xsl:value-of select="$geninfoYellowCount1" />
														</span>
													</xsl:if>
												</xsl:otherwise>
											</xsl:choose>
										</div>
										<div class="row results-button-container">
											<div class="col-12">
												<a class="button btn-secondary-sm" id="giLink" href="#generalMsgs">RESULTS</a>
											</div>
										</div>
									</xsl:if>
								</div>
								<div class="col-4">
									<xsl:variable name="dqinfoRedCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
									<xsl:variable name="dqinfoYellowCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
									<xsl:variable name="dqinfoGreenCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
									<xsl:if test="$dqinfoRedCount1 + $dqinfoYellowCount1 + $dqinfoGreenCount1 != 0">
										<div class="h5-grey msgCountHeader">DATA QUALITY</div>
										<div class="row results-icon">
											<xsl:choose>
												<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Conclusion='Green'">
													<i class="ic-shield-green left green-color" alt="Green" />
												</xsl:when>
												<xsl:otherwise>
													<xsl:if test="$dqinfoRedCount1 &gt; 0">
														<i class="ic-shield-red left red-color" alt="Red" />
														<span class="lead-lg-regular rslts-cnt-paddRight">
															<xsl:value-of select="$dqinfoRedCount1" />
														</span>
													</xsl:if>
													<xsl:if test="$dqinfoYellowCount1 &gt; 0">
														<i class="ic-shield-yellow left yellow-color" alt="Yellow" />
														<span class="lead-lg-regular rslts-cnt-paddRight">
															<xsl:value-of select="$dqinfoYellowCount1" />
														</span>
													</xsl:if>
												</xsl:otherwise>
											</xsl:choose>
										</div>
										<div class="row results-button-container">
											<div class="col-12">
												<a class="button btn-secondary-sm" id="dqLink" href="#dataQualityMsgs">RESULTS</a>
											</div>
										</div>
									</xsl:if>
								</div>
								<div class="col-4">
									<xsl:variable name="eginfoRedCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
									<xsl:variable name="eginfoYellowCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
									<xsl:variable name="eginfoGreenCount1" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
									<xsl:if test="$eginfoRedCount1 + $eginfoYellowCount1 + $eginfoGreenCount1 != 0">
										<div class="h5-grey msgCountHeader">ELIGIBILITY</div>
										<div class="row results-icon">
											<xsl:choose>
												<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Conclusion='Green'">
													<i class="ic-shield-green left green-color" alt="Green" />
												</xsl:when>
												<xsl:otherwise>
													<xsl:if test="$eginfoRedCount1 &gt; 0">
														<i class="ic-shield-red left red-color" alt="Red" />
														<span class="lead-lg-regular rslts-cnt-paddRight">
															<xsl:value-of select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
														</span>
													</xsl:if>
													<xsl:if test="$eginfoYellowCount1 &gt; 0">
														<i class="ic-shield-yellow left yellow-color" alt="Yellow" />
														<span class="lead-lg-regular rslts-cnt-paddRight">
															<xsl:value-of select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
														</span>
													</xsl:if>
												</xsl:otherwise>
											</xsl:choose>
										</div>
										<div class="row results-button-container">
											<div class="col-12">
												<a class="button btn-secondary-sm" id="eligLink" href="#eligibilityMsgs">RESULTS</a>
											</div>
										</div>
									</xsl:if>
								</div>
							</div>
							<div id="infoTabAnchor" />
						</section>
					</div>
				</div>
	
				<div id="closingInfo">
					<div class="h2-blue">
						<span>Closing Information</span>
					</div>
					<div class="col-12 container content bg-white body-md-regular">
						<div id="closingInfoTab" class="tabs-pane active">
							<div class="row">
								<div class="col-3">
									<div class="h5-grey">DATE ISSUED</div>
									<div>
										<span id="dateIssued" class="lead-lg-regular">
											<xsl:call-template name="date">
												<xsl:with-param name="yyyy-mm-dd" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:INTEGRATED_DISCLOSURE/lclaln:INTEGRATED_DISCLOSURE_DETAIL/lclaln:IntegratedDisclosureIssuedDate" />
											</xsl:call-template>
										</span>
									</div>
								</div>
								<div class="col-3">
									<div class="h5-grey">CLOSING DATE</div>
									<div>
										<span id="closingDate" class="lead-lg-regular">
											<xsl:call-template name="date">
												<xsl:with-param name="yyyy-mm-dd" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:ClosingDate" />
											</xsl:call-template>
										</span>
									</div>
								</div>
								<div class="col-3">
									<div class="h5-grey">DISBURSEMENT DATE</div>
									<div>
										<span id="disbursementDate" class="lead-lg-regular">
											<xsl:call-template name="date">
												<xsl:with-param name="yyyy-mm-dd" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:DisbursementDate" />
											</xsl:call-template>
										</span>
									</div>
								</div>
								<div class="col-3">
									<div class="h5-grey">SETTLEMENT AGENT</div>
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
												<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='ClosingAgent'">
													<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
												</xsl:if>
											</xsl:for-each>
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-3">
									<div class="h5-grey">FILE #</div>
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:ClosingAgentOrderNumberIdentifier" />
										</span>
									</div>
								</div>
								<div class="col-3">
									<div class="h5-grey">PROPERTY</div>
									<div class="lead-lg-regular ellipses">
										<span>
											<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:AddressLineText" />
										</span>
									</div>
									<div class="lead-lg-regular word-wrap">
										<span>
											<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:CityName != ''">
												<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:CityName" />
												<xsl:text>, </xsl:text>
											</xsl:if>
											<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:StateCode" />
											<xsl:text> </xsl:text>
											<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:PostalCode" />
										</span>
									</div>
								</div>
								<div class="col-3">
									<xsl:choose>
										<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType != 'Refinance'">
											<div class="h5-grey">SALE PRICE</div>
											<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:SalePrice">
												<div class="lead-lg-regular word-wrap">
													<xsl:text>$</xsl:text>
													<xsl:value-of select="format-number(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:SalePrice,&quot;###,###.##&quot;)" />
												</div>
											</xsl:if>
										</xsl:when>
										<xsl:when test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType = 'Refinance'">
											<div class="h5-grey">APPRAISED PROP. VALUE</div>
											<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:AppraisedValue">
												<div class="lead-lg-regular word-wrap">
													<xsl:text>$</xsl:text>
													<xsl:value-of select="format-number(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:AppraisedValue,'###,###.##')" />
												</div>
											</xsl:if>
										</xsl:when>
									</xsl:choose>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="transInfo">
					<div class="h2-blue">
						<span>Transaction Information</span>
					</div>
					<div class="col-12 container content bg-white body-md-regular">
						<div id="transInfoTab" class="tabs-pane">
							<div class="row">
								<xsl:variable name="PartyNodes" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY" />
								<xsl:variable name="BorrowerPartyNodes" select="$PartyNodes [lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='Borrower']" />
								<xsl:if test="count($BorrowerPartyNodes) &gt; 0">
									<div class="col-4">
										<div class="h5-grey">BORROWER</div>
										<div class="messageToggle">
											<xsl:call-template name="borrowerNames">
												<xsl:with-param name="BorrowerPartyNodes" select="$BorrowerPartyNodes" />
											</xsl:call-template>
										</div>
										<xsl:for-each select="$BorrowerPartyNodes">
											<xsl:if test="position() = 1">
												<div class="word-wrap">
													<span class="lead-lg-regular">
														<xsl:value-of select="addressline" />
													</span>
												</div>
												<xsl:call-template name="borrowerAddress">
													<xsl:with-param name="BorrowerPartyNodes" select="$BorrowerPartyNodes" />
												</xsl:call-template>
											</xsl:if>
										</xsl:for-each>
									</div>
								</xsl:if>
								
								<xsl:variable name="SellerPartyNodes" select="$PartyNodes [lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='PropertySeller']" />
								<xsl:if test="count($SellerPartyNodes) &gt; 0  and sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType !='Refinance'">
									<div class="col-4">
										<div class="h5-grey">SELLER</div>
										<div class="messageToggle">
											<xsl:call-template name="borrowerNames">
												<xsl:with-param name="BorrowerPartyNodes" select="$SellerPartyNodes" />
											</xsl:call-template>
										</div>
										<xsl:call-template name="borrowerAddress">
											<xsl:with-param name="BorrowerPartyNodes" select="$SellerPartyNodes" />
										</xsl:call-template>
									</div>
								</xsl:if>
								
								<div class="col-4">
									<div class="h5-grey">LENDER</div>
									<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
										<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='NotePayTo'">
											<div class="word-wrap">
												<span class="lead-lg-regular">
													<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
												</span>
											</div>
										</xsl:if>
									</xsl:for-each>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="loanInfo">
					<div class="h2-blue">
						<span>Loan Information</span>
					</div>
					<div class="col-12 container content bg-white body-md-regular">
						<div id="loanInfoTab" class="tabs-pane">
							<div class="row">
								<div class="col-3">
									<div class="h5-grey">LOAN TERM</div>
									<div class="word-wrap messageToggle">
										<span class="lead-lg-regular">
											<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm !='' ">
												<xsl:call-template name="loantermformat">
													<xsl:with-param name="loantermvalue" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm" />
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="(sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm !='')and (sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum !='')">
												<span> - </span>
											</xsl:if>
											<xsl:if test="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum !='' ">
												<xsl:call-template name="loantermformat">
													<xsl:with-param name="loantermvalue" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum" />
												</xsl:call-template>
											</xsl:if>
										</span>
									</div>
								</div>
								<div class="col-3">
									<div class="h5-grey">PURPOSE</div>
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType" />
										</span>
									</div>
								</div>
								<div class="col-3">
									<div class="h5-grey">PRODUCT</div>
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:AMORTIZATION/lclaln:AMORTIZATION_RULE/lclaln:AmortizationType" />
										</span>
									</div>
								</div>
								<div class="col-3">
									<div class="h5-grey">LOAN TYPE</div>
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedMortgageTerm/lclaln:LoanType" />
										</span>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-3">
									<div class="h5-grey">LOAN ID #</div>
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:value-of select="sch:EvaluationResults/lclaln:LOAN_IDENTIFIERS/lclaln:LOAN_IDENTIFIER[lclaln:LoanIdentifierType='LenderLoan']/lclaln:LoanIdentifier" />
										</span>
									</div>
								</div>
								<div class="col-3">
									<div class="h5-grey">MIC #</div>
									<div class="word-wrap">
										<span class="lead-lg-regular">
											<xsl:value-of select="sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:MI_DATA/lclaln:MI_DATA_DETAIL/lclaln:MICertificateIdentifier" />
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div id="evalMsgsCount" class="row">
					<div class="col-12">
						<div class="h2-blue">
							<span class="evalMsgSecHeader">Evaluation Messages</span>
							<span class="evalMsgCount">
								<xsl:value-of select="$evalMsgCount" />
							</span>
						</div>
					</div>
					<div id="msgsTabAnchor" />
				</div>
				
				<span id="generalMsgs" />
				<div class="container content bg-white body-md-regular">
					<div id="generalMsgsTab" class="tabs-pane active">
						<div class="row" id="msgsBody">
							<div class="col-12">
								<div class="row active">
									<xsl:variable name="giinfoYellowCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])- count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:MessageCode='MSGGI17'])" />
									<xsl:variable name="giinfoRedCount" select="count(sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
									<div class="col-12" id="sec_giTable">
										<div class="row loanSectionHeader" id="giRowDD">
											<div class="col-12">
												<div class="row">
													<h1 class="h3-grey">
														<span>General Info</span>
														<xsl:call-template name="sectionCircle">
															<xsl:with-param name="redCount" select="$giinfoRedCount" />
															<xsl:with-param name="yellowCount" select="$giinfoYellowCount" />
														</xsl:call-template>
													</h1>
												</div>
											</div>
										</div>
										
										<xsl:variable name="giinfoGreenNodes" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Green']" />
										<xsl:variable name="giinfoRedNodes" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Red']" />
										<xsl:variable name="allYellow" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Yellow']" />
										<xsl:variable name="giinfoYellowNodes" select="$allYellow [lclaln:MessageCode != 'MSGGI17']" />
										
										<div class="row" id="giTable">
										
											<xsl:if test="count($giinfoGreenNodes)&gt;0">
												<xsl:call-template name="lesMessages">
													<xsl:with-param name="message" select="$giinfoGreenNodes" />
													<xsl:with-param name="color" select="'GREEN'" />
												</xsl:call-template>
											</xsl:if>
											
											<xsl:if test="count($giinfoRedNodes)&gt;0">
												<xsl:call-template name="lesMessages">
													<xsl:with-param name="message" select="$giinfoRedNodes" />
													<xsl:with-param name="color" select="'RED'" />
												</xsl:call-template>
											</xsl:if>
											
											<xsl:if test="count($giinfoYellowNodes)&gt;0">
												<xsl:call-template name="lesMessages">
													<xsl:with-param name="message" select="$giinfoYellowNodes" />
													<xsl:with-param name="color" select="'YELLOW'" />
												</xsl:call-template>
											</xsl:if>
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<span id="dataQualityMsgs" />
				<xsl:variable name="dqinfoMessages" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message" />
				<xsl:variable name="dqinfoRedCount" select="count($dqinfoMessages[lclaln:DecisionResult='Red'])" />
				<xsl:variable name="dqinfoYellowCount" select="count($dqinfoMessages[lclaln:DecisionResult='Yellow'])" />
				<xsl:variable name="dqinfoGreenCount" select="count($dqinfoMessages[lclaln:DecisionResult='Green'])" />
				<xsl:if test="$dqinfoRedCount+$dqinfoRedCount+$dqinfoGreenCount != 0">
					<div class="content container bg-white body-md-regular">
						<span id="dqCount">
							<xsl:value-of select="$dqinfoRedCount+$dqinfoRedCount+$dqinfoGreenCount" />
						</span>
						<div id="dataQualityMsgsTab" class="tabs-pane">
							<div class="row">
								<div class="col-12">
								
									<div class="col-12 noPadColumn">
										<div class="h3-grey loanSectionHeader">
											<span>Data Quality</span>
											<xsl:call-template name="sectionCircle">
												<xsl:with-param name="redCount" select="$dqinfoRedCount" />
												<xsl:with-param name="yellowCount" select="$dqinfoYellowCount" />
											</xsl:call-template>
										</div>
									</div>
								
									<xsl:for-each select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message">
										<xsl:variable name="i" select="position()" />
										<xsl:variable name="sectionName" select="lclaln:Section/lclaln:SectionName" />

										<div id="sec_{$i}">
											<!-- OR necessary to display last section (has no next section) -->
											<xsl:if test="(lclaln:Section/lclaln:SectionName != following::*[1]/lclaln:Section/lclaln:SectionName)                          or not(following::*[1]/lclaln:Section/lclaln:SectionName)">
												<xsl:attribute name="class">dqSection accordion</xsl:attribute>
												<xsl:variable name="j" select="position()" />
												<xsl:variable name="sectionDetail" select="lclaln:Section/lclaln:SectionName" />
												<xsl:variable name="sectionValue" select="lclaln:Section/lclaln:SectionValue" />
												<xsl:variable name="dqinfoGreenNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Green']" />
												<xsl:variable name="dqinfoRedNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Red']" />
												<xsl:variable name="dqinfoYellowNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Yellow']" />
												
												<input id="accordion_{$i}" class="accordion-control" type="checkbox" checked="" />
												<label for="accordion_{$i}" class="row loanSection">
													<div id="dqRowDD_{$i}">
														<div class="col-10 noPadColumn">
															<div class="h3-blue">
																<xsl:if test="lclaln:Section/lclaln:SectionValue != 'GSE'">
																	SECTION
																</xsl:if>
																<xsl:value-of select="$sectionValue" />
																-
																<xsl:value-of select="$sectionDetail" />
																
																<xsl:variable name="dqinfoSectionRedCount" select="count($dqinfoRedNodes)" />
																<xsl:variable name="dqinfoSectionYellowCount" select="count($dqinfoYellowNodes)" />
																<xsl:variable name="dqinfoSectionGreenCount" select="count($dqinfoGreenNodes)" />

																<xsl:call-template name="sectionCircle">
																	<xsl:with-param name="redCount" select="$dqinfoSectionRedCount" />
																	<xsl:with-param name="yellowCount" select="$dqinfoSectionYellowCount" />
																</xsl:call-template>
															</div>
														</div>
														
													</div>
												</label>
												<div class="accordion-pane">
													<div class="accordion-article">
														<div class="row">
															<xsl:variable name="dqinfoSectionGreenCount1" select="count($dqinfoGreenNodes)" />
															<xsl:variable name="dqinfoSectionRedCount1" select="count($dqinfoRedNodes)" />
															<xsl:variable name="dqinfoSectionYellowCount1" select="count($dqinfoYellowNodes)" />

															<xsl:choose>
																<xsl:when test="$dqinfoSectionGreenCount1&gt; 0">
																	<xsl:call-template name="lesMessages">
																		<xsl:with-param name="message" select="$dqinfoGreenNodes" />
																		<xsl:with-param name="color" select="'GREEN'" />
																		<xsl:with-param name="type" select="'DQ'" />
																	</xsl:call-template>
																</xsl:when>

																<xsl:otherwise>
																	<xsl:if test="$dqinfoSectionRedCount1&gt;0">
																		<xsl:call-template name="lesMessages">
																			<xsl:with-param name="message" select="$dqinfoRedNodes" />
																			<xsl:with-param name="color" select="'RED'" />
																			<xsl:with-param name="type" select="'DQ'" />
																		</xsl:call-template>
																	</xsl:if>
																	
																	<xsl:if test="$dqinfoSectionYellowCount1&gt;0">
																		<xsl:call-template name="lesMessages">
																			<xsl:with-param name="message" select="$dqinfoYellowNodes" />
																			<xsl:with-param name="color" select="'YELLOW'" />
																			<xsl:with-param name="type" select="'DQ'" />
																		</xsl:call-template>
																	</xsl:if>
																</xsl:otherwise>
															</xsl:choose>

														</div>
													</div>
												</div>
											</xsl:if>
										</div>
									</xsl:for-each>
								</div>
							</div>
						</div>
					</div>
				</xsl:if>
				
				<span id="eligibilityMsgs" />
				<xsl:variable name="eginfoMessages" select="sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message" />
				<xsl:variable name="eginfoRedCount" select="count($eginfoMessages[lclaln:DecisionResult='Red'])" />
				<xsl:variable name="eginfoYellowCount" select="count($eginfoMessages[lclaln:DecisionResult='Yellow'])" />
				<xsl:variable name="eginfoGreenCount" select="count($eginfoMessages[lclaln:DecisionResult='Green'])" />
				<xsl:if test="$eginfoYellowCount + $eginfoRedCount + $eginfoGreenCount != 0">
					<div class="content container bg-white body-md-regular">
						<span id="egCount">
							<xsl:value-of select="$eginfoYellowCount+$eginfoRedCount+$eginfoGreenCount" />
						</span>

						<div id="eligibilityMsgsTab" class="tabs-pane">
							<div class="row" id="msgsBody">
								<div class="col-12">
									<div class="row">
										<div class="col-12" id="sec_eligTable">
											<div class="row loanSectionHeader" id="eligRowDD">
												<div class="col-10">
													<div class="row">
														<h1 class="h3-grey">
															<span>Eligibility</span>
															<xsl:call-template name="sectionCircle">
																<xsl:with-param name="redCount" select="$eginfoRedCount" />
																<xsl:with-param name="yellowCount" select="$eginfoYellowCount" />
															</xsl:call-template>
														</h1>
													</div>
												</div>
											</div>
											
											<xsl:variable name="egGreenNodes" select="$eginfoMessages[lclaln:DecisionResult = 'Green']" />
											<xsl:variable name="egRedNodes" select="$eginfoMessages[lclaln:DecisionResult = 'Red']" />
											<xsl:variable name="egYellowNodes" select="$eginfoMessages[lclaln:DecisionResult = 'Yellow']" />
											
											<div id="eligTable" class="row">
												<xsl:if test="count($egGreenNodes)&gt;0">
													<xsl:call-template name="lesMessages">
														<xsl:with-param name="message" select="$egGreenNodes" />
														<xsl:with-param name="color" select="'GREEN'" />
													</xsl:call-template>
												</xsl:if>
												
												<xsl:if test="count($egRedNodes)&gt;0">
													<xsl:call-template name="lesMessages">
														<xsl:with-param name="message" select="$egRedNodes" />
														<xsl:with-param name="color" select="'RED'" />
													</xsl:call-template>
												</xsl:if>
												
												<xsl:if test="count($egYellowNodes)&gt;0">
													<xsl:call-template name="lesMessages">
														<xsl:with-param name="message" select="$egYellowNodes" />
														<xsl:with-param name="color" select="'YELLOW'" />
													</xsl:call-template>
												</xsl:if>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</xsl:if>
			</section>
		</div>
	</section>
</body>
</html>
</xsl:template>

<xsl:template name="lesMessages">
	<xsl:param name="message" />
	<xsl:param name="color" />
	<xsl:param name="type" select="GIEG" />

	<div class="row noMarginRow avoid-break">
		<div class="col-12">
			<div class="row messageRow">
				<div class="col-12">
					<div class="col-1 noLeftPadding">
						<xsl:if test="$type='DQ'">
							<xsl:attribute name="class">col-1</xsl:attribute>
						</xsl:if>
						<div class="h5-grey">RESULTS</div>
					</div>
					<div class="col-2">
						<div class="h5-grey">MESSAGE CODE</div>
					</div>
					<div class="col-8 noPadColumn">
						<div class="h5-grey">MESSAGES</div>
					</div>
					<div class="col-1 lesMsgIconPadding">
						<div class="h5-grey" />
					</div>
				</div>
			</div>
			<xsl:for-each select="$message">
				<xsl:variable name="mNum" select="position()" />
				<div class="row messageRow">
					<div class="col-12">
						<div class="col-1 noLeftPadding">
							<xsl:if test="$type='DQ'">
								<xsl:attribute name="class">col-1</xsl:attribute>
							</xsl:if>
							<xsl:if test="$mNum=1">
								<xsl:choose>
									<xsl:when test="$color = 'RED'">
										<span class="lead-lg-regular red-color">RED</span>
									</xsl:when>
									<xsl:when test="$color = 'YELLOW'">
										<span class="lead-lg-regular yellow-color">YELLOW</span>
									</xsl:when>
									<xsl:when test="$color = 'GREEN'">
										<span class="lead-lg-regular green-color">GREEN</span>
									</xsl:when>
								</xsl:choose>
							</xsl:if>
						</div>
						<div class="col-2 lead-lg-regular"><xsl:value-of select="lclaln:MessageCode" /></div>
						<div class="col-8 noPadColumn">
							<xsl:choose>
								<xsl:when test="$color = 'GREEN'">
									<div class="lead-lg-regular"><xsl:value-of select="lclaln:MessageText" /></div>
								</xsl:when>
								<xsl:otherwise>
									<div class="lead-lg-regular messageRow wordWrap">
										<span class="display2-blue">Business: </span>
										<span> <xsl:value-of select="lclaln:MessageText" /> </span>
									</div>
									<div class="lead-lg-regular messageRow wordWrap">
										<span class="display2-blue">Technical: </span>
										<span> <xsl:value-of select="lclaln:MessageTextOtherDescription" /> </span>
									</div>
								</xsl:otherwise>
							</xsl:choose>
						</div>
						<div class="col-1 lesMsgIconPadding">
							<xsl:if test="$mNum=1">
								<span>
									<xsl:choose>
										<xsl:when test="$color = 'RED'">
											<i class="ic-critical red-color results-icon-spacing right" />
										</xsl:when>
										<xsl:when test="$color = 'YELLOW'">
											<i class="ic-error yellow-color results-icon-spacing right" />
										</xsl:when>
										<xsl:when test="$color = 'GREEN'">
											<i class="ic-pass green-color results-icon-spacing right" />
										</xsl:when>
									</xsl:choose>
								</span>
							</xsl:if>
						</div>
					</div>
				</div>
			</xsl:for-each>
		</div>
	</div>
</xsl:template>

<xsl:template name="sectionCircle">
	<xsl:param name="redCount" />
	<xsl:param name="yellowCount" />
	
	<xsl:choose>
		<xsl:when test="$redCount&gt;0 and $yellowCount = 0">
			<div class="circle-red-1 lesMsgCircleMargin">
				<xsl:choose>
					<xsl:when test="$redCount &gt; 9">
						<xsl:attribute name="class">circle-red-2 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:when test="$redCount &gt; 99">
						<xsl:attribute name="class">circle-red-3 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:when test="$redCount &gt; 999">
						<xsl:attribute name="class">circle-red-4 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class">circle-red-1 lesMsgCircleMargin</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
			
				<span>
					<xsl:value-of select="$redCount" />
				</span>
			</div>
		</xsl:when>
		<xsl:when test="$redCount=0 and $yellowCount&gt;0">
			<div class="circle-yellow-1 lesMsgCircleMargin">
				<xsl:choose>
					<xsl:when test="$yellowCount &gt; 9">
						<xsl:attribute name="class">circle-yellow-2 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:when test="$yellowCount &gt; 99">
						<xsl:attribute name="class">circle-yellow-3 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:when test="$yellowCount &gt; 999">
						<xsl:attribute name="class">circle-yellow-4 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class">circle-yellow-1 lesMsgCircleMargin</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				
				<span>
					<xsl:value-of select="$yellowCount" />
				</span>
			</div>
		</xsl:when>
		<xsl:when test="$redCount&gt;0 and $yellowCount&gt;0">
			<div class="circle-red-1 lesMsgCircleMargin">
				<xsl:choose>
					<xsl:when test="$redCount &gt; 9">
						<xsl:attribute name="class">circle-red-2 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:when test="$redCount &gt; 99">
						<xsl:attribute name="class">circle-red-3 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:when test="$redCount &gt; 999">
						<xsl:attribute name="class">circle-red-4 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class">circle-red-1 lesMsgCircleMargin</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				
				<span>
					<xsl:value-of select="$redCount" />
				</span>
			</div>
			<div class="circle-yellow-1 lesMsgCircleMargin">
				<xsl:choose>
					<xsl:when test="$yellowCount &gt; 9">
						<xsl:attribute name="class">circle-yellow-2 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:when test="$yellowCount &gt; 99">
						<xsl:attribute name="class">circle-yellow-3 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:when test="$yellowCount &gt; 999">
						<xsl:attribute name="class">circle-yellow-4 lesMsgCircleMargin</xsl:attribute>
					</xsl:when>
					<xsl:otherwise>
						<xsl:attribute name="class">circle-yellow-1 lesMsgCircleMargin</xsl:attribute>
					</xsl:otherwise>
				</xsl:choose>
				
				<span>
					<xsl:value-of select="$yellowCount" />
				</span>
			</div>
		</xsl:when>
		<xsl:otherwise>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>