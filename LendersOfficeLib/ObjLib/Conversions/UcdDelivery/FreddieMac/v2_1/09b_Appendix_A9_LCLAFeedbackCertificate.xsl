﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:sch="http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas" xmlns:lclaln="http://lclaloanresults.freddiemac.com/schema/types" xmlns:msxsl="urn:schemas-microsoft-com:xslt">

<xsl:include href="09f_Appendix_A9_LCLACommonCertificate.xsl" />

<xsl:template match="sch:LCLALoanEvaluationServiceResponse">

	<xsl:call-template name="feedbackCertificate">
		<xsl:with-param name="evaluationResults" select="sch:EvaluationResults" />
		<xsl:with-param name="certificateType" select="'summary'" />
	</xsl:call-template>

</xsl:template>
</xsl:stylesheet>