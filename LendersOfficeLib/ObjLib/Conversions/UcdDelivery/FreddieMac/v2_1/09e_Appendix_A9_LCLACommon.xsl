﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:sch="http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas" xmlns:lclaln="http://lclaloanresults.freddiemac.com/schema/types" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:param name="VirtualRoot" xmlns:msxsl="urn:schemas-microsoft-com:xslt" />
 
<!-- Version 2.1 --> 
 
<xsl:template name="date">
	<xsl:param name="yyyy-mm-dd" />
	
	<xsl:if test="$yyyy-mm-dd !=''">
		<xsl:if test="string-length($yyyy-mm-dd) &gt;9">
			<xsl:variable name="yyyy" select="substring-before($yyyy-mm-dd, '-')" />
			<xsl:variable name="mm-dd" select="substring-after($yyyy-mm-dd, '-')" />
			<xsl:variable name="dd" select="substring-after($mm-dd, '-')" />
			<xsl:variable name="mm" select="substring-before($mm-dd, '-')" />
			<xsl:value-of select="concat($mm,'/',$dd,'/',$yyyy)" />
		</xsl:if>
	</xsl:if>
</xsl:template>


<xsl:template name="loantermformat">
	<xsl:param name="loantermvalue" />
	
	<xsl:variable name="loanTermP" select="substring-after($loantermvalue,'P')" />
	<xsl:variable name="loanterm" select="substring-before($loanTermP,'M')" />
	<xsl:variable name="years" select="floor($loanterm div 12)" />
	<xsl:variable name="months" select="$loanterm mod 12" />
	
	<xsl:if test="$years = 1 and $months = 0">
		<xsl:value-of select="concat($years,' Year')" />
	</xsl:if>
	<xsl:if test="$years = 1 and $months = 1">
		<xsl:value-of select="concat($years,' Year, ' ,$months, ' Month' )" />
	</xsl:if>
	<xsl:if test="$years = 1 and $months &gt; 1">
		<xsl:value-of select="concat($years,' Year, ' ,$months, ' Months')" />
	</xsl:if>
	<xsl:if test="$years &gt; 1 and  $months = 0">
		<xsl:value-of select="concat($years,' Years ')" />
	</xsl:if>
	<xsl:if test="$years &gt; 1 and $months &gt; 1">
		<xsl:value-of select="concat($years,' Years, ' ,$months, ' Months')" />
	</xsl:if>
	<xsl:if test="$years &gt; 1 and $months = 1">
		<xsl:value-of select="concat($years,' Years, ' ,$months, ' Month')" />
	</xsl:if> 
	<xsl:if test="$years &lt; 1 and $months &gt; 1">
		<xsl:value-of select="concat($months, ' Months')" />
	</xsl:if>
	<xsl:if test="$years &lt; 1 and $months = 1">
		<xsl:value-of select="concat($months, ' Month')" />
	</xsl:if>
</xsl:template>

<xsl:template name="formatDateTime">
	<xsl:param name="dateTime" />
	
	<xsl:variable name="YYYY" select="substring-before($dateTime, '-')" />
	<xsl:variable name="MM-DDTHHmmss" select="substring-after($dateTime, '-')" />
	<xsl:variable name="MM" select="substring-before($MM-DDTHHmmss, '-')" />
	<xsl:variable name="DDTHHmmss" select="substring-after($MM-DDTHHmmss, '-')" />
	<xsl:variable name="DD" select="substring-before($DDTHHmmss, 'T')" />
	<xsl:variable name="hhmmss" select="substring-after($DDTHHmmss, 'T')" />
	<xsl:variable name="HH" select="substring-before($hhmmss, ':')" />
	<xsl:variable name="mmss" select="substring-after($hhmmss, ':')" />
	<xsl:variable name="mm" select="substring-before($mmss, ':')" />
	<xsl:variable name="ssExtra" select="substring-after($mmss, ':')" />
	<xsl:variable name="ss" select="substring-before($ssExtra, '.')" />
	
	<xsl:variable name="formattedDateTime" select="concat($MM, '/', $DD, '/', $YYYY, ' ', $HH, ':', $mm, ':', $ss)" />
	<xsl:value-of select="$formattedDateTime" />
</xsl:template>

<xsl:template name="borrowerNames">
	<xsl:param name="BorrowerPartyNodes" />

	<xsl:for-each select="$BorrowerPartyNodes">
		<xsl:variable name="i" select="position()" />
		<xsl:if test="$i &lt; 3">
			<xsl:if test="$i = 2">
				<span class="lead-lg-regular">
					<xsl:text> and </xsl:text>
				</span>
			</xsl:if>
			<span class="lead-lg-regular">
				<xsl:value-of select="lclaln:INDIVIDUAL/lclaln:NAME/lclaln:FirstName" />
				<xsl:text> </xsl:text>
				<xsl:value-of select="lclaln:INDIVIDUAL/lclaln:NAME/lclaln:LastName" />
			</span>
			<span class="lead-lg-regular">
				<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
			</span>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<xsl:template name="borrowerAddress">
	<xsl:param name="BorrowerPartyNodes" />

	<xsl:for-each select="$BorrowerPartyNodes">
		<xsl:if test="position() = 1">
			<div class="word-wrap">
				<span class="lead-lg-regular">
					<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:AddressLineText" />
				</span>
			</div>
			<div class="word-wrap">	
				<span class="lead-lg-regular">
					<xsl:if test="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:CityName!= ''">
						<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:CityName" />
						<xsl:text>, </xsl:text>
					</xsl:if>
					<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:StateCode" />
					<xsl:text> </xsl:text>
					<xsl:value-of select="lclaln:ADDRESSES/lclaln:ADDRESS/lclaln:PostalCode" />
				</span>
			</div>
		</xsl:if>
	</xsl:for-each>
</xsl:template>
	
</xsl:stylesheet>