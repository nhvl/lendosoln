﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:sch="http://www.freddiemac.com/lcla/LCLALoanEvaluationService/schemas" xmlns:lclaln="http://lclaloanresults.freddiemac.com/schema/types" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
  <xsl:import href="09e_Appendix_A9_LCLACommon.xsl" />
  <xsl:param name="VirtualRoot" />


<!-- Version 2.1 -->

<xsl:template name="feedbackCertificate">
<xsl:param name="evaluationResults" />
<xsl:param name="certificateType" />

<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/style.css')}" media="screen,print" />
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/css/lcla_style.css')}" media="screen,print" />
    <link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/css/lcla-global.css')}" media="screen,print" />
	<link rel="stylesheet" href="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/css/lcla_loan_eval_rslts.css')}" media="screen,print" />
	
    <title>Loan Closing Advisor Feedback Certificate</title>
 
	<style>
		@media print {
		  #printWrapper, #footerWrapper {
			display:none !important;
		  }
		  
		  pre {page-break-inside:auto !important;}
		  .main-nav { position:relative !important}
		  .container { padding-top:20px; margin:0 !important}
		  .printArea { width:95% !important}
		  .panel-top, .panel-padding { padding-right:0 !important; padding-left:0 !important;}
		  .panel { border:0 !important} 
		  a[href]:after { content:none !important}
		  .nofixhdr { position:relative !important; width:0;}
		  .noprintpad { padding-top:0px !important}
		  body { width:1040px !important}
		}
	</style>
</head>	
 
<body style="margin:0px; min-width:1000px;">
	<section class="container content bg-white" id="mainBody">
		<section class="col-12">
			<section class="container content" style="margin: 0px; padding: 0px;">
				<section class="row">
					<div class="col-12 printArea noPadColumn" style="padding-bottom: 20px;">
						<div class="col-6" style="margin-top: 8px;">
							<img class="logo" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/Asset/lgo-freddiemac.svg')}" />
						</div>
						<div class="col-6 right" style="text-align: right;">
							<img style="height:32px; width:32px;padding-right:10px" class="logo1" src="{concat($VirtualRoot,'/pml_shared/FreddieMacUcd/v2.1/resources/Asset/ic-app-lcla.svg')}" />
							<span class="h1-blue" style="vertical-align: middle;">Loan Closing Advisor<span class="servicemark" /></span>
						</div>
					</div>
				</section>
			</section>
			<xsl:variable name="generalInfoRedCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
			<section class="container content first bg-white col-12" style="border-top-color:#009ce0;margin-top:0px; padding-bottom:0px; margin-bottom: 40px;">
				<div class="row center" style="margin-bottom: 0px;  margin-left:50px; margin-right:50px; ">
					<i class="ic-certificate active" style="verticle-align:middle;" />
					<span class="h1-blue" style=" padding-left:20px; vertical-align: top;">Loan Closing Advisor Feedback Certificate</span>
				</div>
			</section>
			<section class="container content  bg-white col-12" style="margin-top:0px; padding-top:10px !important;padding-bottom:0px; margin-bottom: 60px;">
				<div class="row center" style="margin-bottom: 0px;  margin-left:50px; margin-right:50px; vertical-align: middle;">
					<xsl:variable name="overallStatus" select="$evaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:OverallUCDRequirementStatus" />
					<xsl:if test="$overallStatus">
						<span class="col-12 overallUcd" style="vertical-align: top;">
							<span class="callout-md-blue">Overall UCD Requirement:</span>
							<xsl:choose>
								<xsl:when test="$overallStatus = 'Satisfied'">
									<i class="ic-confirmation green-color" style="padding:0px 20px !important;font-size:30px; vertical-align: baseline;" />
									<span class="callout-md-green" style="vertical-align: baseline;"><xsl:value-of select="'Satisfied'" /></span>
								</xsl:when>
								<xsl:when test="$overallStatus = 'Satisfied Without a Closing Disclosure PDF'">
									<i class="ic-confirmation green-color" style="padding:0px 20px !important;font-size:30px; vertical-align: baseline;" />
									<span class="callout-md-green" style="vertical-align: baseline;"><xsl:value-of select="'Satisfied' " /></span>
									<span class="body-lg-regular"> <xsl:value-of select="' (Without a Closing Disclosure PDF)'" /></span>
								</xsl:when>
								<xsl:when test="$overallStatus = 'Not Satisfied'">
									<i class="ic-error yellow-color" style="padding:0px 20px !important; font-size: 30px; vertical-align: baseline;" />
									<span class="callout-md-yellow" style="vertical-align: baseline;"><xsl:value-of select="'Not Satisfied'" /></span>
								</xsl:when>
								<xsl:otherwise>
									<span class="callout-md-yellow" style="vertical-align: baseline;" />
								</xsl:otherwise>
							</xsl:choose>
						</span>
					</xsl:if>
					<span class="col-12" style="vertical-align: top;">
						<span class="callout-sm-blue">This Submission's UCD Requirement Status:</span>
						<xsl:choose>
							<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Satisfied'">
								<i class="ic-confirmation green-color" style="padding:0px 20px !important;font-size:24px; vertical-align: baseline;" />
								<span class="callout-sm-green" style="vertical-align: baseline;"><xsl:value-of select="'Satisfied'" /></span>
							</xsl:when>
							<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Satisfied Without a Closing Disclosure PDF'">
								<i class="ic-confirmation green-color" style="padding:0px 20px !important;font-size:24px; vertical-align: baseline;" />
								<span class="callout-sm-green" style="vertical-align: baseline;"><xsl:value-of select="'Satisfied' " /></span>
								<span class="body-md-regular"> <xsl:value-of select="' (Without a Closing Disclosure PDF)'" /></span>
							</xsl:when>
							<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:UCDRequirement/lclaln:UCDRequirementStatus = 'Not Satisfied'">
								<i class="ic-error yellow-color" style="padding:0px 20px !important; font-size: 24px; vertical-align: baseline;" />
								<span class="callout-sm-yellow" style="vertical-align: baseline;"><xsl:value-of select="'Not Satisfied'" /></span>
							</xsl:when>
							<xsl:otherwise>
								<span class="callout-sm-yellow" style="vertical-align: baseline;" />
							</xsl:otherwise>
						</xsl:choose>
					</span>	
				</div>
			</section>
	
			<section class="col-12  printArea noPadColumn" style="margin-left: 0px;">
				<div class="col-6 noLeftPadding">
					<div class="h3-grey line" style="padding-bottom: 15px; margin-bottom: 40px;">Details</div>
					<div class="row certificate-row">
						<div class="col-12 noPadColumn">
							<xsl:variable name="PartyNodes" select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY" />
							<xsl:variable name="BorrowerPartyNodes" select="$PartyNodes [lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='Borrower']" />
							<xsl:if test="count($BorrowerPartyNodes) &gt; 0">
								<div class="col-4">
									<div class="h5-grey">BORROWER</div>
									<div class="messageToggle">
										<xsl:call-template name="borrowerNames">
											<xsl:with-param name="BorrowerPartyNodes" select="$BorrowerPartyNodes" />
										</xsl:call-template>
									</div>
									<xsl:call-template name="borrowerAddress">
										<xsl:with-param name="BorrowerPartyNodes" select="$BorrowerPartyNodes" />
									</xsl:call-template>
								</div>
							</xsl:if>
						
							<div class="col-4">
								<div class="h5-grey">PROPERTY ADDRESS</div>
								<xsl:variable name="propertyAddressLine" select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:AddressLineText" />
								<div class="word-wrap messageToggle">
									<span class="lead-lg-regular" title="{$propertyAddressLine}"><xsl:value-of select="$propertyAddressLine" /></span>
								</div>
								<div class="word-wrap messageToggle">		 		    		
									<span class="lead-lg-regular">
										<xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:CityName" />
										<xsl:if test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:CityName != '' ">
											<xsl:text>, </xsl:text>
										</xsl:if>
										<xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:StateCode" /><xsl:text> </xsl:text>
										<xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:ADDRESS/lclaln:PostalCode" />
									</span>
								</div>
							</div>
							<div class="col-4">
								<div class="h5-grey ">CLOSING DISCLOSURE TYPE</div>
								<div class="word-wrap">						
									<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureType" /></span>
								</div>
							</div>
						</div>
					</div>
					<div class="row certificate-row">
						<div class="col-12 noPadColumn">
							<div class="col-4">
								<div class="h5-grey">CLOSING DATE</div> 
								<div class="word-wrap">
									<span class="lead-lg-regular">
										<xsl:copy>
											<xsl:call-template name="date">
												<xsl:with-param name="yyyy-mm-dd" select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:ClosingDate" />
											</xsl:call-template>
										</xsl:copy>
									</span>
								</div> 
							</div>
							<div class="col-4">
								<div class="h5-grey">DISBURSEMENT DATE</div>
								<div class="word-wrap">
									<span class="lead-lg-regular">
										<xsl:copy>
											<xsl:call-template name="date">
												<xsl:with-param name="yyyy-mm-dd" select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CLOSING_INFORMATION/lclaln:CLOSING_INFORMATION_DETAIL/lclaln:DisbursementDate" />
											</xsl:call-template>
										</xsl:copy>
									</span>
								</div>	
							</div>
							<div class="col-4">
								<xsl:choose>
									<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType != 'Refinance'">
										<div class="h5-grey">SALE PRICE</div>
										<xsl:if test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:SalePrice">
											<div class="lead-lg-regular word-wrap">
												<xsl:text>$</xsl:text>
												<xsl:value-of select="format-number($evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:SalePrice,&quot;###,###.##&quot;)" />
											</div>
										</xsl:if>		
									</xsl:when>
									<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType = 'Refinance'">
										<div class="h5-grey">APPRAISED PROP. VALUE</div>
										<xsl:if test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:AppraisedValue">
											<div class="lead-lg-regular word-wrap">
												<xsl:text>$</xsl:text>
												<xsl:value-of select="format-number($evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedPrice/lclaln:AppraisedValue,'###,###.##')" />
											</div>
										</xsl:if>		
									</xsl:when>
								</xsl:choose>
							</div>
						</div>
					</div>
					<div class="row certificate-row">
						<div class="col-12 noPadColumn">
							<div class="col-4">
								<div class="h5-grey">LENDER</div>
								<xsl:for-each select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY">
									<xsl:if test="lclaln:ROLES/lclaln:ROLE/lclaln:ROLE_DETAIL/lclaln:PartyRoleType='NotePayTo'">
										<div class="word-wrap">
											<span class="lead-lg-regular">
												<xsl:value-of select="lclaln:LEGAL_ENTITY/lclaln:LEGAL_ENTITY_DETAIL/lclaln:FullName" />
											</span>
										</div>
									</xsl:if>
								</xsl:for-each>
							</div>
							<div class="col-4">
								<div class="h5-grey">LENDER LOAN ID</div>
								<div class="word-wrap">
									<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LOAN_IDENTIFIERS/lclaln:LOAN_IDENTIFIER[lclaln:LoanIdentifierType='LenderLoan']/lclaln:LoanIdentifier" /></span>
								</div>
							</div>				       
						</div>
					</div>
				</div>
				<div class="col-6 noPadColumn">
					<div class="h3-grey line" style="padding-bottom: 15px; margin-bottom: 40px;">Evaluation Results</div>
					<div class="row">
						<div class="row certificate-row">
							<div class="col-12">
								<div class="col-4">
									<div class="h5-grey">GENERAL INFO</div>
								</div>
								<div class="col-4">
									<xsl:variable name="geninfoRedCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
									<xsl:variable name="geninfoYellowCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])- count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:MessageCode='MSGGI17'])" />
									<xsl:variable name="geninfoGreenCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
									<xsl:choose>
										<xsl:when test="$geninfoGreenCount&gt; 0">
											<i class="ic-shield-green left green-color" alt="Green" />
										</xsl:when>
										<xsl:otherwise>
											<xsl:if test="$geninfoRedCount&gt; 0">
												<i class="ic-shield-red left red-color" alt="Red" />
												<span class="lead-lg-regular rslts-cnt-paddRight">
													<xsl:value-of select="$geninfoRedCount" />
												</span>
											</xsl:if>
											<xsl:if test="$geninfoYellowCount&gt; 0">
												<i class="ic-shield-yellow left yellow-color" alt="Yellow" />
												<span class="lead-lg-regular rslts-cnt-paddRight">
													<xsl:value-of select="$geninfoYellowCount" />
												</span>
											</xsl:if>
										</xsl:otherwise>
									</xsl:choose>
								</div>
							</div>
						</div>
						
						<xsl:if test="$generalInfoRedCount &lt; 1">		
							<div class="row certificate-row">
								<div class="col-12">
									<div class="col-4">
										<div class="h5-grey">DATA QUALITY</div>
									</div>
									<div class="col-4">
										<xsl:variable name="dqinfoRedCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
										<xsl:variable name="dqinfoYellowCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
										<xsl:variable name="dqinfoGreenCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
										<xsl:choose>
											<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Conclusion='Green'">
												<i class="ic-shield-green left green-color" alt="Green" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:if test="$dqinfoRedCount &gt; 0">
													<i class="ic-shield-red left red-color" alt="Red" />
													<span class="lead-lg-regular rslts-cnt-paddRight">
														<xsl:value-of select="$dqinfoRedCount" />
													</span>
												</xsl:if>
												<xsl:if test="$dqinfoYellowCount &gt; 0">
													<i class="ic-shield-yellow left yellow-color" alt="Yellow" />
													<span class="lead-lg-regular rslts-cnt-paddRight">
														<xsl:value-of select="$dqinfoYellowCount" />
													</span>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
							</div>
							<div class="row certificate-row">
								<div class="col-12">
									<div class="col-4">
										<div class="h5-grey">ELIGIBILITY</div>
									</div>
									<div class="col-4">
										<xsl:variable name="eginfoRedCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
										<xsl:variable name="eginfoYellowCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
										<xsl:variable name="eginfoGreenCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
										<xsl:choose>
											<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Conclusion='Green'">
												<i class="ic-shield-green left green-color" alt="Green" />
											</xsl:when>
											<xsl:otherwise>
												<xsl:if test="$eginfoRedCount &gt; 0">
													<i class="ic-shield-red left red-color" alt="Red" />
													<span class="lead-lg-regular rslts-cnt-paddRight">
														<xsl:value-of select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
													</span>
												</xsl:if>
												<xsl:if test="$eginfoYellowCount &gt; 0">
													<i class="ic-shield-yellow left yellow-color" alt="Yellow" />
													<span class="lead-lg-regular rslts-cnt-paddRight">
														<xsl:value-of select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
													</span>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
							</div>
						</xsl:if>	
					</div>
				</div>
			</section>
			<xsl:if test="$generalInfoRedCount &gt; 1  or $generalInfoRedCount = 1">		
				<section class="col-12 printArea" style="margin-left: 0px;">
					<div class="row">
						<div class="col-12">
							<span class="lead-md-regular red-color">This feedback certificate is invalid due to General Info errors. Please correct and resubmit.</span>
						</div>
					</div>
				</section>
			</xsl:if>
			<xsl:if test="$generalInfoRedCount &lt; 1">		
				<section class="col-12 printArea noPadColumn" style="margin-left: 0px;">
					<div class="col-6 noLeftPadding">
						<div class="h3-grey line" style="padding-bottom: 15px; margin-bottom: 40px;">Loan Information</div>
						<div class="row certificate-row">
							<div class="col-12 noPadColumn">
								<div class="row certificate-row">
									<div class="col-12">
										<div class="col-4">
											<div class="h5-grey">LOAN TYPE</div>
											<div class="word-wrap"> 
												<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedMortgageTerm/lclaln:LoanType" /></span>
											</div>    
										</div>
										<div class="col-4">
											<div class="h5-grey">LOAN TERM</div>
											<div class="word-wrap messageToggle">
												<span class="lead-lg-regular">
										
													<xsl:if test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm !='' ">
														<xsl:copy>
															<xsl:call-template name="loantermformat">
																<xsl:with-param name="loantermvalue" select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm" />
															</xsl:call-template>
														</xsl:copy>
													</xsl:if>
													<xsl:if test="($evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTerm !='')and ($evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum !='')">	
														<span> - </span>	
													</xsl:if>	
													<xsl:if test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum !='' ">
														<xsl:copy>
															<xsl:call-template name="loantermformat">
																<xsl:with-param name="loantermvalue" select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanTerm/lclaln:LoanTermMaximum" />
														  </xsl:call-template>
														</xsl:copy>
													</xsl:if>
												</span>
											</div>
										</div>
										<div class="col-4">
											<div class="h5-grey">PRODUCT</div>
											<div class="word-wrap">
												<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:AMORTIZATION/lclaln:AMORTIZATION_RULE/lclaln:AmortizationType" /></span>	 		    	
											</div>
										</div>
									</div>
								</div>
								<div class="row certificate-row">
									<div class="col-12">
										<div class="col-4">
											<div class="h5-grey">PURPOSE</div>
											<div class="word-wrap">
												<span class="lead-lg-regular">
												<xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedLoanPurposeType/lclaln:LoanPurposeType" /></span>
											</div> 
										</div>
									  
										<div class="col-4">
											<div class="h5-grey">MIC#</div>
											<div class="word-wrap">
												<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:MI_DATA/lclaln:MI_DATA_DETAIL/lclaln:MICertificateIdentifier" /></span>
											</div>
										</div>
									  
										<div class="col-4">
											<div class="h5-grey">APPRAISAL IDENTIFIER</div>
											<div class="word-wrap">
												<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PROPERTY/lclaln:PROPERTY_VALUATIONS/lclaln:PROPERTY_VALUATION/lclaln:PROPERTY_VALUATION_DETAIL/lclaln:AppraisalIdentifier" /></span>
											</div>
										</div>
									</div>
								</div>
						  
								<div class="row certificate-row">
									<div class="col-12">
										<div class="col-4">
											<div class="h5-grey">AUS#</div>
											<div class="word-wrap">
												<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:UNDERWRITING/lclaln:AUTOMATED_UNDERWRITINGS/lclaln:AUTOMATED_UNDERWRITING/lclaln:AutomatedUnderwritingCaseIdentifier" /></span>		                       </div>
										</div>
										
										<xsl:variable name="ausType" select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:UNDERWRITING/lclaln:AUTOMATED_UNDERWRITINGS/lclaln:AUTOMATED_UNDERWRITING/lclaln:AutomatedUnderwritingSystemType" />
										<xsl:variable name="ausTypeOtherDescription" select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:UNDERWRITING/lclaln:AUTOMATED_UNDERWRITINGS/lclaln:AUTOMATED_UNDERWRITING/lclaln:AutomatedUnderwritingSystemTypeOtherDescription" />
										<div class="col-4">
											<div class="h5-grey">AUS Type</div>
											<div class="word-wrap">
												<span class="lead-lg-regular">
													<xsl:choose>
														<xsl:when test="$ausType = 'LoanProspector'">
															LoanProductAdvisor
														</xsl:when>
														<xsl:when test="$ausType = 'Other' and $ausTypeOtherDescription = 'LoanProductAdvisor'">
															LoanProductAdvisor
														</xsl:when>
														<xsl:otherwise>
															<xsl:value-of select="$ausType" />
														</xsl:otherwise>
													</xsl:choose>
												</span>
											</div>
										</div>
										
										<div class="col-4">
											<div class="h5-grey">MERS MIN</div>
											<div class="word-wrap">
												<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LOAN_IDENTIFIERS/lclaln:LOAN_IDENTIFIER[lclaln:LoanIdentifierType='MERS_MIN']/lclaln:LoanIdentifier" /></span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-6 noPadColumn">
						<div class="h3-grey line" style="padding-bottom: 15px; margin-bottom: 40px;">Transaction Information</div>
						<div class="row certificate-row">
							<div class="col-12 noPadColumn">
								<div class="col-4">
									<xsl:choose>
										<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY/lclaln:ROLES/lclaln:ROLE[lclaln:ROLE_DETAIL/lclaln:PartyRoleTypeOtherDescription='Correspondent']/lclaln:PARTY_ROLE_IDENTIFIERS/lclaln:PARTY_ROLE_IDENTIFIER/lclaln:PartyRoleIdentifier != ''">
											<div class="h5-grey">Correspondent ID</div>
											<div class="word-wrap">
												<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY/lclaln:ROLES/lclaln:ROLE[lclaln:ROLE_DETAIL/lclaln:PartyRoleTypeOtherDescription='Correspondent']/lclaln:PARTY_ROLE_IDENTIFIERS/lclaln:PARTY_ROLE_IDENTIFIER/lclaln:PartyRoleIdentifier" /></span>
											</div>
										</xsl:when>
										<xsl:otherwise>
											<div class="h5-grey">SELLER NUMBER</div>
											<div class="word-wrap">
												<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:PARTIES/lclaln:PARTY/lclaln:ROLES/lclaln:ROLE[lclaln:ROLE_DETAIL/lclaln:PartyRoleTypeOtherDescription='Seller']/lclaln:PARTY_ROLE_IDENTIFIERS/lclaln:PARTY_ROLE_IDENTIFIER/lclaln:PartyRoleIdentifier" /></span>
											</div>
										</xsl:otherwise>
									</xsl:choose>	
								</div>
								
								<div class="col-4">
									<div class="h5-grey">BATCH ID</div>
									<div class="word-wrap">
										<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/@BatchID" /></span>
									</div>
								</div>
								
								<div class="col-4">
									<div class="h5-grey">TRANSACTION ID</div>
									<div class="word-wrap">
										<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/@TransactionID" /></span>
									</div>
								</div>
							</div>
						</div>
								
						<div class="row certificate-row">
							<div class="col-12 noPadColumn">
							
								<div class="col-4">
									<div class="h5-grey">CLOSING DISCLOSURE <br />EMBEDDED PDF</div>
									<div class="word-wrap">
										<xsl:choose>
											<xsl:when test="$evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator != '' and ($evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator ='true'             or $evaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator ='1')">
												<span class="lead-lg-regular"><xsl:value-of select="'Yes'" /></span>
											</xsl:when>
											<xsl:otherwise>
												<span class="lead-lg-regular"><xsl:value-of select="'No'" /></span>
											</xsl:otherwise>
										</xsl:choose>
									</div>
								</div>
									
								<div class="col-4">
									<div class="h5-grey">SUBMISSION NUMBER</div>
									<div class="word-wrap">
										<span class="lead-lg-regular"><xsl:value-of select="$evaluationResults/@SubmissionNumber" /></span>
									</div>
								</div>
								
								<div class="col-4">
									<div class="h5-grey">DATE/TIME EVALUATED</div>
									<div class="word-wrap">
										<span id="fileSubDtJs" class="lead-lg-regular">
											<xsl:call-template name="formatDateTime">
												<xsl:with-param name="dateTime" select="$evaluationResults/@SubmissionDateTime" />
											</xsl:call-template>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="breakhere"> </div>
					
					<xsl:if test="$certificateType = 'detail'">
						<xsl:call-template name="messagesSection">
							<xsl:with-param name="evaluationResults" select="$evaluationResults" />
						</xsl:call-template>
					</xsl:if>
					
				</section>
			</xsl:if>
		</section>

		<div id="footerWrapper">
			<div class="col-12 certificate-footer">
				<div class="col-8">
					<span class="body-md-regular">Loan Closing Advisor Feedback Certificate - </span>
					<span id="fileSubDtJs1" class="body-md-regular">
						<xsl:call-template name="formatDateTime">
							<xsl:with-param name="dateTime" select="$evaluationResults/@SubmissionDateTime" />
						</xsl:call-template>
					</span>
				</div>
				<div class="col-2 right page-number">
					<span class="body-md-regular">Page 1 of 1</span>
				</div>
			</div>
		</div>
	</section>
</body>
</html>
</xsl:template>

<xsl:template name="messagesSection">
	<xsl:param name="evaluationResults" />

	<xsl:variable name="evalMsgCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])                                          + count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])                                          + count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])                                          + count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])                                          + count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])                                          + count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
	<div id="evalMsgsCount" class="row">
		<div class="col-12">
			<div class="row noMarginRow">
				<span class="evalMsgSecHeader h3-blue">Evaluation Messages</span>
				<span class="evalMsgCount">
					<xsl:value-of select="$evalMsgCount" />
				</span>
			</div>
		</div>
		<div id="msgsTabAnchor" />
	</div>

	<div class="row">
		<div id="msgsTab" class="col-12 noPadColumn">

			<div class="row noMarginRow noBottomPadding">
				<div class="col-12">
					<div class="row active noMarginRow">
						<xsl:variable name="giinfoYellowCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
						<xsl:variable name="giinfoRedCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
			
						<div class="row avoid-break noMarginRow" id="sec_giTable">
							<div class="row loanSection" id="giRowDD" style="border-radius:4px;padding-left:20px;padding-bottom:15px;margin-bottom:20px;">
								<div class="col-12">
									<div class="row">
										<h1 class="h3-grey" style="font-size:18px; margin:0px;">General Info</h1>
									</div>
								</div>
							</div>
							
							<div class="row" id="giRow">
								<div class="row giEligTable">
									<div class="col-12">
										<xsl:variable name="giinfoGreenNodes" select="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Green']" />
										<xsl:variable name="giinfoRedNodes" select="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Red']" />
										<xsl:variable name="allYellow" select="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Yellow']" />
										<xsl:variable name="giinfoYellowNodes" select="$allYellow [lclaln:MessageCode != 'MSGGI17']" />
										
										<xsl:if test="count($giinfoGreenNodes)&gt;0">
											<xsl:call-template name="formatGIAndEGMessages">
												<xsl:with-param name="message" select="$giinfoGreenNodes" />
												<xsl:with-param name="color" select="'GREEN'" />
											</xsl:call-template>
										</xsl:if>
										
										<xsl:if test="count($giinfoRedNodes)&gt;0">
											<xsl:call-template name="formatGIAndEGMessages">
												<xsl:with-param name="message" select="$giinfoRedNodes" />
												<xsl:with-param name="color" select="'RED'" />
											</xsl:call-template>
										</xsl:if>				
							
										<xsl:if test="count($giinfoYellowNodes)&gt;0">
											<xsl:call-template name="formatGIAndEGMessages">
												<xsl:with-param name="message" select="$giinfoYellowNodes" />
												<xsl:with-param name="color" select="'YELLOW'" />
											</xsl:call-template>
										</xsl:if>	
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<xsl:variable name="dqinfoRedCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
			<xsl:variable name="dqinfoYellowCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
			<xsl:variable name="dqinfoGreenCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
			<span style="display:none;" id="dqCount"> <xsl:value-of select="$dqinfoRedCount+$dqinfoRedCount+$dqinfoGreenCount" /></span>		
			<div class="row noMarginRow">
				<div class="col-12">
					<div class="row loanSection" style="border-radius:4px;padding-left:20px;padding-bottom:15px;margin-bottom:20px;">
						<div class="col-12">
							<div class="row">
								<h1 class="h3-grey" style="font-size:18px;margin:0px;">Data Quality</h1>
							</div>
						</div>
					</div>
					
					<xsl:for-each select="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'DataQuality']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message">
						<xsl:variable name="i" select="position()" />
						<xsl:variable name="sectionName" select="lclaln:Section/lclaln:SectionName" />
						
						<div id="sec_{$i}">
							<xsl:if test="lclaln:Section/lclaln:SectionName != following::*[1]/lclaln:Section/lclaln:SectionName">
								<xsl:variable name="j" select="position()" />
								<xsl:variable name="sectionDetail" select="lclaln:Section/lclaln:SectionName" />
								<xsl:variable name="sectionValue" select="lclaln:Section/lclaln:SectionValue" />
								<xsl:variable name="dqinfoGreenNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Green']" />
								<xsl:variable name="dqinfoRedNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Red']" />
								<xsl:variable name="dqinfoYellowNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Yellow']" />
								<div id="dqRowDD_{$i}" class="row section-sub-header loanSection dq-section-spacing avoid-break" style="border-radius:4px; margin-bottom:20px;">
									<div class="col-10 noPadColumn">
										<div style="color:#006693; font-size:16px;"> Section <xsl:value-of select="$sectionValue" /> - <xsl:value-of select="$sectionDetail" /> </div>
									</div>
								</div>
								
								<xsl:if test="count($dqinfoGreenNodes)&gt;0">
									<xsl:call-template name="formatDQMessages">
										<xsl:with-param name="message" select="$dqinfoGreenNodes" />
										<xsl:with-param name="color" select="'GREEN'" />
									</xsl:call-template>
								</xsl:if>
								
								<xsl:if test="count($dqinfoRedNodes)&gt;0">
									<xsl:call-template name="formatDQMessages">
										<xsl:with-param name="message" select="$dqinfoRedNodes" />
										<xsl:with-param name="color" select="'RED'" />
									</xsl:call-template>
								</xsl:if>
								
								<xsl:if test="count($dqinfoYellowNodes)&gt;0">
									<xsl:call-template name="formatDQMessages">
										<xsl:with-param name="message" select="$dqinfoYellowNodes" />
										<xsl:with-param name="color" select="'YELLOW'" />
									</xsl:call-template>	
								</xsl:if>
								
							</xsl:if>
							
							<xsl:if test="lclaln:Section/lclaln:SectionValue = 'GSE' and preceding-sibling::*[1]/lclaln:Section/lclaln:SectionValue !='GSE'">
								<xsl:variable name="j" select="position()" />
								<xsl:variable name="sectionDetail" select="lclaln:Section/lclaln:SectionName" />
								<xsl:variable name="sectionValue" select="lclaln:Section/lclaln:SectionValue" />
								<xsl:variable name="dqinfoGreenNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Green']" />
								<xsl:variable name="dqinfoRedNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Red']" />
								<xsl:variable name="dqinfoYellowNodes" select="../lclaln:Message[lclaln:Section/lclaln:SectionName = $sectionName and lclaln:DecisionResult = 'Yellow']" />
									
								<div id="dqRowDD_{$i}" class="row section-sub-header loanSection dq-section-spacing avoid-break" style="border-radius:4px;">
									<div class="col-12 noPadColumn">
										<div style="color:#006693;font-size:16px;"> <xsl:value-of select="$sectionValue" /> - <xsl:value-of select="$sectionDetail" /></div>
									</div>
								</div>
								
								<xsl:if test="count($dqinfoGreenNodes)&gt;0">
									<xsl:call-template name="formatDQMessages">
										<xsl:with-param name="message" select="$dqinfoGreenNodes" />
										<xsl:with-param name="color" select="'GREEN'" />
									</xsl:call-template>	
								</xsl:if>
								
								<xsl:if test="count($dqinfoRedNodes)&gt;0">
									<xsl:call-template name="formatDQMessages">
										<xsl:with-param name="message" select="$dqinfoRedNodes" />
										<xsl:with-param name="color" select="'RED'" />
									</xsl:call-template>
								</xsl:if>
								
								<xsl:if test="count($dqinfoYellowNodes)&gt;0">
									<xsl:call-template name="formatDQMessages">
										<xsl:with-param name="message" select="$dqinfoYellowNodes" />
										<xsl:with-param name="color" select="'YELLOW'" />
									</xsl:call-template>
								</xsl:if>	
								
							</xsl:if>
						</div>
					</xsl:for-each> 
				</div>
			</div>
					
			<div class="row noMarginRow">
				<div class="col-12">
					<div class="row active noMarginRow">
						<xsl:variable name="eginfoYellowCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Yellow'])" />
						<xsl:variable name="eginfoRedCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red'])" />
						<xsl:variable name="eginfoGreenCount" select="count($evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Green'])" />
						<div class="row noMarginRow" id="sec_giTable">
							<div class="row loanSection" id="giRowDD" style="border-radius:4px;padding-left:20px;padding-bottom:15px;margin-bottom:20px;">
								<div class="col-12">
									<div class="row">
										<h1 class="h3-grey" style="font-size:18px; margin:0px;">Eligibility</h1>
									</div>
								</div>
							</div>
							
							<div class="row" id="giRow">
								<div class="row giEligTable">
									<div class="col-12">
										<xsl:variable name="egGreenNodes" select="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Green']" />
										<xsl:variable name="egRedNodes" select="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Red']" />
										<xsl:variable name="egYellowNodes" select="$evaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType = 'Eligibility']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult = 'Yellow']" />
										
										<xsl:if test="count($egGreenNodes)&gt;0">
											<xsl:call-template name="formatGIAndEGMessages">
												<xsl:with-param name="message" select="$egGreenNodes" />
												<xsl:with-param name="color" select="'GREEN'" />
											</xsl:call-template>
										</xsl:if>
										
										<xsl:if test="count($egRedNodes)&gt;0">
											<xsl:call-template name="formatGIAndEGMessages">
												<xsl:with-param name="message" select="$egRedNodes" />
												<xsl:with-param name="color" select="'RED'" />
											</xsl:call-template>
										</xsl:if>
										
										<xsl:if test="count($egYellowNodes)&gt;0">
											<xsl:call-template name="formatGIAndEGMessages">
												<xsl:with-param name="message" select="$egYellowNodes" />
												<xsl:with-param name="color" select="'YELLOW'" />
											</xsl:call-template>
										</xsl:if>
											
									</div>
								</div>
							</div>
										
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</xsl:template>

<xsl:template name="formatGIAndEGMessages">
	<xsl:param name="message" />
	<xsl:param name="color" />
	
	<div class="row avoid-break">
		<div class="col-12">
			<div class="row results-div">
				<div class="col-12">
					<div class="col-1 noLeftPadding">
						<div class="h5-grey">RESULTS</div>
					</div>
					<div class="col-1 noPadColumn">
						<div class="h5-grey">MESSAGE CODE</div>
					</div>
					<div class="col-9">
						<div class="h5-grey">MESSAGES</div>
					</div>
					<div class="col-1 msgIconPadding">
						<div class="h5-grey" />
					</div>
				</div>
			</div>
			<xsl:for-each select="$message">
				<xsl:variable name="mNum" select="position()" />
				<div class="row results-div">
					<div class="col-12">
						<div class="col-1 noLeftPadding">
							<xsl:if test="$mNum=1">
								<xsl:choose>
									<xsl:when test="$color = 'RED'">
										<span class="lead-lg-regular red-color"><xsl:value-of select="'RED'" /></span>
									</xsl:when>
									<xsl:when test="$color = 'YELLOW'">
										<span class="lead-lg-regular yellow-color"><xsl:value-of select="'YELLOW'" /></span>
									</xsl:when>
									<xsl:when test="$color = 'GREEN'">
										<span class="lead-lg-regular green-color"><xsl:value-of select="'GREEN'" /></span>
									</xsl:when>
								</xsl:choose>
							</xsl:if>
						</div>
						<div class="col-1 lead-lg-regular noPadColumn"><xsl:value-of select="lclaln:MessageCode" /></div>
						<div class="col-9">
							<div class="lead-lg-regular"><xsl:value-of select="lclaln:MessageText" /></div>
						</div>
						<div class="col-1 msgIconPadding">
							<xsl:if test="$mNum=1">
								<span>
									<xsl:choose>
										<xsl:when test="$color = 'RED'">
											<i class="ic-critical red-color results-icon-spacing" />
										</xsl:when>
										<xsl:when test="$color = 'YELLOW'">
											<i class="ic-error yellow-color results-icon-spacing" />
										</xsl:when>
										<xsl:when test="$color = 'GREEN'">
											<i class="ic-pass green-color results-icon-spacing" />
										</xsl:when>
									</xsl:choose>
								</span>
							</xsl:if>
						</div>
					</div>
				</div>
			</xsl:for-each>
		</div>
	</div>
	<hr class="results-hr" />				
</xsl:template>

<xsl:template name="formatDQMessages">
	<xsl:param name="message" />
	<xsl:param name="color" />
	
	<div class="row avoid-break">
		<div class="col-12 noRightPadding">
			<div class="row dq-results-div">
				<div class="col-12">
					<div class="col-1 noLeftPadding">
						<div class="h5-grey">RESULTS</div>
					</div>
					<div class="col-1 noPadColumn">
						<div class="h5-grey">MESSAGE CODE</div>
					</div>
					<div class="col-9">
						<div class="h5-grey">MESSAGES</div>
					</div>
					<div class="col-1">
						<div class="h5-grey" />
					</div>
				</div>
			</div>
			<xsl:for-each select="$message">
			<xsl:variable name="mNum" select="position()" />
				<div class="row dq-results-div">
					<div class="col-12">
						<div class="col-1 noLeftPadding">
							<xsl:if test="$mNum = 1">
								<xsl:choose>
									<xsl:when test="$color = 'RED'">
										<span class="lead-lg-regular red-color"><xsl:value-of select="'RED'" /></span>
									</xsl:when>
									<xsl:when test="$color = 'YELLOW'">
										<span class="lead-lg-regular yellow-color"><xsl:value-of select="'YELLOW'" /></span>
									</xsl:when>
									<xsl:when test="$color = 'GREEN'">
										<span class="lead-lg-regular green-color"><xsl:value-of select="'GREEN'" /></span>
									</xsl:when>
								</xsl:choose>
							</xsl:if>
						</div>
						<div class="col-1 lead-lg-regular noPadColumn"><xsl:value-of select="lclaln:MessageCode" /></div>
						<div class="col-9">
							<div class="lead-lg-regular"><xsl:value-of select="lclaln:MessageText" /></div>
						</div>
						<div class="col-1">
							<xsl:if test="$mNum = 1">
								<span>
									<xsl:choose>
										<xsl:when test="$color = 'RED'">
											<i class="ic-critical red-color dq-results-icon-spacing" />
										</xsl:when>
										<xsl:when test="$color = 'YELLOW'">
											<i class="ic-error yellow-color dq-results-icon-spacing" />
										</xsl:when>
										<xsl:when test="$color = 'GREEN'">
											<i class="ic-pass green-color dq-results-icon-spacing" />
										</xsl:when>
									</xsl:choose>
								</span>
							</xsl:if>
						</div>
					</div>
				</div>
			</xsl:for-each>	
		</div>
	</div>
	<hr class="dq-results-hr" />
</xsl:template>

</xsl:stylesheet>