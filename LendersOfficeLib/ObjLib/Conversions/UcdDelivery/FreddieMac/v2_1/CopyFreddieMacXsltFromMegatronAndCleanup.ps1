# See parent directory's readme; this is an adaptation of the .bat/.xsl used there
$ScriptDirectory = Split-Path -Parent $MyInvocation.MyCommand.Definition

$SourceDir = '\\megatron\LendersOffice\Integration\Freddie Mac\LoanClosingAdvisor\LCLA_S2S_Spec_v2.1\StyleSheets (non-JS)'
$resourcesCopyDir = [System.IO.Path]::Combine($ScriptDirectory, 'resources')
$XslFile = [System.IO.Path]::Combine($ScriptDirectory, 'PrepareFreddieXslt.xsl')
$resourcesFinalDir = [System.IO.Path]::Combine($ScriptDirectory, '..\..\..\..\..\..\PML\pml_shared\FreddieMacUcd\v2.1\resources') # 6 directories up to C:\LendOSoln

robocopy "$SourceDir\resources" "$resourcesCopyDir" /MIR
Copy-Item -Path "$SourceDir\*.xsl" -Destination $ScriptDirectory

# C:\LendingQB\XslExport\Tools\ExcelToXml.exe is just for the XslTransform.exe input.xml style.xsl syntax
Get-ChildItem -Path "$SourceDir\*.xsl" | ForEach-Object {
  $XslFileBeingTransformed = [System.IO.Path]::Combine($ScriptDirectory, "$($_.Name)")
  C:\LendingQB\XslExport\Tools\ExcelToXml.exe "$XslFileBeingTransformed" "$XslFile"
  Move-Item -Path "$XslFileBeingTransformed.xml" -Destination "$XslFileBeingTransformed" -Force
}

Get-ChildItem -Path $resourcesCopyDir -Recurse -Force -Include 'Thumbs.db' | Remove-Item -Force

# 5 directories up to C:\LendOSoln
robocopy "$resourcesCopyDir" "$resourcesFinalDir" /MIR
Remove-Item $resourcesCopyDir -Recurse -Force
