﻿namespace LendersOffice.ObjLib.Conversions.UcdDelivery.FreddieMac
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml;
    using Integration.UcdDelivery;
    using Integration.UcdDelivery.FreddieMac;
    using LendersOffice.Conversions.Templates;

    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.DocumentationRules", "*", Justification = "Temporary while developing.")]
    public class FreddieMacUcdDeliveryResponseProvider : AbstractResponseProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FannieMaeUcdDeliveryResponseProvider"/> class.
        /// </summary>
        /// <param name="responsePayload">The response payload.</param>
        public FreddieMacUcdDeliveryResponseProvider(string responsePayload)
            : base(responsePayload, null)
        {
        }

        /// <summary>
        /// Logs the response.
        /// </summary>
        public override void LogResponse()
        {
            UcdDeliveryUtilities.LogPayload(this.ResponsePayload, UcdDeliveryUtilities.FreddieMacResponseLogHeader);
        }

        /// <summary>
        /// Parses the Fannie Mae UCD findings XML.
        /// </summary>
        public override void ParseResponse()
        {
            XmlDocument findingsXml = new XmlDocument();
            findingsXml.LoadXml(this.ResponsePayload);
            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(findingsXml.NameTable);
            namespaceManager.AddNamespace("sch", FreddieMacUcdDelivery.FreddieMacSchNamespace);
            namespaceManager.AddNamespace("lclaln", FreddieMacUcdDelivery.FreddieMacLclalnNamespace);
            var errorCode = findingsXml.SelectSingleNode("//sch:LCLALoanEvaluationServiceFault/sch:EvaluationResults/lclaln:ErrorDetails/lclaln:ErrorDetail/lclaln:ErrorCode", namespaceManager)?.InnerText;
            if (!string.IsNullOrEmpty(errorCode))
            {
                this.Errors.Add(findingsXml.SelectSingleNode("//sch:LCLALoanEvaluationServiceFault/sch:EvaluationResults/lclaln:ErrorDetail/lclaln:ErrorMessage", namespaceManager)?.InnerText);
                return;
            }

            var transactionId = findingsXml.SelectSingleNode("//sch:LCLALoanEvaluationServiceResponse/sch:EvaluationResults/@TransactionID", namespaceManager)?.Value;
            var batchId = findingsXml.SelectSingleNode("//sch:LCLALoanEvaluationServiceResponse/sch:EvaluationResults/@BatchID", namespaceManager)?.Value;

            FreddieMacUcdDeliveryStatus status;
            var calculatedClosingDisclosureEmbeddedPDFIndicator = findingsXml.SelectSingleNode("//sch:LCLALoanEvaluationServiceResponse/sch:EvaluationResults/lclaln:LoanResults/lclaln:LoanSummary/lclaln:CALCULATED_VALUES/lclaln:CalculatedClosingDisclosureEmbeddedPDFIndicator", namespaceManager)?.InnerText;
            if (!string.IsNullOrEmpty(calculatedClosingDisclosureEmbeddedPDFIndicator) &&
                (calculatedClosingDisclosureEmbeddedPDFIndicator == "true" || calculatedClosingDisclosureEmbeddedPDFIndicator == "1"))
            {
                status = FreddieMacUcdDeliveryStatus.Satisfied;
            }
            else
            {
                var redCount = findingsXml.SelectNodes("//sch:LCLALoanEvaluationServiceResponse/sch:EvaluationResults/lclaln:LoanResults/lclaln:EvaluatedServices/lclaln:EvaluatedService[lclaln:EvaluatedServiceType='GeneralInformation']/lclaln:Results/lclaln:Decision/lclaln:Messages/lclaln:Message[lclaln:DecisionResult='Red']", namespaceManager);
                if (redCount.Count >= 1)
                {
                    status = FreddieMacUcdDeliveryStatus.NotSatisfied;
                }
                else
                {
                    status = FreddieMacUcdDeliveryStatus.SatisfiedWithoutClosingDisclosure;
                }
            }

            this.ResponseData = new FreddieMacUcdDeliveryResponseData(
                UcdDeliveryResultStatus.Delivered,
                status,
                transactionId,
                deliveryResults: new UcdDeliveryResults(this.ResponsePayload, batchId));
        }
    }
}
