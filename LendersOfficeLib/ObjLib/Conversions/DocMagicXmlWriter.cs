﻿namespace LendersOffice.Conversions
{
    using System.Xml;

    public class DocMagicXmlWriter : XmlWrappingWriter
    {
        public DocMagicXmlWriter(XmlWriter baseWriter) : base(baseWriter) { }

        public override void WriteString(string text)
        {
            base.WriteString(text);
        }
    }
}
