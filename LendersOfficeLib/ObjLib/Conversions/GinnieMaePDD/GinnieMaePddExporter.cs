﻿// <copyright file="GinnieMaePddExporter.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Scott Gettinger
//  Date:   November 06, 2015
// </summary>
namespace LendersOffice.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Conversions;
    using LendersOffice.ObjLib.MortgagePool;
    using Mismo3.Version3;
	using Mismo3Specification;
	using Mismo3Specification.Version3Schema;
    using static Mismo3.Version3.MismoTypeConverter;
    using static Mismo3.Version3.Mismo33DataMapper;

    /// <summary>
    /// Serializes a Ginnie Mae Pool Delivery Dataset submission to an XML string which can be delivered to Ginnie Mae.
    /// </summary>
    public class GinnieMaePddExporter : IPoolExporter
    {
        /// <summary>
        /// Version number for the GNMA format.
        /// </summary>
        private const string AboutVersionIdentifier = "GNMA 1.3";

        /// <summary>
        /// MISMO version number.
        /// </summary>
        private const string MISMOReferenceModelIdentifier = "3.3.0";

        /// <summary>
        /// Format target of this export.
        /// </summary>
        private const FormatTarget PDDTarget = FormatTarget.MismoClosing;

        /// <summary>
        /// A list of acceptable pool issue type codes for GNMA pools. Used in pool validation.
        /// </summary>
        private static List<string> issueTypeCodes = new List<string>()
        {
            "X", "C", "M"
        };

        /// <summary>
        /// Format converter for the export.
        /// </summary>
        private static LosConvert losConvert;

        /// <summary>
        /// A list of acceptable pool suffixes for GNMA pools. Used in pool validation.
        /// </summary>
        private static List<string> poolSuffixes = new List<string>()
        {
            "AF", "AQ", "AR", "AS", "AT", "AX", "BD", "FB", "FL", "FS", "FT", "GA", "GD", "GP", "GT", "JM", "MH", "QL", "RL", "SF", "SL", "SN", "TL", "XL"
        };

        /// <summary>
        /// A regular expression describing the format of a routing number.
        /// </summary>
        private static Regex routingNumberRegex = new Regex(@"\d{9}");

        /// <summary>
        /// A collection of sequence number counters for some MISMO containers.
        /// </summary>
        private Mismo33Counters counters;

        /// <summary>
        /// An internal list of error messages encountered on running the audit.
        /// </summary>
        private HashSet<string> errors = null;

        /// <summary>
        /// A list of loan IDs from the loan pool.
        /// </summary>
        private List<Guid> loanIds;

        /// <summary>
        /// The mortgage pool for this pool exporter.
        /// </summary>
        private MortgagePool mortgagePool;

        /// <summary>
        /// An internal list of warnings encountered on running the audit.
        /// </summary>
        private HashSet<string> warnings = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="GinnieMaePddExporter" /> class.
        /// </summary>
        /// <param name="poolID">The loan pool ID to associate with the exporter.</param>
        public GinnieMaePddExporter(long poolID)
        {
            this.mortgagePool = new MortgagePool(poolID);
            this.mortgagePool.SetFormatTarget(PDDTarget);

            losConvert = new LosConvert(PDDTarget);
            this.counters = new Mismo33Counters();

            this.loanIds = this.mortgagePool.FetchLoansInPool();

            this.RunAudit();
        }

        /// <summary>
        /// Gets a list of errors which prevent the pool from being exported. From IPoolExporter.
        /// </summary>
        /// <value>A HashSet of error strings that occurred during export.</value>
        public HashSet<string> Errors
        {
            get
            {
                return this.errors;
            }
        }

        /// <summary>
        /// Gets the filename for the output file to be assigned. From IPoolExporter.
        /// </summary>
        /// <value>The name of the file for output from the exporter.</value>
        public string OutputFileName
        {
            get
            {
                DateTime date = DateTime.Now;
                return date.ToString("yyyy-MM-dd") + "_" + this.mortgagePool.PoolNumberByAgency;
            }
        }

        /// <summary>
        /// Gets a list of warnings which will not prevent the pool from being exported but should be displayed to the user. From IPoolExporter.
        /// </summary>
        /// <value>A HashSet of warning strings that occurred during export.</value>
        public HashSet<string> Warnings
        {
            get
            {
                return this.warnings;
            }
        }

        /// <summary>
        /// Runs an audit on this exporter's mortgage pool and loans to determine if they are suitable for PDD export.
        /// Results are stored in <see cref="Errors"/> and <see cref="Warnings"/>.
        /// </summary>
        public void RunAudit()
        {
            // Pool validation
            IEnumerable<string> auditResults = new List<string>();

            this.ValidatePool(this.mortgagePool);

            // Loan validations
            foreach (Guid loanId in this.loanIds)
            {
                this.VerifyLoan(loanId);
            }
        }

        /// <summary>
        /// Attempts an export to byte array of this exporter's root "MESSAGE" element. From IPoolExporter.
        /// </summary>
        /// <param name="output">The transmittable byte array of serialized xml.</param>
        /// <returns>A boolean value indicating whether the export attempt was successful.</returns>
        public bool TryExport(out byte[] output)
        {
            output = null;

            this.RunAudit();

            if (this.errors.Count > 0)
            {
                string allErrors = this.errors.Aggregate<string>((x, y) => x + Environment.NewLine + y);
                Tools.LogInfo(string.Format("PDD Exporter: Encountered the following errors on export: {0}{1}", Environment.NewLine, allErrors));
                output = Encoding.UTF8.GetBytes(allErrors);
                return false;
            }

            if (this.warnings.Count > 0)
            {
                string allWarnings = this.warnings.Aggregate<string>((x, y) => x + Environment.NewLine + y);
                Tools.LogInfo(string.Format("PDD Exporter: Encountered the following warnings on export: {0}{1}", Environment.NewLine, allWarnings));
            }

            string exportResult;
            try
            {
                exportResult = SerializationHelper.XmlSerialize(this.CreateMessage(), true, "www.ginniemae.gov");
            }
            catch (CBaseException e)
            {
                Tools.LogError(string.Format("PDD Exporter: Unexpected exception thrown on export: {0}{1}", Environment.NewLine, e.Message));
                this.errors.Add(e.UserMessage + " Reference Number: " + e.ErrorReferenceNumber);
                return false;
            }

            Tools.LogInfo(string.Format("PDD Exporter: Export was successful: {0}{1}", Environment.NewLine, exportResult));
            output = Encoding.UTF8.GetBytes(exportResult);
            return true;
        }

        /// <summary>
        /// Runs an audit on a loan for PDD export. Compiles a list of errors and warnings encountered in the verification and adds them to <see cref="Errors"/> and <see cref="Warnings"/>.
        /// </summary>
        /// <param name="loanId">The ID of the loan to verify.</param>
        public void VerifyLoan(Guid loanId)
        {
            CPageData loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(GinnieMaePddExporter));
            loanData.InitLoad();
            loanData.ByPassFieldSecurityCheck = true;

            PddExportLoanErrorList pddExportErrors = new PddExportLoanErrorList(loanData.sLId, loanData.sLNm, loanData.GetAppData(0).aAppId);
            PddExportLoanErrorList pddExportWarnings = new PddExportLoanErrorList(loanData.sLId, loanData.sLNm, loanData.GetAppData(0).aAppId);

            var applicationData = loanData.GetAppData(0);

            if (string.IsNullOrEmpty(loanData.sLenNum))
            {
                pddExportWarnings.Add(new PddExportErrorItem("sLenNum", ErrorMessages.PDD.EmptyLenderID, "Shipping:: GSE Delivery"));
            }

            if (string.IsNullOrEmpty(loanData.sDocumentNoteD_rep))
            {
                pddExportWarnings.Add(new PddExportErrorItem("sDocumentNoteD", ErrorMessages.PDD.EmptyNoteDate, "Status:: General"));
            }

            if (loanData.sHouseVal == 0)
            {
                pddExportWarnings.Add(new PddExportErrorItem("sHouseVal", ErrorMessages.PDD.EmptyHouseValue, "Loan Info:: This Loan Info"));
            }

            if (string.IsNullOrEmpty(applicationData.aBDecPastOwnership))
            {
                AddError(pddExportWarnings, "aBDecPastOwnership", "Always", "1003");
            }

            if (string.IsNullOrEmpty(loanData.sSpAddr))
            {
                AddError(pddExportWarnings, "sSpAddr", "Always", "1003");
            }

            if (loanData.sSpAddr.Length > 40)
            {
                pddExportWarnings.Add(new PddExportErrorItem("sSpAddr", string.Format(ErrorMessages.PDD.FieldTooLong, "Subject Property Address", 40, loanData.sSpAddr.Length), "Loan Application:: Subject Property Description"));
            }

            if (string.IsNullOrEmpty(loanData.sSpCity))
            {
                AddError(pddExportWarnings, "sSpCity", "Always", "1003");
            }

            if (loanData.sSpCity.Length > 21)
            {
                pddExportWarnings.Add(new PddExportErrorItem("sSpCity", string.Format(ErrorMessages.PDD.FieldTooLong, "Subject Property City", 21, loanData.sSpCity.Length), "Loan Application:: Subject Property Description"));
            }

            if (string.IsNullOrEmpty(loanData.sSpZip))
            {
                AddError(pddExportWarnings, "sSpZip", "Always", "1003");
            }

            if (loanData.sSpZip.Length > 9)
            {
                pddExportWarnings.Add(new PddExportErrorItem("sSpZip", string.Format(ErrorMessages.PDD.FieldTooLong, "Subject Property Zip", 9, loanData.sSpCity.Length), "Loan Application:: Subject Property Description"));
            }

            if (string.IsNullOrEmpty(loanData.sSpState))
            {
                AddError(pddExportWarnings, "sSpState", "Always", "1003");
            }

            if (loanData.sGseSpT == E_sGseSpT.LeaveBlank)
            {
                AddError(pddExportWarnings, "sGseSpT", "Always", "1003");
            }

            if (loanData.sUnitsNum == 0)
            {
                AddError(pddExportWarnings, "sUnitsNum", "Always", "1003");
            }

            if (loanData.sTerm == 0)
            {
                AddError(pddExportWarnings, "sTerm", "Always", "1003");
            }

            if (loanData.sLTotI == 0)
            {
                AddError(pddExportWarnings, "sLTotI", "Always", "1003");
            }

            if (loanData.sFinalLAmt == 0)
            {
                AddError(pddExportWarnings, "sFinalLAmt", "Always", "1003");
            }

            if (string.IsNullOrEmpty(applicationData.aBSsn))
            {
                AddError(pddExportWarnings, "aBSsn", "Always", "1003");
            }

            if (loanData.sFinMethT == E_sFinMethT.ARM && string.IsNullOrEmpty(loanData.sArmIndexLeadDays_rep))
            {
                AddError(pddExportWarnings, "sArmIndexLeadDays", "Amortization Type (sFinMethT) = Adjustable Rate");
            }

            if ((loanData.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium ||
                loanData.sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide ||
                loanData.sGseSpT == E_sGseSpT.ManufacturedHousing ||
                loanData.sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide) &&
                string.IsNullOrEmpty(loanData.sAppSubmittedD_rep))
            {
                AddError(pddExportWarnings, "sAppSubmittedD", "ConstructionMethodType = Manufactured");
            }

            if (string.IsNullOrEmpty(loanData.sSchedDueD1_rep) || !loanData.sSchedDueD1.IsValid)
            {
                pddExportErrors.Add(new PddExportErrorItem("sSchedDueD1", ErrorMessages.PDD.Empty1PaymentDueDate, "Truth In Lending OR Funding:: Settlement Charges"));
            }

            if (string.IsNullOrEmpty(loanData.sQualBottomR_rep))
            {
                AddError(pddExportWarnings, "sQualBottomR", "Always");
            }

            if (string.IsNullOrEmpty(loanData.sNoteIR_rep))
            {
                AddError(pddExportWarnings, "sNoteIR", "Always");
            }

            if (string.IsNullOrEmpty(loanData.sLenderCaseNum))
            {
                AddError(pddExportWarnings, "sLenderCaseNum", "Always");
            }

            if (string.IsNullOrEmpty(loanData.sServicingLastPaymentMadeDueD_rep))
            {
                AddError(pddExportWarnings, "sServicingLastPaymentMadeDueD", "Pooling Date(MM/YYYY) > First Payment Date(MM/YYYY)");
            }

            if (string.IsNullOrEmpty(loanData.sServicingUnpaidPrincipalBalance_rep))
            {
                AddError(pddExportWarnings, "sServicingUnpaidPrincipalBalance", "Always");
            }

            if (string.IsNullOrEmpty(loanData.sRAdjCapR_rep) || loanData.sRAdjCapR < 0)
            {
                AddError(pddExportErrors, "sRAdjCapR", "Always", "Loan Application:: Loan Terms");
            }

            if (string.IsNullOrEmpty(loanData.sRAdjLifeCapR_rep) || loanData.sRAdjLifeCapR < 0)
            {
                AddError(pddExportErrors, "sRAdjLifeCapR", "Always", "Loan Application:: Loan Terms");
            }

            if (string.IsNullOrEmpty(applicationData.aBFirstNm))
            {
                AddError(pddExportWarnings, "aBFirstNm", "Always", "Forms:: 1003 Page 1");
            }

            if (string.IsNullOrEmpty(applicationData.aBLastNm))
            {
                AddError(pddExportWarnings, "aBLastNm", "Always", "Forms:: 1003 Page 1");
            }

            try
            {
                if (loanData.sCreditScoreType2Soft == 0)
                {
                    AddError(pddExportWarnings, "sCreditScoreType2Soft", "Always", "File:: Credit Scores");
                }
            }
            catch (CBaseException e)
            {
                pddExportErrors.Add(new PddExportErrorItem("sCreditScoreType2Soft", "An error occurred with borrower credit scores. (" + e.DeveloperMessage + ")", "File:: Credit Scores"));
                Tools.LogError(e);
            }

            try
            {
                GetMortgageProgramBaseValue(loanData.sLT);
            }
            catch (UnhandledEnumException)
            {
                pddExportErrors.Add(new PddExportErrorItem("sLT", "Invalid Loan Type for PDD export. Make sure the Loan Type is VA, FHA, or USDA rural.", "Forms:: 1003 Page 1"));
            }

            try
            {
                GetLoanOriginatorBaseValue(loanData.sBranchChannelT);
            }
            catch (UnhandledEnumException)
            {
                pddExportErrors.Add(new PddExportErrorItem("sBranchChannelT", "Invalid or blank Third Party Origination Type for PDD export.", "Status:: General", "MESSAGE/DEAL_SETS/DEAL_SET/DEALS/DEAL/PARTIES/PARTY/ROLES/ROLE/LOAN_ORIGINATOR/LoanOriginatorType"));
            }

            IEnumerable<string> errorStrings = pddExportErrors.ErrorList.Select(errorItem => this.GenerateFriendlyErrorMessage(true, "Error", pddExportErrors, errorItem));
            if (this.errors != null)
            {
                this.errors.UnionWith(errorStrings);
            }
            else
            {
                this.errors = new HashSet<string>(errorStrings);
            }

            IEnumerable<string> warningStrings = pddExportWarnings.ErrorList.Select(warningItem => this.GenerateFriendlyErrorMessage(true, "Warning", pddExportWarnings, warningItem));
            if (this.warnings != null)
            {
                this.warnings.UnionWith(warningStrings);
            }
            else
            {
                this.warnings = new HashSet<string>(warningStrings);
            }
        }

        /// <summary>
        /// Adds an error to a list of a loan's errors. Used when a field is not present but should be, based on certain conditions.
        /// </summary>
        /// <param name="errors">The list of errors.</param>
        /// <param name="fieldName">The field associated with the error.</param>
        /// <param name="requiredWhen">Conditions when the field is required to be present.</param>
        /// <param name="pageInfo">Information about the page location where the field can be found.</param>
        private static void AddError(PddExportLoanErrorList errors, string fieldName, string requiredWhen, string pageInfo = "")
        {
            CustomFormField.FormField field = LendersOffice.CustomFormField.FormField.RetrieveById(fieldName);
            var friendlyName = (field == null) ? "(" + fieldName + ")" : "\"" + field.FriendlyName + "\"" + "(" + fieldName + ")";
            if (requiredWhen.Equals("Always"))
            {
                errors.Add(new PddExportErrorItem(fieldName, friendlyName + " is required for all loan files.", pageInfo));
            }
            else
            {
                errors.Add(new PddExportErrorItem(fieldName, friendlyName + " is required when " + requiredWhen + ".", pageInfo));
            }
        }

        /// <summary>
        /// Calculates the loan-to-value ratio for the loan and returns as a string.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose. For certain purposes, LTV does not apply, and we return 0.00.</param>
        /// <param name="houseValue">The sale price of the house.</param>
        /// <param name="finalLoanAmount">The final amount of the mortgage loan.</param>
        /// <returns>The LTV as a string.</returns>
        private static string CalculateLtv(E_sLPurposeT loanPurpose, decimal houseValue, decimal finalLoanAmount)
        {
            if (loanPurpose == E_sLPurposeT.FhaStreamlinedRefinance || loanPurpose == E_sLPurposeT.VaIrrrl || houseValue == 0)
            {
                return "0.00";
            }
            else
            {
                decimal percent = finalLoanAmount / houseValue * 100;
                int result = decimal.ToInt32(percent * 100);
                if (result % 100 > 0)
                {
                    result += 100;
                }

                result /= 100;
                return GetPDDConverter().ToDecimalString(result, FormatDirection.ToRep);
            }
        }

        /// <summary>
        /// Creates an <see cref="ACH"/> element with bank account information for recurring or automatic payments.
        /// </summary>
        /// <param name="accountNum">The account number for this account.</param>
        /// <param name="routingNum">The routing number for this account.</param>
        /// <param name="accountPurpose">The purpose for this account (P&amp;I, Settlement, etc).</param>
        /// <param name="abaName">The American Banking Association abbreviated name of the depository institution.</param>
        /// <param name="thirdPartyName">The subaccount name for the third-party receiving payments.</param>
        /// <returns>An <see cref="ACH"/> element for serialization.</returns>
        private static ACH CreateACH(string accountNum, string routingNum, ACHBankAccountPurposeBase accountPurpose, string abaName = null, string thirdPartyName = null)
        {
            return new ACH()
            {
                ACHBankAccountIdentifier = ToMismoIdentifier(accountNum.Length > 20 ? accountNum.Substring(0, 20) : accountNum.PadLeft(20, '0'), displaySensitive: false),
                ACH_ABARoutingAndTransitIdentifier = ToMismoIdentifier(routingNum, displaySensitive: false),
                ACHBankAccountPurposeType = ToMismoEnum(accountPurpose, displaySensitive: false),
                ACHInstitutionTelegraphicAbbreviationName = ToMismoString(abaName, displaySensitive: false),
                ACHReceiverSubaccountName = ToMismoString(thirdPartyName, displaySensitive: false)
            };
        }

        /// <summary>
        /// Sums up the current principal balance on each loan in the loan pool, to get the total for the whole pool.
        /// </summary>
        /// <param name="mortgagePool">The loan pool to sum balances for.</param>
        /// <returns>The total sum of principal balances of loans in the pool.</returns>
        private static decimal GetCurrentPrincipalBalanceAmount(MortgagePool mortgagePool)
        {
            decimal sumsServicingUnpaidPrincipalBalance = 0;
            List<Guid> loans = mortgagePool.FetchLoansInPool();
            foreach (Guid loanID in loans)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanID, typeof(GinnieMaePddExporter));
                dataLoan.InitLoad();
                dataLoan.SetFormatTarget(FormatTarget.MismoClosing);

                sumsServicingUnpaidPrincipalBalance += dataLoan.sServicingUnpaidPrincipalBalance;
            }

            return sumsServicingUnpaidPrincipalBalance;
        }

        /// <summary>
        /// Small utility function to get the LOSConvert object for PDD.
        /// </summary>
        /// <returns>The LosConvert object.</returns>
        private static LosConvert GetPDDConverter()
        {
            var losConvert = new LosConvert(PDDTarget);
            return losConvert;
        }

        /// <summary>
        /// Creates the ABOUT_VERSION container with information about the file.
        /// </summary>
        /// <returns>An <see cref="ABOUT_VERSION"/> XML container.</returns>
        private ABOUT_VERSION CreateAboutVersion()
        {
            return new ABOUT_VERSION()
            {
                AboutVersionIdentifier = ToMismoIdentifier(AboutVersionIdentifier, displaySensitive: false),
                CreatedDatetime = ToMismoDatetime(DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz"), false)
            };
        }

        /// <summary>
        /// Creates the ABOUT_VERSIONS container with a list of GNMA PDD versions for the file.
        /// </summary>
        /// <returns>An <see cref="ABOUT_VERSIONS"/> XML container.</returns>
        private ABOUT_VERSIONS CreateAboutVersions()
        {
            return new ABOUT_VERSIONS()
            {
                AboutVersion = new List<ABOUT_VERSION>
                {
                    this.CreateAboutVersion()
                }
            };
        }

        /// <summary>
        /// Creates the ACH container in the <see cref="DEAL_SET"/> parent element. Contains an extension with a list of <see cref="ACH"/> containers per GNMA's specifications.
        /// </summary>
        /// <returns>An ACH container with an EXTENSION which has a list of ACH containers for the mortgage pool.</returns>
        private ACH CreateACHDealSet()
        {
            List<ACH> ach = new List<ACH>();

            ach.Add(CreateACH(this.mortgagePool.PIAccountNum.Value, this.mortgagePool.PIRoutingNum.Value, ACHBankAccountPurposeBase.PrincipalAndInterest));
            ach.Add(CreateACH(this.mortgagePool.TIAccountNum.Value, this.mortgagePool.TIRoutingNum.Value, ACHBankAccountPurposeBase.TaxesAndInsurance));

            var achs = new ACHS()
            {
                ACH = ach
            };

            var other = new PDD_ACH_EXTENSION()
            {
                ACHS = achs
            };

            var extension = new ACH_EXTENSION()
            {
                Other = other
            };

            return new ACH()
            {
                Extension = extension
            };
        }

        /// <summary>
        /// Creates a DEALS container, containing the DEAL elements.
        /// </summary>
        /// <returns>A <see cref="DEALS"/> XML container.</returns>
        private DEALS CreateDeals()
        {
            var deals = new DEALS();

            int sequenceNumber = 1;
            foreach (Guid loanID in this.loanIds)
            {
                PddDealContainer dealContainer = new PddDealContainer(loanID, this.mortgagePool, this.counters);
                deals.Deal.Add(dealContainer.CreateDeal(sequenceNumber++));
            }

            return deals;
        }

        /// <summary>
        /// Creates a DEAL_SET container, which corresponds to the LQB loan pool.
        /// </summary>
        /// <returns>A <see cref="DEAL_SET"/> XML container.</returns>
        private DEAL_SET CreateDealSet()
        {
            return new DEAL_SET()
            {
                SequenceNumber = 0,
                Deals = this.CreateDeals(),
                Parties = this.CreateParties(),
                Pool = this.CreatePool(),
                ACH = this.CreateACHDealSet(),
                Extension = this.CreateDealSetExtension()
            };
        }

        /// <summary>
        /// Creates an extension holding extra information about the DEAL_SET container.
        /// </summary>
        /// <returns>A <see cref="DEAL_SET_EXTENSION"/> container.</returns>
        private DEAL_SET_EXTENSION CreateDealSetExtension()
        {
            var other = new PDD_DEAL_SET_EXTENSION()
            {
                Securities = this.CreateSecurities(),
                DocumentSpecificDataSets = this.CreateDocumentSpecificDataSets()
            };

            return new DEAL_SET_EXTENSION()
            {
                Other = other
            };
        }

        /// <summary>
        /// Creates a DEAL_SETS container, containing the DEAL_SET objects for the message. In our message, there is only one DEAL_SET, corresponding to the loan pool.
        /// </summary>
        /// <returns>A <see cref="DEAL_SETS"/> XML container.</returns>
        private DEAL_SETS CreateDealSets()
        {
            var dealSets = new DEAL_SETS();

            dealSets.DealSet.Add(this.CreateDealSet());

            return dealSets;
        }

        /// <summary>
        /// Creates an extension to the DOCUMENT_CERTIFICATION_DETAIL container with details about a document certification.
        /// </summary>
        /// <returns>A <see cref="DOCUMENT_CERTIFICATION_DETAIL"/> container.</returns>
        private DOCUMENT_CERTIFICATION_DETAIL CreateDocumentCertificationDetail()
        {
            var detail = new DOCUMENT_CERTIFICATION_DETAIL()
            {
                DocumentFormPublisherNumberIdentifier = ToMismoIdentifier("11711A", displaySensitive: false),
                DocumentSubmissionIndicator = ToMismoIndicator(this.mortgagePool.IsFormHUD11711ASentToDocumentCustodian)
            };

            if (this.mortgagePool.CertAndAgreementT == E_MortgagePoolCertAndAgreementT.LimitedSecurityAgreement)
            {
                detail.DocumentRequiredIndicator = ToMismoIndicator(true);
            }
            else if (this.mortgagePool.CertAndAgreementT == E_MortgagePoolCertAndAgreementT.NoSecurityAgreement)
            {
                detail.DocumentRequiredIndicator = ToMismoIndicator(false);
            }

            return detail;
        }

        /// <summary>
        /// Creates a DOCUMENT_SPECIFIC_DATA_SET container to be placed in the DEAL_SET's extension.
        /// </summary>
        /// <returns>A <see cref="DOCUMENT_SPECIFIC_DATA_SET"/> container.</returns>
        private DOCUMENT_SPECIFIC_DATA_SET CreateDocumentSpecificDataSet_DealSet()
        {
            return new DOCUMENT_SPECIFIC_DATA_SET()
            {
                Extension = this.CreateDocumentSpecificDataSetExtension()
            };
        }

        /// <summary>
        /// Creates an extension to the DOCUMENT_SPECIFIC_DATA_SET container with extra non-MISMO information.
        /// </summary>
        /// <returns>A <see cref="DOCUMENT_SPECIFIC_DATA_SET_EXTENSION"/> container. Serializes as "EXTENSION".</returns>
        private DOCUMENT_SPECIFIC_DATA_SET_EXTENSION CreateDocumentSpecificDataSetExtension()
        {
            // Build EXTENSION/OTHER/DOCUMENT_CERTIFICATIONS/DOCUMENT_CERTIFICATION/DOCUMENT_CERTIFICATION_DETAIL/ from the inside out.
            var detail = this.CreateDocumentCertificationDetail();

            var documentCertification = new DOCUMENT_CERTIFICATION()
            {
                DocumentCertificationDetail = detail
            };

            var other = new PDD_DOCUMENT_SPECIFIC_DATA_SET_EXTENSION()
            {
                DocumentCertifications = new DOCUMENT_CERTIFICATIONS(documentCertification)
            };

            return new DOCUMENT_SPECIFIC_DATA_SET_EXTENSION()
            {
                Other = other
            };
        }

        /// <summary>
        /// Creates a DOCUMENT_SPECIFIC_DATA_SETS container to contain a list of document specific data sets.
        /// </summary>
        /// <returns>A <see cref="DOCUMENT_SPECIFIC_DATA_SETS"/> container.</returns>
        private DOCUMENT_SPECIFIC_DATA_SETS CreateDocumentSpecificDataSets()
        {
            return new DOCUMENT_SPECIFIC_DATA_SETS()
            {
                DocumentSpecificDataSet = new List<DOCUMENT_SPECIFIC_DATA_SET>()
                {
                    this.CreateDocumentSpecificDataSet_DealSet()
                }
            };
        }

        /// <summary>
        /// Creates the top-level MESSAGE element which contains all of the information to be sent.
        /// </summary>
        /// <returns>A <see cref="MESSAGE"/> XML container.</returns>
        private MESSAGE CreateMessage()
        {
            return new MESSAGE()
            {
                AboutVersions = this.CreateAboutVersions(),
                MISMOReferenceModelIdentifier = MISMOReferenceModelIdentifier,
                MISMOLogicalDataDictionaryIdentifier = null,
                DealSets = this.CreateDealSets()
            };
        }

        /// <summary>
        /// Creates a PARTIES container with information about pool parties.
        /// </summary>
        /// <returns>A <see cref="PARTIES"/> XML container.</returns>
        private PARTIES CreateParties()
        {
            var parties = new PARTIES();

            parties.Party.Add(this.CreatePoolParty(this.mortgagePool.CustodianID, PartyRoleBase.DocumentCustodian));
            parties.Party.Add(this.CreatePoolParty(this.mortgagePool.IssuerID, PartyRoleBase.PoolIssuer));
            parties.Party.Add(this.CreatePoolParty(this.mortgagePool.ServicerID, PartyRoleBase.Servicer));
            parties.Party.Add(this.CreatePoolParty_Trust(this.mortgagePool.TaxID));

            return parties;
        }

        /// <summary>
        /// Creates a PDD-specific extension container with information about a pool.
        /// </summary>
        /// <returns>A <see cref="PDD_POOL_DETAIL_EXTENSION"/> container. Serializes as "OTHER".</returns>
        private PDD_POOL_DETAIL_EXTENSION CreatePddPoolDetailExtension()
        {
            return new PDD_POOL_DETAIL_EXTENSION()
            {
                PoolMaturityPeriodCount = ToMismoCount(this.mortgagePool.Term, losConvert, displaySensitive: false),
                PoolMaturityPeriodType = ToMismoEnum(PoolMaturityPeriodBase.Month, displaySensitive: false),
                GovernmentBondFinancingProgramName = ToMismoString(this.mortgagePool.GovernmentBondFinancingProgramName, displaySensitive: false)
            };
        }

        /// <summary>
        /// Creates a POOL container with information about a loan pool.
        /// </summary>
        /// <returns>A <see cref="POOL"/> XML container.</returns>
        private POOL CreatePool()
        {
            return new POOL()
            {
                PoolCertificate = this.CreatePoolCertificate(this.mortgagePool.InitialPmtD_rep),
                PoolDetail = this.CreatePoolDetail()
            };
        }

        /// <summary>
        /// Creates a POOL_CERTIFICATE container with information about the pool's certificate.
        /// </summary>
        /// <param name="certInitialPaymentDate">The initial payment date on the pool security.</param>
        /// <returns>A <see cref="POOL_CERTIFICATE"/> XML container.</returns>
        private POOL_CERTIFICATE CreatePoolCertificate(string certInitialPaymentDate)
        {
            return new POOL_CERTIFICATE()
            {
                PoolCertificateInitialPaymentDate = ToMismoDate(certInitialPaymentDate)
            };
        }

        /// <summary>
        /// Creates a POOL_DETAIL container with details about a loan pool.
        /// </summary>
        /// <returns>A <see cref="POOL_DETAIL"/> XML container.</returns>
        private POOL_DETAIL CreatePoolDetail()
        {
            var poolDetail = new POOL_DETAIL()
            {
                AmortizationType = ToAmortizationEnum(this.mortgagePool.AmortizationT, displaySensitive: false),
                PoolClassType = ToPoolClassEnum(this.mortgagePool.IssueTypeCode, displaySensitive: false),
                PoolConcurrentTransferIndicator = ToMismoIndicator(false),
                PoolCurrentLoanCount = ToMismoCount(this.mortgagePool.LoanCount, losConvert, displaySensitive: false),
                PoolCurrentPrincipalBalanceAmount = ToMismoAmount(this.mortgagePool.ToRep(GetCurrentPrincipalBalanceAmount(this.mortgagePool)), displaySensitive: false),
                PoolIdentifier = ToMismoIdentifier(this.mortgagePool.BasePoolNumber, displaySensitive: false),
                PoolIssueDate = ToMismoDate(this.mortgagePool.IssueD_rep, displaySensitive: false),
                PoolMaturityDate = ToMismoDate(this.mortgagePool.MaturityD_rep, displaySensitive: false),
                PoolSecurityIssueDateInterestRatePercent = ToMismoPercent(this.mortgagePool.SecurityR_rep, displaySensitive: false),
                PoolStructureType = ToPoolStructureEnum(this.mortgagePool.IssueTypeCode, displaySensitive: false),
                PoolSuffixIdentifier = ToMismoIdentifier(this.mortgagePool.PoolTypeCode, displaySensitive: false),
                PoolingMethodType = ToPoolingMethodEnum(this.mortgagePool.AmortizationMethodT, displaySensitive: false),
                GovernmentBondFinanceIndicator = ToMismoIndicator(this.mortgagePool.GovernmentBondFinance),
                GovernmentBondFinancingProgramType = ToGovernmentBondFinancingProgramEnum(this.mortgagePool.GovernmentBondFinancingProgramType, displaySensitive: false),
                Extension = this.CreatePoolDetailExtension()
            };

            if (this.mortgagePool.AmortizationT == E_sFinMethT.ARM)
            {
                poolDetail.PoolInterestAdjustmentEffectiveDate = ToMismoDate(this.mortgagePool.SecurityChangeD_rep);
                poolDetail.PoolMarginRatePercent = ToMismoPercent(this.mortgagePool.SecurityRMargin_rep);
            }

            return poolDetail;
        }

        /// <summary>
        /// Creates a POOL_DETAIL_EXTENSION container with extra non-MISMO details about a loan pool.
        /// </summary>
        /// <returns>A <see cref="POOL_DETAIL_EXTENSION"/> XML container.</returns>
        private POOL_DETAIL_EXTENSION CreatePoolDetailExtension()
        {
            return new POOL_DETAIL_EXTENSION()
            {
                Other = this.CreatePddPoolDetailExtension()
            };
        }

        /// <summary>
        /// Creates a party for a loan pool. Use CreatePoolParty_Trust for Trusts.
        /// </summary>
        /// <param name="partyID">The party's ID for the pool.</param>
        /// <param name="partyRole">The role of the party for the pool.</param>
        /// <returns>A <see cref="PARTY"/> container.</returns>
        private PARTY CreatePoolParty(string partyID, PartyRoleBase partyRole)
        {
            if (string.IsNullOrEmpty(partyID))
            {
                return null;
            }

            // MESSAGE/DEAL_SETS/DEAL_SET/PARTIES/PARTY/ROLES/ROLE/PARTY_ROLE_IDENTIFIERS/PARTY_ROLE_IDENTIFIER/PartyRoleIdentifier = partyID
            // Work from the inside out to build PARTY
            MISMOIdentifier identifier = ToMismoIdentifier(partyID, displaySensitive: false);

            var roleDetail = new ROLE_DETAIL()
            {
                PartyRoleType = ToMismoEnum(partyRole, displaySensitive: false)
            };

            var partyRoleIdentifier = new PARTY_ROLE_IDENTIFIER()
            {
                PartyRoleIdentifier = identifier
            };

            var role = new ROLE()
            {
                RoleDetail = roleDetail,
                PartyRoleIdentifiers = new PARTY_ROLE_IDENTIFIERS(partyRoleIdentifier)
            };

            return new PARTY()
            {
                Roles = new ROLES(role)
            };
        }

        /// <summary>
        /// Creates a party for a loan pool representing a trust.
        /// </summary>
        /// <param name="taxpayerID">The taxpayer ID of the trust.</param>
        /// <returns>A <see cref="PARTY"/> container.</returns>
        private PARTY CreatePoolParty_Trust(string taxpayerID)
        {
            if (string.IsNullOrEmpty(taxpayerID))
            {
                return null;
            }

            MISMONumericString id = ToMismoNumericString(taxpayerID, displaySensitive: false);

            var roleDetail = new ROLE_DETAIL()
            {
                PartyRoleType = ToMismoEnum(PartyRoleBase.Trust, displaySensitive: false)
            };

            var role = new ROLE()
            {
                RoleDetail = roleDetail
            };

            var taxpayerIdentifier = new TAXPAYER_IDENTIFIER()
            {
                TaxpayerIdentifierType = ToMismoEnum(TaxpayerIdentifierBase.EmployerIdentificationNumber, displaySensitive: false),
                TaxpayerIdentifierValue = id
            };

            return new PARTY()
            {
                Roles = new ROLES(role),
                TaxpayerIdentifiers = new TAXPAYER_IDENTIFIERS(taxpayerIdentifier)
            };
        }

        /// <summary>
        /// Creates a SECURITIES container holding a list of SECURITY containers.
        /// </summary>
        /// <returns>A <see cref="SECURITIES"/> container.</returns>
        private SECURITIES CreateSecurities()
        {
            if (string.IsNullOrEmpty(this.mortgagePool.SettlementD_rep))
            {
                return null;
            }

            var securityDetail = new SECURITY_DETAIL()
            {
                SecurityTradeBookEntryDate = ToMismoDate(this.mortgagePool.SettlementD_rep)
            };

            var security = new SECURITY()
            {
                SecurityDetail = securityDetail,
                SecurityInvestors = this.CreateSecurityInvestors()
            };

            return new SECURITIES(security);
        }

        /// <summary>
        /// Creates a SECURITY_INVESTOR container with information about an investor in a security.
        /// </summary>
        /// <param name="settlementAccount">The settlement account with this investor's banking information.</param>
        /// <returns>A <see cref="SECURITY_INVESTOR"/> container.</returns>
        private SECURITY_INVESTOR CreateSecurityInvestor(MortgagePool.SettlementAccount settlementAccount)
        {
            // Create PARTY/ROLES/ROLE/ROLE_DETAIL/PartyRoleType = Investor
            var role = new ROLE()
            {
                RoleDetail = new ROLE_DETAIL()
                {
                    PartyRoleType = ToMismoEnum(PartyRoleBase.Investor, displaySensitive: false)
                }
            };

            // Create PARTY/LEGAL_ENTITY/LEGAL_ENTITY_DETAIL/FullName
            var legalEntity = new LEGAL_ENTITY()
            {
                LegalEntityDetail = new LEGAL_ENTITY_DETAIL()
                {
                    FullName = ToMismoString(this.mortgagePool.InvestorName, displaySensitive: false)
                }
            };

            // Create /SECURITY_INVESTORS/SECURITY_INVESTOR/PARTIES/PARTY
            var party = new PARTY()
            {
                LegalEntity = legalEntity,
                Roles = new ROLES(role)
            };

            var investorDetail = new INVESTOR_DETAIL
            {
                SecurityOriginalSubscriptionAmount = ToMismoAmount(losConvert.ToMoneyString(settlementAccount.SecurityInvestorOriginalSubscriptionAmount, FormatDirection.ToRep), displaySensitive: false)
            };

            return new SECURITY_INVESTOR()
            {
                Parties = new PARTIES(party),
                ACH = CreateACH(
                        settlementAccount.AccountNumber.Value,
                        settlementAccount.RoutingNumber.Value,
                        ACHBankAccountPurposeBase.Settlement,
                        settlementAccount.ABAName,
                        settlementAccount.ThirdPartyAccountName),
                InvestorDetail = investorDetail
            };
        }

        /// <summary>
        /// Creates a SECURITY_INVESTORS container holding a list of SECURITY_INVESTOR containers with information about investors in a security.
        /// </summary>
        /// <returns>A <see cref="SECURITY_INVESTORS"/> container.</returns>
        private SECURITY_INVESTORS CreateSecurityInvestors()
        {
            if (!this.mortgagePool.SettlementAccounts.Exists(act => act != null))
            {
                return null;
            }

            var securityInvestors = new SECURITY_INVESTORS();

            foreach (MortgagePool.SettlementAccount settlementAccount in this.mortgagePool.SettlementAccounts)
            {
                securityInvestors.SecurityInvestor.Add(this.CreateSecurityInvestor(settlementAccount));
            }

            return securityInvestors;
        }

        /// <summary>
        /// Generates a user-friendly error message based on the contents of a <see cref="PddExportErrorItem"/> along with loan/pool level and error type.
        /// </summary>
        /// <param name="loanLevel">Whether the error is loan-level (true) or pool-level (false).</param>
        /// <param name="errorType">The error type string to add to the message. Currently used values are: Error, Warning.</param>
        /// <param name="errorList">The error list containing the error item. Used to retrieve loan number for error display.</param>
        /// <param name="errorItem">The error item containing information about the specific error that occurred.</param>
        /// <returns>A properly formatted string to give the user information about the error and where/how to correct it.</returns>
        private string GenerateFriendlyErrorMessage(bool loanLevel, string errorType, PddExportLoanErrorList errorList, PddExportErrorItem errorItem)
        {
            string message = string.Format("{0} {1}", loanLevel ? "Loan" : "Pool", errorType);

            if (loanLevel && !string.IsNullOrEmpty(errorList.LoanNumber))
            {
                message += string.Format("- Loan Number \"{0}\"", errorList.LoanNumber);
            }

            message += string.Format(": {0}", errorItem.ErrorMessage);

            if (!string.IsNullOrEmpty(errorItem.PageLocation))
            {
                message += string.Format(" [{0}]", errorItem.PageLocation);
            }

            if (!string.IsNullOrEmpty(errorItem.Xpath))
            {
                message += string.Format(" |{0}|", errorItem.Xpath);
            }

            return message;
        }

        /// <summary>
        /// Runs an audit on a mortgage pool to determine if the pool has the necessary fields defined for PDD export. Adds the results to <see cref="errors"/> and <see cref="warnings"/>.
        /// </summary>
        /// <param name="mortgagePool">The mortgage pool object to verify.</param>
        private void ValidatePool(MortgagePool mortgagePool)
        {
            var errorList = new PddExportLoanErrorList(null, null, null);
            var warningList = new PddExportLoanErrorList(null, null, null);

            if (string.IsNullOrEmpty(mortgagePool.SettlementD_rep))
            {
                warningList.Add(new PddExportErrorItem("Settlement Date", ErrorMessages.PDD.EmptyPoolSettlementDate, "Pool Details"));
            }

            if (string.IsNullOrEmpty(mortgagePool.InitialPmtD_rep))
            {
                warningList.Add(new PddExportErrorItem("Initial Payment Date", ErrorMessages.PDD.EmptyInitialPaymentDate, "Pool Details"));
            }

            if (mortgagePool.Term == 0)
            {
                warningList.Add(new PddExportErrorItem("Term", ErrorMessages.PDD.ZeroTerm, "Pool Details"));
            }

            if (!issueTypeCodes.Exists(str => str.Equals(mortgagePool.IssueTypeCode, StringComparison.OrdinalIgnoreCase)))
            {
                warningList.Add(new PddExportErrorItem("Issue Type Code", ErrorMessages.PDD.WrongIssueTypeCode + string.Join(", ", issueTypeCodes) + ".", "Pool Details"));
            }

            if (!poolSuffixes.Exists(str => str.Equals(mortgagePool.PoolTypeCode, StringComparison.OrdinalIgnoreCase)))
            {
                warningList.Add(new PddExportErrorItem("Pool Type Code / Suffix", ErrorMessages.PDD.WrongPoolTypeCode + string.Join(", ", poolSuffixes) + ".", "Pool Details"));
            }

            if (mortgagePool.LoanCount == 0)
            {
                warningList.Add(new PddExportErrorItem("Loan Count", ErrorMessages.PDD.ZeroLoanCount, "Assign Loans"));
            }

            if (string.IsNullOrEmpty(mortgagePool.IssueD_rep))
            {
                warningList.Add(new PddExportErrorItem("Issue Date", ErrorMessages.PDD.EmptyIssueDate, "Pool Details"));
            }

            if (string.IsNullOrEmpty(mortgagePool.MaturityD_rep))
            {
                warningList.Add(new PddExportErrorItem("Maturity Date", ErrorMessages.PDD.EmptyMaturityDate, "Pool Details"));
            }

            if (string.IsNullOrEmpty(mortgagePool.PIAccountNum.Value))
            {
                warningList.Add(new PddExportErrorItem("Principal and Interest Account Number", ErrorMessages.PDD.PIAccountNumber, "Pool Details"));
            }

            if (string.IsNullOrEmpty(mortgagePool.TIAccountNum.Value))
            {
                warningList.Add(new PddExportErrorItem("Taxes and Insurance Account Number", ErrorMessages.PDD.TIAccountNumber, "Pool Details"));
            }

            if (mortgagePool.SettlementAccounts.All(account => string.IsNullOrEmpty(account.AccountNumber.Value)))
            {
                warningList.Add(new PddExportErrorItem("Settlement Account Number", ErrorMessages.PDD.SettlementAccountNumber, "Pool Details"));
            }

            if (string.IsNullOrEmpty(mortgagePool.PIRoutingNum.Value) || !routingNumberRegex.IsMatch(mortgagePool.PIRoutingNum.Value))
            {
                warningList.Add(new PddExportErrorItem("Principal and Interest Routing Number", ErrorMessages.PDD.PIRoutingNumber, "Pool Details"));
            }

            if (string.IsNullOrEmpty(mortgagePool.TIRoutingNum.Value) || !routingNumberRegex.IsMatch(mortgagePool.TIRoutingNum.Value))
            {
                warningList.Add(new PddExportErrorItem("Taxes and Insurance Routing Number", ErrorMessages.PDD.TIRoutingNumber, "Pool Details"));
            }

            if (mortgagePool.SettlementAccounts.All(account => string.IsNullOrEmpty(account.RoutingNumber.Value) || !routingNumberRegex.IsMatch(account.RoutingNumber.Value)))
            {
                warningList.Add(new PddExportErrorItem("Settlement Routing Number", ErrorMessages.PDD.SettlementRoutingNumber, "Pool Details"));
            }

            IEnumerable<string> errorStrings = errorList.ErrorList.Select(errorItem => this.GenerateFriendlyErrorMessage(false, "Error", errorList, errorItem));
            if (this.errors != null)
            {
                this.errors.UnionWith(errorStrings);
            }
            else
            {
                this.errors = new HashSet<string>(errorStrings);
            }

            IEnumerable<string> warningStrings = warningList.ErrorList.Select(warningItem => this.GenerateFriendlyErrorMessage(false, "Warning", warningList, warningItem));
            if (this.warnings != null)
            {
                this.warnings.UnionWith(warningStrings);
            }
            else
            {
                this.warnings = new HashSet<string>(warningStrings);
            }
        }

        /// <summary>
        /// Contains information about an individual error.
        /// </summary>
        public class PddExportErrorItem
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="PddExportErrorItem"/> class.
            /// </summary>
            public PddExportErrorItem()
            {
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="PddExportErrorItem"/> class.
            /// </summary>
            /// <param name="fieldId">The field relevant to the error.</param>
            /// <param name="errorMessage">The error message to display.</param>
            /// <param name="pageLocation">The page location to address the error.</param>
            /// <param name="xpath">The xpath where the output data point can be found.</param>
            public PddExportErrorItem(string fieldId, string errorMessage, string pageLocation, string xpath = "")
            {
                this.FieldId = fieldId;
                this.ErrorMessage = errorMessage;
                this.PageLocation = pageLocation;
            }

            /// <summary>
            /// Gets or sets the message of the error.
            /// </summary>
            /// <value>The user error message.</value>
            public string ErrorMessage { get; set; }

            /// <summary>
            /// Gets or sets the field name/identification associated with the error.
            /// </summary>
            /// <value>The field ID.</value>
            public string FieldId { get; set; }

            /// <summary>
            /// Gets or sets the page location for the field to be found.
            /// </summary>
            /// <value>The page relevant to the error.</value>
            public string PageLocation { get; set; }

            /// <summary>
            /// Gets or sets the xpath for the field to be found.
            /// </summary>
            /// <value>The PDD Xpath relevant to the error.</value>
            public string Xpath { get; set; }
        }

        /// <summary>
        /// Stores a list of PDD Export errors and error information from a loan export.
        /// </summary>
        public class PddExportLoanErrorList
        {
            /// <summary>
            /// The internal error list.
            /// </summary>
            private List<PddExportErrorItem> internalErrorList = null;

            /// <summary>
            /// Initializes a new instance of the <see cref="PddExportLoanErrorList"/> class.
            /// </summary>
            /// <param name="loanId">The loan ID associated with the errors.</param>
            /// <param name="loanNum">The loan name for the errors.</param>
            /// <param name="appid">The app ID for the loan.</param>
            public PddExportLoanErrorList(Guid? loanId, string loanNum, Guid? appid)
            {
                this.LoanId = loanId;
                this.LoanNumber = loanNum;
                this.AppId = appid;
                this.internalErrorList = new List<PddExportErrorItem>();
            }

            /// <summary>
            /// Gets or sets the application ID of the first application on the loan.
            /// </summary>
            /// <value>The application ID for the loan.</value>
            public Guid? AppId { get; set; }

            /// <summary>
            /// Gets the list of errors.
            /// </summary>
            /// <value>A list of errors that this object stores.</value>
            public IEnumerable<PddExportErrorItem> ErrorList
            {
                get { return this.internalErrorList; }
            }

            /// <summary>
            /// Gets or sets the loan ID where the errors occurred.
            /// </summary>
            /// <value>The loan ID where the errors occurred.</value>
            public Guid? LoanId { get; set; }

            /// <summary>
            /// Gets or sets the loan name of the loan where the errors occurred.
            /// </summary>
            /// <value>The loan name.</value>
            public string LoanNumber { get; set; }

            /// <summary>
            /// Adds an error to the error list.
            /// </summary>
            /// <param name="error">The error to add to the error list.</param>
            internal void Add(PddExportErrorItem error)
            {
                this.internalErrorList.Add(error);
            }
        }

        /// <summary>
        /// Corresponds to an individual LQB loan file. Implemented as its own class with its own <see cref="CPageData"/> (loan data) object which this object loads for serialization methods to access.
        /// </summary>
        private class PddDealContainer
        {
            /// <summary>
            /// A collection of sequence number counters.
            /// </summary>
            private Mismo33Counters counters;

            /// <summary>
            /// Loan data for the associated deal.
            /// </summary>
            private CPageData loanData;

            /// <summary>
            /// The parent pool.
            /// </summary>
            private MortgagePool mortgagePool;

            /// <summary>
            /// Initializes a new instance of the <see cref="PddDealContainer" /> class.
            /// </summary>
            /// <param name="loanID">The loanID for this deal container to draw data from.</param>
            /// <param name="mortgagePool">The mortgage pool of the parent, for some of the fields in the xml.</param>
            /// <param name="counters">The counters object from the parent.</param>
            public PddDealContainer(Guid loanID, MortgagePool mortgagePool, Mismo33Counters counters)
            {
                this.loanData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanID, typeof(PddDealContainer));
                this.loanData.InitLoad();
                this.loanData.SetFormatTarget(PDDTarget);
                this.loanData.ByPassFieldSecurityCheck = true;

                this.mortgagePool = mortgagePool;
                this.counters = counters;
            }

            /// <summary>
            /// Entry point for the <see cref="PddDealContainer"/> class.
            /// Creates a DEAL container, corresponding to an LQB loan and associated data.
            /// </summary>
            /// <param name="sequenceNumber">The unique (per message) sequence ID number for this loan/DEAL.</param>
            /// <returns>A <see cref="DEAL"/> XML container.</returns>
            public DEAL CreateDeal(int sequenceNumber)
            {
                var deal = new DEAL()
                {
                    Collaterals = this.CreateCollaterals(),
                    Loans = this.CreateLoans(),
                    Parties = this.CreateParties(),
                    SequenceNumber = sequenceNumber
                };

                return deal;
            }

            /// <summary>
            /// Creates an ADDRESS container with information about a property's address.
            /// </summary>
            /// <returns>An <see cref="ADDRESS"/> XML container.</returns>
            private ADDRESS CreateAddress()
            {
                return new ADDRESS()
                {
                    AddressLineText = ToMismoString(this.loanData.sSpAddr, displaySensitive: false), // Max length 40
                    CityName = ToMismoString(this.loanData.sSpCity, displaySensitive: false), // max length 21
                    PostalCode = ToMismoCode(this.loanData.sSpZip, displaySensitive: false), // max length 9
                    StateCode = ToMismoCode(this.loanData.sSpState, displaySensitive: false), // max length 2
                    SequenceNumber = 0
                };
            }

            /// <summary>
            /// Creates a ADJUSTMENT container with information about adjustments to the loan.
            /// </summary>
            /// <returns>A <see cref="ADJUSTMENT"/> XML container.</returns>
            private ADJUSTMENT CreateAdjustment()
            {
                if (this.loanData.sFinMethT != E_sFinMethT.ARM)
                {
                    return null;
                }

                return new ADJUSTMENT()
                {
                    InterestRateAdjustment = this.CreateInterestRateAdjustment()
                };
            }

            /// <summary>
            /// Creates a container holding data on the amortization of a loan.
            /// </summary>
            /// <returns>A <see cref="AMORTIZATION"/> container.</returns>
            private AMORTIZATION CreateAmortization()
            {
                return new AMORTIZATION()
                {
                    AmortizationRule = this.CreateAmortizationRule()
                };
            }

            /// <summary>
            /// Creates a container holding data on the rules governing amortization.
            /// </summary>
            /// <returns>An AMORTIZATION_RULE populated with data.</returns>
            private AMORTIZATION_RULE CreateAmortizationRule()
            {
                return new AMORTIZATION_RULE()
                {
                    AmortizationType = ToAmortizationEnum(this.loanData.sFinMethT, displaySensitive: false),
                };
            }

            /// <summary>
            /// Creates a container representing a borrower.
            /// </summary>
            /// <param name="appData">The borrower's application data.</param>
            /// <returns>A BORROWER container.</returns>
            private BORROWER CreateBorrower(CAppData appData)
            {
                return new BORROWER()
                {
                    BorrowerDetail = this.CreateBorrowerDetail(appData),
                    CreditScores = this.CreateCreditScores(appData),
                    Declaration = this.CreateDeclaration(appData),
                };
            }

            /// <summary>
            /// Creates a container with details about a borrower.
            /// </summary>
            /// <param name="appData">The borrower's application data.</param>
            /// <returns>A BORROWER_DETAIL container.</returns>
            private BORROWER_DETAIL CreateBorrowerDetail(CAppData appData)
            {
                return new BORROWER_DETAIL()
                {
                    BorrowerClassificationType = ToBorrowerClassificationEnum(appData.BorrowerModeT, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a COLLATERAL container holding the SUBJECT_PROPERTY element.
            /// </summary>
            /// <returns>A <see cref="COLLATERAL"/> XML container.</returns>
            private COLLATERAL CreateCollateral()
            {
                return new COLLATERAL()
                {
                    SubjectProperty = this.CreateSubjectProperty()
                };
            }

            /// <summary>
            /// Creates a COLLATERALS container holding the COLLATERAL containers for the loan.
            /// </summary>
            /// <returns>A <see cref="COLLATERALS"/> XML container.</returns>
            private COLLATERALS CreateCollaterals()
            {
                var collaterals = new COLLATERALS();

                collaterals.Collateral.Add(this.CreateCollateral());

                return collaterals;
            }

            /// <summary>
            /// Creates a COMBINED_LTV container with the calculated combined LTV ratio percent for this loan.
            /// </summary>
            /// <param name="loanPurpose">The purpose for this loan.</param>
            /// <param name="houseValue">The value of the home.</param>
            /// <param name="finalLoanAmount">The final amount of the loan.</param>
            /// <param name="sequenceNumber">The sequence number for this combined LTV element in the XML.</param>
            /// <returns>A <see cref="COMBINED_LTV"/> XML container.</returns>
            private COMBINED_LTV CreateCombinedLTV(E_sLPurposeT loanPurpose, decimal houseValue, decimal finalLoanAmount, int sequenceNumber)
            {
                var combinedLTV = new COMBINED_LTV();
                combinedLTV.CombinedLTVRatioPercent = ToMismoPercent(CalculateLtv(loanPurpose, houseValue, finalLoanAmount), displaySensitive: false);
                combinedLTV.SequenceNumber = sequenceNumber;
                return combinedLTV;
            }

            /// <summary>
            /// Creates a COMBINED_LTVS container with information about the combined LTV values for the associated pool.
            /// </summary>
            /// <returns>A <see cref="COMBINED_LTVS"/> XML container.</returns>
            private COMBINED_LTVS CreateCombinedLTVs()
            {
                var combinedLTVs = new COMBINED_LTVS();

                combinedLTVs.CombinedLtv.Add(this.CreateCombinedLTV(loanPurpose: this.loanData.sLPurposeT, houseValue: this.loanData.sHouseVal, finalLoanAmount: this.loanData.sFinalLAmt, sequenceNumber: 1));

                return combinedLTVs;
            }

            /// <summary>
            /// Creates a container representing a credit score from a specific reporting agency.
            /// </summary>
            /// <param name="scoreValueRep">The numeric value of the credit score, as a string.</param>
            /// <param name="sequence">The sequence number of the credit score among the list of scores.</param>
            /// <returns>A CREDIT_SCORE container.</returns>
            private CREDIT_SCORE CreateCreditScore(string scoreValueRep, int sequence)
            {
                var creditScore = new CREDIT_SCORE();
                creditScore.SequenceNumber = sequence;

                if (!string.IsNullOrEmpty(scoreValueRep))
                {
                    creditScore.CreditScoreDetail = this.CreateCreditScoreDetail(scoreValueRep);
                }

                return creditScore;
            }

            /// <summary>
            /// Creates a container holding data on a specific credit score.
            /// </summary>
            /// <param name="score">The credit score.</param>
            /// <returns>A CREDIT_SCORE_DETAIL container.</returns>
            private CREDIT_SCORE_DETAIL CreateCreditScoreDetail(string score)
            {
                var creditScoreDetail = new CREDIT_SCORE_DETAIL();
                creditScoreDetail.CreditScoreValue = ToMismoValue(score, isSensitiveData: true, displaySensitive: false);

                return creditScoreDetail;
            }

            /// <summary>
            /// Creates a container to hold the single credit score expected by GNMA. Uses "Credit score type 2 soft" from the loan file.
            /// </summary>
            /// <param name="appData">The borrower's application data.</param>
            /// <returns>A CREDIT_SCORES container.</returns>
            private CREDIT_SCORES CreateCreditScores(CAppData appData)
            {
                var creditScores = new CREDIT_SCORES();
                creditScores.CreditScore.Add(this.CreateCreditScore(scoreValueRep: this.loanData.sCreditScoreType2Soft_rep, sequence: 0));

                return creditScores;
            }

            /// <summary>
            /// Creates a container representing a declaration.
            /// </summary>
            /// <param name="appData">The borrower's application data.</param>
            /// <returns>A DECLARATION container.</returns>
            private DECLARATION CreateDeclaration(CAppData appData)
            {
                return new DECLARATION()
                {
                    DeclarationDetail = this.CreateDeclarationDetail(appData)
                };
            }

            /// <summary>
            /// Creates a container holding data on a declaration.
            /// </summary>
            /// <param name="appData">The borrower's application data.</param>
            /// <returns>A DECLARATION_DETAIL container.</returns>
            private DECLARATION_DETAIL CreateDeclarationDetail(CAppData appData)
            {
                var declarationDetail = new DECLARATION_DETAIL();

                if (this.loanData.sLT == E_sLT.FHA || this.loanData.sLT == E_sLT.VA)
                {
                    declarationDetail.BorrowerFirstTimeHomebuyerIndicator = ToMismoIndicator(appData.aHas1stTimeBuyerTri == E_TriState.Yes);
                }
                else
                {
                    declarationDetail.BorrowerFirstTimeHomebuyerIndicator = ToMismoIndicator(this.loanData.sHas1stTimeBuyer);
                }

                return declarationDetail;
            }

            /// <summary>
            /// Creates a container holding document-specific information on the URLA (1003) document.
            /// </summary>
            /// <returns>A <see cref="DOCUMENT_SPECIFIC_DATA_SET"/> container.</returns>
            private DOCUMENT_SPECIFIC_DATA_SET CreateDocumentSpecificDataSet_Urla()
            {
                return new DOCUMENT_SPECIFIC_DATA_SET()
                {
                    URLA = this.CreateUrla()
                };
            }

            /// <summary>
            /// Creates a container holding information on specific documents.
            /// </summary>
            /// <returns>A <see cref="DOCUMENT_SPECIFIC_DATA_SETS"/> container.</returns>
            private DOCUMENT_SPECIFIC_DATA_SETS CreateDocumentSpecificDataSets()
            {
                var dataSets = new DOCUMENT_SPECIFIC_DATA_SETS();

                dataSets.DocumentSpecificDataSet.Add(this.CreateDocumentSpecificDataSet_Urla());

                return dataSets;
            }

            /// <summary>
            /// Creates a DOWN_PAYMENT holding information on the down payment for a loan.
            /// </summary>
            /// <param name="sequenceNumber">The sequence number of the down payment among the set of down payments.</param>
            /// <returns>A DOWN_PAYMENT populated with data.</returns>
            private DOWN_PAYMENT CreateDownPayment(int sequenceNumber)
            {
                var downPayment = new DOWN_PAYMENT()
                {
                    DownPaymentAmount = ToMismoAmount(this.loanData.sEquityCalc_rep),
                    FundsType = ToFundsEnum(this.loanData.sDwnPmtSrc),
                    SequenceNumber = sequenceNumber
                };

                if (downPayment.FundsType != null && downPayment.FundsType.enumValue == FundsBase.Other)
                {
                    downPayment.FundsTypeOtherDescription = ToMismoString(this.loanData.sDwnPmtSrc);
                }

                return downPayment;
            }

            /// <summary>
            /// Creates a DOWN_PAYMENTS container which holds a set of down payments.
            /// </summary>
            /// <returns>A DOWN_PAYMENTS container.</returns>
            private DOWN_PAYMENTS CreateDownPayments()
            {
                if (this.loanData.sIsRefinancing)
                {
                    return null;
                }

                var downPayments = new DOWN_PAYMENTS();

                downPayments.DownPayment.Add(this.CreateDownPayment(1));

                return downPayments;
            }

            /// <summary>
            /// Creates the first INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE container.
            /// </summary>
            /// <param name="perChangeRateAdjustmentEffectiveDate">The effective date for this rule.</param>
            /// <param name="perChangeMaximumDecreaseRatePercent">The maximum decrease in rate, in percent, as a string.</param>
            /// <param name="perChangeMaximumIncreaseRatePercent">The maximum increase in rate, in percent, as a string.</param>
            /// <returns>A <see cref="INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE"/> XML container.</returns>
            private INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE CreateFirstInterestRatePerChangeAdjustmentRule(string perChangeRateAdjustmentEffectiveDate, string perChangeMaximumDecreaseRatePercent, string perChangeMaximumIncreaseRatePercent)
            {
                return new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE()
                {
                    AdjustmentRuleType = ToMismoEnum(AdjustmentRuleBase.First, displaySensitive: false),
                    PerChangeRateAdjustmentEffectiveDate = ToMismoDate(perChangeRateAdjustmentEffectiveDate),
                    PerChangeMaximumDecreaseRatePercent = ToMismoPercent(perChangeMaximumDecreaseRatePercent, displaySensitive: false),
                    PerChangeMaximumIncreaseRatePercent = ToMismoPercent(perChangeMaximumIncreaseRatePercent, displaySensitive: false),
                    SequenceNumber = 1
                };
            }

            /// <summary>
            /// Creates a GOVERNMENT_LOAN container with information pertaining to a government loan.
            /// </summary>
            /// <returns>A <see cref="GOVERNMENT_LOAN"/> XML container.</returns>
            private GOVERNMENT_LOAN CreateGovernmentLoan()
            {
                return new GOVERNMENT_LOAN()
                {
                    GovernmentRefinanceType = ToGovernmentRefinanceEnum(this.loanData.sLPurposeT, this.loanData.sHasAppraisal, displaySensitive: false),
                    Extension = this.CreateGovernmentLoanExtension()
                };
            }

            /// <summary>
            /// Creates a GOVERNMENT_LOAN_EXTENSION container with extra information for a government loan.
            /// </summary>
            /// <returns>A <see cref="GOVERNMENT_LOAN_EXTENSION"/> XML container. Serializes as "EXTENSION".</returns>
            private GOVERNMENT_LOAN_EXTENSION CreateGovernmentLoanExtension()
            {
                return new GOVERNMENT_LOAN_EXTENSION()
                {
                    Other = this.CreatePddGovernmentLoanExtension()
                };
            }

            /// <summary>
            /// Creates an INDEX_RULE container.
            /// </summary>
            /// <param name="sequenceNumber">The sequence number of this among all INDEX_RULEs.</param>
            /// <returns>A <see cref="INDEX_RULE"/> XML container.</returns>
            private INDEX_RULE CreateIndexRule(int sequenceNumber)
            {
                return new INDEX_RULE()
                {
                    IndexType = ToIndexEnum(this.mortgagePool.IndexT, displaySensitive: false),
                    InterestAndPaymentAdjustmentIndexLeadDaysCount = ToMismoCount(this.loanData.sArmIndexLeadDays_rep, null, displaySensitive: false),
                    SequenceNumber = sequenceNumber
                };
            }

            /// <summary>
            /// Creates an INDEX_RULES container.
            /// </summary>
            /// <returns>A <see cref="INDEX_RULES"/> XML container.</returns>
            private INDEX_RULES CreateIndexRules()
            {
                var indexRules = new INDEX_RULES();

                indexRules.IndexRule.Add(this.CreateIndexRule(1));

                return indexRules;
            }

            /// <summary>
            /// Creates a container with information about an individual.
            /// </summary>
            /// <param name="firstName">The person's first name.</param>
            /// <param name="middleName">The person's middle name.</param>
            /// <param name="lastName">The person's last name.</param>
            /// <param name="suffix">The person's suffix (for example: junior, III).</param>
            /// <returns>An <see cref="INDIVIDUAL"/> XML container.</returns>
            private INDIVIDUAL CreateIndividual(string firstName, string middleName, string lastName, string suffix)
            {
                return new INDIVIDUAL()
                {
                    Name = this.CreateName(firstName, middleName, lastName, suffix)
                };
            }

            /// <summary>
            /// Creates an INTEREST_RATE_ADJUSTMENT container with information about the interest rate adjustment for an ARM.
            /// </summary>
            /// <returns>A <see cref="INTEREST_RATE_ADJUSTMENT"/> XML container.</returns>
            private INTEREST_RATE_ADJUSTMENT CreateInterestRateAdjustment()
            {
                return new INTEREST_RATE_ADJUSTMENT()
                {
                    IndexRules = this.CreateIndexRules(),
                    InterestRateLifetimeAdjustmentRule = this.CreateInterestRateLifetimeAdjustmentRule(),
                    InterestRatePerChangeAdjustmentRules = this.CreateInterestRatePerChangeAdjustmentRules()
                };
            }

            /// <summary>
            /// Creates an INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE container.
            /// </summary>
            /// <returns>A <see cref="INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE"/> XML container.</returns>
            private INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE CreateInterestRateLifetimeAdjustmentRule()
            {
                return new INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE()
                {
                    CeilingRatePercent = ToMismoPercent(this.loanData.sRAdjLifeCapR_rep, displaySensitive: false),
                    FloorRatePercent = ToMismoPercent(this.loanData.sRAdjFloorR_rep, displaySensitive: false),
                    MarginRatePercent = ToMismoPercent(this.loanData.sRAdjMarginR_rep, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates an INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES container.
            /// </summary>
            /// <returns>A <see cref="INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES"/> XML container.</returns>
            private INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES CreateInterestRatePerChangeAdjustmentRules()
            {
                var perChangeAdjustmentRules = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES();

                perChangeAdjustmentRules.InterestRatePerChangeAdjustmentRule.Add(
                    this.CreateFirstInterestRatePerChangeAdjustmentRule(
                        perChangeRateAdjustmentEffectiveDate: this.loanData.sRAdj1stD_rep,
                        perChangeMaximumDecreaseRatePercent: this.loanData.sRAdjCapR_rep,
                        perChangeMaximumIncreaseRatePercent: this.loanData.sRAdjCapR_rep));
                perChangeAdjustmentRules.InterestRatePerChangeAdjustmentRule.Add(
                    this.CreateSubsequentInterestRatePerChangeAdjustmentRule(
                        perChangeMaximumDecreaseRatePercent: this.loanData.sRAdjCapR_rep,
                        perChangeMaximumIncreaseRatePercent: this.loanData.sRAdjCapR_rep,
                        sequence: 2));

                return perChangeAdjustmentRules;
            }

            /// <summary>
            /// Creates a LOAN container with information about this loan in its closing state.
            /// </summary>
            /// <param name="sequenceNumber">The sequence number for this LOAN container among loans.</param>
            /// <returns>A <see cref="LOAN"/> XML container.</returns>
            private LOAN CreateLoanClosing(int sequenceNumber)
            {
                return new LOAN()
                {
                    Adjustment = this.CreateAdjustment(),
                    Amortization = this.CreateAmortization(),
                    DocumentSpecificDataSets = this.CreateDocumentSpecificDataSets(),
                    DownPayments = this.CreateDownPayments(),
                    GovernmentLoan = this.CreateGovernmentLoan(),
                    LoanDetail = this.CreateLoanDetail(),
                    LoanState = this.CreateLoanState(atclosing: true),
                    Ltv = this.CreateLtv(),
                    LoanRoleType = LoanRoleBase.SubjectLoan,
                    Maturity = this.CreateMaturity(),
                    Payment = this.CreatePayment(),
                    Qualification = this.CreateQualification(),
                    TermsOfLoan = this.CreateTermsOfLoan(this.loanData.sIsRefinancing, this.loanData.sDocumentNoteD_rep),
                    Refinance = this.loanData.sIsRefinancing ? this.CreateRefinance(this.loanData.sLPurposeT) : null,
                    SequenceNumber = sequenceNumber,
                };
            }

            /// <summary>
            /// Creates a LOAN container with information about this loan in its current state.
            /// </summary>
            /// <param name="sequenceNumber">The sequence number of this container among LOAN containers.</param>
            /// <returns>A <see cref="LOAN"/> XML container.</returns>
            private LOAN CreateLoanCurrent(int sequenceNumber)
            {
                var loan = new LOAN()
                {
                    LoanDetail = this.CreateLoanDetail(),
                    LoanIdentifiers = this.CreateLoanIdentifiers(),
                    LoanState = this.CreateLoanState(atclosing: false),
                    LoanRoleType = LoanRoleBase.SubjectLoan,
                    MERSRegistrations = this.CreateMersRegistrations(),
                    Payment = this.CreatePayment(),
                    SequenceNumber = sequenceNumber,
                };

                if (this.loanData.sIsRefinancing)
                {
                    loan.Refinance = this.CreateRefinance(this.loanData.sLPurposeT);
                }

                if (this.loanData.sFinMethT == E_sFinMethT.ARM)
                {
                    loan.Adjustment = this.CreateAdjustment();
                }

                return loan;
            }

            /// <summary>
            /// Creates a LOAN_DETAIL container with details about the loan.
            /// </summary>
            /// <returns>A <see cref="LOAN_DETAIL"/> XML container.</returns>
            private LOAN_DETAIL CreateLoanDetail()
            {
                return new LOAN_DETAIL()
                {
                    ApplicationReceivedDate = ToMismoDate(this.loanData.sAppSubmittedD_rep),
                    BuydownTemporarySubsidyFundingIndicator = ToMismoIndicator(this.loanData.sHasTempBuydown),
                    CurrentInterestRatePercent = ToMismoPercent(this.loanData.sNoteIR_rep, displaySensitive: false),

                    // We don't support modification.
                    MortgageModificationIndicator = ToMismoIndicator(false)
                };
            }

            /// <summary>
            /// Creates a LOAN_IDENTIFIER container with information about an identifier for the loan.
            /// </summary>
            /// <returns>A <see cref="LOAN_IDENTIFIER"/> XML container.</returns>
            private LOAN_IDENTIFIER CreateLoanIdentifier()
            {
                return new LOAN_IDENTIFIER()
                {
                    LoanIdentifier = ToMismoIdentifier(this.loanData.sLenderCaseNum, displaySensitive: false),
                    LoanIdentifierType = ToMismoEnum(LoanIdentifierBase.PoolIssuerLoan, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a LOAN_IDENTIFIERS container with a list of identifiers for the loan.
            /// </summary>
            /// <returns>A <see cref="LOAN_IDENTIFIERS"/> XML container.</returns>
            private LOAN_IDENTIFIERS CreateLoanIdentifiers()
            {
                var loanIdentifiers = new LOAN_IDENTIFIERS();

                loanIdentifiers.LoanIdentifier.Add(this.CreateLoanIdentifier());

                return loanIdentifiers;
            }

            /// <summary>
            /// Creates a LOANS container which has information about loans in this deal. For LQB purposes, this contains 2 versions of each loan: one current and one at closing.
            /// </summary>
            /// <returns>An <see cref="LOANS"/> XML container.</returns>
            private LOANS CreateLoans()
            {
                var loans = new LOANS();

                loans.CombinedLtvs = this.CreateCombinedLTVs();
                loans.Loan.Add(this.CreateLoanClosing(1));
                loans.Loan.Add(this.CreateLoanCurrent(2));

                return loans;
            }

            /// <summary>
            /// Creates a LOAN_STATE container with information about a loan's state.
            /// </summary>
            /// <param name="atclosing">A boolean indicating whether the loan is the "AtClosing" or "Current" LOAN container.</param>
            /// <returns>A <see cref="LOAN_STATE"/> XML container.</returns>
            private LOAN_STATE CreateLoanState(bool atclosing)
            {
                return new LOAN_STATE()
                {
                    LoanStateDate = ToMismoDate(this.loanData.sDocumentNoteD_rep),
                    LoanStateType = ToMismoEnum(atclosing ? LoanStateBase.AtClosing : LoanStateBase.Current, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a LTV container with loan's LTV ratio as percent.
            /// </summary>
            /// <returns>A <see cref="LTV"/> XML container.</returns>
            private LTV CreateLtv()
            {
                return new LTV()
                {
                    LTVRatioPercent = ToMismoPercent(CalculateLtv(this.loanData.sLPurposeT, this.loanData.sHouseVal, this.loanData.sFinalLAmt), displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a MATURITY container.
            /// </summary>
            /// <returns>A <see cref="MATURITY"/> XML container.</returns>
            private MATURITY CreateMaturity()
            {
                return new MATURITY()
                {
                    MaturityRule = this.CreateMaturityRule()
                };
            }

            /// <summary>
            /// Creates a MATURITY_RULE container.
            /// </summary>
            /// <returns>A <see cref="MATURITY_RULE"/> XML container.</returns>
            private MATURITY_RULE CreateMaturityRule()
            {
                return new MATURITY_RULE()
                {
                    LoanMaturityDate = ToMismoDate(losConvert.ToDateTimeString(this.loanData.sSchedDueD1.DateTimeForComputation.AddMonths(this.loanData.sDue - 1))),
                    LoanMaturityPeriodCount = ToMismoCount(this.loanData.sDue_rep, losConvert, displaySensitive: false),
                    LoanMaturityPeriodType = ToMismoEnum(LoanMaturityPeriodBase.Month, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a MERS_REGISTRATION container.
            /// </summary>
            /// <returns>A <see cref="MERS_REGISTRATION"/> XML container.</returns>
            private MERS_REGISTRATION CreateMersRegistration()
            {
                return new MERS_REGISTRATION()
                {
                    MERSOriginalMortgageeOfRecordIndicator = ToMismoIndicator(this.loanData.sMersIsOriginalMortgagee)
                };
            }

            /// <summary>
            /// Creates a MERS_REGISTRATIONS container.
            /// </summary>
            /// <returns>A <see cref="MERS_REGISTRATIONS"/> XML container.</returns>
            private MERS_REGISTRATIONS CreateMersRegistrations()
            {
                var mersRegistrations = new MERS_REGISTRATIONS();

                mersRegistrations.MersRegistration.Add(this.CreateMersRegistration());

                return mersRegistrations;
            }

            /// <summary>
            /// Creates a container with name information (for an individual).
            /// </summary>
            /// <param name="firstName">The person's first name.</param>
            /// <param name="middleName">The person's middle name.</param>
            /// <param name="lastName">The person's last name.</param>
            /// <param name="suffix">The person's suffix (for example: junior, III).</param>
            /// <returns>A <see cref="NAME"/> XML container.</returns>
            private NAME CreateName(string firstName, string middleName, string lastName, string suffix)
            {
                return new NAME()
                {
                    FirstName = ToMismoString(firstName, displaySensitive: false),
                    MiddleName = ToMismoString(middleName, displaySensitive: false),
                    LastName = ToMismoString(lastName, displaySensitive: false),
                    SuffixName = ToMismoString(suffix, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a PARTIES container containing the loan's parties.
            /// </summary>
            /// <returns>A <see cref="PARTIES"/> XML container.</returns>
            private PARTIES CreateParties()
            {
                var parties = new PARTIES();

                parties.Party.Add(this.CreatePartyBorrower(this.loanData.GetAppData(0), 0));
                parties.Party.Add(this.CreatePartyLoanOriginator(this.loanData.sBranchChannelT, 1));

                return parties;
            }

            /// <summary>
            /// Creates a PARTY container containing information about a loan's borrower.
            /// </summary>
            /// <param name="appData">The borrower's application data object.</param>
            /// <param name="sequenceNumber">The sequence number of this party in the list of parties.</param>
            /// <returns>A <see cref="PARTY"/> XML container.</returns>
            private PARTY CreatePartyBorrower(CAppData appData, int sequenceNumber)
            {
                return new PARTY()
                {
                    Individual = this.CreateIndividual(appData.aFirstNm, appData.aMidNm, appData.aLastNm, appData.aSuffix),
                    Roles = this.CreateRoles(appData),
                    TaxpayerIdentifiers = Mismo33RequestProvider.CreateTaxpayerIdentifiers(appData.aSsn, TaxpayerIdentifierBase.SocialSecurityNumber, displaySensitive: false),
                    SequenceNumber = this.counters.Parties
                };
            }

            /// <summary>
            /// Creates a party container for the loan originator.
            /// </summary>
            /// <param name="channelType">The originator's channel type.</param>
            /// <param name="sequence">Sequence number for this party in the list of parties.</param>
            /// <returns>A <see cref="PARTY"/> container with information about the loan's originator.</returns>
            private PARTY CreatePartyLoanOriginator(E_BranchChannelT channelType, int sequence)
            {
                return new PARTY()
                {
                    Roles = new ROLES(this.CreateRole_LoanOriginator(channelType, 0)),
                    SequenceNumber = sequence
                };
            }

            /// <summary>
            /// Creates a PAYMENT container.
            /// </summary>
            /// <returns>A <see cref="PAYMENT"/> XML container.</returns>
            private PAYMENT CreatePayment()
            {
                return new PAYMENT()
                {
                    PaymentRule = this.CreatePaymentRule(),
                    PaymentSummary = this.CreatePaymentSummary(),
                    PaymentComponentBreakouts = this.CreatePaymentComponentBreakouts()
                };
            }

            /// <summary>
            /// Creates a container holding information on a payment component breakout.
            /// </summary>
            /// <param name="pipaymentAmount_rep">The principal and interest amount that is part of the total payment being reported.</param>
            /// <param name="sequence">The sequence number of the payment component breakout among the list of breakouts.</param>
            /// <returns>A <see cref="PAYMENT_COMPONENT_BREAKOUT"/> container.</returns>
            private PAYMENT_COMPONENT_BREAKOUT CreatePaymentComponentBreakout(string pipaymentAmount_rep, int sequence)
            {
                var paymentComponentBreakout = new PAYMENT_COMPONENT_BREAKOUT();
                paymentComponentBreakout.SequenceNumber = sequence;
                paymentComponentBreakout.PaymentComponentBreakoutDetail = this.CreatePaymentComponentBreakoutDetail(pipaymentAmount_rep);

                return paymentComponentBreakout;
            }

            /// <summary>
            /// Creates a container holding data on a payment component breakout.
            /// </summary>
            /// <param name="pipaymentAmount_rep">The principal and interest amount that is part of the total payment being reported.</param>
            /// <returns>A PAYMENT_COMPONENT_BREAKOUT_DETAIL populated with data.</returns>
            private PAYMENT_COMPONENT_BREAKOUT_DETAIL CreatePaymentComponentBreakoutDetail(string pipaymentAmount_rep)
            {
                return new PAYMENT_COMPONENT_BREAKOUT_DETAIL()
                {
                    PrincipalAndInterestPaymentAmount = ToMismoAmount(pipaymentAmount_rep, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a container to hold all instances of PAYMENT_COMPONENT_BREAKOUTS.
            /// </summary>
            /// <returns>A PAYMENT_COMPONENT_BREAKOUTS container.</returns>
            private PAYMENT_COMPONENT_BREAKOUTS CreatePaymentComponentBreakouts()
            {
                var paymentComponentBreakouts = new PAYMENT_COMPONENT_BREAKOUTS();
                paymentComponentBreakouts.PaymentComponentBreakout.Add(this.CreatePaymentComponentBreakout(this.loanData.sProThisMPmt_rep, 1));

                return paymentComponentBreakouts;
            }

            /// <summary>
            /// Creates a PAYMENT_RULE container.
            /// </summary>
            /// <returns>A <see cref="PAYMENT_RULE"/> XML container.</returns>
            private PAYMENT_RULE CreatePaymentRule()
            {
                return new PAYMENT_RULE()
                {
                    ScheduledFirstPaymentDate = ToMismoDate(this.loanData.sSchedDueD1_rep)
                };
            }

            /// <summary>
            /// Creates a container holding summary data about a payment.
            /// </summary>
            /// <returns>A <see cref="PAYMENT_SUMMARY"/> XML container.</returns>
            private PAYMENT_SUMMARY CreatePaymentSummary()
            {
                return new PAYMENT_SUMMARY()
                {
                    AggregateLoanCurtailmentAmount = ToMismoAmount(this.loanData.sServicingCollectedFunds_Principal_rep, displaySensitive: false),
                    LastPaidInstallmentDueDate = ToMismoDate(this.loanData.sServicingLastPaymentMadeDueD_rep),
                    UPBAmount = ToMismoAmount(this.loanData.sServicingUnpaidPrincipalBalance_rep, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a LQB_GOVERNMENT_LOAN_EXTENSION container with extra non-MISMO information for a government loan.
            /// </summary>
            /// <returns>A <see cref="LQB_GOVERNMENT_LOAN_EXTENSION"/> XML container. Serializes as "OTHER".</returns>
            private LQB_GOVERNMENT_LOAN_EXTENSION CreatePddGovernmentLoanExtension()
            {
                return new LQB_GOVERNMENT_LOAN_EXTENSION()
                {
                    GovernmentAnnualPremiumAmount = ToMismoAmount(this.loanData.sProMIns_rep, displaySensitive: false),
                    GovernmentAnnualPremiumPercent = ToMismoPercent(losConvert.ToRateString(this.loanData.sProMInsR), displaySensitive: false),
                    GovernmentUpfrontPremiumAmount = ToMismoAmount(this.loanData.sFfUfmip1003_rep, displaySensitive: false),
                    GovernmentUpfrontPremiumPercent = ToMismoPercent(losConvert.ToRateString(this.loanData.sFfUfmipR), displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a PDD_TERMS_OF_LOAN_EXTENSION container with extension information about the loan terms.
            /// </summary>
            /// <param name="mortgageProgramType">The mortgage program type for the loan.</param>
            /// <returns>A <see cref="PDD_TERMS_OF_LOAN_EXTENSION"/> XML container. Serializes as "OTHER".</returns>
            private PDD_TERMS_OF_LOAN_EXTENSION CreatePddTermsOfLoanExtension(E_sLT mortgageProgramType)
            {
                return new PDD_TERMS_OF_LOAN_EXTENSION()
                {
                    MortgageProgramType = ToMortgageProgramEnum(mortgageProgramType, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a PROPERTY_DETAIL container with details about a property.
            /// </summary>
            /// <param name="numUnits">The number of units on the property.</param>
            /// <returns>A <see cref="PROPERTY_DETAIL"/> XML container.</returns>
            private PROPERTY_DETAIL CreatePropertyDetail(int numUnits)
            {
                return new PROPERTY_DETAIL()
                {
                    FinancedUnitCount = ToMismoCount(Math.Min(numUnits, 4), losConvert, displaySensitive: false),
                    ConstructionMethodType = ToConstructionMethodEnum(this.loanData.sGseSpT, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a PROPERTY_VALUATION container with information about a valuation for a property.
            /// </summary>
            /// <returns>A <see cref="PROPERTY_VALUATION"/> XML container.</returns>
            private PROPERTY_VALUATION CreatePropertyValuation()
            {
                return new PROPERTY_VALUATION()
                {
                    PropertyValuationDetail = this.CreatePropertyValuationDetail()
                };
            }

            /// <summary>
            /// Creates a PROPERTY_VALUATION container with details about a valuation for a property.
            /// </summary>
            /// <returns>A <see cref="PROPERTY_VALUATION"/> XML container.</returns>
            private PROPERTY_VALUATION_DETAIL CreatePropertyValuationDetail()
            {
                return new PROPERTY_VALUATION_DETAIL()
                {
                    PropertyValuationAmount = ToMismoAmount(this.loanData.sApprVal_rep, displaySensitive: false),
                    PropertyValuationEffectiveDate = ToMismoDate(this.loanData.sSpValuationEffectiveD_rep)
                };
            }

            /// <summary>
            /// Creates a PROPERTY_VALUATIONS container with property valuations for a property.
            /// </summary>
            /// <returns>A <see cref="PROPERTY_VALUATIONS"/> XML container.</returns>
            private PROPERTY_VALUATIONS CreatePropertyValuations()
            {
                var propertyValuations = new PROPERTY_VALUATIONS();

                propertyValuations.PropertyValuation.Add(this.CreatePropertyValuation());

                return propertyValuations;
            }

            /// <summary>
            /// Creates a QUALIFICATION container with information about the loan qualification.
            /// </summary>
            /// <returns>A <see cref="QUALIFICATION"/> XML container.</returns>
            private QUALIFICATION CreateQualification()
            {
                return new QUALIFICATION()
                {
                    TotalDebtExpenseRatioPercent = ToMismoPercent(this.loanData.sLTotI == 0 ? 0.ToString("F2") : this.loanData.sQualBottomR.ToString("F2"), displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a REFINANCE container with information about loan refinancing.
            /// </summary>
            /// <param name="loanPurpose">The purpose of the loan.</param>
            /// <returns>A <see cref="REFINANCE"/> XML container.</returns>
            private REFINANCE CreateRefinance(E_sLPurposeT loanPurpose)
            {
                return new REFINANCE()
                {
                    RefinanceCashOutDeterminationType = ToRefinanceCashOutDeterminationEnum(string.Empty, loanPurpose, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a ROLE container with information about the BORROWER role for a party.
            /// </summary>
            /// <param name="appData">The borrower's application data.</param>
            /// <param name="sequenceNumber">The sequence number of this role in the party's list of roles.</param>
            /// <returns>A <see cref="ROLE"/> XML container.</returns>
            private ROLE CreateRole_Borrower(CAppData appData, int sequenceNumber)
            {
                return new ROLE()
                {
                    RoleDetail = new ROLE_DETAIL()
                    {
                        PartyRoleType = ToMismoEnum(PartyRoleBase.Borrower, displaySensitive: false)
                    },
                    Borrower = this.CreateBorrower(appData),
                    SequenceNumber = sequenceNumber
                };
            }

            /// <summary>
            /// Creates a role container representing the loan originator role.
            /// </summary>
            /// <param name="channelType">The channel type of the loan originator.</param>
            /// <param name="sequence">The sequence number of the role among the list of roles.</param>
            /// <returns>A <see cref="ROLE"/> container.</returns>
            private ROLE CreateRole_LoanOriginator(E_BranchChannelT channelType, int sequence)
            {
                return new ROLE()
                {
                    LoanOriginator = new LOAN_ORIGINATOR()
                    {
                        LoanOriginatorType = ToLoanOriginatorEnum(channelType),
                    },
                    RoleDetail = new ROLE_DETAIL()
                    {
                        PartyRoleType = ToMismoEnum(PartyRoleBase.LoanOriginator, displaySensitive: false)
                    }
                };
            }

            /// <summary>
            /// Creates a ROLES container with a list of roles for this party.
            /// </summary>
            /// <param name="appData">The application data associated with this party.</param>
            /// <returns>A <see cref="ROLES"/> XML container.</returns>
            private ROLES CreateRoles(CAppData appData)
            {
                var roles = new ROLES();
                int borrowerRoleCounter = 0;
                roles.Role.Add(this.CreateRole_Borrower(appData, ++borrowerRoleCounter));

                return roles;
            }

            /// <summary>
            /// Creates a SUBJECT_PROPERTY container with information about the loan's subject property.
            /// </summary>
            /// <returns>A <see cref="PROPERTY"/> XML container.</returns>
            private PROPERTY CreateSubjectProperty()
            {
                return new PROPERTY()
                {
                    Address = this.CreateAddress(),
                    PropertyDetail = this.CreatePropertyDetail(this.loanData.sUnitsNum),
                    PropertyValuations = this.CreatePropertyValuations()
                };
            }

            /// <summary>
            /// Creates the subsequent INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE container.
            /// </summary>
            /// <param name="perChangeMaximumDecreaseRatePercent">The maximum decrease in rate, in percent, as a string.</param>
            /// <param name="perChangeMaximumIncreaseRatePercent">The maximum increase in rate, in percent, as a string.</param>
            /// <param name="sequence">The sequence number of this rule among per-change adjustment rules.</param>
            /// <returns>A <see cref="INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE"/> XML container.</returns>
            private INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE CreateSubsequentInterestRatePerChangeAdjustmentRule(string perChangeMaximumDecreaseRatePercent, string perChangeMaximumIncreaseRatePercent, int sequence)
            {
                return new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE()
                {
                    PerChangeMaximumDecreaseRatePercent = ToMismoPercent(perChangeMaximumDecreaseRatePercent, displaySensitive: false),
                    PerChangeMaximumIncreaseRatePercent = ToMismoPercent(perChangeMaximumIncreaseRatePercent, displaySensitive: false),
                    AdjustmentRuleType = ToMismoEnum(AdjustmentRuleBase.Subsequent, displaySensitive: false),
                    SequenceNumber = sequence
                };
            }

            /// <summary>
            /// Creates a TERMS_OF_LOAN container with information about the loan terms.
            /// </summary>
            /// <param name="isRefinancing">Whether the loan is a refinancing of a previous loan.</param>
            /// <param name="noteDate">The note date on the loan.</param>
            /// <returns>A <see cref="TERMS_OF_LOAN"/> XML container.</returns>
            private TERMS_OF_LOAN CreateTermsOfLoan(bool isRefinancing, string noteDate)
            {
                return new TERMS_OF_LOAN()
                {
                    LoanPurposeType = ToLoanPurposeEnum(isRefinancing ? E_sLPurposeT.Refin : E_sLPurposeT.Purchase, isHeloc: false, displaySensitive: false),
                    NoteDate = ToMismoDate(noteDate),
                    NoteAmount = ToMismoAmount(this.loanData.sFinalLAmt_rep, displaySensitive: false),
                    Extension = this.CreateTermsOfLoanExtension(this.loanData.sLT),
                    MortgageType = ToMortgageEnum(this.loanData.sLT, displaySensitive: false)
                };
            }

            /// <summary>
            /// Creates a TERMS_OF_LOAN_EXTENSION container with information about the loan terms.
            /// </summary>
            /// <param name="mortgageProgramType">The type of mortgage program.</param>
            /// <returns>A <see cref="TERMS_OF_LOAN_EXTENSION"/> XML container. Serializes as "EXTENSION".</returns>
            private TERMS_OF_LOAN_EXTENSION CreateTermsOfLoanExtension(E_sLT mortgageProgramType)
            {
                return new TERMS_OF_LOAN_EXTENSION()
                {
                    Other = this.CreatePddTermsOfLoanExtension(mortgageProgramType)
                };
            }

            /// <summary>
            /// Creates a container holding information on the URLA (1003) document.
            /// </summary>
            /// <returns>A <see cref="URLA"/> container.</returns>
            private URLA CreateUrla()
            {
                return new URLA()
                {
                    UrlaDetail = new URLA_DETAIL()
                    {
                        PurchasePriceAmount = ToMismoAmount(this.loanData.sPurchPrice_rep, displaySensitive: false)
                    }
                };
            }
        }
    }
}