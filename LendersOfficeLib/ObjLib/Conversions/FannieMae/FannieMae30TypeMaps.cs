using System;

using LendersOffice.Common;
using DataAccess;
using System.Collections.Generic;
using DataAccess.Core.Construction;

namespace LendersOffice.Conversions
{
    /// <summary>
    /// 1093 Mortgage Applied For (Real Estate Loan Type Code)
    /// 
    /// 01 = Conventional
    /// 02 = Veterans Administration
    /// 03 = Federal Housing Administration
    /// 04 = Farmers Home Administration
    /// 07 = Other Real Estate Loan, Other
    /// </summary>
    class TypeMap_sLT 
    {
        public static E_sLT ToLO(string value) 
        {
            
            switch (value) 
            {
                case "01": return E_sLT.Conventional;
                case "02": return E_sLT.VA;
                case "03": return E_sLT.FHA;
                case "04": return E_sLT.UsdaRural;
                case "07": return E_sLT.Other;
                default:
                    return E_sLT.Other;
            }
        }
        public static string ToFannie(E_sLT value) 
        {
            switch (value) 
            {
                case E_sLT.Conventional: return "01";
                case E_sLT.VA: return "02";
                case E_sLT.FHA: return "03";
                case E_sLT.UsdaRural: return "04";
                case E_sLT.Other: return "07";
                default:
                    return "07";
            }
        }
    }

    /// <summary>
    /// 1085 Amortization Type (Loan Payment Type Code)
    /// 
    /// 01 = Adjustable Payment Based on Index
    /// 04 = Growing Equity Mortgage
    /// 05 = Fixed Rate
    /// 06 = Graduated Payment Mortgage (GPM)
    /// 13 = Other Loan Payment Type
    /// </summary>
    class TypeMap_sFinMethT 
    {
        public static E_sFinMethT ToLO(string value) 
        {
            switch (value) 
            {
                case "01": return E_sFinMethT.ARM;
                case "05": return E_sFinMethT.Fixed;
                case "06": return E_sFinMethT.Graduated;
                case "04":
                case "13":
                    return E_sFinMethT.Fixed;
                default:
                    return E_sFinMethT.Fixed;
            }
        }
        public static string ToFannie(E_sFinMethT value) 
        {
            switch (value) 
            {
                case E_sFinMethT.ARM: return "01";
                case E_sFinMethT.Fixed: return "05";
                case E_sFinMethT.Graduated: return "06";
                default:
                    return "05";

            }
        }
    }

	// 10/11/07 db - OPM 18181
	/// <summary>
	/// 1 = Full Documentation (FHA or VA)
	/// 2 = Streamline with Appraisal (FHA Only)
	/// 3 = Streamline without Appraisal (FHA Only)
	/// 4 = Interest Rate Reduction Refinance Loan (VA Only)
    /// H = HOPE For Homeowners (FHA Only)
    /// R - Prior FHA
	/// Note: It might be nice to have an enum for these values instead of just text, in case they ever change
	/// </summary>
	class TypeMap_sFHARefinanceTypeDesc
	{
		public static string ToLO(string value) 
		{
			switch (value) 
			{
				case "1": return "Full Documentation";
				case "2": return "Streamline with Appraisal";
				case "3": return "Streamline without Appraisal";
                case "H": return "HOPE for Homeowners";
                case "R": return "Prior FHA";
				default:
					return "";
			}
		}

		public static string ToFannie(E_sLPurposeT sLPurposeT, bool sFHAPurposeIsStreamlineRefi, E_sTotalScoreRefiT sTotalScoreRefiT) 
		{
            if (sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                return "4";
            }
            if (sFHAPurposeIsStreamlineRefi)
            {
                return "3";
            }
            if (sTotalScoreRefiT == E_sTotalScoreRefiT.FHAToFHANonStreamline)
            {
                return "R";
            }
            else
            {
                return "1";
            }
		}
	}

	class TypeMap_sHmdaPreapprovalT
	{
		public static E_sHmdaPreapprovalT ToLO(string value) 
		{
			switch (value) 
			{
				case "Y": return E_sHmdaPreapprovalT.PreapprovalRequested;
				case "N": return E_sHmdaPreapprovalT.PreapprovalNotRequested;
				default:
					return E_sHmdaPreapprovalT.NotApplicable;
			}
		}
		public static string ToFannie(E_sHmdaPreapprovalT value) 
		{
			switch (value) 
			{
				case E_sHmdaPreapprovalT.LeaveBlank: return " ";
				case E_sHmdaPreapprovalT.NotApplicable:return " ";
				case E_sHmdaPreapprovalT.PreapprovalNotRequested: return "N";
				case E_sHmdaPreapprovalT.PreapprovalRequested: return "Y";
				default:
					return " ";
			}
		}
	}

    class TypeMap_aTotalScoreFhtbCounselingT
    {
        public static E_aTotalScoreFhtbCounselingT ToLO(string value)
        {
            switch (value)
            {
                case "A": return E_aTotalScoreFhtbCounselingT.NotCounseled;
                case "D": return E_aTotalScoreFhtbCounselingT.HudApprovedCounseling;
                default:
                    return E_aTotalScoreFhtbCounselingT.LeaveBlank;
            }
        }
        public static string ToFannie(E_aTotalScoreFhtbCounselingT value)
        {
            switch (value)
            {
                case E_aTotalScoreFhtbCounselingT.HudApprovedCounseling:
                    return "D";
                case E_aTotalScoreFhtbCounselingT.NotCounseled:
                    return "A";
                case E_aTotalScoreFhtbCounselingT.LeaveBlank:
                case E_aTotalScoreFhtbCounselingT.NotApplicable:
                default:
                    return "";
            }
        }
    }
	// 10/11/07 db - OPM 18181
	/// <summary>
	/// 01 = Community Home Buyer Program
	/// 02 = Fannie 97
	/// 03 = Fannie 32
	/// 04 = MyCommunityMortgage
	/// </summary>
	class TypeMap_sFannieCommunityLendingT
	{
		public static E_sFannieCommunityLendingT ToLO(string value) 
		{
			switch (value) 
			{
				case "01": return E_sFannieCommunityLendingT.CommunityHomeBuyer;
				case "02": return E_sFannieCommunityLendingT.Fannie97;
				case "03": return E_sFannieCommunityLendingT.Fannie32;
				case "04": return E_sFannieCommunityLendingT.MyCommunityMortgage;
                case "06": return E_sFannieCommunityLendingT.HFAPreferredRiskSharing;
                case "07": return E_sFannieCommunityLendingT.HFAPreferred;
                case "08": return E_sFannieCommunityLendingT.HomeReady;
				default:
					return E_sFannieCommunityLendingT.LeaveBlank;
			}
		}
		public static string ToFannie(E_sFannieCommunityLendingT value) 
		{
			switch (value) 
			{
				case E_sFannieCommunityLendingT.CommunityHomeBuyer: return "01";
				case E_sFannieCommunityLendingT.Fannie97:return "02";
				case E_sFannieCommunityLendingT.Fannie32: return "03";
				case E_sFannieCommunityLendingT.MyCommunityMortgage: return "04";
                case E_sFannieCommunityLendingT.HFAPreferredRiskSharing: return "06";
                case E_sFannieCommunityLendingT.HFAPreferred: return "07";
                case E_sFannieCommunityLendingT.HomeReady: return "08";
                case E_sFannieCommunityLendingT.LeaveBlank: return " ";
				default:
					return "";
			}
		}
	}

    // 5/21/14 db - OPM 179296
    /// <summary>
    /// 01 = Any payment (including interest only, P&I, etc.) required within first 5 years
    /// 02 = Payments deferred 5 or more years and fully forgiven
    /// 03 = Payments deferred 5 or more years and not fully forgiven
    /// </summary>
    class TypeMap_sFannieCommunitySecondsRepaymentStructureT
    {
        public static E_sFannieCommunitySecondsRepaymentStructureT ToLO(string value)
        {
            switch (value)
            {
                case "01": return E_sFannieCommunitySecondsRepaymentStructureT.AnyPaymentWithin5Years;
                case "02": return E_sFannieCommunitySecondsRepaymentStructureT.PaymentsDeferredFullyForgiven;
                case "03": return E_sFannieCommunitySecondsRepaymentStructureT.PaymentsDeferredNotFullyForgiven;
                default:
                    return E_sFannieCommunitySecondsRepaymentStructureT.LeaveBlank;
            }
        }
        public static string ToFannie(E_sFannieCommunitySecondsRepaymentStructureT value)
        {
            switch (value)
            {
                case E_sFannieCommunitySecondsRepaymentStructureT.AnyPaymentWithin5Years: return "01";
                case E_sFannieCommunitySecondsRepaymentStructureT.PaymentsDeferredFullyForgiven: return "02";
                case E_sFannieCommunitySecondsRepaymentStructureT.PaymentsDeferredNotFullyForgiven: return "03";
                default:
                    return "";
            }
        }
    }
  


    /// <summary>
    /// 1081 Loan Purpose Code
    /// 
    /// 04 = Construct Home, Construction
    /// 05 = Refinance
    /// 13 = Construct New Home and Convert to Permanent, Construction-Permanent
    /// 15 = Other Loan Purpose, Other
    /// 16 = Purchase - Purpose Unidentified, Purchase
    /// </summary>
    class TypeMap_sLPurposeT 
    {
        public static E_sLPurposeT ToLO(string value) 
        {
            switch (value) 
            {
                case "04": return E_sLPurposeT.Construct;
                case "05": return E_sLPurposeT.Refin;
                case "13": return E_sLPurposeT.ConstructPerm;
                case "15": return E_sLPurposeT.Other;
                case "16": return E_sLPurposeT.Purchase;
                default:
                    return E_sLPurposeT.Purchase;
            }
        }
        public static string ToFannie(E_sLPurposeT loanPurpose, ConstructionPurpose constructionPurpose, bool isDuDoExport) 
        {
            switch (loanPurpose) 
            {
                case E_sLPurposeT.Construct:
                    return "04";
                case E_sLPurposeT.VaIrrrl: 
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.Refin:
                    return "05";
                case E_sLPurposeT.ConstructPerm:
                    if (isDuDoExport && constructionPurpose == ConstructionPurpose.ConstructionAndLotPurchase)
                    {
                        // DU guidance specifies that a loan must be entered as Construction if it will be closed
                        // as a Purchase transaction. This overrides the usual Construction-to-Permanent mapping.
                        return "04";
                    }
                    else
                    {
                        return "13";
                    }
                case E_sLPurposeT.Other:
                    return "15";
                case E_sLPurposeT.Purchase:
                    return "16";
                default:
                    Tools.LogWarning(string.Format("Unhandle sLPurposeT type: {0}. Default to purchase", loanPurpose));
                    return "16";
            }
        }
    }

    /// <summary>
    /// 1070 Property Will Be (Type of Residence Code)
    /// 
    /// 1 = Primary Residence
    /// 2 = Secondary Residence
    /// D = Investment Property
    /// </summary>
    class TypeMap_aOccT 
    {
        public static E_aOccT ToLO(string value) 
        {
            switch (value) 
            {
                case "1": return E_aOccT.PrimaryResidence;
                case "2": return E_aOccT.SecondaryResidence;
                case "D": return E_aOccT.Investment;
                default:
                    return E_aOccT.PrimaryResidence;
            }
        }

        public static string ToFannie(E_aOccT value) 
        {
            switch (value) 
            {
                case E_aOccT.Investment: return "D";
                case E_aOccT.SecondaryResidence: return "2";
                case E_aOccT.PrimaryResidence: 
                default:
                    return "1";

                
            }
        }
    }

    /// <summary>
    /// Property Right Code
    /// 
    /// 1 = Fee Simple
    /// 2 = Lease Hold
    /// </summary>
    class TypeMap_sEstateHeldT 
    {
        public static E_sEstateHeldT ToLO(string value) 
        {
            switch (value) 
            {
                case "1": return E_sEstateHeldT.FeeSimple;
                case "2": return E_sEstateHeldT.LeaseHold;
                default:
                    return E_sEstateHeldT.FeeSimple;
            }
        }
        public static string ToFannie(E_sEstateHeldT value) 
        {
            switch (value) 
            {
                case E_sEstateHeldT.LeaseHold: return "2";
                case E_sEstateHeldT.FeeSimple: 
                default:
                    return "1";

            }
        }
    }

    /// <summary>
    /// 1082 Purpose of Refinance Code (Use of proceeds)
    /// 
    /// F1 = Change in Rate/Term-$100 max, No Cash-Out rate/Term - $100 max
    /// 01 = Cash, Cash-Out/Other
    /// 04 = Home Improvement, Cash-Out/Home Improvement
    /// 11 = Debt Consolidation Refinance, Cash-Out/Debt Consolidation
    /// 13 = Change in Loan Type, Limited Cash-Out Rate/Term-1% max
    /// </summary>
    class TypeMap_sRefPurpose 
    {
        public static string ToLO(string value) 
        {
            switch (value) 
            {
                case "F1": return "No Cash-Out Rate/Term";
                case "01": return "Cash-Out/Other";
                case "04": return "Cash-Out/Home Improvement";
                case "11": return "Cash-Out/Debt Consolidation";
                case "13": return "Limited Cash-Out Rate/Term";
                default:
                    return "";
            }
    
        }
        public static string ToFannie(string value) 
        {
            switch (value.ToLower()) 
            {
                case "no cash-out rate/term": return "F1";
                case "cash-out/other": return "01";
                case "cash-out rate/term": return "01";
                case "cash-out/home improvement": return "04";
                case "cash-out/debt consolidation": return "11";
                case "limited cash-out rate/term": return "13";
                default:
                    return "";

            }
        }
    }

    /// <summary>
    /// Y = Made
    /// N = To be made
    /// U = Unknown
    /// </summary>
    class TypeMap_sSpImprovTimeFrameT 
    {
        public static E_sSpImprovTimeFrameT ToLO(string value) 
        {
            switch (value) 
            {
                case "Y": return E_sSpImprovTimeFrameT.Made;
                case "N": return E_sSpImprovTimeFrameT.ToBeMade;
                default:
                    return E_sSpImprovTimeFrameT.LeaveBlank;
            }
        }

        public static string ToFannie(E_sSpImprovTimeFrameT value) 
        {
            switch (value) 
            {
                case E_sSpImprovTimeFrameT.Made: return "Y";
                case E_sSpImprovTimeFrameT.ToBeMade: return "N";
                case E_sSpImprovTimeFrameT.LeaveBlank: 
                default:
                    return "U";

            }
        }
    }

    /// <summary>
    /// 1083 Type of Downpayment Code
    /// 
    /// F1 = Checking/Savings
    /// F2 = Deposit on Sales Contract
    /// F3 = Equity on Sold Property
    /// 03 = Equity from Pending Sale
    /// F4 = Equity from Subject Property
    /// 04 = Cash-Gift, Gift Funds
    /// F5 = Stocks & Bonds
    /// 10 = Lot Equity
    /// 09 = Bridge Loan
    /// 01 = Cash-Borrowed, Unsecured Borrowed Funds
    /// F6 = Trust Funds
    /// F7 = Retirement Funds
    /// 11 = Rent with Option to Purchase
    /// F8 = Life Insurance Cash Value
    /// 14 = Sale of Chattel
    /// 07 = Trade Equity
    /// 06 = Sweat Equity
    /// 02 = Cash on Hand
    /// 13 = Other Type of Down Payment
    /// 28 = Secured Borrowed Funds
    /// </summary>
    class TypeMap_sDwnPmtSrc 
    {
        public static string ToLO(string value) 
        {
            switch (value) 
            {
                case "F1": return "Checking/Savings";
                case "F2": return "Deposit on Sales Contract";
                case "F3": return "Equity on Sold Property";
                case "03": return "Equity from Pending Sale";
                case "F4": return "Equity from Subject Property";
                case "04": return "Gift Funds";
                case "F5": return "Stocks & Bonds";
                case "10": return "Lot Equity";
                case "09": return "Bridge Loan";
                case "01": return "Unsecured Borrowed Funds"; 
                case "F6": return "Trust Funds";
                case "F7": return "Retirement Funds";
                case "11": return "Rent with Option to Purchase";
                case "F8": return "Life Insurance Cash Value";
                case "14": return "Sale of Chattel";
                case "07": return "Trade Equity";
                case "06": return "Sweat Equity";
                case "02": return "Cash on Hand";
                case "13": return "Other Type of Down Payment";
                case "28": return "Secured Borrowed Funds";
                case "H0": return "FHA - Gift - Source N/A";
                case "H1": return "FHA - Gift - Source Relative";
                case "H3": return "FHA - Gift - Source Government Assistance";
                case "H6": return "FHA - Gift - Source Employer";
                case "H4": return "FHA - Gift - Source Nonprofit/Religious/Community - Seller Funded";
                case "H5": return "FHA - Gift - Source Nonprofit/Religious/Community - Non-Seller Funded";
                default:
                    return "";
               
            }
        }
        public static string ToFannie(string value) 
        {
            switch (value.ToLower()) 
            {
                case "checking/savings": return "F1";
                case "deposit on sales contract": return "F2";
                case "equity on sold property": return "F3";
                case "equity from pending sale": return "03";
                case "equity from subject property": return "F4";
                case "gift funds": return "04";
                case "stocks & bonds": return "F5";
                case "lot equity": return "10";
                case "bridge loan": return "09";
                case "unsecured borrowed funds": return "01"; 
                case "trust funds": return "F6";
                case "retirement funds": return "F7";
                case "rent with option to purchase": return "11";
                case "life insurance cash value": return "F8";
                case "sale of chattel": return "14";
                case "trade equity": return "07";
                case "sweat equity": return "06";
                case "cash on hand": return "02";
                case "other type of down payment": return "13";
                case "secured borrowed funds": return "28";
                case "fha - gift - source n/a": return "H0";
                case "fha - gift - source relative": return "H1";
                case "fha - gift - source government assistance": return "H3";
                case "fha - gift - source employer": return "H6";
                case "fha - gift - source nonprofit/religious/community - seller funded": return "H4";
                case "fha - gift - source nonprofit/religious/community - non-seller funded": return "H5";
                default:
                    return "13";
               
            }

        }
    }

    /// <summary>
    /// 1067 Marital Status Code
    /// 
    /// M = Married
    /// S = Separated
    /// U = Unmarried
    /// </summary>
    class TypeMap_aMaritalStatT 
    {
        public static E_aBMaritalStatT ToLO(string value) 
        {
            switch (value) 
            {
                case "M": return E_aBMaritalStatT.Married;
                case "S": return E_aBMaritalStatT.Separated;
                case "U": return E_aBMaritalStatT.NotMarried;
                default:
                    return E_aBMaritalStatT.LeaveBlank;
            }
        }
        public static string ToFannie(E_aBMaritalStatT value) 
        {
            switch (value) 
            {
                case E_aBMaritalStatT.Married: return "M";
                case E_aBMaritalStatT.Separated: return "S";
                case E_aBMaritalStatT.NotMarried: return "U";
                default:
                    return "";
            }
        }
    }

    /// <summary>
    /// 1078 Own/Rent/Living Rent Free (Property Ownership Rights)
    /// 
    /// X = Living Rent Free
    /// R = Rent
    /// O = Own
    /// </summary>
    class TypeMap_aAddrT 
    {
        public static E_aBAddrT ToLO(string value) 
        {
            switch (value) 
            {
                case "R": return E_aBAddrT.Rent;
                case "O": return E_aBAddrT.Own;
                case "X": return E_aBAddrT.LivingRentFree;
                default:
                    return E_aBAddrT.LeaveBlank;
            }
        }
        public static string ToFannie(E_aBAddrT value) 
        {
            switch (value) 
            {
                case E_aBAddrT.Rent: return "R";
                case E_aBAddrT.Own: return "O";
                case E_aBAddrT.LivingRentFree: return "X";
                case E_aBAddrT.LeaveBlank: 
                default:
                    return "";
            }
        }
    }

    class TypeMap_OtherIncomeDescription 
    {
        public static string ToLO(string value)
        {
            E_aOIDescT descriptionType;
            switch (value)
            {
                case "F1":
                    descriptionType = E_aOIDescT.MilitaryBasePay;
                    break;
                case "07":
                    descriptionType = E_aOIDescT.MilitaryRationsAllowance;
                    break;
                case "F2":
                    descriptionType = E_aOIDescT.MilitaryFlightPay;
                    break;
                case "F3":
                    descriptionType = E_aOIDescT.MilitaryHazardPay;
                    break;
                case "02":
                    descriptionType = E_aOIDescT.MilitaryClothesAllowance;
                    break;
                case "04":
                    descriptionType = E_aOIDescT.MilitaryQuartersAllowance;
                    break;
                case "03":
                    descriptionType = E_aOIDescT.MilitaryPropPay;
                    break;
                case "F4":
                    descriptionType = E_aOIDescT.MilitaryOverseasPay;
                    break;
                case "F5":
                    descriptionType = E_aOIDescT.MilitaryCombatPay;
                    break;
                case "F6":
                    descriptionType = E_aOIDescT.MilitaryVariableHousingAllowance;
                    break;
                case "F7":
                    descriptionType = E_aOIDescT.AlimonyChildSupport;
                    break;
                case "F8":
                    descriptionType = E_aOIDescT.NotesReceivableInstallment;
                    break;
                case "41":
                    descriptionType = E_aOIDescT.PensionRetirement;
                    break;
                case "42":
                    descriptionType = E_aOIDescT.SocialSecurityDisability;
                    break;
                case "30":
                    descriptionType = E_aOIDescT.RealEstateMortgageDifferential;
                    break;
                case "F9":
                    descriptionType = E_aOIDescT.Trust;
                    break;
                case "AU":
                    descriptionType = E_aOIDescT.AccessoryUnitIncome;
                    break;
                case "M1":
                    descriptionType = E_aOIDescT.UnemploymentWelfare;
                    break;
                case "M2":
                    descriptionType = E_aOIDescT.AutomobileExpenseAccount;
                    break;
                case "M3":
                    descriptionType = E_aOIDescT.FosterCare;
                    break;
                case "M4":
                    descriptionType = E_aOIDescT.VABenefitsNonEducation;
                    break;
                case "NB":
                    descriptionType = E_aOIDescT.NonBorrowerHouseholdIncome;
                    break;
                case "SI":
                    descriptionType = E_aOIDescT.SubjPropNetCashFlow;
                    break;
                case "S8":
                    descriptionType = E_aOIDescT.HousingChoiceVoucher;
                    break;
                case "BI":
                    descriptionType = E_aOIDescT.BoarderIncome;
                    break;
                case "MC":
                    descriptionType = E_aOIDescT.MortgageCreditCertificate;
                    break;
                case "TC":
                    descriptionType = E_aOIDescT.TrailingCoBorrowerIncome;
                    break;
                case "CG":
                    descriptionType = E_aOIDescT.CapitalGains;
                    break;
                case "EA":
                    descriptionType = E_aOIDescT.EmploymentRelatedAssets;
                    break;
                case "FI":
                    descriptionType = E_aOIDescT.ForeignIncome;
                    break;
                case "RP":
                    descriptionType = E_aOIDescT.RoyaltyPayment;
                    break;
                case "SE":
                    descriptionType = E_aOIDescT.SeasonalIncome;
                    break;
                case "TL":
                    descriptionType = E_aOIDescT.TemporaryLeave;
                    break;
                case "TI":
                    descriptionType = E_aOIDescT.TipIncome;
                    break;
                case "45":
                    descriptionType = E_aOIDescT.Other;
                    break;
                default:
                    return string.Empty;
            }

            return OtherIncome.GetDescription(descriptionType);
        }

        public static string ToFannie(string description, E_sLT sLT)
        {
            // OPM 105216 - FHA/VA loans don't support some newer income types. GF
            bool isFHAOrVA = sLT == E_sLT.FHA || sLT == E_sLT.VA;
            if (description != null && description.Equals("housing choice voucher", StringComparison.OrdinalIgnoreCase))
            {
                return "S8";
            }
            else if (description != null && description.Equals("subject property net cash flow (two-to-four unit owner-occupied properties)", StringComparison.OrdinalIgnoreCase))
            {
                return "SI";
            }
            else
            {
                var descriptionType = OtherIncome.Get_aOIDescT(description);
                switch (descriptionType)
                {
                    case E_aOIDescT.MilitaryBasePay:
                        return "F1";
                    case E_aOIDescT.MilitaryRationsAllowance:
                        return "07";
                    case E_aOIDescT.MilitaryFlightPay:
                        return "F2";
                    case E_aOIDescT.MilitaryHazardPay:
                        return "F3";
                    case E_aOIDescT.MilitaryClothesAllowance:
                        return "02";
                    case E_aOIDescT.MilitaryQuartersAllowance:
                        return "04";
                    case E_aOIDescT.MilitaryPropPay:
                        return "03";
                    case E_aOIDescT.MilitaryOverseasPay:
                        return "F4";
                    case E_aOIDescT.MilitaryCombatPay:
                        return "F5";
                    case E_aOIDescT.MilitaryVariableHousingAllowance:
                        return "F6";
                    case E_aOIDescT.AlimonyChildSupport:
                    case E_aOIDescT.Alimony:
                    case E_aOIDescT.ChildSupport:
                        return "F7";
                    case E_aOIDescT.NotesReceivableInstallment:
                        return "F8";
                    case E_aOIDescT.PensionRetirement:
                        return "41";
                    case E_aOIDescT.SocialSecurityDisability:
                    case E_aOIDescT.SocialSecurity:
                    case E_aOIDescT.Disability:
                        return "42";
                    case E_aOIDescT.RealEstateMortgageDifferential:
                        return "30";
                    case E_aOIDescT.Trust:
                        return "F9";
                    case E_aOIDescT.AccessoryUnitIncome:
                        return "AU";
                    case E_aOIDescT.UnemploymentWelfare:
                        return "M1";
                    case E_aOIDescT.AutomobileExpenseAccount:
                        return "M2";
                    case E_aOIDescT.FosterCare:
                        return "M3";
                    case E_aOIDescT.VABenefitsNonEducation:
                        return "M4";
                    case E_aOIDescT.NonBorrowerHouseholdIncome:
                        return "NB";
                    case E_aOIDescT.SubjPropNetCashFlow:
                        return "SI";
                    case E_aOIDescT.HousingChoiceVoucher:
                        return "S8";
                    case E_aOIDescT.BoarderIncome:
                        return "BI";
                    case E_aOIDescT.MortgageCreditCertificate:
                        return "MC";
                    case E_aOIDescT.TrailingCoBorrowerIncome:
                        return "TC";
                    case E_aOIDescT.CapitalGains:
                        return isFHAOrVA ? "45" : "CG";
                    case E_aOIDescT.EmploymentRelatedAssets:
                        return isFHAOrVA ? "45" : "EA";
                    case E_aOIDescT.ForeignIncome:
                        return isFHAOrVA ? "45" : "FI";
                    case E_aOIDescT.RoyaltyPayment:
                        return isFHAOrVA ? "45" : "RP";
                    case E_aOIDescT.SeasonalIncome:
                        return isFHAOrVA ? "45" : "SE";
                    case E_aOIDescT.TemporaryLeave:
                        return isFHAOrVA ? "45" : "TL";
                    case E_aOIDescT.TipIncome:
                        return isFHAOrVA ? "45" : "TI";
                    case E_aOIDescT.Other:
                    case E_aOIDescT.ContractBasis:
                    case E_aOIDescT.DefinedContributionPlan:
                    case E_aOIDescT.HousingAllowance:
                    case E_aOIDescT.MiscellaneousIncome:
                    case E_aOIDescT.PublicAssistance:
                    case E_aOIDescT.WorkersCompensation:
                        return "45";
                    default:
                        throw new UnhandledEnumException(descriptionType);
                }
            }
        }
    }


	class TypeMap_AssetSpecialT 
	{
		public static string ToFannie(E_AssetSpecialT value) 
		{
			switch (value) 
			{
				case E_AssetSpecialT.Business: return "F8";
				case E_AssetSpecialT.Retirement: return "08";
				case E_AssetSpecialT.CashDeposit: return "F1";
				default:
					return "M1";
			}
		}
	}

    /// <summary>
    /// 569 Account/Asset Type (Account Number Qualifier)
    /// 
    /// 03 = Checking Account
    /// F1 = Cash Deposit on Sales Contract
    /// SG = Savings Account
    /// F2 = Gift Not Deposited
    /// 01 = Certificate of Deposit (Time Dep)
    /// F3 = Money Market Fund
    /// F4 = Mutual Funds
    /// 05 = Stock
    /// 06 = Bond
    /// F5 = Secured Borrowed Funds Not Deposit
    /// F7 = Bridge Loan Not Deposited
    /// 08 = Retirement Funds
    /// F8 = Net Worth of Business Owned
    /// 11 = Trust Funds
    /// M1 = Other Asset Type "Other Non-Liquid Asset"
    /// OL = Other Liquid Asset
    /// NE = Net Equity - Sale of Real Estate
    /// COH = Cash On Hand
    /// GE = Gift Of Equity
    /// </summary>
    class TypeMap_AssetT 
    {
        public static E_AssetT ToLO(string value) 
        {
            switch (value) 
            {
                case "03": return E_AssetT.Checking;
                case "F1": return E_AssetT.CashDeposit;
                case "SG": return E_AssetT.Savings;
                case "F2": return E_AssetT.GiftFunds;
                case "01": return E_AssetT.CertificateOfDeposit;
                case "F3": return E_AssetT.MoneyMarketFund;
                case "F4": return E_AssetT.MutualFunds;
                case "05": return E_AssetT.Stocks;
                case "06": return E_AssetT.Bonds;
                case "F5": return E_AssetT.SecuredBorrowedFundsNotDeposit;
                case "F7": return E_AssetT.BridgeLoanNotDeposited;
                case "08": return E_AssetT.Retirement;
                case "F8": return E_AssetT.Business;
                case "11": return E_AssetT.TrustFunds;
                case "M1": return E_AssetT.OtherIlliquidAsset;
                case "OL": return E_AssetT.OtherLiquidAsset;
                case "NE": return E_AssetT.PendingNetSaleProceedsFromRealEstateAssets;
                case "GE": return E_AssetT.GiftEquity;
                default:
                    return E_AssetT.OtherIlliquidAsset;

            }
        }

        public static string ToFannie(E_AssetT value) 
        {
            switch (value) 
            {
                case E_AssetT.CertificateOfDeposit: return "01";
                case E_AssetT.MoneyMarketFund: return "F3";
                case E_AssetT.MutualFunds: return "F4";
                case E_AssetT.SecuredBorrowedFundsNotDeposit: return "F5";
                case E_AssetT.BridgeLoanNotDeposited: return "F7";
                case E_AssetT.TrustFunds: return "11";
                case E_AssetT.Checking: return "03";
                case E_AssetT.CashDeposit: return "F1";
                case E_AssetT.Savings: return "SG";
                case E_AssetT.GiftFunds: return "F2";
                case E_AssetT.Stocks: return "05";
                case E_AssetT.Bonds: return "06";
                case E_AssetT.Retirement: return "08";
                case E_AssetT.Business: return "F8";
                case E_AssetT.OtherLiquidAsset: return "OL";
                case E_AssetT.PendingNetSaleProceedsFromRealEstateAssets: return "NE";
                case E_AssetT.GiftEquity: return "GE";
                case E_AssetT.OtherIlliquidAsset: 
                default:
                    return "M1";
            }
        }
    }

    /// <summary>
    /// 1075 Status of Plans for Real Estate Asset
    /// 
    ///     S = Already Sold
    ///     H = Will Remain or Become Primary
    ///     P = Pending Sale
    ///     R = Rental Being Held for Income
    /// </summary>
    class TypeMap_ReFieldStat 
    {
        public static string ToLO(string value) 
        {
            switch (value) 
            {
                case "S": return "S";
                case "P": return "PS";
                case "R": return "R";
                default:
                    return "";
            }
        }
        public static string ToFannie(string value) 
        {
            switch (value.ToUpper()) 
            {
                case "S": return "S";
                case "PS": return "P";
                case "R": return "R";
                default:
                    return "H";

            }
        }
    }

    /// <summary>
    /// 1074 Type of Property (Type of Real Estate Asset)
    /// 
    ///     14 = Single Family
    ///     04 = Condominium
    ///     16 = Townhouse
    ///     13 = Co-Operative
    ///     15 = Two-to-Four-Unit Property
    ///     18 = Multifamily (More than 4 units)
    ///     08 = Manufactured/Mobile Home
    ///     02 = Commercial - Non-Residential
    ///     F1 = Mixed Use - Residential
    ///     05 = Farm
    ///     03 = Home & Business Combined
    ///     07 = Land
    /// </summary>
    class TypeMap_ReFieldType 
    {
        public static E_ReoTypeT ToLO(string value) 
        {
            switch (value) 
            {
                case "15": return E_ReoTypeT._2_4Plx;
                case "02": return E_ReoTypeT.ComNR;
                case "F1": return E_ReoTypeT.Mixed;
                case "03": return E_ReoTypeT.ComR;
                case "04": return E_ReoTypeT.Condo;
                case "13": return E_ReoTypeT.Coop;
                case "07": return E_ReoTypeT.Land;
                case "08": return E_ReoTypeT.Mobil;
                case "18": return E_ReoTypeT.Multi;
                case "14": return E_ReoTypeT.SFR;
                case "16": return E_ReoTypeT.Town;
                case "05": return E_ReoTypeT.Farm;
                default:
                    return E_ReoTypeT.LeaveBlank;
            }
        }
        public static string ToFannie(E_ReoTypeT value) 
        {
            switch (value)
            {
                case E_ReoTypeT.LeaveBlank:
                    return "";
                case E_ReoTypeT._2_4Plx:
                    return "15";
                case E_ReoTypeT.ComNR:
                    return "02";
                case E_ReoTypeT.ComR:
                    return "03";
                case E_ReoTypeT.Condo:
                    return "04";
                case E_ReoTypeT.Coop:
                    return "13";
                case E_ReoTypeT.Farm:
                    return "05";
                case E_ReoTypeT.Land:
                    return "07";
                case E_ReoTypeT.Mixed:
                    return "F1";
                case E_ReoTypeT.Mobil:
                    return "08";
                case E_ReoTypeT.Multi:
                    return "18";
                case E_ReoTypeT.SFR:
                    return "14";
                case E_ReoTypeT.Town:
                    return "16";
                case E_ReoTypeT.Other:
                    return "";
                default:
                    throw new UnhandledEnumException(value);
            }
        }
    }

    /// <summary>
    /// 1189 Liability Type (Type of Credit Account)
    /// 
    ///     I = Installment Loan
    ///     O = Open, 30 Day Charge Account
    ///     R = Revolving Charge
    ///     C = Credit Line
    ///     M = Mortgage
    ///     F = Lease Payments
    ///     N = Liens
    ///     A = Taxes
    ///     Z = Other Liability
    /// </summary>
    static class TypeMap_DebtRegularT 
    {
        public static E_DebtRegularT ToLO(string value) 
        {
            switch (value) 
            {
                case "I": return E_DebtRegularT.Installment;
                case "O": return E_DebtRegularT.Open;
                case "R": return E_DebtRegularT.Revolving;
                case "C": return E_DebtRegularT.Mortgage; // OPM 170767; this may get migrated to a new HELOC type per case 60580
                case "M": return E_DebtRegularT.Mortgage;
                default:
                    return E_DebtRegularT.Other;
            }
        }

        public static string ToLODebtOtherDescription(string debtOtherDescription)
        {
            switch (debtOtherDescription)
            {
                case "M":
                    return "Mortgage";
                case "C":
                    return "Home Equity Line of Credit";
                case "I":
                    return "Installment Loan";
                case "F":
                    return "Lease Payments";
                case "N":
                    return "Liens";
                case "O":
                    return "Open, 30day Charge Acc";
                case "R":
                    return "Revolving Charge";
                case "A":
                    return "Taxes";
                default:
                    return "Other Liability";
            }
        }

        public static string ToFannie(E_DebtRegularT value, string debtOtherDescription) 
        {
            switch (value) 
            {
                case E_DebtRegularT.Installment: return "I";
                case E_DebtRegularT.Open: return "O";
                case E_DebtRegularT.Revolving: return "R";
                case E_DebtRegularT.Mortgage: return "M";
                case E_DebtRegularT.Other: return DebtOtherDescriptionToFannie(debtOtherDescription);
                default:
                    return "Z";
            }

        }

        public static string DebtOtherDescriptionToFannie(string debtOtherDescription)
        {
            switch (debtOtherDescription.ToLower())
            {
                case "mortgage":
                    return "M";
                case "home equity line of credit":
                    return "C";
                case "installment loan":
                    return "I";
                case "lease payments":
                    return "F";
                case "liens":
                    return "N";
                case "open, 30day charge acc":
                    return "O";
                case "revolving charge":
                    return "R";
                case "taxes":
                    return "A";
                default:
                    return "Z";
            }
        }
    }

    /// <summary>
    /// Other Credit Type Code
    /// 
    ///     01 = Cash Deposit on sales contract
    ///     02 = Seller Credit
    ///     03 = Lender Credit
    ///     04 = Relocation Funds
    ///     05 = Employer Assisted Housing
    ///     06 = Lease Purchase Fund
    ///     07 = Other
    ///     08 = Borrower Paid Fees
    /// </summary>
    class TypeMap_OtherCreditTypeCode 
    {
        public static string ToLO(string value) 
        {
            switch (value) 
            {
                case "01": return "Cash Deposit on sales contract";
                case "02": return "Seller Credit";
                case "03": return "Lender Credit";
                case "04": return "Relocation Funds";
                case "05": return "Employer Assisted Housing";
                case "06": return "Lease Purchase Fund";
                case "08": return "Borrower Paid Fees";
                default:
                    return "Other";
            }
        }

        public static string ToFannie(string value) 
        {
            switch (value.ToLower()) 
            {
                case "cash deposit on sales contract": return "01";
                case "seller credit": return "02";
                case "lender credit": return "03";
                case "relocation funds": return "04";
                case "employer assisted housing": return "05";
                case "lease purchase fund": return "06";
                case "borrower paid fees": return "08";
                case "sweat equity": return "09";
                default:
                    return "07";
            }
        }
    }

    /// <summary>
    /// 1070 Property Will Be (Type of Residence Code)
    /// 
    ///     1 = Primary Residence
    ///     2 = Secondary Residence
    ///     D = Investment Property
    /// </summary>
    class TypeMap_aDecPastOwnedPropT 
    {
        public static E_aBDecPastOwnedPropT ToLO(string value) 
        {
            switch (value) 
            {
                case "1": return E_aBDecPastOwnedPropT.PR;
                case "2": return E_aBDecPastOwnedPropT.SH;
                case "D": return E_aBDecPastOwnedPropT.IP;
                default: 
                    return E_aBDecPastOwnedPropT.Empty;
            }
        }

        public static string ToFannie(E_aBDecPastOwnedPropT value) 
        {
            switch (value) 
            {
                case E_aBDecPastOwnedPropT.PR: return "1";
                case E_aBDecPastOwnedPropT.SH: return "2";
                case E_aBDecPastOwnedPropT.IP: return "D";
                case E_aBDecPastOwnedPropT.Empty:
                default:
                    return "";

            }
        }
    }

    /// <summary>
    /// 1187 How Title Held (Type of Account)
    /// 
    ///     01 = Sole (Individual)
    ///     25 = Joint With Spouse
    ///     26 = JOint With Other Than Spouse
    /// </summary>
    class TypeMap_aDecPastOwnedPropTitleT 
    {
        public static E_aBDecPastOwnedPropTitleT ToLO(string value) 
        {
            switch (value) 
            {
                case "01": return E_aBDecPastOwnedPropTitleT.S;
                case "25": return E_aBDecPastOwnedPropTitleT.SP;
                case "26": return E_aBDecPastOwnedPropTitleT.O;
                default:
                    return E_aBDecPastOwnedPropTitleT.Empty;
            }
        }
        public static string ToFannie(E_aBDecPastOwnedPropTitleT value) 
        {
            switch (value) 
            {
                case E_aBDecPastOwnedPropTitleT.S: return "01";
                case E_aBDecPastOwnedPropTitleT.SP: return "25";
                case E_aBDecPastOwnedPropTitleT.O: return "26";
                case E_aBDecPastOwnedPropTitleT.Empty:
                default:
                    return "";
            }
        }
    }

    /// <summary>
    /// 1109 Race or Ethnicity Code
    /// 
    ///     I = American Indian or Alaskan Native
    ///     A = Asian or Pacific Islander
    ///     O = White (Non-Hispanic)
    ///     N = Black (Non-Hispanic)
    ///     H = Hispanic
    ///     E = Other Ethnicity
    ///     F = Information Not Provided
    /// </summary>
    class TypeMap_aRaceT 
    {
        public static E_aBRaceT ToLO(string value) 
        {
            switch (value) 
            {
                case "I": return E_aBRaceT.AmericanIndian;
                case "A": return E_aBRaceT.Asian;
                case "O": return E_aBRaceT.White;
                case "N": return E_aBRaceT.Black;
                case "H": return E_aBRaceT.Hispanic;
                case "E": return E_aBRaceT.Other;
                case "F": return E_aBRaceT.NotFurnished;
                default:
                    return E_aBRaceT.LeaveBlank;
            }
        }
    }

    class TypeMap_aGender 
    {
        public static E_GenderT ToLO(string value) 
        {
            switch (value) 
            {
                case "F": return E_GenderT.Female;
                case "M": return E_GenderT.Male;
                case "N": return E_GenderT.NA;
                default:
                    return E_GenderT.LeaveBlank;
            }
        }
        public static string ToFannie(E_GenderT value) 
        {
            switch (value) 
            {
                case E_GenderT.Female: return "F";
                case E_GenderT.Male: return "M";
                case E_GenderT.NA: return "N";
                case E_GenderT.LeaveBlank:
                default:
                    return "I";
            }
        }
    }
    class TypeMap_aHispanicT
    {
        public static E_aHispanicT ToLO(string value) 
        {
            switch (value) 
            {
                case "1": return E_aHispanicT.Hispanic;
                case "2": return E_aHispanicT.NotHispanic;
                default:
                    return E_aHispanicT.LeaveBlank;
            }
        }

        public static string ToFannie(E_aHispanicT value) 
        {
            switch (value) 
            {
                case E_aHispanicT.Hispanic: return "1";
                case E_aHispanicT.NotHispanic: return "2";
                case E_aHispanicT.LeaveBlank:
                default:
                    return "3";
            }
        }

    }

    /// <summary>
    /// 1079 Contact Method Code
    /// 
    ///     F = Face-To-Face
    ///     M = Mail
    ///     T = Telephone
    /// </summary>
    class TypeMap_aIntervwrMethodT 
    {
        public static E_aIntrvwrMethodT ToLO(string value) 
        {
            switch (value) 
            {
                case "F": return E_aIntrvwrMethodT.FaceToFace;
                case "M": return E_aIntrvwrMethodT.ByMail;
                case "T": return E_aIntrvwrMethodT.ByTelephone;
                case "I": return E_aIntrvwrMethodT.Internet;
                default:
                    return E_aIntrvwrMethodT.LeaveBlank;
            }

        }
        public static string ToFannie(E_aIntrvwrMethodT value) 
        {
            switch (value) 
            {
                case E_aIntrvwrMethodT.FaceToFace: return "F";
                case E_aIntrvwrMethodT.ByMail: return "M";
                case E_aIntrvwrMethodT.ByTelephone: return "T";
                case E_aIntrvwrMethodT.Internet: return "I";
                default:
                    return "";
            }
        }
    }

    /// <summary>
    /// 1101 Lien Type Code (Lien Priority Code)
    /// 
    ///     1 = First Mortgage
    ///     2 = Second Mortgage
    ///     F = Other Mortgage
    /// </summary>
    class TypeMap_sLienPosT 
    {
        public static E_sLienPosT ToLO(string value) 
        {
            switch (value) 
            {
                case "1": return E_sLienPosT.First;
                case "2": return E_sLienPosT.Second;
                default:
                    return E_sLienPosT.First;
            }
        }
        public static string ToFannie(E_sLienPosT value) 
        {
            switch (value) 
            {
                case E_sLienPosT.First: return "1";
                case E_sLienPosT.Second: return "2";
                default:
                    return "F";
            }
        }
    }

    /// <summary>
    /// 1103 Loan Documentation Type Code
    ///     A = Alternative (Non-traditional documentation used to determine the creditworthiness of a borrower), Alternative
    ///     F = Full
    ///     R = Reduced
    ///     B = Streamlined refinance
    ///     C = No documentation
    /// 
    /// </summary>
    class TypeMap_sFannieDocT 
    {
        public static E_sFannieDocT ToLO(string value) 
        {
            switch (value) 
            {
                case "A": return E_sFannieDocT.Alternative;
                case "F": return E_sFannieDocT.Full;
                case "R": return E_sFannieDocT.Reduced;
                case "B": return E_sFannieDocT.StreamlinedRefinanced;
                case "C": return E_sFannieDocT.NoDocumentation;
                case "D": return E_sFannieDocT.NoRatio;
                case "E": return E_sFannieDocT.LimitedDocumentation;
                case "U": return E_sFannieDocT.NoIncomeNoEmploymentNoAssets;
                case "G": return E_sFannieDocT.NoIncomeNoAssets;
                case "H": return E_sFannieDocT.NoAssets;
                case "I": return E_sFannieDocT.NoIncomeNoEmployment;
                case "J": return E_sFannieDocT.NoIncome;
                case "K": return E_sFannieDocT.NoVerificationStatedIncomeEmploymentAssets;
                case "L": return E_sFannieDocT.NoVerificationStatedIncomeAssets;
                case "M": return E_sFannieDocT.NoVerificationStatedAssets;
                case "N": return E_sFannieDocT.NoVerificationStatedIncomeEmployment;
                case "O": return E_sFannieDocT.NoVerificationStatedIncome;
                case "P": return E_sFannieDocT.VerbalVOE;
                case "Q": return E_sFannieDocT.OnePaystub;
                case "S": return E_sFannieDocT.OnePaystubAndVerbalVOE;
                case "T": return E_sFannieDocT.OnePaystubOneW2VerbalVOE;
                default:
                    return E_sFannieDocT.LeaveBlank;
            }
        }
        public static string ToFannie(E_sFannieDocT value) 
        {
            switch (value) 
            {
                case E_sFannieDocT.Alternative: return "A";
                case E_sFannieDocT.Full: return "F";
                case E_sFannieDocT.Reduced: return "R";
                case E_sFannieDocT.StreamlinedRefinanced: return "B";
                case E_sFannieDocT.NoDocumentation: return "C";
                case E_sFannieDocT.NoRatio: return "D";
                case E_sFannieDocT.LimitedDocumentation: return "E";
                case E_sFannieDocT.NoIncomeNoEmploymentNoAssets: return "U";
                case E_sFannieDocT.NoIncomeNoAssets: return "G";
                case E_sFannieDocT.NoAssets: return "H";
                case E_sFannieDocT.NoIncomeNoEmployment: return "I";
                case E_sFannieDocT.NoIncome: return "J";
                case E_sFannieDocT.NoVerificationStatedIncomeEmploymentAssets: return "K";
                case E_sFannieDocT.NoVerificationStatedIncomeAssets: return "L";
                case E_sFannieDocT.NoVerificationStatedAssets: return "M";
                case E_sFannieDocT.NoVerificationStatedIncomeEmployment: return "N";
                case E_sFannieDocT.NoVerificationStatedIncome: return "O";
                case E_sFannieDocT.VerbalVOE: return "P";
                case E_sFannieDocT.OnePaystub: return "Q";
                case E_sFannieDocT.OnePaystubAndVerbalVOE: return "S";
                case E_sFannieDocT.OnePaystubOneW2VerbalVOE: return "T";
                default:
                    return "";
            }
        }
    }

    /// <summary>
    /// Property Type Code
    /// 
    ///     01 = Detached
    ///     02 = Attached
    ///     03 = Condominium
    ///     04 = Planned Urban Development (PUD)
    ///     05 = Co-Operative (Co-Op)
    ///     07 = High Rise Condo
    ///     08 = Manufactured Housing
    ///     09 = Detached Condo (New in 3.2)
    ///     10 = Manufactured home/condo/pud/coop (New in 3.2)
    /// </summary>
    class TypeMap_sFannieSpT 
    {
        public static E_sFannieSpT ToLO(string value) 
        {
            switch (value) 
            {
                case "01": return E_sFannieSpT.Detached;
                case "02": return E_sFannieSpT.Attached;
                case "03": return E_sFannieSpT.Condo;
                case "04": return E_sFannieSpT.PUD;
                case "05": return E_sFannieSpT.CoOp;
                case "07": return E_sFannieSpT.HighRiseCondo;
                case "08": return E_sFannieSpT.Manufactured;
                case "09": return E_sFannieSpT.DetachedCondo;
                case "10": return E_sFannieSpT.ManufacturedCondoPudCoop;
                default:
                    return E_sFannieSpT.LeaveBlank;
            }
        }

        public static string ToFannie(E_sFannieSpT value) 
        {
            switch (value) 
            {
                case E_sFannieSpT.Detached: return "01";
                case E_sFannieSpT.Attached: return "02";
                case E_sFannieSpT.Condo: return "03";
                case E_sFannieSpT.PUD: return "04";
                case E_sFannieSpT.CoOp: return "05";
                case E_sFannieSpT.HighRiseCondo: return "07";
                case E_sFannieSpT.Manufactured: return "08";
                case E_sFannieSpT.DetachedCondo: return "09";
                case E_sFannieSpT.ManufacturedCondoPudCoop: return "10";
                default:
                    return "";
            }
        }
    }


    class TypeMap_sSpProjectClassFannieT
    {
        public static E_sSpProjectClassFannieT ToLO(string value)
        {
            switch (value)
            {
                case "09": return  E_sSpProjectClassFannieT.PLimitedReviewNew;
                case "10": return E_sSpProjectClassFannieT.QLimitedReviewEst;
                case "11": return E_sSpProjectClassFannieT.RExpeditedReviewNew;
                case "12": return E_sSpProjectClassFannieT.SExpeditedReviewEst;
                case "13": return E_sSpProjectClassFannieT.TFannieReview;
                case "14": return E_sSpProjectClassFannieT.UFhaApproved;
                case "15": return E_sSpProjectClassFannieT.VRefiPlus;
                case "04": return E_sSpProjectClassFannieT.EPud;
                case "05": return E_sSpProjectClassFannieT.FPud;
                case "17": return E_sSpProjectClassFannieT.TPud;
                case "07": return E_sSpProjectClassFannieT._1Coop;
                case "08": return E_sSpProjectClassFannieT._2Coop;
                case "18": return E_sSpProjectClassFannieT.TCoop;
                default:
                    return E_sSpProjectClassFannieT.LeaveBlank;
            }
        }
        public static string ToFannie(E_sSpProjectClassFannieT value)
        {
            switch (value)
            {
                case E_sSpProjectClassFannieT.GNotInProject:
                case E_sSpProjectClassFannieT.LeaveBlank: return "16";
                case E_sSpProjectClassFannieT.PLimitedReviewNew: return "09";
                case E_sSpProjectClassFannieT.QLimitedReviewEst: return "10";
                case E_sSpProjectClassFannieT.RExpeditedReviewNew: return "11";
                case E_sSpProjectClassFannieT.SExpeditedReviewEst: return "12";
                case E_sSpProjectClassFannieT.TFannieReview: return "13";
                case E_sSpProjectClassFannieT.UFhaApproved: return "14";
                case E_sSpProjectClassFannieT.VRefiPlus: return "15";
                case E_sSpProjectClassFannieT.EPud: return "04";
                case E_sSpProjectClassFannieT.FPud: return "05";
                case E_sSpProjectClassFannieT.TPud: return "17";
                case E_sSpProjectClassFannieT._1Coop: return "07";
                case E_sSpProjectClassFannieT._2Coop: return "08";
                case E_sSpProjectClassFannieT.TCoop: return "18";
                default:
                    throw new UnhandledEnumException(value);
            }
        }
    }

    /// <summary>
    /// Index Type
    ///     0 = Weekly Average CMT
    ///     1 = Monthly Average CMT
    ///     2 = Weekly Average TAAI
    ///     3 = Weekly Average TAABD
    ///     4 = Weekly Average SMTI
    ///     5 = Daily CD Rate
    ///     6 = Weekly Average CD Rate
    ///     7 = Weekly Ave Prime Rate
    ///     8 = T-Bill Daily Value
    ///     9 = 11th District COF
    ///     10 = National Monthly Median Cost of Funds
    ///     11 = Wall Street Journal LIBOR
    ///     12 = Fannie Mae LIBOR
    ///     13 = Freddie Mac LIBOR
    ///     14 = National Average Contract Rate (FHLBB)
    ///     15 = Federal Cost of Funds
    ///     16 = Fannie Mae 60-Day Required Net Yield
    ///     17 = Freddie Mac 60-Day Required Net Yield
    /// </summary>
    class TypeMap_sArmIndexT 
    {
        public static E_sArmIndexT ToLO(string value) 
        {
            switch (value) 
            {
                case "0": return E_sArmIndexT.WeeklyAvgCMT;
                case "1": return E_sArmIndexT.MonthlyAvgCMT;
                case "2": return E_sArmIndexT.WeeklyAvgTAAI;
                case "3": return E_sArmIndexT.WeeklyAvgTAABD;
                case "4": return E_sArmIndexT.WeeklyAvgSMTI;
                case "5": return E_sArmIndexT.DailyCDRate;
                case "6": return E_sArmIndexT.WeeklyAvgCDRate;
                case "7": return E_sArmIndexT.WeeklyAvgPrimeRate;
                case "8": return E_sArmIndexT.TBillDailyValue;
                case "9": return E_sArmIndexT.EleventhDistrictCOF;
                case "10": return E_sArmIndexT.NationalMonthlyMedianCostOfFunds;
                case "11": return E_sArmIndexT.WallStreetJournalLIBOR;
                case "12": return E_sArmIndexT.FannieMaeLIBOR;
                default:
                    return E_sArmIndexT.LeaveBlank;
            }
        }
    }

    /// <summary>
    /// Maps FannieHomebuyerEducation type.
    /// </summary>
    class TypeMap_sFannieHomebuyerEducation
    {
        public static E_FannieHomebuyerEducation ToLO(string value)
        {
            switch (value)
            {
                case "1": return E_FannieHomebuyerEducation.EducationComplete;
                case "2": return E_FannieHomebuyerEducation.OneOnOneCounselingComplete;
                default:
                    return E_FannieHomebuyerEducation.LeaveBlank;
            }
        }
    }

    /// <summary>
    /// 102 = No appraisal/inspection obtained.
    /// 103 = DU Form 2075 - Desktop Underwriter Property Inspection Report
    /// 104 = FNM 2055 / FRE 2055 - Exterior-Only Inspection Residential Appraisal Report
    /// 105 = Form 2055 appraisal with interior/exterior
    /// 107 = Form 2065 appraisal with exterior only
    /// 109 = Form 2095 appraisal with interior/exterior
    /// 110 = FNM 2095 - Exterior-Only Individual Cooperative Interest Appraisal Report
    /// 114 = FNM 1025 / FRE 72 - Small Residential Income Property Appraisal Report
    /// 116 = FNM 1004 / FRE 70 - Uniform Residential Appraisal Report
    /// 120 = Prior appraisal used for the transaction
    /// 125 = Other.
    /// 125 = LP Form 2070 - Loan Prospector Condition and Marketability Report
    /// 130 = Form 26-1805, Certificate of Reasonable Value for VA
    /// 131 = Form 26-8712, Manufactured Home Appraisal Report
    /// 132 = FNM 1004C / FRE 70B - Manufactured Home Appraisal Report
    /// 133 = FNM 1073 / FRE 465 - Individual Condominium Unit Appraisal Report
    /// 134 = FNM 1075 / FRE 466 - Exterior-Only Inspection Individual Condominium Unit Appraisal Report
    /// 135 = FNM 2090 - Individual Cooperative Interest Appraisal Report
    /// 136 = FNM 1004D / FRE 442 - Appraisal Update and/or Completion Report
    /// 137 = FNM 2000 / FRE 1032 - One-Unit Residential Appraisal Field Review Report
    /// 138 = FNM 2000A / FRE 1072 - Two- to Four-Unit Residential Appraisal
    /// </summary>
    class TypeMap_sSpAppraisalFormT
    {
        private static Dictionary<E_sSpAppraisalFormT, string> appraisalFieldworkOrderedCodes = new Dictionary<E_sSpAppraisalFormT, string>
            {
                { E_sSpAppraisalFormT.Blank, null },
                { E_sSpAppraisalFormT.UniformResidentialAppraisalReport, "116" },
                { E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport, "132" },
                { E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport, "136" },
                { E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport, "114" },
                { E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport, "133" },
                { E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport, "134" },
                { E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport, "137" },
                { E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal, "138" },
                { E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport, "104" },
                { E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport, "135" },
                { E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport, "110" },
                { E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport, "103" },
                { E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability, "125" },
                { E_sSpAppraisalFormT.Other, "125" },
            };

        public static string ToFannie(E_sSpAppraisalFormT appraisalFormType)
        {
            string appraisalFieldworkCode = null;
            appraisalFieldworkOrderedCodes.TryGetValue(appraisalFormType, out appraisalFieldworkCode);
            return appraisalFieldworkCode;
        }
    }
}
