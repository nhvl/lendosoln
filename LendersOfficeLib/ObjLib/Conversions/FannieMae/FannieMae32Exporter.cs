namespace LendersOffice.Conversions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Conversions.MORNETPlus;
    using LendersOffice.Conversions.MORNETPlus32;

    public class FannieMae32Exporter
    {
        private CPageData m_dataLoan;
        private ArrayList m_warningMessages = new ArrayList();
        private List<string> m_errorMessages = new List<string>();

        private bool m_isDUDOExport = false;

        public IEnumerable<string> ErrorMessages
        {
            get { return m_errorMessages; }
        }
        public bool IsDUDOExport
        {
            get { return m_isDUDOExport; }
            set { m_isDUDOExport = value; }
        }

        public string sFannieInstitutionId { get; set; }
        //private string m_sFannieInstitutionId = ""; // 1/22/2009 dd - This field should store in LOAN_FILE table.
        //public string sFannieInstitutionId
        //{
        //    get { return m_sFannieInstitutionId; }
        //    set { m_sFannieInstitutionId = value; }
        //}

        public ArrayList WarningMessages 
        {
            get { return m_warningMessages; }
        }

        public FannieMae32Exporter(Guid sLId)
        {
            //m_dataLoan = new CFannieMae32ExporterData(sLId);
            m_dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMae32Exporter));
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlus);

        }

        public string OutputFileName
        {
            get 
            {
                CAppData primaryApp = m_dataLoan.GetAppData( 0 );
                return primaryApp.aBFirstNm + "_" + primaryApp.aBLastNm + ".fnm";
            }
        }
        public byte[] Export() 
        {
            return System.Text.UTF8Encoding.UTF8.GetBytes( ExportToString() );
        }


        public string ExportToString()
        {
            MORNETPlus32Document doc = new MORNETPlus32Document();
            doc.Initialize();
            doc.sDuCaseId = m_dataLoan.sDuCaseId;

            // 10/30/2013 dd - If FannieMae institution ID is set explicit then use that,
            // otherwise use institution id in loan file.
            if (string.IsNullOrEmpty(sFannieInstitutionId) == false)
            {
                doc.sFannieInstitutionId = sFannieInstitutionId;
            }
            else
            {
                doc.sFannieInstitutionId = m_dataLoan.sDuLenderInstitutionId; 
            }
            
            LoadLoan1003(doc._1003);

            LoadAdditionalCaseData(doc.AdditionalCaseData);

            LoadLoanProductData(doc.LoanProductData);

            LoadGovernmentData(doc.GovernmentLoanData);
            
            LoadCommunityLendingData(doc.CommunityLendingData);

//            DebugMORNETPlus(doc);
            return Export(doc);
            
        }

        private string Export(MORNETPlus32Document doc) 
        {
            StringBuilder sb = new StringBuilder();

            ExportSegment(sb, doc.EnvelopeHeader);
            ExportSegment(sb, doc.TransactionHeader);
            ExportSegment(sb, doc.TransactionProcessingInfo);
            
            ExportSegment(sb, doc._1003Header);
            foreach (MORNETPlusSegment segment in doc._1003.SegmentList)
                ExportSegment(sb, segment);

            ExportSegment(sb, doc.AdditionalCaseDataHeader);
            foreach (MORNETPlusSegment segment in doc.AdditionalCaseData.SegmentList)
                ExportSegment(sb, segment);

            ExportSegment(sb, doc.LoanProductDataHeader);
            foreach (MORNETPlusSegment segment in doc.LoanProductData.SegmentList)
                ExportSegment(sb, segment);

            if (doc.GovernmentLoanData.SegmentList.Count > 0) 
            {
                ExportSegment(sb, doc.GovernmentLoanDataHeader);
                foreach (MORNETPlusSegment segment in doc.GovernmentLoanData.SegmentList)
                    ExportSegment(sb, segment);
            }

            if (doc.CommunityLendingData.SegmentList.Count > 0) 
            {
                ExportSegment(sb, doc.CommunityLendingDataHeader);
                foreach (MORNETPlusSegment segment in doc.CommunityLendingData.SegmentList)
                    ExportSegment(sb, segment);
            }

            ExportSegment(sb, doc.TransactionTrailer);
            ExportSegment(sb, doc.EnvelopeTrailer);
            BuildWarningMessages(doc);
            return sb.ToString();
            //return System.Text.UTF8Encoding.UTF8.GetBytes(sb.ToString());

        }
        private void ExportSegment(StringBuilder sb, MORNETPlusSegment segment) 
        {
            sb.Append(segment).Append("\r\n");
        }

        private void LoadLoan1003(MORNETPlusDataset ds) 
        {
            MORNETPlusSegment segment = null;
            int nApps = m_dataLoan.nApps;
            
            CAppData primaryDataApp = m_dataLoan.GetAppData(0); 

            #region 00A - Top of Form (S)
            // 00A-020 The income or assets of a person other than the Borrower (including the Applicant's spouse)
            // 00A-030 The income or assets of the Applicant's spouse will not be used as a basic for ...
            segment = ds.CreateSegment("00A");
            ds.Add(segment);
            segment["00A-020"] = m_dataLoan.sMultiApps ? "Y" : "N";
            segment["00A-030"] = primaryDataApp.aSpouseIExcl ? "Y" : "N";

            #endregion

            #region 01A - Mortgage Type and Terms (S)
            // 01A-020 Mortgage Applied For
            // 01A-030 Mortgage Applied For (Other)
            // 01A-040 Agency Case Number
            // 01A-050 Case Number. A number assigned to the casefile by the lender or originator. The system will assign this number if not specified.
            // 01A-060 Loan Amount
            // 01A-070 Interest Rate
            // 01A-080 No. of months
            // 01A-090 Amortization Type
            // 01A-100 Amortization Type Other Explaination
            // 01A-110 ARM Textual Description
            segment = ds.CreateSegment("01A");
            ds.Add(segment);

            segment["01A-020"] = TypeMap_sLT.ToFannie(m_dataLoan.sLT);
            segment["01A-030"] = m_dataLoan.sLTODesc;
            segment["01A-040"] = m_dataLoan.sAgencyCaseNum; 
            segment["01A-050"] = m_dataLoan.sLenderCaseNum; 
            segment["01A-060"] = m_dataLoan.sLAmtCalc_rep; 
            segment["01A-070"] = m_dataLoan.sNoteIR_rep; 
            segment["01A-080"] = m_dataLoan.sTerm_rep; 
            if (m_dataLoan.sFinMethodPrintAsOther) 
            {
                segment["01A-090"] = "13"; // Amortization Type Other
            } 
            else 
            {
                segment["01A-090"] = TypeMap_sFinMethT.ToFannie(m_dataLoan.sFinMethT); 
            }
            segment["01A-100"] = m_dataLoan.sFinMethPrintAsOtherDesc; // Amortization Type Other Explanation
            segment["01A-110"] = m_dataLoan.sFinMethT == E_sFinMethT.ARM ? m_dataLoan.sFinMethDesc : ""; 

            #endregion

            #region 02A - Property Information (S)
            // 02A-020 Property Street Address
            // 02A-030 Property City
            // 02A-040 Property State
            // 02A-050 Property Zip Code
            // 02A-060 Property Zip Code + 4
            // 02A-070 No. of Units
            // 02A-080 Legal Description of Subject Property Code
            // 02A-090 Legal Description of Subject Property Text
            // 02A-100 Year Built
            segment = ds.CreateSegment("02A");
            ds.Add(segment);
            segment["02A-020"] = m_dataLoan.sSpAddr; 
            segment["02A-030"] = m_dataLoan.sSpCity; 
            segment["02A-040"] = m_dataLoan.sSpState; 
            segment["02A-050"] = m_dataLoan.sSpZip; 
            segment["02A-060"] = ""; // 6/7/2004 dd - Zipcode + 4. We don't have this
            segment["02A-070"] = m_dataLoan.sUnitsNum_rep; 
            segment["02A-080"] = "F1"; // 6/7/2004 dd - Legal Description of Subject Property. F1 = Other
            segment["02A-090"] = m_dataLoan.sSpLegalDesc; 
            segment["02A-100"] = m_dataLoan.sYrBuilt; 

            #endregion

            #region PAI - Property Address Information. Segment is optional. If provided it will be used instead of the single address element (S)
            // PAI-020 House Number
            // PAI-030 Street Name
            // PAI-040 Unit Number
            // 6/7/2004 dd - SKIP This section. 
            // This segment is optional. If provide it will be used instead of the single address element (02A-020).
            // Since Point does not export this, I will skip this for now.
            #endregion

            #region 02B - Purpose of Loan (S)
            // 02B-020 FannieMae reserved for future use.
            // 02B-030 Purpose of Loan
            // 02B-040 Purpose of Loan (Other). Text identifying the reason loan is needed if EDI code is "other"
            // 02B-050 Property will be
            // 02B-060 Manner in which title will be held
            // 02B-070 Estate will be held in
            // 02B-080 (Estate will be held in) Leasehold expiration date
            segment = ds.CreateSegment("02B");
            ds.Add(segment);

            segment["02B-030"] = TypeMap_sLPurposeT.ToFannie(m_dataLoan.sLPurposeT, m_dataLoan.sConstructionPurposeT, this.IsDUDOExport); 
            segment["02B-040"] = m_dataLoan.sLPurposeT == E_sLPurposeT.Other ? m_dataLoan.sOLPurposeDesc : ""; 
            segment["02B-050"] = TypeMap_aOccT.ToFannie(primaryDataApp.aOccT); 
            segment["02B-060"] = primaryDataApp.aManner; 
            segment["02B-070"] = TypeMap_sEstateHeldT.ToFannie(m_dataLoan.sEstateHeldT); 
            segment["02B-080"] = m_dataLoan.sEstateHeldT == E_sEstateHeldT.LeaseHold ? m_dataLoan.sLeaseHoldExpireD_rep : ""; 

            #endregion

            #region 02C - Title Holder (M)
            // 02C-020 Titleholder Name
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                for (int j = 0; j < 2; j++) 
                {
                    string s = (0 == j) ? dataApp.aTitleNm1 : dataApp.aTitleNm2;
                    if (s.TrimWhitespaceAndBOM().Length > 0) 
                    {
                        segment = ds.CreateSegment("02C");
                        ds.Add(segment);

                        segment["02C-020"] = s;
                    }
                }
            }
            #endregion

            #region 02D - Construction to Permanent or Refinance Data (S)
            // 02D-020 Year Lot Acquired (Construction) or Year Acquired (Refinance)
            // 02D-030 Original Cost (Construction or Refinance)
            // 020-040 Amount of Existing Liens (Construction or Refinance)
            // 020-050 (a) Present Value of Lot
            // 020-060 (b) Cost of Improvements
            // 020-070 Purpose of Refinance
            // 020-080 Describe Improvements
            // 020-090 (Describe Improvements) made/to be made
            // 020-100 (Describe Improvements) Cost
            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Construct || m_dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm) 
            {
                segment = ds.CreateSegment("02D");
                ds.Add(segment);

                segment["02D-020"] = m_dataLoan.sLotAcqYr; 
                segment["02D-030"] = m_dataLoan.sLotOrigC_rep; 
                segment["02D-040"] = m_dataLoan.sLotLien_rep; 
                segment["02D-050"] = m_dataLoan.sPresentValOfLot_rep; 
                segment["02D-060"] = m_dataLoan.sLotImprovC_rep; 
            } 
            else if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl) 
            {
                segment = ds.CreateSegment("02D");
                ds.Add(segment);

                string code = "";
                segment["02D-020"] = m_dataLoan.sSpAcqYr; 
                segment["02D-030"] = m_dataLoan.sSpOrigC_rep; 
                segment["02D-040"] = m_dataLoan.sSpLien_rep; 
                code = TypeMap_sRefPurpose.ToFannie(m_dataLoan.sRefPurpose);
                segment["02D-070"] = code; 
                segment["02D-080"] = code == "04" ? m_dataLoan.sSpImprovDesc : ""; 
                segment["02D-090"] = TypeMap_sSpImprovTimeFrameT.ToFannie(m_dataLoan.sSpImprovTimeFrameT); 
                segment["02D-100"] = code == "04" ? m_dataLoan.sSpImprovC_rep : ""; 

            }

            #endregion

            #region 02E - Down Payment (M)


            if (IsDUDOExport && m_dataLoan.sIsUseDUDwnPmtSrc)
            {
                foreach (var downpayment in m_dataLoan.sDUDwnPmtSrc)
                {
                    if (downpayment.Amount > 0)
                    {
                        segment = ds.CreateSegment("02E");
                        ds.Add(segment);

                        segment["02E-020"] = TypeMap_sDwnPmtSrc.ToFannie(downpayment.Source);
                        segment["02E-030"] = downpayment.Amount_rep;
                        segment["02E-040"] = downpayment.Explanation;
                    }
                }
            }
            else
            {
                // 02E-020 Down Payment Type Code
                // 02E-030 Down Payment Amount
                // 02E-040 Down Payment Explanation
                if (m_dataLoan.sEquityCalc_rep.Length > 0)
                {
                    segment = ds.CreateSegment("02E");
                    ds.Add(segment);

                    segment["02E-020"] = TypeMap_sDwnPmtSrc.ToFannie(m_dataLoan.sDwnPmtSrc);
                    segment["02E-030"] = m_dataLoan.sEquityCalc_rep;
                    segment["02E-040"] = m_dataLoan.sDwnPmtSrcExplain;
                }
            }
            
            #endregion

            #region 03A - Applicant(s) Data (SA)
            // 03A-020 Applicant/Co-Applicant Indicator
            // 03A-030 Applicant Social Security Number
            // 03A-040 Applicant First Name
            // 03A-050 Applicant Middle Name
            // 03A-060 Applicant Last Name
            // 03A-070 Applicant Generation
            // 03A-080 Home Phone
            // 03A-090 Age
            // 03A-100 Yrs. School
            // 03A-110 Marital Status
            // 03A-120 Dependents (no.)
            // 03A-130 Completed Jointly/Not Jointly
            // 03A-140 Cross Reference Number
            // 03A-150 Date of Birth
            // 03A-160 Email Address
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                for (int j = 0; j < 2; j++) 
                {
                    bool isBorrower = j == 0;
                    dataApp.BorrowerModeT = isBorrower ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;

                    if ( IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature &&
                        ( dataApp.aTypeT == E_aTypeT.NonTitleSpouse || dataApp.aTypeT == E_aTypeT.TitleOnly ) )
                    {
                        // OPM 184017.  Ignore this non-purchasing spouse
                        continue;
                    }

                    if (dataApp.aSsn.Length > 0)
                    {
                        segment = ds.CreateSegment("03A");
                        ds.Add(segment);
                        segment["03A-020"] = isBorrower ? "BW" : "QZ"; 
                        segment["03A-030"] = dataApp.aSsn; //isBorrower ? dataApp.aBSsn : dataApp.aCSsn; 
                        segment["03A-040"] = dataApp.aFirstNm; //isBorrower ? dataApp.aBFirstNm : dataApp.aCFirstNm; 
                        segment["03A-050"] = dataApp.aMidNm; //isBorrower ? dataApp.aBMidNm : dataApp.aCMidNm; 
                        segment["03A-060"] = dataApp.aLastNm; //isBorrower ? dataApp.aBLastNm : dataApp.aCLastNm; 
                        segment["03A-070"] = dataApp.aSuffix; //isBorrower ? dataApp.aBSuffix : dataApp.aCSuffix; 
                        segment["03A-080"] = dataApp.aHPhone; //isBorrower ? dataApp.aBHPhone : dataApp.aCHPhone; 
                        segment["03A-090"] = dataApp.aAge_rep; //isBorrower ? dataApp.aBAge_rep : dataApp.aCAge_rep; 
                        segment["03A-100"] = dataApp.aSchoolYrs_rep; //isBorrower ? dataApp.aBSchoolYrs_rep : dataApp.aCSchoolYrs_rep; 
                        segment["03A-110"] = TypeMap_aMaritalStatT.ToFannie(dataApp.aMaritalStatT);
                        string dependants = dataApp.aDependNum_rep; ;
                        if (string.IsNullOrEmpty(dependants))
                        {
                            dependants = "0"; //66469 av 10/118/2012
                        }
                        segment["03A-120"] = dependants;
                        segment["03A-130"] = dataApp.aAsstLiaCompletedNotJointly ? "N" : "Y";

                        // OPM 184017. Segment 03A-140 (spouse's SSN) should not be set if spouse is Non-Purchasing.
                        bool spouseIsNonPurchase = (isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly))
                            || (!isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly));

                        segment["03A-140"] = spouseIsNonPurchase ? string.Empty : dataApp.aSpouseSsn; //isBorrower ? dataApp.aCSsn : dataApp.aBSsn; 
                        segment["03A-150"] = dataApp.aDob_rep;//isBorrower ? dataApp.aBDob_rep : dataApp.aCDob_rep; 
                        segment["03A-160"] = dataApp.aEmail; //isBorrower ? dataApp.aBEmail : dataApp.aCEmail; 
                    }
                }
            }
            #endregion

            #region 03B - Dependent's Age (M)
            // 03B-020 Applicant Social Security Number
            // 03B-030 Dependent's Age
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                for (int j = 0; j < 2; j++) 
                {
                    bool isBorrower = j == 0;

                    if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                    {
                        if ((isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                            || (!isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))

                            // OPM 184017.  Ignore this non-purchasing spouse
                            continue;
                    }

                    if ((isBorrower && dataApp.aBDependNum > 0) || (!isBorrower && dataApp.aCDependNum > 0)) 
                    {
                        string ssn = isBorrower ? dataApp.aBSsn : dataApp.aCSsn;

                        if (ssn.Length > 0) 
                        {
                            string s = isBorrower ? dataApp.aBDependAges : dataApp.aCDependAges;
                            string[] ages = s.Split(',', ';', ' ', '-', '&', '+', '~', ':' );
                            for (int k = 0; k < ages.Length; k++) 
                            {
                                string ageStr = ages[k];
                                if ("" == ageStr.TrimWhitespaceAndBOM())
                                    continue;

                                segment = ds.CreateSegment("03B");
                                ds.Add(segment);
                                segment["03B-020"] = ssn;
                                segment["03B-030"] = ageStr;
                            } // for (int k = 0; k < ages.Length; k++) 
                        } // if (ssn.Length > 0) 
                    } // if ((isBorrower && dataApp.aBDependNum > 0) || (!isBorrower && dataApp.aCDependNum > 0)) 
                } // for (int j = 0; j < 2; j++) 
            }
            #endregion

            #region 03C - Applicant(s) Address (M)
            // 03C-020 Applicant Social Security Number
            // 03C-030 Present/Former/Mailing Address
            // 03C-040 Residence Street Address
            // 03C-050 Residence City
            // 03C-060 Residence State
            // 03C-070 Residence Zip Code
            // 03C-080 Residence Zip Code + 4
            // 03C-090 Own/Rent/Living Rent Free
            // 03C-100 No. Yrs
            // 03C-110 No. Months
            // 03C-120 Country
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                YearMonthParser yearMonthParser = new YearMonthParser();
                for (int j = 0; j < 2; j++) 
                {
                    bool isBorrower = j == 0;
                    dataApp.BorrowerModeT = isBorrower ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;

                    if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature
                        && (dataApp.aTypeT == E_aTypeT.NonTitleSpouse || dataApp.aTypeT == E_aTypeT.TitleOnly))
                    {
                        // OPM 184017.  Ignore this non-purchasing spouse
                        continue;
                    }

                    string ssn = dataApp.aSsn; // isBorrower ? dataApp.aBSsn : dataApp.aCSsn;
                    if (ssn.Length > 0) 
                    {
                        // Handle Present Address
                        if (dataApp.aAddr != "") //((isBorrower && dataApp.aBAddr != "") || (!isBorrower && dataApp.aCAddr != "")) 
                        {
                            segment = ds.CreateSegment("03C");
                            ds.Add(segment);

                            segment["03C-020"] = ssn;
                            segment["03C-030"] = "ZG"; // Present address
                            segment["03C-040"] = dataApp.aAddr; //isBorrower ? dataApp.aBAddr : dataApp.aCAddr;
                            segment["03C-050"] = dataApp.aCity; //isBorrower ? dataApp.aBCity : dataApp.aCCity;
                            segment["03C-060"] = dataApp.aState; //isBorrower ? dataApp.aBState : dataApp.aCState;
                            segment["03C-070"] = dataApp.aZip; //isBorrower ? dataApp.aBZip : dataApp.aCZip;
                            // Skip 03C-080. Zipcode + 4
                            segment["03C-090"] = TypeMap_aAddrT.ToFannie(dataApp.aAddrT); //isBorrower ? dataApp.aBAddrT : (E_aBAddrT) dataApp.aCAddrT);
                            
                            yearMonthParser.Parse(dataApp.aAddrYrs); //isBorrower ? dataApp.aBAddrYrs : dataApp.aCAddrYrs);
                            segment["03C-100"] = yearMonthParser.NumberOfYears.ToString();
                            segment["03C-110"] = yearMonthParser.NumberOfMonths.ToString();
                        }

                        // Handle Mailing Address.
                        //if ((isBorrower && !dataApp.aBAddrMailUsePresentAddr && dataApp.aBAddrMail != "") ||
                        //    (!isBorrower && !dataApp.aCAddrMailUsePresentAddr && dataApp.aCAddrMail != "")) 
                        if (!dataApp.aAddrMailUsePresentAddr && dataApp.aAddrMail != "")
                        {
                            segment = ds.CreateSegment("03C");
                            ds.Add(segment);

                            segment["03C-020"] = ssn;
                            segment["03C-030"] = "BH"; // Mailing address. Only if different than present address
                            segment["03C-040"] = dataApp.aAddrMail; //isBorrower ? dataApp.aBAddrMail : dataApp.aCAddrMail;
                            segment["03C-050"] = dataApp.aCityMail; //isBorrower ? dataApp.aBCityMail : dataApp.aCCityMail;
                            segment["03C-060"] = dataApp.aStateMail; //isBorrower ? dataApp.aBStateMail : dataApp.aCStateMail;
                            segment["03C-070"] = dataApp.aZipMail; //isBorrower ? dataApp.aBZipMail : dataApp.aCZipMail;
                            // Skip 03C-080. Zipcode + 4
                            // Skip 03C-090. Mailing address does not have status of Own/Rent/Living Rent Free
                            // Skip 03C-100. Skip No Yrs.
                            // Skip 03C-110. SKip No Months.
                        }

                        // Handle Previous Address
                        //if ((isBorrower && dataApp.aBPrev1Addr != "") || (!isBorrower && dataApp.aCPrev1Addr != "")) 
                        if (dataApp.aPrev1Addr != "")
                        {
                            segment = ds.CreateSegment("03C");
                            ds.Add(segment);

                            segment["03C-020"] = ssn;
                            segment["03C-030"] = "F4"; // Previous
                            segment["03C-040"] = dataApp.aPrev1Addr; //isBorrower ? dataApp.aBPrev1Addr : dataApp.aCPrev1Addr;
                            segment["03C-050"] = dataApp.aPrev1City; //isBorrower ? dataApp.aBPrev1City : dataApp.aCPrev1City;
                            segment["03C-060"] = dataApp.aPrev1State; //isBorrower ? dataApp.aBPrev1State : dataApp.aCPrev1State;
                            segment["03C-070"] = dataApp.aPrev1Zip; //isBorrower ? dataApp.aBPrev1Zip : dataApp.aCPrev1Zip;
                            // Skip 03C-080. Zipcode + 4
                            segment["03C-090"] = TypeMap_aAddrT.ToFannie((E_aBAddrT) dataApp.aPrev1AddrT); //isBorrower ? (E_aBAddrT) dataApp.aBPrev1AddrT : (E_aBAddrT) dataApp.aCPrev1AddrT);
                            
                            yearMonthParser.Parse(dataApp.aPrev1AddrYrs); //isBorrower ? dataApp.aBPrev1AddrYrs : dataApp.aCPrev1AddrYrs);
                            segment["03C-100"] = yearMonthParser.NumberOfYears.ToString();
                            segment["03C-110"] = yearMonthParser.NumberOfMonths.ToString();
                        }

                        // Handle Previous Address
                        //if ((isBorrower && dataApp.aBPrev2Addr != "") || (!isBorrower && dataApp.aCPrev2Addr != "")) 
                        if (dataApp.aPrev2Addr != "")
                        {
                            segment = ds.CreateSegment("03C");
                            ds.Add(segment);

                            segment["03C-020"] = ssn;
                            segment["03C-030"] = "F4"; // Previous
                            segment["03C-040"] = dataApp.aPrev2Addr; //isBorrower ? dataApp.aBPrev2Addr : dataApp.aCPrev2Addr;
                            segment["03C-050"] = dataApp.aPrev2City; //isBorrower ? dataApp.aBPrev2City : dataApp.aCPrev2City;
                            segment["03C-060"] = dataApp.aPrev2State; //isBorrower ? dataApp.aBPrev2State : dataApp.aCPrev2State;
                            segment["03C-070"] = dataApp.aPrev2Zip; //isBorrower ? dataApp.aBPrev2Zip : dataApp.aCPrev2Zip;
                            // Skip 03C-080. Zipcode + 4
                            segment["03C-090"] = TypeMap_aAddrT.ToFannie((E_aBAddrT) dataApp.aPrev2AddrT); //isBorrower ? (E_aBAddrT) dataApp.aBPrev2AddrT : (E_aBAddrT) dataApp.aCPrev2AddrT);
                            
                            yearMonthParser.Parse(dataApp.aPrev2AddrYrs); //isBorrower ? dataApp.aBPrev2AddrYrs : dataApp.aCPrev2AddrYrs);
                            segment["03C-100"] = yearMonthParser.NumberOfYears.ToString();
                            segment["03C-110"] = yearMonthParser.NumberOfMonths.ToString();
                        }
                    }

                }
            }
            #endregion

            #region 04A - Primary Current Employer(s)  (SA)
            // 04A-020 Applicant Social Security Number
            // 04A-030 Employer Name
            // 04A-040 Employer Street Address
            // 04A-050 Employer City
            // 04A-060 Employer State
            // 04A-070 Employer Zip Code
            // 04A-080 Employer Zip Code Plus Four
            // 04A-090 Self Employed
            // 04A-100 Yrs. on this job
            // 04A-110 Months on this job
            // 04A-120 Yrs employed in this line of work/profession
            // 04A-130 Position / Title / Type of Business
            // 04A-140 Business Phone
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                for (int j = 0; j < 2; j++) 
                {
                    bool isBorrower = j == 0;
                    string ssn = isBorrower ? dataApp.aBSsn : dataApp.aCSsn;

                    if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                    {
                        if ((isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                            || (!isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))

                            // OPM 184017.  Ignore this non-purchasing spouse
                            continue;
                    }

                    if (ssn == "") 
                        continue;

                    IPrimaryEmploymentRecord primaryEmployment = isBorrower ? dataApp.aBEmpCollection.GetPrimaryEmp(false) : dataApp.aCEmpCollection.GetPrimaryEmp(false);

                    if (null != primaryEmployment) 
                    {
                        if (primaryEmployment.EmplrNm.TrimWhitespaceAndBOM() != "") 
                        {
                            segment = ds.CreateSegment("04A");
                            ds.Add(segment);

                            segment["04A-020"] = ssn;
                            segment["04A-030"] = primaryEmployment.EmplrNm;
                            segment["04A-040"] = primaryEmployment.EmplrAddr;
                            segment["04A-050"] = primaryEmployment.EmplrCity;
                            segment["04A-060"] = primaryEmployment.EmplrState;
                            segment["04A-070"] = primaryEmployment.EmplrZip;
                            // SKIP 04A-080. Zipcode + 4
                            segment["04A-090"] = primaryEmployment.IsSelfEmplmt ? "Y" : "N";
                            segment["04A-100"] = primaryEmployment.EmplmtLenInYrs.ToString();
                            segment["04A-110"] = primaryEmployment.EmplmtLenInMonths.ToString();
                            segment["04A-120"] = Round(primaryEmployment.ProfLen).ToString();
                            segment["04A-130"] = primaryEmployment.JobTitle;
                            segment["04A-140"] = primaryEmployment.EmplrBusPhone;

                        }
                    }
                }
            }
            #endregion

            #region 04B - Secondary/Previous Employer(s) (M)
            // 04B-020 Applicant Social Secrity Number
            // 04B-030 Employer Name
            // 04B-040 Employer Street Address
            // 04B-050 Employer City
            // 04B-060 Employer State
            // 04B-070 Employer Zip Code
            // 04B-080 Employer Zip Code Plus Four
            // 04B-090 Self Employed
            // 04B-100 Current Employment Flag
            // 04B-110 From Date
            // 04B-120 To Date
            // 04B-130 Monthly Income
            // 04B-140 Position / Title / Type of Business
            // 04B-150 Business Phone
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                for (int j = 0; j < 2; j++) 
                {
                    bool isBorrower = j == 0;
                    string ssn = isBorrower ? dataApp.aBSsn :  dataApp.aCSsn;

                    if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                    {
                        if ((isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                            || (!isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))

                            // OPM 184017.  Ignore this non-purchasing spouse
                            continue;
                    }

                    if ("" != ssn) 
                    {
                        var subCollection = isBorrower ? dataApp.aBEmpCollection.GetSubcollection(true, E_EmpGroupT.Previous) :
                            dataApp.aCEmpCollection.GetSubcollection(true, E_EmpGroupT.Previous);

                        foreach (IRegularEmploymentRecord record in subCollection) 
                        {
                            segment = ds.CreateSegment("04B");
                            ds.Add(segment);

                            segment["04B-020"] = ssn;
                            segment["04B-030"] = record.EmplrNm;
                            segment["04B-040"] = record.EmplrAddr;
                            segment["04B-050"] = record.EmplrCity;
                            segment["04B-060"] = record.EmplrState;
                            segment["04B-070"] = record.EmplrZip;
                            // SKIP 04B-080. Zipcode + 4
                            segment["04B-090"] = record.IsSelfEmplmt ? "Y" : "N";

                            segment["04B-100"] = record.IsCurrent ? "Y": "N";
                            segment["04B-110"] = record.EmplmtStartD_rep;
                            segment["04B-120"] = record.EmplmtEndD_rep;
                            segment["04B-130"] = record.MonI_rep;
                            segment["04B-140"] = record.JobTitle;
                            segment["04B-150"] = record.EmplrBusPhone;
                        }
                    }
                }
            }
            #endregion

            #region 05H - Present/Proposed Housing Expense
            // 05H-020 Applicant Social Security Number
            // 05H-030 Present/Proposed Indicator
            // 05H-040 Housing Payment Type Code
            // 05H-050 Housing Payment Amount

            // 1071 Housing Payment Type Code (General Expense Qualifier)
            //     25 = Rent
            //     26 = First Mortgage P & I
            //     22 = Other Financing P & I
            //     01 = Hazard Insurance
            //     14 = Real Estate Taxes
            //     02 = Mortgage Insurance
            //     06 = Homeowner Association Dues
            //     23 = Other
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                string[][] housingExpense = {
                                                new string[] {"1", "25", dataApp.aPresRent_rep},
                                                new string[] {"1", "26", dataApp.aPres1stM_rep },
                                                new string[] {"1", "22", dataApp.aPresOFin_rep },
                                                new string[] {"1", "01", dataApp.aPresHazIns_rep },
                                                new string[] {"1", "14", dataApp.aPresRealETx_rep },
                                                new string[] {"1", "02", dataApp.aPresMIns_rep },
                                                new string[] {"1", "06", dataApp.aPresHoAssocDues_rep },
                                                new string[] {"1", "23", dataApp.aPresOHExp_rep },
                                                new string[] {"2", "26", m_dataLoan.sProFirstMPmt_rep },
                                                new string[] {"2", "22", m_dataLoan.sProSecondMPmt_rep },
                                                new string[] {"2", "01", m_dataLoan.sProHazIns_rep },
                                                new string[] {"2", "14", m_dataLoan.sProRealETx_rep },
                                                new string[] {"2", "02", m_dataLoan.sProMIns_rep },
                                                new string[] {"2", "06", m_dataLoan.sProHoAssocDues_rep },
                                                new string[] {"2", "23", m_dataLoan.sProOHExp_rep }
                                            };
                foreach (string[] parts in housingExpense) 
                {
                    if (parts[0] == "2" && i != 0)
                    {
                        // 2/6/2012 dd - For the proposed housing expense only export primary borrower.
                        continue;
                    }

                    if (parts[2] != "") 
                    {

                        segment = ds.CreateSegment("05H");
                        ds.Add(segment);
                        segment["05H-020"] = dataApp.aBSsn;
                        segment["05H-030"] = parts[0];
                        segment["05H-040"] = parts[1];
                        segment["05H-050"] = parts[2];
                    }
                }
            }
            #endregion

            #region 05I - Income
            // 05I-020 Applicant Social Security Number
            // 05I-030 Type Of Income Code
            // 05I-040 Income Amount

            // 1186 Type of Income Code
            // F1 = Military Base Pay
            // 07 = Military Rations Allowance
            // F2 = Military Flight Pay
            // F3 = Military Hazard Pay
            // 02 = Military Clothes Allowance
            // 04 = Military Quarters Allowance
            // 03 = Military Prop Pay
            // F4 = Military Overseas Pay
            // F5 = Military Combat Pay
            // F6 = Military Variable Housing Allowance
            // F7 = Alimony/Child Support Income
            // F8 = Notes Receivable/Installment
            // 41 = Pension/Retirement Income
            // 42 = Social Security /Disability Income
            // 30 = Real Estate, Mortgage Differential Income
            // F9 = Trust Income
            // AU = Accessory Unit Income
            // M1 = Unemployment/Welfare Income
            // M2 = Automobile/Expense Account Income
            // M3 = Foster Care
            // M4 = VA Benefits (Non-education)
            // NB = Non-Borrower Household Income
            // 45 = Other Type of Income, Other Income
            // 20 = Base Employment Income
            // 09 = Overtime
            // 08 = Bonuses
            // 10 = Commissions
            // 17 = Dividends/Interest
            // SI = Subject Property Net Cash Flow
            // S8 = Housing Choice Voucher (Sec 8)
            // 33 = Net Rental Income
            // BI = Boarder Income
            // MC = Mortgage Credit Certificate (MCC)
            // CG = Capital Gains
            // EA = Employment Related Assets
            // FI = Foreign Income
            // RP = Royalty Payment
            // SE = Seasonal Income
            // TL = Temporary Leave
            // TI = Tip Income

            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                // OPM 184017. Keep non-purchasing borrowers out of export
                bool nonPurchasingBorrower = false;
                bool nonPurchasingCoBorrower = false;
                if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                {
                    nonPurchasingBorrower = (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly);
                    nonPurchasingCoBorrower = (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly);
                }


                var list = new List<string[]>(20);
                if (!nonPurchasingBorrower)
                {
                    list.Add(new string[] { dataApp.aBSsn, "20", dataApp.aBBaseI_rep });
                    list.Add(new string[] { dataApp.aBSsn, "09", dataApp.aBOvertimeI_rep });
                    list.Add(new string[] { dataApp.aBSsn, "08", dataApp.aBBonusesI_rep });
                    list.Add(new string[] { dataApp.aBSsn, "10", dataApp.aBCommisionI_rep });
                    list.Add(new string[] { dataApp.aBSsn, "17", dataApp.aBDividendI_rep });
                    // 5/30/2011 dd - Subject Property Net Cashflow will be export from Other Income section.
                    //list.Add(new string[] { dataApp.aBSsn, "SI", dataApp.aIsPrimary ? m_dataLoan.sSpRentalIRaw_rep : "" } ); 
                    list.Add(new string[] { dataApp.aBSsn, "33", dataApp.aBNetRentI1003_rep });
                }

                if (!nonPurchasingCoBorrower)
                {
                    list.Add(new string[] { dataApp.aCSsn, "20", dataApp.aCBaseI_rep });
                    list.Add(new string[] { dataApp.aCSsn, "09", dataApp.aCOvertimeI_rep });
                    list.Add(new string[] { dataApp.aCSsn, "08", dataApp.aCBonusesI_rep });
                    list.Add(new string[] { dataApp.aCSsn, "10", dataApp.aCCommisionI_rep });
                    list.Add(new string[] { dataApp.aCSsn, "17", dataApp.aCDividendI_rep });
                    list.Add(new string[] { dataApp.aCSsn, "33", dataApp.aCNetRentI1003_rep });
                }

                // Add the other income items. "Subject Property Net Cash Flow" is a special case which must be added to aTotSpPosCf
                string subjectPropertyNetCashFlowSSN = dataApp.aBSsn;
                string subjectPropertyNetCashFlowCode = TypeMap_OtherIncomeDescription.ToFannie("Subject Property Net Cash Flow", m_dataLoan.sLT);
                var subjectPropertyNetIncome = new OtherIncome() { Amount = dataApp.aTotSpPosCf - dataApp.aSpNegCf, Desc = "Subject Property Net Cash Flow", IsForCoBorrower = false };
                foreach (var income in dataApp.aOtherIncomeList)
                {
                    if ((nonPurchasingBorrower && income.IsForCoBorrower == false)
                        || (nonPurchasingCoBorrower && income.IsForCoBorrower))
                        continue;

                    E_aOIDescT incomeDescription = OtherIncome.Get_aOIDescT(income.Desc);
                    if (incomeDescription == E_aOIDescT.SubjPropNetCashFlow) // 3/1/12 opm 73986 mp: Merge other income items with final subject property net cash flow
                    {
                        subjectPropertyNetIncome.Amount += income.Amount;
                    }
                    else if (incomeDescription == E_aOIDescT.MortgageCreditCertificate
                        && (this.m_dataLoan.sLT.EqualsOneOf(E_sLT.FHA, E_sLT.VA) && this.m_dataLoan.sMortgageCreditCertificateTotal != 0M))
                    {
                        continue; // 463697: MCC is composed of many other income values, so skip 05I-030 when we already have an MCC value in GOA-030
                    }
                    else
                    {
                        string ssn = income.IsForCoBorrower ? dataApp.aCSsn : dataApp.aBSsn;
                        string code = TypeMap_OtherIncomeDescription.ToFannie(income.Desc, m_dataLoan.sLT);
                        list.Add(new string[] { ssn, code, dataApp.m_convertLos.ToMoneyString(income.Amount, FormatDirection.ToRep) });
                    }
                }

                if (E_aOccT.Investment == dataApp.aOccT || m_dataLoan.sUnitsNum > 1)
                {
                    if (subjectPropertyNetIncome.Amount != 0)
                    {
                        list.Add(new string[] { subjectPropertyNetCashFlowSSN, subjectPropertyNetCashFlowCode, dataApp.m_convertLos.ToMoneyString(subjectPropertyNetIncome.Amount, FormatDirection.ToRep) });
                    }
                }

                // opm 132129 - sum all incomes of a given type and ssn.
                var incomeByDistinctSSNAndCode = 
                     from parts in list
                     where parts[0] != ""   // ssn is not empty
                     && parts[2] != ""      // income is not empty.
                     group parts by new { SSN = parts[0], Code = parts[1] } into g
                     select new
                     {
                         g.Key.SSN,
                         g.Key.Code,
                         Income = g.Sum(parts => dataApp.m_convertLos.ToMoney(parts[2]))
                     };

                foreach (var parts in incomeByDistinctSSNAndCode)
                {
                    segment = ds.CreateSegment("05I");
                    ds.Add(segment);

                    segment["05I-020"] = parts.SSN;
                    segment["05I-030"] = parts.Code;
                    segment["05I-040"] = dataApp.m_convertLos.ToMoneyString(parts.Income, FormatDirection.ToRep);
                }
            }
            #endregion

            #region 06B - Life Insurance
            // 06B-020 Applicant Social Security Number
            // 06B-030 Acct. No.
            // 06B-040 Life Insurance Cash or Market Value
            // 06B-050 Life insurance Face Amount
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature
                    && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                {
                    // OPM 184017.  Ignore this non-purchasing spouse
                    continue;
                }
                var record = dataApp.aAssetCollection.GetLifeInsurance(false);
                if (null != record) 
                {
                    if ("" != record.Val_rep || "" != record.FaceVal_rep) 
                    {
                        segment = ds.CreateSegment("06B");
                        ds.Add(segment);

                        segment["06B-020"] = dataApp.aBSsn;
                        segment["06B-030"] = ""; // 6/8/2004 dd - SKIP. We don't have account number for Life Insurance.
                        segment["06B-040"] = record.Val_rep;
                        segment["06B-050"] = record.FaceVal_rep;
                    }
                }
            }
            #endregion

            #region 06C - Assets (M)
            // 06C-020 Applicant Social Security Number
            // 06C-030 Account/Asset Type
            // 06C-040 Depository/Stock/Bond Institution Name
            // 06C-050 Depository Street Address
            // 06C-060 Depository City
            // 06C-070 Depository State
            // 06C-080 Depository Zip Code
            // 06C-090 Depository Zip Code Plust Four
            // 06C-100 Acc. no.
            // 06C-110 Cash or Market Value
            // 06C-120 Number of Stock/Bond Shares
            // 06C-130 Asset Description
            // 06C-140 Reserved for Future Use
            // 06C-150 Reserved for Future Use
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                // OPM 184017. Keep non-purchasing borrowers out of export
                bool nonPurchasingBorrower = false;
                bool nonPurchasingCoBorrower = false;
                if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                {
                    nonPurchasingBorrower = (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly);
                    nonPurchasingCoBorrower = (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly);
                }

                //10/22/07 db - OPM 18382 - retiring 06A segments and adding them to 06C
                var subCollection = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.CashDeposit);
                foreach (var item in subCollection) 
                {
                    var record = (IAssetCashDeposit)item;
                    if ((record.OwnerT == E_AssetOwnerT.Borrower && nonPurchasingBorrower)
                        || (record.OwnerT == E_AssetOwnerT.CoBorrower && nonPurchasingCoBorrower))
                    {
                        continue;
                    }

                    if (record.Val_rep != "") 
                    {
                        segment = ds.CreateSegment("06C");
                        ds.Add(segment);

                        segment["06C-020"] = record.OwnerT == E_AssetOwnerT.Borrower ? dataApp.aBSsn : dataApp.aCSsn;
                        segment["06C-030"] = TypeMap_AssetSpecialT.ToFannie(record.AssetT);
                        segment["06C-040"] = record.Desc;
                        segment["06C-110"] = record.Val_rep;
                    }
                }

                subCollection = dataApp.aAssetCollection.GetSubcollection(false, E_AssetGroupT.LifeInsurance | E_AssetGroupT.Auto | E_AssetGroupT.CashDeposit);
                foreach (var item in subCollection) 
                {
                    var record = (IAsset)item;
                    if (record.Val_rep == "")
                        continue;

                    if ((record.OwnerT == E_AssetOwnerT.Borrower && nonPurchasingBorrower)
                        || (record.OwnerT == E_AssetOwnerT.CoBorrower && nonPurchasingCoBorrower))
                    {
                        continue;
                    }

                    segment = ds.CreateSegment("06C");
                    ds.Add(segment);

                    segment["06C-020"] = record.OwnerT == E_AssetOwnerT.CoBorrower ? dataApp.aCSsn : dataApp.aBSsn;
                    segment["06C-030"] = TypeMap_AssetT.ToFannie(record.AssetT);
                    string comNm = "";
                    string streetAddr = "";
                    string city = "";
                    string state = "";
                    string zip = "";
                    string accountNum = "";
                    if (record.IsRegularType && E_AssetT.OtherIlliquidAsset != record.AssetT) 
                    {
                        var assetRegular = record as IAssetRegular;
                        comNm = assetRegular.ComNm;
                        streetAddr = assetRegular.StAddr;
                        city = assetRegular.City;
                        state = assetRegular.State;
                        zip = assetRegular.Zip;
                        accountNum = assetRegular.AccNum.Value;
                    }
                    segment["06C-040"] = comNm;
                    segment["06C-050"] = streetAddr;
                    segment["06C-060"] = city;
                    segment["06C-070"] = state;
                    segment["06C-080"] = zip;
                    // SKIP 06C-090. Zipcode + 4
                    segment["06C-100"] = accountNum;
                    segment["06C-110"] = record.Val_rep;
                    // SKIP 06C-120. Number of Stock/Bond Shares
                    string desc = "";
                    switch (record.AssetT) 
                    {
                        case E_AssetT.Stocks:
                        case E_AssetT.Bonds:
                        case E_AssetT.OtherIlliquidAsset:
                            desc = record.Desc;
                            break;
                    }
                    segment["06C-130"] = desc;

                }
            }
            #endregion

            #region 06D - Automobiles (M)
            // 06D-020 Applicant Social Security Number
            // 06D-030 Automobile Make/Model
            // 06D-040 Automobile Year
            // 06D-050 Cash or Market Value
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                // OPM 184017. Keep non-purchasing borrowers out of export
                bool nonPurchasingBorrower = false;
                bool nonPurchasingCoBorrower = false;
                if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                {
                    nonPurchasingBorrower = (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly);
                    nonPurchasingCoBorrower = (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly);
                }

                var subCollection = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.Auto);

                foreach (var item in subCollection) 
                {
                    var record = (IAsset)item;
                    if (record.Val_rep != "") 
                    {
                        if ((record.OwnerT == E_AssetOwnerT.Borrower && nonPurchasingBorrower)
                            || (record.OwnerT == E_AssetOwnerT.CoBorrower && nonPurchasingCoBorrower))
                        {
                            continue;
                        }
                        segment = ds.CreateSegment("06D");
                        ds.Add(segment);

                        segment["06D-020"] = record.OwnerT == E_AssetOwnerT.CoBorrower ? dataApp.aCSsn : dataApp.aBSsn;
                        segment["06D-030"] = record.Desc;
                        // SKIP 06D-040. Automobile Year.
                        segment["06D-050"] = record.Val_rep;
                    }
                }
            }
            #endregion

            #region 06F - Alimony, Child Support/Separate Maintenance and/or Job Related Expense(s) (M)
            // 06F-020 Applicant Social Security Number
            // 06F-030 Expense Type Code
            // 06F-040 Monthly Payment Amount
            // 06F-050 Months Left To Pay
            // 06F-060 Alimony/Child Support/Separate Maintenance Owed To
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature
                    && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                {
                    // OPM 184017.  Ignore this non-purchasing spouse
                    continue;
                }

                var subCollection = dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Special);

                foreach (var item in subCollection) 
                {
                    var record = (ILiabilitySpecial)item;
                    if (record.Pmt_rep != "") 
                    {
                        string code = "";
                        string desc = "";
                        switch (record.DebtT) 
                        {
                            case E_DebtSpecialT.JobRelatedExpense:
                                code = "DZ";
                                desc = ((ILiabilityJobExpense) record).ExpenseDesc;
                                break;
                            case E_DebtSpecialT.Alimony:
                                code = "DR";
                                desc = ((ILiabilityAlimony) record).OwedTo;
                                break;
                            case E_DebtSpecialT.ChildSupport:
                                code = "DT";
                                desc = ((ILiabilityChildSupport)record).OwedTo;
                                break;
                        }
                        segment = ds.CreateSegment("06F");
                        ds.Add(segment);

                        segment["06F-020"] = dataApp.aBSsn;
                        segment["06F-030"] = code;
                        segment["06F-040"] = record.Pmt_rep;
                        segment["06F-050"] = record.RemainMons_rep;
                        segment["06F-060"] = desc;

                    }
                }

            }
            #endregion

            Hashtable reoHash = new Hashtable(30);
            int reoIndex = 1;
            #region 06G - Real Estate Owned
            // 06G-020 Applicant Social Security Number
            // 06G-030 Property Street Address
            // 06G-040 Property City
            // 06G-050 Property State
            // 06G-060 Property Zip Code
            // 06G-070 Property Zip Code Plus Four
            // 06G-080 Property Disposition
            // 06G-090 Type Of Property
            // 06G-100 Present Market Value
            // 06G-110 Amount of Mortgages & Liens
            // 06G-120 Gross Rental Income
            // 06G-130 Mortgage Payments
            // 06G-140 Insurance, Maintenance Taxes & Misc.
            // 06G-150 Net Rental Income
            // 06G-160 Current Residence Indicator
            // 06G-170 Subject Property Indicator
            // 06G-180 REO Asset ID
            // 06G-190 Reserved For Future Use
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                // OPM 184017. Keep non-purchasing borrowers out of export
                bool nonPurchasingBorrower = false;
                bool nonPurchasingCoBorrower = false;
                if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                {
                    nonPurchasingBorrower = (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly);
                    nonPurchasingCoBorrower = (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly);
                }


                var subCollection = dataApp.aReCollection.GetSubcollection(true, E_ReoGroupT.All);
                
                foreach (var item in subCollection) 
                {
                    var record = (IRealEstateOwned)item;
                    if ((record.ReOwnerT == E_ReOwnerT.Borrower && nonPurchasingBorrower)
                        || (record.ReOwnerT == E_ReOwnerT.CoBorrower && nonPurchasingCoBorrower))
                    {
                        continue;
                    }

                    segment = ds.CreateSegment("06G");
                    ds.Add(segment);

                    segment["06G-020"] = record.ReOwnerT == E_ReOwnerT.CoBorrower ? dataApp.aCSsn : dataApp.aBSsn;
                    segment["06G-030"] = record.Addr;
                    segment["06G-040"] = record.City;
                    segment["06G-050"] = record.State;
                    segment["06G-060"] = record.Zip;
                    // SKIP 06G-070. Zipcode + 4
                    segment["06G-080"] = TypeMap_ReFieldStat.ToFannie(record.Stat);
                    segment["06G-090"] = TypeMap_ReFieldType.ToFannie(record.TypeT);
                    segment["06G-100"] = record.Val_rep;
                    segment["06G-110"] = record.MAmt_rep;
                    segment["06G-120"] = record.GrossRentI_rep;
                    segment["06G-130"] = record.MPmt_rep;
                    segment["06G-140"] = record.HExp_rep;
                    segment["06G-150"] = record.NetRentI_rep;
                    bool isCurrentResidence = (dataApp.aBAddr != "" && 0 == dataApp.aBAddr.ToUpper().CompareTo(record.Addr.ToUpper()));
                    segment["06G-160"] = isCurrentResidence || record.IsPrimaryResidence ? "Y" : "N";
                    segment["06G-170"] = record.IsSubjectProp ? "Y" : "N";
                    segment["06G-180"] = reoIndex.ToString();
                    reoHash.Add(record.RecordId, reoIndex);
                    reoIndex++;

                }

            }
            #endregion

            #region 06H - Alias Borrower or Co-Borrower cannot have dupliate aliases
            // 06H-020 Applicant Social Security Number
            // 06H-030 Alternate First Name
            // 06H-040 Alternate Middle Name
            // 06H-050 Alternate Last Name
            // 06H-060 Unused
            // 06H-070 Unused
            // SKIP. Don't see POINT exports anything out either.

            #endregion

            #region 06L - Liabilities (M)
            // 06L-020 Applicant Social Security Number
            // 06L-030 Liability Type
            // 06L-040 Creditor Name
            // 06L-050 Creditor Street Address
            // 06L-060 Creditor City
            // 06L-070 Creditor State
            // 06L-080 Creditor Zip Code
            // 06L-090 Creditor Zip Code Plus Four
            // 06L-100 Acct. no.
            // 06L-110 Monthly Payment Amount
            // 06L-120 Months Left to Pay
            // 06L-130 Unpaid Balance
            // 06L-140 Liability will be paid prior to closing
            // 06L-150 REO Asset ID
            // 06L-160 Resubordinated Indicator
            // 06L-170 Omitted Indicator
            // 06L-180 Subject Property Indicator
            // 06L-190 Rental Property Indicator
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                                // OPM 184017. Keep non-purchasing borrowers out of export
                bool nonPurchasingBorrower = false;
                bool nonPurchasingCoBorrower = false;
                if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                {
                    nonPurchasingBorrower = (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly);
                    nonPurchasingCoBorrower = (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly);
                }

                var subCollection = dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Regular);
                
                foreach (ILiabilityRegular record in subCollection) 
                {
                    if ((record.OwnerT == E_LiaOwnerT.Borrower && nonPurchasingBorrower)
                        || (record.OwnerT == E_LiaOwnerT.CoBorrower && nonPurchasingCoBorrower))
                    {
                        continue;
                    }

                    segment = ds.CreateSegment("06L");
                    ds.Add(segment);

                    segment["06L-020"] = record.OwnerT == E_LiaOwnerT.CoBorrower ? dataApp.aCSsn : dataApp.aBSsn;
                    segment["06L-030"] = TypeMap_DebtRegularT.ToFannie(record.DebtT, record.Desc);
                    segment["06L-040"] = record.ComNm;
                    segment["06L-050"] = record.ComAddr;
                    segment["06L-060"] = record.ComCity;
                    segment["06L-070"] = record.ComState;
                    segment["06L-080"] = record.ComZip;
                    // SKIP 06L-090. Zipcode + 4
                    segment["06L-100"] = record.AccNum.Value;

                    // 11/07/07 - db OPM 18886 - Export 0.00 for liability balance and payment instead of blank
                    m_dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlusImportantFields); // Switch to special mode
                    segment["06L-110"] = record.Pmt_rep;
                    segment["06L-130"] = record.Bal_rep;
                    m_dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlus); // Switch back to normal mode

                    // 6/8/2004 dd - Months Left to pay
                    string rep = "";
                    try
                    {
                        decimal pars = decimal.Parse( record.RemainMons_rep );
                        int round = (int) Math.Round( pars, 0 );
                        rep = round.ToString();
                    }
                    catch{}
                    segment["06L-120"] = rep;
                    segment["06L-140"] = record.WillBePdOff ? "Y" : "N";

                    if (record.MatchedReRecordId != Guid.Empty) 
                    {
                        if (reoHash.Contains(record.MatchedReRecordId)) 
                        {
                            int index = (int) reoHash[record.MatchedReRecordId];
                            segment["06L-150"] = index.ToString();
                        }
                    }
                    segment["06L-160"] = ""; // 3/20/2012 dd - FNMA recommend to pass either Y or <blank>.
                    segment["06L-170"] = "N";
                    if (record.DebtT == E_DebtRegularT.Mortgage) // sk 66586 https://opm/default.asp?66586#BugEvent.813932
                    {
                        if (record.ExcFromUnderwriting) 
                        {
                            segment["06L-170"] = "Y";
                        }
                        else if(record.MatchedReRecordId != Guid.Empty)
                        {
                            var reo = dataApp.aReCollection.GetRegRecordOf(record.MatchedReRecordId);
                            if (reo.StatT == E_ReoStatusT.Sale)
                            {
                                segment["06L-170"] = "Y";
                            }
                        }
                    }
                    else // if non-mortgage debt
                    {
                        segment["06L-170"] = record.ExcFromUnderwriting ? "Y" : "N";
                    }

                    // 10/15/2010 dd - OPM 56974 Export subject property lien and rental property lien.
                    string _06L_180 = "N"; // Default to N
                    string _06L_190 = "N"; // Default to N
                    if (record.MatchedReRecordId != Guid.Empty)
                    {
                        var reoCollection = dataApp.aReCollection;
                        var reo =  reoCollection.GetRegRecordOf(record.MatchedReRecordId);
                        if (null != reo)
                        {
                            _06L_180 = reo.IsSubjectProp ? "Y" : "N";
                            _06L_190 = reo.StatT == E_ReoStatusT.Rental ? "Y" : "N";
                        }

                    }
                    segment["06L-180"] = _06L_180; // REVIEW new field for Fannie 3.2
                    segment["06L-190"] = _06L_190; // REVIEW new field for Fannie 3.2

                }

                if (((!IsDUDOExport && m_dataLoan.sExportAdditionalLiabitiesFannieMae)
                    || (IsDUDOExport && m_dataLoan.sExportAdditionalLiabitiesDODU))
                    && dataApp.aPresTotHExpCalc != 0.0M)
                {
                    segment = ds.CreateSegment("06L");
                    ds.Add(segment);

                    segment["06L-020"] = dataApp.aBSsn;
                    segment["06L-030"] = "Z";
                    segment["06L-040"] = dataApp.aPresTotHExpDesc;
                    segment["06L-110"] = dataApp.aPresTotHExpCalc_rep;
                    segment["06L-130"] = m_dataLoan.m_convertLos.ToMoneyString(dataApp.aPresTotHExpCalc * 12, FormatDirection.ToRep);
                    segment["06L-140"] = "N";
                }
            }
            #endregion

            #region 06S - Summary Amounts for income, housing, assets
            // 06S-020 Applicant Social Security Number
            // 06S-030 Summary Amount Type Code
            // 06S-040 Amount

            // OPM 110314
            if (m_dataLoan.sIsOFinCreditLineInDrawPeriod
                && 0 < (m_dataLoan.sSubFin - m_dataLoan.sConcurSubFin)
                && m_dataLoan.sLienPosT == E_sLienPosT.First)
            {
                CAppData dataApp = m_dataLoan.GetAppData(0);

                segment = ds.CreateSegment("06S");
                ds.Add(segment);
                segment["06S-020"] = dataApp.aBSsn;
                segment["06S-030"] = "HMB";
                segment["06S-040"] = m_dataLoan.m_convertLos.ToMoneyString(m_dataLoan.sSubFin - m_dataLoan.sConcurSubFin, FormatDirection.ToRep);
            }

            if (m_dataLoan.sFannieSalesConcessions != 0.00m)
            {
                CAppData dataApp = m_dataLoan.GetAppData(0);

                segment = ds.CreateSegment("06S");
                ds.Add(segment);
                segment["06S-020"] = dataApp.aBSsn;
                segment["06S-030"] = "SCA";
                segment["06S-040"] = m_dataLoan.sFannieSalesConcessions_rep;
            }

            #endregion

            #region 07A - Details of Transaction
            // 07A-020 a. Purchase Price
            // 07A-030 b. Alterations, improvements, repairs,
            // 07A-040 c. Land
            // 07A-050 d. Refinance (Inc. debts to be paid off)
            // 07A-060 e. Estimated prepaid items
            // 07A-070 f. Estimated closing costs
            // 07A-080 g. PMI MIP, Funding Fee
            // 07A-090 h. Discount (if Applicant will pay)
            // 07A-100 j. Subordinate financing
            // 07A-110 k. Applicant's closing costs paid by Seller
            // 07A-120 n. PMI, MIP, Funding Fee financed
            segment = ds.CreateSegment("07A");
            ds.Add(segment);

            segment["07A-020"] = m_dataLoan.sPurchPrice_rep;
            segment["07A-030"] = m_dataLoan.sAltCost_rep;
            segment["07A-040"] = m_dataLoan.sLandCost_rep;
            segment["07A-050"] = m_dataLoan.sRefPdOffAmt1003_rep;
            segment["07A-060"] = m_dataLoan.sTotEstPp1003_rep;
            segment["07A-070"] = m_dataLoan.sTotEstCcNoDiscnt1003_rep;
            segment["07A-080"] = m_dataLoan.sFfUfmip1003_rep;
            segment["07A-090"] = m_dataLoan.sLDiscnt1003_rep;
            segment["07A-100"] = m_dataLoan.sONewFinBal_rep;
            segment["07A-110"] = m_dataLoan.sTotCcPbs_rep;
            segment["07A-120"] = m_dataLoan.sFfUfmipFinanced_rep;
            #endregion

            #region 07B - Other Credits (M)
            Export07B(ds);
            #endregion

            #region 08A - Declarations (SA)
            // 08A-020 Applicant Social Security Number
            // 08A-030 a. Are there any outstanding judgments against you?
            // 08A-040 b. Have you been declared bankrupt within the past 7 years?
            // 08A-050 c. Have you had property foreclosed uporn or given title or deed in lieu thereof in the last 7 years?
            // 08A-060 d. Are you a party to a lawsuit?
            // 08A-070 e. Have you directly or indirectly been obligated on any loan...
            // 08A-080 f. Are you presently delinquent or in default on any Federal debt
            // 08A-090 g. Are you obligated to pay alimony child support or separate maintenance?
            // 08A-100 h. Is any part of the down payment borrowed?
            // 08A-110 i. Are you co-maker or endorser on a note?
            // 08A-120 j. Are you a U.S. citizen? k. Are you a permanent resident alien?
            // 08A-130 l. Do you intend to occupy...
            // 08A-140 m. Have you had an ownership interest
            // 08A-150 m (1) What type of property
            // 08A-160 m (2) How did you hold title
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                for (int j = 0; j < 2; j++) 
                {
                    bool isBorrower = j == 0;

                    if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                    {
                        if ((isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                            || (!isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))

                            // OPM 184017.  Ignore this non-purchasing spouse
                            continue;
                    }

                    if ((isBorrower && dataApp.aBSsn != "") || (!isBorrower && dataApp.aCSsn != "") )
                    {
                        segment = ds.CreateSegment("08A");
                        ds.Add(segment);

                        segment["08A-020"] = isBorrower ? dataApp.aBSsn : dataApp.aCSsn;

                        //08A-030 VIII a. Are there any outstanding judgments against you?
                        segment["08A-030"] =isBorrower ? dataApp.aBDecJudgment : dataApp.aCDecJudgment;

                        //08A-040 VIII b. Have you been declared bankrupt within the past 7 years?
                        segment["08A-040"] = isBorrower ? dataApp.aBDecBankrupt : dataApp.aCDecBankrupt;

                        //08A-050 VIII c. Have you had property foreclosed upon or given title or deed in lieu thereof in the last 7 years?
                        segment["08A-050"] = isBorrower ? dataApp.aBDecForeclosure : dataApp.aCDecForeclosure;

                        //08A-060 VIII d. Are you a party to a lawsuit?
                        segment["08A-060"] = isBorrower ? dataApp.aBDecLawsuit : dataApp.aCDecLawsuit;

                        //08A-070 VIII e. Have you directly or indirectly been obligated on any loan
                        segment["08A-070"] = isBorrower ? dataApp.aBDecObligated : dataApp.aCDecObligated;

                        //08A-080 VIII f. Are you presently delinquent or in default on any Federal debt..
                        segment["08A-080"] = isBorrower ? dataApp.aBDecDelinquent : dataApp.aCDecDelinquent;

                        //08A-090 VIII g. Are you obligated to pay alimony child support or separate maintenance?
                        segment["08A-090"] = isBorrower ? dataApp.aBDecAlimony : dataApp.aCDecAlimony;

                        //08A-100 VIII h. Is any part of the down payment borrowed?
                        segment["08A-100"] = isBorrower ? dataApp.aBDecBorrowing : dataApp.aCDecBorrowing;

                        //08A-110 VIII i. Are you a co-maker or endorser on a note?
                        segment["08A-110"] = isBorrower ? dataApp.aBDecEndorser : dataApp.aCDecEndorser;

                        //08A-120 VIII j. Are you a U.S. citizen? k. Are you a permanent resident alien?
                        // 1066 Citizen Status Code
                        // 01 = U.S. Citizen
                        // 03 = Permanent Resident-Alien
                        // 05 = Non-Permanent Resident-Alien
                        string aDecCitizen = isBorrower ? dataApp.aBDecCitizen : dataApp.aCDecCitizen;
                        string aDecResidency = isBorrower ? dataApp.aBDecResidency : dataApp.aCDecResidency;
                        string fannieCitizenCode = "";
                        switch (aDecCitizen.TrimWhitespaceAndBOM().ToUpper()) 
                        {
                            case "Y": 
                                fannieCitizenCode = "01";
                                break;
                            case "N":
                                switch (aDecResidency.TrimWhitespaceAndBOM().ToUpper()) 
                                {
                                    case "Y":
                                        fannieCitizenCode = "03";
                                        break;
                                    case "N":
                                        fannieCitizenCode = "05";
                                        break;
                                }
                                break;
                        }
                        segment["08A-120"] = fannieCitizenCode;

                        //08A-130 VIII l. Do you intend to occupy...
                        string aDecOcc = isBorrower ? dataApp.aBDecOcc : dataApp.aCDecOcc;
                        if (aDecOcc.TrimWhitespaceAndBOM() == "")
                            aDecOcc = "U";

                        segment["08A-130"] = aDecOcc;

                        //08A-140 VIII m. Have you had an ownership interest...
                        string aDecPastOwnership = isBorrower ? dataApp.aBDecPastOwnership : dataApp.aCDecPastOwnership;
                        if (aDecPastOwnership.TrimWhitespaceAndBOM() == "") 
                            aDecPastOwnership = "U";
                        segment["08A-140"] = aDecPastOwnership;

                        if (aDecPastOwnership == "Y") 
                        {
                            //08A-150 VIII m. (1) What type	of property...
                            segment["08A-150"] = TypeMap_aDecPastOwnedPropT.ToFannie(isBorrower ? dataApp.aBDecPastOwnedPropT : (E_aBDecPastOwnedPropT) dataApp.aCDecPastOwnedPropT);
                            //08A-160 VIII m. (2) How did you hold title
                            segment["08A-160"] = TypeMap_aDecPastOwnedPropTitleT.ToFannie(isBorrower ? dataApp.aBDecPastOwnedPropTitleT : (E_aBDecPastOwnedPropTitleT) dataApp.aCDecPastOwnedPropTitleT);

                        }

                    }
                }
            }
            #endregion

            #region 08B  - Declaration Explanations
            // 08B-020 Applicant Social Security Number
            // 08B-030 Declaration Type Code
            // 08B-040 Declaration Explanation
            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                // 12/18/2014 dd - The type is in FNMA 3.2 Specification.
                dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                if (this.ShouldExportBorrowerData(dataApp))
                {
                    SetDeclarationExplanations(ds, dataApp.aSsn, "91", dataApp.aBDecJudgmentExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "92", dataApp.aBDecBankruptExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "93", dataApp.aBDecForeclosureExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "94", dataApp.aBDecLawsuitExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "95", dataApp.aBDecObligatedExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "96", dataApp.aBDecDelinquentExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "97", dataApp.aBDecAlimonyExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "98", dataApp.aBDecBorrowingExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "99", dataApp.aBDecEndorserExplanation);
                }

                dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                if (this.ShouldExportBorrowerData(dataApp))
                {
                    SetDeclarationExplanations(ds, dataApp.aSsn, "91", dataApp.aCDecJudgmentExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "92", dataApp.aCDecBankruptExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "93", dataApp.aCDecForeclosureExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "94", dataApp.aCDecLawsuitExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "95", dataApp.aCDecObligatedExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "96", dataApp.aCDecDelinquentExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "97", dataApp.aCDecAlimonyExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "98", dataApp.aCDecBorrowingExplanation);
                    SetDeclarationExplanations(ds, dataApp.aSsn, "99", dataApp.aCDecEndorserExplanation);
                }
            }
            #endregion

            #region 09A - Acknowledgment and Agreement
            // 09A-020 Applicant Social Security Number
            // 09A-030 Signature Date
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                for (int j = 0; j < 2; j++) 
                {
                    bool isBorrower = j == 0;

                    if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                    {
                        if ((isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                            || (!isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))

                            // OPM 184017.  Ignore this non-purchasing spouse
                            continue;
                    }

                    if ((isBorrower && dataApp.aBSsn == "") || (!isBorrower && dataApp.aCSsn == ""))
                        continue;

                    segment = ds.CreateSegment("09A");
                    ds.Add(segment);
                    segment["09A-020"] = isBorrower ? dataApp.aBSsn : dataApp.aCSsn;
                    segment["09A-030"] = dataApp.a1003SignD_rep;
                }
            }
            #endregion

            #region 10A - Information for Government Monitoring Purposes
            // 10A-020 Applicant Social Security Number
            // 10A-030 I do not wish to furnish this information
            // 10A-040 Ethnicity
            // 10A-050 Filler
            // 10A-060 Sex
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                for (int j = 0; j < 2; j++) 
                {
                    bool isBorrower = j == 0;

                    if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                    {
                        if ((isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                            || (!isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))

                            // OPM 184017.  Ignore this non-purchasing spouse
                            continue;
                    }

                    if ((isBorrower && dataApp.aBSsn == "") || (!isBorrower && dataApp.aCSsn == ""))
                        continue;

                    segment = ds.CreateSegment("10A");
                    ds.Add(segment);

                    segment["10A-020"] = isBorrower ? dataApp.aBSsn : dataApp.aCSsn;
                    bool aNoFurnish = isBorrower ? dataApp.aBNoFurnish : dataApp.aCNoFurnish;
                    segment["10A-030"] = aNoFurnish ? "Y" : "N";
                    segment["10A-040"] = TypeMap_aHispanicT.ToFannie(isBorrower ? dataApp.aBHispanicTFallback : dataApp.aCHispanicTFallback);
                    segment["10A-050"] = ""; // FILER
                    segment["10A-060"] = TypeMap_aGender.ToFannie(isBorrower ? dataApp.aBGenderFallback : dataApp.aCGenderFallback);
                }
            }
            #endregion

            #region 10B - Interviewer Information
            // 10B-020 This application was taken by
            // 10B-030 Interviewer's Name
            // 10B-040 Interview Date
            // 10B-050 Interviewer's Phone Number
            // 10B-060 Institution Name
            // 10B-070 Institution Street Address
            // 10B-080 Institution Street Address 2
            // 10B-090 Institution City
            // 10B-100 Institution State
            // 10B-110 Institution Zip Code
            // 10B-120 Institution Zip Code Plus Four
            segment = ds.CreateSegment("10B");
            ds.Add(segment);

            segment["10B-020"] = TypeMap_aIntervwrMethodT.ToFannie(primaryDataApp.aIntrvwrMethodT);
            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            segment["10B-030"] = interviewer.PreparerName;
            segment["10B-040"] = primaryDataApp.a1003InterviewD_rep;
            segment["10B-050"] = interviewer.Phone;
            segment["10B-060"] = interviewer.CompanyName;
            segment["10B-070"] = interviewer.StreetAddr;
            // SKIP 10B-080. StreetAddress Line 2
            segment["10B-090"] = interviewer.City;
            segment["10B-100"] = interviewer.State;
            segment["10B-110"] = interviewer.Zip;
            // SKIP 10B-120. Zipcode + 4
            #endregion

            #region 10R - Information for Government Monitoring Purposes reporting on Multiple Race per Applicant
            // 10R-020 Applicant Social Security Number
            // 10R-030 Race
            for (int i = 0; i < nApps; i++) 
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                // OPM 184017. Keep non-purchasing borrowers out of export
                bool nonPurchasingBorrower = false;
                bool nonPurchasingCoBorrower = false;
                if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                {
                    nonPurchasingBorrower = (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly);
                    nonPurchasingCoBorrower = (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly);
                }


                List<string[]> raceList = new List<string[]>();

                if (!nonPurchasingBorrower)
                {
                    raceList.AddRange(new string[][]{  
                                            new string[] { dataApp.aBSsn, "1", dataApp.aBIsAmericanIndian ? "Y" : "" },
                                            new string[] { dataApp.aBSsn, "2", dataApp.aBIsAsian          ? "Y" : "" },
                                            new string[] { dataApp.aBSsn, "3", dataApp.aBIsBlack          ? "Y" : "" },
                                            new string[] { dataApp.aBSsn, "4", dataApp.aBIsPacificIslander? "Y" : "" },
                                            new string[] { dataApp.aBSsn, "5", dataApp.aBIsWhite          ? "Y" : "" }
                                        });

                }

                if (!nonPurchasingCoBorrower)
                {
                    raceList.AddRange(new string[][]{
                                            new string[] { dataApp.aCSsn, "1", dataApp.aCIsAmericanIndian ? "Y" : "" },
                                            new string[] { dataApp.aCSsn, "2", dataApp.aCIsAsian          ? "Y" : "" },
                                            new string[] { dataApp.aCSsn, "3", dataApp.aCIsBlack          ? "Y" : "" },
                                            new string[] { dataApp.aCSsn, "4", dataApp.aCIsPacificIslander? "Y" : "" },
                                            new string[] { dataApp.aCSsn, "5", dataApp.aCIsWhite          ? "Y" : "" }

                    });
                }

                foreach (string[] parts in raceList) 
                {
                    if (parts[0] != "" && parts[2] == "Y") 
                    {
                        segment = ds.CreateSegment("10R");
                        ds.Add(segment);

                        segment["10R-020"] = parts[0];
                        segment["10R-030"] = parts[1];
                    }
                }
                if (dataApp.aBSsn != "" && dataApp.aBNoFurnish && !nonPurchasingBorrower)
                {
                    // 3/21/2012 dd - If No Furnish is check and no race check then export as not furnish
                    if (dataApp.aBIsAmericanIndian == false && dataApp.aBIsAsian == false &&
                        dataApp.aBIsPacificIslander == false && dataApp.aBIsWhite == false &&
                        dataApp.aBIsBlack == false)
                    {
                        segment = ds.CreateSegment("10R");
                        ds.Add(segment);
                        segment["10R-020"] = dataApp.aBSsn;
                        segment["10R-030"] = "6";
                    }
                }
                if (dataApp.aCSsn != "" && dataApp.aCNoFurnish && !nonPurchasingCoBorrower)
                {
                    // 3/21/2012 dd - If No Furnish is check and no race check then export as not furnish
                    if (dataApp.aCIsAmericanIndian == false && dataApp.aCIsAsian == false &&
                        dataApp.aCIsPacificIslander == false && dataApp.aCIsWhite == false &&
                        dataApp.aCIsBlack == false)
                    {
                        segment = ds.CreateSegment("10R");
                        ds.Add(segment);
                        segment["10R-020"] = dataApp.aCSsn;
                        segment["10R-030"] = "6";
                    }
                }
            }
            #endregion
        }

        private void Export07B(MORNETPlusDataset ds)
        {
            if (m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                Export07BFromAdjustmentsAndProrations(ds);
            }
            else
            {
                Export07BFrom1003(ds);
            }
        }

        private void Export07BFrom1003(MORNETPlusDataset ds)
        {
            if (m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "Programming Error.  Should not call Export07BFrom1003 when sLoads1003LineLFromAdjustments.");
            }

            // 07B-020 Other Credit Type Code
            // 07B-030 Amount of Other Credit
            string[][] otherCreditList = {
                                             new string[] {m_dataLoan.sOCredit1Desc, m_dataLoan.sOCredit1Amt_rep},
                                             new string[] {m_dataLoan.sOCredit2Desc, m_dataLoan.sOCredit2Amt_rep},
                                             new string[] {m_dataLoan.sOCredit3Desc, m_dataLoan.sOCredit3Amt_rep},
                                             new string[] {m_dataLoan.sOCredit4Desc, m_dataLoan.sOCredit4Amt_rep},
                                             new string[] {m_dataLoan.sOCredit5Desc, m_dataLoan.sOCredit5Amt_rep}
                                         };
            foreach (string[] parts in otherCreditList)
            {
                if (parts[1] != "")
                {
                    var segment = ds.CreateSegment("07B");
                    ds.Add(segment);

                    segment["07B-020"] = TypeMap_OtherCreditTypeCode.ToFannie(parts[0]);
                    segment["07B-030"] = parts[1];
                }
            }
        }

        private void Export07BFromAdjustmentsAndProrations(MORNETPlusDataset ds)
        {
            if (false == m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                throw new CBaseException(Common.ErrorMessages.Generic, "Programming Error.  Should not call Export07BFromAdjustmentsAndProrations when not sLoads1003LineLFromAdjustments.");
            }

            {
                if (m_dataLoan.sOCredit1Amt_rep != "")
                {
                    var segment = ds.CreateSegment("07B");
                    ds.Add(segment);

                    segment["07B-020"] = TypeMap_OtherCreditTypeCode.ToFannie(m_dataLoan.sOCredit1Desc);
                    segment["07B-030"] = m_dataLoan.sOCredit1Amt_rep;
                }
            }

            foreach (var adjustment in m_dataLoan.sAdjustmentList.Where(a => a.IsPopulateToLineLOn1003))
            {
                var segment = ds.CreateSegment("07B");
                ds.Add(segment);

                segment["07B-020"] = TypeMap_OtherCreditTypeCode.ToFannie(adjustment.Description);
                segment["07B-030"] = adjustment.AmountToBorrower_rep;
            }

            {
                if (m_dataLoan.sOCredit5Amt_rep != "")
                {
                    var segment = ds.CreateSegment("07B");
                    ds.Add(segment);

                    segment["07B-020"] = TypeMap_OtherCreditTypeCode.ToFannie(m_dataLoan.sOCredit5Desc);
                    segment["07B-030"] = m_dataLoan.sOCredit5Amt_rep;
                }
            }

            if (m_dataLoan.sProrationList.IsPopulateToLineLOn1003)
            {
                if (m_dataLoan.sIsIncludeProrationsInTotPp)
                {
                    // Special handling for case 237078. gf
                    var segment = ds.CreateSegment("07B");
                    ds.Add(segment);

                    segment["07B-020"] = TypeMap_OtherCreditTypeCode.ToFannie("Total prorations from seller");
                    segment["07B-030"] = m_dataLoan.sTotalSellerPaidProrations_rep;
                }
                else
                {
                    foreach (var proration in m_dataLoan.sProrationList.Where(p => p.Amount != 0))
                    {
                        var segment = ds.CreateSegment("07B");
                        ds.Add(segment);

                        segment["07B-020"] = TypeMap_OtherCreditTypeCode.ToFannie(proration.Description);
                        segment["07B-030"] = proration.AmountToBorrower_rep;
                    }
                }
            }
        }

        private void SetDeclarationExplanations(MORNETPlusDataset ds, string aSsn, string typeCode, string explanation)
        {
            if (string.IsNullOrEmpty(explanation))
            {
                // 12/18/2014 dd - Only create 08B segment if explanation is not empty.
                return;
            }
            MORNETPlusSegment segment = ds.CreateSegment("08B");
            ds.Add(segment);

            segment["08B-020"] = aSsn;
            segment["08B-030"] = typeCode;
            segment["08B-040"] = explanation;
        }

        private void LoadAdditionalCaseData(MORNETPlusDataset ds) 
        {
            MORNETPlusSegment segment = null;
            int nApps = m_dataLoan.nApps;
            
            CAppData primaryDataApp = m_dataLoan.GetAppData(0); 

            #region 99B - Fannie Mae Transmittal Data
            // 99B-020 Seller Provided Bellow Market Financing
            // 99B-030 Owner of existing mortgage
            // 99B-040 Property Appraised Value
            // 99B-050 Buydown Rate
            // 99B-060 Actual vs. Estimated Appraised Value Indicator
            // 99B-070 Appraisal Fieldwork Ordered
            // 99B-080 Appraiser Name
            // 99B-090 Appraiser Company
            // 99B-100 Appraiser License Number
            // 99B-110 Appraiser License State Code
            segment = ds.CreateSegment("99B");
            ds.Add(segment);

            if (E_sLPurposeT.Purchase == m_dataLoan.sLPurposeT) 
            {
                string code = "";
                switch (m_dataLoan.sIsSellerProvidedBelowMktFin.ToUpper()) 
                {
                    case "Y": code = "Y"; break;
                    case "N": code = "N"; break;
                }

                segment["99B-020"] = code;
            }

            //99B-030 FNM Owner of existing mortgage 5 2 See Fannie Mae and EDI F1 F1
            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin
                || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout
                || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance
                || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl) 
            {
                string code = string.Empty;

                if (m_dataLoan.sSpInvestorCurrentLoanT == E_sSpInvestorCurrentLoanT.FannieMae)
                {
                    code = "01";
                }
                else if (m_dataLoan.sSpInvestorCurrentLoanT == E_sSpInvestorCurrentLoanT.FreddieMac)
                {
                    code = "02";
                }
                else
                {
                    code = "F1";
                }

                segment["99B-030"] = code;
            }

            segment["99B-040"] = m_dataLoan.sApprVal_rep;

            //99B-050 FNM Buydown Rate
            try 
            {
                // Point writes the first month rate resulted of buydown if user checks the "buydown" box in the addendum.
                if (m_dataLoan.sBuydown)
                    segment["99B-050"] = m_dataLoan.sAmortTable.Items[0].Rate_rep;
            } 
            catch {}

            // 99B-060. Actual Vs Estimated Appraised Value Indicator.
            switch(m_dataLoan.sSpValuationMethodT) //OPM 65431
            {
                case E_sSpValuationMethodT.None:
                case E_sSpValuationMethodT.LeaveBlank:
                    segment["99B-060"] = "02"; //Estimated
                    break;
                case E_sSpValuationMethodT.AutomatedValuationModel:
                case E_sSpValuationMethodT.DesktopAppraisal:
                case E_sSpValuationMethodT.DriveBy:
                case E_sSpValuationMethodT.FullAppraisal:
                case E_sSpValuationMethodT.PriorAppraisalUsed:
                    segment["99B-060"] = "01"; //Actual
                    break;
            }

            // 99B-070 Apprisal Fieldwork ordered.
            string appraisalFieldworkCode = TypeMap_sSpAppraisalFormT.ToFannie(m_dataLoan.sSpAppraisalFormT);
            if (!string.IsNullOrEmpty(appraisalFieldworkCode))
            {
                segment["99B-070"] = appraisalFieldworkCode;
            }

            CAgentFields appraiser = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            segment["99B-080"] = appraiser.AgentName;
            segment["99B-090"] = appraiser.CompanyName;
            segment["99B-100"] = appraiser.LicenseNumOfAgent;
            segment["99B-110"] = string.IsNullOrEmpty(appraiser.LicenseNumOfAgent) ? string.Empty : m_dataLoan.sSpState; // Appraiser License State Code. Using state from loan since that's what's used to look up license number.
            #endregion

            #region ADS - Additional Data Segment.
            // ADS-020 Name
            // ADS-030 Value
            // 12/21/2009 dd - OPM 43711 - Export Loan Originator Id and Loan Origination Company Id
            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            var adsManager = new AdditionalDataSegmentManager();
            adsManager.AddSegment("LoanOriginatorID", interviewer.LoanOriginatorIdentifier);
            adsManager.AddSegment("LoanOriginationCompanyID", interviewer.CompanyLoanOriginatorIdentifier);
            adsManager.AddSegment("TotalMortgagedPropertiesCount", m_dataLoan.sNumFinancedProperties_rep);
            adsManager.AddSegment("FIPSCodeIdentifier", m_dataLoan.sFannieFips);

            if (!string.IsNullOrEmpty(this.m_dataLoan.sSpAppraisalId))
            {
                adsManager.AddSegment("AppraisalIdentifier", this.m_dataLoan.sSpAppraisalId);
            }

            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                var appData = m_dataLoan.GetAppData(i);

                appData.BorrowerModeT = E_BorrowerModeT.Borrower;
                if (this.ShouldExportBorrowerData(appData) && !string.IsNullOrEmpty(appData.aSsn))
                {
                    
                    this.PopulateBorrowerHmdaData(appData, adsManager);
                }

                appData.BorrowerModeT = E_BorrowerModeT.Coborrower;
                if (this.ShouldExportBorrowerData(appData) && !string.IsNullOrEmpty(appData.aCSsn))
                {
                    this.PopulateBorrowerHmdaData(appData, adsManager);
                }
            }

            foreach (var ads in adsManager.Segments)
            {
                segment = ds.CreateSegment("ADS");
                ds.Add(segment);
                segment["ADS-020"] = ads.Name;
                segment["ADS-030"] = ads.Value;
            }

            foreach (DataAccess.FannieMae.FannieMaeThirdPartyProvider provider in m_dataLoan.sDuThirdPartyProviders.GetProviders())
            {
                segment = ds.CreateSegment("ADS");
                ds.Add(segment);
                segment["ADS-020"] = provider.Name;
                segment["ADS-030"] = provider.ReferenceNumber;
            }
            #endregion

            #region SCA - Score
            // SCA-020 Score ID
            // SCA-030 Score Value
            // SCA-050 Score Date
            //FUTURE: Currently we don't store credit scores. Point doesn't export their credit scores out either.
            #endregion

        }

        private void LoadLoanProductData(MORNETPlusDataset ds) 
        {
            MORNETPlusSegment segment = null;
            int nApps = m_dataLoan.nApps;
            
            CAppData primaryDataApp = m_dataLoan.GetAppData(0); 

            #region LNC - Loan Characteristics for Eligibility
            // LNC-020 Lien Type Code
            // LNC-030 Loan Documentation Type Code
            // LNC-040 Subject Proeprty Type Code
            // LNC-050 Reserved for future use
            // LNC-060 Reserved for future use
            // LNC-070 Reserved for future use
            // LNC-080 Reserved for future use
            // LNC-090 Project Classification Code
            // LNC-100 Negative Amortization Limit Percent
            // LNC-110 Baloon Indicator
            // LNC-120 Filler
            // LNC-130 Filler
            // LNC-140 Homebuyer Education Completion Indicator
            // LNC-150 Maximum Lifetime Rate Increase
            // LNC-160 Payment Adjustment Life Percent Cap
            // LNC-170 Payment Adjustment Life Amount Cap
            // LNC-180 Will Escrow be Waived
            // LNC-190 Scheduled Loan Closing Date
            // LNC-200 Scheduled First Payment Date
            // LNC-210 MI Coverage Percent
            // LNC-220 MI Insurer Code
            // LNC-230 APR Spread
            // LNC-240 HOEPA
            // LNC-250 PreApproval
            segment = ds.CreateSegment("LNC");
            ds.Add(segment);

            segment["LNC-020"] = TypeMap_sLienPosT.ToFannie(m_dataLoan.sLienPosT);
            segment["LNC-030"] = TypeMap_sFannieDocT.ToFannie(m_dataLoan.sFannieDocT);

            // OPM 469839
            // If MH Advantage is true, LNC-040 = 11
            segment["LNC-040"] = this.m_dataLoan.sHomeIsMhAdvantageTri == E_TriState.Yes ? "11" : TypeMap_sFannieSpT.ToFannie(m_dataLoan.sFannieSpT);

            segment["LNC-090"] = TypeMap_sSpProjectClassFannieT.ToFannie(m_dataLoan.sSpProjectClassFannieT);
            segment["LNC-100"] = m_dataLoan.sPmtAdjMaxBalPc_rep;
            segment["LNC-110"] = m_dataLoan.sBalloonPmt ? "Y" : "N";

            // LNC-150 Homebuyer Education Completion
            if (m_dataLoan.sFannieHomebuyerEducationT != E_FannieHomebuyerEducation.LeaveBlank)
            {
                segment["LNC-140"] = m_dataLoan.sFannieHomebuyerEducationT.ToString("d"); // 1 = Homebuyer Education Complete; 2 = One-on-one Counseling Complete
            }

            //LNC-150 Loan Maximum Lifetime Rate Increase
            segment["LNC-150"] = E_sFinMethT.ARM == m_dataLoan.sFinMethT ? m_dataLoan.sRAdjLifeCapR_rep : "";
            //LNC-160 Loan Payment Adjustment Life Percent Cap
            segment["LNC-160"] = E_sFinMethT.ARM == m_dataLoan.sFinMethT ? m_dataLoan.sPmtAdjMaxBalPc_rep : "";

            //LNC-170 Loan Payment Adjustment Life Amount Cap. SKIP
            
            //LNC-180 Loan Will Escrow be Waived?
            segment["LNC-180"] = m_dataLoan.sWillEscrowBeWaived ? "Y" : "N";

            //LNC-190 Loan Scheduled Loan Closing Date
            segment["LNC-190"] = m_dataLoan.sEstCloseD_rep;

            //LNC-200 Loan Scheduled First Payment Date
            segment["LNC-200"] = m_dataLoan.sSchedDueD1_rep;

            // LNC-250 PreApproval
            segment["LNC-250"] = TypeMap_sHmdaPreapprovalT.ToFannie(m_dataLoan.sHmdaPreapprovalT);

            //LNC-210 Loan MI Coverage Percent
            segment["LNC-210"] = m_dataLoan.sMInsCoverPc_rep;

            //LNC-220 Loan MI Insurer Code
             /* MI Insurer Code
                -001 = GE Mortgage Insurance Corporation, GE
                -006 = Mortgage Guarantee Insurance Corporation, MGIC
                -011 = PMI Mortgage Insurance Company, PMI
                -012 = United Guarantee Residential Insurance Company, UG
                -013 = Republic Mortgage Insurance Company, RMIC
                -017 = Commonwealth Mortgage Assurance Company,	CMAC
                -024 = Triad Guarantee Residential Insurance Company, Triad
                033 = Amerin Guarantee Corporation, Amerin
                038 = CMG Mortgage Insurance Co., an affiliate of PMI (credit unions only), CMG
             */
            string code = "";
            switch( m_dataLoan.sMInsCode )
            {
                case "01": code = "001"; break;
                case "06": code = "006"; break;
                case "11": code = "011"; break;
                case "12": code = "012"; break;
                case "13": code = "013"; break;
                case "17": code = "017"; break;
                case "24": code = "024"; break;
                case "33": code = "033"; break;
                default:   code = ""; break;
            }
            segment["LNC-220"] = code;

            // LNC-230 APR Spread
            var aprRateSpread = this.m_dataLoan.sHmdaAprRateSpreadFannie;
            if (!string.IsNullOrEmpty(aprRateSpread))
            {
                segment["LNC-230"] = aprRateSpread;
            }

            // LNC-240 HOEPA
            segment["LNC-240"] = m_dataLoan.sHmdaReportAsHoepaLoan ? "Y" : "N";

            // LNC-250 Pre-Approved Loan ??? SKIP
            if(m_dataLoan.sHmdaPreapprovalT == E_sHmdaPreapprovalT.PreapprovalRequested || m_dataLoan.sHmdaPreapprovalT == E_sHmdaPreapprovalT.PreapprovalNotRequested)
            {
                segment["LNC-250"] = m_dataLoan.sHmdaPreapprovalT == E_sHmdaPreapprovalT.PreapprovalRequested ? "Y" : "N";
            }
            #endregion

            #region PID - Product Identification
            // PID-020 Product Description
            // PID-030 Product Code
            // PID-040 Product Plan Number
            segment = ds.CreateSegment("PID");
            ds.Add(segment);

            segment["PID-020"] = m_dataLoan.sFannieProdDesc;


            segment["PID-030"] = m_dataLoan.sFannieProductCode;

            //PID-040 Product Product Plan Number
            segment["PID-040"] = m_dataLoan.sFannieARMPlanNum;
            #endregion

            #region PCH - Product Characteristics
            // PCH-020 Mortgage Term
            // PCH-030 Assumable Loan Indicator
            // PCH-040 Payment Frequency Code
            // PCH-050 Prepayment Penalty Indicator
            // PCH-060 Prepayment Resticted Indicator
            // PCH-070 Repayment Type Code
            segment = ds.CreateSegment("PCH");
            ds.Add(segment);

            //PCH-020 Product Characteristics Mortgage Term
            if (m_dataLoan.sBalloonPmt)    //av opm 56081 du & lp balloon term 
            {
                segment["PCH-020"] = m_dataLoan.sDue_rep;
            }
            //PCH-030 Product Characteristics Assumable Loan Indicator
            code = "";
            switch (m_dataLoan.sAssumeLT) 
            {
                case E_sAssumeLT.May: code = "Y"; break;
                case E_sAssumeLT.MayNot: code = "N"; break;
            }
            segment["PCH-030"] = code;

            //PCH-040 Product Characteristics Payment Frequency Code
            segment["PCH-040"] = "01"; // 6/8/2004 dd - Always set to Monthly.

            //PCH-050 Product Characteristics Prepayment Penalty Indicator
            switch (m_dataLoan.sPrepmtPenaltyT) 
            {
                case E_sPrepmtPenaltyT.May: code = "Y"; break;
                case E_sPrepmtPenaltyT.WillNot: code = "N"; break;
                case E_sPrepmtPenaltyT.LeaveBlank:
                default:
                    code = "";
                    break;
            }
            segment["PCH-050"] = code;

            //PCH-060 Product Characteristics Prepayment Restricted Indicator. SKIP

            //PCH-070 Product Characteristics Repayment Type Code. 
            // N = Fully Amortizing (No Negative Amortization)
            // F1 = Scheduled Amortization
            // F2 = Interest Only
            // P = Possible Negative Amortization
            // S = Scheduled Negative Amortization
            if (m_dataLoan.sIOnlyMon > 0)
            {
                segment["PCH-070"] = "F2";
            }
            else
            {
                segment["PCH-070"] = "N"; // 3/21/2012 dd - FNMA recommend use N.
            }

            #endregion

            #region ARM - ARM
            // ARM-020 ARM Index Value
            // ARM-030 Index Type (Index Name)
            // ARM-040 ARM Index Margin
            // ARM-050 ARM Qualifying Rate
            if (m_dataLoan.sFinMethT == E_sFinMethT.ARM) 
            {
                segment = ds.CreateSegment("ARM");
                ds.Add(segment);;
                //ARM-020 ARM ARM Index Value
                segment["ARM-020"] = m_dataLoan.sRAdjIndexR_rep;

                //ARM-030 ARM Index Type (Index Name) 11 2 See Fannie Mae and EDI Null Space
                /*
                Index Type
                    -0 = Weekly Average CMT
                    -1 = Monthly Average CMT
                    -2 = Weekly Average TAAI
                    -3 = Weekly Average TAABD
                    -4 = Weekly Average SMTI
                    -5 = Daily CD Rate
                    -6 = Weekly Average CD Rate
                    -7 = Weekly Ave Prime Rate
                    -8 = T-Bill Daily Value
                    -9 = 11th District COF
                    -10 = National Monthly Median Cost of Funds
                    -11 = Wall Street Journal LIBOR
                    -12 = Fannie Mae LIBOR
                    13 = Freddie Mac LIBOR
                    14 = National Average Contract Rate (FHLBB)
                    15 = Federal Cost of Funds
                    16 = Fannie Mae 60-Day Required Net Yield
                    17 = Freddie Mac 60-Day Required Net Yield
                */
                switch( m_dataLoan.sArmIndexT )
                {
                    case E_sArmIndexT.WeeklyAvgCMT:						code	= "0"; break;
                    case E_sArmIndexT.MonthlyAvgCMT:					code	= "1"; break;
                    case E_sArmIndexT.WeeklyAvgTAAI:					code	= "2"; break;
                    case E_sArmIndexT.WeeklyAvgTAABD:					code	= "3"; break;
                    case E_sArmIndexT.WeeklyAvgSMTI:					code	= "4"; break;
                    case E_sArmIndexT.DailyCDRate:						code	= "5"; break;
                    case E_sArmIndexT.WeeklyAvgCDRate:					code	= "6"; break;
                    case E_sArmIndexT.WeeklyAvgPrimeRate:				code	= "7"; break;
                    case E_sArmIndexT.TBillDailyValue:					code	= "8"; break;
                    case E_sArmIndexT.EleventhDistrictCOF:				code	= "9"; break;
                    case E_sArmIndexT.NationalMonthlyMedianCostOfFunds:	code	= "10"; break;
                    case E_sArmIndexT.WallStreetJournalLIBOR:			code	= "11"; break;
                    case E_sArmIndexT.FannieMaeLIBOR:					code	= "12"; break;
                    case E_sArmIndexT.LeaveBlank:
                    default:
                        code	= ""; break;

                }
                segment["ARM-030"] = code;

                //ARM-040 ARM ARM Index Margin
                segment["ARM-040"] = m_dataLoan.sRAdjMarginR_rep;

                //ARM-050 ARM ARM Qualifying Rate
                segment["ARM-050"] = m_dataLoan.sQualIR_rep;


            }
            #endregion

            #region PAJ - Payment Adjustment Occurrences
            // PAJ-020 Payment Adjustment Period Number
            // PAJ-030 Payment Adjustment Duration
            // PAJ-040 Payment Adjustment Frequency
            // PAJ-050 Payment Adjustment Type Code
            // PAJ-060 Payment Adjustment Percent
            // PAJ-070 Payment Adjustment Amount
            // PAJ-080 Payment Adjustment Percent Cap
            // PAJ-090 Payment Adjustment Amount Cap
            // PAJ-100 Months to First Payment Adjustment
            #endregion 

            #region RAJ - Rate Adjustment Occurrences
            // RAJ-020 Rate Adjustment Period Number
            // RAJ-030 Rate Adjustment Duration
            // RAJ-040 Rate Adjustment Frequency
            // RAJ-050 Rate Adjustment Calculation Method Code
            // RAJ-060 Rate Adjustment Percent
            // RAJ-070 Rate Adjustment Cap
            // RAJ-080 Months to First Rate Adjustment
            #endregion

            #region BUA - Buydown Data
            // BUA-020 Buydown Frequency
            // BUA-030 Buydown Duration
            // BUA-040 Increase Rate
            // BUA-050 Funding Code
            // BUA-060 Buydown Base Date Code
            // BUA-070 Buydown Type Indicator
            if (m_dataLoan.sBuydown) 
            {
                segment = ds.CreateSegment("BUA");
                ds.Add(segment);

                int buydownDuration = m_dataLoan.sBuydwnMon1 + m_dataLoan.sBuydwnMon2 + m_dataLoan.sBuydwnMon3 + m_dataLoan.sBuydwnMon4 + m_dataLoan.sBuydwnMon5;

                segment["BUA-020"] = m_dataLoan.sBuydwnMon1_rep;
                segment["BUA-030"] = m_dataLoan.m_convertLos.ToCountString(buydownDuration);
                segment["BUA-040"] = m_dataLoan.sSchedIR1_rep;
                segment["BUA-050"] = "Y";

                //Buydown Base Date Code: A code indicating the date from which the duration of the buydown is
                //	counted. Note: This field is not currently used (this note is pulled from Fannie document)
                //BUA-060 Buydown Buydown Base Date Code 18 1 N Values: 0 = Note Date 1 = First Payment Date 
                //2 = Last Payment Date N/A Space, N/A
                segment["BUA-060"] = "0";


                //BUA-070 Buydown Buydown Type Indicator (Perm. Or Temp.) 19 1 0=Permanent 1=Temporary N/A Space, N/A
                segment["BUA-070"] = (buydownDuration == 0 || buydownDuration >= m_dataLoan.sTerm) ? "0" : "1";
            }
            #endregion

        }

        private void LoadGovernmentData(MORNETPlusDataset ds)
        {
            MORNETPlusSegment segment = null;
            int nApps = m_dataLoan.nApps;

            E_sLT sLT = m_dataLoan.sLT;
            
            CAppData primaryDataApp = m_dataLoan.GetAppData(0);

            #region IDA - Casefile Identification
            if (sLT == E_sLT.FHA || sLT == E_sLT.VA)
            {
                // IDA-020 Filler - Reserved for future use, fill with blanks
                segment = ds.CreateSegment("IDA");
                ds.Add(segment);
            }
            #endregion

            #region LEA - Lender Data
            if (sLT == E_sLT.FHA || sLT == E_sLT.VA)
            {
                // LEA-020 FHA Lender Identifier
                // LEA-030 FHA Sponsor Identifier
                // LEA-040 Sponsored Originator EIN
                // LEA-050 Filler - Reserved for future use, fill with blanks
                segment = ds.CreateSegment("LEA");
                ds.Add(segment);
                if (m_dataLoan.sFhaLenderIdT == E_sFhaLenderIdT.RegularFhaLender)
                {
                    segment["LEA-020"] = m_dataLoan.sFHALenderIdCode; //FHA Statement of Appraised Value
                }

                segment["LEA-030"] = m_dataLoan.sFHASponsorAgentIdCode; //FHA Statement of Appraised Value
                if (m_dataLoan.sFhaLenderIdT == E_sFhaLenderIdT.SponsoredOriginatorEIN)
                {
                    segment["LEA-040"] = m_dataLoan.sSponsoredOriginatorEIN; // 2/4/2013 dd - OPM 94903
                }
            }
            #endregion

            #region GOA - Government Data For Both FHA/VA Loans
            if (sLT == E_sLT.FHA || sLT == E_sLT.VA) 
            {
                // GOA-020 Energy Efficient New Home
                // GOA-030 MCC
                // GOA-040 Seller Concessions
                // GOA-050 Borrower Total Closing Costs Fees
                // GOA-060 Borrower Total Closing Costs Points (%)
                // GOA-070 Seller Total Closing Costs Fees
                // GOA-080 Seller Total Closing Costs Points (%)
                // GOA-090 Other Total Closing Costs Fees
                // GOA-100 Other Total Closing Costs Points (%)
                // GOA-110 Type of Refinance
                // GOA-120 Property County
                segment = ds.CreateSegment("GOA");
                ds.Add(segment);

                if (m_dataLoan.sMortgageCreditCertificateTotal != 0.00m)
                {
                    segment["GOA-030"] = m_dataLoan.sMortgageCreditCertificateTotal_rep;
                }

                //FHA MCAW - Refi A3: OPM 63572 - M.P.
                if (sLT == E_sLT.FHA)
                {
                    segment["GOA-040"] = m_dataLoan.sFHASellerContributionExceedingMaximum_rep;
                }
                else // if (sLT == E_sLT.VA)
                {
                    segment["GOA-040"] = m_dataLoan.sFHASellerContribution_rep;
                }
                
                segment["GOA-050"] = m_dataLoan.sFHACcPbb_rep; //FHA MCAW - Refi
                segment["GOA-070"] = m_dataLoan.sFHACcPbs_rep; //FHA MCAW - Refi

                // OPM 48337
                if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                {
                    segment["GOA-110"] = TypeMap_sFHARefinanceTypeDesc.ToFannie(m_dataLoan.sLPurposeT, m_dataLoan.sFHAPurposeIsStreamlineRefi,
                        m_dataLoan.sTotalScoreRefiT);
                }
                segment["GOA-120"] = m_dataLoan.sSpCounty; // VA Loan Summary
            }
            #endregion

            #region GOB - Government Data For FHA Loans Only
            if (sLT == E_sLT.FHA) 
            {
                // GOB-020 Section of the Act
                // GOB-030 Allowable Repairs
                // GOB-040 MIP Upfront (%)
                // GOB-050 MIP Refund Amount
                // GOB-060 First Renewal Rate (%)
                segment = ds.CreateSegment("GOB");
                ds.Add(segment);

                segment["GOB-020"] = m_dataLoan.sFHAHousingActSection; //FHA MCAW - Refi
                segment["GOB-040"] = m_dataLoan.sFfUfmipR_rep; //FHA MCAW - Refi
                segment["GOB-050"] = m_dataLoan.sFHASalesConcessions_rep; //FHA MCAW - Refi
            }

            #endregion

            #region GOC - Government Data For VA Loans Only
            if (sLT == E_sLT.VA) 
            {
                // GOC-020 CoBorrower Married to Primary Borrower
                // GOC-030 Entitlement Amount
                // GOC-040 Monthly Maintenance
                // GOC-050 Monthly Utilities
                // GOC-060 Funding Fee (%)
                segment = ds.CreateSegment("GOC");
                ds.Add(segment);

                bool areAllCoBorrowersMarried = true;
                bool sendGOc20 = false;

                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    CAppData app = m_dataLoan.GetAppData(i);
                    if (!app.aCIsValidNameSsn )
                    {
                        continue;
                    }
                    //we only want to send when all apps that have coborower are all set to married.
                    areAllCoBorrowersMarried = app.aCMaritalStatT == E_aCMaritalStatT.Married && app.aBMaritalStatT == E_aBMaritalStatT.Married;
                    sendGOc20 = true;
                }

                if (sendGOc20)
                {
                    segment["GOC-020"] = areAllCoBorrowersMarried ? "Y" : "N";
                }
                segment["GOC-030"] = primaryDataApp.aVaEntitleAmt_rep; //VA Loan Summary
                segment["GOC-040"] = m_dataLoan.sVaProMaintenancePmt_rep.TrimWhitespaceAndBOM(); //VA Loan Analysis
                segment["GOC-050"] = m_dataLoan.sVaProUtilityPmt_rep.TrimWhitespaceAndBOM(); //VA Loan Analysis
                segment["GOC-060"] = m_dataLoan.sFfUfmipR_rep; //Loan Info Up-front MIP/FF
            }
            #endregion

            #region GOD - Taxes (From VA folder -> Loan Analysis Page)
            if (sLT == E_sLT.VA) 
            {
                // GOD-020 Applicant Social Security Number
                // GOD-030 Federal Tax
                // GOD-040 State Tax
                // GOD-050 Local Income Tax
                // GOD-060 Social Security Tax
                // GOD-070 Total Non-Taxable Income - Primary
                // GOD-080 Total Non-Taxable Income - Other
                // GOD-090 Total Taxable Income - Primary
                // GOD-100 Total Taxable Income - Other
            
                for (int i = 0; i < nApps; i++) 
                {
                    CAppData dataApp = m_dataLoan.GetAppData(i);

                    for (int j = 0; j < 2; j++) 
                    {
                        bool isBorrower = j == 0;

                    if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                    {
                        if ((isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                            || (!isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))

                            // OPM 184017.  Ignore this non-purchasing spouse
                            continue;
                    }


                        if ((isBorrower && dataApp.aBSsn.Length > 0) || (!isBorrower && dataApp.aCSsn.Length > 0)) 
                        {
                            segment = ds.CreateSegment("GOD");
                            ds.Add(segment);

                            segment["GOD-020"] = isBorrower ? dataApp.aBSsn : dataApp.aCSsn; //FHA MCAW - Refi
                            segment["GOD-030"] = isBorrower ? dataApp.aVaBTotITax_rep : dataApp.aVaCTotITax_rep; //VA Loan Analysis 
                            segment["GOD-040"] = isBorrower ? dataApp.aVaBStateITax_rep : dataApp.aVaCStateITax_rep; //VA Loan Analysis 
                            segment["GOD-060"] = isBorrower ? dataApp.aVaBSsnTax_rep : dataApp.aVaCSsnTax_rep; //VA Loan Analysis 
                            segment["GOD-070"] = isBorrower ? dataApp.aVaBONetI_rep : dataApp.aVaCONetI_rep; //VA Loan Analysis 
                            segment["GOD-090"] = isBorrower ? dataApp.aVaBEmplmtI_rep : dataApp.aVaCEmplmtI_rep; //VA Loan Analysis 
                        }
                    }
                }
            }
            #endregion

            #region GOE - Credit Data
            if (sLT == E_sLT.VA || sLT == E_sLT.FHA) 
            {
                // GOE-020 Applicant Social Security Number
                // GOE-030 CAIVR #
                // GOE-040 Borrower Credit Rating - Fannie supplied (therefore only for imports from fannie)
                // GOE-050 Bankruptcy < 3 Years - Fannie supplied (therefore only for imports from fannie)
                // GOE-060 Foreclosure - Fannie supplied (therefore only for imports from fannie)
                // GOE-070 Credit Report Score Type - Fannie supplied (therefore only for imports from fannie)
            
                for (int i = 0; i < nApps; i++) 
                {
                    CAppData dataApp = m_dataLoan.GetAppData(i);

                    for (int j = 0; j < 2; j++) 
                    {
                        bool isBorrower = j == 0;

                        if (IsDUDOExport && dataApp.LoanData.BrokerDB.IsUseNewNonPurchaseSpouseFeature)
                        {
                            if ((isBorrower && (dataApp.aBTypeT == E_aTypeT.NonTitleSpouse || dataApp.aBTypeT == E_aTypeT.TitleOnly))
                                || (!isBorrower && (dataApp.aCTypeT == E_aTypeT.NonTitleSpouse || dataApp.aCTypeT == E_aTypeT.TitleOnly)))

                                // OPM 184017.  Ignore this non-purchasing spouse
                                continue;
                        }


                        if ((isBorrower && dataApp.aBSsn.Length > 0) || (!isBorrower && dataApp.aCSsn.Length > 0)) 
                        {
                            segment = ds.CreateSegment("GOE");
                            ds.Add(segment);

                            segment["GOE-020"] = isBorrower ? dataApp.aBSsn : dataApp.aCSsn; //FHA MCAW - Refi
                            segment["GOE-030"] = isBorrower ? dataApp.aFHABCaivrsNum : dataApp.aFHACCaivrsNum; //FHA MCAW - Refi
                            segment["GOE-070"] = TypeMap_aTotalScoreFhtbCounselingT.ToFannie(isBorrower ? dataApp.aBTotalScoreFhtbCounselingT : dataApp.aCTotalScoreFhtbCounselingT);
                        }
                    }
                }
            }
            #endregion
        }

        private void LoadCommunityLendingData(MORNETPlusDataset ds)
        {
            MORNETPlusSegment segment = null;
            int nApps = m_dataLoan.nApps;
            
            CAppData primaryDataApp = m_dataLoan.GetAppData(0); 

            #region LMD - Community Lending Data
            // LMD-020 Metropolitan Statistical Area or County
            // LMD-030 Community Lending Product
            // LMD-040 Fannie Neighbors eligible
            // LMD-050 Community Seconds
            // LMD-060 HUD Median Income
            // LMD-070 Income Limit Adjustment Factors
            // LMD-080 Community Lending Income Limit

            if(m_dataLoan.sIsCommunityLending)
            {
                segment = ds.CreateSegment("LMD");
                ds.Add(segment);

                segment["LMD-020"] = m_dataLoan.sFannieMsa;
                // 3/21/2012 dd - LMD-030 - will also contain LMD-035
                // LMD-030 - 2 characters
                // LMD-035 - 38 characters

                string lmd030 = TypeMap_sFannieCommunityLendingT.ToFannie(m_dataLoan.sFannieCommunityLendingT);
                string lmd035 = TypeMap_sFannieCommunitySecondsRepaymentStructureT.ToFannie(m_dataLoan.sFannieCommunitySecondsRepaymentStructureT);

                segment["LMD-030"] = lmd030 + lmd035;
                segment["LMD-040"] = m_dataLoan.sIsFannieNeighbors ? "Y" : "N";
                segment["LMD-050"] = m_dataLoan.sIsCommunitySecond ? "Y" : "N";
                segment["LMD-070"] = m_dataLoan.sFannieIncomeLimitAdjPc_rep;
            }
            #endregion
        }

        private int Round( decimal d )
        {
            return (int)(d + 0.5M);
        }
        private void BuildWarningMessages(MORNETPlus32Document doc) 
        {
            MORNETPlusDataset[] dataSets = { 
                                               doc._1003,
                                               doc.AdditionalCaseData,
                                               doc.LoanProductData,
                                               doc.GovernmentLoanData,
                                               doc.CommunityLendingData
                                           };
            foreach (MORNETPlusDataset ds in dataSets) 
            {
                foreach (MORNETPlusSegment segment in ds.SegmentList) 
                {
                    foreach (string msg in segment.WarningMessages) 
                    {
                        m_warningMessages.Add(msg);

                    }
                    m_errorMessages.AddRange(segment.ErrorMessages);
                }
            }
        }
        private void DebugMORNETPlus(MORNETPlus32Document doc) 
        {
            Log(doc.EnvelopeHeader);
            Log(doc.TransactionHeader);
            Log(doc.TransactionProcessingInfo);
            
            Log(doc._1003Header);

            foreach (MORNETPlusSegment segment in doc._1003.SegmentList) 
            {
                Log(segment);
            }
            Log(doc.AdditionalCaseDataHeader);
            foreach (MORNETPlusSegment segment in doc.AdditionalCaseData.SegmentList) 
            {
                Log(segment);
            }
            Log(doc.LoanProductDataHeader);
            foreach (MORNETPlusSegment segment in doc.LoanProductData.SegmentList) 
            {
                Log(segment);
            }

            Log(doc.TransactionTrailer);
            Log(doc.EnvelopeTrailer);
        }
        private void Log(MORNETPlusSegment segment) 
        {
            string s = segment.ToString();
            Tools.LogInfo("[" + s + "]. Length = " + s.Length);
        }

        /// <summary>
        /// Adds the HMDA datapoints for a single borrower to the Additional Data Segment.
        /// </summary>
        /// <param name="appData">The borrower information.</param>
        /// <param name="adsManager">The ADS dataset to populate.</param>
        private void PopulateBorrowerHmdaData(CAppData appData, AdditionalDataSegmentManager adsManager)
        {
            this.GenerateGenderData(appData, adsManager);
            this.GenerateEthnicityData(appData, adsManager);
            this.GenerateRaceData(appData, adsManager);

            adsManager.AddRepeatableSegment("ApplicationTakenMethodType", this.ToApplicationTakenMethodType(appData.aInterviewMethodT), appData.aSsn);
        }

        /// <summary>
        /// Generate the HMDA gender datapoints for a borrower.
        /// </summary>
        /// <param name="appData">The borrower information.</param>
        /// <param name="adsManager">The ADS dataset to populate.</param>
        private void GenerateGenderData(CAppData appData, AdditionalDataSegmentManager adsManager)
        {
            var gender = this.ToGenderType(appData.aGender, appData.aInterviewMethodT);
            if (!string.IsNullOrEmpty(gender))
            {
                adsManager.AddRepeatableSegment("HMDAGenderType", gender, appData.aSsn);
            }

            adsManager.AddRepeatableSegment("HMDAGenderRefusalIndicator", this.ToGenderRefusalIndicator(appData.aGender), appData.aSsn);
            adsManager.AddRepeatableSegment("HMDAGenderCollectedBasedOnVisual", this.ToYN(appData.aSexCollectedByObservationOrSurname), appData.aSsn);
        }

        /// <summary>
        /// Converts a tristate to a Y/N string.
        /// </summary>
        /// <param name="triState">A tristate value.</param>
        /// <returns>A string with the value "Y" or "N".</returns>
        private string ToYN(E_TriState triState)
        {
            return ToYN(triState == E_TriState.Yes);
        }

        /// <summary>
        /// Converts a boolean to a Y/N string.
        /// </summary>
        /// <param name="indicator">A boolean indicator.</param>
        /// <returns>A string with the value "Y" or "N".</returns>
        private string ToYN(bool indicator)
        {
            return indicator ? "Y" : "N";
        }

        /// <summary>
        /// Determines the borrower's gender. There should only be one instance of this value.
        /// </summary>
        /// <param name="gender">The borrower's gender.</param>
        /// <param name="interviewMethod">The application interview method.</param>
        /// <returns>The gender value to send to FNMA.</returns>
        private string ToGenderType(E_GenderT gender, E_aIntrvwrMethodT interviewMethod)
        {
            if (gender == E_GenderT.MaleAndFemale || gender == E_GenderT.MaleFemaleNotFurnished)
            {
                return "ApplicantSelectedBothMaleAndFemale";
            }
            else if (gender == E_GenderT.Female || gender == E_GenderT.FemaleAndNotFurnished)
            {
                return "Female";
            }
            else if (gender == E_GenderT.Male || gender == E_GenderT.MaleAndNotFurnished)
            {
                return "Male";
            }
            else if (gender == E_GenderT.NA)
            {
                return "NotApplicable";
            }
            else if (gender == E_GenderT.Unfurnished
                && (interviewMethod != E_aIntrvwrMethodT.LeaveBlank
                && interviewMethod != E_aIntrvwrMethodT.FaceToFace))
            {
                return "InformationNotProvidedUnknown";
            }

            return string.Empty;
        }

        /// <summary>
        /// Determines whether the borrower refused to indicate their gender.
        /// </summary>
        /// <param name="gender">The borrower's gender.</param>
        /// <returns>A boolean indicating whether the borrower refused to indicate their gender.</returns>
        private string ToGenderRefusalIndicator(E_GenderT gender)
        {
            return this.ToYN(gender == E_GenderT.Unfurnished
                || gender == E_GenderT.FemaleAndNotFurnished
                || gender == E_GenderT.MaleAndNotFurnished
                || gender == E_GenderT.MaleFemaleNotFurnished);
        }

        /// <summary>
        /// Generate the HMDA ethnicity datapoints for a borrower.
        /// </summary>
        /// <param name="appData">The borrower information.</param>
        /// <param name="adsManager">The ADS dataset to populate.</param>
        private void GenerateEthnicityData(CAppData appData, AdditionalDataSegmentManager adsManager)
        {
            this.GenerateEthnicityTypes(appData, adsManager);
            this.GenerateEthnicityOriginTypes(appData, adsManager);
            adsManager.AddRepeatableSegment("HMDAEthnicityRefusalIndicator", this.ToYN(appData.aDoesNotWishToProvideEthnicity), appData.aSsn);
            adsManager.AddRepeatableSegment("HMDAEthnicityCollectedBasedOnVisual", this.ToYN(appData.aEthnicityCollectedByObservationOrSurname), appData.aSsn);
        }

        /// <summary>
        /// Generates the ethnicity of the borrower. More than one indicator can appear.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="adsManager">The ADS list.</param>
        private void GenerateEthnicityTypes(CAppData appData, AdditionalDataSegmentManager adsManager)
        {
            string fieldName = "HMDAEthnicityType";

            if (appData.aHispanicT == E_aHispanicT.Hispanic
                || appData.aHispanicT == E_aHispanicT.BothHispanicAndNotHispanic)
            {
                adsManager.AddRepeatableSegment(fieldName, "HispanicOrLatino", appData.aSsn);
            }

            if (appData.aHispanicT == E_aHispanicT.BothHispanicAndNotHispanic
                || appData.aHispanicT == E_aHispanicT.NotHispanic)
            {
                adsManager.AddRepeatableSegment(fieldName, "NotHispanicOrLatino", appData.aSsn);
            }

            if (appData.aHispanicT == E_aHispanicT.LeaveBlank
                && appData.aInterviewMethodT != E_aIntrvwrMethodT.LeaveBlank
                && appData.aInterviewMethodT != E_aIntrvwrMethodT.FaceToFace)
            {
                adsManager.AddRepeatableSegment(fieldName, "InformationNotProvidedByApplicantInMIT", appData.aSsn);
            }
        }

        /// <summary>
        /// Generates the ethnicity origin of the borrower. More than one indicator can appear.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="adsManager">The ADS list.</param>
        private void GenerateEthnicityOriginTypes(CAppData appData, AdditionalDataSegmentManager adsManager)
        {
            string fieldName = "HMDAEthnicityOriginType";

            if (appData.aIsCuban)
            {
                adsManager.AddRepeatableSegment(fieldName, "Cuban", appData.aSsn);
            }

            if (appData.aIsMexican)
            {
                adsManager.AddRepeatableSegment(fieldName, "Mexican", appData.aSsn);
            }

            if (appData.aIsPuertoRican)
            {
                adsManager.AddRepeatableSegment(fieldName, "PuertoRican", appData.aSsn);
            }

            if (appData.aIsOtherHispanicOrLatino)
            {
                adsManager.AddRepeatableSegment(fieldName, "Other", appData.aSsn);
                
                if (!string.IsNullOrEmpty(appData.aOtherHispanicOrLatinoDescription))
                {
                    adsManager.AddRepeatableSegment("HMDAEthnicityOriginTypeOtherDesc", appData.aOtherHispanicOrLatinoDescription, appData.aSsn);
                }
            }
        }

        /// <summary>
        /// Generate the HMDA race datapoints for a borrower.
        /// </summary>
        /// <param name="appData">The borrower information.</param>
        /// <param name="adsManager">The ADS dataset to populate.</param>
        private void GenerateRaceData(CAppData appData, AdditionalDataSegmentManager adsManager)
        {
            this.GenerateRaceTypes(appData, adsManager);
            adsManager.AddRepeatableSegment("HMDARaceRefusalIndicator", this.ToYN(appData.aDoesNotWishToProvideRace), appData.aSsn);
            adsManager.AddRepeatableSegment("HMDARaceCollectedBasedOnVisual", this.ToYN(appData.aRaceCollectedByObservationOrSurname), appData.aSsn);
        }

        /// <summary>
        /// Generates the race of the borrower. More than one indicator can appear. The sequence number
        /// for a race should be passed down and used to link any related race designations.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="adsManager">The ADS list.</param>
        private void GenerateRaceTypes(CAppData appData, AdditionalDataSegmentManager adsManager)
        {
            string fieldName = "HMDARaceType";
            int sequenceNumber = 1;

            if (this.GenerateAmericanIndianRaceTypes(appData, adsManager, sequenceNumber))
            {
                sequenceNumber++;
            }

            if (this.GenerateAsianRaceTypes(appData, adsManager, sequenceNumber))
            {
                sequenceNumber++;
            }

            if (appData.aIsBlack)
            {
                adsManager.AddRepeatableSegment(fieldName, "BlackOrAfricanAmerican", appData.aSsn, sequenceNumber++);
            }

            if (this.GeneratePacificIslanderRaceTypes(appData, adsManager, sequenceNumber))
            {
                sequenceNumber++;
            }

            if (appData.aIsWhite)
            {
                adsManager.AddRepeatableSegment(fieldName, "White", appData.aSsn, sequenceNumber++);
            }

            if (appData.aDoesNotWishToProvideRace)
            {
                adsManager.AddRepeatableSegment(fieldName, "InformationNotProvidedByApplicantInMIT", appData.aSsn, sequenceNumber++);
            }
        }

        /// <summary>
        /// Generates the American Indian race and additional description of the borrower.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="adsManager">The ADS list.</param>
        /// <param name="sequenceNumber">The sequence number of the linked race category.</param>
        /// <returns>A boolean indicating whether any American Indian race datapoints were exported.</returns>
        private bool GenerateAmericanIndianRaceTypes(CAppData appData, AdditionalDataSegmentManager adsManager, int sequenceNumber)
        {
            bool sequenceNumberUsed = false;

            if (appData.aIsAmericanIndian)
            {
                adsManager.AddRepeatableSegment("HMDARaceType", "AmericanIndianOrAlaskaNative", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (!string.IsNullOrEmpty(appData.aOtherAmericanIndianDescription))
            {
                adsManager.AddRepeatableSegment("HMDARaceTypeAdditionalDescription", appData.aOtherAmericanIndianDescription, appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            return sequenceNumberUsed;
        }

        /// <summary>
        /// Generates the Asian race designations of the borrower. More than one indicator can appear.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="adsManager">The ADS list.</param>
        /// <param name="sequenceNumber">The sequence number of the linked race category.</param>
        /// <returns>A boolean indicating whether any Asian race designations were exported.</returns>
        private bool GenerateAsianRaceTypes(CAppData appData, AdditionalDataSegmentManager adsManager, int sequenceNumber)
        {
            bool sequenceNumberUsed = false;

            if (appData.aIsAsian)
            {
                adsManager.AddRepeatableSegment("HMDARaceType", "Asian", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            string fieldName = "HMDARaceDesignationType";

            if (appData.aIsAsianIndian)
            {
                adsManager.AddRepeatableSegment(fieldName, "AsianIndian", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsChinese)
            {
                adsManager.AddRepeatableSegment(fieldName, "Chinese", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsFilipino)
            {
                adsManager.AddRepeatableSegment(fieldName, "Filipino", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsJapanese)
            {
                adsManager.AddRepeatableSegment(fieldName, "Japanese", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsKorean)
            {
                adsManager.AddRepeatableSegment(fieldName, "Korean", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsVietnamese)
            {
                adsManager.AddRepeatableSegment(fieldName, "Vietnamese", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsOtherAsian)
            {
                adsManager.AddRepeatableSegment(fieldName, "OtherAsian", appData.aSsn, sequenceNumber);

                if (!string.IsNullOrEmpty(appData.aOtherAsianDescription))
                {
                    adsManager.AddRepeatableSegment("HMDARaceDesignationOtherAsnDesc", appData.aOtherAsianDescription, appData.aSsn, sequenceNumber);
                }

                sequenceNumberUsed = true;
            }

            return sequenceNumberUsed;
        }

        /// <summary>
        /// Generates the Pacific Islander race designations of the borrower. More than one indicator can appear.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="adsManager">The ADS list.</param>
        /// <param name="sequenceNumber">The sequence number of the linked race category.</param>
        /// <returns>A boolean indicating whether any Pacific Islander designations were exported.</returns>
        private bool GeneratePacificIslanderRaceTypes(CAppData appData, AdditionalDataSegmentManager adsManager, int sequenceNumber)
        {
            bool sequenceNumberUsed = false;

            if (appData.aIsPacificIslander)
            {
                adsManager.AddRepeatableSegment("HMDARaceType", "NativeHawaiianOrOtherPacificIslander", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            string fieldName = "HMDARaceDesignationType";

            if (appData.aIsNativeHawaiian)
            {
                adsManager.AddRepeatableSegment(fieldName, "NativeHawaiian", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsGuamanianOrChamorro)
            {
                adsManager.AddRepeatableSegment(fieldName, "GuamanianOrChamorro", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsSamoan)
            {
                adsManager.AddRepeatableSegment(fieldName, "Samoan", appData.aSsn, sequenceNumber);
                sequenceNumberUsed = true;
            }

            if (appData.aIsOtherPacificIslander)
            {
                adsManager.AddRepeatableSegment(fieldName, "OtherPacificIslander", appData.aSsn, sequenceNumber);

                if (!string.IsNullOrEmpty(appData.aOtherPacificIslanderDescription))
                {
                    adsManager.AddRepeatableSegment("HMDARaceDesignationOtherPIDesc", appData.aOtherPacificIslanderDescription, appData.aSsn, sequenceNumber);
                }

                sequenceNumberUsed = true;
            }

            return sequenceNumberUsed;
        }

        /// <summary>
        /// Determines the method used to take the application.
        /// </summary>
        /// <param name="method">The method used.</param>
        /// <returns>The method value to send to FNMA.</returns>
        private string ToApplicationTakenMethodType(E_aIntrvwrMethodT method)
        {
            switch (method)
            {
                case E_aIntrvwrMethodT.LeaveBlank:
                    return string.Empty;
                case E_aIntrvwrMethodT.ByMail:
                    return "Mail";
                case E_aIntrvwrMethodT.ByTelephone:
                    return "Telephone";
                case E_aIntrvwrMethodT.FaceToFace:
                    return "FaceToFace";
                case E_aIntrvwrMethodT.Internet:
                    return "Internet";
                default:
                    throw new UnhandledEnumException(method);
            }
        }

        /// <summary>
        /// Determines whether borrower data should be exported. Generally this will return
        /// true unless the lender is using the Non-Purchase Spouse feature and the borrower
        /// is not a co-signer to the mortgage loan.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A boolean indicating whether borrower information should be exported.</returns>
        private bool ShouldExportBorrowerData(CAppData appData)
        {
            if (this.IsDUDOExport && this.m_dataLoan.BrokerDB.IsUseNewNonPurchaseSpouseFeature
                && (appData.aTypeT == E_aTypeT.NonTitleSpouse || appData.aTypeT == E_aTypeT.TitleOnly))
            {
                return false;
            }

            return true;
        }
    }
}
