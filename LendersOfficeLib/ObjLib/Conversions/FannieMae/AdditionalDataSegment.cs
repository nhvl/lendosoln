﻿namespace LendersOffice.Conversions
{
    using System.Text.RegularExpressions;

    /// <summary>
    /// Represents an ADS segment in a FNM 3.2 payload.
    /// </summary>
    public class AdditionalDataSegment
    {
        /// <summary>
        /// The character limit for ADS values.
        /// </summary>
        public static readonly int MaxValueLength = 50;

        /// <summary>
        /// The designated delimiter for ADS segments.
        /// </summary>
        public static readonly char Delimiter = ':';

        /// <summary>
        /// Initializes a new instance of the <see cref="AdditionalDataSegment"/> class.
        /// </summary>
        /// <param name="name">The segment name.</param>
        /// <param name="value">The segment value.</param>
        public AdditionalDataSegment(string name, string value)
        {
            this.Name = name;
            this.Value = value;
        }

        /// <summary>
        /// Gets the name of the segment.
        /// </summary>
        /// <value>The name of the segment.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets the value of the segment.
        /// </summary>
        /// <value>The value of the segment.</value>
        public string Value { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the segment is repeatable. All HMDA segments
        /// are repeatable, as well as any that begin with an SSN.
        /// </summary>
        /// <value>A boolean indicating whether the segment is repeatable.</value>
        public bool IsRepeatable
        {
            get
            {
                // ^     - Start of string
                // \d{9} - SSN (9 digits)
                // :     - ADS separator
                // .*    - Any other data included
                var repeatableValueRegex = new Regex(@"^\d{9}" + Delimiter + @".*");

                return this.Name.Contains("HMDA") || repeatableValueRegex.IsMatch(this.Value);
            }
        }
    }
}
