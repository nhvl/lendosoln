using System;
using System.Collections;
using System.IO;

using DataAccess;
using LendersOffice.Security;
using LendersOffice.Common;
namespace LendersOffice.Conversions
{

    public enum FannieMaeImportSource 
    {
        LendersOffice = 0,
        PriceMyLoan = 1,
        H4H = 2,
        AUD = 3
    }
    public class InvalidFannieFileFormatException : CBaseException
    {
		public InvalidFannieFileFormatException( string userMessage, string devMessage ) : base( userMessage, devMessage ) 
		{
		}

		public InvalidFannieFileFormatException( ) : base ( "Unsupported file version.", "Invalid FNMA file" ) 
		{

		}

    }
	/// <summary>
	/// This class will invoke the correct FannieMae 3.0 and 3.2 import class.
	/// </summary>
	public class FannieMaeImporter
	{
        private const string INVALID = "INVALID";
        private const string FANNIE_MAE_30 = "3.00";
        private const string FANNIE_MAE_32 = "3.20";
		
		# region fannie mae import w/ error reporting  

        public static CPageData ImportIntoLead(AbstractUserPrincipal principal, Stream stream, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateID, out string errors)
        {
            return ImportIntoLead(principal, principal.BranchId, stream, source, addEmployeeAsOfficialAgent, templateID, out errors);
        }
        public static CPageData ImportIntoLead(AbstractUserPrincipal principal, Guid branchId, Stream stream, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateID, out string errors)
        {
            string[] lines = null;
            using (StreamReader reader = new StreamReader(stream))
            {
                ArrayList list = new ArrayList();
                string line = null;
                while ((line = reader.ReadLine()) != null)
                    list.Add(line);

                lines = (string[])list.ToArray(typeof(string));

            }

            CPageData dataLoan = null;

            string version = GetVersion(lines);
            if (version == FANNIE_MAE_32)
            {
                FannieMae32Importer importer = new FannieMae32Importer(source);
                importer.AddEmployeeAsOfficialAgent = addEmployeeAsOfficialAgent;
                importer.CreateLead = true;
                dataLoan = importer.Import(lines, principal, branchId, templateID);
                errors = importer.HasSkippedSections ? importer.WarningMessage : string.Empty;
                return dataLoan;
            }
            else
                throw new InvalidFannieFileFormatException();
        }

		public static CPageData Import ( AbstractUserPrincipal principal, Stream stream, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateID, out string errors ) 
        {
            return Import(principal, principal.BranchId, stream, source, addEmployeeAsOfficialAgent, templateID, out errors);
        }
        public static CPageData Import(AbstractUserPrincipal principal, Guid branchId, Stream stream, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateID, out string errors) 
		{
			string[] lines = null;
			using (StreamReader reader = new StreamReader(stream)) 
			{
				ArrayList list = new ArrayList();
				string line = null;
				while ((line = reader.ReadLine()) != null)
					list.Add(line);

				lines = (string[]) list.ToArray(typeof(string));

			}

			return Import( principal, branchId, lines, source, addEmployeeAsOfficialAgent, templateID, out errors );
		}

        public  static CPageData Import ( AbstractUserPrincipal principal, string[] lines, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateID, out string errors )
        {
            return Import(principal, principal.BranchId, lines, source, addEmployeeAsOfficialAgent, templateID, out errors);
        }
		public  static CPageData Import ( AbstractUserPrincipal principal, Guid branchId, string[] lines, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateID, out string errors )
		{
			CPageData dataLoan = null;

			string version = GetVersion(lines);
			if (version == FANNIE_MAE_32)
                dataLoan = Import32(principal, branchId, lines, source, addEmployeeAsOfficialAgent, templateID, out errors);
			else
				throw new InvalidFannieFileFormatException();

			return dataLoan;
		}

        private static CPageData Import32(AbstractUserPrincipal principal, Guid branchId, string[] lines, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateId, out string errors) 
        {
            FannieMae32Importer importer = new FannieMae32Importer(source);
            importer.AddEmployeeAsOfficialAgent = addEmployeeAsOfficialAgent;
            CPageData dataLoan = importer.Import(lines, principal, branchId, templateId);
			errors = importer.HasSkippedSections ? importer.WarningMessage : string.Empty; 
            return dataLoan;
        }
       
		#endregion 

		public static CPageData Import(AbstractUserPrincipal principal, System.IO.Stream stream, bool addEmployeeAsOfficialAgent, Guid templateId) 
        {
            return Import(principal, stream, FannieMaeImportSource.LendersOffice, addEmployeeAsOfficialAgent, templateId, true);
        }

        public static CPageData Import(AbstractUserPrincipal principal, System.IO.Stream stream, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateId, bool importMultipleApps) 
        {
            string[] lines = null;
            using (StreamReader reader = new StreamReader(stream)) 
            {
                ArrayList list = new ArrayList();
                string line = null;
                while ((line = reader.ReadLine()) != null)
                    list.Add(line);

                lines = (string[]) list.ToArray(typeof(string));

            }

            return Import(principal, lines, source, addEmployeeAsOfficialAgent, templateId, importMultipleApps);
        }

        public static CPageData Import(AbstractUserPrincipal principal, string[] lines, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateId, bool importMultipleApps, bool isLead = false) 
        {
            CPageData dataLoan = null;

            string version = GetVersion(lines);
            if (version == FANNIE_MAE_32)
                dataLoan = Import32(principal, lines, source, addEmployeeAsOfficialAgent, templateId, importMultipleApps, isLead);
            else
                throw new InvalidFannieFileFormatException();

            return dataLoan;

        }

        private static CPageData Import32(AbstractUserPrincipal principal, string[] lines, FannieMaeImportSource source, bool addEmployeeAsOfficialAgent, Guid templateId, bool importMultipleApps, bool isLead) 
        {
            FannieMae32Importer importer = new FannieMae32Importer(source);
            importer.CreateLead = isLead;
            importer.ImportMultipleApps = importMultipleApps;
            importer.AddEmployeeAsOfficialAgent = addEmployeeAsOfficialAgent;
            CPageData dataLoan = importer.Import(lines, principal, templateId);

            return dataLoan;
        }
        
        private static CPageData Import( AbstractUserPrincipal principal, System.IO.Stream stream, bool addEmployeeAsOfficialAgent, Guid templateId, bool importMultipleApps )
        {
            return Import(principal, stream, FannieMaeImportSource.PriceMyLoan, addEmployeeAsOfficialAgent, templateId, importMultipleApps);
        }
        /// <summary>
        /// Return INVALID, 3.00, 3.20
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        private static string GetVersion(string[] lines) 
        {
            // 6/8/2004 dd - Base on Fannie 3.0 and Fannie 3.2 Format, file version should be on the 4th line.

            string ret = INVALID;

            if (lines.Length > 4) 
            {
                if (lines[0].StartsWith("EH") && lines[1].StartsWith("TH") && lines[2].StartsWith("TPI") && lines[3].StartsWith("000")) 
                {
                    if (lines[3].Length > 11) 
                    {
                        string version = lines[3].Substring(6, 5).TrimWhitespaceAndBOM();
                        if (version == FANNIE_MAE_30)
                            ret = FANNIE_MAE_30;
                        else if (version == FANNIE_MAE_32)
                            ret = FANNIE_MAE_32;
                    }
                }
            }

            if (ret == FANNIE_MAE_30)
            {
                throw new InvalidFannieFileFormatException(ErrorMessages.FannieMae30ImportError, "Cannot import 3.0 fannie files anymore");
            }
            return ret;

        }
	}
}
