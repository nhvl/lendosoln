﻿namespace LendersOffice.Conversions
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Manages data flow for the FNM 3.2 Additional Data Segment.
    /// </summary>
    public class AdditionalDataSegmentManager
    {
        /// <summary>
        /// Gets the segment list.
        /// </summary>
        /// <value>The segment list.</value>
        public List<AdditionalDataSegment> Segments { get; private set; } = new List<AdditionalDataSegment>();

        /// <summary>
        /// Adds a segment to the ADS.
        /// </summary>
        /// <param name="name">The segment name.</param>
        /// <param name="value">The segment value.</param>
        public void AddSegment(string name, string value)
        {
            var segment = new AdditionalDataSegment(name, value);

            // OPM 465446: If FNMA 3.2 field is missing ADS segment name or value, just skip.
            // No need to reject the entire file.
            if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(value))
            {
                return;
            }

            if (!segment.IsRepeatable && this.Segments.Any(s => name.Equals(s.Name)))
            {
                var message = $"ADS Field {name} appears more than once in the file.";
                throw new InvalidFannieFileFormatException(message, message);
            }

            this.Segments.Add(segment);
        }

        /// <summary>
        /// Adds a repeatable segment to the ADS.
        /// </summary>
        /// <param name="field">The datapoint name.</param>
        /// <param name="value">The datapoint value.</param>
        /// <param name="ssn">The borrower's SSN, used for disambiguation.</param>
        /// <param name="sequenceNumber">The sequence number, if applicable.</param>
        public void AddRepeatableSegment(string field, string value, string ssn, int? sequenceNumber = null)
        {
            string sequenceNumberSegment = string.Empty;
            if (sequenceNumber != null)
            {
                sequenceNumberSegment = $"{sequenceNumber}:";
            }

            var compositeValue = $"{ssn.Replace("-", string.Empty)}:{sequenceNumberSegment}{value}";

            // Some freeform segments can be longer than the character limit.
            // In this case, the segment can be repeated with the remaining data.
            string extraText = string.Empty;
            if (compositeValue.Length > AdditionalDataSegment.MaxValueLength)
            {
                extraText = compositeValue.Substring(startIndex: AdditionalDataSegment.MaxValueLength);
                compositeValue = compositeValue.Substring(startIndex: 0, length: AdditionalDataSegment.MaxValueLength);
            }

            this.AddSegment(field, compositeValue);

            if (!string.IsNullOrEmpty(extraText))
            {
                this.AddRepeatableSegment(field, extraText, ssn, sequenceNumber);
            }
        }
    }
}
