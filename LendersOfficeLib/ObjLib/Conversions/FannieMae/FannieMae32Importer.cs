namespace LendersOffice.Conversions
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using DataAccess;
    using DataAccess.Core.Construction;
    using DataAccess.FannieMae;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions.MORNETPlus;
    using LendersOffice.Conversions.MORNETPlus32;
    using LendersOffice.CreditReport;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Migration.LoanDataMigrations;

    public class ApplicantInfo
    {
        public string aBSsn { get; set; }
        public string aBSsn_rep
        {
            get { return FriendlySsn(aBSsn); }
        }
        public string aBNm { get; set; }
        public string aCSsn { get; set; }
        public string aCSsn_rep
        {
            get { return FriendlySsn(aCSsn); }
        }
        public string aCNm { get; set; }
        public Guid aAppId { get; set; }
        private string FriendlySsn(string ssn)
        {
            if (!string.IsNullOrEmpty(ssn))
            {
                if (ssn.Length == 9)
                {
                    return ssn.Substring(0, 3) + "-" + ssn.Substring(3, 2) + "-" + ssn.Substring(5);
                }
                else
                {
                    return ssn;
                }
            }
            return string.Empty;
        }
        public ApplicantInfo()
        {
            aBSsn = "";
            aBNm = "";
            aCSsn = "";
            aCNm = "";
        }
        public bool IsEquals(ApplicantInfo o)
        {
            if (o == null)
            {
                return false;
            }

            return SafeInsensitiveCompare(aBSsn, o.aBSsn) && SafeInsensitiveCompare(aBNm, o.aBNm) 
                && SafeInsensitiveCompare(aCSsn, o.aCSsn) && SafeInsensitiveCompare(aCNm, o.aCNm);
        }
        private bool SafeInsensitiveCompare(string x, string y)
        {
            if (string.IsNullOrEmpty(x) && string.IsNullOrEmpty(y))
            {
                return true;
            }
            else if (string.IsNullOrEmpty(x) || string.IsNullOrEmpty(y))
            {
                return false;
            }
            else
            {
                return x.ToLower() == y.ToLower();
            }
        }
    }
    public enum E_ComparisionResult
    {
        Matched,
        Added,
        Deleted,
        Changed
    }
    public class ApplicantInfoCompareResult
    {
        public E_ComparisionResult Result { get; set; }
        public ApplicantInfo OldData { get; set; }
        public ApplicantInfo NewData { get; set; }

        private ApplicantInfo ValidData
        {
            get
            {
                switch (Result)
                {
                    case E_ComparisionResult.Matched:
                        return OldData;
                    case E_ComparisionResult.Added:
                        return NewData;
                    case E_ComparisionResult.Deleted:
                        return OldData;
                    case E_ComparisionResult.Changed:
                        return OldData;
                    default:
                        throw new UnhandledEnumException(Result);
                }
            }
        }
        public string aBNm_rep
        {
            get { return ValidData.aBNm; }
        }
        public string aBSsn_rep
        {
            get { return ValidData.aBSsn_rep; }
        }
        public string aCNm_rep
        {
            get { return ValidData.aCNm; }
        }
        public string aCSsn_rep
        {
            get { return ValidData.aCSsn_rep; }
        }
        public string Result_rep
        {
            get
            {
                switch (Result)
                {
                    case E_ComparisionResult.Matched:
                        return "Matched";
                    case E_ComparisionResult.Added:
                        return "Will be added*";
                    case E_ComparisionResult.Deleted:
                        return "Will be deleted";
                    case E_ComparisionResult.Changed:
                        return "";
                    default:
                        throw new UnhandledEnumException(Result);
                }
            }
        }
    }
    
    
    
    public class FannieMae32Importer 
    {


        protected CPageData m_dataLoan;
        private CLoanFileCreator m_loanFileCreator = null;
        private AbstractUserPrincipal m_principal;
        private FannieMaeImportSource m_fannieMaeImportSource;
        private Guid m_branchId = Guid.Empty;
        private Guid m_templateId = Guid.Empty;
        private MORNETPlus32Document m_mornetDoc;
        private bool m_importMultipleApps = true;
        private bool m_IssFfUfmipRSet = false;

        private bool m_clearExistingCollectionData = false;// 10/5/2007 dd - By default clear existing collections will be false when import. It is only set to true when import into existing.

        private decimal m_totalSubjectPropertyNetCashflow; // 5/8/2013 dt - Needed temporarily to store during import - iOPM 118545
        private bool m_hasSubjectPropNetCashFlow;

        // 4/22/2005 dd - Default value was true.
        // However OPM #1663 - Kevin Bagley want to have this turn off. Therefore make this a user option.
        private bool m_addEmployeeAsOfficialAgent = true;
        public bool IsByPassPermission { get; set; }
        public bool CreateLead
        {
            get;
            set;
        }
        public bool AddEmployeeAsOfficialAgent 
        {
            get { return m_addEmployeeAsOfficialAgent; }
            set { m_addEmployeeAsOfficialAgent = value; }
        }

        public bool HasSkippedSections 
        {
            get 
            {
                return m_mornetDoc._1003.Skipped   
                    || m_mornetDoc.AdditionalCaseData.Skipped
                    || m_mornetDoc.CommunityLendingData.Skipped 
                    || m_mornetDoc.GovernmentLoanData.Skipped 
                    || m_mornetDoc.LoanProductData.Skipped ; 
            }
        }

        /// <summary>
        /// By default this is set to true. av opm  19499  Allow importing multiple applications in PML from Fannie files
        /// </summary>
        public bool ImportMultipleApps
        {
            get { return m_importMultipleApps; }
            set { m_importMultipleApps = value; }
        }
        public string WarningMessage 
        {
            get 
            {
                ArrayList skippedSections = new ArrayList(); 

                if ( m_mornetDoc._1003.Skipped  )												skippedSections.Add("1003 Application data");
                if ( m_mornetDoc.AdditionalCaseData.Skipped )									skippedSections.Add("Additional Case data"); 
                if ( m_mornetDoc.CommunityLendingData.Skipped )									skippedSections.Add("Community Lending data");
                if ( m_mornetDoc.GovernmentLoanData.Skipped )									skippedSections.Add("Government Loan data");
                if ( m_mornetDoc.LoanProductData.Skipped )										skippedSections.Add("Loan Product data");						
                return ErrorMessages.GetFannieMaeWarnings(skippedSections );
            }
        }
        public FannieMae32Importer() : this(FannieMaeImportSource.LendersOffice) 
        {
        }
        public FannieMae32Importer(FannieMaeImportSource source) 
        {
            m_fannieMaeImportSource = source;
        }

        public void ImportIntoExisting(string fnmContent, Guid sLId) 
        {
            // 10/4/2007 dd - When import into existing I will wipe out employment, assets, liabilities and REO collection;
            m_clearExistingCollectionData = true;
            string[] lines = fnmContent.Split('\n');

            m_mornetDoc = new MORNETPlus32Document();
            m_mornetDoc.Load(lines);

            if ((m_fannieMaeImportSource == FannieMaeImportSource.H4H) || (m_fannieMaeImportSource == FannieMaeImportSource.AUD))
            {
                List<ApplicantInfoCompareResult> result;
                bool hasConflict = HasApplicantConflict(fnmContent, sLId, out result);

                if (hasConflict)
                {
                    // 12/10/2009 dd - Need to delete applicant that is obsolete.
                    List<Guid> deleteAppList = new List<Guid>();
                    foreach (var o in result)
                    {
                        if (o.Result == E_ComparisionResult.Deleted)
                        {
                            deleteAppList.Add(o.OldData.aAppId);
                        }
                    }

                    CPageData dataLoan = null;
                    if (this.IsByPassPermission)
                    {
                        dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(FannieMae32Importer));
                        dataLoan.DisableFieldEnforcement();
                    }
                    else
                    {
                        dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMae32Importer));
                    }
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    foreach (var id in deleteAppList)
                    {
                        if (dataLoan.DelApp(id))
                        {
                            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                        }
                        else
                        {
                            // 12/10/2009 dd - The reason it could not delete is because it the only app.
                            CAppData dataApp = dataLoan.GetAppData(0);
                            dataApp.aReCollection.ClearAll();
                            dataApp.aLiaCollection.ClearAll();
                            dataApp.aAssetCollection.ClearAll();
                            
                            dataApp.DelMarriedCobor();
                            dataApp.SwapMarriedBorAndCobor();
                            dataApp.DelMarriedCobor();
                            CreditReportServer.DeleteCreditReport(dataLoan.sBrokerId, dataApp.aAppId); // 12/17/2009 dd - Remove the credit report.
                            dataLoan.Save();
                        }

                    }
                }
            }
            // 8/19/2009 dd - I cannot use CFannieMae32ImporterData. The reason is that CFannieMae32ImporterData will not
            // override existing value for field already contains data. CFannieMae32ImporterData should only be use for import with a template.
            // Since we are import into existing file. Override everything from new FNMA data.
            if (this.IsByPassPermission)
            { 
                m_dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(FannieMae32Importer));
                m_dataLoan.DisableFieldEnforcement();
            }
            else
            {
                m_dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMae32Importer));
            }

            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            m_dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlus);

            // 2/18/2010 dd - OPM 41755 - Pull the Institution ID from the FNMA 3.2
            m_dataLoan.sDuLenderInstitutionId = m_mornetDoc.sFannieInstitutionId;

            ClearExistingLoan();
            ParseLoan1003(m_mornetDoc._1003);
            ParseAdditionalCaseData(m_mornetDoc.AdditionalCaseData);
            ParseLoanProductData(m_mornetDoc.LoanProductData);
            ParseGovernmentData(m_mornetDoc.GovernmentLoanData);
            ParseCommunityLendingData(m_mornetDoc.CommunityLendingData);
            ImportGrossRent();

            Commit();            
        }

        public bool HasApplicantConflict(string fnmaContent, Guid sLId, out List<ApplicantInfoCompareResult> result)
        {
            string[] lines = fnmaContent.Split('\n');

            MORNETPlus32Document mornetDoc = new MORNETPlus32Document();
            mornetDoc.Load(lines);

            List<ApplicantInfo> fnmaApplicants = new List<ApplicantInfo>();

            foreach (MORNETPlusSegment segment in mornetDoc._1003.GetSegment("03A"))
            {
                bool isBorrower = segment.Get("03A-020") == "BW";
                string aSSn = segment.Get("03A-030");
                string aFirstNm = segment.Get("03A-040");
                string aMidNm = segment.Get("03A-050");
                string aLastNm = segment.Get("03A-060");
                string aSuffix = segment.Get("03A-070");
                string aNm = Tools.ComposeFullName(aFirstNm, aMidNm, aLastNm, aSuffix);
                string crossReferenceSsn = segment.Get("03A-140");

                if (isBorrower)
                {
                    fnmaApplicants.Add(new ApplicantInfo() { aBSsn = aSSn, aBNm = aNm, aCSsn = crossReferenceSsn });
                }
                else
                {
                    bool isMatch = false;
                    foreach (var o in fnmaApplicants)
                    {
                        if (o.aBSsn == crossReferenceSsn)
                        {
                            o.aCSsn = aSSn;
                            o.aCNm = aNm;
                            isMatch = true;
                            break;
                        }
                    }
                    if (!isMatch)
                    {
                        throw new CBaseException("Invalid FNMA 3.2 file.", "Could not find SSN of applicant");
                    }
                }
            }

            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FannieMae32Importer));
            dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlus);
            dataLoan.InitLoad();

            List<ApplicantInfo> loanApplicants = new List<ApplicantInfo>();
            for (int i = 0; i < dataLoan.nApps; i++)
            {
                CAppData dataApp = dataLoan.GetAppData(i);
                loanApplicants.Add(new ApplicantInfo() 
                { 
                    aBNm = dataApp.aBNm, aBSsn = dataApp.aBSsn, aCNm = dataApp.aCNm, aCSsn = dataApp.aCSsn, aAppId = dataApp.aAppId 
                });
            }

            return !IsMatch(loanApplicants, fnmaApplicants, out result);

        }

        public bool IsMatch(List<ApplicantInfo> oldDataList, List<ApplicantInfo> newDataList, out List<ApplicantInfoCompareResult> result)
        {
            bool isMatch = true;

            result = new List<ApplicantInfoCompareResult>();
            int oldDataCount = oldDataList.Count;

            List<int> newDataIndexList = new List<int>();
            for (int i = 0; i < newDataList.Count; i++)
            {
                newDataIndexList.Add(i);
            }
            for (int i = 0; i < oldDataCount; i++)
            {
                ApplicantInfo oldData = oldDataList[i];

                bool isFoundInNewList = false;
                foreach (int j in newDataIndexList)
                {
                    ApplicantInfo newData = newDataList[j];

                    if (oldData.IsEquals(newData))
                    {
                        result.Add(new ApplicantInfoCompareResult() { Result = E_ComparisionResult.Matched, OldData = oldData, NewData = newData });

                        newDataIndexList.Remove(j);
                        isFoundInNewList = true;
                        break;
                    }
                }
                if (!isFoundInNewList)
                {
                    result.Add(new ApplicantInfoCompareResult() { Result = E_ComparisionResult.Deleted, OldData = oldData });
                    isMatch = false;
                }
            }
            foreach (int j in newDataIndexList)
            {
                result.Add(new ApplicantInfoCompareResult() { Result = E_ComparisionResult.Added, NewData = newDataList[j] });
                isMatch = false;
            }
            return isMatch;

        }
        /// <summary>
        /// Only create new loan file for now.
        /// </summary>
        /// <param name="srcLoanID"></param>
        /// <param name="fileName"></param>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public CPageData Import(string fileName, AbstractUserPrincipal principal, Guid templateId) 
        {
            m_mornetDoc = new MORNETPlus32Document();
            m_mornetDoc.Load(fileName);
            return ImportImpl(m_mornetDoc, principal, templateId);
        }

        /// <summary>
        /// Only create new loan file for now.
        /// </summary>
        /// <param name="srcLoanID"></param>
        /// <param name="fileName"></param>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public CPageData Import(StreamReader stream, AbstractUserPrincipal principal, Guid templateId) 
        {
            m_mornetDoc = new MORNETPlus32Document();
            m_mornetDoc.Load(stream);
            return ImportImpl(m_mornetDoc, principal, templateId);

        }
        /// <summary>
        /// Only create new loan file for now.
        /// </summary>
        /// <param name="srcLoanID"></param>
        /// <param name="fileName"></param>
        /// <param name="employeeID"></param>
        /// <returns></returns>
        public CPageData Import(string[] lines, AbstractUserPrincipal principal, Guid templateId) 
        {
            return Import(lines, principal, principal.BranchId, templateId);
        }
        public CPageData Import(string[] lines, AbstractUserPrincipal principal, Guid branchId, Guid templateId) 
        {
            m_mornetDoc = new MORNETPlus32Document();
            m_mornetDoc.Load(lines);

            return ImportImpl(m_mornetDoc, principal, branchId, templateId);
        }

        private CPageData ImportImpl(MORNETPlus32Document doc, AbstractUserPrincipal principal, Guid templateId)
        {
            return ImportImpl(doc, principal, principal.BranchId, templateId);
        }
        private CPageData ImportImpl(MORNETPlus32Document doc, AbstractUserPrincipal principal, Guid branchId, Guid templateId) 
        {
            m_principal = principal;
            m_branchId = branchId;
            m_templateId = templateId;
            Initialize();
            m_dataLoan.sDuCaseId = doc.sDuCaseId;
            // ejm opm 467549 - We want to use the HMDA GMI data coming from the import file. The import file uses existance as true/false for the race data. 
            // So we need to clear out the HMDA GMI data before applying it to match what the import file has. 
            this.ClearHmdaInformation(); 
            ParseLoan1003(doc._1003);
            ParseAdditionalCaseData(doc.AdditionalCaseData);
            ParseLoanProductData(doc.LoanProductData);
            ParseGovernmentData(doc.GovernmentLoanData);
            ParseCommunityLendingData(doc.CommunityLendingData);
            ImportGrossRent();
            Commit();
            return m_dataLoan;
        }
        protected virtual void Initialize() 
        {
            m_loanFileCreator = CLoanFileCreator.GetCreator(m_principal, m_branchId, LendersOffice.Audit.E_LoanCreationSource.FannieMaeImport);
            Guid sLId = Guid.Empty;

            if (CreateLead)
            {
                sLId = m_loanFileCreator.BeginCreateLead(m_templateId, addEmployeeAsOfficialAgent: m_addEmployeeAsOfficialAgent);
            }
            else
            {
                sLId = m_loanFileCreator.BeginCreateImportBaseLoanFile(
                        sourceFileId: m_templateId,
                        setInitialEmployeeRoles: true,
                        addEmployeeOfficialAgent: m_addEmployeeAsOfficialAgent,
                        assignEmployeesFromRelationships: true,
                        branchIdToUse: m_branchId);
            }

            m_dataLoan = new CFannieMae32ImporterData(sLId);
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            m_dataLoan.SetFormatTarget(FormatTarget.FannieMaeMornetPlus);
        }
        /// <summary>
        /// Return the dataApp that contains SSN either for borrower or co-borrower. Return NULL, if dataApp could not be found.
        /// 
        /// All dataApp must be available when doing this search.
        /// </summary>
        /// <param name="ssn"></param>
        /// <returns></returns>
        private CAppData FindDataAppBySsn(string ssn)
        {
            // 12/12/2006 nw - OPM 8483 - If the FNMA file is missing the cross reference SSNs, return null
            if (string.IsNullOrEmpty(ssn))
            {
                return null;
            }

            ssn = ssn.Replace("-", string.Empty);
            int appCount = m_dataLoan.nApps;

            for (int i = 0; i < appCount; i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);

                if (ssn.Equals(dataApp.aBSsn.Replace("-", string.Empty)))
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    return dataApp;
                }
                else if (ssn.Equals(dataApp.aCSsn.Replace("-", string.Empty)))
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    return dataApp;
                }
            }

            return null;
        }

        private void ClearExistingLoan()
        {
            // HMDA information should always be cleared.
            this.ClearHmdaInformation();

            if (PrincipalFactory.CurrentPrincipal.BrokerId != new Guid("baf07293-f626-44c6-9750-706199a7c0b6"))
            {
                return;
            }
            m_dataLoan.sFHALenderIdCode = "";
            m_dataLoan.sFfUfmip1003Lckd = false;
            m_dataLoan.sFfUfmipR_rep = "";
            m_dataLoan.sMipPiaMon_rep = "";
            m_dataLoan.sProMInsR_rep = "";
            m_dataLoan.sProMInsLckd = false;
            m_dataLoan.sProMIns = 0;
            m_dataLoan.sProdIsFhaMipFinanced = false;
            m_dataLoan.sFfUfmipR = 0;
            m_dataLoan.sLAmtCalc = 0;
            m_dataLoan.sUfCashPd = 0;
            m_dataLoan.sMultiApps = false;
            m_dataLoan.sLT = 0;
            m_dataLoan.sLTODesc = "";
            m_dataLoan.sAgencyCaseNum = "";
            m_dataLoan.sLenderCaseNumLckd = false;
            m_dataLoan.sLenderCaseNum = "";
            m_dataLoan.sLAmtLckd = false;
            m_dataLoan.sLAmtCalc_rep = "";
            m_dataLoan.sFinMethodPrintAsOther = false;
            m_dataLoan.sFinMethPrintAsOtherDesc = "";
            m_dataLoan.sFinMethDesc = "";
            m_dataLoan.sSpAddr = "";
            m_dataLoan.sSpCity = "";
            m_dataLoan.sSpState = "";
            m_dataLoan.sSpZip = "";
            m_dataLoan.sUnitsNum_rep = "";
            m_dataLoan.sSpLegalDesc = "";
            m_dataLoan.sYrBuilt = "";
            m_dataLoan.sLPurposeT = 0;
            m_dataLoan.sOLPurposeDesc = "";
            m_dataLoan.sEstateHeldT = 0;
            m_dataLoan.sLeaseHoldExpireD_rep = "";
            m_dataLoan.sLotAcqYr = "";
            m_dataLoan.sLotOrigC_rep = "";
            m_dataLoan.sLotLien_rep = "";
            m_dataLoan.sPresentValOfLot_rep = "";
            m_dataLoan.sLotImprovC_rep = "";
            m_dataLoan.sSpAcqYr = "";
            m_dataLoan.sSpOrigC_rep = "";
            m_dataLoan.sSpLien_rep = "";
            m_dataLoan.sRefPurpose = "";
            m_dataLoan.sSpImprovDesc = "";
            m_dataLoan.sSpImprovTimeFrameT = 0;
            m_dataLoan.sSpImprovC_rep = "";
            m_dataLoan.sDwnPmtSrc = "";
            m_dataLoan.sEquityCalc_rep = "";
            m_dataLoan.sDwnPmtSrcExplain = "";
            m_dataLoan.sProHazInsR_rep = "";
            m_dataLoan.sProHazInsMb_rep = "";
            m_dataLoan.sProRealETxR_rep = "";
            m_dataLoan.sProRealETxMb_rep = "";
            m_dataLoan.sProMIns_rep = "";
            m_dataLoan.sProHoAssocDues_rep = "";
            m_dataLoan.sProOHExp_rep = "";
            m_dataLoan.sProOHExpLckd = false;
            m_dataLoan.sSpCountRentalIForPrimaryResidToo = false;
            m_dataLoan.sPurchPrice_rep = "";
            m_dataLoan.sAltCost_rep = "";
            m_dataLoan.sLandCost_rep = "";
            m_dataLoan.sRefPdOffAmt1003Lckd = false;
            m_dataLoan.sRefPdOffAmt1003_rep = "";
            m_dataLoan.sTotEstPp1003Lckd = false;
            m_dataLoan.sTotEstPp1003_rep = "";
            m_dataLoan.sTotEstCc1003Lckd = false;
            m_dataLoan.sTotEstCcNoDiscnt1003_rep = "";
            m_dataLoan.sFfUfmip1003_rep = "";
            m_dataLoan.sLDiscnt1003Lckd = false;
            m_dataLoan.sLDiscnt1003_rep = "";
            m_dataLoan.sIsOFinNew = false;
            m_dataLoan.sConcurSubFin_rep = "";
            m_dataLoan.s1stMtgOrigLAmt_rep = "";
            m_dataLoan.sTotCcPbsLocked = false;
            m_dataLoan.sTotCcPbs_rep = "";
            m_dataLoan.sFfUfMipIsBeingFinanced = false;
            m_dataLoan.sUfCashPdLckd = false;
            m_dataLoan.sOCredit1Amt_rep = "";
            m_dataLoan.sOCredit2Desc = "";
            m_dataLoan.sOCredit3Desc = "";
            m_dataLoan.sOCredit4Desc = "";
            m_dataLoan.sOCredit2Amt_rep = "";
            m_dataLoan.sOCredit3Amt_rep = "";
            m_dataLoan.sOCredit4Amt_rep = "";
            m_dataLoan.sOCredit4Amt = 0;
            m_dataLoan.sIsSellerProvidedBelowMktFin = "";
            m_dataLoan.sApprVal_rep = "";
            m_dataLoan.sLienPosT = 0;
            m_dataLoan.sFannieDocT = 0;
            m_dataLoan.sFannieSpT = 0;
            m_dataLoan.sSpProjectClassFannieT = 0;
            m_dataLoan.sPmtAdjMaxBalPc_rep = "";
            m_dataLoan.sRAdjLifeCapR_rep = "";
            m_dataLoan.sWillEscrowBeWaived = false;
            m_dataLoan.sEstCloseD_rep = "";
            m_dataLoan.sEstCloseDLckd = false;
            m_dataLoan.sSchedDueD1_rep = "";
            m_dataLoan.sSchedDueD1Lckd = false;
            m_dataLoan.sMInsCoverPc_rep = "";
            m_dataLoan.sHmdaAprRateSpread = "";
            m_dataLoan.sHmdaReportAsHoepaLoan = false;
            m_dataLoan.sHmdaPreapprovalT = 0;
            m_dataLoan.sFannieProdDesc = "";
            m_dataLoan.sFannieARMPlanNum = "";
            m_dataLoan.sFannieARMPlanNumLckd = false;
            m_dataLoan.sAssumeLT = 0;
            m_dataLoan.sPrepmtPenaltyT = 0;
            m_dataLoan.sIOnlyMon_rep = "";
            m_dataLoan.sRAdjIndexR_rep = "";
            m_dataLoan.sArmIndexT = 0;
            m_dataLoan.sArmIndexTLckd = false;
            m_dataLoan.sFHASponsorAgentIdCode = "";
            m_dataLoan.sFHASellerContribution_rep = "";
            m_dataLoan.sFHACcPbs_rep = "";
            m_dataLoan.sFHARefinanceTypeDesc = "";
            m_dataLoan.sFHAPurposeIsStreamlineRefi = false;
            m_dataLoan.sFHAPurposeIsStreamlineRefiWithAppr = false;
            m_dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr = false;
            m_dataLoan.sVaLCodeT = 0;
            m_dataLoan.sTotalScoreRefiT = 0;
            m_dataLoan.sSpCounty = "";
            m_dataLoan.sFHAHousingActSection = "";
            m_dataLoan.sTotalScoreFhaProductT = 0;
            m_dataLoan.sFHASalesConcessions_rep = "";
            m_dataLoan.sIsRenovationLoan = false;
            m_dataLoan.sVaProMaintenancePmt_rep = "";
            m_dataLoan.sVaProUtilityPmt_rep = "";
            m_dataLoan.sIsCommunityLending = false;
            m_dataLoan.sFannieMsa = "";
            m_dataLoan.sFannieCommunityLendingT = 0;
            m_dataLoan.sIsFannieNeighbors = false;
            m_dataLoan.sIsCommunitySecond = false;
            m_dataLoan.sFannieIncomeLimitAdjPc_rep = "";
            m_dataLoan.sOccR = 0;
            m_dataLoan.sSpGrossRent = 0;
            m_dataLoan.sNumFinancedProperties_rep = "";

            if (!m_dataLoan.sIsRateLocked)
            {
                m_dataLoan.sNoteIR_rep = "";
                m_dataLoan.sTerm_rep = "";
                m_dataLoan.sDue_rep = "";
                m_dataLoan.sRAdjMarginR_rep = "";
                m_dataLoan.sQualIR_rep = "";
                m_dataLoan.sQualIRLckd = false;
            }

            if (m_dataLoan.sIsIncomeCollectionEnabled)
            {
                m_dataLoan.ClearIncomeSources();
            }

            for (int i = 0; i < (m_dataLoan.nApps * 2); i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i/2);
                dataApp.BorrowerModeT = i % 2 == 0 ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;
                dataApp.aPresRent_rep = "";
                dataApp.aPres1stM_rep = "";
                dataApp.aPresOFin_rep = "";
                dataApp.aBFirstNm = "";
                dataApp.aBMidNm = "";
                dataApp.aBLastNm = "";
                dataApp.aBSuffix = "";
                dataApp.aBHPhone = "";
                dataApp.aBAge_rep = "";
                dataApp.aBSchoolYrs_rep = "";
                dataApp.aBMaritalStatT = 0;
                dataApp.aBDependNum_rep = "";
                dataApp.aBDob_rep = "";
                dataApp.aBEmail = "";
                dataApp.aCFirstNm = "";
                dataApp.aCMidNm = "";
                dataApp.aCLastNm = "";
                dataApp.aCSuffix = "";
                dataApp.aCHPhone = "";
                dataApp.aCAge_rep = "";
                dataApp.aCSchoolYrs_rep = "";
                dataApp.aCMaritalStatT = 0;
                dataApp.aCDependNum_rep = "";
                dataApp.aCDob_rep = "";
                dataApp.aCEmail = "";
                dataApp.aAsstLiaCompletedNotJointly = false;
                dataApp.aOccT = 0;
                dataApp.aBDependAges = "";
                dataApp.aCDependAges = "";
                dataApp.aBAddr = "";
                dataApp.aBCity = "";
                dataApp.aBState = "";
                dataApp.aBZip = "";
                dataApp.aBAddrT = 0;
                dataApp.aBAddrYrs = "";
                dataApp.aBAddrMailUsePresentAddr = false;
                dataApp.aBAddrMail = "";
                dataApp.aBCityMail = "";
                dataApp.aBStateMail = "";
                dataApp.aBZipMail = "";
                dataApp.aBPrev1Addr = "";
                dataApp.aBPrev1City = "";
                dataApp.aBPrev1State = "";
                dataApp.aBPrev1Zip = "";
                dataApp.aBPrev1AddrT = 0;
                dataApp.aBPrev1AddrYrs = "";
                dataApp.aBPrev2Addr = "";
                dataApp.aBPrev2City = "";
                dataApp.aBPrev2State = "";
                dataApp.aBPrev2Zip = "";
                dataApp.aBPrev2AddrT = 0;
                dataApp.aBPrev2AddrYrs = "";
                dataApp.aCAddr = "";
                dataApp.aCCity = "";
                dataApp.aCState = "";
                dataApp.aCZip = "";
                dataApp.aCAddrT = 0;
                dataApp.aCAddrYrs = "";
                dataApp.aCAddrMailUsePresentAddr = false;
                dataApp.aCAddrMail = "";
                dataApp.aCCityMail = "";
                dataApp.aCStateMail = "";
                dataApp.aCZipMail = "";
                dataApp.aCPrev1Addr = "";
                dataApp.aCPrev1City = "";
                dataApp.aCPrev1State = "";
                dataApp.aCPrev1Zip = "";
                dataApp.aCPrev1AddrT = 0;
                dataApp.aCPrev1AddrYrs = "";
                dataApp.aCPrev2Addr = "";
                dataApp.aCPrev2City = "";
                dataApp.aCPrev2State = "";
                dataApp.aCPrev2Zip = "";
                dataApp.aCPrev2AddrT = 0;
                dataApp.aCPrev2AddrYrs = "";
                dataApp.aPresTotHExpLckd = false;
                dataApp.aPresTotHExpCalc_rep = "";
                dataApp.aPresTotHExpDesc = "";
                dataApp.aBBusPhone = "";
                dataApp.aCBusPhone = "";
                dataApp.aPresHazIns_rep = "";
                dataApp.aPresRealETx_rep = "";
                dataApp.aPresMIns_rep = "";
                dataApp.aPresHoAssocDues_rep = "";
                dataApp.aPresOHExp_rep = "";

                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                {
                    dataApp.aBaseI_rep = "";
                    dataApp.aOvertimeI_rep = "";
                    dataApp.aBonusesI_rep = "";
                    dataApp.aCommisionI_rep = "";
                    dataApp.aDividendI_rep = "";
                }

                dataApp.aNetRentI1003Lckd = false;
                dataApp.aNetRentI1003_rep = "";
                dataApp.aOpNegCfLckd = false;
                dataApp.aOpNegCf_rep = "";
                dataApp.aBDecJudgment = "";
                dataApp.aBDecBankrupt = "";
                dataApp.aBDecForeclosure = "";
                dataApp.aBDecLawsuit = "";
                dataApp.aBDecObligated = "";
                dataApp.aBDecDelinquent = "";
                dataApp.aBDecAlimony = "";
                dataApp.aBDecBorrowing = "";
                dataApp.aBDecEndorser = "";
                dataApp.aBDecCitizen = "";
                dataApp.aBDecResidency = "";
                dataApp.aBDecOcc = "";
                dataApp.aBDecPastOwnership = "";
                dataApp.aBDecPastOwnedPropT = 0;
                dataApp.aBDecPastOwnedPropTitleT = 0;
                dataApp.aCDecJudgment = "";
                dataApp.aCDecBankrupt = "";
                dataApp.aCDecForeclosure = "";
                dataApp.aCDecLawsuit = "";
                dataApp.aCDecObligated = "";
                dataApp.aCDecDelinquent = "";
                dataApp.aCDecAlimony = "";
                dataApp.aCDecBorrowing = "";
                dataApp.aCDecEndorser = "";
                dataApp.aCDecCitizen = "";
                dataApp.aCDecResidency = "";
                dataApp.aCDecOcc = "";
                dataApp.aCDecPastOwnership = "";
                dataApp.aCDecPastOwnedPropT = 0;
                dataApp.aCDecPastOwnedPropTitleT = 0;
                dataApp.a1003SignD_rep = "";
                dataApp.aBNoFurnish = false;
                dataApp.aCNoFurnish = false;
                dataApp.aVaEntitleAmt_rep = "";
                dataApp.aVaBStateITax_rep = "";
                dataApp.aVaBSsnTax_rep = "";
                dataApp.aVaBONetI_rep = "";
                dataApp.aVaBEmplmtI_rep = "";
                dataApp.aVaCStateITax_rep = "";
                dataApp.aVaCSsnTax_rep = "";
                dataApp.aVaCONetI_rep = "";
                dataApp.aVaCEmplmtI_rep = "";
                dataApp.aFHABCaivrsNum = "";
                dataApp.aBTotalScoreFhtbCounselingT = 0;
                dataApp.aFHACCaivrsNum = "";
                dataApp.aCTotalScoreFhtbCounselingT = 0;

                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                {
                    dataApp.aOtherIncomeList = new List<OtherIncome>();
                }
            }
        }

        /// <summary>
        /// Clears all HMDA information from each application on file.
        /// </summary>
        private void ClearHmdaInformation()
        {
            int appCount = this.m_dataLoan.nApps;
            for (int i = 0; i < appCount; i++)
            {
                var dataApp = this.m_dataLoan.GetAppData(i);

                dataApp.aBGender = E_GenderT.LeaveBlank;
                dataApp.aCGender = E_GenderT.LeaveBlank;
                dataApp.aBInterviewMethodT = E_aIntrvwrMethodT.LeaveBlank;
                dataApp.aCInterviewMethodT = E_aIntrvwrMethodT.LeaveBlank;
                dataApp.aBSexCollectedByObservationOrSurname = E_TriState.Blank;
                dataApp.aCSexCollectedByObservationOrSurname = E_TriState.Blank;
                dataApp.aBDoesNotWishToProvideEthnicity = false;
                dataApp.aCDoesNotWishToProvideEthnicity = false;
                dataApp.aBEthnicityCollectedByObservationOrSurname = E_TriState.Blank;
                dataApp.aCEthnicityCollectedByObservationOrSurname = E_TriState.Blank;
                dataApp.aBHispanicT = E_aHispanicT.LeaveBlank;
                dataApp.aCHispanicT = E_aHispanicT.LeaveBlank;
                dataApp.aBIsCuban = false;
                dataApp.aCIsCuban = false;
                dataApp.aBIsMexican = false;
                dataApp.aCIsMexican = false;
                dataApp.aBIsPuertoRican = false;
                dataApp.aCIsPuertoRican = false;
                dataApp.aBIsOtherHispanicOrLatino = false;
                dataApp.aCIsOtherHispanicOrLatino = false;
                dataApp.aBOtherHispanicOrLatinoDescription = string.Empty;
                dataApp.aCOtherHispanicOrLatinoDescription = string.Empty;
                dataApp.aBDoesNotWishToProvideEthnicity = false;
                dataApp.aCDoesNotWishToProvideEthnicity = false;
                dataApp.aBEthnicityCollectedByObservationOrSurname = E_TriState.Blank;
                dataApp.aCEthnicityCollectedByObservationOrSurname = E_TriState.Blank;
                dataApp.aBIsAmericanIndian = false;
                dataApp.aCIsAmericanIndian = false;
                dataApp.aBOtherAmericanIndianDescription = string.Empty;
                dataApp.aCOtherAmericanIndianDescription = string.Empty;
                dataApp.aBIsBlack = false;
                dataApp.aCIsBlack = false;
                dataApp.aBIsWhite = false;
                dataApp.aCIsWhite = false;
                dataApp.aBIsAsian = false;
                dataApp.aCIsAsian = false;
                dataApp.aBIsAsianIndian = false;
                dataApp.aCIsAsianIndian = false;
                dataApp.aBIsChinese = false;
                dataApp.aCIsChinese = false;
                dataApp.aBIsFilipino = false;
                dataApp.aCIsFilipino = false;
                dataApp.aBIsJapanese = false;
                dataApp.aCIsJapanese = false;
                dataApp.aBIsKorean = false;
                dataApp.aCIsKorean = false;
                dataApp.aBIsVietnamese = false;
                dataApp.aCIsVietnamese = false;
                dataApp.aBIsOtherAsian = false;
                dataApp.aCIsOtherAsian = false;
                dataApp.aBOtherAsianDescription = string.Empty;
                dataApp.aCOtherAsianDescription = string.Empty;
                dataApp.aBIsPacificIslander = false;
                dataApp.aCIsPacificIslander = false;
                dataApp.aBIsNativeHawaiian = false;
                dataApp.aCIsNativeHawaiian = false;
                dataApp.aBIsGuamanianOrChamorro = false;
                dataApp.aCIsGuamanianOrChamorro = false;
                dataApp.aBIsSamoan = false;
                dataApp.aCIsSamoan = false;
                dataApp.aBIsOtherPacificIslander = false;
                dataApp.aCIsOtherPacificIslander = false;
                dataApp.aBOtherPacificIslanderDescription = string.Empty;
                dataApp.aCOtherPacificIslanderDescription = string.Empty;
                dataApp.aBDoesNotWishToProvideRace = false;
                dataApp.aCDoesNotWishToProvideRace = false;
                dataApp.aBRaceCollectedByObservationOrSurname = E_TriState.Blank;
                dataApp.aCRaceCollectedByObservationOrSurname = E_TriState.Blank;
            }
        }

        private void Commit() 
        {
            int nApps = m_dataLoan.nApps;
            for (int i = 0; i < nApps; i++) 
            {
                m_dataLoan.GetAppData(i).aLiaCollection.Flush();
            }
            if (m_fannieMaeImportSource == FannieMaeImportSource.PriceMyLoan) 
            {
                m_dataLoan.TransformDataToPml(E_TransformToPmlT.FromFannieMae /* ,Guid.Empty */);

            }
            else if ((m_fannieMaeImportSource == FannieMaeImportSource.H4H) || (m_fannieMaeImportSource == FannieMaeImportSource.AUD))
            {
                AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

                BranchDB branchDB = new BranchDB(principal.BranchId, principal.BrokerId);
                branchDB.Retrieve();
                m_dataLoan.sFHALenderIdCode = branchDB.BranchCode;
                m_dataLoan.AssignBranch(principal.BranchId);

                if (m_fannieMaeImportSource == FannieMaeImportSource.H4H)
                {
                    // 12/11/2009 dd - For H4H import apply the correct value for UFMIP and monthly MIP on import.
                    // sFfUfmipR = 2%
                    // sMipPiaMon = 12
                    // If (sUfCashPd > 0) then set sUfCashPd = sFfUfmip1003 rounded down to nearest dollar.
                    // sProMInsR = .75%
                    // sProMInsLckd is true
                    // sProMIns = sLAmtCalc * sProMInsR / 12

                    bool isMipFinanced = false;
                    if (m_dataLoan.sFfUfmipFinanced > 0)
                    {
                        // 12/14/2009 dd - Need to retrieve whether the original fnma file has MIP finance.
                        isMipFinanced = true;
                    }
                    m_dataLoan.sFfUfmip1003Lckd = false; // 12/21/2009 dd - This to force sFfUfmip1003 calculate base on 2% OPM 43679.
                    m_dataLoan.sFfUfmipR_rep = "2";
                    m_dataLoan.sMipPiaMon_rep = "12";
                    m_dataLoan.sProMInsR_rep = ".75";
                    m_dataLoan.sProMInsLckd = true;
                    m_dataLoan.sProMIns = m_dataLoan.sLAmtCalc * m_dataLoan.sProMInsR / 1200;
                    m_dataLoan.sProdIsFhaMipFinanced = isMipFinanced;
                }
                if (m_fannieMaeImportSource == FannieMaeImportSource.AUD && !m_IssFfUfmipRSet)
                {
                    m_dataLoan.sFfUfmipR = 0;
                    if (m_dataLoan.sLAmtCalc != 0)
                    {
                        m_dataLoan.sFfUfmipR = Math.Round(100 * (m_dataLoan.sFfUfmip1003 / m_dataLoan.sLAmtCalc), 3);
                    }
                }
                
                
                // OPM 32441 - this will automatically calculate now
                /*if (isMipFinanced)
                {
                    m_dataLoan.sUfCashPd = m_dataLoan.sFfUfmip1003 - Math.Floor(m_dataLoan.sFfUfmip1003);
                }
                else
                {
                    m_dataLoan.sUfCashPd = m_dataLoan.sFfUfmip1003;
                }*/

                for (int i = 0; i < nApps; i++)
                {
                    CAppData dataApp = m_dataLoan.GetAppData(i);
                    if (m_fannieMaeImportSource == FannieMaeImportSource.H4H)
                    {
                        dataApp.aPresRent_rep = ""; // 3/10/2010 dd - OPM 43150 - When import for H4H wipe out present rent.
                        dataApp.aPres1stM_rep = ""; // 3/10/2010 dd - OPM 43150 - When import for H4H wipe out present rent.
                        dataApp.aPresOFin_rep = ""; // 3/10/2010 dd - OPM 43150 - When import for H4H wipe out present rent.
                    }
                    
                    ILiaCollection aLiaCollection = dataApp.aLiaCollection;
                    foreach (ILiabilityRegular lia in aLiaCollection.GetSubcollection(true, E_DebtGroupT.Mortgage))
                    {
                        if (lia.IsSubjectPropertyMortgage && lia.FullyIndexedPITIPayment <= 0)
                        {
                            lia.FullyIndexedPITIPayment = lia.Pmt;
                            lia.Update();
                        }
                    }
                }
                // 12/17/2009 dd - OPM 43610 - Use appraisal value for value of subject property reo.
                if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout ||
                    m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity)
                {
                    bool hasSubjPropertyReo = false;

                    for (int i = 0; i < nApps; i++)
                    {
                        CAppData dataApp = m_dataLoan.GetAppData(i);

                        #region Update REO with appraised value.
                        var reCollection = dataApp.aReCollection;
                        foreach (var item in reCollection.GetSubcollection(true, E_ReoGroupT.All))
                        {
                            var reo = (IRealEstateOwned)item;
                            if (reo.IsSubjectProp)
                            {
                                hasSubjPropertyReo = true;

                                if (reo.Val <= 0)
                                {
                                    reo.Val_rep = m_dataLoan.sApprVal_rep;
                                    reo.Update();
                                }
                                break;
                            }
                            else if (reo.Addr.ToLower() == m_dataLoan.sSpAddr.ToLower())
                            {
                                hasSubjPropertyReo = true;
                                if (reo.Val <= 0)
                                {
                                    reo.Val_rep = m_dataLoan.sApprVal_rep;
                                    reo.Update();
                                }
                                break;

                            }
                        }
                        if (hasSubjPropertyReo)
                        {
                            break;
                        }
                        #endregion
                    }
                    if (hasSubjPropertyReo == false)
                    {
                        // 12/17/2009 dd - If REO does not found then create reo and fill out appraise value.
                        CAppData dataApp = m_dataLoan.GetAppData(0);
                        var reo = dataApp.aReCollection.AddRegularRecord();
                        reo.IsSubjectProp = true;
                        reo.Addr = m_dataLoan.sSpAddr;
                        reo.City = m_dataLoan.sSpCity;
                        reo.State = m_dataLoan.sSpState;
                        reo.Zip = m_dataLoan.sSpZip;
                        reo.Val_rep = m_dataLoan.sApprVal_rep;
                        reo.Update();

                    }

                }
            }            
            m_dataLoan.Save();
            if (null != m_loanFileCreator) 
            {
                m_loanFileCreator.CommitFileCreation(false);
                // 8/2/2004 dd - The reason I set sLNm here is so that when I get dataLoan.sLNm it will be official name
                // instead of temporary loan name.
                m_dataLoan.SetsLNmWithPermissionBypass( m_loanFileCreator.LoanName);
            }

            //REMN hotfix - dont populate interviewee for them.
            if (PrincipalFactory.CurrentPrincipal.BrokerId != new Guid("baf07293-f626-44c6-9750-706199a7c0b6"))
            {
                //OPM 33815 - Populate interviewer to loan officer if loan officer is not already created for the loan and the interviewer exists in the file
                IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if ((interviewer != CPreparerFields.Empty) && (agent == CAgentFields.Empty))
                {
                    CAgentFields loanofficer = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                    interviewer.SyncToAgent(loanofficer);
                    loanofficer.Update();
                }
            }
        }

        public List<List<String>> GetBorrowerBasicInfo(MORNETPlusDataset ds)
        {
            List<List<String>> apps = new List<List<String>>();

            foreach (MORNETPlusSegment segment in ds.GetSegment("03A"))
            {
                List<String> data = new List<String>();
                data.Add(segment.Get("03A-030"));
                data.Add(segment.Get("03A-040"));
                data.Add(segment.Get("03A-060"));

                apps.Add(data);

            }
            return apps;
        }

        /// <summary>
        /// Table MDF-6
        /// </summary>
        /// <param name="ds"></param>
        private void ParseLoan1003(MORNETPlusDataset ds)
        {
            int index = 0;
            bool isBorrower = false;
            Hashtable hashTable = null;
            CAppData dataApp = null;
            YearMonthParser yearMonthParser = new YearMonthParser();
            m_totalSubjectPropertyNetCashflow = 0;
            m_hasSubjectPropNetCashFlow = false;

            if (m_clearExistingCollectionData)
            {
                if (m_dataLoan.sIsIncomeCollectionEnabled)
                {
                    m_dataLoan.ClearIncomeSources();
                }

                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    dataApp = m_dataLoan.GetAppData(i);
                    dataApp.aBEmpCollection.ClearAll();
                    dataApp.aCEmpCollection.ClearAll();

                    if (m_fannieMaeImportSource != FannieMaeImportSource.H4H && m_fannieMaeImportSource != FannieMaeImportSource.AUD)
                    {
                        dataApp.aLiaCollection.ClearAll();
                        dataApp.aAssetCollection.ClearAll();
                        dataApp.aReCollection.ClearAll();
                    }
                }
                dataApp = null;
            }

            #region 00A - Top of Form (S)
            // 00A-020 The income or assets of a person other than the Borrower (including the Applicant's spouse)
            // 00A-030 The income or assets of the Applicant's spouse will not be used as a basic for ...
            foreach (MORNETPlusSegment segment in ds.GetSegment("00A"))
            {
                m_dataLoan.sMultiApps = segment.GetBool("00A-020");
                m_dataLoan.GetAppData(0).aSpouseIExcl = segment.GetBool("00A-030");
            }
            #endregion

            #region 01A - Mortgage Type and Terms (S)
            // 01A-020 Mortgage Applied For
            // 01A-030 Mortgage Applied For (Other)
            // 01A-040 Agency Case Number
            // 01A-050 Case Number. A number assigned to the casefile by the lender or originator. The system will assign this number if not specified.
            // 01A-060 Loan Amount
            // 01A-070 Interest Rate
            // 01A-080 No. of months
            // 01A-090 Amortization Type
            // 01A-100 Amortization Type Other Explaination
            // 01A-110 ARM Textual Description
            bool hasLoanAmount = false;
            foreach (MORNETPlusSegment segment in ds.GetSegment("01A"))
            {
                m_dataLoan.sLT = TypeMap_sLT.ToLO(segment.Get("01A-020"));
                m_dataLoan.sLTODesc = segment.Get("01A-030");
                m_dataLoan.sAgencyCaseNum = segment.Get("01A-040");
                if (!m_dataLoan.sLenderCaseNum.Equals(segment.Get("01A-050")) && !string.IsNullOrEmpty(segment.Get("01A-050")))
                    m_dataLoan.sLenderCaseNumLckd = true;
                m_dataLoan.sLenderCaseNum = segment.Get("01A-050");
                if (!string.IsNullOrEmpty(segment.Get("01A-060")))
                {
                    hasLoanAmount = true;
                }
                if (AreSameValue(m_dataLoan.sLAmtCalc_rep, segment.Get("01A-060")) == false)
                {
                    m_dataLoan.sLAmtLckd = true; // 5/6/2004 dd - Set Loan Amount Lock on, so user can specify loan amount directly.
                    m_dataLoan.sLAmtCalc_rep = segment.Get("01A-060");
                }
                if (!m_dataLoan.sIsRateLocked)
                {
                    m_dataLoan.sNoteIR_rep = segment.Get("01A-070");
                    m_dataLoan.sTerm_rep = segment.Get("01A-080");

                }

                string _01A_090 = segment.Get("01A-090");

                if (_01A_090 == "13")
                {
                    m_dataLoan.sFinMethodPrintAsOther = true;
                }
                else
                {
                    if (!m_dataLoan.sIsRateLocked)
                    {
                        m_dataLoan.sFinMethT = TypeMap_sFinMethT.ToLO(segment.Get("01A-090"));
                    }
                }
                m_dataLoan.sFinMethPrintAsOtherDesc = segment.Get("01A-100");
                m_dataLoan.sFinMethDesc = segment.Get("01A-110");

            }
            #endregion

            #region 02A - Property Information (S)
            // 02A-020 Property Street Address
            // 02A-030 Property City
            // 02A-040 Property State
            // 02A-050 Property Zip Code
            // 02A-060 Property Zip Code + 4
            // 02A-070 No. of Units
            // 02A-080 Legal Description of Subject Property Code
            // 02A-090 Legal Description of Subject Property Text
            // 02A-100 Year Built
            foreach (MORNETPlusSegment segment in ds.GetSegment("02A"))
            {
                m_dataLoan.sSpAddr = segment.Get("02A-020");
                m_dataLoan.sSpCity = segment.Get("02A-030");
                m_dataLoan.sSpState = segment.Get("02A-040");
                m_dataLoan.sSpZip = segment.Get("02A-050");
                //                segment.Get("02A-060"); // 5/6/2004 dd - WE DON'T HAVE THIS. Property Zipcode Plus Four
                m_dataLoan.sUnitsNum_rep = segment.Get("02A-070");
                //                segment.Get("02A-080"); // 5/6/2004 dd - WE DON'T HAVE THIS. Legal Description Property Code
                m_dataLoan.sSpLegalDesc = segment.Get("02A-090");
                m_dataLoan.sYrBuilt = segment.Get("02A-100");

                //Auto Populate County with ZipCode
                string county = Tools.GetCountyFromZipCode(m_dataLoan.sSpZip);
                if (!string.IsNullOrEmpty(county))
                {
                    m_dataLoan.sSpCounty = county;
                }
            }
            #endregion

            #region PAI - Property Address Information. Segment is optional. If provided it will be used instead of the single address element (S)
            // PAI-020 House Number
            // PAI-030 Street Name
            // PAI-040 Unit Number

            // SKIP
            #endregion

            E_aOccT? cached_aOccT = new E_aOccT?();
            #region 02B - Purpose of Loan (S)
            // 02B-020 FannieMae reserved for future use.
            // 02B-030 Purpose of Loan
            // 02B-040 Purpose of Loan (Other). Text identifying the reason loan is needed if EDI code is "other"
            // 02B-050 Property will be
            // 02B-060 Manner in which title will be held
            // 02B-070 Estate will be held in
            // 02B-080 (Estate will be held in) Leasehold expiration date
            foreach (MORNETPlusSegment segment in ds.GetSegment("02B"))
            {
                m_dataLoan.sLPurposeT = TypeMap_sLPurposeT.ToLO(segment.Get("02B-030"));
                m_dataLoan.sOLPurposeDesc = segment.Get("02B-040");
                m_dataLoan.GetAppData(0).aOccT = TypeMap_aOccT.ToLO(segment.Get("02B-050"));
                cached_aOccT = m_dataLoan.GetAppData(0).aOccT;
                m_dataLoan.GetAppData(0).aManner = segment.Get("02B-060");
                m_dataLoan.sEstateHeldT = TypeMap_sEstateHeldT.ToLO(segment.Get("02B-070"));
                m_dataLoan.sLeaseHoldExpireD_rep = segment.Get("02B-080");
            }
            #endregion

            #region 02C - Title Holder (M)
            // 02C-020 Titleholder Name
            index = 0;
            foreach (MORNETPlusSegment segment in ds.GetSegment("02C"))
            {
                if (index == 0)
                    m_dataLoan.GetAppData(0).aTitleNm1 = segment.Get("02C-020");
                else
                    m_dataLoan.GetAppData(0).aTitleNm2 = segment.Get("02C-020");
                index++;
            }
            #endregion

            #region 02D - Construction to Permanent or Refinance Data (S)
            // 02D-020 Year Lot Acquired (Construction) or Year Acquired (Refinance)
            // 02D-030 Original Cost (Construction or Refinance)
            // 020-040 Amount of Existing Liens (Construction or Refinance)
            // 020-050 (a) Present Value of Lot
            // 020-060 (b) Cost of Improvements
            // 020-070 Purpose of Refinance
            // 020-080 Describe Improvements
            // 020-090 (Describe Improvements) made/to be made
            // 020-100 (Describe Improvements) Cost
            foreach (MORNETPlusSegment segment in ds.GetSegment("02D"))
            {
                if (m_dataLoan.sLPurposeT == E_sLPurposeT.Construct || m_dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm)
                {
                    m_dataLoan.sLotAcqYr = segment.Get("02D-020");
                    m_dataLoan.sLotOrigC_rep = segment.Get("02D-030");
                    m_dataLoan.sLotLien_rep = segment.Get("02D-040");
                    m_dataLoan.sPresentValOfLot_rep = segment.Get("02D-050");
                    m_dataLoan.sLotImprovC_rep = segment.Get("02D-060");
                }
                else if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                {
                    m_dataLoan.sSpAcqYr = segment.Get("02D-020");
                    m_dataLoan.sSpOrigC_rep = segment.Get("02D-030");
                    m_dataLoan.sSpLien_rep = segment.Get("02D-040");
                    m_dataLoan.sRefPurpose = TypeMap_sRefPurpose.ToLO(segment.Get("02D-070"));

                    if (m_dataLoan.sRefPurpose == "Cash-Out Rate/Term" ||
                        m_dataLoan.sRefPurpose == "Cash-Out/Home Improvement" ||
                        m_dataLoan.sRefPurpose == "Cash-Out/Debt Consolidation" ||
                        m_dataLoan.sRefPurpose == "Cash-Out/Other"
                        )
                    {
                        m_dataLoan.sLPurposeT = E_sLPurposeT.RefinCashout;
                    }
                    m_dataLoan.sSpImprovDesc = segment.Get("02D-080");
                    m_dataLoan.sSpImprovTimeFrameT = TypeMap_sSpImprovTimeFrameT.ToLO(segment.Get("02D-090"));
                    m_dataLoan.sSpImprovC_rep = segment.Get("02D-100");
                }
            }
            #endregion

            #region 02E - Down Payment (M)
            // 02E-020 Down Payment Type Code
            // 02E-030 Down Payment Amount
            // 02E-040 Down Payment Explanation

            int iNumDwnPmtGifts = 0;
            E_GiftFundSourceT eDwnPmtGiftSource = E_GiftFundSourceT.Blank;
            foreach (MORNETPlusSegment segment in ds.GetSegment("02E"))
            {
                string source = segment.Get("02E-020");
                m_dataLoan.sDwnPmtSrc = TypeMap_sDwnPmtSrc.ToLO(source);
                // If the downpayment has a gift source AND there's only one gift source then the largest
                //  asset of gift funds type will be given the downpayment gift source type (see region 06C).
                //  If there is 0 or more than one gift downpayment, do nothing. Spec in opm 90538
                if (IsDwnPmtSourceGift(source))
                {
                    iNumDwnPmtGifts++;
                    eDwnPmtGiftSource = ConvertToGiftSource(source);
                }
                if (!hasLoanAmount)
                {
                    m_dataLoan.sEquityCalc_rep = segment.Get("02E-030"); // 5/6/2004 dd - How to set sEquityCalc_rep, sLAmtLckd is set to true.
                    m_dataLoan.sDwnPmtSrcExplain = segment.Get("02E-040");
                }
            }
            #endregion

            #region 03A - Applicant(s) Data (SA)
            // 03A-020 Applicant/Co-Applicant Indicator
            // 03A-030 Applicant Social Security Number
            // 03A-040 Applicant First Name
            // 03A-050 Applicant Middle Name
            // 03A-060 Applicant Last Name
            // 03A-070 Applicant Generation
            // 03A-080 Home Phone
            // 03A-090 Age
            // 03A-100 Yrs. School
            // 03A-110 Marital Status
            // 03A-120 Dependents (no.)
            // 03A-130 Completed Jointly/Not Jointly
            // 03A-140 Cross Reference Number
            // 03A-150 Date of Birth
            // 03A-160 Email Address
            bool isPrimaryApp = true;
            foreach (MORNETPlusSegment segment in ds.GetSegment("03A"))
            {
                isBorrower = segment.Get("03A-020") == "BW";
                string aSsn = segment.Get("03A-030");
                string aCrossReferenceSsn = segment.Get("03A-140");

                // 1) Check to see if cross reference ssn already existed.
                // 2) If cross reference ssn does not existed then create new dataapp.
                // 10/21/2007 dd - We are now allow import into existing for DU Integration, we need to search for existing borrower ssn
                //                 before search the cross reference.
                dataApp = FindDataAppBySsn(aSsn);

                if (null == dataApp)
                {
                    dataApp = FindDataAppBySsn(aCrossReferenceSsn);
                    if (null == dataApp)
                    {
                        if (isPrimaryApp)
                        {
                            dataApp = m_dataLoan.GetAppData(0);
                            isPrimaryApp = false;
                        }
                        else
                        {
                            //05/17/08 19499 We only need the primary applicants for PML. 
                            //if they are not listed next to each then we cant break out.  This will pretty much stop any new ones. 
                            //while the if statement lets primary be created.  av 
                            //05/17/09 30671  changing the key off since we sometimes want to support multiple apps in pml.
                            if (!m_importMultipleApps)
                            {
                                continue;
                            }
                            m_dataLoan.Save();
                            int iApp = m_dataLoan.AddNewApp();
                            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                            dataApp = m_dataLoan.GetAppData(iApp);
                        }
                    }
                }
                isPrimaryApp = false; // 8/19/2009 dd - Primary app will set to false as soon as we set some data.
                dataApp.BorrowerModeT = isBorrower ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;
                dataApp.aSsn = aSsn;
                dataApp.aFirstNm = segment.Get("03A-040");
                dataApp.aMidNm = segment.Get("03A-050");
                dataApp.aLastNm = segment.Get("03A-060");
                dataApp.aSuffix = segment.Get("03A-070");
                dataApp.aHPhone = segment.Get("03A-080");
                dataApp.aAge_rep = segment.Get("03A-090");
                dataApp.aSchoolYrs_rep = segment.Get("03A-100");
                dataApp.aMaritalStatT = TypeMap_aMaritalStatT.ToLO(segment.Get("03A-110"));
                dataApp.aDependNum_rep = segment.Get("03A-120");
                dataApp.aAsstLiaCompletedNotJointly = !segment.GetBool("03A-130");
                dataApp.aDob_rep = segment.Get("03A-150");
                dataApp.aEmail = segment.Get("03A-160");
                if (cached_aOccT.HasValue)
                {
                    dataApp.aOccT = cached_aOccT.Value;
                }
            }
            #endregion

            #region 03B - Dependent's Age (M)
            // 03B-020 Applicant Social Security Number
            // 03B-030 Dependent's Age
            Dictionary<string, string> _hash = new Dictionary<string, string>();
            foreach (MORNETPlusSegment segment in ds.GetSegment("03B"))
            {
                string aSsn = segment.Get("03B-020");
                string dependentAge = segment.Get("03B-030");

                string ages = dependentAge;
                if (_hash.ContainsKey(aSsn))
                {
                    ages = _hash[aSsn] + ", " + dependentAge;
                }
                _hash[aSsn] = ages;
            }
            foreach (var o in _hash)
            {
                string aSsn = o.Key;
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;
                dataApp.BorrowerModeT = isBorrower ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;

                dataApp.aDependAges = o.Value;
                /*
                if (isBorrower) {
                    if (dataApp.aBDependAges.TrimWhitespaceAndBOM() != "") 
                    {
                        dataApp.aBDependAges += ", ";
                    }
                    dataApp.aBDependAges += segment.Get("03B-030");
                } 
                else 
                {
                    if (dataApp.aCDependAges.TrimWhitespaceAndBOM() != "") 
                    {
                        dataApp.aCDependAges += ", ";
                    }

                    dataApp.aCDependAges += segment.Get("03B-030");

                }
                 */
            }

            #endregion

            #region 03C - Applicant(s) Address (M)
            // 03C-020 Applicant Social Security Number
            // 03C-030 Present/Former/Mailing Address
            // 03C-040 Residence Street Address
            // 03C-050 Residence City
            // 03C-060 Residence State
            // 03C-070 Residence Zip Code
            // 03C-080 Residence Zip Code + 4
            // 03C-090 Own/Rent/Living Rent Free
            // 03C-100 No. Yrs
            // 03C-110 No. Months
            // 03C-120 Country
            hashTable = new Hashtable();
            foreach (MORNETPlusSegment segment in ds.GetSegment("03C"))
            {
                string aSsn = segment.Get("03C-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                bool isPresentAddr = segment.Get("03C-030") == "ZG";
                bool isMailingAddr = segment.Get("03C-030") == "BH";

                string aAddr = segment.Get("03C-040");
                string aCity = segment.Get("03C-050");
                string aState = segment.Get("03C-060");
                string aZip = segment.Get("03C-070");
                //                segment.Get("03C-080"); // 5/6/2004 dd - Don't have Zip code plus 4
                E_aBAddrT aAddrT = TypeMap_aAddrT.ToLO(segment.Get("03C-090"));
                yearMonthParser.ParseStr(segment.Get("03C-100"), segment.Get("03C-110"));
                string aAddrYrs = yearMonthParser.YearsInDecimal.ToString();
                if (isBorrower)
                {
                    if (isPresentAddr)
                    {
                        dataApp.aBAddr = aAddr;
                        dataApp.aBCity = aCity;
                        dataApp.aBState = aState;
                        dataApp.aBZip = aZip;
                        dataApp.aBAddrT = aAddrT;
                        dataApp.aBAddrYrs = aAddrYrs;
                    }
                    else if (isMailingAddr)
                    {
                        dataApp.aBAddrMailUsePresentAddr = false;
                        dataApp.aBAddrMail = aAddr;
                        dataApp.aBCityMail = aCity;
                        dataApp.aBStateMail = aState;
                        dataApp.aBZipMail = aZip;

                    }
                    else
                    {
                        int aPrevAddrIndex = 0;
                        if (hashTable.Contains(aSsn))
                        {
                            aPrevAddrIndex = (int)hashTable[aSsn];
                        }
                        if (aPrevAddrIndex == 0)
                        {
                            dataApp.aBPrev1Addr = aAddr;
                            dataApp.aBPrev1City = aCity;
                            dataApp.aBPrev1State = aState;
                            dataApp.aBPrev1Zip = aZip;
                            dataApp.aBPrev1AddrT = (E_aBPrev1AddrT)aAddrT;
                            dataApp.aBPrev1AddrYrs = aAddrYrs;
                            aPrevAddrIndex++;
                        }
                        else if (aPrevAddrIndex == 1)
                        {
                            dataApp.aBPrev2Addr = aAddr;
                            dataApp.aBPrev2City = aCity;
                            dataApp.aBPrev2State = aState;
                            dataApp.aBPrev2Zip = aZip;
                            dataApp.aBPrev2AddrT = (E_aBPrev2AddrT)aAddrT;
                            dataApp.aBPrev2AddrYrs = aAddrYrs;

                            aPrevAddrIndex++;
                        }
                        hashTable[aSsn] = aPrevAddrIndex;


                    }
                }
                else
                {
                    if (isPresentAddr)
                    {
                        dataApp.aCAddr = aAddr;
                        dataApp.aCCity = aCity;
                        dataApp.aCState = aState;
                        dataApp.aCZip = aZip;
                        dataApp.aCAddrT = (E_aCAddrT)aAddrT;
                        dataApp.aCAddrYrs = aAddrYrs;
                    }
                    else if (isMailingAddr)
                    {
                        dataApp.aCAddrMailUsePresentAddr = false;
                        dataApp.aCAddrMail = aAddr;
                        dataApp.aCCityMail = aCity;
                        dataApp.aCStateMail = aState;
                        dataApp.aCZipMail = aZip;
                    }
                    else
                    {
                        int aPrevAddrIndex = 0;
                        if (hashTable.Contains(aSsn))
                        {
                            aPrevAddrIndex = (int)hashTable[aSsn];
                        }
                        if (aPrevAddrIndex == 0)
                        {
                            dataApp.aCPrev1Addr = aAddr;
                            dataApp.aCPrev1City = aCity;
                            dataApp.aCPrev1State = aState;
                            dataApp.aCPrev1Zip = aZip;
                            dataApp.aCPrev1AddrT = (E_aCPrev1AddrT)aAddrT;
                            dataApp.aCPrev1AddrYrs = aAddrYrs;
                            aPrevAddrIndex++;
                        }
                        else if (aPrevAddrIndex == 1)
                        {
                            dataApp.aCPrev2Addr = aAddr;
                            dataApp.aCPrev2City = aCity;
                            dataApp.aCPrev2State = aState;
                            dataApp.aCPrev2Zip = aZip;
                            dataApp.aCPrev2AddrT = (E_aCPrev2AddrT)aAddrT;
                            dataApp.aCPrev2AddrYrs = aAddrYrs;

                            aPrevAddrIndex++;
                        }
                        hashTable[aSsn] = aPrevAddrIndex;

                    }


                }
            }
            #endregion

            // 5/26/2011 dd - I need to process the 06x series before process 05I income section. The reason is
            // Net rental income is calculate base on 06G reo and we do not to over lock the net rental income.

            // OPM 20691 - Temporarily un-retire this segment until Fannie Mae officially no longer accepts it
            // OPM 20691 7/14/10 - re-retiring this segment because it is officially no longer supported.  The code is just
            // commented so that it can be uncommented if an unknown situation related to this segment arises.
            // OPM 55950 9/2/10 - re-un-retiring the segment because some LOS have not upgraded their system to
            // handle this segment that has been retired for 2 years now and had a grace period even before that.
            #region 06A (TEMPORARILY UN-RETIRED) - Cash Deposit on Sales Contract. If applicable, enter cash deposit here or in the 06C assets segment, BUT NOT BOTH

            // 06A-020 Applicant Social Security Number
            // 06A-030 Cash deposit toward purchase held by
            // 06A-040 Cash or Market Value
            hashTable = new Hashtable();

            foreach (MORNETPlusSegment segment in ds.GetSegment("06A"))
            {
                string aSsn = segment.Get("06A-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                // 5/7/2004 dd - Only handle the first 2 instance of cash deposit for now.
                IAssetCashDeposit record = null;

                int cashDepositIndex = 0;
                if (hashTable.Contains(dataApp.aBSsn))
                    cashDepositIndex = (int)hashTable[dataApp.aBSsn];

                if (cashDepositIndex == 0)
                    record = dataApp.aAssetCollection.GetCashDeposit1(true);
                else if (cashDepositIndex == 1)
                    record = dataApp.aAssetCollection.GetCashDeposit2(true);

                if (null != record)
                {
                    // segment.Get("06A-020");
                    record.Desc = segment.Get("06A-030");
                    record.Val_rep = segment.Get("06A-040");
                    record.Update();
                    cashDepositIndex++;
                }
                hashTable[dataApp.aBSsn] = cashDepositIndex;

            }
            #endregion

            #region 06B - Life Insurance
            // 06B-020 Applicant Social Security Number
            // 06B-030 Acct. No.
            // 06B-040 Life Insurance Cash or Market Value
            // 06B-050 Life insurance Face Amount
            foreach (MORNETPlusSegment segment in ds.GetSegment("06B"))
            {
                string aSsn = segment.Get("06B-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                var record = dataApp.aAssetCollection.GetLifeInsurance(true);
                // segment.Get("06B-020");
                // record.Acsegment.Get("06B-030"); // 5/7/2004 dd - DON'T HAVE. Acct. no.
                record.Val_rep = segment.Get("06B-040");
                record.FaceVal_rep = segment.Get("06B-050");
                record.Update();
            }
            #endregion

            #region 06C - Assets (M)
            Dictionary<Guid, List<Guid>> assetDictionary = new Dictionary<Guid, List<Guid>>();

            if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
            {
                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    CAppData a = m_dataLoan.GetAppData(i);
                    List<Guid> assetList = new List<Guid>();
                    foreach (var item in a.aAssetCollection.GetSubcollection(true, E_AssetGroupT.Regular))
                    {
                        var o = (IAsset)item;
                        assetList.Add(o.RecordId);
                    }
                    assetDictionary.Add(a.aAppId, assetList);
                }
            }

            // 06C-020 Applicant Social Security Number
            // 06C-030 Account/Asset Type
            // 06C-040 Depository/Stock/Bond Institution Name
            // 06C-050 Depository Street Address
            // 06C-060 Depository City
            // 06C-070 Depository State
            // 06C-080 Depository Zip Code
            // 06C-090 Depository Zip Code Plust Four
            // 06C-100 Acc. no.
            // 06C-110 Cash or Market Value
            // 06C-120 Number of Stock/Bond Shares
            // 06C-130 Asset Description
            // 06C-140 Reserved for Future Use
            // 06C-150 Reserved for Future Use
            hashTable = new Hashtable();
            IAssetRegular aMaxGiftAsset = null;
            foreach (MORNETPlusSegment segment in ds.GetSegment("06C"))
            {
                string aSsn = segment.Get("06C-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                E_AssetT assetT = TypeMap_AssetT.ToLO(segment.Get("06C-030"));
                switch (assetT)
                {
                    case E_AssetT.Business:
                        var business = dataApp.aAssetCollection.GetBusinessWorth(true);
                        business.Val_rep = segment.Get("06C-110");
                        business.Update();
                        break;
                    case E_AssetT.Retirement:
                        var retirement = dataApp.aAssetCollection.GetRetirement(true);
                        retirement.Val_rep = segment.Get("06C-110");
                        retirement.Update();
                        break;
                    case E_AssetT.CashDeposit:
                        // 5/7/2004 dd - Only handle the first 2 instance of cash deposit for now.
                        IAssetCashDeposit record = null;
                        int cashDepositIndex = 0;

                        if (hashTable.Contains(dataApp.aBSsn))
                            cashDepositIndex = (int)hashTable[dataApp.aBSsn];

                        if (cashDepositIndex == 0)
                            record = dataApp.aAssetCollection.GetCashDeposit1(true);
                        else if (cashDepositIndex == 1)
                            record = dataApp.aAssetCollection.GetCashDeposit2(true);

                        if (null != record)
                        {
                            record.Desc = segment.Get("06C-040");
                            record.Val_rep = segment.Get("06C-110");
                            record.Update();
                            cashDepositIndex++;
                        }

                        hashTable[dataApp.aBSsn] = cashDepositIndex;
                        break;
                    default:
                        IAssetRegular asset = null;
                        if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
                        {
                            List<Guid> assetList = assetDictionary[dataApp.aAppId];
                            bool isFound = false;
                            foreach (Guid id in assetList)
                            {
                                var a = dataApp.aAssetCollection.GetRegRecordOf(id);
                                if (a.AccNum.Value != "" && a.ComNm != "" && a.AccNum.Value == segment.Get("06C-100") && a.ComNm == segment.Get("06C-040"))
                                {
                                    asset = a;
                                    assetList.Remove(id);
                                    isFound = true;
                                    break;
                                }
                            }
                            if (!isFound)
                            {
                                asset = dataApp.aAssetCollection.AddRegularRecord();
                            }
                        }
                        else
                        {
                            asset = dataApp.aAssetCollection.AddRegularRecord();
                        }
                        asset.AssetT = (E_AssetRegularT)assetT;
                        asset.OwnerT = isBorrower ? E_AssetOwnerT.Borrower : E_AssetOwnerT.CoBorrower;
                        asset.ComNm = segment.Get("06C-040");
                        asset.StAddr = segment.Get("06C-050");
                        asset.City = segment.Get("06C-060");
                        asset.State = segment.Get("06C-070");
                        asset.Zip = segment.Get("06C-080");
                        asset.AccNum = segment.Get("06C-100");
                        asset.Val_rep = segment.Get("06C-110");
                        asset.Desc = segment.Get("06C-130");
                        asset.AccNm = isBorrower ? dataApp.aBNm : dataApp.aCNm;
                        // Keep track of the largest gift asset; See the down payment region above.
                        if ((iNumDwnPmtGifts == 1) && (asset.AssetT == E_AssetRegularT.GiftFunds))
                        {
                            if (aMaxGiftAsset == null)
                                aMaxGiftAsset = asset;
                            else if (aMaxGiftAsset.Val < asset.Val)
                                aMaxGiftAsset = asset;
                        }
                        asset.Update();
                        break;
                }
            }
            if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
            {
                foreach (var o in assetDictionary)
                {
                    CAppData a = m_dataLoan.GetAppData(o.Key);
                    foreach (var id in o.Value)
                    {
                        var asset = a.aAssetCollection.GetRegRecordOf(id);
                        asset.IsOnDeathRow = true;
                    }
                }
            }
            // Give the largest gift asset the same gift source as the down payment; See the down payment region above.
            if (aMaxGiftAsset != null)
            {
                aMaxGiftAsset.GiftSource = eDwnPmtGiftSource;
                aMaxGiftAsset.Update();
            }
            #endregion

            #region 06D - Automobiles (M)
            // 06D-020 Applicant Social Security Number
            // 06D-030 Automobile Make/Model
            // 06D-040 Automobile Year
            // 06D-050 Cash or Market Value
            foreach (MORNETPlusSegment segment in ds.GetSegment("06D"))
            {
                string aSsn = segment.Get("06D-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                var asset = dataApp.aAssetCollection.AddRegularRecord();
                asset.AssetT = E_AssetRegularT.Auto;
                asset.Desc = segment.Get("06D-030");
                if (segment.Get("06D-040") != "")
                {
                    asset.Desc += " / YR " + segment.Get("06D-040");
                }
                asset.Val_rep = segment.Get("06D-050");
                asset.Update();

            }
            #endregion

            #region 06F - Alimony, Child Support/Separate Maintenance and/or Job Related Expense(s) (M)
            // 06F-020 Applicant Social Security Number
            // 06F-030 Expense Type Code
            // 06F-040 Monthly Payment Amount
            // 06F-050 Months Left To Pay
            // 06F-060 Alimony/Child Support/Separate Maintenance Owed To
            hashTable = new Hashtable();
            foreach (MORNETPlusSegment segment in ds.GetSegment("06F"))
            {
                string aSsn = segment.Get("06F-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                // Expense Type Code
                // DR = Alimony
                // DT = Child Support
                // DV = Separate Maintenance Payment
                // DZ = Job Related Expense
                // EE = Other
                string code = segment.Get("06F-030");

                switch (code)
                {
                    case "DR":
                    case "DV":
                        // 5/7/2004 dd - All end result from bad decision when married.
                        var alimony = dataApp.aLiaCollection.GetAlimony(true);
                        alimony.Pmt_rep = segment.Get("06F-040");
                        alimony.RemainMons_rep = segment.Get("06F-050");
                        alimony.OwedTo = segment.Get("06F-060");
                        alimony.Update();
                        break;
                    case "DT":
                        var childSupport = dataApp.aLiaCollection.GetChildSupport(true);
                        childSupport.Pmt_rep = segment.Get("06F-040");
                        childSupport.RemainMons_rep = segment.Get("06F-050");
                        childSupport.OwedTo = segment.Get("06F-060");
                        childSupport.Update();
                        break;
                    case "DZ":
                        int jobExpenseIndex = 0;

                        if (hashTable.Contains(dataApp.aBSsn))
                            jobExpenseIndex = (int)hashTable[dataApp.aBSsn];

                        ILiabilityJobExpense jobExpense = null;
                        if (jobExpenseIndex == 0)
                            jobExpense = dataApp.aLiaCollection.GetJobRelated1(true);
                        else if (jobExpenseIndex == 1)
                            jobExpense = dataApp.aLiaCollection.GetJobRelated2(true);
                        if (null != jobExpense)
                        {
                            jobExpense.Pmt_rep = segment.Get("06F-040");
                            jobExpense.ExpenseDesc = segment.Get("06F-060");
                            jobExpense.Update();
                        }
                        jobExpenseIndex++;
                        hashTable[dataApp.aBSsn] = jobExpenseIndex;
                        break;

                }

            }
            #endregion

            #region 06G - Real Estate Owned
            Dictionary<Guid, List<Guid>> reoDictionary = new Dictionary<Guid, List<Guid>>();
            if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
            {
                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    CAppData a = m_dataLoan.GetAppData(i);
                    List<Guid> reoList = new List<Guid>();
                    foreach (var item in a.aReCollection.GetSubcollection(true, E_ReoGroupT.All))
                    {
                        var o = (IRealEstateOwned)item;
                        reoList.Add(o.RecordId);
                    }
                    reoDictionary.Add(a.aAppId, reoList);
                }
            }
            // 06G-020 Applicant Social Security Number
            // 06G-030 Property Street Address
            // 06G-040 Property City
            // 06G-050 Property State
            // 06G-060 Property Zip Code
            // 06G-070 Property Zip Code Plus Four
            // 06G-080 Property Disposition
            // 06G-090 Type Of Property
            // 06G-100 Present Market Value
            // 06G-110 Amount of Mortgages & Liens
            // 06G-120 Gross Rental Income
            // 06G-130 Mortgage Payments
            // 06G-140 Insurance, Maintenance Taxes & Misc.
            // 06G-150 Net Rental Income
            // 06G-160 Current Residence Indicator
            // 06G-170 Subject Property Indicator
            // 06G-180 REO Asset ID
            // 06G-190 Reserved For Future Use
            Hashtable reoHash = new Hashtable();
            foreach (MORNETPlusSegment segment in ds.GetSegment("06G"))
            {
                string aSsn = segment.Get("06G-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                IRealEstateOwned record = null;
                if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
                {
                    List<Guid> reoList = reoDictionary[dataApp.aAppId];
                    bool isFound = false;
                    foreach (Guid i in reoList)
                    {
                        var a = dataApp.aReCollection.GetRegRecordOf(i);
                        if (a.Addr != "" && a.Addr == segment.Get("06G-030"))
                        {
                            record = a;
                            reoList.Remove(i);
                            isFound = true;
                            break;
                        }
                    }
                    if (!isFound)
                    {
                        record = dataApp.aReCollection.AddRegularRecord();
                    }
                }
                else
                {
                    record = dataApp.aReCollection.AddRegularRecord();
                }
                record.ReOwnerT = isBorrower ? E_ReOwnerT.Borrower : E_ReOwnerT.CoBorrower;

                record.Addr = segment.Get("06G-030");
                record.City = segment.Get("06G-040");
                record.State = segment.Get("06G-050");
                record.Zip = segment.Get("06G-060");
                //segment.Get("06G-070");

                record.Stat = TypeMap_ReFieldStat.ToLO(segment.Get("06G-080"));
                record.TypeT = TypeMap_ReFieldType.ToLO(segment.Get("06G-090"));
                record.Val_rep = segment.Get("06G-100");
                record.MAmt_rep = segment.Get("06G-110");
                record.GrossRentI_rep = segment.Get("06G-120");
                record.MPmt_rep = segment.Get("06G-130");
                record.HExp_rep = segment.Get("06G-140");
                record.NetRentI_rep = segment.Get("06G-150");
                record.NetRentILckd = true;
                record.IsPrimaryResidence = segment.GetBool("06G-160");
                record.IsSubjectProp = segment.GetBool("06G-170");

                decimal netRentalIncome = 0;
                if (decimal.TryParse(segment.Get("06G-150"), out netRentalIncome))
                {

                    if (record.GrossRentI > 0 && netRentalIncome != 0)
                    {
                        // 10/29/2010 dd - OPM 58528 - Since FNMA does not store Occ Rate per REO, we need
                        // to reverse calculation so that Net Income will match after import.
                        decimal occR = (netRentalIncome + record.MPmt + record.HExp) / record.GrossRentI * 100;
                        record.OccR = (int)occR;
                    }
                }
                else
                {
                    netRentalIncome = 0;
                }
                string id = segment.Get("06G-180");
                // segment.Get("06G-190"); // 5/7/2004 dd - FannieMae reserved for future use

                // 9/13/2010 dd - Calculate Negative Cashflow for AUD File.
                // 10/29/2010 dd - OPM 58528 - Always Check Force Cashflow Calculate for PendingSale and SFR
                //if (m_fannieMaeImportSource == FannieMaeImportSource.AUD)
                //{
                if (record.StatT == E_ReoStatusT.PendingSale || record.StatT == E_ReoStatusT.Residence)
                {
                    if (netRentalIncome != 0)
                    {
                        // 9/13/2010 dd - Force calculation of negative cashflow when REO is pending sale or blank status.
                        record.IsForceCalcNetRentalI = true;
                    }
                }
                //}
                record.Update();

                if (reoHash.Contains(id))
                    continue;

                reoHash.Add(id, record);
            }
            if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
            {
                foreach (var o in reoDictionary)
                {
                    CAppData a = m_dataLoan.GetAppData(o.Key);
                    foreach (var id in o.Value)
                    {
                        var reo = a.aReCollection.GetRegRecordOf(id);
                        reo.IsOnDeathRow = true;
                    }
                }
            }
            #endregion

            #region 06H - Alias Borrower or Co-Borrower cannot have dupliate aliases
            // 06H-020 Applicant Social Security Number
            // 06H-030 Alternate First Name
            // 06H-040 Alternate Middle Name
            // 06H-050 Alternate Last Name
            // 06H-060 Unused
            // 06H-070 Unused

            //            foreach (MORNETPlusSegment segment in ds.GetSegment("06H")) 
            //            {
            //                // 5/7/2004 dd - SKIP This section. POINT doesn't export from this section.
            //            }
            #endregion



            #region 06L - Liabilities (M)
            // 12/10/2009 dd - For H4H import we are try to match existing liability before remove obsolete record.
            Dictionary<Guid, List<Guid>> liabilityDictionary = new Dictionary<Guid, List<Guid>>();
            if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
            {
                for (int i = 0; i < m_dataLoan.nApps; i++)
                {
                    CAppData a = m_dataLoan.GetAppData(i);
                    List<Guid> liabilityList = new List<Guid>();
                    foreach (var item in a.aLiaCollection.GetSubcollection(true, E_DebtGroupT.Regular))
                    {
                        var o = (ILiability)item;
                        liabilityList.Add(o.RecordId);
                    }
                    liabilityDictionary.Add(a.aAppId, liabilityList);
                }
            }
            // 06L-020 Applicant Social Security Number
            // 06L-030 Liability Type
            // 06L-040 Creditor Name
            // 06L-050 Creditor Street Address
            // 06L-060 Creditor City
            // 06L-070 Creditor State
            // 06L-080 Creditor Zip Code
            // 06L-090 Creditor Zip Code Plus Four
            // 06L-100 Acct. no.
            // 06L-110 Monthly Payment Amount
            // 06L-120 Months Left to Pay
            // 06L-130 Unpaid Balance
            // 06L-140 Liability will be paid prior to closing
            // 06L-150 REO Asset ID
            // 06L-160 Resubordinated Indicator
            // 06L-170 Omitted Indicator
            // 06L-180 Subject Property Indicator
            // 06L-190 Rental Property Indicator
            foreach (MORNETPlusSegment segment in ds.GetSegment("06L"))
            {
                string aSsn = segment.Get("06L-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                ILiabilityRegular record = null;
                if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
                {
                    List<Guid> liabilityList = liabilityDictionary[dataApp.aAppId];
                    bool isFound = false;
                    foreach (Guid id in liabilityList)
                    {
                        ILiabilityRegular a = dataApp.aLiaCollection.GetRegRecordOf(id);
                        if (a.IsSameTradeline(segment.Get("06L-100"), segment.Get("06L-040")))
                        {
                            record = a;
                            liabilityList.Remove(id);
                            isFound = true;
                            break;
                        }
                    }
                    if (!isFound)
                    {
                        record = dataApp.aLiaCollection.AddRegularRecord();
                    }
                }
                else
                {
                    string description = segment.Get("06L-040");
                    if (!String.IsNullOrEmpty(description) && (dataApp.aPresTotHExpDesc == description || description == ConstApp.DefaultaPresTotHExpDesc))
                    {
                        dataApp.aPresTotHExpLckd = false;
                        dataApp.aPresTotHExpCalc_rep = segment.Get("06L-110");
                        dataApp.aPresTotHExpDesc = description;

                        continue;
                    }
                    else
                    {
                        record = dataApp.aLiaCollection.AddRegularRecord();
                    }
                }
                record.OwnerT = isBorrower ? E_LiaOwnerT.Borrower : E_LiaOwnerT.CoBorrower;

                record.DebtT = TypeMap_DebtRegularT.ToLO(segment.Get("06L-030"));
                record.Desc = TypeMap_DebtRegularT.ToLODebtOtherDescription(segment.Get("06L-030"));
                record.ComNm = segment.Get("06L-040");
                record.ComAddr = segment.Get("06L-050");
                record.ComCity = segment.Get("06L-060");
                record.ComState = segment.Get("06L-070");
                record.ComZip = segment.Get("06L-080");
                // segment.Get("06L-090"); // 5/7/2004 dd - DON'T HAVE THIS. Zipcode + 4
                record.AccNum = segment.Get("06L-100");
                record.Pmt_rep = segment.Get("06L-110");
                record.RemainMons_rep = segment.Get("06L-120");
                record.Bal_rep = segment.Get("06L-130");
                record.WillBePdOff = segment.GetBool("06L-140");
                if (segment.Get("06L-150") != "")
                {
                    var re = (IRealEstateOwned)reoHash[segment.Get("06L-150")];
                    if (null != re)
                    {
                        record.MatchedReRecordId = re.RecordId;
                        // 10/15/2010 dd - OPM 56974.
                        if (segment.Get("06L-180") == "Y")
                        {
                            re.IsSubjectProp = true;
                        }
                        if (segment.Get("06L-190") == "Y")
                        {
                            re.StatT = E_ReoStatusT.Rental;
                        }
                    }
                }
                // segment.Get("06L-160"); // 5/7/2004 dd - DON'T HAVE THIS. Resubordinated Indicator
                record.NotUsedInRatio = segment.GetBool("06L-170");
                record.ExcFromUnderwriting = segment.GetBool("06L-170");  // opm 131895
                if (record.WillBePdOff)
                {
                    record.NotUsedInRatio = true;
                }

            }
            if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
            {
                // 12/10/2009 dd - Clear out obsolete tradeline for H4H re-import.
                foreach (var o in liabilityDictionary)
                {
                    CAppData a = m_dataLoan.GetAppData(o.Key);
                    foreach (var id in o.Value)
                    {
                        ILiabilityRegular lia = a.aLiaCollection.GetRegRecordOf(id);
                        lia.IsOnDeathRow = true;
                    }
                }
            }
            #endregion

            // 5/7/2004 dd - TODO: Do I need to clear out previous employment records? What happen I import FNMA to existing loan, should employment record override all existing records?
            // Check POINT behavior
            #region 04A - Primary Current Employer(s)  (SA)
            // 04A-020 Applicant Social Security Number
            // 04A-030 Employer Name
            // 04A-040 Employer Street Address
            // 04A-050 Employer City
            // 04A-060 Employer State
            // 04A-070 Employer Zip Code
            // 04A-080 Employer Zip Code Plus Four
            // 04A-090 Self Employed
            // 04A-100 Yrs. on this job
            // 04A-110 Months on this job
            // 04A-120 Yrs employed in this line of work/profession
            // 04A-130 Position / Title / Type of Business
            // 04A-140 Business Phone
            foreach (MORNETPlusSegment segment in ds.GetSegment("04A"))
            {
                string aSsn = segment.Get("04A-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                IEmpCollection collection = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;
                IPrimaryEmploymentRecord record = collection.GetPrimaryEmp(true);
                record.EmplrNm = segment.Get("04A-030");
                record.EmplrAddr = segment.Get("04A-040");
                record.EmplrCity = segment.Get("04A-050");
                record.EmplrState = segment.Get("04A-060");
                record.EmplrZip = segment.Get("04A-070");
                //                segment.Get("04A-080");
                record.IsSelfEmplmt = segment.GetBool("04A-090");
                yearMonthParser.ParseStr(segment.Get("04A-100"), segment.Get("04A-110"));
                record.EmplmtLen_rep = yearMonthParser.YearsInDecimal.ToString();
                record.ProfLen_rep = segment.Get("04A-120");
                record.JobTitle = segment.Get("04A-130");
                record.EmplrBusPhone = segment.Get("04A-140");
                record.Update();
                // 6/28/2006 dd - Store Business Phone into aBBusPhone
                if (isBorrower)
                    dataApp.aBBusPhone = segment.Get("04A-140");
                else
                    dataApp.aCBusPhone = segment.Get("04A-140");
            }
            #endregion

            #region 04B - Secondary/Previous Employer(s) (M)
            // 04B-020 Applicant Social Secrity Number
            // 04B-030 Employer Name
            // 04B-040 Employer Street Address
            // 04B-050 Employer City
            // 04B-060 Employer State
            // 04B-070 Employer Zip Code
            // 04B-080 Employer Zip Code Plus Four
            // 04B-090 Self Employed
            // 04B-100 Current Employment Flag
            // 04B-110 From Date
            // 04B-120 To Date
            // 04B-130 Monthly Income
            // 04B-140 Position / Title / Type of Business
            // 04B-150 Business Phone
            foreach (MORNETPlusSegment segment in ds.GetSegment("04B"))
            {
                string aSsn = segment.Get("04B-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                IEmpCollection collection = isBorrower ? dataApp.aBEmpCollection : dataApp.aCEmpCollection;

                IRegularEmploymentRecord record = collection.AddRegularRecord();

                record.EmplrNm = segment.Get("04B-030");
                record.EmplrAddr = segment.Get("04B-040");
                record.EmplrCity = segment.Get("04B-050");
                record.EmplrState = segment.Get("04B-060");
                record.EmplrZip = segment.Get("04B-070");
                //                segment.Get("04B-080"); // 5/7/2004 dd - DON'T HAVE THIS. Zipcode + 4
                record.IsSelfEmplmt = segment.GetBool("04B-090");
                record.EmplmtStartD_rep = segment.Get("04B-110");
                record.EmplmtEndD_rep = segment.Get("04B-120");
                record.MonI_rep = segment.Get("04B-130");
                record.JobTitle = segment.Get("04B-140");
                record.EmplrBusPhone = segment.Get("04B-150");
                record.IsCurrent = segment.Get("04B-100") == "Y"; ; // 5/7/2004 dd - Y=Current Employer, N = Previous. WHat to do here?

                record.Update();
            }
            #endregion

            #region 05H - Present/Proposed Housing Expense
            // 05H-020 Applicant Social Security Number
            // 05H-030 Present/Proposed Indicator
            // 05H-040 Housing Payment Type Code
            // 05H-050 Housing Payment Amount

            // 1071 Housing Payment Type Code (General Expense Qualifier)
            //     25 = Rent
            //     26 = First Mortgage P & I
            //     22 = Other Financing P & I
            //     01 = Hazard Insurance
            //     14 = Real Estate Taxes
            //     02 = Mortgage Insurance
            //     06 = Homeowner Association Dues
            //     23 = Other
            foreach (MORNETPlusSegment segment in ds.GetSegment("05H"))
            {
                string aSsn = segment.Get("05H-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                // 1 = Present Housing Expense
                // 2 = Proposed Housing Expense
                bool isPresentHousingExpense = segment.Get("05H-030") == "1";
                string code = segment.Get("05H-040");
                string payment = segment.Get("05H-050");

                if (isPresentHousingExpense)
                {
                    switch (code)
                    {
                        case "25": dataApp.aPresRent_rep = payment; break;
                        case "26": dataApp.aPres1stM_rep = payment; break;
                        case "22": dataApp.aPresOFin_rep = payment; break;
                        case "01": dataApp.aPresHazIns_rep = payment; break;
                        case "14": dataApp.aPresRealETx_rep = payment; break;
                        case "02": dataApp.aPresMIns_rep = payment; break;
                        case "06": dataApp.aPresHoAssocDues_rep = payment; break;
                        case "23": dataApp.aPresOHExp_rep = payment; break;
                        default:
                            Tools.LogWarning("Invalid code for 05H-040: Code = " + code);
                            break;
                    }
                }
                else
                {
                    switch (code)
                    {
                        case "25":
                            // There is no proposed rent.
                            break;
                        case "26":
                            //                            m_dataLoan.sProFirstMPmt_rep = payment; // 5/7/2004 dd - READ-ONLY
                            break;
                        case "22":
                            //                            m_dataLoan.sProSecondMPmt_rep = payment; // 5/7/2004 dd - READ-ONLY
                            break;
                        case "01":
                            m_dataLoan.sProHazInsR_rep = "0.00";
                            m_dataLoan.sProHazInsMb_rep = payment;
                            break;
                        case "14":
                            m_dataLoan.sProRealETxR_rep = "0.00";
                            m_dataLoan.sProRealETxMb_rep = payment;
                            break;
                        case "02":
                            if (payment != m_dataLoan.sProMIns_rep)
                            {
                                m_dataLoan.sProMInsLckd = true;
                                m_dataLoan.sProMIns_rep = payment;
                            }
                            break;
                        case "06":
                            m_dataLoan.sProHoAssocDues_rep = payment;
                            break;
                        case "23":
                            if (payment != m_dataLoan.sProOHExp_rep)
                            {
                                m_dataLoan.sProOHExp_rep = payment;
                                m_dataLoan.sProOHExpLckd = true;
                            }
                            break;
                        default:
                            Tools.LogWarning("Invalid code for 05H-040: Code = " + code);
                            break;
                    }
                }
            }
            #endregion

            #region 05I - Income
            // 05I-020 Applicant Social Security Number
            // 05I-030 Type Of Income Code
            // 05I-040 Income Amount

            // 1186 Type of Income Code
            // F1 = Military Base Pay
            // 07 = Military Rations Allowance
            // F2 = Military Flight Pay
            // F3 = Military Hazard Pay
            // 02 = Military Clothes Allowance
            // 04 = Military Quarters Allowance
            // 03 = Military Prop Pay
            // F4 = Military Overseas Pay
            // F5 = Military Combat Pay
            // F6 = Military Variable Housing Allowance
            // F7 = Alimony/Child Support Income
            // F8 = Notes Receivable/Installment
            // 41 = Pension/Retirement Income
            // 42 = Social Security /Disability Income
            // 30 = Real Estate, Mortgage Differential Income
            // F9 = Trust Income
            // AU = Accessory Unit Income
            // M1 = Unemployment/Welfare Income
            // M2 = Automobile/Expense Account Income
            // M3 = Foster Care
            // M4 = VA Benefits (Non-education)
            // NB = Non-Borrower Household Income
            // 45 = Other Type of Income, Other Income
            // 20 = Base Employment Income
            // 09 = Overtime
            // 08 = Bonuses
            // 10 = Commissions
            // 17 = Dividends/Interest
            // SI = Subject Property Net Cash Flow
            // S8 = Housing Choice Voucher (Sec 8)
            // 33 = Net Rental Income
            // CG = Capital Gains
            // EA = Employment Related Assets
            // FI = Foreign Income
            // RP = Royalty Payment
            // SE = Seasonal Income
            // TL = Temporary Leave
            // TI = Tip Income
            Dictionary<Guid, List<OtherIncome>> aOIPerApp = new Dictionary<Guid, List<OtherIncome>>();
            foreach (MORNETPlusSegment segment in ds.GetSegment("05I"))
            {
                string aSsn = segment.Get("05I-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;
                dataApp.BorrowerModeT = isBorrower ? E_BorrowerModeT.Borrower : E_BorrowerModeT.Coborrower;
                var consumerId = dataApp.aConsumerId.ToIdentifier<DataObjectKind.Consumer>();

                string code = segment.Get("05I-030");
                string value = segment.Get("05I-040");

                switch (code)
                {
                    case "20":
                        if (!m_dataLoan.sIsIncomeCollectionEnabled)
                        {
                            dataApp.aBaseI_rep = value;
                        }
                        else
                        {
                            m_dataLoan.AddIncomeSource(
                                consumerId,
                                new LendingQB.Core.Data.IncomeSource
                                {
                                    IncomeType = IncomeType.BaseIncome,
                                    MonthlyAmountData = Money.Create(m_dataLoan.m_convertLos.ToMoney(value))
                                });
                        }
                        break;
                    case "09":
                        if (!m_dataLoan.sIsIncomeCollectionEnabled)
                        {
                            dataApp.aOvertimeI_rep = value;
                        }
                        else
                        {
                            m_dataLoan.AddIncomeSource(
                                consumerId,
                                new LendingQB.Core.Data.IncomeSource
                                {
                                    IncomeType = IncomeType.Overtime,
                                    MonthlyAmountData = Money.Create(m_dataLoan.m_convertLos.ToMoney(value))
                                });
                        }
                        break;
                    case "08":
                        if (!m_dataLoan.sIsIncomeCollectionEnabled)
                        {
                            dataApp.aBonusesI_rep = value;
                        }
                        else
                        {
                            m_dataLoan.AddIncomeSource(
                                consumerId,
                                new LendingQB.Core.Data.IncomeSource
                                {
                                    IncomeType = IncomeType.Bonuses,
                                    MonthlyAmountData = Money.Create(m_dataLoan.m_convertLos.ToMoney(value))
                                });
                        }
                        break;
                    case "10":
                        if (!m_dataLoan.sIsIncomeCollectionEnabled)
                        {
                            dataApp.aCommisionI_rep = value;
                        }
                        else
                        {
                            m_dataLoan.AddIncomeSource(
                                consumerId,
                                new LendingQB.Core.Data.IncomeSource
                                {
                                    IncomeType = IncomeType.Commission,
                                    MonthlyAmountData = Money.Create(m_dataLoan.m_convertLos.ToMoney(value))
                                });
                        }
                        break;
                    case "17":
                        if (!m_dataLoan.sIsIncomeCollectionEnabled)
                        {
                            dataApp.aDividendI_rep = value;
                        }
                        else
                        {
                            m_dataLoan.AddIncomeSource(
                                consumerId,
                                new LendingQB.Core.Data.IncomeSource
                                {
                                    IncomeType = IncomeType.DividendsOrInterest,
                                    MonthlyAmountData = Money.Create(m_dataLoan.m_convertLos.ToMoney(value))
                                });
                        }
                        break;
                    case "33":
                        decimal d = 0;
                        if (decimal.TryParse(value, out d))
                        {
                            if (d >= 0)
                            {
                                if (AreSameValue(value, dataApp.aNetRentI1003_rep) == false)
                                {
                                    dataApp.aNetRentI1003Lckd = true;
                                    dataApp.aNetRentI1003_rep = value;
                                }
                            }
                            else
                            {
                                // 11/18/2009 dd - OPM 17988 - When net rental income is negative use aOpNegCf
                                dataApp.aOpNegCfLckd = true;
                                dataApp.aOpNegCf_rep = (-1 * d).ToString();
                            }
                        }
                        break;
                    case "SI":
                        // 5/7/2013 dt - OPM 118545 - Total these up for processing later
                        m_dataLoan.sSpCountRentalIForPrimaryResidToo = true;
                        m_hasSubjectPropNetCashFlow = true;
                        m_totalSubjectPropertyNetCashflow += m_dataLoan.m_convertLos.ToMoney(value);
                        break;
                    default:
                        string desc = TypeMap_OtherIncomeDescription.ToLO(code);
                        if (aOIPerApp.ContainsKey(dataApp.aAppId) == false)
                        {
                            aOIPerApp.Add(dataApp.aAppId, new List<OtherIncome>());
                        }

                        aOIPerApp[dataApp.aAppId].Add(new OtherIncome()
                        {
                            Amount = m_dataLoan.m_convertLos.ToMoney(value),
                            Desc = desc,
                            IsForCoBorrower = !isBorrower
                        });
                        break;
                }


            }

            foreach (KeyValuePair<Guid, List<OtherIncome>> appIncome in aOIPerApp)
            {
                dataApp = m_dataLoan.GetAppData(appIncome.Key);
                List<OtherIncome> otherIncomesForApp = appIncome.Value;
                var borrowerId = dataApp.aBConsumerId;
                var coborrowerId = dataApp.aCConsumerId;

                if (!m_dataLoan.sIsIncomeCollectionEnabled)
                {
                    dataApp.aOtherIncomeList = otherIncomesForApp;
                }
                else
                {
                    var migratedOtherIncomes = IncomeCollectionMigration.GetMigratedOtherIncome(otherIncomesForApp, borrowerId, coborrowerId);
                    foreach (var otherIncomeTuple in migratedOtherIncomes)
                    {
                        m_dataLoan.AddIncomeSource(otherIncomeTuple.Item1, otherIncomeTuple.Item2);
                    }
                }
            }
            #endregion


            #region 07A - Details of Transaction
            // 07A-020 a. Purchase Price
            // 07A-030 b. Alterations, improvements, repairs,
            // 07A-040 c. Land
            // 07A-050 d. Refinance (Inc. debts to be paid off)
            // 07A-060 e. Estimated prepaid items
            // 07A-070 f. Estimated closing costs
            // 07A-080 g. PMI MIP, Funding Fee
            // 07A-090 h. Discount (if Applicant will pay)
            // 07A-100 j. Subordinate financing
            // 07A-110 k. Applicant's closing costs paid by Seller
            // 07A-120 n. PMI, MIP, Funding Fee financed
            foreach (MORNETPlusSegment segment in ds.GetSegment("07A"))
            {
                m_dataLoan.sPurchPrice_rep = segment.Get("07A-020");

                // opm 473167 - for full renovation support loans, set sTotalRenovationCostsLckd to true and 
                // set 07A-030 (b. Alterations, improvements, repairs) to sTotalRenovationCosts
                if (m_dataLoan.BrokerDB.EnableRenovationLoanSupport)
                {
                    if (m_dataLoan.sIsRenovationLoan)
                    {
                        m_dataLoan.sTotalRenovationCostsLckd = true;
                        m_dataLoan.sTotalRenovationCosts_rep = segment.Get("07A-030");
                    }                    
                    else
                    {
                        m_dataLoan.sAltCostLckd = true;
                        m_dataLoan.sAltCost_rep = segment.Get("07A-030");
                    }
                }
                else
                {
                    m_dataLoan.sAltCost_rep = segment.Get("07A-030");
                }

                m_dataLoan.sLandCost_rep = segment.Get("07A-040");
                if (AreSameValue(m_dataLoan.sRefPdOffAmt1003_rep, segment.Get("07A-050")) == false)
                {
                    m_dataLoan.sRefPdOffAmt1003Lckd = true;
                    m_dataLoan.sRefPdOffAmt1003_rep = segment.Get("07A-050");
                }
                if (AreSameValue(m_dataLoan.sTotEstPp1003_rep, segment.Get("07A-060")) == false)
                {
                    m_dataLoan.sTotEstPp1003Lckd = true;
                    m_dataLoan.sTotEstPp1003_rep = segment.Get("07A-060");
                }
                if (AreSameValue(m_dataLoan.sTotEstCcNoDiscnt1003_rep, segment.Get("07A-070")) == false)
                {
                    m_dataLoan.sTotEstCc1003Lckd = true;
                    m_dataLoan.sTotEstCcNoDiscnt1003_rep = segment.Get("07A-070");
                }
                if (AreSameValue(m_dataLoan.sFfUfmip1003_rep, segment.Get("07A-080")) == false)
                {
                    m_dataLoan.sFfUfmip1003Lckd = true;
                    m_dataLoan.sFfUfmip1003_rep = segment.Get("07A-080");
                }
                if (AreSameValue(m_dataLoan.sLDiscnt1003_rep, segment.Get("07A-090")) == false)
                {
                    m_dataLoan.sLDiscnt1003Lckd = true;
                    m_dataLoan.sLDiscnt1003_rep = segment.Get("07A-090");
                }
                // j. Subordinate Financing.
                // Reverse calculation of field sONewFinBal. 
                // TODO: What is the best way for importing sONewFinBal without reverse engineer.
                decimal f07100; 
                if (Decimal.TryParse(segment.Get("07A-100"), out f07100) && f07100 > 0 )
                {
                    m_dataLoan.sIsOFinNew = true;
                    if (m_dataLoan.sLienPosT == E_sLienPosT.First)
                    {
                        m_dataLoan.sConcurSubFin_rep = segment.Get("07A-100");
                    }
                    else if (m_dataLoan.sLienPosT == E_sLienPosT.Second)
                    {
                        m_dataLoan.s1stMtgOrigLAmt_rep = segment.Get("07A-100");
                    }
                }

                if (AreSameValue(m_dataLoan.sTotCcPbs_rep, segment.Get("07A-110")) == false)
                {
                    m_dataLoan.sTotCcPbsLocked = true;
                    m_dataLoan.sTotCcPbs_rep = segment.Get("07A-110");
                }

                //av hotfix 51101 5/18/10   Imported Fannie Mae files do not pick up UFMIP financed amount
                decimal f071_120;
                if (decimal.TryParse(segment.Get("07A-120"), out f071_120) && f071_120 > 0 )
                {
                    m_dataLoan.sFfUfMipIsBeingFinanced = true;

                    if (m_dataLoan.sLT != E_sLT.FHA)
                    {
                        m_dataLoan.sUfCashPdLckd = true;
                        //todo move this into datalayer
                        m_dataLoan.sUfCashPd = m_dataLoan.sFfUfmip1003 - f071_120;  
                    }

                }
            }
            #endregion

            #region 06S - Undrawn HELOC and IPCs
            // 06S-020 Applicant Social Security Number
            // 06S-030 Summary Amount Type Code
            // 06S-040 Amount

            decimal sum_06S = 0;
            foreach (MORNETPlusSegment segment in ds.GetSegment("06S"))
            {
                if (m_dataLoan.sLienPosT == E_sLienPosT.First && segment.Get("06S-030") == "HMB") {
                    string amount = segment.Get("06S-040");
                    
                    decimal parsedAmount;
                    if (decimal.TryParse(amount, out parsedAmount))
                    {
                        sum_06S += parsedAmount;
                    }

                    m_dataLoan.sIsOFinCreditLineInDrawPeriod = true;
                }
            }
            // sSubFin == sConcurSubFin + (the sum of the 06S-040 segments where 06S-030 is "HMB").    
            m_dataLoan.sSubFin = m_dataLoan.sConcurSubFin + sum_06S;
            #endregion

            #region 07B - Other Credits (M)
            Import07B(ds);
            #endregion

            #region 08A - Declarations (SA)
            // 08A-020 Applicant Social Security Number
            // 08A-030 a. Are there any outstanding judgments against you?
            // 08A-040 b. Have you been declared bankrupt within the past 7 years?
            // 08A-050 c. Have you had property foreclosed uporn or given title or deed in lieu thereof in the last 7 years?
            // 08A-060 d. Are you a party to a lawsuit?
            // 08A-070 e. Have you directly or indirectly been obligated on any loan...
            // 08A-080 f. Are you presently delinquent or in default on any Federal debt
            // 08A-090 g. Are you obligated to pay alimony child support or separate maintenance?
            // 08A-100 h. Is any part of the down payment borrowed?
            // 08A-110 i. Are you co-maker or endorser on a note?
            // 08A-120 j. Are you a U.S. citizen? k. Are you a permanent resident alien?
            // 08A-130 l. Do you intend to occupy...
            // 08A-140 m. Have you had an ownership interest
            // 08A-150 m (1) What type of property
            // 08A-160 m (2) How did you hold title
            foreach (MORNETPlusSegment segment in ds.GetSegment("08A"))
            {
                string aSsn = segment.Get("08A-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                string aDecJudgment = segment.Get("08A-030");
                string aDecBankrupt = segment.Get("08A-040");
                string aDecForeclosure = segment.Get("08A-050");
                string aDecLawsuit = segment.Get("08A-060");
                string aDecObligated = segment.Get("08A-070");
                string aDecDelinquent = segment.Get("08A-080");
                string aDecAlimony = segment.Get("08A-090");
                string aDecBorrowing = segment.Get("08A-100");
                string aDecEndorser = segment.Get("08A-110");

                // 1066 Citizen Status Code
                // 01 = U.S. Citizen
                // 03 = Permanent Resident-Alien
                // 05 = Non-Permanent Resident-Alien
                string citizen = segment.Get("08A-120");
                string aDecCitizen = "";
                string aDecResidency = "";

                if (citizen == "01")
                {
                    aDecCitizen = "Y";
                    aDecResidency = "N";
                }
                else if (citizen == "03")
                {
                    aDecCitizen = "N";
                    aDecResidency = "Y";
                }
                else if (citizen == "05")
                {
                    aDecCitizen = "N";
                    aDecResidency = "N";
                }

                string aDecOcc = segment.Get("08A-130");
                string aDecPastOwnership = segment.Get("08A-140");
                E_aBDecPastOwnedPropT aDecPastOwnedPropT = TypeMap_aDecPastOwnedPropT.ToLO(segment.Get("08A-150"));
                E_aBDecPastOwnedPropTitleT aDecPastOwnedPropTitleT = TypeMap_aDecPastOwnedPropTitleT.ToLO(segment.Get("08A-160"));
     
                if (isBorrower)
                {
                    dataApp.aBDecJudgment = aDecJudgment;
                    dataApp.aBDecBankrupt = aDecBankrupt;
                    dataApp.aBDecForeclosure = aDecForeclosure;
                    dataApp.aBDecLawsuit = aDecLawsuit;
                    dataApp.aBDecObligated = aDecObligated;
                    dataApp.aBDecDelinquent = aDecDelinquent;
                    dataApp.aBDecAlimony = aDecAlimony;
                    dataApp.aBDecBorrowing = aDecBorrowing;
                    dataApp.aBDecEndorser = aDecEndorser;
                    dataApp.aBDecCitizen = aDecCitizen;
                    dataApp.aBDecResidency = aDecResidency;
                    dataApp.aBDecOcc = aDecOcc;
                    dataApp.aBDecPastOwnership = aDecPastOwnership;
                    dataApp.aBDecPastOwnedPropT = aDecPastOwnedPropT;
                    dataApp.aBDecPastOwnedPropTitleT = aDecPastOwnedPropTitleT;
                }
                else
                {
                    dataApp.aCDecJudgment = aDecJudgment;
                    dataApp.aCDecBankrupt = aDecBankrupt;
                    dataApp.aCDecForeclosure = aDecForeclosure;
                    dataApp.aCDecLawsuit = aDecLawsuit;
                    dataApp.aCDecObligated = aDecObligated;
                    dataApp.aCDecDelinquent = aDecDelinquent;
                    dataApp.aCDecAlimony = aDecAlimony;
                    dataApp.aCDecBorrowing = aDecBorrowing;
                    dataApp.aCDecEndorser = aDecEndorser;
                    dataApp.aCDecCitizen = aDecCitizen;
                    dataApp.aCDecResidency = aDecResidency;
                    dataApp.aCDecOcc = aDecOcc;
                    dataApp.aCDecPastOwnership = aDecPastOwnership;
                    dataApp.aCDecPastOwnedPropT = (E_aCDecPastOwnedPropT)aDecPastOwnedPropT;
                    dataApp.aCDecPastOwnedPropTitleT = (E_aCDecPastOwnedPropTitleT)aDecPastOwnedPropTitleT;

                }

            }


            //OPM 105950 av 11 26 2012
            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                CAppData app = m_dataLoan.GetAppData(i);
                if (app.aBDecOcc.ToUpper() == "N" && app.aCDecOcc.ToUpper() == "N" && app.aOccT == E_aOccT.PrimaryResidence)
                {
                    app.aOccT = E_aOccT.Investment;
                }
            }

            #endregion

            #region 08B  - Declaration Explanations
            // 08B-020 Applicant Social Security Number
            // 08B-030 Declaration Type Code
            // 08B-040 Declaration Explanation
            foreach (MORNETPlusSegment segment in ds.GetSegment("08B"))
            {
                // 5/7/2004 dd - SKIP.
                string aSsn = segment.Get("08B-020");
                string typeCode = segment.Get("08B-030");
                string explanation = segment.Get("08B-040");

                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                if (isBorrower)
                {
                    if (typeCode == "91")
                    {
                        dataApp.aBDecJudgmentExplanation = explanation;
                    }
                    else if (typeCode == "92")
                    {
                        dataApp.aBDecBankruptExplanation = explanation;
                    }
                    else if (typeCode == "93")
                    {
                        dataApp.aBDecForeclosureExplanation = explanation;
                    }
                    else if (typeCode == "94")
                    {
                        dataApp.aBDecLawsuitExplanation = explanation;
                    }
                    else if (typeCode == "95")
                    {
                        dataApp.aBDecObligatedExplanation = explanation;
                    }
                    else if (typeCode == "96")
                    {
                        dataApp.aBDecDelinquentExplanation = explanation;
                    }
                    else if (typeCode == "97")
                    {
                        dataApp.aBDecAlimonyExplanation = explanation;
                    }
                    else if (typeCode == "98")
                    {
                        dataApp.aBDecBorrowingExplanation = explanation;
                    }
                    else if (typeCode == "99")
                    {
                        dataApp.aBDecEndorserExplanation = explanation;
                    }
                }
                else
                {
                    if (typeCode == "91")
                    {
                        dataApp.aCDecJudgmentExplanation = explanation;
                    }
                    else if (typeCode == "92")
                    {
                        dataApp.aCDecBankruptExplanation = explanation;
                    }
                    else if (typeCode == "93")
                    {
                        dataApp.aCDecForeclosureExplanation = explanation;
                    }
                    else if (typeCode == "94")
                    {
                        dataApp.aCDecLawsuitExplanation = explanation;
                    }
                    else if (typeCode == "95")
                    {
                        dataApp.aCDecObligatedExplanation = explanation;
                    }
                    else if (typeCode == "96")
                    {
                        dataApp.aCDecDelinquentExplanation = explanation;
                    }
                    else if (typeCode == "97")
                    {
                        dataApp.aCDecAlimonyExplanation = explanation;
                    }
                    else if (typeCode == "98")
                    {
                        dataApp.aCDecBorrowingExplanation = explanation;
                    }
                    else if (typeCode == "99")
                    {
                        dataApp.aCDecEndorserExplanation = explanation;
                    }
                }
            }
            #endregion

            #region 09A - Acknowledgment and Agreement
            // 09A-020 Applicant Social Security Number
            // 09A-030 Signature Date
            foreach (MORNETPlusSegment segment in ds.GetSegment("09A"))
            {
                string aSsn = segment.Get("09A-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;
                dataApp.a1003SignD_rep = segment.Get("09A-030");
            }
            #endregion

            #region 10A - Information for Government Monitoring Purposes
            // 10A-020 Applicant Social Security Number
            // 10A-030 I do not wish to furnish this information
            // 10A-040 Ethnicity
            // 10A-050 Filler
            // 10A-060 Sex
            foreach (MORNETPlusSegment segment in ds.GetSegment("10A"))
            {
                string aSsn = segment.Get("10A-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                bool aNoFurnish = segment.GetBool("10A-030");
                E_aHispanicT aHispanicT = TypeMap_aHispanicT.ToLO(segment.Get("10A-040"));
                E_GenderT aGender = TypeMap_aGender.ToLO(segment.Get("10A-060"));

                if (isBorrower)
                {
                    dataApp.aBNoFurnishLckd = true;
                    dataApp.aBNoFurnish = aNoFurnish;
                    dataApp.aBHispanicT = aHispanicT;
                    dataApp.aBGender = aGender;
                }
                else
                {
                    dataApp.aCNoFurnishLckd = true;
                    dataApp.aCNoFurnish = aNoFurnish;
                    dataApp.aCHispanicT = aHispanicT;
                    dataApp.aCGender = aGender;
                }

            }
            #endregion

            #region 10B - Interviewer Information
            // 10B-020 This application was taken by
            // 10B-030 Interviewer's Name
            // 10B-040 Interview Date
            // 10B-050 Interviewer's Phone Number
            // 10B-060 Institution Name
            // 10B-070 Institution Street Address
            // 10B-080 Institution Street Address 2
            // 10B-090 Institution City
            // 10B-100 Institution State
            // 10B-110 Institution Zip Code
            // 10B-120 Institution Zip Code Plus Four
            if (PrincipalFactory.CurrentPrincipal.BrokerId != new Guid("baf07293-f626-44c6-9750-706199a7c0b6"))
            {
                foreach (MORNETPlusSegment segment in ds.GetSegment("10B"))
                {
                    E_aIntrvwrMethodT interviewMethod = TypeMap_aIntervwrMethodT.ToLO(segment.Get("10B-020"));
                    m_dataLoan.GetAppData(0).aBInterviewMethodT = interviewMethod;
                    m_dataLoan.GetAppData(0).aCInterviewMethodT = interviewMethod;

                    // Setting the interview date may create and set the interview date
                    // for the 1003 interviewer preparer for files that are in loan versions
                    // prior to v17. Performing the set prior to setting information to the 
                    // preparer below to avoid any duplication issues (ie creating 2 preparers).
                    if (!string.IsNullOrEmpty(segment.Get("10B-040")))
                    {
                        foreach (var app in m_dataLoan.Apps)
                        {
                            app.a1003InterviewD_rep = segment.Get("10B-040");
                            app.a1003InterviewDLckd = true;
                        }
                    }

                    IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                    interviewer.PreparerName = segment.Get("10B-030");
                    interviewer.Phone = segment.Get("10B-050");
                    interviewer.CompanyName = segment.Get("10B-060");
                    interviewer.StreetAddr = segment.Get("10B-070");
                    //                segment.Get("10B-080"); // 5/7/2004 dd - DON'T HAVE THIS FIELD. Street Address 2
                    interviewer.City = segment.Get("10B-090");
                    interviewer.State = segment.Get("10B-100");
                    interviewer.Zip = segment.Get("10B-110");
                    //                segment.Get("10B-120"); // 5/7/2004 dd - DON'T HAVE THIS FIELD. Zipcode + 4
                    interviewer.Update();
                }
                {
                    //OPM 33815 - Populate interviewer to loan officer if loan officer is not already created for the loan and the interviewer exists in the file
                    IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                    CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

                    if ((interviewer != CPreparerFields.Empty) && (agent == CAgentFields.Empty))
                    {
                        CAgentFields loanofficer = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                        interviewer.SyncToAgent(loanofficer);
                        loanofficer.Update();
                    }
                }
            }

            #endregion

            #region 10R - Information for Government Monitoring Purposes reporting on Multiple Race per Applicant
            // 10R-020 Applicant Social Security Number
            // 10R-030 Race
            foreach (MORNETPlusSegment segment in ds.GetSegment("10R"))
            {
                string aSsn = segment.Get("10R-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;

                switch (segment.Get("10R-030"))
                {
                    case "1":
                        if (isBorrower)
                            dataApp.aBIsAmericanIndian = true;
                        else
                            dataApp.aCIsAmericanIndian = true;
                        break;
                    case "2":
                        if (isBorrower)
                            dataApp.aBIsAsian = true;
                        else
                            dataApp.aCIsAsian = true;
                        break;
                    case "3":
                        if (isBorrower)
                            dataApp.aBIsBlack = true;
                        else
                            dataApp.aCIsBlack = true;
                        break;
                    case "4":
                        if (isBorrower)
                            dataApp.aBIsPacificIslander = true;
                        else
                            dataApp.aCIsPacificIslander = true;
                        break;
                    case "5":
                        if (isBorrower)
                            dataApp.aBIsWhite = true;
                        else
                            dataApp.aCIsWhite = true;
                        break;
                }

            }
            #endregion

            for (int i = 0; i < m_dataLoan.nApps; i++)
            {
                dataApp = m_dataLoan.GetAppData(i);

                dataApp.aPresTotHExpLckd = false;
                decimal calcValue = dataApp.aPresTotHExpCalc;
                dataApp.aPresTotHExpLckd = true;
                decimal savedValue = dataApp.aPresTotHExpCalc;

                dataApp.aPresTotHExpLckd = calcValue != savedValue;
            }

            // This call must remain at the end of the method. It uses imported 1003 data to calculate construction datapoints.
            CalculateConstructionData();
        }

        private void CalculateConstructionData()
        {
            this.m_dataLoan.sConstructionPurposeT = ConstructionLoanUtilities.CalculateConstructionPurpose(this.m_dataLoan);
            this.m_dataLoan.sLotOwnerT = ConstructionLoanUtilities.CalculateLotOwner(this.m_dataLoan);
        }

        private void Import07B(MORNETPlusDataset ds)
        {
            if (false == m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                Import07BInto1003NotAdjustments(ds);
                return;
            }

            // sLoads1003LineLFromAdjustments is true.
            if (m_dataLoan.sProrationList.IsPopulateToLineLOn1003 && m_dataLoan.sProrationList.Any(p => p.Amount != 0))
            {
                Tools.LogInfo("Skipping import of 07B segments because loan has nonzero prorations on it that will populate to the 1003. OPM 212045.");
                return;
            }

            // sLoads1003LineLFromAdjustments AND (!sProrationList.IsPopulateToLineLOn1003 OR !sProrationList.Any(p => p.Amount > 0))
            Import07BIntoAdjustments(ds);
        }

        private void ClearExistingAdjustmentsThatPopulateTo1003()
        {
            if(false == m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming error.  Should not call ClearExistingAdjustmentsThatPopulateTo1003 unless sLoads1003LineLFromAdjustments");
            }

            if (m_dataLoan.sProrationList.IsPopulateToLineLOn1003 && m_dataLoan.sProrationList.Any(p => p.Amount != 0))
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming error.  Should not call ClearExistingAdjustmentsThatPopulateTo1003 if sProrationList.IsPopulateToLineLOn1003 and has nonzero proration.");
            }

            var adjustments = m_dataLoan.sAdjustmentList;

            adjustments.ClearAdjustmentsThatPopulateTo1003();

            m_dataLoan.sAdjustmentList = adjustments;
        }

        private void Import07BIntoAdjustments(MORNETPlusDataset ds)
        {
            if(false == m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming error.  Should not call Import07BIntoAdjustments unless sLoads1003LineLFromAdjustments");
            }

            if (m_dataLoan.sProrationList.IsPopulateToLineLOn1003 && m_dataLoan.sProrationList.Any(p => p.Amount != 0))
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming error.  Should not call Import07BIntoAdjustments if sProrationList.IsPopulateToLineLOn1003 and has nonzero proration.");
            }

            ClearExistingAdjustmentsThatPopulateTo1003();

            foreach (MORNETPlusSegment segment in ds.GetSegment("07B"))
            {
                string desc = TypeMap_OtherCreditTypeCode.ToLO(segment.Get("07B-020"));
                string amt = segment.Get("07B-030");

                if (desc == "Cash Deposit on sales contract" || desc == "Lender Credit")
                {
                    continue;
                }

                if (desc == "Seller Credit")
                {
                    m_dataLoan.sAdjustmentList.SellerCredit.Amount_rep = amt;
                    m_dataLoan.sAdjustmentList.SellerCredit.PopulateTo = E_PopulateToT.LineLOn1003;
                }
                else
                {
                    m_dataLoan.sAdjustmentList.AddWithLoanVersion(desc, amt);                    
                }
            }

            m_dataLoan.sAdjustmentList = m_dataLoan.sAdjustmentList;
        }

        private void Import07BInto1003NotAdjustments(MORNETPlusDataset ds)
        {
            if (m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                throw new CBaseException(ErrorMessages.Generic, "Programming error.  Should not call Import07BInto1003NotAdjustments if sLoads1003LineLFromAdjustments");
            }

            // 07B-020 Other Credit Type Code
            // 07B-030 Amount of Other Credit
            int otherCreditIndex = 0;

            //  m_dataLoan.sOCredit1Amt_rep = "";

            m_dataLoan.sOCredit2Desc = "";
            m_dataLoan.sOCredit3Desc = "";
            m_dataLoan.sOCredit4Desc = "";
            m_dataLoan.sOCredit2Amt_rep = "";
            m_dataLoan.sOCredit3Amt_rep = "";
            m_dataLoan.sOCredit4Amt_rep = "";

            // Cannot set a field to a (non-empty?) vaue more than once in the Fannie importer when using a template.
            // Working around this via temp variables via iOPM 34519.
            string sOCredit4Desc = m_dataLoan.sOCredit4Desc;
            decimal sOCredit4Amt = m_dataLoan.sOCredit4Amt;

            decimal lenderCreditTotal = 0;
            foreach (MORNETPlusSegment segment in ds.GetSegment("07B"))
            {
                // Skip Cash Deposit in Other credit section. Since Cash deposit will get calculate automatically.
                // Start the first other credit item in the second line of 1003 print out.
                string desc = TypeMap_OtherCreditTypeCode.ToLO(segment.Get("07B-020"));
                string amt = segment.Get("07B-030");

                if (desc.Equals("Lender Credit", StringComparison.OrdinalIgnoreCase) == true)
                {
                    // 9/30/2011 dd - Since sOCredit5Amt is a read-only field we must factor this in when
                    // import from FNMA in order to avoid double count.
                    lenderCreditTotal += m_dataLoan.m_convertLos.ToMoney(amt);

                    continue;
                }

                if (desc != "Cash Deposit on sales contract")
                {
                    if (otherCreditIndex == 0)
                    {
                        //m_dataLoan.sOCredit2Lckd = true;
                        m_dataLoan.sOCredit2Desc = desc;
                        m_dataLoan.sOCredit2Amt_rep = amt;
                    }
                    else if (otherCreditIndex == 1)
                    {
                        m_dataLoan.sOCredit3Desc = desc;
                        m_dataLoan.sOCredit3Amt_rep = amt;
                    }
                    else if (otherCreditIndex == 2)
                    {
                        sOCredit4Desc = desc;
                        sOCredit4Amt = m_dataLoan.m_convertLos.ToMoney(amt);
                    }
                    else
                    {
                        // 9/30/2011 dd - Since we have a fix number of other credit, we will sum of the rest of 
                        // credit.

                        sOCredit4Amt += m_dataLoan.m_convertLos.ToMoney(amt);
                        sOCredit4Desc = "Other";
                    }
                    otherCreditIndex++;
                }

            }
            string sOCredit5Amt_rep = m_dataLoan.sOCredit5Amt_rep.TrimWhitespaceAndBOM();// 9/30/2011 dd - FNMA use padding space.
            decimal sOCredit5Amt = m_dataLoan.m_convertLos.ToMoney(sOCredit5Amt_rep);



            if (lenderCreditTotal != 0 || sOCredit5Amt != 0)
            {
                decimal v = lenderCreditTotal - sOCredit5Amt;
                if (v != 0)
                {
                    string amt = m_dataLoan.m_convertLos.ToMoneyString(v, FormatDirection.ToRep);
                    if (otherCreditIndex == 0)
                    {
                        m_dataLoan.sOCredit2Desc = "Lender Credit";
                        m_dataLoan.sOCredit2Amt_rep = amt;
                    }
                    else if (otherCreditIndex == 1)
                    {
                        m_dataLoan.sOCredit3Desc = "Lender Credit";
                        m_dataLoan.sOCredit3Amt_rep = amt;
                    }
                    else if (otherCreditIndex == 2)
                    {
                        sOCredit4Desc = "Lender Credit";
                        sOCredit4Amt = m_dataLoan.m_convertLos.ToMoney(amt);
                    }
                    else
                    {
                        // 9/30/2011 dd - Since we have a fix number of other credit, we will sum of the rest of 
                        // credit.
                        sOCredit4Amt += m_dataLoan.m_convertLos.ToMoney(amt);
                        sOCredit4Desc = "Other";
                    }

                    otherCreditIndex++;
                }

            }

            m_dataLoan.sOCredit4Desc = sOCredit4Desc;
            m_dataLoan.sOCredit4Amt = sOCredit4Amt;
        }

        private bool IsDwnPmtSourceGift(string sFNMADownPaymentSrc)
        {
            switch (sFNMADownPaymentSrc)
            {
                case "H0": 
                case "H1": 
                case "H3": 
                case "H6": 
                case "H4": 
                case "H5": 
                    return true;
                default:
                    return false;
            }
        }
        private E_GiftFundSourceT ConvertToGiftSource(string sFNMADownPaymentSrc)
        {
            switch (sFNMADownPaymentSrc)
            {
                case "H0":
                    return E_GiftFundSourceT.Other;
                case "H1":
                    return E_GiftFundSourceT.Relative;
                case "H3":
                    return E_GiftFundSourceT.Government;
                case "H6":
                    return E_GiftFundSourceT.Employer;
                case "H4":
                case "H5":
                    return E_GiftFundSourceT.Nonprofit;
                default:
                    return E_GiftFundSourceT.Blank;
            }
        }

        private E_sSpInvestorCurrentLoanT ConvertToCurrentLoanInvestor(string ownerOfExistingMortgage)
        {
            switch (ownerOfExistingMortgage)
            {
                case "01":
                    return E_sSpInvestorCurrentLoanT.FannieMae;
                case "02":
                    return E_sSpInvestorCurrentLoanT.FreddieMac;
                case "03":
                case "F1":
                    return E_sSpInvestorCurrentLoanT.Unknown;
                default:
                    throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext($"Invalid value \"{ownerOfExistingMortgage}\" for segment 99B-030"));
            }
        }

        /// <summary>
        /// Table MDF-7. Additional Case Data Format.
        /// </summary>
        /// <param name="ds"></param>
        private void ParseAdditionalCaseData(MORNETPlusDataset ds) 
        {
            #region 99B - Fannie Mae Transmittal Data
            // 99B-020 Seller Provided Bellow Market Financing
            // 99B-030 Owner of existing mortgage
            // 99B-040 Property Appraised Value
            // 99B-050 Buydown Rate
            // 99B-060 Actual vs. Estimated Appraised Value Indicator
            // 99B-070 Appraisal Fieldwork Ordered
            // 99B-080 Appraiser Name
            // 99B-090 Appraiser Company
            // 99B-100 Appraiser License Number
            // 99B-110 Appraiser License State Code
            foreach (MORNETPlusSegment segment in ds.GetSegment("99B")) 
            {
                m_dataLoan.sIsSellerProvidedBelowMktFin = segment.Get("99B-020");
                m_dataLoan.sApprVal_rep = segment.Get("99B-040");
                //                segment.Get("99B-050");// 5/7/2004 dd - TODO: Deal with buydown rate.

                string ownerOfCurrentLoan = segment.Get("99B-030");
                if (!string.IsNullOrEmpty(ownerOfCurrentLoan))
                {
                    m_dataLoan.sSpInvestorCurrentLoanT = this.ConvertToCurrentLoanInvestor(ownerOfCurrentLoan);
                }

                string agentName = segment.Get("99B-080");
                string companyName = segment.Get("99B-090");
                string licenseNumOfAgent = segment.Get("99B-100");
                if (!string.IsNullOrEmpty(agentName) || !string.IsNullOrEmpty(companyName) || !string.IsNullOrEmpty(licenseNumOfAgent))
                {
                    CAgentFields appraiser = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.CreateNew);
                    appraiser.AgentName = agentName;
                    appraiser.CompanyName = companyName;
                    appraiser.LicenseNumOfAgent = licenseNumOfAgent;
                    appraiser.Update();
                }

            }
            #endregion

            #region ADS - Additional Data Segment.
            var fannieMaeThirdPartyProviders = new List<FannieMaeThirdPartyProvider>();
            var adsManager = new AdditionalDataSegmentManager();

            foreach (MORNETPlusSegment segment in ds.GetSegment("ADS"))
            {
                string key = segment.Get("ADS-020"); // Name
                string value = segment.Get("ADS-030"); // Value

                var serviceProvider = DuServiceProviders.RetrieveByProviderName(key);
                if (serviceProvider.HasValue)
                {
                    fannieMaeThirdPartyProviders.Add(new FannieMaeThirdPartyProvider(serviceProvider.Value, value));
                }
                else
                {
                    adsManager.AddSegment(key, value);
                }
            }

            IEnumerable<FannieMaeThirdPartyProvider> existingProviders = m_dataLoan.sDuThirdPartyProviders.GetProviders();
            m_dataLoan.sDuThirdPartyProviders = m_dataLoan.sDuThirdPartyProviders.AddProviders(fannieMaeThirdPartyProviders.Except(existingProviders.Intersect(fannieMaeThirdPartyProviders)));

            this.ProcessAdditionalDataSegments(adsManager);
            #endregion
        }

        /// <summary>
        /// Processes all Additional Data Segments left after removing the third-party service providers.
        /// </summary>
        /// <param name="adsManager">The ADS manager.</param>
        private void ProcessAdditionalDataSegments(AdditionalDataSegmentManager adsManager)
        {
            var uniqueSegments = adsManager.Segments.Where(s => !s.IsRepeatable).ToDictionary(s => s.Name, s => s.Value);
            this.ProcessUniqueSegments(uniqueSegments);

            var repeatableSegments = adsManager.Segments.Where(s => s.IsRepeatable);
            var segmentListsBySsn = new Dictionary<string, List<AdditionalDataSegment>>();

            foreach (var segment in repeatableSegments)
            {
                // The SSN will be the first portion of the value, so we can read up to the delimiter.
                var ssn = string.Concat(segment.Value.TakeWhile(c => c != AdditionalDataSegment.Delimiter));

                if (!segmentListsBySsn.ContainsKey(ssn))
                {
                    segmentListsBySsn[ssn] = new List<AdditionalDataSegment>();
                }

                segmentListsBySsn[ssn].Add(segment);
            }

            foreach (var kvp in segmentListsBySsn)
            {
                this.ProcessBorrowerSegments(kvp.Key, kvp.Value);
            }
        }

        /// <summary>
        /// Processes all non-repeatable segments (not keyed with an SSN in the value).
        /// </summary>
        /// <param name="adsHash">The unique segments.</param>
        private void ProcessUniqueSegments(Dictionary<string, string> adsHash)
        {
            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
            string loanOriginatorID;
            string loanOriginationCompanyID;

            if (adsHash.TryGetValue("LoanOriginatorID", out loanOriginatorID))
            {
                interviewer.LoanOriginatorIdentifier = loanOriginatorID;
            }

            if (adsHash.TryGetValue("LoanOriginationCompanyID", out loanOriginationCompanyID))
            {
                interviewer.CompanyLoanOriginatorIdentifier = loanOriginationCompanyID;
            }
            interviewer.Update();

            //populate nmls info to lo record so du doesnt break BAF07293-F626-44C6-9750-706199A7C0B6
            if (PrincipalFactory.CurrentPrincipal.BrokerId == new Guid("baf07293-f626-44c6-9750-706199a7c0b6"))
            {
                CAgentFields loanOfficer = m_dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);

                if (string.IsNullOrEmpty(loanOriginatorID) == false)
                {
                    loanOfficer.LoanOriginatorIdentifier = loanOriginatorID;
                }
                if (string.IsNullOrEmpty(loanOriginationCompanyID) == false)
                {
                    loanOfficer.CompanyLoanOriginatorIdentifier = loanOriginationCompanyID;
                }
                Tools.LogInfo("loanOriginatorID " + loanOriginatorID + " loanOriginationCompanyID:" + loanOriginationCompanyID);
                loanOfficer.Update();
            }

            string totalMortgagedPropertiesCount = string.Empty;

            if (adsHash.TryGetValue("TotalMortgagedPropertiesCount", out totalMortgagedPropertiesCount))
            {
                this.m_dataLoan.sNumFinancedProperties_rep = totalMortgagedPropertiesCount;
            }

            string appraisalIdentifier = string.Empty;

            if (adsHash.TryGetValue("AppraisalIdentifier", out appraisalIdentifier))
            {
                this.m_dataLoan.sSpAppraisalId = appraisalIdentifier;
            }

            string fipsCode = string.Empty;
            if (adsHash.TryGetValue("FIPSCodeIdentifier", out fipsCode))
            {
                if (!fipsCode.Equals(m_dataLoan.sFannieFips))
                {
                    m_dataLoan.sFannieFips = fipsCode;
                    m_dataLoan.sFannieFipsLckd = true;
                }
            }
        }

        /// <summary>
        /// Imports a set of Additional Data Segments associated with a specific borrower.
        /// </summary>
        /// <param name="ssn">The borrower's SSN.</param>
        /// <param name="segments">The segments associated with the borrower.</param>
        private void ProcessBorrowerSegments(string ssn, List<AdditionalDataSegment> segments)
        {
            var appData = this.FindDataAppBySsn(ssn);

            if (appData == null)
            {
                Tools.LogWarning($"Unable to link SSN \"{ssn}\" to an application for FNM 3.2 import.");
                return;
            }

            // These freeform description fields can have their data spread over multiple segments.
            var ethnicityOriginTypeOtherDescription = string.Empty;
            var raceTypeOtherDescription = string.Empty;
            var raceDesignationOtherAsianDescription = string.Empty;
            var raceDesignationOtherPacificIslanderDescription = string.Empty;

            foreach (var segment in segments)
            {
                // Borrower segments have the following structure: <ssn>:<sequenceNumber>:<data>.
                // Note that the sequence number only applies to certain data points, so a segment
                // will either have two or three distinct values.
                var values = segment.Value.Split(new[] { AdditionalDataSegment.Delimiter });
                var data = values.Last();

                switch (segment.Name.ToLower())
                {
                    case "hmdagendertype":
                        this.ProcessHmdaGenderType(appData, data);
                        break;
                    case "hmdagenderrefusalindicator":
                        this.ProcessHmdaGenderRefusalIndicator(appData, data);
                        break;
                    case "hmdagendercollectedbasedonvisual":
                        appData.aSexCollectedByObservationOrSurname = data == "Y" ? E_TriState.Yes : E_TriState.No;
                        break;
                    case "hmdaethnicitytype":
                        this.ProcessHmdaEthnicityType(appData, data);
                        break;
                    case "hmdaethnicityorigintype":
                        this.ProcessHmdaEthnicityOriginType(appData, data);
                        break;
                    case "hmdaethnicityorigintypeotherdesc":
                        ethnicityOriginTypeOtherDescription += data;
                        break;
                    case "hmdaethnicityrefusalindicator":
                        appData.aDoesNotWishToProvideEthnicity = data == "Y";
                        break;
                    case "hmdaethnicitycollectedbasedonvisual":
                        appData.aEthnicityCollectedByObservationOrSurname = data == "Y" ? E_TriState.Yes : E_TriState.No;
                        break;
                    case "hmdaracetype":
                        this.ProcessHmdaRaceType(appData, data);
                        break;
                    case "hmdaracetypeadditionaldescription":
                        raceTypeOtherDescription += data;
                        break;
                    case "hmdaracedesignationtype":
                        this.ProcessHmdaRaceDesignationType(appData, data);
                        break;
                    case "hmdaracedesignationotherasndesc":
                        raceDesignationOtherAsianDescription += data;
                        break;
                    case "hmdaracedesignationotherpidesc":
                        raceDesignationOtherPacificIslanderDescription += data;
                        break;
                    case "hmdaracerefusalindicator":
                        appData.aDoesNotWishToProvideRace = data == "Y";
                        break;
                    case "hmdaracecollectedbasedonvisual":
                        appData.aRaceCollectedByObservationOrSurname = data == "Y" ? E_TriState.Yes : E_TriState.No;
                        break;
                    case "applicationtakenmethodtype":
                        this.ProcessApplicationTakenMethodType(appData, data);
                        break;
                    default:
                        string message = $"ADS segment \"{segment.Name}\" is not handled.";
                        throw new InvalidFannieFileFormatException(message, message);
                }
            }

            if (!string.IsNullOrEmpty(ethnicityOriginTypeOtherDescription))
            {
                appData.aOtherHispanicOrLatinoDescription = ethnicityOriginTypeOtherDescription;
            }

            if (!string.IsNullOrEmpty(raceTypeOtherDescription))
            {
                appData.aOtherAmericanIndianDescription = raceTypeOtherDescription;
            }

            if (!string.IsNullOrEmpty(raceDesignationOtherAsianDescription))
            {
                appData.aOtherAsianDescription = raceDesignationOtherAsianDescription;
            }

            if (!string.IsNullOrEmpty(raceDesignationOtherPacificIslanderDescription))
            {
                appData.aOtherPacificIslanderDescription = raceDesignationOtherPacificIslanderDescription;
            }
        }

        /// <summary>
        /// Imports the gender type.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="value">The value returned by FNMA.</param>
        private void ProcessHmdaGenderType(CAppData appData, string value)
        {
            string message = string.Empty;
            switch (value.ToLower())
            {
                case "applicantselectedbothmaleandfemale":
                    appData.aGender = E_GenderT.MaleAndFemale;
                    break;
                case "female":
                    appData.aGender = E_GenderT.Female;
                    break;
                case "male":
                    appData.aGender = E_GenderT.Male;
                    break;
                case "informationnotprovidedunknown":
                    appData.aGender = E_GenderT.Unfurnished;
                    break;
                case "notapplicable":
                    message = $"Gender type \"{value}\" is not a supported value.";
                    throw new InvalidFannieFileFormatException(message, message);
                default:
                    message = $"Gender type \"{value}\" is not handled.";
                    throw new InvalidFannieFileFormatException(message, message);
            }
        }

        /// <summary>
        /// Imports the gender refusal indicator.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="value">The value returned by FNMA.</param>
        private void ProcessHmdaGenderRefusalIndicator(CAppData appData, string value)
        {
            if (value == "Y")
            {
                if (appData.aGender == E_GenderT.Male)
                {
                    appData.aGender = E_GenderT.MaleAndNotFurnished;
                }
                else if (appData.aGender == E_GenderT.Female)
                {
                    appData.aGender = E_GenderT.FemaleAndNotFurnished;
                }
                else if (appData.aGender == E_GenderT.MaleAndFemale)
                {
                    appData.aGender = E_GenderT.MaleFemaleNotFurnished;
                }
                else if (appData.aGender == E_GenderT.LeaveBlank)
                {
                    appData.aGender = E_GenderT.Unfurnished;
                }
            }
            else
            {
                if (appData.aGender == E_GenderT.MaleAndNotFurnished)
                {
                    appData.aGender = E_GenderT.Male;
                }
                else if (appData.aGender == E_GenderT.FemaleAndNotFurnished)
                {
                    appData.aGender = E_GenderT.Female;
                }
                else if (appData.aGender == E_GenderT.MaleFemaleNotFurnished)
                {
                    appData.aGender = E_GenderT.MaleAndFemale;
                }
                else if (appData.aGender == E_GenderT.Unfurnished)
                {
                    appData.aGender = E_GenderT.LeaveBlank;
                }
            }
        }

        /// <summary>
        /// Imports the ethnicity type.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="value">The value returned by FNMA.</param>
        private void ProcessHmdaEthnicityType(CAppData appData, string value)
        {
            string message = string.Empty;
            switch (value.ToLower())
            {
                case "hispanicorlatino":
                    appData.aHispanicT = appData.aHispanicT == E_aHispanicT.NotHispanic
                        ? E_aHispanicT.BothHispanicAndNotHispanic
                        : E_aHispanicT.Hispanic;
                    break;
                case "nothispanicorlatino":
                    appData.aHispanicT = appData.aHispanicT == E_aHispanicT.Hispanic
                        ? E_aHispanicT.BothHispanicAndNotHispanic
                        : E_aHispanicT.NotHispanic;
                    break;
                case "informationnotprovidedbyapplicantinmit":
                    appData.aHispanicT = E_aHispanicT.LeaveBlank;
                    break;
                case "notapplicable":
                    message = $"Ethnicity type \"{value}\" is not a supported value.";
                    throw new InvalidFannieFileFormatException(message, message);
                default:
                    message = $"Ethnicity type \"{value}\" is not handled";
                    throw new InvalidFannieFileFormatException(message, message);
            }
        }

        /// <summary>
        /// Imports the ethnicity origin type.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="value">The value returned by FNMA.</param>
        private void ProcessHmdaEthnicityOriginType(CAppData appData, string value)
        {
            switch (value.ToLower())
            {
                case "cuban":
                    appData.aIsCuban = true;
                    break;
                case "mexican":
                    appData.aIsMexican = true;
                    break;
                case "puertorican":
                    appData.aIsPuertoRican = true;
                    break;
                case "other":
                    appData.aIsOtherHispanicOrLatino = true;
                    break;
                default:
                    var message = $"Ethnicity origin type \"{value}\" is not handled.";
                    throw new InvalidFannieFileFormatException(message, message);
            }
        }

        /// <summary>
        /// Imports the race type.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="value">The value returned by FNMA.</param>
        private void ProcessHmdaRaceType(CAppData appData, string value)
        {
            string message = string.Empty;
            switch (value.ToLower())
            {
                case "americanindianoralaskanative":
                    appData.aIsAmericanIndian = true;
                    break;
                case "asian":
                    appData.aIsAsian = true;
                    break;
                case "blackorafricanamerican":
                    appData.aIsBlack = true;
                    break;
                case "nativehawaiianorotherpacificislander":
                    appData.aIsPacificIslander = true;
                    break;
                case "white":
                    appData.aIsWhite = true;
                    break;
                case "informationnotprovidedbyapplicantinmit":
                    appData.aDoesNotWishToProvideRace = true;
                    break;
                case "notapplicable":
                    message = $"Race type \"{value}\" is not a supported value.";
                    throw new InvalidFannieFileFormatException(message, message);
                default:
                    message = $"Race type \"{value}\" is not handled.";
                    throw new InvalidFannieFileFormatException(message, message);
            }
        }

        /// <summary>
        /// Imports the race designation type.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="value">The value returned by FNMA.</param>
        private void ProcessHmdaRaceDesignationType(CAppData appData, string value)
        {
            switch (value.ToLower())
            {
                case "asianindian":
                    appData.aIsAsianIndian = true;
                    break;
                case "chinese":
                    appData.aIsChinese = true;
                    break;
                case "filipino":
                    appData.aIsFilipino = true;
                    break;
                case "guamanianorchamorro":
                    appData.aIsGuamanianOrChamorro = true;
                    break;
                case "japanese":
                    appData.aIsJapanese = true;
                    break;
                case "korean":
                    appData.aIsKorean = true;
                    break;
                case "nativehawaiian":
                    appData.aIsNativeHawaiian = true;
                    break;
                case "samoan":
                    appData.aIsSamoan = true;
                    break;
                case "vietnamese":
                    appData.aIsVietnamese = true;
                    break;
                case "otherasian":
                    appData.aIsOtherAsian = true;
                    break;
                case "otherpacificislander":
                    appData.aIsOtherPacificIslander = true;
                    break;
                default:
                    var message = $"Race designation type \"{value}\" is not handled.";
                    throw new InvalidFannieFileFormatException(message, message);
            }
        }

        /// <summary>
        /// Imports the application taken method type.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <param name="value">The value returned from FNMA.</param>
        private void ProcessApplicationTakenMethodType(CAppData appData, string value)
        {
            switch (value.ToLower())
            {
                case "facetoface":
                    appData.aInterviewMethodT = E_aIntrvwrMethodT.FaceToFace;
                    break;
                case "mail":
                    appData.aInterviewMethodT = E_aIntrvwrMethodT.ByMail;
                    break;
                case "telephone":
                    appData.aInterviewMethodT = E_aIntrvwrMethodT.ByTelephone;
                    break;
                case "internet":
                    appData.aInterviewMethodT = E_aIntrvwrMethodT.Internet;
                    break;
                case "":
                    appData.aInterviewMethodT = E_aIntrvwrMethodT.LeaveBlank;
                    break;
                default:
                    var message = $"Application taken method \"{value}\" is not handled.";
                    throw new InvalidFannieFileFormatException(message, message);
            }
        }

        /// <summary>
        /// Table MDF-8. Loan Product Data Format
        /// </summary>
        /// <param name="ds"></param>
        private void ParseLoanProductData(MORNETPlusDataset ds) 
        {
            #region LNC - Loan Characteristics for Eligibility
            // LNC-020 Lien Type Code
            // LNC-030 Loan Documentation Type Code
            // LNC-040 Subject Proeprty Type Code
            // LNC-050 Reserved for future use
            // LNC-060 Reserved for future use
            // LNC-070 Reserved for future use
            // LNC-080 Reserved for future use
            // LNC-090 Project Classification Code
            // LNC-100 Negative Amortization Limit Percent
            // LNC-110 Baloon Indicator
            // LNC-120 Filler
            // LNC-130 Filler
            // LNC-140 Homebuyer Education Completion Indicator
            // LNC-150 Maximum Lifetime Rate Increase
            // LNC-160 Payment Adjustment Life Percent Cap
            // LNC-170 Payment Adjustment Life Amount Cap
            // LNC-180 Will Escrow be Waived
            // LNC-190 Scheduled Loan Closing Date
            // LNC-200 Scheduled First Payment Date
            // LNC-210 MI Coverage Percent
            // LNC-220 MI Insurer Code
            // LNC-230 APR Spread
            // LNC-240 HOEPA
            // LNC-250 PreApproval

            foreach (MORNETPlusSegment segment in ds.GetSegment("LNC")) 
            {
                m_dataLoan.sLienPosT = TypeMap_sLienPosT.ToLO(segment.Get("LNC-020"));
                m_dataLoan.sFannieDocT = TypeMap_sFannieDocT.ToLO(segment.Get("LNC-030"));

                // OPM 469839
                // When LNC-040 is set to 11, set property type manufactured and MH advantage true.
                // Otherwise, use existing type mappings.
                var propertyTypeCode = segment.Get("LNC-040");
                if (propertyTypeCode == "11")
                {
                    m_dataLoan.sFannieSpT = E_sFannieSpT.Manufactured;
                    m_dataLoan.sHomeIsMhAdvantageTri = E_TriState.Yes;
                }
                else
                {
                    m_dataLoan.sFannieSpT = TypeMap_sFannieSpT.ToLO(propertyTypeCode);
                }

                m_dataLoan.sSpProjectClassFannieT = TypeMap_sSpProjectClassFannieT.ToLO(segment.Get("LNC-090"));
                m_dataLoan.sPmtAdjMaxBalPc_rep = segment.Get("LNC-100");
                m_dataLoan.sFannieHomebuyerEducationT = TypeMap_sFannieHomebuyerEducation.ToLO(segment.Get("LNC-140"));
                m_dataLoan.sRAdjLifeCapR_rep = segment.Get("LNC-150");
                m_dataLoan.sPmtAdjMaxBalPc_rep = segment.Get("LNC-160");
                //                segment.Get("LNC-170"); // SKIP
                m_dataLoan.sWillEscrowBeWaived = segment.GetBool("LNC-180");
                // 4/28/2015 - IR - OPM 212635: Set sEstCloseD and/or sSchedDueD1 only if definied in FNMA 3.2 file.
                string lnc0190 = segment.Get("LNC-190");
                if (false == string.IsNullOrEmpty(lnc0190))
                {
                    m_dataLoan.sEstCloseD_rep = lnc0190;
                    m_dataLoan.sEstCloseDLckd = true;
                }
                string lnc0200 = segment.Get("LNC-200");
                if (false == string.IsNullOrEmpty(lnc0200))
                {
                    m_dataLoan.sSchedDueD1_rep = lnc0200;
                    // 9/26/2013 gf - opm 130112 1st pmt date is now a calculated
                    // field. Lock to maintain imported date.
                    m_dataLoan.sSchedDueD1Lckd = true;
                }
                m_dataLoan.sMInsCoverPc_rep = segment.Get("LNC-210");
                m_dataLoan.sHmdaAprRateSpread = segment.Get("LNC-230");
                m_dataLoan.sHmdaReportAsHoepaLoan = segment.GetBool("LNC-240");
                m_dataLoan.sHmdaPreapprovalT = TypeMap_sHmdaPreapprovalT.ToLO(segment.Get("LNC-250"));
            }
            #endregion

            #region PID - Product Identification
            // PID-020 Product Description
            // PID-030 Product Code
            // PID-040 Product Plan Number
            foreach (MORNETPlusSegment segment in ds.GetSegment("PID")) 
            {
                m_dataLoan.sFannieProdDesc = segment.Get("PID-020"); // 6/30/2009 dd - OPM 32367.
                m_dataLoan.sFannieProductCode =  segment.Get("PID-030");
                m_dataLoan.sFannieARMPlanNum = segment.Get("PID-040");
                m_dataLoan.sFannieARMPlanNumLckd = true;
            }
            #endregion

            #region PCH - Product Characteristics
            // PCH-020 Mortgage Term
            // PCH-030 Assumable Loan Indicator
            // PCH-040 Payment Frequency Code
            // PCH-050 Prepayment Penalty Indicator
            // PCH-060 Prepayment Resticted Indicator
            // PCH-070 Repayment Type Code
            foreach (MORNETPlusSegment segment in ds.GetSegment("PCH")) 
            {
                if (!m_dataLoan.sIsRateLocked) 
                {
                    m_dataLoan.sDue_rep = segment.Get("PCH-020");
                }
                if (segment.Get("PCH-030") == "")
                    m_dataLoan.sAssumeLT = E_sAssumeLT.LeaveBlank;
                else
                    m_dataLoan.sAssumeLT = segment.GetBool("PCH-030") ? E_sAssumeLT.May : E_sAssumeLT.MayNot;
                //                segment.Get("PCH-040"); // Always monthly

                if (segment.Get("PCH-050") == "")
                    m_dataLoan.sPrepmtPenaltyT = E_sPrepmtPenaltyT.LeaveBlank;
                else
                    m_dataLoan.sPrepmtPenaltyT = segment.GetBool("PCH-050") ? E_sPrepmtPenaltyT.May : E_sPrepmtPenaltyT.WillNot;
                //                segment.Get("PCH-060"); // DON'T HAVE
                if (segment.Get("PCH-070") == "F2")
                {
                    // 3/21/2012 dd - Interest Only
                    m_dataLoan.sIOnlyMon_rep = "12";
                }
                else
                {
                    m_dataLoan.sIOnlyMon_rep = "0";
                }

            }
            #endregion

            #region ARM - ARM
            // ARM-020 ARM Index Value
            // ARM-030 Index Type (Index Name)
            // ARM-040 ARM Index Margin
            // ARM-050 ARM Qualifying Rate

            foreach (MORNETPlusSegment segment in ds.GetSegment("ARM")) 
            {
                m_dataLoan.sRAdjIndexR_rep = segment.Get("ARM-020");
                m_dataLoan.sArmIndexT = TypeMap_sArmIndexT.ToLO(segment.Get("ARM-030"));
                m_dataLoan.sArmIndexTLckd = true;

                if (!m_dataLoan.sIsRateLocked) 
                {
                    m_dataLoan.sRAdjMarginR_rep = segment.Get("ARM-040");
                    m_dataLoan.sQualIRLckd = true;
                    m_dataLoan.sQualIR_rep = segment.Get("ARM-050");
                }

            }
            #endregion

            #region PAJ - Payment Adjustment Occurrences
            // PAJ-020 Payment Adjustment Period Number
            // PAJ-030 Payment Adjustment Duration
            // PAJ-040 Payment Adjustment Frequency
            // PAJ-050 Payment Adjustment Type Code
            // PAJ-060 Payment Adjustment Percent
            // PAJ-070 Payment Adjustment Amount
            // PAJ-080 Payment Adjustment Percent Cap
            // PAJ-090 Payment Adjustment Amount Cap
            // PAJ-100 Months to First Payment Adjustment
            //            foreach (MORNETPlusSegment segment in ds.GetSegment("PAJ")) 
            //            {
            //                // 5/7/2004 dd - SKIP
            //            }
            #endregion

            #region RAJ - Rate Adjustment Occurrences
            // RAJ-020 Rate Adjustment Period Number
            // RAJ-030 Rate Adjustment Duration
            // RAJ-040 Rate Adjustment Frequency
            // RAJ-050 Rate Adjustment Calculation Method Code
            // RAJ-060 Rate Adjustment Percent
            // RAJ-070 Rate Adjustment Cap
            // RAJ-080 Months to First Rate Adjustment
            //            foreach (MORNETPlusSegment segment in ds.GetSegment("RAJ")) 
            //            {
            //                // 5/7/2004 dd - SKIP
            //            }
            #endregion

            #region BUA - Buydown Data
            // BUA-020 Buydown Frequency
            // BUA-030 Buydown Duration
            // BUA-040 Increase Rate
            // BUA-050 Funding Code
            // BUA-060 Buydown Base Date Code
            // BUA-070 Buydown Type Indicator

            //            foreach (MORNETPlusSegment segment in ds.GetSegment("BUA")) 
            //            {
            //                // 5/7/2004 dd - SKIP
            //            }
            #endregion

        }


        private void ParseGovernmentData(MORNETPlusDataset ds)
        {
            CAppData dataApp = null;
            #region IDA - Casefile Identification
            // IDA-020 Filler - Reserved for future use, fill with blanks
            #endregion

            #region LEA - Lender Data
            // LEA-020 FHA Lender Identifier
            // LEA-030 FHA Sponsor Identifier
            // LEA-040 Filler - Reserved for future use, fill with blanks
            foreach (MORNETPlusSegment segment in ds.GetSegment("LEA")) 
            {
                var fhaLenderIdFromFnmaFile = segment.Get("LEA-020");
                if (!string.IsNullOrWhiteSpace(fhaLenderIdFromFnmaFile))
                {
                    m_dataLoan.sFHALenderIdCode = fhaLenderIdFromFnmaFile;
                }

                var fhaSponsorIdFromFnmaFile = segment.Get("LEA-030");
                if (!string.IsNullOrWhiteSpace(fhaSponsorIdFromFnmaFile))
                {
                    m_dataLoan.sFHASponsorAgentIdCode = fhaSponsorIdFromFnmaFile;
                }

                string taxIdFromFnmaFile = segment.Get("LEA-040");
                if (!string.IsNullOrWhiteSpace(taxIdFromFnmaFile))
                {
                    bool useNewTaxIdBehavior = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(
                        m_dataLoan.sLoanVersionT,
                        LoanVersionT.V18_ConsolidateTaxIdFields);

                    if (!useNewTaxIdBehavior
                        || m_dataLoan.sSponsoredOriginatorEIN != taxIdFromFnmaFile)
                    {
                        // Note: 
                        // * For loan versions prior to 18, this will set the preparer (and maybe agent)
                        //   fields for the Tax ID.
                        // * For loan versions on/after 18, this will set sFHASponsoredOriginatorEIN.
                        try
                        {
                            m_dataLoan.sSponsoredOriginatorEIN = taxIdFromFnmaFile;

                            if (useNewTaxIdBehavior)
                            {
                                m_dataLoan.sFhaSponsoredOriginatorEinLckd = true;
                            }
                        }
                        catch (FieldInvalidValueException)
                        {
                            Tools.LogInfo($"Unable to import sSponsoredOriginatorEIN. LoanId: {this.m_dataLoan.sLId}, Value: {taxIdFromFnmaFile}");
                        }
                    }
                }
            }

            #endregion

            #region GOA - Government Data For Both FHA/VA Loans
            // GOA-020 Energy Efficient New Home
            // GOA-030 MCC
            // GOA-040 Seller Concessions
            // GOA-050 Borrower Total Closing Costs Fees
            // GOA-060 Borrower Total Closing Costs Points (%)
            // GOA-070 Seller Total Closing Costs Fees
            // GOA-080 Seller Total Closing Costs Points (%)
            // GOA-090 Other Total Closing Costs Fees
            // GOA-100 Other Total Closing Costs Points (%)
            // GOA-110 Type of Refinance
            // GOA-120 Property County

            foreach (MORNETPlusSegment segment in ds.GetSegment("GOA")) 
            {
                m_dataLoan.sFHASellerContribution_rep = segment.Get("GOA-040");
                //m_dataLoan.sFHACcPbb_rep = segment.Get("GOA-050");
                m_dataLoan.sFHACcPbs_rep = segment.Get("GOA-070");
                
                // OPM 48337 - Ignore this segment if the loan is not a refinance
                if (m_dataLoan.sLPurposeT == E_sLPurposeT.Refin || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
                {
                    m_dataLoan.sFHARefinanceTypeDesc = TypeMap_sFHARefinanceTypeDesc.ToLO(segment.Get("GOA-110"));

                    if (segment.Get("GOA-110") == "2" && m_dataLoan.sLT == E_sLT.FHA)
                    {
                        m_dataLoan.sFHAPurposeIsStreamlineRefi = true;
                        m_dataLoan.sFHAPurposeIsStreamlineRefiWithAppr = true;
                        m_dataLoan.sLPurposeT = E_sLPurposeT.FhaStreamlinedRefinance;
                    }

                    if (segment.Get("GOA-110") == "3" && m_dataLoan.sLT == E_sLT.FHA)
                    {
                        m_dataLoan.sFHAPurposeIsStreamlineRefi = true;
                        m_dataLoan.sFHAPurposeIsStreamlineRefiWithoutAppr = true;
                        m_dataLoan.sLPurposeT = E_sLPurposeT.FhaStreamlinedRefinance;
                    }

                    if (segment.Get("GOA-110") == "4" && m_dataLoan.sLT == E_sLT.VA)
                    {
                        m_dataLoan.sLPurposeT = E_sLPurposeT.VaIrrrl;
                        m_dataLoan.sVaLCodeT = E_sVaLCodeT.Irrrl;
                    }

                    if (segment.Get("GOA-110").ToUpper() == "R")
                    {
                        m_dataLoan.sTotalScoreRefiT = E_sTotalScoreRefiT.FHAToFHANonStreamline;
                    }
                }

                string county = segment.Get("GOA-120");
                //OPM 47889
                // If supplied data is a valid County for sSpState, use it
                // Otherwise use the county specified by sSpZip
                var counties = StateInfo.Instance.GetCountiesIn(m_dataLoan.sSpState);
                if (counties.Contains(county, StringComparer.InvariantCultureIgnoreCase))
                {
                    m_dataLoan.sSpCounty = county;
                }
            }
            #endregion

            #region GOB - Government Data For FHA Loans Only
            // GOB-020 Section of the Act
            // GOB-030 Allowable Repairs
            // GOB-040 MIP Upfront (%)
            // GOB-050 MIP Refund Amount
            // GOB-060 First Renewal Rate (%)

            foreach (MORNETPlusSegment segment in ds.GetSegment("GOB")) 
            {
                m_dataLoan.sFHAHousingActSection = segment.Get("GOB-020");

                // OPM 103015.
                switch (segment.Get("GOB-020"))
                {
                    case "": m_dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.LeaveBlank; break;
                    case "203(b)": m_dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.Standard; break;
                    case "203(k)": m_dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.Rehabilitation; break;
                    case "203(k)/251": m_dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.Rehabilitation; break;
                    case "234(c)": m_dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.Standard; break;
                    case "234(c)/251": m_dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.Standard; break;
                    case "257": m_dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.LeaveBlank; break;
                    default: m_dataLoan.sTotalScoreFhaProductT = E_sTotalScoreFhaProductT.LeaveBlank; break;
                }

                decimal v;
                if (decimal.TryParse(segment.Get("GOB-040"), out v))
                {
                    if (v > 0)
                    {
                        // 10/28/2009 dd - OPM 41580. Since DO/DU does not return MIP Upfront %, therefore
                        // we will not override the value if it is missing.
                        m_dataLoan.sFfUfmipR_rep = segment.Get("GOB-040");
                        if (m_fannieMaeImportSource == FannieMaeImportSource.AUD)
                        {
                            m_IssFfUfmipRSet = true;
                        }
                    }
                }
                
                m_dataLoan.sFHASalesConcessions_rep = segment.Get("GOB-050");
            }

            if (m_dataLoan.sFHAHousingActSection == "203(k)" || m_dataLoan.sFHAHousingActSection == "203(k)/251")
            {
                var broker = PrincipalFactory.CurrentPrincipal.BrokerDB;
                var isRemn = broker.BrokerID == new Guid("baf07293-f626-44c6-9750-706199a7c0b6");

                if (isRemn || broker.IsEnableRenovationCheckboxInPML || broker.EnableRenovationLoanSupport)
                {
                    m_dataLoan.sIsRenovationLoan = true;

                    if (isRemn)
                    {
                        this.m_dataLoan.sAltCostLckd = true;
                    }
                }
            }
            #endregion

            #region GOC - Government Data For VA Loans Only
            // GOC-020 CoBorrower Married to Primary Borrower
            // GOC-030 Entitlement Amount
            // GOC-040 Monthly Maintenance
            // GOC-050 Monthly Utilities
            // GOC-060 Funding Fee (%)

            dataApp = m_dataLoan.GetAppData(0);
            foreach (MORNETPlusSegment segment in ds.GetSegment("GOC")) 
            {
                dataApp.aVaEntitleAmt_rep = segment.Get("GOC-030");
                m_dataLoan.sVaProMaintenancePmt_rep = segment.Get("GOC-040");
                m_dataLoan.sVaProUtilityPmt_rep = segment.Get("GOC-050");
            }
            #endregion

            #region GOD - Taxes (From VA folder -> Loan Analysis Page)
            // GOD-020 Applicant Social Security Number
            // GOD-030 Federal Tax
            // GOD-040 State Tax
            // GOD-050 Local Income Tax
            // GOD-060 Social Security Tax
            // GOD-070 Total Non-Taxable Income - Primary
            // GOD-080 Total Non-Taxable Income - Other
            // GOD-090 Total Taxable Income - Primary
            // GOD-100 Total Taxable Income - Other

            bool isBorrower;
            foreach (MORNETPlusSegment segment in ds.GetSegment("GOD")) 
            {
                string aSsn = segment.Get("GOD-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;
                if(isBorrower)
                {
                    //dataApp.aVaBTotITax_rep = segment.Get("GOD-030");
                    dataApp.aVaBStateITax_rep = segment.Get("GOD-040");
                    dataApp.aVaBSsnTax_rep = segment.Get("GOD-060");
                    dataApp.aVaBONetI_rep = segment.Get("GOD-070");
                    dataApp.aVaBEmplmtI_rep = segment.Get("GOD-090");
                }
                else
                {
                    //dataApp.aVaCTotITax_rep = segment.Get("GOD-030");
                    dataApp.aVaCStateITax_rep = segment.Get("GOD-040");
                    dataApp.aVaCSsnTax_rep = segment.Get("GOD-060");
                    dataApp.aVaCONetI_rep = segment.Get("GOD-070");
                    dataApp.aVaCEmplmtI_rep = segment.Get("GOD-090");
                }
            }
            #endregion

            #region GOE - Credit Data
            // GOE-020 Applicant Social Security Number
            // GOE-030 CAIVR #
            // GOE-040 Borrower Credit Rating - Fannie supplied (therefore only for imports from fannie)
            // GOE-050 Bankruptcy < 3 Years - Fannie supplied (therefore only for imports from fannie)
            // GOE-060 Foreclosure - Fannie supplied (therefore only for imports from fannie)
            // GOE-070 First Time Homebuyer Counsel Type - "" Blank
            //                                             A - Not counseled
            //                                             D - HUD Approved Counseline

            foreach (MORNETPlusSegment segment in ds.GetSegment("GOE")) 
            {
                string aSsn = segment.Get("GOE-020");
                dataApp = FindDataAppBySsn(aSsn);
                if (null == dataApp) break;
                isBorrower = dataApp.aBSsn == aSsn;
            
                if(isBorrower)
                {
                    dataApp.aFHABCaivrsNum = segment.Get("GOE-030");
                    dataApp.aBTotalScoreFhtbCounselingT = TypeMap_aTotalScoreFhtbCounselingT.ToLO(segment.Get("GOE-070"));
                }
                else
                {
                    dataApp.aFHACCaivrsNum = segment.Get("GOE-030");
                    dataApp.aCTotalScoreFhtbCounselingT = TypeMap_aTotalScoreFhtbCounselingT.ToLO(segment.Get("GOE-070"));

                }
            }
            #endregion
        }
        
        private void ParseCommunityLendingData(MORNETPlusDataset ds)
        {
            #region LMD - Community Lending Data
            // LMD-020 Metropolitan Statistical Area or County
            // LMD-030 Community Lending Product
            // LMD-040 Fannie Neighbors eligible
            // LMD-050 Community Seconds
            // LMD-060 HUD Median Income
            // LMD-070 Income Limit Adjustment Factors
            // LMD-080 Community Lending Income Limit
            ArrayList segments = ds.GetSegment("LMD");
            if(segments.Count > 0)
                m_dataLoan.sIsCommunityLending = true;
            foreach (MORNETPlusSegment segment in segments) 
            {
                m_dataLoan.sFannieMsa = segment.Get("LMD-020");
                // 3/21/2012 dd - LMD-030 - will also contain LMD-035
                // LMD-030 - 2 characters
                // LMD-035 - 38 characters
                string str = segment.Get("LMD-030");

                string lmd030 = string.Empty;
                string lmd035 = string.Empty;
                if (string.IsNullOrEmpty(str) == false && str.Length > 2)
                {
                    lmd035 = str.Substring(2);
                }
                lmd030 = string.IsNullOrEmpty(str) ? "" : str.Substring(0, 2);
                m_dataLoan.sFannieCommunityLendingT = TypeMap_sFannieCommunityLendingT.ToLO(lmd030);
                m_dataLoan.sFannieCommunitySecondsRepaymentStructureT = TypeMap_sFannieCommunitySecondsRepaymentStructureT.ToLO(lmd035);
                m_dataLoan.sIsFannieNeighbors = segment.GetBool("LMD-040");
                m_dataLoan.sIsCommunitySecond = segment.GetBool("LMD-050");
                m_dataLoan.sFannieIncomeLimitAdjPc_rep = segment.Get("LMD-070");
            }
            #endregion
        }

        /// <summary>
        /// Importing sSpGrossRent
        /// 
        /// OPM 73986 m.p.
        /// </summary>
        private void ImportGrossRent()
        {
            this.m_dataLoan.ImportGrossRent(this.m_hasSubjectPropNetCashFlow, this.m_totalSubjectPropertyNetCashflow);
        }

        private bool AreSameValue(string value1, string value2)
        {
            decimal v1;
            decimal v2;

            if (decimal.TryParse(value1.TrimWhitespaceAndBOM(), out v1) == false)
            {
                v1 = 0;
            }
            if (decimal.TryParse(value2.TrimWhitespaceAndBOM(), out v2) == false)
            {
                v2 = 0;
            }
            return v1 == v2;
        }
    }
}