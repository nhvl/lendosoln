﻿// <copyright file="ClosingCostFeeWrapper.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Isaac Ribakoff
//    Date:   8/11/2015
// </summary>
namespace LendersOffice.Conversions
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;

    /// <summary>
    /// Provides a means to push LOXml data into a LoanClosingCostFee object.
    /// </summary>
    internal class ClosingCostFeeWrapper
    {
        /// <summary>
        /// Action in LOXml to add Fee.
        /// </summary>
        internal static readonly string FEEACTIONADD = "0";

        /// <summary>
        /// Action in LOXml to remove Fee.
        /// </summary>
        internal static readonly string FEEACTIONDELETE = "1";

        /// <summary>
        /// Initializes a new instance of the <see cref="ClosingCostFeeWrapper" /> class.
        /// </summary>
        /// <param name="feeProperties">A collection of LoanClosingCostFee properties to be modified.</param>
        /// <param name="paymentRecords">A collection of payments to be added, modified, or removed from the LoanClosingCostFee object.</param>
        /// <param name="isBorrowerClosingCostFee">A boolean indicating whether the LoanClosingCostFee is a BorrowerClosingCostFee.</param>
        internal ClosingCostFeeWrapper(NameValueCollection feeProperties, IEnumerable<ClosingCostFeePaymentWrapper> paymentRecords, bool isBorrowerClosingCostFee)
        {
            this.FeeProperties = feeProperties;
            this.PaymentRecords = paymentRecords;
            this.IsBorrowerClosingCostFee = isBorrowerClosingCostFee;
            this.ClosingCostFeeMarkedForDeletion = this.FeeProperties["action"] == FEEACTIONDELETE;
        }

        /// <summary>
        /// Gets the LoanClosingCostFee object to be modified.
        /// </summary>
        internal LoanClosingCostFee ClosingCostFee { get; private set; }

        /// <summary>
        /// Gets a value indicating whether associated ClosingCostFee should be removed from the set.
        /// </summary>
        internal bool ClosingCostFeeMarkedForDeletion { get; private set; }

        /// <summary>
        /// Gets a collection of LoanClosingCostFee properties to be modified.
        /// </summary>
        internal NameValueCollection FeeProperties { get; private set; }

        /// <summary>
        /// Gets a collection of payments to be added, modified, or removed from the LoanClosingCostFee object.
        /// </summary>
        internal IEnumerable<ClosingCostFeePaymentWrapper> PaymentRecords { get; private set; }

        /// <summary>
        /// Gets a value indicating whether whether the LoanClosingCostFee is a BorrowerClosingCostFee.
        /// </summary>
        internal bool IsBorrowerClosingCostFee { get; private set; }

        /// <summary>
        /// Attempts to match a given closing cost fee type id with either an existing closing cost fee or available closing cost fee in the set.
        /// </summary>
        /// <param name="closingCostSet">The set of existing closing cost fees in the loan file.</param>
        /// <param name="setFilter">To filter out the passed in set during enumeration.</param>
        /// <param name="availableFeesToAdd">The set of available closing cost fees to be added to the loan file.</param>
        /// <param name="collectionId">The name of the closing cost set being modified.</param>
        /// <param name="warningMessage">StringBuilder to which failure message is appended if closing cost fee cannot be matched.</param>
        /// <returns>Returns a boolean indicating whether wrapper object could be matched with a new or existing closing cost fee.</returns>
        internal bool TryMatchClosingCostFee(LoanClosingCostSet closingCostSet, Func<BaseClosingCostFee, bool> setFilter, IEnumerable<BaseClosingCostFee> availableFeesToAdd, string collectionId, StringBuilder warningMessage)
        {
            Guid closingCostfeeId;
            string closingCostfeeIdUnparsed = this.FeeProperties["closingcostfeetype"];

            if (string.IsNullOrEmpty(closingCostfeeIdUnparsed))
            {
                warningMessage.Append("WARNING: A record in " + collectionId + " does not contain ClosingCostFeeType. This collection will not be modified.").Append(Environment.NewLine);
                return false;
            }
            else if (!Guid.TryParse(closingCostfeeIdUnparsed, out closingCostfeeId))
            {
                warningMessage.Append("WARNING: A record in " + collectionId + " has invalid ClosingCostFeeType: " + closingCostfeeIdUnparsed + ". This collection will not be modified.").Append(Environment.NewLine);
                return false;
            }

            LoanClosingCostFee matchedClosingCostFee = (LoanClosingCostFee)closingCostSet.GetFees(setFilter).FirstOrDefault(f => f.ClosingCostFeeTypeId.Equals(closingCostfeeId));
            if (matchedClosingCostFee == null)
            {
                if (!this.ClosingCostFeeMarkedForDeletion)
                {
                    FeeSetupClosingCostFee matchingFee = (FeeSetupClosingCostFee)availableFeesToAdd.FirstOrDefault(f => f.ClosingCostFeeTypeId.Equals(closingCostfeeId));
                    if (matchingFee != null)
                    {
                        if (this.IsBorrowerClosingCostFee)
                        {
                            matchedClosingCostFee = matchingFee.ConvertToBorrowerClosingCostFee();
                        }
                        else
                        {
                            matchedClosingCostFee = new SellerClosingCostFee();
                            matchingFee.InitializeLoanFee(matchedClosingCostFee);
                        }

                        matchedClosingCostFee.SetClosingCostSet(closingCostSet);
                        matchedClosingCostFee.ChildPaymentList.Clear(); ////Payments will be later set at AddOrUpdate.
                    }
                }

                if (matchedClosingCostFee == null)
                {
                    warningMessage.Append("WARNING: A record in " + collectionId + " has non-matching ClosingCostFeeType: " + closingCostfeeIdUnparsed + ". This collection will not be modified.").Append(Environment.NewLine);
                    return false;
                }
            }

            this.ClosingCostFee = matchedClosingCostFee;

            ////Try matching fee payments.
            foreach (ClosingCostFeePaymentWrapper closingCostFeePaymentWrapper in this.PaymentRecords)
            {
                if (!closingCostFeePaymentWrapper.TryMatchClosingCostFeePayment(this, collectionId, warningMessage))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Pushes updates to associated LoanClosingCostFee object.
        /// </summary>
        /// <param name="dataLoan">The associated loan.</param>
        internal void UpdateClosingCostFee(CPageData dataLoan)
        {
            SetClosingCostFeeProperties(dataLoan, this.ClosingCostFee, this.FeeProperties, this.IsBorrowerClosingCostFee);
            foreach (ClosingCostFeePaymentWrapper paymentRecord in this.PaymentRecords)
            {
                if (paymentRecord.ClosingCostFeePaymentMarkedForDeletion)
                {
                    this.ClosingCostFee.RemovePayment(paymentRecord.ClosingCostFeePayment);
                }
                else
                {
                    paymentRecord.UpdateClosingCostFeePayment();
                    if (paymentRecord.ClosingCostFeePaymentIsNew)
                    {
                        this.ClosingCostFee.ChildPaymentList.Add(paymentRecord.ClosingCostFeePayment);
                    }
                }
            }
        }

        /// <summary>
        /// Applies updates to LoanClosingCostFee object.
        /// </summary>
        /// <param name="dataLoan">The associated loan.</param>
        /// <param name="closingCostFee">The LoanClosingCostFee object to be updated.</param>
        /// <param name="feeProperties">Fee properties to be applied.</param>
        /// <param name="isBorrowerClosingCostFee">Value indicating whether LoanClosingCostFee is a BorrowerClosingCostFee.</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("LendingQBStyleCop.LendingQBCustomRules", "LB1001:DefaultSwitchCaseMustThrowException", Justification = "This is not an enumerated switch. Invalid keys should simply be skipped")]
        private static void SetClosingCostFeeProperties(CPageData dataLoan, LoanClosingCostFee closingCostFee, NameValueCollection feeProperties, bool isBorrowerClosingCostFee)
        {
            bool b;
            foreach (string key in feeProperties)
            {
                string value = feeProperties[key];
                switch (key)
                {
                    case "baseamount":
                        closingCostFee.BaseAmount_rep = value;
                        break;
                    case "beneficiary":
                        try
                        {
                            E_AgentRoleT agentRoleT = (E_AgentRoleT)Enum.Parse(typeof(E_AgentRoleT), value);
                            closingCostFee.Beneficiary = agentRoleT;
                        }
                        catch (ArgumentException) { }
                        break;
                    case "canshop":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.CanShop = b;
                        }

                        break;
                    case "description":
                        closingCostFee.Description = value;
                        break;
                    case "dflp":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.Dflp = b;
                        }

                        break;
                    case "didshop":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.DidShop = b;
                        }

                        break;
                    case "formulat":
                        try
                        {
                            E_ClosingCostFeeFormulaT formulaT = (E_ClosingCostFeeFormulaT)Enum.Parse(typeof(E_ClosingCostFeeFormulaT), value);
                            closingCostFee.FormulaT = formulaT;
                        }
                        catch (ArgumentException) { }
                        break;
                    case "gfeproviderchoicet":
                        if (isBorrowerClosingCostFee)
                        {
                            try
                            {
                                E_GfeProviderChoiceT gfeProviderChoiceT = (E_GfeProviderChoiceT)Enum.Parse(typeof(E_GfeProviderChoiceT), value);
                                ((BorrowerClosingCostFee)closingCostFee).GfeProviderChoiceT = gfeProviderChoiceT;
                            }
                            catch (ArgumentException)
                            {
                            }
                        }

                        break;
                    case "gferesponsiblepartyt":
                        try
                        {
                            E_GfeResponsiblePartyT gfeResponsiblePartyT = (E_GfeResponsiblePartyT)Enum.Parse(typeof(E_GfeResponsiblePartyT), value);
                            closingCostFee.GfeResponsiblePartyT = gfeResponsiblePartyT;
                        }
                        catch (ArgumentException)
                        { }
                        break;
                    case "gfesectiont":
                        try
                        {
                            E_GfeSectionT gfeSectionT = (E_GfeSectionT)Enum.Parse(typeof(E_GfeSectionT), value);
                            closingCostFee.GfeSectionT = gfeSectionT;
                        }
                        catch (ArgumentException) { }
                        break;
                    case "hudline":
                        closingCostFee.HudLine_rep = value;
                        break;
                    case "isaffiliate":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.IsAffiliate = b;
                        }

                        break;
                    case "isapr":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.IsApr = b;
                        }

                        break;
                    case "isfhaallowable":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.IsFhaAllowable = b;
                        }

                        break;
                    case "isoptional":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.IsOptional = b;
                        }

                        break;
                    case "isshowqmwarning":
                        if (isBorrowerClosingCostFee && bool.TryParse(value, out b))
                        {
                            ((BorrowerClosingCostFee)closingCostFee).IsShowQmWarning = b;
                        }

                        break;
                    case "isthirdparty":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.IsThirdParty = b;
                        }

                        break;
                    case "istitlefee":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.IsTitleFee = b;
                        }

                        break;
                    case "isvaallowable":
                        if (bool.TryParse(value, out b))
                        {
                            closingCostFee.IsVaAllowable = b;
                        }

                        break;
                    case "legacygfefieldt":
                        try
                        {
                            E_LegacyGfeFieldT legacyGfeFieldT = (E_LegacyGfeFieldT)Enum.Parse(typeof(E_LegacyGfeFieldT), value);
                            closingCostFee.LegacyGfeFieldT = legacyGfeFieldT;
                        }
                        catch (ArgumentException) { }
                        break;
                    case "mismofeet":
                        try
                        {
                            E_ClosingCostFeeMismoFeeT mismoFeeT = (E_ClosingCostFeeMismoFeeT)Enum.Parse(typeof(E_ClosingCostFeeMismoFeeT), value);
                            closingCostFee.MismoFeeT = mismoFeeT;
                        }
                        catch (ArgumentException) { }
                        break;
                    case "numberofperiods":
                        closingCostFee.NumberOfPeriods_rep = value;
                        break;
                    case "percent":
                        closingCostFee.Percent_rep = value;
                        break;
                    case "percentbaset":
                        try
                        {
                            E_PercentBaseT percentBaseT = (E_PercentBaseT)Enum.Parse(typeof(E_PercentBaseT), value);
                            closingCostFee.PercentBaseT = percentBaseT;
                        }
                        catch (ArgumentException) { }
                        break;
                    case "providerchosenbylender":
                        if (isBorrowerClosingCostFee)
                        {
                            try
                            {
                                E_AgentRoleT providerChosenByLender = (E_AgentRoleT)Enum.Parse(typeof(E_AgentRoleT), value);
                                ((BorrowerClosingCostFee)closingCostFee).ProviderChosenByLender = providerChosenByLender;
                            }
                            catch (ArgumentException)
                            {
                            }
                        }

                        break;
                    case "beneficiaryagentid":
                        Guid agentId;
                        if (Guid.TryParse(value, out agentId))
                        {
                            if (!dataLoan.GetAgentFields(agentId).IsNewRecord)
                            {
                               closingCostFee.BeneficiaryAgentId = agentId;
                            }
                        }

                        break;
                    default:
                        break;
                }
            }
        }
    }
}
