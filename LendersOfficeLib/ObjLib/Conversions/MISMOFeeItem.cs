﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mismo.Closing2_6;

namespace LendersOffice.Conversions
{
    public class MISMOFeeItem
    {
        public E_RespaFeeRespaSectionClassificationType ClassificationType;
        public E_RespaFeeGfeAggregationType AggregationType;
        public E_RespaFeeType Type;
        public string TrueHUDLineNumber;
        public string DMLineNumber;
        public string Description;
        public bool IsPaid;
        public string Amount;
        public int Props;
        public string Point;
        public string FixedAmount;
        public string GFEDisclosedAmount;
        public string PaidToOverride;
        public string ID;
        public string TotalAmount;

        public MISMOFeeItem(E_RespaFeeRespaSectionClassificationType classificationType,
            E_RespaFeeGfeAggregationType aggregationType,
            E_RespaFeeType type,
            string trueHUDLineNumber,
            string dmLineNumber,
            string description,
            bool isPaid,
            string amount,
            int props,
            string point,
            string fixedAmount,
            string gfeDisclosedAmount,
            string paidToOverride,
            string id,
            string totalAmount)
        {
            ClassificationType = classificationType;
            AggregationType = aggregationType;
            Type = type;
            TrueHUDLineNumber = trueHUDLineNumber;
            DMLineNumber = dmLineNumber;
            Description = description;
            IsPaid = isPaid;
            Amount = amount;
            Props = props;
            Point = point;
            FixedAmount = fixedAmount;
            GFEDisclosedAmount = gfeDisclosedAmount;
            PaidToOverride = paidToOverride;
            ID = id;
            TotalAmount = totalAmount;
        }
    }
}
