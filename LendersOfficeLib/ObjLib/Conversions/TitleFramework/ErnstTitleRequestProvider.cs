﻿namespace LendersOffice.Conversions.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using Common;
    using DataAccess;
    using Ernst;
    using Ernst.Request;
    using LendersOffice.Conversions.Templates;
    using LendersOffice.Integration.Templates;
    using LendersOffice.Integration.TitleFramework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Generates a Title request using Ernst's proprietary XML format.
    /// </summary>
    public class ErnstTitleRequestProvider : IRequestProvider
    {
        /// <summary>
        /// The request payload.
        /// </summary>
        private readonly Lazy<Request> payload;

        /// <summary>
        /// The lazy loaded storage of the loan data.
        /// </summary>
        private readonly Lazy<CPageData> lazyLoanData;

        /// <summary>
        /// The data for the request generation.
        /// </summary>
        private ErnstQuoteRequestData requestData;

        /// <summary>
        /// A payload auditor.
        /// </summary>
        private ErnstTitleRequestAuditor auditor;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstTitleRequestProvider"/> class.
        /// </summary>
        /// <param name="requestData">The data for the request generation.</param>
        public ErnstTitleRequestProvider(ErnstQuoteRequestData requestData)
        {
            this.requestData = requestData;
            this.auditor = new ErnstTitleRequestAuditor();

            this.lazyLoanData = new Lazy<CPageData>(() => this.InitializeLoanData());

            this.payload = new Lazy<Request>(() => this.CreateRequest());
        }

        /// <summary>
        /// The loan data object.
        /// </summary>
        private CPageData LoanData => this.lazyLoanData.Value; // Lazy so that derived classes can fire their constructor before Initialize runs

        /// <summary>
        /// Audits the Ernst request and returns the result.
        /// </summary>
        /// <returns>An audit result.</returns>
        public IntegrationAuditResult AuditRequest()
        {
            if (!this.payload.IsValueCreated)
            {
                var request = this.payload.Value;
            }
            
            return this.auditor.Results;
        }

        /// <summary>
        /// Generates an Ernst request payload and serializes it to a string.
        /// </summary>
        /// <returns>A payload string.</returns>
        public string SerializeRequest()
        {
            var request = this.payload.Value;
            return SerializationHelper.XmlSerialize(request);
        }

        /// <summary>
        /// Logs a request payload to PB.
        /// </summary>
        /// <param name="request">The request string.</param>
        public void LogRequest(string request = null)
        {
            if (string.IsNullOrEmpty(request))
            {
                return;
            }

            XElement root = XElement.Parse(request);
            var descendants = root.Descendants();

            var passwords = descendants.Where(el => el.Name.LocalName.Equals("Password", StringComparison.OrdinalIgnoreCase));
            foreach (var password in passwords)
            {
                password.Value = "******";
            }

            TitleUtilities.LogPayload(root.ToString(SaveOptions.DisableFormatting), isResponse: false);
        }

        /// <summary>
        /// Initializes the loan data object for the export from the request.
        /// </summary>
        /// <returns>An object providing the loan data for the request.</returns>
        protected virtual CPageData InitializeLoanData()
        {
            var loanData = CPageData.CreateUsingSmartDependency(this.requestData.LoanId, typeof(ErnstTitleRequestProvider));
            loanData.InitLoad();
            return loanData;
        }

        /// <summary>
        /// Fixes up the county into the real name, since we don't always store the right value.
        /// </summary>
        /// <param name="lqbCounty">The actual county that our system contains.</param>
        /// <param name="state">The state of <paramref name="lqbCounty"/>.</param>
        /// <returns>The actual county name.</returns>
        private static string GetCorrectedCounty(string lqbCounty, string state)
        {
            if (StringComparer.OrdinalIgnoreCase.Equals(state, "FL") && StringComparer.OrdinalIgnoreCase.Equals(lqbCounty, "Miami dade"))
            {
                return "Miami-Dade";
            }

            return lqbCounty;
        }

        /// <summary>
        /// Fixes up the county into a value Ernst can process.
        /// </summary>
        /// <param name="actualCounty">The actual county that our system contains.</param>
        /// <param name="state">The state of <paramref name="actualCounty"/>.</param>
        /// <returns>The county value Ernst expects.</returns>
        private static string GetErnstRequestPropertyCounty(string actualCounty, string state)
        {
            actualCounty = GetCorrectedCounty(actualCounty, state);
            Dictionary<string, string> countyFixup = null;
            if (StringComparer.OrdinalIgnoreCase.Equals(state, "AK"))
            {
                countyFixup = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "Dillingham", "Bristol Bay" },
                    { "Southeast Fairbanks", "Bristol Bay" },
                    { "Denali", "Talkeetna" },
                };
            }
            else if (StringComparer.OrdinalIgnoreCase.Equals(state, "HI"))
            {
                return "Hawaii";
            }
            else if (StringComparer.OrdinalIgnoreCase.Equals(state, "VA"))
            {
                countyFixup = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    { "Covington City", "Alleghany" },
                    { "Falls Church City", "Arlington" },
                    { "Galax City", "Carroll" },
                    { "Fairfax City", "Fairfax" },
                    { "Emporia City", "Greensville" },
                    { "Williamsburg City", "James City" },
                    { "Manassas Park City", "Prince William" },
                    { "Manassas City", "Prince William" },
                    { "Lexington City", "Rockbridge" },
                    { "Harrisonburg City", "Rockingham" },
                    { "Franklin City", "Southampton" },
                    { "Norton City", "Wise" },
                    { "Poquoson City", "York" },
                };
            }

            return countyFixup?.GetValueOrNull(actualCounty) ?? actualCounty;
        }

        /// <summary>
        /// Parses and returns the surname of a loan agent individual.
        /// </summary>
        /// <param name="agent">The agent to get the surname of.</param>
        /// <returns>The agent's surname.</returns>
        private static string GetAgentSurname(CAgentFields agent)
        {
            var nameParser = new CommonLib.Name();
            try
            {
                nameParser.ParseName(agent.AgentName);
            }
            catch (NullReferenceException)
            {
                Tools.LogWarning($"ErnstTitleRequestProvider: Name parser couldn't parse name \"{agent.AgentName}\".");
                return null;
            }

            return nameParser.LastName;
        }

        /// <summary>
        /// Gets the count of "grantors" on an application who will appear on the property title.
        /// </summary>
        /// <param name="app">The loan application.</param>
        /// <returns>The number of grantors on the property title (0, 1, or 2).</returns>
        private static int GetAppGrantorCount(CAppData app)
        {
            int grantorCount = 0;
            if (app.aBTypeT == E_aTypeT.Individual || app.aBTypeT == E_aTypeT.TitleOnly)
            {
                grantorCount++;
            }

            if (app.aCTypeT == E_aTypeT.Individual || app.aCTypeT == E_aTypeT.TitleOnly)
            {
                grantorCount++;
            }

            return grantorCount;
        }

        /// <summary>
        /// Gets a list of the surnames on an application.
        /// </summary>
        /// <param name="app">The loan application.</param>
        /// <returns>A list containing the borrower and co-borrower surnames, if those borrowers will appear on the title and are non-empty.</returns>
        private static List<string> GetAppGrantorSurnames(CAppData app)
        {
            List<string> surnames = new List<string>();
            if ((app.aBTypeT == E_aTypeT.Individual || app.aBTypeT == E_aTypeT.TitleOnly) && !string.IsNullOrEmpty(app.aBLastNm))
            {
                surnames.Add(app.aBLastNm);
            }

            if ((app.aCTypeT == E_aTypeT.Individual || app.aCTypeT == E_aTypeT.TitleOnly) && !string.IsNullOrEmpty(app.aCLastNm))
            {
                surnames.Add(app.aCLastNm);
            }

            return surnames;
        }

        /// <summary>
        /// Uses the configuration file to write vendor-specific data points to the request.
        /// </summary>
        /// <param name="request">The request payload.</param>
        private void WriteDatapointsFromVendorConfig(Request request)
        {
            var config = this.requestData.ConfigurationDefaultValues;

            if (config != null)
            {
                UnitedStatesState? propertyState = UnitedStatesState.CreateWithValidation(this.LoanData.sSpState);
                E_sLT loanType = this.LoanData.sLT;

                Dictionary<SimpleXPath, string> finalValues = new Dictionary<SimpleXPath, string>();
                List<ErnstConfigurationEntry> functionEntries = new List<ErnstConfigurationEntry>();
                ErnstConfigurationPredefinedFunctionResolver resolver = new ErnstConfigurationPredefinedFunctionResolver();
                string error;

                foreach (var entry in config.GetPathsFor(propertyState, loanType))
                {
                    if (entry.PredefinedFunction == null)
                    {
                        finalValues.Add(entry.PathIdentifier, entry.DefaultValue.Value);
                        this.requestData.ExportedConfigurationItems.Add(entry);
                    }
                    else
                    {
                        functionEntries.Add(entry);
                    }
                }

                // Functions need to be applied after all initial entries have been sorted out.
                foreach (var functionEntry in functionEntries)
                {
                    var startingValue = finalValues.GetValueOrNull(functionEntry.PathIdentifier) ?? string.Empty;
                    string finalValue;

                    var applyStatus = resolver.ApplyFunction(startingValue, functionEntry.DefaultValue.Value, this.LoanData, functionEntry.PredefinedFunction, out finalValue, out error);
                    if (applyStatus == ErnstConfigFunctionResolverApplyStatus.Applied)
                    {
                        finalValues[functionEntry.PathIdentifier] = finalValue;
                        this.requestData.ExportedConfigurationItems.Add(functionEntry);
                    }
                    else if (applyStatus != ErnstConfigFunctionResolverApplyStatus.ConditionNotMet)
                    {
                        throw new DeveloperException(ErrorMessage.BadConfiguration, new SimpleContext(error));
                    }
                }

                foreach (var entry in finalValues)
                {
                    var setter = new ErnstRequestValueSetter(entry.Key, ErnstConfigurationValue.Create(entry.Value).Value);
                    setter.SetValue(request);
                }
            }
        }

        /// <summary>
        /// Creates a Block6Request container (Inspection fees).
        /// </summary>
        /// <returns>A Block6Request container.</returns>
        private Block6Request CreateBlock6Request()
        {
            var request = new Block6Request();

            request.Version = 1m;
            request.Property = this.CreateBlock6RequestProperty();
            request.Standard = this.CreateStandard();
            request.Pest = this.CreatePest();

            return request;
        }

        /// <summary>
        /// Creates a Block6RequestProperty container.
        /// </summary>
        /// <returns>A Block6RequestProperty container.</returns>
        private Block6RequestProperty CreateBlock6RequestProperty()
        {
            var property = new Block6RequestProperty();
            property.Address1 = this.LoanData.sSpAddr;
            property.City = this.LoanData.sSpCity;
            property.County = GetCorrectedCounty(this.LoanData.sSpCounty, this.LoanData.sSpState);
            property.State = this.LoanData.sSpState;
            property.Zip = this.LoanData.sSpZip;

            property.YearBuilt = this.LoanData.sYrBuilt;

            this.auditor.ValidateBlock6RequestProperty(property);

            return property;
        }

        /// <summary>
        /// Creates an ErnstRequest container.
        /// </summary>
        /// <returns>An ErnstRequest container.</returns>
        private ErnstRequest CreateErnstRequest()
        {
            var request = new ErnstRequest();

            request.Version = 1m;
            request.TransactionCode = "100";

            if (this.LoanData.sEstCloseD.IsValid)
            {
                request.EstimatedClosingDate = this.LoanData.sEstCloseD.DateTimeForComputation;
            }

            request.Property = this.CreateProperty();
            request.Mortgage = this.CreateMortgage();
            request.Deed = this.CreateDeed();
            request.Subordination = this.CreateSubordination();
            request.POA = this.CreatePoa();

            return request;
        }

        /// <summary>
        /// Creates an ErnstRequestPOA container.
        /// </summary>
        /// <returns>An ErnstRequestPOA container.</returns>
        private ErnstRequestPOA CreatePoa()
        {
            return new ErnstRequestPOA
            {
                NumberOfPowersOfAttorney = this.LoanData.Apps.SelectMany(app => new int[] { string.IsNullOrEmpty(app.aBPowerOfAttorneyNm) ? 0 : 1, string.IsNullOrEmpty(app.aCPowerOfAttorneyNm) ? 0 : 1 }).Sum().ToString()
            };
        }

        /// <summary>
        /// Creates an ErnstRequestSubordination container.
        /// </summary>
        /// <returns>An ErnstRequestSubordination container.</returns>
        private ErnstRequestSubordination CreateSubordination()
        {
            return new ErnstRequestSubordination
            {
                NumberOfSubordinations = this.LoanData.sMortgageLiaList.Count(lia => lia.IsPiggyBack).ToString(),
            };
        }

        /// <summary>
        /// Creates an ErnstRequestDeed container.
        /// </summary>
        /// <returns>An ErnstRequestDeed container.</returns>
        private ErnstRequestDeed CreateDeed()
        {
            int numberOfSellers = this.LoanData.sSellerCollection.ListOfSellers.Count(seller => !string.IsNullOrEmpty(seller.Name));

            int numberOfSurnames = this.LoanData.sAgents.Any(agent => agent.AgentRoleT == E_AgentRoleT.Seller && !string.IsNullOrEmpty(GetAgentSurname(agent)))
                            ? this.LoanData.sAgents.Where(agent => agent.AgentRoleT == E_AgentRoleT.Seller).Select(GetAgentSurname).Distinct().Count(name => !string.IsNullOrEmpty(name))
                            : numberOfSellers;
            int numberOfSignatures = numberOfSellers > 0
                            ? numberOfSellers
                            : this.LoanData.sAgents.Count(agent => agent.AgentRoleT == E_AgentRoleT.Seller && !string.IsNullOrEmpty(agent.AgentName));

            return new ErnstRequestDeed
            {
                Index = new ErnstIndex
                {
                    NumberOfGrantors = this.LoanData.Apps.Sum(GetAppGrantorCount).ToString(),
                    NumberOfSurnames = numberOfSurnames.ToString(),
                    NumberOfSignatures = numberOfSignatures.ToString()
                }
            };
        }

        /// <summary>
        /// Creates an ErnstRequestMortgage container.
        /// </summary>
        /// <returns>An ErnstRequestMortgage container.</returns>
        private ErnstRequestMortgage CreateMortgage()
        {
            int mortgageGrantorCount = this.LoanData.Apps.Sum(GetAppGrantorCount);
            int numberOfSignatures = string.IsNullOrEmpty(this.LoanData.sTrustName) ? mortgageGrantorCount : 2 * mortgageGrantorCount;
            return new ErnstRequestMortgage
            {
                Index = new ErnstIndex
                {
                    NumberOfGrantors = mortgageGrantorCount.ToString(),
                    NumberOfSurnames = this.LoanData.Apps.SelectMany(GetAppGrantorSurnames).Distinct().Count().ToString(),
                    NumberOfSignatures = numberOfSignatures.ToString(),
                }
            };
        }

        /// <summary>
        /// Creates a TitleRequestItemizedSettlementFees container.
        /// </summary>
        /// <returns>A TitleRequestItemizedSettlementFees container.</returns>
        private TitleRequestItemizedSettlementFees CreateItemizedSettlementFees()
        {
            var itemizedSettlementFees = new TitleRequestItemizedSettlementFees();
            itemizedSettlementFees.Requested = true;

            return itemizedSettlementFees;
        }

        /// <summary>
        /// Creates a TitlePolicy container for the lender policy.
        /// </summary>
        /// <returns>A TitlePolicy container.</returns>
        private TitlePolicy CreateLendersPolicy()
        {
            var policy = new TitlePolicy();
            policy.Requested = true;
            policy.PolicyAmount = this.LoanData.sFinalLAmt;

            return policy;
        }

        /// <summary>
        /// Creates a TitlePolicy container for the owner policy.
        /// </summary>
        /// <returns>A TitlePolicy container.</returns>
        private TitlePolicy CreateOwnersPolicy()
        {
            var policy = new TitlePolicy();
            policy.Requested = true;
            policy.PolicyAmount = Math.Max(this.LoanData.sApprVal, this.LoanData.sPurchPrice);

            return policy;
        }

        /// <summary>
        /// Creates a Block6Service container.
        /// </summary>
        /// <returns>A Block6Service container.</returns>
        private Block6Service CreatePest()
        {
            var pest = new Block6Service();
            pest.Requested = true;

            return pest;
        }

        /// <summary>
        /// Creates a Property container.
        /// </summary>
        /// <returns>A Property container.</returns>
        private Property CreateProperty()
        {
            var property = new Property();

            property.FullAddress = this.LoanData.sSpAddr;
            property.City = this.LoanData.sSpCity;
            property.County = GetErnstRequestPropertyCounty(this.LoanData.sSpCounty, this.LoanData.sSpState);
            property.State = this.LoanData.sSpState;

            property.EstimatedValue = this.LoanData.sIsRefinancing ? default(ErnstString) : (ErnstString)this.LoanData.sTransferTaxEstimatedBasisValue_rep;
            property.MortgageAmount = this.LoanData.sFinalLAmt_rep;
            property.UnpaidPrincipalBalance = this.LoanData.sSpLien_rep;

            property.StateQuestions = this.CreateStateQuestions();

            this.auditor.ValidateProperty(property);

            return property;
        }

        /// <summary>
        /// Creates a PropertyTaxRequest container.
        /// </summary>
        /// <returns>A PropertyTaxRequest container.</returns>
        private PropertyTaxRequest CreatePropertyTaxRequest()
        {
            var request = new PropertyTaxRequest();

            request.Version = 0m;
            request.Property = this.CreatePropertyTaxRequestProperty();

            return request;
        }

        /// <summary>
        /// Creates a PropertyTaxRequestProperty container.
        /// </summary>
        /// <returns>A PropertyTaxRequestProperty container.</returns>
        private PropertyTaxRequestProperty CreatePropertyTaxRequestProperty()
        {
            var property = new PropertyTaxRequestProperty();
            property.StreetAddress = this.LoanData.sSpAddr;
            property.City = this.LoanData.sSpCity;
            property.State = this.LoanData.sSpState;
            property.PostalCode = this.LoanData.sSpZip;

            this.auditor.ValidatePropertyTaxRequestProperty(property);

            return property;
        }

        /// <summary>
        /// Creates a Request container.
        /// </summary>
        /// <returns>A Request container.</returns>
        /// <remarks>
        /// As the export process also hooks into the auditor and the request data to save the
        /// information of the order, this method has side effects, meaning access should go through
        /// <see cref="payload"/>.
        /// </remarks>
        private Request CreateRequest()
        {
            var request = new Request();

            request.Version = 2m;
            request.TransactionDate = DateTime.Now;

            request.Authentication = this.CreateRequestAuthentication();
            request.RequestInfo = this.CreateRequestInfo();

            var feesToInclude = this.requestData.FeesToInclude;

            if (feesToInclude.HasFlag(ErnstQuoteFeeCategoryFlags.Government))
            {
                request.ErnstRequest = this.CreateErnstRequest();
            }

            if (feesToInclude.HasFlag(ErnstQuoteFeeCategoryFlags.Title))
            {
                request.TitleRequest = this.CreateTitleRequest();
            }

            if (feesToInclude.HasFlag(ErnstQuoteFeeCategoryFlags.Inspection))
            {
                request.Block6Request = this.CreateBlock6Request();
            }

            if (feesToInclude.HasFlag(ErnstQuoteFeeCategoryFlags.PropertyTax))
            {
                request.PropertyTaxRequest = this.CreatePropertyTaxRequest();
            }

            this.WriteDatapointsFromVendorConfig(request);

            // Vendor config may write values to elements that should not be included for a given request type
            if (!feesToInclude.HasFlag(ErnstQuoteFeeCategoryFlags.Government))
            {
                request.ErnstRequest = null;
            }

            if (!feesToInclude.HasFlag(ErnstQuoteFeeCategoryFlags.Title))
            {
                request.TitleRequest = null;
            }

            if (!feesToInclude.HasFlag(ErnstQuoteFeeCategoryFlags.Inspection))
            {
                request.Block6Request = null;
            }

            if (!feesToInclude.HasFlag(ErnstQuoteFeeCategoryFlags.PropertyTax))
            {
                request.PropertyTaxRequest = null;
            }

            return request;
        }

        /// <summary>
        /// Creates a RequestAuthentication container.
        /// </summary>
        /// <returns>A RequestAuthentication container.</returns>
        private RequestAuthentication CreateRequestAuthentication()
        {
            var authentication = new RequestAuthentication();
            authentication.UserID = this.requestData.Username;
            authentication.Password = this.requestData.Password;

            return authentication;
        }

        /// <summary>
        /// Creates a RequestInfo container.
        /// </summary>
        /// <returns>A RequestInfo container.</returns>
        private RequestInfo CreateRequestInfo()
        {
            var requestInfo = new RequestInfo();
            requestInfo.Loan = this.CreateRequestInfoLoan();
            requestInfo.Property = this.CreateRequestInfoProperty();

            return requestInfo;
        }

        /// <summary>
        /// Creates a RequestInfoLoan container.
        /// </summary>
        /// <returns>A RequestInfoLoan container.</returns>
        private RequestInfoLoan CreateRequestInfoLoan()
        {
            var loan = new RequestInfoLoan();
            loan.RateType = this.ToRequestInfoLoanRateType(this.LoanData.sFinMethT);
            loan.BalloonIndicator = this.LoanData.sGfeIsBalloon;
            loan.Texas50a6ApplicabilityIndicator = new ErnstBoolean(this.LoanData.sProdIsTexas50a6Loan);

            return loan;
        }

        /// <summary>
        /// Returns the rate type corresponding to the loan financing method.
        /// </summary>
        /// <param name="financeMethod">The loan financing method.</param>
        /// <returns>The corresponding rate type.</returns>
        private RequestInfoLoanRateType ToRequestInfoLoanRateType(E_sFinMethT financeMethod)
        {
            if (financeMethod == E_sFinMethT.ARM)
            {
                return RequestInfoLoanRateType.AdjustableRate;
            }
            else if (financeMethod == E_sFinMethT.Graduated)
            {
                return RequestInfoLoanRateType.GPM;
            }
            else
            {
                return RequestInfoLoanRateType.Fixed;
            }
        }

        /// <summary>
        /// Creates a RequestInfoProperty container.
        /// </summary>
        /// <returns>A RequestInfoProperty container.</returns>
        private RequestInfoProperty CreateRequestInfoProperty()
        {
            var property = new RequestInfoProperty();
            property.ProjectLegalStructure = this.ToRequestInfoPropertyProjectLegalStructure(this.LoanData.sGseSpT);
            property.ConstructionMethod = this.ToRequestInfoPropertyConstructionMethod(this.LoanData.sGseSpT);
            property.PUD = this.LoanData.sSpIsInPud;

            return property;
        }

        /// <summary>
        /// Returns the structure type corresponding to the GSE property type.
        /// </summary>
        /// <param name="propertyType">The GSE property type.</param>
        /// <returns>The corresponding structure type.</returns>
        private RequestInfoPropertyProjectLegalStructure ToRequestInfoPropertyProjectLegalStructure(E_sGseSpT propertyType)
        {
            if (propertyType == E_sGseSpT.Condominium ||
                propertyType == E_sGseSpT.DetachedCondominium ||
                propertyType == E_sGseSpT.HighRiseCondominium || 
                propertyType == E_sGseSpT.ManufacturedHomeCondominium)
            {
                return RequestInfoPropertyProjectLegalStructure.Condominium;
            }
            else if (propertyType == E_sGseSpT.Cooperative)
            {
                return RequestInfoPropertyProjectLegalStructure.Cooperative;
            }
            else
            {
                return RequestInfoPropertyProjectLegalStructure.Unknown;
            }
        }

        /// <summary>
        /// Returns the construction method corresponding to the GSE property type.
        /// </summary>
        /// <param name="propertyType">The GSE property type.</param>
        /// <returns>The corresponding construction method.</returns>
        private RequestInfoPropertyConstructionMethod ToRequestInfoPropertyConstructionMethod(E_sGseSpT propertyType)
        {
            if (propertyType == E_sGseSpT.ManufacturedHomeCondominium ||
                propertyType == E_sGseSpT.ManufacturedHomeMultiwide ||
                propertyType == E_sGseSpT.ManufacturedHousing ||
                propertyType == E_sGseSpT.ManufacturedHousingSingleWide)
            {
                return RequestInfoPropertyConstructionMethod.Manufactured;
            }
            else
            {
                return RequestInfoPropertyConstructionMethod.SiteBuilt;
            }
        }

        /// <summary>
        /// Creates a Block6Service container for the standard service.
        /// </summary>
        /// <returns>A Block6Service container.</returns>
        private Block6Service CreateStandard()
        {
            var standard = new Block6Service();
            standard.Requested = true;

            return standard;
        }

        /// <summary>
        /// Creates the hardcoded answers to the state questions.
        /// </summary>
        /// <returns>The state questions element with the specified questions answered.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            "StyleCop.CSharp.NamingRules",
            "SA1305:FieldNamesMustNotUseHungarianNotation",
            Justification = "The properties using \"Hungarian\" notation are actually just properties of CPageData, cached as local variables.")]
        private PropertyStateQuestions CreateStateQuestions()
        {
            // The logic of this function is horrible, but intended to follow the Excel mapping file as closely as possible.
            // The mapping breaks down into mutually exclusive cases by the state, but splitting this into functions by state
            // didn't seem particularly helpful.
            var questions = new PropertyStateQuestions();
            const decimal OneMillion = 1000000;
            string sSpState = this.LoanData.sSpState;
            if (sSpState == "AL")
            {
                if (this.LoanData.sIsRefinancing)
                {
                    questions.Q1 = 0;
                    questions.Q2 = 0;
                    questions.Q3 = 0;
                    questions.Q4 = 0;
                }
                else
                {
                    questions.Q4 = 1;
                }

                if (this.LoanData.BrokerDB.IsFederalCreditUnion)
                {
                    questions.Q11 = 1;
                }
            }
            else if (sSpState == "CA")
            {
                questions.Q4 = 0;
                questions.Q12 = 0;
            }
            else if (sSpState == "CT")
            {
                questions.Q4 = 1;
                questions.Q5 = 1;
            }
            else if (sSpState == "DC")
            {
                questions.Q1 = 1;
                questions.Q10 = 1;
                questions.Q4 = 1;
                if (!this.LoanData.sIsRefinancing)
                {
                    questions.Q6 = 1;
                }
            }
            else if (sSpState == "DE")
            {
                questions.Q4 = 0;
            }
            else if (sSpState == "FL")
            {
                questions.Q2 = 0;
                questions.Q1 = 0;
                if (this.LoanData.sProdSpT == E_sProdSpT.SFR)
                {
                    questions.Q4 = 1;
                }

                if (this.LoanData.BrokerDB.IsCreditUnion)
                {
                    questions.Q11 = 1;
                }
            }
            else if (sSpState == "GA")
            {
                questions.Q1 = 0;
                if (this.LoanData.sDue <= 36)
                {
                    questions.Q2 = 1;
                }

                if (this.LoanData.BrokerDB.IsCreditUnion)
                {
                    questions.Q11 = 1;
                }
            }
            else if (sSpState == "HI")
            {
                if (this.LoanData.sProdSpT.EqualsOneOf(E_sProdSpT.SFR, E_sProdSpT.Condo))
                {
                    questions.Q4 = 1;
                }
            }
            else if (sSpState == "IL")
            {
                questions.Q1 = 0;
            }
            else if (sSpState == "KS")
            {
                questions.Q1 = 0;
                questions.Q2 = 0;
                if (this.LoanData.sProdSpT == E_sProdSpT.SFR)
                {
                    questions.Q3 = 1;
                }
            }
            else if (sSpState == "LA")
            {
                if (this.LoanData.sLienPosT == E_sLienPosT.Second)
                {
                    questions.Q1 = 1;
                }

                questions.Q2 = this.LoanData.sIsRefinancing ? 0 : 1;
                if (this.LoanData.sProdSpT.EqualsOneOf(E_sProdSpT.SFR, E_sProdSpT.PUD, E_sProdSpT.Condo))
                {
                    questions.Q3 = 1;
                }
            }
            else if (sSpState == "MD")
            {
                questions.Q12 = 0;
                questions.Q7 = 0;
                questions.Q8 = 0;
                if (this.LoanData.sOccT == E_sOccT.PrimaryResidence)
                {
                    questions.Q5 = 1;
                    questions.Q6 = 1;
                    questions.Q10 = 1;
                    questions.Q17 = 1;
                    questions.Q18 = 1;
                }

                if (this.LoanData.sIsRefinancing)
                {
                    questions.Q2 = 0;
                    questions.Q3 = 1;
                }
                else
                {
                    questions.Q2 = 1;
                }
            }
            else if (sSpState == "MN")
            {
                questions.Q1 = 0;
                questions.Q4 = 0;
            }
            else if (sSpState == "NJ")
            {
                if (this.LoanData.sHouseVal > OneMillion)
                {
                    questions.Q4 = 1;
                }
            }
            else if (sSpState == "NY")
            {
                questions.Q12 = 0;
                questions.Q17 = 1;
                questions.Q18 = 1;
                questions.Q7 = 0;
                E_sProdSpT sProdSpT = this.LoanData.sProdSpT;
                if (sProdSpT == E_sProdSpT.FourUnits)
                {
                    questions.Q5 = 0;
                }

                int sSpCountyFips = this.LoanData.sSpCountyFips;
                if (sProdSpT.EqualsOneOf(E_sProdSpT.SFR, E_sProdSpT.PUD, E_sProdSpT.TwoUnits, E_sProdSpT.Condo)
                    || (sProdSpT == E_sProdSpT.CoOp && !sSpCountyFips.EqualsOneOf(36005, 36047, 36061, 36081, 36085)))
                {
                    questions.Q1 = 1;
                }

                if (sProdSpT == E_sProdSpT.SFR)
                {
                    questions.Q10 = 1;
                }

                if (this.LoanData.BrokerDB.IsCreditUnion)
                {
                    questions.Q11 = 1;
                }

                if (sProdSpT == E_sProdSpT.CoOp)
                {
                    questions.Q14 = 1;
                }

                if (sProdSpT == E_sProdSpT.ThreeUnits)
                {
                    questions.Q2 = 1;
                }

                if (sSpCountyFips.EqualsOneOf(36005, 36047, 36061, 36081, 36085)
                    && sProdSpT.EqualsOneOf(E_sProdSpT.SFR, E_sProdSpT.PUD, E_sProdSpT.TwoUnits, E_sProdSpT.ThreeUnits, E_sProdSpT.Condo, E_sProdSpT.CoOp))
                {
                    questions.Q4 = 1;
                }

                if ((this.LoanData.sHouseVal >= OneMillion)
                    && sProdSpT.EqualsOneOf(E_sProdSpT.SFR, E_sProdSpT.PUD, E_sProdSpT.TwoUnits, E_sProdSpT.ThreeUnits, E_sProdSpT.Condo, E_sProdSpT.CoOp))
                {
                    questions.Q6 = 1;
                    questions.V1 = 100;
                }

                if (this.LoanData.sIsRefinancing)
                {
                    if (this.LoanData.sIsOFinCreditLineInDrawPeriod && !this.LoanData.sIsOFinNew && this.LoanData.sOccT == E_sOccT.PrimaryResidence)
                    {
                        questions.Q3 = 1;
                    }
                    else
                    {
                        questions.Q8 = 1;
                    }
                }

                ////if (/*field for existing liens which will not be paid off*/)
                ////{
                ////    questions.Q9 = 1;
                ////    questions.V5 = (int)this.loanData.sSubFin;
                ////}
            }
            else if (sSpState == "OK")
            {
                questions.Q1 = 0;
                if (this.LoanData.BrokerDB.IsFederalCreditUnion)
                {
                    questions.Q11 = 1;
                }
            }
            else if (sSpState == "PA")
            {
                questions.Q1 = 0;
                questions.Q2 = 0;
            }
            else if (sSpState == "TN")
            {
                questions.Q1 = 0;
            }
            else if (sSpState == "UT")
            {
                questions.Q1 = 1;
            }
            else if (sSpState == "VA")
            {
                if (this.LoanData.sIsRefinancing)
                {
                    questions.Q3 = 1;
                }
            }
            else if (sSpState == "VT")
            {
                if (this.LoanData.sOccT == E_sOccT.PrimaryResidence)
                {
                    questions.Q4 = 1;
                }
            }
            else if (sSpState == "WA")
            {
                questions.Q7 = 0;
            }
            else
            {
                questions = null;
            }

            return questions;
        }

        /// <summary>
        /// Creates a TitleRequest container.
        /// </summary>
        /// <returns>A TitleRequest container.</returns>
        private TitleRequest CreateTitleRequest()
        {
            var request = new TitleRequest();

            request.Version = 1m;
            request.ProviderID = this.requestData.TitleProviderCode;
            request.PolicyType = TitleRequestPolicyType.New;
            request.UseCommonEndorsements = true;
            request.UseSimultaneousRates = true;

            request.ItemizedSettlementFees = this.CreateItemizedSettlementFees();
            request.LendersPolicy = this.CreateLendersPolicy();
            request.OwnersPolicy = this.CreateOwnersPolicy();
            request.Property = this.CreateTitleRequestProperty();

            return request;
        }

        /// <summary>
        /// Creates a TitleRequestProperty container.
        /// </summary>
        /// <returns>A TitleRequestProperty container.</returns>
        private TitleRequestProperty CreateTitleRequestProperty()
        {
            var property = new TitleRequestProperty();

            property.City = this.LoanData.sSpCity;
            property.County = GetCorrectedCounty(this.LoanData.sSpCounty, this.LoanData.sSpState);
            property.State = this.LoanData.sSpState;
            property.Zip = this.LoanData.sSpZip;

            property.LoanType = this.LoanData.sIsRefinancing
                ? TitleRequestPropertyLoanType.Refinance
                : TitleRequestPropertyLoanType.Sale;
            property.ClosingDate = this.LoanData.sEstCloseD.DateTimeForComputation;

            this.auditor.ValidateTitleRequestProperty(property);

            return property;
        }
    }
}
