﻿namespace LendersOffice.Conversions.TitleFramework
{
    using Ernst.Request;
    using LendersOffice.Conversions.Templates;
    using Security;
    using TitleFrameworkErrors = LendersOffice.Common.ErrorMessages.TitleFrameworkErrors;

    /// <summary>
    /// Audits an Ernst Title request to ensure it contains required data points.
    /// </summary>
    public class ErnstTitleRequestAuditor : AbstractIntegrationRequestAuditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstTitleRequestAuditor"/> class.
        /// </summary>
        public ErnstTitleRequestAuditor()
            : base()
        {
        }

        /// <summary>
        /// Validates a property container for Government fees.
        /// </summary>
        /// <param name="property">The property container.</param>
        public void ValidateProperty(Property property)
        {
            var sectionName = "Government Fee Property Data";
            var subjectPropertyFieldPath = @"/newlos/Forms/Loan1003.aspx?pg=0";

            this.RecordErrorIfEmpty(
                sectionName,
                "Address",
                $"{subjectPropertyFieldPath}&highlightId=sSpAddr",
                property?.FullAddress,
                TitleFrameworkErrors.MissingAddress);

            this.RecordErrorIfEmpty(
                sectionName,
                "City",
                $"{subjectPropertyFieldPath}&highlightId=sSpCity",
                property?.City,
                TitleFrameworkErrors.MissingCity);

            this.RecordErrorIfEmpty(
                sectionName,
                "County",
                $"{subjectPropertyFieldPath}&highlightId=sSpCounty",
                property?.County,
                TitleFrameworkErrors.MissingCounty);

            this.RecordErrorIfEmpty(
                sectionName,
                "State",
                $"{subjectPropertyFieldPath}&highlightId=sSpState",
                property?.State,
                TitleFrameworkErrors.MissingState);

            var appraisedValueFieldPath = PrincipalFactory.CurrentPrincipal.BrokerDB.IsEnableBigLoanPage
                ? @"/newlos/BigLoanInfo.aspx?"
                : @"/newlos/LoanInfo.aspx?pg=0&";
            this.RecordErrorOnTest(
                property == null || (property.EstimatedValueSpecified && IsBlankMoney(property?.EstimatedValue)), // e.g. a refi will choose not to specify an estimated value
                sectionName,
                "Appraised Value",
                $"{appraisedValueFieldPath}highlightId=sApprVal",
                property?.EstimatedValue,
                TitleFrameworkErrors.MissingAppraisedValue);

            this.RecordErrorOnTest(
                IsBlankMoney(property?.MortgageAmount),
                sectionName,
                "Mortgage Amount",
                $"{subjectPropertyFieldPath}&highlightId=sFinalLAmt",
                property?.MortgageAmount,
                TitleFrameworkErrors.MissingLoanAmount);

            this.RecordErrorIfEmpty(
                sectionName,
                "Unpaid Principal Balance",
                $"{subjectPropertyFieldPath}&highlightId=sSpLien",
                property?.UnpaidPrincipalBalance,
                TitleFrameworkErrors.MissingLienAmount);
        }

        /// <summary>
        /// Validates a property container for title fees.
        /// </summary>
        /// <param name="property">The property container.</param>
        public void ValidateTitleRequestProperty(TitleRequestProperty property)
        {
            var sectionName = "Title Fee Property Data";
            var subjectPropertyFieldPath = @"/newlos/Forms/Loan1003.aspx?pg=0";

            this.RecordErrorIfEmpty(
                sectionName,
                "City",
                $"{subjectPropertyFieldPath}&highlightId=sSpCity",
                property?.City,
                TitleFrameworkErrors.MissingCity);

            this.RecordErrorIfEmpty(
                sectionName,
                "County",
                $"{subjectPropertyFieldPath}&highlightId=sSpCounty",
                property?.County,
                TitleFrameworkErrors.MissingCounty);

            this.RecordErrorIfEmpty(
                sectionName,
                "State",
                $"{subjectPropertyFieldPath}&highlightId=sSpState",
                property?.State,
                TitleFrameworkErrors.MissingState);

            this.RecordErrorIfEmpty(
                sectionName,
                "Postal Code",
                $"{subjectPropertyFieldPath}&highlightId=sSpZip",
                property?.Zip,
                TitleFrameworkErrors.MissingZip);
        }

        /// <summary>
        /// Validates a property container for inspection fees.
        /// </summary>
        /// <param name="property">The property container.</param>
        public void ValidateBlock6RequestProperty(Block6RequestProperty property)
        {
            var sectionName = "Inspection Fee Property Data";
            var subjectPropertyFieldPath = @"/newlos/Forms/Loan1003.aspx?pg=0";

            this.RecordErrorIfEmpty(
                sectionName,
                "Address",
                $"{subjectPropertyFieldPath}&highlightId=sSpAddr",
                property?.Address1,
                TitleFrameworkErrors.MissingAddress);

            this.RecordErrorIfEmpty(
                sectionName,
                "City",
                $"{subjectPropertyFieldPath}&highlightId=sSpCity",
                property?.City,
                TitleFrameworkErrors.MissingCity);

            this.RecordErrorIfEmpty(
                sectionName,
                "County",
                $"{subjectPropertyFieldPath}&highlightId=sSpCounty",
                property?.County,
                TitleFrameworkErrors.MissingCounty);

            this.RecordErrorIfEmpty(
                sectionName,
                "State",
                $"{subjectPropertyFieldPath}&highlightId=sSpState",
                property?.State,
                TitleFrameworkErrors.MissingState);

            this.RecordErrorIfEmpty(
                sectionName,
                "Postal Code",
                $"{subjectPropertyFieldPath}&highlightId=sSpZip",
                property?.Zip,
                TitleFrameworkErrors.MissingZip);

            this.RecordErrorIfEmpty(
                sectionName,
                "Year Built",
                $"{subjectPropertyFieldPath}&highlightId=sYrBuilt",
                property?.YearBuilt,
                TitleFrameworkErrors.MissingYearBuilt);
        }

        /// <summary>
        /// Validates a property container for property taxes.
        /// </summary>
        /// <param name="property">The property container.</param>
        public void ValidatePropertyTaxRequestProperty(PropertyTaxRequestProperty property)
        {
            var sectionName = "Property Tax Data";
            var subjectPropertyFieldPath = @"/newlos/Forms/Loan1003.aspx?pg=0";

            this.RecordErrorIfEmpty(
                sectionName,
                "Address",
                $"{subjectPropertyFieldPath}&highlightId=sSpAddr",
                property?.StreetAddress,
                TitleFrameworkErrors.MissingAddress);

            this.RecordErrorIfEmpty(
                sectionName,
                "City",
                $"{subjectPropertyFieldPath}&highlightId=sSpCity",
                property?.City,
                TitleFrameworkErrors.MissingCity);

            this.RecordErrorIfEmpty(
                sectionName,
                "State",
                $"{subjectPropertyFieldPath}&highlightId=sSpState",
                property?.State,
                TitleFrameworkErrors.MissingState);

            this.RecordErrorIfEmpty(
                sectionName,
                "Postal Code",
                $"{subjectPropertyFieldPath}&highlightId=sSpZip",
                property?.PostalCode,
                TitleFrameworkErrors.MissingZip);
        }

        /// <summary>
        /// Checks if a string is empty, whitespace, or $0, since all of these are non-amounts of money.
        /// </summary>
        /// <param name="str">The string to check.</param>
        /// <returns>True if the string is null/empty or a zero money amount.</returns>
        private static bool IsBlankMoney(Ernst.ErnstString str)
        {
            return ((string)str ?? string.Empty).Trim('$', '0', '.', ',', ' ') == string.Empty;
        }
    }
}
