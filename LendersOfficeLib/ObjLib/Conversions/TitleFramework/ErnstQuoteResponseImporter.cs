﻿namespace LendersOffice.Conversions.TitleFramework
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using Ernst;
    using Ernst.Response;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Defines a simple method for converting an Ernst <see cref="Response"/> object into a <see cref="TitleIntegrationQuote"/>.
    /// </summary>
    public static class ErnstQuoteResponseImporter
    {
        /// <summary>
        /// Extracts the necessary data from <paramref name="response"/> to construct a quote.
        /// </summary>
        /// <param name="response">The response from Ernst.</param>
        /// <returns>A quote of the data that will be imported.</returns>
        public static TitleIntegrationQuote GetQuoteData(Response response)
        {
            Dictionary<E_LegacyGfeFieldT, List<TitleIntegrationFee>> calculationFees = GetFeesFromCalculationElement(response?.ErnstResponse?.Display?.Calculation, response?.ErnstResponse?.Request?.State);

            Dictionary<E_LegacyGfeFieldT, List<TitleIntegrationFee>> titleResponseFees = GetFeesFromTitleResponseElement(response?.TitleResponse);

            ServiceProviderInfo titleProvider = null;
            var providerList = new List<ServiceProviderInfo>();
            if (response?.TitleResponse?.ProviderSpecified ?? false)
            {
                Provider p = response.TitleResponse.Provider;
                titleProvider = new ServiceProviderInfo(E_AgentRoleT.Title, p.Name, p.Address, p.City, p.State, p.Zip, p.Phone, p.Website, p.Disclaimer);
                providerList.Add(titleProvider);
            }

            List<TitleIntegrationFeeType> feesInQuote = new List<TitleIntegrationFeeType>(calculationFees.Count + titleResponseFees.Count);
            foreach (var feeTypeMapping in titleResponseFees)
            {
                feesInQuote.Add(new TitleIntegrationFeeType(feeTypeMapping.Key, titleProvider?.AgentRole, feeTypeMapping.Value));
            }

            foreach (var feeTypeMapping in calculationFees)
            {
                int indexOfDuplicate = feesInQuote.FindIndex(type => type.FeeType == feeTypeMapping.Key);
                if (indexOfDuplicate >= 0)
                {
                    TitleIntegrationFeeType oldFeeType = feesInQuote[indexOfDuplicate];
                    feesInQuote[indexOfDuplicate] = new TitleIntegrationFeeType(oldFeeType.FeeType, oldFeeType.ProviderRole, new List<TitleIntegrationFee>(oldFeeType.Fees.Concat(feeTypeMapping.Value)));
                }
                else
                {
                    feesInQuote.Add(new TitleIntegrationFeeType(feeTypeMapping.Key, null, feeTypeMapping.Value));
                }
            }

            List<string> messages = new List<string>();
            messages.AddIfNotNullOrEmpty(response?.TitleResponse?.CallForQuote?.Message);

            return new TitleIntegrationQuote(
                feesInQuote,
                providerList,
                messages,
                response?.TitleResponse?.CallForQuoteSettlement ?? response?.TitleResponse != null, // defaults to true if TitleResponse is present; false otherwise
                response?.TitleResponse?.CallForQuoteEndorsement ?? response?.TitleResponse != null,
                response?.TitleResponse?.LendersPolicy?.CallForQuote ?? response?.TitleResponse?.LendersPolicy != null,
                response?.TitleResponse?.OwnersPolicy?.CallForQuote ?? response?.TitleResponse?.OwnersPolicy != null);
        }

        /// <summary>
        /// Gets the fees from the <see cref="Calculation"/> element, which usually means recording fees and taxes.
        /// </summary>
        /// <param name="calculation">The element to inspect.</param>
        /// <param name="state">The state as passed back in the response.</param>
        /// <returns>The fees, grouped by fee type that were found in <paramref name="calculation"/>.</returns>
        public static Dictionary<E_LegacyGfeFieldT, List<TitleIntegrationFee>> GetFeesFromCalculationElement(Calculation calculation, ErnstString state)
        {
            var feesByType = new Dictionary<E_LegacyGfeFieldT, List<TitleIntegrationFee>>();

            // Recording Fees
            AddFeeToType(E_LegacyGfeFieldT.sRecMortgage, CreateBorrowerFee("Mortgage Recording Fee", calculation?.Mortgage?.Fee), feesByType);
            AddFeeToType(E_LegacyGfeFieldT.sRecDeed, CreateBorrowerFee("Deed Recording Fee", calculation?.Deed?.Fee), feesByType);
            AddFeeToType(E_LegacyGfeFieldT.sRecMortgage, CreateBorrowerFee("Assignment Recording Fee", calculation?.Assignment?.Fee), feesByType);
            AddFeeToType(E_LegacyGfeFieldT.sRecRelease, CreateBorrowerFee("Release Recording Fee", calculation?.Release?.Fee), feesByType);
            AddFeeToType(E_LegacyGfeFieldT.sRecMortgage, CreateBorrowerFee("Subordination Recording Fee", calculation?.Subordination?.Fee), feesByType);
            AddFeeToType(E_LegacyGfeFieldT.sRecMortgage, CreateBorrowerFee("POA Recording Fee", calculation?.POA?.Fee), feesByType);

            // We don't want to bother with this fee if it doesn't exist or if it has a value <= 0.
            var residentialMortgageFee = calculation?.Mortgage?.ResidentialMortgageFee;
            if (calculation?.Mortgage?.ResidentialMortgageFee != null)
            {
                var parsedState = UnitedStatesState.CreateWithValidation(state?.Value?.ToUpper());
                var description = "Residential Mortgage Fee";
                if (parsedState.HasValue)
                {
                    // We have the state, we'll prepend the state to the description;
                    description = $"{parsedState.Value.ToString()} Residential Mortgage Fee";
                }

                AddFeeToType(E_LegacyGfeFieldT.sStateRtc, CreateBorrowerFee(description, residentialMortgageFee), feesByType);
            }

            // Taxes
            foreach (CFPB2015CFPBTax tax in (calculation?.CFPB2015?.CFPBTaxList).CoalesceWithEmpty())
            {
                if (tax?.TaxType?.EnumValue == null)
                {
                    continue; // Ernst sends an empty element instead of an empty list sometimes
                }

                string taxTypeDescription = GetTaxTypeDescription(tax.TaxType);
                foreach (CFPB2015CFPBTaxTaxingEntity taxingEntity in (tax?.TaxingEntities?.TaxingEntityList).CoalesceWithEmpty())
                {
                    E_LegacyGfeFieldT feeTypeId;
                    string jurisdiction;
                    switch (taxingEntity.TaxingEntityJurisdiction?.EnumValue)
                    {
                        case CFPB2015CFPBTaxTaxingEntityTaxingEntityJurisdiction.State:
                            feeTypeId = E_LegacyGfeFieldT.sStateRtc;
                            jurisdiction = "State";
                            break;
                        case CFPB2015CFPBTaxTaxingEntityTaxingEntityJurisdiction.City:
                            feeTypeId = E_LegacyGfeFieldT.sCountyRtc;
                            jurisdiction = "City";
                            break;
                        case CFPB2015CFPBTaxTaxingEntityTaxingEntityJurisdiction.County:
                            feeTypeId = E_LegacyGfeFieldT.sCountyRtc;
                            jurisdiction = "County";
                            break;
                        default:
                            throw new UnhandledEnumException(taxingEntity.TaxingEntityJurisdiction?.EnumValue);
                    }

                    TitleIntegrationFee fee = CreateTitleFee(jurisdiction + " " + taxTypeDescription, taxingEntity.BuyerSplit, taxingEntity.SellerSplit, taxingEntity.LenderSplit);
                    AddFeeToType(feeTypeId, fee, feesByType);
                }
            }

            return feesByType;
        }

        /// <summary>
        /// Gets the fees from the <see cref="TitleResponse"/> element, which usually means escrow fees as well as Lender's and Owner's title insurance.
        /// </summary>
        /// <param name="titleResponse">The element to inspect.</param>
        /// <returns>The fees, grouped by fee type that were found in <paramref name="titleResponse"/>.</returns>
        public static Dictionary<E_LegacyGfeFieldT, List<TitleIntegrationFee>> GetFeesFromTitleResponseElement(TitleResponse titleResponse)
        {
            var feesByType = new Dictionary<E_LegacyGfeFieldT, List<TitleIntegrationFee>>();

            // Endorsement Fees
            foreach (Fee fee in (titleResponse?.Endorsements?.EndorsementList).CoalesceWithEmpty())
            {
                AddFeeToType(
                    E_LegacyGfeFieldT.sTitleInsF,
                    new TitleIntegrationFee(fee.Name, fee.Amount, null, null, fee.CallForQuote),
                    feesByType);
            }

            // Settlement Fees 
            foreach (Fee fee in (titleResponse?.ItemizedSettlementFees?.SettlementFeeList).CoalesceWithEmpty())
            {
                AddFeeToType(
                    E_LegacyGfeFieldT.sEscrowF,
                    new TitleIntegrationFee(fee.Name, fee.Amount, null, null, fee.CallForQuote),
                    feesByType);
            }

            // Lender's Title Policy
            foreach (Fee charge in (titleResponse?.LendersPolicy?.FullPolicy?.ItemizedCharges?.ChargeList).CoalesceWithEmpty())
            {
                AddFeeToType(
                    E_LegacyGfeFieldT.sTitleInsF,
                    new TitleIntegrationFee(charge.Name, charge.Amount, null, null, charge.CallForQuote),
                    feesByType);
            }

            // Owner's Title Policy
            foreach (Fee charge in (titleResponse?.OwnersPolicy?.SimoPolicy?.ItemizedCharges?.ChargeList).CoalesceWithEmpty())
            {
                AddFeeToType(
                    E_LegacyGfeFieldT.sOwnerTitleInsF,
                    new TitleIntegrationFee(charge.Name, charge.Amount, null, null, charge.CallForQuote),
                    feesByType);
            }

            return feesByType;
        }

        /// <summary>
        /// Adds the specified fee to <paramref name="feesByType"/>.
        /// </summary>
        /// <param name="feeTypeId">The type of the fee, which serves as the key to <paramref name="feesByType"/>.</param>
        /// <param name="fee">The fee to add.</param>
        /// <param name="feesByType">The collection to add the specified fee to.</param>
        private static void AddFeeToType(E_LegacyGfeFieldT feeTypeId, TitleIntegrationFee fee, Dictionary<E_LegacyGfeFieldT, List<TitleIntegrationFee>> feesByType)
        {
            if (fee == null)
            {
                return;
            }

            List<TitleIntegrationFee> fees;
            if (!feesByType.TryGetValue(feeTypeId, out fees))
            {
                fees = new List<TitleIntegrationFee>();
                feesByType.Add(feeTypeId, fees);
            }

            fees.Add(fee);
        }

        /// <summary>
        /// Creates a simple title fee with only a borrower amount, or nothing if the fee is not valid.
        /// </summary>
        /// <param name="description">The description of the fee.</param>
        /// <param name="borrowerResponsibleAmount">The string amount of the fee.</param>
        /// <returns>A fee represented by the amount, or null if <paramref name="borrowerResponsibleAmount"/> is not a valid amount.</returns>
        private static TitleIntegrationFee CreateBorrowerFee(string description, ErnstString borrowerResponsibleAmount)
        {
            return CreateTitleFee(description, borrowerResponsibleAmount, null, null);
        }

        /// <summary>
        /// Create a title fee from a set of strings, or nothing if there is no valid fee amount.
        /// </summary>
        /// <param name="description">The description of the fee.</param>
        /// <param name="borrowerResponsibleAmount">The string amount of the borrower's portion of the fee.</param>
        /// <param name="sellerResponsibleAmount">The string amount of the seller's portion of the fee.</param>
        /// <param name="lenderResponsibleAmount">The string amount of the lender's portion of the fee.</param>
        /// <returns>A fee represented by the amount, or null if the fee does not have a valid amount.</returns>
        private static TitleIntegrationFee CreateTitleFee(string description, ErnstString borrowerResponsibleAmount, ErnstString sellerResponsibleAmount, ErnstString lenderResponsibleAmount)
        {
            decimal? borrowerAmount = borrowerResponsibleAmount?.Value.ToNullable<decimal>(decimal.TryParse);
            decimal? sellerAmount = sellerResponsibleAmount?.Value.ToNullable<decimal>(decimal.TryParse);
            decimal? lenderAmount = lenderResponsibleAmount?.Value.ToNullable<decimal>(decimal.TryParse);
            if (borrowerAmount.HasValue || sellerAmount.HasValue || lenderAmount.HasValue)
            {
                return new TitleIntegrationFee(description, borrowerAmount, sellerAmount, lenderAmount, null);
            }

            return null;
        }

        /// <summary>
        /// Gets the user-friendly description of the value of the <seealso cref="CFPB2015CFPBTaxTaxType"/> enum.
        /// </summary>
        /// <param name="taxType">The enum to evaluate.</param>
        /// <returns>A user-friendly description of the value.</returns>
        private static string GetTaxTypeDescription(ErnstEnum<CFPB2015CFPBTaxTaxType> taxType)
        {
            switch (taxType?.EnumValue)
            {
                case CFPB2015CFPBTaxTaxType.MortgageTax: return "Mortgage Tax";
                case CFPB2015CFPBTaxTaxType.DeedTax: return "Deed Tax";
                case CFPB2015CFPBTaxTaxType.MortgageIntangibleTax: return "Mortgage Intangible Tax";
                case CFPB2015CFPBTaxTaxType.DeedIntangibleTax: return "Deed Intangible Tax";
                case CFPB2015CFPBTaxTaxType.MortgageRecordationTax: return "Mortgage Recordation Tax";
                case CFPB2015CFPBTaxTaxType.DeedRecordationTax: return "Deed Recordation Tax";
                case CFPB2015CFPBTaxTaxType.MortgageConservationFund: return "Mortgage Conservation Fund";
                case CFPB2015CFPBTaxTaxType.DeedConservationFund: return "Deed Conservation Fund";
                case CFPB2015CFPBTaxTaxType.MansionTax: return "Mansion Tax";
                case CFPB2015CFPBTaxTaxType.GrantorTax: return "Grantor Tax";
                case CFPB2015CFPBTaxTaxType.GranteeTax: return "Grantee Tax";
                case CFPB2015CFPBTaxTaxType.LandBankTax: return "Land Bank Tax";
                case CFPB2015CFPBTaxTaxType.SalesDisclosureFormFee: return "Sales Disclosure Form Fee";
                case CFPB2015CFPBTaxTaxType.AuditorTransferFee: return "Auditor Transfer Fee";
                case null:
                    throw new GenericUserErrorMessageException("TaxType was null valued.");
                default:
                    throw new UnhandledEnumException(taxType.EnumValue);
            }
        }
    }
}
