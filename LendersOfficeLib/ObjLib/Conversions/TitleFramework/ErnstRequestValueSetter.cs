﻿namespace LendersOffice.Conversions.TitleFramework
{
    using System;
    using System.Linq;
    using global::Ernst;
    using LendersOffice.Common;
    using LendersOffice.Integration.TitleFramework;
    using LqbGrammar.DataTypes;
    using LqbGrammar.DataTypes.PathDispatch;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Represents a setter for a single value and path on an Ernst request payload.
    /// </summary>
    public partial class ErnstRequestValueSetter
    {
        /// <summary>
        /// The value being set.
        /// </summary>
        private readonly string valueToSet;

        /// <summary>
        /// The entire path to the value that is being set.
        /// </summary>
        private readonly SimpleXPath fullPath;

        /// <summary>
        /// Initializes a new instance of the <see cref="ErnstRequestValueSetter"/> class.
        /// </summary>
        /// <param name="path">The path to set.</param>
        /// <param name="value">The value to set at the specified path.</param>
        public ErnstRequestValueSetter(SimpleXPath path, ErnstConfigurationValue value)
        {
            this.fullPath = path;
            this.valueToSet = value.Value;
        }

        /// <summary>
        /// Gets the exception thrown when we are unable to navigate to the full path.
        /// </summary>
        /// <value>The exception thrown when we are unable to navigate to the full path.</value>
        private Exception UnrecognizedPath
        {
            get { return new ValidationException(ValidationErrorMessage.Create("\"" + this.fullPath + "\" is not a recognized path").ForceValue()); }
        }

        /// <summary>
        /// Gets the exception thrown when we are unable to set the specified value at the path.
        /// </summary>
        /// <value>The exception thrown when we are unable to set the specified value at the path.</value>
        private Exception UnableToSetValue
        {
            get { return new ValidationException(ValidationErrorMessage.Create("Unable to set value at \"" + this.fullPath + "\" to \"" + this.valueToSet + "\"").ForceValue()); }
        }

        /// <summary>
        /// Sets the value and path on <paramref name="request"/>.
        /// </summary>
        /// <param name="request">The request to set the value.</param>
        public void SetValue(Ernst.Request.Request request)
        {
            if (this.fullPath.DataPath.Head.ToString() != "Request")
            {
                throw this.UnrecognizedPath;
            }

            this.SetValue(request, this.fullPath.DataPath.Tail);
        }

        /// <summary>
        /// Validates that <paramref name="path"/> is non-empty, throwing if does not have entries.
        /// </summary>
        /// <param name="path">The path to validate.</param>
        private void EnsureNonEmptyPath(DataPath path)
        {
            if (path.Length == 0)
            {
                throw this.UnrecognizedPath;
            }
        }

        /// <summary>
        /// Validates that <paramref name="path"/> is empty, throwing if it has entries.
        /// </summary>
        /// <param name="path">The path to validate.</param>
        private void EnsureEmptyPath(DataPath path)
        {
            if (path.Length != 0)
            {
                throw this.UnrecognizedPath;
            }
        }

        /// <summary>
        /// Sets the value of <paramref name="booleanNode"/>.
        /// </summary>
        /// <param name="booleanNode">The node to set the value on.</param>
        /// <param name="path">The remaining path to evaluate.</param>
        private void SetValue(ErnstBoolean booleanNode, DataPath path)
        {
            this.EnsureEmptyPath(path);

            bool typedValueToSet;
            if (this.valueToSet == ErnstBoolean.TrueValue)
            {
                typedValueToSet = true;
            }
            else if (this.valueToSet == ErnstBoolean.FalseValue)
            {
                typedValueToSet = false;
            }
            else
            {
                throw this.UnableToSetValue;
            }

            booleanNode.BooleanValue = typedValueToSet;
        }

        /// <summary>
        /// Sets the value of <paramref name="decimalNode"/>.
        /// </summary>
        /// <param name="decimalNode">The node to set the value on.</param>
        /// <param name="path">The remaining path to evaluate.</param>
        private void SetValue(ErnstDecimal decimalNode, DataPath path)
        {
            this.EnsureEmptyPath(path);
            decimal? typedValueToSet = this.valueToSet.ToNullable<decimal>(decimal.TryParse);
            if (!typedValueToSet.HasValue)
            {
                throw this.UnableToSetValue;
            }

            decimalNode.DecimalValue = typedValueToSet.Value;
        }

        /// <summary>
        /// Sets the value of <paramref name="enumNode"/>.
        /// </summary>
        /// <param name="enumNode">The node to set the value on.</param>
        /// <param name="path">The remaining path to evaluate.</param>
        /// <typeparam name="TEnum">The type of enum being set.</typeparam>
        private void SetValue<TEnum>(ErnstEnum<TEnum> enumNode, DataPath path) where TEnum : struct
        {
            this.EnsureEmptyPath(path);

            TEnum? typedValueToSet = null;
            foreach (System.Reflection.FieldInfo enumValue in typeof(TEnum).GetFields(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static))
            {
                var xmlEnumAttr = (System.Xml.Serialization.XmlEnumAttribute)enumValue.GetCustomAttributes(typeof(System.Xml.Serialization.XmlEnumAttribute), false).SingleOrDefault();
                if (string.Equals(this.valueToSet, xmlEnumAttr?.Name ?? enumValue.Name))
                {
                    typedValueToSet = (TEnum)Enum.Parse(typeof(TEnum), enumValue.Name);
                    break;
                }
            }

            if (!typedValueToSet.HasValue)
            {
                throw this.UnableToSetValue;
            }

            enumNode.EnumValue = typedValueToSet.Value;
        }

        /// <summary>
        /// Sets the value of <paramref name="integerNode"/>.
        /// </summary>
        /// <param name="integerNode">The node to set the value on.</param>
        /// <param name="path">The remaining path to evaluate.</param>
        private void SetValue(ErnstInteger integerNode, DataPath path)
        {
            this.EnsureEmptyPath(path);
            int? typedValueToSet = this.valueToSet.ToNullable<int>(int.TryParse);
            if (!typedValueToSet.HasValue)
            {
                throw this.UnableToSetValue;
            }

            integerNode.IntegerValue = typedValueToSet.Value;
        }

        /// <summary>
        /// Sets the value of <paramref name="stringNode"/>.
        /// </summary>
        /// <param name="stringNode">The node to set the value on.</param>
        /// <param name="path">The remaining path to evaluate.</param>
        private void SetValue(ErnstString stringNode, DataPath path)
        {
            this.EnsureEmptyPath(path);
            stringNode.Value = this.valueToSet;
        }
    }
}
