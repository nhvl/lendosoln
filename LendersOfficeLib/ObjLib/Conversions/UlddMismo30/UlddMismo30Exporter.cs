﻿//using System;
//using DataAccess;
//using LendersOffice.Reminders;
//using LendersOffice.Security;
//using LendersOffice.Admin;
//using System.IO;
//using System.Xml;
//using System.Text;
//using LendersOfficeApp.los.admin;
//using LendersOffice.ObjLib.Task;
//using System.Collections.Generic;
//using Mismo.ULDD;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Reflection;
//using System.Collections;
//using LendersOffice.ObjLib.DocMagicLib;
//using System.Text.RegularExpressions;

//namespace LendersOffice.Conversions.UlddMismo30
//{
//    public class UlddMismo30Exporter 
//    {
//        private CPageData m_dataLoan = null;
//        private CPageData m_dataLoanLinked = null;
//        private CAppData m_dataApp = null;
//        public UlddMismo30Exporter(Guid sLId, Guid aAppId)
//        {
//            m_dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(UlddMismo30Exporter));
//            m_dataLoan.InitLoad();
//            if (m_dataLoan.sLinkedLoanInfo.IsLoanLinked)
//            {
//                m_dataLoanLinked = CPageData.CreateUsingSmartDependency(m_dataLoan.sLinkedLoanInfo.LinkedLId, typeof(UlddMismo30Exporter));
//                m_dataLoanLinked.InitLoad();
//                m_dataLoanLinked.SetFormatTarget(FormatTarget.MismoClosing);
//            }

//            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
//            m_dataApp = m_dataLoan.GetAppData(aAppId);
//        }

//        public String verifyEmptyField()
//        {
//            String str = "";           
//            if (m_dataLoan.sLenNum == "")                
//                str += "sLenNum,";

//            if (m_dataLoan.sDocumentNoteD.ToString() == "")                
//                str += "sDocumentNoteD,";

//            if (m_dataLoan.sSchedDueD1.ToString() == "")
//                str += "sSchedDueD1,";

//            if (m_dataLoan.sHouseVal.Equals(0))                
//                str += "sHouseVal,";

//            if (E_sGseDeliveryTargetT.GinnieMae == m_dataLoan.sGseDeliveryTargetT)
//                str += "sGseDeliveryTargetT_GinnieMae,";

//            if (E_sGseDeliveryTargetT.Other_None == m_dataLoan.sGseDeliveryTargetT)
//                str += "sGseDeliveryTargetT_Other_None,";

//            if (m_dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
//            {
//                if (!Valid10081009Selections(E_10081009.Line1008))
//                    str += "Error_1008,";

//                if (!Valid10081009Selections(E_10081009.Line1009))
//                    str += "Error_1009,";
//            }

//            if (str.Length == 0)
//                return str;
//            else
//                return str.Substring(0, str.Length-1);
//        }

//        /// <summary>
//        /// Returns whether the selections for the given line are valid.
//        /// </summary>
//        /// <param name="line">The selection to be validated.</param>
//        /// <returns>
//        /// Returns true if selections are valid for given line (1008/1009). It is valid if 1008/1009 are
//        /// used exactly once when the dollar amount is > 0. Will return false if 1008/1009 is used for
//        /// more than one field regardless of dollar amount. OPM 90127
//        /// </returns>
//        public bool Valid10081009Selections(E_10081009 line)
//        {
//            bool selectedInFirstTaxDDL = false;
//            bool selectedInSecondTaxDDL = false;
//            bool selectedInWindIns = false;
//            bool selectedInCondoIns = false;

//            if (line == E_10081009.Line1008)
//            {
//                selectedInFirstTaxDDL = m_dataLoan.sTaxTable1008DescT == E_TableSettlementDescT.Use1008DescT;
//                selectedInSecondTaxDDL = m_dataLoan.sTaxTable1009DescT == E_TableSettlementDescT.Use1008DescT;
//                selectedInWindIns = m_dataLoan.sWindInsSettlementChargeT == E_sInsSettlementChargeT.Line1008;
//                selectedInCondoIns = m_dataLoan.sCondoHO6InsSettlementChargeT == E_sInsSettlementChargeT.Line1008;
//            }
//            else
//            {
//                selectedInFirstTaxDDL = m_dataLoan.sTaxTable1008DescT == E_TableSettlementDescT.Use1009DescT;
//                selectedInSecondTaxDDL = m_dataLoan.sTaxTable1009DescT == E_TableSettlementDescT.Use1009DescT;
//                selectedInWindIns = m_dataLoan.sWindInsSettlementChargeT == E_sInsSettlementChargeT.Line1009;
//                selectedInCondoIns = m_dataLoan.sCondoHO6InsSettlementChargeT == E_sInsSettlementChargeT.Line1009;
//            }

//            bool selectedInAtLeastOneIns = selectedInWindIns || selectedInCondoIns;
//            bool selectedInTwoTaxes = selectedInFirstTaxDDL && selectedInSecondTaxDDL;
//            bool selectedInFirstTaxAndIns = selectedInFirstTaxDDL && selectedInAtLeastOneIns;
//            bool selectedInSecondTaxAndIns = selectedInSecondTaxDDL && selectedInAtLeastOneIns;
//            bool selectedInBothIns = selectedInWindIns && selectedInCondoIns;

//            if (selectedInTwoTaxes || selectedInFirstTaxAndIns || selectedInSecondTaxAndIns || selectedInBothIns)
//                return false;

//            // If the dollar amount is > $0, then it must be selected in exactly ONE of the DDLs
//            // The above made sure it is selected in only one of the valid locations, here we just 
//            // need to make sure it is selected somewhere.
//            bool selectedAtLeastOnce = selectedInFirstTaxDDL || selectedInSecondTaxDDL ||
//                                       selectedInWindIns || selectedInCondoIns;

//            if (line == E_10081009.Line1008)
//            {
//                if ((m_dataLoan.s1006ProHExp > 0) && !selectedAtLeastOnce)
//                    return false;
//            }
//            else
//            {
//                if ((m_dataLoan.s1007ProHExp > 0) && !selectedAtLeastOnce)
//                    return false;
//            }

//            // If it is selected in one of the Tax tables, then the type for that tax table must not be blank.
//            if (selectedInFirstTaxDDL && m_dataLoan.sTaxTable1008T == E_TaxTableTaxT.LeaveBlank)
//                return false;

//            if (selectedInSecondTaxDDL && m_dataLoan.sTaxTable1009T == E_TaxTableTaxT.LeaveBlank)
//                return false;

//            return true;
//        }

//        public string Export()
//        {
//            byte[] bytes = null;

//            XEMessage message = CreateMessage();
//            int contentLength = 0;
//            using (MemoryStream stream = new MemoryStream(5000))
//            {
//                XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);
//                writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");

//                message.GenerateXml(writer);

//                writer.Flush();
//                contentLength = (int)stream.Position;

//                bytes = stream.GetBuffer();
//            }
//            if (null != bytes)
//            {
//                return System.Text.Encoding.ASCII.GetString(bytes, 0, contentLength);
//            }
//            else
//            {
//                return null;
//            }
//        }

//        private XEMessage CreateMessage()
//        {
//            XEMessage message = new XEMessage();

//            message.SetAboutVersions(CreateAboutVersions());
//            message.SetDealSets(CreateDealSets());

//            return message;
//        }

//        private XEDealSets CreateDealSets()
//        {
//            XEDealSets dealSets = new XEDealSets();

//            dealSets.SetDealSet(CreateDealSet());
//            dealSets.SetParties(CreateParties(false));

//            return dealSets;
//        }

//        private XEDealSet CreateDealSet()
//        {
//            XEDealSet dealSet = new XEDealSet();

//            dealSet.SetDeals(CreateDeals());

//            return dealSet;
//        }

//        private XEDeals CreateDeals()
//        {
//            XEDeals deals = new XEDeals();

//            deals.SetDeal(CreateDeal());

//            return deals;
//        }

//        private XEDeal CreateDeal()
//        {
//            XEDeal deal = new XEDeal();

//            deal.SetCollaterals(CreateCollaterals());
//            deal.SetLoans(CreateLoans());
//            deal.SetParties(CreateParties(true));

//            return deal;
//        }

//        private XEParties CreateParties(bool fromDeal)
//        {
//            XEParties parties = new XEParties();

//            if (fromDeal)
//            {
//                parties.AddParty(CreatePartyAppraiser());
//                parties.AddParty(CreatePartyAppraiserSupervisor());
                
//                int appPos = 0;
//                CAppData app;
//                List<AppRelationshipData> borrowerApps = new List<AppRelationshipData>();

//                do
//                {
//                    app = m_dataLoan.GetAppData(appPos);
//                    AppRelationshipData appData = new AppRelationshipData();
//                    appData.AddressLineText = app.aBAddrMail;
//                    appData.AddressType = "Mailing";
//                    appData.CityName = app.aBCityMail;
//                    appData.CountryCode = "US";
//                    appData.PostalCode = app.aBZipMail;
//                    appData.StateCode = app.aBStateMail;
//                    appData.FirstName = app.aBFirstNm;
//                    appData.LastName = app.aBLastNm;
//                    appData.SuffixName = app.aBSuffix;
//                    appData.BorrowerAgeAtApplicationYearsCount = app.aBAge_rep;
//                    appData.MiddleName = app.aBMidNm;
//                    appData.BorrowerBirthDate = app.aBDob_rep;
//                    appData.BorrowerClassificationType = appPos == 0 ? "Primary" : "Secondary";
//                    appData.BorrowerMailToAddressSameasPropertyIndicator = app.aBAddrMailUsePresentAddr;
//                    appData.BorrowerQualifyingIncomeAmount = app.aBTotI_rep;
//                    appData.HomeOwnershipCounselingSource = app.aBHomeOwnershipCounselingSourceT;
//                    appData.HomeOwnershipCounselingFormat = app.aBHomeOwnershipCounselingFormatT;
//                    appData.ExperianScore = app.aBExperianScore_rep;
//                    appData.TransUnionScore = app.aBTransUnionScore_rep;
//                    appData.EquifaxScore = app.aBEquifaxScore_rep;
//                    appData.DecPastOwnership = app.aBDecPastOwnership;
//                    appData.DecResidency = app.aBDecResidency;
//                    appData.DecCitizen = app.aBDecCitizen;
//                    appData.Gender = app.aBGender;
//                    appData.HMDAEthnicityType = app.aBHispanicT;
//                    appData.aBIsAmericanIndian = app.aBIsAmericanIndian;
//                    appData.aBIsAsian = app.aBIsAsian;
//                    appData.aBIsBlack = app.aBIsBlack;
//                    appData.aBIsPacificIslander = app.aBIsPacificIslander;
//                    appData.aBIsWhite = app.aBIsWhite;
//                    appData.Ssn = app.aBSsn;
//                    borrowerApps.Add(appData);

//                    if (!app.aBHasSpouse)
//                    {
//                        appPos++;
//                    }
//                    else
//                    {
//                        break;
//                    }
//                } while (appPos < 2 && appPos < m_dataLoan.nApps);

//                if (appPos == 0 && app.aBHasSpouse)
//                {
//                    AppRelationshipData appData = new AppRelationshipData();
//                    appData.AddressLineText = app.aCAddrMail;
//                    appData.AddressType = "Mailing";
//                    appData.CityName = app.aCCityMail;
//                    appData.CountryCode = "US";
//                    appData.PostalCode = app.aCZipMail;
//                    appData.StateCode = app.aCStateMail;
//                    appData.FirstName = app.aCFirstNm;
//                    appData.LastName = app.aCLastNm;
//                    appData.SuffixName = app.aCSuffix;
//                    appData.BorrowerAgeAtApplicationYearsCount = app.aCAge_rep;
//                    appData.MiddleName = app.aCMidNm;
//                    appData.BorrowerBirthDate = app.aCDob_rep;
//                    appData.BorrowerClassificationType = "Secondary";
//                    appData.BorrowerMailToAddressSameasPropertyIndicator = app.aCAddrMailUsePresentAddr;
//                    appData.BorrowerQualifyingIncomeAmount = app.aCTotI_rep;
//                    appData.HomeOwnershipCounselingSource = app.aCHomeOwnershipCounselingSourceT;
//                    appData.HomeOwnershipCounselingFormat = app.aCHomeOwnershipCounselingFormatT;
//                    appData.ExperianScore = app.aCExperianScore_rep;
//                    appData.TransUnionScore = app.aCTransUnionScore_rep;
//                    appData.EquifaxScore = app.aCEquifaxScore_rep;
//                    appData.DecPastOwnership = app.aCDecPastOwnership;
//                    appData.DecResidency = app.aCDecResidency;
//                    appData.DecCitizen = app.aCDecCitizen;
//                    appData.Gender = app.aCGender;
//                    appData.HMDAEthnicityType = app.aCHispanicT;
//                    appData.aBIsAmericanIndian = app.aCIsAmericanIndian;
//                    appData.aBIsAsian = app.aCIsAsian;
//                    appData.aBIsBlack = app.aCIsBlack;
//                    appData.aBIsPacificIslander = app.aCIsPacificIslander;
//                    appData.aBIsWhite = app.aCIsWhite;
//                    appData.Ssn = app.aCSsn;
//                    borrowerApps.Add(appData);
//                }

//                parties.AddParty(CreatePartyBorrower(borrowerApps[0]));
//                if (borrowerApps.Count > 1)
//                    parties.AddParty(CreatePartyBorrower(borrowerApps[1]));

//                parties.AddParty(CreatePartyDocumentCustodian());
//                parties.AddParty(CreatePartyLoanOriginationCompany());
//                parties.AddParty(CreatePartyLoanOriginator());
//                parties.AddParty(CreatePartyLoanSeller());
//                parties.AddParty(CreatePartyNotePayTo());
//                parties.AddParty(CreatePartyPayee());
//                parties.AddParty(CreatePartyServicer());
//            }
//            else
//            {
//                parties.AddParty(CreatePartyLoanDeliveryFilePreparer());
//            }

//            return parties;
//        }

//        private XEParty CreatePartyNotePayTo()
//        {
//            XELegalEntityDetail legalEntityDetail = new XELegalEntityDetail();
//            DocMagicAlternateLender altLender = new DocMagicAlternateLender(m_dataLoan.sDocMagicAlternateLenderId);
//            legalEntityDetail.FullName = altLender.MakePaymentsToName;

//            XELegalEntity legalEntity = new XELegalEntity();
//            legalEntity.SetLegalEntityDetail(legalEntityDetail);

//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "NotePayTo";

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);
//            party.SetLegalEntity(legalEntity);

//            return party;
//        }

//        private XEParty CreatePartyPayee()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "Payee";

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);

//            XEPartyRoleIdentifier partyRoleIdentifier = new XEPartyRoleIdentifier();
//            partyRoleIdentifier.PartyRoleIdentifier = m_dataLoan.sGseDeliveryPayeeId;

//            XEPartyRoleIdentifiers partyRoleIdentifiers = new XEPartyRoleIdentifiers();
//            partyRoleIdentifiers.SetPartyRoleIdentifier(partyRoleIdentifier);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            roles.SetPartyRoleIdentifiers(partyRoleIdentifiers);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyServicer()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "Servicer";

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);

//            XEPartyRoleIdentifier partyRoleIdentifier = new XEPartyRoleIdentifier();
//            partyRoleIdentifier.PartyRoleIdentifier = m_dataLoan.sGseDeliveryServicerId;

//            XEPartyRoleIdentifiers partyRoleIdentifiers = new XEPartyRoleIdentifiers();
//            partyRoleIdentifiers.SetPartyRoleIdentifier(partyRoleIdentifier);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            roles.SetPartyRoleIdentifiers(partyRoleIdentifiers);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyLoanDeliveryFilePreparer()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "LoanDeliveryFilePreparer";

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);

//            XEPartyRoleIdentifier partyRoleIdentifier = new XEPartyRoleIdentifier();
//            partyRoleIdentifier.PartyRoleIdentifier = "VMLILQ01D";

//            XEPartyRoleIdentifiers partyRoleIdentifiers = new XEPartyRoleIdentifiers();
//            partyRoleIdentifiers.SetPartyRoleIdentifier(partyRoleIdentifier);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            roles.SetPartyRoleIdentifiers(partyRoleIdentifiers);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyLoanSeller()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "LoanSeller";

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);

//            XEPartyRoleIdentifier partyRoleIdentifier = new XEPartyRoleIdentifier();
//            partyRoleIdentifier.PartyRoleIdentifier = m_dataLoan.sLenNum;

//            XEPartyRoleIdentifiers partyRoleIdentifiers = new XEPartyRoleIdentifiers();
//            partyRoleIdentifiers.SetPartyRoleIdentifier(partyRoleIdentifier);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            roles.SetPartyRoleIdentifiers(partyRoleIdentifiers);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyLoanOriginator()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "LoanOriginator";

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);

//            XELoanOriginator loanOriginator = new XELoanOriginator();
//            if (m_dataLoan.sBranchChannelT != E_BranchChannelT.Blank)
//            {
//                loanOriginator.LoanOriginatorType = ToMismo(m_dataLoan.sBranchChannelT);
//                role.SetLoanOriginator(loanOriginator);
//            }

//            XEPartyRoleIdentifier partyRoleIdentifier = new XEPartyRoleIdentifier();
//            partyRoleIdentifier.PartyRoleIdentifier = m_dataLoan.sApp1003InterviewerLoanOriginatorIdentifier;

//            XEPartyRoleIdentifiers partyRoleIdentifiers = new XEPartyRoleIdentifiers();
//            partyRoleIdentifiers.SetPartyRoleIdentifier(partyRoleIdentifier);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            roles.SetPartyRoleIdentifiers(partyRoleIdentifiers);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyLoanOriginationCompany()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "LoanOriginationCompany";

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);

//            XEPartyRoleIdentifier partyRoleIdentifier = new XEPartyRoleIdentifier();
//            partyRoleIdentifier.PartyRoleIdentifier = m_dataLoan.sApp1003InterviewerLoanOriginationCompanyIdentifier;

//            XEPartyRoleIdentifiers partyRoleIdentifiers = new XEPartyRoleIdentifiers();
//            partyRoleIdentifiers.SetPartyRoleIdentifier(partyRoleIdentifier);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            roles.SetPartyRoleIdentifiers(partyRoleIdentifiers);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyDocumentCustodian()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "DocumentCustodian";

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);

//            XEPartyRoleIdentifier partyRoleIdentifier = new XEPartyRoleIdentifier();
//            partyRoleIdentifier.PartyRoleIdentifier = m_dataLoan.sGseDeliveryDocumentCustodianId;

//            XEPartyRoleIdentifiers partyRoleIdentifiers = new XEPartyRoleIdentifiers();
//            partyRoleIdentifiers.SetPartyRoleIdentifier(partyRoleIdentifier);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            roles.SetPartyRoleIdentifiers(partyRoleIdentifiers);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyAppraiserSupervisor()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "AppraiserSupervisor";

//            XEAppraiserSupervisor appraiserSupervisor = new XEAppraiserSupervisor();
//            XEAppraiserLicense appraiserLicense = new XEAppraiserLicense();
//            List<CAgentFields> agents = m_dataLoan.GetAgentsOfRole(E_AgentRoleT.Other, E_ReturnOptionIfNotExist.ReturnEmptyObject);
//            foreach (CAgentFields agent in agents)
//            {
//                if (agent.AgentRoleT == E_AgentRoleT.Other && agent.OtherAgentRoleTDesc == "AppraiserSupervisor")
//                {
//                    appraiserLicense.AppraiserLicenseIdentifier = agent.LicenseNumOfAgent;
//                    break;
//                }
//            }
//            appraiserSupervisor.SetAppraiserLicense(appraiserLicense);

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);
//            role.SetAppraiserSupervisor(appraiserSupervisor);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyAppraiser()
//        {
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "Appraiser";

//            XEAppraiser appraiser = new XEAppraiser();
//            XEAppraiserLicense appraiserLicense = new XEAppraiserLicense();
//            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
//            if (agent.AgentRoleT == E_AgentRoleT.Appraiser)
//            {
//                appraiserLicense.AppraiserLicenseIdentifier = agent.LicenseNumOfAgent;
//            }
//            appraiser.SetAppraiserLicense(appraiserLicense);

//            XERole role = new XERole();
//            role.SetRoleDetail(roleDetail);
//            role.SetAppraiser(appraiser);

//            XERoles roles = new XERoles();
//            roles.AddRole(role);

//            XEParty party = new XEParty();
//            party.SetRoles(roles);

//            return party;
//        }

//        private XEParty CreatePartyBorrower11(List<AppRelationshipData> data)
//        {
//            XEParty party = new XEParty();

//            XEAddresses addresses = new XEAddresses();
//            addresses.SetAddress(CreateAddress(data[0]));
//            party.SetAddresses(addresses);

//            party.AddIndividual(CreateIndividual(data[0]));
            
//            XERole role = new XERole();
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "Borrower";
//            role.SetRoleDetail(roleDetail);
//            role.AddBorrower(CreateBorrower(data[0]));
//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            party.SetRoles(roles);

//            if (data.Count > 1)
//            {
//                party.AddIndividual(CreateIndividual(data[1]));
                
//                role = new XERole();
//                role.SetRoleDetail(roleDetail);
//                role.AddBorrower(CreateBorrower(data[1]));
//                roles = new XERoles();
//                roles.AddRole(role);
//                party.SetRoles(roles);
//            }

//            party.SetTaxpayerIdentifiers(CreateTaxpayerIdentifiers11(data));

//            return party;
//        }

//        private XEParty CreatePartyBorrower(AppRelationshipData data)
//        {
//            XEParty party = new XEParty();

//            XEAddresses addresses = new XEAddresses();
//            addresses.SetAddress(CreateAddress(data));
//            party.SetAddresses(addresses);

//            party.AddIndividual(CreateIndividual(data));

//            XERole role = new XERole();
//            XERoleDetail roleDetail = new XERoleDetail();
//            roleDetail.PartyRoleType = "Borrower";
//            role.SetRoleDetail(roleDetail);
//            role.AddBorrower(CreateBorrower(data));
//            XERoles roles = new XERoles();
//            roles.AddRole(role);
//            party.SetRoles(roles);

//            party.SetTaxpayerIdentifiers(CreateTaxpayerIdentifiers(data));

//            return party;
//        }

//        private XETaxpayerIdentifiers CreateTaxpayerIdentifiers11(List<AppRelationshipData> data)
//        {
//            XETaxpayerIdentifiers taxpayerIdentifiers = new XETaxpayerIdentifiers();

//            taxpayerIdentifiers.AddTaxpayerIdentifier(CreateTaxpayerIdentifier(data[0]));

//            if (data.Count > 1)
//            {
//                taxpayerIdentifiers.AddTaxpayerIdentifier(CreateTaxpayerIdentifier(data[1]));
//            }
//            return taxpayerIdentifiers;
//        }

//        private XETaxpayerIdentifiers CreateTaxpayerIdentifiers(AppRelationshipData data)
//        {
//            XETaxpayerIdentifiers taxpayerIdentifiers = new XETaxpayerIdentifiers();

//            taxpayerIdentifiers.AddTaxpayerIdentifier(CreateTaxpayerIdentifier(data));
//            return taxpayerIdentifiers;
//        }

//        private XETaxpayerIdentifier CreateTaxpayerIdentifier(AppRelationshipData data)
//        {
//            XETaxpayerIdentifier taxpayerIdentifier = new XETaxpayerIdentifier();

//            taxpayerIdentifier.TaxpayerIdentifierValue = data.Ssn;

//            if (!string.IsNullOrEmpty(data.Ssn) && data.Ssn[0] == '9')
//            {
//                taxpayerIdentifier.TaxpayerIdentifierType = "IndividualTaxpayerIdentificationNumber";
//            }
//            else
//            {
//                taxpayerIdentifier.TaxpayerIdentifierType = "SocialSecurityNumber";
//            }

//            return taxpayerIdentifier;
//        }

//        private XEBorrower CreateBorrower(AppRelationshipData data)
//        {
//            XEBorrower borrower = new XEBorrower();

//            borrower.SetBorrowerDetail(CreateBorrowerDetail(data));
//            borrower.SetCounselingConfirmation(CreateCounselingConfirmation(data));
//            borrower.AddCreditScores(CreateCreditScores(data));
//            borrower.SetDeclaration(CreateDeclaration(data));
//            borrower.SetGovernmentMonitoring(CreateGovernmentMonitoring(data));

//            return borrower;
//        }

//        private XEBorrowerDetail CreateBorrowerDetail(AppRelationshipData data)
//        {
//            XEBorrowerDetail borrowerDetail = new XEBorrowerDetail();

//            borrowerDetail.BorrowerAgeAtApplicationYearsCount = data.BorrowerAgeAtApplicationYearsCount;
//            borrowerDetail.BorrowerBirthDate = data.BorrowerBirthDate;
//            borrowerDetail.BorrowerClassificationType = data.BorrowerClassificationType;
//            borrowerDetail.BorrowerMailToAddressSameasPropertyIndicator = data.BorrowerMailToAddressSameasPropertyIndicator;
//            borrowerDetail.BorrowerQualifyingIncomeAmount = TruncateMoneyAmount(data.BorrowerQualifyingIncomeAmount);

//            return borrowerDetail;
//        }

//        private XECounselingConfirmation CreateCounselingConfirmation(AppRelationshipData data)
//        {
//            XECounselingConfirmation counselingConfirmation = new XECounselingConfirmation();
                
//            switch (data.HomeOwnershipCounselingSource)
//            {
//                case E_aHomeOwnershipCounselingSourceT.BorrowerDidNotParticipate:
//                    counselingConfirmation.CounselingConfirmationType = E_CounselingConfirmationType.Other;
//                    counselingConfirmation.CounselingConfirmationTypeOtherDescription = E_CounselingConfirmationTypeOtherDescription.BorrowerDidNotParticipate;
//                    break;
//                case E_aHomeOwnershipCounselingSourceT.GovernmentAgency:
//                    counselingConfirmation.CounselingConfirmationType = E_CounselingConfirmationType.GovernmentAgency;
//                    break;
//                case E_aHomeOwnershipCounselingSourceT.HUDApprovedCounselingAgency:
//                    counselingConfirmation.CounselingConfirmationType = E_CounselingConfirmationType.HUDApprovedCounselingAgency;
//                    break;
//                case E_aHomeOwnershipCounselingSourceT.LenderTrainedCounseling:
//                    counselingConfirmation.CounselingConfirmationType = E_CounselingConfirmationType.LenderTrainedCounseling;
//                    break;
//                case E_aHomeOwnershipCounselingSourceT.MortgageInsuranceCompany:
//                    counselingConfirmation.CounselingConfirmationType = E_CounselingConfirmationType.Other;
//                    counselingConfirmation.CounselingConfirmationTypeOtherDescription = E_CounselingConfirmationTypeOtherDescription.MortgageInsuranceCompany;
//                    break;
//                case E_aHomeOwnershipCounselingSourceT.NoBorrowerCounseling:
//                    counselingConfirmation.CounselingConfirmationType = E_CounselingConfirmationType.NoBorrowerCounseling;
//                    break;
//                case E_aHomeOwnershipCounselingSourceT.NonProfitOrganization:
//                    counselingConfirmation.CounselingConfirmationType = E_CounselingConfirmationType.Other;
//                    counselingConfirmation.CounselingConfirmationTypeOtherDescription = E_CounselingConfirmationTypeOtherDescription.NonProfitOrganization;
//                    break;
//            }

//            switch (data.HomeOwnershipCounselingFormat)
//            {
//                case E_aHomeOwnershipCounselingFormatT.BorrowerDidNotParticipate:
//                    counselingConfirmation.CounselingFormatType = E_CounselingFormatType.Other;
//                    counselingConfirmation.CounselingFormatTypeOtherDescription = E_CounselingFormatTypeOtherDescription.BorrowerDidNotParticipate;
//                    break;
//                case E_aHomeOwnershipCounselingFormatT.BorrowerEducationNotRequired:
//                    counselingConfirmation.CounselingFormatType = E_CounselingFormatType.BorrowerEducationNotRequired;
//                    break;
//                case E_aHomeOwnershipCounselingFormatT.Classroom:
//                    counselingConfirmation.CounselingFormatType = E_CounselingFormatType.Classroom;
//                    break;
//                case E_aHomeOwnershipCounselingFormatT.HomeStudy:
//                    counselingConfirmation.CounselingFormatType = E_CounselingFormatType.HomeStudy;
//                    break;
//                case E_aHomeOwnershipCounselingFormatT.Individual:
//                    counselingConfirmation.CounselingFormatType = E_CounselingFormatType.Individual;
//                    break;
//            }

//            return counselingConfirmation;
//        }

//        private XECreditScores CreateCreditScores(AppRelationshipData data)
//        {
//            int EquifaxScore = 0;
//            try { EquifaxScore = Convert.ToInt32(data.EquifaxScore); }
//            catch (FormatException) { }

//            int ExperianScore = 0;
//            try { ExperianScore = Convert.ToInt32(data.ExperianScore); }
//            catch (FormatException) { }

//            int TransUnionScore = 0;
//            try { TransUnionScore = Convert.ToInt32(data.TransUnionScore); }
//            catch (FormatException) { }

//            int score = 0;
//            int creditScoresOnFile = 0;

//            if (EquifaxScore > 0)
//                creditScoresOnFile++;
//            if (ExperianScore > 0)
//                creditScoresOnFile++;
//            if (TransUnionScore > 0)
//                creditScoresOnFile++;

//            if (creditScoresOnFile == 1)
//            {
//                if (EquifaxScore > score)
//                    score = EquifaxScore;
//                else if (ExperianScore > score)
//                    score = ExperianScore;
//                else if (TransUnionScore > score)
//                    score = TransUnionScore;
//            }
//            else if (creditScoresOnFile == 2)
//            {
//                score = 9999;
//                if (EquifaxScore < score && EquifaxScore != 0)
//                    score = EquifaxScore;
//                else if (ExperianScore < score && ExperianScore != 0)
//                    score = ExperianScore;
//                else if (TransUnionScore < score && TransUnionScore != 0)
//                    score = TransUnionScore;
//            }
//            else if (creditScoresOnFile == 3)
//            {
//                if (EquifaxScore >= ExperianScore && EquifaxScore >= TransUnionScore)
//                {
//                    if (ExperianScore >= TransUnionScore)
//                        score = ExperianScore;
//                    else
//                        score = TransUnionScore;
//                }
//                else if (EquifaxScore <= ExperianScore && EquifaxScore >= TransUnionScore)
//                {
//                    if (EquifaxScore >= TransUnionScore)
//                        score = EquifaxScore;
//                    else
//                        score = TransUnionScore;
//                }
//                else
//                {
//                    if (EquifaxScore >= ExperianScore)
//                        score = EquifaxScore;
//                    else
//                        score = ExperianScore;
//                }
//            }

//            E_CreditRepositorySourceType type = E_CreditRepositorySourceType.Equifax;
//            if (score == ExperianScore)
//                type = E_CreditRepositorySourceType.Experian;
//            if (score == TransUnionScore)
//                type = E_CreditRepositorySourceType.TransUnion;

//            XECreditScores creditScores = new XECreditScores();

//            XECreditScore creditScore = new XECreditScore();
//            XECreditScoreDetail creditScoreDetail = new XECreditScoreDetail();
//            creditScoreDetail.CreditRepositorySourceType = type;
//            creditScoreDetail.CreditScoreValue = score.ToString();
//            creditScore.AddCreditScoreDetail(creditScoreDetail);
//            creditScores.AddCreditScore(creditScore);

//            return creditScores;
//        }

//        private XEDeclaration CreateDeclaration(AppRelationshipData data)
//        {
//            XEDeclaration declaration = new XEDeclaration();

//            declaration.SetDeclarationDetail(CreateDeclarationDetail(data));

//            return declaration;
//        }

//        private XEDeclarationDetail CreateDeclarationDetail(AppRelationshipData data)
//        {
//            XEDeclarationDetail declarationDetail = new XEDeclarationDetail();

//            if (data.DecPastOwnership.Equals("N"))
//            {
//                declarationDetail.BorrowerFirstTimeHomebuyerIndicator = "true";
//            }
            
//            if (data.DecCitizen == "Y")
//            {
//                declarationDetail.CitizenshipResidencyType = E_CitizenshipResidencyType.USCitizen;
//            }
//            else if (data.DecCitizen != "Y" && data.DecResidency == "Y")
//            {
//                declarationDetail.CitizenshipResidencyType = E_CitizenshipResidencyType.PermanentResidentAlien;
//            }
//            else if (data.DecCitizen != "Y" && data.DecResidency == "N")
//            {
//                declarationDetail.CitizenshipResidencyType = E_CitizenshipResidencyType.NonPermanentResidentAlien;
//            }

//            return declarationDetail;
//        }

//        private XEGovernmentMonitoring CreateGovernmentMonitoring(AppRelationshipData data)
//        {
//            XEGovernmentMonitoring governmentMonitoring = new XEGovernmentMonitoring();

//            governmentMonitoring.SetHudaRaces(CreateHudaRaces(data));
//            governmentMonitoring.SetGovernmentMonitoringDetail(CreateGovernmentMonitoringDetail(data));

//            return governmentMonitoring;
//        }

//        private XEHudaRaces CreateHudaRaces(AppRelationshipData data)
//        {
//            XEHudaRaces hudaRaces = new XEHudaRaces();

//            hudaRaces.SetHudaRace(CreateHudaRace(data));

//            return hudaRaces;
//        }

//        private XEHudaRace CreateHudaRace(AppRelationshipData data)
//        {
//            XEHudaRace hudaRace = new XEHudaRace();

//            if (data.aBIsAmericanIndian)
//            {
//                hudaRace.HMDARaceType = E_HMDARaceType.AmericanIndianOrAlaskaNative;
//            }
//            else if (data.aBIsAsian)
//            {
//                hudaRace.HMDARaceType = E_HMDARaceType.Asian;
//            }
//            else if (data.aBIsBlack)
//            {
//                hudaRace.HMDARaceType = E_HMDARaceType.BlackOrAfricanAmerican;
//            }
//            else if (data.aBIsPacificIslander)
//            {
//                hudaRace.HMDARaceType = E_HMDARaceType.NativeHawaiianOrOtherPacificIslander;
//            }
//            else if (data.aBIsWhite)
//            {
//                hudaRace.HMDARaceType = E_HMDARaceType.White;
//            }
//            else
//            {
//                hudaRace.HMDARaceType = E_HMDARaceType.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
//            }

//            return hudaRace;
//        }

//        private XEGovernmentMonitoringDetail CreateGovernmentMonitoringDetail(AppRelationshipData data)
//        {
//            XEGovernmentMonitoringDetail governmentMonitoringDetail = new XEGovernmentMonitoringDetail();

//            governmentMonitoringDetail.GenderType = ToMismo(data.Gender);
//            governmentMonitoringDetail.HMDAEthnicityType = ToMismo(data.HMDAEthnicityType);

//            return governmentMonitoringDetail;
//        }

//        private XEIndividual CreateIndividual(AppRelationshipData data)
//        {
//            XEIndividual individual = new XEIndividual();

//            individual.SetName(CreateName(data));

//            return individual;
//        }

//        private XEName CreateName(AppRelationshipData data)
//        {
//            XEName name = new XEName();

//            name.FirstName = data.FirstName;
//            if (!string.IsNullOrEmpty(data.MiddleName))
//            {
//                name.MiddleName = data.MiddleName.Substring(0, 1);
//            }
//            name.SuffixName = data.SuffixName;
//            name.LastName = data.LastName;

//            return name;
//        }

//        private XEAddress CreateAddress(AppRelationshipData data)
//        {
//            if (string.IsNullOrEmpty(data.AddressLineText)
//                && string.IsNullOrEmpty(data.CityName)
//                && string.IsNullOrEmpty(data.StateCode)
//                && string.IsNullOrEmpty(data.PostalCode))
//            {
//                return null;
//            }

//            XEAddress address = new XEAddress();

//            address.AddressLineText = data.AddressLineText;
//            address.CityName = data.CityName;
//            address.PostalCode = data.PostalCode;
//            address.StateCode = data.StateCode;
//            address.AddressType = data.AddressType;
//            address.CountryCode = data.CountryCode;

//            return address;
//        }

//        private XELoans CreateLoans()
//        {
//            XELoans loans = new XELoans();

//            loans.SetCombinedLtvs(CreateCombinedLtvs());
//            loans.AddLoan(CreateLoanClosing());
//            loans.AddLoan(CreateLoanCurrent());

//            if (m_dataLoan.sLienPosT == E_sLienPosT.Second && m_dataLoan.sLinkedLoanInfo.IsLoanLinked)
//            {
//                loans.AddLoan(CreateLoanClosingRelated());
//                loans.AddLoan(CreateLoanCurrentRelated());
//            }
//            else if (m_dataLoan.sLienPosT == E_sLienPosT.First && m_dataLoan.sLinkedLoanInfo.IsLoanLinked)
//            {
//                loans.AddLoan(CreateLoanCurrentRelated2nd());
//            }

//            return loans;
//        }

//        private XELoan CreateLoanCurrentRelated2nd()
//        {
//            XELoan loan = new XELoan();

//            loan.LoanRoleType = "RelatedLoan";
//            loan.SetLoanDetail(CreateLoanDetailCurrentRelated());
//            loan.SetLoanState(CreateLoanStateCurrentRelated());
//            if (m_dataLoanLinked.m_convertLos.ToMoney(m_dataLoanLinked.sUPBAmount_rep) > 0.0M)
//                loan.SetPayment(CreatePaymentCurrentRelated());
//            loan.SetTermsOfMortgage(CreateTermsOfMortgageCurrentRelated());

//            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sSubFin_rep) != 0.0M)
//            {
//                loan.SetHeloc(CreateHelocCurrentRelated());
//            }
//            return loan;
//        }

//        private XELoanState CreateLoanStateCurrentRelated()
//        {
//            XELoanState loanState = new XELoanState();

//            loanState.LoanStateDate = m_dataLoan.m_convertLos.ToDateTimeString(DateTime.Now);
//            loanState.LoanStateType = "Current";

//            return loanState;
//        }

//        private XETermsOfMortgage CreateTermsOfMortgageCurrentRelated()
//        {
//            XETermsOfMortgage termsOfMortgage = new XETermsOfMortgage();

//            termsOfMortgage.LienPriorityType = E_LienPriorityType.SecondLien;
//            termsOfMortgage.MortgageType = ToMismo(m_dataLoanLinked.sLT);

//            return termsOfMortgage;
//        }

//        private XELoan CreateLoanCurrentRelated()
//        {
//            XELoan loan = new XELoan();

//            loan.LoanRoleType = "RelatedLoan";
//            loan.SetLoanDetail(CreateLoanDetailCurrentRelated());
//            loan.SetLoanState(CreateLoanStateCurrentRelated());
//            loan.SetPayment(CreatePaymentCurrentRelated());

//            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sSubFin_rep) != 0.0M)
//            {
//                loan.SetHeloc(CreateHelocCurrentRelated());
//            }

//            return loan;
//        }

//        private XEHeloc CreateHelocCurrentRelated()
//        {
//            XEHeloc heloc = new XEHeloc();

//            heloc.SetHelocOccurrences(CreateHelocOccurrencesCurrentRelated());

//            return heloc;
//        }

//        private XEHelocOccurrences CreateHelocOccurrencesCurrentRelated()
//        {
//            XEHelocOccurrences helocOccurrences = new XEHelocOccurrences();

//            helocOccurrences.SetHelocOccurrence(CreateHelocOccurrenceCurrentRelated());

//            return helocOccurrences;
//        }

//        private XEHelocOccurrence CreateHelocOccurrenceCurrentRelated()
//        {
//            XEHelocOccurrence helocOccurrence = new XEHelocOccurrence();

//            helocOccurrence.CurrentHELOCMaximumBalanceAmount = TruncateMoneyAmount(m_dataLoan.sSubFin_rep);
//            helocOccurrence.HELOCBalanceAmount = TruncateMoneyAmount(m_dataLoan.sConcurSubFin_rep);

//            return helocOccurrence;
//        }

//        private XEPaymentSummary CreatePaymentSummaryCurrentRelated()
//        {
//            XEPaymentSummary paymentSummary = new XEPaymentSummary();

//            paymentSummary.UPBAmount = m_dataLoanLinked.sUPBAmount_rep;

//            return paymentSummary;
//        }

//        private XEPayment CreatePaymentCurrentRelated()
//        {
//            XEPayment payment = new XEPayment();

//            payment.SetPaymentSummary(CreatePaymentSummaryCurrentRelated());

//            return payment;
//        }

//        private XELoanDetail CreateLoanDetailCurrentRelated()
//        {
//            XELoanDetail loanDetail = new XELoanDetail();

//            if (m_dataLoan.sIsOFinCreditLineInDrawPeriod)
//            {
//                loanDetail.HELOCIndicator = "true";
//            }

//            return loanDetail;
//        }

//        private XELoan CreateLoanClosingRelated()
//        {
//            XELoan loan = new XELoan();

//            loan.LoanRoleType = "RelatedLoan";
//            loan.SetAmortization(CreateAmortizationClosingRelated());
//            loan.SetLoanDetail(CreateLoanDetailClosingRelated());
//            loan.SetLoanState(CreateLoanStateClosingRelated());
//            loan.SetMaturity(CreateMaturityClosingRelated());
//            loan.SetPayment(CreatePaymentClosingRelated());
//            loan.SetTermsOfMortgage(CreateTermsOfMortgageClosingRelated());

//            return loan;
//        }

//        private XETermsOfMortgage CreateTermsOfMortgageClosingRelated()
//        {
//            XETermsOfMortgage termsOfMortgage = new XETermsOfMortgage();

//            termsOfMortgage.LienPriorityType = E_LienPriorityType.FirstLien;
//            termsOfMortgage.NoteAmount = m_dataLoanLinked.sFinalLAmt_rep;

//            return termsOfMortgage;
//        }

//        private XEPaymentRule CreatePaymentRuleClosingRelated()
//        {
//            XEPaymentRule paymentRule = new XEPaymentRule();

//            paymentRule.ScheduledFirstPaymentDate = m_dataLoanLinked.sSchedDueD1_rep;

//            return paymentRule;
//        }

//        private XEPayment CreatePaymentClosingRelated()
//        {
//            XEPayment payment = new XEPayment();

//            payment.SetPaymentRule(CreatePaymentRuleClosingRelated());

//            return payment;
//        }

//        private XEMaturityRule CreateMaturityRuleClosingRelated()
//        {
//            XEMaturityRule maturityRule = new XEMaturityRule();

//            maturityRule.LoanMaturityPeriodCount = m_dataLoanLinked.sTerm_rep;
//            maturityRule.LoanMaturityPeriodType = "Month";

//            return maturityRule;
//        }

//        private XEMaturity CreateMaturityClosingRelated()
//        {
//            XEMaturity maturity = new XEMaturity();

//            maturity.SetMaturityRule(CreateMaturityRuleClosingRelated());

//            return maturity;
//        }

//        private XELoanDetail CreateLoanDetailClosingRelated()
//        {
//            XELoanDetail loanDetail = new XELoanDetail();

//            loanDetail.BalloonIndicator = (m_dataLoanLinked.sTerm > m_dataLoanLinked.sDue).ToString().ToLower();

//            return loanDetail;
//        }

//        private XELoanState CreateLoanStateClosingRelated()
//        {
//            XELoanState loanState = new XELoanState();

//            loanState.LoanStateDate = m_dataLoanLinked.sDocumentNoteD_rep;
//            loanState.LoanStateType = "AtClosing";

//            return loanState;
//        }

//        private XEAmortization CreateAmortizationClosingRelated()
//        {
//            XEAmortization amortization = new XEAmortization();

//            amortization.SetAmortizationRule(CreateAmortizationRuleClosingRelated());

//            return amortization;
//        }

//        private XEAmortizationRule CreateAmortizationRuleClosingRelated()
//        {
//            XEAmortizationRule amortizationRule = new XEAmortizationRule();

//            amortizationRule.LoanAmortizationType = ToMismo(m_dataLoanLinked.sFinMethT);

//            return amortizationRule;
//        }

//        private XELoan CreateLoanCurrent()
//        {
//            XELoan loan = new XELoan();

//            loan.LoanRoleType = "SubjectLoan";
//            if (m_dataLoan.sFinMethT == E_sFinMethT.ARM)
//                loan.SetAdjustment(CreateAdjustmentCurrent());

//            if (m_dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
//            {
//                loan.SetEscrow(CreateEscrow());
//            }
//            loan.SetInvestorFeatures(CreateInvestorFeatures());
//            loan.SetInvestorLoanInformation(CreateInvestorLoanInformationCurrent());
//            loan.SetLoanComments(CreateLoanCommentsCurrent());
//            loan.SetLoanDetail(CreateLoanDetailCurrent());
//            loan.SetLoanIdentifiers(CreateLoanIdentifiersCurrent());
//            loan.SetLoanPrograms(CreateLoanProgramsCurrent());
//            loan.SetLoanState(CreateLoanStateCurrent());
//            loan.SetMiData(CreateMiDataCurrent());
//            loan.SetPayment(CreatePaymentCurrent());
//            loan.SetSelectedLoanProduct(CreateSelectedLoanProductCurrent());
//            loan.SetServicing(CreateServicingCurrent());

//            return loan;
//        }

//        private XELoanComment CreateLoanCommentCurrent()
//        {
//            XELoanComment loanComment = new XELoanComment();

//            loanComment.LoanCommentText = m_dataLoan.sGseDeliveryComments;

//            return loanComment;
//        }

//        private XELoanComments CreateLoanCommentsCurrent()
//        {
//            XELoanComments loanComments = new XELoanComments();

//            loanComments.SetLoanComment(CreateLoanCommentCurrent());

//            return loanComments;
//        }

//        private XELoanPrograms CreateLoanProgramsCurrent()
//        {
//            XELoanPrograms loanPrograms = new XELoanPrograms();

//            loanPrograms.SetLoanProgram(CreateLoanProgramCurrent());

//            return loanPrograms;
//        }

//        private XELoanProgram CreateLoanProgramCurrent()
//        {
//            XELoanProgram loanProgram = new XELoanProgram();

//            if (m_dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac)
//            {
//                switch (m_dataLoan.sGseFreddieMacProductT)
//                {
//                    case E_sGseFreddieMacProductT.None:
//                        break;
//                    case E_sGseFreddieMacProductT.AMinusMortgage:
//                        loanProgram.LoanProgramIdentifier = "AMinusMortgage";
//                        break;
//                    case E_sGseFreddieMacProductT.ConstructionConversion:
//                        loanProgram.LoanProgramIdentifier = "ConstructionConversion";
//                        break;
//                    case E_sGseFreddieMacProductT.DisasterReliefProgram:
//                        loanProgram.LoanProgramIdentifier = "DisasterReliefProgram";
//                        break;
//                    case E_sGseFreddieMacProductT.HomePossible97:
//                        loanProgram.LoanProgramIdentifier = "HomePossible97";
//                        break;
//                    case E_sGseFreddieMacProductT.HomePossibleNeighborhoodSolution97:
//                        loanProgram.LoanProgramIdentifier = "HomePossibleNeighborhoodSolution97";
//                        break;
//                    case E_sGseFreddieMacProductT.Renovation:
//                        loanProgram.LoanProgramIdentifier = "Renovation";
//                        break;
//                }
//            }
//            else
//            {
//                if (m_dataApp.aBDecPastOwnership == "N")
//                {
//                    loanProgram.LoanProgramIdentifier = "LoanFirstTimeHomebuyer";
//                }
//            }

//            return loanProgram;
//        }

//        private XEDelinquencySummary CreateDelinquencySummaryCurrent()
//        {
//            XEDelinquencySummary delinquencySummary = new XEDelinquencySummary();

//            int count = 0;
//            foreach(CServicingPaymentFields spf in m_dataLoan.sServicingPayments)
//            {
//                DateTime paymentD = spf.PaymentD;
//                DateTime dueD = spf.DueD;

//                if (dueD == DateTime.MinValue || dueD.AddMonths(12) < DateTime.Now)
//                    continue;

//                if (paymentD == DateTime.MinValue)
//                    paymentD = DateTime.Now;

//                if (dueD.Day == 1)
//                    dueD = dueD.AddMonths(1);
//                else
//                    dueD = dueD.AddDays(30);

//                if (paymentD > dueD)
//                    count++;
//            }

//            delinquencySummary.DelinquentPaymentsOverPastTwelveMonthsCount = count.ToString();

//            return delinquencySummary;
//        }

//        private XEServicing CreateServicingCurrent()
//        {
//            XEServicing servicing = new XEServicing();

//            servicing.SetDelinquencySummary(CreateDelinquencySummaryCurrent());

//            return servicing;
//        }

//        private XELoanProductDetail CreateLoanProductDetailCurrent()
//        {
//            XELoanProductDetail loanProductDetail = new XELoanProductDetail();

//            if (m_dataLoan.sSpGseRefiProgramT != E_sSpGseRefiProgramT.None)
//            {
//                if (m_dataLoan.sProdIsTexas50a6Loan)
//                {
//                    loanProductDetail.RefinanceProgramIdentifier = E_RefinanceProgramIdentifier.TexasEquity;
//                }
//                else
//                {
//                    switch (m_dataLoan.sSpGseRefiProgramT)
//                    {
//                        case E_sSpGseRefiProgramT.None:
//                            break;
//                        case E_sSpGseRefiProgramT.DURefiPlus:
//                            loanProductDetail.RefinanceProgramIdentifier = E_RefinanceProgramIdentifier.DURefiPlus;
//                            break;
//                        case E_sSpGseRefiProgramT.RefiPlus:
//                            loanProductDetail.RefinanceProgramIdentifier = E_RefinanceProgramIdentifier.RefiPlus;
//                            break;
//                        case E_sSpGseRefiProgramT.ReliefRefiOpenAccess:
//                            loanProductDetail.RefinanceProgramIdentifier = E_RefinanceProgramIdentifier.ReliefRefinanceOpenAccess;
//                            break;
//                        case E_sSpGseRefiProgramT.ReliefRefiSameServicer:
//                            loanProductDetail.RefinanceProgramIdentifier = E_RefinanceProgramIdentifier.ReliefRefinanceSameServicer;
//                            break;
//                        case E_sSpGseRefiProgramT.FHLBDisasterResponse:
//                            loanProductDetail.RefinanceProgramIdentifier = E_RefinanceProgramIdentifier.DisasterResponse;
//                            break;
//                    }
//                }
//            }

//            return loanProductDetail;
//        }

//        private XESelectedLoanProduct CreateSelectedLoanProductCurrent()
//        {
//            XESelectedLoanProduct selectedLoanProduct = new XESelectedLoanProduct();

//            selectedLoanProduct.SetLoanProductDetail(CreateLoanProductDetailCurrent());

//            return selectedLoanProduct;
//        }

//        private XEPaymentComponentBreakout CreatePaymentComponentBreakoutCurrent()
//        {
//            XEPaymentComponentBreakout paymentComponentBreakout = new XEPaymentComponentBreakout();

//            paymentComponentBreakout.PrincipalAndInterestPaymentAmount = m_dataLoan.sProThisMPmt_rep;

//            return paymentComponentBreakout;
//        }

//        private XEPaymentComponentBreakouts CreatePaymentComponentBreakoutsCurrent()
//        {
//            XEPaymentComponentBreakouts paymentComponentBreakouts = new XEPaymentComponentBreakouts();

//            paymentComponentBreakouts.SetPaymentComponentBreakout(CreatePaymentComponentBreakoutCurrent());

//            return paymentComponentBreakouts;
//        }

//        private XEPaymentSummary CreatePaymentSummaryCurrent()
//        {
//            XEPaymentSummary paymentSummary = new XEPaymentSummary();

//            DateTime LastPaymentReceivedDate = DateTime.MinValue;
//            DateTime LastPaidInstallmentDueDate = DateTime.MinValue;
            
//            //decimal AggregateLoanCurtailmentAmount = 0.0M;
//            foreach(CServicingPaymentFields spf in m_dataLoan.sServicingPayments)
//            {
//                /*if (spf.PaymentAmt > spf.DueAmt)
//                {
//                    AggregateLoanCurtailmentAmount += spf.PaymentAmt - spf.DueAmt;
//                }*/

//                if (spf.PaymentD > LastPaymentReceivedDate)
//                {
//                    LastPaymentReceivedDate = spf.PaymentD;
//                }

//                if (spf.PaymentD != DateTime.MinValue && spf.DueD > LastPaidInstallmentDueDate)
//                {
//                    LastPaidInstallmentDueDate = spf.DueD;
//                }
//            }

//            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sServicingCollectedFunds_Principal_rep) > 0M)
//            {
//                paymentSummary.AggregateLoanCurtailmentAmount = TruncateMoneyAmount(m_dataLoan.sServicingCollectedFunds_Principal_rep);
//            }

//            if (LastPaidInstallmentDueDate != DateTime.MinValue)
//            {
//                paymentSummary.LastPaidInstallmentDueDate = m_dataLoan.m_convertLos.ToDateTimeString(LastPaidInstallmentDueDate);
//            }
//            else
//            {
//                paymentSummary.LastPaidInstallmentDueDate = m_dataLoan.m_convertLos.ToDateTimeString(m_dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(-1));
//            }

//            if (LastPaymentReceivedDate != DateTime.MinValue)
//            {
//                paymentSummary.LastPaymentReceivedDate = m_dataLoan.m_convertLos.ToDateTimeString(LastPaymentReceivedDate);
//            }

//            paymentSummary.UPBAmount = TruncateMoneyAmount(m_dataLoan.sUPBAmount_rep);

//            return paymentSummary;
//        }

//        private XEPayment CreatePaymentCurrent()
//        {
//            XEPayment payment = new XEPayment();

//            payment.SetPaymentComponentBreakouts(CreatePaymentComponentBreakoutsCurrent());
//            payment.SetPaymentSummary(CreatePaymentSummaryCurrent());

//            return payment;
//        }

//        private XEMiDataDetail CreateMiDataDetailCurrent()
//        {
//            XEMiDataDetail miDataDetail = new XEMiDataDetail();

//            miDataDetail.LenderPaidMIInterestRateAdjustmentPercent = m_dataLoan.sMiLenderPaidAdj_rep;
//            miDataDetail.MICertificateIdentifier = m_dataLoan.sMiCertId;

//            if (!string.IsNullOrEmpty(miDataDetail.MICertificateIdentifier))
//            {
//                miDataDetail.MIPremiumFinancedIndicator = m_dataLoan.sFfUfMipIsBeingFinanced.ToString().ToLower();

//                if (m_dataLoan.sFfUfMipIsBeingFinanced)
//                {
//                    miDataDetail.MIPremiumFinancedAmount = m_dataLoan.sFfUfmipFinanced_rep;
//                }
//            }

//            if (m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.FHA
//                && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.USDA
//                && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.LeaveBlank
//                && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.VA)
//            {
//                if (m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.Amerin
//                    && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.CAHLIF
//                    && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.CMGPre94
//                    && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.Commonwealth
//                    && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.MIF
//                    && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.RMICNC
//                    && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.Verex
//                    && m_dataLoan.sMiCompanyNmT != E_sMiCompanyNmT.WiscMtgAssr)
//                {
//                    miDataDetail.MICompanyNameType = ToMismo(m_dataLoan.sMiCompanyNmT);
//                }
//                else
//                {
//                    miDataDetail.MICompanyNameTypeOtherDescription = ToMismo(m_dataLoan.sMiCompanyNmT);
//                    miDataDetail.MICompanyNameType = "Other";
//                }
//            }
//            miDataDetail.MICoveragePercent = TruncateMoneyAmount(m_dataLoan.sMiLenderPaidCoverage_rep);

//            if (m_dataLoan.sMiInsuranceT != E_sMiInsuranceT.InvestorPaid && m_dataLoan.sMiInsuranceT != E_sMiInsuranceT.None)
//            {
//                miDataDetail.MIPremiumSourceType = ToMismo(m_dataLoan.sMiInsuranceT);
//            }
//            else if (m_dataLoan.sLT == E_sLT.Conventional)
//            {
//                if (m_dataLoan.sMiReasonForAbsenceT == E_sMiReasonForAbsenceT.MiCanceledBasedOnCurrentLtv
//                    || m_dataLoan.sMiReasonForAbsenceT == E_sMiReasonForAbsenceT.NoMiBasedOnOriginalLtv)
//                {
//                    miDataDetail.PrimaryMIAbsenceReasonType = ToMismo(m_dataLoan.sMiReasonForAbsenceT);
//                }
//                else
//                {
//                    miDataDetail.PrimaryMIAbsenceReasonType = E_PrimaryMIAbsenceReasonType.Other;
//                    miDataDetail.PrimaryMIAbsenceReasonTypeOtherDescription = ToMismo(m_dataLoan.sMiReasonForAbsenceT);
//                }
//            }

//            return miDataDetail;
//        }

//        private XEMiData CreateMiDataCurrent()
//        {
//            XEMiData miData = new XEMiData();

//            miData.SetMiDataDetail(CreateMiDataDetailCurrent());

//            return miData;
//        }

//        private XELoanState CreateLoanStateCurrent()
//        {
//            XELoanState loanState = new XELoanState();

//            loanState.LoanStateDate = m_dataLoan.m_convertLos.ToDateTimeString(DateTime.Now);
//            loanState.LoanStateType = "Current";

//            return loanState;
//        }

//        private XELoanIdentifiers CreateLoanIdentifiersCurrent()
//        {
//            XELoanIdentifiers loanIdentifiers = new XELoanIdentifiers();

//            XELoanIdentifier loanIdentifier = new XELoanIdentifier();
//            if (!string.IsNullOrEmpty(m_dataLoan.sGseDeliveryCommitmentId))
//            {
//                loanIdentifier.InvestorCommitmentIdentifier = m_dataLoan.sGseDeliveryCommitmentId;
//                loanIdentifiers.AddLoanIdentifier(loanIdentifier);
//            }

//            loanIdentifier = new XELoanIdentifier();
//            if (!string.IsNullOrEmpty(m_dataLoan.sGseDeliveryContractId))
//            {
//                loanIdentifier.InvestorContractIdentifier = m_dataLoan.sGseDeliveryContractId;
//                loanIdentifiers.AddLoanIdentifier(loanIdentifier);
//            }

//            loanIdentifier = new XELoanIdentifier();
//            if (!string.IsNullOrEmpty(m_dataLoan.sMersMin))
//            {
//                loanIdentifier.MERS_MINIdentifier = m_dataLoan.sMersMin;
//                loanIdentifiers.AddLoanIdentifier(loanIdentifier);
//            }

//            loanIdentifier = new XELoanIdentifier();
//            if (!string.IsNullOrEmpty(m_dataLoan.sLenNum))
//            {
//                loanIdentifier.SellerLoanIdentifier = m_dataLoan.sLenLNum;
//                loanIdentifiers.AddLoanIdentifier(loanIdentifier);
//            }

//            /*loanIdentifier = new XELoanIdentifier();
//            if (!string.IsNullOrEmpty(m_dataLoan.sLenNum))
//            {
//                loanIdentifier.ServicerLoanIdentifier = m_dataLoan.sLenNum;
//                loanIdentifiers.AddLoanIdentifier(loanIdentifier);
//            }*/

//            return loanIdentifiers;
//        }

//        private XELoanDetail CreateLoanDetailCurrent()
//        {
//            XELoanDetail loanDetail = new XELoanDetail();

//            if (m_dataLoan.sTerm > m_dataLoan.sDue)
//            {
//                loanDetail.BalloonResetIndicator = "false";
//            }
//            if (m_dataLoan.sFinMethT == E_sFinMethT.ARM)
//            {
//                loanDetail.CurrentInterestRatePercent = m_dataLoan.sNoteIR_rep;
//            }
//            loanDetail.MortgageModificationIndicator = "false";

//            return loanDetail;
//        }

//        private XEInvestorLoanInformation CreateInvestorLoanInformationCurrent()
//        {
//            XEInvestorLoanInformation investorLoanInformation = new XEInvestorLoanInformation();

//            if (m_dataLoan.sGseDeliveryLoanDeliveryT == E_sGseDeliveryLoanDeliveryT.MBS)
//            {
//                investorLoanInformation.BaseGuarantyFeePercent = m_dataLoan.sGseDeliveryBaseGuaranteeF_rep;
//                investorLoanInformation.GuarantyFeeAfterAlternatePaymentMethodPercent = m_dataLoan.sGseDeliveryBaseGuaranteeF_rep;
//                investorLoanInformation.GuarantyFeePercent = m_dataLoan.sGseDeliveryBaseGuaranteeF_rep;
//                investorLoanInformation.InvestorRemittanceDay = m_dataLoan.sGseDeliveryRemittanceNumOfDays_rep;
//                investorLoanInformation.LoanAcquisitionScheduledUPBAmount = m_dataLoan.sGseDeliveryScheduleUPBAmt_rep;

//                decimal buyF = m_dataLoan.m_convertLos.ToRate(m_dataLoan.sGseDeliveryBuyF_rep);
//                investorLoanInformation.LoanBuyupBuydownBasisPointNumber = m_dataLoan.m_convertLos.ToRateStringNoPercent(Math.Abs(buyF));
                
//                if (buyF == 0)
//                {
//                    investorLoanInformation.LoanBuyupBuydownType = E_LoanBuyupBuydownType.BuyupBuydownDoesNotApply;
//                }
//                else if (buyF < 0)
//                {
//                    investorLoanInformation.LoanBuyupBuydownType = E_LoanBuyupBuydownType.Buydown;
//                }
//                else
//                {
//                    investorLoanInformation.LoanBuyupBuydownType = E_LoanBuyupBuydownType.Buyup;
//                }

//                investorLoanInformation.LoanDefaultLossPartyType = ToMismo(m_dataLoan.sGseDeliveryLoanDefaultLossPartyT);
//                investorLoanInformation.REOMarketingPartyType = ToMismo(m_dataLoan.sGseDeliveryREOMarketingPartyT);
//            }

//            if (m_dataLoan.sSpGseCollateralProgramT != E_sSpGseCollateralProgramT.DURefiPlus
//                && m_dataLoan.sSpGseCollateralProgramT != E_sSpGseCollateralProgramT.None
//                && m_dataLoan.sSpGseCollateralProgramT != E_sSpGseCollateralProgramT.PropertyInspectionReportForm2075)
//            {
//                investorLoanInformation.InvestorCollateralProgramIdentifier = ToMismo(m_dataLoan.sSpGseCollateralProgramT);
//            }
//            investorLoanInformation.InvestorOwnershipPercent = TruncateMoneyAmount(m_dataLoan.sGseDeliveryOwnershipPc_rep);
//            investorLoanInformation.InvestorProductPlanIdentifier = m_dataLoan.sGseDeliveryPlanId;
//            investorLoanInformation.InvestorRemittanceType = ToMismo(m_dataLoan.sGseDeliveryRemittanceT);

//            return investorLoanInformation;
//        }

//        private XEInvestorFeatures CreateInvestorFeatures()
//        {
//            XEInvestorFeatures investorFeatures = new XEInvestorFeatures();

//            if (!string.IsNullOrEmpty(m_dataLoan.sGseDeliveryFeature1Id))
//            {
//                investorFeatures.AddInvestorFeature(CreateInvestorFeature(m_dataLoan.sGseDeliveryFeature1Id));
//            }

//            if (!string.IsNullOrEmpty(m_dataLoan.sGseDeliveryFeature2Id))
//            {
//                investorFeatures.AddInvestorFeature(CreateInvestorFeature(m_dataLoan.sGseDeliveryFeature2Id));
//            }

//            if (!string.IsNullOrEmpty(m_dataLoan.sGseDeliveryFeature3Id))
//            {
//                investorFeatures.AddInvestorFeature(CreateInvestorFeature(m_dataLoan.sGseDeliveryFeature3Id));
//            }

//            if (!string.IsNullOrEmpty(m_dataLoan.sGseDeliveryFeature4Id))
//            {
//                investorFeatures.AddInvestorFeature(CreateInvestorFeature(m_dataLoan.sGseDeliveryFeature4Id));
//            }

//            if (!string.IsNullOrEmpty(m_dataLoan.sGseDeliveryFeature5Id))
//            {
//                investorFeatures.AddInvestorFeature(CreateInvestorFeature(m_dataLoan.sGseDeliveryFeature5Id));
//            }

//            if (!string.IsNullOrEmpty(m_dataLoan.sGseDeliveryFeature6Id))
//            {
//                investorFeatures.AddInvestorFeature(CreateInvestorFeature(m_dataLoan.sGseDeliveryFeature6Id));
//            }

//            return investorFeatures;
//        }

//        private XEInvestorFeature CreateInvestorFeature(string id)
//        {
//            XEInvestorFeature investorFeature = new XEInvestorFeature();

//            investorFeature.InvestorFeatureIdentifier = id;

//            return investorFeature;
//        }

//        private XEEscrow CreateEscrow()
//        {
//            XEEscrow escrow = new XEEscrow();
//            XEEscrowItems escrowItems = CreateEscrowItems();

//            if (escrowItems == null)
//                return null;

//            escrow.SetEscrowItems(escrowItems);
            
//            return escrow;
//        }

//        private string GetEscrowItemType(E_TaxTableTaxT tax, string defaultType)
//        {
//            switch (tax)
//            {
//                case E_TaxTableTaxT.Borough: return "BoroughPropertyTax";
//                case E_TaxTableTaxT.City: return "CityPropertyTax";
//                case E_TaxTableTaxT.County: return "CountyPropertyTax";
//                case E_TaxTableTaxT.FireDist: return "DistrictPropertyTax";
//                case E_TaxTableTaxT.LeaveBlank: return defaultType;
//                case E_TaxTableTaxT.LocalImprovementDist: return "DistrictPropertyTax";
//                case E_TaxTableTaxT.Miscellaneous: return "OtherTax";
//                case E_TaxTableTaxT.MunicipalUtilDist: return "DistrictPropertyTax";
//                case E_TaxTableTaxT.School: return "SchoolPropertyTax";
//                case E_TaxTableTaxT.SpecialAssessmentDist: return "DistrictPropertyTax";
//                case E_TaxTableTaxT.Town: return "CityPropertyTax";
//                case E_TaxTableTaxT.Township: return "TownshipPropertyTax";
//                case E_TaxTableTaxT.Utility: return "OtherTax";
//                case E_TaxTableTaxT.Village: return "CityPropertyTax";
//                case E_TaxTableTaxT.WasteFeeDist: return "DistrictPropertyTax";
//                case E_TaxTableTaxT.Water_Irrigation: return "OtherTax";
//                case E_TaxTableTaxT.Water_Sewer: return "OtherTax";
//                default: 
//                    Tools.LogBug("Unhandled enum value for E_TaxTableTaxT");
//                    return defaultType;
//            }
//        }

//        private XEEscrowItems CreateEscrowItems()
//        {
//            XEEscrowItems escrowItems = new XEEscrowItems();
//            bool hasEntries = false;
//            bool isFreddieMac = m_dataLoan.sGseDeliveryTargetT == E_sGseDeliveryTargetT.FreddieMac;

//            XEEscrowItem escrowItem = CreateEscrowItem("HazardInsurance", m_dataLoan.sProHazIns_rep);
//            if (escrowItem != null)
//            {
//                escrowItems.AddEscrowItem(escrowItem);
//                hasEntries = true;
//            }

//            escrowItem = CreateEscrowItem("MortgageInsurance", m_dataLoan.sProMIns_rep);
//            if (escrowItem != null)
//            {
//                escrowItems.AddEscrowItem(escrowItem);
//                hasEntries = true;
//            }

//            if (isFreddieMac)
//            {
//                string type = GetEscrowItemType(m_dataLoan.sTaxTableRealETxT, "CountyPropertyTax");
//                escrowItem = CreateEscrowItem(type, m_dataLoan.sProRealETx_rep);
//            }
//            else
//            {
//                escrowItem = CreateEscrowItem("CountyPropertyTax", m_dataLoan.sProRealETx_rep);
//            }
//            if (escrowItem != null)
//            {
//                escrowItems.AddEscrowItem(escrowItem);
//                hasEntries = true;
//            }

//            if (isFreddieMac)
//            {
//                string type = GetEscrowItemType(m_dataLoan.sTaxTableSchoolT, "SchoolPropertyTax");
//                escrowItem = CreateEscrowItem(type, m_dataLoan.sProSchoolTx_rep);
//            }
//            else
//            {
//                escrowItem = CreateEscrowItem("SchoolPropertyTax", m_dataLoan.sProSchoolTx_rep);
//            }
//            if (escrowItem != null)
//            {
//                escrowItems.AddEscrowItem(escrowItem);
//                hasEntries = true;
//            }

//            escrowItem = CreateEscrowItem("FloodInsurance", m_dataLoan.sProFloodIns_rep);
//            if (escrowItem != null)
//            {
//                escrowItems.AddEscrowItem(escrowItem);
//                hasEntries = true;
//            }

//            if (isFreddieMac)
//            {
//                // Check if either of the tax dropdowns have 1008 selected.
//                // If it is not one of those, then check the insurance pages.
                
//                string type = "";
//                bool shouldHaveFailedAudit = false;

//                if (m_dataLoan.sTaxTable1008DescT == E_TableSettlementDescT.Use1008DescT)
//                {
//                    type = GetEscrowItemType(m_dataLoan.sTaxTable1008T, "default");
//                }
//                else if (m_dataLoan.sTaxTable1009DescT == E_TableSettlementDescT.Use1008DescT)
//                {
//                    type = GetEscrowItemType(m_dataLoan.sTaxTable1009T, "default");
//                }
//                else if (m_dataLoan.sWindInsSettlementChargeT == E_sInsSettlementChargeT.Line1008)
//                {
//                    type = "StormInsurance";
//                }
//                else if (m_dataLoan.sCondoHO6InsSettlementChargeT == E_sInsSettlementChargeT.Line1008)
//                {
//                    type = "HazardInsurance";
//                }
//                else // We cannot determine the type, which is fine as long as the dollar amount is 0
//                {
//                    if (m_dataLoan.s1006ProHExp > 0)
//                        shouldHaveFailedAudit = true;
//                }

//                if (type.Equals("default") || shouldHaveFailedAudit)
//                {
//                    string devMessage = "Failed to determine type of item. Should have failed audit.";
//                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, devMessage);
//                }
//                escrowItem = CreateEscrowItem(type, m_dataLoan.s1006ProHExp_rep);
//            }
//            else
//            {
//                escrowItem = CreateEscrowItem("OtherTax", m_dataLoan.s1006ProHExp_rep);
//            }
//            if (escrowItem != null)
//            {
//                escrowItems.AddEscrowItem(escrowItem);
//                hasEntries = true;
//            }

//            if (isFreddieMac)
//            {
//                // Check if either of the tax dropdowns have 1009 selected.
//                // If it is not one of those, then check the insurance pages.

//                string type = "";
//                bool shouldHaveFailedAudit = false;

//                if (m_dataLoan.sTaxTable1008DescT == E_TableSettlementDescT.Use1009DescT)
//                {
//                    type = GetEscrowItemType(m_dataLoan.sTaxTable1008T, "default");
//                }
//                else if (m_dataLoan.sTaxTable1009DescT == E_TableSettlementDescT.Use1009DescT)
//                {
//                    type = GetEscrowItemType(m_dataLoan.sTaxTable1009T, "default");
//                }
//                else if (m_dataLoan.sWindInsSettlementChargeT == E_sInsSettlementChargeT.Line1009)
//                {
//                    type = "StormInsurance";
//                }
//                else if (m_dataLoan.sCondoHO6InsSettlementChargeT == E_sInsSettlementChargeT.Line1009)
//                {
//                    type = "HazardInsurance";
//                }
//                else
//                {
//                    if (m_dataLoan.s1007ProHExp > 0)
//                        shouldHaveFailedAudit = true;
//                }
                
//                if (type.Equals("default") || shouldHaveFailedAudit)
//                {
//                    string devMessage = "Failed to determine type of item. Should have failed audit.";
//                    throw new CBaseException(LendersOffice.Common.ErrorMessages.Generic, devMessage);
//                }
//                escrowItem = CreateEscrowItem(type, m_dataLoan.s1007ProHExp_rep);
//            }
//            else
//            {
//                escrowItem = CreateEscrowItem("OtherTax", m_dataLoan.s1007ProHExp_rep);
//            }
//            if (escrowItem != null)
//            {
//                escrowItems.AddEscrowItem(escrowItem);
//                hasEntries = true;
//            }

//            if (!hasEntries)
//                return null;

//            return escrowItems;
//        }

//        private XEEscrowItem CreateEscrowItem(string type, string amt)
//        {
//            if (m_dataLoan.m_convertLos.ToMoney(amt) == 0)
//                return null;

//            XEEscrowItem escrowItem = new XEEscrowItem();

//            escrowItem.SetEscrowItemDetail(CreateEscrowItemDetail(type, amt));

//            return escrowItem;
//        }

//        private XEEscrowItemDetail CreateEscrowItemDetail(string type, string amt)
//        {
//            XEEscrowItemDetail escrowItemDetail = new XEEscrowItemDetail();

//            escrowItemDetail.EscrowItemType = type;
//            escrowItemDetail.EscrowMonthlyPaymentAmount = amt;

//            return escrowItemDetail;
//        }

//        private XEAdjustment CreateAdjustmentCurrent()
//        {
//            XEAdjustment adjustment = new XEAdjustment();

//            adjustment.SetRateOrPaymentChangeOccurences(CreateRateOrPaymentChangeOccurencesCurrent());

//            return adjustment;
//        }

//        private XERateOrPaymentChangeOccurences CreateRateOrPaymentChangeOccurencesCurrent()
//        {
//            XERateOrPaymentChangeOccurences RateOrPaymentChangeOccurences = new XERateOrPaymentChangeOccurences();

//            RateOrPaymentChangeOccurences.SetRateOrPaymentChangeOccurence(CreateSetRateOrPaymentChangeOccurenceCurrent());

//            return RateOrPaymentChangeOccurences;
//        }

//        private XERateOrPaymentChangeOccurence CreateSetRateOrPaymentChangeOccurenceCurrent()
//        {
//            XERateOrPaymentChangeOccurence RateOrPaymentChangeOccurence = new XERateOrPaymentChangeOccurence();

//            if (m_dataLoan.sAmortTable != null && m_dataLoan.sAmortTable.nRows > 0)
//            {
//                decimal rate = m_dataLoan.sAmortTable.Items[0].Rate;
//                for (int i = 1; i < m_dataLoan.sAmortTable.nRows; i++)
//                {
//                    AmortItem item = m_dataLoan.sAmortTable.Items[i];
//                    if (item.Rate != rate && DateTime.Now < item.DueDate.DateTimeForComputation.AddMonths(-1))
//                    {
//                        RateOrPaymentChangeOccurence.NextRateAdjustmentEffectiveDate = m_dataLoan.m_convertLos.ToDateTimeString(item.DueDate.DateTimeForComputation.AddMonths(-1));
//                        break;
//                    }

//                    rate = item.Rate;
//                }
//            }

//            return RateOrPaymentChangeOccurence;
//        }

//        private XECombinedLtvs CreateCombinedLtvs()
//        {
//            XECombinedLtvs combinedLtvs = new XECombinedLtvs();

//            combinedLtvs.SetCombinedLtv(CreateCombinedLtv());

//            return combinedLtvs;
//        }

//        private XECombinedLtv CreateCombinedLtv()
//        {
//            XECombinedLtv combinedLtv = new XECombinedLtv();

//            combinedLtv.CombinedLTVRatioPercent = RoundLTVRatioPercent(m_dataLoan.sCltvR);

//            if (m_dataLoan.sLienPosT != E_sLienPosT.Second)
//            {
//                combinedLtv.HomeEquityCombinedLTVRatioPercent = RoundLTVRatioPercent(m_dataLoan.sHcltvR);
//            }

//            return combinedLtv;
//        }

//        private XELoan CreateLoanClosing()
//        {
//            XELoan loan = new XELoan();

//            loan.LoanRoleType = "SubjectLoan";

//            if (m_dataLoan.sFinMethT == E_sFinMethT.ARM)
//                loan.SetAdjustment(CreateAdjustment());

//            loan.SetAmortization(CreateAmortization());
//            loan.SetClosingInformation(CreateClosingInformation());
//            loan.SetDownPayments(CreateDownPayments());
//            loan.SetGovernmentLoan(CreateGovernmentLoan());
//            loan.SetHmdaLoan(CreateHmdaLoan());
//            loan.SetInterestCalculation(CreateInterestCalculation());
//            loan.SetInterestOnly(CreateInterestOnly());
//            loan.SetInvestorLoanInformation(CreateInvestorLoanInformation());
//            loan.SetLoanDetail(CreateLoanDetail());
//            //loan.SetLoanLevelCredit(CreateLoanLevelCredit());
//            loan.SetLoanState(CreateLoanState());
//            loan.SetLtv(CreateLtv());
//            loan.SetMaturity(CreateMaturity());
//            loan.SetPayment(CreatePayment());
//            loan.SetQualification(CreateQualification());
//            loan.SetRefinance(CreateRefinance());
//            loan.SetSelectedLoanProduct(CreateSelectedLoanProduct());
//            loan.SetTermsOfMortgage(CreateTermsOfMortgage());
//            loan.SetUnderwriting(CreateUnderwriting());
//            loan.SetFormSpecificContents(CreateFormSpecificContents());

//            return loan;
//        }

//        private XEFormSpecificContents CreateFormSpecificContents()
//        {
//            XEFormSpecificContents formSpecificContents = new XEFormSpecificContents();

//            formSpecificContents.SetFormSpecificContent(CreateFormSpecificContent());

//            return formSpecificContents;
//        }

//        private XEFormSpecificContent CreateFormSpecificContent()
//        {
//            XEFormSpecificContent formSpecificContent = new XEFormSpecificContent();

//            formSpecificContent.SetUrla(CreateUrla());

//            return formSpecificContent;
//        }

//        private XEUrla CreateUrla()
//        {
//            XEUrla urla = new XEUrla();

//            urla.SetUrlaDetail(CreateUrlaDetail());

//            return urla;
//        }

//        private XEUrlaDetail CreateUrlaDetail()
//        {
//            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sPurchPrice_rep) == 0.0M || m_dataLoan.sLienPosT == E_sLienPosT.Second)
//                return null;

//            XEUrlaDetail urlaDetail = new XEUrlaDetail();

//            urlaDetail.PurchasePriceAmount = TruncateMoneyAmount(m_dataLoan.sPurchPrice_rep);

//            return urlaDetail;
//        }

//        private XEUnderwriting CreateUnderwriting()
//        {
//            XEUnderwriting underwriting = new XEUnderwriting();

//            underwriting.SetAutomatedUnderwritings(CreateAutomatedUnderwritings());
//            underwriting.SetUnderwritingDetail(CreateUnderwritingDetail());

//            return underwriting;
//        }

//        private XEUnderwritingDetail CreateUnderwritingDetail()
//        {
//            XEUnderwritingDetail underwritingDetail = new XEUnderwritingDetail();

//            underwritingDetail.LoanManualUnderwritingIndicator = m_dataLoan.sIsManualUw.ToString().ToLower();

//            return underwritingDetail;
//        }

//        private XEAutomatedUnderwritings CreateAutomatedUnderwritings()
//        {
//            XEAutomatedUnderwritings automatedUnderwritings = new XEAutomatedUnderwritings();

//            automatedUnderwritings.SetAutomatedUnderwriting(CreateAutomatedUnderwriting());

//            return automatedUnderwritings;
//        }

//        private XEAutomatedUnderwriting CreateAutomatedUnderwriting()
//        {
//            XEAutomatedUnderwriting automatedUnderwriting = new XEAutomatedUnderwriting();

//            automatedUnderwriting.AutomatedUnderwritingCaseIdentifier = string.IsNullOrEmpty(m_dataLoan.sDuCaseId) ? m_dataLoan.sLpAusKey : m_dataLoan.sDuCaseId;

//            if (m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.NA
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.Total_ApproveEligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.Total_ApproveIneligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.Total_ReferEligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.Total_ReferIneligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_AcceptEligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_AcceptIneligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_ReferEligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_ReferIneligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible
//                && m_dataLoan.sProd3rdPartyUwResultT != E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible)
//            {
//                automatedUnderwriting.AutomatedUnderwritingRecommendationDescription = ToMismo(m_dataLoan.sProd3rdPartyUwResultT);
//            }

//            if (m_dataLoan.sIsDuUw)
//            {
//                automatedUnderwriting.AutomatedUnderwritingSystemType = E_AutomatedUnderwritingSystemType.DesktopUnderwriter;
//            }
//            else if (m_dataLoan.sIsLpUw)
//            {
//                automatedUnderwriting.AutomatedUnderwritingSystemType = E_AutomatedUnderwritingSystemType.LoanProspector;
//            }
//            else if (m_dataLoan.sIsOtherUw)
//            {
//                automatedUnderwriting.AutomatedUnderwritingSystemType = E_AutomatedUnderwritingSystemType.Other;
//            }

//            return automatedUnderwriting;
//        }

//        private XETermsOfMortgage CreateTermsOfMortgage()
//        {
//            XETermsOfMortgage termsOfMortgage = new XETermsOfMortgage();

//            termsOfMortgage.LienPriorityType = ToMismo(m_dataLoan.sLienPosT);
//            termsOfMortgage.MortgageType = ToMismo(m_dataLoan.sLT);
//            termsOfMortgage.NoteAmount = TruncateMoneyAmount(m_dataLoan.sFinalLAmt_rep);
//            termsOfMortgage.NoteDate = m_dataLoan.sDocumentNoteD_rep;
//            termsOfMortgage.NoteRatePercent = m_dataLoan.sNoteIR_rep;

//            if (m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase || m_dataLoan.sLPurposeT == E_sLPurposeT.Refin
//                || m_dataLoan.sLPurposeT == E_sLPurposeT.Construct || m_dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm
//                || m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance || m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity
//                || m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout || m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl)
//                termsOfMortgage.LoanPurposeType = ToMismo(m_dataLoan.sLPurposeT);

//            return termsOfMortgage;
//        }

//        private XESelectedLoanProduct CreateSelectedLoanProduct()
//        {
//            XESelectedLoanProduct selectedLoanProduct = new XESelectedLoanProduct();

//            //selectedLoanProduct.SetLoanProductDetail(CreateLoanProductDetail());
//            selectedLoanProduct.SetPriceLocks(CreatePriceLocks());

//            return selectedLoanProduct;
//        }

//        private XEPriceLock CreatePriceLock()
//        {
//            XEPriceLock priceLock = new XEPriceLock();

//            priceLock.PriceLockDatetime = m_dataLoan.sRLckdD_rep;

//            return priceLock;
//        }

//        private XEPriceLocks CreatePriceLocks()
//        {
//            XEPriceLocks priceLocks = new XEPriceLocks();

//            priceLocks.SetPriceLock(CreatePriceLock());

//            return priceLocks;
//        }

//        private XERefinance CreateRefinance()
//        {
//            XERefinance refinance = new XERefinance();

//            if (m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.CashOutDebtConsolidation
//                || m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.CashOutHomeImprovement
//                || m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.CashOutOther)
//            {
//                refinance.RefinanceCashOutDeterminationType = E_RefinanceCashOutDeterminationType.CashOut;
//            }
//            else if (m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutFHAStreamlinedRefinance
//                || m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutFREOwnedRefinance
//                || m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutOther
//                || m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.NoCashOutStreamlinedRefinance
//                || (m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.ChangeInRateTerm && m_dataLoan.sLT != E_sLT.Conventional))
//            {
//                refinance.RefinanceCashOutDeterminationType = E_RefinanceCashOutDeterminationType.NoCashOut;
//            }
//            else if(m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.CashOutLimited
//                || (m_dataLoan.sGseRefPurposeT == E_sGseRefPurposeT.ChangeInRateTerm && m_dataLoan.sLT == E_sLT.Conventional))
//            {
//                refinance.RefinanceCashOutDeterminationType = E_RefinanceCashOutDeterminationType.LimitedCashOut;
//            }

//            return refinance;
//        }

//        private XEPayment CreatePayment()
//        {
//            XEPayment payment = new XEPayment();

//            payment.SetPaymentRule(CreatePaymentRule());

//            return payment;
//        }

//        private XEPaymentRule CreatePaymentRule()
//        {
//            XEPaymentRule paymentRule = new XEPaymentRule();

//            paymentRule.InitialPrincipalAndInterestPaymentAmount = m_dataLoan.sProThisMPmt_rep;
//            paymentRule.PaymentFrequencyType = "Monthly";
//            paymentRule.ScheduledFirstPaymentDate = m_dataLoan.sSchedDueD1_rep;

//            return paymentRule;
//        }

//        private XEMaturity CreateMaturity()
//        {
//            XEMaturity maturity = new XEMaturity();

//            maturity.SetMaturityRule(CreateMaturityRule());

//            return maturity;
//        }

//        private XEQualification CreateQualification()
//        {
//            XEQualification qualification = new XEQualification();

//            qualification.TotalLiabilitiesMonthlyPaymentAmount = TruncateMoneyAmount(m_dataApp.aTransmTotMonPmt_rep);
//            qualification.TotalMonthlyIncomeAmount = TruncateMoneyAmount(m_dataLoan.sLTotI_rep);
//            qualification.TotalMonthlyProposedHousingExpenseAmount = TruncateMoneyAmount(m_dataLoan.sMonthlyPmt_rep);

//            return qualification;
//        }

//        private XEMaturityRule CreateMaturityRule()
//        {
//            XEMaturityRule maturityRule = new XEMaturityRule();

//            maturityRule.LoanMaturityDate = m_dataLoan.m_convertLos.ToDateTimeString(m_dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(m_dataLoan.sDue - 1));
//            maturityRule.LoanMaturityPeriodCount = m_dataLoan.sDue_rep;
//            maturityRule.LoanMaturityPeriodType = "Month";

//            return maturityRule;
//        }

//        private XELtv CreateLtv()
//        {
//            XELtv ltv = new XELtv();

//            ltv.BaseLTVRatioPercent = RoundLTVRatioPercent(m_dataLoan.sLAmt1003 / m_dataLoan.sHouseVal * 100);

//            if (m_dataLoan.sHouseVal != 0)
//            {
//                ltv.LTVRatioPercent = RoundLTVRatioPercent(m_dataLoan.sFinalLAmt / m_dataLoan.sHouseVal * 100);
//            }

//            return ltv;
//        }

//        private XELoanState CreateLoanState()
//        {
//            XELoanState loanState = new XELoanState();

//            loanState.LoanStateDate = m_dataLoan.sDocumentNoteD_rep;
//            loanState.LoanStateType = "AtClosing";

//            return loanState;
//        }

//        private XELoanLevelCredit CreateLoanLevelCredit()
//        {
//            XELoanLevelCredit loanLevelCredit = new XELoanLevelCredit();

//            if (string.IsNullOrEmpty(m_dataLoan.sCreditScoreType2Soft_rep))
//                return null;

//            loanLevelCredit.SetLoanLevelCreditDetail(CreateLoanLevelCreditDetail());

//            return loanLevelCredit;
//        }

//        private XELoanLevelCreditDetail CreateLoanLevelCreditDetail()
//        {
//            XELoanLevelCreditDetail loanLevelCreditDetail = new XELoanLevelCreditDetail();

//            loanLevelCreditDetail.LoanLevelCreditScoreSelectionMethodType = "MiddleOrLowerThenLowest";
//            loanLevelCreditDetail.LoanLevelCreditScoreValue = m_dataLoan.sCreditScoreType2Soft_rep;

//            return loanLevelCreditDetail;
//        }

//        private XEInterestCalculation CreateInterestCalculation()
//        {
//            XEInterestCalculation interestCalculation = new XEInterestCalculation();

//            interestCalculation.SetInterestCalculationRules(CreateInterestCalculationRules());

//            return interestCalculation;
//        }

//        private XEInterestCalculationRules CreateInterestCalculationRules()
//        {
//            XEInterestCalculationRules interestCalculationRules = new XEInterestCalculationRules();
//            interestCalculationRules.SetInterestCalculationRule(CreateInterestCalculationRule());
//            return interestCalculationRules;
//        }

//        private XEInterestCalculationRule CreateInterestCalculationRule()
//        {
//            XEInterestCalculationRule interestCalculationRule = new XEInterestCalculationRule();
//            interestCalculationRule.InterestCalculationType = "Compound";
//            interestCalculationRule.LoanInterestAccrualStartDate = m_dataLoan.sFundD_rep;
//            interestCalculationRule.InterestCalculationPeriodType = "Month";
//            return interestCalculationRule;
//        }

//        private XEInterestOnly CreateInterestOnly()
//        {
//            if (m_dataLoan.sIOnlyMon == 0)
//                return null;

//            XEInterestOnly interestOnly = new XEInterestOnly();
//            interestOnly.InterestOnlyEndDate = m_dataLoan.m_convertLos.ToDateTimeString(m_dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(m_dataLoan.sIOnlyMon - 1));
//            return interestOnly;
//        }

//        private XEInvestorLoanInformation CreateInvestorLoanInformation()
//        {
//            XEInvestorLoanInformation investorLoanInformation = new XEInvestorLoanInformation();
//            investorLoanInformation.RelatedInvestorLoanIdentifier = m_dataLoan.sSpGseLoanNumCurrentLoan;

//            if (m_dataLoan.sSpInvestorCurrentLoanT == E_sSpInvestorCurrentLoanT.FannieMae)
//            {
//                investorLoanInformation.RelatedLoanInvestorType = "FNM";
//            }
//            else if (m_dataLoan.sSpInvestorCurrentLoanT == E_sSpInvestorCurrentLoanT.FreddieMac)
//            {
//                investorLoanInformation.RelatedLoanInvestorType = "FRE";
//            }
//            else
//            {
//                if (string.IsNullOrEmpty(m_dataLoan.sSpGseLoanNumCurrentLoan))
//                    return null;
//            }

//            return investorLoanInformation;
//        }

//        private XELoanDetail CreateLoanDetail()
//        {
//            XELoanDetail loanDetail = new XELoanDetail();

//            loanDetail.ApplicationReceivedDate = m_dataLoan.sAppSubmittedD_rep;
//            loanDetail.BalloonIndicator = (m_dataLoan.sTerm > m_dataLoan.sDue).ToString().ToLower();

//            int borrowerCount = 0;
//            for( int i = 0; i < m_dataLoan.nApps; i++)
//            {
//                CAppData tempApp = m_dataLoan.GetAppData(i);
//                if (tempApp.aBIsValidNameSsn)
//                    borrowerCount++;
                
//                if (tempApp.aCIsValidNameSsn)
//                    borrowerCount++;
//            }
//            loanDetail.BorrowerCount = borrowerCount.ToString();

//            loanDetail.BuydownTemporarySubsidyIndicator = 
//                (m_dataLoan.sBuydwnR1 != 0 || m_dataLoan.sBuydwnMon1 != 0 ||
//                m_dataLoan.sBuydwnR2 != 0 || m_dataLoan.sBuydwnMon2 != 0 ||
//                m_dataLoan.sBuydwnR3 != 0 || m_dataLoan.sBuydwnMon3 != 0 ||
//                m_dataLoan.sBuydwnR4 != 0 || m_dataLoan.sBuydwnMon4 != 0 ||
//                m_dataLoan.sBuydwnR5 != 0 || m_dataLoan.sBuydwnMon5 != 0).ToString().ToLower();

//            if (m_dataLoan.sAssumeLT != E_sAssumeLT.LeaveBlank)
//                loanDetail.AssumabilityIndicator = (m_dataLoan.sAssumeLT != E_sAssumeLT.MayNot).ToString().ToLower();

//            loanDetail.CapitalizedLoanIndicator = m_dataLoan.sIsOptionArm.ToString().ToLower();
//            loanDetail.ConstructionLoanIndicator = "false";
//            loanDetail.ConvertibleIndicator = "false";
//            loanDetail.EscrowIndicator = m_dataLoan.sWillEscrowBeWaived.ToString().ToLower();
//            loanDetail.InitialFixedPeriodEffectiveMonthsCount = m_dataLoan.sRAdj1stCapMon_rep;
//            loanDetail.InterestOnlyIndicator = (m_dataLoan.sIOnlyMon > 0).ToString().ToLower();
//            loanDetail.LoanAffordableIndicator = (m_dataLoan.sIsCommunityLending || m_dataLoan.sIsCommLen).ToString().ToLower();

//            if (m_dataLoan.sPrepmtPenaltyT != E_sPrepmtPenaltyT.LeaveBlank)
//                loanDetail.PrepaymentPenaltyIndicator = (m_dataLoan.sPrepmtPenaltyT == E_sPrepmtPenaltyT.May).ToString().ToLower();
            
//            loanDetail.RelocationLoanIndicator = "false";
//            loanDetail.SharedEquityIndicator = "false";

//            return loanDetail;
//        }

//        private XEHmdaLoan CreateHmdaLoan()
//        {
//            XEHmdaLoan hmdaLoan = new XEHmdaLoan();

//            hmdaLoan.HMDA_HOEPALoanStatusIndicator = m_dataLoan.sHmdaReportAsHoepaLoan.ToString().ToLower();
//            hmdaLoan.HMDARateSpreadPercent = FormatHMDARateSpreadPercent(m_dataLoan.sHmdaAprRateSpread);

//            return hmdaLoan;
//        }

//        private XEAmortization CreateAmortization()
//        {
//            XEAmortization amortization = new XEAmortization();

//            amortization.SetAmortizationRule(CreateAmortizationRule());

//            return amortization;
//        }

//        private XEAmortizationRule CreateAmortizationRule()
//        {
//            XEAmortizationRule amortizationRule = new XEAmortizationRule();

//            amortizationRule.LoanAmortizationPeriodCount = m_dataLoan.sTerm_rep;
//            amortizationRule.LoanAmortizationPeriodType = "Month";
//            amortizationRule.LoanAmortizationType = ToMismo(m_dataLoan.sFinMethT);

//            return amortizationRule;
//        }

//        private XEClosingInformation CreateClosingInformation()
//        {
//            XEClosingInformation closingInformation = new XEClosingInformation();
//            XEClosingCostFunds closingCostFunds = CreateClosingCostFunds();
//            XECollectedOtherFunds collectedOtherFunds = CreateCollectedOtherFunds();

//            if (closingCostFunds == null && collectedOtherFunds == null)
//                return null;

//            closingInformation.SetClosingCostFunds(closingCostFunds);
//            closingInformation.SetCollectedOtherFunds(collectedOtherFunds);

//            return closingInformation;
//        }

//        private XECollectedOtherFunds CreateCollectedOtherFunds()
//        {
//            XECollectedOtherFunds collectedOtherFunds = new XECollectedOtherFunds();

//            if (m_dataLoan.m_convertLos.ToDecimal(m_dataLoan.sInitialDeposit_Escrow_rep) == 0)
//                return null;

//            collectedOtherFunds.SetCollectedOtherFund(CreateCollectedOtherFund());

//            return collectedOtherFunds;
//        }

//        private XECollectedOtherFund CreateCollectedOtherFund()
//        {
//            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sInitialDeposit_Escrow_rep) == 0.0M)
//                return null;

//            XECollectedOtherFund collectedOtherFund = new XECollectedOtherFund();

//            collectedOtherFund.OtherFundsCollectedAtClosingAmount = m_dataLoan.sInitialDeposit_Escrow_rep;
//            collectedOtherFund.OtherFundsCollectedAtClosingType = "EscrowFunds";

//            return collectedOtherFund;
//        }

//        private XEClosingCostFunds CreateClosingCostFunds()
//        {
//            XEClosingCostFunds closingCostFunds = new XEClosingCostFunds();

//            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sCommunityLendingClosingCost1Amt_rep) != 0
//                || m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sCommunityLendingClosingCost2Amt_rep) != 0
//                || m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sCommunityLendingClosingCost3Amt_rep) != 0
//                || m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sCommunityLendingClosingCost4Amt_rep) != 0)
//            {
//                closingCostFunds.AddClosingCostFund(CreateClosingCostFund(m_dataLoan.sCommunityLendingClosingCost1T, m_dataLoan.sCommunityLendingClosingCostSource1T, m_dataLoan.sCommunityLendingClosingCost1Amt_rep));
//                closingCostFunds.AddClosingCostFund(CreateClosingCostFund(m_dataLoan.sCommunityLendingClosingCost2T, m_dataLoan.sCommunityLendingClosingCostSource2T, m_dataLoan.sCommunityLendingClosingCost2Amt_rep));
//                closingCostFunds.AddClosingCostFund(CreateClosingCostFund(m_dataLoan.sCommunityLendingClosingCost3T, m_dataLoan.sCommunityLendingClosingCostSource3T, m_dataLoan.sCommunityLendingClosingCost3Amt_rep));
//                closingCostFunds.AddClosingCostFund(CreateClosingCostFund(m_dataLoan.sCommunityLendingClosingCost4T, m_dataLoan.sCommunityLendingClosingCostSource4T, m_dataLoan.sCommunityLendingClosingCost4Amt_rep));
//            }
//            else
//            {
//                return null;
//            }

//            return closingCostFunds;
//        }

//        private XEClosingCostFund CreateClosingCostFund(E_sCommunityLendingClosingCostT costT, E_sCommunityLendingClosingCostSourceT sourceT, string amt)
//        {
//            if (m_dataLoan.m_convertLos.ToMoney(amt) == 0)
//                return null;

//            XEClosingCostFund closingCostFund = new XEClosingCostFund();
//            closingCostFund.ClosingCostContributionAmount = amt;
//            if (costT != E_sCommunityLendingClosingCostT.LeaveBlank)
//            {
//                if (costT == E_sCommunityLendingClosingCostT.SecondaryHELOC
//                    || costT == E_sCommunityLendingClosingCostT.SecondaryClosedEnd
//                    || costT == E_sCommunityLendingClosingCostT.AggregatedRemainingTypes)
//                {
//                    closingCostFund.ClosingCostFundsTypeOtherDescription = ToMismo(costT);
//                    closingCostFund.ClosingCostFundsType = E_ClosingCostFundsType.Other;
//                }
//                else
//                {
//                    closingCostFund.ClosingCostFundsType = ToMismo(costT);
//                }
//            }
//            if (sourceT != E_sCommunityLendingClosingCostSourceT.LeaveBlank)
//            {
//                if (sourceT == E_sCommunityLendingClosingCostSourceT.UsdaRuralHousing
//                    || sourceT == E_sCommunityLendingClosingCostSourceT.FHLBAffordableHousingProgram
//                    || sourceT == E_sCommunityLendingClosingCostSourceT.AggregatedRemainingTypes)
//                {
//                    closingCostFund.ClosingCostSourceTypeOtherDescription = ToMismo(sourceT);
//                    closingCostFund.ClosingCostSourceType = E_ClosingCostSourceType.Other;
//                }
//                else
//                {
//                    closingCostFund.ClosingCostSourceType = ToMismo(sourceT);
//                }
//            }
            
//            return closingCostFund;
//        }

//        private XEDownPayments CreateDownPayments()
//        {
//            XEDownPayments downPayments = new XEDownPayments();

//            if (m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sCommunityLendingDownPaymentSource1Amt_rep) != 0
//                || m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sCommunityLendingDownPaymentSource2Amt_rep) != 0
//                || m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sCommunityLendingDownPaymentSource3Amt_rep) != 0
//                || m_dataLoan.m_convertLos.ToMoney(m_dataLoan.sCommunityLendingDownPaymentSource4Amt_rep) != 0)
//            {
//                downPayments.AddDownPayment(CreateDownPayment(m_dataLoan.sCommunityLendingDownPayment1T, m_dataLoan.sCommunityLendingDownPaymentSource1T, m_dataLoan.sCommunityLendingDownPaymentSource1Amt_rep));
//                downPayments.AddDownPayment(CreateDownPayment(m_dataLoan.sCommunityLendingDownPayment2T, m_dataLoan.sCommunityLendingDownPaymentSource2T, m_dataLoan.sCommunityLendingDownPaymentSource2Amt_rep));
//                downPayments.AddDownPayment(CreateDownPayment(m_dataLoan.sCommunityLendingDownPayment3T, m_dataLoan.sCommunityLendingDownPaymentSource3T, m_dataLoan.sCommunityLendingDownPaymentSource3Amt_rep));
//                downPayments.AddDownPayment(CreateDownPayment(m_dataLoan.sCommunityLendingDownPayment4T, m_dataLoan.sCommunityLendingDownPaymentSource4T, m_dataLoan.sCommunityLendingDownPaymentSource4Amt_rep));
//            }
//            else
//            {
//                return null;
//            }
            
//            return downPayments;
//        }

//        private XEDownPayment CreateDownPayment(E_sCommunityLendingDownPaymentT paymentT, E_sCommunityLendingDownPaymentSourceT sourceT, string amt)
//        {
//            if (m_dataLoan.m_convertLos.ToMoney(amt) == 0)
//                return null;

//            XEDownPayment downPayment = new XEDownPayment();
//            downPayment.DownPaymentAmount = amt;
//            if (paymentT != E_sCommunityLendingDownPaymentT.LeaveBlank)
//            {
//                if (paymentT == E_sCommunityLendingDownPaymentT.SecondaryFinancingClosedEnd
//                    || paymentT == E_sCommunityLendingDownPaymentT.SecondaryFinancingHELOC
//                    || paymentT == E_sCommunityLendingDownPaymentT.AggregatedRemainingTypes)
//                {
//                    downPayment.DownPaymentTypeOtherDescription = ToMismo(paymentT);
//                    downPayment.DownPaymentType = E_DownPaymentType.OtherTypeOfDownPayment;
//                }
//                else
//                {
//                    downPayment.DownPaymentType = ToMismo(paymentT);
//                }
//            }
//            if (sourceT != E_sCommunityLendingDownPaymentSourceT.LeaveBlank)
//            {
//                if (sourceT == E_sCommunityLendingDownPaymentSourceT.UsdaRuralHousing
//                    || sourceT == E_sCommunityLendingDownPaymentSourceT.FHLBAffordableHousingProgram
//                    || sourceT == E_sCommunityLendingDownPaymentSourceT.AggregatedRemainingTypes)
//                {
//                    downPayment.DownPaymentSourceTypeOtherDescription = ToMismo(sourceT);
//                    downPayment.DownPaymentSourceType = E_DownPaymentSourceType.Other;
//                }
//                else
//                {
//                    downPayment.DownPaymentSourceType = ToMismo(sourceT);
//                }
//            }

//            return downPayment;
//        }

//        private XEGovernmentLoan CreateGovernmentLoan()
//        {
//            if (string.IsNullOrEmpty(m_dataLoan.sFHAHousingActSection))
//                return null;

//            XEGovernmentLoan governmentLoan = new XEGovernmentLoan();

//            Regex rgx = new Regex("[^a-zA-Z0-9]");
//            governmentLoan.SectionOfActType = rgx.Replace(m_dataLoan.sFHAHousingActSection, "").ToUpper();

//            return governmentLoan;
//        }

//        private XEAdjustment CreateAdjustment()
//        {
//            XEAdjustment adjustment = new XEAdjustment();

//            adjustment.SetInterestRateAdjustment(CreateInterestRateAdjustment());
//            //adjustment.SetRateOrPaymentChangeOccurences(CreateRateOrPaymentChangeOccurences());

//            return adjustment;
//        }

//        private XEInterestRateAdjustment CreateInterestRateAdjustment()
//        {
//            XEInterestRateAdjustment interestRateAdjustment = new XEInterestRateAdjustment();

//            interestRateAdjustment.SetIndexRules(CreateIndexRules());
//            interestRateAdjustment.SetInterestRateLifetimeAdjustmentRule(CreateInterestRateLifetimeAdjustmentRule());
//            interestRateAdjustment.SetInterestRatePerChangeAdjustmentRules(CreateInterestRatePerChangeAdjustmentRules());

//            return interestRateAdjustment;
//        }

//        private XEInterestRatePerChangeAdjustmentRules CreateInterestRatePerChangeAdjustmentRules()
//        {
//            XEInterestRatePerChangeAdjustmentRules interestRatePerChangeAdjustmentRules = new XEInterestRatePerChangeAdjustmentRules();

//            XEInterestRatePerChangeAdjustmentRule interestRatePerChangeAdjustmentRule = new XEInterestRatePerChangeAdjustmentRule();
//            interestRatePerChangeAdjustmentRule.AdjustmentRuleType = "First";
//            interestRatePerChangeAdjustmentRule.PerChangeMaximumDecreaseRatePercent = m_dataLoan.sRAdj1stCapR_rep;
//            interestRatePerChangeAdjustmentRule.PerChangeMaximumIncreaseRatePercent = m_dataLoan.sRAdj1stCapR_rep;
//            interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentEffectiveDate = m_dataLoan.m_convertLos.ToDateTimeString(m_dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(m_dataLoan.sRAdj1stCapMon - 1));
//            interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentFrequencyMonthsCount = m_dataLoan.sRAdjCapMon_rep;
//            interestRatePerChangeAdjustmentRules.AddInterestRatePerChangeAdjustmentRule(interestRatePerChangeAdjustmentRule);

//            interestRatePerChangeAdjustmentRule = new XEInterestRatePerChangeAdjustmentRule();
//            interestRatePerChangeAdjustmentRule.AdjustmentRuleType = "Subsequent";
//            interestRatePerChangeAdjustmentRule.PerChangeMaximumDecreaseRatePercent = m_dataLoan.sRAdjCapR_rep;
//            interestRatePerChangeAdjustmentRule.PerChangeMaximumIncreaseRatePercent = m_dataLoan.sRAdjCapR_rep;
//            interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentEffectiveDate = m_dataLoan.m_convertLos.ToDateTimeString(m_dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(m_dataLoan.sRAdj1stCapMon - 1 + m_dataLoan.sRAdjCapMon));
//            interestRatePerChangeAdjustmentRule.PerChangeRateAdjustmentFrequencyMonthsCount = m_dataLoan.sRAdjCapMon_rep;
//            interestRatePerChangeAdjustmentRules.AddInterestRatePerChangeAdjustmentRule(interestRatePerChangeAdjustmentRule);

//            return interestRatePerChangeAdjustmentRules;
//        }

//        private XEInterestRateLifetimeAdjustmentRule CreateInterestRateLifetimeAdjustmentRule()
//        {
//            XEInterestRateLifetimeAdjustmentRule interestRateLifetimeAdjustmentRule = new XEInterestRateLifetimeAdjustmentRule();

//            interestRateLifetimeAdjustmentRule.CeilingRatePercent = m_dataLoan.m_convertLos.ToRateString(m_dataLoan.sRAdjLifeCapR + m_dataLoan.sNoteIR);
//            interestRateLifetimeAdjustmentRule.FirstRateChangePaymentEffectiveDate = m_dataLoan.m_convertLos.ToDateTimeString(m_dataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(m_dataLoan.sRAdj1stCapMon));
//            interestRateLifetimeAdjustmentRule.FloorRatePercent = m_dataLoan.sRAdjFloorR_rep;
//            interestRateLifetimeAdjustmentRule.InterestRateRoundingPercent = m_dataLoan.sRAdjRoundToR_rep;
//            interestRateLifetimeAdjustmentRule.InterestRateRoundingType = ToMismo(m_dataLoan.sRAdjRoundT);
//            interestRateLifetimeAdjustmentRule.MarginRatePercent = m_dataLoan.sRAdjMarginR_rep;

//            return interestRateLifetimeAdjustmentRule;
//        }

//        private XEIndexRules CreateIndexRules()
//        {
//            XEIndexRules indexRules = new XEIndexRules();

//            indexRules.SetIndexRule(CreateIndexRule());

//            return indexRules;
//        }

//        private XEIndexRule CreateIndexRule()
//        {
//            XEIndexRule indexRule = new XEIndexRule();
//            if (E_sGseDeliveryTargetT.GinnieMae == m_dataLoan.sGseDeliveryTargetT)
//            {
//                throw new NotSupportedException(LendersOffice.Common.ErrorMessages.ULDD.GSeTargetGinnieMaeExportNotAvailable);
//            }
//            if (E_sGseDeliveryTargetT.Other_None == m_dataLoan.sGseDeliveryTargetT)
//            {
//                throw new NotSupportedException(LendersOffice.Common.ErrorMessages.ULDD.GseTargetOtherExportNotAvailable);
//            }
            
//            if (E_sGseDeliveryTargetT.FannieMae == m_dataLoan.sGseDeliveryTargetT)
//            {
//                indexRule.IndexSourceType = "Other";
//            }
//            indexRule.InterestAndPaymentAdjustmentIndexLeadDaysCount = m_dataLoan.sArmIndexLeadDays_rep;

//            if (E_sGseDeliveryTargetT.FannieMae == m_dataLoan.sGseDeliveryTargetT)
//            {
//                if (m_dataLoan.sArmIndexT != E_sArmIndexT.LeaveBlank)
//                {
//                    if (m_dataLoan.sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && m_dataLoan.sRAdjCapMon == 6)
//                        indexRule.IndexSourceTypeOtherDescription = "6MonthTreasuryConstantMaturitiesWeeklyAverage";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && m_dataLoan.sRAdjCapMon == 36)
//                        indexRule.IndexSourceTypeOtherDescription = "3YearTreasuryConstantMaturitiesWeeklyAverage";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && m_dataLoan.sRAdjCapMon == 36)
//                        indexRule.IndexSourceTypeOtherDescription = "3YearTreasuryConstantMaturitiesMonthlyAverage";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && m_dataLoan.sRAdjCapMon == 60)
//                        indexRule.IndexSourceTypeOtherDescription = "5YearTreasuryConstantMaturitiesWeeklyAverage";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && m_dataLoan.sRAdjCapMon == 60)
//                        indexRule.IndexSourceTypeOtherDescription = "5YearTreasuryConstantMaturitiesMonthlyAverage";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.WeeklyAvgCMT && m_dataLoan.sRAdjCapMon == 12)
//                        indexRule.IndexSourceTypeOtherDescription = "1YearTreasuryConstantMaturitiesWeeklyAverage";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.MonthlyAvgCMT && m_dataLoan.sRAdjCapMon == 12)
//                        indexRule.IndexSourceTypeOtherDescription = "1YearTreasuryConstantMaturitiesMonthlyAverage";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.NationalMonthlyMedianCostOfFunds)
//                        indexRule.IndexSourceTypeOtherDescription = "NationalMonthlyMedianCostOfFundsRateMonthlyAverage";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && m_dataLoan.sRAdjCapMon == 6)
//                        indexRule.IndexSourceTypeOtherDescription = "6MonthWallStreetJournalLIBORRateDaily";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && m_dataLoan.sRAdjCapMon == 3)
//                        indexRule.IndexSourceTypeOtherDescription = "3MonthWallStreetJournalLIBORrateDaily";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && m_dataLoan.sRAdjCapMon == 1)
//                        indexRule.IndexSourceTypeOtherDescription = "1MonthWallStreetJournalLIBORRateDaily";
//                    else if (m_dataLoan.sArmIndexT == E_sArmIndexT.WallStreetJournalLIBOR && m_dataLoan.sRAdjCapMon == 12)
//                        indexRule.IndexSourceTypeOtherDescription = "1YearWallStreetJournalLIBORRateDaily";
//                }
//                else
//                {
//                    if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.SixMonthTreasury && m_dataLoan.sRAdjCapMon == 6)
//                        indexRule.IndexSourceTypeOtherDescription = "6MonthTreasuryConstantMaturitiesWeeklyAverage";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.ThreeYearTreasury && m_dataLoan.sRAdjCapMon == 36)
//                        indexRule.IndexSourceTypeOtherDescription = "3YearTreasuryConstantMaturitiesWeeklyAverage";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.ThreeYearTreasury && m_dataLoan.sRAdjCapMon == 36)
//                        indexRule.IndexSourceTypeOtherDescription = "3YearTreasuryConstantMaturitiesMonthlyAverage";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.OneYearTreasury && m_dataLoan.sRAdjCapMon == 12)
//                        indexRule.IndexSourceTypeOtherDescription = "1YearTreasuryConstantMaturitiesWeeklyAverage";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.OneYearTreasury && m_dataLoan.sRAdjCapMon == 12)
//                        indexRule.IndexSourceTypeOtherDescription = "1YearTreasuryConstantMaturitiesMonthlyAverage";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds)
//                        indexRule.IndexSourceTypeOtherDescription = "NationalMonthlyMedianCostOfFundsRateMonthlyAverage";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && m_dataLoan.sRAdjCapMon == 6)
//                        indexRule.IndexSourceTypeOtherDescription = "6MonthWallStreetJournalLIBORRateDaily";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && m_dataLoan.sRAdjCapMon == 3)
//                        indexRule.IndexSourceTypeOtherDescription = "3MonthWallStreetJournalLIBORrateDaily";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && m_dataLoan.sRAdjCapMon == 1)
//                        indexRule.IndexSourceTypeOtherDescription = "1MonthWallStreetJournalLIBORRateDaily";
//                    else if (m_dataLoan.sFreddieArmIndexT == E_sFreddieArmIndexT.LIBOR && m_dataLoan.sRAdjCapMon == 12)
//                        indexRule.IndexSourceTypeOtherDescription = "1YearWallStreetJournalLIBORRateDaily";
//                }
//            }
//            else if (E_sGseDeliveryTargetT.FreddieMac == m_dataLoan.sGseDeliveryTargetT) // 8/23/2012 SK opm 89891
//            {
//                bool useArmIndexT = false;
//                switch (m_dataLoan.sFreddieArmIndexT)
//                {
//                    case E_sFreddieArmIndexT.LeaveBlank:
//                        useArmIndexT = true;
//                        break;
//                    case E_sFreddieArmIndexT.OneYearTreasury:
//                        indexRule.IndexSourceType = "WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15";
//                        break;
//                    case E_sFreddieArmIndexT.ThreeYearTreasury:
//                        indexRule.IndexSourceType = "WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15";
//                        break;
//                    case E_sFreddieArmIndexT.SixMonthTreasury:
//                    case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds:
//                    case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds:
//                        break;
//                    case E_sFreddieArmIndexT.LIBOR:
//                        switch (m_dataLoan.sRAdjCapMon)
//                        {
//                            case 6:
//                                indexRule.IndexSourceType = "SixMonthLIBOR_WSJDaily";
//                                break;
//                            case 12:
//                                indexRule.IndexSourceType = "LIBOROneYearWSJDaily";
//                                break;
//                            default:
//                                break;
//                        }
//                        break;
//                    case E_sFreddieArmIndexT.Other:
//                        break;
//                }
//                if (false == useArmIndexT)
//                {
//                    return indexRule;
//                }
//                switch (m_dataLoan.sArmIndexT)
//                {
//                    case E_sArmIndexT.LeaveBlank:
//                    case E_sArmIndexT.MonthlyAvgCMT:
//                        break;
//                    case E_sArmIndexT.WeeklyAvgCMT:
//                        switch(m_dataLoan.sRAdjCapMon)
//                        {
//                            case 36:
//                                indexRule.IndexSourceType = "WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15";
//                                break;
//                            case 60:
//                                indexRule.IndexSourceType = "WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15";
//                                break;
//                            case 12:
//                                indexRule.IndexSourceType = "WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15";
//                                break;
//                            default:
//                                break;
//                        }
//                        break;
//                    case E_sArmIndexT.WeeklyAvgTAAI:
//                    case E_sArmIndexT.WeeklyAvgTAABD:
//                    case E_sArmIndexT.WeeklyAvgSMTI:
//                    case E_sArmIndexT.DailyCDRate:
//                    case E_sArmIndexT.WeeklyAvgCDRate:
//                    case E_sArmIndexT.WeeklyAvgPrimeRate:
//                    case E_sArmIndexT.TBillDailyValue:
//                    case E_sArmIndexT.EleventhDistrictCOF:
//                    case E_sArmIndexT.NationalMonthlyMedianCostOfFunds:
//                        break;
//                    case E_sArmIndexT.WallStreetJournalLIBOR:
//                        switch (m_dataLoan.sRAdjCapMon)
//                        {
//                            case 6:
//                                indexRule.IndexSourceType = "SixMonthLIBOR_WSJDaily";
//                                break;
//                            case 12:
//                                indexRule.IndexSourceType = "LIBOROneYearWSJDaily";
//                                break;
//                            default:
//                                break;
//                        }
//                        break;
//                    case E_sArmIndexT.FannieMaeLIBOR:
//                    default:
//                        break;
//                }
//            }

//            return indexRule;
//        }

//        private XECollaterals CreateCollaterals()
//        {
//            XECollaterals collaterals = new XECollaterals();

//            collaterals.SetCollateral(CreateCollateral());

//            return collaterals;
//        }

//        private XECollateral CreateCollateral()
//        {
//            XECollateral collateral = new XECollateral();

//            collateral.SetProperties(CreateProperties());

//            return collateral;
//        }

//        private XEProperties CreateProperties()
//        {
//            XEProperties properties = new XEProperties();

//            properties.SetProperty(CreateProperty());

//            return properties;
//        }

//        private XEProperty CreateProperty()
//        {
//            XEProperty property = new XEProperty();

//            property.SetAddress(CreateAddress());
//            property.SetProject(CreateProject());
//            property.SetFloodDetermination(CreateFloodDetermination());
//            property.SetPropertyDetail(CreatePropertyDetail());
//            property.SetPropertyUnits(CreatePropertyUnits());
//            property.SetPropertyValuations(CreatePropertyValuations());

//            return property;
//        }

//        private XEFloodDetermination CreateFloodDetermination()
//        {
//            XEFloodDetermination floodDetermination = new XEFloodDetermination();

//            floodDetermination.SetFloodDeterminationDetail(CreateFloodDeterminationDetail());

//            return floodDetermination;
//        }

//        private XEFloodDeterminationDetail CreateFloodDeterminationDetail()
//        {
//            XEFloodDeterminationDetail floodDeterminationDetail = new XEFloodDeterminationDetail();

//            floodDeterminationDetail.SpecialFloodHazardAreaIndicator = (m_dataLoan.sFloodHazardBuilding || m_dataLoan.sFloodHazardMobileHome).ToString().ToLower();

//            return floodDeterminationDetail;
//        }

//        private XEPropertyValuations CreatePropertyValuations()
//        {
//            XEPropertyValuations propertyValuations = new XEPropertyValuations();

//            propertyValuations.SetPropertyValuation(CreatePropertyValuation());

//            return propertyValuations;
//        }

//        private XEPropertyValuation CreatePropertyValuation()
//        {
//            XEPropertyValuation propertyValuation = new XEPropertyValuation();

//            propertyValuation.SetAvms(CreateAvms());
//            propertyValuation.SetPropertyValuationDetail(CreatePropertyValuationDetail());

//            return propertyValuation;
//        }

//        private XEAvms CreateAvms()
//        {
//            XEAvms avms = new XEAvms();
//            XEAvm avm = CreateAvm();

//            if (avm == null)
//            {
//                return null;
//            }
//            else
//            {
//                avms.SetAvm(avm);
//            }

//            return avms;
//        }

//        private XEAvm CreateAvm()
//        {
//            XEAvm avm = new XEAvm();

//            if (m_dataLoan.sSpAvmModelT != E_sSpAvmModelT.MTM && m_dataLoan.sSpAvmModelT != E_sSpAvmModelT.LeaveBlank)
//            {
//                avm.AVMModelNameType = ToMismo(m_dataLoan.sSpAvmModelT);
//            }
//            else if (m_dataLoan.sSpAvmModelT == E_sSpAvmModelT.MTM)
//            {
//                avm.AVMModelNameTypeOtherDescription = E_AVMModelNameType.MTM;
//                avm.AVMModelNameType = E_AVMModelNameType.Other;
//            }
//            else
//            {
//                return null;
//            }

//            return avm;
//        }

//        private XEPropertyValuationDetail CreatePropertyValuationDetail()
//        {
//            XEPropertyValuationDetail propertyValuationDetail = new XEPropertyValuationDetail();

//            propertyValuationDetail.AppraisalIdentifier = m_dataLoan.sSpAppraisalId;

//            string propertyValuationAmount = "";
//            if (m_dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.PriorAppraisalUsed)
//            {
//                propertyValuationAmount = TruncateMoneyAmount(m_dataLoan.sOriginalAppraisedValue_rep);
//            }
//            else if (m_dataLoan.sSpValuationMethodT == E_sSpValuationMethodT.None
//                && m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase
//                && m_dataLoan.sApprVal == 0.0M)
//            {
//                propertyValuationAmount = TruncateMoneyAmount(m_dataLoan.sPurchPrice_rep);
//            }
//            else
//            {
//                propertyValuationAmount = TruncateMoneyAmount(m_dataLoan.sApprVal_rep);
//            }
//            propertyValuationDetail.PropertyValuationAmount = propertyValuationAmount;

//            propertyValuationDetail.PropertyValuationEffectiveDate = m_dataLoan.sSpValuationEffectiveD_rep;

//            if (m_dataLoan.sSpValuationMethodT != E_sSpValuationMethodT.LeaveBlank)
//            {
//                propertyValuationDetail.PropertyValuationMethodType = ToMismo(m_dataLoan.sSpValuationMethodT);
//            }

//            return propertyValuationDetail;
//        }

//        private XEPropertyUnits CreatePropertyUnits()
//        {
//            XEPropertyUnits propertyUnits = new XEPropertyUnits();

//            XEPropertyUnit propertyUnit = new XEPropertyUnit();
//            if (m_dataLoan.sUnitsNum > 0)
//            {
//                propertyUnit.AddPropertyUnitDetail(CreatePropertyUnitDetail(m_dataLoan.sSpUnit1BedroomsCount_rep, m_dataLoan.sSpUnit1EligibleRent_rep));
//                propertyUnits.AddPropertyUnit(propertyUnit);
//            }

//            if (m_dataLoan.sUnitsNum > 1)
//            {
//                propertyUnit = new XEPropertyUnit();
//                propertyUnit.AddPropertyUnitDetail(CreatePropertyUnitDetail(m_dataLoan.sSpUnit2BedroomsCount_rep, m_dataLoan.sSpUnit2EligibleRent_rep));
//                propertyUnits.AddPropertyUnit(propertyUnit);
//            }
            
//            if (m_dataLoan.sUnitsNum > 2)
//            {
//                propertyUnit = new XEPropertyUnit();
//                propertyUnit.AddPropertyUnitDetail(CreatePropertyUnitDetail(m_dataLoan.sSpUnit3BedroomsCount_rep, m_dataLoan.sSpUnit3EligibleRent_rep));
//                propertyUnits.AddPropertyUnit(propertyUnit);
//            }

//            if (m_dataLoan.sUnitsNum > 3)
//            {
//                propertyUnit = new XEPropertyUnit();
//                propertyUnit.AddPropertyUnitDetail(CreatePropertyUnitDetail(m_dataLoan.sSpUnit4BedroomsCount_rep, m_dataLoan.sSpUnit4EligibleRent_rep));
//                propertyUnits.AddPropertyUnit(propertyUnit);
//            }

//            return propertyUnits;
//        }

//        private XEPropertyUnitDetail CreatePropertyUnitDetail(string bedroomCount, string propertyDwellingUnitEligibleRentAmount)
//        {
//            int bedroomCountInt = 0;
//            try { bedroomCountInt = Convert.ToInt32(bedroomCount); }
//            catch (FormatException) {}

//            if (bedroomCountInt == 0)
//                return null;

//            XEPropertyUnitDetail propertyUnitDetail = new XEPropertyUnitDetail();

//            propertyUnitDetail.BedroomCount = bedroomCountInt.ToString();
//            propertyUnitDetail.PropertyDwellingUnitEligibleRentAmount = TruncateMoneyAmount(propertyDwellingUnitEligibleRentAmount);

//            return propertyUnitDetail;
//        }

//        private XEPropertyDetail CreatePropertyDetail()
//        {
//            XEPropertyDetail propertyDetail = new XEPropertyDetail();

//            propertyDetail.AttachmentType = ToMismo(m_dataLoan.sProdSpStructureT);
            
//            if (m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHousing
//                || m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide
//                || m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium
//                || m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide)
//            {
//                propertyDetail.ConstructionMethodType = E_ConstructionMethodType.Manufactured;
//            }
//            else if (m_dataLoan.sGseSpT == E_sGseSpT.Modular
//                || m_dataLoan.sGseSpT == E_sGseSpT.Condominium
//                || m_dataLoan.sGseSpT == E_sGseSpT.Cooperative
//                || m_dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium
//                || m_dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium)
//            {
//                propertyDetail.ConstructionMethodType = E_ConstructionMethodType.Modular;
//            }
//            else
//            {
//                propertyDetail.ConstructionMethodType = E_ConstructionMethodType.SiteBuilt;
//            }

//            propertyDetail.FinancedUnitCount = m_dataLoan.sUnitsNum_rep;
//            propertyDetail.PropertyEstateType = ToMismo(m_dataLoan.sEstateHeldT);
//            propertyDetail.PropertyFloodInsuranceIndicator = m_dataLoan.sProFloodIns > 0;
//            propertyDetail.PropertyStructureBuiltYear = m_dataLoan.sYrBuilt;
//            propertyDetail.PropertyUsageType = ToMismo(m_dataApp.aOccT);

//            return propertyDetail;
//        }

//        private XEProject CreateProject()
//        {
//            XEProject project = new XEProject();

//            project.SetProjectDetail(CreateProjectDetail());

//            return project;
//        }

//        private XEProjectDetail CreateProjectDetail()
//        {
//            XEProjectDetail projectDetail = new XEProjectDetail();

//            if (m_dataLoan.sSpProjectStatusT != E_sSpProjectStatusT.LeaveBlank)
//            {
//                projectDetail.CondominiumProjectStatusType = ToMismo(m_dataLoan.sSpProjectStatusT);
//            }
//            projectDetail.FNMCondominiumProjectManagerProjectIdentifier = m_dataLoan.sCpmProjectId;

//            if (m_dataLoan.sSpProjectAttachmentT != E_sSpProjectAttachmentT.LeaveBlank)
//            {
//                projectDetail.ProjectAttachmentType = ToMismo(m_dataLoan.sSpProjectAttachmentT);
//            }

//            if (m_dataLoan.sLT == E_sLT.Conventional)
//            {
//                if (m_dataLoan.sSpProjectClassFannieT != E_sSpProjectClassFannieT.LeaveBlank)
//                {
//                    switch (m_dataLoan.sSpProjectClassFannieT)
//                    {
//                        case E_sSpProjectClassFannieT._1Coop:
//                            projectDetail.ProjectClassificationIdentifier = "1";
//                            break;
//                        case E_sSpProjectClassFannieT._2Coop:
//                            projectDetail.ProjectClassificationIdentifier = "2";
//                            break;
//                        case E_sSpProjectClassFannieT.EPud:
//                            projectDetail.ProjectClassificationIdentifier = "E";
//                            break;
//                        case E_sSpProjectClassFannieT.FPud:
//                            projectDetail.ProjectClassificationIdentifier = "F";
//                            break;
//                        case E_sSpProjectClassFannieT.PLimitedReviewNew:
//                            projectDetail.ProjectClassificationIdentifier = "P";
//                            break;
//                        case E_sSpProjectClassFannieT.QLimitedReviewEst:
//                            projectDetail.ProjectClassificationIdentifier = "Q";
//                            break;
//                        case E_sSpProjectClassFannieT.RExpeditedReviewNew:
//                            projectDetail.ProjectClassificationIdentifier = "R";
//                            break;
//                        case E_sSpProjectClassFannieT.SExpeditedReviewEst:
//                            projectDetail.ProjectClassificationIdentifier = "S";
//                            break;
//                        case E_sSpProjectClassFannieT.TCoop:
//                        case E_sSpProjectClassFannieT.TFannieReview:
//                        case E_sSpProjectClassFannieT.TPud:
//                            projectDetail.ProjectClassificationIdentifier = "T";
//                            break;
//                        case E_sSpProjectClassFannieT.UFhaApproved:
//                            projectDetail.ProjectClassificationIdentifier = "U";
//                            break;
//                        case E_sSpProjectClassFannieT.VRefiPlus:
//                            projectDetail.ProjectClassificationIdentifier = "V";
//                            break;
//                    }
//                }
//                else if (m_dataLoan.sSpProjectClassFreddieT != E_sSpProjectClassFreddieT.LeaveBlank)
//                {
//                    switch (m_dataLoan.sSpProjectClassFreddieT)
//                    {
//                        case E_sSpProjectClassFreddieT.FreddieMacStreamlinedReview:
//                            projectDetail.ProjectClassificationIdentifier = "StreamlinedReview";
//                            break;
//                        case E_sSpProjectClassFreddieT.FreddieMacEstablishedProject:
//                        case E_sSpProjectClassFreddieT.FreddieMacNewProject:
//                        case E_sSpProjectClassFreddieT.FreddieMacDetachedProject:
//                        case E_sSpProjectClassFreddieT.FreddieMac2To4UnitProject:
//                            projectDetail.ProjectClassificationIdentifier = "FullReview";
//                            break;
//                        case E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewCPM:
//                            projectDetail.ProjectClassificationIdentifier = "CondominiumProjectManagerReview";
//                            break;
//                        case E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewPERS:
//                            projectDetail.ProjectClassificationIdentifier = "ProjectEligibilityReviewService";
//                            break;
//                        case E_sSpProjectClassFreddieT.FreddieMacReciprocalReviewFHA:
//                            projectDetail.ProjectClassificationIdentifier = "FHA_Approved";
//                            break;
//                    }
//                }
//                else if (m_dataLoan.sSpGseRefiProgramT == E_sSpGseRefiProgramT.ReliefRefiSameServicer)
//                {
//                    projectDetail.ProjectClassificationIdentifier = "ExemptFromReview";
//                }
//                else
//                {
//                    projectDetail.ProjectClassificationIdentifier = "G";
//                }
//            }

//            if (m_dataLoan.sSpProjectDesignT != E_sSpProjectDesignT.LeaveBlank)
//            {
//                projectDetail.ProjectDesignType = ToMismo(m_dataLoan.sSpProjectDesignT);
//            }
//            projectDetail.ProjectDwellingUnitCount = m_dataLoan.sSpProjectDwellingUnitCount_rep;
//            projectDetail.ProjectDwellingUnitsSoldCount = m_dataLoan.sSpProjectDwellingUnitSoldCount_rep;
//            if (m_dataLoan.sGseSpT == E_sGseSpT.Cooperative)
//            {
//                projectDetail.ProjectLegalStructureType = E_ProjectLegalStructureType.Cooperative;
//            }
//            else if (m_dataLoan.sGseSpT == E_sGseSpT.DetachedCondominium
//                || m_dataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium
//                || m_dataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium
//                || m_dataLoan.sGseSpT == E_sGseSpT.Condominium)
//            {
//                projectDetail.ProjectLegalStructureType = E_ProjectLegalStructureType.Condominium;
//            }
//            projectDetail.ProjectName = m_dataLoan.sProjNm;
//            projectDetail.PUDIndicator = m_dataLoan.sGseSpT == E_sGseSpT.PUD;

//            return projectDetail;
//        }

//        private XEAddress CreateAddress()
//        {
//            XEAddress address = new XEAddress();

//            address.AddressLineText = m_dataLoan.sSpAddr;
//            address.CityName = m_dataLoan.sSpCity;
//            address.PostalCode = m_dataLoan.sSpZip;
//            address.StateCode = m_dataLoan.sSpState;

//            return address;
//        }

//        private XEAboutVersions CreateAboutVersions()
//        {
//            XEAboutVersions aboutVersions = new XEAboutVersions();

//            aboutVersions.SetAboutVersion(CreateAboutVersion());

//            return aboutVersions;
//        }

//        private XEAboutVersion CreateAboutVersion()
//        {
//            XEAboutVersion aboutVersion = new XEAboutVersion();
//            aboutVersion.CreatedDateTime = string.Format("{0:yyyy-MM-dd}T{1:HH:mm:ss}", System.DateTime.Now, System.DateTime.Now);
//            if (E_sGseDeliveryTargetT.FreddieMac == m_dataLoan.sGseDeliveryTargetT)
//            {
//                aboutVersion.AboutVersionIdentifier = "FRE 1.0.4";
//            }
//            return aboutVersion;
//        }

//        #region EnumMapping
//        private E_AutomatedUnderwritingRecommendationDescription ToMismo(E_sProd3rdPartyUwResultT sProd3rdPartyUwResultT)
//        {
//            switch (sProd3rdPartyUwResultT)
//            {
//                case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.ApproveEligible;
//                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.ApproveIneligible;
//                case E_sProd3rdPartyUwResultT.DU_EAIEligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.EAIEligible;
//                case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.EAIIEligible;
//                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.EAIIIEligible;
//                case E_sProd3rdPartyUwResultT.DU_ReferEligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.ReferEligible;
//                case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.ReferIneligible;
//                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
//                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.ReferWithCautionIV;
//                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
//                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.Accept;
//                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
//                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
//                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
//                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
//                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
//                    return E_AutomatedUnderwritingRecommendationDescription.CautionEligibleForAMinus;
//                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
//                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
//                    return E_AutomatedUnderwritingRecommendationDescription.C1Caution;
//                case E_sProd3rdPartyUwResultT.Lp_Refer:
//                    return E_AutomatedUnderwritingRecommendationDescription.Unknown;
//                case E_sProd3rdPartyUwResultT.OutOfScope:
//                    return E_AutomatedUnderwritingRecommendationDescription.OutofScope;
//                default:
//                    throw new UnhandledEnumException(sProd3rdPartyUwResultT);
//            }
//        }

//        private E_ProjectAttachmentType ToMismo(E_sSpProjectAttachmentT sSpProjectAttachmentT)
//        {
//            switch (sSpProjectAttachmentT)
//            {
//                case E_sSpProjectAttachmentT.Attached:
//                    return E_ProjectAttachmentType.Attached;
//                case E_sSpProjectAttachmentT.Detached:
//                    return E_ProjectAttachmentType.Detached;
//                case E_sSpProjectAttachmentT.LeaveBlank:
//                default:
//                    throw new UnhandledEnumException(sSpProjectAttachmentT);
//            }
//        }

//        private E_ProjectDesignType ToMismo(E_sSpProjectDesignT sSpProjectDesignT)
//        {
//            switch (sSpProjectDesignT)
//            {
//                case E_sSpProjectDesignT.HighRise:
//                    return E_ProjectDesignType.HighriseProject;
//                case E_sSpProjectDesignT.MidRise:
//                    return E_ProjectDesignType.MidriseProject;
//                case E_sSpProjectDesignT.LowRise:
//                    return E_ProjectDesignType.GardenProject;
//                case E_sSpProjectDesignT.Townhouse:
//                    return E_ProjectDesignType.TownhouseRowhouse;
//                case E_sSpProjectDesignT.LeaveBlank:
//                default:
//                    throw new UnhandledEnumException(sSpProjectDesignT);
//            }
//        }

//        private E_CondominiumProjectStatusType ToMismo(E_sSpProjectStatusT sSpProjectStatusT)
//        {
//            switch (sSpProjectStatusT)
//            {
//                case E_sSpProjectStatusT.Existing:
//                    return E_CondominiumProjectStatusType.Established;
//                case E_sSpProjectStatusT.New:
//                    return E_CondominiumProjectStatusType.New;
//                case E_sSpProjectStatusT.LeaveBlank:
//                default:
//                    throw new UnhandledEnumException(sSpProjectStatusT);
//            }
//        }

//        private E_PropertyValuationMethodType ToMismo(E_sSpValuationMethodT sSpValuationMethodT)
//        {
//            switch (sSpValuationMethodT)
//            {
//                case E_sSpValuationMethodT.AutomatedValuationModel:
//                    return E_PropertyValuationMethodType.AutomatedValuationModel;
//                case E_sSpValuationMethodT.DesktopAppraisal:
//                    return E_PropertyValuationMethodType.DesktopAppraisal;
//                case E_sSpValuationMethodT.DriveBy:
//                    return E_PropertyValuationMethodType.DriveBy;
//                case E_sSpValuationMethodT.FullAppraisal:
//                    return E_PropertyValuationMethodType.FullAppraisal;
//                case E_sSpValuationMethodT.None:
//                    return E_PropertyValuationMethodType.None;
//                case E_sSpValuationMethodT.PriorAppraisalUsed:
//                    return E_PropertyValuationMethodType.PriorAppraisalUsed;
//                case E_sSpValuationMethodT.LeaveBlank:
//                default:
//                    throw new UnhandledEnumException(sSpValuationMethodT);
//            }
//        }

//        private E_AVMModelNameType ToMismo(E_sSpAvmModelT sSpAvmModelT)
//        {
//            switch (sSpAvmModelT)
//            {
//                case E_sSpAvmModelT.AutomatedPropertyService:
//                    return E_AVMModelNameType.AutomatedPropertyService;
//                case E_sSpAvmModelT.Casa:
//                    return E_AVMModelNameType.Casa;
//                case E_sSpAvmModelT.FidelityHansen:
//                    return E_AVMModelNameType.FidelityHansen;
//                case E_sSpAvmModelT.HomePriceAnalyzer:
//                    return E_AVMModelNameType.HomePriceAnalyzer;
//                case E_sSpAvmModelT.HomePriceIndex:
//                    return E_AVMModelNameType.HomePriceIndex;
//                case E_sSpAvmModelT.HomeValueExplorer:
//                    return E_AVMModelNameType.HomeValueExplorer;
//                case E_sSpAvmModelT.Indicator:
//                    return E_AVMModelNameType.Indicator;
//                case E_sSpAvmModelT.MTM:
//                    return E_AVMModelNameType.MTM;
//                case E_sSpAvmModelT.NetValue:
//                    return E_AVMModelNameType.NetValue;
//                case E_sSpAvmModelT.Pass:
//                    return E_AVMModelNameType.Pass;
//                case E_sSpAvmModelT.PropertySurveyAnalysisReport:
//                    return E_AVMModelNameType.PropertySurveyAnalysisReport;
//                case E_sSpAvmModelT.ValueFinder:
//                    return E_AVMModelNameType.ValueFinder;
//                case E_sSpAvmModelT.ValuePoint:
//                    return E_AVMModelNameType.ValuePoint;
//                case E_sSpAvmModelT.ValuePoint4:
//                    return E_AVMModelNameType.ValuePoint4;
//                case E_sSpAvmModelT.ValuePointPlus:
//                    return E_AVMModelNameType.ValuePointPlus;
//                case E_sSpAvmModelT.ValueSure:
//                    return E_AVMModelNameType.ValueSure;
//                case E_sSpAvmModelT.ValueWizard:
//                    return E_AVMModelNameType.ValueWizard;
//                case E_sSpAvmModelT.ValueWizardPlus:
//                    return E_AVMModelNameType.ValueWizardPlus;
//                case E_sSpAvmModelT.VeroIndexPlus:
//                    return E_AVMModelNameType.VeroIndexPlus;
//                case E_sSpAvmModelT.VeroValue:
//                    return E_AVMModelNameType.VeroValue;
//                case E_sSpAvmModelT.LeaveBlank:
//                default:
//                    throw new UnhandledEnumException(sSpAvmModelT);
//            }
//        }

//        private E_AttachmentType ToMismo(E_sProdSpStructureT sProdSpStructureT)
//        {
//            switch (sProdSpStructureT)
//            {
//                case E_sProdSpStructureT.Attached:
//                    return E_AttachmentType.Attached;
//                case E_sProdSpStructureT.Detached:
//                    return E_AttachmentType.Detached;
//                default:
//                    throw new UnhandledEnumException(sProdSpStructureT);
//            }
//        }

//        private E_PropertyEstateType ToMismo(E_sEstateHeldT sEstateHeldT)
//        {
//            switch (sEstateHeldT)
//            {
//                case E_sEstateHeldT.FeeSimple:
//                    return E_PropertyEstateType.FeeSimple;
//                case E_sEstateHeldT.LeaseHold:
//                    return E_PropertyEstateType.Leasehold;
//                default:
//                    throw new UnhandledEnumException(sEstateHeldT);
//            }
//        }

//        private E_PropertyUsageType ToMismo(E_aOccT aOccT)
//        {
//            switch (aOccT)
//            {
//                case E_aOccT.Investment:
//                    return E_PropertyUsageType.Investment;
//                case E_aOccT.PrimaryResidence:
//                    return E_PropertyUsageType.PrimaryResidence;
//                case E_aOccT.SecondaryResidence:
//                    return E_PropertyUsageType.SecondHome;
//                default:
//                    throw new UnhandledEnumException(aOccT);
//            }
//        }

//        private E_HMDAEthnicityType ToMismo(E_aHispanicT aHispanicT)
//        {
//            switch (aHispanicT)
//            {
//                case E_aHispanicT.Hispanic:
//                    return E_HMDAEthnicityType.HispanicOrLatino;
//                case E_aHispanicT.NotHispanic:
//                    return E_HMDAEthnicityType.NotHispanicOrLatino;
//                case E_aHispanicT.LeaveBlank:
//                    return E_HMDAEthnicityType.NotApplicable;
//                default:
//                    throw new UnhandledEnumException(aHispanicT);
//            }
//        }

//        private E_GenderType ToMismo(E_GenderT GenderT)
//        {
//            switch (GenderT)
//            {
//                case E_GenderT.Male:
//                    return E_GenderType.Male;
//                case E_GenderT.Female:
//                    return E_GenderType.Female;
//                case E_GenderT.Unfurnished:
//                case E_GenderT.LeaveBlank:
//                case E_GenderT.NA:
//                    return E_GenderType.InformationNotProvidedUnknown;
//                default:
//                    throw new UnhandledEnumException(GenderT);
//            }
//        }

//        private E_InterestRateRoundingType ToMismo(E_sRAdjRoundT sRAdjRoundT)
//        {
//            switch (sRAdjRoundT)
//            {
//                case E_sRAdjRoundT.Down:
//                    return E_InterestRateRoundingType.Down;
//                case E_sRAdjRoundT.Normal:
//                    return E_InterestRateRoundingType.Nearest;
//                case E_sRAdjRoundT.Up:
//                    return E_InterestRateRoundingType.Up;
//                default:
//                    throw new UnhandledEnumException(sRAdjRoundT);
//            }
//        }

//        private E_LoanAmortizationType ToMismo(E_sFinMethT sFinMethT)
//        {
//            switch (sFinMethT)
//            {
//                case E_sFinMethT.ARM:
//                    return E_LoanAmortizationType.AdjustableRate;
//                case E_sFinMethT.Fixed:
//                    return E_LoanAmortizationType.Fixed;
//                case E_sFinMethT.Graduated:
//                    return E_LoanAmortizationType.GraduatedPaymentARM;
//                default:
//                    throw new UnhandledEnumException(sFinMethT);
//            }
//        }

//        private E_ClosingCostFundsType ToMismo(E_sCommunityLendingClosingCostT sCommunityLendingClosingCostT)
//        {
//            switch (sCommunityLendingClosingCostT)
//            {
//                case E_sCommunityLendingClosingCostT.AggregatedRemainingTypes:
//                    return E_ClosingCostFundsType.AggregatedRemainingTypes;
//                case E_sCommunityLendingClosingCostT.BridgeLoan:
//                    return E_ClosingCostFundsType.BridgeLoan;
//                case E_sCommunityLendingClosingCostT.CashOnHand:
//                    return E_ClosingCostFundsType.CashOnHand;
//                case E_sCommunityLendingClosingCostT.CheckingSavings:
//                    return E_ClosingCostFundsType.CheckingSavings;
//                case E_sCommunityLendingClosingCostT.Contribution:
//                    return E_ClosingCostFundsType.Contribution;
//                case E_sCommunityLendingClosingCostT.CreditCard:
//                    return E_ClosingCostFundsType.CreditCard;
//                case E_sCommunityLendingClosingCostT.GiftFunds:
//                    return E_ClosingCostFundsType.GiftFunds;
//                case E_sCommunityLendingClosingCostT.Grant:
//                    return E_ClosingCostFundsType.Grant;
//                case E_sCommunityLendingClosingCostT.PremiumFunds:
//                    return E_ClosingCostFundsType.PremiumFunds;
//                case E_sCommunityLendingClosingCostT.SecondaryClosedEnd:
//                    return E_ClosingCostFundsType.SecondaryFinancingClosedEnd;
//                case E_sCommunityLendingClosingCostT.SecondaryHELOC:
//                    return E_ClosingCostFundsType.SecondaryFinancingHELOC;
//                case E_sCommunityLendingClosingCostT.SecuredLoan:
//                    return E_ClosingCostFundsType.SecuredLoan;
//                case E_sCommunityLendingClosingCostT.SweatEquity:
//                    return E_ClosingCostFundsType.SweatEquity;
//                case E_sCommunityLendingClosingCostT.UnsecuredBorrowedFunds:
//                    return E_ClosingCostFundsType.UnsecuredBorrowedFunds;
//                default:
//                    throw new UnhandledEnumException(sCommunityLendingClosingCostT);
//            }
//        }

//        private E_ClosingCostSourceType ToMismo(E_sCommunityLendingClosingCostSourceT sCommunityLendingClosingCostSourceT)
//        {
//            switch (sCommunityLendingClosingCostSourceT)
//            {
//                case E_sCommunityLendingClosingCostSourceT.AggregatedRemainingTypes:
//                    return E_ClosingCostSourceType.AggregatedRemainingSourceTypes;
//                case E_sCommunityLendingClosingCostSourceT.Borrower:
//                    return E_ClosingCostSourceType.Borrower;
//                case E_sCommunityLendingClosingCostSourceT.CommunityNonProfit:
//                    return E_ClosingCostSourceType.CommunityNonProfit;
//                case E_sCommunityLendingClosingCostSourceT.FederalAgency:
//                    return E_ClosingCostSourceType.FederalAgency;
//                case E_sCommunityLendingClosingCostSourceT.FHLBAffordableHousingProgram:
//                    return E_ClosingCostSourceType.FHLBAffordableHousingProgram;
//                case E_sCommunityLendingClosingCostSourceT.Lender:
//                    return E_ClosingCostSourceType.Lender;
//                case E_sCommunityLendingClosingCostSourceT.LocalAgency:
//                    return E_ClosingCostSourceType.LocalAgency;
//                case E_sCommunityLendingClosingCostSourceT.PropertySeller:
//                    return E_ClosingCostSourceType.PropertySeller;
//                case E_sCommunityLendingClosingCostSourceT.Relative:
//                    return E_ClosingCostSourceType.Relative;
//                case E_sCommunityLendingClosingCostSourceT.ReligiousNonProfit:
//                    return E_ClosingCostSourceType.ReligiousNonProfit;
//                case E_sCommunityLendingClosingCostSourceT.StateAgency:
//                    return E_ClosingCostSourceType.StateAgency;
//                case E_sCommunityLendingClosingCostSourceT.UsdaRuralHousing:
//                    return E_ClosingCostSourceType.USDARuralHousing;
//                default:
//                    throw new UnhandledEnumException(sCommunityLendingClosingCostSourceT);
//            }
//        }

//        private E_LienPriorityType ToMismo(E_sLienPosT sLienPosT)
//        {
//            switch (sLienPosT)
//            {
//                case E_sLienPosT.First:
//                    return E_LienPriorityType.FirstLien;
//                case E_sLienPosT.Second:
//                    return E_LienPriorityType.SecondLien;
//                default:
//                    throw new UnhandledEnumException(sLienPosT);
//            }
//        }

//        private E_LoanPurposeType ToMismo(E_sLPurposeT sLpurposeT)
//        {
//            switch (sLpurposeT)
//            {
//                case E_sLPurposeT.Purchase:
//                case E_sLPurposeT.Construct:
//                case E_sLPurposeT.ConstructPerm:
//                    return E_LoanPurposeType.Purchase;
//                case E_sLPurposeT.Refin:
//                case E_sLPurposeT.RefinCashout:
                    
//                case E_sLPurposeT.FhaStreamlinedRefinance:
//                case E_sLPurposeT.VaIrrrl:
//                case E_sLPurposeT.HomeEquity:
//                    return E_LoanPurposeType.Refinance;
//                default:
//                    throw new UnhandledEnumException(sLpurposeT);
//            }
//        }

//        private E_MortgageType ToMismo(E_sLT sLT)
//        {
//            switch (sLT)
//            {
//                case E_sLT.Conventional:
//                    return E_MortgageType.Conventional;
//                case E_sLT.FHA:
//                    return E_MortgageType.FHA;
//                case E_sLT.UsdaRural:
//                    return E_MortgageType.USDARuralHousing;
//                case E_sLT.VA:
//                    return E_MortgageType.VA;
//                case E_sLT.Other:
//                    return E_MortgageType.Other;
//                default:
//                    throw new UnhandledEnumException(sLT);
//            }
//        }

//        private E_LoanOriginatorType ToMismo(E_BranchChannelT sBranchChannelT)
//        {
//            switch (sBranchChannelT)
//            {
//                case E_BranchChannelT.Broker:
//                    return E_LoanOriginatorType.Broker;
//                case E_BranchChannelT.Correspondent:
//                    return E_LoanOriginatorType.Correspondent;
//                case E_BranchChannelT.Wholesale:
//                case E_BranchChannelT.Retail:
//                    return E_LoanOriginatorType.Lender;
//                default:
//                    throw new UnhandledEnumException(sBranchChannelT);
//            }
//        }

//        private E_DownPaymentType ToMismo(E_sCommunityLendingDownPaymentT sCommunityLendingDownPaymentT)
//        {
//            switch (sCommunityLendingDownPaymentT)
//            {
//                case E_sCommunityLendingDownPaymentT.AggregatedRemainingTypes:
//                    return E_DownPaymentType.AggregatedRemainingTypes;
//                case E_sCommunityLendingDownPaymentT.BridgeLoan:
//                    return E_DownPaymentType.BridgeLoan;
//                case E_sCommunityLendingDownPaymentT.CashOnHand:
//                    return E_DownPaymentType.CashOnHand;
//                case E_sCommunityLendingDownPaymentT.CheckingSavings:
//                    return E_DownPaymentType.CheckingSavings;
//                case E_sCommunityLendingDownPaymentT.GiftFunds:
//                    return E_DownPaymentType.GiftFunds;
//                case E_sCommunityLendingDownPaymentT.SecondaryFinancingClosedEnd:
//                    return E_DownPaymentType.SecondaryFinancingClosedEnd;
//                case E_sCommunityLendingDownPaymentT.SecondaryFinancingHELOC:
//                    return E_DownPaymentType.SecondaryFinancingHELOC;
//                case E_sCommunityLendingDownPaymentT.SecuredBorrowedFunds:
//                    return E_DownPaymentType.SecuredBorrowedFunds;
//                case E_sCommunityLendingDownPaymentT.SweatEquity:
//                    return E_DownPaymentType.SweatEquity;
//                case E_sCommunityLendingDownPaymentT.UnsecuredBorrowedFunds:
//                    return E_DownPaymentType.UnsecuredBorrowedFunds;
//                default:
//                    throw new UnhandledEnumException(sCommunityLendingDownPaymentT);
//            }
//        }

//        private E_DownPaymentSourceType ToMismo(E_sCommunityLendingDownPaymentSourceT sCommunityLendingDownPaymentSourceT)
//        {
//            switch (sCommunityLendingDownPaymentSourceT)
//            {
//                case E_sCommunityLendingDownPaymentSourceT.AggregatedRemainingTypes:
//                    return E_DownPaymentSourceType.AggregatedRemainingSourceTypes;
//                case E_sCommunityLendingDownPaymentSourceT.Borrower:
//                    return E_DownPaymentSourceType.Borrower;
//                case E_sCommunityLendingDownPaymentSourceT.CommunityNonProfit:
//                    return E_DownPaymentSourceType.CommunityNonProfit;
//                case E_sCommunityLendingDownPaymentSourceT.FederalAgency:
//                    return E_DownPaymentSourceType.FederalAgency;
//                case E_sCommunityLendingDownPaymentSourceT.FHLBAffordableHousingProgram:
//                    return E_DownPaymentSourceType.FHLBAffordableHousingProgram;
//                case E_sCommunityLendingDownPaymentSourceT.LocalAgency:
//                    return E_DownPaymentSourceType.LocalAgency;
//                case E_sCommunityLendingDownPaymentSourceT.Relative:
//                    return E_DownPaymentSourceType.Relative;
//                case E_sCommunityLendingDownPaymentSourceT.ReligiousNonProfit:
//                    return E_DownPaymentSourceType.ReligiousNonProfit;
//                case E_sCommunityLendingDownPaymentSourceT.StateAgency:
//                    return E_DownPaymentSourceType.StateAgency;
//                case E_sCommunityLendingDownPaymentSourceT.UsdaRuralHousing:
//                    return E_DownPaymentSourceType.USDARuralHousing;
//                default:
//                    throw new UnhandledEnumException(sCommunityLendingDownPaymentSourceT);                    
//            }
//        }

//        private E_InvestorRemittanceType ToMismo(E_sGseDeliveryRemittanceT sGseDeliveryRemittanceT)
//        {
//            switch(sGseDeliveryRemittanceT)
//            {
//                case E_sGseDeliveryRemittanceT.ActualActual:
//                    return E_InvestorRemittanceType.ActualInterestActualPrincipal;
//                case E_sGseDeliveryRemittanceT.ScheduledActual:
//                    return E_InvestorRemittanceType.ScheduledInterestActualPrincipal;
//                case E_sGseDeliveryRemittanceT.ScheduledScheduled:
//                    return E_InvestorRemittanceType.ScheduledInterestScheduledPrincipal;
//                default:
//                    throw new UnhandledEnumException(sGseDeliveryRemittanceT);   
//            }
//        }

//        private E_InvestorCollateralProgramIdentifier ToMismo(E_sSpGseCollateralProgramT sSpGseCollateralProgramT)
//        {
//            switch(sSpGseCollateralProgramT)
//            {
//                case E_sSpGseCollateralProgramT.PropertyInspectionAlternative:
//                    return E_InvestorCollateralProgramIdentifier.PropertyInspectionAlternative;
//                case E_sSpGseCollateralProgramT.PropertyInspectionWaiver:
//                    return E_InvestorCollateralProgramIdentifier.PropertyInspectionWaiver;
//                default:
//                    throw new UnhandledEnumException(sSpGseCollateralProgramT);   
//            }
//        }

//        private E_LoanDefaultLossPartyType ToMismo(E_sGseDeliveryLoanDefaultLossPartyT sGseDeliveryLoanDefaultLossPartyT)
//        {
//            switch(sGseDeliveryLoanDefaultLossPartyT)
//            {
//                case E_sGseDeliveryLoanDefaultLossPartyT.Investor:
//                    return E_LoanDefaultLossPartyType.Investor;
//                case E_sGseDeliveryLoanDefaultLossPartyT.Lender:
//                    return E_LoanDefaultLossPartyType.Lender;
//                case E_sGseDeliveryLoanDefaultLossPartyT.Shared:
//                    return E_LoanDefaultLossPartyType.Shared;
//                default:
//                    throw new UnhandledEnumException(sGseDeliveryLoanDefaultLossPartyT);   
//            }
//        }

//        private E_REOMarketingPartyType ToMismo(E_sGseDeliveryREOMarketingPartyT sGseDeliveryREOMarketingPartyT)
//        {
//            switch(sGseDeliveryREOMarketingPartyT)
//            {
//                case E_sGseDeliveryREOMarketingPartyT.Investor: 
//                    return E_REOMarketingPartyType.Investor;
//                case E_sGseDeliveryREOMarketingPartyT.Lender:
//                    return E_REOMarketingPartyType.Lender;
//                default:
//                    throw new UnhandledEnumException(sGseDeliveryREOMarketingPartyT);   
//            }
//        }

//        private string ToMismo(E_sMiCompanyNmT sMiCompanyNmT)
//        {
//            switch(sMiCompanyNmT)
//            {
//                case E_sMiCompanyNmT.Amerin:
//                    return "Amerin";
//                case E_sMiCompanyNmT.CAHLIF:
//                    return "CAHLIF";
//                case E_sMiCompanyNmT.CMG:
//                    return "CMG";
//                case E_sMiCompanyNmT.CMGPre94:
//                    return "CMGPreSep94";
//                case E_sMiCompanyNmT.Commonwealth:
//                    return "Commonwealth";
//                case E_sMiCompanyNmT.Essent:
//                    return "Essent";
//                case E_sMiCompanyNmT.Genworth:
//                    return "Genworth";
//                case E_sMiCompanyNmT.MDHousing:
//                    return "MDHousing";
//                case E_sMiCompanyNmT.MGIC:
//                    return "MGIC";
//                case E_sMiCompanyNmT.MIF:
//                    return "MIF";
//                case E_sMiCompanyNmT.PMI:
//                    return "PMI";
//                case E_sMiCompanyNmT.Radian:
//                    return "Radian";
//                case E_sMiCompanyNmT.RMIC:
//                    return "RMIC";
//                case E_sMiCompanyNmT.SONYMA:
//                    return "SONYMA";
//                case E_sMiCompanyNmT.Triad:
//                    return "Triad";
//                case E_sMiCompanyNmT.UnitedGuaranty:
//                    return "UGI";
//                case E_sMiCompanyNmT.Verex:
//                    return "Verex";
//                case E_sMiCompanyNmT.WiscMtgAssr:
//                    return "WiscMtgAssr";
//                case E_sMiCompanyNmT.RMICNC:
//                    return "RMIC-NC";
//                default:
//                    throw new UnhandledEnumException(sMiCompanyNmT); 
//            }
//        }

//        private E_MIPremiumSourceType ToMismo(E_sMiInsuranceT sMiInsuranceT)
//        {
//            switch(sMiInsuranceT)
//            {
//                case E_sMiInsuranceT.BorrowerPaid:
//                    return E_MIPremiumSourceType.Borrower;
//                case E_sMiInsuranceT.LenderPaid:
//                    return E_MIPremiumSourceType.Lender;
//                default:
//                    throw new UnhandledEnumException(sMiInsuranceT); 
//            }
//        }

//        private E_PrimaryMIAbsenceReasonType ToMismo(E_sMiReasonForAbsenceT sMiReasonForAbsenceT)
//        {
//            switch (sMiReasonForAbsenceT)
//            {
//                case E_sMiReasonForAbsenceT.IndemnificationInLieuOfMi:
//                    return E_PrimaryMIAbsenceReasonType.IndemnificationInLieuOfMI;
//                case E_sMiReasonForAbsenceT.MiCanceledBasedOnCurrentLtv:
//                    return E_PrimaryMIAbsenceReasonType.MICanceledBasedOnCurrentLTV;
//                case E_sMiReasonForAbsenceT.NoMiBasedOMortgageBeingRefi:
//                    return E_PrimaryMIAbsenceReasonType.NoMIBasedonMortgageBeingRefinanced;
//                case E_sMiReasonForAbsenceT.NoMiBasedOnOriginalLtv:
//                    return E_PrimaryMIAbsenceReasonType.NoMIBasedOnOriginalLTV;
//                case E_sMiReasonForAbsenceT.RecourseInLieuOfMi:
//                    return E_PrimaryMIAbsenceReasonType.RecourseInLieuOfMI;
//                default:
//                    throw new UnhandledEnumException(sMiReasonForAbsenceT); 
//            }
//        }
//        #endregion

//        private string RoundLTVRatioPercent(decimal value)
//        {
//            // Rounding rules:
//            // Check the formatting for LTVRatioPercent, it looks like it should be multiplied by 100. 
//            // Also, according to the Fannie notes, it looks like they want whole numbers:
//            //
//            // Only whole numbers will be supported at this time.  The LTVRatioPercent must be truncated 
//            // (shortened) to two decimal places.  The truncated result must be rounded up to the next 
//            // whole percent.  For example:  96.001% will be delivered as 96; 80.01% will be delivered as 81.

//            int result = decimal.ToInt32(value * 100);

//            if (result % 100 > 0)
//                result += 100;

//            result /= 100;

//            return result.ToString();
//        }

//        private string TruncateMoneyAmount(string value)
//        {
//            //return value;

//            if (string.IsNullOrEmpty(value))
//                return "";

//            decimal amount = m_dataLoan.m_convertLos.ToMoney(value);
//            amount = decimal.Round(amount);

//            return decimal.ToInt32(amount).ToString();
//        }

//        private string FormatHMDARateSpreadPercent(string value)
//        {
//            // Need to format to dd.dd, rounding up.

//            if (string.IsNullOrEmpty(value))
//                return "";

//            decimal d;
//            try
//            {
//                d = Convert.ToDecimal(value);
//            }
//            catch (FormatException)
//            {
//                return "";
//            }

//            return Math.Round(d, 2).ToString();
//        }

//        protected class AppRelationshipData
//        {
//            public string AddressLineText { get; set; }
//            public string AddressType { get; set; }
//            public string CityName { get; set; }
//            public string CountryCode { get; set; }
//            public string PostalCode { get; set; }
//            public string StateCode { get; set; }
//            public string FirstName { get; set; }
//            public string LastName { get; set; }
//            public string SuffixName { get; set; }
//            public string BorrowerAgeAtApplicationYearsCount { get; set; }
//            public string MiddleName { get; set; }
//            public string BorrowerBirthDate { get; set; }
//            public string BorrowerClassificationType { get; set; }
//            public bool BorrowerMailToAddressSameasPropertyIndicator { get; set; }
//            public string BorrowerQualifyingIncomeAmount { get; set; }
//            public E_aHomeOwnershipCounselingSourceT HomeOwnershipCounselingSource { get; set; }
//            public E_aHomeOwnershipCounselingFormatT HomeOwnershipCounselingFormat { get; set; }
//            public string ExperianScore { get; set; }
//            public string TransUnionScore { get; set; }
//            public string EquifaxScore { get; set; }
//            public string DecPastOwnership { get; set; }
//            public string DecCitizen { get; set; }
//            public string DecResidency { get; set; }
//            public E_GenderT Gender { get; set; }
//            public E_aHispanicT HMDAEthnicityType { get; set; }
//            public bool aBIsAmericanIndian { get; set; }
//            public bool aBIsAsian { get; set; }
//            public bool aBIsBlack { get; set; }
//            public bool aBIsPacificIslander { get; set; }
//            public bool aBIsWhite { get; set; }
//            public string Ssn { get; set; }

//            public AppRelationshipData() { }
//        }
//    }
//}