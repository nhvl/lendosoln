﻿namespace LendersOffice.Conversions
{
    using Newtonsoft.Json;

    /// <summary>
    /// Represents the result of parsing a string into a value.
    /// </summary>
    /// <typeparam name="T">The type of the value being parsed.</typeparam>
    [JsonObject(MemberSerialization.OptIn)]
    public class ParseResult<T>
    {
        /// <summary>
        /// Prevents a default instance of the <see cref="ParseResult{T}"/> class from being created.
        /// </summary>
        [JsonConstructor]
        private ParseResult()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ParseResult{T}"/> class.
        /// </summary>
        /// <param name="isSuccessful">A value indicating whether the parsing was successful.</param>
        /// <param name="input">The input string as consumed by the parser.</param>
        /// <param name="value">The value parsed from the input, or the default if parsing was unsuccessful.</param>
        /// <param name="errorMessage">The error message to display to the user.</param>
        private ParseResult(bool isSuccessful, string input, T value = default(T), string errorMessage = null)
        {
            this.IsSuccessful = isSuccessful;
            this.Input = input;
            this.ValueOrDefault = value;
            this.ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets a value indicating whether the parsing was successful.
        /// </summary>
        /// <value>A value indicating whether the parsing was successful.</value>
        [JsonProperty]
        public bool IsSuccessful { get; private set; }

        /// <summary>
        /// Gets the input string as consumed by the parser.
        /// </summary>
        /// <value>The input string as consumed by the parser.</value>
        /// <remarks>
        /// This is useful especially if the parser does something to fix-up invalid input, e.g., removing
        /// a Document Type Definition (DTD) from an XML file.
        /// </remarks>
        [JsonProperty]
        public string Input { get; private set; }

        /// <summary>
        /// Gets the value parsed from the input, or the default if parsing was unsuccessful.
        /// </summary>
        /// <value>The value parsed from the input, or the default if parsing was unsuccessful.</value>
        [JsonProperty]
        public T ValueOrDefault { get; private set; }

        /// <summary>
        /// Gets the error message associated with any parsing failure.  This may be null
        /// even for unsuccessful results.
        /// </summary>
        /// <value>The error message associated with any parsing failure.</value>
        [JsonProperty]
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// Creates a success result to the parsing operation.
        /// </summary>
        /// <param name="input">The input string as consumed by the parser.</param>
        /// <param name="value">The value parsed from the input.</param>
        /// <returns>A parse result item representing a successful parse.</returns>
        public static ParseResult<T> Success(string input, T value)
        {
            return new ParseResult<T>(true, input, value: value);
        }

        /// <summary>
        /// Creates a failure result to the parsing operation.
        /// </summary>
        /// <param name="input">The input string as consumed by the parser.</param>
        /// <param name="errorMessage">The error message to display to the user.</param>
        /// <returns>A parse result item representing a failed parse.</returns>
        public static ParseResult<T> Failure(string input, string errorMessage = null)
        {
            return new ParseResult<T>(false, input, errorMessage: errorMessage);
        }
    }
}
