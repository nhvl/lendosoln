﻿using System;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Reminders;
using LendersOffice.Security;
using System.Collections.Generic;
using LendersOffice.ObjLib.Task;
using LendingQB.Core.Data;
using LqbGrammar.DataTypes;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    public class PointDataConversion
    {
        /// <summary>
        /// Use this to ensure the roundtripping when import and export loan agent name.
        /// </summary>
        /// 
        private Guid m_brokerId;
        private Guid loanId;
        private BrokerLoanAssignmentTable m_employeeTable = new BrokerLoanAssignmentTable();
        //private RoleList m_employeeRoles = RoleList.RetrieveFromCache(true);
        private CommonLib.Address m_addressParser;
        private CommonLib.Name m_nameParser;
        private CTriStateParser m_triStateParser = new CTriStateParser();
        private bool hasSubjectPropertyNetCashFlow = false;
        private decimal totalSubjectPropertyNetCashFlow = 0;

        private Guid m_defaultAssignLoanOfficerID;
        private Guid m_defaultAssignLoanProcessorID;
        private Guid m_defaultAssignLoanOpenerID;
        private Guid m_defaultAssignLenderAccountExecID;
        private Guid m_defaultAssignCallCenterAgentID;
        private Guid m_defaultAssignRealEstateAgentID;
        private Guid m_defaultAssignManagerID;
        private Guid m_defaultAssignLockDeskID;  //OPM 21891 av 
        private Guid m_defaultAssignCloserID;
        
        private Hashtable m_roleChanges = new Hashtable();

        static BrokerUserPrincipal BrokerUser
        {
            get { return BrokerUserPrincipal.CurrentPrincipal; }
        }

        public Guid DefaultAssignLoanOfficerID
        {
            get { return m_defaultAssignLoanOfficerID; }
            set { m_defaultAssignLoanOfficerID = value; }
        }

        public Guid DefaultAssignLoanProcessorID
        {
            get { return m_defaultAssignLoanProcessorID; }
            set { m_defaultAssignLoanProcessorID = value; }
        }

        public Guid DefaultAssignLoanOpenerID
        {
            get { return m_defaultAssignLoanOpenerID; }
            set { m_defaultAssignLoanOpenerID = value; }
        }

        public Guid DefaultAssignLenderAccountExecID
        {
            get { return m_defaultAssignLenderAccountExecID; }
            set { m_defaultAssignLenderAccountExecID = value;}
        }

        //OPM 21891 av 
        public Guid DefaultAssignLockDescId
        {
            get { return m_defaultAssignLockDeskID; }
            set { m_defaultAssignLockDeskID = value; }
        }
        public Guid DefaultAssignCallCenterAgentID
        {
            get { return m_defaultAssignCallCenterAgentID; }
            set { m_defaultAssignCallCenterAgentID = value; }
        }

        public Guid DefaultAssignRealEstateAgentID
        {
            get { return m_defaultAssignRealEstateAgentID; }
            set { m_defaultAssignRealEstateAgentID = value; }
        }

        public Guid DefaultAssignManagerID
        {
            get { return m_defaultAssignManagerID; }
            set { m_defaultAssignManagerID = value; }
        }
        public Guid DefaultAssignCloserID
        {
            get { return m_defaultAssignCloserID; }
            set { m_defaultAssignCloserID = value; }
        }
        public Hashtable RoleChanges
        {
            get
            {
                return m_roleChanges;
            }
        }

        /// <summary>
        /// Get or set whether the point importer will import the loan number from the point file.
        /// <value>True if importing the loan number from point false otherwise.</value>
        /// </summary>
        public bool UseLoanNumberFromPointFile
        {
            get;
            set;
        }

        public bool IsRolesImportedAsAgents = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="PointDataConversion"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="loanId">The loan id that will be imported/exported.</param>
        /// <remarks>
        /// 2018-11-07 - I (GF) added the loan id restriction because I needed to total up a
        /// value across all apps for a loan file. I didn't see the point import/export being
        /// used with more than a single file, so it seemed the easiest way to accomplish the
        /// loan-level total was to restrict the use of an instance to a single loan.
        /// </remarks>
        public PointDataConversion(Guid brokerId, Guid loanId)
        {
            this.loanId = loanId;
            this.UseLoanNumberFromPointFile = true;

            m_addressParser = new CommonLib.Address();
            m_nameParser = new CommonLib.Name();

            try
            {
                m_employeeTable.Retrieve(brokerId);
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to load broker role assignment table for " + brokerId + ".", e);
            }

            m_brokerId = brokerId;
        }

        private Guid MapToEmployeeId(string firstNm, string lastNm, E_RoleT roleType)
        {
            //Guid givenRoleId = role.Id;

            string givenFirstNm = firstNm.ToUpper();
            string givenLastNm = lastNm.ToUpper();
            IEnumerable<BrokerLoanAssignmentTable.Spec> employeeSet = m_employeeTable[roleType];

            if (employeeSet != null)
            {
                foreach (BrokerLoanAssignmentTable.Spec eS in employeeSet)
                {
                    String fNm = eS.FirstName.ToUpper();
                    String lNm = eS.LastName.ToUpper();

                    if (fNm == givenFirstNm && lNm == givenLastNm)
                    {
                        return eS.EmployeeId;
                    }
                }
            }



            return Guid.Empty;
        }

        // Used for OPM 9831 to find the Manager and Lender Account Exec relationship of the current
        // assigned Loan Officer
        private Guid GetRelationshipOfGivenUser(Guid employeeId, E_RoleT roleType)
        {
            if (employeeId == Guid.Empty)
            {
                return Guid.Empty;
            }

            //string roleDesc = Role.Get(roleType).Desc;

            BrokerUserPermissions buP = new BrokerUserPermissions();
            EmployeeRelationships eRl = new EmployeeRelationships();
            EmployeeRoles employeeRoles = new EmployeeRoles(BrokerUser.BrokerId, employeeId);

            buP.Retrieve(BrokerUser.BrokerId, BrokerUser.EmployeeId);

            eRl.Retrieve(BrokerUser.BrokerId, employeeId);

            if (employeeRoles.IsInRole(roleType) == true)
            {
                return employeeId;
            }

            var relationshipSet = eRl[RelationshipSetType.LendingQB];

            if (relationshipSet.Contains(roleType))
            {
                Guid matchingEmployeeId = relationshipSet[roleType].EmployeeId;
                EmployeeDetails eD = new EmployeeDetails();
                eD.Retrieve(BrokerUser.BrokerId, matchingEmployeeId);

                if (buP.HasPermission(Permission.AllowLoanAssignmentsToAnyBranch) == true || eD.BranchId == BrokerUser.BranchId)
                {
                    return matchingEmployeeId;
                }
            }
            
            return Guid.Empty;
        }

        /// <summary>
        /// The Agent data is stored in a data table. Currently I do not believe theres a way to delete an entry. 
        /// This method deletes the row from the given data table.  05/20/08 av 2199
        /// </summary>
        /// <param name="recordId"></param>
        /// <param name="dt"></param>
        private void RemoveRecordId(string recordId, DataTable dt)
        {
            for (int x = 0; x < dt.Rows.Count; x++)
            {
                DataRow row = dt.Rows[x];
                if (row["RecordId"] != null && row["RecordId"].ToString() == recordId)
                {
                    dt.Rows.Remove(row);
                    return;
                }
            }
        }

        /// <summary>
        ///	Overwrites the current agent of the roel with a new one. It returns a new empty agent object 
        ///	with the given row. 05/20/08 av opm 2199
        /// </summary>
        /// <param name="role"></param>
        /// <param name="dt"></param>
        /// <param name="loanData"></param>
        /// <returns></returns>
        private CAgentFields GetNewAgentFor(E_AgentRoleT role, DataTable dt, CPointData loanData)
        {
            CAgentFields agent = loanData.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (agent.IsValid && agent.AgentRoleT == role) //opm 118240 - Right now for broker rep if we find a loan officer instead loan officer is deleted. I should really add a ReturnEmptyDoNotFallback instead.
            {
                RemoveRecordId(agent.RecordId.ToString(), dt);
            }
            return loanData.GetAgentOfRole(role, E_ReturnOptionIfNotExist.CreateNewDoNotFallBack);
        }
        public void TransferDataFromPoint(CPointData loanData, NameValueCollectionWrapper input, int iApp, bool clearAgents)
        {
            if (loanData.sLId != this.loanId)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Attempted to import data into a loan file that does not match the loan id specified in the constructor.");
            }

            if (iApp == 0 && clearAgents)
            {
                loanData.sAgentDataSetClear();
            }
            TransferDataFromPoint(loanData, input, iApp);
        }
        /// <summary>
        /// Populate the loanData object with Point information from a NameValueCollection
        /// </summary>
        /// <param name="loanData"></param>
        /// <param name="input"></param>
        /// <param name="iApp">Borrower/Co-Borrower identifier. Starts with 0.</param>
        public void TransferDataFromPoint(CPointData loanData, NameValueCollectionWrapper input, int iApp)
        {
            if (loanData.sLId != this.loanId)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Attempted to import data into a loan file that does not match the loan id specified in the constructor.");
            }

            loanData.SetFormatTarget(FormatTarget.PointNative);
            int i;
            BrokerDB broker = BrokerDB.RetrieveById(m_brokerId);

            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (principal == null)
            {
                throw new AccessDenied();
            }

            m_roleChanges.Clear();

            LosConvert convertLos = loanData.m_convertLos;
            string val;
            #region SHARED FIELDS
            if (0 == iApp)
            {
                this.hasSubjectPropertyNetCashFlow = false;
                this.totalSubjectPropertyNetCashFlow = 0;
                //05/20/08 av opm 2199
                //loanData.sAgentDataSetClear();
                loanData.sCondDataSetClear();
                loanData.sPreparerDataSetClear();
                if (string.IsNullOrEmpty(input.Shared["1"]) == false && UseLoanNumberFromPointFile)
                {
                    loanData.SetsLNmWithPermissionBypass(input.Shared["1"]);
                }

                StringBuilder strBuilder = new StringBuilder(100);

                loanData.sVaTitleLimitDesc = CombineFieldsToLines(input.Shared, 7805, 7807);
                loanData.sVaTitleLimitIsCondo = "X" == input.Shared["7808"];
                loanData.sVaTitleLimitIsPud = "X" == input.Shared["7809"];
                loanData.sSpLotDimension = input.Shared["7908"];
                loanData.sSpLotIsIrregular = "X" == input.Shared["7810"];
                loanData.sSpLotIrregularSqf = input.Shared["7811"];
                loanData.sSpLotIsAcres = "X" == input.Shared["7812"];
                loanData.sSpLotAcres = input.Shared["7813"];
                loanData.sMersMin = input.Shared["22"];

                E_sSpUtilT spUtilT; // public, community, individual
                if ("X" == input.Shared["7814"])
                    spUtilT = E_sSpUtilT.Public;
                else if ("X" == input.Shared["7818"])
                    spUtilT = E_sSpUtilT.Community;
                else if ("X" == input.Shared["7822"])
                    spUtilT = E_sSpUtilT.Individual;
                else
                    spUtilT = E_sSpUtilT.LeaveBlank;
                loanData.sSpUtilElecT = spUtilT;

                if ("X" == input.Shared["7815"])
                    spUtilT = E_sSpUtilT.Public;
                else if ("X" == input.Shared["7819"])
                    spUtilT = E_sSpUtilT.Community;
                else if ("X" == input.Shared["7823"])
                    spUtilT = E_sSpUtilT.Individual;
                else
                    spUtilT = E_sSpUtilT.LeaveBlank;
                loanData.sSpUtilGasT = spUtilT;

                if ("X" == input.Shared["7816"])
                    spUtilT = E_sSpUtilT.Public;
                else if ("X" == input.Shared["7820"])
                    spUtilT = E_sSpUtilT.Community;
                else if ("X" == input.Shared["7824"])
                    spUtilT = E_sSpUtilT.Individual;
                else
                    spUtilT = E_sSpUtilT.LeaveBlank;
                loanData.sSpUtilWaterT = spUtilT;

                // int not null default( 0 ) --,,
                if ("X" == input.Shared["7817"])
                    spUtilT = E_sSpUtilT.Public;
                else if ("X" == input.Shared["7821"])
                    spUtilT = E_sSpUtilT.Community;
                else if ("X" == input.Shared["7825"])
                    spUtilT = E_sSpUtilT.Individual;
                else
                    spUtilT = E_sSpUtilT.LeaveBlank;
                loanData.sSpUtilSanSewerT = spUtilT;

                loanData.sSpHasOven = "X" == input.Shared["7826"];
                loanData.sSpHasRefrig = "X" == input.Shared["7827"];
                loanData.sSpHasDishWasher = "X" == input.Shared["7828"];
                loanData.sSpHasClothesWasher = "X" == input.Shared["7829"];
                loanData.sSpHasDryer = "X" == input.Shared["7830"];
                loanData.sSpHasGarbageDisposal = "X" == input.Shared["7831"];
                loanData.sSpHasVentFan = "X" == input.Shared["7832"];
                loanData.sSpHasWwCarpet = "X" == input.Shared["7833"];
                loanData.sSpHasOtherEquip = "X" == input.Shared["7909"];
                loanData.sSpOtherEquipDesc = input.Shared["7910"];
                //sVaBuildingStatusT int not null default( 0 ), --7834:proposed,7836:Existing,7837:UnderConstruction,7838:Repair. Calc of sBuildingStatusT
                E_sVaBuildingStatusT vaBuildStatT;
                if ("X" == input.Shared["7834"])
                    vaBuildStatT = E_sVaBuildingStatusT.Proposed;
                else if ("X" == input.Shared["7836"])
                    vaBuildStatT = E_sVaBuildingStatusT.Existing;
                else if ("X" == input.Shared["7837"])
                    vaBuildStatT = E_sVaBuildingStatusT.UnderConstruction;
                else if ("X" == input.Shared["7838"])
                    vaBuildStatT = E_sVaBuildingStatusT.Repair;
                else
                    vaBuildStatT = E_sVaBuildingStatusT.LeaveBlank;
                loanData.sVaBuildingStatusT = vaBuildStatT;
                loanData.sVaBuildingStatusTLckd = E_sVaBuildingStatusT.LeaveBlank != vaBuildStatT;

                E_sVaBuildingT vaBuildT;
                if ("X" == input.Shared["7839"])
                    vaBuildT = E_sVaBuildingT.Detached;
                else if ("X" == input.Shared["7840"])
                    vaBuildT = E_sVaBuildingT.SemiDetached;
                else if ("X" == input.Shared["7841"])
                    vaBuildT = E_sVaBuildingT.Row;
                else if ("X" == input.Shared["7842"])
                    vaBuildT = E_sVaBuildingT.AptUnit;
                else
                    vaBuildT = E_sVaBuildingT.LeaveBlank;
                loanData.sVaBuildingT = vaBuildT;

                loanData.sVaIsFactoryFabricatedTri = FromXYesToTriState(input.Shared["7843"], input.Shared["7844"]);
                loanData.sVaNumOfBuildings = input.Shared["7845"];
                loanData.sVaNumOfLivingUnits = input.Shared["7846"];
                loanData.sVaStreetAccessPrivateTri = FromXYesToTriState(input.Shared["7847"], input.Shared["7848"]);
                loanData.sVaStreetMaintenancePrivateTri = FromXYesToTriState(input.Shared["7849"], input.Shared["7850"]);
                loanData.sVaConstructWarrantyTri = PointDataConversion.FromXYesToTriState(input.Shared["7851"], input.Shared["7852"]);
                loanData.sVaConstructWarrantyProgramNm = input.Shared["7853"];
                loanData.sVaConstructExpiredD_rep = input.Shared["7854"];
                loanData.sVaConstructCompleteD_rep = input.Shared["7855"];
                loanData.sVaOwnerNm = input.Shared["7856"];

                E_sVaSpOccT vaSpOccT;
                if ("X" == input.Shared["7857"])
                    vaSpOccT = E_sVaSpOccT.OccByOwner;
                else if ("X" == input.Shared["7858"])
                    vaSpOccT = E_sVaSpOccT.NeverOcc;
                else if ("X" == input.Shared["7859"])
                    vaSpOccT = E_sVaSpOccT.Vacant;
                else if ("X" == input.Shared["7860"])
                    vaSpOccT = E_sVaSpOccT.Rental;
                else
                    vaSpOccT = E_sVaSpOccT.LeaveBlank;
                loanData.sVaSpOccT = vaSpOccT;

                loanData.sVaSpRentalMonthly_rep = input.Shared["7861"];
                loanData.sVaSpOccupantNm = input.Shared["7862"];
                loanData.sVaSpOccupantPhone = input.Shared["7863"];
                //--Export the name of broker (7864)and telephone(7865)
                loanData.sVaSpAvailableForInspectDesc = input.Shared["7866"];
                E_sVaSpAvailableForInspectTimeT vaInspectTimeT;
                if ("X" == input.Shared["7867"])
                    vaInspectTimeT = E_sVaSpAvailableForInspectTimeT.AM;
                else if ("X" == input.Shared["7878"])
                    vaInspectTimeT = E_sVaSpAvailableForInspectTimeT.PM;
                else
                    vaInspectTimeT = E_sVaSpAvailableForInspectTimeT.LeaveBlank;
                loanData.sVaSpAvailableForInspectTimeT = vaInspectTimeT;//int not null default( 0 ), --7867:am,7878:pm, need "both" option as well
                //--7869,7870 (keys at address, use preparer)
                //--item 25: use sFHALenderIdCode
                //--item 26: use sFHASponsorAgentIdCode
                //--item 27: use sFHACondCommInstCaseRef

                E_sVaConstructComplianceInspectionMadeByT vaInspectMadeByT;
                if ("X" == input.Shared["7874"])
                    vaInspectMadeByT = E_sVaConstructComplianceInspectionMadeByT.Fha;
                else if ("X" == input.Shared["7875"])
                    vaInspectMadeByT = E_sVaConstructComplianceInspectionMadeByT.Va;
                else if ("X" == input.Shared["7876"])
                    vaInspectMadeByT = E_sVaConstructComplianceInspectionMadeByT.None;
                else
                    vaInspectMadeByT = E_sVaConstructComplianceInspectionMadeByT.LeaveBlank;
                loanData.sVaConstructComplianceInspectionMadeByT = vaInspectMadeByT;

                loanData.sVaConstructPlansFirstSubmitTri = FromXYesToTriState(input.Shared["7877"], input.Shared["7878"]);
                loanData.sVaConstructPrevPlansCaseNum = input.Shared["7879"];
                //--builder:7880:name,7882:street address,7883:city,state,and zip,7884:phone
                //--warrantor:7885:name,7887:streetAddr,7888,citystatezip,7889:phone
                loanData.sVaSpecialAssessmentsComments = CombineFieldsToLines(input.Shared, new int[] { 7881, 7890, 7891 });
                //--item 31: use sProRealETxPerYr
                loanData.sSpMineralRightsReservedTri = FromXYesToTriState(input.Shared["7893"], input.Shared["7895"]);
                loanData.sSpMineralRightsReservedExplain = CombineFieldsToLines(input.Shared, new int[] { 7894, 7907 });
                loanData.sSpLeaseIs99Yrs = "X" == input.Shared["7896"];
                loanData.sSpLeaseIsRenewable = "X" == input.Shared["7897"];
                //--item 33b: use sLeaseHoldExpireD, need to export to 7899
                loanData.sSpLeaseAnnualGroundRent_rep = input.Shared["7900"];
                //--item 34a: sPurchPrice_rep
                loanData.sLotPurchaseSeparatelyTri = FromXYesToTriState(input.Shared["7901"], input.Shared["7902"]);
                loanData.sVaRefiAmt_rep = input.Shared["7903"];
                loanData.sVaSaleContractAttachedTri = FromXYesToTriState(input.Shared["7904"], input.Shared["7905"]);
                loanData.sVaPrevApprovedContractNum = input.Shared["7906"];
                //--Exporting the preparer at the end of the Reasonable Value form 7920, 7921,7922,7923

                //---------------
                loanData.sVaIrrrlsUsedOnlyPdInFullLNum = input.Shared["8378"];
                loanData.sVaIrrrlsUsedOnlyOrigLAmt_rep = input.Shared["8385"];
                loanData.sVaIrrrlsUsedOnlyOrigIR_rep = input.Shared["8386"];
                loanData.sVaIrrrlsUsedOnlyRemarks = CombineFieldsToLines(input.Shared, 8380, 8384);
                //sVaFfExemptTri tinyint not null default( 0 ) --Point doesn't have this, it always default to NO
                loanData.sVaRefiWsExistingVaLBal_rep = input.Shared["7680"];
                loanData.sVaRefiWsCashPmtFromVet_rep = input.Shared["7681"];

                loanData.sVaRefiWsDiscntPc_rep = input.Shared["7683"];

                loanData.sVaRefiWsOrigFeePc_rep = input.Shared["7685"];

                loanData.sVaRefiWsFfPc_rep = input.Shared["7687"];

                loanData.sVaRefiWsAllowableCcAndPp_rep = input.Shared["7689"];

                loanData.sVaRefiWsFinalDiscntPc_rep = input.Shared["7691"];
                loanData.sVaIsAutoIrrrlProc = input.Shared["8301"] == "X";

                E_sVaLPurposeT sVaLPurpT;
                if ("X" == input.Shared["8315"])
                    sVaLPurpT = E_sVaLPurposeT.Alteration;
                else if ("X" == input.Shared["8314"])
                    sVaLPurpT = E_sVaLPurposeT.Condo;
                else if ("X" == input.Shared["8312"])
                    sVaLPurpT = E_sVaLPurposeT.Home;
                else if ("X" == input.Shared["8313"])
                    sVaLPurpT = E_sVaLPurposeT.ManufacturedHome;
                else if ("X" == input.Shared["8316"])
                    sVaLPurpT = E_sVaLPurposeT.Refinance;
                else
                    sVaLPurpT = E_sVaLPurposeT.LeaveBlank;
                loanData.sVaLPurposeT = sVaLPurpT;
                loanData.sVaLPurposeTLckd = E_sVaLPurposeT.LeaveBlank != sVaLPurpT;

                E_sVaLCodeT vaCodeT;

                if ("X" == input.Shared["8325"])
                    vaCodeT = E_sVaLCodeT.Purchase;
                else if ("X" == input.Shared["8326"])
                    vaCodeT = E_sVaLCodeT.Irrrl;
                else if ("X" == input.Shared["8327"])
                    vaCodeT = E_sVaLCodeT.CashoutRefin;
                else if ("X" == input.Shared["8328"])
                    vaCodeT = E_sVaLCodeT.ManufacturedHomeRefi;
                else if ("X" == input.Shared["8329"])
                    vaCodeT = E_sVaLCodeT.RefiOver90Rv;
                else
                    vaCodeT = E_sVaLCodeT.LeaveBlank;
                loanData.sVaLCodeT = vaCodeT;
                loanData.sVaLCodeTLckd = E_sVaLCodeT.LeaveBlank != vaCodeT;

                E_sVaFinMethT vaFinMethT;
                if ("X" == input.Shared["8330"])
                    vaFinMethT = E_sVaFinMethT.RegularFixed;
                else if ("X" == input.Shared["8331"])
                    vaFinMethT = E_sVaFinMethT.GpmNeverExceedCrv;
                else if ("X" == input.Shared["8332"])
                    vaFinMethT = E_sVaFinMethT.GpmOther;
                else if ("X" == input.Shared["8333"])
                    vaFinMethT = E_sVaFinMethT.Gem;
                else if ("X" == input.Shared["8334"])
                    vaFinMethT = E_sVaFinMethT.TemporaryBuydown;
                else if ("X" == input.Shared["8335"])
                    vaFinMethT = E_sVaFinMethT.HybridArm;
                else
                    vaFinMethT = E_sVaFinMethT.LeaveBlank;
                loanData.sVaFinMethT = vaFinMethT;
                loanData.sVaFinMethTLckd = E_sVaFinMethT.LeaveBlank != vaFinMethT;

                E_sVaHybridArmT vaHybridArmT;
                switch (input.Shared["8291"].TrimWhitespaceAndBOM())
                {
                    case "3/1": vaHybridArmT = E_sVaHybridArmT.ThreeOne; break;
                    case "5/1": vaHybridArmT = E_sVaHybridArmT.FiveOne; break;
                    case "7/11": vaHybridArmT = E_sVaHybridArmT.SevenEleven; break;
                    case "10/1": vaHybridArmT = E_sVaHybridArmT.TenOne; break;
                    case "": vaHybridArmT = E_sVaHybridArmT.LeaveBlank; break;
                    default:
                        vaHybridArmT = E_sVaHybridArmT.LeaveBlank;
                        Tools.LogBug(string.Format("Point file has value {0}, that we cannot represent it with E_sVaHybridArmT", input.Shared["8291"]));
                        break;
                }
                loanData.sVaHybridArmT = vaHybridArmT;

                E_sVaOwnershipT vaOwnershipT;
                if ("X" == input.Shared["8317"])
                    vaOwnershipT = E_sVaOwnershipT.SoleOwnership;
                else if ("X" == input.Shared["8318"])
                    vaOwnershipT = E_sVaOwnershipT.JointWithOtherVets;
                else if ("X" == input.Shared["8319"])
                    vaOwnershipT = E_sVaOwnershipT.JointWithNonVet;
                else
                    vaOwnershipT = E_sVaOwnershipT.LeaveBlank;
                loanData.sVaOwnershipT = vaOwnershipT;

                //input.Shared["8345"] = loanData.sVaEnergyImprovNone ? "X" : "";
                loanData.sVaEnergyImprovIsSolar = "X" == input.Shared["8346"];
                loanData.sVaEnergyImprovIsMajorSystem = "X" == input.Shared["8347"];
                loanData.sVaEnergyImprovIsNewFeature = "X" == input.Shared["8348"];
                loanData.sVaEnergyImprovIsInsulation = "X" == input.Shared["8349"];
                loanData.sVaEnergyImprovIsOther = "X" == input.Shared["8350"];
                loanData.sVaEnergyImprovAmt_rep = input.Shared["8351"];

                E_sVaApprT vaApprT;
                if ("X" == input.Shared["8352"])
                    vaApprT = E_sVaApprT.SingleProp;
                else if ("X" == input.Shared["8353"])
                    vaApprT = E_sVaApprT.MasterCrvCase;
                else if ("X" == input.Shared["8354"])
                    vaApprT = E_sVaApprT.LappLenderAppr;
                else if ("X" == input.Shared["8355"])
                    vaApprT = E_sVaApprT.ManufacturedHome;
                else if ("X" == input.Shared["8356"])
                    vaApprT = E_sVaApprT.HudVaConversion;
                else if ("X" == input.Shared["8457"])// TODO: Review this one, might be just 8357, Point shows 8457 though
                    vaApprT = E_sVaApprT.PropMgmtCase;
                else
                    vaApprT = E_sVaApprT.LeaveBlank;
                loanData.sVaApprT = vaApprT;

                E_sVaStructureT vaStructureT;
                if ("X" == input.Shared["8361"])
                    vaStructureT = E_sVaStructureT.Conventional;
                else if ("X" == input.Shared["8362"])
                    vaStructureT = E_sVaStructureT.SingleWideMobil;
                else if ("X" == input.Shared["8363"])
                    vaStructureT = E_sVaStructureT.DoubleWideMobil;
                else if ("X" == input.Shared["8364"])
                    vaStructureT = E_sVaStructureT.MobilLotOnly;
                else if ("X" == input.Shared["8365"])
                    vaStructureT = E_sVaStructureT.PrefabricatedHome;
                else if ("X" == input.Shared["8366"])
                    vaStructureT = E_sVaStructureT.CondoConversion;
                else
                    vaStructureT = E_sVaStructureT.LeaveBlank;
                loanData.sVaStructureT = vaStructureT;

                E_sVaPropDesignationT vaPropDesT;
                if ("X" == input.Shared["8367"])
                    vaPropDesT = E_sVaPropDesignationT.ExistingOcc;
                else if ("X" == input.Shared["8368"])
                    vaPropDesT = E_sVaPropDesignationT.ProposedConstruction;
                else if ("X" == input.Shared["8369"])
                    vaPropDesT = E_sVaPropDesignationT.ExistingNew;
                else if ("X" == input.Shared["8370"])
                    vaPropDesT = E_sVaPropDesignationT.EnergyImprovement;
                else
                    vaPropDesT = E_sVaPropDesignationT.LeaveBlank;
                loanData.sVaPropDesignationT = vaPropDesT;

                loanData.sVaMcrvNum = input.Shared["8371"];

                E_sVaManufacturedHomeT vaManuHomeT;
                if ("X" == input.Shared["8357"])
                    vaManuHomeT = E_sVaManufacturedHomeT.NotMobilHome;
                else if ("X" == input.Shared["8358"])
                    vaManuHomeT = E_sVaManufacturedHomeT.MobilHomeOnly;
                else if ("X" == input.Shared["8359"])
                    vaManuHomeT = E_sVaManufacturedHomeT.MobilHomeVetOwnedLot;
                else if ("X" == input.Shared["8360"])
                    vaManuHomeT = E_sVaManufacturedHomeT.MobilHomeOnPermanentFoundation;
                else
                    vaManuHomeT = E_sVaManufacturedHomeT.LeaveBlank;
                loanData.sVaManufacturedHomeT = vaManuHomeT;

                loanData.sVaLenSarId = input.Shared["8548"];// varchar(50) not null default( '' ), --Lender Sar ID:8548
                loanData.sVaSarNotifIssuedD_rep = input.Shared["8299"];

                switch (input.Shared["8295"].TrimWhitespaceAndBOM().ToUpper())
                {
                    case "YES": loanData.sVaAppraisalOrSarAdjustmentTri = E_TriState.Yes; break;
                    case "NO": loanData.sVaAppraisalOrSarAdjustmentTri = E_TriState.No; break;
                    default: loanData.sVaAppraisalOrSarAdjustmentTri = E_TriState.Blank; break;
                }

                E_sVaAutoUnderwritingT vaAutoUnderwriteT;
                switch (input.Shared["8294"].TrimWhitespaceAndBOM().ToUpper())
                {
                    case "LP": vaAutoUnderwriteT = E_sVaAutoUnderwritingT.Lp; break;
                    case "DU": vaAutoUnderwriteT = E_sVaAutoUnderwritingT.Du; break;
                    case "PMI AURA": vaAutoUnderwriteT = E_sVaAutoUnderwritingT.PmiAura; break;
                    case "CLUES": vaAutoUnderwriteT = E_sVaAutoUnderwritingT.Clues; break;
                    case "ZIPPY": vaAutoUnderwriteT = E_sVaAutoUnderwritingT.Zippy; break;
                    case "": vaAutoUnderwriteT = E_sVaAutoUnderwritingT.LeaveBlank; break;
                    default:
                        vaAutoUnderwriteT = E_sVaAutoUnderwritingT.LeaveBlank;
                        Tools.LogBug(string.Format("Point file has value {0} that we cannot represent with E_sVaAutoUnderwritingT. In Point Import", input.Shared["8294"]));
                        break;
                }
                loanData.sVaAutoUnderwritingT = vaAutoUnderwriteT;

                switch (input.Shared["8293"].TrimWhitespaceAndBOM().ToUpper())
                {
                    case "REFER": loanData.sVaRiskT = E_sVaRiskT.Refer; break;
                    case "APPROVE": loanData.sVaRiskT = E_sVaRiskT.Approve; break;
                    case "": loanData.sVaRiskT = E_sVaRiskT.LeaveBlank; break;
                    default: Tools.LogBug(string.Format("Point file has value {0} that we cannot represent with sVaRiskT. In Point import.", input.Shared["8293"]));
                        break;
                }

                loanData.sVaVetMedianCrScore_rep = input.Shared["8292"];//int not null default( 0 ), --8292, score of vet only, add this to Credit Scores screen
                loanData.sVaVetMedianCrScoreLckd = "" != input.Shared["8292"];
                loanData.sVaLDiscntPc_rep = input.Shared["8374"];// decimal(9,3) not null default( 0 ), --:calc based on sLDiscntPc and sVaLDiscntLckd
                loanData.sVaLDiscnt_rep = input.Shared["8375"]; // money not null default( 0 ), --: calc based on sLDiscnt and sVaLDiscntLckd
                loanData.sVaLDiscntLckd = ("" != input.Shared["8374"]) || ("" != input.Shared["8375"]);

                loanData.sVaLDiscntPcPbb_rep = input.Shared["8376"];//(9,3) not null default( 0 ), --8376:calc based sVaLDiscntPc and sVaLDsicntPbbLckd
                loanData.sVaLDiscntPbb_rep = input.Shared["8377"];// money not null default( 0 ), --8377:calc based sVaLDiscnt and sVaLDsicntPbbLckd
                loanData.sVaLDiscntPbbLckd = ("" != input.Shared["8376"]) || ("" != input.Shared["8377"]);
                //-----------------

                val = input.Shared["2196"];
                loanData.sVaCashdwnPmt_rep = val;
                loanData.sVaCashdwnPmtLckd = "" != val;

                val = input.Shared["7741"];
                loanData.sVaProThisMPmt_rep = val;
                loanData.sVaProThisMPmtLckd = "" != val;

                val = input.Shared["7742"];
                loanData.sVaProRealETx_rep = val;
                loanData.sVaProRealETxLckd = "" != val;

                val = input.Shared["7743"];
                loanData.sVaProHazIns_rep = val;
                loanData.sVaProHazInsLckd = "" != val;

                loanData.sVaProMaintenancePmt_rep = input.Shared["7745"];
                loanData.sVaProUtilityPmt_rep = input.Shared["7746"];

                loanData.sProFloodInsFaceAmt_rep = input.Shared["9365"];
                loanData.sVaAgent1RoleDesc = input.Shared["9381"];
                loanData.sVaAgent2RoleDesc = input.Shared["9385"];
                loanData.sVaAgent3RoleDesc = input.Shared["9389"];
                loanData.sVaAgent4RoleDesc = input.Shared["9393"];
                loanData.sVaAgent5RoleDesc = input.Shared["9397"];

                loanData.sVaIsAutoProc = "X" == input.Shared["8300"];
                loanData.sVaIsPriorApprovalProc = "X" == input.Shared["8302"];

                loanData.sLenderCaseNum = input.Shared["327"];

                val = input.Shared["9350"];
                loanData.sVaLenderCaseNum = val;
                loanData.sVaLenderCaseNumLckd = (val != "") && (val != loanData.sLenderCaseNum);

                loanData.sVaIsGuarantyEvidenceRequested = "X" == input.Shared["9352"];
                loanData.sVaIsInsuranceEvidenceRequested = "X" == input.Shared["9353"];
                loanData.sLNoteD_rep = input.Shared["9354"];
                loanData.sLProceedPaidOutD_rep = input.Shared["9355"];

                val = input.Shared["9356"];
                loanData.sMaturityD_rep = val;
                loanData.sMaturityDLckd = "" != val;

                if ("X" == input.Shared["9357"])
                {
                    loanData.sVaLienPosT = E_sVaLienPosT.FirstChattel;
                    loanData.sVaLienPosTLckd = true;
                }
                else if ("X" == input.Shared["9358"])
                {
                    loanData.sVaLienPosT = E_sVaLienPosT.Unsecured;
                    loanData.sVaLienPosTLckd = true;
                }
                else if ("X" == input.Shared["9359"])
                {
                    loanData.sVaLienPosT = E_sVaLienPosT.Other;
                    loanData.sVaLienPosTLckd = true;
                }
                else // first and second are taken care by sLienPosT field
                    loanData.sVaLienPosTLckd = false;

                loanData.sVaLienPosOtherDesc = input.Shared["9360"];

                if ("X" == input.Shared["9361"])
                {
                    loanData.sVaEstateHeldT = E_sVaEstateHeldT.Other;
                    loanData.sVaEstateHeldTLckd = true;
                }
                else // fee and leasehold are taken care by sEstateHeldT
                    loanData.sVaEstateHeldTLckd = false;

                loanData.sVaEstateHeldOtherDesc = input.Shared["9362"];
                val = input.Shared["7892"];
                if ("" != val)
                {
                    loanData.sProRealETxPerYr_rep = val;
                    loanData.sProRealETxPerYrLckd = true;
                }
                else
                    loanData.sProRealETxPerYrLckd = false;

                loanData.sProHazInsFaceAmt_rep = input.Shared["9363"];

                val = input.Shared["9364"];
                if ("" != val)
                {
                    loanData.sProHazInsPerYr_rep = val;
                    loanData.sProHazInsPerYrLckd = true;
                }
                else
                    loanData.sProHazInsPerYrLckd = false;

                val = input.Shared["9365"];
                if ("" != val)
                {
                    loanData.sProFloodInsPerYr_rep = val;
                    loanData.sProFloodInsPerYrLckd = true;
                }
                else
                    loanData.sProFloodInsPerYrLckd = false;

                //loanData.sVaSpecialAssessPmtPerYear_rep = input.Shared["9367"];
                loanData.sVaSpecialAssessPmt_rep = input.Shared["7744"];
                loanData.sVaSpecialAssessUnpaid_rep = input.Shared["9368"];

                val = input.Shared["9369"];
                if ("" != val)
                {
                    loanData.sVaMaintainAssessPmtPerYear_rep = val;
                    loanData.sVaMaintainAssessPmtPerYearLckd = true;
                }
                else
                    loanData.sVaMaintainAssessPmtPerYearLckd = false;

                loanData.sVaMaintainAssessPmt_rep = input.Shared["7747"];

                val = input.Shared["9370"];
                loanData.sVaNonrealtyAcquiredWithLDesc = val;
                loanData.sVaNonrealtyAcquiredWithLDescPrintSeparately = val.Length > 150;

                val = input.Shared["9371"];
                loanData.sVaAdditionalSecurityTakenDesc = val;
                loanData.sVaAdditionalSecurityTakenDescPrintSeparately = val.Length > 150;

                loanData.sLotAcquiredD_rep = input.Shared["9372"];
                loanData.sVaLotPurchPrice_rep = input.Shared["9373"];

                if ("X" == input.Shared["9374"])
                    loanData.sVaLProceedDepositT = E_sVaLProceedDepositT.Escrow;
                else if ("X" == input.Shared["9375"])
                    loanData.sVaLProceedDepositT = E_sVaLProceedDepositT.EarmarkedAcc;
                else
                    loanData.sVaLProceedDepositT = E_sVaLProceedDepositT.Blank;

                loanData.sVaLProceedDepositDesc = input.Shared["9376"];
                loanData.sIsAlterationCompleted = "X" == input.Shared["9398"];
                //---------------

                val = input.Shared["2791"];
                loanData.sHcltvR_rep = val;
                loanData.sHcltvRLckd = "" != val;

                val = input.Shared["2790"];
                loanData.sDebtToHousingGapR_rep = val;
                loanData.sDebtToHousingGapRLckd = "" != val;
                loanData.sApprFull = "X" == input.Shared["2051"];
                loanData.sApprDriveBy = "X" == input.Shared["2052"];
                loanData.sIsSpReviewNoAppr = "X" == input.Shared["2057"];
                loanData.sSpReviewFormNum = input.Shared["2058"];
                loanData.sTransmFntc_rep = input.Shared["2814"];
                loanData.sTransmFntcLckd = true;
                loanData.sVerifAssetAmt_rep = input.Shared["2815"];
                loanData.sFntcSrc = input.Shared["2816"];
                loanData.sRsrvMonNumDesc = input.Shared["2817"];
                loanData.sInterestedPartyContribR_rep = input.Shared["2818"];
                loanData.sIsManualUw = "X" == input.Shared["2792"];
                loanData.sIsDuUw = "X" == input.Shared["2793"];
                loanData.sIsLpUw = "X" == input.Shared["2794"];
                loanData.sIsOtherUw = "X" == input.Shared["2795"];
                loanData.sOtherUwDesc = input.Shared["2796"];
                loanData.sAusRecommendation = input.Shared["2797"];
                loanData.sLpAusKey = input.Shared["2798"];
                loanData.sDuCaseId = input.Shared["3890"]; //av opm 20261 changed from 2798 
                loanData.sLpDocClass = input.Shared["2799"];
                loanData.sRepCrScore = input.Shared["2836"];
                loanData.sIsCommLen = "X" == input.Shared["8870"];
                loanData.sIsHOwnershipEdCertInFile = "X" == input.Shared["2813"];
                loanData.sIsMOrigBroker = "X" == input.Shared["2767"];
                loanData.sIsMOrigCorrespondent = "X" == input.Shared["2768"];
                loanData.sTransmBuydwnTermDesc = input.Shared["2789"];


                E_sFredProcPointT procPointT;
                switch (input.Shared["5703"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "prequal (no urla)":
                        procPointT = E_sFredProcPointT.Prequalification;
                        break;
                    case "application/processing":
                        procPointT = E_sFredProcPointT.Application;
                        break;
                    case "final disposition":
                        procPointT = E_sFredProcPointT.FinalDisposition;
                        break;
                    case "post-closing qc":
                        procPointT = E_sFredProcPointT.PostClosingQualityControl;
                        break;
                    case "underwriting":
                        procPointT = E_sFredProcPointT.Underwriting;
                        break;
                    default:
                        procPointT = E_sFredProcPointT.LeaveBlank;
                        break;
                }
                loanData.sFredProcPointT = procPointT;

                // 7/10/2006 dd - POINT use 2729 for the property type in Transmittal Summary
                E_sGseSpT gseSpT;
                switch (input.Shared["2729"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "1": // "1 unit"
                        gseSpT = E_sGseSpT.Attached;
                        break;
                    case "3": // "condo":
                        gseSpT = E_sGseSpT.Condominium;
                        break;
                    case "4": // "pud":
                        gseSpT = E_sGseSpT.PUD;
                        break;
                    case "5": //"co-op":
                        gseSpT = E_sGseSpT.Cooperative;
                        break;
                    case "7": //"manufactured multiwide":
                        gseSpT = E_sGseSpT.ManufacturedHomeMultiwide;
                        break;
                    case "6": //"manufactured single wide":
                        gseSpT = E_sGseSpT.ManufacturedHousingSingleWide;
                        break;
                    default:
                        gseSpT = E_sGseSpT.LeaveBlank;
                        break;
                }
                /*
				E_sGseSpT gseSpT;
				switch( input.Shared["5705"].TrimWhitespaceAndBOM().ToLower() )
				{
					case "low-rise condo (up to 4)":
						gseSpT = E_sGseSpT.Condominium;
						break;
					case "high rise condo (5 or more)":
						gseSpT = E_sGseSpT.HighRiseCondominium;
						break;
					case "single family detached":
						gseSpT = E_sGseSpT.Detached;
						break;
					case "single family attached":
						gseSpT = E_sGseSpT.Attached;
						break;
					case "single family attached pud":
						gseSpT = E_sGseSpT.PUD;
						break;
					case "single family detached pud":
						gseSpT = E_sGseSpT.PUD;
						break;
					default:
					case "2 to 4 family":
					case "multifamily":
						gseSpT = E_sGseSpT.LeaveBlank;
						break;
					case "cooperative":
						gseSpT = E_sGseSpT.Cooperative;
						break;
					case "manufactured home singlewide":
						gseSpT = E_sGseSpT.ManufacturedHousingSingleWide;
						break;
					case "manufactured home multiwide":
						gseSpT = E_sGseSpT.ManufacturedHomeMultiwide;
						break;
				}
                */
                loanData.sGseSpT = gseSpT;



                loanData.sSpMarketVal_rep = input.Shared["2904"];

                E_sBuildingStatusT buildingStatusT;
                switch (input.Shared["5721"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "existing":
                        buildingStatusT = E_sBuildingStatusT.Existing;
                        break;
                    case "proposed":
                        buildingStatusT = E_sBuildingStatusT.Proposed;
                        break;
                    case "alteration, improvement, repair":
                        buildingStatusT = E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab;
                        break;
                    case "substantially rehabilitated":
                        buildingStatusT = E_sBuildingStatusT.SubstantiallyRehabilitated;
                        break;
                    case "under construction":
                        buildingStatusT = E_sBuildingStatusT.UnderConstruction;
                        break;
                    default:
                        buildingStatusT = E_sBuildingStatusT.LeaveBlank;
                        break;
                }
                loanData.sBuildingStatusT = buildingStatusT;

                loanData.sHelocCreditLimit_rep = input.Shared["5778"];
                loanData.sHelocBal_rep = input.Shared["5777"];

                E_sFreddieDocT fredDocT;
                switch (input.Shared["5738"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "full documentation":
                        fredDocT = E_sFreddieDocT.FullDocumentation;
                        break;
                    case "no dv, ev or iv":
                        fredDocT = E_sFreddieDocT.NoDepEmpIncVerif;
                        break;
                    case "no dv":
                        fredDocT = E_sFreddieDocT.NoDepositVerif;
                        break;
                    case "no ev or iv":
                        fredDocT = E_sFreddieDocT.NoEmpIncVerif;
                        break;
                    default:
                        fredDocT = E_sFreddieDocT.LeaveBlank;
                        break;
                }
                loanData.sFreddieDocT = fredDocT;
                loanData.sMICoveragePc_rep = input.Shared["5743"];
                loanData.sFredAffordProgId = input.Shared["5733"];

                E_sFreddieArmIndexT fredIndexT;
                switch (input.Shared["5736"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "1-year treasury":
                        fredIndexT = E_sFreddieArmIndexT.OneYearTreasury;
                        break;
                    case "3-year treasury":
                        fredIndexT = E_sFreddieArmIndexT.ThreeYearTreasury;
                        break;
                    case "6-month t-bill":
                        fredIndexT = E_sFreddieArmIndexT.SixMonthTreasury;
                        break;
                    case "cofi (11th district monthly cof)":
                        fredIndexT = E_sFreddieArmIndexT.EleventhDistrictCostOfFunds;
                        break;
                    case "national median cof":
                        fredIndexT = E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds;
                        break;
                    case "libor":
                        fredIndexT = E_sFreddieArmIndexT.LIBOR;
                        break;
                    default:
                    case "other":
                        fredIndexT = E_sFreddieArmIndexT.Other;
                        break;
                }
                loanData.sFreddieArmIndexT = fredIndexT;
                loanData.sFreddieArmIndexTLckd = true;

                E_sBuydownContributorT buydownContributorT;
                switch (input.Shared["5737"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "borrower":
                        buydownContributorT = E_sBuydownContributorT.Borrower;
                        break;
                    case "builder":
                        buydownContributorT = E_sBuydownContributorT.Builder;
                        break;
                    case "lender":
                        buydownContributorT = E_sBuydownContributorT.LenderPremiumFinanced;
                        break;
                    case "seller":
                        buydownContributorT = E_sBuydownContributorT.Seller;
                        break;
                    default:
                    case "other":
                        buydownContributorT = E_sBuydownContributorT.Other;
                        break;
                }
                loanData.sBuydownContributorT = buydownContributorT;


                loanData.sTexasDiscWillSubmitToLender = "X" == input.Shared["2770"];
                loanData.sTexasDiscAsIndependentContractor = "X" == input.Shared["2771"];
                loanData.sTexasDiscWillActAsFollows = "X" == input.Shared["2772"];
                loanData.sTexasDiscWillActAsFollowsDesc = input.Shared["2773"];
                loanData.sTexasDiscCompensationIncluded = "X" == input.Shared["2774"];
                loanData.sTexasDiscChargeVaried = "X" == input.Shared["2775"];
                loanData.sTexasDiscReceivedF_rep = input.Shared["2776"];
                bool _sTexasDiscAppFInc = "X" == input.Shared["2777"];
                string _sTexasDiscAppFAmt_rep = input.Shared["2778"];
                loanData.Set_sTexasDiscAppFInc_sTexasDiscAppFAmt(_sTexasDiscAppFInc, _sTexasDiscAppFAmt_rep);

                bool _sTexasDiscProcFInc = "X" == input.Shared["2779"];
                string _sTexasDiscProcF = input.Shared["5355"]; // 4/15/2010 dd - This field is same as sProcF
                loanData.Set_sTexasDiscProcFInc_sTexasDiscProcF(_sTexasDiscProcFInc, _sTexasDiscProcF);

                bool _sTexasDiscApprFInc = "X" == input.Shared["2780"];
                string _sTexasDiscApprF = input.Shared["1005"]; // 4/15/2010 dd - This field is same as sAppr
                loanData.Set_sTexasDiscApprFInc_sTexasDiscApprF(_sTexasDiscApprFInc, _sTexasDiscApprF);

                bool _sTexasDiscCrFInc = "X" == input.Shared["2781"];
                string _sTexasDiscCrF = input.Shared["1006"]; // 4/15/2010 dd - This field is same as sCrF
                loanData.Set_sTexasDiscCrFInc_sTexasDiscCrF(_sTexasDiscCrFInc, _sTexasDiscCrF);

                bool _sTexasDiscAutoUnderwritingFInc = "X" == input.Shared["2782"];
                string _sTexasDiscAutoUnderwritingFAmt_rep = input.Shared["2784"];

                loanData.Set_sTexasDiscAutoUnderwritingFInc_sTexasDiscAutoUnderwritingFAmt(_sTexasDiscAutoUnderwritingFInc, _sTexasDiscAutoUnderwritingFAmt_rep);
                loanData.sTexasDiscOF1IncDesc = input.Shared["2785"];
                loanData.sTexasDiscOF1IncAmt_rep = input.Shared["2786"];
                loanData.sTexasDiscOF2IncDesc = input.Shared["2787"];
                loanData.sTexasDiscOF2IncAmt_rep = input.Shared["2788"];
                loanData.sTexasDiscNonRefundAmt_rep = input.Shared["2783"];

                //loanData.sFloodHazardBuilding = 11415
                //sFloodHazardMobileHome bit not null default( 0 ), --11416
                loanData.sFloodHazardCommunityDesc = input.Shared["2423"];
                loanData.sFloodHazardFedInsAvail = "X" == input.Shared["2428"];
                loanData.sFloodHazardFedInsNotAvail = "X" == input.Shared["2431"];
                loanData.sCommitExpD_rep = input.Shared["7369"];
                loanData.sCommitRepayTermsDesc = CombineFieldsToLines(input.Shared, 7353, 7360);
                loanData.sCommitTitleEvidence = input.Shared["7361"];
                loanData.sCommitReturnToAboveAddr = "X" == input.Shared["7350"];
                loanData.sCommitReturnToFollowAddr = "X" == input.Shared["7351"];
                loanData.sCommitReturnWithinDays_rep = input.Shared["7352"];

                loanData.sFHA203kSpHudOwnedTri = FromXYesToTriState(input.Shared["8107"]);
                loanData.sFHAPropImprovHasFedPastDueTri = FromXYesToTriState(input.Shared["8451"]);
                loanData.sFHAPropImprovHasFHAPendingAppTri = FromXYesToTriState(input.Shared["8452"]);
                loanData.sFHAPropImprovHasFHAPendingAppWithWhom = input.Shared["8453"];
                loanData.sFHAPropImprovRefinTitle1LoanTri = FromXYesToTriState(input.Shared["8454"]);
                loanData.sFHAPropImprovRefinTitle1LoanNum = input.Shared["8455"];
                loanData.sFHAPropImprovRefinTitle1LoanBal_rep = input.Shared["8456"];
                loanData.sFHAPropImprovBorrRelativeNm = input.Shared["128"];
                loanData.sFHAPropImprovBorrRelativeRelationship = input.Shared["126"];
                loanData.sFHAPropImprovBorrRelativePhone = input.Shared["125"];
                loanData.sFHAPropImprovBorrRelativeAddr = input.Shared["127"];

                loanData.sFHAPropImprovCoborRelativeNm = input.Shared["178"];
                loanData.sFHAPropImprovCoborRelativeRelationship = input.Shared["176"];
                loanData.sFHAPropImprovCoborRelativePhone = input.Shared["175"];
                loanData.sFHAPropImprovCoborRelativeAddr = input.Shared["177"];


                loanData.sFHAPropImprovBorrHasChecking = "X" == input.Shared["8570"];
                loanData.sFHAPropImprovBorrHasSaving = "X" == input.Shared["8571"];
                loanData.sFHAPropImprovBorrHasNoBankAcount = !loanData.sFHAPropImprovBorrHasChecking && !loanData.sFHAPropImprovBorrHasSaving;
                loanData.sFHAPropImprovCoborHasChecking = "X" == input.Shared["8576"];
                loanData.sFHAPropImprovCoborHasSaving = "X" == input.Shared["8577"];
                loanData.sFHAPropImprovCoborHasNoBankAcount = !loanData.sFHAPropImprovCoborHasChecking && !loanData.sFHAPropImprovCoborHasSaving;


                loanData.sFHAPropImprovBorrBankInfo = input.Shared["8573"];
                loanData.sFHAPropImprovCoborBankInfo = input.Shared["8579"];

                loanData.sFHAPropImprovIsPropSingleFamily = "X" == input.Shared["936"];
                loanData.sFHAPropImprovIsPropMultifamily = "X" == input.Shared["938"];
                loanData.sFHAPropImprovIsPropNonresidential = "X" == input.Shared["8560"];
                loanData.sFHAPropImprovNonresidentialUsageDesc = input.Shared["8561"];
                loanData.sFHAPropImprovIsPropManufacturedHome = "X" == input.Shared["7957"];
                loanData.sFHAPropImprovIsPropHistoricResidential = "X" == input.Shared["8562"];
                loanData.sFHAPropImprovIsPropHistoricResidentialUnitsNum = input.Shared["8563"];
                loanData.sFHAPropImprovIsPropHealthCareFacility = "X" == input.Shared["8564"];
                loanData.sFHAPropImprovIsPropOwnedByBorr = "X" == input.Shared["8461"];
                loanData.sFHAPropImprovIsPropLeasedFromSomeone = "X" == input.Shared["8462"];
                loanData.sFHAPropImprovIsPropBeingPurchasedOnContract = "X" == input.Shared["8463"];
                loanData.sFHAPropImprovIsThereMortOnProp = "X" == input.Shared["8464"];
                loanData.sFHAPropImprovLeaseOwnerInfo = input.Shared["8465"];
                loanData.sFHAPropImprovLeaseMonPmt_rep = input.Shared["8468"];
                loanData.sFHAPropImprovLeaseExpireD_rep = input.Shared["8469"];
                loanData.sFHAPropImprovIsPropNewOccMoreThan90Days = "X" == input.Shared["8551"];
                loanData.sFHAPropImprovDealerContractorContactInfo = input.Shared["8552"];

                loanData.sFloridaContractDays_rep = input.Shared["3918"];
                loanData.sFloridaBorrEstimateSpVal_rep = input.Shared["3915"];
                loanData.sFloridaBorrEstimateExistingMBal_rep = input.Shared["3916"];
                loanData.sFloridaBorrDeposit_rep = input.Shared["3909"];
                loanData.sFloridaBrokerFeeR_rep = input.Shared["3900"];
                loanData.sFloridaBrokerFeeMb_rep = input.Shared["3901"];
                loanData.sFloridaAdditionalCompMinR_rep = input.Shared["3903"];
                loanData.sFloridaAdditionalCompMinMb_rep = input.Shared["3904"];
                loanData.sFloridaAdditionalCompMaxR_rep = input.Shared["3906"];
                loanData.sFloridaAdditionalCompMaxMb_rep = input.Shared["3907"];
                loanData.sFloridaAppFee_rep = input.Shared["3917"];
                loanData.sFloridaIsAppFeeRefundable = "X" == input.Shared["3911"];
                loanData.sFloridaIsAppFeeApplicableToCc = "X" == input.Shared["3914"];

                loanData.sTitleReqOwnerNm = input.Shared["7856"];
                loanData.sTitleReqOwnerPhone = input.Shared["7915"];

                loanData.sTitleReqPriorPolicy = "X" == input.Shared["5870"];
                loanData.sTitleReqWarrantyDeed = "X" == input.Shared["5871"];
                loanData.sTitleReqInsRequirements = "X" == input.Shared["5872"];
                loanData.sTitleReqSurvey = "X" == input.Shared["5873"];
                loanData.sTitleReqContract = "X" == input.Shared["5874"];
                loanData.sTitleReqPolicyTypeDesc = input.Shared["5875"];
                loanData.sTitleReqMailAway = "X" == input.Shared["5876"];

                strBuilder.Remove(0, strBuilder.Length);
                strBuilder.Append(input.Shared["5877"].ToString());
                strBuilder.Append(input.Shared["5878"].ToString());
                strBuilder.Append(input.Shared["5879"].ToString());
                strBuilder.Append(input.Shared["5880"].ToString());
                strBuilder.Append(input.Shared["5881"].ToString());
                strBuilder.Append(input.Shared["5882"].ToString());
                strBuilder.Append(input.Shared["5883"].ToString());
                strBuilder.Append(input.Shared["5884"].ToString());
                strBuilder.Append(input.Shared["5885"].ToString());
                strBuilder.Append(input.Shared["5886"].ToString());

                loanData.sTitleReqInstruction = strBuilder.ToString().TrimWhitespaceAndBOM();

                loanData.sInsReqReplacement = input.Shared["5891"];
                loanData.sInsReqFlood = "X" == input.Shared["5892"];
                loanData.sInsReqWind = "X" == input.Shared["5893"];
                loanData.sInsReqHazard = "X" == input.Shared["5894"];
                loanData.sInsReqEscrow = "X" == input.Shared["5895"];
                loanData.sInsReqComments = input.Shared["5896"];

                loanData.s3rdPartyOrigNmLn1 = input.Shared["2712"];
                loanData.s3rdPartyOrigNmLn2 = input.Shared["2713"];

                // seller information
                loanData.sLenNm = input.Shared["2720"];
                loanData.sLenNum = input.Shared["2723"];
                loanData.sLenLNum = input.Shared["2725"];
                loanData.sLenAddr = input.Shared["2721"];
                m_addressParser.ParseCityStateZip(input.Shared["2722"]);
                loanData.sLenCity = m_addressParser.City;
                loanData.sLenState = m_addressParser.State;
                loanData.sLenZip = m_addressParser.Zipcode;
                loanData.sLenContactTitle = input.Shared["2731"];
                loanData.sLenContactPhone = input.Shared["2732"];	// 2733 is ignored
                loanData.sLenContactNm = input.Shared["2730"];
                if ("X" == input.Shared["2734"])
                    loanData.sLenCommitT = E_sLenCommitT.Standard;
                else if ("X" == input.Shared["2735"])
                    loanData.sLenCommitT = E_sLenCommitT.Negotiated;

                loanData.sInvestorLockLoanNum = input.Shared["2724"];
                loanData.sContractNum = input.Shared["2727"];
                loanData.sCommitNum = input.Shared["2726"];

                if ("X" == input.Shared["2715"])
                    loanData.s1stMOwnerT = E_s1stMOwnerT.FannieMae;
                else if ("X" == input.Shared["2716"])
                    loanData.s1stMOwnerT = E_s1stMOwnerT.FreddieMac;
                else if ("X" == input.Shared["2717"])
                    loanData.s1stMOwnerT = E_s1stMOwnerT.SellerOrOther;
                loanData.sTotEstPp1003Lckd = "X" == input.Shared["831"];
                loanData.sTotEstPp1003_rep = input.Shared["805"];

                loanData.sAggregateAdjRsrv_rep = input.Shared["1039"];


                if ("X" == input.Shared["26"])
                    loanData.sLT = E_sLT.Conventional;
                else if ("X" == input.Shared["27"])
                    loanData.sLT = E_sLT.VA;
                else if ("X" == input.Shared["28"])
                    loanData.sLT = E_sLT.FHA;
                else if ("X" == input.Shared["29"])
                    loanData.sLT = E_sLT.UsdaRural;
                else if ("X" == input.Shared["1196"])
                    loanData.sLT = E_sLT.Other;

                if ("X" == input.Shared["915"])
                    loanData.sLienPosT = E_sLienPosT.First;
                else if ("X" == input.Shared["916"])
                    loanData.sLienPosT = E_sLienPosT.Second;

                if ("X" == input.Shared["1190"])
                    loanData.sLPurposeT = E_sLPurposeT.Purchase;
                else if ("X" == input.Shared["1191"])
                    loanData.sLPurposeT = E_sLPurposeT.ConstructPerm;
                else if ("X" == input.Shared["1192"])
                    loanData.sLPurposeT = E_sLPurposeT.Construct;
                else if ("X" == input.Shared["1193"])
                    loanData.sLPurposeT = E_sLPurposeT.RefinCashout;
                else if ("X" == input.Shared["1194"])
                    loanData.sLPurposeT = E_sLPurposeT.Other;
                else if ("X" == input.Shared["1198"])
                    loanData.sLPurposeT = E_sLPurposeT.Refin;
                loanData.sOLPurposeDesc = input.Shared["1195"];

                loanData.sPurchPrice_rep = input.Shared["800"];
                loanData.sEquityCalc_rep = input.Shared["525"];
                loanData.sApprVal_rep = input.Shared["801"];
                loanData.sNoteIR_rep = input.Shared["12"];
                loanData.sQualIRLckd = true;
                loanData.sQualIR_rep = input.Shared["14"];
                loanData.sTerm_rep = input.Shared["13"];
                loanData.sDue_rep = input.Shared["3190"];
                loanData.sSpAddr = input.Shared["31"];
                loanData.sSpCity = input.Shared["32"];
                loanData.sSpState = input.Shared["33"];
                loanData.sSpZip = input.Shared["34"];
                loanData.sSpCounty = input.Shared["35"];

                // property type
                if ("X" == input.Shared["932"])
                    loanData.sSpT = E_sSpT.DetachedHousing;
                else if ("X" == input.Shared["933"])
                    loanData.sSpT = E_sSpT.AttachedHousing;
                else if ("X" == input.Shared["929"])
                    loanData.sSpT = E_sSpT.Condominium;
                else if ("X" == input.Shared["930"])
                    loanData.sSpT = E_sSpT.PUD;
                else if ("X" == input.Shared["931"])
                    loanData.sSpT = E_sSpT.COOP;

                // todo: We have this as mutually exclusive whereas Point has 3 categories for them.
                // property classification
                if ("X" == input.Shared["2700"])
                    loanData.sSpProjClassT = E_sSpProjClassT.AIIICondo;
                else if ("X" == input.Shared["2701"])
                    loanData.sSpProjClassT = E_sSpProjClassT.BIICondo;
                else if ("X" == input.Shared["2702"])
                    loanData.sSpProjClassT = E_sSpProjClassT.CICondo;
                else if ("X" == input.Shared["2703"])
                    loanData.sSpProjClassT = E_sSpProjClassT.EPUD;
                else if ("X" == input.Shared["2704"])
                    loanData.sSpProjClassT = E_sSpProjClassT.FPUD;
                else if ("X" == input.Shared["2705"])
                    loanData.sSpProjClassT = E_sSpProjClassT.IIIPUD;
                else if ("X" == input.Shared["2706"])
                    loanData.sSpProjClassT = E_sSpProjClassT.COOP1;
                else if ("X" == input.Shared["2707"])
                    loanData.sSpProjClassT = E_sSpProjClassT.COOP2;


                loanData.sProRealETxR_rep = "";
                loanData.sProRealETxT = E_PercentBaseT.SalesPrice;
                /*
                E_sProRealETxT txT;
                switch(input.Shared["414"])
                {
                    case "Loan Amount":
                        txT = E_sProRealETxT.LoanAmount ; break ;
                    case "Sales Price":
                        txT = E_sProRealETxT.SalesPrice ; break ;
                    case "Appraisal Val":
                        txT = E_sProRealETxT.AppraisalValue ; break;
                        break ;
                }
                loanData.sProRealETxT = txT;
                */
                // Since Point users can override the calculated value
                // we just grab the result to make sure that we always
                // have the correct numbers at the end.
                loanData.sProRealETxMb_rep = input.Shared["754"];


                loanData.sProHoAssocDues_rep = input.Shared["756"];
                loanData.sProOHExp_rep = input.Shared["757"];
                loanData.sProOHExpLckd = true;
                loanData.sReqTopR_rep = input.Shared["544"];
                loanData.sReqBottomR_rep = input.Shared["545"];
                loanData.sMaxLtv_rep = input.Shared["546"];
                loanData.sMaxCltv_rep = input.Shared["547"];
                loanData.sSpGrossRentLckd = true;
                loanData.sSpGrossRent_rep = input.Shared["2848"];
                loanData.sOccRLckd = true;
                loanData.sOccR_rep = input.Shared["2849"]; // Point has this as app specific, but Genesis seems to have this for each property. We do what Genesis does.
                loanData.sAprIncludesReqDeposit = "X" == input.Shared["2155"];
                loanData.sHasDemandFeature = "X" == input.Shared["2156"];
                loanData.sHasVarRFeature = "X" == input.Shared["2157"];
                loanData.sVarRNotes = (input.Shared["2201"] + " " + input.Shared["2202"] + " " + input.Shared["2205"] + " " + input.Shared["2206"]).TrimEnd();

                loanData.sRAdj1stCapR_rep = input.Shared["2338"];
                loanData.sRAdj1stCapMon_rep = input.Shared["2330"];
                loanData.sRAdjCapR_rep = input.Shared["2324"];
                loanData.sRAdjCapMon_rep = input.Shared["2331"];
                loanData.sRAdjLifeCapR_rep = input.Shared["2325"];
                loanData.sRAdjMarginR_rep = input.Shared["2322"];
                string v2329 = input.Shared["2329"];
                loanData.sRAdjWorstIndex = "" == v2329;
                loanData.sRAdjIndexR_rep = v2329;
                loanData.sRAdjFloorR_rep = input.Shared["2332"];

                /*
                 * 0 - normal
                 * 1 - up
                 * 2 - down
                 */
                if ("X" == input.Shared["2320"])
                    loanData.sRAdjRoundT = DataAccess.E_sRAdjRoundT.Up;
                else if ("X" == input.Shared["2321"])
                    loanData.sRAdjRoundT = DataAccess.E_sRAdjRoundT.Down;
                else
                    loanData.sRAdjRoundT = DataAccess.E_sRAdjRoundT.Normal;

                loanData.sRAdjRoundToR_rep = input.Shared["2323"];
                loanData.sPmtAdjCapR_rep = input.Shared["2327"];
                loanData.sPmtAdjCapMon_rep = input.Shared["2335"];
                loanData.sPmtAdjRecastPeriodMon_rep = input.Shared["2336"];
                loanData.sPmtAdjRecastStop_rep = input.Shared["2337"];
                loanData.sPmtAdjMaxBalPc_rep = input.Shared["2328"];
                loanData.sBuydwnR1_rep = input.Shared["2450"];
                loanData.sBuydwnMon1_rep = input.Shared["2451"];
                loanData.sBuydwnR2_rep = input.Shared["2452"];
                loanData.sBuydwnMon2_rep = input.Shared["2453"];
                loanData.sBuydwnR3_rep = input.Shared["2454"];
                loanData.sBuydwnMon3_rep = input.Shared["2455"];
                loanData.sBuydwnR4_rep = input.Shared["2456"];
                loanData.sBuydwnMon4_rep = input.Shared["2457"];
                loanData.sBuydwnR5_rep = input.Shared["2458"];
                loanData.sBuydwnMon5_rep = input.Shared["2459"];
                loanData.sGradPmtYrs_rep = input.Shared["3201"];
                loanData.sGradPmtR_rep = input.Shared["3200"];
                loanData.sBiweeklyPmt = "X" == input.Shared["556"];
                loanData.sIOnlyMon_rep = input.Shared["555"];
                loanData.sProMInsMon_rep = input.Shared["964"];
                loanData.sProMIns2Mon_rep = input.Shared["963"];
                loanData.sProMInsCancelLtv_rep = input.Shared["961"];
                loanData.sProMInsMidptCancel = "X" == input.Shared["949"];

                if ("" != input.Shared["57"])
                {
                    loanData.sSchedDueD1_rep = input.Shared["57"];
                    // 9/26/2013 gf - opm 130112 1st pmt date is now a calculated
                    // field. Lock to maintain imported date.
                    loanData.sSchedDueD1Lckd = true;
                }
                else if ("" != input.Shared["20001"])
                {
                    loanData.sSchedDueD1_rep = input.Shared["20001"];
                    // 9/26/2013 gf - opm 130112 1st pmt date is now a calculated
                    // field. Lock to maintain imported date.
                    loanData.sSchedDueD1Lckd = true;
                }

                loanData.sSecurityPurch = "X" == input.Shared["2171"];
                loanData.sSecurityCurrentOwn = "X" == input.Shared["2172"];
                loanData.sFilingF = input.Shared["2161"];
                loanData.sLateDays = input.Shared["2190"];
                loanData.sLateChargePc = input.Shared["2185"];
                loanData.sLateChargeBaseDesc = input.Shared["2159"];

                /*
                 * 0 - will not
                 * 1 - may
                 */
                if ("X" == input.Shared["2173"])
                    loanData.sPrepmtPenaltyT = DataAccess.E_sPrepmtPenaltyT.May;
                else if ("X" == input.Shared["2174"])
                    loanData.sPrepmtPenaltyT = DataAccess.E_sPrepmtPenaltyT.WillNot;

                /*
                 * 0 - will not
                 * 1 - may
                 */
                if ("X" == input.Shared["2175"])
                    loanData.sPrepmtRefundT = DataAccess.E_sPrepmtRefundT.May;
                else if ("X" == input.Shared["2176"])
                    loanData.sPrepmtRefundT = DataAccess.E_sPrepmtRefundT.WillNot;

                /*
                 * 0 - cannot
                 * 1 - may
                 */
                if ("X" == input.Shared["2177"])
                    loanData.sAssumeLT = DataAccess.E_sAssumeLT.May;
                else if ("X" == input.Shared["2178"])
                    loanData.sAssumeLT = DataAccess.E_sAssumeLT.MaySubjectToCondition;
                else if ("X" == input.Shared["2179"])
                    loanData.sAssumeLT = DataAccess.E_sAssumeLT.MayNot;
                loanData.sAsteriskEstimate = "X" == input.Shared["1037"];
                loanData.sOnlyLatePmtEstimate = "X" == input.Shared["1036"];

                loanData.sLOrigFPc_rep = input.Shared["965"];
                loanData.sLOrigFMb_rep = input.Shared["1022"];
                loanData.sLOrigFProps = ToGfeItemProps_Pack(loanData, input.Shared["1081"], input.Shared["8220"], input.Shared["8260"], input.Shared["3107"]);
                loanData.sLDiscntPc_rep = input.Shared["967"];
                loanData.sLDiscntFMb_rep = input.Shared["968"];
                loanData.sLDiscntProps = ToGfeItemProps_Pack(loanData, input.Shared["1082"], input.Shared["8221"], input.Shared["2065"], input.Shared["3108"]);
                loanData.sApprFPaid = "X" == input.Shared["5029"];
                loanData.sApprF_rep = input.Shared["1005"];
                loanData.sApprFProps = ToGfeItemProps_Pack(loanData, input.Shared["1068"], input.Shared["8222"], input.Shared["8262"], input.Shared["5029"]);
                loanData.sCrFPaid = "X" == input.Shared["5030"];
                loanData.sCrF_rep = input.Shared["1006"];
                loanData.sCrFProps = ToGfeItemProps_Pack(loanData, input.Shared["1069"], input.Shared["8223"], input.Shared["8263"], input.Shared["5030"]);
                loanData.sInspectF_rep = input.Shared["1000"];
                loanData.sInspectFProps = ToGfeItemProps_Pack(loanData, input.Shared["1140"], input.Shared["8224"], input.Shared["8274"], input.Shared["3111"]);
                loanData.sMBrokFPc_rep = input.Shared["969"];
                loanData.sMBrokFMb_rep = input.Shared["970"];
                loanData.sMBrokFProps = ToGfeItemProps_Pack(loanData, input.Shared["1083"], input.Shared["8225"], input.Shared["8275"], input.Shared["3112"]);
                loanData.sTxServF_rep = input.Shared["1025"];
                loanData.sTxServFProps = ToGfeItemProps_Pack(loanData, input.Shared["1084"], input.Shared["8226"], input.Shared["8276"], input.Shared["3113"]);
                loanData.sProcFPaid = "X" == input.Shared["5031"];
                loanData.sProcF_rep = input.Shared["5355"];
                loanData.sProcFProps = ToGfeItemProps_Pack(loanData, input.Shared["1065"], input.Shared["8227"], input.Shared["8277"], input.Shared["5031"]);
                loanData.sUwF_rep = input.Shared["5356"];
                loanData.sUwFProps = ToGfeItemProps_Pack(loanData, input.Shared["1066"], input.Shared["8228"], input.Shared["8278"], input.Shared["3115"]);
                loanData.sWireF_rep = input.Shared["5357"];
                loanData.sWireFProps = ToGfeItemProps_Pack(loanData, input.Shared["1141"], input.Shared["8229"], input.Shared["8279"], input.Shared["3116"]);
                loanData.s800U1FDesc = input.Shared["978"];
                loanData.s800U1F_rep = input.Shared["1031"];
                loanData.s800U1FProps = ToGfeItemProps_Pack(loanData, input.Shared["1060"], input.Shared["8230"], input.Shared["8264"], input.Shared["3117"]);
                loanData.s800U2FDesc = input.Shared["979"];
                loanData.s800U2F_rep = input.Shared["1032"];
                loanData.s800U2FProps = ToGfeItemProps_Pack(loanData, input.Shared["1061"], input.Shared["8231"], input.Shared["8265"], input.Shared["3118"]);
                loanData.s800U3FDesc = input.Shared["980"];
                loanData.s800U3F_rep = input.Shared["1033"];
                loanData.s800U3FProps = ToGfeItemProps_Pack(loanData, input.Shared["1062"], input.Shared["8232"], input.Shared["8266"], input.Shared["3119"]);
                loanData.s800U4FDesc = input.Shared["984"];
                loanData.s800U4F_rep = input.Shared["1002"];
                loanData.s800U4FProps = ToGfeItemProps_Pack(loanData, input.Shared["1063"], input.Shared["8233"], input.Shared["8267"], input.Shared["3120"]);
                // 2/15/2006 dd - POINT has more line for 800 items. LendingQB only support 5 extra lines for 800 items.
                if (input.Shared["1021"] == "" && input.Shared["1003"] == "" && input.Shared["1004"] == "")
                {
                    loanData.s800U5FDesc = input.Shared["981"];

                    loanData.s800U5F_rep = input.Shared["1020"];
                    loanData.s800U5FProps = ToGfeItemProps_Pack(loanData, input.Shared["1142"], input.Shared["2091"], input.Shared["2067"], input.Shared["3152"]);
                }
                else
                {
                    // 2/15/2006 dd - Use the total.
                    loanData.s800U5FDesc = "Additional Items";
                    loanData.s800U5F_rep = input.Shared["1020"];
                    try
                    {
                        loanData.s800U5F += decimal.Parse(input.Shared["1021"]);
                    }
                    catch { }
                    try
                    {
                        loanData.s800U5F += decimal.Parse(input.Shared["1003"]);
                    }
                    catch { }
                    try
                    {
                        loanData.s800U5F += decimal.Parse(input.Shared["1004"]);
                    }
                    catch { }
                }
                //loanData.s800U6FDesc = input.Shared["985"] ;
                //loanData.s800U6F_rep = input.Shared["1021"] ;
                //TODO: Point doesn't show the ID's for these items
                //loanData.s800U6FProps = ToGfeItemProps_Pack(loanData, input.Shared["?"], input.Shared["?"], input.Shared["?"]) ;

                //loanData.s800U6FDesc = input.Shared["988"] ;
                //loanData.s800U6F_rep = input.Shared["1003"] ;
                //TODO: Point doesn't document ID's for these items
                //loanData.s800U6FProps = ToGfeItemProps_Pack(loanData, input.Shared["?"], input.Shared["?"], input.Shared["?"]) ;

                //loanData.s800U6FDesc = input.Shared["989"] ;
                //loanData.s800U6F_rep = input.Shared["1004"] ;
                //TODO: Point doesn't document ID's for these items
                //loanData.s800U6FProps = ToGfeItemProps_Pack(loanData, input.Shared["?"], input.Shared["?"], input.Shared["?"]) ;


                loanData.sIPiaDy_rep = input.Shared["966"]; // 973, Point bug to show incorrect field id, it swapped between 963 and 973 ?
                loanData.sIPiaDyLckd = true;
                loanData.sIPiaProps = ToGfeItemProps_Pack(loanData, input.Shared["1085"], input.Shared["8249"], input.Shared["2071"], input.Shared["3121"]);
                loanData.sMipPiaProps = ToGfeItemProps_Pack(loanData, input.Shared["1086"], input.Shared["8250"], input.Shared["2072"], input.Shared["3122"]);
                loanData.sHazInsPiaMon_rep = input.Shared["404"];
                loanData.sHazInsPiaProps = ToGfeItemProps_Pack(loanData, input.Shared["1146"], input.Shared["8251"], input.Shared["2073"], input.Shared["3123"]);
                loanData.s904PiaDesc = input.Shared["998"];
                loanData.s904Pia_rep = input.Shared["5358"];
                loanData.s904PiaProps = ToGfeItemProps_Pack(loanData, input.Shared["1147"], input.Shared["8252"], input.Shared["2074"], input.Shared["3124"]);

                loanData.s900U1PiaDesc = input.Shared["971"];
                loanData.s900U1Pia_rep = input.Shared["1038"];
                loanData.s900U1PiaProps = ToGfeItemProps_Pack(loanData, input.Shared["1064"], input.Shared["8254"], input.Shared["2076"], input.Shared["3126"]);

                loanData.sVaFfProps = ToGfeItemProps_Pack(loanData, input.Shared["1087"], input.Shared["8253"], "" /* Point doesn't have F for this item. */, input.Shared["3125"]);
                loanData.sHazInsRsrvMonLckd = true; // opm 180983
                loanData.sHazInsRsrvMon_rep = input.Shared["993"];
                loanData.sHazInsRsrvProps = ToGfeItemProps_Pack(loanData, input.Shared["1149"], input.Shared["8255"], input.Shared["2077"], input.Shared["3127"]);
                loanData.sMInsRsrvMonLckd = true; // opm 180983
                loanData.sMInsRsrvMon_rep = input.Shared["997"];
                loanData.sMInsRsrvProps = ToGfeItemProps_Pack(loanData, input.Shared["1088"], input.Shared["8256"], input.Shared["2078"], input.Shared["3128"]);
                loanData.sSchoolTxRsrvMonLckd = true; // opm 180983
                loanData.sSchoolTxRsrvMon_rep = input.Shared["1034"];
                loanData.sProSchoolTx_rep = input.Shared["1035"];
                loanData.sSchoolTxRsrvProps = ToGfeItemProps_Pack(loanData, input.Shared["1150"], input.Shared["2095"], input.Shared["2079"], input.Shared["3129"]);
                loanData.sRealETxRsrvMonLckd = true; // opm 180983
                loanData.sRealETxRsrvMon_rep = input.Shared["995"];
                loanData.sRealETxRsrvProps = ToGfeItemProps_Pack(loanData, input.Shared["1151"], input.Shared["8257"], input.Shared["2080"], input.Shared["3130"]);
                loanData.sFloodInsRsrvMonLckd = true; // opm 180983
                loanData.sFloodInsRsrvMon_rep = input.Shared["1040"];
                loanData.sProFloodIns_rep = input.Shared["1041"];
                loanData.sFloodInsRsrvProps = ToGfeItemProps_Pack(loanData, input.Shared["1152"], input.Shared["2096"], input.Shared["2081"], input.Shared["3131"]);
                loanData.s1006ProHExpDesc = input.Shared["743"];
                loanData.s1006RsrvMonLckd = true; // opm 180983
                loanData.s1006RsrvMon_rep = input.Shared["1058"];
                loanData.s1006ProHExp_rep = input.Shared["1096"];
                loanData.s1006RsrvProps = ToGfeItemProps_Pack(loanData, input.Shared["1153"], input.Shared["2097"], input.Shared["2082"], input.Shared["3132"]);
                loanData.s1007ProHExpDesc = input.Shared["745"];
                loanData.s1007RsrvMonLckd = true; // opm 180983
                loanData.s1007RsrvMon_rep = input.Shared["1126"];
                loanData.s1007ProHExp_rep = input.Shared["1097"];
                loanData.s1007RsrvProps = ToGfeItemProps_Pack(loanData, input.Shared["1154"], input.Shared["2098"], input.Shared["2083"], input.Shared["3133"]);
                loanData.sAggregateAdjRsrvProps = ToGfeItemProps_Pack(loanData, input.Shared["1155"], input.Shared["8258"], "", input.Shared["3134"]);

                loanData.sEscrowFTable = input.Shared["300"];
                loanData.sEscrowF_rep = input.Shared["1010"];
                loanData.sEscrowFProps = ToGfeItemProps_Pack(loanData, input.Shared["1067"], input.Shared["8234"], input.Shared["8268"], input.Shared["3135"]);
                loanData.sDocPrepF_rep = input.Shared["1001"];
                loanData.sDocPrepFProps = ToGfeItemProps_Pack(loanData, input.Shared["1156"], input.Shared["8235"], input.Shared["8269"], input.Shared["3136"]);
                loanData.sNotaryF_rep = input.Shared["1012"];
                loanData.sNotaryFProps = ToGfeItemProps_Pack(loanData, input.Shared["1157"], input.Shared["8236"], input.Shared["8280"], input.Shared["3137"]);
                loanData.sAttorneyF_rep = input.Shared["1042"];
                loanData.sAttorneyFProps = ToGfeItemProps_Pack(loanData, input.Shared["1090"], input.Shared["8237"], input.Shared["8281"], input.Shared["3138"]);
                loanData.sTitleInsFTable = input.Shared["992"];
                loanData.sTitleInsF_rep = input.Shared["1014"];
                loanData.sTitleInsFProps = ToGfeItemProps_Pack(loanData, input.Shared["1158"], input.Shared["8238"], input.Shared["8270"], input.Shared["3139"]);
                loanData.sU1TcDesc = input.Shared["990"];
                loanData.sU1Tc_rep = input.Shared["1016"];
                loanData.sU1TcProps = ToGfeItemProps_Pack(loanData, input.Shared["1091"], input.Shared["8239"], input.Shared["8282"], input.Shared["3140"]);
                loanData.sU2TcDesc = input.Shared["991"];
                loanData.sU2Tc_rep = input.Shared["1017"];
                loanData.sU2TcProps = ToGfeItemProps_Pack(loanData, input.Shared["1092"], input.Shared["8240"], input.Shared["8283"], input.Shared["3141"]);
                loanData.sU3TcDesc = input.Shared["1128"];
                loanData.sU3Tc_rep = input.Shared["1129"];
                loanData.sU4TcDesc = input.Shared["1130"];
                loanData.sU4Tc_rep = input.Shared["1131"];


                //975-1043: 1162, 8242, 8272
                loanData.sCountyRtcMb_rep = input.Shared["1043"];
                loanData.sCountyRtcProps = ToGfeItemProps_Pack(loanData, input.Shared["1162"], input.Shared["8242"], input.Shared["8272"], input.Shared["3143"]);
                //976-1044: 1163, 8243, 8284
                loanData.sStateRtcMb_rep = input.Shared["1044"];
                loanData.sStateRtcProps = ToGfeItemProps_Pack(loanData, input.Shared["1163"], input.Shared["8243"], input.Shared["8284"], input.Shared["3144"]);

                loanData.sRecFDesc = input.Shared["974"];
                loanData.sRecFMb_rep = input.Shared["1015"];
                loanData.sRecFProps = ToGfeItemProps_Pack(loanData, input.Shared["1161"], input.Shared["8241"], input.Shared["8271"], input.Shared["3142"]);
                loanData.sCountyRtcDesc = input.Shared["975"];
                loanData.sCountyRtcProps = ToGfeItemProps_Pack(loanData, input.Shared["1162"], input.Shared["8242"], input.Shared["8272"], input.Shared["3143"]);
                loanData.sStateRtcDesc = input.Shared["976"];
                loanData.sStateRtcProps = ToGfeItemProps_Pack(loanData, input.Shared["1163"], input.Shared["8243"], input.Shared["8284"], input.Shared["3144"]);
                loanData.sU1GovRtcDesc = input.Shared["977"];
                loanData.sU1GovRtcMb_rep = input.Shared["1045"];
                loanData.sU1GovRtcProps = ToGfeItemProps_Pack(loanData, input.Shared["1164"], input.Shared["8244"], input.Shared["8285"], input.Shared["3145"]);
                loanData.sU2GovRtcDesc = input.Shared["1132"];
                loanData.sU2GovRtcMb_rep = input.Shared["1133"];
                loanData.sU2GovRtcProps = ToGfeItemProps_Pack(loanData, input.Shared["1165"], input.Shared["2101"], input.Shared["2087"], input.Shared["3146"]);
                loanData.sU3GovRtcDesc = input.Shared["1134"];
                loanData.sU3GovRtcMb_rep = input.Shared["1135"];
                loanData.sU3GovRtcProps = ToGfeItemProps_Pack(loanData, input.Shared["1166"], input.Shared["2102"], input.Shared["2088"], input.Shared["3147"]);
                loanData.sPestInspectF_rep = input.Shared["1030"];
                loanData.sPestInspectFProps = ToGfeItemProps_Pack(loanData, input.Shared["1167"], input.Shared["8245"], input.Shared["8273"], input.Shared["3148"]);
                loanData.sU1ScDesc = input.Shared["986"];
                loanData.sU1Sc_rep = input.Shared["1007"];
                loanData.sU1ScProps = ToGfeItemProps_Pack(loanData, input.Shared["1093"], input.Shared["8246"], input.Shared["8286"], input.Shared["3149"]);
                loanData.sU2ScDesc = input.Shared["987"];
                loanData.sU2Sc_rep = input.Shared["1008"];
                loanData.sU2ScProps = ToGfeItemProps_Pack(loanData, input.Shared["1094"], input.Shared["8247"], input.Shared["8287"], input.Shared["3150"]);
                loanData.sU3ScDesc = input.Shared["982"];
                loanData.sU3Sc_rep = input.Shared["1046"];
                loanData.sU3ScProps = ToGfeItemProps_Pack(loanData, input.Shared["1095"], input.Shared["8248"], input.Shared["8288"], input.Shared["3151"]);
                loanData.sU4ScDesc = input.Shared["1136"];
                loanData.sU4Sc_rep = input.Shared["1137"];
                loanData.sU5ScDesc = input.Shared["1138"];
                loanData.sU5Sc_rep = input.Shared["1139"];
                loanData.sBrokComp1Lckd = true;
                loanData.sBrokComp1Desc = input.Shared["5366"];
                loanData.sBrokComp1_rep = input.Shared["5367"];
                loanData.sBrokComp2Desc = input.Shared["5368"];
                loanData.sBrokComp2_rep = input.Shared["5369"];
                loanData.sTotCcPbs_rep = input.Shared["810"];
                loanData.sTotCcPbsLocked = "X" == input.Shared["832"];

                #region OPM 794
                loanData.sBrokComp2Mlds_rep = input.Shared["5409"];
                loanData.sBrokComp2MldsLckd = true;
                loanData.sBrokComp1Mlds_rep = input.Shared["5492"];
                loanData.sBrokComp1MldsLckd = true;
                #endregion

                decimal fntc = 0;
                string fntcDesc = "";
                if (input.Shared["5291"].Length > 0)
                {
                    fntc += convertLos.ToMoney(input.Shared["5291"]);
                    fntcDesc = input.Shared["5290"];
                }
                if (input.Shared["5373"].Length > 0)
                {
                    fntc += convertLos.ToMoney(input.Shared["5373"]);
                    fntcDesc += "; " + input.Shared["5372"];
                }
                if (input.Shared["5293"].Length > 0)
                {
                    fntc += convertLos.ToMoney(input.Shared["5293"]);
                    fntcDesc += "; " + input.Shared["5292"];
                }
                loanData.sU1FntcDesc = fntcDesc;
                loanData.sU1Fntc = fntc;

                loanData.sGfeProvByBrok = "X" == input.Shared["1049"];

                loanData.sMultiApps = "X" == input.Shared["1180"];
                loanData.sLTODesc = input.Shared["1197"];
                loanData.sAgencyCaseNum = input.Shared["901"];


                // 0 - FIX, 1 - ARM, 2 - GPM
                if ("X" == input.Shared["562"])
                {
                    // 12/15/2004 dd - Add "Other" option to import. If "Other" is select in point, set sFinMethT = Fixed.
                    loanData.sFinMethodPrintAsOther = true;
                    loanData.sFinMethPrintAsOtherDesc = input.Shared["563"];
                    loanData.sFinMethT = DataAccess.E_sFinMethT.Fixed;
                }
                else
                {
                    if ("X" == input.Shared["550"])
                        loanData.sFinMethT = DataAccess.E_sFinMethT.Fixed;
                    else if ("X" == input.Shared["552"])
                        loanData.sFinMethT = DataAccess.E_sFinMethT.Graduated;
                    else if ("X" == input.Shared["560"])
                        loanData.sFinMethT = DataAccess.E_sFinMethT.ARM;
                    loanData.sFinMethDesc = input.Shared["561"] + " " + input.Shared["563"];
                }
                loanData.sUnitsNum_rep = input.Shared["36"];
                loanData.sYrBuilt = input.Shared["37"];
                loanData.sSpLegalDesc = input.Shared["1206"] + " " + input.Shared["1207"];
                loanData.sLotAcqYr = input.Shared["1208"];
                loanData.sLotOrigC_rep = input.Shared["1209"];
                loanData.sLotLien_rep = input.Shared["1220"];
                loanData.sLotVal_rep = input.Shared["1210"];
                loanData.sLotImprovC_rep = input.Shared["1211"];
                loanData.sSpAcqYr = input.Shared["1212"];
                loanData.sSpOrigC_rep = input.Shared["1213"];
                loanData.sSpLien_rep = input.Shared["1214"];
                loanData.sRefPurpose = input.Shared["1215"];
                loanData.sSpImprovDesc = input.Shared["1219"];
                if ("X" == input.Shared["1217"])
                    loanData.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.Made;
                else if ("X" == input.Shared["1218"])
                    loanData.sSpImprovTimeFrameT = E_sSpImprovTimeFrameT.ToBeMade;
                loanData.sSpImprovC_rep = input.Shared["1221"];

                if ("X" == input.Shared["1228"])
                    loanData.sEstateHeldT = E_sEstateHeldT.LeaseHold;
                else //if ("X" == input.Shared["1227"])
                    loanData.sEstateHeldT = E_sEstateHeldT.FeeSimple;

                loanData.sLeaseHoldExpireD_rep = input.Shared["1229"];
                loanData.sDwnPmtSrc = input.Shared["1225"];
                loanData.sDwnPmtSrcExplain = input.Shared["1226"];

                loanData.sAltCost_rep = input.Shared["802"];
                loanData.sLandCost_rep = input.Shared["803"];

                loanData.sRefPdOffAmt1003Lckd = "X" == input.Shared["830"];
                loanData.sRefPdOffAmt1003_rep = input.Shared["804"];

                loanData.sMipPiaMon_rep = "12"; // it would affect much because
                // we lock sFfUfmip1003Lckd
                loanData.sFfUfmip1003Lckd = true; //"X" == input.Shared["831"];
                loanData.sFfUfmip1003_rep = input.Shared["807"]; // same as 1028 for us
                loanData.sLDiscnt1003Lckd = "X" == input.Shared["831"];
                loanData.sLDiscnt1003_rep = input.Shared["808"];
                loanData.sLoads1003LineLFromAdjustments = false; // opm 212045.
                loanData.sOCredit1Lckd = true;
                loanData.sOCredit1Desc = input.Shared["811"];
                loanData.sOCredit1Amt_rep = input.Shared["812"];
                loanData.sOCredit2Desc = input.Shared["813"];
                //loanData.sOCredit2Lckd = true;
                loanData.sOCredit2Amt_rep = input.Shared["814"];
                loanData.sOCredit3Desc = input.Shared["815"];
                loanData.sOCredit3Amt_rep = input.Shared["816"];
                loanData.sOCredit4Desc = input.Shared["1626"];
                loanData.sOCredit4Amt_rep = input.Shared["1627"];

                // If we have correct loan amount, equity will be figured out correctly
                // but sometimes that we don't have equity amount or purchase amount, just 
                // loan amount. That's why we should lock loanamount at every import.
                loanData.sLAmtLckd = true;
                loanData.sLAmtCalc_rep = input.Shared["11"];
                loanData.sTransNetCashLckd = "X" == input.Shared["834"];
                loanData.sTransNetCash_rep = input.Shared["820"];
                loanData.sTotEstCc1003Lckd = "X" == input.Shared["831"];
                loanData.sTotEstCcNoDiscnt1003_rep = input.Shared["806"];

                loanData.sProMInsR2_rep = input.Shared["405"];

                // statuses
                if (!string.IsNullOrEmpty(input.Shared["6075"]))
                {
                    loanData.sEstCloseD_rep = input.Shared["6075"];
                    loanData.sEstCloseDLckd = true;
                }
                loanData.sEstCloseN = input.Shared["6076"];
                if (string.IsNullOrEmpty(input.Shared["6020"]) == false)
                {

                    loanData.sOpenedD_rep = input.Shared["6020"];
                }
                else
                {
                    // 5/10/2011 dd - When there is no open date from Point file, default to today date.
                    loanData.sOpenedD_rep = (CDateTime.Create(DateTime.Now)).ToString(loanData.m_convertLos);
                }
                loanData.sOpenedN = input.Shared["6021"];
                loanData.sSubmitD_rep = input.Shared["6025"];
                loanData.sSubmitN = input.Shared["6026"];
                loanData.sApprovD_rep = input.Shared["6030"];
                loanData.sApprovN = input.Shared["6031"];
                loanData.sDocsD_rep = input.Shared["6035"];
                loanData.sDocsN = input.Shared["6036"];
                loanData.sFundD_rep = input.Shared["6040"];
                loanData.sFundN = input.Shared["6041"];
                loanData.sRecordedD_rep = input.Shared["6070"];
                loanData.sRecordedN = input.Shared["6071"];
                loanData.sClosedD_rep = input.Shared["6055"];
                loanData.sClosedN = input.Shared["6056"];

                loanData.sCanceledD_rep = input.Shared["6050"];
                loanData.sCanceledN = input.Shared["6051"];
                loanData.sSuspendedD_rep = input.Shared["6065"];
                loanData.sSuspendedN = input.Shared["6066"];
                loanData.sRejectD_rep = input.Shared["6045"];
                loanData.sRejectN = input.Shared["6046"];
                loanData.sDocsBackD_rep = input.Shared["11442"];
                loanData.sDocsBackN = input.Shared["11443"];

                loanData.sU1LStatDesc = input.Shared["6084"];
                loanData.sU1LStatD_rep = input.Shared["6080"];
                loanData.sU1LStatN = input.Shared["6081"];
                loanData.sU2LStatDesc = input.Shared["6089"];
                loanData.sU2LStatD_rep = input.Shared["6085"];
                loanData.sU2LStatN = input.Shared["6086"];
                loanData.sU3LStatDesc = input.Shared["6094"];
                loanData.sU3LStatD_rep = input.Shared["6090"];
                loanData.sU3LStatN = input.Shared["6091"];
                loanData.sU4LStatDesc = input.Shared["6099"];
                loanData.sU4LStatD_rep = input.Shared["6095"];
                loanData.sU4LStatN = input.Shared["6096"];
                loanData.sPrelimRprtOd_rep = input.Shared["6300"];
                loanData.sPrelimRprtRd_rep = input.Shared["6301"];
                loanData.sPrelimRprtN = input.Shared["6302"];

                loanData.sApprRprtOd_rep = input.Shared["6315"];
                loanData.sApprRprtRd_rep = input.Shared["6316"];
                loanData.sApprRprtN = input.Shared["6317"];
                loanData.sTilGfeOd_rep = input.Shared["6320"];
                loanData.sTilGfeRd_rep = input.Shared["6321"];
                loanData.sTilGfeN = input.Shared["6322"];
                loanData.sU1DocStatDesc = input.Shared["6329"];
                loanData.sU1DocStatOd_rep = input.Shared["6325"];
                loanData.sU1DocStatRd_rep = input.Shared["6326"];
                loanData.sU1DocStatN = input.Shared["6327"];
                loanData.sU2DocStatDesc = input.Shared["6334"];
                loanData.sU2DocStatOd_rep = input.Shared["6330"];
                loanData.sU2DocStatRd_rep = input.Shared["6331"];
                loanData.sU2DocStatN = input.Shared["6332"];
                loanData.sU3DocStatDesc = input.Shared["6339"];
                loanData.sU3DocStatOd_rep = input.Shared["6335"];
                loanData.sU3DocStatRd_rep = input.Shared["6336"];
                loanData.sU3DocStatN = input.Shared["6337"];
                loanData.sU4DocStatDesc = input.Shared["6344"];
                loanData.sU4DocStatOd_rep = input.Shared["6340"];
                loanData.sU4DocStatRd_rep = input.Shared["6341"];
                loanData.sU4DocStatN = input.Shared["6342"];
                loanData.sU5DocStatDesc = input.Shared["6349"];
                loanData.sU5DocStatOd_rep = input.Shared["6345"];
                loanData.sU5DocStatRd_rep = input.Shared["6346"];
                loanData.sU5DocStatN = input.Shared["6347"];
                loanData.sTrNotes = input.Shared["15"];

                loanData.sTransmUwerComments = input.Shared["885"] + " " + input.Shared["886"] + " " +
                    input.Shared["887"] + " " + input.Shared["888"] + " " + input.Shared["889"] + " " + input.Shared["890"] +
                    input.Shared["891"] + " " + input.Shared["892"];
                loanData.sTransmUwerComments = loanData.sTransmUwerComments.TrimWhitespaceAndBOM();	// get rid of extra spaces

                switch (input.Shared["412"])
                {
                    case "Loan Amount":
                        loanData.sProHazInsT = E_PercentBaseT.LoanAmount;
                        break;
                    case "Sales Price":
                        loanData.sProHazInsT = E_PercentBaseT.SalesPrice;
                        break;
                    case "Appraisal Val":
                        loanData.sProHazInsT = E_PercentBaseT.AppraisalValue;
                        break;
                }
                switch (input.Shared["413"])
                {
                    case "Loan Amount":
                        loanData.sProMInsT = E_PercentBaseT.LoanAmount;
                        break;
                    case "Sales Price":
                        loanData.sProMInsT = E_PercentBaseT.SalesPrice;
                        break;
                    case "Appraisal Val":
                        loanData.sProMInsT = E_PercentBaseT.AppraisalValue;
                        break;
                }

                loanData.sProjNm = input.Shared["911"];
                loanData.sNoteD_rep = input.Shared["49"];
                if ("X" == input.Shared["2710"])
                    loanData.sMOrigT = E_sMOrigT.Seller;
                else if ("X" == input.Shared["2711"])
                    loanData.sMOrigT = E_sMOrigT.ThirdParty;

                loanData.sMInsCoverPc_rep = input.Shared["2752"];
                loanData.sMInsCode = input.Shared["2750"];
                loanData.sMInsCertNum = input.Shared["2751"];
                loanData.sLTotNumOfB = input.Shared["2749"];
                //loanData.sUwerNm = input.Shared["942"];
                loanData.sUseAdjustorCover = "X" == input.Shared["2753"];
                loanData.sSpecCode1 = input.Shared["2754"];
                loanData.sSpecCode2 = input.Shared["2755"];
                loanData.sSpecCode3 = input.Shared["2756"];
                loanData.sSpecCode4 = input.Shared["2757"];
                loanData.sSpecCode5 = input.Shared["2758"];
                loanData.sSpecCode6 = input.Shared["2759"];

                if ("X" == input.Shared["2760"])
                    loanData.sQualIRDeriveT = E_sQualIRDeriveT.NoteRateDependent;
                else if ("X" == input.Shared["2765"])
                    loanData.sQualIRDeriveT = E_sQualIRDeriveT.BoughtDownRate;
                else if ("X" == input.Shared["2766"])
                    loanData.sQualIRDeriveT = E_sQualIRDeriveT.Other;

                E_sFannieSpT fannieSpT;
                switch (input.Shared["928"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "detached": fannieSpT = E_sFannieSpT.Detached; break;
                    case "attached": fannieSpT = E_sFannieSpT.Attached; break;
                    case "condo": fannieSpT = E_sFannieSpT.Condo; break;
                    case "pud": fannieSpT = E_sFannieSpT.PUD; break;
                    case "co-op": fannieSpT = E_sFannieSpT.CoOp; break;
                    case "high rise condo": fannieSpT = E_sFannieSpT.HighRiseCondo; break;
                    case "manufactured": fannieSpT = E_sFannieSpT.Manufactured; break;
                    case "detached condo": fannieSpT = E_sFannieSpT.DetachedCondo; break;
                    case "manufactured home/condo/pud/co-op": fannieSpT = E_sFannieSpT.ManufacturedCondoPudCoop; break;
                    default: fannieSpT = E_sFannieSpT.LeaveBlank; break;
                }
                loanData.sFannieSpT = fannieSpT;

                E_sFannieDocT fannieDocT;
                switch (input.Shared["3898"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "alternative": fannieDocT = E_sFannieDocT.Alternative; break;
                    case "full": fannieDocT = E_sFannieDocT.Full; break;
                    case "no documentation": fannieDocT = E_sFannieDocT.NoDocumentation; break;
                    case "reduced": fannieDocT = E_sFannieDocT.Reduced; break;
                    case "streamlined refinance": fannieDocT = E_sFannieDocT.StreamlinedRefinanced; break;
                    case "no ratio": fannieDocT = E_sFannieDocT.NoRatio; break;
                    case "limited documentation": fannieDocT = E_sFannieDocT.LimitedDocumentation; break;
                    case "no income and no assets on 1003": fannieDocT = E_sFannieDocT.NoIncomeNoAssets; break;
                    case "no assets on 1003": fannieDocT = E_sFannieDocT.NoAssets; break;
                    case "no income and no employment on 1003": fannieDocT = E_sFannieDocT.NoIncomeNoEmployment; break;
                    case "no income on 1003": fannieDocT = E_sFannieDocT.NoIncome; break;
                    case "no verification of stated income, employment or assets": fannieDocT = E_sFannieDocT.NoVerificationStatedIncomeEmploymentAssets; break;
                    case "no verification of state income or assets": fannieDocT = E_sFannieDocT.NoVerificationStatedIncomeAssets; break;
                    case "no verification of stated assets": fannieDocT = E_sFannieDocT.NoVerificationStatedAssets; break;
                    case "no verification of stated income or employment": fannieDocT = E_sFannieDocT.NoVerificationStatedIncomeEmployment; break;
                    case "no verification of stated income": fannieDocT = E_sFannieDocT.NoVerificationStatedIncome; break;
                    case "verbal verification of employment (vvoe)": fannieDocT = E_sFannieDocT.VerbalVOE; break;
                    case "one paystub": fannieDocT = E_sFannieDocT.OnePaystub; break;
                    case "one paystub and vvoe": fannieDocT = E_sFannieDocT.OnePaystubAndVerbalVOE; break;
                    case "one paystub and one w-2 and vvoe or one yr 1040": fannieDocT = E_sFannieDocT.OnePaystubOneW2VerbalVOE; break;
                    case "no income, no employment and no assets on 1003": fannieDocT = E_sFannieDocT.NoIncomeNoEmploymentNoAssets; break;

                    default: fannieDocT = E_sFannieDocT.LeaveBlank; break;
                }
                loanData.sFannieDocT = fannieDocT;

                loanData.sFannieARMPlanNum = input.Shared["3892"];
                loanData.sFannieARMPlanNumLckd = true;
                E_sArmIndexT armIndexT;
                switch (input.Shared["3896"].TrimWhitespaceAndBOM().ToLower())
                {
                    case "weekly average cmt": armIndexT = E_sArmIndexT.WeeklyAvgCMT; break;
                    case "monthly average cmt": armIndexT = E_sArmIndexT.MonthlyAvgCMT; break;
                    case "weekly average taai": armIndexT = E_sArmIndexT.WeeklyAvgTAAI; break;
                    case "weekly average taabd": armIndexT = E_sArmIndexT.WeeklyAvgTAABD; break;
                    case "weekly average smti": armIndexT = E_sArmIndexT.WeeklyAvgSMTI; break;
                    case "daily cd rate": armIndexT = E_sArmIndexT.DailyCDRate; break;
                    case "weekly average cd rate": armIndexT = E_sArmIndexT.WeeklyAvgCDRate; break;
                    case "weekly ave prime rate": armIndexT = E_sArmIndexT.WeeklyAvgPrimeRate; break;
                    case "t-bill daily value": armIndexT = E_sArmIndexT.TBillDailyValue; break;
                    case "11th district cof": armIndexT = E_sArmIndexT.EleventhDistrictCOF; break;
                    case "national monthly median cost of funds": armIndexT = E_sArmIndexT.NationalMonthlyMedianCostOfFunds; break;
                    case "wall street journal libor": armIndexT = E_sArmIndexT.WallStreetJournalLIBOR; break;
                    case "fannie mae libor": armIndexT = E_sArmIndexT.FannieMaeLIBOR; break;
                    default: armIndexT = E_sArmIndexT.LeaveBlank; break;
                }
                loanData.sArmIndexT = armIndexT;
                loanData.sArmIndexTLckd = true;
                loanData.sIsSellerProvidedBelowMktFin = input.Shared["423"];
                loanData.sWillEscrowBeWaived = "X" == input.Shared["9309"];
                loanData.sBuydown = "X" == input.Shared["554"];

                loanData.sIfPurchInsFrCreditor = "X" == input.Shared["2168"];
                loanData.sIfPurchPropInsFrCreditor = "X" == input.Shared["2169"];
                loanData.sIfPurchFloodInsFrCreditor = "X" == input.Shared["2170"];
                loanData.sInsFrCreditorAmt_rep = input.Shared["2164"];

                loanData.sIsLicensedLender = "X" == input.Shared["11420"];
                loanData.sIsCorrespondentLender = "X" == input.Shared["11421"];
                loanData.sIsOtherTypeLender = "X" == input.Shared["11422"];
                loanData.sOtherTypeLenderDesc = input.Shared["11423"];
                if ("X" == input.Shared["11424"])
                    loanData.sRefundabilityOfFeeToLenderT = E_sRefundabilityOfFeeToLenderT.Nonrefundable;
                else if ("X" == input.Shared["11425"])
                    loanData.sRefundabilityOfFeeToLenderT = E_sRefundabilityOfFeeToLenderT.Refundable;
                else if ("X" == input.Shared["11426"])
                    loanData.sRefundabilityOfFeeToLenderT = E_sRefundabilityOfFeeToLenderT.NA;
                else
                    loanData.sRefundabilityOfFeeToLenderT = E_sRefundabilityOfFeeToLenderT.Blank;

                loanData.sCommitmentEstimateDays = input.Shared["11427"];

                if ("X" == input.Shared["2174"])
                    loanData.sMldsPpmtT = E_sMldsPpmtT.NoPenalty;
                else if ("X" == input.Shared["5482"])
                    loanData.sMldsPpmtT = E_sMldsPpmtT.Other;
                else if ("X" == input.Shared["5354"])
                    loanData.sMldsPpmtT = E_sMldsPpmtT.Penalty;
                else
                    loanData.sMldsPpmtT = E_sMldsPpmtT.LeaveBlank;

                if ("X" == input.Shared["5483"])
                    loanData.sMldsPpmtBaseT = E_sMldsPpmtBaseT.OriginalBalance;
                else if ("X" == input.Shared["5484"])
                    loanData.sMldsPpmtBaseT = E_sMldsPpmtBaseT.UnpaidBalance;

                loanData.sMldsPpmtMonMax_rep = input.Shared["5468"];

                loanData.sLienholder1NmBefore = input.Shared["5412"];
                loanData.sLien1AmtBefore_rep = input.Shared["5413"];
                loanData.sLien1PriorityBefore = input.Shared["5411"];

                loanData.sLienholder2NmBefore = input.Shared["5417"];
                loanData.sLien2AmtBefore_rep = input.Shared["5418"];
                loanData.sLien2PriorityBefore = input.Shared["5416"];

                loanData.sLienholder3NmBefore = input.Shared["5422"];
                loanData.sLien3AmtBefore_rep = input.Shared["5423"];
                loanData.sLien3PriorityBefore = input.Shared["5421"];

                loanData.sLienholder1NmAfter = input.Shared["5432"];
                loanData.sLien1AmtAfter_rep = input.Shared["5433"];
                loanData.sLien1PriorityAfter = input.Shared["5431"];

                loanData.sLienholder2NmAfter = input.Shared["5437"];
                loanData.sLien2AmtAfter_rep = input.Shared["5438"];
                loanData.sLien2PriorityAfter = input.Shared["5436"];

                loanData.sLienholder3NmAfter = input.Shared["5442"];
                loanData.sLien3AmtAfter_rep = input.Shared["5443"];
                loanData.sLien3PriorityAfter = input.Shared["5441"];

                if ("X" == input.Shared["5485"])
                    loanData.sBrokControlledFundT = E_sBrokControlledFundT.May;
                else if ("X" == input.Shared["5486"])
                    loanData.sBrokControlledFundT = E_sBrokControlledFundT.Will;
                else if ("X" == input.Shared["5487"])
                    loanData.sBrokControlledFundT = E_sBrokControlledFundT.WillNot;
                else
                    loanData.sBrokControlledFundT = E_sBrokControlledFundT.LeaveBlank;

                #region Trust Accounts
                // 1/12/2005 dd - Task #791 - Once we have the accountant role added to the system, in Point import, 
                // honor the option "Only Accountant Can Modify Trust Account data"
                if (System.Threading.Thread.CurrentPrincipal.IsInRole(ConstApp.ROLE_ACCOUNTANT) || !broker.IsOnlyAccountantCanModifyTrustAccount)
                {
                    loanData.sTrust1Desc = input.Shared["6600"];
                    loanData.sTrust1ReceivableAmt_rep = input.Shared["6601"];
                    loanData.sTrust1PayableAmt_rep = input.Shared["6602"];
                    loanData.sTrust1D_rep = input.Shared["6603"];
                    loanData.sTrust1ReceivableChkNum = input.Shared["6604"];
                    loanData.sTrust1PayableChkNum = input.Shared["6607"];
                    loanData.sTrust1ReceivableN = input.Shared["6608"];

                    loanData.sTrust2Desc = input.Shared["6610"];
                    loanData.sTrust2ReceivableAmt_rep = input.Shared["6611"];
                    loanData.sTrust2PayableAmt_rep = input.Shared["6612"];
                    loanData.sTrust2D_rep = input.Shared["6613"];
                    loanData.sTrust2ReceivableChkNum = input.Shared["6614"];
                    loanData.sTrust2PayableChkNum = input.Shared["6617"];
                    loanData.sTrust2ReceivableN = input.Shared["6618"];

                    loanData.sTrust3Desc = input.Shared["6620"];
                    loanData.sTrust3ReceivableAmt_rep = input.Shared["6621"];
                    loanData.sTrust3PayableAmt_rep = input.Shared["6622"];
                    loanData.sTrust3D_rep = input.Shared["6623"];
                    loanData.sTrust3ReceivableChkNum = input.Shared["6624"];
                    loanData.sTrust3PayableChkNum = input.Shared["6627"];
                    loanData.sTrust3ReceivableN = input.Shared["6628"];

                    loanData.sTrust4Desc = input.Shared["6630"];
                    loanData.sTrust4ReceivableAmt_rep = input.Shared["6631"];
                    loanData.sTrust4PayableAmt_rep = input.Shared["6632"];
                    loanData.sTrust4D_rep = input.Shared["6633"];
                    loanData.sTrust4ReceivableChkNum = input.Shared["6634"];
                    loanData.sTrust4PayableChkNum = input.Shared["6637"];
                    loanData.sTrust4ReceivableN = input.Shared["6638"];

                    loanData.sTrust5Desc = input.Shared["6640"];
                    loanData.sTrust5ReceivableAmt_rep = input.Shared["6641"];
                    loanData.sTrust5PayableAmt_rep = input.Shared["6642"];
                    loanData.sTrust5D_rep = input.Shared["6643"];
                    loanData.sTrust5ReceivableChkNum = input.Shared["6644"];
                    loanData.sTrust5PayableChkNum = input.Shared["6647"];
                    loanData.sTrust5ReceivableN = input.Shared["6648"];

                    loanData.sTrust6Desc = input.Shared["6650"];
                    loanData.sTrust6ReceivableAmt_rep = input.Shared["6651"];
                    loanData.sTrust6PayableAmt_rep = input.Shared["6652"];
                    loanData.sTrust6D_rep = input.Shared["6653"];
                    loanData.sTrust6ReceivableChkNum = input.Shared["6654"];
                    loanData.sTrust6PayableChkNum = input.Shared["6657"];
                    loanData.sTrust6ReceivableN = input.Shared["6658"];

                    loanData.sTrust7Desc = input.Shared["6660"];
                    loanData.sTrust7ReceivableAmt_rep = input.Shared["6661"];
                    loanData.sTrust7PayableAmt_rep = input.Shared["6662"];
                    loanData.sTrust7D_rep = input.Shared["6663"];
                    loanData.sTrust7ReceivableChkNum = input.Shared["6664"];
                    loanData.sTrust7PayableChkNum = input.Shared["6667"];
                    loanData.sTrust7ReceivableN = input.Shared["6668"];

                    loanData.sTrust8Desc = input.Shared["6670"];
                    loanData.sTrust8ReceivableAmt_rep = input.Shared["6671"];
                    loanData.sTrust8PayableAmt_rep = input.Shared["6672"];
                    loanData.sTrust8D_rep = input.Shared["6673"];
                    loanData.sTrust8ReceivableChkNum = input.Shared["6674"];
                    loanData.sTrust8PayableChkNum = input.Shared["6677"];
                    loanData.sTrust8ReceivableN = input.Shared["6678"];

                    loanData.sTrust9Desc = input.Shared["7200"];
                    loanData.sTrust9ReceivableAmt_rep = input.Shared["7201"];
                    loanData.sTrust9PayableAmt_rep = input.Shared["7202"];
                    loanData.sTrust9D_rep = input.Shared["7203"];
                    loanData.sTrust9ReceivableChkNum = input.Shared["7204"];
                    loanData.sTrust9PayableChkNum = input.Shared["7207"];
                    loanData.sTrust9ReceivableN = input.Shared["7208"];

                    loanData.sTrust10Desc = input.Shared["7210"];
                    loanData.sTrust10ReceivableAmt_rep = input.Shared["7211"];
                    loanData.sTrust10PayableAmt_rep = input.Shared["7212"];
                    loanData.sTrust10D_rep = input.Shared["7213"];
                    loanData.sTrust10ReceivableChkNum = input.Shared["7214"];
                    loanData.sTrust10PayableChkNum = input.Shared["7217"];
                    loanData.sTrust10ReceivableN = input.Shared["7218"];

                    loanData.sTrust11Desc = input.Shared["7220"];
                    loanData.sTrust11ReceivableAmt_rep = input.Shared["7221"];
                    loanData.sTrust11PayableAmt_rep = input.Shared["7222"];
                    loanData.sTrust11D_rep = input.Shared["7223"];
                    loanData.sTrust11ReceivableChkNum = input.Shared["7224"];
                    loanData.sTrust11PayableChkNum = input.Shared["7227"];
                    loanData.sTrust11ReceivableN = input.Shared["7228"];

                    loanData.sTrust12Desc = input.Shared["7230"];
                    loanData.sTrust12ReceivableAmt_rep = input.Shared["7231"];
                    loanData.sTrust12PayableAmt_rep = input.Shared["7232"];
                    loanData.sTrust12D_rep = input.Shared["7233"];
                    loanData.sTrust12ReceivableChkNum = input.Shared["7234"];
                    loanData.sTrust12PayableChkNum = input.Shared["7237"];
                    loanData.sTrust12ReceivableN = input.Shared["7238"];

                    loanData.sTrust13Desc = input.Shared["7240"];
                    loanData.sTrust13ReceivableAmt_rep = input.Shared["7241"];
                    loanData.sTrust13PayableAmt_rep = input.Shared["7242"];
                    loanData.sTrust13D_rep = input.Shared["7243"];
                    loanData.sTrust13ReceivableChkNum = input.Shared["7244"];
                    loanData.sTrust13PayableChkNum = input.Shared["7247"];
                    loanData.sTrust13ReceivableN = input.Shared["7248"];

                    loanData.sTrust14Desc = input.Shared["7250"];
                    loanData.sTrust14ReceivableAmt_rep = input.Shared["7251"];
                    loanData.sTrust14PayableAmt_rep = input.Shared["7252"];
                    loanData.sTrust14D_rep = input.Shared["7253"];
                    loanData.sTrust14ReceivableChkNum = input.Shared["7254"];
                    loanData.sTrust14PayableChkNum = input.Shared["7257"];
                    loanData.sTrust14ReceivableN = input.Shared["7258"];

                    loanData.sTrust15Desc = input.Shared["7260"];
                    loanData.sTrust15ReceivableAmt_rep = input.Shared["7261"];
                    loanData.sTrust15PayableAmt_rep = input.Shared["7262"];
                    loanData.sTrust15D_rep = input.Shared["7263"];
                    loanData.sTrust15ReceivableChkNum = input.Shared["7264"];
                    loanData.sTrust15PayableChkNum = input.Shared["7267"];
                    loanData.sTrust15ReceivableN = input.Shared["7268"];

                    loanData.sTrust16Desc = input.Shared["7270"];
                    loanData.sTrust16ReceivableAmt_rep = input.Shared["7271"];
                    loanData.sTrust16PayableAmt_rep = input.Shared["7272"];
                    loanData.sTrust16D_rep = input.Shared["7273"];
                    loanData.sTrust16ReceivableChkNum = input.Shared["7274"];
                    loanData.sTrust16PayableChkNum = input.Shared["7277"];
                    loanData.sTrust16ReceivableN = input.Shared["7278"];
                }
                #endregion

                // Conditions	
                int idCondBase = 6700;
                BrokerDB db = BrokerDB.RetrieveById(principal.BrokerId);
                if (db.IsUseNewTaskSystem)
                {
                    IConditionImporter conditionImporter = loanData.GetConditionImporter("POINT");
                    for (int iCond = 0; iCond < 40; ++iCond)
                    {
                        int idCurCond = (2 * iCond) + idCondBase;
                        string strCond = input.Shared[idCurCond.ToString()];
                        string strStatus = input.Shared[(idCurCond + 1).ToString()];
                        if ("" != strCond || "" != strStatus)
                        {
                            conditionImporter.AddUnique(strStatus, strCond);
                        }
                    }
                }
                else
                {

                    // 12/13/2006 nw - OPM 8410 - Handle the 2 different ways that we store conditions
                    if (principal.HasLenderDefaultFeatures == false)	// conditions stored in XML
                    {
                        for (int iCond = 0; iCond < 40; ++iCond)
                        {
                            int idCurCond = (2 * iCond) + idCondBase;
                            string strCond = input.Shared[idCurCond.ToString()];
                            string strStatus = input.Shared[(idCurCond + 1).ToString()];
                            if ("" != strCond || "" != strStatus)
                            {
                                DataAccess.CCondFieldsObsolete condFields = loanData.GetCondFieldsObsolete(-1);
                                condFields.CondDesc = strCond;
                                condFields.PriorToEventDesc = strStatus;
                                condFields.IsRequired = true;
                                condFields.Update();
                            }
                        }
                    }
                    else	// conditions stored in DB
                    {
                        LoanConditionSetObsolete lcSet = new LoanConditionSetObsolete();
                        CConditionSetObsolete dupeSet = new CConditionSetObsolete(loanData.sLId);

                        for (int iCond = 0; iCond < 40; ++iCond)
                        {
                            int idCurCond = (2 * iCond) + idCondBase;
                            string strCond = input.Shared[idCurCond.ToString()];
                            string strStatus = input.Shared[(idCurCond + 1).ToString()];
                            if ("" != strCond || "" != strStatus)
                            {
                                bool isNew = true;
                                foreach (var condition in dupeSet)
                                {
                                    if (condition.CondDesc == strCond && condition.CondCategoryDesc == strStatus)
                                    {
                                        // We alread have this one.
                                        isNew = false;
                                        break;
                                    }
                                }

                                if (isNew)
                                {
                                    CLoanConditionObsolete cond = new CLoanConditionObsolete(loanData.sBrokerId);
                                    cond.CondDesc = strCond;
                                    cond.CondCategoryDesc = strStatus;
                                    cond.LoanId = loanData.sLId;

                                    lcSet.Add(cond);
                                }
                            }
                        }
                        lcSet.Save(loanData.sBrokerId);
                    }
                }

                // FHA Fields
                string sFHAPurchPrice = input.Shared["8059"];
                if ("" == sFHAPurchPrice)
                    loanData.sFHAPurchPriceSrcT = E_sFHAPurchPriceSrcT.PurchPrice;
                else
                {
                    loanData.sFHAPurchPriceSrcT = E_sFHAPurchPriceSrcT.Locked;
                    loanData.sFHAPurchPrice_rep = sFHAPurchPrice;
                }
                loanData.sFHACondCommUnderNationalHousingAct = "X" == input.Shared["7940"];
                loanData.sFHACondCommNationalHousingActSection = input.Shared["7948"];
                loanData.sFHACondCommSeeBelow = "X" == input.Shared["7941"];
                loanData.sFHACondCommInstCaseRef = input.Shared["7873"];
                loanData.sFHACondCommIssuedD_rep = input.Shared["7946"];
                loanData.sFHACondCommExpiredD_rep = input.Shared["7947"];
                loanData.sFHACondCommImprovedLivingArea = input.Shared["7949"];
                loanData.sFHACondCommCondoComExp_rep = input.Shared["7950"];
                //--sFHACondCommMonExpenseEstimate money --7951
                loanData.sFHACondCommRemainEconLife = input.Shared["7954"];
                loanData.sFHACondCommMaxFinanceEligibleTri = FromXYesToTriState(input.Shared["7955"], input.Shared["7956"]);
                loanData.sFHACondCommIsManufacturedHousing = "X" == input.Shared["7957"];
                loanData.sFHACondCommIsSection221d = "X" == input.Shared["7959"];
                loanData.sFHACondCommSection221dAmt_rep = input.Shared["7967"];
                loanData.sFHACondCommIsAssuranceOfCompletion = "X" == input.Shared["7960"];
                loanData.sFHACondCommAssuranceOfCompletionAmt_rep = input.Shared["7968"];
                loanData.sFHACondCommSeeAttached = "X" == input.Shared["7961"];
                loanData.sFHACondCommSeeAttachedDesc = input.Shared["7966"];
                loanData.sFHACondCommSeeFollowingCondsOnBack = "X" == input.Shared["7962"];
                loanData.sFHACondCommCondsOnBack1 = input.Shared["7990"];
                loanData.sFHACondCommCondsOnBack2 = input.Shared["7991"];
                loanData.sFHACondCommCondsOnBack3 = input.Shared["7992"];
                loanData.sFHACondCommCondsOnBack4 = input.Shared["7993"];
                loanData.sFHACondCommCondsOnBack5 = input.Shared["7994"];
                loanData.sFHACondCommCondsOnBack6 = input.Shared["7995"];
                loanData.sFHACondCommCondsOnBack7 = input.Shared["7996"];

                loanData.sFHAInsEndorseProgId = input.Shared["8440"];

                input.Shared["8441"] = FromTriStateToXYes(loanData.sFHAInsEndorseInclSolarWindHeater);
                input.Shared["941"] = FromBoolToXYes(loanData.sFHAInsEndorseWillBorrBeOccupant);
                input.Shared["8442"] = FromBoolToXYes(loanData.sFHAInsEndorseWillBorrBeLandlord);
                input.Shared["8443"] = FromBoolToXYes(loanData.sFHAInsEndorseWillBorrBeCorp);

                //--Exist: sFHAIsNonProfit --925 (Point bug on this form, it shows 924 on the screen
                //--Exist: sFHAIsGovAgency --926
                //--sFHAIsEndorseIsGovAgencyOrNonProfit == sFHAIsNonProfit | sFHAIsGovAgency, --?? for the print out
                loanData.sFHAInsEndorseRepairEscrowTri = FromXYesToTriState(input.Shared["8444"]);
                loanData.sFHAInsEndorseRepairCompleteD = input.Shared["8445"];
                loanData.sFHAInsEndorseRepairEscrowAmt_rep = input.Shared["8446"];
                switch (input.Shared["8448"].ToLower())
                {
                    case "lender": loanData.sFHAInsEndorseCounselingT = E_sFHAInsEndorseCounselingT.Lender; break;
                    case "none": loanData.sFHAInsEndorseCounselingT = E_sFHAInsEndorseCounselingT.None; break;
                    case "third party": loanData.sFHAInsEndorseCounselingT = E_sFHAInsEndorseCounselingT.ThirdParty; break;
                    default: loanData.sFHAInsEndorseCounselingT = E_sFHAInsEndorseCounselingT.Blank; break;
                }

                loanData.sFHAInsEndorseOrigMortgageeId = input.Shared["8584"];
                loanData.sFHAInsEndorseAgentNum = input.Shared["8585"];
                loanData.sFHAInsEndorseMMaturityDate = input.Shared["8588"];
                loanData.sFHAInsEndorseWarrantyEnrollNum = input.Shared["8589"];
                loanData.sFHAInsEndorseAmortPlanCode = input.Shared["8590"];
                loanData.sFHAInsEndorseConstructionCode = input.Shared["8592"];
                loanData.sFHAInsEndorseLivingUnits = input.Shared["8593"];
                loanData.sFHAInsEndorseExemptFromSSNTri = FromXYesToTriState(input.Shared["8447"]);
                loanData.sFHAInsEndorseVeteranPrefTri = FromXYesToTriState(input.Shared["8582"]);
                loanData.sFHAInsEndorseEnergyEffMTri = FromXYesToTriState(input.Shared["8583"]);
                loanData.sFHAInsEndorseIssueMicInSponsorNmTri = FromXYesToTriState(input.Shared["8586"]);
                loanData.sFHAInsEndorseMailToSponsorTri = FromXYesToTriState(input.Shared["8587"]);
                loanData.sFHAInsEndorseCurrentPmtTri = FromXYesToTriState(input.Shared["8591"]);
                loanData.sFHAInsEndorseUfmipFinancedTri = FromXYesToTriState(input.Shared["8594"]);

                loanData.sFHAApprIsFairTri = ("X" == input.Shared["8081"]) ? E_TriState.Yes : E_TriState.No;
                loanData.sFHAApprNotFairExplanation = input.Shared["8082"];
                loanData.sFHAApprQualityComment = input.Shared["8083"];
                loanData.sFHAApprAreComparablesAcceptableTri = ("X" == input.Shared["8084"]) ? E_TriState.Yes : E_TriState.No;
                loanData.sFHAApprComparablesComment = input.Shared["8085"];
                loanData.sFHAApprAdjAcceptableTri = ("X" == input.Shared["8086"]) ? E_TriState.Yes : E_TriState.No;
                loanData.sFHAApprAdjNotAcceptableExplanation = input.Shared["8087"];
                loanData.sFHAApprIsValAcceptableTri = ("X" == input.Shared["8088"]) ? E_TriState.Yes : E_TriState.No;
                loanData.sFHAApprValNeedCorrectionTri = ("X" == input.Shared["8089"]) ? E_TriState.Yes : E_TriState.No;
                loanData.sFHAApprCorrectedVal_rep = input.Shared["8090"];
                loanData.sFHAApprValCorrectionJustification = input.Shared["8091"];
                loanData.sFHAApprRepairConditions = input.Shared["8092"];
                loanData.sFHAApprOtherComments = input.Shared["8093"];
                loanData.sFHALDiscnt_rep = input.Shared["7549"];
                loanData.sFHAProMInsMon_rep = input.Shared["7573"];

                loanData.sFHALenderIdCode = FirstNonEmptyString(input.Shared["7588"], input.Shared["7943"]);
                loanData.sFHASponsorAgentIdCode = FirstNonEmptyString(input.Shared["7589"], input.Shared["7944"]);
                loanData.sFHAPurposeIsPurchaseExistHome = "X" == input.Shared["7600"];
                loanData.sFHAPurposeIsFinanceImprovement = "X" == input.Shared["7601"];
                loanData.sFHAPurposeIsRefinance = "X" == input.Shared["7602"];
                loanData.sFHAPurposeIsPurchaseNewCondo = "X" == input.Shared["7603"];
                loanData.sFHAPurposeIsPurchaseExistCondo = "X" == input.Shared["7604"];
                loanData.sFHAPurposeIsPurchaseNewHome = "X" == input.Shared["7605"];
                loanData.sFHAPurposeIsConstructHome = "X" == input.Shared["7606"];
                loanData.sFHAPurposeIsFinanceCoopPurchase = "X" == input.Shared["7607"];
                loanData.sFHAPurposeIsPurchaseManufacturedHome = "X" == input.Shared["7608"];
                loanData.sFHAPurposeIsManufacturedHomeAndLot = "X" == input.Shared["7609"];
                loanData.sFHAPurposeIsRefiManufacturedHomeToBuyLot = "X" == input.Shared["7610"];
                loanData.sFHAPurposeIsRefiManufacturedHomeOrLotLoan = "X" == input.Shared["7611"];


                loanData.sFHARefinanceTypeDesc = input.Shared["7614"];

                loanData.sFHAPro1stMPmt_rep = input.Shared["7650"];
                if (loanData.sFHAProMIns_rep != input.Shared["7651"])
                {
                    loanData.sFHAProMIns_rep = input.Shared["7651"];
                    loanData.sFHAProMInsLckd = true;
                }
                loanData.sFHAProHoAssocDues_rep = input.Shared["7652"];
                loanData.sFHAProGroundRent_rep = input.Shared["7653"];
                loanData.sFHAPro2ndFinPmt_rep = input.Shared["7654"];
                loanData.sFHAProHazIns_rep = input.Shared["7655"];
                loanData.sFHAProRealETx_rep = input.Shared["7656"];

                if ("X" == input.Shared["8100"])
                    loanData.sFHACommitStageT = E_sFHACommitStageT.Conditional;
                else if ("X" == input.Shared["8101"])
                    loanData.sFHACommitStageT = E_sFHACommitStageT.Firm;
                else
                    loanData.sFHACommitStageT = E_sFHACommitStageT.Blank;

                loanData.sFHAIsGovAgency = "X" == input.Shared["926"];
                loanData.sFHAIsNonProfit = "X" == input.Shared["925"];
                loanData.sFHAIsEscrowCommitment = "X" == input.Shared["8108"];
                loanData.sFHAIsExistingDebt = "X" == input.Shared["8109"];
                loanData.sFHASpAsIsVal_rep = input.Shared["8102"];
                loanData.sFHASpAfterImprovedVal_rep = input.Shared["8103"];
                //--sFHASpAfterImprovedVal110pc -- 8104
                loanData.sFHA203kBorPdCc_rep = input.Shared["8105"];
                loanData.sFHA203kAllowEnergyImprovement_rep = input.Shared["8163"];
                loanData.sFHA203kRepairCost_rep = input.Shared["8110"];
                loanData.sFHA203kRepairCostReserveR_rep = input.Shared["8140"];
                //--sFHA203kRepairCostReserve money 8111
                loanData.sFHA203kInspectCount_rep = input.Shared["8141"];
                loanData.sFHA203kCostPerInspect_rep = input.Shared["8142"];
                loanData.sFHA203kTitleDrawCount_rep = input.Shared["8143"];
                loanData.sFHA203kCostPerTitleDraw_rep = input.Shared["8144"];
                //--sFHA203kInspectAndTitleFee money 8112
                loanData.sFHA203kMonhtlyPmtEscrowed_rep = input.Shared["8145"];
                loanData.sFHA203kPmtEscrowedMonths_rep = input.Shared["8146"];
                //--sFHA203kPmtEscrowed money --8113
                //--sFHA203kRehabEscrowAccountSubtot  -- 8114
                loanData.sFHA203kEngineeringFee_rep = input.Shared["8115"];
                loanData.sFHA203kConsultantFee_rep = input.Shared["8116"];
                loanData.sFHA203kOtherFee_rep = input.Shared["8117"];
                loanData.sFHA203kPlanReviewerMiles_rep = input.Shared["8147"];
                loanData.sFHA203kPlanReviewerCostPerMile_rep = input.Shared["8148"];
                //--sFHA203kPlanReviewerFee money --8118
                //--sFHA203kB5toB9Subtot money --8119
                //--sFHA203kSupplementOrigFee money --8120
                loanData.sFHA203kRepairDiscountFeePt_rep = input.Shared["8149"];
                //--sFHA203kRepairDiscount money 8121
                //--sFHA203kReleaseAtClosingSubtot money 8122
                //--sFHA203kRehabCostTot money 8123
                loanData.sFHA203kMaxMAmtC5_rep = input.Shared["8128"];
                loanData.sFHA203kMaxMAmtC5Lckd = "X" == input.Shared["8076"];
                loanData.sFHA203kActualCashInvRequired_rep = input.Shared["8137"];
                loanData.sFHA203kActualCashInvRequiredLckd = "X" == input.Shared["8075"];
                loanData.sFHA203kAdjMaxMAmt_rep = input.Shared["8166"];
                loanData.sFHA203kD1_rep = input.Shared["8129"];
                loanData.sFHA203kD1Lckd = "X" == input.Shared["8074"];
                loanData.sFHA203kMaxMAmtD5_rep = input.Shared["8133"];
                loanData.sFHA203kMaxMAmtD5Lckd = "X" == input.Shared["8077"];
                loanData.sFHA203kE1_rep = input.Shared["8134"];
                loanData.sFHA203kE1Lckd = "X" == input.Shared["8078"];
                loanData.sFHA203kEnergyEfficientMAmt_rep = input.Shared["8164"];
                loanData.sFHA203kRemarks = input.Shared["8155"] + " " + input.Shared["8156"];
                loanData.sFHA203kEscrowedFundsTot_rep = input.Shared["8165"];

                if ("X" == input.Shared["8135"])
                    loanData.sFHA203kBorrAcknowledgeT = E_sFHA203kBorrAcknowledgeT.ToBorrower;
                else if ("X" == input.Shared["8139"])
                    loanData.sFHA203kBorrAcknowledgeT = E_sFHA203kBorrAcknowledgeT.ToPrinciple;
                else if ("X" == input.Shared["8150"])
                    loanData.sFHA203kBorrAcknowledgeT = E_sFHA203kBorrAcknowledgeT.Other;
                else
                    loanData.sFHA203kBorrAcknowledgeT = E_sFHA203kBorrAcknowledgeT.Blank;

                loanData.sFHA203kBorrAcknowledgeDesc = input.Shared["8161"];

                if (string.IsNullOrEmpty(input.Shared["7522"].TrimWhitespaceAndBOM()) == false)
                {
                    try
                    {
                        decimal cashPd = decimal.Parse(input.Shared["7522"]);
                        if (cashPd != loanData.sFfUfmip1003)
                        {
                            loanData.sFfUfMipIsBeingFinanced = true;
                            loanData.sUfCashPdLckd = true;
                        }
                        loanData.SetsUfCashPdViaImport(input.Shared["7522"]); // OPM 32441
                    }
                    catch (Exception e)
                    {
                        Tools.LogError("Unable to parse cash paid as a decimal during Point import: " + input.Shared["7522"], e);
                    }
                }

                loanData.sFfUfmipR_rep = input.Shared["7687"];
                loanData.sFHAHousingActSection = input.Shared["7501"];

                if ("X" == input.Shared["7502"])
                    loanData.sFHAConstructionT = E_sFHAConstructionT.Existing;
                else if ("X" == input.Shared["7503"])
                    loanData.sFHAConstructionT = E_sFHAConstructionT.Proposed;
                else
                    loanData.sFHAConstructionT = E_sFHAConstructionT.Blank;

                loanData.sFHACcTot_rep = input.Shared["7670"];
                loanData.sFHACcPbs_rep = input.Shared["7671"];
                loanData.sFHAReqAdj_rep = input.Shared["8063"];
                loanData.sFHALtv_rep = input.Shared["8065"]; // should be the same as 8080
                loanData.sFHALtvLckd = "X" == input.Shared["8073"];
                loanData.sFHAPrepaidExp_rep = input.Shared["7521"];
                loanData.sFHADiscountPoints_rep = input.Shared["7520"];
                loanData.sFHAImprovements_rep = input.Shared["7519"];
                loanData.sFHANonrealty_rep = input.Shared["7523"];
                loanData.sFHANonrealtyLckd = "X" == input.Shared["7612"];
                loanData.sFHAAmtPaid_rep = input.Shared["7524"];
                loanData.sFHA2ndMSrc = input.Shared["8071"];
                loanData.sFHA2ndMAmt_rep = input.Shared["7528"];
                loanData.sFHASellerContribution_rep = input.Shared["7553"];
                loanData.sFHAExcessContribution_rep = input.Shared["7554"];
                loanData.sFHAGiftAmtTot_rep = input.Shared["7568"];
                bool fNeedLineBreak = false;
                for (int iField = 7555; iField <= 7564; ++iField)
                {
                    val = input.Shared[iField.ToString()];
                    if ("" != val)
                    {
                        if (fNeedLineBreak)
                            strBuilder.Append("\n");
                        fNeedLineBreak = true;
                        strBuilder.Append(val);
                    }
                }
                loanData.sFHACreditAnalysisRemarks = strBuilder.ToString();
                loanData.sFHAReqCashInv_rep = input.Shared["7615"];
                loanData.sFHAExistingMLien_rep = input.Shared["7529"];
                loanData.sFHASalesConcessionsDesc = input.Shared["7676"];
                loanData.sFHASalesConcessions_rep = input.Shared["7675"];
                //--sFHAMBasisRefin		money				7537
                loanData.sFHAMBasisRefinMultiply_rep = input.Shared["7514"];
                loanData.sFHAMBasisRefinMultiplyLckd = "X" == input.Shared["7516"];
                loanData.sFHAApprValMultiply_rep = input.Shared["7515"];
                loanData.sFHAApprValMultiplyLckd = "X" == input.Shared["7517"];
                loanData.sFHAIsAmtPdInCash = "X" == input.Shared["7510"];
                loanData.sFHAIsAmtPdInOther = "X" == input.Shared["7511"];
                loanData.sFHAIsAmtToBePdInCash = "X" == input.Shared["7512"];
                loanData.sFHAIsAmtToBePdInOther = "X" == input.Shared["7513"];
                //--sFHAAmtToBePd		money					7525
                loanData.sFHAImprovementsDesc = input.Shared["7518"];
                //--sFHAReqTot		money					20345
                //--sFHAReqInvestment		money					7536


                // HMDA 
                try { loanData.sHmdaPreapprovalT = (E_sHmdaPreapprovalT)int.Parse(input.Shared["6280"]); }
                catch { loanData.sHmdaPreapprovalT = E_sHmdaPreapprovalT.LeaveBlank; }

                loanData.sHmdaAprRateSpread = input.Shared["6281"];
                loanData.sHmdaReportAsHoepaLoan = "X" == input.Shared["6282"];

                try { loanData.sHmdaLienT = (E_sHmdaLienT)int.Parse(input.Shared["6283"]); }
                catch { loanData.sHmdaLienT = E_sHmdaLienT.LeaveBlank; }

                try
                {
                    E_sHmdaPropT t = (E_sHmdaPropT)int.Parse(input.Shared["6284"]);
                    if (t != loanData.sHmdaPropT)
                    {
                        // 1/27/2010 dd - Only set sHmdaPropT if our value is different than Point.
                        loanData.sHmdaPropTLckd = true;
                        loanData.sHmdaPropT = t;
                    }
                }
                catch { loanData.sHmdaPropT = E_sHmdaPropT.LeaveBlank; }

                loanData.sHmdaPurchaser2004T = E_sHmdaPurchaser2004T.LeaveBlank;
                string s6285 = input.Shared["6285"];
                if ("" != s6285)
                {
                    try { loanData.sHmdaPurchaser2004T = (E_sHmdaPurchaser2004T)(1 + int.Parse(s6285)); }
                    catch { }
                }


                loanData.sHmdaExcludedFromReport = "X" == input.Shared["6223"];
                loanData.sHmdaReportAsHomeImprov = "X" == input.Shared["6228"];
                loanData.sHmdaMsaNum = input.Shared["6210"];
                loanData.sHmdaCountyCode = input.Shared["6211"];
                loanData.sHmdaCensusTract = input.Shared["6212"];
                loanData.sHmdaPurchaser = input.Shared["6214"];
                loanData.sHmdaActionTaken = input.Shared["6216"];
                loanData.sHmdaActionD_rep = input.Shared["6217"];
                loanData.sHmdaDenialReason1 = input.Shared["6215"];
                loanData.sHmdaDenialReason2 = input.Shared["6218"];
                loanData.sHmdaDenialReason3 = input.Shared["6219"];
                loanData.sHmdaLoanDenied = "X" == input.Shared["6047"];
                loanData.sHmdaDeniedFormDone = "X" == input.Shared["6271"];
                loanData.sHmdaCounterOfferMade = "X" == input.Shared["6276"];
                loanData.sHmdaLoanDeniedBy = input.Shared["6048"];
                loanData.sHmdaDeniedFormDoneD_rep = input.Shared["3840"];
                loanData.sHmdaDeniedFormDoneBy = input.Shared["6272"];
                loanData.sHmdaCounterOfferMadeD_rep = input.Shared["6275"];
                loanData.sHmdaCounterOfferMadeBy = input.Shared["6277"];
                loanData.sHmdaCounterOfferDetails = input.Shared["6278"];

                loanData.sLpTemplateNm = input.Shared["7403"];
                loanData.sCcTemplateNm = input.Shared["7404"];

                loanData.sRLckdD_rep = input.Shared["6061"];
                loanData.Set_sRLckdExpiredD_Manually(input.Shared["6063"]);

                loanData.sProfitGrossBorPnt_rep = input.Shared["6170"];
                loanData.sProfitGrossBorMb_rep = input.Shared["6171"];
                // In POINT, the label is Lender Charge. Therefore Gross Profit = Borrower Point - Lender Charge.
                // In our system, we use Rebate Fee, therefore we have to take a negative of Lender Charge in Point.
                try
                {
                    loanData.sProfitRebatePnt = -1 * decimal.Parse(input.Shared["6064"]);
                }
                catch { }
                try
                {
                    loanData.sProfitRebateMb = -1 * decimal.Parse(input.Shared["6176"]);
                }
                catch { }


                loanData.sSubmitBrokerFax = input.Shared["4011"];
                loanData.sSubmitProgramCode = input.Shared["4010"];
                loanData.sSubmitPropTDesc = input.Shared["4000"];
                loanData.sSubmitDocFull = "X" == input.Shared["4001"];
                loanData.sSubmitDocOther = "X" == input.Shared["4002"];
                loanData.sSubmitImpoundTaxes = "X" == input.Shared["4003"];
                loanData.sSubmitImpoundHazard = "X" == input.Shared["4004"];
                loanData.sSubmitImpoundMI = "X" == input.Shared["4005"];
                loanData.sSubmitImpoundFlood = "X" == input.Shared["4006"];
                loanData.sSubmitImpoundOther = "X" == input.Shared["4007"];
                loanData.sSubmitImpoundOtherDesc = input.Shared["4008"];
                loanData.sSubmitBuydownDesc = input.Shared["4013"];
                loanData.sSubmitAmortDesc = input.Shared["4012"];
                loanData.sSubmitRAdjustOtherDesc = input.Shared["4014"];
                loanData.sSubmitInitPmtCapR_rep = input.Shared["2326"];
                loanData.sRLckdIsLocked = "X" == input.Shared["6101"];
                loanData.sRLckdNumOfDays_rep = input.Shared["6062"];
                loanData.sSubmitMIYes = "X" == input.Shared["4016"];
                loanData.sSubmitMITypeDesc = input.Shared["4018"];
                loanData.sDemandLOrigFeeLenderPc_rep = input.Shared["4080"];
                loanData.sDemandLOrigFeeLenderMb_rep = input.Shared["4081"];
                //loanData.sDemandLOrigFeeLenderAmt_rep = input.Shared["4082"]; calculated field
                //loanData.sDemandLOrigFeeBrokerPc_rep = input.Shared["4040"];
                //loanData.sDemandLOrigFeeBrokerMb_rep = input.Shared["4041"];
                //loanData.sDemandLOrigFeeBrokerAmt_rep = input.Shared["4042"]; cal
                loanData.sDemandLDiscountLenderPc_rep = input.Shared["4083"];
                loanData.sDemandLDiscountLenderMb_rep = input.Shared["4084"];
                //loanData.sDemandLDiscountLenderAmt_rep = input.Shared["4085"];
                //loanData.sDemandLDiscountBrokerPc_rep = input.Shared["4043"];
                //loanData.sDemandLDiscountBrokerMb_rep = input.Shared["4044"];
                //loanData.sDemandLDiscountLenderAmt_rep = input.Shared["4045"];
                loanData.sDemandYieldSpreadPremiumLenderPc_rep = input.Shared["4086"];
                loanData.sDemandYieldSpreadPremiumLenderMb_rep = input.Shared["4087"];
                //loanData.sDemandYieldSpreadPremiumLenderAmt_rep = input.Shared["4088"];
                //loanData.sDemandYieldSpreadPremiumBrokerPc_rep = input.Shared["4046"];
                //loanData.sDemandYieldSpreadPremiumBrokerMb_rep = input.Shared["4047"];
                //loanData.sDemandYieldSpreadPremiumBrokerAmt_rep = input.Shared["4048"];
                loanData.sDemandYieldSpreadPremiumBorrowerPc_rep = input.Shared["4026"];
                loanData.sDemandYieldSpreadPremiumBorrowerMb_rep = input.Shared["4027"];
                //loanData.sDemandYieldSpreadPremiumBrokerAmt_rep = input.Shared["4028"];
                loanData.sDemandApprFeeLenderAmt_rep = input.Shared["4089"];
                loanData.sDemandApprFeeBrokerPaidAmt_rep = input.Shared["4050"];
                //loanData.sDemandApprFeeBrokerDueAmt_rep = input.Shared["4051"];
                loanData.sDemandCreditReportFeeLenderAmt_rep = input.Shared["4090"];
                loanData.sDemandCreditReportFeeBrokerPaidAmt_rep = input.Shared["4052"];
                //loanData.sDemandCreditReportBrokerDueAmt_rep = input.Shared["4053"];
                loanData.sDemandProcessingFeeLenderAmt_rep = input.Shared["4091"];
                loanData.sDemandProcessingFeeBrokerPaidAmt_rep = input.Shared["4054"];
                //loanData.sDemandProcessingFeeBrokerDueAmt_rep = input.Shared["4055"];
                loanData.sDemandLoanDocFeeLenderAmt_rep = input.Shared["4092"];
                loanData.sDemandLoanDocFeeBrokerPaidAmt_rep = input.Shared["4056"];
                //loanData.sDemandLoanDocBrokerDueAmt_rep = input.Shared["4057"];
                loanData.sDemandLoanDocFeeBorrowerAmt_rep = input.Shared["4074"];

                loanData.sDemandU1LenderDesc = input.Shared["4019"];
                loanData.sDemandU2LenderDesc = input.Shared["4020"];
                loanData.sDemandU3LenderDesc = input.Shared["4021"];
                loanData.sDemandU4LenderDesc = input.Shared["4022"];
                loanData.sDemandU5LenderDesc = input.Shared["4023"];
                loanData.sDemandU6LenderDesc = input.Shared["4024"];
                loanData.sDemandU7LenderDesc = input.Shared["4025"];

                loanData.sDemandU1LenderAmt_rep = input.Shared["4030"];
                loanData.sDemandU2LenderAmt_rep = input.Shared["4031"];
                loanData.sDemandU3LenderAmt_rep = input.Shared["4032"];
                loanData.sDemandU4LenderAmt_rep = input.Shared["4033"];
                loanData.sDemandU5LenderAmt_rep = input.Shared["4034"];
                loanData.sDemandU6LenderAmt_rep = input.Shared["4035"];
                loanData.sDemandU7LenderAmt_rep = input.Shared["4036"];

                loanData.sDemandU1BrokerPaidAmt_rep = input.Shared["4058"];
                loanData.sDemandU2BrokerPaidAmt_rep = input.Shared["4060"];
                loanData.sDemandU3BrokerPaidAmt_rep = input.Shared["4062"];
                loanData.sDemandU4BrokerPaidAmt_rep = input.Shared["4064"];
                loanData.sDemandU5BrokerPaidAmt_rep = input.Shared["4066"];
                loanData.sDemandU6BrokerPaidAmt_rep = input.Shared["4068"];
                loanData.sDemandU7BrokerPaidAmt_rep = input.Shared["4070"];

                //loanData.sDemandU1BrokerDueAmt_rep = input.Shared["4059"];
                //loanData.sDemandU2BrokerDueAmt_rep = input.Shared["4061"];
                //loanData.sDemandU3BrokerDueAmt_rep = input.Shared["4063"];
                //loanData.sDemandU4BrokerDueAmt_rep = input.Shared["4065"];
                //loanData.sDemandU5BrokerDueAmt_rep = input.Shared["4067"];
                //loanData.sDemandU6BrokerDueAmt_rep = input.Shared["4069"];
                //loanData.sDemandU7BrokerDueAmt_rep = input.Shared["4071"];

                loanData.sDemandU1BorrowerAmt_rep = input.Shared["4093"];
                loanData.sDemandU2BorrowerAmt_rep = input.Shared["4094"];
                loanData.sDemandU3BorrowerAmt_rep = input.Shared["4095"];
                loanData.sDemandU4BorrowerAmt_rep = input.Shared["4096"];
                loanData.sDemandU5BorrowerAmt_rep = input.Shared["4097"];
                loanData.sDemandU6BorrowerAmt_rep = input.Shared["4098"];
                loanData.sDemandU7BorrowerAmt_rep = input.Shared["4099"];

                //loanData.sDemandTotalLenderAmt_rep	  = input.Shared["4037"];
                //loanData.sDemandTotalBrokerPaidAmt_rep  = input.Shared["4072"];
                //loanData.sDemandTotalBrokerDueAmt_rep	  = input.Shared["4073"];
                //loanData.sDemandTotalBorrowerAmt_rep	  = input.Shared["4100"];
                strBuilder.Remove(0, strBuilder.Length);

                strBuilder.Append(input.Shared["4101"]);
                strBuilder.Append(input.Shared["4102"]);
                strBuilder.Append(input.Shared["4103"]);
                strBuilder.Append(input.Shared["4104"]);
                strBuilder.Append(input.Shared["4105"]);
                strBuilder.Append(input.Shared["4106"]);
                strBuilder.Append(input.Shared["4107"]);
                strBuilder.Append(input.Shared["4108"]);
                strBuilder.Append(input.Shared["4109"]);
                strBuilder.Append(input.Shared["4110"]);
                strBuilder.Append(input.Shared["4111"]);
                strBuilder.Append(input.Shared["4112"]);
                strBuilder.Append(input.Shared["4113"]);
                strBuilder.Append(input.Shared["4114"]);

                loanData.sDemandNotes = strBuilder.ToString().TrimWhitespaceAndBOM();

                loanData.sLenderNumVerif = input.Shared["215"];


                // 2/3/2005 dd - Survey Request fields
                loanData.sAttachedContractBit = "X" == input.Shared["2405"];
                loanData.sAttachedSurveyBit = "X" == input.Shared["2406"];
                loanData.sAttachedPlansBit = "X" == input.Shared["2407"];
                loanData.sAttachedCostsBit = "X" == input.Shared["2408"];
                loanData.sAttachedSpecsBit = "X" == input.Shared["2409"];
                loanData.sSurveyRequestN = input.Shared["2411"];
                // ASSIGNED EMPLOYEES FROM INTERNAL TRACK

                String agentNm = "";

                // Underwriter
                //loanData.sEmployeeUnderwriterId = Guid.Empty; // We don't support assigned underwriter yet.

                // Administrator
                //loanData.sEmployeeAdministratorId = Guid.Empty; // We don't support assigned administrator.

                // Loan Opener
                // 7/21/2004 dd - Use loan opener specify by user at import time.

                RoleChangeRecord openRecord = new RoleChangeRecord();

                m_roleChanges.Add(CEmployeeFields.s_LoanOpenerId, openRecord);

                openRecord.Changed = m_defaultAssignLoanOpenerID; // Point doesn't have loan opener field.

                openRecord.Current = loanData.sEmployeeLoanOpenerId;
                loanData.sEmployeeLoanOpenerId = openRecord.Changed;


                // Lock Desk 
                // 04/29/2008 av
                RoleChangeRecord lockDeskRecord = new RoleChangeRecord();
                m_roleChanges.Add(CEmployeeFields.s_LockDeskRoleId, lockDeskRecord);
                lockDeskRecord.Changed = this.m_defaultAssignLockDeskID;
                lockDeskRecord.Current = loanData.sEmployeeLockDeskId;
                loanData.sEmployeeLockDeskId = lockDeskRecord.Changed;


                //Closer - aj
                RoleChangeRecord closerRecord = new RoleChangeRecord();
                m_roleChanges.Add(CEmployeeFields.s_CloserId, closerRecord);
                closerRecord.Changed = m_defaultAssignCloserID;
                closerRecord.Current = loanData.sEmployeeCloserId;
                loanData.sEmployeeCloserId = closerRecord.Changed;

                // Telemarketer
                // 8/16/2004 kb - Use call center agent specified in ui at import.

                RoleChangeRecord teleRecord = new RoleChangeRecord();

                m_roleChanges.Add(CEmployeeFields.s_CallCenterAgentRoleId, teleRecord);

                teleRecord.Changed = m_defaultAssignCallCenterAgentID; // Point doesn't have field for telemarketer.

                teleRecord.Current = loanData.sEmployeeCallCenterAgentId;
                loanData.sEmployeeCallCenterAgentId = teleRecord.Changed;





                // Agent, Loan rep in the Borrower Information. This is used when she is an internal employee.
                // 7/15/2004 dd - 11111111-1111-1111-1111-111111111111   Try to match Loan Processor name from Point file with name from DB.
                //                00000000-0000-0000-0000-000000000000   Don't Assign Processor
                //                      ELSE                             Assign using internal employee id
                //
                // 8/3/2004 kb - I need the previous processor id so I can
                // notify the employee (assuming we find him/her) that they
                // are not the processor when we import.  So, we will pull
                // the employee upfront.

                RoleChangeRecord lrepRecord = new RoleChangeRecord();

                m_roleChanges.Add(CEmployeeFields.s_LoanRepRoleId, lrepRecord);
                agentNm = input.Shared["19"].TrimWhitespaceAndBOM();

                if ("" != agentNm)
                {
                    m_nameParser.ParseName(agentNm);

                    lrepRecord.Default = MapToEmployeeId
                        (m_nameParser.FirstName
                        , m_nameParser.LastName
                        , E_RoleT.LoanOfficer
                        );
                }

                if (m_defaultAssignLoanOfficerID == new Guid("11111111-1111-1111-1111-111111111111"))
                {
                    lrepRecord.Changed = lrepRecord.Default;
                    m_defaultAssignLoanOfficerID = lrepRecord.Default;
                }
                else if (m_defaultAssignLoanOfficerID != Guid.Empty)
                {
                    lrepRecord.Changed = m_defaultAssignLoanOfficerID;
                }

                lrepRecord.Current = loanData.sEmployeeLoanRepId;
                loanData.sEmployeeLoanRepId = lrepRecord.Changed;
                Guid currentLOGuid = lrepRecord.Changed;

                // Manager
                // 7/21/2004 dd - Use manager specify by user at import time.
                // OPM 9831 - use the manager relationship of the assigned loan officer
                RoleChangeRecord lmgrRecord = new RoleChangeRecord();
                m_roleChanges.Add(CEmployeeFields.s_ManagerRoleId, lmgrRecord);

                if (m_defaultAssignManagerID == new Guid("11111111-1111-1111-1111-111111111111"))
                {
                    lmgrRecord.Default = GetRelationshipOfGivenUser(currentLOGuid, E_RoleT.Manager);
                    lmgrRecord.Changed = lmgrRecord.Default;
                    m_defaultAssignManagerID = lmgrRecord.Default;
                }
                else
                {
                    lmgrRecord.Changed = m_defaultAssignManagerID; // Point doesn't have manager role field.
                }

                lmgrRecord.Current = loanData.sEmployeeManagerId;
                loanData.sEmployeeManagerId = lmgrRecord.Changed;


                // Processor
                // 7/15/2004 dd - 11111111-1111-1111-1111-111111111111   Try to match Loan Processor name from Point file with name from DB.
                //                00000000-0000-0000-0000-000000000000   Don't Assign Processor
                //                      ELSE                             Assign using internal employee id
                //
                // 8/3/2004 kb - I need the previous processor id so I can
                // notify the employee (assuming we find him/her) that they
                // are not the processor when we import.  So, we will pull
                // the employee upfront.

                RoleChangeRecord procRecord = new RoleChangeRecord();

                m_roleChanges.Add(CEmployeeFields.s_ProcessorRoleId, procRecord);

                // OPM 9831 - no longer match the processor data from the imported file,
                // instead match the processor based on the assigned LO's processor relationship setting.
                /*agentNm = input.Shared["18"].TrimWhitespaceAndBOM();

                if ("" != agentNm)
                {
                    m_nameParser.ParseName(agentNm);

                    procRecord.Default = MapToEmployeeId
                        (m_nameParser.FirstName
                        , m_nameParser.LastName
                        , CEmployeeFields.E_Role.Processor
                        );
                }*/

                //Get the LO's processor here
                if (m_defaultAssignLoanProcessorID == new Guid("11111111-1111-1111-1111-111111111111"))
                {
                    procRecord.Default = GetRelationshipOfGivenUser(currentLOGuid, E_RoleT.Processor);
                    procRecord.Changed = procRecord.Default;
                    m_defaultAssignLoanProcessorID = procRecord.Default;
                }
                else if (m_defaultAssignLoanProcessorID != Guid.Empty)
                {
                    procRecord.Changed = m_defaultAssignLoanProcessorID;
                }

                procRecord.Current = loanData.sEmployeeProcessorId;
                loanData.sEmployeeProcessorId = procRecord.Changed;

                // LenderAccountExec
                //
                // 8/16/2004 kb - We now set the assignment in the ui.
                // OPM 9831 - use the lender AE relationship of the assigned loan officer
                RoleChangeRecord llaeRecord = new RoleChangeRecord();

                m_roleChanges.Add(CEmployeeFields.s_LenderAccountExecRoleId, llaeRecord);

                //Get the LO's lender AE here
                if (m_defaultAssignLenderAccountExecID == new Guid("11111111-1111-1111-1111-111111111111"))
                {
                    llaeRecord.Default = GetRelationshipOfGivenUser(currentLOGuid, E_RoleT.RealEstateAgent);
                    llaeRecord.Changed = llaeRecord.Default;
                    m_defaultAssignLenderAccountExecID = llaeRecord.Default;
                }
                else
                {
                    llaeRecord.Changed = m_defaultAssignLenderAccountExecID; // Point lacks this role assignment.
                }
                llaeRecord.Current = loanData.sEmployeeLenderAccExecId;
                loanData.sEmployeeLenderAccExecId = llaeRecord.Changed;

                // RealEstateAgent
                // See input.Shared["6140"] below
                //
                // 8/16/2004 kb - Not anymore.  We now set the assignment in the ui.

                RoleChangeRecord lreaRecord = new RoleChangeRecord();

                m_roleChanges.Add(CEmployeeFields.s_RealEstateAgentRoleId, lreaRecord);

                lreaRecord.Changed = m_defaultAssignRealEstateAgentID; // Point has this role as an agent, but we want explicitness.

                lreaRecord.Current = loanData.sEmployeeRealEstateAgentId;
                loanData.sEmployeeRealEstateAgentId = lreaRecord.Changed;

                // Funder
                // loanData.sEmployeeFunderId = Guid.Empty; // Point doesn't have funder field. We don't support assignment either.

                #region Agents
                // AGENTS FROM AGENTS TRACK


                DataSet ds = loanData.sAgentDataSet;
                DataTable dt = ds.Tables[0];

                // Loan rep in the Agent Track screen. This is used when she is not an employee.
                //av 05 20 08 opm 2199 
                if ((!IsRolesImportedAsAgents || m_defaultAssignLoanOfficerID == Guid.Empty) && (input.Shared["1508"].TrimWhitespaceAndBOM() != String.Empty || input.Shared["1509"].TrimWhitespaceAndBOM() != String.Empty || input.Shared["12366"] != String.Empty))
                {
                    //av 05 20 08 opm 2199 
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.LoanOfficer, dt, loanData);
                    agent.AgentName = input.Shared["1508"].TrimWhitespaceAndBOM();
                    agent.Phone = input.Shared["1509"];
                    agent.FaxNum = input.Shared["1516"];
                    agent.CompanyName = input.Shared["381"];
                    agent.StreetAddr = input.Shared["383"];
                    agent.City = input.Shared["384"];
                    agent.State = input.Shared["385"];
                    agent.Zip = input.Shared["386"];
                    agent.Notes = input.Shared["6178"];
                    //av 05 20 08 opm 2199 
                    agent.EmailAddr = input.Shared["12366"];
                    agent.CellPhone = input.Shared["12365"];

                    agent.CommissionPointOfLoanAmount_rep = input.Shared["6177"];
                    agent.CommissionPointOfGrossProfit_rep = input.Shared["6179"];
                    agent.CommissionMinBase_rep = input.Shared["6175"];
                    agent.Update();
                }
                else if (!IsRolesImportedAsAgents || (m_defaultAssignLoanOfficerID == Guid.Empty))
                {
                    //OPM 33815 : The 1003 Section X Interviewer information from the Point file should populate to the Official Loan Officer contact record if  Loan Officer was not created with role assignment or imported from point file contacts                    
                    string nmAgent = input.Shared["1508"].TrimWhitespaceAndBOM();
                    string nmComp = input.Shared["381"].TrimWhitespaceAndBOM();
                    if (nmAgent != string.Empty || nmComp != string.Empty)
                    {
                        CAgentFields loanOfficer = GetNewAgentFor(E_AgentRoleT.LoanOfficer, dt, loanData);

                        loanOfficer.AgentName = nmAgent;
                        loanOfficer.Phone = input.Shared["1509"];
                        loanOfficer.CompanyName = nmComp;
                        loanOfficer.StreetAddr = input.Shared["383"];
                        loanOfficer.City = input.Shared["384"];
                        loanOfficer.State = input.Shared["385"];
                        loanOfficer.Zip = input.Shared["386"];
                        loanOfficer.PhoneOfCompany = input.Shared["382"];
                        loanOfficer.FaxOfCompany = input.Shared["387"];
                        loanOfficer.Update();
                    }
                }

                // Loan processor 
                //av 05 20 08 opm 2199 
                if ((!IsRolesImportedAsAgents || m_defaultAssignLoanProcessorID == Guid.Empty) && (input.Shared["18"].TrimWhitespaceAndBOM() != String.Empty))
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Processor, dt, loanData);
                    agent.AgentName = input.Shared["18"].TrimWhitespaceAndBOM();
                    agent.Phone = input.Shared["6359"];
                    agent.FaxNum = input.Shared["6360"];
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["6384"];
                    agent.EmailAddr = input.Shared["6385"];
                    agent.Update();
                }

                // Appraiser
                agentNm = input.Shared["330"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    //av 05 20 08 opm 2199 
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Appraiser, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["332"];
                    agent.FaxNum = input.Shared["335"];
                    agent.CompanyName = input.Shared["331"];
                    agent.StreetAddr = input.Shared["333"];
                    m_addressParser.ParseCityStateZip(input.Shared["334"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.LicenseNumOfAgent = input.Shared["339"];
                    agent.CaseNum = input.Shared["337"];
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12367"];
                    agent.EmailAddr = input.Shared["12368"];

                    agent.Update();
                }

                // Consumer Reporting Agency
                if ("" != input.Shared["3841"].TrimWhitespaceAndBOM())
                {
                    //av 05 20 08 opm 2199 
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.CreditReport, dt, loanData);
                    agent.CompanyName = input.Shared["3841"];
                    agent.StreetAddr = input.Shared["3842"];
                    agent.Phone = input.Shared["3844"];
                    m_addressParser.ParseCityStateZip(input.Shared["3843"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.Update();
                }

                // Escrow
                agentNm = input.Shared["6110"].TrimWhitespaceAndBOM();
                if (("" != agentNm) || ("" != input.Shared["6119"]))
                {
                    //av 05 20 08 opm 2199 
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Escrow, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["6112"];
                    agent.FaxNum = input.Shared["6115"];
                    agent.CompanyName = input.Shared["6111"];
                    agent.StreetAddr = input.Shared["6113"];
                    m_addressParser.ParseCityStateZip(input.Shared["6114"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.CaseNum = input.Shared["6119"];

                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12369"];
                    agent.EmailAddr = input.Shared["12370"];
                    agent.Update();
                }

                // Title
                agentNm = input.Shared["6120"].TrimWhitespaceAndBOM();
                if (("" != agentNm) || ("" != input.Shared["6129"]))
                {
                    //av 05 20 08 opm 2199 
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Title, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["6122"];
                    agent.FaxNum = input.Shared["6125"];
                    agent.CompanyName = input.Shared["6121"];
                    agent.StreetAddr = input.Shared["6123"];
                    m_addressParser.ParseCityStateZip(input.Shared["6124"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.CaseNum = input.Shared["6129"];
                    agent.EmailAddr = input.Shared["12372"];// opm 18526 av 05/14/08
                    agent.CellPhone = input.Shared["12371"];
                    agent.Update();

                }

                // Buyer Attorney
                agentNm = input.Shared["430"].TrimWhitespaceAndBOM();
                if (("" != agentNm) || ("" != input.Shared["439"]))
                {
                    //av 05 20 08 opm 2199 
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.BuyerAttorney, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["432"];
                    agent.FaxNum = input.Shared["435"];
                    agent.CompanyName = input.Shared["431"];
                    agent.StreetAddr = input.Shared["433"];
                    m_addressParser.ParseCityStateZip(input.Shared["434"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.LicenseNumOfAgent = input.Shared["439"];
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12373"];
                    agent.EmailAddr = input.Shared["12374"];
                    agent.Update();
                }

                // Buyer Agent
                agentNm = input.Shared["6191"].TrimWhitespaceAndBOM();
                if (("" != agentNm))
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.BuyerAgent, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.CompanyName = input.Shared["6196"];
                    agent.Phone = input.Shared["6192"];
                    agent.FaxNum = input.Shared["6193"];
                    agent.CellPhone = input.Shared["6194"];
                    agent.EmailAddr = input.Shared["6195"];
                    agent.StreetAddr = input.Shared["6197"];
                    m_addressParser.ParseCityStateZip(input.Shared["6198"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.Update();
                }


                // Selling Attorney
                agentNm = input.Shared["440"].TrimWhitespaceAndBOM();
                if (("" != agentNm) || ("" != input.Shared["449"]))
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.SellerAttorney, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["442"];
                    agent.FaxNum = input.Shared["445"];
                    agent.CompanyName = input.Shared["441"];
                    agent.StreetAddr = input.Shared["443"];
                    m_addressParser.ParseCityStateZip(input.Shared["444"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.LicenseNumOfAgent = input.Shared["449"];
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12375"];
                    agent.EmailAddr = input.Shared["12376"];
                    agent.Update();
                }

                // Listing Agent
                agentNm = input.Shared["6130"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.ListingAgent, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["6132"];
                    agent.FaxNum = input.Shared["6135"];
                    agent.CompanyName = input.Shared["6131"];
                    agent.StreetAddr = input.Shared["6133"];
                    m_addressParser.ParseCityStateZip(input.Shared["6134"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12377"];
                    agent.EmailAddr = input.Shared["12378"];
                    agent.Update();
                }

                //Investor Agent
                // 11/28/07 db - OPM 17176
                agentNm = input.Shared["7340"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Investor, dt, loanData);
                    agent.AgentName = input.Shared["7340"];
                    agent.CompanyName = input.Shared["7341"];
                    agent.Phone = input.Shared["7342"];
                    agent.StreetAddr = input.Shared["7343"];
                    agent.City = input.Shared["7344"];
                    agent.State = input.Shared["7345"];
                    agent.Zip = input.Shared["7346"];
                    agent.InvestorBasisPoints_rep = input.Shared["7347"];
                    agent.FaxNum = input.Shared["7348"];
                    agent.CellPhone = input.Shared["12363"];
                    agent.EmailAddr = input.Shared["12364"];
                    agent.LicenseNumOfAgent = input.Shared["7349"];
                    agent.Notes = input.Shared["6381"];
                    agent.Update();
                }

                // Flood Insurance Agent
                // db - OPM 33365
                agentNm = input.Shared["12786"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.FloodProvider, dt, loanData);
                    agent.AgentName = input.Shared["12786"];
                    agent.CompanyName = input.Shared["12787"];
                    agent.Phone = input.Shared["13000"];
                    agent.StreetAddr = input.Shared["12762"];
                    agent.City = input.Shared["12763"];
                    agent.State = input.Shared["12764"];
                    agent.Zip = input.Shared["12765"];
                    agent.FaxNum = input.Shared["13002"];
                    agent.CellPhone = input.Shared["13001"];
                    agent.EmailAddr = input.Shared["13003"];
                    agent.CaseNum = input.Shared["12969"];
                    agent.Update();
                }

                // Selling Agent

                agentNm = input.Shared["6140"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    m_nameParser.ParseName(agentNm);
                    Guid foundId = MapToEmployeeId(m_nameParser.FirstName, m_nameParser.LastName, E_RoleT.RealEstateAgent);

                    // 8/16/2004 kb - We now set this assignment through the import ui.
                    // We can reverse this by uncommenting to overwrite the user's
                    // setting.
                    //
                    // loanData.sEmployeeRealEstateAgentId = foundId;

                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.SellingAgent, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["6142"];
                    agent.FaxNum = input.Shared["6145"];
                    agent.CompanyName = input.Shared["6141"];
                    agent.StreetAddr = input.Shared["6143"];
                    m_addressParser.ParseCityStateZip(input.Shared["6144"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12379"];
                    agent.EmailAddr = input.Shared["12380"];
                    agent.Update();
                }
                else
                {
                    // 8/16/2004 kb - We now set this assignment through the import ui.
                    // We can reverse this by uncommenting to overwrite the user's
                    // setting.
                    //
                    // loanData.sEmployeeRealEstateAgentId = Guid.Empty;
                }

                // Builder
                agentNm = input.Shared["360"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Builder, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["362"];
                    agent.FaxNum = input.Shared["368"];
                    agent.CompanyName = input.Shared["361"];
                    agent.StreetAddr = input.Shared["363"];
                    m_addressParser.ParseCityStateZip(input.Shared["364"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.CaseNum = input.Shared["369"];
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12381"];
                    agent.EmailAddr = input.Shared["12382"];
                    agent.Update();
                }

                // Seller
                agentNm = input.Shared["7300"].TrimWhitespaceAndBOM() + " " + input.Shared["7301"].TrimWhitespaceAndBOM();
                agentNm = agentNm.TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Seller, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["7307"];
                    agent.FaxNum = input.Shared["7308"];
                    agent.StreetAddr = input.Shared["7302"];
                    agent.City = input.Shared["7303"];
                    agent.State = input.Shared["7304"];
                    agent.Zip = input.Shared["7305"];
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12383"];
                    agent.EmailAddr = input.Shared["12384"];
                    agent.Update();
                }

                // Hazard Insurance Agent
                agentNm = input.Shared["450"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.HazardInsurance, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["451"];
                    agent.FaxNum = input.Shared["452"];
                    agent.CompanyName = input.Shared["453"];
                    agent.StreetAddr = input.Shared["454"];
                    m_addressParser.ParseCityStateZip(input.Shared["455"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.CaseNum = input.Shared["456"];
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12385"];
                    agent.EmailAddr = input.Shared["12386"];
                    agent.Update();
                }

                // Mortgage Insurance Agent
                agentNm = input.Shared["460"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.MortgageInsurance, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["461"];
                    agent.FaxNum = input.Shared["462"];
                    agent.CompanyName = input.Shared["463"];
                    agent.StreetAddr = input.Shared["464"];
                    m_addressParser.ParseCityStateZip(input.Shared["465"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12387"];
                    agent.EmailAddr = input.Shared["12388"];
                    agent.Update();
                }

                // Surveyor
                agentNm = input.Shared["470"].TrimWhitespaceAndBOM();
                if ("" != agentNm)
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Surveyor, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["471"];
                    agent.FaxNum = input.Shared["472"];
                    agent.CompanyName = input.Shared["473"];
                    agent.StreetAddr = input.Shared["474"];
                    m_addressParser.ParseCityStateZip(input.Shared["475"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    //av 05 20 08 opm 2199 
                    agent.CellPhone = input.Shared["12389"];
                    agent.EmailAddr = input.Shared["12390"];
                    agent.Update();
                }

                // Lender submitted to from General  av 
                agentNm = input.Shared["6000"].TrimWhitespaceAndBOM();
                string ComNm = input.Shared["6001"].TrimWhitespaceAndBOM();
                //If lender role is already assigned through manual assignement, do not create the Lender contact
                if ((!IsRolesImportedAsAgents || DefaultAssignLenderAccountExecID == Guid.Empty) && ("" != agentNm) || ("" != ComNm))
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Lender, dt, loanData);
                    agent.CompanyName = ComNm;
                    agent.AgentName = agentNm;
                    agent.StreetAddr = input.Shared["6003"];
                    m_addressParser.ParseCityStateZip(input.Shared["6004"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.AgentName = input.Shared["6000"];
                    agent.Phone = input.Shared["6002"];
                    agent.FaxNum = input.Shared["6005"];
                    agent.CaseNum = input.Shared["6007"];
                    //av 05 20 8 2199
                    agent.CellPhone = input.Shared["12357"];
                    agent.EmailAddr = input.Shared["12358"];
                    agent.Update();
                }

                // 3/9/2007 nw - OPM 10444 - Broker from General tracking and Status. av 
                agentNm = input.Shared["6371"].TrimWhitespaceAndBOM();
                ComNm = input.Shared["6370"].TrimWhitespaceAndBOM();
                if (("" != agentNm) || ("" != ComNm))
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.BrokerRep, dt, loanData);
                    agent.CompanyName = ComNm;
                    agent.AgentName = agentNm;
                    agent.StreetAddr = input.Shared["6372"];
                    m_addressParser.ParseCityStateZip(input.Shared["6373"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.Phone = input.Shared["6374"];
                    agent.FaxNum = input.Shared["6375"];
                    //av 05 20 8 2199
                    agent.EmailAddr = input.Shared["12356"];
                    agent.CellPhone = input.Shared["12355"];
                    agent.Notes = input.Shared["6377"];
                    agent.Update();
                }


                // Underwritter  Tracking:Internal av 
                agentNm = input.Shared["942"].TrimWhitespaceAndBOM();
                string chumsid = input.Shared["5756"].TrimWhitespaceAndBOM();
                if (("" != agentNm) || ("" != chumsid))
                {
                    CAgentFields agent = GetNewAgentFor(E_AgentRoleT.Underwriter, dt, loanData);
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["6363"];
                    agent.FaxNum = input.Shared["6364"];
                    agent.LicenseNumOfAgent = chumsid;
                    //av 05 20 8 2199
                    agent.CellPhone = input.Shared["6386"];
                    agent.EmailAddr = input.Shared["6387"];
                    agent.Update();
                }

                // ECOA, pulling from the broker setting
                string ecoaAddressDefault = broker.ECOAAddressDefault.TrimWhitespaceAndBOM();
                if ("" != ecoaAddressDefault)
                {
                    string[] pieces = ecoaAddressDefault.Split(new char[] { '\r', '\n', ',' });
                    int num = pieces.Length;
                    if (num > 0)
                    {
                        CAgentFields agent = loanData.GetAgentOfRole(E_AgentRoleT.ECOA, E_ReturnOptionIfNotExist.CreateNew);
                        int nProcessed = 0;
                        for (int iPiece = 0; iPiece < num; ++iPiece)
                        {
                            string piece = pieces[iPiece].TrimWhitespaceAndBOM();
                            if ("" != piece)
                            {
                                switch (nProcessed)
                                {
                                    case 0: agent.CompanyName = piece; break;
                                    case 1: agent.StreetAddr = piece; break;
                                    case 2: agent.City = piece; break;
                                    case 3:
                                        {
                                            m_addressParser.ParseCityStateZip(pieces[iPiece].TrimWhitespaceAndBOM());
                                            agent.State = m_addressParser.State;
                                            agent.Zip = m_addressParser.Zipcode;
                                            break;
                                        }
                                }
                                if (nProcessed >= 3)
                                    break;

                                ++nProcessed;
                            }
                        }
                        agent.Update();
                    }
                }

                // Other 1
                string otherDesc = input.Shared["480"].TrimWhitespaceAndBOM();
                agentNm = input.Shared["481"].TrimWhitespaceAndBOM();
                if (("" != otherDesc) || ("" != agentNm))
                {
                    //av 05 20 8 2199 use to get the same object repeatedly
                    CAgentFields agent = loanData.GetAgentFields(-1);
                    agent.AgentRoleT = E_AgentRoleT.Other;
                    agent.OtherAgentRoleTDesc = otherDesc;
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["482"];
                    agent.FaxNum = input.Shared["483"];
                    agent.CompanyName = input.Shared["484"];
                    agent.StreetAddr = input.Shared["485"];
                    m_addressParser.ParseCityStateZip(input.Shared["486"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.Notes = input.Shared["487"] + "; " + input.Shared["488"];
                    //av 05 20 8 2199
                    agent.CellPhone = input.Shared["12391"];
                    agent.EmailAddr = input.Shared["12392"];
                    agent.Update();
                }


                // Other 2
                otherDesc = input.Shared["490"].TrimWhitespaceAndBOM();
                agentNm = input.Shared["491"].TrimWhitespaceAndBOM();
                if (("" != otherDesc) || ("" != agentNm))
                {
                    CAgentFields agent = loanData.GetAgentFields(-1);
                    agent.AgentRoleT = E_AgentRoleT.Other;
                    agent.OtherAgentRoleTDesc = otherDesc;
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["492"];
                    agent.FaxNum = input.Shared["493"];
                    agent.CompanyName = input.Shared["494"];
                    agent.StreetAddr = input.Shared["495"];
                    m_addressParser.ParseCityStateZip(input.Shared["496"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.Notes = input.Shared["497"] + "; " + input.Shared["498"];
                    //av 05 20 8 2199
                    agent.CellPhone = input.Shared["12393"];
                    agent.EmailAddr = input.Shared["12394"];
                    agent.Update();
                }

                // Other 3
                otherDesc = input.Shared["500"].TrimWhitespaceAndBOM();
                agentNm = input.Shared["501"].TrimWhitespaceAndBOM();
                if (("" != otherDesc) || ("" != agentNm))
                {
                    CAgentFields agent = loanData.GetAgentFields(-1);
                    agent.AgentRoleT = E_AgentRoleT.Other;
                    agent.OtherAgentRoleTDesc = otherDesc;
                    agent.AgentName = agentNm;
                    agent.Phone = input.Shared["502"];
                    agent.FaxNum = input.Shared["503"];
                    agent.CompanyName = input.Shared["504"];
                    agent.StreetAddr = input.Shared["505"];
                    m_addressParser.ParseCityStateZip(input.Shared["506"]);
                    agent.City = m_addressParser.City;
                    agent.State = m_addressParser.State;
                    agent.Zip = m_addressParser.Zipcode;
                    agent.Notes = input.Shared["507"] + "; " + input.Shared["508"];
                    //av 05 20 8 2199
                    agent.CellPhone = input.Shared["12395"];
                    agent.EmailAddr = input.Shared["12396"];
                    agent.Update();
                }

                #endregion

                // PREPARERS OF FORMS	

                IPreparerFields prep;

                // Request of appraisal
                string prepNm = input.Shared["1704"].TrimWhitespaceAndBOM();
                string prepTitle = input.Shared["1705"].TrimWhitespaceAndBOM();
                if (("" != prepNm) || ("" != prepTitle))
                {
                    prep = loanData.GetPreparerOfForm(E_PreparerFormT.RequestOfAppraisal, E_ReturnOptionIfNotExist.CreateNew);
                    prep.PreparerName = input.Shared["1704"];
                    prep.Title = input.Shared["1705"];
                    prep.Update();
                }

                // State Of Denial Statement
                prepNm = input.Shared["3849"].TrimWhitespaceAndBOM();
                if ("" != prepNm)
                {
                    // OPM 236902, 2/10/2016, ML
                    var loContact = loanData.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
                    loContact.AgentName = prepNm;
                    loContact.Update();
                }

                prepNm = input.Shared["3913"].TrimWhitespaceAndBOM();
                if ("" != prepNm)
                {
                    prep = loanData.GetPreparerOfForm(E_PreparerFormT.FloridaMortBrokerBusinessContract, E_ReturnOptionIfNotExist.CreateNew);
                    prep.LicenseNumOfCompany = prepNm;
                    prep.Update();
                }

                // GFE, AND CA MLDS
                {
                    string nmAgent = input.Shared["5402"].TrimWhitespaceAndBOM();
                    string licAgent = input.Shared["5403"].TrimWhitespaceAndBOM();
                    string nmComp = input.Shared["5400"].TrimWhitespaceAndBOM();
                    string licComp = input.Shared["5401"].TrimWhitespaceAndBOM();
                    string prepDate = input.Shared["2181"].TrimWhitespaceAndBOM();
                    if ("" != prepDate || "" != nmAgent || "" != licAgent || "" != nmComp || "" != licComp)
                    {
                        prep = loanData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
                        prep.PreparerName = nmAgent;
                        prep.LicenseNumOfAgent = licAgent;
                        prep.CompanyName = nmComp;
                        prep.LicenseNumOfCompany = licComp;
                        prep.PrepareDate_rep = prepDate;
                        prep.Update();
                    }
                }

                // 1003
                {
                    //av 52142 Where is the 1003 interviewer info coming from
                    if (!IsRolesImportedAsAgents || DefaultAssignLoanOfficerID == Guid.Empty)
                    {

                        string nmAgent = input.Shared["1508"].TrimWhitespaceAndBOM();
                        string nmComp = input.Shared["381"].TrimWhitespaceAndBOM();
                        if ("" != nmAgent || "" != nmComp)
                        {
                            prep = loanData.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);
                            prep.PreparerName = nmAgent;
                            prep.Phone = input.Shared["1509"];

                            prep.CompanyName = nmComp;
                            prep.StreetAddr = input.Shared["383"];
                            prep.City = input.Shared["384"];
                            prep.State = input.Shared["385"];
                            prep.Zip = input.Shared["386"];
                            prep.PhoneOfCompany = input.Shared["382"];
                            prep.FaxOfCompany = input.Shared["387"];
                            prep.IsLocked = true;
                            prep.Update();
                        }
                    }
                }

                // 11428
                {
                    string nmAgent = input.Shared["11428"].TrimWhitespaceAndBOM();
                    if ("" != nmAgent)
                    {
                        prep = loanData.GetPreparerOfForm(E_PreparerFormT.FloridaLenderDisclosure, E_ReturnOptionIfNotExist.CreateNew);
                        prep.PreparerName = nmAgent;
                        prep.StreetAddr = input.Shared["11429"];
                        prep.City = input.Shared["11430"];
                        prep.State = input.Shared["11431"];
                        prep.Zip = input.Shared["11432"];
                        prep.Update();
                    }
                }

                string s = input.Shared["5890"].TrimWhitespaceAndBOM();
                if ("" != s)
                {
                    prep = loanData.GetPreparerOfForm(E_PreparerFormT.RequestOfInsurance, E_ReturnOptionIfNotExist.CreateNew);
                    prep.PrepareDate_rep = s;
                    prep.Update();
                }

                s = input.Shared["336"].TrimWhitespaceAndBOM();
                if ("" != s)
                {
                    prep = loanData.GetPreparerOfForm(E_PreparerFormT.RequestOfAppraisal, E_ReturnOptionIfNotExist.CreateNew);
                    prep.PrepareDate_rep = s;
                    prep.Update();
                }

                s = input.Shared["5888"].TrimWhitespaceAndBOM();
                if ("" != s)
                {
                    prep = loanData.GetPreparerOfForm(E_PreparerFormT.RequestOfTitle, E_ReturnOptionIfNotExist.CreateNew);
                    prep.PrepareDate_rep = s;
                    prep.Update();
                }

                s = input.Shared["7942"].TrimWhitespaceAndBOM();
                if ("" != s)
                {
                    prep = loanData.GetPreparerOfForm(E_PreparerFormT.FHAConditionalCommitment, E_ReturnOptionIfNotExist.CreateNew);
                    prep.PreparerName = s;
                    prep.Update();
                }

                loanData.sApprRprtDueD_rep = input.Shared["2050"];

                //NOTE: loanData.sStatusT is taken care of in CPointData.Flush()

                switch (loanData.sLienPosT)
                {
                    case E_sLienPosT.First:
                        loanData.sSubFin_rep = input.Shared["2714"];
                        loanData.sConcurSubFin_rep = input.Shared["809"];
                        loanData.sSubFinMb_rep = input.Shared["752"];
                        loanData.s1stMtgOrigLAmt_rep = "";
                        loanData.sRemain1stMBal_rep = "";
                        loanData.sRemain1stMPmt_rep = "";
                        break;
                    case E_sLienPosT.Second:
                        if ("" != input.Shared["2718"])
                            loanData.s1stMtgOrigLAmt_rep = input.Shared["2718"];
                        else
                            loanData.s1stMtgOrigLAmt_rep = input.Shared["2800"];

                        if ("" != input.Shared["2719"])
                            loanData.sRemain1stMBal_rep = input.Shared["2719"];
                        else
                            loanData.sRemain1stMBal_rep = input.Shared["2800"];

                        loanData.sRemain1stMPmt_rep = input.Shared["751"];
                        loanData.sSubFin_rep = "";
                        loanData.sConcurSubFin_rep = "";
                        loanData.sSubFinMb_rep = "";
                        break;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Programming error:  Unhandled lien position type.");
                }

                //Reverse calculations
                if (input.Shared["753"].Length > 0)
                {
                    // TODO: Handle this case so we can import sProHazInsR
                    if (loanData.sProHazInsBaseAmt != 0)
                        loanData.sProHazInsR = convertLos.ToMoney(input.Shared["753"]) * 1200.00M / loanData.sProHazInsBaseAmt;
                    else
                        loanData.sProHazInsMb_rep = input.Shared["753"];
                }

                // 9/12/2007 - OPM 17495 db
                // 402 = rate
                // 755 = monthly payment
                // 14602 = monthly payment override checkbox
                if (loanData.sProMInsBaseAmt == 0)
                {
                    loanData.sProMInsR_rep = input.Shared["402"];
                    loanData.sProMInsMb_rep = input.Shared["755"];
                }
                else
                {
                    if (input.Shared["402"].Length > 0)
                    {
                        decimal rateFromPointFile = Math.Round(convertLos.ToMoney(input.Shared["402"]), 3);
                        if (input.Shared["755"].Length > 0)
                        {
                            //calculate monthly payment to see if it matches the rate in the point file
                            decimal calculatedMonthlyPayment = Math.Round((loanData.sProMInsBaseAmt * (rateFromPointFile * .01M)) / 12, 2);
                            if (calculatedMonthlyPayment == convertLos.ToMoney(input.Shared["755"]))
                            {
                                //if the calculated monthly payment matches the monthly payment from the input file, use the rate from the input file directly
                                loanData.sProMInsR = rateFromPointFile;
                            }
                            else
                            {
                                //the calculated monthly payment doesn't match the monthly payment from the input file,
                                //so we'll take the monthly payment directly from the input file and ignore the rate field
                                //loanData.sProMInsR = 0;
                                //loanData.sProMInsMb_rep = input.Shared["755"];

                                // 5/15/2009 dd - The calculated monthly payment doesn't match the monthly payment from the input file
                                // so we'll take the monthly payment directly from the input file and leave the rate field alone.
                                loanData.sProMIns_rep = input.Shared["755"];
                                loanData.sProMInsLckd = true;
                            }
                        }
                        else
                        {
                            //no monthly payment was given in the input file, so we'll use the rate directly from the input file and it will get calculated,
                            //if the 'override' checkbox was checked in Point with a blank textbox value, then we need the monthly payment value to stay blank
                            if (!input.Shared["14602"].Equals("X"))
                                loanData.sProMInsR = rateFromPointFile;
                        }
                    }
                    else //no rate was given in the input file
                    {
                        loanData.sProMInsR = 0;
                        if (input.Shared["755"].Length > 0)
                        {
                            //no rate was given in the input file but a monthly payment was, so we'll use the monthly payment directly
                            //loanData.sProMInsMb_rep = input.Shared["755"];
                            // 5/15/2009 dd - No rate was given in the input file but a monthly payment was. So we set the mothly payment directly to our sProMIns.
                            loanData.sProMIns_rep = input.Shared["755"];
                            loanData.sProMInsLckd = true;
                        }
                    }
                }


                // 3/7/2007 nw - OPM 11207
                loanData.sONewFinCc_rep = input.Shared["2801"];

                if (loanData.sIsIncomeCollectionEnabled)
                {
                    // Only clear loan-level income for primary app.
                    loanData.ClearIncomeSources();
                }
            }
            #endregion // SHARED FIELDS
            #region PER-APP DATA

            CAppData appData = loanData.GetAppData(iApp);
            appData.aReCollection.ClearAll();
            appData.aBEmpCollection.ClearAll();
            appData.aCEmpCollection.ClearAll();
            appData.aVorCollection.Clear();
            appData.aLiaCollection.ClearAll();
            appData.aAssetCollection.ClearAll();

            if (!string.IsNullOrEmpty(input.App["3550"]))
            {
                appData.aCreditReportIdLckd = true;
                appData.aCreditReportId = input.App["3550"];
            }

            appData.aHas1stTimeBuyerTri = FromXYesToTriState(input.App["7505"], input.App["7506"]);
            appData.aVaBorrCertHadVaLoanTri = FromXYesToTriState(input.App["7638"], input.App["7639"]);
            appData.aFHABorrCertOccIsAsHome = "X" == input.App["7640"];
            appData.aFHABorrCertOccIsAsHomeForActiveSpouse = "X" == input.App["7641"];
            appData.aFHABorrCertOccIsAsHomePrev = "X" == input.App["7642"];
            appData.aFHABorrCertOccIsAsHomePrevForActiveSpouse = "X" == input.App["7643"];

            appData.aFHABorrCertOwnOrSoldOtherFHAPropTri = FromXYesToTriState(input.App["7630"], input.App["7626"]);
            appData.aFHABorrCertOtherPropToBeSoldTri = FromXYesToTriState(input.App["7631"], input.App["7627"]);
            appData.aFHABorrCertOtherPropSalesPrice_rep = input.App["7632"];
            appData.aFHABorrCertOtherPropOrigMAmt_rep = input.App["7633"];
            appData.aFHABorrCertOtherPropStAddr = input.App["7634"];

            m_addressParser.ParseCityStateZip(input.App["7635"]);
            appData.aFHABorrCertOtherPropCity = m_addressParser.City;
            appData.aFHABorrCertOtherPropState = m_addressParser.State;
            appData.aFHABorrCertOtherPropZip = m_addressParser.Zipcode;
            appData.aFHABorrCertOtherPropCoveredByThisLoanTri = FromXYesToTriState(input.App["7636"], input.App["7628"]);
            appData.aFHABorrCertOwnMoreThan4DwellingsTri = FromXYesToTriState(input.App["7637"], input.App["7629"]);
            appData.aFHABorrCertInformedPropVal_rep = input.App["7644"];
            appData.aFHABorrCertInformedPropValDeterminedByVA = "X" == input.App["7645"];
            appData.aFHABorrCertInformedPropValDeterminedByHUD = "X" == input.App["7646"];
            appData.aFHABorrCertInformedPropValAwareAtContractSigning = "X" == input.App["7647"];
            appData.aFHABorrCertInformedPropValNotAwareAtContractSigning = "X" == input.App["7648"];
            appData.aFHABorrCertReceivedLeadPaintPoisonInfoTri = FromXYesToTriState(input.App["7649"], input.App["7595"]);


            appData.aVaClaimFolderNum = input.App["7796"];
            appData.aVaServiceNum = input.App["7797"];
            appData.aVaIndebtCertifyTri = PointDataConversion.FromXYesToTriState(input.App["7798"], input.App["7799"]);

            appData.aVaEntitleCode = input.App["8322"];// varchar(50) not null default( '' ), --8322 -use a combo for 01,02,....11
            appData.aVaEntitleAmt_rep = input.App["8323"]; // money not null default( 0 ), --8323

            E_aVaServiceBranchT vaServiceBranchT;
            if ("X" == input.App["8305"])
                vaServiceBranchT = E_aVaServiceBranchT.AirForce;
            else if ("X" == input.App["8303"])
                vaServiceBranchT = E_aVaServiceBranchT.Army;
            else if ("X" == input.App["8307"])
                vaServiceBranchT = E_aVaServiceBranchT.CoastGuard;
            else if ("X" == input.App["8306"])
                vaServiceBranchT = E_aVaServiceBranchT.Marine;
            else if ("X" == input.App["8304"])
                vaServiceBranchT = E_aVaServiceBranchT.Navy;
            else if ("X" == input.App["8308"])
                vaServiceBranchT = E_aVaServiceBranchT.Other;
            else
                vaServiceBranchT = E_aVaServiceBranchT.LeaveBlank;
            appData.aVaServiceBranchT = vaServiceBranchT;

            E_aVaMilitaryStatT vaMilStatT;
            if ("X" == input.App["8310"])
                vaMilStatT = E_aVaMilitaryStatT.InService;
            else if ("X" == input.App["8311"])
                vaMilStatT = E_aVaMilitaryStatT.SeparatedFromService;
            else
                vaMilStatT = E_aVaMilitaryStatT.LeaveBlank;
            appData.aVaMilitaryStatT = vaMilStatT;

            appData.aVaIsVeteranFirstTimeBuyerTri = FromXYesToTriState(input.App["7590"]);
            //appData.sVaTotMonGrossI_rep = input.App["8379"];
            //appData.sVaIsCoborIConsidered = FromXYesToTriState( input.App["7750"] );


            appData.aVaUtilityIncludedTri = PointDataConversion.FromXYesToTriState(input.App["7701"], input.App["7596"]);

            val = input.App["7750"];
            if ("" != val)
            {
                appData.aVaCEmplmtI_rep = val;
                appData.aVaCEmplmtILckd = true;
            }
            else
                appData.aVaCEmplmtILckd = false;

            appData.aVaCFedITax_rep = input.App["7751"];
            appData.aVaCStateITax_rep = input.App["7752"];
            appData.aVaCSsnTax_rep = input.App["7753"];
            appData.aVaCOITax_rep = input.App["7754"];
            val = input.App["7757"];
            if ("" != val)
            {
                appData.aVaCONetI_rep = val;
                appData.aVaCONetILckd = true;
            }
            else
                appData.aVaCONetILckd = false;

            //aVaONetIDesc varchar(100) not null default( '' ), -- Point doesn't have this.
            val = input.App["7760"];
            if ("" != val)
            {
                appData.aVaBEmplmtI_rep = val;
                appData.aVaBEmplmtILckd = true;
            }
            else
                appData.aVaBEmplmtILckd = false;
            appData.aVaBFedITax_rep = input.App["7761"];
            appData.aVaBStateITax_rep = input.App["7762"];
            appData.aVaBSsnTax_rep = input.App["7763"];
            appData.aVaBOITax_rep = input.App["7764"];

            val = input.App["7767"];
            if ("" != val)
            {
                appData.aVaBONetI_rep = val;
                appData.aVaBONetILckd = true;
            }
            else
                appData.aVaBONetILckd = false;

            appData.aVaFamilySuportGuidelineAmt_rep = input.App["7778"];
            appData.aVaCrRecordSatisfyTri = FromXYesToTriState(input.App["7781"], input.App["7782"]);
            appData.aVaLMeetCrStandardTri = FromXYesToTriState(input.App["7783"], input.App["7793"]);

            appData.aVaLAnalysisN = PointDataConversion.CombineFieldsToLines(input.App, 7784, 7792);

            appData.aActiveMilitaryStatTri = PointDataConversion.FromXYesToTriState(input.App["8400"]);
            appData.aWereActiveMilitaryDutyDayAfterTri = PointDataConversion.FromXYesToTriState(input.App["8401"]);

            if ("X" == input.App["7591"])
                appData.aVaVestTitleT = E_aVaVestTitleT.Veteran;
            else if ("X" == input.App["7592"])
                appData.aVaVestTitleT = E_aVaVestTitleT.VeteranAndSpouse;
            else if ("X" == input.App["7593"])
                appData.aVaVestTitleT = E_aVaVestTitleT.Other;
            appData.aVaVestTitleODesc = input.App["7594"];
            appData.aVaServ1StartD_rep = input.App["8170"];
            appData.aVaServ2StartD_rep = input.App["8176"];
            appData.aVaServ3StartD_rep = input.App["8182"];
            appData.aVaServ4StartD_rep = input.App["8188"];
            appData.aVaServ1EndD_rep = input.App["8171"];
            appData.aVaServ2EndD_rep = input.App["8177"];
            appData.aVaServ3EndD_rep = input.App["8183"];
            appData.aVaServ4EndD_rep = input.App["8189"];
            appData.aVaServ1FullNm = input.App["8172"];
            appData.aVaServ2FullNm = input.App["8178"];
            appData.aVaServ3FullNm = input.App["8184"];
            appData.aVaServ4FullNm = input.App["8190"];
            appData.aVaServ1Ssn = input.App["8174"];
            appData.aVaServ2Ssn = input.App["8180"];
            appData.aVaServ3Ssn = input.App["8186"];
            appData.aVaServ4Ssn = input.App["8192"];
            appData.aVaServ1Num = input.App["8173"];
            appData.aVaServ2Num = input.App["8179"];
            appData.aVaServ3Num = input.App["8185"];
            appData.aVaServ4Num = input.App["8191"];
            appData.aVaServ1BranchNum = input.App["8175"];
            appData.aVaServ2BranchNum = input.App["8181"];
            appData.aVaServ3BranchNum = input.App["8187"];
            appData.aVaServ4BranchNum = input.App["8193"];
            appData.aVaDischargedDisabilityIs = "X" == input.App["8194"];
            appData.aVaClaimNum = input.App["8195"];

            IVaPastLoanCollection vaPastLColl = appData.aVaPastLCollection;

            IVaPastLoan vaPastL = vaPastLColl.EnsureRegularRecordAt(0);
            vaPastL.LoanTypeDesc = input.App["8203"];
            vaPastL.StAddr = input.App["8205"]; // POINT id jumps by 1 here!
            m_addressParser.ParseCityStateZip(input.App["8206"]);
            vaPastL.City = m_addressParser.City;
            vaPastL.State = m_addressParser.State;
            vaPastL.Zip = m_addressParser.Zipcode;
            vaPastL.LoanD_rep = input.App["8207"];
            vaPastL.IsStillOwned = ("X" == input.App["8208"]) ? E_TriState.Yes : E_TriState.No;
            vaPastL.PropSoldD_rep = input.App["8209"];
            vaPastL.VaLoanNum = input.App["8214"];

            for (int iPastL = 1; iPastL < 6; ++iPastL)
            {
                vaPastL = vaPastLColl.EnsureRegularRecordAt(iPastL);
                int seed;
                switch (iPastL)
                {

                    case 1: seed = 8405; break;
                    case 2: seed = 8412; break;
                    case 3: seed = 8419; break;
                    case 4: seed = 8426; break;
                    case 5: seed = 8433; break;
                    default:
                        Tools.LogBug("aVaPastLCollection should never contain more than 6 records at any given time");
                        seed = 0;
                        break;
                }
                if (0 == seed)
                    break;

                vaPastL.LoanTypeDesc = input.App[seed];
                vaPastL.StAddr = input.App[seed + 1];
                m_addressParser.ParseCityStateZip(input.App[seed + 2]);
                vaPastL.City = m_addressParser.City;
                vaPastL.State = m_addressParser.State;
                vaPastL.Zip = m_addressParser.Zipcode;
                vaPastL.LoanD_rep = input.App[seed + 3];
                vaPastL.IsStillOwned = ("X" == input.App[seed + 4]) ? E_TriState.Yes : E_TriState.No;
                vaPastL.PropSoldD_rep = input.App[seed + 5];
                vaPastL.VaLoanNum = input.App[seed + 6];
            }

            appData.aFHADebtInstallBal_rep = input.App["7532"];
            appData.aFHAOtherDebtBal_rep = input.App["7533"];
            appData.aFHAGiftFundSrc = input.App["8069"];
            appData.aFHAGiftFundAmt_rep = input.App["8070"];
            appData.aFHAAssetAvail_rep = input.App["7527"];
            appData.aFHADebtInstallPmt_rep = input.App["7530"];
            appData.aFHAChildSupportPmt_rep = input.App["7535"];
            appData.aFHAOtherDebtPmt_rep = input.App["7531"];

            appData.aFHABBaseI_rep = input.App["7660"]; // 7660 is combined version of 600
            appData.aFHABOI_rep = input.App["7661"];
            appData.aFHACBaseI_rep = input.App["7662"];
            appData.aFHACOI_rep = input.App["7663"];
            appData.aFHANetRentalI_rep = input.App["7664"];
            appData.aFHABCaivrsNum = input.App["7550"];
            appData.aFHACCaivrsNum = input.App["7551"];
            appData.aFHABLpdGsa = input.App["7565"];
            appData.aFHACLpdGsa = input.App["7566"];
            appData.aFHACreditRating = input.App["7545"];
            appData.aFHARatingIAdequacy = input.App["7546"];
            appData.aFHARatingIStability = input.App["7547"];
            appData.aFHARatingAssetAdequacy = input.App["7548"];

            appData.aTitleNm1 = input.App["1222"];
            appData.aTitleNm2 = input.App["1223"];
            appData.aManner = input.App["1224"];

            appData.aBDob_rep = input.App["118"];
            appData.aCDob_rep = input.App["168"];
            appData.aBMidNm = input.App["117"];
            appData.aCMidNm = input.App["167"];
            appData.aBSuffix = input.App["119"];
            appData.aCSuffix = input.App["169"];
            appData.aBAddrMailUsePresentAddr = "X" == input.App["204"];
            appData.aCAddrMailUsePresentAddr = "X" == input.App["224"];
            appData.aBAddrMail = input.App["200"];
            appData.aCAddrMail = input.App["220"];
            appData.aBCityMail = input.App["201"];
            appData.aCCityMail = input.App["221"];
            appData.aBStateMail = input.App["202"];
            appData.aCStateMail = input.App["222"];
            appData.aBZipMail = input.App["203"];
            appData.aCZipMail = input.App["223"];
            appData.aBCellPhone = input.App["139"];
            appData.aCCellPhone = input.App["189"];

            appData.aBIsAmericanIndian = "X" == input.App["2823"];
            appData.aBIsPacificIslander = "X" == input.App["2824"];
            appData.aBIsAsian = "X" == input.App["2825"];
            appData.aBIsWhite = "X" == input.App["2826"];
            appData.aBIsBlack = "X" == input.App["2827"];
            appData.aCIsAmericanIndian = "X" == input.App["2831"];
            appData.aCIsPacificIslander = "X" == input.App["2832"];
            appData.aCIsAsian = "X" == input.App["2833"];
            appData.aCIsWhite = "X" == input.App["2834"];
            appData.aCIsBlack = "X" == input.App["2835"];


            E_aHispanicT hispanicT;
            if ("X" == input.App["2821"])
                hispanicT = E_aHispanicT.Hispanic;
            else if ("X" == input.App["2822"])
                hispanicT = E_aHispanicT.NotHispanic;
            else
                hispanicT = E_aHispanicT.LeaveBlank;
            appData.aBHispanicT = hispanicT;

            if ("X" == input.App["2829"])
                hispanicT = E_aHispanicT.Hispanic;
            else if ("X" == input.App["2830"])
                hispanicT = E_aHispanicT.NotHispanic;
            else
                hispanicT = E_aHispanicT.LeaveBlank;
            appData.aCHispanicT = hispanicT;

            appData.aOIFrom1008Desc = input.App["860"];
            appData.aBOIFrom1008_rep = input.App["861"];
            appData.aCOIFrom1008_rep = input.App["862"];
            if ("X" == input.App["836"])
            {
                appData.aSpNegCfLckd = true;
                appData.aOpNegCfLckd = true;
                appData.aBSpPosCf_rep = input.App["870"];
                appData.aCSpPosCf_rep = input.App["871"];
                appData.aSpNegCf_rep = input.App["875"];
                appData.aOpNegCf_rep = input.App["882"];
            }
            else
            {
                appData.aSpNegCfLckd = false;
                appData.aOpNegCfLckd = false;
            }

            if ("X" == input.App["921"])
                appData.aOccT = E_aOccT.PrimaryResidence;
            else if ("X" == input.App["923"])
                appData.aOccT = E_aOccT.SecondaryResidence;
            else if ("X" == input.App["924"])
                appData.aOccT = E_aOccT.Investment;

            // primary application info
            appData.aBAddr = input.App["102"];
            appData.aCAddr = input.App["152"];
            appData.aBCity = input.App["103"];
            appData.aCCity = input.App["153"];
            appData.aBState = input.App["104"];
            appData.aCState = input.App["154"];
            appData.aBZip = input.App["105"];
            appData.aCZip = input.App["155"];
            appData.aBFirstNm = input.App["100"];
            appData.aCFirstNm = input.App["150"];
            appData.aBLastNm = input.App["101"] != "" ? input.App["101"] : input.App["116"];
            appData.aCLastNm = input.App["151"];
            appData.aBHPhone = input.App["106"];
            appData.aCHPhone = input.App["156"];
            appData.aBBusPhone = input.App["136"];
            appData.aCBusPhone = input.App["186"];
            appData.aBFax = input.App["107"];
            appData.aCFax = input.App["157"];
            appData.aBSsn = input.App["108"];
            appData.aCSsn = input.App["158"];
            appData.aBEmail = input.App["112"];
            appData.aCEmail = input.App["162"];

            var borrowerId = appData.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>();
            var coborrowerId = appData.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();

            if (!loanData.sIsIncomeCollectionEnabled)
            {
                appData.aBBaseI_rep = input.App["600"];
                appData.aCBaseI_rep = input.App["650"];
                appData.aBOvertimeI_rep = input.App["601"];
                appData.aCOvertimeI_rep = input.App["651"];
                appData.aBBonusesI_rep = input.App["602"];
                appData.aCBonusesI_rep = input.App["652"];
                appData.aBCommisionI_rep = input.App["603"];
                appData.aCCommisionI_rep = input.App["653"];
                appData.aBDividendI_rep = input.App["604"];
                appData.aCDividendI_rep = input.App["654"];
            }
            else
            {
                loanData.AddIncomeSource(
                    borrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["600"]))
                    });
                loanData.AddIncomeSource(
                    coborrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.BaseIncome,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["650"]))
                    });
                loanData.AddIncomeSource(
                    borrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Overtime,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["601"]))
                    });
                loanData.AddIncomeSource(
                    coborrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Overtime,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["651"]))
                    });
                loanData.AddIncomeSource(
                    borrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Bonuses,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["602"]))
                    });
                loanData.AddIncomeSource(
                    coborrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Bonuses,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["652"]))
                    });
                loanData.AddIncomeSource(
                    borrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Commission,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["603"]))
                    });
                loanData.AddIncomeSource(
                    coborrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.Commission,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["653"]))
                    });
                loanData.AddIncomeSource(
                    borrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.DividendsOrInterest,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["604"]))
                    });
                loanData.AddIncomeSource(
                    coborrowerId,
                    new IncomeSource
                    {
                        IncomeType = IncomeType.DividendsOrInterest,
                        MonthlyAmountData = Money.Create(convertLos.ToMoney(input.App["654"]))
                    });
            }

            appData.aNetRentI1003Lckd = true;
            appData.aBNetRentI1003_rep = input.App["605"];
            appData.aCNetRentI1003_rep = input.App["655"];

            ImportOtherIncome(loanData, input, convertLos, appData);

            appData.aPresRent_rep = input.App["700"];
            appData.aPres1stM_rep = input.App["701"];
            appData.aPresOFin_rep = input.App["702"];
            appData.aPresHazIns_rep = input.App["703"];
            appData.aPresRealETx_rep = input.App["704"];
            appData.aPresMIns_rep = input.App["705"];
            appData.aPresHoAssocDues_rep = input.App["706"];
            appData.aPresOHExp_rep = input.App["707"];
            appData.aSpouseIExcl = "X" == input.App["1181"];

            appData.aBAge_rep = input.App["109"];
            appData.aCAge_rep = input.App["159"];
            appData.aBSchoolYrs_rep = input.App["110"];
            appData.aCSchoolYrs_rep = input.App["160"];
            // 0 - married, 1 - not married, 2 - separated
            if ("X" == input.App["144"])
                appData.aBMaritalStatT = DataAccess.E_aBMaritalStatT.Married;
            else if ("X" == input.App["145"])
                appData.aBMaritalStatT = DataAccess.E_aBMaritalStatT.Separated;
            else if ("X" == input.App["146"])
                appData.aBMaritalStatT = DataAccess.E_aBMaritalStatT.NotMarried;
            else
                appData.aBMaritalStatT = DataAccess.E_aBMaritalStatT.LeaveBlank;

            if ("X" == input.App["194"])
                appData.aCMaritalStatT = DataAccess.E_aCMaritalStatT.Married;
            else if ("X" == input.App["195"])
                appData.aCMaritalStatT = DataAccess.E_aCMaritalStatT.Separated;
            else if ("X" == input.App["196"])
                appData.aCMaritalStatT = DataAccess.E_aCMaritalStatT.NotMarried;
            else
                appData.aCMaritalStatT = DataAccess.E_aCMaritalStatT.LeaveBlank;


            // 0 - own, 1 - rent
            if ("X" == input.App["113"])
                appData.aBAddrT = DataAccess.E_aBAddrT.Own;
            else if ("X" == input.App["114"])
                appData.aBAddrT = DataAccess.E_aBAddrT.Rent;
            else
                appData.aBAddrT = DataAccess.E_aBAddrT.LeaveBlank;

            if ("X" == input.App["163"])
                appData.aCAddrT = DataAccess.E_aCAddrT.Own;
            else if ("X" == input.App["164"])
                appData.aCAddrT = DataAccess.E_aCAddrT.Rent;
            else
                appData.aCAddrT = DataAccess.E_aCAddrT.LeaveBlank;

            if ("X" == input.App["5500"])
                appData.aBPrev1AddrT = DataAccess.E_aBPrev1AddrT.Own;
            else if ("X" == input.App["5501"])
                appData.aBPrev1AddrT = DataAccess.E_aBPrev1AddrT.Rent;
            else
                appData.aBPrev1AddrT = DataAccess.E_aBPrev1AddrT.LeaveBlank;

            if ("X" == input.App["5520"])
                appData.aCPrev1AddrT = DataAccess.E_aCPrev1AddrT.Own;
            else if ("X" == input.App["5521"])
                appData.aCPrev1AddrT = DataAccess.E_aCPrev1AddrT.Rent;
            else
                appData.aCPrev1AddrT = DataAccess.E_aCPrev1AddrT.LeaveBlank;

            appData.aBAddrYrs = input.App["111"];
            appData.aCAddrYrs = input.App["161"];
            appData.aBPrev1AddrYrs = input.App["5502"];
            appData.aCPrev1AddrYrs = input.App["5522"];

            appData.aBPrev1Addr = input.App["5503"];
            m_addressParser.ParseCityStateZip(input.App["5504"]);
            appData.aBPrev1City = m_addressParser.City;
            appData.aBPrev1State = m_addressParser.State;
            appData.aBPrev1Zip = m_addressParser.Zipcode;

            appData.aCPrev1Addr = input.App["5523"];
            m_addressParser.ParseCityStateZip(input.App["5524"]);
            appData.aCPrev1City = m_addressParser.City;
            appData.aCPrev1State = m_addressParser.State;
            appData.aCPrev1Zip = m_addressParser.Zipcode;

            if ("X" == input.App["5510"])
                appData.aBPrev2AddrT = DataAccess.E_aBPrev2AddrT.Own;
            else if ("X" == input.App["5511"])
                appData.aBPrev2AddrT = DataAccess.E_aBPrev2AddrT.Rent;
            else
                appData.aBPrev2AddrT = DataAccess.E_aBPrev2AddrT.LeaveBlank;

            if ("X" == input.App["5530"])
                appData.aCPrev2AddrT = DataAccess.E_aCPrev2AddrT.Own;
            else if ("X" == input.App["5531"])
                appData.aCPrev2AddrT = DataAccess.E_aCPrev2AddrT.Rent;
            else
                appData.aCPrev2AddrT = DataAccess.E_aCPrev2AddrT.LeaveBlank;

            appData.aBPrev2AddrYrs = input.App["5512"];
            appData.aCPrev2AddrYrs = input.App["5532"];

            appData.aBPrev2Addr = input.App["5513"];
            m_addressParser.ParseCityStateZip(input.App["5514"]);
            appData.aBPrev2City = m_addressParser.City;
            appData.aBPrev2State = m_addressParser.State;
            appData.aBPrev2Zip = m_addressParser.Zipcode;

            appData.aCPrev2Addr = input.App["5533"];
            m_addressParser.ParseCityStateZip(input.App["5534"]);
            appData.aCPrev2City = m_addressParser.City;
            appData.aCPrev2State = m_addressParser.State;
            appData.aCPrev2Zip = m_addressParser.Zipcode;

            appData.aBDependNum_rep = input.App["129"];
            appData.aCDependNum_rep = input.App["179"];
            appData.aBDependAges = input.App["130"];
            appData.aCDependAges = input.App["180"];

            // borrower employment info
            /*
            DataSet ds = appData.aBEmplmtDataSet ;
            DataTable table = ds.Tables["EmplmtXmlContent"] ;
            DataRow row = table.NewRow() ;
            table.Rows.Add(row) ;
            */

            var borPrimaryEmp = new PrimaryEmploymentRecord();
            YearMonthParser pars = new YearMonthParser();
            if (pars.ParseStr(input.App["205"], input.App["206"])) // latest version of Point use these fields instead of 137
                borPrimaryEmp.EmplmtLen_rep = pars.YearsInDecimal.ToString();
            else
                borPrimaryEmp.EmplmtLen_rep = input.App["137"];

            borPrimaryEmp.IsSelfEmplmt = "X" == input.App["141"];
            borPrimaryEmp.EmplrBusPhone = input.App["136"];
            borPrimaryEmp.ProfLen_rep = input.App["138"];
            borPrimaryEmp.JobTitle = input.App["135"];

            borPrimaryEmp.EmplrNm = input.App["148"];
            borPrimaryEmp.EmplrAddr = input.App["149"];
            borPrimaryEmp.EmplrCity = input.App["140"];
            borPrimaryEmp.EmplrState = input.App["142"];
            borPrimaryEmp.EmplrZip = input.App["143"];

            IPrimaryEmploymentRecord primaryEmp = appData.aBEmpCollection.GetPrimaryEmp(borPrimaryEmp.HasNonBlankValue);
            if (primaryEmp != null)
            {
                borPrimaryEmp.ApplyTo(primaryEmp);
            }

            // co-borrower employment info
            var cobPrimaryEmp = new PrimaryEmploymentRecord();
            if (pars.ParseStr(input.App["225"], input.App["226"])) // latest version of Point use these fields instead of 187
                cobPrimaryEmp.EmplmtLen_rep = pars.YearsInDecimal.ToString();
            else
                cobPrimaryEmp.EmplmtLen_rep = input.App["187"];

            cobPrimaryEmp.IsSelfEmplmt = "X" == input.App["191"];
            cobPrimaryEmp.EmplrBusPhone = input.App["186"];

            cobPrimaryEmp.ProfLen_rep = input.App["188"];
            cobPrimaryEmp.JobTitle = input.App["185"];

            cobPrimaryEmp.EmplrNm = input.App["198"];
            cobPrimaryEmp.EmplrAddr = input.App["199"];
            cobPrimaryEmp.EmplrCity = input.App["190"];
            cobPrimaryEmp.EmplrState = input.App["192"];
            cobPrimaryEmp.EmplrZip = input.App["193"];

            primaryEmp = appData.aCEmpCollection.GetPrimaryEmp(cobPrimaryEmp.HasNonBlankValue);
            if (primaryEmp != null)
            {
                cobPrimaryEmp.ApplyTo(primaryEmp);
            }

            // PAST EMPLOYMENT RECORDS

            // 1/19/2006 dd - Previous employment id are store in following order.
            //  self-employed, company name, company address, city, state, zip, start date, end date, monthly income, title, phone.
            string[,] borrowerFieldIds = {
                                            { "5540", "5220", "5221", "5222", "5223", "5224", "5546", "5547", "5548", "5544", "5545", "4116" },
                                            { "5550", "5225", "5226", "5227", "5228", "5229", "5556", "5557", "5558", "5554", "5555", "4117" },
                                            { "5580", "5230", "5231", "5232", "5233", "5234", "5586", "5587", "5588", "5584", "5585", "4118" },
                                            { "5600", "5235", "5236", "5237", "5238", "5239", "5606", "5607", "5608", "5604", "5605", "4119" },
                                            { "5610", "5240", "5241", "5242", "5243", "5244", "5616", "5617", "5618", "5614", "5615", "4120" },
                                            { "5620", "5245", "5246", "5247", "5248", "5249", "5626", "5627", "5628", "5624", "5625", "4121" },
                                            { "5630", "5250", "5251", "5252", "5253", "5254", "5636", "5637", "5638", "5634", "5635", "4122" },
                                            { "5640", "5255", "5256", "5257", "5258", "5259", "5646", "5647", "5648", "5644", "5645", "4123" }

                                         };
            string[,] coborrowerFieldIds = {
                                          { "5560", "5260", "5261", "5262", "5263", "5264", "5566", "5567", "5568", "5564", "5565", "4124" },
                                          { "5570", "5265", "5266", "5267", "5268", "5269", "5576", "5577", "5578", "5574", "5575", "4125" },
                                          { "5590", "5270", "5271", "5272", "5273", "5274", "5596", "5597", "5598", "5594", "5595", "4126" },
                                          { "5650", "5275", "5276", "5277", "5278", "5279", "5656", "5657", "5658", "5654", "5655", "4127" },
                                          { "5660", "5280", "5281", "5282", "5283", "5284", "5666", "5667", "5668", "5664", "5665", "4128" },
                                          { "5670", "5285", "5286", "5287", "5288", "5289", "5676", "5677", "5678", "5674", "5675", "4129" },
                                          { "5680", "5010", "5011", "5012", "5013", "5014", "5686", "5687", "5688", "5684", "5685", "4130" },
                                          { "5690", "5015", "5016", "5017", "5018", "5019", "5696", "5697", "5698", "5694", "5695", "4131" }
                                           };


            IRegularEmploymentRecord empFields = null;

            for (int index = 0; index < 2; index++)
            {
                string[,] ids = null;
                IEmpCollection empCollection = null;
                if (index == 0)
                {
                    ids = borrowerFieldIds;
                    empCollection = appData.aBEmpCollection;
                }
                else
                {
                    ids = coborrowerFieldIds;
                    empCollection = appData.aCEmpCollection;
                }
                int rowCount = ids.Length / 11;

                for (int j = 0; j < rowCount; j++)
                {
                    if ("" != input.App[ids[j, 1]])
                    {
                        empFields = empCollection.AddRegularRecord();
                        empFields.IsSelfEmplmt = "X" == input.App[ids[j, 0]];
                        empFields.EmplrNm = input.App[ids[j, 1]];
                        empFields.EmplrAddr = input.App[ids[j, 2]];
                        empFields.EmplrCity = input.App[ids[j, 3]];
                        empFields.EmplrState = input.App[ids[j, 4]];
                        empFields.EmplrZip = input.App[ids[j, 5]];
                        empFields.EmplmtStartD_rep = input.App[ids[j, 6]];
                        empFields.EmplmtEndD_rep = input.App[ids[j, 7]];
                        empFields.MonI_rep = input.App[ids[j, 8]];
                        empFields.JobTitle = input.App[ids[j, 9]];
                        empFields.EmplrBusPhone = input.App[ids[j, 10]];
                        empFields.IsCurrent = input.App[ids[j, 11]] == "X";
                    }
                }
            }
            #region OBSOLETE - No longer valid with Point 5.0+
            //            // count field is in 5549 for borrower and 5569 for coborrower
            //            int iEmplmntRecord;
            //            CEmpRegRec empFields;
            //
            //			for (i = 5540, iEmplmntRecord = 0; i < 5700 ; i += 10, ++iEmplmntRecord )
            //			{
            //				if ("" != input.App[(i+1).ToString()])
            //				{
            //					switch( i )
            //					{
            //							// Borrower
            //						case 5540:
            //						case 5550:
            //						case 5580:
            //						case 5600:
            //						case 5610:
            //						case 5620:
            //						case 5630:
            //						case 5640:
            //							empFields = appData.aBEmpCollection.AddRegularRecord();
            //							break;
            //							// Co-Borrower
            //						case 5560:
            //						case 5570:
            //						case 5590:
            //						case 5650:
            //						case 5660:
            //						case 5670:
            //						case 5680:
            //						case 5690:
            //							empFields = appData.aCEmpCollection.AddRegularRecord();
            //							break;
            //						default:
            //							continue;
            //					}
            //					empFields.IsSelfEmplmt = "X" == input.App[(i).ToString()] ; //row["IsSelfEmplmt"] 
            //					empFields.EmplrNm = input.App[(i+1).ToString()] ; //row["EmplrNm"]'
            //					empFields.EmplrAddr = input.App[(i+2).ToString()];
            //
            //					m_addressParser.ParseCityStateZip(input.App[(i+3).ToString()]) ;
            //					empFields.EmplrCity = m_addressParser.City; //row["EmplrCity"] = 
            //					empFields.EmplrState = m_addressParser.State; //row["EmplrState"]
            //					empFields.EmplrZip = m_addressParser.Zipcode; //row["EmplrZip"] = 
            //					
            //					empFields.JobTitle = input.App[(i+4).ToString()] ; //row["JobTitle"] =
            //					empFields.EmplrBusPhone = input.App[(i+5).ToString()] ; //row["EmplrBusPhone"] = 
            //
            //					empFields.EmplmtStartD_rep = input.App[(i+6).ToString()];
            //					empFields.EmplmtEndD_rep = input.App[(i+7).ToString()];
            //
            //					empFields.MonI_rep = input.App[(i+8).ToString()];
            //					empFields.EmplmtStat = E_EmplmtStat.Previous;
            //				}
            //			}
            #endregion


            appData.aAltNm1 = input.App["1360"];
            appData.aAltNm1AccNum = input.App["1362"];
            appData.aAltNm1CreditorNm = input.App["1361"];

            appData.aAltNm2 = input.App["1363"];
            appData.aAltNm2AccNum = input.App["1365"];
            appData.aAltNm2CreditorNm = input.App["1364"];

            // borrower's declarations
            appData.aBDecJudgment = input.App["1465"];
            appData.aCDecJudgment = input.App["1477"];
            appData.aBDecBankrupt = input.App["1466"];
            appData.aCDecBankrupt = input.App["1478"];
            appData.aBDecForeclosure = input.App["1467"];
            appData.aCDecForeclosure = input.App["1479"];
            appData.aBDecLawsuit = input.App["1468"];
            appData.aCDecLawsuit = input.App["1480"];
            appData.aBDecObligated = input.App["1469"];
            appData.aCDecObligated = input.App["1481"];
            appData.aBDecDelinquent = input.App["1470"];
            appData.aCDecDelinquent = input.App["1482"];
            appData.aBDecAlimony = input.App["1471"];
            appData.aCDecAlimony = input.App["1483"];
            appData.aBDecBorrowing = input.App["1472"];
            appData.aCDecBorrowing = input.App["1484"];
            appData.aBDecEndorser = input.App["1473"];
            appData.aCDecEndorser = input.App["1485"];
            appData.aBDecCitizen = input.App["1474"];
            appData.aCDecCitizen = input.App["1486"];
            appData.aBDecResidency = input.App["1475"];
            appData.aCDecResidency = input.App["1487"];
            appData.aBDecOcc = input.App["1476"];
            appData.aCDecOcc = input.App["1488"];
            appData.aBDecPastOwnership = input.App["1620"];
            appData.aCDecPastOwnership = input.App["1623"]; ;

            appData.aBNoFurnishLckd = true;
            appData.aBNoFurnish = "X" == input.App["1489"];
            appData.aCNoFurnishLckd = true;
            appData.aCNoFurnish = "X" == input.App["1497"];
            if ("X" == input.App["1490"])
                appData.aBRaceT = E_aBRaceT.AmericanIndian;
            else if ("X" == input.App["1491"])
                appData.aBRaceT = E_aBRaceT.Asian;
            else if ("X" == input.App["1492"])
                appData.aBRaceT = E_aBRaceT.Black;
            else if ("X" == input.App["1493"])
                appData.aBRaceT = E_aBRaceT.Hispanic;
            else if ("X" == input.App["1494"])
                appData.aBRaceT = E_aBRaceT.White;
            else if ("X" == input.App["1628"])
                appData.aBRaceT = E_aBRaceT.Other;
            appData.aBORaceDesc = input.App["1629"];

            if ("X" == input.App["1498"])
                appData.aCRaceT = E_aCRaceT.AmericanIndian;
            else if ("X" == input.App["1499"])
                appData.aCRaceT = E_aCRaceT.Asian;
            else if ("X" == input.App["1500"])
                appData.aCRaceT = E_aCRaceT.Black;
            else if ("X" == input.App["1501"])
                appData.aCRaceT = E_aCRaceT.Hispanic;
            else if ("X" == input.App["1502"])
                appData.aCRaceT = E_aCRaceT.White;
            else if ("X" == input.App["1630"])
                appData.aCRaceT = E_aCRaceT.Other;
            appData.aCORaceDesc = input.App["1631"];

            if ("X" == input.App["1495"])
                appData.aBGender = E_GenderT.Female;
            else if ("X" == input.App["1496"])
                appData.aBGender = E_GenderT.Male;
            if ("X" == input.App["1503"])
                appData.aCGender = E_GenderT.Female;
            else if ("X" == input.App["1504"])
                appData.aCGender = E_GenderT.Male;

            switch (input.App["1621"])
            {
                case "IP":
                    appData.aBDecPastOwnedPropT = E_aBDecPastOwnedPropT.IP;
                    break;
                case "PR":
                    appData.aBDecPastOwnedPropT = E_aBDecPastOwnedPropT.PR;
                    break;
                case "SH":
                    appData.aBDecPastOwnedPropT = E_aBDecPastOwnedPropT.SH;
                    break;
            }
            switch (input.App["1622"])
            {
                case "O":
                    appData.aBDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.O;
                    break;
                case "S":
                    appData.aBDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.S;
                    break;
                case "SP":
                    appData.aBDecPastOwnedPropTitleT = E_aBDecPastOwnedPropTitleT.SP;
                    break;
            }

            switch (input.App["1624"])
            {
                case "IP":
                    appData.aCDecPastOwnedPropT = E_aCDecPastOwnedPropT.IP;
                    break;
                case "PR":
                    appData.aCDecPastOwnedPropT = E_aCDecPastOwnedPropT.PR;
                    break;
                case "SH":
                    appData.aCDecPastOwnedPropT = E_aCDecPastOwnedPropT.SH;
                    break;
            }
            switch (input.App["1625"])
            {
                case "O":
                    appData.aCDecPastOwnedPropTitleT = E_aCDecPastOwnedPropTitleT.O;
                    break;
                case "S":
                    appData.aCDecPastOwnedPropTitleT = E_aCDecPastOwnedPropTitleT.S;
                    break;
                case "SP":
                    appData.aCDecPastOwnedPropTitleT = E_aCDecPastOwnedPropTitleT.SP;
                    break;
            }
            if ("X" == input.App["1505"])
                appData.aBInterviewMethodT = appData.aCInterviewMethodT = E_aIntrvwrMethodT.FaceToFace;
            else if ("X" == input.App["1506"])
                appData.aBInterviewMethodT = appData.aCInterviewMethodT = E_aIntrvwrMethodT.ByMail;
            else if ("X" == input.App["1507"])
                appData.aBInterviewMethodT = appData.aCInterviewMethodT = E_aIntrvwrMethodT.ByTelephone;
            else if ("X" == input.App["1518"])
                appData.aBInterviewMethodT = appData.aCInterviewMethodT = E_aIntrvwrMethodT.Internet;

            #region Assets
            // assets
            /*
                asset-type
                0 - auto
                1 - bonds
                2 - business
                3 - checking
                4 - gift funds
                5 - life insurance
                6 - retirement
                7 - savings
                8 - stocks
                9 - other
             */
            bool bJoint = "X" == input.App["1183"];	// filed jointly or not
            IAssetCollection assetColl = appData.aAssetCollection;

            string v = input.App["1291"].ToString(); // cash dep 1
            if ("" != v)
            {
                var cashdep1 = assetColl.GetCashDeposit1(true);
                cashdep1.Val_rep = v;
                cashdep1.Desc = input.App["1290"].ToString();
                cashdep1.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            v = input.App["1293"].ToString(); // cash dep 2
            if ("" != v)
            {
                var cashdep2 = assetColl.GetCashDeposit2(true);
                cashdep2.Val_rep = v;
                cashdep2.Desc = input.App["1292"].ToString();
                cashdep2.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            IAssetRegular stock;
            v = input.App["1367"].ToString(); // stocks 1
            if ("" != v)
            {
                stock = assetColl.AddRegularRecord();
                stock.AssetT = E_AssetRegularT.Stocks;
                stock.Val_rep = v;
                stock.Desc = input.App["1366"].ToString();
                stock.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            v = input.App["1369"].ToString(); // stocks 2
            if ("" != v)
            {
                stock = assetColl.AddRegularRecord();
                stock.AssetT = E_AssetRegularT.Stocks;
                stock.Val_rep = v;
                stock.Desc = input.App["1368"].ToString();
                stock.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }


            v = input.App["1388"].ToString(); // stocks 3
            if ("" != v)
            {
                stock = assetColl.AddRegularRecord();
                stock.AssetT = E_AssetRegularT.Stocks;
                stock.Val_rep = v;
                stock.Desc = input.App["1387"].ToString();
                stock.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            v = input.App["1371"].ToString(); // life insurance
            if ("" != v)
            {
                var lifeIns = assetColl.GetLifeInsurance(true);
                lifeIns.Val_rep = v;
                lifeIns.FaceVal_rep = input.App["1370"].ToString();
                lifeIns.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            v = input.App["1373"].ToString(); // retirement
            if ("" != v)
            {
                var retirement = assetColl.GetRetirement(true);
                retirement.Val_rep = v;
                retirement.Desc = input.App["Vested Retired Interests"].ToString();
                retirement.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            v = input.App["1374"].ToString(); // business
            if ("" != v)
            {
                var business = assetColl.GetBusinessWorth(true);
                business.Val_rep = v;
                business.Desc = input.App["Business Owned"].ToString();
                business.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            IAssetRegular auto;
            v = input.App["1376"].ToString(); // auto 1
            if ("" != v)
            {
                auto = assetColl.AddRegularRecord();
                auto.AssetT = E_AssetRegularT.Auto;
                auto.Val_rep = v;
                auto.Desc = input.App["1375"].ToString();
                auto.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            v = input.App["1378"].ToString(); // auto 2
            if ("" != v)
            {
                auto = assetColl.AddRegularRecord();
                auto.AssetT = E_AssetRegularT.Auto;
                auto.Val_rep = v;
                auto.Desc = input.App["1377"].ToString();
                auto.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            v = input.App["1380"].ToString(); // auto 3
            if ("" != v)
            {
                auto = assetColl.AddRegularRecord();
                auto.AssetT = E_AssetRegularT.Auto;
                auto.Val_rep = v;
                auto.Desc = input.App["1379"].ToString();
                auto.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            IAssetRegular illiquid;
            v = input.App["1382"].ToString(); // illiquid 1
            if ("" != v)
            {
                illiquid = assetColl.AddRegularRecord();
                illiquid.AssetT = E_AssetRegularT.OtherIlliquidAsset;
                illiquid.Val_rep = v;
                illiquid.Desc = input.App["1381"].ToString();
                illiquid.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            v = input.App["1384"].ToString(); // illiquid 2
            if ("" != v)
            {
                illiquid = assetColl.AddRegularRecord();
                illiquid.AssetT = E_AssetRegularT.OtherIlliquidAsset;
                illiquid.Val_rep = v;
                illiquid.Desc = input.App["1383"].ToString();
                illiquid.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }
            v = input.App["1386"].ToString(); // illiquid 3
            if ("" != v)
            {
                illiquid = assetColl.AddRegularRecord();
                illiquid.AssetT = E_AssetRegularT.OtherIlliquidAsset;
                illiquid.Val_rep = v;
                illiquid.Desc = input.App["1385"].ToString();
                illiquid.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }
            v = input.App["1399"].ToString(); // illiquid 4
            if ("" != v)
            {
                illiquid = assetColl.AddRegularRecord();
                illiquid.AssetT = E_AssetRegularT.OtherIlliquidAsset;
                illiquid.Val_rep = v;
                illiquid.Desc = input.App["1398"].ToString();
                illiquid.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
            }

            // institution assets
            // 9/30/2005 dd - POINT only support max of 20 assets (as of POINT 5.0)
            // Asset types are store from 8880 - 8899
            // Asset Description are store from 1294 - 1354 in group of 6 and 3920 - 3968
            int iAssetCount = 0;
            for (i = 1294; i < 1360; i += 6)
            {

                AddBankAsset(appData, input.App[i.ToString()], input.App[(i + 4).ToString()],
                    input.App[(i + 1).ToString()], input.App[(i + 2).ToString()], input.App[(i + 5).ToString()], input.App[(8880 + iAssetCount).ToString()], bJoint);
                iAssetCount++;
            }
            for (i = 3920; i < 3974; i += 6)
            {
                AddBankAsset(appData, input.App[i.ToString()], input.App[(i + 4).ToString()],
                    input.App[(i + 1).ToString()], input.App[(i + 2).ToString()], input.App[(i + 5).ToString()], input.App[(8880 + iAssetCount).ToString()], bJoint);
                iAssetCount++;
            }
            appData.aAssetCollection.Flush();

            Guid[] realEstateIds = new Guid[25];


            string occR = input.App["2849"];// Point has this as app specific, but Genesis seems to have this for each property. We do what Genesis does.
            // owned real estate information
            // 9/7/2005 dd - Point does not store 25 REO record in consecutive order, the first 10 records are from 2900 - 2989, next 5 is 1400 - 1459, and last 10 is 3600 - 3699
            int[] reStartIds = { 2900, 2910, 2920, 2930, 2940, 2950, 2960, 2970, 2980, 1400, 1410, 1420, 1430, 1440, 1450, 3600, 3610, 3620, 3630, 3640, 3650, 3660, 3670, 3680, 3690 };

            for (int iRe = 0; iRe < reStartIds.Length; ++iRe)
            {
                i = reStartIds[iRe];
                string addr1 = input.App[i.ToString()];						//0 Property Address
                if ("" != addr1)
                {
                    string addr2 = input.App[(i + 1).ToString()];				//1 Property Address
                    string status = input.App[(i + 2).ToString()];			//2 S/PS/R
                    string type = input.App[(i + 3).ToString()];				//3 Type of Property
                    string marketVal = input.App[(i + 4).ToString()];			//4 Present Market Value
                    string mortgage = input.App[(i + 5).ToString()];			//5 Amount of Mortgages and Liens
                    string rentalIncome = input.App[(i + 6).ToString()];		//6 Gross Rental Income
                    string pmt = input.App[(i + 7).ToString()];				//7 Mortgage Payments
                    string otherExpenses = input.App[(i + 8).ToString()];		//8 Insurance, Maintenance, Taxes and Misc.
                    string netRentalIncome = input.App[(i + 9).ToString()];	//9 Net Rental Income

                    // get a new liability record
                    var re = appData.aReCollection.AddRegularRecord();

                    // Store the id of the record so that we can match with liability later (used for FannieMae)
                    realEstateIds[iRe] = re.RecordId;

                    re.Addr = addr1;

                    m_addressParser.ParseCityStateZip(addr2);
                    re.City = m_addressParser.City;
                    re.State = m_addressParser.State;
                    re.Zip = m_addressParser.Zipcode;

                    re.Stat = status;
                    if (null != type)
                    {
                        switch (type.ToUpper())
                        {
                            case "SFR": re.Type = "SFR"; break;
                            case "2-4PLX": re.Type = "2-4Plx"; break;
                            case "COM-NR": re.Type = "Com-NR"; break;
                            case "COM-R": re.Type = "Com-R"; break;
                            case "CONDO": re.Type = "Condo"; break;
                            case "COOP": re.Type = "Coop"; break;
                            case "FARM": re.Type = "Farm"; break;
                            case "LAND": re.Type = "Land"; break;
                            case "MIXED": re.Type = "Mixed"; break;
                            case "MOBIL": re.Type = "Mobil"; break;
                            case "MULTI": re.Type = "Multi"; break;
                            case "TOWN": re.Type = "Town"; break;
                            case "OTHER": re.Type = "Other"; break;
                            case "": re.Type = ""; break;
                            default:
                                re.Type = type;
                                Tools.LogBug(type + " is not handled for REO Type.");
                                break;

                        }
                    }

                    re.Val_rep = marketVal;
                    re.MAmt_rep = mortgage;
                    re.GrossRentI_rep = rentalIncome;
                    re.MPmt_rep = pmt;
                    re.HExp_rep = otherExpenses;
                    re.NetRentI_rep = netRentalIncome;
                    re.NetRentILckd = true;
                    re.OccR_rep = occR;

                    decimal netRentalIncomeValue;
                    if (decimal.TryParse(netRentalIncome, out netRentalIncomeValue))
                    {
                        if (re.GrossRentI > 0 && netRentalIncomeValue != 0)
                        {
                            // 11/30/2010 dd - OPM 49162 - Reverse calculate OccR so it net rent is match.
                            decimal r = (netRentalIncomeValue + re.MPmt + re.HExp) / re.GrossRentI * 100;
                            re.OccR = (int)r;
                        }
                    }
                    if (re.StatT == E_ReoStatusT.PendingSale || re.StatT == E_ReoStatusT.Residence)
                    {
                        re.IsForceCalcNetRentalI = true;
                    }
                    re.Update();
                }
            }
            appData.aReCollection.Flush();

            #endregion

            #region Liabilities
            // liability information
            int iLiaRecord;
            ILiaCollection liaColl = appData.aLiaCollection;
            for (i = 4500, iLiaRecord = 0; i < 4900; i += 10, ++iLiaRecord)
            {
                // process only tradelines where there exists a positive balance
                string bal = input.App[(i + 7).ToString()].TrimWhitespaceAndBOM();
                if (bal != "" && !bal.StartsWith("(")) // OPM 47001 - don't import liabilities with negative balances (surrounded by () )
                {
                    // get a new liability record
                    ILiabilityRegular liability = liaColl.AddRegularRecord();

                    liability.ComNm = input.App[i.ToString()];
                    liability.ComAddr = input.App[(i + 1).ToString()];

                    m_addressParser.ParseCityStateZip(input.App[(i + 2).ToString()]);
                    liability.ComCity = m_addressParser.City;
                    liability.ComState = m_addressParser.State;
                    liability.ComZip = m_addressParser.Zipcode;
                    liability.AccNm = appData.aBNm;

                    liability.AccNum = input.App[(i + 4).ToString()];

                    switch (input.App[(i + 5).ToString()])
                    {
                        case "R":
                            liability.DebtT = E_DebtRegularT.Revolving;
                            break;
                        case "M":
                            liability.DebtT = E_DebtRegularT.Mortgage;
                            break;
                        case "L":
                            liability.DebtT = E_DebtRegularT.Installment;
                            break;
                        default:
                            liability.DebtT = E_DebtRegularT.Other;
                            break;
                    }
                    bool willBePaidOff = "X" == input.App[(i + 6).ToString()];
                    liability.WillBePdOff = willBePaidOff;

                    liability.Bal_rep = input.App[(i + 7).ToString()];

                    liability.Pmt_repPoint = input.App[(i + 8).ToString()];
                    liability.NotUsedInRatio = willBePaidOff; // OPM 47001
                    liability.RemainMons_rep = input.App[(i + 9).ToString()];
                    // 7/26/2005 dd - Import POINT Liability type description.
                    liability.Desc = input.App[(8910 + iLiaRecord).ToString()];

                    liability.MatchedReRecordId = Guid.Empty;
                    try
                    {
                        string valStr = input.App[(3850 + iLiaRecord).ToString()];
                        if (valStr.Length > 0)
                        {
                            int iMatchedReo = int.Parse(valStr) - 1; // From 1 based to 0 based
                            if (iMatchedReo >= 0)
                                liability.MatchedReRecordId = realEstateIds[iMatchedReo];
                        }
                    }
                    catch
                    { }

                    //liability.Update() ;

                }
            }

            // alimony
            if ("" != input.App["1391"])
            {
                ILiabilityAlimony liability = appData.aLiaCollection.GetAlimony(true);
                liability.OwedTo = input.App["1390"];
                liability.Pmt_repPoint = input.App["1391"];
                liability.RemainMons_rep = input.App["1462"];
                //liability.Update() ;
            }

            // job expense 1
            if ("" != input.App["1393"])
            {
                ILiabilityJobExpense liability = appData.aLiaCollection.GetJobRelated1(true);
                liability.ExpenseDesc = input.App["1392"];
                liability.Pmt_repPoint = input.App["1393"];
                //liability.Update() ;
            }

            // job expense 2
            if ("" != input.App["1395"])
            {
                ILiabilityJobExpense liability = appData.aLiaCollection.GetJobRelated2(true);
                liability.ExpenseDesc = input.App["1394"];
                liability.Pmt_repPoint = input.App["1395"];
                //liability.Update() ;
            }

            liaColl.Flush();

            #endregion

            appData.aCrOd_rep = input.App["6305"];
            appData.aCrRd_rep = input.App["6306"];
            appData.aCrN = input.App["6307"];
            appData.aBusCrOd_rep = input.App["6310"];
            appData.aBusCrRd_rep = input.App["6311"];
            appData.aBusCrN = input.App["6312"];

            appData.a1003SignD_rep = input.App["426"];
            appData.aAsstLiaCompletedNotJointly = "X" == input.App["1184"];

            appData.aBExperianScore_rep = input.App["5032"];
            appData.aCExperianScore_rep = input.App["5033"];
            appData.aBTransUnionScore_rep = input.App["5034"];
            appData.aCTransUnionScore_rep = input.App["5035"];
            appData.aBEquifaxScore_rep = input.App["5036"];
            appData.aCEquifaxScore_rep = input.App["5037"];

            //06 10 08 22583 av
            appData.aBExperianCreatedD_rep = input.App["11393"];
            appData.aBExperianFactors = input.App["11394"];
            appData.aCExperianCreatedD_rep = input.App["11395"];
            appData.aCExperianFactors = input.App["11396"];

            appData.aBTransUnionCreatedD_rep = input.App["11400"];
            appData.aBTransUnionFactors = input.App["11401"];
            appData.aCTransUnionCreatedD_rep = input.App["11402"];
            appData.aCTransUnionFactors = input.App["11403"];


            appData.aBEquifaxCreatedD_rep = input.App["11407"];
            appData.aBEquifaxFactors = input.App["11408"];
            appData.aCEquifaxCreatedD_rep = input.App["11409"];
            appData.aCEquifaxFactors = input.App["11410"];

            //06 10 08 22583 av
            appData.aBMinFicoScore_rep = input.App["5027"]; // This field is not used. Just to ensure roundtrip fidelity.
            appData.aCMinFicoScore_rep = input.App["5028"]; // This field is not used. Just to ensure roundtrip fidelity.

            appData.a1003ContEditSheet = input.App["4900"];

            appData.aDenialNoCreditFile = "X" == input.App["3800"];
            appData.aDenialInsufficientCreditRef = "X" == input.App["3801"];
            appData.aDenialInsufficientCreditFile = "X" == input.App["3802"];
            appData.aDenialUnableVerifyCreditRef = "X" == input.App["3803"];
            appData.aDenialGarnishment = "X" == input.App["3804"];
            appData.aDenialExcessiveObligations = "X" == input.App["3805"];
            appData.aDenialInsufficientIncome = "X" == input.App["3806"];
            appData.aDenialUnacceptablePmtRecord = "X" == input.App["3807"];
            appData.aDenialLackOfCashReserves = "X" == input.App["3808"];
            appData.aDenialDeliquentCreditObligations = "X" == input.App["3809"];
            appData.aDenialBankruptcy = "X" == input.App["3810"];
            appData.aDenialInfoFromConsumerReportAgency = "X" == input.App["3811"];
            appData.aDenialUnableVerifyEmployment = "X" == input.App["3812"];
            appData.aDenialLenOfEmployment = "X" == input.App["3813"];
            appData.aDenialTemporaryEmployment = "X" == input.App["3814"];
            appData.aDenialInsufficientIncomeForMortgagePmt = "X" == input.App["3815"];
            appData.aDenialUnableVerifyIncome = "X" == input.App["3816"];
            appData.aDenialTempResidence = "X" == input.App["3817"];
            appData.aDenialShortResidencePeriod = "X" == input.App["3818"];
            appData.aDenialUnableVerifyResidence = "X" == input.App["3819"];
            appData.aDenialByHUD = "X" == input.App["3821"];
            appData.aDenialByVA = "X" == input.App["3822"];
            appData.aDenialByFedNationalMortAssoc = "X" == input.App["3823"];
            appData.aDenialByFedHomeLoanMortCorp = "X" == input.App["3824"];
            appData.aDenialByOther = "X" == input.App["3825"];
            appData.aDenialByOtherDesc = input.App["3839"];
            appData.aDenialInsufficientFundsToClose = "X" == input.App["3826"];
            appData.aDenialCreditAppIncomplete = "X" == input.App["3827"];
            appData.aDenialInadequateCollateral = "X" == input.App["3828"];
            appData.aDenialUnacceptableProp = "X" == input.App["3829"];
            appData.aDenialInsufficientPropData = "X" == input.App["3830"];
            appData.aDenialUnacceptableAppraisal = "X" == input.App["3831"];
            appData.aDenialUnacceptableLeasehold = "X" == input.App["3832"];
            appData.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds = "X" == input.App["3833"];
            appData.aDenialWithdrawnByApp = "X" == input.App["3834"];
            appData.aDenialOtherReason1 = "X" == input.App["3846"];
            appData.aDenialOtherReason1Desc = input.App["3847"];
            appData.aDenialOtherReason2 = "X" == input.App["3845"];
            appData.aDenialOtherReason2Desc = input.App["3848"];
            appData.aDenialDecisionBasedOnReportAgency = "X" == input.App["3837"];
            appData.aDenialDecisionBasedOnCRA = "X" == input.App["3838"];
            appData.aDenialHasAdditionalStatement = "X" == input.App["3790"];

            StringBuilder sBuilder = new StringBuilder(100);
            for (int iDenial = 3791; iDenial <= 3796; ++iDenial)
            {
                sBuilder.Append(input.App[iDenial.ToString("D")] + " ");
            }
            appData.aDenialAdditionalStatement = sBuilder.ToString();

            if (!string.IsNullOrEmpty(input.App["421"]))
            {
                appData.a1003InterviewD_rep = input.App["421"];
                appData.a1003InterviewDLckd = true;
            }

            #endregion // PER-APP DATA

            if (loanData.sIsIncomeCollectionEnabled)
            {
                // We don't know how many times this method is going to be called for a single loan import, so just keep tallying
                // and setting to the loan.
                loanData.ImportGrossRent(this.hasSubjectPropertyNetCashFlow, this.totalSubjectPropertyNetCashFlow);
            }

            // debug code, comment out when not using
            /*
            NameValueCollection nvc = input.GetCollection();
            foreach (string key in nvc)
            {
                Tools.LogInfo("id[" + key + "]=" + nvc[key]);
            }*/
        }

        private void ImportOtherIncome(CPointData loanData, NameValueCollectionWrapper input, LosConvert convertLos, CAppData appData)
        {
            if (!loanData.sIsIncomeCollectionEnabled)
            {
                ImportOtherIncomeLegacy(input, convertLos, appData);
            }
            else
            {
                var otherIncomes = new List<OtherIncome>();
                otherIncomes.AddIfNotNull(GetOtherIncome(loanData, input.App["1230"], input.App["1231"], input.App["1232"], convertLos));
                otherIncomes.AddIfNotNull(GetOtherIncome(loanData, input.App["1233"], input.App["1234"], input.App["1235"], convertLos));
                otherIncomes.AddIfNotNull(GetOtherIncome(loanData, input.App["1236"], input.App["1237"], input.App["1238"], convertLos));

                foreach (var consumerAndIncomeSource in IncomeCollectionMigration.GetMigratedOtherIncome(otherIncomes, appData.aBConsumerId, appData.aCConsumerId))
                {
                    loanData.AddIncomeSource(consumerAndIncomeSource.Item1, consumerAndIncomeSource.Item2);
                }
            }
        }

        private void ImportOtherIncomeLegacy(NameValueCollectionWrapper input, LosConvert convertLos, CAppData appData)
        {
            string BorrOtherIncomeMergeDesc = "";
            string CoborrOtherIncomeMergeDesc = "";

            switch (input.App["1230"])
            {
                default: // Not considered.
                    break;
                case "B":
                    BorrOtherIncomeMergeDesc += input.App["1231"];
                    break;
                case "C":
                    CoborrOtherIncomeMergeDesc += input.App["1231"];
                    break;
            }

            switch (input.App["1233"])
            {
                default: // Not considered.
                    break;
                case "B":
                    if (BorrOtherIncomeMergeDesc.Length > 0)
                        BorrOtherIncomeMergeDesc += "; ";
                    BorrOtherIncomeMergeDesc += input.App["1234"];
                    break;
                case "C":
                    if (CoborrOtherIncomeMergeDesc.Length > 0)
                        CoborrOtherIncomeMergeDesc += "; ";
                    CoborrOtherIncomeMergeDesc += input.App["1234"];
                    break;
            }

            switch (input.App["1236"])
            {
                default: // Not considered.
                    break;
                case "B":
                    if (BorrOtherIncomeMergeDesc.Length > 0)
                        BorrOtherIncomeMergeDesc += "; ";
                    BorrOtherIncomeMergeDesc += input.App["1237"];
                    break;
                case "C":
                    if (CoborrOtherIncomeMergeDesc.Length > 0)
                        CoborrOtherIncomeMergeDesc += "; ";
                    CoborrOtherIncomeMergeDesc += input.App["1237"];
                    break;
            }

            List<OtherIncome> otherIncomeList = new List<OtherIncome>();
            otherIncomeList.Add(new OtherIncome()
            {
                Amount = convertLos.ToMoney(input.App["606"]) + convertLos.ToMoney(input.App["607"]),
                Desc = BorrOtherIncomeMergeDesc,
                IsForCoBorrower = false
            });

            otherIncomeList.Add(new OtherIncome()
            {
                Amount = convertLos.ToMoney(input.App["656"]) + convertLos.ToMoney(input.App["657"]),
                Desc = CoborrOtherIncomeMergeDesc,
                IsForCoBorrower = true
            });

            appData.aOtherIncomeList = otherIncomeList;
        }

        private OtherIncome GetOtherIncome(CPointData loanData, string owner, string description, string amountStr, LosConvert convertLos)
        {
            bool isForCoborrower;

            switch (owner)
            {
                default:
                    return null;
                case "B":
                    isForCoborrower = false;
                    break;
                case "C":
                    isForCoborrower = true;
                    break;
            }

            decimal amount = convertLos.ToMoney(amountStr);

            if (description?.Equals(OtherIncome.GetDescription(E_aOIDescT.SubjPropNetCashFlow), StringComparison.OrdinalIgnoreCase) ?? false)
            {
                loanData.sSpCountRentalIForPrimaryResidToo = true;
                this.hasSubjectPropertyNetCashFlow = true;
                this.totalSubjectPropertyNetCashFlow += amount;
                return null;
            }
            else
            {
                return new OtherIncome
                {
                    Desc = description,
                    Amount = amount,
                    IsForCoBorrower = isForCoborrower
                };
            }

        }

        public class RoleChangeRecord
        {
            public Guid Current = Guid.Empty;
            public Guid Default = Guid.Empty;
            public Guid Changed = Guid.Empty;
        }

        /// <summary>
        /// This class exists to gather the employment data points together so we can judge whether to import all or none.
        /// Names are copied from <see cref="IPrimaryEmploymentRecord"/> according to their target.
        /// </summary>
        private class PrimaryEmploymentRecord
        {
            public bool HasNonBlankValue => !string.IsNullOrEmpty(this.EmplmtLen_rep)
                || this.IsSelfEmplmt
                || !string.IsNullOrEmpty(this.EmplrBusPhone)
                || !string.IsNullOrEmpty(this.ProfLen_rep)
                || !string.IsNullOrEmpty(this.JobTitle)
                || !string.IsNullOrEmpty(this.EmplrNm)
                || !string.IsNullOrEmpty(this.EmplrAddr)
                || !string.IsNullOrEmpty(this.EmplrCity)
                || !string.IsNullOrEmpty(this.EmplrState)
                || !string.IsNullOrEmpty(this.EmplrZip);

            public string EmplmtLen_rep { get; set; }

            public bool IsSelfEmplmt { get; set; }

            public string EmplrBusPhone { get; set; }

            public string ProfLen_rep { get; set; }

            public string JobTitle { get; set; }

            public string EmplrNm { get; set; }

            public string EmplrAddr { get; set; }

            public string EmplrCity { get; set; }

            public string EmplrState { get; set; }

            public string EmplrZip { get; set; }

            public void ApplyTo(IPrimaryEmploymentRecord employment)
            {
                employment.EmplmtLen_rep = this.EmplmtLen_rep;
                employment.IsSelfEmplmt = this.IsSelfEmplmt;
                employment.EmplrBusPhone = this.EmplrBusPhone;
                employment.ProfLen_rep = this.ProfLen_rep;
                employment.JobTitle = this.JobTitle;
                employment.EmplmtStat = E_EmplmtStat.Current;
                employment.EmplrNm = this.EmplrNm;
                employment.EmplrAddr = this.EmplrAddr;
                employment.EmplrCity = this.EmplrCity;
                employment.EmplrState = this.EmplrState;
                employment.EmplrZip = this.EmplrZip;
            }
        }

        public void TransferDataToPoint(CPointData loanData, NameValueCollectionWrapper input, int iApp, bool alwaysLoadFullData = false)
        {
            if (loanData.sLId != this.loanId)
            {
                throw new CBaseException(
                    ErrorMessages.Generic,
                    "Attempted to import data into a loan file that does not match the loan id specified in the constructor.");
            }

            BrokerDB broker = BrokerDB.RetrieveById(m_brokerId);
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null == principal)
            {
                throw new AccessDenied();
            }
            
            #region SHARED FIELDS
            if (0 == iApp || alwaysLoadFullData)
            {
                input.Shared["1"] = loanData.sLNm;

                CAppData primeApp = loanData.GetAppData(0);
                input.Shared["2"] = primeApp.aBLastNm;
                input.Shared["3"] = primeApp.aBFirstNm;
                input.Shared["4"] = loanData.sEmployeeLoanRep.FullName;

                input.Shared["22"] = loanData.sMersMin; 
                input.Shared["5"] = loanData.sOpenedD_rep;
                //input.Shared["7"] = @"C:\PNTDATA8\BORROWER\Thinh_Jose.BRW"; file location

                input.Shared["2904"] = loanData.sSpMarketVal_rep;

                string newVal = "";

                SplitAndAssignToPoint(input.Shared, 7805, 7807, loanData.sVaTitleLimitDesc);
                input.Shared["7808"] = loanData.sVaTitleLimitIsCondo ? "X" : "";
                input.Shared["7809"] = loanData.sVaTitleLimitIsPud ? "X" : "";
                input.Shared["7908"] = loanData.sSpLotDimension;
                input.Shared["7810"] = loanData.sSpLotIsIrregular ? "X" : "";
                input.Shared["7811"] = loanData.sSpLotIrregularSqf;
                input.Shared["7812"] = loanData.sSpLotIsAcres ? "X" : "";
                input.Shared["7813"] = loanData.sSpLotAcres;

                switch (loanData.sSpUtilElecT)
                {
                    case E_sSpUtilT.Public: input.Shared["7814"] = "X"; break;
                    case E_sSpUtilT.Community: input.Shared["7818"] = "X"; break;
                    case E_sSpUtilT.Individual: input.Shared["7822"] = "X"; break;
                }

                switch (loanData.sSpUtilGasT)
                {
                    case E_sSpUtilT.Public: input.Shared["7815"] = "X"; break;
                    case E_sSpUtilT.Community: input.Shared["7819"] = "X"; break;
                    case E_sSpUtilT.Individual: input.Shared["7823"] = "X"; break;
                }

                switch (loanData.sSpUtilWaterT)
                {
                    case E_sSpUtilT.Public: input.Shared["7816"] = "X"; break;
                    case E_sSpUtilT.Community: input.Shared["7820"] = "X"; break;
                    case E_sSpUtilT.Individual: input.Shared["7824"] = "X"; break;
                }

                switch (loanData.sSpUtilSanSewerT)
                {
                    case E_sSpUtilT.Public: input.Shared["7817"] = "X"; break;
                    case E_sSpUtilT.Community: input.Shared["7821"] = "X"; break;
                    case E_sSpUtilT.Individual: input.Shared["7825"] = "X"; break;
                }

                input.Shared["7826"] = loanData.sSpHasOven ? "X" : "";
                input.Shared["7827"] = loanData.sSpHasRefrig ? "X" : "";
                input.Shared["7828"] = loanData.sSpHasDishWasher ? "X" : "";
                input.Shared["7829"] = loanData.sSpHasClothesWasher ? "X" : "";
                input.Shared["7830"] = loanData.sSpHasDryer ? "X" : "";
                input.Shared["7831"] = loanData.sSpHasGarbageDisposal ? "X" : "";
                input.Shared["7832"] = loanData.sSpHasVentFan ? "X" : "";
                input.Shared["7833"] = loanData.sSpHasWwCarpet ? "X" : "";
                input.Shared["7909"] = loanData.sSpHasOtherEquip ? "X" : "";
                input.Shared["7910"] = loanData.sSpOtherEquipDesc;

                switch (loanData.sVaBuildingStatusT)
                {
                    case E_sVaBuildingStatusT.Proposed: input.Shared["7834"] = "X"; break;
                    case E_sVaBuildingStatusT.Existing: input.Shared["7836"] = "X"; break;
                    case E_sVaBuildingStatusT.UnderConstruction: input.Shared["7837"] = "X"; break;
                    case E_sVaBuildingStatusT.Repair: input.Shared["7838"] = "X"; break;
                    case E_sVaBuildingStatusT.LeaveBlank: break;
                    default:
                        Tools.LogBug("Unhandled enum value for E_sVaBuildingStatusT in Point export");
                        break;
                }

                switch (loanData.sVaBuildingT)
                {
                    case E_sVaBuildingT.Detached: input.Shared["7839"] = "X"; break;
                    case E_sVaBuildingT.SemiDetached: input.Shared["7840"] = "X"; break;
                    case E_sVaBuildingT.Row: input.Shared["7841"] = "X"; break;
                    case E_sVaBuildingT.AptUnit: input.Shared["7842"] = "X"; break;
                    case E_sVaBuildingT.LeaveBlank: break;
                    default:
                        Tools.LogBug("Unhanded enum value for E_sVaBuildingT in Point export");
                        break;
                }
                input.Shared["7843"] = FromTriStateToXYes(loanData.sVaIsFactoryFabricatedTri);
                input.Shared["7844"] = FromTriStateToXNo(loanData.sVaIsFactoryFabricatedTri);
                input.Shared["7845"] = loanData.sVaNumOfBuildings;
                input.Shared["7846"] = loanData.sVaNumOfLivingUnits;
                input.Shared["7847"] = FromTriStateToXYes(loanData.sVaStreetAccessPrivateTri);
                input.Shared["7848"] = FromTriStateToXNo(loanData.sVaStreetAccessPrivateTri);
                input.Shared["7849"] = FromTriStateToXYes(loanData.sVaStreetMaintenancePrivateTri);
                input.Shared["7850"] = FromTriStateToXNo(loanData.sVaStreetMaintenancePrivateTri);
                input.Shared["7851"] = FromTriStateToXYes(loanData.sVaConstructWarrantyTri);
                input.Shared["7852"] = FromTriStateToXNo(loanData.sVaConstructWarrantyTri);
                input.Shared["7853"] = loanData.sVaConstructWarrantyProgramNm;
                input.Shared["7854"] = loanData.sVaConstructExpiredD_rep;
                input.Shared["7855"] = loanData.sVaConstructCompleteD_rep;
                input.Shared["7856"] = loanData.sVaOwnerNm;

                switch (loanData.sVaSpOccT)
                {
                    case E_sVaSpOccT.OccByOwner: input.Shared["7857"] = "X"; break;
                    case E_sVaSpOccT.NeverOcc: input.Shared["7858"] = "X"; break;
                    case E_sVaSpOccT.Vacant: input.Shared["7859"] = "X"; break;
                    case E_sVaSpOccT.Rental: input.Shared["7860"] = "X"; break;
                    case E_sVaSpOccT.LeaveBlank: break;
                    default:
                        Tools.LogBug("Unhandled enum value for E_sVaSpOccT in Point Export");
                        break;
                }
                input.Shared["7861"] = loanData.sVaSpRentalMonthly_rep;
                input.Shared["7862"] = loanData.sVaSpOccupantNm;
                input.Shared["7863"] = loanData.sVaSpOccupantPhone;
                //--TODO: Export the name of broker (7864)and telephone(7865)
                input.Shared["7866"] = loanData.sVaSpAvailableForInspectDesc;

                switch (loanData.sVaSpAvailableForInspectTimeT)
                {
                    case E_sVaSpAvailableForInspectTimeT.AM: input.Shared["7867"] = "X"; break;
                    case E_sVaSpAvailableForInspectTimeT.PM: input.Shared["7878"] = "X"; break;
                    case E_sVaSpAvailableForInspectTimeT.BothAmPm:
                        input.Shared["7867"] = "X";
                        input.Shared["7878"] = "X";
                        break;
                    case E_sVaSpAvailableForInspectTimeT.LeaveBlank: break;
                    default:
                        Tools.LogBug("Unhanded enum value for E_sVaSpAvailableForInspectTimeT in Point Export");
                        break;
                }

                //--TODO: 7869,7870 (keys at address, use preparer)
                switch (loanData.sVaConstructComplianceInspectionMadeByT)
                {
                    case E_sVaConstructComplianceInspectionMadeByT.Fha:
                        input.Shared["7874"] = "X";
                        break;
                    case E_sVaConstructComplianceInspectionMadeByT.Va:
                        input.Shared["7875"] = "X";
                        break;
                    case E_sVaConstructComplianceInspectionMadeByT.None:
                        input.Shared["7876"] = "X";
                        break;
                    case E_sVaConstructComplianceInspectionMadeByT.LeaveBlank:
                        break;
                    default:
                        Tools.LogBug("Unhanded enum value for E_sVaConstructComplianceInspectionMadeByT in Point Export");
                        break;
                }

                input.Shared["7877"] = FromTriStateToXYes(loanData.sVaConstructPlansFirstSubmitTri);
                input.Shared["7878"] = FromTriStateToXNo(loanData.sVaConstructPlansFirstSubmitTri);
                input.Shared["7879"] = loanData.sVaConstructPrevPlansCaseNum;

                //--TODO: builder:7880:name,7882:street address,7883:city,state,and zip,7884:phone
                //--TODO: warrantor:7885:name,7887:streetAddr,7888,citystatezip,7889:phone
                SplitAndAssignToPoint(input.Shared, new int[] { 7881, 7890, 7891 }, loanData.sVaSpecialAssessmentsComments);
                //--item 31: use sProRealETxPerYr
                input.Shared["7893"] = FromTriStateToXYes(loanData.sSpMineralRightsReservedTri);
                input.Shared["7895"] = FromTriStateToXNo(loanData.sSpMineralRightsReservedTri);

                SplitAndAssignToPoint(input.Shared, new int[] { 7894, 7907 }, loanData.sSpMineralRightsReservedExplain);

                input.Shared["7896"] = loanData.sSpLeaseIs99Yrs ? "X" : "";
                input.Shared["7897"] = loanData.sSpLeaseIsRenewable ? "X" : "";
                input.Shared["7899"] = loanData.sLeaseHoldExpireD_rep;
                input.Shared["7900"] = loanData.sSpLeaseAnnualGroundRent_rep;
                //--item 34a: sPurchPrice_rep
                input.Shared["7901"] = FromTriStateToXYes(loanData.sLotPurchaseSeparatelyTri);
                input.Shared["7902"] = FromTriStateToXNo(loanData.sLotPurchaseSeparatelyTri);
                input.Shared["7903"] = loanData.sVaRefiAmt_rep;
                input.Shared["7904"] = FromTriStateToXYes(loanData.sVaSaleContractAttachedTri);
                input.Shared["7905"] = FromTriStateToXNo(loanData.sVaSaleContractAttachedTri);
                input.Shared["7906"] = loanData.sVaPrevApprovedContractNum;
                //--TODO: Exporting the preparer at the end of the Reasonable Value form 7920, 7921,7922,7923

                //---------------

                input.Shared["8378"] = loanData.sVaIrrrlsUsedOnlyPdInFullLNum;
                input.Shared["8385"] = loanData.sVaIrrrlsUsedOnlyOrigLAmt_rep;
                input.Shared["8386"] = loanData.sVaIrrrlsUsedOnlyOrigIR_rep;
                SplitAndAssignToPoint(input.Shared, 8380, 8384, loanData.sVaIrrrlsUsedOnlyRemarks);
                //sVaFfExemptTri tinyint not null default( 0 ) --Point doesn't have this, it always default to NO
                input.Shared["7680"] = loanData.sVaRefiWsExistingVaLBal_rep;
                input.Shared["7681"] = loanData.sVaRefiWsCashPmtFromVet_rep;
                input.Shared["7682"] = loanData.sVaRefiWsExistingVaLBalAfterCashPmt_rep;
                input.Shared["7683"] = loanData.sVaRefiWsDiscntPc_rep;
                input.Shared["7684"] = loanData.sVaRefiWsDiscnt_rep;
                input.Shared["7685"] = loanData.sVaRefiWsOrigFeePc_rep;
                input.Shared["7686"] = loanData.sVaRefiWsOrigFee_rep;
                input.Shared["7687"] = loanData.sVaRefiWsFfPc_rep;
                input.Shared["7688"] = loanData.sVaRefiWsFf_rep;
                input.Shared["7689"] = loanData.sVaRefiWsAllowableCcAndPp_rep;
                input.Shared["7690"] = loanData.sVaRefiWsPreliminaryTot_rep;
                input.Shared["7691"] = loanData.sVaRefiWsFinalDiscntPc_rep;
                input.Shared["7692"] = loanData.sVaRefiWsFinalDiscnt_rep;
                input.Shared["7693"] = loanData.sVaRefiWsFinalSubtotalItem12_rep;
                input.Shared["7694"] = loanData.sVaRefiWsFinalSubtotalItem14_rep;
                input.Shared["7696"] = loanData.sVaRefiWsFinalSubtotalItem16_rep;
                input.Shared["7695"] = loanData.sVaRefiWsFinalFf_rep;
                input.Shared["7698"] = loanData.sVaRefiWsMaxLAmt_rep;

                input.Shared["8301"] = loanData.sVaIsAutoIrrrlProc ? "X" : "";
                switch (loanData.sVaLPurposeT)
                {
                    case E_sVaLPurposeT.Alteration: input.Shared["8315"] = "X"; break;
                    case E_sVaLPurposeT.Condo: input.Shared["8314"] = "X"; break;
                    case E_sVaLPurposeT.Home: input.Shared["8312"] = "X"; break;
                    case E_sVaLPurposeT.LeaveBlank: break;
                    case E_sVaLPurposeT.ManufacturedHome: input.Shared["8313"] = "X"; break;
                    case E_sVaLPurposeT.Refinance: input.Shared["8316"] = "X"; break;
                }
                //sVaLPurposeTLckd bit not null default(0),
                //sVaLCodeT int not null default( 0 ), --,8326:Irrrl, 8327:Cashout refi, 8328:Manufactured home refi, 8329:ref over 90 of RV
                //				    -- probably a combination of the loan type and subject property type
                switch (loanData.sVaLCodeT)
                {
                    case E_sVaLCodeT.LeaveBlank: break;
                    case E_sVaLCodeT.Purchase: input.Shared["8325"] = "X"; break;
                    case E_sVaLCodeT.Irrrl: input.Shared["8326"] = "X"; break;
                    case E_sVaLCodeT.CashoutRefin: input.Shared["8327"] = "X"; break;
                    case E_sVaLCodeT.ManufacturedHomeRefi: input.Shared["8328"] = "X"; break;
                    case E_sVaLCodeT.RefiOver90Rv: input.Shared["8329"] = "X"; break;
                }
                //sVaLCodeTLckd bit not null default( 0 ), 
                //sVaFinMethT int not null default( 0 ), --8330:regular fixed, 8331:GPM-Never Exceed CRV, 8332:Other GPMs, 8333:GEM, 8334:Temporary buydown, 8335:Hybrid ARM
                //				--probably a calc based sFinMethT and other guys in TIL
                switch (loanData.sVaFinMethT)
                {
                    case E_sVaFinMethT.LeaveBlank: break;
                    case E_sVaFinMethT.RegularFixed: input.Shared["8330"] = "X"; break;
                    case E_sVaFinMethT.GpmNeverExceedCrv: input.Shared["8331"] = "X"; break;
                    case E_sVaFinMethT.GpmOther: input.Shared["8332"] = "X"; break;
                    case E_sVaFinMethT.Gem: input.Shared["8333"] = "X"; break;
                    case E_sVaFinMethT.TemporaryBuydown: input.Shared["8334"] = "X"; break;
                    case E_sVaFinMethT.HybridArm: input.Shared["8335"] = "X"; break;
                    case E_sVaFinMethT.ARM: input.Shared["8335"] = "X"; break; // 7/31/2008 dd - Export as HybridArm because Point does not have this new option yet.
                    default:
                        Tools.LogBug("Unhandled enum value of E_sVaFinMethT in Point Export");
                        break;
                }
                //sVaFinMethTLckd bit not null default( 0 ),--
                switch (loanData.sVaHybridArmT)
                {
                    case E_sVaHybridArmT.LeaveBlank: break;
                    case E_sVaHybridArmT.ThreeOne: input.Shared["8291"] = "3/1"; break;
                    case E_sVaHybridArmT.FiveOne: input.Shared["8291"] = "5/1"; break;
                    case E_sVaHybridArmT.SevenEleven: input.Shared["8291"] = "7/11"; break;
                    case E_sVaHybridArmT.TenOne: input.Shared["8291"] = "10/1"; break;
                    default:
                        Tools.LogBug("Unhanded enum value of E_sVaHybridArmT in Point Export");
                        break;
                }

                switch (loanData.sVaOwnershipT)
                {
                    case E_sVaOwnershipT.LeaveBlank: break;
                    case E_sVaOwnershipT.SoleOwnership: input.Shared["8317"] = "X"; break;
                    case E_sVaOwnershipT.JointWithOtherVets: input.Shared["8318"] = "X"; break;
                    case E_sVaOwnershipT.JointWithNonVet: input.Shared["8319"] = "X"; break;
                    default:
                        Tools.LogBug("Unhanded enum value of E_sVaOwnershipT in Point export");
                        break;
                }
                input.Shared["8345"] = loanData.sVaEnergyImprovNone ? "X" : "";
                input.Shared["8346"] = loanData.sVaEnergyImprovIsSolar ? "X" : "";
                input.Shared["8347"] = loanData.sVaEnergyImprovIsMajorSystem ? "X" : "";
                input.Shared["8348"] = loanData.sVaEnergyImprovIsNewFeature ? "X" : "";
                input.Shared["8349"] = loanData.sVaEnergyImprovIsInsulation ? "X" : "";
                input.Shared["8350"] = loanData.sVaEnergyImprovIsOther ? "X" : "";
                input.Shared["8351"] = loanData.sVaEnergyImprovAmt_rep;

                switch (loanData.sVaApprT)
                {
                    case E_sVaApprT.LeaveBlank: break;
                    case E_sVaApprT.SingleProp: input.Shared["8352"] = "X"; break;
                    case E_sVaApprT.MasterCrvCase: input.Shared["8353"] = "X"; break;
                    case E_sVaApprT.LappLenderAppr: input.Shared["8354"] = "X"; break;
                    case E_sVaApprT.ManufacturedHome: input.Shared["8355"] = "X"; break;
                    case E_sVaApprT.HudVaConversion: input.Shared["8356"] = "X"; break;
                    case E_sVaApprT.PropMgmtCase: input.Shared["8457"] = "X"; break; // TODO: Review this one, might be just 8357, Point shows 8457 though
                    default:
                        Tools.LogBug("Unhanded enum value of E_sVaApprT in Point Export");
                        break;
                }

                switch (loanData.sVaStructureT)
                {
                    case E_sVaStructureT.LeaveBlank: break;
                    case E_sVaStructureT.Conventional: input.Shared["8361"] = "X"; break;
                    case E_sVaStructureT.SingleWideMobil: input.Shared["8362"] = "X"; break;
                    case E_sVaStructureT.DoubleWideMobil: input.Shared["8363"] = "X"; break;
                    case E_sVaStructureT.MobilLotOnly: input.Shared["8364"] = "X"; break;
                    case E_sVaStructureT.PrefabricatedHome: input.Shared["8365"] = "X"; break;
                    case E_sVaStructureT.CondoConversion: input.Shared["8366"] = "X"; break;
                    default:
                        Tools.LogBug("Unhanded enum value of E_sVaStructureT in Point export");
                        break;
                }

                switch (loanData.sVaPropDesignationT)
                {
                    case E_sVaPropDesignationT.LeaveBlank: break;
                    case E_sVaPropDesignationT.ExistingOcc: input.Shared["8367"] = "X"; break;
                    case E_sVaPropDesignationT.ProposedConstruction: input.Shared["8368"] = "X"; break;
                    case E_sVaPropDesignationT.ExistingNew: input.Shared["8369"] = "X"; break;
                    case E_sVaPropDesignationT.EnergyImprovement: input.Shared["8370"] = "X"; break;
                    default:
                        Tools.LogBug("Unhanded enum value of E_sVaPropDesignationT in Point export");
                        break;
                    //int not null default( 0 ), --8367:Existing Occupied, 8368:appraised as proposed construction, 8369:new existing, 3870:energy improvments
                }

                input.Shared["8371"] = loanData.sVaMcrvNum;
                switch (loanData.sVaManufacturedHomeT)
                {
                    case E_sVaManufacturedHomeT.LeaveBlank: break;
                    case E_sVaManufacturedHomeT.NotMobilHome: input.Shared["8357"] = "X"; break;
                    case E_sVaManufacturedHomeT.MobilHomeOnly: input.Shared["8358"] = "X"; break;
                    case E_sVaManufacturedHomeT.MobilHomeVetOwnedLot: input.Shared["8359"] = "X"; break;
                    case E_sVaManufacturedHomeT.MobilHomeOnPermanentFoundation: input.Shared["8360"] = "X"; break;
                    default:
                        Tools.LogBug("Unhanded enum value of E_sVaManufacturedHomeT in Point export");
                        break;
                }
                input.Shared["8548"] = loanData.sVaLenSarId;// varchar(50) not null default( '' ), --Lender Sar ID:8548
                input.Shared["8299"] = loanData.sVaSarNotifIssuedD_rep;
                switch (loanData.sVaAppraisalOrSarAdjustmentTri)
                {
                    case E_TriState.Blank: input.Shared["8295"] = ""; break;
                    case E_TriState.No: input.Shared["8295"] = "No"; break;
                    case E_TriState.Yes: input.Shared["8295"] = "Yes"; break;
                }

                switch (loanData.sVaAutoUnderwritingT)
                {
                    //int not null default( 0 ), --8294:LP,DU,PMI Aura,Clues,Zippy
                    case E_sVaAutoUnderwritingT.LeaveBlank: input.Shared["8294"] = ""; break;
                    case E_sVaAutoUnderwritingT.Lp: input.Shared["8294"] = "LP"; break;
                    case E_sVaAutoUnderwritingT.Du: input.Shared["8294"] = "DU"; break;
                    case E_sVaAutoUnderwritingT.PmiAura: input.Shared["8294"] = "PMI AURA"; break;
                    case E_sVaAutoUnderwritingT.Clues: input.Shared["8294"] = "CLUES"; break;
                    case E_sVaAutoUnderwritingT.Zippy: input.Shared["8294"] = "ZIPPY"; break;
                    default:
                        Tools.LogBug("Unhanded enum value of E_sVaAutoUnderwritingT in Point export");
                        break;
                }

                switch (loanData.sVaRiskT)
                {
                    case E_sVaRiskT.LeaveBlank: input.Shared["8293"] = ""; break;
                    case E_sVaRiskT.Refer: input.Shared["8293"] = "Refer"; break;
                    case E_sVaRiskT.Approve: input.Shared["8293"] = "Approve"; break;
                }
                input.Shared["8292"] = loanData.sVaVetMedianCrScore_rep;//int not null default( 0 ), --8292, score of vet only, add this to Credit Scores screen
                //sVaVetMedianCrScoreLckd bit not null default( 0 ), 
                input.Shared["8374"] = loanData.sVaLDiscntPc_rep;// decimal(9,3) not null default( 0 ), --:calc based on sLDiscntPc and sVaLDiscntLckd
                input.Shared["8375"] = loanData.sVaLDiscnt_rep; // money not null default( 0 ), --: calc based on sLDiscnt and sVaLDiscntLckd
                //sVaLDiscntLckd bit not null default( 0 ), --for both Pc and amount
                input.Shared["8376"] = loanData.sVaLDiscntPcPbb_rep;//(9,3) not null default( 0 ), --8376:calc based sVaLDiscntPc and sVaLDsicntPbbLckd
                input.Shared["8377"] = loanData.sVaLDiscntPbb_rep;// money not null default( 0 ), --8377:calc based sVaLDiscnt and sVaLDsicntPbbLckd
                //sVaLDiscntPbbLckd bit not null default( 0 ) --for both sVaLDiscntPcPbb and sVaLDiscntPbb

                input.Shared["2196"] = loanData.sVaCashdwnPmt_rep;
                //sVaCashdwnPmtLckd bit not null default( 0 ),
                input.Shared["7741"] = loanData.sVaProThisMPmt_rep;
                //sVaProThisMPmtLckd bit not null default( 0 ),
                input.Shared["7742"] = loanData.sVaProRealETx_rep;
                //sVaProRealETxLckd bit not null default( 0 ),
                input.Shared["7743"] = loanData.sVaProHazIns_rep;
                //sVaProHazInsLckd bit not null default( 0 ),-- 
                input.Shared["7745"] = loanData.sVaProMaintenancePmt_rep;
                input.Shared["7746"] = loanData.sVaProUtilityPmt_rep;
                input.Shared["7748"] = loanData.sVaProMonthlyPmt_rep;


                input.Shared["9365"] = loanData.sProFloodInsFaceAmt_rep;
                input.Shared["9381"] = loanData.sVaAgent1RoleDesc;
                input.Shared["9385"] = loanData.sVaAgent2RoleDesc;
                input.Shared["9389"] = loanData.sVaAgent3RoleDesc;
                input.Shared["9393"] = loanData.sVaAgent4RoleDesc;
                input.Shared["9397"] = loanData.sVaAgent5RoleDesc;

                input.Shared["8300"] = loanData.sVaIsAutoProc ? "X" : "";
                input.Shared["8302"] = loanData.sVaIsPriorApprovalProc ? "X" : "";
                input.Shared["9350"] = loanData.sVaLenderCaseNum;
                //sVaLenderCaseNumLckd bit not null default( 0 ),
                input.Shared["9352"] = loanData.sVaIsGuarantyEvidenceRequested ? "X" : "";
                input.Shared["9353"] = loanData.sVaIsInsuranceEvidenceRequested ? "X" : "";
                input.Shared["9354"] = loanData.sLNoteD_rep;
                input.Shared["9355"] = loanData.sLProceedPaidOutD_rep;
                input.Shared["9356"] = loanData.sMaturityD_rep;
                //sMaturityDLckd smalldatetime,
                switch (loanData.sVaLienPosT)
                {
                    case E_sVaLienPosT.Blank:
                    case E_sVaLienPosT.First: // first and second are taken care by sLienPosT field
                    case E_sVaLienPosT.Second:
                        break;
                    case E_sVaLienPosT.FirstChattel:
                        input.Shared["9357"] = "X";
                        break;
                    case E_sVaLienPosT.Unsecured:
                        input.Shared["9358"] = "X";
                        break;
                    case E_sVaLienPosT.Other:
                        input.Shared["9359"] = "X";
                        break;
                }



                //sVaLienPosTLckd bit not null default( 0 ),
                input.Shared["9360"] = loanData.sVaLienPosOtherDesc;
                switch (loanData.sVaEstateHeldT)
                {
                    case E_sVaEstateHeldT.Blank:
                    case E_sVaEstateHeldT.FeeSimple: // fee and leasehold are taken care by sEstateHeldT
                    case E_sVaEstateHeldT.LeaseHold:
                        break;
                    case E_sVaEstateHeldT.Other:
                        input.Shared["9361"] = "X";
                        break;

                }
                //sVaEstateHeldTLckd bit not null default( 0 ),
                input.Shared["9362"] = loanData.sVaEstateHeldOtherDesc;
                input.Shared["7892"] = loanData.sProRealETxPerYr_rep;
                //sProRealETxPerYrLckd bit not null default( 0 ),
                input.Shared["9363"] = loanData.sProHazInsFaceAmt_rep;
                input.Shared["9364"] = loanData.sProHazInsPerYr_rep;
                //sProHazInsPerYrLckd bit not null default( 0 ),
                input.Shared["9365"] = loanData.sProFloodInsPerYr_rep;
                //sProFloodInsPerYrLckd bit not null default( 0 ),
                input.Shared["9367"] = loanData.sVaSpecialAssessPmtPerYear_rep;
                input.Shared["7744"] = loanData.sVaSpecialAssessPmt_rep;
                input.Shared["9368"] = loanData.sVaSpecialAssessUnpaid_rep;
                input.Shared["9369"] = loanData.sVaMaintainAssessPmtPerYear_rep;
                //sVaMaintainAssessPmtPerYearLckd bit not null default (0), 
                input.Shared["7747"] = loanData.sVaMaintainAssessPmt_rep;
                input.Shared["9370"] = loanData.sVaNonrealtyAcquiredWithLDesc;
                //sVaNonrealtyAcquiredWithLDescPrintSeparately bit not null default( 0 ), if long, auto turn this on.
                input.Shared["9371"] = loanData.sVaAdditionalSecurityTakenDesc;
                //sVaAdditionalSecurityTakenDescPrintSeparately bit not null default( 0 ),if long, auto turn this on.
                input.Shared["9372"] = loanData.sLotAcquiredD_rep;
                input.Shared["9373"] = loanData.sVaLotPurchPrice_rep;
                switch (loanData.sVaLProceedDepositT)
                {
                    case E_sVaLProceedDepositT.Blank: break;
                    case E_sVaLProceedDepositT.Escrow:
                        input.Shared["9374"] = "X";
                        break;
                    case E_sVaLProceedDepositT.EarmarkedAcc:
                        input.Shared["9375"] = "X";
                        break;

                }
                input.Shared["9376"] = loanData.sVaLProceedDepositDesc;
                input.Shared["9398"] = loanData.sIsAlterationCompleted ? "X" : "";

                //---------------
                switch (loanData.sEstateHeldT)
                {
                    case E_sEstateHeldT.FeeSimple:
                        input.Shared["1227"] = "X";
                        break;
                    case E_sEstateHeldT.LeaseHold:
                        input.Shared["1228"] = "X";
                        break;
                }

                input.Shared["1229"] = loanData.sLeaseHoldExpireD_rep;

                input.Shared["2791"] = loanData.sHcltvR_rep;
                input.Shared["2790"] = loanData.sDebtToHousingGapR_rep;
                input.Shared["2051"] = FromBoolToXYes(loanData.sApprFull);
                input.Shared["2052"] = FromBoolToXYes(loanData.sApprDriveBy);
                input.Shared["2057"] = FromBoolToXYes(loanData.sIsSpReviewNoAppr);
                input.Shared["2058"] = loanData.sSpReviewFormNum;
                input.Shared["2814"] = loanData.sTransmFntc_rep;
                input.Shared["2815"] = loanData.sVerifAssetAmt_rep;
                input.Shared["2816"] = loanData.sFntcSrc;
                input.Shared["2817"] = loanData.sRsrvMonNumDesc;
                input.Shared["2818"] = loanData.sInterestedPartyContribR_rep;
                input.Shared["2792"] = FromBoolToXYes(loanData.sIsManualUw);
                input.Shared["2793"] = FromBoolToXYes(loanData.sIsDuUw);
                input.Shared["2794"] = FromBoolToXYes(loanData.sIsLpUw);
                input.Shared["2795"] = FromBoolToXYes(loanData.sIsOtherUw);
                input.Shared["2796"] = loanData.sOtherUwDesc;
                input.Shared["2797"] = loanData.sAusRecommendation;

                //av  opm 20261
                input.Shared["2798"] = loanData.sLpAusKey;
                input.Shared["3890"] = loanData.sDuCaseId;

                input.Shared["2799"] = loanData.sLpDocClass;
                input.Shared["2836"] = loanData.sRepCrScore;
                input.Shared["8870"] = FromBoolToXYes(loanData.sIsCommLen);
                input.Shared["2813"] = FromBoolToXYes(loanData.sIsHOwnershipEdCertInFile);
                input.Shared["2767"] = FromBoolToXYes(loanData.sIsMOrigBroker);
                input.Shared["2768"] = FromBoolToXYes(loanData.sIsMOrigCorrespondent);
                input.Shared["2789"] = loanData.sTransmBuydwnTermDesc;

                switch (loanData.sFredProcPointT)
                {
                    case E_sFredProcPointT.Application:
                        newVal = "Application/Processing";
                        break;
                    case E_sFredProcPointT.FinalDisposition:
                        newVal = "Final Disposition";
                        break;
                    case E_sFredProcPointT.LeaveBlank:
                        newVal = "";
                        break;
                    case E_sFredProcPointT.PostClosingQualityControl:
                        newVal = "Post-Closing QC";
                        break;
                    case E_sFredProcPointT.Prequalification:
                        newVal = "Prequal (No URLA)";
                        break;
                    case E_sFredProcPointT.Underwriting:
                        newVal = "Underwriting";
                        break;
                    default:
                        Tools.LogBug("Unhandled enum value of E_sFredProcPointT in Point Export");
                        newVal = "";
                        break;
                }
                input.Shared["5703"] = newVal;

                switch (loanData.sGseSpT)
                {
                    case E_sGseSpT.Attached:
                    case E_sGseSpT.Detached:
                        newVal = "1"; //"1 Unit";
                        break;
                    case E_sGseSpT.DetachedCondominium:
                    case E_sGseSpT.HighRiseCondominium:
                    case E_sGseSpT.Condominium:
                        newVal = "3"; // "Condo";
                        break;
                    case E_sGseSpT.PUD:
                        newVal = "4"; // "PUD";
                        break;
                    case E_sGseSpT.Cooperative:
                        newVal = "5"; // "Co-Op";
                        break;
                    case E_sGseSpT.ManufacturedHomeMultiwide:
                        newVal = "7"; // "Manufactured Multiwide";
                        break;
                    case E_sGseSpT.ManufacturedHousingSingleWide:
                        newVal = "6"; // "Manufactured Single Wide";
                        break;
                    case E_sGseSpT.LeaveBlank:
                    case E_sGseSpT.ManufacturedHomeCondominium:
                    case E_sGseSpT.ManufacturedHousing:
                    case E_sGseSpT.Modular:
                    default:
                        newVal = "";
                        break;

                }
                input.Shared["2729"] = newVal;
                /*
            switch( loanData.sGseSpT )
            {
                case E_sGseSpT.Attached:
                    newVal = "Single Family Attached";
                    break;
                case E_sGseSpT.Condominium:
                    newVal = "Low-rise Condo (up to 4)";
                    break;
                case E_sGseSpT.Cooperative:
                    newVal = "Cooperative"; 
                    break;
                case E_sGseSpT.Detached:
                    newVal = "Single Family Detached";					   
                    break;
                case E_sGseSpT.HighRiseCondominium:
                    newVal = "High rise Condo (5 or more)";
                    break;
                case E_sGseSpT.ManufacturedHomeMultiwide:
                    newVal = "Manufactured Home Multiwide";
                    break;
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    newVal = "Manufactured Home Singlewide";
                    break;
                case E_sGseSpT.PUD: // not sure attached or detached pud
                case E_sGseSpT.LeaveBlank:
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.Modular:
                    newVal = "";
                    break;
                default:
                    newVal = "";
                    Tools.LogBug( "Unhandled enum value of E_sGseSpT in Point export." );
                    break;
            }
            input.Shared["5705"] = newVal;
            */

                switch (loanData.sBuildingStatusT)
                {
                    case E_sBuildingStatusT.LeaveBlank: newVal = ""; break;
                    case E_sBuildingStatusT.Existing: newVal = "Existing"; break;
                    case E_sBuildingStatusT.Proposed: newVal = "Proposed"; break;
                    case E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab: newVal = "Alteration, improvement, repair"; break;
                    case E_sBuildingStatusT.SubstantiallyRehabilitated: newVal = "Substantially rehabilitated"; break;
                    case E_sBuildingStatusT.UnderConstruction: newVal = "Under Construction"; break;
                    default:
                        Tools.LogBug("Unhandled enum value of E_sBuildingStatusT in Point export.");
                        newVal = "";
                        break;
                }
                input.Shared["5721"] = newVal;

                input.Shared["5778"] = loanData.sHelocCreditLimit_rep;
                input.Shared["5777"] = loanData.sHelocBal_rep;

                switch (loanData.sFreddieDocT)
                {
                    case E_sFreddieDocT.FullDocumentation: newVal = "Full documentation"; break;
                    case E_sFreddieDocT.LeaveBlank: newVal = ""; break;
                    case E_sFreddieDocT.NoDepEmpIncVerif: newVal = "No DV, EV or IV"; break;
                    case E_sFreddieDocT.NoDepositVerif: newVal = "No DV"; break;
                    case E_sFreddieDocT.NoDoc: newVal = ""; break;
                    case E_sFreddieDocT.NoEmpIncVerif: newVal = "No EV or IV"; break;
                    default:
                        Tools.LogBug("Unhandled enum value of E_sFreddieDocT in Point export.");
                        newVal = "";
                        break;
                }
                input.Shared["5738"] = newVal;
                input.Shared["5743"] = loanData.sMICoveragePc_rep;
                input.Shared["5733"] = loanData.sFredAffordProgId;

                switch (loanData.sFreddieArmIndexT)
                {
                    case E_sFreddieArmIndexT.LeaveBlank: newVal = ""; break;
                    case E_sFreddieArmIndexT.OneYearTreasury: newVal = "1-Year Treasury"; break;
                    case E_sFreddieArmIndexT.ThreeYearTreasury: newVal = "3-Year Treasury"; break;
                    case E_sFreddieArmIndexT.SixMonthTreasury: newVal = "6-Month T-Bill"; break;
                    case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds: newVal = "COFI (11th District Monthly COF)"; break;
                    case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds: newVal = "National Median COF"; break;
                    case E_sFreddieArmIndexT.LIBOR: newVal = "LIBOR"; break;
                    case E_sFreddieArmIndexT.Other: newVal = "Other"; break;
                    default:
                        Tools.LogBug("Unhandled enum value of E_sFreddieArmIndexT in Point export.");
                        newVal = "";
                        break;
                }
                input.Shared["5736"] = newVal;

                switch (loanData.sBuydownContributorT)
                {
                    case E_sBuydownContributorT.LeaveBlank: newVal = ""; break;
                    case E_sBuydownContributorT.Borrower: newVal = "Borrower"; break;
                    case E_sBuydownContributorT.Builder: newVal = "Builder"; break;
                    case E_sBuydownContributorT.LenderPremiumFinanced: newVal = "Lender"; break;
                    case E_sBuydownContributorT.Other: newVal = "Other"; break;
                    case E_sBuydownContributorT.Seller: newVal = "Seller"; break;
                    case E_sBuydownContributorT.Unassigned: newVal = "Unassigned"; break;
                    case E_sBuydownContributorT.Parent: newVal = "Parent"; break;
                    case E_sBuydownContributorT.NonParentRelative: newVal = "NonParentRelative"; break;
                    case E_sBuydownContributorT.UnrelatedFriend: newVal = "UnrelatedFriend"; break;
                    case E_sBuydownContributorT.Employer: newVal = "Employer"; break;
                    default:
                        Tools.LogBug("Unhandled enum value of E_sBuydownContributorT in Point export.");
                        newVal = "";
                        break;

                }
                input.Shared["5737"] = newVal;

                input.Shared["2770"] = FromBoolToXYes(loanData.sTexasDiscWillSubmitToLender);
                input.Shared["2771"] = FromBoolToXYes(loanData.sTexasDiscAsIndependentContractor);
                input.Shared["2772"] = FromBoolToXYes(loanData.sTexasDiscWillActAsFollows);
                input.Shared["2773"] = loanData.sTexasDiscWillActAsFollowsDesc;
                input.Shared["2774"] = FromBoolToXYes(loanData.sTexasDiscCompensationIncluded);
                input.Shared["2775"] = FromBoolToXYes(loanData.sTexasDiscChargeVaried);
                input.Shared["2776"] = loanData.sTexasDiscReceivedF_rep;
                input.Shared["2777"] = FromBoolToXYes(loanData.sTexasDiscAppFInc);
                input.Shared["2778"] = loanData.sTexasDiscAppFAmt_rep;
                input.Shared["2779"] = FromBoolToXYes(loanData.sTexasDiscProcFInc);
                input.Shared["2780"] = FromBoolToXYes(loanData.sTexasDiscApprFInc);
                input.Shared["2781"] = FromBoolToXYes(loanData.sTexasDiscCrFInc);
                input.Shared["2782"] = FromBoolToXYes(loanData.sTexasDiscAutoUnderwritingFInc);
                input.Shared["2784"] = loanData.sTexasDiscAutoUnderwritingFAmt_rep;
                input.Shared["2785"] = loanData.sTexasDiscOF1IncDesc;
                input.Shared["2786"] = loanData.sTexasDiscOF1IncAmt_rep;
                input.Shared["2787"] = loanData.sTexasDiscOF2IncDesc;
                input.Shared["2788"] = loanData.sTexasDiscOF2IncAmt_rep;
                input.Shared["2783"] = loanData.sTexasDiscNonRefundAmt_rep;

                //loanData.sFloodHazardBuilding = 11415
                //sFloodHazardMobileHome bit not null default( 0 ), --11416
                input.Shared["2423"] = loanData.sFloodHazardCommunityDesc;
                input.Shared["2428"] = FromBoolToXYes(loanData.sFloodHazardFedInsAvail);
                input.Shared["2431"] = FromBoolToXYes(loanData.sFloodHazardFedInsNotAvail);
                input.Shared["7369"] = loanData.sCommitExpD_rep;
                SplitAndAssignToPoint(input.Shared, 7353, 7360, loanData.sCommitRepayTermsDesc);
                input.Shared["7361"] = loanData.sCommitTitleEvidence;
                input.Shared["7350"] = FromBoolToXYes(loanData.sCommitReturnToAboveAddr);
                input.Shared["7351"] = FromBoolToXYes(loanData.sCommitReturnToFollowAddr);
                input.Shared["7352"] = loanData.sCommitReturnWithinDays_rep;

                input.Shared["8107"] = FromTriStateToXYes(loanData.sFHA203kSpHudOwnedTri);
                input.Shared["8451"] = FromTriStateToXYes(loanData.sFHAPropImprovHasFedPastDueTri);
                input.Shared["8452"] = FromTriStateToXYes(loanData.sFHAPropImprovHasFHAPendingAppTri);
                input.Shared["8453"] = loanData.sFHAPropImprovHasFHAPendingAppWithWhom;
                input.Shared["8454"] = FromTriStateToXYes(loanData.sFHAPropImprovRefinTitle1LoanTri);
                input.Shared["8455"] = loanData.sFHAPropImprovRefinTitle1LoanNum;
                input.Shared["8456"] = loanData.sFHAPropImprovRefinTitle1LoanBal_rep;
                input.Shared["128"] = loanData.sFHAPropImprovBorrRelativeNm;
                input.Shared["126"] = loanData.sFHAPropImprovBorrRelativeRelationship;
                input.Shared["125"] = loanData.sFHAPropImprovBorrRelativePhone;
                input.Shared["127"] = loanData.sFHAPropImprovBorrRelativeAddr;

                input.Shared["178"] = loanData.sFHAPropImprovCoborRelativeNm;
                input.Shared["176"] = loanData.sFHAPropImprovCoborRelativeRelationship;
                input.Shared["175"] = loanData.sFHAPropImprovCoborRelativePhone;
                input.Shared["177"] = loanData.sFHAPropImprovCoborRelativeAddr;

                input.Shared["8570"] = FromBoolToXYes(loanData.sFHAPropImprovBorrHasChecking);
                input.Shared["8571"] = FromBoolToXYes(loanData.sFHAPropImprovBorrHasSaving);
                input.Shared["8576"] = FromBoolToXYes(loanData.sFHAPropImprovCoborHasChecking);
                input.Shared["8577"] = FromBoolToXYes(loanData.sFHAPropImprovCoborHasSaving);
                input.Shared["8573"] = loanData.sFHAPropImprovBorrBankInfo.Value;
                input.Shared["8579"] = loanData.sFHAPropImprovCoborBankInfo.Value;

                input.Shared["936"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropSingleFamily);
                input.Shared["938"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropMultifamily);
                input.Shared["8560"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropNonresidential);
                input.Shared["8561"] = loanData.sFHAPropImprovNonresidentialUsageDesc;
                input.Shared["7957"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropManufacturedHome);
                input.Shared["8562"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropHistoricResidential);
                input.Shared["8563"] = loanData.sFHAPropImprovIsPropHistoricResidentialUnitsNum;
                input.Shared["8564"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropHealthCareFacility);
                input.Shared["8461"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropOwnedByBorr);
                input.Shared["8462"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropLeasedFromSomeone);
                input.Shared["8463"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropBeingPurchasedOnContract);
                input.Shared["8464"] = FromBoolToXYes(loanData.sFHAPropImprovIsThereMortOnProp);
                input.Shared["8465"] = loanData.sFHAPropImprovLeaseOwnerInfo;
                input.Shared["8468"] = loanData.sFHAPropImprovLeaseMonPmt_rep;
                input.Shared["8469"] = loanData.sFHAPropImprovLeaseExpireD_rep;
                input.Shared["8551"] = FromBoolToXYes(loanData.sFHAPropImprovIsPropNewOccMoreThan90Days);
                input.Shared["8552"] = loanData.sFHAPropImprovDealerContractorContactInfo;

                input.Shared["3918"] = loanData.sFloridaContractDays_rep;
                input.Shared["3915"] = loanData.sFloridaBorrEstimateSpVal_rep;
                input.Shared["3916"] = loanData.sFloridaBorrEstimateExistingMBal_rep;
                input.Shared["3909"] = loanData.sFloridaBorrDeposit_rep;
                input.Shared["3900"] = loanData.sFloridaBrokerFeeR_rep;
                input.Shared["3901"] = loanData.sFloridaBrokerFeeMb_rep;
                input.Shared["3903"] = loanData.sFloridaAdditionalCompMinR_rep;
                input.Shared["3904"] = loanData.sFloridaAdditionalCompMinMb_rep;
                input.Shared["3906"] = loanData.sFloridaAdditionalCompMaxR_rep;
                input.Shared["3907"] = loanData.sFloridaAdditionalCompMaxMb_rep;
                input.Shared["3917"] = loanData.sFloridaAppFee_rep;
                if (loanData.sFloridaIsAppFeeRefundable)
                {
                    input.Shared["3911"] = "X";
                    input.Shared["3912"] = "";
                }
                else
                {
                    input.Shared["3911"] = "";
                    input.Shared["3912"] = "X";
                }
                input.Shared["3914"] = loanData.sFloridaIsAppFeeApplicableToCc ? "X" : "";
                input.Shared["3902"] = loanData.sFloridaBrokerFee_rep;
                input.Shared["3905"] = loanData.sFloridaAdditionalCompMin_rep;
                input.Shared["3908"] = loanData.sFloridaAdditionalCompMax_rep;

                input.Shared["7856"] = loanData.sTitleReqOwnerNm;
                input.Shared["7915"] = loanData.sTitleReqOwnerPhone;

                if (loanData.sTitleReqPriorPolicy)
                    input.Shared["5870"] = "X";
                if (loanData.sTitleReqWarrantyDeed)
                    input.Shared["5871"] = "X";
                if (loanData.sTitleReqInsRequirements)
                    input.Shared["5872"] = "X";
                if (loanData.sTitleReqSurvey)
                    input.Shared["5873"] = "X";
                if (loanData.sTitleReqContract)
                    input.Shared["5874"] = "X";
                input.Shared["5875"] = loanData.sTitleReqPolicyTypeDesc;
                if (loanData.sTitleReqMailAway)
                    input.Shared["5876"] = "X";

                // 10/26/2004 dd - sTitleReqInstruction is from 5877 - 5886. Each line is about 250 characters long.
                string sTitleReqInstruction = loanData.sTitleReqInstruction.TrimWhitespaceAndBOM();
                if (sTitleReqInstruction.Length > 0)
                {
                    // 10/26/2004 dd - When import back, POINT only take the first 1000 characters.
                    sTitleReqInstruction.Substring(0, Math.Min(1000, sTitleReqInstruction.Length));

                    string[] pieces = Split(sTitleReqInstruction, 250, 4);
                    for (int i = 5877; i < pieces.Length + 5877; i++)
                        input.Shared[i.ToString()] = pieces[i - 5877];
                    //                    input.Shared["5877"] = loanData.sTitleReqInstruction;
                }
                input.Shared["5891"] = loanData.sInsReqReplacement;
                if (loanData.sInsReqFlood)
                    input.Shared["5892"] = "X";
                if (loanData.sInsReqWind)
                    input.Shared["5893"] = "X";
                if (loanData.sInsReqHazard)
                    input.Shared["5894"] = "X";
                if (loanData.sInsReqEscrow)
                    input.Shared["5895"] = "X";
                input.Shared["5896"] = loanData.sInsReqComments;

                input.Shared["2712"] = loanData.s3rdPartyOrigNmLn1;
                input.Shared["2713"] = loanData.s3rdPartyOrigNmLn2;

                // seller information
                input.Shared["2720"] = loanData.sLenNm;
                input.Shared["2723"] = loanData.sLenNum;
                input.Shared["2725"] = loanData.sLenLNum;
                input.Shared["2721"] = loanData.sLenAddr;
                input.Shared["2722"] = Tools.CombineCityStateZip(loanData.sLenCity, loanData.sLenState, loanData.sLenZip);
                input.Shared["2731"] = loanData.sLenContactTitle;
                input.Shared["2732"] = loanData.sLenContactPhone;	// 2733 is ignored
                input.Shared["2730"] = loanData.sLenContactNm;
                switch (loanData.sLenCommitT)
                {
                    case E_sLenCommitT.Standard:
                        input.Shared["2734"] = "X";
                        break;
                    case E_sLenCommitT.Negotiated:
                        input.Shared["2735"] = "X";
                        break;
                }

                input.Shared["2724"] = loanData.sInvestorLockLoanNum;
                input.Shared["2740"] = loanData.sHas1stTimeBuyer ? "X" : "";

                input.Shared["2727"] = loanData.sContractNum;
                input.Shared["2726"] = loanData.sCommitNum;

                switch (loanData.s1stMOwnerT)
                {
                    case E_s1stMOwnerT.FannieMae:
                        input.Shared["2715"] = "X";
                        break;
                    case E_s1stMOwnerT.FreddieMac:
                        input.Shared["2716"] = "X";
                        break;
                    case E_s1stMOwnerT.SellerOrOther:
                        input.Shared["2717"] = "X";
                        break;
                }
                input.Shared["831"] = "X";
                input.Shared["805"] = loanData.sTotEstPp1003_rep;


                input.Shared["1039"] = loanData.sAggregateAdjRsrv_rep;


                switch (loanData.sLT)
                {
                    case E_sLT.Conventional:
                        input.Shared["26"] = "X";
                        break;
                    case E_sLT.VA:
                        input.Shared["27"] = "X";
                        break;
                    case E_sLT.FHA:
                        input.Shared["28"] = "X";
                        break;
                    case E_sLT.UsdaRural:
                        input.Shared["29"] = "X";
                        break;
                    case E_sLT.Other:
                        input.Shared["1196"] = "X";
                        break;
                }

                switch (loanData.sLienPosT)
                {
                    case E_sLienPosT.First:
                        input.Shared["915"] = "X";
                        break;
                    case E_sLienPosT.Second:
                        input.Shared["916"] = "X";
                        break;
                }

                switch (loanData.sLPurposeT)
                {
                    case E_sLPurposeT.Purchase:
                        input.Shared["1190"] = "X";
                        break;
                    case E_sLPurposeT.ConstructPerm:
                        input.Shared["1191"] = "X";
                        break;
                    case E_sLPurposeT.Construct:
                        input.Shared["1192"] = "X";
                        break;
                    case E_sLPurposeT.RefinCashout:
                        input.Shared["1193"] = "X";
                        break;
                    case E_sLPurposeT.Other:
                        input.Shared["1194"] = "X";
                        break;
                    case E_sLPurposeT.VaIrrrl:
                    case E_sLPurposeT.FhaStreamlinedRefinance:
                    case E_sLPurposeT.Refin:
                        input.Shared["1198"] = "X";
                        break;
                    default:
                        throw new UnhandledEnumException(loanData.sLPurposeT);
                }
                input.Shared["1195"] = loanData.sOLPurposeDesc;

                input.Shared["800"] = loanData.sPurchPrice_rep;
                input.Shared["525"] = loanData.sEquityCalc_rep;
                input.Shared["801"] = loanData.sApprVal_rep;
                if (broker.IsExportPricingInfoTo3rdParty) // OPM 34444
                {
                    input.Shared["12"] = loanData.sNoteIR_rep;
                    input.Shared["14"] = loanData.sQualIR_rep;
                }

                input.Shared["13"] = loanData.sTerm_rep;
                input.Shared["3190"] = loanData.sDue_rep;
                input.Shared["31"] = loanData.sSpAddr;
                input.Shared["32"] = loanData.sSpCity;
                input.Shared["33"] = loanData.sSpState;
                input.Shared["34"] = loanData.sSpZip;
                input.Shared["35"] = loanData.sSpCounty;

                // property type
                switch (loanData.sSpT)
                {
                    case E_sSpT.DetachedHousing:
                        input.Shared["932"] = "X";
                        break;
                    case E_sSpT.AttachedHousing:
                        input.Shared["933"] = "X";
                        break;
                    case E_sSpT.Condominium:
                        input.Shared["929"] = "X";
                        break;
                    case E_sSpT.PUD:
                        input.Shared["930"] = "X";
                        break;
                    case E_sSpT.COOP:
                        input.Shared["931"] = "X";
                        break;
                }

                // todo: We have this as mutually exclusive whereas Point has 3 categories for them.
                // property classification
                switch (loanData.sSpProjClassT)
                {
                    case E_sSpProjClassT.AIIICondo:
                        input.Shared["2700"] = "X";
                        break;
                    case E_sSpProjClassT.BIICondo:
                        input.Shared["2701"] = "X";
                        break;
                    case E_sSpProjClassT.CICondo:
                        input.Shared["2702"] = "X";
                        break;
                    case E_sSpProjClassT.EPUD:
                        input.Shared["2703"] = "X";
                        break;
                    case E_sSpProjClassT.FPUD:
                        input.Shared["2704"] = "X";
                        break;
                    case E_sSpProjClassT.IIIPUD:
                        input.Shared["2705"] = "X";
                        break;
                    case E_sSpProjClassT.COOP1:
                        input.Shared["2706"] = "X";
                        break;
                    case E_sSpProjClassT.COOP2:
                        input.Shared["2707"] = "X";
                        break;
                }

                input.Shared["401"] = loanData.sProRealETxR_rep;
                input.Shared["754"] = loanData.sProRealETx_rep;
                input.Shared["756"] = loanData.sProHoAssocDues_rep;
                input.Shared["757"] = loanData.sProOHExp_rep;
                input.Shared["544"] = loanData.sReqTopR_rep;
                input.Shared["545"] = loanData.sReqBottomR_rep;
                input.Shared["546"] = loanData.sMaxLtv_rep;
                input.Shared["547"] = loanData.sMaxCltv_rep;

                input.Shared["2718"] = loanData.s1stMtgOrigLAmt_rep;
                input.Shared["2719"] = loanData.sRemain1stMBal_rep;
                input.Shared["751"] = loanData.sRemain1stMPmt_rep;
                input.Shared["2714"] = loanData.sSubFin_rep;
                if (E_sLienPosT.First == loanData.sLienPosT)
                    input.Shared["809"] = loanData.sONewFinBal_rep;
                else
                    input.Shared["2800"] = loanData.sONewFinBal_rep;

                input.Shared["2848"] = loanData.sSpGrossRent_rep;
                //#if xxxxyy
                input.Shared["2849"] = loanData.sOccR_rep; // Point has this as app specific, but Genesis seems to have this for each property. We do what Genesis does.

                if (loanData.sAprIncludesReqDeposit)
                    input.Shared["2155"] = "X";
                if (loanData.sHasDemandFeature)
                    input.Shared["2156"] = "X";
                if (loanData.sHasVarRFeature)
                    input.Shared["2157"] = "X";

                //string longStr = loanData.sVarRNotes.TrimWhitespaceAndBOM();		

                string sVarRNotesStr = loanData.sVarRNotes.TrimWhitespaceAndBOM();
                if (sVarRNotesStr.Length > 0)
                {
                    string[] pieces = Split(sVarRNotesStr, 180, 4);
                    for (int iPiece = 0; iPiece < pieces.Length; ++iPiece)
                    {
                        int iBase;
                        switch (iPiece)
                        {
                            case 0: iBase = 2201; break;
                            case 1: iBase = 2202; break;
                            case 2: iBase = 2205; break;
                            case 3: iBase = 2206; break;
                            default: throw new CBaseException(ErrorMessages.Generic, "Programming error during exporting sVarRNotes");
                        }
                        input.App[iBase.ToString("D")] = pieces[iPiece];
                    }
                }

                if (broker.IsExportPricingInfoTo3rdParty) // OPM 34444
                {
                    input.Shared["2338"] = loanData.sRAdj1stCapR_rep;
                    input.Shared["2330"] = loanData.sRAdj1stCapMon_rep;
                    input.Shared["2324"] = loanData.sRAdjCapR_rep;
                    input.Shared["2331"] = loanData.sRAdjCapMon_rep;
                    input.Shared["2325"] = loanData.sRAdjLifeCapR_rep;
                    input.Shared["2322"] = loanData.sRAdjMarginR_rep; 
                    input.Shared["2329"] = loanData.sRAdjIndexR_rep;
                    input.Shared["2332"] = loanData.sRAdjFloorR_rep;
                }

                /*
                 * 0 - normal
                 * 1 - up
                 * 2 - down
                 */
                switch (loanData.sRAdjRoundT)
                {
                    case DataAccess.E_sRAdjRoundT.Up:
                        input.Shared["2320"] = "X";
                        break;
                    case DataAccess.E_sRAdjRoundT.Down:
                        input.Shared["2321"] = "X";
                        break;
                    case DataAccess.E_sRAdjRoundT.Normal:
                        // do nothing
                        break;
                }

                input.Shared["2323"] = loanData.sRAdjRoundToR_rep;
                input.Shared["2327"] = loanData.sPmtAdjCapR_rep;
                input.Shared["2335"] = loanData.sPmtAdjCapMon_rep;
                input.Shared["2336"] = loanData.sPmtAdjRecastPeriodMon_rep;
                input.Shared["2337"] = loanData.sPmtAdjRecastStop_rep;
                input.Shared["2328"] = loanData.sPmtAdjMaxBalPc_rep;
                input.Shared["2450"] = loanData.sBuydwnR1_rep;
                input.Shared["2451"] = loanData.sBuydwnMon1_rep;
                input.Shared["2452"] = loanData.sBuydwnR2_rep;
                input.Shared["2453"] = loanData.sBuydwnMon2_rep;
                input.Shared["2454"] = loanData.sBuydwnR3_rep;
                input.Shared["2455"] = loanData.sBuydwnMon3_rep;
                input.Shared["2456"] = loanData.sBuydwnR4_rep;
                input.Shared["2457"] = loanData.sBuydwnMon4_rep;
                input.Shared["2458"] = loanData.sBuydwnR5_rep;
                input.Shared["2459"] = loanData.sBuydwnMon5_rep;
                input.Shared["3201"] = loanData.sGradPmtYrs_rep;
                input.Shared["3200"] = loanData.sGradPmtR_rep;
                if (loanData.sBiweeklyPmt)
                    input.Shared["556"] = "X";
                input.Shared["555"] = loanData.sIOnlyMon_rep;
                input.Shared["964"] = loanData.sProMInsMon_rep;
                input.Shared["963"] = loanData.sProMIns2Mon_rep;
                input.Shared["961"] = loanData.sProMInsCancelLtv_rep;
                if (loanData.sProMInsMidptCancel)
                    input.Shared["949"] = "X";

                input.Shared["57"] = loanData.sSchedDueD1_rep;	// 20001 is ignored

                if (loanData.sSecurityPurch)
                    input.Shared["2171"] = "X";
                if (loanData.sSecurityCurrentOwn)
                    input.Shared["2172"] = "X";
                input.Shared["2161"] = loanData.sFilingF;
                input.Shared["2190"] = loanData.sLateDays;
                input.Shared["2185"] = loanData.sLateChargePc;
                input.Shared["2159"] = loanData.sLateChargeBaseDesc;

                /*
                 * 0 - will not
                 * 1 - may
                 */
                switch (loanData.sPrepmtPenaltyT)
                {
                    case DataAccess.E_sPrepmtPenaltyT.May:
                        input.Shared["2173"] = "X";
                        break;
                    case DataAccess.E_sPrepmtPenaltyT.WillNot:
                        input.Shared["2174"] = "X";
                        break;
                }

                /*
                 * 0 - will not
                 * 1 - may
                 */
                switch (loanData.sPrepmtRefundT)
                {
                    case DataAccess.E_sPrepmtRefundT.May:
                        input.Shared["2175"] = "X";
                        break;
                    case DataAccess.E_sPrepmtRefundT.WillNot:
                        input.Shared["2176"] = "X";
                        break;
                }

                /*
                 * 0 - cannot
                 * 1 - may
                 */
                switch (loanData.sAssumeLT)
                {
                    case DataAccess.E_sAssumeLT.May:
                        input.Shared["2177"] = "X";
                        break;
                    case DataAccess.E_sAssumeLT.MaySubjectToCondition:
                        input.Shared["2178"] = "X";
                        break;
                    case DataAccess.E_sAssumeLT.MayNot:
                        input.Shared["2179"] = "X";
                        break;
                }
                if (loanData.sAsteriskEstimate)
                    input.Shared["1037"] = "X";
                if (loanData.sOnlyLatePmtEstimate)
                    input.Shared["1036"] = "X";

                input.Shared["6075"] = loanData.sEstCloseD_rep;

                #region 801 Loan Origination Fee
                input.Shared["965"] = loanData.sLOrigFPc_rep;
                input.Shared["1022"] = loanData.sLOrigFMb_rep;

                GfeItemPropsUnpacker GIPUnpacker = new GfeItemPropsUnpacker();
                GIPUnpacker.Unpack(loanData.sLOrigFProps);
                input.Shared["1081"] = GIPUnpacker.Pfc;
                input.Shared["8220"] = GIPUnpacker.Pbs;
                input.Shared["8260"] = GIPUnpacker.Fa;
                input.Shared["3107"] = GIPUnpacker.Poc;
                #endregion

                #region 802 Loan Discount
                input.Shared["967"] = loanData.sLDiscntPc_rep;
                input.Shared["968"] = loanData.sLDiscntFMb_rep;
                GIPUnpacker.Unpack(loanData.sLDiscntProps);
                input.Shared["1082"] = GIPUnpacker.Pfc;
                input.Shared["8221"] = GIPUnpacker.Pbs;
                input.Shared["2065"] = GIPUnpacker.Fa;
                input.Shared["3108"] = GIPUnpacker.Poc;
                #endregion

                #region 803 Appraisal Fee
                if (loanData.sApprFPaid)
                    input.Shared["5029"] = "X";
                input.Shared["1005"] = loanData.sApprF_rep;
                GIPUnpacker.Unpack(loanData.sApprFProps);
                input.Shared["1068"] = GIPUnpacker.Pfc;
                input.Shared["8222"] = GIPUnpacker.Pbs;
                input.Shared["8262"] = GIPUnpacker.Fa;
                input.Shared["5029"] = GIPUnpacker.Poc;
                #endregion

                #region 804 Credit Report
                if (loanData.sCrFPaid)
                    input.Shared["5030"] = "X";
                input.Shared["1006"] = loanData.sCrF_rep;
                GIPUnpacker.Unpack(loanData.sCrFProps);
                input.Shared["1069"] = GIPUnpacker.Pfc;
                input.Shared["8223"] = GIPUnpacker.Pbs;
                input.Shared["8263"] = GIPUnpacker.Fa;
                input.Shared["5030"] = GIPUnpacker.Poc;
                #endregion

                #region 805 Lender Inspection Fee
                input.Shared["1000"] = loanData.sInspectF_rep;
                GIPUnpacker.Unpack(loanData.sInspectFProps);
                input.Shared["1140"] = GIPUnpacker.Pfc;
                input.Shared["8224"] = GIPUnpacker.Pbs;
                input.Shared["8274"] = GIPUnpacker.Fa;
                input.Shared["3111"] = GIPUnpacker.Poc;
                #endregion

                #region 808 Mortgage Broker Fee
                input.Shared["969"] = loanData.sMBrokFPc_rep;
                input.Shared["970"] = loanData.sMBrokFMb_rep;
                GIPUnpacker.Unpack(loanData.sMBrokFProps);
                input.Shared["1083"] = GIPUnpacker.Pfc;
                input.Shared["8225"] = GIPUnpacker.Pbs;
                input.Shared["8275"] = GIPUnpacker.Fa;
                input.Shared["3112"] = GIPUnpacker.Poc;
                #endregion

                #region 809 Tax Service Fee
                input.Shared["1025"] = loanData.sTxServF_rep;
                GIPUnpacker.Unpack(loanData.sTxServFProps);
                input.Shared["1084"] = GIPUnpacker.Pfc;
                input.Shared["8226"] = GIPUnpacker.Pbs;
                input.Shared["8276"] = GIPUnpacker.Fa;
                input.Shared["3113"] = GIPUnpacker.Poc;
                #endregion

                #region 810 Processing Fee
                if (loanData.sProcFPaid)
                    input.Shared["5031"] = "X";
                input.Shared["5355"] = loanData.sProcF_rep;
                GIPUnpacker.Unpack(loanData.sProcFProps);
                input.Shared["1065"] = GIPUnpacker.Pfc;
                input.Shared["8227"] = GIPUnpacker.Pbs;
                input.Shared["8277"] = GIPUnpacker.Fa;
                input.Shared["5031"] = GIPUnpacker.Poc;
                #endregion

                #region 811 Underwriting Fee
                input.Shared["5356"] = loanData.sUwF_rep;
                GIPUnpacker.Unpack(loanData.sUwFProps);
                input.Shared["1066"] = GIPUnpacker.Pfc;
                input.Shared["8228"] = GIPUnpacker.Pbs;
                input.Shared["8278"] = GIPUnpacker.Fa;
                input.Shared["3115"] = GIPUnpacker.Poc;
                #endregion

                #region 812 Wire Transfer
                input.Shared["5357"] = loanData.sWireF_rep;
                GIPUnpacker.Unpack(loanData.sWireFProps);
                input.Shared["1141"] = GIPUnpacker.Pfc;
                input.Shared["8229"] = GIPUnpacker.Pbs;
                input.Shared["8279"] = GIPUnpacker.Fa;
                input.Shared["3116"] = GIPUnpacker.Poc;
                #endregion

                #region 800 Custom #1
                input.Shared["978"] = loanData.s800U1FDesc;
                input.Shared["1031"] = loanData.s800U1F_rep;
                GIPUnpacker.Unpack(loanData.s800U1FProps);
                input.Shared["1060"] = GIPUnpacker.Pfc;
                input.Shared["8230"] = GIPUnpacker.Pbs;
                input.Shared["8264"] = GIPUnpacker.Fa;
                input.Shared["3117"] = GIPUnpacker.Poc;
                #endregion

                #region 800 Custom #2
                input.Shared["979"] = loanData.s800U2FDesc;
                input.Shared["1032"] = loanData.s800U2F_rep;
                GIPUnpacker.Unpack(loanData.s800U2FProps);
                input.Shared["1061"] = GIPUnpacker.Pfc;
                input.Shared["8231"] = GIPUnpacker.Pbs;
                input.Shared["8265"] = GIPUnpacker.Fa;
                input.Shared["3118"] = GIPUnpacker.Poc;
                #endregion

                #region 800 Custom #3
                input.Shared["980"] = loanData.s800U3FDesc;
                input.Shared["1033"] = loanData.s800U3F_rep;
                GIPUnpacker.Unpack(loanData.s800U3FProps);
                input.Shared["1062"] = GIPUnpacker.Pfc;
                input.Shared["8232"] = GIPUnpacker.Pbs;
                input.Shared["8266"] = GIPUnpacker.Fa;
                input.Shared["3119"] = GIPUnpacker.Poc;
                #endregion

                #region 800 Custom #4
                input.Shared["984"] = loanData.s800U4FDesc;
                input.Shared["1002"] = loanData.s800U4F_rep;
                GIPUnpacker.Unpack(loanData.s800U4FProps);
                input.Shared["1063"] = GIPUnpacker.Pfc;
                input.Shared["8233"] = GIPUnpacker.Pbs;
                input.Shared["8267"] = GIPUnpacker.Fa;
                input.Shared["3120"] = GIPUnpacker.Poc;
                #endregion

                #region 800 Custom #5
                input.Shared["981"] = loanData.s800U5FDesc;
                input.Shared["1020"] = loanData.s800U5F_rep;
                GIPUnpacker.Unpack(loanData.s800U5FProps);
                input.Shared["1142"] = GIPUnpacker.Pfc;
                input.Shared["2091"] = GIPUnpacker.Pbs;
                input.Shared["2067"] = GIPUnpacker.Fa;

                input.Shared["3152"] = GIPUnpacker.Poc;
                #endregion

                #region 901
                input.Shared["966"] = loanData.sIPiaDy_rep;
                input.Shared["973"] = loanData.sIPerDay_rep;
                GIPUnpacker.Unpack(loanData.sIPiaProps);
                input.Shared["1085"] = GIPUnpacker.Pfc;
                input.Shared["8249"] = GIPUnpacker.Pbs;
                input.Shared["2071"] = GIPUnpacker.Fa;
                input.Shared["3121"] = GIPUnpacker.Poc;
                #endregion

                #region 902 Mortgage Insurance Premium

                GIPUnpacker.Unpack(loanData.sMipPiaProps);
                input.Shared["1086"] = GIPUnpacker.Pfc;
                input.Shared["8250"] = GIPUnpacker.Pbs;
                input.Shared["2072"] = GIPUnpacker.Fa;
                input.Shared["3122"] = GIPUnpacker.Poc;
                #endregion

                #region 903 Hazard Insurance Premium
                input.Shared["404"] = loanData.sHazInsPiaMon_rep;
                GIPUnpacker.Unpack(loanData.sHazInsPiaProps);
                input.Shared["1146"] = GIPUnpacker.Pfc;
                input.Shared["8251"] = GIPUnpacker.Pbs;
                input.Shared["2073"] = GIPUnpacker.Fa;
                input.Shared["3123"] = GIPUnpacker.Poc;
                #endregion

                #region 904
                input.Shared["998"] = loanData.s904PiaDesc;
                input.Shared["5358"] = loanData.s904Pia_rep;
                GIPUnpacker.Unpack(loanData.s904PiaProps);
                input.Shared["1147"] = GIPUnpacker.Pfc;
                input.Shared["8252"] = GIPUnpacker.Pbs;
                input.Shared["2074"] = GIPUnpacker.Fa;
                input.Shared["3124"] = GIPUnpacker.Poc;
                #endregion

                #region 905 VA Funding Fee
                input.Shared["5359"] = loanData.sVaFf_rep;
                GIPUnpacker.Unpack(loanData.sVaFfProps);
                input.Shared["1087"] = GIPUnpacker.Pfc;
                input.Shared["8253"] = GIPUnpacker.Pbs;
                input.Shared["3125"] = GIPUnpacker.Poc;
                #endregion

                #region 900 Custom
                input.Shared["971"] = loanData.s900U1PiaDesc;
                input.Shared["1038"] = loanData.s900U1Pia_rep;
                GIPUnpacker.Unpack(loanData.s900U1PiaProps);
                input.Shared["1064"] = GIPUnpacker.Pfc;
                input.Shared["8254"] = GIPUnpacker.Pbs;
                input.Shared["2076"] = GIPUnpacker.Fa;
                input.Shared["3126"] = GIPUnpacker.Poc;
                #endregion

                #region 1001 Haz ins. reserve
                input.Shared["993"] = loanData.sHazInsRsrvMon_rep;
                GIPUnpacker.Unpack(loanData.sHazInsRsrvProps);
                input.Shared["1149"] = GIPUnpacker.Pfc;
                input.Shared["8255"] = GIPUnpacker.Pbs;
                input.Shared["2077"] = GIPUnpacker.Fa;
                input.Shared["3127"] = GIPUnpacker.Poc;
                #endregion

                #region 1002 Mtg Ins. Reserve
                input.Shared["997"] = loanData.sMInsRsrvMon_rep;
                GIPUnpacker.Unpack(loanData.sMInsRsrvProps);
                input.Shared["1088"] = GIPUnpacker.Pfc;
                input.Shared["8256"] = GIPUnpacker.Pbs;
                input.Shared["2078"] = GIPUnpacker.Fa;
                input.Shared["3128"] = GIPUnpacker.Poc;
                #endregion

                #region 1003 School Tax
                input.Shared["1034"] = loanData.sSchoolTxRsrvMon_rep;
                input.Shared["1035"] = loanData.sProSchoolTx_rep;
                GIPUnpacker.Unpack(loanData.sSchoolTxRsrvProps);
                input.Shared["1150"] = GIPUnpacker.Pfc;
                input.Shared["2095"] = GIPUnpacker.Pbs;
                input.Shared["2079"] = GIPUnpacker.Fa;
                input.Shared["3129"] = GIPUnpacker.Poc;
                #endregion

                #region 1004 Tax reserve
                input.Shared["995"] = loanData.sRealETxRsrvMon_rep;
                GIPUnpacker.Unpack(loanData.sRealETxRsrvProps);
                input.Shared["1151"] = GIPUnpacker.Pfc;
                input.Shared["8257"] = GIPUnpacker.Pbs;
                input.Shared["2080"] = GIPUnpacker.Fa;
                input.Shared["3130"] = GIPUnpacker.Poc;
                #endregion

                #region 1005 Flood Ins Reserve
                input.Shared["1040"] = loanData.sFloodInsRsrvMon_rep;
                input.Shared["1041"] = loanData.sProFloodIns_rep;
                GIPUnpacker.Unpack(loanData.sFloodInsRsrvProps);
                input.Shared["1152"] = GIPUnpacker.Pfc;
                input.Shared["2096"] = GIPUnpacker.Pbs;
                input.Shared["2081"] = GIPUnpacker.Fa;
                input.Shared["3131"] = GIPUnpacker.Poc;
                #endregion

                #region 1006
                input.Shared["743"] = loanData.s1006ProHExpDesc;
                input.Shared["1058"] = loanData.s1006RsrvMon_rep;
                input.Shared["1096"] = loanData.s1006ProHExp_rep;
                GIPUnpacker.Unpack(loanData.s1006RsrvProps);
                input.Shared["1153"] = GIPUnpacker.Pfc;
                input.Shared["2097"] = GIPUnpacker.Pbs;
                input.Shared["2082"] = GIPUnpacker.Fa;
                input.Shared["3132"] = GIPUnpacker.Poc;
                #endregion

                #region 1007
                input.Shared["745"] = loanData.s1007ProHExpDesc;
                input.Shared["1126"] = loanData.s1007RsrvMon_rep;
                input.Shared["1097"] = loanData.s1007ProHExp_rep;
                GIPUnpacker.Unpack(loanData.s1007RsrvProps);
                input.Shared["1154"] = GIPUnpacker.Pfc;
                input.Shared["2098"] = GIPUnpacker.Pbs;
                input.Shared["2083"] = GIPUnpacker.Fa;
                input.Shared["3133"] = GIPUnpacker.Poc;
                #endregion

                #region 1008 Aggregate Adjustment
                GIPUnpacker.Unpack(loanData.sAggregateAdjRsrvProps);
                input.Shared["1155"] = GIPUnpacker.Pfc;
                input.Shared["8258"] = GIPUnpacker.Pbs;
                input.Shared["3134"] = GIPUnpacker.Poc;
                #endregion

                #region 1101 Closing/Escrow Fee
                input.Shared["300"] = loanData.sEscrowFTable;
                input.Shared["1010"] = loanData.sEscrowF_rep;
                GIPUnpacker.Unpack(loanData.sEscrowFProps);
                input.Shared["1067"] = GIPUnpacker.Pfc;
                input.Shared["8234"] = GIPUnpacker.Pbs;
                input.Shared["8268"] = GIPUnpacker.Fa;
                input.Shared["3135"] = GIPUnpacker.Poc;
                #endregion

                #region 1105 Doc Preparation Fee
                input.Shared["1001"] = loanData.sDocPrepF_rep;
                GIPUnpacker.Unpack(loanData.sDocPrepFProps);
                input.Shared["1156"] = GIPUnpacker.Pfc;
                input.Shared["8235"] = GIPUnpacker.Pbs;
                input.Shared["8269"] = GIPUnpacker.Fa;
                input.Shared["3136"] = GIPUnpacker.Poc;
                #endregion

                #region 1106 Notary Fees
                input.Shared["1012"] = loanData.sNotaryF_rep;
                GIPUnpacker.Unpack(loanData.sNotaryFProps);
                input.Shared["1157"] = GIPUnpacker.Pfc;
                input.Shared["8236"] = GIPUnpacker.Pbs;
                input.Shared["8280"] = GIPUnpacker.Fa;
                input.Shared["3137"] = GIPUnpacker.Poc;
                #endregion

                #region 1107 Attorney Fees
                input.Shared["1042"] = loanData.sAttorneyF_rep;
                GIPUnpacker.Unpack(loanData.sAttorneyFProps);
                input.Shared["1090"] = GIPUnpacker.Pfc;
                input.Shared["8237"] = GIPUnpacker.Pbs;
                input.Shared["8281"] = GIPUnpacker.Fa;
                input.Shared["3138"] = GIPUnpacker.Poc;
                #endregion

                #region 1108 Title Insurance
                input.Shared["992"] = loanData.sTitleInsFTable;
                input.Shared["1014"] = loanData.sTitleInsF_rep;
                GIPUnpacker.Unpack(loanData.sTitleInsFProps);
                input.Shared["1158"] = GIPUnpacker.Pfc;
                input.Shared["8238"] = GIPUnpacker.Pbs;
                input.Shared["8270"] = GIPUnpacker.Fa;
                input.Shared["3139"] = GIPUnpacker.Poc;
                #endregion

                #region 11xx Custom #1
                input.Shared["990"] = loanData.sU1TcDesc;
                input.Shared["1016"] = loanData.sU1Tc_rep;
                GIPUnpacker.Unpack(loanData.sU1TcProps);
                input.Shared["1091"] = GIPUnpacker.Pfc;
                input.Shared["8239"] = GIPUnpacker.Pbs;
                input.Shared["8282"] = GIPUnpacker.Fa;
                input.Shared["3140"] = GIPUnpacker.Poc;
                #endregion

                #region 11xx Custom #2
                input.Shared["991"] = loanData.sU2TcDesc;
                input.Shared["1017"] = loanData.sU2Tc_rep;
                GIPUnpacker.Unpack(loanData.sU2TcProps);
                input.Shared["1092"] = GIPUnpacker.Pfc;
                input.Shared["8240"] = GIPUnpacker.Pbs;
                input.Shared["8283"] = GIPUnpacker.Fa;
                input.Shared["3141"] = GIPUnpacker.Poc;
                #endregion

                #region 11xx Custom #3
                input.Shared["1128"] = loanData.sU3TcDesc;
                input.Shared["1129"] = loanData.sU3Tc_rep;
                GIPUnpacker.Unpack(loanData.sU3TcProps);
                input.Shared["1159"] = GIPUnpacker.Pfc;
                input.Shared["2099"] = GIPUnpacker.Pbs;
                input.Shared["2085"] = GIPUnpacker.Fa;
                input.Shared["3156"] = GIPUnpacker.Poc;
                #endregion

                #region 11xx Custom #4
                input.Shared["1130"] = loanData.sU4TcDesc;
                input.Shared["1131"] = loanData.sU4Tc_rep;
                GIPUnpacker.Unpack(loanData.sU4TcProps);
                input.Shared["1160"] = GIPUnpacker.Pfc;
                input.Shared["2100"] = GIPUnpacker.Pbs;
                input.Shared["2086"] = GIPUnpacker.Fa;
                input.Shared["3157"] = GIPUnpacker.Poc;
                #endregion

                #region 1201 Recording Fees
                input.Shared["1015"] = loanData.sRecF_rep;
                GIPUnpacker.Unpack(loanData.sRecFProps);
                input.Shared["1161"] = GIPUnpacker.Pfc;
                input.Shared["8241"] = GIPUnpacker.Pbs;
                input.Shared["8271"] = GIPUnpacker.Fa;
                input.Shared["3142"] = GIPUnpacker.Poc;
                #endregion

                #region 1202 City tax stamps
                GIPUnpacker.Unpack(loanData.sCountyRtcProps);
                input.Shared["1162"] = GIPUnpacker.Pfc;
                input.Shared["8242"] = GIPUnpacker.Pbs;
                input.Shared["8272"] = GIPUnpacker.Fa;
                input.Shared["3143"] = GIPUnpacker.Poc;
                #endregion

                #region 1204 State tax/stamps
                GIPUnpacker.Unpack(loanData.sStateRtcProps);
                input.Shared["1163"] = GIPUnpacker.Pfc;
                input.Shared["8243"] = GIPUnpacker.Pbs;
                input.Shared["8284"] = GIPUnpacker.Fa;
                input.Shared["3144"] = GIPUnpacker.Poc;
                #endregion

                #region 1200 Custom #1
                input.Shared["977"] = loanData.sU1GovRtcDesc;
                GIPUnpacker.Unpack(loanData.sU1GovRtcProps);
                input.Shared["1164"] = GIPUnpacker.Pfc;
                input.Shared["8244"] = GIPUnpacker.Pbs;
                input.Shared["8285"] = GIPUnpacker.Fa;
                input.Shared["3145"] = GIPUnpacker.Poc;
                #endregion

                #region 1200 Custom #2
                input.Shared["1132"] = loanData.sU2GovRtcDesc;
                GIPUnpacker.Unpack(loanData.sU2GovRtcProps);
                input.Shared["1165"] = GIPUnpacker.Pfc;
                input.Shared["2101"] = GIPUnpacker.Pbs;
                input.Shared["2087"] = GIPUnpacker.Fa;
                input.Shared["3146"] = GIPUnpacker.Poc;
                #endregion

                #region 1200 Custom #3
                input.Shared["1134"] = loanData.sU3GovRtcDesc;
                GIPUnpacker.Unpack(loanData.sU3GovRtcProps);
                input.Shared["1166"] = GIPUnpacker.Pfc;
                input.Shared["2102"] = GIPUnpacker.Pbs;
                input.Shared["2088"] = GIPUnpacker.Fa;
                input.Shared["3147"] = GIPUnpacker.Poc;
                #endregion

                #region 1302 Pest Inspection
                input.Shared["1030"] = loanData.sPestInspectF_rep;
                GIPUnpacker.Unpack(loanData.sPestInspectFProps);
                input.Shared["1167"] = GIPUnpacker.Pfc;
                input.Shared["8245"] = GIPUnpacker.Pbs;
                input.Shared["8273"] = GIPUnpacker.Fa;
                input.Shared["3148"] = GIPUnpacker.Poc;
                #endregion

                #region 1300 Custom #1
                input.Shared["986"] = loanData.sU1ScDesc;
                input.Shared["1007"] = loanData.sU1Sc_rep;
                GIPUnpacker.Unpack(loanData.sU1ScProps);
                input.Shared["1093"] = GIPUnpacker.Pfc;
                input.Shared["8246"] = GIPUnpacker.Pbs;
                input.Shared["8286"] = GIPUnpacker.Fa;
                input.Shared["3149"] = GIPUnpacker.Poc;
                #endregion

                #region 1300 Custom #2
                input.Shared["987"] = loanData.sU2ScDesc;
                input.Shared["1008"] = loanData.sU2Sc_rep;
                GIPUnpacker.Unpack(loanData.sU2ScProps);
                input.Shared["1094"] = GIPUnpacker.Pfc;
                input.Shared["8247"] = GIPUnpacker.Pbs;
                input.Shared["8287"] = GIPUnpacker.Fa;
                input.Shared["3150"] = GIPUnpacker.Poc;
                #endregion

                #region 1300 Custom #3
                input.Shared["982"] = loanData.sU3ScDesc;
                input.Shared["1046"] = loanData.sU3Sc_rep;
                GIPUnpacker.Unpack(loanData.sU3ScProps);
                input.Shared["1095"] = GIPUnpacker.Pfc;
                input.Shared["8248"] = GIPUnpacker.Pbs;
                input.Shared["8288"] = GIPUnpacker.Fa;
                input.Shared["3151"] = GIPUnpacker.Poc;
                #endregion

                #region 1300 Custom #4
                input.Shared["1136"] = loanData.sU4ScDesc;
                input.Shared["1137"] = loanData.sU4Sc_rep;
                GIPUnpacker.Unpack(loanData.sU4ScProps);
                input.Shared["1168"] = GIPUnpacker.Pfc;
                input.Shared["2103"] = GIPUnpacker.Pbs;
                input.Shared["2089"] = GIPUnpacker.Fa;
                input.Shared["3158"] = GIPUnpacker.Poc;

                #endregion

                #region 1300 Custom #5
                input.Shared["1138"] = loanData.sU5ScDesc;
                input.Shared["1139"] = loanData.sU5Sc_rep;
                GIPUnpacker.Unpack(loanData.sU5ScProps);
                input.Shared["1169"] = GIPUnpacker.Pfc;
                input.Shared["2104"] = GIPUnpacker.Pbs;
                input.Shared["2090"] = GIPUnpacker.Fa;
                input.Shared["3159"] = GIPUnpacker.Poc;

                #endregion
                input.Shared["5366"] = loanData.sBrokComp1Desc;
                input.Shared["5367"] = loanData.sBrokComp1_rep;
                input.Shared["5368"] = loanData.sBrokComp2Desc;
                input.Shared["5369"] = loanData.sBrokComp2_rep;

                input.Shared["5409"] = loanData.sBrokComp2Mlds_rep;
                input.Shared["5492"] = loanData.sBrokComp1Mlds_rep;

                input.Shared["810"] = loanData.sTotCcPbs_rep;
                if (loanData.sTotCcPbsLocked)
                    input.Shared["832"] = "X";

                input.Shared["5290"] = loanData.sU1FntcDesc;
                input.Shared["5291"] = loanData.sU1Fntc_rep;

                if (loanData.sGfeProvByBrok)
                    input.Shared["1049"] = "X";

                if (loanData.sMultiApps)
                    input.Shared["1180"] = "X";
                input.Shared["1197"] = loanData.sLTODesc;
                input.Shared["901"] = loanData.sAgencyCaseNum;
                input.Shared["327"] = loanData.sLenderCaseNum;

                // 0 - FIX, 1 - ARM, 2 - GPM
                if (loanData.sFinMethodPrintAsOther)
                {
                    // 12/15/2004 dd - Added
                    input.Shared["562"] = "X";
                    input.Shared["563"] = loanData.sFinMethPrintAsOtherDesc;
                }
                else
                {
                    switch (loanData.sFinMethT)
                    {
                        case DataAccess.E_sFinMethT.Fixed:
                            input.Shared["550"] = "X";
                            break;
                        case DataAccess.E_sFinMethT.Graduated:
                            input.Shared["552"] = "X";
                            break;
                        case DataAccess.E_sFinMethT.ARM:
                            input.Shared["560"] = "X";
                            break;
                    }
                    input.Shared["561"] = loanData.sFinMethDesc;
                }

                input.Shared["36"] = loanData.sUnitsNum_rep;
                input.Shared["37"] = loanData.sYrBuilt;
                input.Shared["1206"] = loanData.sSpLegalDesc;	// 1207 is ignored
                input.Shared["1208"] = loanData.sLotAcqYr;
                input.Shared["1209"] = loanData.sLotOrigC_rep;
                input.Shared["1220"] = loanData.sLotLien_rep;
                input.Shared["1210"] = loanData.sLotVal_rep;
                input.Shared["1211"] = loanData.sLotImprovC_rep;
                input.Shared["1212"] = loanData.sSpAcqYr;
                input.Shared["1213"] = loanData.sSpOrigC_rep;
                input.Shared["1214"] = loanData.sSpLien_rep;
                input.Shared["1215"] = loanData.sRefPurpose;
                input.Shared["1219"] = loanData.sSpImprovDesc;
                switch (loanData.sSpImprovTimeFrameT)
                {
                    case E_sSpImprovTimeFrameT.Made:
                        input.Shared["1217"] = "X";
                        break;
                    case E_sSpImprovTimeFrameT.ToBeMade:
                        input.Shared["1218"] = "X";
                        break;
                }
                input.Shared["1221"] = loanData.sSpImprovC_rep;

                input.Shared["1225"] = loanData.sDwnPmtSrc;
                input.Shared["1226"] = loanData.sDwnPmtSrcExplain;

                input.Shared["802"] = loanData.sAltCost_rep;
                input.Shared["803"] = loanData.sLandCost_rep;

                //if (loanData.sRefPdOffAmt1003Lckd)
                input.Shared["830"] = "X";
                input.Shared["804"] = loanData.sRefPdOffAmt1003_rep;

                //if (loanData.sFfUfmip1003Lckd)
                input.Shared["831"] = "X";
                input.Shared["807"] = loanData.sFfUfmip1003_rep; // same as 1028 for us
                //if (loanData.sLDiscnt1003Lckd)
                input.Shared["831"] = "X";
                input.Shared["808"] = loanData.sLDiscnt1003_rep;
                input.Shared["811"] = loanData.sOCredit1Desc;
                input.Shared["812"] = loanData.sOCredit1Amt_rep;
                input.Shared["813"] = loanData.sOCredit2Desc;
                input.Shared["814"] = loanData.sOCredit2Amt_rep;
                input.Shared["815"] = loanData.sOCredit3Desc;
                input.Shared["816"] = loanData.sOCredit3Amt_rep;
                input.Shared["1626"] = loanData.sOCredit4Desc;
                input.Shared["1627"] = loanData.sOCredit4Amt_rep;
                //#if xxxxyy
                input.Shared["11"] = loanData.sLAmtCalc_rep;
                //if (loanData.sTransNetCashLckd)
                input.Shared["834"] = "X";
                input.Shared["820"] = loanData.sTransNetCash_rep;
                //if (loanData.sTotEstCc1003Lckd)
                input.Shared["831"] = "X";
                input.Shared["806"] = loanData.sTotEstCcNoDiscnt1003_rep;
                input.Shared["405"] = loanData.sProMInsR2_rep;

                //statuses
                input.Shared["6075"] = loanData.sEstCloseD_rep;
                input.Shared["6076"] = loanData.sEstCloseN;
                input.Shared["6020"] = loanData.sOpenedD_rep;
                input.Shared["6021"] = loanData.sOpenedN;
                input.Shared["6025"] = loanData.sSubmitD_rep;
                input.Shared["6026"] = loanData.sSubmitN;
                input.Shared["6030"] = loanData.sApprovD_rep;
                input.Shared["6031"] = loanData.sApprovN;
                input.Shared["6035"] = loanData.sDocsD_rep;
                input.Shared["6036"] = loanData.sDocsN;
                input.Shared["6040"] = loanData.sFundD_rep;
                input.Shared["6041"] = loanData.sFundN;
                input.Shared["6070"] = loanData.sRecordedD_rep;
                input.Shared["6071"] = loanData.sRecordedN;
                input.Shared["6055"] = loanData.sClosedD_rep;
                input.Shared["6056"] = loanData.sClosedN;

                input.Shared["6050"] = loanData.sCanceledD_rep;
                input.Shared["6051"] = loanData.sCanceledN;
                input.Shared["6065"] = loanData.sSuspendedD_rep;
                input.Shared["6066"] = loanData.sSuspendedN;
                input.Shared["6045"] = loanData.sRejectD_rep;
                input.Shared["6046"] = loanData.sRejectN;
                input.Shared["11442"] = loanData.sDocsBackD_rep;
                input.Shared["11443"] = loanData.sDocsBackN;

                input.Shared["6084"] = loanData.sU1LStatDesc;
                input.Shared["6080"] = loanData.sU1LStatD_rep;
                input.Shared["6081"] = loanData.sU1LStatN;
                input.Shared["6089"] = loanData.sU2LStatDesc;
                input.Shared["6085"] = loanData.sU2LStatD_rep;
                input.Shared["6086"] = loanData.sU2LStatN;
                input.Shared["6094"] = loanData.sU3LStatDesc;
                input.Shared["6090"] = loanData.sU3LStatD_rep;
                input.Shared["6091"] = loanData.sU3LStatN;
                input.Shared["6099"] = loanData.sU4LStatDesc;
                input.Shared["6095"] = loanData.sU4LStatD_rep;
                input.Shared["6096"] = loanData.sU4LStatN;
                input.Shared["6033"] = loanData.sPrelimRprtOd_rep;
                input.Shared["6301"] = loanData.sPrelimRprtRd_rep;
                input.Shared["6302"] = loanData.sPrelimRprtN;

                input.Shared["6315"] = loanData.sApprRprtOd_rep;
                input.Shared["6316"] = loanData.sApprRprtRd_rep;
                input.Shared["6317"] = loanData.sApprRprtN;
                input.Shared["6320"] = loanData.sTilGfeOd_rep;
                input.Shared["6321"] = loanData.sTilGfeRd_rep;
                input.Shared["6322"] = loanData.sTilGfeN;
                input.Shared["6329"] = loanData.sU1DocStatDesc;
                input.Shared["6325"] = loanData.sU1DocStatOd_rep;
                input.Shared["6326"] = loanData.sU1DocStatRd_rep;
                input.Shared["6327"] = loanData.sU1DocStatN;
                input.Shared["6334"] = loanData.sU2DocStatDesc;
                input.Shared["6330"] = loanData.sU2DocStatOd_rep;
                input.Shared["6331"] = loanData.sU2DocStatRd_rep;
                input.Shared["6332"] = loanData.sU2DocStatN;
                input.Shared["6339"] = loanData.sU3DocStatDesc;
                input.Shared["6335"] = loanData.sU3DocStatOd_rep;
                input.Shared["6336"] = loanData.sU3DocStatRd_rep;
                input.Shared["6337"] = loanData.sU3DocStatN;
                input.Shared["6344"] = loanData.sU4DocStatDesc;
                input.Shared["6340"] = loanData.sU4DocStatOd_rep;
                input.Shared["6341"] = loanData.sU4DocStatRd_rep;
                input.Shared["6342"] = loanData.sU4DocStatN;
                input.Shared["6349"] = loanData.sU5DocStatDesc;
                input.Shared["6345"] = loanData.sU5DocStatOd_rep;
                input.Shared["6346"] = loanData.sU5DocStatRd_rep;
                input.Shared["6347"] = loanData.sU5DocStatN;


                input.Shared["15"] = loanData.sTrNotes;

                //input.Shared["885"] = loanData.sTransmUwerComments.TrimWhitespaceAndBOM() ;	

                string sTransmUwerCommentsStr = loanData.sTransmUwerComments.TrimWhitespaceAndBOM();
                if (sTransmUwerCommentsStr.Length > 0)
                {
                    string[] pieces = Split(sTransmUwerCommentsStr, 180, 8);
                    for (int iPiece = 0; iPiece < pieces.Length; ++iPiece)
                    {
                        input.App[(885 + iPiece).ToString("D")] = pieces[iPiece];
                    }
                }


                switch (loanData.sProHazInsT)
                {
                    case E_PercentBaseT.LoanAmount:
                        input.Shared["412"] = "Loan Amount";
                        break;
                    case E_PercentBaseT.SalesPrice:
                        input.Shared["412"] = "Sales Price";
                        break;
                    case E_PercentBaseT.AppraisalValue:
                        input.Shared["412"] = "Appraisal Val";
                        break;
                }
                switch (loanData.sProMInsT)
                {
                    case E_PercentBaseT.LoanAmount:
                        input.Shared["413"] = "Loan Amount";
                        break;
                    case E_PercentBaseT.SalesPrice:
                        input.Shared["413"] = "Sales Price";
                        break;
                    case E_PercentBaseT.AppraisalValue:
                        input.Shared["413"] = "Appraisal Val";
                        break;
                }

                input.Shared["911"] = loanData.sProjNm;
                input.Shared["49"] = loanData.sNoteD_rep;
                switch (loanData.sMOrigT)
                {
                    case E_sMOrigT.Seller:
                        input.Shared["2710"] = "X";
                        break;
                    case E_sMOrigT.ThirdParty:
                        input.Shared["2711"] = "X";
                        break;
                }

                input.Shared["2752"] = loanData.sMInsCoverPc_rep;
                input.Shared["2750"] = loanData.sMInsCode;
                input.Shared["2751"] = loanData.sMInsCertNum;
                input.Shared["2749"] = loanData.sLTotNumOfB;
                //input.Shared["942"] = loanData.sUwerNm ; // TODO: Pickup Underwritter from rolodex.
                if (loanData.sUseAdjustorCover)
                    input.Shared["2753"] = "X";

                input.Shared["2754"] = loanData.sSpecCode1;
                input.Shared["2755"] = loanData.sSpecCode2;
                input.Shared["2756"] = loanData.sSpecCode3;
                input.Shared["2757"] = loanData.sSpecCode4;
                input.Shared["2758"] = loanData.sSpecCode5;
                input.Shared["2759"] = loanData.sSpecCode6;

                switch (loanData.sQualIRDeriveT)
                {
                    case E_sQualIRDeriveT.NoteRateDependent:
                        decimal rateDelta = loanData.sQualIR - loanData.sNoteIR;
                        if (0 == rateDelta)
                            input.Shared["2760"] = "X";
                        if (rateDelta > 0)
                        {
                            input.Shared["2761"] = "X";
                            input.Shared["2762"] = loanData.m_convertLos.ToRateString(rateDelta);
                        }
                        if (rateDelta < 0)
                        {
                            input.Shared["2763"] = "X";
                            input.Shared["2764"] = loanData.m_convertLos.ToRateString(-1 * rateDelta); ;
                        }
                        break;
                    case E_sQualIRDeriveT.BoughtDownRate:
                        input.Shared["2765"] = "X";
                        break;
                    case E_sQualIRDeriveT.Other:
                        input.Shared["2766"] = "X";
                        break;

                }

                // 8/4/2006 dd - Point use 1245 for importing sFannieSpT
                switch (loanData.sFannieSpT)
                {
                    case E_sFannieSpT.Detached:
                        input.Shared["928"] = "detached";
                        input.Shared["1245"] = "01";
                        break;
                    case E_sFannieSpT.Attached:
                        input.Shared["928"] = "attached";
                        input.Shared["1245"] = "02";
                        break;
                    case E_sFannieSpT.Condo:
                        input.Shared["928"] = "condo";
                        input.Shared["1245"] = "03";
                        break;
                    case E_sFannieSpT.PUD:
                        input.Shared["928"] = "pud";
                        input.Shared["1245"] = "04";
                        break;
                    case E_sFannieSpT.CoOp:
                        input.Shared["928"] = "co-op";
                        input.Shared["1245"] = "05";
                        break;
                    case E_sFannieSpT.HighRiseCondo:
                        input.Shared["928"] = "high rise condo";
                        input.Shared["1245"] = "07";
                        break;
                    case E_sFannieSpT.Manufactured:
                        input.Shared["928"] = "manufactured";
                        input.Shared["1245"] = "08";
                        break;
                    case E_sFannieSpT.DetachedCondo:
                        input.Shared["928"] = "detached condo";
                        input.Shared["1245"] = "09";
                        break;
                    case E_sFannieSpT.ManufacturedCondoPudCoop:
                        input.Shared["928"] = "manufactured home/condo/pud/co-op";
                        input.Shared["1245"] = "10";
                        break;
                }

                switch (loanData.sFannieDocT)
                {
                    case E_sFannieDocT.Alternative:
                        input.Shared["3898"] = "alternative";
                        input.Shared["1246"] = "A";
                        break;
                    case E_sFannieDocT.Full:
                        input.Shared["3898"] = "full";
                        input.Shared["1246"] = "F";
                        break;
                    case E_sFannieDocT.NoDocumentation:
                        input.Shared["3898"] = "no documentation";
                        input.Shared["1246"] = "C";
                        break;
                    case E_sFannieDocT.Reduced:
                        input.Shared["3898"] = "reduced";
                        input.Shared["1246"] = "R";
                        break;
                    case E_sFannieDocT.StreamlinedRefinanced:
                        input.Shared["3898"] = "streamlined refinance";
                        input.Shared["1246"] = "B";
                        break;
                    case E_sFannieDocT.NoRatio:
                        input.Shared["3898"] = "no ratio";
                        input.Shared["1246"] = "D";
                        break;
                    case E_sFannieDocT.LimitedDocumentation:
                        input.Shared["3898"] = "limited documentation";
                        input.Shared["1246"] = "E";
                        break;
                    case E_sFannieDocT.NoIncomeNoAssets:
                        input.Shared["3898"] = "no income and no assets on 1003";
                        input.Shared["1246"] = "G";
                        break;
                    case E_sFannieDocT.NoAssets:
                        input.Shared["3898"] = "no assets on 1003";
                        input.Shared["1246"] = "H";
                        break;
                    case E_sFannieDocT.NoIncomeNoEmployment:
                        input.Shared["3898"] = "no income and no employment on 1003";
                        input.Shared["1246"] = "I";
                        break;
                    case E_sFannieDocT.NoIncome:
                        input.Shared["3898"] = "no income on 1003";
                        input.Shared["1246"] = "J";
                        break;
                    case E_sFannieDocT.NoVerificationStatedIncomeEmploymentAssets:
                        input.Shared["3898"] = "no verification of stated income, employment or assets";
                        input.Shared["1246"] = "K";
                        break;
                    case E_sFannieDocT.NoVerificationStatedIncomeAssets:
                        input.Shared["3898"] = "no verification of state income or assets";
                        input.Shared["1246"] = "L";
                        break;
                    case E_sFannieDocT.NoVerificationStatedAssets:
                        input.Shared["3898"] = "no verification of stated assets";
                        input.Shared["1246"] = "M";
                        break;
                    case E_sFannieDocT.NoVerificationStatedIncomeEmployment:
                        input.Shared["3898"] = "no verification of stated income or employment";
                        input.Shared["1246"] = "N";
                        break;
                    case E_sFannieDocT.NoVerificationStatedIncome:
                        input.Shared["3898"] = "no verification of stated income";
                        input.Shared["1246"] = "O";
                        break;
                    case E_sFannieDocT.VerbalVOE:
                        input.Shared["3898"] = "verbal verification of employment (vvoe)";
                        input.Shared["1246"] = "P";
                        break;
                    case E_sFannieDocT.OnePaystub:
                        input.Shared["3898"] = "one paystub";
                        input.Shared["1246"] = "Q";
                        break;
                    case E_sFannieDocT.OnePaystubAndVerbalVOE:
                        input.Shared["3898"] = "one paystub and vvoe";
                        input.Shared["1246"] = "S";
                        break;
                    case E_sFannieDocT.OnePaystubOneW2VerbalVOE:
                        input.Shared["3898"] = "one paystub and one w-2 and vvoe or one yr 1040";
                        input.Shared["1246"] = "T";
                        break;
                    case E_sFannieDocT.NoIncomeNoEmploymentNoAssets:
                        input.Shared["3898"] = "no income, no employment and no assets on 1003";
                        input.Shared["1246"] = "U";
                        break;
                }

                if (loanData.sBalloonPmt)
                    input.Shared["553"] = "X";

                input.Shared["3892"] = loanData.sFannieARMPlanNum;
                switch (loanData.sArmIndexT)
                {
                    case E_sArmIndexT.WeeklyAvgCMT:
                        input.Shared["3896"] = "weekly average cmt";
                        break;
                    case E_sArmIndexT.MonthlyAvgCMT:
                        input.Shared["3896"] = "monthly average cmt";
                        break;
                    case E_sArmIndexT.WeeklyAvgTAAI:
                        input.Shared["3896"] = "weekly average taai";
                        break;
                    case E_sArmIndexT.WeeklyAvgTAABD:
                        input.Shared["3896"] = "weekly average taabd";
                        break;
                    case E_sArmIndexT.WeeklyAvgSMTI:
                        input.Shared["3896"] = "weekly average smti";
                        break;
                    case E_sArmIndexT.DailyCDRate:
                        input.Shared["3896"] = "daily cd rate";
                        break;
                    case E_sArmIndexT.WeeklyAvgCDRate:
                        input.Shared["3896"] = "weekly average cd rate";
                        break;
                    case E_sArmIndexT.WeeklyAvgPrimeRate:
                        input.Shared["3896"] = "weekly ave prime rate";
                        break;
                    case E_sArmIndexT.TBillDailyValue:
                        input.Shared["3896"] = "t-bill daily value";
                        break;
                    case E_sArmIndexT.EleventhDistrictCOF:
                        input.Shared["3896"] = "11th district cof";
                        break;
                    case E_sArmIndexT.NationalMonthlyMedianCostOfFunds:
                        input.Shared["3896"] = "national monthly median cost of funds";
                        break;
                    case E_sArmIndexT.WallStreetJournalLIBOR:
                        input.Shared["3896"] = "wall street journal libor";
                        break;
                    case E_sArmIndexT.FannieMaeLIBOR:
                        input.Shared["3896"] = "fannie mae libor";
                        break;
                }

                input.Shared["423"] = loanData.sIsSellerProvidedBelowMktFin;
                if (loanData.sWillEscrowBeWaived)
                    input.Shared["9309"] = "X";
                if (loanData.sBuydown)
                    input.Shared["554"] = "X";

                if (loanData.sIfPurchInsFrCreditor)
                    input.Shared["2168"] = "X";

                if (loanData.sIfPurchPropInsFrCreditor)
                    input.Shared["2169"] = "X";

                if (loanData.sIfPurchFloodInsFrCreditor)
                    input.Shared["2170"] = "X";

                input.Shared["2164"] = loanData.sInsFrCreditorAmt_rep;

                // This section must be at the end of this section
                // because the previous assigned data will be needed here.

                //Calculating per month value of insurance.
                input.Shared["753"] = string.Format("{0:f2}", (loanData.sProHazInsR * loanData.sProHazInsBaseAmt) / 1200.00M);
                // OPM 17495 db - Take the value directly from the Point file instead of performing a reverse calculation
                //input.Shared["755"] = string.Format("{0:f2}", (loanData.sProMInsR * loanData.sProMInsBaseAmt) / 1200.00M) ;
                input.Shared["755"] = string.Format("{0:f2}", loanData.sProMIns);
                input.Shared["402"] = loanData.sProMInsR_rep;

                // SHARED CALCULATED FIELDS

                input.Shared["3561"] = loanData.sTotCcPto_rep;
                input.Shared["3562"] = loanData.sTotCcPtb_rep;

                input.Shared["5359"] = loanData.sVaFf_rep;
                input.Shared["821"] = loanData.sTotTransC_rep;
                input.Shared["818"] = loanData.sFfUfmipFinanced_rep;
                input.Shared["1135"] = loanData.sU3GovRtc_rep;
                input.Shared["1133"] = loanData.sU2GovRtc_rep;
                input.Shared["1045"] = loanData.sU1GovRtc_rep;


                /*
                input.Shared["20300"] = loanData.sTransmProRent_rep;
                input.Shared["20304"] = loanData.sTransmProRealETx_rep;
                input.Shared["20302"] = loanData.sTransmProOFin_rep;
                input.Shared["20305"] = loanData.sTransmProMIns_rep;
                input.Shared["20306"] = loanData.sTransmProHoAssocDues_rep;
                input.Shared["20303"] = loanData.sTransmProHazIns_rep;
                input.Shared["20301"] = loanData.sTransmPro1stM_rep;
                input.Shared["20307"] = loanData.sTransmOtherProHExp_rep;
                */
                input.Shared["983"] = loanData.sTotEstSc_rep;
                input.Shared["1053"] = loanData.sTotEstPp_rep;
                //input.Shared["20200"] = loanData.sTotEstFntc_rep;
                input.Shared["1052"] = loanData.sTotEstCc_rep;
                input.Shared["1044"] = loanData.sStateRtc_rep;
                input.Shared["2162"] = loanData.sSpFullAddr;
                input.Shared["1011"] = loanData.sSchoolTxRsrv_rep;
                input.Shared["2197"] = loanData.sSchedPmtTot_rep;
                /*
                input.Shared["20048"] = loanData.sSchedPmtTot_rep; // 2197 and 20048 are mapped to the same value.
                input.Shared["20040"] = loanData.sSchedPmtNum9_rep;
                input.Shared["20035"] = loanData.sSchedPmtNum8_rep;
                input.Shared["20030"] = loanData.sSchedPmtNum7_rep;
                input.Shared["20025"] = loanData.sSchedPmtNum6_rep;
                input.Shared["20020"] = loanData.sSchedPmtNum5_rep;
                input.Shared["20015"] = loanData.sSchedPmtNum4_rep;
                input.Shared["20010"] = loanData.sSchedPmtNum3_rep;
                input.Shared["20005"] = loanData.sSchedPmtNum2_rep;
                //sSchedPmtNum10
                input.Shared["20000"] = loanData.sSchedPmtNum1_rep;
                input.Shared["20043"] = loanData.sSchedPmt9_rep;
                input.Shared["20038"] = loanData.sSchedPmt8_rep;
                input.Shared["20033"] = loanData.sSchedPmt7_rep;
                input.Shared["20028"] = loanData.sSchedPmt6_rep;
                input.Shared["20023"] = loanData.sSchedPmt5_rep;
                input.Shared["20018"] = loanData.sSchedPmt4_rep;
                input.Shared["20013"] = loanData.sSchedPmt3_rep;
                input.Shared["20008"] = loanData.sSchedPmt2_rep;
                //sSchedPmt10
                input.Shared["20003"] = loanData.sSchedPmt1_rep;
                input.Shared["20045"] = loanData.sSchedPmtNumTot_rep;
                input.Shared["20042"] = loanData.sSchedIR9_rep;
                input.Shared["20037"] = loanData.sSchedIR8_rep;
                input.Shared["20032"] = loanData.sSchedIR7_rep;
                input.Shared["20027"] = loanData.sSchedIR6_rep;
                input.Shared["20022"] = loanData.sSchedIR5_rep;
                input.Shared["20017"] = loanData.sSchedIR4_rep;
                input.Shared["20012"] = loanData.sSchedIR3_rep;
                input.Shared["20007"] = loanData.sSchedIR2_rep;
                //sSchedIR10
                input.Shared["20002"] = loanData.sSchedIR1_rep;
                input.Shared["20041"] = loanData.sSchedDueD9_rep;
                input.Shared["20036"] = loanData.sSchedDueD8_rep;
                input.Shared["20031"] = loanData.sSchedDueD7_rep;
                input.Shared["20026"] = loanData.sSchedDueD6_rep;
                input.Shared["20021"] = loanData.sSchedDueD5_rep;
                input.Shared["20016"] = loanData.sSchedDueD4_rep;
                input.Shared["20011"] = loanData.sSchedDueD3_rep;
                input.Shared["20006"] = loanData.sSchedDueD2_rep;
                */
                //sSchedDueD10
                input.Shared["57"] = loanData.sSchedDueD1_rep;
                /*
                input.Shared["20001"] = loanData.sSchedDueD1_rep; // both 57 and 20001 are mapped to the same value.
                input.Shared["20044"] = loanData.sSchedBal9_rep;
                input.Shared["20039"] = loanData.sSchedBal8_rep;
                input.Shared["20034"] = loanData.sSchedBal7_rep;
                input.Shared["20029"] = loanData.sSchedBal6_rep;
                input.Shared["20024"] = loanData.sSchedBal5_rep;
                input.Shared["20019"] = loanData.sSchedBal4_rep;
                input.Shared["20014"] = loanData.sSchedBal3_rep;
                input.Shared["20009"] = loanData.sSchedBal2_rep;
                //sSchedBal10
                input.Shared["20004"] = loanData.sSchedBal1_rep;
                //input.Shared["20053"] = loanData.sReqLTotI_rep; // Reverse calculations are not support 
                */
                if (E_sLPurposeT.Purchase == loanData.sLPurposeT)
                    input.Shared["5371"] = loanData.sPurchPrice_rep;
                else
                    input.Shared["5371"] = loanData.sRefPdOffAmtGfe_rep;

                input.Shared["999"] = loanData.sRealETxRsrv_rep;
                //input.Shared["20330"] = loanData.sQualTopR_rep; //related to 542 which is for one borrower 1003
                //input.Shared["20331"] = loanData.sQualBottomR_rep; // related to 543	which is for one borrower 1003 
                input.Shared["760"] = loanData.sMonthlyPmt_rep;
                input.Shared["752"] = loanData.sProOFinPmt_rep;
                input.Shared["962"] = loanData.sProMIns2_rep;
                input.Shared["755"] = loanData.sProMIns_rep;
                input.Shared["753"] = loanData.sProHazIns_rep;
                /*
                input.Shared["20318"] = loanData.sPrimAppTotSpPosCf_rep;
                input.Shared["20316"] = loanData.sPrimAppTotNonbaseI_rep;
                input.Shared["20319"] = loanData.sPrimAppTotI_rep; 
                input.Shared["20315"] = loanData.sPrimAppTotBaseI_rep;
                input.Shared["20323"] = loanData.sNprimAppTotSpPosCf_rep;
                input.Shared["20321"] = loanData.sNprimAppsTotNonbaseI_rep;
                input.Shared["20324"] = loanData.sNprimAppTotI_rep;
                input.Shared["20320"] = loanData.sNprimAppsTotBaseI_rep;
                */
                input.Shared["527"] = loanData.sProThisMPmt_rep;
                input.Shared["1028"] = loanData.sFfUfmip1003_rep; // same as 807 for us
                input.Shared["1029"] = loanData.sMInsRsrv_rep;
                input.Shared["1026"] = loanData.sMBrokF_rep;
                //input.Shared["20052"] = loanData.sMaxPVal_rep; // Reverse calculations are not support 
                //input.Shared["2001"] = loanData.sMaxLLimit_rep; // Reverse calculations are not support 
                //input.Shared["20050"] = loanData.sMaxLAmt_rep;// Reverse calculations are not support 
                //input.Shared["20054"] = loanData.sMaxAllowedDebt_rep;// Reverse calculations are not support 
                input.Shared["540"] = loanData.sLtvR_rep;
                /*
                input.Shared["20328"] = loanData.sLTotSpPosCf_rep;
                input.Shared["20326"] = loanData.sLTotNonbaseI_rep;
                //input.Shared["20329"] = loanData.sLTotI_rep; TEMPORARILY COMMENTED OUT// TODO-thinh: Watch out for this one, 20329 doesn't show up in Point 4, 881 is incorrectly used in Point instead.
                input.Shared["20325"] = loanData.sLTotBaseI_rep;
                input.Shared["20100"] = loanData.sLotWImprovTot_rep;
                */
                input.Shared["1023"] = loanData.sLOrigF_rep;
                input.Shared["1024"] = loanData.sLDiscnt_rep;
                input.Shared["1027"] = loanData.sIPia_rep;
                input.Shared["1047"] = loanData.sHazInsRsrv_rep;
                input.Shared["1009"] = loanData.sHazInsPia_rep;
                input.Shared["1048"] = loanData.sFloodInsRsrv_rep;
                input.Shared["2195"] = loanData.sFinCharge_rep;
                input.Shared["819"] = loanData.sFinalLAmt_rep;
                input.Shared["524"] = loanData.sDownPmtPc_rep;
                //input.Shared["20051"] = loanData.sDownPmtNeeded_rep;// Reverse calculations are not support 
                input.Shared["1043"] = loanData.sCountyRtc_rep;
                input.Shared["541"] = loanData.sCltvR_rep;
                //input.Shared["20049"] = loanData.sApr_rep;
                //input.Shared["8012"] = loanData.sApr_rep;
                input.Shared["1127"] = loanData.s1007Rsrv_rep;
                input.Shared["1059"] = loanData.s1006Rsrv_rep;
                //#if xxxxyy	
                //input.Shared["881"]	= loanData.sLTotI_rep; 881 is for single 1008 only.

                if (loanData.sIsLicensedLender)
                    input.Shared["11420"] = "X";
                if (loanData.sIsCorrespondentLender)
                    input.Shared["11421"] = "X";
                if (loanData.sIsOtherTypeLender)
                    input.Shared["11422"] = "X";

                input.Shared["11423"] = loanData.sOtherTypeLenderDesc;

                switch (loanData.sRefundabilityOfFeeToLenderT)
                {
                    case E_sRefundabilityOfFeeToLenderT.Nonrefundable: input.Shared["11424"] = "X"; break;
                    case E_sRefundabilityOfFeeToLenderT.Refundable: input.Shared["11425"] = "X"; break;
                    case E_sRefundabilityOfFeeToLenderT.NA: input.Shared["11426"] = "X"; break;
                    case E_sRefundabilityOfFeeToLenderT.Blank:			// fall through
                    default:
                        break;
                }

                input.Shared["11427"] = loanData.sCommitmentEstimateDays;
                input.Shared["6600"] = loanData.sTrust1Desc;
                input.Shared["6601"] = loanData.sTrust1ReceivableAmt_rep;
                input.Shared["6602"] = loanData.sTrust1PayableAmt_rep;
                input.Shared["6603"] = loanData.sTrust1D_rep;
                input.Shared["6604"] = loanData.sTrust1ReceivableChkNum.Value;
                input.Shared["6607"] = loanData.sTrust1PayableChkNum.Value;
                input.Shared["6608"] = loanData.sTrust1ReceivableN;

                input.Shared["6610"] = loanData.sTrust2Desc;
                input.Shared["6611"] = loanData.sTrust2ReceivableAmt_rep;
                input.Shared["6612"] = loanData.sTrust2PayableAmt_rep;
                input.Shared["6613"] = loanData.sTrust2D_rep;
                input.Shared["6614"] = loanData.sTrust2ReceivableChkNum.Value;
                input.Shared["6617"] = loanData.sTrust2PayableChkNum.Value;
                input.Shared["6618"] = loanData.sTrust2ReceivableN;

                input.Shared["6620"] = loanData.sTrust3Desc;
                input.Shared["6621"] = loanData.sTrust3ReceivableAmt_rep;
                input.Shared["6622"] = loanData.sTrust3PayableAmt_rep;
                input.Shared["6623"] = loanData.sTrust3D_rep;
                input.Shared["6624"] = loanData.sTrust3ReceivableChkNum.Value;
                input.Shared["6627"] = loanData.sTrust3PayableChkNum.Value;
                input.Shared["6628"] = loanData.sTrust3ReceivableN;

                input.Shared["6630"] = loanData.sTrust4Desc;
                input.Shared["6631"] = loanData.sTrust4ReceivableAmt_rep;
                input.Shared["6632"] = loanData.sTrust4PayableAmt_rep;
                input.Shared["6633"] = loanData.sTrust4D_rep;
                input.Shared["6634"] = loanData.sTrust4ReceivableChkNum.Value;
                input.Shared["6637"] = loanData.sTrust4PayableChkNum.Value;
                input.Shared["6638"] = loanData.sTrust4ReceivableN;

                input.Shared["6640"] = loanData.sTrust5Desc;
                input.Shared["6641"] = loanData.sTrust5ReceivableAmt_rep;
                input.Shared["6642"] = loanData.sTrust5PayableAmt_rep;
                input.Shared["6643"] = loanData.sTrust5D_rep;
                input.Shared["6644"] = loanData.sTrust5ReceivableChkNum.Value;
                input.Shared["6647"] = loanData.sTrust5PayableChkNum.Value;
                input.Shared["6648"] = loanData.sTrust5ReceivableN;

                input.Shared["6650"] = loanData.sTrust6Desc;
                input.Shared["6651"] = loanData.sTrust6ReceivableAmt_rep;
                input.Shared["6652"] = loanData.sTrust6PayableAmt_rep;
                input.Shared["6653"] = loanData.sTrust6D_rep;
                input.Shared["6654"] = loanData.sTrust6ReceivableChkNum.Value;
                input.Shared["6657"] = loanData.sTrust6PayableChkNum.Value;
                input.Shared["6658"] = loanData.sTrust6ReceivableN;

                input.Shared["6660"] = loanData.sTrust7Desc;
                input.Shared["6661"] = loanData.sTrust7ReceivableAmt_rep;
                input.Shared["6662"] = loanData.sTrust7PayableAmt_rep;
                input.Shared["6663"] = loanData.sTrust7D_rep;
                input.Shared["6664"] = loanData.sTrust7ReceivableChkNum.Value;
                input.Shared["6667"] = loanData.sTrust7PayableChkNum.Value;
                input.Shared["6668"] = loanData.sTrust7ReceivableN;

                input.Shared["6670"] = loanData.sTrust8Desc;
                input.Shared["6671"] = loanData.sTrust8ReceivableAmt_rep;
                input.Shared["6672"] = loanData.sTrust8PayableAmt_rep;
                input.Shared["6673"] = loanData.sTrust8D_rep;
                input.Shared["6674"] = loanData.sTrust8ReceivableChkNum.Value;
                input.Shared["6677"] = loanData.sTrust8PayableChkNum.Value;
                input.Shared["6678"] = loanData.sTrust8ReceivableN;

                input.Shared["7200"] = loanData.sTrust9Desc;
                input.Shared["7201"] = loanData.sTrust9ReceivableAmt_rep;
                input.Shared["7202"] = loanData.sTrust9PayableAmt_rep;
                input.Shared["7203"] = loanData.sTrust9D_rep;
                input.Shared["7204"] = loanData.sTrust9ReceivableChkNum.Value;
                input.Shared["7207"] = loanData.sTrust9PayableChkNum.Value;
                input.Shared["7208"] = loanData.sTrust9ReceivableN;

                input.Shared["7210"] = loanData.sTrust10Desc;
                input.Shared["7211"] = loanData.sTrust10ReceivableAmt_rep;
                input.Shared["7212"] = loanData.sTrust10PayableAmt_rep;
                input.Shared["7213"] = loanData.sTrust10D_rep;
                input.Shared["7214"] = loanData.sTrust10ReceivableChkNum.Value;
                input.Shared["7217"] = loanData.sTrust10PayableChkNum.Value;
                input.Shared["7218"] = loanData.sTrust10ReceivableN;

                input.Shared["7220"] = loanData.sTrust11Desc;
                input.Shared["7221"] = loanData.sTrust11ReceivableAmt_rep;
                input.Shared["7222"] = loanData.sTrust11PayableAmt_rep;
                input.Shared["7223"] = loanData.sTrust11D_rep;
                input.Shared["7224"] = loanData.sTrust11ReceivableChkNum.Value;
                input.Shared["7227"] = loanData.sTrust11PayableChkNum.Value;
                input.Shared["7228"] = loanData.sTrust11ReceivableN;

                input.Shared["7230"] = loanData.sTrust12Desc;
                input.Shared["7231"] = loanData.sTrust12ReceivableAmt_rep;
                input.Shared["7232"] = loanData.sTrust12PayableAmt_rep;
                input.Shared["7233"] = loanData.sTrust12D_rep;
                input.Shared["7234"] = loanData.sTrust12ReceivableChkNum.Value;
                input.Shared["7237"] = loanData.sTrust12PayableChkNum.Value;
                input.Shared["7238"] = loanData.sTrust12ReceivableN;

                input.Shared["7240"] = loanData.sTrust13Desc;
                input.Shared["7241"] = loanData.sTrust13ReceivableAmt_rep;
                input.Shared["7242"] = loanData.sTrust13PayableAmt_rep;
                input.Shared["7243"] = loanData.sTrust13D_rep;
                input.Shared["7244"] = loanData.sTrust13ReceivableChkNum.Value;
                input.Shared["7247"] = loanData.sTrust13PayableChkNum.Value;
                input.Shared["7248"] = loanData.sTrust13ReceivableN;

                input.Shared["7250"] = loanData.sTrust14Desc;
                input.Shared["7251"] = loanData.sTrust14ReceivableAmt_rep;
                input.Shared["7252"] = loanData.sTrust14PayableAmt_rep;
                input.Shared["7253"] = loanData.sTrust14D_rep;
                input.Shared["7254"] = loanData.sTrust14ReceivableChkNum.Value;
                input.Shared["7257"] = loanData.sTrust14PayableChkNum.Value;
                input.Shared["7258"] = loanData.sTrust14ReceivableN;

                input.Shared["7260"] = loanData.sTrust15Desc;
                input.Shared["7261"] = loanData.sTrust15ReceivableAmt_rep;
                input.Shared["7262"] = loanData.sTrust15PayableAmt_rep;
                input.Shared["7263"] = loanData.sTrust15D_rep;
                input.Shared["7264"] = loanData.sTrust15ReceivableChkNum.Value;
                input.Shared["7267"] = loanData.sTrust15PayableChkNum.Value;
                input.Shared["7268"] = loanData.sTrust15ReceivableN;

                input.Shared["7270"] = loanData.sTrust16Desc;
                input.Shared["7271"] = loanData.sTrust16ReceivableAmt_rep;
                input.Shared["7272"] = loanData.sTrust16PayableAmt_rep;
                input.Shared["7273"] = loanData.sTrust16D_rep;
                input.Shared["7274"] = loanData.sTrust16ReceivableChkNum.Value;
                input.Shared["7277"] = loanData.sTrust16PayableChkNum.Value;
                input.Shared["7278"] = loanData.sTrust16ReceivableN;

                input.Shared["6692"] = loanData.sTrustReceivableTotal_rep;
                input.Shared["6691"] = loanData.sTrustPayableTotal_rep;
                input.Shared["6699"] = loanData.sTrustBalance_rep;

                int idCondBase = 6700;
                if (false == broker.IsUseNewTaskSystem)
                {
                    // 12/13/2006 nw - OPM 8410 - Handle the 2 different ways that we store conditions
                    if (principal.HasLenderDefaultFeatures == false)	// conditions stored in XML
                    {
                        int nCond = Math.Min(40, loanData.GetCondRecordCount());
                        for (int iCond = 0; iCond < nCond; ++iCond)
                        {
                            int idCurCond = idCondBase + 2 * iCond;
                            DataAccess.CCondFieldsObsolete condFields = loanData.GetCondFieldsObsolete(iCond);
                            input.Shared[idCurCond.ToString()] = condFields.CondDesc.Substring(0, Math.Min(68, condFields.CondDesc.Length));
                            input.Shared[(idCurCond + 1).ToString()] = condFields.PriorToEventDesc.Substring(0, Math.Min(9, condFields.PriorToEventDesc.Length));
                        }
                    }
                    else	// conditions stored in DB
                    {
                        int idCurCond = idCondBase;
                        CConditionSetObsolete conditions = new CConditionSetObsolete(loanData.sLId);
                        foreach (CConditionObsolete cond in conditions)
                        {
                            input.Shared[idCurCond.ToString()] = cond.CondDesc.Substring(0, Math.Min(68, cond.CondDesc.Length));
                            input.Shared[(idCurCond + 1).ToString()] = cond.CondCategoryDesc.Substring(0, Math.Min(9, cond.CondCategoryDesc.Length));
                            idCurCond += 2;
                            if (idCurCond >= 6780)	// Point only supports up to 40 conditions
                                break;
                        }
                    }
                }
                else
                {
                    int idCurCond = idCondBase;
                    List<Task> conditions = Task.GetActiveConditionsByLoanId(broker.BrokerID, loanData.sLId, false);

                    foreach (Task cond in conditions)
                    {
                        input.Shared[idCurCond.ToString()] = cond.TaskSubject.Substring(0, Math.Min(68, cond.TaskSubject.Length));
                        input.Shared[(idCurCond + 1).ToString()] = cond.CondCategoryId_rep.Substring(0, Math.Min(9, cond.CondCategoryId_rep.Length));
                        idCurCond += 2;
                        if (idCurCond >= 6780)	// Point only supports up to 40 conditions
                            break;
                    }
                }

                // FHA FIELDS
                input.Shared["8059"] = loanData.sFHAPurchPrice_rep;
                input.Shared["7940"] = FromBoolToXYes(loanData.sFHACondCommUnderNationalHousingAct);
                input.Shared["7948"] = loanData.sFHACondCommNationalHousingActSection;
                input.Shared["7941"] = FromBoolToXYes(loanData.sFHACondCommSeeBelow);
                input.Shared["7873"] = loanData.sFHACondCommInstCaseRef;
                input.Shared["7946"] = loanData.sFHACondCommIssuedD_rep;
                input.Shared["7947"] = loanData.sFHACondCommExpiredD_rep;
                input.Shared["7949"] = loanData.sFHACondCommImprovedLivingArea;
                input.Shared["7950"] = loanData.sFHACondCommCondoComExp_rep;
                input.Shared["7951"] = loanData.sFHACondCommMonExpenseEstimate_rep;
                input.Shared["7954"] = loanData.sFHACondCommRemainEconLife;

                m_triStateParser.Parse(loanData.sFHACondCommMaxFinanceEligibleTri);
                input.Shared["7955"] = m_triStateParser.xYes;
                input.Shared["7956"] = m_triStateParser.xNo;

                input.Shared["7957"] = FromBoolToXYes(loanData.sFHACondCommIsManufacturedHousing);
                input.Shared["7959"] = FromBoolToXYes(loanData.sFHACondCommIsSection221d);
                input.Shared["7967"] = loanData.sFHACondCommSection221dAmt_rep;
                input.Shared["7960"] = FromBoolToXYes(loanData.sFHACondCommIsAssuranceOfCompletion);
                input.Shared["7968"] = loanData.sFHACondCommAssuranceOfCompletionAmt_rep;
                input.Shared["7961"] = FromBoolToXYes(loanData.sFHACondCommSeeAttached);
                input.Shared["7966"] = loanData.sFHACondCommSeeAttachedDesc;
                input.Shared["7962"] = FromBoolToXYes(loanData.sFHACondCommSeeFollowingCondsOnBack);
                input.Shared["7990"] = loanData.sFHACondCommCondsOnBack1;
                input.Shared["7991"] = loanData.sFHACondCommCondsOnBack2;
                input.Shared["7992"] = loanData.sFHACondCommCondsOnBack3;
                input.Shared["7993"] = loanData.sFHACondCommCondsOnBack4;
                input.Shared["7994"] = loanData.sFHACondCommCondsOnBack5;
                input.Shared["7995"] = loanData.sFHACondCommCondsOnBack6;
                input.Shared["7996"] = loanData.sFHACondCommCondsOnBack7;

                input.Shared["8440"] = loanData.sFHAInsEndorseProgId;
                input.Shared["8441"] = FromTriStateToXYes(loanData.sFHAInsEndorseInclSolarWindHeater);
                input.Shared["941"] = FromBoolToXYes(loanData.sFHAInsEndorseWillBorrBeOccupant);
                input.Shared["8442"] = FromBoolToXYes(loanData.sFHAInsEndorseWillBorrBeLandlord);
                input.Shared["8443"] = FromBoolToXYes(loanData.sFHAInsEndorseWillBorrBeCorp);
                //--Exist: sFHAIsNonProfit --925 (Point bug on this form, it shows 924 on the screen
                //--Exist: sFHAIsGovAgency --926
                //--sFHAIsEndorseIsGovAgencyOrNonProfit == sFHAIsNonProfit | sFHAIsGovAgency, --?? for the print out
                input.Shared["8444"] = PointDataConversion.FromTriStateToXYes(loanData.sFHAInsEndorseRepairEscrowTri);
                input.Shared["8445"] = loanData.sFHAInsEndorseRepairCompleteD;
                input.Shared["8446"] = loanData.sFHAInsEndorseRepairEscrowAmt_rep;
                switch (loanData.sFHAInsEndorseCounselingT)
                {
                    case E_sFHAInsEndorseCounselingT.Lender: input.Shared["8448"] = "Lender"; break;
                    case E_sFHAInsEndorseCounselingT.None: input.Shared["8448"] = "None"; break;
                    case E_sFHAInsEndorseCounselingT.ThirdParty: input.Shared["8448"] = "Third Party"; break;
                    case E_sFHAInsEndorseCounselingT.Blank: input.Shared["8448"] = ""; break;
                }

                input.Shared["8584"] = loanData.sFHAInsEndorseOrigMortgageeId;
                input.Shared["8585"] = loanData.sFHAInsEndorseAgentNum;
                input.Shared["8588"] = loanData.sFHAInsEndorseMMaturityDate;
                input.Shared["8589"] = loanData.sFHAInsEndorseWarrantyEnrollNum;
                input.Shared["8590"] = loanData.sFHAInsEndorseAmortPlanCode;
                input.Shared["8592"] = loanData.sFHAInsEndorseConstructionCode;
                input.Shared["8593"] = loanData.sFHAInsEndorseLivingUnits;
                input.Shared["8447"] = FromTriStateToXYes(loanData.sFHAInsEndorseExemptFromSSNTri);
                input.Shared["8582"] = FromTriStateToXYes(loanData.sFHAInsEndorseVeteranPrefTri);
                input.Shared["8583"] = FromTriStateToXYes(loanData.sFHAInsEndorseEnergyEffMTri);
                input.Shared["8586"] = FromTriStateToXYes(loanData.sFHAInsEndorseIssueMicInSponsorNmTri);
                input.Shared["8587"] = FromTriStateToXYes(loanData.sFHAInsEndorseMailToSponsorTri);
                input.Shared["8591"] = FromTriStateToXYes(loanData.sFHAInsEndorseCurrentPmtTri);
                input.Shared["8594"] = FromTriStateToXYes(loanData.sFHAInsEndorseUfmipFinancedTri);

                m_triStateParser.Parse(loanData.sFHAApprIsFairTri);
                input.Shared["8081"] = m_triStateParser.xYes;
                input.Shared["8082"] = loanData.sFHAApprNotFairExplanation;
                input.Shared["8083"] = loanData.sFHAApprQualityComment;
                m_triStateParser.Parse(loanData.sFHAApprAreComparablesAcceptableTri);
                input.Shared["8084"] = m_triStateParser.xYes;
                input.Shared["8085"] = loanData.sFHAApprComparablesComment;
                m_triStateParser.Parse(loanData.sFHAApprAdjAcceptableTri);
                input.Shared["8086"] = m_triStateParser.xYes;
                input.Shared["8087"] = loanData.sFHAApprAdjNotAcceptableExplanation;
                m_triStateParser.Parse(loanData.sFHAApprIsValAcceptableTri);
                input.Shared["8088"] = m_triStateParser.xYes;
                m_triStateParser.Parse(loanData.sFHAApprValNeedCorrectionTri);
                input.Shared["8089"] = m_triStateParser.xYes;
                input.Shared["8090"] = loanData.sFHAApprCorrectedVal_rep;
                input.Shared["8091"] = loanData.sFHAApprValCorrectionJustification;
                input.Shared["8092"] = loanData.sFHAApprRepairConditions;
                input.Shared["8093"] = loanData.sFHAApprOtherComments;
                input.Shared["7549"] = loanData.sFHALDiscnt_rep;
                input.Shared["7573"] = loanData.sFHAProMInsMon_rep;
                input.Shared["7588"] = loanData.sFHALenderIdCode;
                input.Shared["7943"] = loanData.sFHALenderIdCode;
                input.Shared["7589"] = loanData.sFHASponsorAgentIdCode;
                input.Shared["7944"] = loanData.sFHASponsorAgentIdCode;
                input.Shared["7600"] = loanData.sFHAPurposeIsPurchaseExistHome ? "X" : "";
                input.Shared["7601"] = loanData.sFHAPurposeIsFinanceImprovement ? "X" : "";
                input.Shared["7602"] = loanData.sFHAPurposeIsRefinance ? "X" : "";
                input.Shared["7603"] = loanData.sFHAPurposeIsPurchaseNewCondo ? "X" : "";
                input.Shared["7604"] = loanData.sFHAPurposeIsPurchaseExistCondo ? "X" : "";
                input.Shared["7605"] = loanData.sFHAPurposeIsPurchaseNewHome ? "X" : "";
                input.Shared["7606"] = loanData.sFHAPurposeIsConstructHome ? "X" : "";
                input.Shared["7607"] = loanData.sFHAPurposeIsFinanceCoopPurchase ? "X" : "";
                input.Shared["7608"] = loanData.sFHAPurposeIsPurchaseManufacturedHome ? "X" : "";
                input.Shared["7609"] = loanData.sFHAPurposeIsManufacturedHomeAndLot ? "X" : "";
                input.Shared["7610"] = loanData.sFHAPurposeIsRefiManufacturedHomeToBuyLot ? "X" : "";
                input.Shared["7611"] = loanData.sFHAPurposeIsRefiManufacturedHomeOrLotLoan ? "X" : "";


                input.Shared["7614"] = loanData.sFHARefinanceTypeDesc;
                input.Shared["7650"] = loanData.sFHAPro1stMPmt_rep;
                input.Shared["7651"] = loanData.sFHAProMIns_rep;
                input.Shared["7652"] = loanData.sFHAProHoAssocDues_rep;
                input.Shared["7653"] = loanData.sFHAProGroundRent_rep;
                input.Shared["7654"] = loanData.sFHAPro2ndFinPmt_rep;
                input.Shared["7655"] = loanData.sFHAProHazIns_rep;
                input.Shared["7656"] = loanData.sFHAProRealETx_rep;

                input.Shared["8100"] = "";
                input.Shared["8101"] = "";
                switch (loanData.sFHACommitStageT)
                {
                    default:
                    case E_sFHACommitStageT.Blank:
                        break;
                    case E_sFHACommitStageT.Conditional:
                        input.Shared["8100"] = "X"; break;
                    case E_sFHACommitStageT.Firm:
                        input.Shared["8101"] = "X"; break;
                }

                input.Shared["926"] = loanData.sFHAIsGovAgency ? "X" : "";
                input.Shared["925"] = loanData.sFHAIsNonProfit ? "X" : "";
                input.Shared["8108"] = loanData.sFHAIsEscrowCommitment ? "X" : "";
                input.Shared["8109"] = loanData.sFHAIsExistingDebt ? "X" : "";
                input.Shared["8102"] = loanData.sFHASpAsIsVal_rep;
                input.Shared["8103"] = loanData.sFHASpAfterImprovedVal_rep;
                input.Shared["8104"] = loanData.sFHASpAfterImprovedVal110pc_rep;
                input.Shared["8105"] = loanData.sFHA203kBorPdCc_rep;
                input.Shared["8163"] = loanData.sFHA203kAllowEnergyImprovement_rep;
                input.Shared["8110"] = loanData.sFHA203kRepairCost_rep;
                input.Shared["8140"] = loanData.sFHA203kRepairCostReserveR_rep;
                input.Shared["8111"] = loanData.sFHA203kRepairCostReserve_rep;
                input.Shared["8141"] = loanData.sFHA203kInspectCount_rep;
                input.Shared["8142"] = loanData.sFHA203kCostPerInspect_rep;
                input.Shared["8143"] = loanData.sFHA203kTitleDrawCount_rep;
                input.Shared["8144"] = loanData.sFHA203kCostPerTitleDraw_rep;
                input.Shared["8112"] = loanData.sFHA203kInspectAndTitleFee_rep;
                input.Shared["8145"] = loanData.sFHA203kMonhtlyPmtEscrowed_rep;
                input.Shared["8146"] = loanData.sFHA203kPmtEscrowedMonths_rep;
                input.Shared["8113"] = loanData.sFHA203kPmtEscrowed_rep;
                input.Shared["8114"] = loanData.sFHA203kRehabEscrowAccountSubtot_rep;
                input.Shared["8115"] = loanData.sFHA203kEngineeringFee_rep;
                input.Shared["8116"] = loanData.sFHA203kConsultantFee_rep;
                input.Shared["8117"] = loanData.sFHA203kOtherFee_rep;
                input.Shared["8147"] = loanData.sFHA203kPlanReviewerMiles_rep;
                input.Shared["8148"] = loanData.sFHA203kPlanReviewerCostPerMile_rep;
                input.Shared["8118"] = loanData.sFHA203kPlanReviewerFee_rep;
                input.Shared["8119"] = loanData.sFHA203kB5toB9Subtot_rep;
                input.Shared["8120"] = loanData.sFHA203kSupplementOrigFee_rep;
                input.Shared["8149"] = loanData.sFHA203kRepairDiscountFeePt_rep;
                input.Shared["8121"] = loanData.sFHA203kRepairDiscount_rep;
                input.Shared["8122"] = loanData.sFHA203kReleaseAtClosingSubtot_rep;
                input.Shared["8123"] = loanData.sFHA203kRehabCostTot_rep;
                input.Shared["8124"] = loanData.sFHA203kSalesPriceOrAsIsVal_rep;
                //--sFHA203kC1PlusC2 money 20470 watch out don't export
                input.Shared["8126"] = loanData.sFHA203kC1PlusC2OrA4_rep;
                input.Shared["8127"] = loanData.sFHA203kInvestmentRequired_rep;
                input.Shared["8128"] = loanData.sFHA203kMaxMAmtC5_rep;
                if (loanData.sFHA203kMaxMAmtC5Lckd)
                    input.Shared["8076"] = "X";
                input.Shared["8131"] = loanData.sFHA203kC5Part1_rep;
                input.Shared["8152"] = loanData.sFHA203kC5Part2_rep;
                input.Shared["8137"] = loanData.sFHA203kActualCashInvRequired_rep;
                if (loanData.sFHA203kActualCashInvRequiredLckd)
                    input.Shared["8075"] = "X";
                input.Shared["8166"] = loanData.sFHA203kAdjMaxMAmt_rep;
                input.Shared["8154"] = loanData.sFHA203kFHAMipRefund_rep;
                input.Shared["8129"] = loanData.sFHA203kD1_rep;
                if (loanData.sFHA203kD1Lckd)
                    input.Shared["8074"] = "X";
                //--sFHA203kA2PlusB14 money  20472
                input.Shared["8130"] = loanData.sFHA203kD2_rep;
                //--sFHA203kD2PlusD3 money 20473
                input.Shared["8132"] = loanData.sFHA203kD4_rep;
                input.Shared["8133"] = loanData.sFHA203kMaxMAmtD5_rep;
                if (loanData.sFHA203kMaxMAmtD5Lckd)
                    input.Shared["8077"] = "X";
                input.Shared["8138"] = loanData.sFHA203kBorrRequiredInv_rep;
                input.Shared["8134"] = loanData.sFHA203kE1_rep;
                loanData.sFHA203kE1Lckd = "X" == input.Shared["8078"];
                //--sFHA203kE2 money 20474
                input.Shared["8136"] = loanData.sFHA203kEscrowCommitmentTot_rep;
                input.Shared["8125"] = loanData.sFHA203kE4Part1_rep;
                input.Shared["8138"] = loanData.sFHA203kE4Part2_rep;
                //--sFHA203kE4 money 20475
                input.Shared["8164"] = loanData.sFHA203kEnergyEfficientMAmt_rep;
                string[] remarks = Split(loanData.sFHA203kRemarks, 180, 2);
                //8155, 8156
                for (int i = 0; i < remarks.Length; ++i)
                {
                    input.Shared[(8155 + i).ToString()] = remarks[i];
                }
                input.Shared["8165"] = loanData.sFHA203kEscrowedFundsTot_rep;
                switch (loanData.sFHA203kBorrAcknowledgeT)
                {
                    default:
                    case E_sFHA203kBorrAcknowledgeT.Blank: break;
                    case E_sFHA203kBorrAcknowledgeT.Other: input.Shared["8150"] = "X"; break;
                    case E_sFHA203kBorrAcknowledgeT.ToBorrower: input.Shared["8135"] = "X"; break;
                    case E_sFHA203kBorrAcknowledgeT.ToPrinciple: input.Shared["8139"] = "X"; break;
                }
                input.Shared["8161"] = loanData.sFHA203kBorrAcknowledgeDesc;


                input.Shared["7522"] = loanData.sUfCashPd_rep;
                input.Shared["7687"] = loanData.sFfUfmipR_rep;
                input.Shared["7501"] = loanData.sFHAHousingActSection;

                switch (loanData.sFHAConstructionT)
                {
                    case E_sFHAConstructionT.Blank:
                        break; //no-op
                    case E_sFHAConstructionT.New:
                    case E_sFHAConstructionT.Existing:
                        input.Shared["7502"] = "X"; break;
                    case E_sFHAConstructionT.Proposed:
                        input.Shared["7503"] = "X"; break;
                    default:
                        throw new CBaseException(ErrorMessages.Generic, "Unhandled enum value for sFHAConstructionT.");
                }

                //#if xxxxyy
                input.Shared["7670"] = loanData.sFHACcTot_rep;
                input.Shared["7671"] = loanData.sFHACcPbs_rep;
                input.Shared["8063"] = loanData.sFHAReqAdj_rep;
                input.Shared["8065"] = loanData.sFHALtv_rep; // should be the same as 8080
                input.Shared["8080"] = loanData.sFHALtvPurch_rep;
                input.Shared["8073"] = loanData.sFHALtvLckd ? "X" : "";
                input.Shared["7521"] = loanData.sFHAPrepaidExp_rep;
                input.Shared["7520"] = loanData.sFHADiscountPoints_rep;
                input.Shared["7519"] = loanData.sFHAImprovements_rep;
                input.Shared["7523"] = loanData.sFHANonrealty_rep;
                input.Shared["7612"] = loanData.sFHANonrealtyLckd ? "X" : "";

                input.Shared["7524"] = loanData.sFHAAmtPaid_rep;
                input.Shared["8071"] = loanData.sFHA2ndMSrc;
                input.Shared["7528"] = loanData.sFHA2ndMAmt_rep;
                input.Shared["7553"] = loanData.sFHASellerContribution_rep;
                input.Shared["7554"] = loanData.sFHAExcessContribution_rep;
                input.Shared["7568"] = loanData.sFHAGiftAmtTot_rep;

                input.Shared["7672"] = loanData.sFHACcPbb_rep;
                input.Shared["2438"] = loanData.sBuydownResultIR_rep;
                input.Shared["8060"] = loanData.sFHAUnadjAcquisition_rep;
                input.Shared["8061"] = loanData.sFHAStatutoryInvestReq_rep;
                input.Shared["8062"] = loanData.sFHAHouseVal_rep;
                input.Shared["8064"] = loanData.sFHAMBasis_rep;
                input.Shared["8066"] = loanData.sFHAMAmt_rep;
                input.Shared["8067"] = loanData.sFHAMinDownPmt_rep;
                input.Shared["8068"] = loanData.sFHATotCashToClose_rep;
                input.Shared["8072"] = loanData.sFHACashReserves_rep;
                //input.Shared["20469"]			= loanData.sFHACashReserves_rep; // the combined version of 8072
                input.Shared["7665"] = loanData.sFHAGrossMonI_rep;
                //input.Shared["20443"]			= loanData.sFHAGrossMonI_rep; // the combined versin of 7665
                input.Shared["7534"] = loanData.sFHADebtPmtTot_rep;
                input.Shared["7657"] = loanData.sFHAMonthlyPmt_rep;
                //input.Shared["20457"]			= loanData.sFHAMonthlyPmt_rep; // the combined version of 20457
                input.Shared["7658"] = loanData.sFHAPmtFixedTot_rep;
                //input.Shared["20458"]			= loanData.sFHAPmtFixedTot_rep; // the combined version of 20458
                //input.Shared["20347"]			= loanData.sFHAMPmtToIRatio_rep;
                //input.Shared["20348"]			= loanData.sFHAFixedPmtToIRatio_rep; 
                input.Shared["7552"] = loanData.sPurchPrice6pc_rep;

                /*
                StringBuilder strBuilder = new StringBuilder( 100 );
                for( int iField = 7555; iField <= 7564; ++iField )
                {
                    string val = input.Shared[iField.ToString()];
                    if( "" != val )
                        strBuilder.Append( val );
                }
                loanData.sFHACreditAnalysisRemarks = strBuilder.ToString();		
                */
                //input.Shared["7555"] = loanData.sFHACreditAnalysisRemarks;

                string sFHACreditAnalysisRemarksStr = loanData.sFHACreditAnalysisRemarks.TrimWhitespaceAndBOM();
                if (sFHACreditAnalysisRemarksStr.Length > 0)
                {
                    string[] pieces = Split(sFHACreditAnalysisRemarksStr, 180, 10);
                    for (int iPiece = 0; iPiece < pieces.Length; ++iPiece)
                    {
                        input.App[(7555 + iPiece).ToString("D")] = pieces[iPiece];
                    }
                }


                input.Shared["7618"] = loanData.sFHA203kRehabCost_rep;
                input.Shared["7615"] = loanData.sFHAReqCashInv_rep;
                input.Shared["7529"] = loanData.sFHAExistingMLien_rep;
                input.Shared["7676"] = loanData.sFHASalesConcessionsDesc;
                input.Shared["7675"] = loanData.sFHASalesConcessions_rep;
                input.Shared["7537"] = loanData.sFHAMBasisRefin_rep;
                input.Shared["7514"] = loanData.sFHAMBasisRefinMultiply_rep;
                input.Shared["7516"] = loanData.sFHAMBasisRefinMultiplyLckd ? "X" : "";
                input.Shared["7515"] = loanData.sFHAApprValMultiply_rep;
                input.Shared["7517"] = loanData.sFHAApprValMultiplyLckd ? "X" : "";
                input.Shared["7510"] = loanData.sFHAIsAmtPdInCash ? "X" : "";
                input.Shared["7511"] = loanData.sFHAIsAmtPdInOther ? "X" : "";
                input.Shared["7512"] = loanData.sFHAIsAmtToBePdInCash ? "X" : "";
                input.Shared["7513"] = loanData.sFHAIsAmtToBePdInOther ? "X" : "";
                input.Shared["7525"] = loanData.sFHAAmtToBePd_rep;
                input.Shared["7518"] = loanData.sFHAImprovementsDesc;
                //input.Shared["20345"]						= loanData.sFHAReqTot_rep;
                input.Shared["7536"] = loanData.sFHAReqInvestment_rep;
                //#if xxxxyy
                // HMDA fields
                if (E_sHmdaPreapprovalT.LeaveBlank == loanData.sHmdaPreapprovalT)
                    input.Shared["6280"] = "";
                else
                    input.Shared["6280"] = loanData.sHmdaPreapprovalT.ToString("D");

                input.Shared["6281"] = loanData.sHmdaAprRateSpread;
                loanData.sHmdaReportAsHoepaLoan = "X" == input.Shared["6282"];

                if (E_sHmdaLienT.LeaveBlank == loanData.sHmdaLienT)
                    input.Shared["6283"] = "";
                else
                    input.Shared["6283"] = loanData.sHmdaLienT.ToString("D");

                if (E_sHmdaPropT.LeaveBlank == loanData.sHmdaPropT)
                    input.Shared["6284"] = "";
                else
                    input.Shared["6284"] = loanData.sHmdaPropT.ToString("D");

                if (E_sHmdaPurchaser2004T.LeaveBlank == loanData.sHmdaPurchaser2004T)
                    input.Shared["6285"] = "";
                else
                {
                    int n6285 = (int)loanData.sHmdaPurchaser2004T;
                    input.Shared["6285"] = (n6285 - 1).ToString();
                }

                if (loanData.sHmdaExcludedFromReport)
                    input.Shared["6223"] = "X";
                if (loanData.sHmdaReportAsHomeImprov)
                    input.Shared["6228"] = "X";
                input.Shared["6210"] = loanData.sHmdaMsaNum;
                input.Shared["6211"] = loanData.sHmdaCountyCode;
                input.Shared["6212"] = loanData.sHmdaCensusTract;
                input.Shared["6214"] = loanData.sHmdaPurchaser;
                input.Shared["6216"] = loanData.sHmdaActionTaken;
                input.Shared["6217"] = loanData.sHmdaActionD_rep;
                input.Shared["6215"] = loanData.sHmdaDenialReason1;
                input.Shared["6218"] = loanData.sHmdaDenialReason2;
                input.Shared["6219"] = loanData.sHmdaDenialReason3;
                if (loanData.sHmdaLoanDenied)
                    input.Shared["6047"] = "X";
                if (loanData.sHmdaDeniedFormDone)
                    input.Shared["6271"] = "X";
                if (loanData.sHmdaCounterOfferMade)
                    input.Shared["6276"] = "X";
                input.Shared["6048"] = loanData.sHmdaLoanDeniedBy;
                input.Shared["3840"] = loanData.sHmdaDeniedFormDoneD_rep;
                input.Shared["6272"] = loanData.sHmdaDeniedFormDoneBy;
                input.Shared["6275"] = loanData.sHmdaCounterOfferMadeD_rep;
                input.Shared["6277"] = loanData.sHmdaCounterOfferMadeBy;
                input.Shared["6278"] = loanData.sHmdaCounterOfferDetails;

                input.Shared["7403"] = loanData.sLpTemplateNm;
                input.Shared["4009"] = loanData.sLpTemplateNm;
                input.Shared["7404"] = loanData.sCcTemplateNm;

                input.Shared["6061"] = loanData.sRLckdD_rep;
                input.Shared["6063"] = loanData.sRLckdExpiredD_rep;

                input.Shared["6170"] = loanData.sProfitGrossBorPnt_rep;
                input.Shared["6171"] = loanData.sProfitGrossBorMb_rep;
                input.Shared["6169"] = loanData.sProfitGrossBor_rep;

                input.Shared["6064"] = loanData.sProfitGrossLenderPnt_rep;
                input.Shared["6176"] = loanData.sProfitGrossLenderMb_rep;
                input.Shared["6168"] = loanData.sProfitGrossLender_rep;


                input.Shared["6172"] = loanData.sGrossProfit_rep;
                input.Shared["6177"] = loanData.sCommissionTotalPointOfLoanAmount_rep;
                input.Shared["6179"] = loanData.sCommissionTotalPointOfGrossProfit_rep;
                input.Shared["6175"] = loanData.sCommissionTotalMb_rep;

                // 3/9/2010 dd - We are now protecting field sCommissionTotal_rep, sNetProfit_rep.
                // If user does not have permisison to read finance folder and closing folder then we do not export commission info.

                if (principal.HasPermission(Permission.AllowAccountantRead) && principal.HasPermission(Permission.AllowCloserRead))
                {
                    input.Shared["6173"] = loanData.sCommissionTotal_rep;
                    input.Shared["6174"] = loanData.sNetProfit_rep;
                }

                /*
                Commission
                CommissionPointOfGrossProfit
                CommissionPointOfLoanAmount
                CommissionMinBase
                InvestorSoldDate
                InvestorBasisPoints
                 */
                input.Shared["3154"] = loanData.sTransmTotMonPmt_rep;

                //#region LOAN SUBMISSION
                input.Shared["4011"] = loanData.sSubmitBrokerFax;
                input.Shared["4010"] = loanData.sSubmitProgramCode;

                input.Shared["4000"] = loanData.sSubmitPropTDesc;
                if (loanData.sSubmitDocFull)
                    input.Shared["4001"] = "X";
                if (loanData.sSubmitDocOther)
                    input.Shared["4002"] = "X";


                if (loanData.sSubmitImpoundTaxes)
                    input.Shared["4003"] = "X";
                if (loanData.sSubmitImpoundHazard)
                    input.Shared["4004"] = "X";
                if (loanData.sSubmitImpoundMI)
                    input.Shared["4005"] = "X";
                if (loanData.sSubmitImpoundFlood)
                    input.Shared["4006"] = "X";
                if (loanData.sSubmitImpoundOther)
                    input.Shared["4007"] = "X";
                input.Shared["4008"] = loanData.sSubmitImpoundOtherDesc;
                input.Shared["4013"] = loanData.sSubmitBuydownDesc;
                input.Shared["4012"] = loanData.sSubmitAmortDesc;
                input.Shared["4014"] = loanData.sSubmitRAdjustOtherDesc;
                input.Shared["2326"] = loanData.sSubmitInitPmtCapR_rep;
                if (loanData.sRLckdIsLocked)
                {
                    input.Shared["6101"] = "X";
                }
                else
                {
                    input.Shared["6100"] = "X";
                }
                //av opm 26446 this is the version to export when the broker has lender defaults is turned on 
                input.Shared["6062"] = principal.HasLenderDefaultFeatures ? loanData.sRLckdDays_rep : loanData.sRLckdNumOfDays_rep;
                if (loanData.sSubmitMIYes)
                    input.Shared["4016"] = "X";
                else
                    input.Shared["4017"] = "X";

                //#if xxxxyy
                input.Shared["4018"] = loanData.sSubmitMITypeDesc;
                input.Shared["4080"] = loanData.sDemandLOrigFeeLenderPc_rep;
                input.Shared["4081"] = loanData.sDemandLOrigFeeLenderMb_rep;
                input.Shared["4082"] = loanData.sDemandLOrigFeeLenderAmt_rep;
                input.Shared["4040"] = loanData.sDemandLOrigFeeBrokerPc_rep;
                input.Shared["4041"] = loanData.sDemandLOrigFeeBrokerMb_rep;//
                input.Shared["4042"] = loanData.sDemandLOrigFeeBrokerAmt_rep; //cal
                input.Shared["4083"] = loanData.sDemandLDiscountLenderPc_rep;
                input.Shared["4084"] = loanData.sDemandLDiscountLenderMb_rep;
                input.Shared["4085"] = loanData.sDemandLDiscountLenderAmt_rep;//
                input.Shared["4043"] = loanData.sDemandLDiscountBrokerPc_rep;
                input.Shared["4044"] = loanData.sDemandLDiscountBrokerMb_rep;
                input.Shared["4045"] = loanData.sDemandLDiscountLenderAmt_rep;//
                input.Shared["4086"] = loanData.sDemandYieldSpreadPremiumLenderPc_rep;
                input.Shared["4087"] = loanData.sDemandYieldSpreadPremiumLenderMb_rep;
                input.Shared["4088"] = loanData.sDemandYieldSpreadPremiumLenderAmt_rep;
                input.Shared["4046"] = loanData.sDemandYieldSpreadPremiumBrokerPc_rep;//
                input.Shared["4047"] = loanData.sDemandYieldSpreadPremiumBrokerMb_rep;//
                input.Shared["4048"] = loanData.sDemandYieldSpreadPremiumBrokerAmt_rep;//
                input.Shared["4026"] = loanData.sDemandYieldSpreadPremiumBorrowerPc_rep;
                input.Shared["4027"] = loanData.sDemandYieldSpreadPremiumBorrowerMb_rep;
                input.Shared["4028"] = loanData.sDemandYieldSpreadPremiumBrokerAmt_rep;//
                input.Shared["4089"] = loanData.sDemandApprFeeLenderAmt_rep;
                input.Shared["4050"] = loanData.sDemandApprFeeBrokerPaidAmt_rep;
                input.Shared["4051"] = loanData.sDemandApprFeeBrokerDueAmt_rep;
                input.Shared["4090"] = loanData.sDemandCreditReportFeeLenderAmt_rep;
                input.Shared["4052"] = loanData.sDemandCreditReportFeeBrokerPaidAmt_rep;
                input.Shared["4053"] = loanData.sDemandCreditReportFeeBrokerDueAmt_rep;
                input.Shared["4091"] = loanData.sDemandProcessingFeeLenderAmt_rep;
                input.Shared["4054"] = loanData.sDemandProcessingFeeBrokerPaidAmt_rep;
                input.Shared["4055"] = loanData.sDemandProcessingFeeBrokerDueAmt_rep;//
                input.Shared["4092"] = loanData.sDemandLoanDocFeeLenderAmt_rep;
                input.Shared["4056"] = loanData.sDemandLoanDocFeeBrokerPaidAmt_rep;
                input.Shared["4057"] = loanData.sDemandLoanDocFeeBrokerDueAmt_rep;//
                input.Shared["4074"] = loanData.sDemandLoanDocFeeBorrowerAmt_rep;
                //#if xxxxyy		
                input.Shared["4019"] = loanData.sDemandU1LenderDesc;
                input.Shared["4020"] = loanData.sDemandU2LenderDesc;
                input.Shared["4021"] = loanData.sDemandU3LenderDesc;
                input.Shared["4022"] = loanData.sDemandU4LenderDesc;
                input.Shared["4023"] = loanData.sDemandU5LenderDesc;
                input.Shared["4024"] = loanData.sDemandU6LenderDesc;
                input.Shared["4025"] = loanData.sDemandU7LenderDesc;

                input.Shared["4030"] = loanData.sDemandU1LenderAmt_rep;
                input.Shared["4031"] = loanData.sDemandU2LenderAmt_rep;
                input.Shared["4032"] = loanData.sDemandU3LenderAmt_rep;
                input.Shared["4033"] = loanData.sDemandU4LenderAmt_rep;
                input.Shared["4034"] = loanData.sDemandU5LenderAmt_rep;
                input.Shared["4035"] = loanData.sDemandU6LenderAmt_rep;
                input.Shared["4036"] = loanData.sDemandU7LenderAmt_rep;

                input.Shared["4058"] = loanData.sDemandU1BrokerPaidAmt_rep;
                input.Shared["4060"] = loanData.sDemandU2BrokerPaidAmt_rep;
                input.Shared["4062"] = loanData.sDemandU3BrokerPaidAmt_rep;
                input.Shared["4064"] = loanData.sDemandU4BrokerPaidAmt_rep;
                input.Shared["4066"] = loanData.sDemandU5BrokerPaidAmt_rep;
                input.Shared["4068"] = loanData.sDemandU6BrokerPaidAmt_rep;
                input.Shared["4070"] = loanData.sDemandU7BrokerPaidAmt_rep;

                input.Shared["4059"] = loanData.sDemandU1BrokerDueAmt_rep;//
                input.Shared["4061"] = loanData.sDemandU2BrokerDueAmt_rep;//
                input.Shared["4063"] = loanData.sDemandU3BrokerDueAmt_rep;//
                input.Shared["4065"] = loanData.sDemandU4BrokerDueAmt_rep;//
                input.Shared["4067"] = loanData.sDemandU5BrokerDueAmt_rep;//
                input.Shared["4069"] = loanData.sDemandU6BrokerDueAmt_rep;//
                input.Shared["4071"] = loanData.sDemandU7BrokerDueAmt_rep;//
                //#if xxxxyy
                input.Shared["4093"] = loanData.sDemandU1BorrowerAmt_rep;
                input.Shared["4094"] = loanData.sDemandU2BorrowerAmt_rep;
                input.Shared["4095"] = loanData.sDemandU3BorrowerAmt_rep;
                input.Shared["4096"] = loanData.sDemandU4BorrowerAmt_rep;
                input.Shared["4097"] = loanData.sDemandU5BorrowerAmt_rep;
                input.Shared["4098"] = loanData.sDemandU6BorrowerAmt_rep;
                input.Shared["4099"] = loanData.sDemandU7BorrowerAmt_rep;
                //#if xxxxyy
                input.Shared["4037"] = loanData.sDemandTotalLenderAmt_rep;
                input.Shared["4072"] = loanData.sDemandTotalBrokerPaidAmt_rep;
                input.Shared["4073"] = loanData.sDemandTotalBrokerDueAmt_rep;
                input.Shared["4100"] = loanData.sDemandTotalBorrowerAmt_rep;
                //>>>>#if xxxxyy
                //input.Shared["4101"]	 = loanData.sDemandNotes;

                string longStr = loanData.sDemandNotes.TrimWhitespaceAndBOM();
                if (longStr.Length > 0)
                {   // Point can go up to about between 190-200 chars per line before
                    // declaring the resulted file is corrupted.
                    string[] pieces = Split(longStr, 180, 13);
                    for (int iPiece = 0; iPiece < pieces.Length; ++iPiece)
                    {
                        int resultedId = iPiece + 4101;
                        if (resultedId > 4113)
                            break;
                        input.Shared[resultedId.ToString("D")] = pieces[iPiece];
                    }
                }

                //>>>#endif
                input.Shared["215"] = loanData.sLenderNumVerif;

                // 2/3/2005 dd - Survey Request
                input.Shared["2405"] = loanData.sAttachedContractBit ? "X" : "";
                input.Shared["2406"] = loanData.sAttachedSurveyBit ? "X" : "";
                input.Shared["2407"] = loanData.sAttachedPlansBit ? "X" : "";
                input.Shared["2408"] = loanData.sAttachedCostsBit ? "X" : "";
                input.Shared["2409"] = loanData.sAttachedSpecsBit ? "X" : "";
                input.Shared["2411"] = loanData.sSurveyRequestN;
                //#endif //xxxxyy

                //#endregion // LOAN SUBMISSION


                // ASSIGNED EMPLOYEES TO INTERNAL TRACK

                // Loan rep. Internal
                /*
                    CEmployeeFields loanRep = loanData.sEmployeeLoanRep;
                    input.Shared["19"] = loanRep.FullName;
                    input.Shared["6355"] = loanRep.Phone;
                    input.Shared["6356"] = loanRep.Fax;
                */

                // Processor
                /*
                CEmployeeFields processor = loanData.sEmployeeProcessor;
                input.Shared["18"] = processor.FullName;
                input.Shared["6359"] = processor.Phone;
                input.Shared["6360"] = processor.Fax;
                */

                // Selling agent
                // See input.Shared["6140"] below.								

                // AGENTS FROM AGENTS TRACK - THESE ARE TYPICALLY EXTERNAL PEOPLE

                int nAgent = loanData.sAgentCollection.GetAgentRecordCount();


                int baseIdForOther = 480;

                for (int iAgent = 0; iAgent < nAgent; ++iAgent)
                {
                    CAgentFields fields = loanData.GetAgentFields(iAgent);
                    switch (fields.AgentRoleT)
                    {
                        case E_AgentRoleT.Appraiser:
                            input.Shared["330"] = fields.AgentName;
                            input.Shared["332"] = fields.Phone;
                            input.Shared["335"] = fields.FaxNum;
                            input.Shared["331"] = fields.CompanyName;
                            input.Shared["333"] = fields.StreetAddr;
                            input.Shared["334"] = fields.CityStateZip;
                            input.Shared["339"] = fields.LicenseNumOfAgent;
                            input.Shared["337"] = fields.CaseNum;
                            input.Shared["12367"] = fields.CellPhone; //av opm 2199 5 20 8
                            input.Shared["12368"] = fields.EmailAddr;

                            break;
                        case E_AgentRoleT.Bank:
                            break;
                        case E_AgentRoleT.Builder:
                            input.Shared["360"] = fields.AgentName;
                            input.Shared["362"] = fields.Phone;
                            input.Shared["368"] = fields.FaxNum;
                            input.Shared["361"] = fields.CompanyName;
                            input.Shared["363"] = fields.StreetAddr;
                            input.Shared["364"] = fields.CityStateZip;
                            input.Shared["369"] = fields.CaseNum;
                            input.Shared["12381"] = fields.CellPhone;
                            input.Shared["12382"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.BuyerAttorney:
                            input.Shared["439"] = fields.LicenseNumOfAgent;
                            input.Shared["430"] = fields.AgentName;
                            input.Shared["432"] = fields.Phone;
                            input.Shared["435"] = fields.FaxNum;
                            input.Shared["431"] = fields.CompanyName;
                            input.Shared["433"] = fields.StreetAddr;
                            input.Shared["434"] = fields.CityStateZip;
                            input.Shared["12373"] = fields.CellPhone;
                            input.Shared["12374"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.BuyerAgent:
                            input.Shared["6191"] = fields.AgentName;
                            input.Shared["6192"] = fields.Phone;
                            input.Shared["6193"] = fields.FaxNum;
                            input.Shared["6194"] = fields.CellPhone;
                            input.Shared["6195"] = fields.EmailAddr;
                            input.Shared["6196"] = fields.CompanyName;
                            input.Shared["6197"] = fields.StreetAddr;
                            input.Shared["6198"] = fields.CityStateZip;
                            break;
                        case E_AgentRoleT.ClosingAgent:
                        case E_AgentRoleT.CallCenterAgent:
                            break;
                        case E_AgentRoleT.CreditReport:
                            input.Shared["3841"] = fields.CompanyName;
                            input.Shared["3842"] = fields.StreetAddr;
                            input.Shared["3843"] = fields.CityStateZip;
                            input.Shared["3844"] = fields.Phone;
                            break;
                        case E_AgentRoleT.CreditReportAgency2:
                        case E_AgentRoleT.CreditReportAgency3:
                        case E_AgentRoleT.ECOA:
                            break;
                        case E_AgentRoleT.Escrow:
                            input.Shared["6119"] = fields.CaseNum;
                            input.Shared["6110"] = fields.AgentName;
                            input.Shared["6112"] = fields.Phone;
                            input.Shared["6115"] = fields.FaxNum;
                            input.Shared["6111"] = fields.CompanyName;
                            input.Shared["6113"] = fields.StreetAddr;
                            input.Shared["6114"] = fields.CityStateZip;
                            input.Shared["12369"] = fields.CellPhone;
                            input.Shared["12370"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.FloodProvider: // db - OPM 33365
                            input.Shared["12786"] = fields.AgentName;
                            input.Shared["13000"] = fields.Phone;
                            input.Shared["13002"] = fields.FaxNum;
                            input.Shared["12787"] = fields.CompanyName;
                            input.Shared["12762"] = fields.StreetAddr;
                            input.Shared["12763"] = fields.City;
                            input.Shared["12764"] = fields.State;
                            input.Shared["12765"] = fields.Zip;
                            input.Shared["12969"] = fields.CaseNum;
                            input.Shared["13001"] = fields.CellPhone;
                            input.Shared["13003"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.HazardInsurance:
                            input.Shared["450"] = fields.AgentName;
                            input.Shared["451"] = fields.Phone;
                            input.Shared["452"] = fields.FaxNum;
                            input.Shared["453"] = fields.CompanyName;
                            input.Shared["454"] = fields.StreetAddr;
                            input.Shared["455"] = fields.CityStateZip;
                            input.Shared["456"] = fields.CaseNum;
                            input.Shared["12385"] = fields.CellPhone;
                            input.Shared["12386"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.HomeOwnerInsurance:
                        case E_AgentRoleT.HomeOwnerAssociation:
                        case E_AgentRoleT.Investor:
                            // 11/28/07 db - OPM 17176
                            input.Shared["7340"] = fields.AgentName;
                            input.Shared["7341"] = fields.CompanyName;
                            input.Shared["7342"] = fields.Phone;
                            input.Shared["7343"] = fields.StreetAddr;
                            input.Shared["7344"] = fields.City;
                            input.Shared["7345"] = fields.State;
                            input.Shared["7346"] = fields.Zip;
                            input.Shared["7347"] = fields.InvestorBasisPoints_rep;
                            input.Shared["7348"] = fields.FaxNum;
                            input.Shared["7349"] = fields.LicenseNumOfAgent;
                            input.Shared["6381"] = fields.Notes;
                            input.Shared["12363"] = fields.CellPhone;
                            input.Shared["12364"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.ListingAgent:
                            input.Shared["6130"] = fields.AgentName;
                            input.Shared["6132"] = fields.Phone;
                            input.Shared["6135"] = fields.FaxNum;
                            input.Shared["6131"] = fields.CompanyName;
                            input.Shared["6133"] = fields.StreetAddr;
                            input.Shared["6134"] = fields.CityStateZip;
                            input.Shared["12377"] = fields.CellPhone;
                            input.Shared["12378"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.LoanOfficer:
                            input.Shared["1508"] = fields.AgentName;
                            input.Shared["1509"] = fields.Phone;
                            input.Shared["1516"] = fields.FaxNum;
                            input.Shared["6178"] = fields.Notes;
                            // Making both Point's loan rep data set the same.
                            input.Shared["19"] = fields.AgentName;
                            input.Shared["6355"] = fields.Phone;
                            input.Shared["6356"] = fields.FaxNum;
                            input.Shared["6382"] = fields.CellPhone;
                            input.Shared["6383"] = fields.EmailAddr;
                            input.Shared["12365"] = fields.CellPhone;
                            input.Shared["12366"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.LoanOpener:
                        case E_AgentRoleT.Manager:
                        case E_AgentRoleT.MarketingLead:
                        case E_AgentRoleT.Mortgagee:
                            break;
                        case E_AgentRoleT.MortgageInsurance:
                            input.Shared["460"] = fields.AgentName;
                            input.Shared["461"] = fields.Phone;
                            input.Shared["462"] = fields.FaxNum;
                            input.Shared["463"] = fields.CompanyName;
                            input.Shared["464"] = fields.StreetAddr;
                            input.Shared["465"] = fields.CityStateZip;
                            input.Shared["12387"] = fields.CellPhone;
                            input.Shared["12388"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.Realtor:
                            break;
                        case E_AgentRoleT.Other:
                            {
                                int email_cellBaseAddress = 0;
                                // Find the next slot
                                if ("" == input.Shared["480"])
                                {
                                    baseIdForOther = 480;
                                    email_cellBaseAddress = 12391;
                                }
                                else if ("" == input.Shared["490"])
                                {
                                    baseIdForOther = 490;
                                    email_cellBaseAddress = 12393;
                                }
                                else if ("" == input.Shared["500"])
                                {
                                    baseIdForOther = 500;
                                    email_cellBaseAddress = 12395;
                                }
                                else
                                    break; // We ran out of space to export these fields.
                                input.Shared[baseIdForOther.ToString()] = fields.OtherAgentRoleTDesc;
                                input.Shared[(baseIdForOther + 1).ToString()] = fields.AgentName;
                                input.Shared[(baseIdForOther + 2).ToString()] = fields.Phone;
                                input.Shared[(baseIdForOther + 3).ToString()] = fields.FaxNum;
                                input.Shared[(baseIdForOther + 4).ToString()] = fields.CompanyName;
                                input.Shared[(baseIdForOther + 5).ToString()] = fields.StreetAddr;
                                input.Shared[(baseIdForOther + 6).ToString()] = fields.CityStateZip;
                                input.Shared[email_cellBaseAddress.ToString()] = fields.CellPhone;
                                input.Shared[(email_cellBaseAddress + 1).ToString()] = fields.EmailAddr;

                                int lenNotes = fields.Notes.Length;
                                if (lenNotes > 0)
                                {
                                    input.Shared[(baseIdForOther + 7).ToString()] = fields.Notes.Substring(0, Math.Min(lenNotes, 44));
                                    if (lenNotes > 44)
                                        input.Shared[(baseIdForOther + 8).ToString()] = fields.Notes.Substring(44, lenNotes - 44);
                                }
                            }
                            break;
                        case E_AgentRoleT.Seller:
                            m_nameParser.ParseName(fields.AgentName);
                            input.Shared["7300"] = m_nameParser.FirstName;
                            input.Shared["7301"] = m_nameParser.LastName;
                            input.Shared["7307"] = fields.Phone;
                            input.Shared["7308"] = fields.FaxNum;
                            input.Shared["7302"] = fields.StreetAddr;
                            input.Shared["7303"] = fields.City;
                            input.Shared["7304"] = fields.State;
                            input.Shared["7305"] = fields.Zip;
                            input.Shared["12383"] = fields.CellPhone;
                            input.Shared["12384"] = fields.EmailAddr;
                            break;

                        case E_AgentRoleT.SellerAttorney:
                            input.Shared["449"] = fields.LicenseNumOfAgent;
                            input.Shared["440"] = fields.AgentName;
                            input.Shared["442"] = fields.Phone;
                            input.Shared["445"] = fields.FaxNum;
                            input.Shared["441"] = fields.CompanyName;
                            input.Shared["443"] = fields.StreetAddr;
                            input.Shared["444"] = fields.CityStateZip;
                            input.Shared["12375"] = fields.CellPhone;
                            input.Shared["12376"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.SellingAgent:
                            input.Shared["6140"] = fields.AgentName;
                            input.Shared["6142"] = fields.Phone;
                            input.Shared["6145"] = fields.FaxNum;
                            input.Shared["6141"] = fields.CompanyName;
                            input.Shared["6143"] = fields.StreetAddr;
                            input.Shared["6144"] = fields.CityStateZip;
                            input.Shared["12379"] = fields.CellPhone;
                            input.Shared["12380"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.Servicing:
                            break;
                        case E_AgentRoleT.Title:
                            input.Shared["6129"] = fields.CaseNum;
                            input.Shared["6120"] = fields.AgentName;
                            input.Shared["6122"] = fields.Phone;
                            input.Shared["6125"] = fields.FaxNum;
                            input.Shared["6121"] = fields.CompanyName;
                            input.Shared["6123"] = fields.StreetAddr;
                            input.Shared["6124"] = fields.CityStateZip;
                            input.Shared["12372"] = fields.EmailAddr; // opm 18526 av 05/06/08
                            input.Shared["12371"] = fields.CellPhone;
                            break;
                        case E_AgentRoleT.Surveyor:
                            input.Shared["470"] = fields.AgentName;
                            input.Shared["471"] = fields.Phone;
                            input.Shared["472"] = fields.FaxNum;
                            input.Shared["473"] = fields.CompanyName;
                            input.Shared["474"] = fields.StreetAddr;
                            input.Shared["475"] = fields.CityStateZip;
                            input.Shared["12389"] = fields.CellPhone;
                            input.Shared["12390"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.BrokerRep: // obsolete //av 06 10 08 
                            // 3/8/2007 nw - OPM 10444
                            input.Shared["6370"] = fields.CompanyName;
                            input.Shared["6371"] = fields.AgentName;
                            input.Shared["6372"] = fields.StreetAddr;
                            input.Shared["6373"] = fields.CityStateZip;
                            input.Shared["6374"] = fields.Phone;
                            input.Shared["6375"] = fields.FaxNum;
                            input.Shared["6376"] = fields.LicenseNumOfCompany;	// fields.LicenseNumOfAgent; ??
                            input.Shared["6377"] = fields.Notes;
                            input.Shared["12355"] = fields.CellPhone;
                            input.Shared["12356"] = fields.EmailAddr;
                            input.Shared["6377"] = fields.Notes;

                            break;
                        case E_AgentRoleT.Broker://BrokerRep: //obsolete
                            break;
                        case E_AgentRoleT.Underwriter:
                            input.Shared["942"] = fields.AgentName;
                            input.Shared["5756"] = fields.LicenseNumOfAgent;
                            input.Shared["6363"] = fields.Phone;
                            input.Shared["6364"] = fields.FaxNum;
                            input.Shared["6386"] = fields.CellPhone;
                            input.Shared["6387"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.Lender:
                            input.Shared["6001"] = fields.CompanyName;
                            input.Shared["6003"] = fields.StreetAddr;
                            input.Shared["6004"] = fields.CityStateZip;
                            input.Shared["6000"] = fields.AgentName;
                            input.Shared["6002"] = fields.Phone;
                            input.Shared["6005"] = fields.FaxNum;
                            input.Shared["6007"] = fields.CaseNum;
                            input.Shared["12357"] = fields.CellPhone;
                            input.Shared["12358"] = fields.EmailAddr;
                            break;
                        case E_AgentRoleT.Processor:
                        case E_AgentRoleT.BrokerProcessor:
                            input.Shared["18"] = fields.AgentName;
                            input.Shared["6359"] = fields.Phone;
                            input.Shared["6360"] = fields.FaxNum;
                            input.Shared["6384"] = fields.EmailAddr;
                            input.Shared["6385"] = fields.CellPhone;
                            break;
                        case E_AgentRoleT.PropertyManagement:
                        case E_AgentRoleT.TitleUnderwriter:
                        case E_AgentRoleT.CreditAuditor:
                        case E_AgentRoleT.DisclosureDesk:
                        case E_AgentRoleT.JuniorProcessor:
                        case E_AgentRoleT.JuniorUnderwriter:
                        case E_AgentRoleT.LegalAuditor:
                        case E_AgentRoleT.LoanOfficerAssistant:
                        case E_AgentRoleT.Purchaser:
                        case E_AgentRoleT.QCCompliance:
                        case E_AgentRoleT.Secondary:
                        case E_AgentRoleT.ExternalSecondary:
                        case E_AgentRoleT.ExternalPostCloser:
                        case E_AgentRoleT.Referral:
                            break;
                        default:
                            Tools.LogError("E_AgentRoleT enum value of " + fields.AgentRoleT.ToString("D") + " is not exported to Point.");
                            break;

                    }// switch
                } // for, looping through external agents.

                // PREPARERS OF FORMS



                // Request Of Appraisal
                IPreparerFields prep = loanData.GetPreparerOfForm(E_PreparerFormT.RequestOfAppraisal, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                {
                    input.Shared["1704"] = prep.PreparerName;
                    input.Shared["1705"] = prep.Title;
                }

                prep = loanData.GetPreparerOfForm(E_PreparerFormT.FloridaLenderDisclosure, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                {
                    input.Shared["11428"] = prep.PreparerName;
                    input.Shared["11429"] = prep.StreetAddr;
                    input.Shared["11430"] = prep.City;
                    input.Shared["11431"] = prep.State;
                    input.Shared["11432"] = prep.Zip;
                }

                // State Of Denial Statement
                prep = loanData.GetPreparerOfForm(E_PreparerFormT.CreditDenialStatement, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                    input.Shared["3849"] = prep.PreparerName;


                prep = loanData.GetPreparerOfForm(E_PreparerFormT.FloridaMortBrokerBusinessContract, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                {
                    input.Shared["3913"] = prep.LicenseNumOfCompany;
                }

                // 1003
                prep = loanData.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                {
                    if ("" == input.Shared["1508"])
                        input.Shared["1508"] = prep.PreparerName;
                    if ("" == input.Shared["1509"])
                        input.Shared["1509"] = prep.Phone;

                    input.Shared["381"] = prep.CompanyName;
                    input.Shared["383"] = prep.StreetAddr;
                    input.Shared["384"] = prep.City;
                    input.Shared["385"] = prep.State;
                    input.Shared["386"] = prep.Zip;
                    input.Shared["382"] = prep.PhoneOfCompany;
                    input.Shared["387"] = prep.FaxOfCompany;
                }

                if (loanData.sApp1003InterviewerPrepareDate.IsValid)
                {
                    input.Shared["421"] = loanData.sApp1003InterviewerPrepareDate_rep;
                }

                prep = loanData.GetPreparerOfForm(E_PreparerFormT.RequestOfInsurance, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                    input.Shared["5890"] = prep.PrepareDate_rep;

                prep = loanData.GetPreparerOfForm(E_PreparerFormT.RequestOfAppraisal, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                    input.Shared["336"] = prep.PrepareDate_rep;

                prep = loanData.GetPreparerOfForm(E_PreparerFormT.RequestOfTitle, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                    input.Shared["5888"] = prep.PrepareDate_rep;

                prep = loanData.GetPreparerOfForm(E_PreparerFormT.Gfe, E_ReturnOptionIfNotExist.CreateNew);
                if (prep.IsValid)
                {
                    input.Shared["2181"] = prep.PrepareDate_rep;
                    input.Shared["5402"] = prep.PreparerName;
                    input.Shared["5403"] = prep.LicenseNumOfAgent;
                    input.Shared["5400"] = prep.CompanyName;
                    input.Shared["5401"] = prep.LicenseNumOfCompany;
                }

                prep = loanData.GetPreparerOfForm(E_PreparerFormT.FHAConditionalCommitment, E_ReturnOptionIfNotExist.ReturnEmptyObject);
                if (prep.IsValid)
                {
                    input.Shared["7942"] = prep.PreparerName;
                }


                input.Shared["2050"] = loanData.sApprRprtDueD_rep;
                //#endif //xxxyyy

                // 3/7/2007 nw - OPM 11207
                input.Shared["2801"] = loanData.sONewFinCc_rep;

                // 3/8/2007 nw - OPM 10444
                input.Shared["20"] = loanData.BranchNm; ;

                switch (loanData.sProdDocT)
                {
                    case E_sProdDocT.Full:
                        input.Shared["2002"] = "Full";
                        break;
                    case E_sProdDocT.Alt:
                        input.Shared["2002"] = "Alt";
                        break;
                    case E_sProdDocT.Light:
                        input.Shared["2002"] = "Light";
                        break;
                    case E_sProdDocT.SIVA:
                        input.Shared["2002"] = "SIVA";
                        break;
                    case E_sProdDocT.VISA:
                        input.Shared["2002"] = "VISA";
                        break;
                    case E_sProdDocT.SISA:
                        input.Shared["2002"] = "SISA";
                        break;
                    case E_sProdDocT.NIVA:
                        input.Shared["2002"] = "NIVA";
                        break;
                    case E_sProdDocT.NINA:
                        input.Shared["2002"] = "NINA";
                        break;
                    case E_sProdDocT.NISA:
                        input.Shared["2002"] = "NISA";
                        break;
                    case E_sProdDocT.NINANE:
                        input.Shared["2002"] = "NINANE";
                        break;
                    case E_sProdDocT.NIVANE:
                        input.Shared["2002"] = "NIVANE";
                        break;
                    case E_sProdDocT.VINA:
                        input.Shared["2002"] = "VINA";
                        break;
                    case E_sProdDocT.Streamline: // OPM 30110
                        input.Shared["2002"] = "Streamline";
                        break;
                    case E_sProdDocT._12MoPersonalBankStatements:
                    case E_sProdDocT._24MoPersonalBankStatements:
                    case E_sProdDocT._12MoBusinessBankStatements:
                    case E_sProdDocT._24MoBusinessBankStatements:
                    case E_sProdDocT.OtherBankStatements:
                    case E_sProdDocT._1YrTaxReturns:
                    case E_sProdDocT.AssetUtilization:
                    case E_sProdDocT.DebtServiceCoverage:
                    case E_sProdDocT.NoIncome:
                    case E_sProdDocT.Voe:
                        input.Shared["2002"] = "Alt";
                        break;
                    default:
                        Tools.LogError("E_sProdDocT enum value of " + loanData.sProdDocT.ToString("D") + " is not exported to Point.");
                        break;
                }

                // 3/20/2007 nw - OPM 11793
                input.Shared["3220"] = loanData.sLeadSrcDesc;
                // 08 - 14 - 08  av OPM 24106  -- this is for datatrac
                input.Shared["6178"] = loanData.sLeadSrcDesc;


            } // if( 0 == iApp )
            #endregion // SHARED FIELDS
            #region PER-APP DATA

            input.InitToWriteNewApp();

            CAppData appData = loanData.GetAppData(iApp);

            m_triStateParser.Parse(appData.aHas1stTimeBuyerTri);
            input.App["7505"] = m_triStateParser.xYes;
            input.App["7506"] = m_triStateParser.xNo;

            m_triStateParser.Parse(appData.aVaBorrCertHadVaLoanTri);
            input.App["7638"] = m_triStateParser.xYes;
            input.App["7639"] = m_triStateParser.xNo;

            input.App["7640"] = appData.aFHABorrCertOccIsAsHome ? "X" : "";
            input.App["7641"] = appData.aFHABorrCertOccIsAsHomeForActiveSpouse ? "X" : "";
            input.App["7642"] = appData.aFHABorrCertOccIsAsHomePrev ? "X" : "";
            input.App["7643"] = appData.aFHABorrCertOccIsAsHomePrevForActiveSpouse ? "X" : "";

            m_triStateParser.Parse(appData.aFHABorrCertOwnOrSoldOtherFHAPropTri);
            input.App["7630"] = m_triStateParser.xYes;
            input.App["7626"] = m_triStateParser.xNo;
            m_triStateParser.Parse(appData.aFHABorrCertOtherPropToBeSoldTri);
            input.App["7631"] = m_triStateParser.xYes;
            input.App["7627"] = m_triStateParser.xNo;
            input.App["7632"] = appData.aFHABorrCertOtherPropSalesPrice_rep;
            input.App["7633"] = appData.aFHABorrCertOtherPropOrigMAmt_rep;
            input.App["7634"] = appData.aFHABorrCertOtherPropStAddr;
            input.App["7635"] = Tools.CombineCityStateZip(appData.aFHABorrCertOtherPropCity, appData.aFHABorrCertOtherPropState, appData.aFHABorrCertOtherPropZip);
            m_triStateParser.Parse(appData.aFHABorrCertOtherPropCoveredByThisLoanTri);
            input.App["7636"] = m_triStateParser.xYes;
            input.App["7628"] = m_triStateParser.xNo;
            m_triStateParser.Parse(appData.aFHABorrCertOwnMoreThan4DwellingsTri);
            input.App["7637"] = m_triStateParser.xYes;
            input.App["7629"] = m_triStateParser.xNo;
            input.App["7644"] = appData.aFHABorrCertInformedPropVal_rep;
            input.App["7645"] = appData.aFHABorrCertInformedPropValDeterminedByVA ? "X" : "";
            input.App["7646"] = appData.aFHABorrCertInformedPropValDeterminedByHUD ? "X" : "";
            input.App["7647"] = appData.aFHABorrCertInformedPropValAwareAtContractSigning ? "X" : "";
            input.App["7648"] = appData.aFHABorrCertInformedPropValNotAwareAtContractSigning ? "X" : "";
            m_triStateParser.Parse(appData.aFHABorrCertReceivedLeadPaintPoisonInfoTri);
            input.App["7649"] = m_triStateParser.xYes;
            input.App["7695"] = m_triStateParser.xNo;


            input.App["7796"] = appData.aVaClaimFolderNum;
            input.App["7797"] = appData.aVaServiceNum;
            input.App["7798"] = PointDataConversion.FromTriStateToXYes(appData.aVaIndebtCertifyTri);
            input.App["7799"] = PointDataConversion.FromTriStateToXYes(appData.aVaIndebtCertifyTri);

            input.App["8322"] = appData.aVaEntitleCode;// varchar(50) not null default( '' ), --8322 -use a combo for 01,02,....11
            input.App["8323"] = appData.aVaEntitleAmt_rep; // money not null default( 0 ), --8323
            switch (appData.aVaServiceBranchT)
            {
                case E_aVaServiceBranchT.AirForce: input.App["8305"] = "X"; break;
                case E_aVaServiceBranchT.Army: input.App["8303"] = "X"; break;
                case E_aVaServiceBranchT.CoastGuard: input.App["8307"] = "X"; break;
                case E_aVaServiceBranchT.LeaveBlank: break;
                case E_aVaServiceBranchT.Marine: input.App["8306"] = "X"; break;
                case E_aVaServiceBranchT.Navy: input.App["8304"] = "X"; break;
                case E_aVaServiceBranchT.Other: input.App["8308"] = "X"; break;
                default: Tools.LogBug("Unhandled enum value of E_aVaServiceBranchT in Point Export"); break;

            }

            switch (appData.aVaMilitaryStatT)
            {
                case E_aVaMilitaryStatT.InService: input.App["8310"] = "X"; break;
                case E_aVaMilitaryStatT.SeparatedFromService: input.App["8311"] = "X"; break;
                case E_aVaMilitaryStatT.LeaveBlank: break;
                default: Tools.LogBug("Unhandled enum value of E_aVaMilitaryStatT in Point Export"); break;

            }

            input.App["7590"] = FromTriStateToXYes(appData.aVaIsVeteranFirstTimeBuyerTri);
            input.App["8379"] = appData.aVaTotMonGrossI_rep;
            input.App["7750"] = appData.aVaIsCoborIConsidered ? "X" : "";


            input.App["7701"] = PointDataConversion.FromTriStateToXYes(appData.aVaUtilityIncludedTri);
            input.App["7596"] = PointDataConversion.FromTriStateToXNo(appData.aVaUtilityIncludedTri);

            IPrimaryEmploymentRecord empRec = appData.aBEmpCollection.GetPrimaryEmp(false);
            if (null != empRec)
            {
                input.App["205"] = empRec.EmplmtLenInYrs_rep;
                input.App["206"] = empRec.EmplmtLenInMonths_rep;
            }

            empRec = appData.aCEmpCollection.GetPrimaryEmp(false);
            if (null != empRec)
            {
                input.App["225"] = empRec.EmplmtLenInYrs_rep;
                input.App["226"] = empRec.EmplmtLenInMonths_rep;
            }

            input.App["7750"] = appData.aVaCEmplmtI_rep;
            //aVaCEmplmtILckd bit not null default( 0 ), --
            input.App["7751"] = appData.aVaCFedITax_rep;
            input.App["7752"] = appData.aVaCStateITax_rep;
            input.App["7753"] = appData.aVaCSsnTax_rep;
            input.App["7754"] = appData.aVaCOITax_rep;
            input.App["7755"] = appData.aVaCTotITax_rep;
            input.App["7756"] = appData.aVaCTakehomeEmplmtI_rep;
            input.App["7757"] = appData.aVaCONetI_rep;
            //aVaCONetILckd bit not null default( 0 ), 
            //aVaONetIDesc varchar(100) not null default( '' ), -- Point doesn't have this.
            input.App["7758"] = appData.aVaCNetI_rep;
            input.App["7760"] = appData.aVaBEmplmtI_rep;
            //aVaBEmplmtILckd bit not null default( 0 ), --
            input.App["7761"] = appData.aVaBFedITax_rep;
            input.App["7762"] = appData.aVaBStateITax_rep;
            input.App["7763"] = appData.aVaBSsnTax_rep;
            input.App["7764"] = appData.aVaBOITax_rep;
            input.App["7765"] = appData.aVaBTotITax_rep;
            input.App["7766"] = appData.aVaBTakehomeEmplmtI_rep;
            input.App["7767"] = appData.aVaBONetI_rep;
            //aVaBONetILckd bit not null default( 0 ), 					
            input.App["7768"] = appData.aVaBNetI_rep;
            input.App["7770"] = appData.aVaTotEmplmtI_rep;
            input.App["7771"] = appData.aVaTotITax_rep;
            input.App["7772"] = appData.aVaTakehomeEmplmtI_rep;
            input.App["7773"] = appData.aVaONetI_rep;
            input.App["7774"] = appData.aVaNetI_rep;

            //TODO: --Export liability for VA Loan Analysis 
            input.App["7776"] = appData.aVaNetEffectiveI_rep;
            input.App["7779"] = appData.aVaFamilySupportBal_rep;
            input.App["7778"] = appData.aVaFamilySuportGuidelineAmt_rep;
            input.App["7780"] = appData.aVaRatio_rep;
            input.App["7781"] = FromTriStateToXYes(appData.aVaCrRecordSatisfyTri);
            input.App["7782"] = FromTriStateToXNo(appData.aVaCrRecordSatisfyTri);
            input.App["7783"] = FromTriStateToXYes(appData.aVaLMeetCrStandardTri);
            input.App["7793"] = FromTriStateToXNo(appData.aVaLMeetCrStandardTri);

            SplitAndAssignToPoint(input.App, 7784, 7792, appData.aVaLAnalysisN);
            input.App["7730"] = appData.aVaLiaMonTotExisting_rep;
            input.App["7731"] = appData.aVaLiaMonTotDeducted_rep;

            input.App["8400"] = FromTriStateToXYes(appData.aActiveMilitaryStatTri);
            input.App["8401"] = FromTriStateToXYes(appData.aWereActiveMilitaryDutyDayAfterTri);

            switch (appData.aVaVestTitleT)
            {
                case E_aVaVestTitleT.Blank: break;
                case E_aVaVestTitleT.Veteran: input.App["7591"] = "X"; break;
                case E_aVaVestTitleT.VeteranAndSpouse: input.App["7592"] = "X"; break;
                case E_aVaVestTitleT.Other: input.App["7593"] = "X"; break;
                default: Tools.LogBug("Unhandled value of E_aVaVestTitleT in Point export."); break;
            }

            input.App["7594"] = appData.aVaVestTitleODesc;
            input.App["8170"] = appData.aVaServ1StartD_rep;
            input.App["8176"] = appData.aVaServ2StartD_rep;
            input.App["8182"] = appData.aVaServ3StartD_rep;
            input.App["8188"] = appData.aVaServ4StartD_rep;
            input.App["8171"] = appData.aVaServ1EndD_rep;
            input.App["8177"] = appData.aVaServ2EndD_rep;
            input.App["8183"] = appData.aVaServ3EndD_rep;
            input.App["8189"] = appData.aVaServ4EndD_rep;
            input.App["8172"] = appData.aVaServ1FullNm;
            input.App["8178"] = appData.aVaServ2FullNm;
            input.App["8184"] = appData.aVaServ3FullNm;
            input.App["8190"] = appData.aVaServ4FullNm;
            input.App["8174"] = appData.aVaServ1Ssn;
            input.App["8180"] = appData.aVaServ2Ssn;
            input.App["8186"] = appData.aVaServ3Ssn;
            input.App["8192"] = appData.aVaServ4Ssn;
            input.App["8173"] = appData.aVaServ1Num;
            input.App["8179"] = appData.aVaServ2Num;
            input.App["8185"] = appData.aVaServ3Num;
            input.App["8191"] = appData.aVaServ4Num;
            input.App["8175"] = appData.aVaServ1BranchNum;
            input.App["8181"] = appData.aVaServ2BranchNum;
            input.App["8187"] = appData.aVaServ3BranchNum;
            input.App["8193"] = appData.aVaServ4BranchNum;
            input.App["8194"] = appData.aVaDischargedDisabilityIs ? "X" : "";
            input.App["8195"] = appData.aVaClaimNum;

            IVaPastLoanCollection vaPastLColl = appData.aVaPastLCollection;
            var vaSubcoll = vaPastLColl.GetSubcollection(true, E_VaPastLGroupT.ALL);

            int iVaPastL = 0;
            foreach (IVaPastLoan vaPastL in vaSubcoll)
            {
                int seed;
                switch (iVaPastL)
                {
                    case 0:
                        input.App["8203"] = vaPastL.LoanTypeDesc;
                        input.App["8205"] = vaPastL.StAddr; // POINT id jumps by 1 here!
                        input.App["8206"] = Tools.CombineCityStateZip(vaPastL.City, vaPastL.State, vaPastL.Zip);
                        input.App["8207"] = vaPastL.LoanD_rep;
                        input.App["8208"] = FromTriStateToXYes(vaPastL.IsStillOwned);
                        input.App["8209"] = vaPastL.PropSoldD_rep;
                        input.App["8214"] = vaPastL.VaLoanNum;
                        seed = 0;
                        break;
                    case 1: seed = 8405; break;
                    case 2: seed = 8412; break;
                    case 3: seed = 8419; break;
                    case 4: seed = 8426; break;
                    case 5: seed = 8433; break;
                    default:
                        Tools.LogBug("aVaPastLCollection should never contain more than 6 records at any given time. loanid = "
                            + loanData.sLId.ToString());
                        seed = 0;
                        break;
                }

                if (0 != seed)
                {
                    input.App[seed] = vaPastL.LoanTypeDesc;
                    input.App[seed + 1] = vaPastL.StAddr;
                    input.App[seed + 2] = Tools.CombineCityStateZip(vaPastL.City, vaPastL.State, vaPastL.Zip);
                    input.App[seed + 3] = vaPastL.LoanD_rep;
                    input.App[seed + 4] = FromTriStateToXYes(vaPastL.IsStillOwned);
                    input.App[seed + 5] = vaPastL.PropSoldD_rep;
                    input.App[seed + 6] = vaPastL.VaLoanNum;
                }

                ++iVaPastL;
            }

            input.App["7532"] = appData.aFHADebtInstallBal_rep;
            input.App["7533"] = appData.aFHAOtherDebtBal_rep;
            input.App["8069"] = appData.aFHAGiftFundSrc;
            input.App["8070"] = appData.aFHAGiftFundAmt_rep;
            input.App["7527"] = appData.aFHAAssetAvail_rep;
            input.App["7530"] = appData.aFHADebtInstallPmt_rep;
            input.App["7535"] = appData.aFHAChildSupportPmt_rep;
            input.App["7531"] = appData.aFHAOtherDebtPmt_rep;
            input.App["7660"] = appData.aFHABBaseI_rep; // 7660 is combined version of 600
            input.App["7661"] = appData.aFHABOI_rep;
            input.App["7662"] = appData.aFHACBaseI_rep;
            input.App["7663"] = appData.aFHACOI_rep;
            input.App["7664"] = appData.aFHANetRentalI_rep;
            input.App["7545"] = appData.aFHACreditRating;
            input.App["7546"] = appData.aFHARatingIAdequacy;
            input.App["7547"] = appData.aFHARatingIStability;
            input.App["7548"] = appData.aFHARatingAssetAdequacy;
            input.App["7550"] = appData.aFHABCaivrsNum;
            input.App["7551"] = appData.aFHACCaivrsNum;
            input.App["7565"] = appData.aFHABLpdGsa;
            input.App["7566"] = appData.aFHACLpdGsa;

            switch (appData.aOccT)
            {
                case E_aOccT.PrimaryResidence:
                    input.App["921"] = "X";
                    break;
                case E_aOccT.SecondaryResidence:
                    input.App["923"] = "X";
                    break;
                case E_aOccT.Investment:
                    input.App["924"] = "X";
                    break;
            }


            input.App["102"] = appData.aBAddr;
            input.App["152"] = appData.aCAddr;
            input.App["103"] = appData.aBCity;
            input.App["153"] = appData.aCCity;
            input.App["104"] = appData.aBState;
            input.App["154"] = appData.aCState;
            input.App["105"] = appData.aBZip;
            input.App["155"] = appData.aCZip;
            input.App["100"] = appData.aBFirstNm;
            input.App["150"] = appData.aCFirstNm;
            input.App["101"] = appData.aBLastNm;
            input.App["116"] = appData.aBLastNm;
            input.App["151"] = appData.aCLastNm;
            input.App["106"] = appData.aBHPhone;
            input.App["156"] = appData.aCHPhone;
            input.App["136"] = appData.aBBusPhone;
            input.App["186"] = appData.aCBusPhone;
            input.App["107"] = appData.aBFax;
            input.App["157"] = appData.aCFax;
            input.App["108"] = appData.aBSsn;
            input.App["158"] = appData.aCSsn;
            input.App["112"] = appData.aBEmail;
            input.App["162"] = appData.aCEmail;
            input.App["600"] = appData.aBBaseI_rep;
            input.App["650"] = appData.aCBaseI_rep;
            input.App["601"] = appData.aBOvertimeI_rep;
            input.App["651"] = appData.aCOvertimeI_rep;
            input.App["602"] = appData.aBBonusesI_rep;
            input.App["652"] = appData.aCBonusesI_rep;
            input.App["603"] = appData.aBCommisionI_rep;
            input.App["653"] = appData.aCCommisionI_rep;
            input.App["604"] = appData.aBDividendI_rep;
            input.App["654"] = appData.aCDividendI_rep;
            input.App["605"] = appData.aBNetRentI1003_rep;
            input.App["655"] = appData.aCNetRentI1003_rep;


            // Per OPM 60794. LQB can have unlimited amt of other incomes,
            // but point has 3 spots.
            // If there are 3 or fewer others in LQB, go one to one.
            // Otherwise, borrower gets number one spot, coborrower gets two.


            int incomeCount = 0;
            foreach (var otherIncome in appData.aOtherIncomeList)
            {
                if (appData.aOtherIncomeList.Count <= 3)
                {
                    // Because there are few enough incomes, can go one-to-one.
                    switch (incomeCount)
                    {
                        case 0:
                            input.App["1230"] = otherIncome.IsForCoBorrower ? "C" : "B";
                            input.App["1231"] = otherIncome.Desc;
                            input.App["1232"] = appData.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep);
                            break;
                        case 1:
                            input.App["1233"] = otherIncome.IsForCoBorrower ? "C" : "B";
                            input.App["1234"] = otherIncome.Desc;
                            input.App["1235"] = appData.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep);
                            break;
                        case 2:
                            input.App["1236"] = otherIncome.IsForCoBorrower ? "C" : "B";
                            input.App["1237"] = otherIncome.Desc;
                            input.App["1238"] = appData.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep);
                            break;
                    }
                    incomeCount++;
                }
                else
                {
                    // Too many incomes for point. Borrower gets number one spot, coborrower gets the other.
                    if (otherIncome.IsForCoBorrower == false)
                    {
                        input.App["1230"] = "B";
                        input.App["1231"] = string.IsNullOrEmpty(input.App["1231"]) ? otherIncome.Desc : input.App["1231"] + "; " + otherIncome.Desc;
                        input.App["1232"] = string.IsNullOrEmpty(input.App["1231"]) ?
                            appData.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep)
                            : appData.m_convertLos.ToMoneyString ( appData.m_convertLos.ToDecimal(input.App["1231"]) + otherIncome.Amount,FormatDirection.ToRep );
                    }
                    else
                    {
                        input.App["1233"] = "C";
                        input.App["1234"] = string.IsNullOrEmpty(input.App["1234"]) ? otherIncome.Desc : input.App["1234"] + "; " + otherIncome.Desc;
                        input.App["1235"] = string.IsNullOrEmpty(input.App["1235"]) ?
                            appData.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep)
                            : appData.m_convertLos.ToMoneyString(appData.m_convertLos.ToDecimal(input.App["1235"]) + otherIncome.Amount, FormatDirection.ToRep);
                    }
                }

            }

            input.App["607"] = appData.aBTotOI_rep;
            input.App["657"] = appData.aCTotOI_rep;

            input.App["700"] = appData.aPresRent_rep;
            input.App["701"] = appData.aPres1stM_rep;
            input.App["702"] = appData.aPresOFin_rep;
            input.App["703"] = appData.aPresHazIns_rep;
            input.App["704"] = appData.aPresRealETx_rep;
            input.App["705"] = appData.aPresMIns_rep;
            input.App["706"] = appData.aPresHoAssocDues_rep;
            input.App["707"] = appData.aPresOHExp_rep;
            input.App["1181"] = appData.aSpouseIExcl ? "X" : "";

            input.App["109"] = appData.aBAge_rep;
            input.App["159"] = appData.aCAge_rep;
            input.App["110"] = appData.aBSchoolYrs_rep;
            input.App["160"] = appData.aCSchoolYrs_rep;

            // 0 - married, 1 - not married, 2 - separated
            switch (appData.aBMaritalStatT)
            {
                case DataAccess.E_aBMaritalStatT.Married:
                    input.App["144"] = "X";
                    break;
                case DataAccess.E_aBMaritalStatT.Separated:
                    input.App["145"] = "X";
                    break;
                case DataAccess.E_aBMaritalStatT.NotMarried:
                    input.App["146"] = "X";
                    break;
            }

            switch (appData.aCMaritalStatT)
            {
                case DataAccess.E_aCMaritalStatT.Married:
                    input.App["194"] = "X";
                    break;
                case DataAccess.E_aCMaritalStatT.Separated:
                    input.App["195"] = "X";
                    break;
                case DataAccess.E_aCMaritalStatT.NotMarried:
                    input.App["196"] = "X";
                    break;
            }

            // 0 - own, 1 - rent
            switch (appData.aBAddrT)
            {
                case DataAccess.E_aBAddrT.Own:
                    input.App["113"] = "X";
                    break;
                case DataAccess.E_aBAddrT.Rent:
                case DataAccess.E_aBAddrT.LivingRentFree:
                    input.App["114"] = "X";
                    break;
            }

            switch (appData.aCAddrT)
            {
                case DataAccess.E_aCAddrT.Own:
                    input.App["163"] = "X";
                    break;
                case DataAccess.E_aCAddrT.Rent:
                case DataAccess.E_aCAddrT.LivingRentFree:
                    input.App["164"] = "X";
                    break;
            }

            switch (appData.aBPrev1AddrT)
            {
                case DataAccess.E_aBPrev1AddrT.Own:
                    input.App["5500"] = "X";
                    break;
                case DataAccess.E_aBPrev1AddrT.Rent:
                case DataAccess.E_aBPrev1AddrT.LivingRentFree:
                    input.App["5501"] = "X";
                    break;
            }

            switch (appData.aCPrev1AddrT)
            {
                case E_aCPrev1AddrT.Own:
                    input.App["5520"] = "X";
                    break;
                case DataAccess.E_aCPrev1AddrT.Rent:
                case DataAccess.E_aCPrev1AddrT.LivingRentFree:
                    input.App["5521"] = "X";
                    break;
            }

            input.App["111"] = appData.aBAddrYrs;
            input.App["161"] = appData.aCAddrYrs;
            input.App["5502"] = appData.aBPrev1AddrYrs;
            input.App["5522"] = appData.aCPrev1AddrYrs;

            input.App["5503"] = appData.aBPrev1Addr;
            input.App["5504"] = Tools.CombineCityStateZip(appData.aBPrev1City, appData.aBPrev1State, appData.aBPrev1Zip);

            input.App["5523"] = appData.aCPrev1Addr;
            input.App["5524"] = Tools.CombineCityStateZip(appData.aCPrev1City, appData.aCPrev1State, appData.aCPrev1Zip);

            switch (appData.aBPrev2AddrT)
            {
                case DataAccess.E_aBPrev2AddrT.Own:
                    input.App["5510"] = "X";
                    break;
                case DataAccess.E_aBPrev2AddrT.Rent:
                case DataAccess.E_aBPrev2AddrT.LivingRentFree:
                    input.App["5511"] = "X";
                    break;
            }

            switch (appData.aCPrev2AddrT)
            {
                case DataAccess.E_aCPrev2AddrT.Own:
                    input.App["5530"] = "X";
                    break;
                case DataAccess.E_aCPrev2AddrT.Rent:
                case DataAccess.E_aCPrev2AddrT.LivingRentFree:
                    input.App["5531"] = "X";
                    break;
            }
            input.App["5512"] = appData.aBPrev2AddrYrs;
            input.App["5532"] = appData.aCPrev2AddrYrs;

            input.App["5513"] = appData.aBPrev2Addr;
            input.App["5514"] = Tools.CombineCityStateZip(appData.aBPrev2City, appData.aBPrev2State, appData.aBPrev2Zip);

            input.App["5533"] = appData.aCPrev2Addr;
            input.App["5534"] = Tools.CombineCityStateZip(appData.aCPrev2City, appData.aCPrev2State, appData.aCPrev2Zip);

            input.App["129"] = appData.aBDependNum_rep;
            input.App["179"] = appData.aCDependNum_rep;
            input.App["130"] = appData.aBDependAges;
            input.App["180"] = appData.aCDependAges;

            // borrower employment info
            /*
            DataSet ds = appData.aBEmplmtDataSet ;
            DataTable table = ds.Tables["EmplmtXmlContent"] ;
            DataRow row = table.NewRow() ;
            table.Rows.Add(row) ;
            */

            IPrimaryEmploymentRecord primaryRec = appData.aBEmpCollection.GetPrimaryEmp(false);
            if (null != primaryRec)
            {
                if (primaryRec.IsSelfEmplmt)
                    input.App["141"] = "X"; //row["IsSelfEmplmt"] 
                input.App["136"] = primaryRec.EmplrBusPhone; //row["EmplrBusPhone"] = 
                input.App["138"] = primaryRec.ProfLen_rep;//row["ProfLen"]
                input.App["135"] = primaryRec.JobTitle; //row["JobTitle"] =

                // These new fields already in affect after 4.3
                input.App["148"] = primaryRec.EmplrNm;
                input.App["149"] = primaryRec.EmplrAddr;
                input.App["140"] = primaryRec.EmplrCity;
                input.App["142"] = primaryRec.EmplrState;
                input.App["143"] = primaryRec.EmplrZip;
                // end new fields.

                input.App["131"] = primaryRec.EmplrNm; //row["EmplrNm"]
                input.App["137"] = primaryRec.EmplmtLen_rep; //row["EmplmtLen"]
                input.App["132"] = primaryRec.EmplrAddr; //row["EmplrAddr"] = 
                input.App["133"] = Tools.CombineCityStateZip(primaryRec.EmplrCity, primaryRec.EmplrState, primaryRec.EmplrZip);
            }
            // 1/19/2006 dd - Previous employment id are store in following order.
            //  self-employed, company name, company address, city, state, zip, start date, end date, monthly income, title, phone, is current
            string[,] borrowerFieldIds = {
                                            { "5540", "5220", "5221", "5222", "5223", "5224", "5546", "5547", "5548", "5544", "5545", "4116"  },
                                            { "5550", "5225", "5226", "5227", "5228", "5229", "5556", "5557", "5558", "5554", "5555", "4117"  },
                                            { "5580", "5230", "5231", "5232", "5233", "5234", "5586", "5587", "5588", "5584", "5585", "4118"  },
                                            { "5600", "5235", "5236", "5237", "5238", "5239", "5606", "5607", "5608", "5604", "5605", "4119"  },
                                            { "5610", "5240", "5241", "5242", "5243", "5244", "5616", "5617", "5618", "5614", "5615", "4120"  },
                                            { "5620", "5245", "5246", "5247", "5248", "5249", "5626", "5627", "5628", "5624", "5625", "4121"  },
                                            { "5630", "5250", "5251", "5252", "5253", "5254", "5636", "5637", "5638", "5634", "5635", "4122"  },
                                            { "5640", "5255", "5256", "5257", "5258", "5259", "5646", "5647", "5648", "5644", "5645", "4123"  }
                                            
                                         };
            string[,] coborrowerFieldIds = {
                                             { "5560", "5260", "5261", "5262", "5263", "5264", "5566", "5567", "5568", "5564", "5565", "4124"  },
                                             { "5570", "5265", "5266", "5267", "5268", "5269", "5576", "5577", "5578", "5574", "5575", "4125"  },
                                             { "5590", "5270", "5271", "5272", "5273", "5274", "5596", "5597", "5598", "5594", "5595", "4126"  },
                                             { "5650", "5275", "5276", "5277", "5278", "5279", "5656", "5657", "5658", "5654", "5655", "4127"  },
                                             { "5660", "5280", "5281", "5282", "5283", "5284", "5666", "5667", "5668", "5664", "5665", "4128"  },
                                             { "5670", "5285", "5286", "5287", "5288", "5289", "5676", "5677", "5678", "5674", "5675", "4129"  },
                                             { "5680", "5010", "5011", "5012", "5013", "5014", "5686", "5687", "5688", "5684", "5685", "4130"  },
                                             { "5690", "5015", "5016", "5017", "5018", "5019", "5696", "5697", "5698", "5694", "5695", "4131"  }
                                           };

            var empSubcoll = appData.aBEmpCollection.GetSubcollection(true, E_EmpGroupT.Previous);
            int nPrevCount = 0;
            //for( int iEmplnt = 0; iEmplnt < nBEmplmtRecordCount; ++iEmplnt )
            foreach (IRegularEmploymentRecord rec in empSubcoll)
            {
                if (8 <= nPrevCount)// 8 is Point's max
                    break;

                int iBase;
                switch (nPrevCount)
                {
                    case 0: iBase = 5540; break;
                    case 1: iBase = 5550; break;
                    case 2: iBase = 5580; break;
                    case 3: iBase = 5600; break;
                    case 4: iBase = 5610; break;
                    case 5: iBase = 5620; break;
                    case 6: iBase = 5630; break;
                    case 7: iBase = 5640; break;
                    default: throw new CBaseException(ErrorMessages.Generic, "Programming error:  Unexpected nPrevCount value for borrower employment record export.");
                }

                if (rec.IsSelfEmplmt)
                    input.App[iBase.ToString()] = "X";

                input.App[(iBase + 1).ToString()] = rec.EmplrNm;
                input.App[(iBase + 2).ToString()] = rec.EmplrAddr;
                input.App[(iBase + 3).ToString()] = Tools.CombineCityStateZip(rec.EmplrCity, rec.EmplrState, rec.EmplrZip);
                input.App[(iBase + 4).ToString()] = rec.JobTitle;
                input.App[(iBase + 5).ToString()] = rec.EmplrBusPhone;
                input.App[(iBase + 6).ToString()] = rec.EmplmtStartD_rep;
                input.App[(iBase + 7).ToString()] = rec.EmplmtEndD_rep;
                input.App[(iBase + 8).ToString()] = rec.MonI_rep;

                // Duplicate export using new ids just in case.

                input.App[borrowerFieldIds[nPrevCount, 1]] = rec.EmplrNm;
                input.App[borrowerFieldIds[nPrevCount, 2]] = rec.EmplrAddr;
                input.App[borrowerFieldIds[nPrevCount, 3]] = rec.EmplrCity;
                input.App[borrowerFieldIds[nPrevCount, 4]] = rec.EmplrState;
                input.App[borrowerFieldIds[nPrevCount, 5]] = rec.EmplrZip;
                input.App[borrowerFieldIds[nPrevCount, 6]] = rec.EmplmtStartD_rep;
                input.App[borrowerFieldIds[nPrevCount, 7]] = rec.EmplmtEndD_rep;
                input.App[borrowerFieldIds[nPrevCount, 8]] = rec.MonI_rep;
                input.App[borrowerFieldIds[nPrevCount, 9]] = rec.JobTitle;
                input.App[borrowerFieldIds[nPrevCount, 10]] = rec.EmplrBusPhone;
                input.App[borrowerFieldIds[nPrevCount, 11]] = rec.IsCurrent ? "X" : "";

                nPrevCount++;

            }

            input.App["5549"] = (0 == nPrevCount) ? "1" : nPrevCount.ToString();

            primaryRec = appData.aCEmpCollection.GetPrimaryEmp(false);
            if (null != primaryRec)
            {
                if (primaryRec.IsSelfEmplmt)
                    input.App["191"] = "X"; //row["IsSelfEmplmt"] 
                input.App["186"] = primaryRec.EmplrBusPhone; //row["EmplrBusPhone"] = 
                input.App["188"] = primaryRec.ProfLen_rep;//row["ProfLen"]
                input.App["185"] = primaryRec.JobTitle; //row["JobTitle"] =

                // These new fields already in affect after 4.3
                input.App["198"] = primaryRec.EmplrNm;
                input.App["199"] = primaryRec.EmplrAddr;
                input.App["190"] = primaryRec.EmplrCity;
                input.App["192"] = primaryRec.EmplrState;
                input.App["193"] = primaryRec.EmplrZip;
                // end new fields

                input.App["181"] = primaryRec.EmplrNm; //row["EmplrNm"]
                input.App["187"] = primaryRec.EmplmtLen_rep; //row["EmplmtLen"]
                input.App["182"] = primaryRec.EmplrAddr; //row["EmplrAddr"] = 
                input.App["183"] = Tools.CombineCityStateZip(primaryRec.EmplrCity, primaryRec.EmplrState, primaryRec.EmplrZip);
            }

            empSubcoll = appData.aCEmpCollection.GetSubcollection(true, E_EmpGroupT.Previous);
            nPrevCount = 0;
            foreach (IRegularEmploymentRecord rec in empSubcoll)
            {
                if (8 <= nPrevCount) // 8 is Point's max	
                    break;

                int iBase;
                switch (nPrevCount)
                {
                    case 0: iBase = 5560; break;
                    case 1: iBase = 5570; break;
                    case 2: iBase = 5590; break;
                    case 3: iBase = 5650; break;
                    case 4: iBase = 5660; break;
                    case 5: iBase = 5670; break;
                    case 6: iBase = 5680; break;
                    case 7: iBase = 5690; break;
                    default: throw new CBaseException(ErrorMessages.Generic, "Programming error:  Unexpected nPrevCount value for coborrower employment record export.");
                }

                if (rec.IsSelfEmplmt)
                    input.App[iBase.ToString()] = "X";

                input.App[(iBase + 1).ToString()] = rec.EmplrNm;
                input.App[(iBase + 2).ToString()] = rec.EmplrAddr;
                input.App[(iBase + 3).ToString()] = Tools.CombineCityStateZip(rec.EmplrCity, rec.EmplrState, rec.EmplrZip);
                input.App[(iBase + 4).ToString()] = rec.JobTitle;
                input.App[(iBase + 5).ToString()] = rec.EmplrBusPhone;
                input.App[(iBase + 6).ToString()] = rec.EmplmtStartD_rep;
                input.App[(iBase + 7).ToString()] = rec.EmplmtEndD_rep;
                input.App[(iBase + 8).ToString()] = rec.MonI_rep;

                // Duplicate export using new ids just in case.

                input.App[coborrowerFieldIds[nPrevCount, 1]] = rec.EmplrNm;
                input.App[coborrowerFieldIds[nPrevCount, 2]] = rec.EmplrAddr;
                input.App[coborrowerFieldIds[nPrevCount, 3]] = rec.EmplrCity;
                input.App[coborrowerFieldIds[nPrevCount, 4]] = rec.EmplrState;
                input.App[coborrowerFieldIds[nPrevCount, 5]] = rec.EmplrZip;
                input.App[coborrowerFieldIds[nPrevCount, 6]] = rec.EmplmtStartD_rep;
                input.App[coborrowerFieldIds[nPrevCount, 7]] = rec.EmplmtEndD_rep;
                input.App[coborrowerFieldIds[nPrevCount, 8]] = rec.MonI_rep;
                input.App[coborrowerFieldIds[nPrevCount, 9]] = rec.JobTitle;
                input.App[coborrowerFieldIds[nPrevCount, 10]] = rec.EmplrBusPhone;
                input.App[coborrowerFieldIds[nPrevCount, 11]] = rec.IsCurrent ? "X" : "";

                nPrevCount++;

            }

            input.App["5569"] = (0 == nPrevCount) ? "1" : nPrevCount.ToString();

            input.App["1360"] = appData.aAltNm1;
            input.App["1362"] = appData.aAltNm1AccNum.Value;
            input.App["1361"] = appData.aAltNm1CreditorNm;

            input.App["1363"] = appData.aAltNm2;
            input.App["1365"] = appData.aAltNm2AccNum.Value;
            input.App["1364"] = appData.aAltNm2CreditorNm;

            // borrower's declarations
            input.App["1465"] = appData.aBDecJudgment;
            input.App["1477"] = appData.aCDecJudgment;
            input.App["1466"] = appData.aBDecBankrupt;
            input.App["1478"] = appData.aCDecBankrupt;
            input.App["1467"] = appData.aBDecForeclosure;
            input.App["1479"] = appData.aCDecForeclosure;
            input.App["1468"] = appData.aBDecLawsuit;
            input.App["1480"] = appData.aCDecLawsuit;
            input.App["1469"] = appData.aBDecObligated;
            input.App["1481"] = appData.aCDecObligated;
            input.App["1470"] = appData.aBDecDelinquent;
            input.App["1482"] = appData.aCDecDelinquent;
            input.App["1471"] = appData.aBDecAlimony;
            input.App["1483"] = appData.aCDecAlimony;
            input.App["1472"] = appData.aBDecBorrowing;
            input.App["1484"] = appData.aCDecBorrowing;
            input.App["1473"] = appData.aBDecEndorser;
            input.App["1485"] = appData.aCDecEndorser;
            input.App["1474"] = appData.aBDecCitizen;
            input.App["1486"] = appData.aCDecCitizen;
            input.App["1475"] = appData.aBDecResidency;
            input.App["1487"] = appData.aCDecResidency;
            input.App["1476"] = appData.aBDecOcc;
            input.App["1488"] = appData.aCDecOcc;
            input.App["1620"] = appData.aBDecPastOwnership;
            input.App["1623"] = appData.aCDecPastOwnership;

            input.App["1489"] = appData.aBNoFurnish ? "X" : "";
            input.App["1497"] = appData.aCNoFurnish ? "X" : "";

            switch (appData.aBRaceT)
            {
                case E_aBRaceT.AmericanIndian:
                    input.App["1490"] = "X";
                    break;
                case E_aBRaceT.Asian:
                    input.App["1491"] = "X";
                    break;
                case E_aBRaceT.Black:
                    input.App["1492"] = "X";
                    break;
                case E_aBRaceT.Hispanic:
                    input.App["1493"] = "X";
                    break;
                case E_aBRaceT.White:
                    input.App["1494"] = "X";
                    break;
                case E_aBRaceT.Other:
                    input.App["1628"] = "X";
                    break;
            }
            input.App["1629"] = appData.aBORaceDesc;

            switch (appData.aCRaceT)
            {
                case E_aCRaceT.AmericanIndian:
                    input.App["1498"] = "X";
                    break;
                case E_aCRaceT.Asian:
                    input.App["1499"] = "X";
                    break;
                case E_aCRaceT.Black:
                    input.App["1500"] = "X";
                    break;
                case E_aCRaceT.Hispanic:
                    input.App["1501"] = "X";
                    break;
                case E_aCRaceT.White:
                    input.App["1502"] = "X";
                    break;
                case E_aCRaceT.Other:
                    input.App["1630"] = "X";
                    break;
            }
            input.App["1631"] = appData.aCORaceDesc;

            switch (appData.aBGenderFallback)
            {
                case E_GenderT.Male:
                    input.App["1496"] = "X";
                    break;
                case E_GenderT.Female:
                    input.App["1495"] = "X";
                    break;
            }

            switch (appData.aCGenderFallback)
            {
                case E_GenderT.Male:
                    input.App["1504"] = "X";
                    break;
                case E_GenderT.Female:
                    input.App["1503"] = "X";
                    break;
            }

            switch (appData.aBDecPastOwnedPropT)
            {
                case E_aBDecPastOwnedPropT.IP: input.App["1621"] = "IP"; break;
                case E_aBDecPastOwnedPropT.PR: input.App["1621"] = "PR"; break;
                case E_aBDecPastOwnedPropT.SH: input.App["1621"] = "SH"; break;
                default: input.App["1621"] = ""; break;
            }
            switch (appData.aBDecPastOwnedPropTitleT)
            {
                case E_aBDecPastOwnedPropTitleT.O: input.App["1622"] = "O"; break;
                case E_aBDecPastOwnedPropTitleT.S: input.App["1622"] = "S"; break;
                case E_aBDecPastOwnedPropTitleT.SP: input.App["1622"] = "SP"; break;
                default: input.App["1622"] = ""; break;
            }

            switch (appData.aCDecPastOwnedPropT)
            {
                case E_aCDecPastOwnedPropT.IP: input.App["1624"] = "IP"; break;
                case E_aCDecPastOwnedPropT.PR: input.App["1624"] = "PR"; break;
                case E_aCDecPastOwnedPropT.SH: input.App["1624"] = "SH"; break;
                default: input.App["1624"] = ""; break;
            }
            switch (appData.aCDecPastOwnedPropTitleT)
            {
                case E_aCDecPastOwnedPropTitleT.O: input.App["1625"] = "O"; break;
                case E_aCDecPastOwnedPropTitleT.S: input.App["1625"] = "S"; break;
                case E_aCDecPastOwnedPropTitleT.SP: input.App["1625"] = "SP"; break;
                default: input.App["1625"] = ""; break;
            }

            switch (appData.aIntrvwrMethodT)
            {
                case E_aIntrvwrMethodT.FaceToFace: input.App["1505"] = "X"; break;
                case E_aIntrvwrMethodT.ByMail: input.App["1506"] = "X"; break;
                case E_aIntrvwrMethodT.ByTelephone: input.App["1507"] = "X"; break;
            }

            input.App["4900"] = appData.a1003ContEditSheet;


            // assets
            /*
                asset-type
                0 - auto
                1 - bonds
                2 - business
                3 - checking
                4 - gift funds
                5 - life insurance
                6 - retirement
                7 - savings
                8 - stocks
                9 - other
             */
            input.App["1375"] = "";
            input.App["1376"] = "";
            input.App["1378"] = "";
            input.App["1379"] = "";
            input.App["1380"] = "";
            input.App["1366"] = "";
            input.App["1367"] = "";
            input.App["1368"] = "";
            input.App["1369"] = "";
            input.App["1387"] = "";
            input.App["1388"] = "";
            input.App["1374"] = "";
            input.App["1370"] = "";

            int nOffset = 0;
            int iChecking = 0, iStocks = 0, iAuto = 0, iOther = 0, iCashDeposit = 0;
            decimal otherCashDeposit = 0;
            var assetColl = appData.aAssetCollection.GetSubcollection(true, E_AssetGroupT.All);

            bool bJoint = false;
            foreach (var item in assetColl)
            {
                var fields = (IAsset)item;
                //var fields = ( i < countAssetRegular ) ? assetColl.GetRegularRecordAt( i ) : assetColl.GetSpecialRecordAt( i );
                if (E_AssetOwnerT.Joint == fields.OwnerT)
                    bJoint = true;
                bool bOther = false;
                switch (fields.AssetT)
                {
                    case E_AssetT.Auto:
                        switch (iAuto++)
                        {
                            case 0:
                                input.App["1375"] = fields.Desc;
                                input.App["1376"] = fields.Val_rep;
                                break;
                            case 1:
                                input.App["1377"] = fields.Desc;
                                input.App["1378"] = fields.Val_rep;
                                break;
                            case 2:
                                input.App["1379"] = fields.Desc;
                                input.App["1380"] = fields.Val_rep;
                                break;
                            default:
                                bOther = true;
                                break;
                        }
                        break;
                    case E_AssetT.Bonds:
                    case E_AssetT.Stocks:
                        switch (iStocks++)
                        {
                            case 0:
                                input.App["1366"] = fields.Desc;
                                input.App["1367"] = fields.Val_rep;
                                break;
                            case 1:
                                input.App["1368"] = fields.Desc;
                                input.App["1369"] = fields.Val_rep;
                                break;
                            case 2:
                                input.App["1387"] = fields.Desc;
                                input.App["1388"] = fields.Val_rep;
                                break;
                            default:
                                bOther = true;
                                break;
                        }
                        break;
                    case E_AssetT.Business:
                        input.App["1374"] = fields.Val_rep;
                        break;
                    case E_AssetT.CashDeposit:
                        {
                            switch (iCashDeposit++)
                            {
                                case 0:
                                    input.App["1290"] = fields.Desc;
                                    input.App["1291"] = fields.Val_rep;
                                    break;
                                case 1:
                                    input.App["1292"] = fields.Desc;
                                    input.App["1293"] = fields.Val_rep;
                                    otherCashDeposit += fields.Val;
                                    break;
                                default:
                                    input.App["1292"] = input.App["1292"] + "+" + fields.Desc;
                                    input.App["1293"] = loanData.m_convertLos.ToMoneyString(otherCashDeposit + fields.Val, FormatDirection.ToRep);
                                    break;
                            }
                            break;
                        }
                    case E_AssetT.OtherLiquidAsset:
                    case E_AssetT.GiftFunds:
                    case E_AssetT.Checking:
                    case E_AssetT.Savings:
                    case E_AssetT.GiftEquity:
                        var regRec = fields as IAssetRegular;

                        if (iChecking < 20)
                        {
                            if (iChecking < 12)
                                nOffset = iChecking * 6 + 1294;
                            else
                                nOffset = (iChecking - 12) * 6 + 3920;

                            string typeDesc = "";
                            switch (regRec.AssetT)
                            {
                                case E_AssetRegularT.Checking:
                                    typeDesc = "Checking Account"; break;
                                case E_AssetRegularT.Savings:
                                    typeDesc = "Savings"; break;
                                case E_AssetRegularT.GiftFunds:
                                    typeDesc = "Gift"; break;
                                case E_AssetRegularT.GiftEquity:
                                    typeDesc = "Gift of Equity"; break;
                                case E_AssetRegularT.OtherLiquidAsset:
                                    typeDesc = regRec.OtherTypeDesc; break;

                                default:
                                    Tools.LogError("Programming error during exporting asset, unhandled liquid asset enum type for desc");
                                    break;
                            }
                            input.App[(8880 + iChecking).ToString()] = typeDesc;

                            input.App[nOffset.ToString()] = regRec.ComNm;
                            input.App[(nOffset + 1).ToString()] = regRec.StAddr;
                            input.App[(nOffset + 2).ToString()] = Tools.CombineCityStateZip(regRec.City, regRec.State, regRec.Zip);
                            //input.App[(nOffset+3).ToString()] = xxx ; -- not used
                            input.App[(nOffset + 4).ToString()] = regRec.AccNum.Value;
                            input.App[(nOffset + 5).ToString()] = regRec.Val_rep;

                            iChecking++;
                        }
                        else
                            bOther = true;

                        break;
                    case E_AssetT.Retirement:
                        input.App["1373"] = fields.Val_rep;
                        break;
                    case E_AssetT.LifeInsurance:
                        var lifeIns = fields as IAssetLifeInsurance;
                        input.App["1370"] = lifeIns.FaceVal_rep;
                        input.App["1371"] = lifeIns.Val_rep;
                        break;
                    case E_AssetT.OtherIlliquidAsset:
                    default:
                        bOther = true;
                        break;
                }

                if (bOther)
                {
                    switch (iOther++)
                    {
                        case 0:
                            input.App["1381"] = fields.Desc;
                            input.App["1382"] = fields.Val_rep;
                            break;
                        case 1:
                            input.App["1383"] = fields.Desc;
                            input.App["1384"] = fields.Val_rep;
                            break;
                        case 2:
                            input.App["1385"] = fields.Desc;
                            input.App["1386"] = fields.Val_rep;
                            break;
                        case 3:
                            input.App["1398"] = fields.Desc;
                            input.App["1399"] = fields.Val_rep;
                            break;
                    }
                }
            }

            input.App["1280"] = ((iChecking > 0) ? iChecking : 1).ToString();

            if (bJoint)
                input.App["1183"] = "X";	// filed jointly or not

            // real estate assets

            // 9/7/2005 dd - Point does not store 25 REO record in consecutive order, the first 10 records are from 2900 - 2989, next 5 is 1400 - 1459, and last 10 is 3600 - 3699
            int[] reStartIds = { 2900, 2910, 2920, 2930, 2940, 2950, 2960, 2970, 2980, 1400, 1410, 1420, 1430, 1440, 1450, 3600, 3610, 3620, 3630, 3640, 3650, 3660, 3670, 3680, 3690 };

            var reSubcoll = appData.aReCollection.GetSubcollection(true, E_ReoGroupT.All);
            List<Guid> reos = new List<Guid>();
            int nCount = reSubcoll.Count;
            int iReExported = 0;
            foreach (var item in reSubcoll)
            {
                var re = (IRealEstateOwned)item;
                if (iReExported >= reStartIds.Length)
                    break; // too many ... time to quit while we're still ahead.
                nOffset = reStartIds[iReExported];
                input.App[nOffset.ToString()] = re.Addr;
                input.App[(nOffset + 1).ToString()] = Tools.CombineCityStateZip(re.City, re.State, re.Zip);	//1 Property Address

                input.App[(nOffset + 2).ToString()] = re.Stat;	//2 S/PS/R

                input.App[(nOffset + 3).ToString()] = re.Type;		//3 Type of Property
                input.App[(nOffset + 4).ToString()] = re.Val_rep;	//4 Present Market Value
                input.App[(nOffset + 5).ToString()] = re.MAmt_rep;	//5 Amount of Mortgages and Liens
                input.App[(nOffset + 6).ToString()] = re.GrossRentI_rep;	//6 Gross Rental Income
                input.App[(nOffset + 7).ToString()] = re.MPmt_rep;	//7 Mortgage Payments
                input.App[(nOffset + 8).ToString()] = re.HExp_rep;	//8 Insurance, Maintenance, Taxes and Misc.
                input.App[(nOffset + 9).ToString()] = re.NetRentI_rep;	//9 Net Rental Income

                // only applies to the first real estate entry
                if (0 == iReExported)
                    input.App["2849"] = re.OccR_rep;
                iReExported++;
                reos.Add(re.RecordId);
            }

            input.App["1282"] = ((iReExported > 0) ? iReExported : 1).ToString();


            // liability information

            ILiaCollection liaRecordList = appData.aLiaCollection;
            var alimony = liaRecordList.GetAlimony(false);
            if (null != alimony)
            {
                input.App["1390"] = alimony.OwedTo;
                input.App["1391"] = alimony.Pmt_repPoint;
                input.App["1462"] = alimony.RemainMons_rep;
            }
            var jobRelated1 = liaRecordList.GetJobRelated1(false);
            if (null != jobRelated1)
            {
                input.App["1392"] = jobRelated1.ExpenseDesc;
                input.App["1393"] = jobRelated1.Pmt_repPoint;
            }

            var jobRelated2 = liaRecordList.GetJobRelated2(false);
            if (null != jobRelated2)
            {
                input.App["1394"] = jobRelated2.ExpenseDesc;
                input.App["1395"] = jobRelated2.Pmt_repPoint;
            }

            nCount = liaRecordList.CountRegular;
            if (nCount > 40) { nCount = 40; } //fields 4500 up only supports 40 liabilities.
            input.App["1281"] = ((nCount > 0) ? nCount : 1).ToString();

            for (int i = 0; i < nCount; i++)
            {
                var liability = liaRecordList.GetRegularRecordAt(i);

                nOffset = 4500 + i * 10;
                //input.App[(nOffset-2).ToString()] = liability.Desc;
                input.App[nOffset.ToString()] = liability.ComNm;
                input.App[(nOffset + 1).ToString()] = liability.ComAddr;
                input.App[(nOffset + 2).ToString()] = Tools.CombineCityStateZip(liability.ComCity, liability.ComState, liability.ComZip);
                input.App[(nOffset + 4).ToString()] = liability.AccNum.Value;

                switch (liability.DebtT)
                {
                    case E_DebtRegularT.Revolving:
                        input.App[(nOffset + 5).ToString()] = "R";
                        break;
                    case E_DebtRegularT.Mortgage:
                        input.App[(nOffset + 5).ToString()] = "M";
                        break;
                    case E_DebtRegularT.Installment:
                        input.App[(nOffset + 5).ToString()] = "L";
                        break;
                    default:
                        // do nothing
                        break;
                }
                if (liability.MatchedReRecordId != Guid.Empty && reos.Contains(liability.MatchedReRecordId))
                {                                                                     //REOs have an ID starting at 1. It is pretty much the location of where we store them.
                    input.App[(3850 + i).ToString()] = (reos.IndexOf(liability.MatchedReRecordId)+1).ToString(); 
                }
                if (liability.WillBePdOff)
                {
                    input.App[(nOffset + 6).ToString()] = "X";
                }
                
                input.App[(nOffset + 7).ToString()] = liability.Bal_rep;

                input.App[(nOffset + 8).ToString()] = liability.Pmt_repPoint;
                input.App[(nOffset + 9).ToString()] = liability.RemainMons_rep;
                // 7/26/2005 dd - Export our liability description to POINT description.
                // Point description is 24 characters or less.
                input.App[(8910 + i).ToString()] = liability.Desc;

            

            }


            if (appData.aDenialNoCreditFile)
                input.App["3800"] = "X";

            input.App["3800"] = appData.aDenialNoCreditFile ? "X" : "";
            input.App["3801"] = appData.aDenialInsufficientCreditRef ? "X" : "";
            input.App["3802"] = appData.aDenialInsufficientCreditFile ? "X" : "";
            input.App["3803"] = appData.aDenialUnableVerifyCreditRef ? "X" : "";
            input.App["3804"] = appData.aDenialGarnishment ? "X" : "";
            input.App["3805"] = appData.aDenialExcessiveObligations ? "X" : "";
            input.App["3806"] = appData.aDenialInsufficientIncome ? "X" : "";
            input.App["3807"] = appData.aDenialUnacceptablePmtRecord ? "X" : "";
            input.App["3808"] = appData.aDenialLackOfCashReserves ? "X" : "";
            input.App["3809"] = appData.aDenialDeliquentCreditObligations ? "X" : "";
            input.App["3810"] = appData.aDenialBankruptcy ? "X" : "";
            input.App["3811"] = appData.aDenialInfoFromConsumerReportAgency ? "X" : "";
            input.App["3812"] = appData.aDenialUnableVerifyEmployment ? "X" : "";
            input.App["3813"] = appData.aDenialLenOfEmployment ? "X" : "";
            input.App["3814"] = appData.aDenialTemporaryEmployment ? "X" : "";
            input.App["3815"] = appData.aDenialInsufficientIncomeForMortgagePmt ? "X" : "";
            input.App["3816"] = appData.aDenialUnableVerifyIncome ? "X" : "";
            input.App["3817"] = appData.aDenialTempResidence ? "X" : "";
            input.App["3818"] = appData.aDenialShortResidencePeriod ? "X" : "";
            input.App["3819"] = appData.aDenialUnableVerifyResidence ? "X" : "";
            input.App["3821"] = appData.aDenialByHUD ? "X" : "";
            input.App["3822"] = appData.aDenialByVA ? "X" : "";
            input.App["3823"] = appData.aDenialByFedNationalMortAssoc ? "X" : "";
            input.App["3824"] = appData.aDenialByFedHomeLoanMortCorp ? "X" : "";
            input.App["3825"] = appData.aDenialByOther ? "X" : "";
            input.App["3839"] = appData.aDenialByOtherDesc;
            input.App["3826"] = appData.aDenialInsufficientFundsToClose ? "X" : "";
            input.App["3827"] = appData.aDenialCreditAppIncomplete ? "X" : "";
            input.App["3828"] = appData.aDenialInadequateCollateral ? "X" : "";
            input.App["3829"] = appData.aDenialUnacceptableProp ? "X" : "";
            input.App["3830"] = appData.aDenialInsufficientPropData ? "X" : "";
            input.App["3831"] = appData.aDenialUnacceptableAppraisal ? "X" : "";
            input.App["3832"] = appData.aDenialUnacceptableLeasehold ? "X" : "";
            input.App["3833"] = appData.aDenialNotGrantCreditToAnyAppOnSuchTermsAndConds ? "X" : "";
            input.App["3834"] = appData.aDenialWithdrawnByApp ? "X" : "";
            input.App["3846"] = appData.aDenialOtherReason1 ? "X" : "";
            input.App["3847"] = appData.aDenialOtherReason1Desc;
            input.App["3845"] = appData.aDenialOtherReason2 ? "X" : "";
            input.App["3848"] = appData.aDenialOtherReason2Desc;
            input.App["3837"] = appData.aDenialDecisionBasedOnReportAgency ? "X" : "";
            input.App["3838"] = appData.aDenialDecisionBasedOnCRA ? "X" : "";
            input.App["3790"] = appData.aDenialHasAdditionalStatement ? "X" : "";

            string denialStatement = appData.aDenialAdditionalStatement.TrimWhitespaceAndBOM();
            if (denialStatement.Length > 0)
            {
                string[] pieces = Split(denialStatement, 74, 3796 - 3791 + 1);
                for (int iPiece = 0; iPiece < pieces.Length; ++iPiece)
                {
                    input.App[(3791 + iPiece).ToString("D")] = pieces[iPiece];
                }
            }

            input.App["6305"] = appData.aCrOd_rep;
            input.App["6306"] = appData.aCrRd_rep;
            input.App["6307"] = appData.aCrN;
            input.App["6310"] = appData.aBusCrOd_rep;
            input.App["6311"] = appData.aBusCrRd_rep;
            input.App["6312"] = appData.aBusCrN;

            input.App["426"] = appData.a1003SignD_rep;

            input.App["421"] = loanData.sApp1003InterviewerPrepareDate_rep; // make sure all 1003 in Point has the same date.
            if (appData.aAsstLiaCompletedNotJointly)
                input.App["1184"] = "X";
            else
                input.App["1183"] = "X";

            input.App["5032"] = appData.aBExperianScore_rep;
            input.App["5033"] = appData.aCExperianScore_rep;
            input.App["5034"] = appData.aBTransUnionScore_rep;
            input.App["5035"] = appData.aCTransUnionScore_rep;
            input.App["5036"] = appData.aBEquifaxScore_rep;
            input.App["5037"] = appData.aCEquifaxScore_rep;

            //av 22583 jun 10
            input.App["11393"] = appData.aBExperianCreatedD_rep;
            input.App["11394"] = appData.aBExperianFactors;
            input.App["11395"] = appData.aCExperianCreatedD_rep;
            input.App["11396"] = appData.aCExperianFactors;

            input.App["11400"] = appData.aBTransUnionCreatedD_rep;
            input.App["11401"] = appData.aBTransUnionFactors;
            input.App["11402"] = appData.aCTransUnionCreatedD_rep;
            input.App["11403"] = appData.aCTransUnionFactors;

            input.App["11407"] = appData.aBEquifaxCreatedD_rep;
            input.App["11408"] = appData.aBEquifaxFactors;
            input.App["11409"] = appData.aCEquifaxCreatedD_rep;
            input.App["11410"] = appData.aCEquifaxFactors;
            //av 22583 jun 10

            input.App["5027"] = appData.aBMinFicoScore_rep;
            input.App["5028"] = appData.aCMinFicoScore_rep;
            input.App["860"] = appData.aOIFrom1008Desc;
            input.App["861"] = appData.aBOIFrom1008_rep;
            input.App["862"] = appData.aCOIFrom1008_rep;
            input.App["863"] = appData.aTotOIFrom1008_rep;
            input.App["836"] = (appData.aOpNegCfLckd || appData.aSpNegCfLckd) ? "X" : "";

            input.App["118"] = appData.aBDob_rep;
            input.App["168"] = appData.aCDob_rep;
            input.App["117"] = appData.aBMidNm;
            input.App["167"] = appData.aCMidNm;
            input.App["119"] = appData.aBSuffix;
            input.App["169"] = appData.aCSuffix;
            input.App["204"] = appData.aBAddrMailUsePresentAddr ? "X" : "";
            input.App["224"] = appData.aCAddrMailUsePresentAddr ? "X" : "";

            input.App["200"] = appData.aBAddrMail;
            input.App["220"] = appData.aCAddrMail;
            input.App["201"] = appData.aBCityMail;
            input.App["221"] = appData.aCCityMail;
            input.App["202"] = appData.aBStateMail;
            input.App["222"] = appData.aCStateMail;
            input.App["203"] = appData.aBZipMail;
            input.App["223"] = appData.aCZipMail;
            input.App["139"] = appData.aBCellPhone;
            input.App["189"] = appData.aCCellPhone;

            input.App["2823"] = appData.aBIsAmericanIndian ? "X" : "";
            input.App["2824"] = appData.aBIsPacificIslander ? "X" : "";
            input.App["2825"] = appData.aBIsAsian ? "X" : "";
            input.App["2826"] = appData.aBIsWhite ? "X" : "";
            input.App["2827"] = appData.aBIsBlack ? "X" : "";
            input.App["2831"] = appData.aCIsAmericanIndian ? "X" : "";
            input.App["2832"] = appData.aCIsPacificIslander ? "X" : "";
            input.App["2833"] = appData.aCIsAsian ? "X" : "";
            input.App["2834"] = appData.aCIsWhite ? "X" : "";
            input.App["2835"] = appData.aCIsBlack ? "X" : "";

            input.App["2821"] = "";
            input.App["2822"] = "";
            switch (appData.aBHispanicTFallback)
            {
                case E_aHispanicT.Hispanic:
                    input.App["2821"] = "X"; break;
                case E_aHispanicT.NotHispanic:
                    input.App["2822"] = "X"; break;
            }

            input.App["2829"] = "";
            input.App["2830"] = "";
            switch (appData.aCHispanicTFallback)
            {
                case E_aHispanicT.Hispanic:
                    input.App["2829"] = "X"; break;
                case E_aHispanicT.NotHispanic:
                    input.App["2830"] = "X"; break;
            }

            input.App["1222"] = appData.aTitleNm1;
            input.App["1223"] = appData.aTitleNm2;
            input.App["1224"] = appData.aManner;

            //CALCULATED FIELDS
            input.App["870"] = appData.aBSpPosCf_rep;
            input.App["871"] = appData.aCSpPosCf_rep;
            input.App["872"] = appData.aTotSpPosCf_rep;
            input.App["876"] = appData.aTransmBTotI_rep;
            input.App["879"] = appData.aTransmCTotI_rep;
            input.App["881"] = appData.aTransmTotI_rep;
            input.App["884"] = appData.aTransmProTotHExp_rep;
            input.App["875"] = appData.aSpNegCf_rep;
            input.App["882"] = appData.aOpNegCf_rep;
            input.App["1397"] = appData.aTransmOMonPmt_rep;
            input.App["883"] = appData.aTransmTotMonPmt_rep;
            input.App["542"] = appData.aQualTopR_rep;
            input.App["543"] = appData.aQualBottomR_rep;


            input.App["947"] = appData.aTotNonbaseI_rep;
            input.App["621"] = appData.aTotOvertimeI_rep;
            input.App["627"] = appData.aTotOI_rep;
            input.App["625"] = appData.aTotNetRentI1003_rep;
            input.App["630"] = appData.aTotI_rep;
            input.App["624"] = appData.aTotDividendI_rep;
            input.App["623"] = appData.aTotCommisionI_rep;
            input.App["622"] = appData.aTotBonusesI_rep;
            input.App["620"] = appData.aTotBaseI_rep;
            input.App["1389"] = appData.aAsstValTot_rep;
            input.App["1510"] = appData.aReTotVal_rep;
            input.App["1515"] = appData.aReTotNetRentI_rep;
            input.App["1513"] = appData.aReTotMPmt_rep;
            input.App["1511"] = appData.aReTotMAmt_rep;

            input.App["1514"] = appData.aReTotHExp_rep;
            input.App["1512"] = appData.aReTotGrossRentI_rep;
            input.App["710"] = appData.aPresTotHExp_rep;

            //input.App["20105"]	= appData.aNetWorth_rep; NOTE: Export to this field id or any id in the 20000 range will cause corruption msg from Point when it opens the result file.
            //input.App["1397"]	= appData.aLiaMonTot_rep; TODO-thinh: Watch out for this, Point displays 1397 in both per app and shared, bug in field displaying but the real data is still correct in Point.
            input.App["1396"] = appData.aLiaBalTot_rep;
            input.App["660"] = appData.aCTotI_rep;

            //input.App["874"]	= appData.aCNonbaseI_rep; NOTE: we don't support single app 1008 (Transmittal Summary)
            input.App["610"] = appData.aBTotI_rep;
            input.App["873"] = appData.aBNonbaseI_rep;
            input.App["874"] = appData.aCNonbaseI_rep;
            //	aAsstValTot
            //	aAsstNonReSolidTot
            input.App["1372"] = appData.aAsstLiqTot_rep;


            // 4/13/2007 nw - OPM 12435 - Export credit reference number
            SqlParameter[] param = { new SqlParameter("@ApplicationID", appData.aAppId) };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(loanData.sBrokerId, "RetrieveCreditReport", param))
            {
                if (reader.Read())
                {
                    input.Shared["3550"] = (string)reader["ExternalFileID"];
                }
            }


            #endregion // PER-APP DATA

            // debug code, comment out when not using
            /*
            NameValueCollection nvc = input.GetCollection();
            foreach (string key in nvc)
            {
                Tools.LogInfo("id[" + key + "]=" + nvc[key]);
            }*/
        }



        #region STATIC TOOLS

        /*
		internal void ExportBool( NameValueCollectionWrapper input, int fieldId, bool val )
		{
			ExportBool( input, fieldId.ToString("D"), val );
		}
		internal void ExportBool( NameValueCollectionWrapper input, string fieldId, bool val )
		{
			input[fieldId] = val ? "X" : "";
		}
		*/
        internal string[] Split(string s, int nGroupSize, int maxGroupNumber)
        {
            ArrayList result = new ArrayList(maxGroupNumber);

            int nExp = 0;
            int len = s.Length;
            while (nExp < len)
            {
                int start = nExp;
                nExp = Math.Min(nExp + nGroupSize, len);

                result.Add(s.Substring(start, nExp - start));
            }
            return (string[])result.ToArray(typeof(string));
        }

        internal void SplitAndAssignToPoint(CFieldsBase pointObj, int idStart, int idLast, string str)
        {
            string[] pieces = Split(str, 180, idLast - idStart + 1);
            for (int iPiece = 0; iPiece < pieces.Length; ++iPiece)
            {
                pointObj[(idStart + iPiece).ToString("D")] = pieces[iPiece];
            }
        }
        internal void SplitAndAssignToPoint(CFieldsBase pointObj, int[] ids, string str)
        {
            string[] pieces = Split(str, 180, ids.Length);
            for (int iPiece = 0; iPiece < pieces.Length; ++iPiece)
            {
                int pointId = ids[iPiece];
                pointObj[pointId.ToString("D")] = pieces[iPiece];
            }
        }

        private static string CombineFieldsToLines(CFieldsBase pointObj, int idStart, int idLast)
        {
            StringBuilder strBuilder = new StringBuilder(200);
            for (int iField = idStart; iField <= idLast; ++iField)
            {
                string val = pointObj[iField.ToString()];
                if ("" != val)
                {
                    if (strBuilder.Length > 0)
                        strBuilder.Append("\n");
                    strBuilder.Append(val);
                }
            }

            return strBuilder.ToString();
        }

        private static string CombineFieldsToLines(CFieldsBase pointObj, int[] ids)
        {
            StringBuilder strBuilder = new StringBuilder(200);
            for (int i = 0; i < ids.Length; ++i)
            {
                int iField = ids[i];
                string val = pointObj[iField.ToString()];
                if ("" != val)
                {
                    if (strBuilder.Length > 0)
                        strBuilder.Append("\n");
                    strBuilder.Append(val);
                }
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// return DateTime.MinValue if str cannot be converted to a valid datetime.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static DateTime FromStrToDate(string str)
        {
            try
            {
                return DateTime.Parse(str);
            }
            catch
            {
                return DateTime.MinValue;
            }
        }


        private static E_TriState FromXYesToTriState(string xYes, string xNo)
        {
            if ("X" == xYes)
                return E_TriState.Yes;
            if ("X" == xNo)
                return E_TriState.No;
            return E_TriState.Blank;
        }

        private static E_TriState FromXYesToTriState(string xYes)
        {
            if ("X" == xYes)
                return E_TriState.Yes;
            return E_TriState.No;
        }

        private static string FromTriStateToXYes(E_TriState triState)
        {
            if (E_TriState.Yes == triState)
                return "X";
            return "";
        }

        private static string FromTriStateToXNo(E_TriState triState)
        {
            if (E_TriState.No == triState)
                return "X";
            return "";
        }

        private static string FirstNonEmptyString(string s1, string s2)
        {
            if ("" != s1)
                return s1;
            return s2;
        }

        private static string FromBoolToXYes(bool bit)
        {
            return bit ? "X" : "";
        }

        /// <summary>
        /// There are no sCrFPfc, sCrFPbs, sCrFFa; just use sCrFProps with this function
        /// to assign value to it. Similar to other GFE items.
        ///			dataLoanObj.sCrFProps = m_convertLos.GfeItemProps_Pack( bool apr, bool toBr, int payer )
        ///	where:
        ///		PFC S 	F  		apr	br	payer
        ///		0	0	0		0	0	0
        ///		0	0	1		0	0	0 (FHA?)
        ///		0	1	0		0	0	2
        ///		0	1	1		0	0	2(x)
        ///		1	0	0		1	0	0
        ///		1	0	1		1	0	0 (FHA?)
        ///		1	1	0		1	0	2
        ///		1	1	1		1	0	2(x)
        ///		
        ///		(x) means "shouldn't happen", in the 2 cases, if the seller agrees to pay, I
        ///		don't think it make sense for the buyer to finance the charge.
        ///		
        ///		Note that Pfc is always the same as apr.
        ///		toBr always 0, Point doesn't allow users to specify payee or financing closing cost.
        /// </summary>
        static int ToGfeItemProps_Pack(CPointData loanData, string Pfc, string Pbs, string Fa, string Poc)
        {
            int payer;
            if ("X" == Pbs)
                payer = 2;
            /*  Point doesn't seem to support financing closing costs. Fa is for FHA Allowance.
            else if ("X" == Fa)
                payer = 1 ;
            */
            else
                payer = 0;

            return LosConvert.GfeItemProps_Pack("X" == Pfc, "X" == Pbs, payer, "X" == Fa, "X" == Poc);
        }

        private void AddBankAsset(CAppData dataApp, string sComNm, string sAccNum,
            string sAddress, string sCityStateZip, string sBalance, string assetType, bool bJoint)
        {
            if ("" == sComNm && "" == sBalance) return;

            var fields = dataApp.aAssetCollection.AddRegularRecord();
            switch (assetType.ToLower())
            {
                case "checking account":
                    fields.AssetT = E_AssetRegularT.Checking;
                    break;
                case "savings":
                    fields.AssetT = E_AssetRegularT.Savings;
                    break;
                case "gift":
                    fields.AssetT = E_AssetRegularT.GiftFunds;
                    break;
                case "gift of equity":
                    fields.AssetT = E_AssetRegularT.GiftEquity;
                    break;
                default:
                    fields.AssetT = E_AssetRegularT.OtherLiquidAsset;
                    fields.OtherTypeDesc = assetType;
                    break;
            }
            fields.ComNm = sComNm;
            fields.AccNum = sAccNum;
            fields.StAddr = sAddress;

            if (sCityStateZip.Length > 0)
            {
                m_addressParser.ParseCityStateZip(sCityStateZip);
                fields.City = m_addressParser.City;
                fields.State = m_addressParser.State;
                fields.Zip = m_addressParser.Zipcode;
            }
            fields.Val_rep = sBalance;


            // 0 - borrower, 1 - co-borrower, 2 - joint
            // default to borrower if necessary
            fields.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;

            string nm = dataApp.aBNm;

            if (bJoint)
            {
                if (dataApp.aCNm != "")
                    nm += " & " + dataApp.aCNm;
            }
            fields.AccNm = nm;
        }
        static void AddLifeInsAsset(CAppData dataApp, string sFaceValue, string sActualValue, bool bJoint)
        {
            if ("" == sFaceValue && "" == sActualValue) return;

            var fields = dataApp.aAssetCollection.GetLifeInsurance(true);
            fields.FaceVal_rep = sFaceValue;
            fields.Val_rep = sActualValue;
            fields.Desc = "Life Insurance";

            // 0 - borrower, 1 - co-borrower, 2 - joint
            // default to borrower if necessary
            fields.OwnerT = bJoint ? E_AssetOwnerT.Joint : E_AssetOwnerT.Borrower;
        }

        #endregion // STATIC TOOLS
    }
}
