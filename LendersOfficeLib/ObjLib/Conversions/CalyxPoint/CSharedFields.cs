﻿
namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    public class CSharedFields : CFieldsBase
    {

        internal CSharedFields(NameValueCollectionWrapper parent)
            : base(parent)
        { }
        public override string this[string sKey]
        {
            get { return m_parent.GetSharedField(sKey); }
            set { m_parent.SetSharedField(sKey, value); }
        }
        public override string this[int intKey]
        {
            get { return this[intKey.ToString()]; }
            set { this[intKey.ToString()] = value; }
        }
    }


}
