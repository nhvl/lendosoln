﻿namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// PointExportHelper provides utility methods for exporting Calyx Point files as a zip.
    /// </summary>
    public class PointExportHelper
    {
        /// <summary>
        /// Creates a string representation of a zip file containing every point file for the loan.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="fileName">The filename that the user specified.</param>
        /// <param name="extension">The extension that the user selected for the borrower file.</param>
        /// <returns>A string representation of the zip file.</returns>
        public static string CreateCalyxExportZip(Guid loanId, Guid brokerId, string fileName, string extension)
        {
            var nameToFileDict = new Dictionary<string, byte[]>();
            UpdateFileDictionaryFromLoanId(nameToFileDict, loanId, brokerId, fileName, extension);

            return CompressionHelper.CreateZipArchiveFromFileDict(nameToFileDict);
        }

        /// <summary>
        /// Creates a string representation of a zip file containing every point file for the given list of loans.
        /// </summary>
        /// <param name="cacheId">The id used to retrieve the cached loan info.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="useBorrName">Determines whether the borrower name will be preprended to the loan number.</param>
        /// <returns>A string representation of the zip file.</returns>
        public static string CreateCalyxExportZip(string cacheId, Guid brokerId, bool useBorrName)
        {
            List<Guid> loanIds = RetrieveLoanListFromCache(cacheId);
            var nameToFileDict = new Dictionary<string, byte[]>();
            foreach (var loanId in loanIds)
            {
                UpdateFileDictionaryFromLoanId(nameToFileDict, loanId, brokerId, string.Empty, "BRW", true, useBorrName);
            }

            return CompressionHelper.CreateZipArchiveFromFileDict(nameToFileDict);
        }

        /// <summary>
        /// Updates the given dictionary with the data for each file to be added in the archive.
        /// </summary>
        /// <param name="fileDict">The dictionary that will store the file names and the data.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="fileName">The filename that the user specified.</param>
        /// <param name="extension">The extension that the user selected for the borrower file.</param>
        /// <param name="isBatch">Determines whether the update is part of a batch operation.</param>
        /// <param name="useBorrName">Determines whether the borrower name will be preprended to the loan number.</param>
        private static void UpdateFileDictionaryFromLoanId(Dictionary<string, byte[]> fileDict, Guid loanId, Guid brokerId, string fileName, string extension, bool isBatch = false, bool useBorrName = false)
        {
            CPointData loanData;
            loanData = new CPointData(loanId);
            loanData.SetFormatTarget(FormatTarget.PointNative);
            loanData.InitLoad();

            var appData = loanData.Apps.First();
            fileName = !isBatch ? fileName 
                : useBorrName ? appData.aBLastNm + "_" + appData.aBFirstNm + "_" + loanData.sLNm 
                : loanData.sLNm;

            NameValueCollectionWrapper rg = new NameValueCollectionWrapper(new NameValueCollection());
            PointInteropLib.DataFile datafile = new PointInteropLib.DataFile();
            PointDataConversion pointConversion = new PointDataConversion(brokerId, loanData.sLId);

            for (int i = 0; i < loanData.Apps.Count(); i++)
            {
                using (MemoryStream stream = new MemoryStream(100000))
                {
                    pointConversion.TransferDataToPoint(loanData, rg, i, true);

                    string fullFileName = fileName + "." + (i == 0 ? extension : "CB" + i);
                    datafile.WriteSortedStream(stream, rg.GetCollection());
                    fileDict.Add(fullFileName, stream.GetBuffer());
                }
            }
        }

        /// <summary>
        /// Retreives a list of loan ids stored in the auto expired text cache.
        /// </summary>
        /// <param name="autoExpireCacheId">The cache id.</param>
        /// <returns>The list of loan ids.</returns>
        private static List<Guid> RetrieveLoanListFromCache(string autoExpireCacheId)
        {
            string loanList = DataAccess.AutoExpiredTextCache.GetFromCache("BatchExport" + autoExpireCacheId);
            string[] items = loanList.Split(',');
            return items.Select(item => new Guid(item.Substring(0, 36))).ToList();
        }
    }
}
