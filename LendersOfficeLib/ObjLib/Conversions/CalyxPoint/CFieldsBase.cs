﻿
namespace LendersOfficeApp.LegacyLink.CalyxPoint
{

    public abstract class CFieldsBase
    {
        protected NameValueCollectionWrapper m_parent;
        internal CFieldsBase(NameValueCollectionWrapper parent)
        {
            m_parent = parent;
        }

        abstract public string this[string sKey]
        {
            get;
            set;
        }

        abstract public string this[int intKey]
        {
            get;
            set;
        }
    }

}
