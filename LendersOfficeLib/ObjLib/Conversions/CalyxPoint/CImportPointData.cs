﻿using System;
using DataAccess;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    /// <summary>
    /// Override normal import data object so we can bypass
    /// security checking.
    /// </summary>

    public class CImportPointData : CPointData
    {
        /// <summary>
        /// Override access control checking.
        /// </summary>

        protected override bool m_enforceAccessControl
        {
            // 11/5/2004 kb - Skip security checking when we
            // import files.  Fix this up soon.
            //
            // 12/14/2004 kb - Looks like we're keeping
            // it from now on.

            get
            {
                return false;
            }
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public CImportPointData(Guid loanId)
            : base(loanId)
        {
            // OPM 208757, 1/25/2016, ML
            // We want to bypass field security checks when importing
            // from Point.
            this.ByPassFieldSecurityCheck = true;
        }

    }
}
