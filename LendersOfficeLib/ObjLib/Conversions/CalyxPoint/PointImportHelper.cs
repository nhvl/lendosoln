﻿using System;
using System.Collections.Specialized;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    public class PointImportHelper
    {
        public bool ResetDuCaseFileId { get; set; }

        public PointImportHelper()
        {
            ResetDuCaseFileId = false; 
        }

        public void Import(CPointData dataLoan, Guid brokerID, System.IO.Stream stream, Guid loanOfficerID, Guid processorID, Boolean bPreserveInitialAssignments, bool are2ndLiensAllowed)
        {
            ImportSingleApp(dataLoan, brokerID, stream, loanOfficerID, processorID, bPreserveInitialAssignments, are2ndLiensAllowed);
        }

        private PointDataConversion ImportSingleApp(CPointData dataLoan, Guid brokerID, System.IO.Stream stream, Guid loanOfficerID, Guid processorID, Boolean bPreserveInitialAssignments, bool are2ndLiensAllowed)
        {
            // 8/19/2004 dd - Reason I need to create this class because
            // NameValueCollectionWrapper, PointDataConversion are all
            // internal class.  With the help of this class I can import
            // a given POINT file. Usage of this class is PriceMyLoan.
            //
            // 10/11/2004 kb - We need to preserve loan assignments that
            // exist prior to import, so we shall respect a new flag that
            // caches the current employee assignments as the default ones
            // for import.  Beware: the assignments buried in the imported
            // point file may take preference -- only two roles (agent and
            // officer are susceptible to this).

            PointInteropLib.DataFile reader = new PointInteropLib.DataFile();
            NameValueCollection input = reader.ReadStream(stream);

            NameValueCollectionWrapper nvw = new NameValueCollectionWrapper(input);
            PointDataConversion pointConversion = new PointDataConversion(brokerID, dataLoan.sLId);

            pointConversion.DefaultAssignLoanOfficerID = loanOfficerID;
            pointConversion.DefaultAssignLoanProcessorID = processorID;

            if (bPreserveInitialAssignments == true)
            {
                // 10/11/2004 kb - We set these defaults so that the import
                // will reapply them to the data loan file.  We *especially*
                // need this assignment to stick for lender account exec for
                // pml users who import from point.  The initial lender acct
                // exec assignment comes from the pml user's preferred acct
                // exec relationship -- we use this employee and need to
                // keep this employee as the assigned lender acct exec.

                pointConversion.DefaultAssignManagerID = dataLoan.sEmployeeManagerId;
                pointConversion.DefaultAssignLenderAccountExecID = dataLoan.sEmployeeLenderAccExecId;
                pointConversion.DefaultAssignCallCenterAgentID = dataLoan.sEmployeeCallCenterAgentId;
                pointConversion.DefaultAssignLoanOpenerID = dataLoan.sEmployeeLoanOpenerId;
                pointConversion.DefaultAssignLockDescId = dataLoan.sEmployeeLockDeskId; //21891
            }

            if (!are2ndLiensAllowed && nvw.GetSharedField("915") != "X")
            {
                throw new CBaseException(ErrorMessages.SecondLienNotSupportedPML, "2nd Liens in PMl turned off user Tried to export point file with either second lient or other lien.");
            }

            if (ResetDuCaseFileId)
            {
                nvw.Shared["3890"] = ""; 
            }
            pointConversion.IsRolesImportedAsAgents = true;
            pointConversion.TransferDataFromPoint(dataLoan, nvw, 0);
            return pointConversion;
        }

        public void Import(CPointData dataLoan, Guid brokerID, Guid loanOfficerID, Guid processorID, Boolean bPreserveInitialAssignments, bool are2ndLiensAllowed, System.IO.Stream stream, params System.IO.Stream[] coStreams)
        {

            PointDataConversion pointConversion = ImportSingleApp(dataLoan, brokerID, stream, loanOfficerID, processorID, bPreserveInitialAssignments, are2ndLiensAllowed);

            PointInteropLib.DataFile reader;
            NameValueCollection input;
            NameValueCollectionWrapper nvw;

            for (int i = 0; i < coStreams.Length; i++)
            {
                reader = new PointInteropLib.DataFile();
                input = reader.ReadStream(coStreams[i]);
                nvw = new NameValueCollectionWrapper(input);
                dataLoan.Save();
                int iNewApp = dataLoan.AddNewApp();
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                pointConversion.TransferDataFromPoint(dataLoan, nvw, iNewApp);
            }
        }
    }
}


