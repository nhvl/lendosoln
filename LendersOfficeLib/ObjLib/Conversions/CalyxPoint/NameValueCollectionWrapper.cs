﻿using System.Collections;
using System.Collections.Specialized;
using DataAccess;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    /// <summary>
    /// This wrapper's primary purpose is to prevent NULLs from being returned
    /// when the client tries to read specific fields. The client will get a blank
    /// string instead.
    /// </summary>
    public class NameValueCollectionWrapper
    {
        private NameValueCollection m_col;
        private Hashtable _m_appWriteFields;
        private CSharedFields m_shared;
        private CAppFields m_app;
        public CSharedFields Shared
        {
            get { return m_shared; }
        }
        public CAppFields App
        {
            get { return m_app; }
        }

        public void DebugPointInput(string value)
        {
            SortedList sortedList = new SortedList(m_col.Count);

            foreach (string key in m_col.Keys)
            {
                sortedList.Add(key, m_col[key]);
            }

            IDictionaryEnumerator enumerator = sortedList.GetEnumerator();

            while (enumerator.MoveNext())
            {

                Tools.LogError("Key=" + enumerator.Key + ", value=" + enumerator.Value);
            }

        }


        private Hashtable AppWriteFields
        {
            get
            {
                if (null == _m_appWriteFields)
                    _m_appWriteFields = new Hashtable(300);
                return _m_appWriteFields;
            }

        }

        public NameValueCollectionWrapper(NameValueCollection col)
        {
            m_col = col;
            _m_appWriteFields = null;
            m_shared = new CSharedFields(this);
            m_app = new CAppFields(this);
        }
        public NameValueCollection GetCollection()
        {
            return m_col;
        }
        /// <summary>
        /// Write: Use this for shared fields only. Read: Use this for both shared fields and per-app fields.
        /// </summary>
        private string this[string sKey]
        {
            get
            {
                string s = m_col[sKey];
                if (null == s)
                    return "";
                else
                    return s;
            }
            set
            {
                m_col[sKey] = value;
            }
        }

        public void SetSharedField(string sKey, string val)
        {
            this[sKey] = val;
        }

        public string GetSharedField(string sKey)
        {
            return this[sKey];
        }
        public void SetAppField(string sKey, string val)
        {
            Hashtable fields = AppWriteFields;
            if (!fields.ContainsKey(sKey))
                fields.Add(sKey, null);
            this[sKey] = val;
        }
        public string GetAppField(string sKey)
        {
            return this[sKey];
        }

        public void InitToWriteNewApp()
        {
            Hashtable fields = AppWriteFields;
            int nAppFields = fields.Count;

            foreach (string key in fields.Keys)
                m_col.Remove(key);

            fields.Clear();
        }
    }

}
