﻿
namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    public class CAppFields : CFieldsBase
    {
        internal CAppFields(NameValueCollectionWrapper parent) : base(parent)
        { }
        public override string this[string sKey]
        {
            get { return m_parent.GetAppField(sKey); }
            set { m_parent.SetAppField(sKey, value); }
        }
        public override string this[int intKey]
        {
            get { return this[intKey.ToString()]; }
            set { this[intKey.ToString()] = value; }
        }
    }

}
