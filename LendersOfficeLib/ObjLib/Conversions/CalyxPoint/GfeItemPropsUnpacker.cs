﻿using DataAccess;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{
    public class GfeItemPropsUnpacker
    {
        public string Pfc = "";
        public string Pbs = "";
        public string Fa = "";
        public string Poc = "";

        ///		PFC		S			F						apr	br	payer
        ///		0			0			0						0		0	0
        ///		0			0			1						0		0	0 (FHA?)
        ///		0			1			0						0		0	2
        ///		0			1			1						0		0	2(x)
        ///		1			0			0						1		0	0
        ///		1			0			1						1		0	0 (FHA?)
        ///		1			1			0						1		0	2
        ///		1			1			1						1		0	2(x)
        ///		
        ///		(x) means "shouldn't happen", in the 2 cases, if the seller agrees to pay, I
        ///		don't think it make sense for the buyer to finance the charge.
        ///		
        ///		Note that Pfc is always the same as apr.
        ///		toBr always 0, Point doesn't allow users to specify payee or financing closing cost.


        public void Unpack(int nProps)
        {
            Pfc = "";
            Pbs = "";
            Fa = "";
            Poc = "";

            if (LosConvert.GfeItemProps_Apr(nProps))
            {
                Pfc = "X";
            }

            if (LosConvert.GfeItemProps_Poc(nProps))
                Poc = "X";

            /// 0: Paid by borrower, out of pocket.
            /// 1: Paid by borrower but he/she will finance it.
            /// 2: Paid by seller
            /// >=3: Paid by someone other than seller or borrower (broker or lender)
            switch (LosConvert.GfeItemProps_Payer(nProps))
            {
                default:
                    break;
                case 2:
                    Pbs = "X";
                    break;
            }


        }
    }
}
