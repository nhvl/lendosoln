﻿using DataAccess;

namespace LendersOfficeApp.LegacyLink.CalyxPoint
{

    public class CTriStateParser
    {
        private E_TriState m_triState = E_TriState.Blank;
        public CTriStateParser()
        { }
        public void Parse(E_TriState triState)
        {
            m_triState = triState;
        }

        public string xYes
        {
            get
            {
                if (E_TriState.Yes == m_triState)
                    return "X";
                return "";
            }
        }

        public string xNo
        {
            get
            {
                if (E_TriState.No == m_triState)
                    return "X";
                return "";
            }
        }
    }
}
