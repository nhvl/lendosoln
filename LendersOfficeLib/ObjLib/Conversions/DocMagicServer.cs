﻿namespace LendersOffice.Conversions
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;
    using Common;
    using Constants;
    using DataAccess;
    using DocMagic.DsiDocRequest;
    using DocMagic.DsiDocResponse;
    using Integration.DocumentVendor;

    /// <summary>
    /// Sends transmissions to and receives responses from DocMagic.
    /// </summary>
    public static class DocMagicServer2
    {
        /// <summary>
        /// The generic log header for document transactions.
        /// </summary>
        private const string DocLogHeader = "DocMagicServer2";

        /// <summary>
        /// The request log header for document transactions.
        /// </summary>
        private const string DocRequestLogHeader = "DocMagicServer2Request";

        /// <summary>
        /// The response log header for document transactions.
        /// </summary>
        private const string DocResponseLogHeader = "DocMagicServer2Response";

        /// <summary>
        /// The generic log header for UCD transactions.
        /// </summary>
        private const string UcdLogHeader = "UCD Data Service";

        /// <summary>
        /// The request log header for UCD transactions.
        /// </summary>
        private const string UcdRequestLogHeader = "UCD Data Service Request";

        /// <summary>
        /// The response log header for UCD transactions.
        /// </summary>
        private const string UcdResponseLogHeader = "UCD Data Service Response";

        /// <summary>
        /// A list of tags to redact from a log if the log would otherwise be too long.
        /// </summary>
        private static readonly string[] RedactedTags =
        {
            "ProofSheetHTML",
            "AuditHTML",
            "APRPaymentCalculationHTML",
            "Section32CalculationHTML",
            "GFEComparisonHTML",
            "ImpoundAnalysisHTML",
            "DocHTML",
            "CheckHTML",
            "EmbeddedContent",
            "EmbeddedFileContent"
        };

        /// <summary>
        /// The type of request being placed to DocMagic.
        /// </summary>
        /// <remarks>This enum does not have values because I do not want it stored/persisted.  If you do persist it, add values.</remarks>
        public enum RequestType
        {
            /// <summary>
            /// The existing, legacy DocMagic.
            /// </summary>
            StandardDocuments,

            /// <summary>
            /// DocMagic's Uniform Closing Dataset web services.
            /// </summary>
            UniformClosingDataset,

            /// <summary>
            /// DocMagic's appraisal delivery web services.
            /// </summary>
            AppraisalDelivery,
        }

        /// <summary>
        /// Submits a request to DocMagic.
        /// </summary>
        /// <param name="request">The request payload.</param>
        /// <param name="requestType">The type of request being issued.</param>
        /// <param name="docMagicVendor">The doc magic vendor.</param>
        /// <returns>A response payload.</returns>
        public static DsiDocumentServerResponse Submit(DsiDocumentServerRequest request, RequestType requestType = RequestType.StandardDocuments, VendorConfig docMagicVendor = null)
        {
            DsiDocumentServerResponse dsiDocumentServerResponse = null;
            try
            {
                if (docMagicVendor != null && docMagicVendor.PlatformType != E_DocumentVendor.DocMagic)
                {
                    throw new CBaseException(ErrorMessages.Generic, "Non-DocMagic vendor passed in.");
                }

                bool isTargetBeta = false;
                if (docMagicVendor != null)
                {
                    isTargetBeta = docMagicVendor.IsTestVendor;
                }
                else
                {
                    if (request.CustomerInformation.CustomerId == ConstStage.DocMagicServerBetaCustomerId)
                    {
                        if (string.Compare(request.CustomerInformation.UserName, ConstStage.DocMagicServerBetaUsername, true) != 0)
                        {
                            isTargetBeta = true;
                        }
                    }
                }

                string url = isTargetBeta ? ConstStage.DocMagicBetaServerUrl : ConstStage.DocMagicProductionServerUrl;
                string docTypeUrl = ConstStage.DocMagicMismoClosingV2DocTypeUrl;
                if (requestType != RequestType.StandardDocuments)
                {
                    docTypeUrl = isTargetBeta ? ConstStage.DocMagicUcdBetaDocTypeUrl : ConstStage.DocMagicUcdProductionDocTypeUrl; // UCD/General DM web services all use the same doc type
                }

                Tools.LogInfo(GetLogHeader(requestType), "Url=[" + url + "]");

                byte[] bytes = null;

                using (MemoryStream stream = new MemoryStream(5000))
                {
                    var settings = new XmlWriterSettings();
                    settings.Encoding = Encoding.ASCII; // Strange. Why not UTF-8?
                    using (DocMagicXmlWriter writer = new DocMagicXmlWriter(XmlWriter.Create(stream, settings))) 
                    {
                        writer.WriteProcessingInstruction("xml", "version=\"1.0\" encoding=\"UTF-8\"");
                        writer.WriteDocType("DSIDocumentServerRequest", "DSIDocumentServerRequest", docTypeUrl, null);
                        request.WriteXml(writer);
                    }

                    bytes = stream.ToArray();
                }

                LogRequest(bytes, requestType);

                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml"; // OPM 23648 - DocMagic indicates that the content type should always be text/xml and not application/x-www-form-urlencoded as it previously was.
                webRequest.ContentLength = bytes.Length;

                using (Stream stream = webRequest.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

                StringBuilder sb = new StringBuilder();
                using (WebResponse webResponse = webRequest.GetResponse())
                {
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        byte[] buffer = new byte[60000];
                        int size = stream.Read(buffer, 0, buffer.Length);

                        while (size > 0)
                        {
                            string chunk = System.Text.Encoding.UTF8.GetString(buffer, 0, size);
                            sb.Append(chunk);
                            size = stream.Read(buffer, 0, buffer.Length);
                        }
                    }
                }

                XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
                xmlReaderSettings.XmlResolver = null;
                xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;

                LogResponse(sb, xmlReaderSettings, requestType);

                dsiDocumentServerResponse = new DsiDocumentServerResponse();
                using (XmlReader reader = XmlReader.Create(new StringReader(sb.ToString()), xmlReaderSettings))
                {
                    dsiDocumentServerResponse.ReadXml(reader);
                }
            }
            catch (Exception exc)
            {
                Tools.LogErrorWithCriticalTracking(exc);
                throw;
            }

            return dsiDocumentServerResponse;
        }

        /// <summary>
        /// Validates a DocMagic server response.
        /// </summary>
        /// <param name="stream">A stream containing the response XML.</param>
        /// <returns>A boolean indicating whether the response is valid.</returns>
        public static bool ValidateDSIDocumentServerResponse(Stream stream)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.DTD;
            settings.ValidationEventHandler += new ValidationEventHandler((sender, args) => Tools.LogInfo("ARGH! " + args.Message));

            XmlReader reader = XmlReader.Create(stream, settings);

            while (reader.Read())
            {
            }

            return false;
        }

        /// <summary>
        /// Writes a log for a DocMagic request.
        /// </summary>
        /// <param name="bytes">The request bytes.</param>
        /// <param name="requestType">The type of request being issued.</param>
        private static void LogRequest(byte[] bytes, RequestType requestType)
        {
            string logHeader = GetRequestHeader(requestType);

            if (Tools.IsLogLargeRequest(bytes.Length))
            {
                string bytesString = Encoding.ASCII.GetString(bytes);

                // Mask out the password in PaulBunyan log. Use nongreedy matching so we don't stomp over values
                bytesString = Regex.Replace(bytesString, "<Password>[^ ]+?</Password>", "<Password>******</Password>");

                if (requestType == RequestType.UniformClosingDataset)
                {
                    // Do the same for GSE credentials.
                    bytesString = Regex.Replace(bytesString, "<LoginAccountPassword>[^ ]+?</LoginAccountPassword>", "<LoginAccountPassword>******</LoginAccountPassword>");
                }

                Tools.LogInfo(logHeader, FormatXmlForDebug(bytesString));
            }
            else
            {
                Tools.LogInfo(logHeader, bytes.Length + " bytes and it exceeding threshold for logging.");
            }
        }

        /// <summary>
        /// Writes a log for a DocMagic response.
        /// </summary>
        /// <param name="sb">A string builder containing the response.</param>
        /// <param name="xmlReaderSettings">A set of XML reader settings.</param>
        /// <param name="requestType">The type of request being issued.</param>
        private static void LogResponse(StringBuilder sb, XmlReaderSettings xmlReaderSettings, RequestType requestType)
        {
            string logHeader = GetResponseHeader(requestType);

            if (Tools.IsLogLargeRequest(sb.Length))
            {
                StringBuilder tempResponseStringBuilder = new StringBuilder();
                if (sb.Length <= 20000)
                {
                    tempResponseStringBuilder.AppendLine("Response:" + Environment.NewLine + FormatXmlForDebug(sb.ToString()));
                }
                else
                {
                    tempResponseStringBuilder.AppendLine("Response was too large, parts redacted. Original length was: " + sb.Length);

                    XmlDocument xdoc = new XmlDocument();

                    using (XmlReader reader = XmlReader.Create(new StringReader(sb.ToString()), xmlReaderSettings))
                    {
                        xdoc.Load(reader);
                    }

                    foreach (var tagName in RedactedTags)
                    {
                        foreach (XmlNode node in xdoc.GetElementsByTagName(tagName))
                        {
                            node.InnerText = "Redacted for space, length was " + (node.InnerText ?? string.Empty).Length;
                        }
                    }

                    if (xdoc.InnerXml.Length <= 60000)
                    {
                        tempResponseStringBuilder.AppendLine("Redacted response:" + Environment.NewLine + FormatXmlForDebug(xdoc.InnerXml));
                    }
                    else
                    {
                        var responseStatus = "Unknown, couldn't find it";
                        var statusNodes = xdoc.GetElementsByTagName("DSIDocumentServerResponse");
                        if (statusNodes.Count > 0)
                        {
                            var statusAttr = statusNodes[0].Attributes["status"];
                            if (statusAttr != default(XmlAttribute))
                            {
                                responseStatus = statusAttr.Value;
                            }
                        }

                        tempResponseStringBuilder.AppendLine("Redacted response was still to large (final size: " + xdoc.InnerXml.Length + "), not logging it. Response status was: " + responseStatus);
                    }
                }

                Tools.LogInfo(logHeader, tempResponseStringBuilder.ToString());
            }
            else
            {
                Tools.LogInfo(logHeader, sb.Length + " bytes and it exceeding threshold for logging.");
            }
        }

        /// <summary>
        /// Formats XML by passing it though an XDocument.
        /// </summary>
        /// <param name="xml">An XML string.</param>
        /// <returns>A formatted XML string.</returns>
        private static string FormatXmlForDebug(string xml)
        {
            try
            {
                XDocument xdoc = XDocument.Parse(xml);
                return xdoc.ToString(SaveOptions.None);
            }
            catch (SystemException exc) when (exc is FormatException || exc is XmlException)
            {
            }

            return xml;
        }

        /// <summary>
        /// Gets the generic log header.
        /// </summary>
        /// <param name="requestType">The type of request being issued.</param>
        /// <returns>The matching log header.</returns>
        private static string GetLogHeader(RequestType requestType)
        {
            return requestType == RequestType.UniformClosingDataset ? UcdLogHeader : DocLogHeader;
        }

        /// <summary>
        /// Gets the request log header.
        /// </summary>
        /// <param name="requestType">The type of request being issued.</param>
        /// <returns>The matching log header.</returns>
        private static string GetRequestHeader(RequestType requestType)
        {
            return requestType == RequestType.UniformClosingDataset ? UcdRequestLogHeader : DocRequestLogHeader;
        }

        /// <summary>
        /// Gets the response log header.
        /// </summary>
        /// <param name="requestType">The type of request being issued.</param>
        /// <returns>The matching log header.</returns>
        private static string GetResponseHeader(RequestType requestType)
        {
            return requestType == RequestType.UniformClosingDataset ? UcdResponseLogHeader : DocResponseLogHeader;
        }
    }
}
