using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Conversions.Mismo23
{
    public class AssetTypeMap
    {
        public static string ToMismo(E_AssetT assetType)
        {
            switch (assetType)
            {

                /* UNUSED TYPES
                "BridgeLoanNotDeposited" /> 
                "CertificateOfDepositTimeDeposit" /> 
                "CashOnHand"
                "GiftsNotDeposited" /> 
                "MoneyMarketFund" /> 
                "MutualFund" /> 
                "PendingNetSaleProceedsFromRealEstateAssets" />  
                "SecuredBorrowedFundsNotDeposited" /> 
                "TrustAccount" /> 
                */
                case E_AssetT.Auto:
                    return "Automobile";
                case E_AssetT.Bonds:
                    return "Bond";
                case E_AssetT.Business:
                    return "NetWorthOfBusinessOwned";
                case E_AssetT.Checking:
                    return "CheckingAccount";
                case E_AssetT.GiftFunds:
                    return "GiftsTotal";
                case E_AssetT.LifeInsurance:
                    return "LifeInsurance";
                case E_AssetT.Retirement:
                    return "RetirementFund";
                case E_AssetT.Savings:
                    return "SavingsAccount";
                case E_AssetT.Stocks:
                    return "Stock";
                case E_AssetT.CashDeposit:
                    return "EarnestMoneyCashDepositTowardPurchase";
                case E_AssetT.OtherLiquidAsset:
                    return "OtherLiquidAssets";
                default: // Fall through
                case E_AssetT.OtherIlliquidAsset:
                    return "OtherNonLiquidAssets";
            }
        }
        public static E_AssetT FromMismo(string assetType)
        {
            switch (assetType)
            {
                case "Automobile":
                    return E_AssetT.Auto;
                case "Bond":
                    return E_AssetT.Bonds;
                case "NetWorthOfBusinessOwned":
                    return E_AssetT.Business;
                case "CheckingAccount":
                    return E_AssetT.Checking;
                case "GiftsTotal":
                case "GiftsNotDeposited":
                    return E_AssetT.GiftFunds;
                case "LifeInsurance":
                    return E_AssetT.LifeInsurance;
                case "RetirementFund":
                    return E_AssetT.Retirement;
                case "SavingsAccount":
                    return E_AssetT.Savings;
                case "Stock":
                case "MoneyMarketFund":
                case "MutualFund":
                    return E_AssetT.Stocks;
                case "EarnestMoneyCashDepositTowardPurchase":
                    return E_AssetT.CashDeposit;
                case "OtherLiquidAssets":
                case "BridgeLoanNotDeposited":
                case "CertificateOfDepositTimeDeposit":
                case "CashOnHand":
                case "SecuredBorrowedFundsNotDeposited":
                    return E_AssetT.OtherLiquidAsset;
                default: // fall through
                case "OtherNonLiquidAssets":
                case "PendingNetSaleProceedsFromRealEstateAssets":
                case "TrustAccount":
                    return E_AssetT.OtherIlliquidAsset;
            }
        }
    }
    public class LiabilityTypeMap
    {
        public static E_DebtT FromMismo(string type)
        {
            switch (type)
            {
                /* UNUSED TYPES
                    "ChildCare" /> 
                    "CollectionsJudgementsAndLiens" /> 
                    "HELOC" /> 
                    "LeasePayments" /> 
                    "Open30DayChargeAccount" /> ???
                    "SeparateMaintenanceExpense" /> 
                    "Taxes" /> 
                */

                case "Alimony":
                    return E_DebtT.Alimony;
                case "ChildSupport":
                    return E_DebtT.ChildSupport;
                case "Installment":
                    return E_DebtT.Installment;
                case "JobRelatedExpenses":
                    return E_DebtT.JobRelatedExpense;
                case "MortgageLoan":
                    return E_DebtT.Mortgage;
                case "OpenThirtyDayChargeAccount":
                case "Open30DayChargeAccount":
                    return E_DebtT.Open;
                case "OtherLiability":
                    return E_DebtT.Other;
                case "Revolving":
                    return E_DebtT.Revolving;
                default:
                    return E_DebtT.Other;
            }
        }
        public static string ToMismo(E_DebtT type)
        {
            switch (type)
            {
                //Debt types we don't have:
                // HELOC  SeparateMaintenanceExpense   ChildCare LeasePayments CollectionsJudgementsAndLiens  OtherExpense  Taxes 
                case E_DebtT.Alimony:
                    return "Alimony";
                case E_DebtT.ChildSupport:
                    return "ChildSupport";
                case E_DebtT.Installment:
                    return "Installment";
                case E_DebtT.JobRelatedExpense:
                    return "JobRelatedExpenses";
                case E_DebtT.Mortgage:
                    return "MortgageLoan";
                case E_DebtT.Open:
                    return "Open30DayChargeAccount";
                case E_DebtT.Other:
                    return "OtherLiability";
                case E_DebtT.Revolving:
                    return "Revolving";
                default:
                    throw new CBaseException(ErrorMessages.EnumValueNotHandled, "Programming error:  Unexpected Debt Type");
            }
        }
    }
    public class MaritalStatusMap
    {
        public static string ToMismo(E_aBMaritalStatT status)
        {
            switch (status)
            {
                case E_aBMaritalStatT.Married:
                    return "Married";
                case E_aBMaritalStatT.NotMarried:
                    return "Unmarried";
                case E_aBMaritalStatT.Separated:
                    return "Separated";
                case E_aBMaritalStatT.LeaveBlank: // fall through
                default:
                    return "NotProvided";
            }
        }
        public static E_aBMaritalStatT FromMismo(string status)
        {
            switch (status)
            {
                case "NotProvided":
                case "Unknown":
                    return E_aBMaritalStatT.LeaveBlank;
                case "Married":
                    return E_aBMaritalStatT.Married;
                case "Unmarried":
                    return E_aBMaritalStatT.NotMarried;
                case "Separated":
                    return E_aBMaritalStatT.Separated;
                default:
                    return E_aBMaritalStatT.LeaveBlank;
            }
        }
    }
    public class AddressTypeMap
    {
        public static string ToMismo(E_aBAddrT addrT)
        {
            switch (addrT)
            {
                case E_aBAddrT.Own:
                    return "Own";
                case E_aBAddrT.Rent:
                    return "Rent";
                case E_aBAddrT.LeaveBlank: // fall through
                default:
                    return string.Empty;
            }
        }
        public static E_aBAddrT FromMismo(string addrT)
        {
            switch (addrT)
            {
                case "Own":
                    return E_aBAddrT.Own;
                case "Rent":
                    return E_aBAddrT.Rent;
                default:
                    return E_aBAddrT.LeaveBlank;
            }
        }
    }
    public class DownPaymentMap
    {
        public static string FromMismo(string sMismoFormat)
        {
            switch (sMismoFormat)
            {
                case "BridgeLoan": return "Bridge Loan";
                case "CashOnHand": return "Cash On Hand";
                case "CheckingSavings": return "Checking/Savings";
                case "DepositOnSalesContract": return "Deposit On Sales Contract";
                case "EquityOnPendingSale": return "Equity From Pending Sale";
                case "EquityOnSoldProperty": return "Equity From subject property";
                case "EquityOnSubjectProperty": return "Equity On Sold Property";
                case "GiftFunds": return "Gift Funds";
                case "LifeInsuranceCashValue": return "Life Insurance Cash Value";
                case "LotEquity": return "Lot Equity";
                case "OtherTypeOfDownPayment": return "Other Type Of Down Payment";
                case "RentWithOptionToPurchase": return "Rent With Option To Purchase";
                case "RetirementFunds": return "Retirement Funds";
                case "SaleOfChattel": return "Sale Of Chattel";
                case "SecuredBorrowedFunds": return "Secured Borrowed Funds";
                case "StocksAndBonds": return "Stocks & Bonds";
                case "SweatEquity": return "Sweat Equity";
                case "TradeEquity": return "Trade Equity";
                case "TrustFunds": return "Trust Funds";
                case "UnsecuredBorrowedFunds": return "Unsecured Borrowed Funds";
                case "ForgivableSecuredLoan": return "Forgivable Secured Loan";
                default: return string.Empty;
            }
        }
        public static string ToMismo(string sLOFormat)
        {
            switch (sLOFormat.TrimWhitespaceAndBOM().ToLower())
            {
                case "bridge loan": return "BridgeLoan";
                case "cash on hand": return "CashOnHand";
                case "checking/savings": return "CheckingSavings";
                case "deposit on sales contract": return "DepositOnSalesContract";
                case "equity from pending sale": return "EquityOnPendingSale";
                case "equity from subject property": return "EquityOnSoldProperty";
                case "equity on sold property": return "EquityOnSubjectProperty";
                case "gift funds": return "GiftFunds";
                case "life insurance cash value": return "LifeInsuranceCashValue";
                case "lot equity": return "LotEquity";
                case "other type of down payment": return "OtherTypeOfDownPayment";
                case "rent with option to purchase": return "RentWithOptionToPurchase";
                case "retirement funds": return "RetirementFunds";
                case "sale of chattel": return "SaleOfChattel";
                case "secured borrowed funds": return "SecuredBorrowedFunds";
                case "stocks & bonds": return "StocksAndBonds";
                case "sweat equity": return "SweatEquity";
                case "trade equity": return "TradeEquity";
                case "trust funds": return "TrustFunds";
                case "unsecured borrowed funds": return "UnsecuredBorrowedFunds";
                case "forgivable secured loan": return "ForgivableSecuredLoan";

                default: return string.Empty;
            }
        }
    }
    public class LienPositionMap
    {
        public static string ToMismo(E_sLienPosT position)
        {
            switch (position)
            {
                case E_sLienPosT.First:
                    return "FirstLien";
                case E_sLienPosT.Second:
                    return "SecondLien";
                default:
                    return "Other";
            }
        }
        public static E_sLienPosT FromMismo(string position)
        {
            switch (position)
            {
                case "FirstLien":
                    return E_sLienPosT.First;
                case "SecondLien":
                    return E_sLienPosT.Second;
                default:
                    return E_sLienPosT.First;	// assume first
            }
        }
    }

    public class GSEPropertyTypeMap
    {
        public static E_sGseSpT FromMismo(string type)
        {
            switch (type)
            {
                case "Attached":
                    return E_sGseSpT.Attached;
                case "Condominium":
                    return E_sGseSpT.Condominium;
                case "Cooperative":
                    return E_sGseSpT.Cooperative;
                case "Detached":
                    return E_sGseSpT.Detached;
                case "HighRiseCondominium":
                    return E_sGseSpT.HighRiseCondominium;
                case "ManufacturedHousing":
                    return E_sGseSpT.ManufacturedHousing;
                case "Modular":
                    return E_sGseSpT.Modular;
                case "PUD":
                    return E_sGseSpT.PUD;
                case "ManufacturedHousingSingleWide":
                    return E_sGseSpT.ManufacturedHousingSingleWide;
                case "ManufacturedHousingDoubleWide":
                    return E_sGseSpT.LeaveBlank; // 4/28/2004 dd - We don't have this enum.
                case "DetachedCondominium":
                    return E_sGseSpT.DetachedCondominium;
                case "ManufacturedHomeCondominium":
                    return E_sGseSpT.ManufacturedHomeCondominium;
                case "ManufacturedHousingMultiWide":
                    return E_sGseSpT.ManufacturedHomeMultiwide;
                case "ManufacturedHomeCondominiumOrPUDOrCooperative":
                    return E_sGseSpT.ManufacturedHomeCondominium;
                default:
                    return E_sGseSpT.LeaveBlank;
            }
        }
        public static string ToMismo(E_sGseSpT type)
        {
            switch (type)
            {
                case E_sGseSpT.Attached:
                    return "Attached";
                case E_sGseSpT.Condominium:
                    return "Condominium";
                case E_sGseSpT.Cooperative:
                    return "Cooperative";
                case E_sGseSpT.Detached:
                    return "Detached";
                case E_sGseSpT.HighRiseCondominium:
                    return "HighRiseCondominium";
                case E_sGseSpT.ManufacturedHousing:
                    return "ManufacturedHousing";
                case E_sGseSpT.Modular:
                    return "Modular";
                case E_sGseSpT.PUD:
                    return "PUD";
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return "ManufacturedHousingSingleWide";
                case E_sGseSpT.DetachedCondominium:
                    return "DetachedCondominium";
                case E_sGseSpT.ManufacturedHomeCondominium:
                    return "ManufacturedHomeCondominium";
                case E_sGseSpT.ManufacturedHomeMultiwide:
                    return "ManufacturedHousingMultiWide";
                default:
                    return string.Empty;
            }
        }
    }
    public class PropertyTypeMap
    {
        public static string ToMismo(E_sSpT type)
        {
            switch (type)
            {
                case E_sSpT.AttachedHousing:
                    return "Attached";
                case E_sSpT.DetachedHousing:
                    return "Detached";
                case E_sSpT.Condominium:
                    return "Condominium";
                case E_sSpT.PUD:
                    return "PUD";
                default:
                    return string.Empty;
            }
        }
        public static E_sSpT FromMismo(string type)
        {
            switch (type)
            {
                case "Attached":
                    return E_sSpT.AttachedHousing;
                case "Detached":
                    return E_sSpT.DetachedHousing;
                case "Condominium":
                    return E_sSpT.Condominium;
                case "PUD":
                    return E_sSpT.PUD;
                default:
                    return E_sSpT.LeaveBlank;
            }
        }
    }
    public class LoanPurposeTypeMap
    {
        public static string ToMismo(E_sLPurposeT purpose)
        {
            switch (purpose)
            {
                case E_sLPurposeT.Purchase:
                    return "Purchase";
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                    return "Refinance";
                case E_sLPurposeT.Construct:
                    return "ConstructionOnly";
                case E_sLPurposeT.ConstructPerm:
                    return "ConstructionToPermanent";
                default:
                    return "Other";
            }
        }
        public static E_sLPurposeT FromMismo(string type)
        {
            switch (type)
            {
                case "Purchase":
                    return E_sLPurposeT.Purchase;
                case "Refinance":
                    return E_sLPurposeT.Refin;
                case "ConstructionOnly":
                    return E_sLPurposeT.Construct;
                case "ConstructionToPermanent":
                    return E_sLPurposeT.ConstructPerm;
                default:
                    return E_sLPurposeT.Other;
            }
        }
    }
    public class EstateHeldTypeMap
    {
        public static string ToMismo(E_sEstateHeldT type)
        {
            switch (type)
            {
                case E_sEstateHeldT.FeeSimple:
                    return "FeeSimple";
                case E_sEstateHeldT.LeaseHold:
                    return "Leasehold";
                default:
                    return string.Empty;
            }
        }
        public static E_sEstateHeldT FromMismo(string type)
        {
            switch (type)
            {
                case "FeeSimple":
                    return E_sEstateHeldT.FeeSimple;
                case "Leasehold":
                    return E_sEstateHeldT.LeaseHold;
                default:
                    return E_sEstateHeldT.FeeSimple;	// what else can I do
            }
        }
    }
    public class OccupancyTypeMap
    {
        public static string ToMismo(E_aOccT type)
        {
            switch (type)
            {
                case E_aOccT.Investment:
                    return "Investor";
                case E_aOccT.PrimaryResidence:
                    return "PrimaryResidence";
                case E_aOccT.SecondaryResidence:
                    return "SecondHome";
                default:
                    return string.Empty;
            }
        }
        public static E_aOccT FromMismo(string type)
        {
            switch (type)
            {
                case "Investor":
                    return E_aOccT.Investment;
                case "PrimaryResidence":
                    return E_aOccT.PrimaryResidence;
                case "SecondHome":
                    return E_aOccT.SecondaryResidence;
                default:
                    return E_aOccT.PrimaryResidence;
            }
        }
    }
    public class GSERefiPurposeMap
    {
        public static string ToMismo(string purpose)
        {
            switch (purpose.TrimWhitespaceAndBOM().ToLower())
            {
                case "no cash-out rate/term":
                    return "ChangeInRateTerm";
                case "limited cash-out rate/term":
                    return "CashOutLimited";
                case "cash-out/home improvement":
                    return "CashOutHomeImprovement";
                case "cash-out/debt consolidation":
                    return "CashOutDebtConsolidation";
                case "cash-out/other":
                    return "CashOutOther";
                case "no cash-out fha streamlined refinance":
                    return "NoCashOutFHAStreamlinedRefinance";
                case "no cash-out freowned refinance":
                    return "NoCashOutFREOwnedRefinance";
                case "no cash-out/other":
                    return "NoCashOutOther";
                case "no cash-out streamlined refinance":
                    return "NoCashOutStreamlinedRefinance";
                default:
                    return string.Empty;
            }
        }
        public static string FromMismo(string purpose)
        {
            switch (purpose)
            {
                case "CashOutDebtConsolidation":
                    return "Cash-Out/Debt Consolidation";
                case "CashOutHomeImprovement":
                    return "Cash-Out/Home Improvement";
                case "CashOutLimited":
                    return "Limited Cash-Out Rate/Term";
                case "CashOutOther":
                    return "Cash-Out/Other";
                case "NoCashOutFHAStreamlinedRefinance":
                    return "No Cash-Out FHA Streamlined Refinance";
                case "NoCashOutFREOwnedRefinance":
                    return "No Cash-Out FREOwned Refinance";
                case "NoCashOutOther":
                    return "No Cash-Out/Other";
                case "NoCashOutStreamlinedRefinance":
                    return "No Cash-Out Streamlined Refinance";
                case "ChangeInRateTerm":
                    return "No Cash-Out Rate/Term";
                default:
                    return string.Empty;
            }
        }
    }
    public class FinanceMethodMap
    {
        public static string ToMismo(E_sFinMethT method)
        {
            switch (method)
            {
                case E_sFinMethT.Fixed:
                    return "Fixed";
                case E_sFinMethT.Graduated:
                    return "GraduatedPaymentMortgage";
                case E_sFinMethT.ARM:
                    return "AdjustableRate";
                default:
                    throw new CBaseException(ErrorMessages.EnumValueNotHandled, "Programming error:  unknown finance method");
            }
        }
        public static E_sFinMethT FromMismo(string method)
        {
            switch (method)
            {
                case "AdjustableRate":
                    return E_sFinMethT.ARM;
                case "Fixed":
                    return E_sFinMethT.Fixed;
                case "GraduatedPaymentMortgage":
                    return E_sFinMethT.Graduated;
                case "GrowingEquityMortgage":
                case "OtherAmortizationType":
                default: // T THINH: todo: should we add an Other? We can use the combination of Fixed and write more info to sFinMethDesc
                    return E_sFinMethT.Fixed;
            }
        }
    }
    public class LoanTypeMap
    {
        public static string ToMismo(E_sLT type)
        {
            switch (type)
            {
                case E_sLT.Conventional:
                    return "Conventional";
                case E_sLT.FHA:
                    return "FHA";
                case E_sLT.UsdaRural:
                    return "FarmersHomeAdministration";
                case E_sLT.VA:
                    return "VA";
                default:
                    return "Other";
            }
        }
        public static E_sLT FromMismo(string type)
        {
            switch (type)
            {
                case "Conventional":
                    return E_sLT.Conventional;
                case "FHA":
                    return E_sLT.FHA;
                case "FarmersHomeAdministration":
                    return E_sLT.UsdaRural;
                case "VA":
                    return E_sLT.VA;
                default:
                    return E_sLT.Other;
            }
        }
    }
    public class ReoStatusMap
    {
        public static string ToMismo(string status)
        {
            switch (status.ToUpper())
            {
                case "S":	// sold
                    return "Sold";
                case "PS":	// sale pending
                    return "PendingSale";
                case "R":	// Rental
                    return "RetainForRental";
                default:
                    return string.Empty;
            }
        }
        public static string FromMismo(string status)
        {
            switch (status)
            {
                case "PendingSale":
                    return "PS";
                case "RetainForRental":
                    return "R";
                case "Sold":
                    return "S";
                case "RetainForPrimaryOrSecondaryResidence":
                default:
                    return string.Empty;
            }
        }
    }
    public class REOGsePropertyTypeMap
    {
        public static string ToMismo(E_ReoTypeT type)
        {
            switch (type)
            {
                case E_ReoTypeT._2_4Plx:
                    return "TwoToFourUnitProperty";
                case E_ReoTypeT.ComNR:
                    return "CommercialNonResidential";
                case E_ReoTypeT.Mixed:
                    return "MixedUseResidential";
                case E_ReoTypeT.ComR:
                    return "HomeAndBusinessCombined";
                case E_ReoTypeT.Condo:
                    return "Condominium";
                case E_ReoTypeT.Coop:
                    return "Cooperative";
                case E_ReoTypeT.Land:
                    return "Land";
                case E_ReoTypeT.Mobil:
                    return "ManufacturedMobileHome";
                case E_ReoTypeT.Multi:
                    return "MultifamilyMoreThanFourUnits";
                case E_ReoTypeT.SFR:
                    return "SingleFamily";
                case E_ReoTypeT.Town:
                    return "Townhouse";
                case E_ReoTypeT.Farm:
                    return "Farm";
                default:
                    return string.Empty; // this is how Point behaves.
            }
        }
        public static E_ReoTypeT FromMismo(string type)
        {
            switch (type)
            {
                case "TwoToFourUnitProperty":
                    return E_ReoTypeT._2_4Plx;
                case "CommercialNonResidential":
                    return E_ReoTypeT.ComNR;
                case "MixedUseResidential":
                    return E_ReoTypeT.Mixed;
                case "HomeAndBusinessCombined":
                    return E_ReoTypeT.ComR;
                case "Condominium":
                    return E_ReoTypeT.Condo;
                case "Cooperative":
                    return E_ReoTypeT.Coop;
                case "Land":
                    return E_ReoTypeT.Land;
                case "ManufacturedMobileHome":
                    return E_ReoTypeT.Mobil;
                case "MultifamilyMoreThanFourUnits":
                    return E_ReoTypeT.Multi;
                case "SingleFamily":
                    return E_ReoTypeT.SFR;
                case "Townhouse":
                    return E_ReoTypeT.Town;
                case "Farm":
                    return E_ReoTypeT.Farm;
                default:
                    return E_ReoTypeT.LeaveBlank; // this is how Point behaves.
            }
        }
    }
    public class PriorPropTitleTypeMap
    {
        public static string ToMismo(E_aBDecPastOwnedPropTitleT type)
        {
            switch (type)
            {
                case E_aBDecPastOwnedPropTitleT.O:
                    return "JointWithOtherThanSpouse";
                case E_aBDecPastOwnedPropTitleT.S:
                    return "Sole";
                case E_aBDecPastOwnedPropTitleT.SP:
                    return "JointWithSpouse";
                default:
                    return string.Empty;
            }
        }
        public static E_aBDecPastOwnedPropTitleT FromMismo(string type)
        {
            switch (type)
            {
                case "JointWithOtherThanSpouse":
                    return E_aBDecPastOwnedPropTitleT.O;
                case "Sole":
                    return E_aBDecPastOwnedPropTitleT.S;
                case "JointWithSpouse":
                    return E_aBDecPastOwnedPropTitleT.SP;
                default:
                    return E_aBDecPastOwnedPropTitleT.Empty;
            }
        }
    }
    public class PriorPropUsageTypeMap
    {
        public static string ToMismo(E_aBDecPastOwnedPropT type)
        {
            switch (type)
            {
                case E_aBDecPastOwnedPropT.PR:
                    return "PrimaryResidence";
                case E_aBDecPastOwnedPropT.SH:
                    return "SecondaryResidence";
                case E_aBDecPastOwnedPropT.IP:
                    return "Investment";
                default:
                    return string.Empty;
            }
        }
        public static E_aBDecPastOwnedPropT FromMismo(string type)
        {
            switch (type)
            {
                case "PrimaryResidence":
                    return E_aBDecPastOwnedPropT.PR;
                case "SecondaryResidence":
                    return E_aBDecPastOwnedPropT.SH;
                case "Investment":
                    return E_aBDecPastOwnedPropT.IP;
                default:
                    return E_aBDecPastOwnedPropT.Empty;
            }
        }
    }
    public class AssumabilityMap
    {
        public static string ToMismo(E_sAssumeLT assume)
        {
            switch (assume)
            {
                case E_sAssumeLT.May:
                case E_sAssumeLT.MaySubjectToCondition:
                    return "Y";
                case E_sAssumeLT.MayNot:
                    return "N";
                default:
                    return string.Empty;
            }
        }
        public static E_sAssumeLT FromMismo(string assume)
        {
            switch (assume)
            {
                case "Y":
                    return E_sAssumeLT.May;
                case "N":
                    return E_sAssumeLT.MayNot;
                default:
                    return E_sAssumeLT.LeaveBlank;
            }
        }
    }
    public class PrepayPenaltyMap
    {
        public static string ToMismo(E_sPrepmtPenaltyT penalty)
        {
            switch (penalty)
            {
                case E_sPrepmtPenaltyT.May:
                    return "Y";
                case E_sPrepmtPenaltyT.WillNot:
                    return "N";
                default:
                    return string.Empty;
            }
        }
        public static E_sPrepmtPenaltyT FromMismo(string penalty)
        {
            switch (penalty)
            {
                case "Y":
                    return E_sPrepmtPenaltyT.May;
                case "N":
                    return E_sPrepmtPenaltyT.WillNot;
                default:
                    return E_sPrepmtPenaltyT.LeaveBlank;
            }
        }
    }
    public class PrepayRefundChargeMap
    {
        public static string ToMismo(E_sPrepmtRefundT refund)
        {
            switch (refund)
            {
                case E_sPrepmtRefundT.May:
                    return "Y";
                case E_sPrepmtRefundT.WillNot:
                    return "N";
                default:
                    return string.Empty;
            }
        }
        public static E_sPrepmtRefundT FromMismo(string refund)
        {
            switch (refund)
            {
                case "Y":
                    return E_sPrepmtRefundT.May;
                case "N":
                    return E_sPrepmtRefundT.WillNot;
                default:
                    return E_sPrepmtRefundT.LeaveBlank;
            }
        }
    }
    public class GenderTypeMap
    {
        public static string ToMismo(E_GenderT type)
        {
            switch (type)
            {
                case E_GenderT.Male:
                    return "Male";
                case E_GenderT.Female:
                    return "Female";
                case E_GenderT.NA:
                case E_GenderT.Unfurnished:
                case E_GenderT.LeaveBlank:
                default:
                    return "InformationNotProvidedUnknown";
            }
        }
        public static E_GenderT FromMismo(string type)
        {
            switch (type)
            {
                case "Male":
                    return E_GenderT.Male;
                case "Female":
                    return E_GenderT.Female;
                case "InformationNotProvidedUnknown":
                    return E_GenderT.Unfurnished;
                default:
                    return E_GenderT.LeaveBlank;
            }
        }
    }
    public class RaceTypeMap
    {
        public static string ToMismo(E_aBRaceT type)
        {
            switch (type)
            {
                case E_aBRaceT.AmericanIndian:
                    return "AmericanIndianOrAlaskanNative";
                case E_aBRaceT.Asian:
                    return "AsianOrPacificIslander";
                case E_aBRaceT.Black:
                    return "BlackNotOfHispanicOrigin";
                case E_aBRaceT.Hispanic:
                    return "Hispanic";
                case E_aBRaceT.White:
                    return "WhiteNotOfHispanicOrigin";
                case E_aBRaceT.Other:
                    return "Other";
                case E_aBRaceT.NotFurnished:
                    return "InformationNotProvided";
                case E_aBRaceT.NotApplicable:
                case E_aBRaceT.LeaveBlank:
                default:
                    return string.Empty;
            }
        }
        public static E_aBRaceT FromMismo(string type)
        {
            switch (type)
            {
                case "AmericanIndianOrAlaskanNative":
                    return E_aBRaceT.AmericanIndian;
                case "AsianOrPacificIslander":
                    return E_aBRaceT.Asian;
                case "BlackNotOfHispanicOrigin":
                    return E_aBRaceT.Black;
                case "Hispanic":
                    return E_aBRaceT.Hispanic;
                case "WhiteNotOfHispanicOrigin":
                    return E_aBRaceT.White;
                case "Other":
                    return E_aBRaceT.Other;
                case "InformationNotProvided":
                    return E_aBRaceT.NotFurnished;
                default:
                    return E_aBRaceT.LeaveBlank;
            }
        }
    }
    public class HispanicTypeMap
    {
        public static string ToMismo(E_aHispanicT type)
        {
            switch (type)
            {
                case E_aHispanicT.Hispanic:
                    return "HispanicOrLatino";
                case E_aHispanicT.NotHispanic:
                    return "NotHispanicOrLatino";
                case E_aHispanicT.LeaveBlank:
                default:
                    return "InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication";
            }
        }
        public static E_aHispanicT FromMismo(string type)
        {
            switch (type)
            {
                case "HispanicOrLatino":
                    return E_aHispanicT.Hispanic;
                case "NotHispanicOrLatino":
                    return E_aHispanicT.NotHispanic;
                case "InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication":
                default:
                    return E_aHispanicT.LeaveBlank;
            }
        }

    }
    public class ReoDispositionMap
    {
        public static string ToMismo(E_ReoStatusT disp)
        {
            switch (disp)
            {
                case E_ReoStatusT.PendingSale:
                    return "PendingSale";
                case E_ReoStatusT.Rental:
                    return "RetainForRental";
                case E_ReoStatusT.Residence:
                    return "RetainForPrimaryOrSecondaryResidence";
                case E_ReoStatusT.Sale:
                    return "Sold";
                default:
                    return string.Empty;
            }
        }
        public static E_ReoStatusT FromMismo(string disp)
        {
            switch (disp)
            {
                case "PendingSale":
                    return E_ReoStatusT.PendingSale;
                case "RetainForRental":
                    return E_ReoStatusT.Rental;
                case "RetainForPrimaryOrSecondaryResidence":
                    return E_ReoStatusT.Residence;
                case "Sold":
                    return E_ReoStatusT.Sale;
                default:
                    return E_ReoStatusT.PendingSale; // assume existing property is for sale
            }
        }
    }
    public class PurchaseCreditMap
    {
        public static string FromMismo(string sType, string sSourceType)
        {
            switch (sType)
            {
                case "EarnestMoney":
                    return "Cash Deposit On Sales Contract";
                case "RelocationFunds":
                    return "Relocation Funds";
                case "EmployerAssistedHousing":
                    return "Employer Assisted Housing";
                case "LeasePurchaseFund":
                    return "Lease Purchase Fund";
                case "Other":
                    return "Other";
            }
            switch (sSourceType)
            {
                case "BorrowerPaidOutsideClosing":
                    return "Borrower Paid Fees";
                case "PropertySeller":
                    return "Seller Credit";
                case "Lender":
                    return "Lender Credit";
            }
            return "Other";
        }
    }
    public class InterviewerMethodMap
    {
        public static E_aIntrvwrMethodT FromMismo(string type)
        {
            switch (type)
            {
                case "FaceToFace": return E_aIntrvwrMethodT.FaceToFace;
                case "Mail": return E_aIntrvwrMethodT.ByMail;
                case "Telephone": return E_aIntrvwrMethodT.ByTelephone;
                case "Internet": return E_aIntrvwrMethodT.Internet;
                default:
                    return E_aIntrvwrMethodT.LeaveBlank;
            }
        }
        public static string ToMismo(E_aIntrvwrMethodT type)
        {
            switch (type)
            {
                case E_aIntrvwrMethodT.FaceToFace: return "FaceToFace";
                case E_aIntrvwrMethodT.ByMail: return "Mail";
                case E_aIntrvwrMethodT.ByTelephone: return "Telephone";
                case E_aIntrvwrMethodT.Internet: return "Internet";
                default:
                    return string.Empty;
            }
        }
    }

    public class CaseStateTypeMap
    {
        public static E_sFredProcPointT FromMismo(string type)
        {
            switch (type)
            {
                case "Application":
                    return E_sFredProcPointT.Application;
                case "FinalDisposition":
                    return E_sFredProcPointT.FinalDisposition;
                case "PostClosingQualityControl":
                    return E_sFredProcPointT.PostClosingQualityControl;
                case "Prequalification":
                    return E_sFredProcPointT.Prequalification;
                case "Underwriting":
                    return E_sFredProcPointT.Underwriting;
                default:
                    return E_sFredProcPointT.LeaveBlank;
            }
        }
        public static string ToMismo(E_sFredProcPointT type)
        {
            switch (type)
            {
                case E_sFredProcPointT.Application:
                    return "Application";
                case E_sFredProcPointT.FinalDisposition:
                    return "FinalDisposition";
                case E_sFredProcPointT.PostClosingQualityControl:
                    return "PostClosingQualityControl";
                case E_sFredProcPointT.Prequalification:
                    return "Prequalification";
                case E_sFredProcPointT.Underwriting:
                    return "Underwriting";
                default:
                    return string.Empty;
            }
        }
    }
    public class RefinanceImprovementTypeMap
    {
        public static E_sSpImprovTimeFrameT FromMismo(string type)
        {
            switch (type)
            {
                case "Made":
                    return E_sSpImprovTimeFrameT.Made;
                case "ToBeMade":
                    return E_sSpImprovTimeFrameT.ToBeMade;
                default:
                    return E_sSpImprovTimeFrameT.LeaveBlank;
            }
        }
        public static string ToMismo(E_sSpImprovTimeFrameT type)
        {
            switch (type)
            {
                case E_sSpImprovTimeFrameT.Made:
                    return "Made";
                case E_sSpImprovTimeFrameT.ToBeMade:
                    return "ToBeMade";
                default:
                    return "Unknown";
            }
        }
    }

    /// <summary>
    /// Converts LQB E_sFannieCommunityLendingT to MISMO FNMCommunityLendingProductType.
    /// </summary>
    public static class FNMACommunityLendingTypeMap
    {
        /// <summary>
        /// Converts the given FNMA Community Lending type to MISMO 2.3.1 FNMCommunityLendingProductType.
        /// </summary>
        /// <param name="type">The community lending type</param>
        /// <returns>The MISMO 2.3.1 FNMCommunityLendingProductType as a string.</returns>
        public static string ToMismo(E_sFannieCommunityLendingT type)
        {
            switch (type)
            {
                case E_sFannieCommunityLendingT.CommunityHomeBuyer:
                    return "CommunityHomeBuyerProgram";
                case E_sFannieCommunityLendingT.Fannie32:
                    return "Fannie32";
                case E_sFannieCommunityLendingT.Fannie97:
                    return "Fannie97";
                case E_sFannieCommunityLendingT.LeaveBlank:
                    return string.Empty;
                case E_sFannieCommunityLendingT.MyCommunityMortgage:
                    return "MyCommunityMortgage";
                case E_sFannieCommunityLendingT.HFAPreferred:
                case E_sFannieCommunityLendingT.HFAPreferredRiskSharing:
                case E_sFannieCommunityLendingT.HomeReady:
                    return "Other";
                default:
                    throw new UnhandledEnumException(type);
            }
        }
    }

    /// <summary>
    /// Converts LQB E_sProdDocT and E_sLPurposeT to MISMO LoanDocumentationType.
    /// </summary>
    public static class LoanDocumentationTypeMap
    {
        /// <summary>
        /// Converts the loan doc type to MISMO 2.3.1 LoanDocumentationType.
        /// </summary>
        /// <param name="documentationType">The loan documentation type [sProdDocT].</param>
        /// <param name="transactionType">The loan transaction/purpose type [sLPurposeT].</param>
        /// <returns>The MISMO 2.3.1 LoanDocumentationType as a string.</returns>
        public static string ToMismo(E_sProdDocT documentationType, E_sLPurposeT transactionType)
        {
            if (transactionType == E_sLPurposeT.FhaStreamlinedRefinance || transactionType == E_sLPurposeT.VaIrrrl)
            {
                return "StreamlineRefinance";
            }

            switch (documentationType)
            {
                case E_sProdDocT.Full:
                    return "FullDocumentation";
                case E_sProdDocT.Alt:
                    return "Alternative";
                case E_sProdDocT.Light:
                    return "Reduced";
                case E_sProdDocT.NINA:
                    return "NoDocumentation";
                case E_sProdDocT.NISA:
                    return "NoIncomeOn1003";
                case E_sProdDocT.NINANE:
                    return "NoDepositVerificationEmploymentVerificationOrIncomeVerification";
                case E_sProdDocT.NIVA:
                    return "NoRatio";
                case E_sProdDocT.SISA:
                    return "NoVerificationOfStatedIncomeOrAssests";
                case E_sProdDocT.SIVA:
                    return "NoVerificationOfStatedIncome";
                case E_sProdDocT.VISA:
                    return "NoVerificationOfStatedAssets";
                case E_sProdDocT.NIVANE:
                    return "NoIncomeOn1003";
                case E_sProdDocT.VINA:
                    return "NoDepositVerification";
                case E_sProdDocT.Streamline:
                    return "StreamlineRefinance";
                case E_sProdDocT._12MoPersonalBankStatements:
                case E_sProdDocT._24MoPersonalBankStatements:
                case E_sProdDocT._12MoBusinessBankStatements:
                case E_sProdDocT._24MoBusinessBankStatements:
                case E_sProdDocT.OtherBankStatements:
                case E_sProdDocT._1YrTaxReturns:
                case E_sProdDocT.Voe:
                    return "Alternative";
                case E_sProdDocT.AssetUtilization:
                case E_sProdDocT.DebtServiceCoverage:
                    return "NoRatio";
                case E_sProdDocT.NoIncome:
                    return "NoDocumentation";
                default:
                    throw new UnhandledEnumException(documentationType);
            }
        }
    }

    /// <summary>
    /// Converts LQB E_sSpAppraisalFormT and E_sSpValuationMethodT to MISMO Valuation MethodType.
    /// </summary>
    public static class ValuationMethodTypeMap
    {
        /// <summary>
        /// Converts the Appraisal Form Type and Property Valuation Method to the MISMO 2.3.1 Valuation MethodType.
        /// </summary>
        /// <param name="appraisalForm">The appraisal form type [sSpAppraisalFormT].</param>
        /// <param name="appraisalMethod">The appraisal method type [sSpValuationMethodT].</param>
        /// <returns>The MISMO 2.3.1 valuation methodtype as a string.</returns>
        public static string ToMismo(E_sSpAppraisalFormT appraisalForm, E_sSpValuationMethodT appraisalMethod)
        {
            switch(appraisalMethod)
            {
                case E_sSpValuationMethodT.AutomatedValuationModel:
                    return "AutomatedValuationModel";
                case E_sSpValuationMethodT.DriveBy:
                    return "DriveBy";
                case E_sSpValuationMethodT.None:
                    return "None";
                case E_sSpValuationMethodT.PriorAppraisalUsed:
                    return "PriorAppraisalUsed";
                case E_sSpValuationMethodT.DesktopAppraisal:
                case E_sSpValuationMethodT.DeskReview:
                case E_sSpValuationMethodT.FieldReview:
                case E_sSpValuationMethodT.FullAppraisal:
                case E_sSpValuationMethodT.LeaveBlank:
                case E_sSpValuationMethodT.Other:
                    // Fall back on the form type
                    break;
                default:
                    throw new UnhandledEnumException(appraisalMethod);
            }

            switch(appraisalForm)
            {
                case E_sSpAppraisalFormT.Blank:
                    return appraisalMethod == E_sSpValuationMethodT.FullAppraisal ? "FullAppraisal" : string.Empty;
                case E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport:
                    return "FNM2075";
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport:
                    return "FNM2095Exterior";
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport:
                    return "FNM2055Exterior";
                case E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport:
                    return "FNM1073";
                case E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport:
                    return "FNM1025";
                case E_sSpAppraisalFormT.UniformResidentialAppraisalReport:
                    return "FNM1004";
                case E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport:
                case E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport:
                case E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability:
                case E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport:
                case E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport:
                case E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal:
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport:
                case E_sSpAppraisalFormT.Other:
                    return "Other";
                default:
                    throw new UnhandledEnumException(appraisalForm);
            }
        }
    }
}
