﻿namespace LendersOffice.Conversions
{
    /// <summary>
    /// Indicates a MISMO payload format.
    /// </summary>
    public enum MismoPayloadFormat
    {
        /// <summary>
        /// The MISMO 2.6 format.
        /// </summary>
        Mismo26 = 0,

        /// <summary>
        /// The MISMO 3.3 format.
        /// </summary>
        Mismo33 = 1,

        /// <summary>
        /// The MISMO 3.4 format.
        /// </summary>
        Mismo34 = 2
    }
}
