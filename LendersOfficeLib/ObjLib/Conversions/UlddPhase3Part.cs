﻿namespace LendersOffice.ObjLib.Conversions
{
    using System;

    /// <summary>
    /// A temporary enum for specifying which part of ULDD Phase 3 export to enable while transitioning.
    /// </summary>
    public enum UlddPhase3Part
    {
        /// <summary>
        /// Default value: don't use Phase 3, stick with the Phase 2 values instead.
        /// </summary>
        UsePhase2Instead = 0,

        /// <summary>
        /// Part 1 of the ULDD Phase 3 data. Note that the parts don't have the same data for different GSEs.
        /// </summary>
        Part1 = 1,

        /// <summary>
        /// Part 2 of the ULDD Phase 3 data. Note that the parts don't have the same data for different GSEs.
        /// </summary>
        Part2 = 2,

        /// <summary>
        /// This value might be changed as we roll out the parts for all users.
        /// </summary>
        SystemDefault = UsePhase2Instead
    }
}
