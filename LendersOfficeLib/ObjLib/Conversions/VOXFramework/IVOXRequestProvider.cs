﻿namespace LendersOffice.Conversions.VOXFramework
{
    using LendersOffice.Conversions.Templates;
    using LendersOffice.Integration.VOXFramework;

    /// <summary>
    /// An interface that should be followed by all VOX request providers.
    /// </summary>
    public interface IVOXRequestProvider : IRequestProvider
    {
        /// <summary>
        /// Gets the VOX server to send the request.
        /// </summary>
        /// <value>The VOX server to send the request.</value>
        VOXServer Server { get; }
    }
}
