﻿namespace LendersOffice.Conversions.VOXFramework.Mismo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Integration.VOXFramework;
    using Mismo3Specification.Version4Schema;
    using static Mismo3.Version4.CommonBuilder;
    using static Mismo3.Version4.TypeBuilder;

    /// <summary>
    /// Prepares a VOA payload according to MCL's Smart API format.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed.")]
    public class MismoVOARequestProvider : AbstractMismoVOXRequestProvider
    {
        /// <summary>
        /// A collection of assets keyed by their IDs.
        /// </summary>
        private Dictionary<Guid, IAssetRegular> assetsById;

        /// <summary>
        /// A collection of applications keyed by associated assets.
        /// </summary>
        private Dictionary<Guid, CAppData> appsByAssetId;

        /// <summary>
        /// A collection of asset holder IDs keyed by their associated asset IDs.
        /// </summary>
        private Dictionary<Guid, Guid> assetHolderIdsByAssetId;

        /// <summary>
        /// A collection of asset holders keyed by their IDs.
        /// </summary>
        private Dictionary<Guid, AssetHolder> assetHoldersById;

        /// <summary>
        /// Indicates whether this is a VOD request, a special case of VOA.
        /// </summary>
        private bool isVOD;

        /// <summary>
        /// Initializes a new instance of the <see cref="MismoVOARequestProvider"/> class.
        /// </summary>
        /// <param name="requestData">The configuration for the request.</param>
        /// <param name="individualServiceOrdersToRequest">The orders to request from the vendor.</param>
        public MismoVOARequestProvider(VOARequestData requestData, IReadOnlyCollection<VOXRequestOrder> individualServiceOrdersToRequest)
            : base(requestData, typeof(MismoVOARequestProvider), individualServiceOrdersToRequest)
        {
            this.AbstractAuditor = new MismoVOARequestAuditor(requestData.IsForTpo, requestData.LoanId);

            this.isVOD = this.RequestData.VerificationType == VOAVerificationT.Deposits;
            this.InitializeApplicationsByAsset();
            this.InitializeAssetHolders();
        }

        /// <summary>
        /// Gets the configuration data for the request.
        /// </summary>
        /// <value>The configuration data for the request.</value>
        protected VOARequestData RequestData
        {
            get
            {
                return this.AbstractRequestData as VOARequestData;
            }
        }

        /// <summary>
        /// Gets the request auditor.
        /// </summary>
        /// <value>The request auditor.</value>
        protected MismoVOARequestAuditor Auditor
        {
            get
            {
                return this.AbstractAuditor as MismoVOARequestAuditor;
            }
        }

        /// <summary>
        /// Initializes a label tuple for each party that will be added to the request. This
        /// includes both borrowers on each application as well as each asset holder.
        /// </summary>
        protected override void InitializePartyLabels()
        {
            this.PartyLabels = new Dictionary<Guid, Tuple<string, string>>();
            foreach (var applicationId in this.RequestData.BorrowersInRequest.Select(b => b.Key).Distinct())
            {
                this.PartyLabels[applicationId] = new Tuple<string, string>(string.Empty, string.Empty);
            }

            if (this.RequestData.RequestType == VOXRequestT.Initial)
            {
                foreach (var assetId in this.RequestData.AssetsToInclude.Select(asset => asset.AssetId))
                {
                    this.PartyLabels[assetId] = new Tuple<string, string>(string.Empty, string.Empty);
                }
            }
        }

        /// <summary>
        /// Populates a dictionary sorting apps by their associated asset. This makes it easier to
        /// populate some of the data later in the exporter.
        /// </summary>
        protected void InitializeApplicationsByAsset()
        {
            if (this.RequestData.RequestType != VOXRequestT.Initial)
            {
                return;
            }

            var appsCount = this.DataLoan.nApps;
            var includedAssets = this.RequestData.AssetsToInclude.ToList();
            this.assetsById = new Dictionary<Guid, IAssetRegular>();
            this.appsByAssetId = new Dictionary<Guid, CAppData>();

            for (int appIndex = 0; appIndex < appsCount; appIndex++)
            {
                CAppData appData = this.DataLoan.GetAppData(appIndex);
                IAssetCollection assetCollection = appData.aAssetCollection;

                // Determines whether the considered asset and current application
                // match an asset in the AssetsToInclude list.
                Func<IAsset, bool> shouldIncludeAsset = asset =>
                    includedAssets.Any(includedAsset => includedAsset.AppId == appData.aAppId
                    && includedAsset.AssetId == asset.RecordId);

                for (int i = 0; i < assetCollection.CountRegular; i++)
                {
                    var asset = assetCollection.GetRegularRecordAt(i);
                    if (shouldIncludeAsset(asset))
                    {
                        this.assetsById[asset.RecordId] = asset;
                        this.appsByAssetId[asset.RecordId] = appData;
                        this.RequestData.RecordAssetExport(new VOAOrderAssetRecord(asset.RecordId, asset.ComNm, asset.AccNum));
                    }
                }
            }
        }

        /// <summary>
        /// Populates the asset holder dictionaries from asset data, deduplicating
        /// any that have the same data.
        /// </summary>
        protected void InitializeAssetHolders()
        {
            if (this.RequestData.RequestType != VOXRequestT.Initial)
            {
                return;
            }

            this.assetHoldersById = new Dictionary<Guid, AssetHolder>();
            this.assetHolderIdsByAssetId = new Dictionary<Guid, Guid>();
            
            foreach (var asset in this.assetsById.Values)
            {
                var existingHolder = this.assetHoldersById.Values.FirstOrDefault(h => h.HoldsAsset(asset));
                if (existingHolder.IsValid)
                {
                    this.assetHolderIdsByAssetId[asset.RecordId] = existingHolder.Id;
                }
                else
                {
                    var newHolder = new AssetHolder(asset.ComNm, asset.StAddr, asset.City, asset.State, asset.Zip, asset.PhoneNumber);
                    this.assetHoldersById[newHolder.Id] = newHolder;
                    this.assetHolderIdsByAssetId[asset.RecordId] = newHolder.Id;
                }
            }
        }

        /// <summary>
        /// Creates an ASSET container.
        /// </summary>
        /// <param name="assetData">Data on an asset.</param>
        /// <param name="appData">Data on an application.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>An ASSET container.</returns>
        protected ASSET CreateAsset(IAssetRegular assetData, CAppData appData, int sequenceNumber)
        {
            var asset = new ASSET();
            asset.SequenceNumber = sequenceNumber;
            asset.XlinkLabel = $"Asset{sequenceNumber}";

            // Create a relationship to the corresponding borrower.
            if (assetData.OwnerT == E_AssetOwnerT.Borrower || assetData.OwnerT == E_AssetOwnerT.Joint)
            {
                var borrowerLabel = this.PartyLabels[appData.aAppId].Item1;
                this.CreateRelationship_AssetParty(asset.XlinkLabel, borrowerLabel, isLegalEntity: false);
            }
            else
            {
                var coborrowerLabel = this.PartyLabels[appData.aAppId].Item2;
                this.CreateRelationship_AssetParty(asset.XlinkLabel, coborrowerLabel, isLegalEntity: false);
            }

            // Finally, add a relationship to the institution holding the asset
            var institutionLabel = this.PartyLabels[assetData.RecordId].Item1;
            this.CreateRelationship_AssetParty(asset.XlinkLabel, institutionLabel, isLegalEntity: true);

            asset.AssetDetail = this.CreateAssetDetail(assetData);
            asset.AssetHolder = this.CreateAssetHolder(assetData, appData);

            this.Auditor.ValidateAsset(asset, assetData.AccNm);
            return asset;
        }

        /// <summary>
        /// Creates an ASSETS container.
        /// </summary>
        /// <returns>An ASSETS container.</returns>
        protected ASSETS CreateAssets()
        {
            var assets = new ASSETS();

            foreach (var assetId in this.assetsById.Keys)
            {
                assets.AssetList.Add(this.CreateAsset(this.assetsById[assetId], this.appsByAssetId[assetId], assets.AssetList.Count(a => a != null) + 1));
            }

            return assets;
        }

        /// <summary>
        /// Creates an ASSET_DETAIL container.
        /// </summary>
        /// <param name="asset">Data on an asset.</param>
        /// <returns>An ASSET_DETAIL container.</returns>
        protected ASSET_DETAIL CreateAssetDetail(IAssetRegular asset)
        {
            var assetDetail = new ASSET_DETAIL();
            assetDetail.AssetAccountIdentifier = ToMismoIdentifier(asset.AccNum.Value, isSensitiveData: true);
            assetDetail.AssetAccountInNameOfDescription = ToMismoString(asset.AccNm);
            assetDetail.AssetCashOrMarketValueAmount = ToMismoAmount(asset.Val_rep);
            assetDetail.AssetDescription = ToMismoString(this.FilterStringIfMcl(asset.ComNm));
            assetDetail.AssetType = ToMismoEnum(GetAssetBaseValue(asset.AssetT));

            if (assetDetail.AssetType?.EnumValue == AssetBase.Other)
            {
                assetDetail.AssetTypeOtherDescription = ToMismoString(asset.OtherTypeDesc);
            }

            return assetDetail;
        }

        /// <summary>
        /// Creates an ASSET_HOLDER container.
        /// </summary>
        /// <param name="assetData">Data on an asset.</param>
        /// <param name="appData">Data on an application.</param>
        /// <returns>An ASSET_HOLDER container.</returns>
        protected ASSET_HOLDER CreateAssetHolder(IAssetRegular assetData, CAppData appData)
        {
            var assetHolder = new ASSET_HOLDER();

            var firstName = assetData.OwnerT != E_AssetOwnerT.CoBorrower ? appData.aBFirstNm : appData.aCFirstNm;
            var lastName = assetData.OwnerT != E_AssetOwnerT.CoBorrower ? appData.aBLastNm : appData.aCLastNm;

            assetHolder.Name = this.CreateName(firstName, string.Empty, lastName);

            return assetHolder;
        }

        /// <summary>
        /// Creates a BORROWER container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <param name="partySequenceNumber">The sequence number for the parent PARTY container.</param>
        /// <returns>A BORROWER container.</returns>
        protected BORROWER CreateBorrower(CAppData appData, int partySequenceNumber)
        {
            var borrower = new BORROWER();
            borrower.XlinkLabel = $"Borrower{partySequenceNumber}";
            borrower.BorrowerDetail = this.CreateBorrowerDetail(appData);
            borrower.Residences = this.CreateResidences(appData);

            return borrower;
        }

        /// <summary>
        /// Creates a BORROWER_DETAIL container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <returns>A BORROWER_DETAIL container.</returns>
        protected BORROWER_DETAIL CreateBorrowerDetail(CAppData appData)
        {
            var borrowerDetail = new BORROWER_DETAIL();
            borrowerDetail.BorrowerBirthDate = ToMismoDate(appData.aDob_rep);

            return borrowerDetail;
        }

        /// <summary>
        /// Creates a DEAL container.
        /// </summary>
        /// <returns>A DEAL container.</returns>
        protected override DEAL CreateDeal()
        {
            var deal = base.CreateDeal();

            if (this.RequestData.RequestType == VOXRequestT.Initial)
            {
                deal.Assets = this.CreateAssets();
            }

            return deal;
        }

        /// <summary>
        /// Creates a DOCUMENT container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>A DOCUMENT container.</returns>
        protected DOCUMENT CreateDocument(VOXBorrowerAuthDoc authDoc, int sequenceNumber)
        {
            var document = new DOCUMENT();
            document.SequenceNumber = sequenceNumber;
            document.XlinkLabel = $"Document{sequenceNumber}";

            string borrowerLabel = authDoc.IsForCoborrower
                ? this.PartyLabels[authDoc.AppId].Item2
                : this.PartyLabels[authDoc.AppId].Item1;

            // Since the BORROWER label always has the same sequence number as
            // its parent PARTY, we can do a simple string replace.
            this.CreateRelationship_DocumentRole(document.XlinkLabel, borrowerLabel.Replace("Party", "Borrower"));

            document.DocumentClassification = this.CreateDocumentClassification(authDoc);
            document.Views = this.CreateViews(authDoc);

            if (this.RequestData.VerificationType == VOAVerificationT.Deposits)
            {
                var appData = this.DataLoan.GetAppData(authDoc.AppId);
                appData.BorrowerModeT = authDoc.IsForCoborrower ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;
                this.Auditor.ValidateDocument(document, appData.aNm);
            }

            return document;
        }

        /// <summary>
        /// Creates a DOCUMENTS container.
        /// </summary>
        /// <returns>A DOCUMENTS container.</returns>
        protected override DOCUMENTS CreateDocuments()
        {
            var documents = new DOCUMENTS();

            foreach (var authDoc in this.RequestData.BorrowerAuthDocs)
            {
                documents.DocumentList.Add(this.CreateDocument(authDoc, documents.DocumentList.Count(d => d != null) + 1));
            }

            return documents;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY container.
        /// </summary>
        /// <param name="assetHolder">The asset holder data.</param>
        /// <returns>A LEGAL_ENTITY container.</returns>
        protected LEGAL_ENTITY CreateLegalEntity(AssetHolder assetHolder)
        {
            var legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = this.CreateLegalEntityDetail(assetHolder.CompanyName);

            if (this.isVOD)
            {
                legalEntity.Contacts = this.CreateContacts(string.Empty, assetHolder.Phone, ContactPointRoleBase.Work, string.Empty);
            }

            return legalEntity;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY_DETAIL container.
        /// </summary>
        /// <param name="name">The entity's name.</param>
        /// <returns>A LEGAL_ENTITY_DETAIL container.</returns>
        protected LEGAL_ENTITY_DETAIL CreateLegalEntityDetail(string name)
        {
            var legalEntityDetail = new LEGAL_ENTITY_DETAIL();
            legalEntityDetail.FullName = ToMismoString(this.FilterStringIfMcl(name));

            return legalEntityDetail;
        }

        /// <summary>
        /// Creates a LOAN container.
        /// </summary>
        /// <returns>A LOAN container.</returns>
        protected override LOAN CreateLoan()
        {
            var loan = new LOAN();
            loan.SequenceNumber = 1;
            loan.LoanIdentifiers = this.CreateLoanIdentifiers();

            this.Auditor.ValidateLoan(loan);
            return loan;
        }

        /// <summary>
        /// Creates a PARTIES container.
        /// </summary>
        /// <returns>A PARTIES container.</returns>
        protected override PARTIES CreateParties()
        {
            var parties = new PARTIES();

            // Selected borrowers
            foreach (var borrower in this.RequestData.BorrowersInRequest)
            {
                var appId = borrower.Key;
                var isForCoborrower = borrower.Value;
                var appData = this.DataLoan.GetAppData(appId);

                appData.BorrowerModeT = isForCoborrower ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;

                parties.PartyList.Add(this.CreateParty(appData, parties.PartyList.Count(p => p != null) + 1));
            }

            if (this.RequestData.RequestType == VOXRequestT.Initial)
            {
                // Asset holders
                foreach (var assetHolder in this.assetHoldersById.Values)
                {
                    parties.PartyList.Add(this.CreateParty(assetHolder, parties.PartyList.Count(p => p != null) + 1));
                }
            }

            return parties;
        }

        /// <summary>
        /// Creates a PARTY container for a borrower.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>A PARTY container.</returns>
        protected PARTY CreateParty(CAppData appData, int sequenceNumber)
        {
            var party = new PARTY();
            party.SequenceNumber = sequenceNumber;
            party.XlinkLabel = $"Party{sequenceNumber}";

            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                this.PartyLabels[appData.aAppId] = new Tuple<string, string>(party.XlinkLabel, this.PartyLabels[appData.aAppId].Item2);
            }
            else
            {
                this.PartyLabels[appData.aAppId] = new Tuple<string, string>(this.PartyLabels[appData.aAppId].Item1, party.XlinkLabel);
            }

            party.Individual = this.CreateIndividual(appData);
            party.Roles = this.CreateRoles(appData, sequenceNumber);
            party.TaxpayerIdentifiers = this.CreateTaxpayerIdentifiers(appData.aSsn);

            this.Auditor.ValidateParty_Borrower(party, appData.BorrowerModeT, appData.aNm, this.isVOD);
            return party;
        }

        /// <summary>
        /// Creates a PARTY container for an asset holder.
        /// </summary>
        /// <param name="assetHolder">Data on an asset holder.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>A PARTY container.</returns>
        protected PARTY CreateParty(AssetHolder assetHolder, int sequenceNumber)
        {
            var party = new PARTY();
            party.SequenceNumber = sequenceNumber;
            party.XlinkLabel = $"Party{sequenceNumber}";

            var associatedAssets = this.assetHolderIdsByAssetId.Where(kvp => kvp.Value == assetHolder.Id).Select(kvp => kvp.Key);
            foreach (Guid assetId in associatedAssets)
            {
                this.PartyLabels[assetId] = new Tuple<string, string>(party.XlinkLabel, string.Empty);
            }

            party.LegalEntity = this.CreateLegalEntity(assetHolder);
            party.Addresses = this.CreateAddresses(assetHolder.Address, assetHolder.City, assetHolder.State, assetHolder.Zip);

            return party;
        }

        /// <summary>
        /// Creates a relationship between an asset and a party.
        /// </summary>
        /// <param name="assetLabel">The asset label.</param>
        /// <param name="partyLabel">The party label.</param>
        /// <param name="isLegalEntity">Indicates whether the party is a legal entity.</param>
        protected void CreateRelationship_AssetParty(string assetLabel, string partyLabel, bool isLegalEntity)
        {
            var arcrole = isLegalEntity ? ArcroleVerbPhrase.IsHeldBy : ArcroleVerbPhrase.IsOwnedBy;
            this.CreateRelationship(arcrole, "ASSET", "PARTY", assetLabel, partyLabel);
        }

        /// <summary>
        /// Creates a RESIDENCES container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <returns>A RESIDENCES container.</returns>
        protected RESIDENCES CreateResidences(CAppData appData)
        {
            var residences = new RESIDENCES();
            residences.ResidenceList.Add(this.CreateResidence(appData));

            return residences;
        }

        /// <summary>
        /// Creates a RESIDENCE container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <returns>A RESIDENCE container.</returns>
        protected RESIDENCE CreateResidence(CAppData appData)
        {
            var residence = new RESIDENCE();
            residence.Address = this.CreateAddress(appData.aAddr, appData.aCity, appData.aState, appData.aZip);
            residence.ResidenceDetail = this.CreateResidenceDetail();

            return residence;
        }

        /// <summary>
        /// Creates a RESIDENCE_DETAIL container.
        /// </summary>
        /// <returns>A RESIDENCE_DETAIL container.</returns>
        protected RESIDENCE_DETAIL CreateResidenceDetail()
        {
            var residenceDetail = new RESIDENCE_DETAIL();
            residenceDetail.BorrowerResidencyType = ToMismoEnum(BorrowerResidencyBase.Current);

            return residenceDetail;
        }

        /// <summary>
        /// Creates a ROLE container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <param name="partySequenceNumber">The sequence number for the parent PARTY container.</param>
        /// <returns>A ROLE container.</returns>
        protected ROLE CreateRole(CAppData appData, int partySequenceNumber)
        {
            var role = new ROLE();
            role.SequenceNumber = 1;
            role.Borrower = this.CreateBorrower(appData, partySequenceNumber);
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.Borrower);

            return role;
        }

        /// <summary>
        /// Creates a ROLE_DETAIL container.
        /// </summary>
        /// <param name="role">The role type.</param>
        /// <returns>A ROLE_DETAIL container.</returns>
        protected ROLE_DETAIL CreateRoleDetail(PartyRoleBase role)
        {
            var roleDetail = new ROLE_DETAIL();
            roleDetail.PartyRoleType = ToMismoEnum(role);

            return roleDetail;
        }

        /// <summary>
        /// Creates a ROLES container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <param name="partySequenceNumber">The sequence number for the parent PARTY container.</param>
        /// <returns>A ROLES container.</returns>
        protected ROLES CreateRoles(CAppData appData, int partySequenceNumber)
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole(appData, partySequenceNumber));

            return roles;
        }

        /// <summary>
        /// Creates a SERVICE container for an Initial request.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <param name="appData">Data on an application.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>A SERVICE container.</returns>
        protected SERVICE CreateService(VOXRequestOrder orderToRequest, CAppData appData, int sequenceNumber)
        {
            var service = new SERVICE();
            service.SequenceNumber = sequenceNumber;
            service.XlinkLabel = $"Service{sequenceNumber}";

            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                var borrowerLabel = this.PartyLabels[appData.aAppId].Item1;
                this.CreateRelationship_PartyService(borrowerLabel, service.XlinkLabel);
            }
            else if (appData.BorrowerModeT == E_BorrowerModeT.Coborrower)
            {
                var coborrowerLabel = this.PartyLabels[appData.aAppId].Item2;
                this.CreateRelationship_PartyService(coborrowerLabel, service.XlinkLabel);
            }

            service.VerificationOfIncome = this.CreateVerificationOfIncome(appData.aSsn);

            if (this.RequestData.RequestType == VOXRequestT.Initial && this.RequestData.PayWithCreditCard)
            {
                service.ServicePayments = this.CreateServicePayments();
            }

            service.ServiceProduct = this.CreateServiceProduct(orderToRequest);
            service.ServiceProductFulfillment = this.CreateServiceProductFulfillment();

            this.Auditor.ValidateService(service, appData.aNm, this.RequestData.RequestType, this.RequestData.VerificationType, this.RequestData.PayloadFormatT, includeCCPayment: this.RequestData.PayWithCreditCard);
            return service;
        }

        /// <summary>
        /// Creates a SERVICE container for a Refresh request.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <returns>A SERVICE container.</returns>
        protected SERVICE CreateService_Refresh(VOXRequestOrder orderToRequest)
        {
            var service = new SERVICE();
            service.SequenceNumber = 1; // Refresh only has one service.
            service.XlinkLabel = $"Service{service.SequenceNumber}";
            
            service.VerificationOfIncome = this.CreateVerificationOfIncome(string.Empty);
            service.ServiceProduct = this.CreateServiceProduct(orderToRequest);
            service.ServiceProductFulfillment = this.CreateServiceProductFulfillment();

            this.Auditor.ValidateService(service, string.Empty, this.RequestData.RequestType, this.RequestData.VerificationType, this.RequestData.PayloadFormatT, includeCCPayment: false);
            return service;
        }

        /// <summary>
        /// Creates a SERVICES container.
        /// </summary>
        /// <returns>A SERVICES container.</returns>
        protected override SERVICES CreateServices()
        {
            var services = new SERVICES();

            if (this.RequestData.RequestType == VOXRequestT.Initial || this.RequestData.RequestType == VOXRequestT.Get)
            {
                foreach (VOXRequestOrder orderToRequest in this.IndividualServiceOrdersToRequest)
                {
                    var appData = this.DataLoan.GetAppData(orderToRequest.ApplicationId);
                    appData.BorrowerModeT = orderToRequest.IsForCoborrower.Value ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;

                    services.ServiceList.Add(this.CreateService(orderToRequest, appData, services.ServiceList.Count(s => s != null) + 1));
                }
            }
            else
            {
                services.ServiceList.Add(this.CreateService_Refresh(this.IndividualServiceOrdersToRequest.Single()));
            }

            return services;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT container.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <returns>A SERVICE_PRODUCT container.</returns>
        protected SERVICE_PRODUCT CreateServiceProduct(VOXRequestOrder orderToRequest)
        {
            var serviceProduct = new SERVICE_PRODUCT();
            serviceProduct.ServiceProductRequest = this.CreateServiceProductRequest(orderToRequest);

            return serviceProduct;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_DETAIL container.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <returns>A SERVICE_PRODUCT_DETAIL container.</returns>
        protected SERVICE_PRODUCT_DETAIL CreateServiceProductDetail(VOXRequestOrder orderToRequest)
        {
            var serviceProductDetail = new SERVICE_PRODUCT_DETAIL();

            serviceProductDetail.ServiceProductDescription = ToMismoString(this.isVOD ? "VOD" : "VOA");
            serviceProductDetail.ServiceProductIdentifier = ToMismoIdentifier(orderToRequest.TransactionId.ToString());
            serviceProductDetail.Extension = this.CreateServiceProductDetailExtension();

            return serviceProductDetail;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_DETAIL_EXTENSION container.
        /// </summary>
        /// <returns>A SERVICE_PRODUCT_DETAIL_EXTENSION container.</returns>
        protected SERVICE_PRODUCT_DETAIL_EXTENSION CreateServiceProductDetailExtension()
        {
            var extension = new SERVICE_PRODUCT_DETAIL_EXTENSION();

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                var mclExtension = new MCL_SERVICE_PRODUCT_DETAIL_EXTENSION();
                mclExtension.ServicePreferredResponseFormats = new MCL_SERVICE_PREFERRED_RESPONSE_FORMATS();
                mclExtension.ServicePreferredResponseFormats.ServicePreferredResponseFormat = new List<MCL_SERVICE_PREFERRED_RESPONSE_FORMAT>();

                var pdfFormat = new MCL_SERVICE_PREFERRED_RESPONSE_FORMAT();
                pdfFormat.ServicePreferredResponseFormatDetail = new MCL_SERVICE_PREFERRED_RESPONSE_FORMAT_DETAIL() { PreferredResponseFormatType = ToMismoString("Pdf") };

                var xmlFormat = new MCL_SERVICE_PREFERRED_RESPONSE_FORMAT();
                xmlFormat.ServicePreferredResponseFormatDetail = new MCL_SERVICE_PREFERRED_RESPONSE_FORMAT_DETAIL() { PreferredResponseFormatType = ToMismoString("Xml") };

                mclExtension.ServicePreferredResponseFormats.ServicePreferredResponseFormat.Add(pdfFormat);
                mclExtension.ServicePreferredResponseFormats.ServicePreferredResponseFormat.Add(xmlFormat);

                extension.Other = mclExtension;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                var lqbExtension = new LQB_SERVICE_PRODUCT_DETAIL_EXTENSION();
                lqbExtension.PreferredResponseFormatType = ToMismoString("Pdf");
                extension.Other = lqbExtension;
            }

            return extension;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_FULFILLMENT container.
        /// </summary>
        /// <returns>A SERVICE_PRODUCT_FULFILLMENT container.</returns>
        protected SERVICE_PRODUCT_FULFILLMENT CreateServiceProductFulfillment()
        {
            var serviceProductFulfillment = new SERVICE_PRODUCT_FULFILLMENT();

            if (this.RequestData.RequestType == VOXRequestT.Initial)
            {
                serviceProductFulfillment.ContactPoints = this.CreateContactPoints(this.RequestData.NotificationEmail, string.Empty, ContactPointRoleBase.Blank, string.Empty);
            }

            serviceProductFulfillment.ServiceProductFulfillmentDetail = this.CreateServiceProductFulfillmentDetail();

            return serviceProductFulfillment;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_FULFILLMENT_DETAIL container.
        /// </summary>
        /// <returns>A SERVICE_PRODUCT_FULFILLMENT_DETAIL container.</returns>
        protected SERVICE_PRODUCT_FULFILLMENT_DETAIL CreateServiceProductFulfillmentDetail()
        {
            var serviceProductFulfillmentDetail = new SERVICE_PRODUCT_FULFILLMENT_DETAIL();

            if (this.RequestData.RequestType == VOXRequestT.Initial)
            {
                serviceProductFulfillmentDetail.ServiceRangeStartDate = ToMismoDate(this.DataLoan.m_convertLos.ToDateTimeString(this.RequestData.RequestDate));
            }
            else
            {
                serviceProductFulfillmentDetail.VendorOrderIdentifier = ToMismoIdentifier(this.RequestData.PreviousOrder?.OrderNumber);
            }

            return serviceProductFulfillmentDetail;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_REQUEST container.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <returns>A SERVICE_PRODUCT_REQUEST container.</returns>
        protected SERVICE_PRODUCT_REQUEST CreateServiceProductRequest(VOXRequestOrder orderToRequest)
        {
            var serviceProductRequest = new SERVICE_PRODUCT_REQUEST();
            serviceProductRequest.ServiceProductDetail = this.CreateServiceProductDetail(orderToRequest);

            return serviceProductRequest;
        }

        /// <summary>
        /// Creates a VERIFICATION_OF_INCOME container.
        /// </summary>
        /// <param name="ssn">A social security number.</param>
        /// <returns>A VERIFICATION_OF_INCOME container.</returns>
        protected VERIFICATION_OF_INCOME CreateVerificationOfIncome(string ssn)
        {
            var verificationOfIncome = new VERIFICATION_OF_INCOME();
            verificationOfIncome.VerificationOfIncomeRequest = this.CreateVerificationOfIncomeRequest(ssn);

            return verificationOfIncome;
        }

        /// <summary>
        /// Creates a VERIFICATION_OF_INCOME_REQUEST container.
        /// </summary>
        /// <param name="ssn">A social security number.</param>
        /// <returns>A VERIFICATION_OF_INCOME_REQUEST container.</returns>
        protected VERIFICATION_OF_INCOME_REQUEST CreateVerificationOfIncomeRequest(string ssn)
        {
            var verificationOfIncomeRequest = new VERIFICATION_OF_INCOME_REQUEST();
            verificationOfIncomeRequest.Extension = this.CreateVerificationOfIncomeRequestExtension(ssn);

            return verificationOfIncomeRequest;
        }

        /// <summary>
        /// Creates a VERIFICATION_OF_INCOME_REQUEST_EXTENSION container.
        /// </summary>
        /// <param name="ssn">A social security number.</param>
        /// <returns>A VERIFICATION_OF_INCOME_REQUEST_EXTENSION container.</returns>
        protected VERIFICATION_OF_INCOME_REQUEST_EXTENSION CreateVerificationOfIncomeRequestExtension(string ssn)
        {
            var extension = new VERIFICATION_OF_INCOME_REQUEST_EXTENSION();

            string accountHistory = this.RequestData.AccountHistoryOption == 999 ? "MaxAvailable" : $"_{ this.RequestData.AccountHistoryOption.ToString() }Days";
            string refreshPeriod = this.RequestData.RefreshPeriodOption == 0 ? "None" : $"_{ this.RequestData.RefreshPeriodOption.ToString() }Days";

            string action = string.Empty;

            switch (this.RequestData.RequestType)
            {
                case VOXRequestT.Initial:
                    action = "Order";
                    break;
                case VOXRequestT.Get:
                    action = "Get";
                    break;
                case VOXRequestT.Refresh:
                    action = "Refresh";
                    break;
                default:
                    throw new UnhandledEnumException(this.RequestData.RequestType);
            }

            string last4OfSsn = string.Empty;
            if (!string.IsNullOrEmpty(ssn))
            {
                // SSN is intentionally left out of Get/Refresh requests.
                last4OfSsn = ssn.Substring(Math.Max(0, ssn.Length - 4));
            }

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                var mclExtension = new MCL_VERIFICATION_OF_INCOME_REQUEST_EXTENSION();

                if (this.RequestData.RequestType == VOXRequestT.Initial)
                {
                    mclExtension.FinancialAccountHistoryLength = ToMismoString(accountHistory);
                    mclExtension.RefreshPeriod = ToMismoString(refreshPeriod);
                    mclExtension.SocialSecurityNumberLastFourDigits = ToMismoString(last4OfSsn);
                }

                mclExtension.VerificationOfIncomeRequestActionType = ToMismoString(action);
                extension.Other = mclExtension;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                var lqbExtension = new MCL_VERIFICATION_OF_INCOME_REQUEST_EXTENSION();

                if (this.RequestData.RequestType == VOXRequestT.Initial)
                {
                    lqbExtension.FinancialAccountHistoryLength = ToMismoString(accountHistory);
                    lqbExtension.RefreshPeriod = ToMismoString(refreshPeriod);
                    lqbExtension.SocialSecurityNumberLastFourDigits = ToMismoString(last4OfSsn);
                }

                lqbExtension.VerificationOfIncomeRequestActionType = ToMismoString(action);
                extension.Other = lqbExtension;
            }

            return extension;
        }

        /// <summary>
        /// Represents a legal entity that holds an asset.
        /// </summary>
        /// <remarks>This struct is necessary to ensure that duplicate asset holders aren't sent.</remarks>
        protected struct AssetHolder
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AssetHolder"/> struct.
            /// </summary>
            /// <param name="name">The company name.</param>
            /// <param name="address">The street address.</param>
            /// <param name="city">The city name.</param>
            /// <param name="state">The state name.</param>
            /// <param name="zip">The ZIP code.</param>
            /// <param name="phone">The phone number.</param>
            public AssetHolder(string name, string address, string city, string state, string zip, string phone)
            {
                this.Id = Guid.NewGuid();
                this.CompanyName = name;
                this.Address = address;
                this.City = city;
                this.State = state;
                this.Zip = zip;
                this.Phone = phone;
            }

            /// <summary>
            /// Gets a unique ID for this asset holder. This is not persistent, it only exists
            /// for the duration of the export run.
            /// </summary>
            /// <value>A unique ID for this asset holder.</value>
            public Guid Id { get; private set; }

            /// <summary>
            /// Gets the company name.
            /// </summary>
            /// <value>The company name.</value>
            public string CompanyName { get; private set; }

            /// <summary>
            /// Gets the street address.
            /// </summary>
            /// <value>The street address.</value>
            public string Address { get; private set; }

            /// <summary>
            /// Gets the city name.
            /// </summary>
            /// <value>The city name.</value>
            public string City { get; private set; }

            /// <summary>
            /// Gets the state name.
            /// </summary>
            /// <value>The state name.</value>
            public string State { get; private set; }

            /// <summary>
            /// Gets the ZIP code.
            /// </summary>
            /// <value>The ZIP code.</value>
            public string Zip { get; private set; }

            /// <summary>
            /// Gets the phone number.
            /// </summary>
            /// <value>The phone number.</value>
            public string Phone { get; private set; }

            /// <summary>
            /// Gets a value indicating whether this is a valid asset holder. An invalid holder can
            /// be created by using the default constructor, which will default the ID to the empty GUID.
            /// </summary>
            /// <value>A boolean indicating whether the asset holder is valid.</value>
            public bool IsValid
            {
                get
                {
                    return this.Id != Guid.Empty;
                }
            }

            /// <summary>
            /// Indicates whether the asset holder holds the given asset by comparing the string values.
            /// </summary>
            /// <param name="asset">The asset to test.</param>
            /// <returns>A boolean indicating whether the given asset is held by the asset holder.</returns>
            public bool HoldsAsset(IAssetRegular asset)
            {
                return this.CompanyName.Equals(asset.ComNm)
                    && this.Address.Equals(asset.StAddr)
                    && this.City.Equals(asset.City)
                    && this.State.Equals(asset.State)
                    && this.Zip.Equals(asset.Zip)
                    && this.Phone.Equals(asset.PhoneNumber);
            }
        }
    }
}
