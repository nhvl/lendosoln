﻿namespace LendersOffice.Conversions.VOXFramework.Mismo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using Common;
    using DataAccess;
    using EDocs;
    using LendersOffice.Integration.Templates;
    using LendersOffice.Integration.VOXFramework;
    using Mismo3Specification;
    using Mismo3Specification.Version4Schema;
    using static LendersOffice.Conversions.Mismo3.Version4.TypeBuilder;

    /// <summary>
    /// The base class for VOX requests that use the MCL Smart API format.
    /// </summary>
    public abstract class AbstractMismoVOXRequestProvider : IVOXRequestProvider
    {
        /// <summary>
        /// The root element of the export data.
        /// </summary>
        private readonly Lazy<MESSAGE> message;

        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractMismoVOXRequestProvider"/> class.
        /// </summary>
        /// <param name="requestData">The configuration data for the request.</param>
        /// <param name="derivedExporterType">The derived exporter type.</param>
        /// <param name="individualServiceOrdersToRequest">The orders to request from the vendor.</param>
        public AbstractMismoVOXRequestProvider(AbstractVOXRequestData requestData, Type derivedExporterType, IReadOnlyCollection<VOXRequestOrder> individualServiceOrdersToRequest)
        {
            this.AbstractRequestData = requestData;

            this.DataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypassAndGuaranteedFields(
                this.AbstractRequestData.LoanId,
                derivedExporterType,
                CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(AbstractMismoVOXRequestProvider)));
            this.DataLoan.InitLoad();
            this.DataLoan.SetFormatTarget(FormatTarget.MismoClosing);

            this.AllRelationships = new List<RELATIONSHIP>();

            this.InitializePartyLabels();

            this.IndividualServiceOrdersToRequest = individualServiceOrdersToRequest;
            this.message = new Lazy<MESSAGE>(() => this.CreateMessage());
        }

        /// <summary>
        /// Gets the VOX server to send the request.
        /// </summary>
        /// <value>The VOX server to send the request.</value>
        public VOXServer Server
        {
            get
            {
                if (this.AbstractRequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
                {
                    return new MclVoxServer();
                }

                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the actual orders from the request data that will be sent to the vendor.
        /// </summary>
        /// <value>The orders that will be sent to the vendor.</value>
        public IReadOnlyCollection<VOXRequestOrder> IndividualServiceOrdersToRequest { get; }

        /// <summary>
        /// Gets or sets the abstract configuration data for the request.
        /// </summary>
        /// <value>The abstract configuration data for the request.</value>
        protected AbstractVOXRequestData AbstractRequestData { get; set; }

        /// <summary>
        /// Gets or sets the abstract request auditor.
        /// </summary>
        /// <value>The abstract request auditor.</value>
        protected AbstractMismoVOXRequestAuditor AbstractAuditor { get; set; }

        /// <summary>
        /// Gets or sets the loan file associated with the request.
        /// </summary>
        /// <value>The loan file associated with the request.</value>
        protected CPageData DataLoan { get; set; }

        /// <summary>
        /// Gets or sets a list of relationships that will be populated throughout the export.
        /// </summary>
        /// <value>A list of relationships.</value>
        protected List<RELATIONSHIP> AllRelationships { get; set; }

        /// <summary>
        /// Gets or sets a dictionary of unique identifiers for PARTY elements, linking them to their <c>xlink</c> labels.
        /// </summary>
        /// <value>A dictionary of <c>xlink</c> labels.</value>
        /// <remarks>The tuple is there to support applications, where one ID identifies both
        /// a borrower and co-borrower. Other constructs should only have one label in the tuple.</remarks>
        protected Dictionary<Guid, Tuple<string, string>> PartyLabels { get; set; }

        /// <summary>
        /// Audits the payload and returns a result detailing any errors.
        /// </summary>
        /// <returns>An audit result.</returns>
        public IntegrationAuditResult AuditRequest()
        {
            MESSAGE message = this.message.Value; // populating the export data also populates the audit data
            return this.AbstractAuditor.Results;
        }

        /// <summary>
        /// Serializes the payload to an XML string.
        /// </summary>
        /// <returns>The payload string.</returns>
        public string SerializeRequest()
        {
            XmlSerializerNamespaces namespaces = null;
            XmlAttributeOverrides overrides = null;

            if (this.AbstractRequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                namespaces = ExtensionNamespaces.MCLNamespaces;
                overrides = ExtensionNamespaces.MCLOverrides;
            }
            else if (this.AbstractRequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                namespaces = ExtensionNamespaces.LQBNamespaces;
                overrides = ExtensionNamespaces.LQBOverrides;
            }

            var request = SerializationHelper.XmlSerialize(this.message.Value, false, namespaces, overrides);
            return request;
        }

        /// <summary>
        /// Logs the request.
        /// </summary>
        /// <param name="request">The request as a string.</param>
        public void LogRequest(string request = null)
        {
            if (string.IsNullOrEmpty(request))
            {
                return;
            }

            XElement root = XElement.Parse(request);
            var descendants = root.Descendants();
            var ssns = descendants.Where(el => el.Name.LocalName.Equals("TaxpayerIdentifierValue", StringComparison.OrdinalIgnoreCase));
            foreach (var ssn in ssns)
            {
                ssn.Value = "***-**-****";
            }

            var cardNumbers = descendants.Where(el => el.Name.LocalName.Equals("ServicePaymentAccountIdentifier", StringComparison.OrdinalIgnoreCase));
            foreach (var cardNumber in cardNumbers)
            {
                cardNumber.Value = "**************";
            }

            var cardCvvs = descendants.Where(el => el.Name.LocalName.Equals("ServicePaymentSecondaryCreditAccountIdentifier", StringComparison.OrdinalIgnoreCase));
            foreach (var cardCvv in cardCvvs)
            {
                cardCvv.Value = "***";
            }

            var documents = descendants.Where(el => el.Name.LocalName.Equals("EmbeddedContentXML", StringComparison.OrdinalIgnoreCase));
            foreach (var docs in documents)
            {
                docs.Value = "[Removed]";
            }

            VOXUtilities.LogPayload(root.ToString(SaveOptions.DisableFormatting), false);
        }

        /// <summary>
        /// Filters a string value to remove characters that the MCL SmartAPI cannot accept.
        /// </summary>
        /// <param name="value">The value to filter.</param>
        /// <returns>A filtered string if the payload format is MCL, otherwise, the unchanged value.</returns>
        protected string FilterStringIfMcl(string value)
        {
            string mclRegexPattern = @"[^a-zA-Z0-9 ,#&.!'""\^-]+";
            if (this.AbstractRequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                value = Regex.Replace(value, mclRegexPattern, string.Empty);
            }

            return value;
        }

        /// <summary>
        /// An abstract method forcing derived classes to initialize the party label dictionary.
        /// </summary>
        protected abstract void InitializePartyLabels();

        /// <summary>
        /// Creates an ABOUT_VERSION container.
        /// </summary>
        /// <returns>An ABOUT_VERSION container.</returns>
        protected ABOUT_VERSION CreateAboutVersion()
        {
            var aboutVersion = new ABOUT_VERSION();
            aboutVersion.SequenceNumber = 1;
            aboutVersion.DataVersionIdentifier = ToMismoIdentifier("201703");

            this.AbstractAuditor.ValidateAboutVersion(aboutVersion);
            return aboutVersion;
        }

        /// <summary>
        /// Creates an ABOUT_VERSIONS container.
        /// </summary>
        /// <returns>An ABOUT_VERSIONS container.</returns>
        protected ABOUT_VERSIONS CreateAboutVersions()
        {
            var aboutVersions = new ABOUT_VERSIONS();
            aboutVersions.AboutVersionList.Add(this.CreateAboutVersion());

            return aboutVersions;
        }

        /// <summary>
        /// Creates an ADDRESS container.
        /// </summary>
        /// <param name="streetAddress">The street address.</param>
        /// <param name="city">The address city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The ZIP code.</param>
        /// <returns>An ADDRESS container.</returns>
        protected ADDRESS CreateAddress(string streetAddress, string city, string state, string zip)
        {
            var address = new ADDRESS();
            address.SequenceNumber = 1;
            address.AddressLineText = ToMismoString(streetAddress);
            address.CityName = ToMismoString(city);
            address.StateCode = ToMismoCode(state);
            address.PostalCode = ToMismoCode(zip);

            return address;
        }

        /// <summary>
        /// Creates an ADDRESSES container.
        /// </summary>
        /// <param name="streetAddress">The street address.</param>
        /// <param name="city">The address city.</param>
        /// <param name="state">The state.</param>
        /// <param name="zip">The ZIP code.</param>
        /// <returns>An ADDRESSES container.</returns>
        protected ADDRESSES CreateAddresses(string streetAddress, string city, string state, string zip)
        {
            var addresses = new ADDRESSES();
            addresses.AddressList.Add(this.CreateAddress(streetAddress, city, state, zip));

            return addresses;
        }

        /// <summary>
        /// Creates a CONTACT container.
        /// </summary>
        /// <param name="email">An email address.</param>
        /// <param name="phone">A phone number.</param>
        /// <param name="phoneType">A phone number type.</param>
        /// <param name="fax">A fax number.</param>
        /// <returns>A CONTACT container.</returns>
        protected CONTACT CreateContact(string email, string phone, ContactPointRoleBase phoneType, string fax)
        {
            var contact = new CONTACT();
            contact.ContactPoints = this.CreateContactPoints(email, phone, phoneType, fax);

            return contact;
        }

        /// <summary>
        /// Creates a CONTACTS container.
        /// </summary>
        /// <param name="email">An email address.</param>
        /// <param name="phone">A phone number.</param>
        /// <param name="phoneType">A phone number type.</param>
        /// <param name="fax">A fax number.</param>
        /// <returns>A CONTACTS container.</returns>
        protected CONTACTS CreateContacts(string email, string phone, ContactPointRoleBase phoneType, string fax)
        {
            var contacts = new CONTACTS();
            contacts.ContactList.Add(this.CreateContact(email, phone, phoneType, fax));

            return contacts;
        }

        /// <summary>
        /// Creates a CONTACT_POINT container with an email address.
        /// </summary>
        /// <param name="email">The email address.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        protected CONTACT_POINT CreateContactPoint_Email(string email, int sequenceNumber)
        {
            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequenceNumber;
            contactPoint.ContactPointEmail = this.CreateContactPointEmail(email);

            return contactPoint;
        }

        /// <summary>
        /// Creates a CONTACT_POINT container with a fax number.
        /// </summary>
        /// <param name="fax">The fax number.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        protected CONTACT_POINT CreateContactPoint_Fax(string fax, int sequenceNumber)
        {
            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequenceNumber;
            contactPoint.ContactPointTelephone = this.CreateContactPointTelephone(fax, isFax: true);
            contactPoint.ContactPointDetail = this.CreateContactPointDetail(ContactPointRoleBase.Other, "Fax");

            return contactPoint;
        }

        /// <summary>
        /// Creates a CONTACT_POINT container with a phone number.
        /// </summary>
        /// <param name="phone">The phone number.</param>
        /// <param name="phoneType">The phone number type.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        protected CONTACT_POINT CreateContactPoint_Phone(string phone, ContactPointRoleBase phoneType, int sequenceNumber)
        {
            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequenceNumber;
            contactPoint.ContactPointTelephone = this.CreateContactPointTelephone(phone);
            contactPoint.ContactPointDetail = this.CreateContactPointDetail(phoneType);

            return contactPoint;
        }

        /// <summary>
        /// Creates a CONTACT_POINTS container.
        /// </summary>
        /// <param name="appData">Data on a borrower.</param>
        /// <returns>A CONTACT_POINTS container.</returns>
        protected CONTACT_POINTS CreateContactPoints(CAppData appData)
        {
            var contactPoints = new CONTACT_POINTS();

            if (!string.IsNullOrEmpty(appData.aEmail))
            {
                contactPoints.ContactPointList.Add(this.CreateContactPoint_Email(appData.aEmail, contactPoints.ContactPointList.Count(c => c != null) + 1));
            }

            if (!string.IsNullOrEmpty(appData.aHPhone))
            {
                contactPoints.ContactPointList.Add(this.CreateContactPoint_Phone(appData.aHPhone, ContactPointRoleBase.Home, contactPoints.ContactPointList.Count(c => c != null) + 1));
            }

            if (!string.IsNullOrEmpty(appData.aCellPhone))
            {
                contactPoints.ContactPointList.Add(this.CreateContactPoint_Phone(appData.aCellPhone, ContactPointRoleBase.Mobile, contactPoints.ContactPointList.Count(c => c != null) + 1));
            }

            if (!string.IsNullOrEmpty(appData.aBusPhone))
            {
                contactPoints.ContactPointList.Add(this.CreateContactPoint_Phone(appData.aBusPhone, ContactPointRoleBase.Work, contactPoints.ContactPointList.Count(c => c != null) + 1));
            }

            return contactPoints;
        }

        /// <summary>
        /// Creates a CONTACT_POINTS container.
        /// </summary>
        /// <param name="email">An email address.</param>
        /// <param name="phone">A phone number.</param>
        /// <param name="phoneType">A phone number type.</param>
        /// <param name="fax">A fax number.</param>
        /// <returns>A CONTACT_POINTS container.</returns>
        protected CONTACT_POINTS CreateContactPoints(string email, string phone, ContactPointRoleBase phoneType, string fax)
        {
            var contactPoints = new CONTACT_POINTS();

            if (!string.IsNullOrEmpty(email))
            {
                contactPoints.ContactPointList.Add(this.CreateContactPoint_Email(email, contactPoints.ContactPointList.Count(c => c != null) + 1));
            }

            if (!string.IsNullOrEmpty(phone))
            {
                contactPoints.ContactPointList.Add(this.CreateContactPoint_Phone(phone, phoneType, contactPoints.ContactPointList.Count(c => c != null) + 1));
            }

            if (!string.IsNullOrEmpty(fax))
            {
                contactPoints.ContactPointList.Add(this.CreateContactPoint_Fax(fax, contactPoints.ContactPointList.Count(c => c != null) + 1));
            }

            return contactPoints;
        }

        /// <summary>
        /// Creates a CONTACT_POINT_EMAIL container.
        /// </summary>
        /// <param name="email">The email address.</param>
        /// <returns>A CONTACT_POINT_EMAIL container.</returns>
        protected CONTACT_POINT_EMAIL CreateContactPointEmail(string email)
        {
            var contactPointEmail = new CONTACT_POINT_EMAIL();
            contactPointEmail.ContactPointEmailValue = ToMismoValue(email);

            return contactPointEmail;
        }

        /// <summary>
        /// Creates a CONTACT_POINT_TELEPHONE container.
        /// </summary>
        /// <param name="phone">The phone number.</param>
        /// <param name="isFax">Indicates whether the number is a fax number.</param>
        /// <returns>A CONTACT_POINT_TELEPHONE container.</returns>
        protected CONTACT_POINT_TELEPHONE CreateContactPointTelephone(string phone, bool isFax = false)
        {
            var contactPointTelephone = new CONTACT_POINT_TELEPHONE();

            if (isFax)
            {
                contactPointTelephone.ContactPointFaxValue = ToMismoNumericString(phone);
            }
            else
            {
                contactPointTelephone.ContactPointTelephoneValue = ToMismoNumericString(phone);
            }

            return contactPointTelephone;
        }

        /// <summary>
        /// Creates a CONTACT_POINT_DETAIL container.
        /// </summary>
        /// <param name="contactType">The contact type.</param>
        /// <param name="contactTypeOtherDescription">The contact description if the type is Other.</param>
        /// <returns>A CONTACT_POINT_DETAIL container.</returns>
        protected CONTACT_POINT_DETAIL CreateContactPointDetail(ContactPointRoleBase contactType, string contactTypeOtherDescription = null)
        {
            var contactPointDetail = new CONTACT_POINT_DETAIL();
            contactPointDetail.ContactPointRoleType = ToMismoEnum(contactType);

            if (contactType == ContactPointRoleBase.Other && !string.IsNullOrEmpty(contactTypeOtherDescription))
            {
                contactPointDetail.ContactPointRoleTypeOtherDescription = ToMismoString(contactTypeOtherDescription);
            }

            return contactPointDetail;
        }

        /// <summary>
        /// Creates a DEAL container.
        /// </summary>
        /// <returns>A DEAL container.</returns>
        protected virtual DEAL CreateDeal()
        {
            var deal = new DEAL();
            deal.SequenceNumber = 1;

            deal.Parties = this.CreateParties(); // This should always be called first to prevent issues generating relationships.
            deal.Loans = this.CreateLoans();
            deal.Services = this.CreateServices();

            return deal;
        }

        /// <summary>
        /// Creates a DEALS container.
        /// </summary>
        /// <returns>A DEALS container.</returns>
        protected DEALS CreateDeals()
        {
            var deals = new DEALS();
            deals.DealList.Add(this.CreateDeal());

            return deals;
        }

        /// <summary>
        /// Creates a DEAL_SET container.
        /// </summary>
        /// <returns>A DEAL_SET container.</returns>
        protected DEAL_SET CreateDealSet()
        {
            var dealSet = new DEAL_SET();
            dealSet.SequenceNumber = 1;
            dealSet.Deals = this.CreateDeals();

            return dealSet;
        }

        /// <summary>
        /// Creates a DEAL_SETS container.
        /// </summary>
        /// <returns>A DEAL_SETS container.</returns>
        protected DEAL_SETS CreateDealSets()
        {
            var dealSets = new DEAL_SETS();
            dealSets.DealSetList.Add(this.CreateDealSet());

            return dealSets;
        }

        /// <summary>
        /// Forces a derived class to populate a DOCUMENTS container.
        /// </summary>
        /// <returns>A DOCUMENTS container.</returns>
        protected abstract DOCUMENTS CreateDocuments();

        /// <summary>
        /// Creates a DOCUMENT_CLASS container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <returns>A DOCUMENT_CLASS container.</returns>
        protected DOCUMENT_CLASS CreateDocumentClass(VOXBorrowerAuthDoc authDoc)
        {
            var documentClass = new DOCUMENT_CLASS();
            documentClass.SequenceNumber = 1;
            documentClass.DocumentType = ToMismoEnum(DocumentBase.Other);
            documentClass.DocumentTypeOtherDescription = ToMismoString("BorrowerWrittenAuthorization");

            return documentClass;
        }

        /// <summary>
        /// Creates a DOCUMENT_CLASSES container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <returns>A DOCUMENT_CLASSES container.</returns>
        protected DOCUMENT_CLASSES CreateDocumentClasses(VOXBorrowerAuthDoc authDoc)
        {
            var documentClasses = new DOCUMENT_CLASSES();
            documentClasses.DocumentClassList.Add(this.CreateDocumentClass(authDoc));

            return documentClasses;
        }

        /// <summary>
        /// Creates a DOCUMENT_CLASSIFICATION container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <returns>A DOCUMENT_CLASSIFICATION container.</returns>
        protected DOCUMENT_CLASSIFICATION CreateDocumentClassification(VOXBorrowerAuthDoc authDoc)
        {
            var documentClassificiation = new DOCUMENT_CLASSIFICATION();
            documentClassificiation.DocumentClasses = this.CreateDocumentClasses(authDoc);

            return documentClassificiation;
        }

        /// <summary>
        /// Creates a FOREIGN_OBJECT container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <returns>A FOREIGN_OBJECT container.</returns>
        protected FOREIGN_OBJECT CreateForeignObject(VOXBorrowerAuthDoc authDoc)
        {
            var foreignObject = new FOREIGN_OBJECT();
            foreignObject.CharacterEncodingSetType = ToMismoEnum(CharacterEncodingSetBase.UTF8);
            foreignObject.MIMETypeIdentifier = ToMismoIdentifier("application/pdf");
            foreignObject.ObjectEncodingType = ToMismoEnum(ObjectEncodingBase.Base64);

            var repo = EDocumentRepository.GetUserRepository(this.AbstractRequestData.Principal);
            var document = repo.GetDocumentById(authDoc.DocId);

            string tempPath;
            try
            {
                tempPath = document.GetPDFTempFile_Current();
            }
            catch (FileNotFoundException)
            {
                tempPath = document.GetPDFTempFile_Original();
            }

            var documentContents = File.ReadAllBytes(tempPath);
            foreignObject.EmbeddedContentXML = ToMismoXml(documentContents);

            return foreignObject;
        }

        /// <summary>
        /// Creates a DOCUMENT_SET container.
        /// </summary>
        /// <returns>A DOCUMENT_SET container.</returns>
        protected DOCUMENT_SET CreateDocumentSet()
        {
            var documentSet = new DOCUMENT_SET();
            documentSet.Documents = this.CreateDocuments();

            return documentSet;
        }

        /// <summary>
        /// Creates a DOCUMENT_SETS container.
        /// </summary>
        /// <returns>A DOCUMENT_SETS container.</returns>
        protected DOCUMENT_SETS CreateDocumentSets()
        {
            var documentSets = new DOCUMENT_SETS();
            documentSets.DocumentSetList.Add(this.CreateDocumentSet());

            return documentSets;
        }

        /// <summary>
        /// Creates an INDIVIDUAL container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <returns>An INDIVIDUAL container.</returns>
        protected INDIVIDUAL CreateIndividual(CAppData appData)
        {
            var individual = new INDIVIDUAL();

            individual.ContactPoints = this.CreateContactPoints(appData);

            var firstName = appData.aFirstNm;
            var middleName = appData.aMidNm;
            var lastName = appData.aLastNm;
            if (this.AbstractRequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                firstName = this.RemoveInvalidNameCharsFromNameForMcl(firstName);
                middleName = this.RemoveInvalidNameCharsFromNameForMcl(middleName);
                lastName = this.RemoveInvalidNameCharsFromNameForMcl(lastName);
            }

            individual.Name = this.CreateName(firstName, middleName, lastName);

            return individual;
        }

        /// <summary>
        /// Forces derived classes to populate a LOAN container.
        /// </summary>
        /// <returns>A LOAN container.</returns>
        protected abstract LOAN CreateLoan();

        /// <summary>
        /// Creates a LOANS container.
        /// </summary>
        /// <returns>A LOANS container.</returns>
        protected LOANS CreateLoans()
        {
            var loans = new LOANS();
            loans.LoanList.Add(this.CreateLoan());

            return loans;
        }

        /// <summary>
        /// Creates a LOAN_IDENTIFIER container.
        /// </summary>
        /// <returns>A LOAN_IDENTIFIER container.</returns>
        protected LOAN_IDENTIFIER CreateLoanIdentifier()
        {
            var loanIdentifier = new LOAN_IDENTIFIER();
            loanIdentifier.SequenceNumber = 1;
            loanIdentifier.LoanIdentifier = ToMismoIdentifier(this.DataLoan.sLenderCaseNum);

            return loanIdentifier;
        }

        /// <summary>
        /// Creates a LOAN_IDENTIFIERS container.
        /// </summary>
        /// <returns>A LOAN_IDENTIFIERS container.</returns>
        protected LOAN_IDENTIFIERS CreateLoanIdentifiers()
        {
            var loanIdentifiers = new LOAN_IDENTIFIERS();
            loanIdentifiers.LoanIdentifierList.Add(this.CreateLoanIdentifier());

            return loanIdentifiers;
        }

        /// <summary>
        /// Creates a NAME container.
        /// </summary>
        /// <param name="first">The first name.</param>
        /// <param name="middle">The middle name.</param>
        /// <param name="last">The last name.</param>
        /// <returns>A NAME container.</returns>
        protected NAME CreateName(string first, string middle, string last)
        {
            var name = new NAME();
            name.FirstName = ToMismoString(first);
            name.MiddleName = ToMismoString(middle);
            name.LastName = ToMismoString(last);

            return name;
        }

        /// <summary>
        /// Forces derived classes to populate a PARTIES container.
        /// </summary>
        /// <returns>A PARTIES container.</returns>
        protected abstract PARTIES CreateParties();

        /// <summary>
        /// Creates a relationship with the given data and adds it to the relationship list.
        /// </summary>
        /// <param name="arcroleVerbPhrase">The relationship verb phrase.</param>
        /// <param name="fromName">The first element in the relationship.</param>
        /// <param name="toname">The second element in the relationship.</param>
        /// <param name="fromLabel">The label for the first element in the relationship.</param>
        /// <param name="tolabel">The label for the second element in the relationship.</param>
        protected void CreateRelationship(ArcroleVerbPhrase arcroleVerbPhrase, string fromName, string toname, string fromLabel, string tolabel)
        {
            var relationship = new RELATIONSHIP();
            relationship.SequenceNumber = this.AllRelationships.Count(r => r != null) + 1;

            relationship.Arcrole = $"{Mismo3Constants.MclXlinkArcroleUrn}{fromName}_{Mismo3Utilities.GetXmlEnumName(arcroleVerbPhrase)}_{toname}";
            relationship.From = fromLabel;
            relationship.To = tolabel;

            this.AllRelationships.Add(relationship);
        }

        /// <summary>
        /// Creates a relationship between a DOCUMENT and a ROLE.
        /// </summary>
        /// <param name="documentLabel">The document label.</param>
        /// <param name="roleLabel">The role label.</param>
        protected void CreateRelationship_DocumentRole(string documentLabel, string roleLabel)
        {
            this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, nameof(DOCUMENT), nameof(ROLE), documentLabel, roleLabel);
        }

        /// <summary>
        /// Creates a relationship between a PARTY and a SERVICE.
        /// </summary>
        /// <param name="partyLabel">The party label.</param>
        /// <param name="serviceLabel">The service label.</param>
        protected void CreateRelationship_PartyService(string partyLabel, string serviceLabel)
        {
            this.CreateRelationship(ArcroleVerbPhrase.IsVerifiedBy, nameof(PARTY), nameof(SERVICE), partyLabel, serviceLabel);
        }

        /// <summary>
        /// Populates the RELATIONSHIPS container with the data in the relationship list.
        /// </summary>
        /// <returns>A RELATIONSHIPS container.</returns>
        protected RELATIONSHIPS CreateRelationships()
        {
            var relationships = new RELATIONSHIPS();
            relationships.RelationshipList.AddRange(this.AllRelationships);

            return relationships;
        }

        /// <summary>
        /// Forces derived classes to populate a SERVICES container.
        /// </summary>
        /// <returns>A SERVICES container.</returns>
        protected abstract SERVICES CreateServices();

        /// <summary>
        /// Creates a SERVICE_PAYMENT container.
        /// </summary>
        /// <returns>A SERVICE_PAYMENT.</returns>
        protected SERVICE_PAYMENT CreateServicePayment()
        {
            var servicePayment = new SERVICE_PAYMENT();
            servicePayment.Address = this.CreateAddress(this.AbstractRequestData.BillingStreetAddress, this.AbstractRequestData.BillingCity, this.AbstractRequestData.BillingState, this.AbstractRequestData.BillingZipcode);

            var firstName = this.AbstractRequestData.BillingFirstName;
            var middleName = this.AbstractRequestData.BillingMiddleName;
            var lastName = this.AbstractRequestData.BillingLastName;
            if (this.AbstractRequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                firstName = this.RemoveInvalidNameCharsFromNameForMcl(firstName);
                middleName = this.RemoveInvalidNameCharsFromNameForMcl(middleName);
                lastName = this.RemoveInvalidNameCharsFromNameForMcl(lastName);
            }

            servicePayment.Name = this.CreateName(firstName, middleName, lastName);
            servicePayment.ServicePaymentDetail = this.CreateServicePaymentDetail();

            return servicePayment;
        }

        /// <summary>
        /// Creates a SERVICE_PAYMENTS container.
        /// </summary>
        /// <returns>A SERVICE_PAYMENTS container.</returns>
        protected SERVICE_PAYMENTS CreateServicePayments()
        {
            var servicePayments = new SERVICE_PAYMENTS();
            servicePayments.ServicePaymentList.Add(this.CreateServicePayment());

            return servicePayments;
        }

        /// <summary>
        /// Creates a SERVICE_PAYMENT_DETAIL container.
        /// </summary>
        /// <returns>A SERVICE_PAYMENT_DETAIL container.</returns>
        protected SERVICE_PAYMENT_DETAIL CreateServicePaymentDetail()
        {
            var servicePaymentDetail = new SERVICE_PAYMENT_DETAIL();
            servicePaymentDetail.ServicePaymentAccountIdentifier = ToMismoIdentifier(this.AbstractRequestData.BillingCardNumber);
            servicePaymentDetail.ServicePaymentSecondaryCreditAccountIdentifier = ToMismoIdentifier(this.AbstractRequestData.BillingCVV);

            var expiration = new DateTime(this.AbstractRequestData.BillingExpirationYear, this.AbstractRequestData.BillingExpirationMonth, 1).ToString("yyyy-MM");
            servicePaymentDetail.ServicePaymentCreditAccountExpirationDate = ToMismoDate(expiration);

            return servicePaymentDetail;
        }

        /// <summary>
        /// Creates a TAXPAYER_IDENTIFIER container.
        /// </summary>
        /// <param name="ssn">A social security number.</param>
        /// <returns>A TAXPAYER_IDENTIFIER container.</returns>
        protected TAXPAYER_IDENTIFIER CreateTaxpayerIdentifier(string ssn)
        {
            var taxpayerIdentifier = new TAXPAYER_IDENTIFIER();
            taxpayerIdentifier.SequenceNumber = 1;
            taxpayerIdentifier.TaxpayerIdentifierType = ToMismoEnum(TaxpayerIdentifierBase.SocialSecurityNumber, true);
            taxpayerIdentifier.TaxpayerIdentifierValue = ToMismoNumericString(ssn, true);

            return taxpayerIdentifier;
        }

        /// <summary>
        /// Creates a TAXPAYER_IDENTIFIERS container.
        /// </summary>
        /// <param name="ssn">A social security number.</param>
        /// <returns>A TAXPAYER_IDENTIFIERS container.</returns>
        protected TAXPAYER_IDENTIFIERS CreateTaxpayerIdentifiers(string ssn)
        {
            var taxpayerIdentifiers = new TAXPAYER_IDENTIFIERS();
            taxpayerIdentifiers.TaxpayerIdentifierList.Add(this.CreateTaxpayerIdentifier(ssn));
            return taxpayerIdentifiers;
        }

        /// <summary>
        /// Creates a VIEW container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <returns>A VIEW container.</returns>
        protected VIEW CreateView(VOXBorrowerAuthDoc authDoc)
        {
            var view = new VIEW();
            view.SequenceNumber = 1;
            view.ViewFiles = this.CreateViewFiles(authDoc);

            return view;
        }

        /// <summary>
        /// Creates a VIEWS container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <returns>A VIEWS container.</returns>
        protected VIEWS CreateViews(VOXBorrowerAuthDoc authDoc)
        {
            var views = new VIEWS();
            views.ViewList.Add(this.CreateView(authDoc));

            return views;
        }

        /// <summary>
        /// Creates a VIEW_FILE container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <returns>A VIEW_FILE container.</returns>
        protected VIEW_FILE CreateViewFile(VOXBorrowerAuthDoc authDoc)
        {
            var viewFile = new VIEW_FILE();
            viewFile.SequenceNumber = 1;
            viewFile.ForeignObject = this.CreateForeignObject(authDoc);

            return viewFile;
        }

        /// <summary>
        /// Creates a VIEW_FILES container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <returns>A VIEW_FILES container.</returns>
        protected VIEW_FILES CreateViewFiles(VOXBorrowerAuthDoc authDoc)
        {
            var viewFiles = new VIEW_FILES();
            viewFiles.ViewFileList.Add(this.CreateViewFile(authDoc));

            return viewFiles;
        }

        /// <summary>
        /// Removes invalid characters from the input name.
        /// </summary>
        /// <param name="name">The name to fix up.</param>
        /// <returns>The fixed name.</returns>
        /// <remarks>
        /// MCL Smart API doesn't allow certain characters in some of their NAME containers, thus the need for the boolean parameter.
        /// The error message is [Alphabetical characters, space, and { -, ' } are allowed and the first character needs to be alphabetical].
        /// </remarks>
        protected string RemoveInvalidNameCharsFromNameForMcl(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return name;
            }

            var mclPattern = @"^[^a-zA-Z]+|[^a-zA-Z- ']";
            return Regex.Replace(name, mclPattern, string.Empty);
        }

        /// <summary>
        /// Creates a MESSAGE container, which is the root of the Mismo 3.4 export.
        /// </summary>
        /// <returns>A MESSAGE container.</returns>
        /// <remarks>
        /// As the export process also hooks into the auditor and the request data to save the
        /// information of the order, this method has side effects, meaning access should go through
        /// <see cref="message"/>.
        /// </remarks>
        private MESSAGE CreateMessage()
        {
            var message = new MESSAGE();

            message.AboutVersions = this.CreateAboutVersions();
            message.DealSets = this.CreateDealSets();

            if (this.AbstractRequestData.RequestType == VOXRequestT.Initial)
            {
                message.DocumentSets = this.CreateDocumentSets();

                // DO NOT MOVE THIS CALL - Relationships must be created after all other data.
                message.DealSets.DealSetList.First().Deals.DealList.First().Relationships = this.CreateRelationships();
            }

            return message;
        }
    }
}
