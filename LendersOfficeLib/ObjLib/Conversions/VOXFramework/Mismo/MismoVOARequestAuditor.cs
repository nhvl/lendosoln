﻿namespace LendersOffice.Conversions.VOXFramework.Mismo
{
    using System;
    using System.Linq;
    using Common;
    using DataAccess;
    using Integration.VOXFramework;
    using Mismo3Specification;
    using Mismo3Specification.Version4Schema;
    using VOXErrors = LendersOffice.Common.ErrorMessages.VOXErrors;

    /// <summary>
    /// Validates the data in a VOA request.
    /// </summary>
    public class MismoVOARequestAuditor : AbstractMismoVOXRequestAuditor
    {
        /// <summary>
        /// Whether or not this is for TPO portal.
        /// </summary>
        /// <remarks>
        /// I don't want to do it this way but this is the quickest for now. For a refactor, I plan on passing in a page dictionary or something
        /// into the auditor and use that to find the page links.
        /// </remarks>
        private bool isTpo;

        /// <summary>
        /// The loan id.
        /// </summary>
        private Guid loanId;

        /// <summary>
        /// Initializes a new instance of the <see cref="MismoVOARequestAuditor"/> class.
        /// </summary>
        /// <param name="isTpo">Whether or not the audit is for TPO.</param>
        /// <param name="loanId">The loan id.</param>
        public MismoVOARequestAuditor(bool isTpo, Guid loanId) : base()
        {
            this.isTpo = isTpo;
            this.loanId = loanId;
        }

        /// <summary>
        /// Validates an asset container.
        /// </summary>
        /// <param name="asset">The asset container.</param>
        /// <param name="assetName">The name of the asset.</param>
        public void ValidateAsset(ASSET asset, string assetName)
        {
            var sectionName = $"Asset - {assetName}";
            var fieldPath = this.isTpo ? $"/webapp/Loan1003.aspx?src=pipeline&p=1&loanid={this.loanId.ToString("N")}#tabs-2" : @"/newlos/BorrowerInfo.aspx?pg=5";

            var assetDetail = asset?.AssetDetail;

            this.RecordErrorIfEmpty(
                sectionName,
                "Account Number",
                fieldPath,
                assetDetail?.AssetAccountIdentifier?.Value,
                VOXErrors.Asset.MissingAccountNumber);

            this.RecordErrorIfEmpty(
                sectionName,
                "Account Holder Name",
                fieldPath,
                assetDetail?.AssetAccountInNameOfDescription?.Value,
                VOXErrors.Asset.MissingAccountName);

            this.RecordErrorIfEmpty(
                sectionName,
                "Cash Value",
                fieldPath,
                assetDetail?.AssetCashOrMarketValueAmount?.Value,
                VOXErrors.Asset.MissingCashValue);

            this.RecordErrorIfEmpty(
                sectionName,
                "Description",
                fieldPath,
                assetDetail?.AssetDescription?.Value,
                VOXErrors.Asset.MissingDescription);

            this.RecordErrorIfEmpty(
                sectionName,
                "Account Type",
                fieldPath,
                assetDetail?.AssetType?.Value,
                VOXErrors.Asset.MissingAccountType);

            var assetHolderName = asset?.AssetHolder?.Name as NAME;

            this.RecordErrorIfEmpty(
                sectionName,
                "Asset Holder First Name",
                fieldPath,
                assetHolderName?.FirstName?.Value,
                VOXErrors.Asset.MissingFirstName);

            this.RecordErrorIfEmpty(
                sectionName,
                "Asset Holder Last Name",
                fieldPath,
                assetHolderName?.LastName?.Value,
                VOXErrors.Asset.MissingLastName);
        }

        /// <summary>
        /// Validates a loan container.
        /// </summary>
        /// <param name="loan">The loan container.</param>
        public void ValidateLoan(LOAN loan)
        {
            var sectionName = "General Loan Data";
            var fieldPath = this.isTpo ? $"/webapp/Loan1003.aspx?src=pipeline&p=1&loanid={this.loanId.ToString("N")}#tabs-1" : @"/newlos/Status/General.aspx?pg=5";

            var loanIdentifier = loan?.LoanIdentifiers?.LoanIdentifierList?.FirstOrDefault();

            this.RecordErrorIfEmpty(
                sectionName,
                "Lender Case Number",
                $"{fieldPath}highlightId=sLenderCaseNum",
                loanIdentifier?.LoanIdentifier?.Value,
                VOXErrors.Loan.MissingLenderCaseNumber);
        }

        /// <summary>
        /// Validates a party container representing a borrower.
        /// </summary>
        /// <param name="party">The party container.</param>
        /// <param name="borrowerMode">Indicates whether the party is a borrower or coborrower.</param>
        /// <param name="partyName">The party's name.</param>
        /// <param name="isVOD">Indicates whether the request is VOD, a special case of VOA.</param>
        public void ValidateParty_Borrower(PARTY party, E_BorrowerModeT borrowerMode, string partyName, bool isVOD)
        {
            string sectionName = $"Borrower - {partyName}";
            var fieldPath = this.isTpo ? $"/webapp/Loan1003.aspx?src=pipeline&p=1&loanid={this.loanId.ToString("N")}#tabs-1" : @"/newlos/BorrowerInfo.aspx?";

            var individualName = party?.Individual?.Name;

            var firstNameFieldName = borrowerMode == E_BorrowerModeT.Borrower ? nameof(CAppData.aBFirstNm) : nameof(CAppData.aCFirstNm);
            this.RecordErrorIfEmpty(
                sectionName,
                "First Name",
                $"{fieldPath}highlightId={firstNameFieldName}",
                individualName?.FirstName?.Value,
                VOXErrors.Party.MissingFirstName);

            var lastNameFieldName = borrowerMode == E_BorrowerModeT.Borrower ? nameof(CAppData.aBLastNm) : nameof(CAppData.aCLastNm);
            this.RecordErrorIfEmpty(
                sectionName,
                "Last Name",
                $"{fieldPath}highlightId={lastNameFieldName}",
                individualName?.LastName?.Value,
                VOXErrors.Party.MissingLastName);

            var contactPointEmail = party?.Individual?.ContactPoints?.ContactPointList?.FirstOrDefault(c => c.ContactPointEmail != null);

            var emailFieldName = borrowerMode == E_BorrowerModeT.Borrower ? nameof(CAppData.aBEmail) : nameof(CAppData.aCEmail);
            this.RecordErrorIfEmpty(
                sectionName,
                "Email",
                $"{fieldPath}highlightId={emailFieldName}",
                contactPointEmail?.ContactPointEmail?.ContactPointEmailValue?.Value,
                VOXErrors.Party.MissingEmail);

            var roleDetail = party?.Roles?.RoleList?.FirstOrDefault()?.RoleDetail;

            this.RecordErrorIfEmpty(
                sectionName,
                "Role",
                fieldPath,
                roleDetail?.PartyRoleType?.Value,
                VOXErrors.GenericError);

            var taxpayerIdentifier = party?.TaxpayerIdentifiers?.TaxpayerIdentifierList?.FirstOrDefault();

            var ssnFieldName = borrowerMode == E_BorrowerModeT.Borrower ? nameof(CAppData.aBSsn) : nameof(CAppData.aCSsn);
            this.RecordErrorIfEmpty(
                sectionName,
                "Social Security Number",
                $"{fieldPath}highlightId={ssnFieldName}",
                taxpayerIdentifier.TaxpayerIdentifierValue?.Value,
                VOXErrors.Party.MissingSsn);

            if (isVOD)
            {
                var borrowerDetail = party?.Roles?.RoleList?.FirstOrDefault()
                    ?.Borrower?.BorrowerDetail;

                var dobFieldName = borrowerMode == E_BorrowerModeT.Borrower ? nameof(CAppData.aBDob) : nameof(CAppData.aCDob);
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Date of Birth",
                    $"{fieldPath}highlightId={dobFieldName}",
                    borrowerDetail?.BorrowerBirthDate?.Value,
                    VOXErrors.Party.MissingDateOfBirth);
            }
        }

        /// <summary>
        /// Validates a service container.
        /// </summary>
        /// <param name="service">The service container.</param>
        /// <param name="borrowerName">The name of the associated borrower.</param>
        /// <param name="requestType">The request type.</param>
        /// <param name="verificationType">The verification type.</param>
        /// <param name="payloadType">The type of payload being exported.</param>
        /// <param name="includeCCPayment">Indicates whether a credit card payment is included.</param>
        public void ValidateService(SERVICE service, string borrowerName, VOXRequestT requestType, VOAVerificationT verificationType, PayloadFormatT payloadType, bool includeCCPayment)
        {
            string sectionName = requestType == VOXRequestT.Initial ? $"Service Data - {borrowerName}" : "Service Data";
            var fieldPath = string.Empty;

            var requestExtension = service?.VerificationOfIncome?.VerificationOfIncomeRequest?.Extension?.Other;

            if (payloadType == PayloadFormatT.MCLSmartApi)
            {
                this.ValidateVerificationOfIncomeRequestExtension_Mcl(requestExtension, sectionName, fieldPath, requestType, verificationType);
            }
            else if (payloadType == PayloadFormatT.LQBMismo)
            {
                this.ValidateVerificationOfIncomeRequestExtension_Lqb(requestExtension, sectionName, fieldPath, requestType, verificationType);
            }

            var serviceProductDetail = service?.ServiceProduct?.ServiceProductRequest?.ServiceProductDetail;

            this.RecordErrorIfEmpty(
                sectionName,
                "Service Product Description",
                fieldPath,
                serviceProductDetail.ServiceProductDescription?.Value,
                VOXErrors.GenericError);

            if (requestType != VOXRequestT.Initial)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Service Product Identifier",
                    fieldPath,
                    serviceProductDetail.ServiceProductIdentifier?.Value,
                    VOXErrors.GenericError);
            }

            var serviceProductDetailExtension = serviceProductDetail?.Extension?.Other;

            if (payloadType == PayloadFormatT.MCLSmartApi)
            {
                this.ValidateServiceProductDetailExtension_Mcl(serviceProductDetailExtension, sectionName, fieldPath);
            }
            else if (payloadType == PayloadFormatT.LQBMismo)
            {
                this.ValidateServiceProductDetailExtension_Lqb(serviceProductDetailExtension, sectionName, fieldPath);
            }

            var serviceProductFulfillmentDetail = service?.ServiceProductFulfillment?.ServiceProductFulfillmentDetail;

            if (requestType == VOXRequestT.Initial)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Service Start Date",
                    fieldPath,
                    serviceProductFulfillmentDetail?.ServiceRangeStartDate?.Value,
                    VOXErrors.GenericError);
            }
            else
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Vendor Order Identifier",
                    fieldPath,
                    serviceProductFulfillmentDetail?.VendorOrderIdentifier?.Value,
                    VOXErrors.GenericError);
            }

            if (includeCCPayment)
            {
                var servicePayment = service?.ServicePayments?.ServicePaymentList?.FirstOrDefault();
                this.ValidateServicePayment(servicePayment, sectionName);
            }
        }

        /// <summary>
        /// Validates a verification of income request extension container using the MCL Smart API format.
        /// </summary>
        /// <param name="extension">The extension container.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldPath">The field path.</param>
        /// <param name="requestType">The request type.</param>
        /// <param name="verificationType">The verification type.</param>
        private void ValidateVerificationOfIncomeRequestExtension_Mcl(OTHER_BASE extension, string sectionName, string fieldPath, VOXRequestT requestType, VOAVerificationT verificationType)
        {
            var requestExtension = extension as MCL_VERIFICATION_OF_INCOME_REQUEST_EXTENSION;

            this.RecordErrorIfEmpty(
                sectionName,
                "Verification Request Type",
                fieldPath,
                requestExtension?.VerificationOfIncomeRequestActionType?.Value,
                VOXErrors.GenericError);

            if (requestType == VOXRequestT.Initial && verificationType == VOAVerificationT.Assets)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Account History Length",
                    fieldPath,
                    requestExtension?.FinancialAccountHistoryLength?.Value,
                    VOXErrors.GenericError);

                this.RecordErrorIfEmpty(
                    sectionName,
                    "Refresh Period",
                    fieldPath,
                    requestExtension?.RefreshPeriod?.Value,
                    VOXErrors.GenericError);

                this.RecordErrorIfEmpty(
                    sectionName,
                    "Last 4 Digits of SSN",
                    fieldPath,
                    requestExtension?.SocialSecurityNumberLastFourDigits?.Value,
                    VOXErrors.GenericError);
            }
        }

        /// <summary>
        /// Validates a verification of income request extension container using the LQB proprietary format.
        /// </summary>
        /// <param name="extension">The extension container.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldPath">The field path.</param>
        /// <param name="requestType">The request type.</param>
        /// <param name="verificationType">The verification type.</param>
        private void ValidateVerificationOfIncomeRequestExtension_Lqb(OTHER_BASE extension, string sectionName, string fieldPath, VOXRequestT requestType, VOAVerificationT verificationType)
        {
            var requestExtension = extension as LQB_VERIFICATION_OF_INCOME_REQUEST_EXTENSION;

            this.RecordErrorIfEmpty(
                sectionName,
                "Verification Request Type",
                fieldPath,
                requestExtension?.VerificationOfIncomeRequestActionType?.Value,
                VOXErrors.GenericError);

            if (requestType == VOXRequestT.Initial && verificationType == VOAVerificationT.Assets)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Account History Length",
                    fieldPath,
                    requestExtension?.FinancialAccountHistoryLength?.Value,
                    VOXErrors.GenericError);

                this.RecordErrorIfEmpty(
                    sectionName,
                    "Refresh Period",
                    fieldPath,
                    requestExtension?.RefreshPeriod?.Value,
                    VOXErrors.GenericError);

                this.RecordErrorIfEmpty(
                    sectionName,
                    "Last 4 Digits of SSN",
                    fieldPath,
                    requestExtension?.SocialSecurityNumberLastFourDigits?.Value,
                    VOXErrors.GenericError);
            }
        }

        /// <summary>
        /// Validates a service product detail extension using the MCL Smart API format.
        /// </summary>
        /// <param name="extension">The extension container.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldPath">The field path.</param>
        private void ValidateServiceProductDetailExtension_Mcl(OTHER_BASE extension, string sectionName, string fieldPath)
        {
            var serviceProductDetailExtension = extension as MCL_SERVICE_PRODUCT_DETAIL_EXTENSION;

            if (serviceProductDetailExtension.PreferredResponseFormatTypeSpecified)
            {
                this.RecordErrorIfEmpty(
                sectionName,
                "Preferred Response Format",
                fieldPath,
                serviceProductDetailExtension.PreferredResponseFormatType?.Value,
                VOXErrors.GenericError);
            }

            if (serviceProductDetailExtension.ServicePreferredResponseFormatsSpecified)
            {
                foreach (var preferredResponseFormat in (serviceProductDetailExtension.ServicePreferredResponseFormats?.ServicePreferredResponseFormat).CoalesceWithEmpty())
                {
                    this.RecordErrorIfEmpty(
                        sectionName,
                        "Preferred response Format",
                        fieldPath,
                        preferredResponseFormat?.ServicePreferredResponseFormatDetail?.PreferredResponseFormatType?.Value,
                        VOXErrors.GenericError);
                }
            }
        }

        /// <summary>
        /// Validates a service product detail extension using the LQB proprietary format.
        /// </summary>
        /// <param name="extension">The extension container.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldPath">The field path.</param>
        private void ValidateServiceProductDetailExtension_Lqb(OTHER_BASE extension, string sectionName, string fieldPath)
        {
            var serviceProductDetailExtension = extension as LQB_SERVICE_PRODUCT_DETAIL_EXTENSION;

            this.RecordErrorIfEmpty(
                sectionName,
                "Preferred Response Format",
                fieldPath,
                serviceProductDetailExtension.PreferredResponseFormatType?.Value,
                VOXErrors.GenericError);
        }
    }
}
