﻿namespace LendersOffice.Conversions.VOXFramework.Mismo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Integration.VOXFramework;
    using Mismo3Specification.Version4Schema;
    using static Mismo3.Version4.TypeBuilder;

    /// <summary>
    /// Prepares a VOE payload according to MCL's SMart API Format.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed.")]
    public class MismoVOERequestProvider : AbstractMismoVOXRequestProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MismoVOERequestProvider"/> class.
        /// </summary>
        /// <param name="requestData">The request configuration.</param>
        /// <param name="individualServiceOrdersToRequest">The orders to request from the vendor.</param>
        public MismoVOERequestProvider(VOERequestData requestData, IReadOnlyCollection<VOXRequestOrder> individualServiceOrdersToRequest)
            : base(requestData, typeof(MismoVOERequestProvider), individualServiceOrdersToRequest)
        {
            this.AbstractAuditor = new MismoVOERequestAuditor();
        }

        /// <summary>
        /// Gets the configuration data for the request.
        /// </summary>
        /// <value>The configuration data for the request.</value>
        protected VOERequestData RequestData
        {
            get
            {
                return this.AbstractRequestData as VOERequestData;
            }
        }

        /// <summary>
        /// Gets the request auditor.
        /// </summary>
        /// <value>The request auditor.</value>
        protected MismoVOERequestAuditor Auditor
        {
            get
            {
                return this.AbstractAuditor as MismoVOERequestAuditor;
            }
        }

        /// <summary>
        /// Initializes a label tuple for each party that will be added to the request. This
        /// includes both borrowers on each application as well as each employer.
        /// </summary>
        protected override void InitializePartyLabels()
        {
            this.PartyLabels = new Dictionary<Guid, Tuple<string, string>>();

            if (this.RequestData.RequestType == VOXRequestT.Initial)
            {
                foreach (var applicationId in this.RequestData.BorrowersInRequest.Select(b => b.Key).Distinct())
                {
                    this.PartyLabels[applicationId] = new Tuple<string, string>(string.Empty, string.Empty);
                }

                foreach (var employerId in this.RequestData.EmploymentData.Select(data => data.EmploymentRecordId))
                {
                    this.PartyLabels[employerId] = new Tuple<string, string>(string.Empty, string.Empty);
                }
            }
        }

        /// <summary>
        /// Creates a BORROWER container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <param name="partySequenceNumber">The sequence number of the parent PARTY.</param>
        /// <returns>A BORROWER container.</returns>
        protected BORROWER CreateBorrower(CAppData appData, int partySequenceNumber)
        {
            var borrower = new BORROWER();
            borrower.Residences = this.CreateResidences(appData);
            borrower.XlinkLabel = $"Borrower{partySequenceNumber}";

            borrower.Employers = this.CreateEmployers(appData);

            var selfEmploymentData = this.RequestData.SelfEmploymentData.Where(
                data => data.AppId == appData.aAppId
                    && ((data.IsCoborrower && appData.BorrowerModeT == E_BorrowerModeT.Coborrower)
                    || (!data.IsCoborrower && appData.BorrowerModeT == E_BorrowerModeT.Borrower)))
                .FirstOrDefault();

            bool isSelfEmployed = selfEmploymentData != null;
            if (isSelfEmployed)
            {
                borrower.Extension = this.CreateBorrowerExtension(selfEmploymentData);
            }

            return borrower;
        }

        /// <summary>
        /// Creates a BORROWER_EXTENSION container.
        /// </summary>
        /// <param name="selfEmploymentData">A borrower's self-employment data.</param>
        /// <returns>A BORROWER_EXTENSION container.</returns>
        protected BORROWER_EXTENSION CreateBorrowerExtension(VOESelfEmploymentData selfEmploymentData)
        {
            var extension = new BORROWER_EXTENSION();

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                var mclExtension = new MCL_BORROWER_EXTENSION();
                mclExtension.AreTaxesSelfPrepared = ToMismoIndicator(selfEmploymentData.TaxesSelfPrepared);
                extension.Other = mclExtension;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                var lqbExtension = new LQB_BORROWER_EXTENSION();
                lqbExtension.AreTaxesSelfPrepared = ToMismoIndicator(selfEmploymentData.TaxesSelfPrepared);
                extension.Other = lqbExtension;
            }

            return extension;
        }

        /// <summary>
        /// Creates a DOCUMENT container.
        /// </summary>
        /// <param name="authDoc">A borrower authorization document.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>A DOCUMENT container.</returns>
        protected DOCUMENT CreateDocument(VOXBorrowerAuthDoc authDoc, int sequenceNumber)
        {
            var document = new DOCUMENT();
            document.SequenceNumber = sequenceNumber;
            document.XlinkLabel = $"Document{sequenceNumber}";

            string borrowerLabel = authDoc.IsForCoborrower
                ? this.PartyLabels[authDoc.AppId].Item2
                : this.PartyLabels[authDoc.AppId].Item1;

            // Since the BORROWER label always has the same sequence number as
            // its parent PARTY, we can do a simple string replace.
            this.CreateRelationship_DocumentRole(document.XlinkLabel, borrowerLabel.Replace("Party", "Borrower"));

            document.DocumentClassification = this.CreateDocumentClassification(authDoc);
            document.Views = this.CreateViews(authDoc);

            var appData = this.DataLoan.GetAppData(authDoc.AppId);
            appData.BorrowerModeT = authDoc.IsForCoborrower ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;
            this.Auditor.ValidateDocument(document, appData.aNm);

            return document;
        }

        /// <summary>
        /// Creates a DOCUMENTS container.
        /// </summary>
        /// <returns>A DOCUMENTS container.</returns>
        protected override DOCUMENTS CreateDocuments()
        {
            var documents = new DOCUMENTS();

            foreach (var authDoc in this.RequestData.BorrowerAuthDocs)
            {
                documents.DocumentList.Add(this.CreateDocument(authDoc, documents.DocumentList.Count(d => d != null) + 1));
            }

            return documents;
        }

        /// <summary>
        /// Creates an EMPLOYER container.
        /// </summary>
        /// <param name="employmentData">Data on an employment.</param>
        /// <param name="appData">Data on an application.</param>
        /// <param name="employerId">The employer ID.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>An EMPLOYER container.</returns>
        protected EMPLOYER CreateEmployer(IEmploymentRecord employmentData, CAppData appData, Guid employerId, int sequenceNumber)
        {
            var employer = new EMPLOYER();
            employer.SequenceNumber = sequenceNumber;
            employer.XlinkLabel = $"Employer{sequenceNumber}";

            this.PartyLabels[employerId] = new Tuple<string, string>(employer.XlinkLabel, string.Empty);

            employer.LegalEntity = this.CreateLegalEntity(employmentData);
            employer.Address = this.CreateAddress(employmentData.EmplrAddr, employmentData.EmplrCity, employmentData.EmplrState, employmentData.EmplrZip);
            employer.Employment = this.CreateEmployment(employmentData, appData);

            this.Auditor.ValidateEmployer(employer, employmentData.EmplrNm);
            return employer;
        }

        /// <summary>
        /// Creates an EMPLOYERS container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <returns>An EMPLOYERS container.</returns>
        protected EMPLOYERS CreateEmployers(CAppData appData)
        {
            var employers = new EMPLOYERS();

            List<IEmploymentRecord> employments = new List<IEmploymentRecord>();
            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                foreach (IEmploymentRecord employment in appData.aBEmpCollection.GetSubcollection(true, E_EmpGroupT.All))
                {
                    if (this.RequestData.EmploymentData.Any(e => e.AppId == appData.aAppId && e.EmploymentRecordId == employment.RecordId && !e.IsForCoborrower))
                    {
                        employments.Add(employment);
                    }
                }
            }
            else
            {
                foreach (IEmploymentRecord employment in appData.aCEmpCollection.GetSubcollection(true, E_EmpGroupT.All))
                {
                    if (this.RequestData.EmploymentData.Any(e => e.AppId == appData.aAppId && e.EmploymentRecordId == employment.RecordId && e.IsForCoborrower))
                    {
                        employments.Add(employment);
                    }
                }
            }

            foreach (var employmentData in employments)
            {
                employers.EmployerList.Add(this.CreateEmployer(employmentData, appData, employmentData.RecordId, employers.EmployerList.Count(e => e != null) + 1));
                this.RequestData.RecordEmploymentExport(new VOEOrderEmploymentRecord(employmentData.RecordId, employmentData.EmplrNm, employmentData.IsSelfEmplmt));
            }

            return employers;
        }

        /// <summary>
        /// Creates an EMPLOYMENT container.
        /// </summary>
        /// <param name="employmentData">Data on an employment.</param>
        /// <param name="appData">Data on an application.</param>
        /// <returns>An EMPLOYMENT container.</returns>
        protected EMPLOYMENT CreateEmployment(IEmploymentRecord employmentData, CAppData appData)
        {
            var employment = new EMPLOYMENT();
            employment.EmploymentMonthsOnJobCount = ToMismoCount(employmentData.EmplmtLenInMonths_rep, this.DataLoan.m_convertLos);
            employment.EmploymentYearsOnJobCount = ToMismoCount(employmentData.EmplmtLenInYrs_rep, this.DataLoan.m_convertLos);
            employment.EmploymentPositionDescription = ToMismoString(employmentData.JobTitle);

            var employmentStatus = employmentData.IsCurrent ? EmploymentStatusBase.Current : EmploymentStatusBase.Previous;
            employment.EmploymentStatusType = ToMismoEnum(employmentStatus);

            employment.Extension = this.CreateEmploymentExtension(employmentData, appData);

            return employment;
        }

        /// <summary>
        /// Creates an EMPLOYMENT_EXTENSION container.
        /// </summary>
        /// <param name="employmentData">Data on an employment.</param>
        /// <param name="appData">Data on an application.</param>
        /// <returns>An EMPLOYMENT_EXTENSION container.</returns>
        protected EMPLOYMENT_EXTENSION CreateEmploymentExtension(IEmploymentRecord employmentData, CAppData appData)
        {
            var extension = new EMPLOYMENT_EXTENSION();

            var salaryKey = this.RequestData.SalaryKeyData.Where(
                key => key.AppId == appData.aAppId
                    && ((key.IsCoborrower && appData.BorrowerModeT == E_BorrowerModeT.Coborrower)
                        || (!key.IsCoborrower && appData.BorrowerModeT == E_BorrowerModeT.Borrower)))
                .FirstOrDefault();

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                var mclExtension = new MCL_EMPLOYMENT_EXTENSION();
                string employeeIdVoe = employmentData.EmployeeIdVoe;
                mclExtension.EmployeeIdentifier = ToMismoIdentifier(string.IsNullOrWhiteSpace(employeeIdVoe) && this.RequestData.UseSpecificEmployerRecordSearch ? "OVERRIDE" : employeeIdVoe);

                if (salaryKey != null)
                {
                    mclExtension.SalaryKey = ToMismoString(salaryKey.SalaryKey);
                }

                extension.Other = mclExtension;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                var mclExtension = new LQB_EMPLOYMENT_EXTENSION();
                mclExtension.EmployeeIdentifier = ToMismoIdentifier(employmentData.EmployeeIdVoe);

                if (salaryKey != null)
                {
                    mclExtension.SalaryKey = ToMismoString(salaryKey.SalaryKey);
                }

                extension.Other = mclExtension;
            }

            return extension;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY container.
        /// </summary>
        /// <param name="employmentData">Data on an employment.</param>
        /// <returns>A LEGAL_ENTITY container.</returns>
        protected LEGAL_ENTITY CreateLegalEntity(IEmploymentRecord employmentData)
        {
            var legalEntity = new LEGAL_ENTITY();
            legalEntity.Contacts = this.CreateContacts(string.Empty, employmentData.EmplrBusPhone, ContactPointRoleBase.Work, employmentData.EmplrFax);
            legalEntity.LegalEntityDetail = this.CreateLegalEntityDetail(employmentData);

            return legalEntity;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY container.
        /// </summary>
        /// <param name="preparerData">Data on a tax preparer.</param>
        /// <returns>A LEGAL_ENTITY container.</returns>
        protected LEGAL_ENTITY CreateLegalEntity(VOESelfEmploymentData preparerData)
        {
            var legalEntity = new LEGAL_ENTITY();
            legalEntity.Contacts = this.CreateContacts(string.Empty, preparerData.TaxPhoneNumber, ContactPointRoleBase.Work, preparerData.TaxFaxNumber);
            legalEntity.LegalEntityDetail = this.CreateLegalEntityDetail(preparerData);

            return legalEntity;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY_DETAIL container.
        /// </summary>
        /// <param name="employmentData">Data on an employment.</param>
        /// <returns>A LEGAL_ENTITY_DETAIL container.</returns>
        protected LEGAL_ENTITY_DETAIL CreateLegalEntityDetail(IEmploymentRecord employmentData)
        {
            var legalEntityDetail = new LEGAL_ENTITY_DETAIL();
            legalEntityDetail.FullName = ToMismoString(this.FilterStringIfMcl(employmentData.EmplrNm));
            legalEntityDetail.Extension = this.CreateLegalEntityDetailExtension(employmentData);

            return legalEntityDetail;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY_DETAIL container.
        /// </summary>
        /// <param name="preparerData">Data on a tax preparer.</param>
        /// <returns>A LEGAL_ENTITY_DETAIL container.</returns>
        protected LEGAL_ENTITY_DETAIL CreateLegalEntityDetail(VOESelfEmploymentData preparerData)
        {
            var legalEntityDetail = new LEGAL_ENTITY_DETAIL();
            legalEntityDetail.FullName = ToMismoString(preparerData.CompanyName);

            return legalEntityDetail;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY_DETAIL_EXTENSION container.
        /// </summary>
        /// <param name="employmentData">Data on an employment.</param>
        /// <returns>A LEGAL_ENTITY_DETAIL_EXTENSION container.</returns>
        protected LEGAL_ENTITY_DETAIL_EXTENSION CreateLegalEntityDetailExtension(IEmploymentRecord employmentData)
        {
            var extension = new LEGAL_ENTITY_DETAIL_EXTENSION();

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                var mclExtension = new MCL_LEGAL_ENTITY_DETAIL_EXTENSION();
                mclExtension.EmployerCode = ToMismoCode(employmentData.EmployerCodeVoe);
                extension.Other = mclExtension;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                var lqbExtension = new LQB_LEGAL_ENTITY_DETAIL_EXTENSION();
                lqbExtension.EmployerCode = ToMismoCode(employmentData.EmployerCodeVoe);
                extension.Other = lqbExtension;
            }

            return extension;
        }

        /// <summary>
        /// Creates a LOAN container.
        /// </summary>
        /// <returns>A LOAN container.</returns>
        protected override LOAN CreateLoan()
        {
            var loan = new LOAN();
            loan.LoanIdentifiers = this.CreateLoanIdentifiers();
            loan.TermsOfLoan = this.CreateTermsOfLoan();

            return loan;
        }

        /// <summary>
        /// Creates a PARTIES container.
        /// </summary>
        /// <returns>A PARTIES container.</returns>
        protected override PARTIES CreateParties()
        {
            if (this.RequestData.RequestType != VOXRequestT.Initial)
            {
                return null;
            }

            var parties = new PARTIES();

            // Selected borrowers
            foreach (var borrower in this.RequestData.BorrowersInRequest)
            {
                var appId = borrower.Key;
                var isForCoborrower = borrower.Value;
                var appData = this.DataLoan.GetAppData(appId);

                appData.BorrowerModeT = isForCoborrower ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;

                parties.PartyList.Add(this.CreateParty(appData, parties.PartyList.Count(p => p != null) + 1));
                this.RequestData.RecordBorrowerExport(appId, isForCoborrower, appData.aNm, appData.aSsnLastFour);
            }

            // Tax Preparers
            foreach (var preparerData in this.RequestData.SelfEmploymentData.Where(d => !d.TaxesSelfPrepared))
            {
                parties.PartyList.Add(this.CreateParty(preparerData, parties.PartyList.Count(p => p != null) + 1));
            }

            return parties;
        }

        /// <summary>
        /// Creates a PARTY container representing a borrower.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>A PARTY container.</returns>
        protected PARTY CreateParty(CAppData appData, int sequenceNumber)
        {
            var party = new PARTY();
            party.SequenceNumber = sequenceNumber;
            party.XlinkLabel = $"Party{sequenceNumber}";

            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                this.PartyLabels[appData.aAppId] = new Tuple<string, string>(party.XlinkLabel, this.PartyLabels[appData.aAppId].Item2);
            }
            else
            {
                this.PartyLabels[appData.aAppId] = new Tuple<string, string>(this.PartyLabels[appData.aAppId].Item1, party.XlinkLabel);
            }

            party.Individual = this.CreateIndividual(appData);
            party.Roles = this.CreateRoles(appData, sequenceNumber);
            party.TaxpayerIdentifiers = this.CreateTaxpayerIdentifiers(appData.aSsn);

            this.Auditor.ValidateParty_Borrower(party, appData.aNm);
            return party;
        }

        /// <summary>
        /// Creates a PARTY container representing a tax preparer.
        /// </summary>
        /// <param name="preparerData">Data on a tax preparer.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>A PARTY container.</returns>
        protected PARTY CreateParty(VOESelfEmploymentData preparerData, int sequenceNumber)
        {
            var party = new PARTY();
            party.SequenceNumber = sequenceNumber;
            party.XlinkLabel = $"Party{sequenceNumber}";

            string borrowerLabel = preparerData.IsCoborrower
                ? this.PartyLabels[preparerData.AppId].Item2
                : this.PartyLabels[preparerData.AppId].Item1;

            // Since the BORROWER label always has the same sequence number as
            // its parent PARTY, we can do a simple string replace.
            this.CreateRelationship_PartyRole(party.XlinkLabel, borrowerLabel.Replace("Party", "Borrower"));

            party.LegalEntity = this.CreateLegalEntity(preparerData);
            party.Addresses = this.CreateAddresses(preparerData.TaxStreetAddress, preparerData.TaxCity, preparerData.TaxState, preparerData.TaxZipcode);
            party.Roles = this.CreateRoles(preparerData);

            this.Auditor.ValidateParty_TaxPreparer(party, preparerData.CompanyName);
            return party;
        }

        /// <summary>
        /// Creates a RELATIONSHIP between a PARTY and a ROLE.
        /// </summary>
        /// <param name="partyLabel">The party label.</param>
        /// <param name="roleLabel">The role label.</param>
        protected void CreateRelationship_PartyRole(string partyLabel, string roleLabel)
        {
            this.CreateRelationship(ArcroleVerbPhrase.IsTaxPreparerFor, "PARTY", "ROLE", partyLabel, roleLabel);
        }

        /// <summary>
        /// Creates a RELATIONSHIP between an EMPLOYER and a SERVICE.
        /// </summary>
        /// <param name="employerLabel">The employer label.</param>
        /// <param name="serviceLabel">The service label.</param>
        protected void CreateRelationship_EmployerService(string employerLabel, string serviceLabel)
        {
            this.CreateRelationship(ArcroleVerbPhrase.IsVerifiedBy, "EMPLOYER", "SERVICE", employerLabel, serviceLabel);
        }

        /// <summary>
        /// Creates a RESIDENCE container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <returns>A RESIDENCE container.</returns>
        protected RESIDENCE CreateResidence(CAppData appData)
        {
            var residence = new RESIDENCE();
            residence.Address = this.CreateAddress(appData.aAddr, appData.aCity, appData.aState, appData.aZip);
            residence.ResidenceDetail = this.CreateResidenceDetail();

            return residence;
        }

        /// <summary>
        /// Creates a RESIDENCES container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <returns>A RESIDENCES container.</returns>
        protected RESIDENCES CreateResidences(CAppData appData)
        {
            var residences = new RESIDENCES();
            residences.ResidenceList.Add(this.CreateResidence(appData));

            return residences;
        }

        /// <summary>
        /// Creates a RESIDENCE_DETAIL container.
        /// </summary>
        /// <returns>A RESIDENCE_DETAIL container.</returns>
        protected RESIDENCE_DETAIL CreateResidenceDetail()
        {
            var residenceDetail = new RESIDENCE_DETAIL();
            residenceDetail.BorrowerResidencyType = ToMismoEnum(BorrowerResidencyBase.Current);

            return residenceDetail;
        }

        /// <summary>
        /// Creates a ROLE container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <param name="partySequenceNumber">The sequence number for the parent PARTY container.</param>
        /// <returns>A ROLE container.</returns>
        protected ROLE CreateRole(CAppData appData, int partySequenceNumber)
        {
            var role = new ROLE();
            role.SequenceNumber = 1;

            role.Borrower = this.CreateBorrower(appData, partySequenceNumber);
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.Borrower);

            return role;
        }

        /// <summary>
        /// Creates a ROLE container.
        /// </summary>
        /// <param name="preparerData">Data on a tax preparer.</param>
        /// <returns>A ROLE container.</returns>
        protected ROLE CreateRole(VOESelfEmploymentData preparerData)
        {
            var role = new ROLE();
            role.SequenceNumber = 1;

            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.Other, "TaxPreparer");

            return role;
        }

        /// <summary>
        /// Creates a ROLES container.
        /// </summary>
        /// <param name="appData">Data on an application.</param>
        /// <param name="partySequenceNumber">The sequence number for the parent PARTY container.</param>
        /// <returns>A ROLES container.</returns>
        protected ROLES CreateRoles(CAppData appData, int partySequenceNumber)
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole(appData, partySequenceNumber));

            return roles;
        }

        /// <summary>
        /// Creates a ROLES container.
        /// </summary>
        /// <param name="preparerData">Data on a tax preparer.</param>
        /// <returns>A ROLES container.</returns>
        protected ROLES CreateRoles(VOESelfEmploymentData preparerData)
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole(preparerData));

            return roles;
        }

        /// <summary>
        /// Creates a ROLE_DETAIL container.
        /// </summary>
        /// <param name="type">The role type.</param>
        /// <param name="otherDescription">The role description if it's of type Other.</param>
        /// <returns>A ROLE_DETAIL container.</returns>
        protected ROLE_DETAIL CreateRoleDetail(PartyRoleBase type, string otherDescription = null)
        {
            var roleDetail = new ROLE_DETAIL();
            roleDetail.PartyRoleType = ToMismoEnum(type);

            if (type == PartyRoleBase.Other && !string.IsNullOrEmpty(otherDescription))
            {
                roleDetail.PartyRoleTypeOtherDescription = ToMismoString(otherDescription);
            }

            return roleDetail;
        }

        /// <summary>
        /// Creates a SERVICE container for an Initial request.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <param name="employer">Data on an employment.</param>
        /// <param name="borrowerName">The associated borrower name.</param>
        /// <param name="isSelfEmployed">Indicates whether the borrower is self-employed.</param>
        /// <param name="notes">Additional notes for self-employed borrowers.</param>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>A SERVICE container.</returns>
        protected SERVICE CreateService(VOXRequestOrder orderToRequest, VOEEmploymentData employer, string borrowerName, bool isSelfEmployed, string notes, int sequenceNumber)
        {
            var service = new SERVICE();
            service.SequenceNumber = sequenceNumber;
            service.XlinkLabel = $"Service{sequenceNumber}";

            if (employer != null)
            {
                if (employer.IsForCoborrower)
                {
                    var coborrowerLabel = this.PartyLabels[employer.AppId].Item2;
                    this.CreateRelationship_PartyService(coborrowerLabel, service.XlinkLabel);
                }
                else
                {
                    var borrowerLabel = this.PartyLabels[employer.AppId].Item1;
                    this.CreateRelationship_PartyService(borrowerLabel, service.XlinkLabel);
                }

                var employerLabel = this.PartyLabels[employer.EmploymentRecordId].Item1;
                this.CreateRelationship_EmployerService(employerLabel, service.XlinkLabel);
                this.RequestData.RecordServiceTransactionIdEmploymentRecordIdRelationshipExport(orderToRequest.TransactionId, employer.EmploymentRecordId);
            }
            else if (orderToRequest.IsForCoborrower.Value)
            {
                var coborrowerLabel = this.PartyLabels[orderToRequest.ApplicationId].Item2;
                this.CreateRelationship_PartyService(coborrowerLabel, service.XlinkLabel);
            }
            else if (!orderToRequest.IsForCoborrower.Value)
            {
                var borrowerLabel = this.PartyLabels[orderToRequest.ApplicationId].Item1;
                this.CreateRelationship_PartyService(borrowerLabel, service.XlinkLabel);
            }

            service.ServiceProduct = this.CreateServiceProduct(orderToRequest);
            service.ServiceProductFulfillment = this.CreateServiceProductFulfillment();
            service.VerificationOfIncome = this.CreateVerificationOfIncome(isSelfEmployed);

            if (this.RequestData.RequestType == VOXRequestT.Initial && this.RequestData.PayWithCreditCard)
            {
                service.ServicePayments = this.CreateServicePayments();
            }

            if (!string.IsNullOrEmpty(notes))
            {
                service.Extension = this.CreateServiceExtension(notes);
            }

            this.Auditor.ValidateService(service, borrowerName, this.RequestData.RequestType, this.RequestData.PayloadFormatT, this.RequestData.PayWithCreditCard);
            return service;
        }

        /// <summary>
        /// Creates a SERVICE container for a Get or Re-verify request.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <returns>A SERVICE container.</returns>
        protected SERVICE CreateService_GetReverify(VOXRequestOrder orderToRequest)
        {
            var service = new SERVICE();
            service.SequenceNumber = 1; // Get+Reverify only have one service.
            service.XlinkLabel = $"Service{service.SequenceNumber}";

            service.VerificationOfIncome = this.CreateVerificationOfIncome();
            service.ServiceProduct = this.CreateServiceProduct(orderToRequest);
            service.ServiceProductFulfillment = this.CreateServiceProductFulfillment();

            this.Auditor.ValidateService(service, string.Empty, this.RequestData.RequestType, this.RequestData.PayloadFormatT, this.RequestData.PayWithCreditCard);
            return service;
        }

        /// <summary>
        /// Creates a SERVICE_EXTENSION container.
        /// </summary>
        /// <param name="notes">Additional notes for a self-employed borrower.</param>
        /// <returns>A SERVICE_EXTENSION container.</returns>
        protected SERVICE_EXTENSION CreateServiceExtension(string notes)
        {
            var extension = new SERVICE_EXTENSION();

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                var mclExtension = new MCL_SERVICE_EXTENSION();
                mclExtension.AdditionalNotes = ToMismoString(notes);
                extension.Other = mclExtension;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                var lqbExtension = new LQB_SERVICE_EXTENSION();
                lqbExtension.AdditionalNotes = ToMismoString(notes);
                extension.Other = lqbExtension;
            }

            return extension;
        }

        /// <summary>
        /// Creates a SERVICES container.
        /// </summary>
        /// <returns>A SERVICES container.</returns>
        protected override SERVICES CreateServices()
        {
            var services = new SERVICES();

            if (this.RequestData.RequestType == VOXRequestT.Initial)
            {
                foreach (VOXRequestOrder orderToRequest in this.IndividualServiceOrdersToRequest)
                {
                    VOEEmploymentData employer = this.RequestData.EmploymentData.SingleOrDefault(e => orderToRequest.LinkedRecords.Contains(e.EmploymentRecordId));
                    CAppData appData = this.DataLoan.GetAppData(employer?.AppId ?? orderToRequest.ApplicationId);
                    var selfEmploymentData = this.RequestData.SelfEmploymentData.Where(
                        d => d.AppId == appData.aAppId
                            && ((d.IsCoborrower && appData.BorrowerModeT == E_BorrowerModeT.Coborrower)
                            || (!d.IsCoborrower && appData.BorrowerModeT == E_BorrowerModeT.Borrower)))
                        .FirstOrDefault();

                    bool isSelfEmployed = selfEmploymentData != null;
                    string selfEmploymentNotes = selfEmploymentData?.Notes;
                    services.ServiceList.Add(this.CreateService(orderToRequest, employer, appData.aNm, isSelfEmployed, selfEmploymentNotes, services.ServiceList.Count(s => s != null) + 1));
                }
            }
            else
            {
                services.ServiceList.Add(this.CreateService_GetReverify(this.IndividualServiceOrdersToRequest.Single()));
            }

            return services;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT container.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <returns>A SERVICE_PRODUCT container.</returns>
        protected SERVICE_PRODUCT CreateServiceProduct(VOXRequestOrder orderToRequest)
        {
            var serviceProduct = new SERVICE_PRODUCT();
            serviceProduct.ServiceProductRequest = this.CreateServiceProductRequest(orderToRequest);

            return serviceProduct;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_DETAIL container.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <returns>A SERVICE_PRODUCT_DETAIL container.</returns>
        protected SERVICE_PRODUCT_DETAIL CreateServiceProductDetail(VOXRequestOrder orderToRequest)
        {
            var serviceProductDetail = new SERVICE_PRODUCT_DETAIL();
            serviceProductDetail.ServiceProductDescription = ToMismoString("VOE");

            serviceProductDetail.ServiceProductIdentifier = ToMismoIdentifier(orderToRequest.TransactionId.ToString());

            serviceProductDetail.Extension = this.CreateServiceProductDetailExtension();

            return serviceProductDetail;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_DETAIL_EXTENSION container.
        /// </summary>
        /// <returns>A SERVICE_PRODUCT_DETAIL_EXTENSION container.</returns>
        protected SERVICE_PRODUCT_DETAIL_EXTENSION CreateServiceProductDetailExtension()
        {
            var extension = new SERVICE_PRODUCT_DETAIL_EXTENSION();

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                var mclExtension = new MCL_SERVICE_PRODUCT_DETAIL_EXTENSION();
                mclExtension.ServicePreferredResponseFormats = new MCL_SERVICE_PREFERRED_RESPONSE_FORMATS();
                mclExtension.ServicePreferredResponseFormats.ServicePreferredResponseFormat = new List<MCL_SERVICE_PREFERRED_RESPONSE_FORMAT>();

                var pdfFormat = new MCL_SERVICE_PREFERRED_RESPONSE_FORMAT();
                pdfFormat.ServicePreferredResponseFormatDetail = new MCL_SERVICE_PREFERRED_RESPONSE_FORMAT_DETAIL() { PreferredResponseFormatType = ToMismoString("Pdf") };

                var xmlFormat = new MCL_SERVICE_PREFERRED_RESPONSE_FORMAT();
                xmlFormat.ServicePreferredResponseFormatDetail = new MCL_SERVICE_PREFERRED_RESPONSE_FORMAT_DETAIL() { PreferredResponseFormatType = ToMismoString("Xml") };

                mclExtension.ServicePreferredResponseFormats.ServicePreferredResponseFormat.Add(pdfFormat);
                mclExtension.ServicePreferredResponseFormats.ServicePreferredResponseFormat.Add(xmlFormat);
                extension.Other = mclExtension;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                var lqbExtension = new LQB_SERVICE_PRODUCT_DETAIL_EXTENSION();
                lqbExtension.PreferredResponseFormatType = ToMismoString("Pdf");
                extension.Other = lqbExtension;
            }

            return extension;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_FULFILLMENT container.
        /// </summary>
        /// <returns>A SERVICE_PRODUCT_FULFILLMENT container.</returns>
        protected SERVICE_PRODUCT_FULFILLMENT CreateServiceProductFulfillment()
        {
            var serviceProductFulfillment = new SERVICE_PRODUCT_FULFILLMENT();
            serviceProductFulfillment.ContactPoints = this.CreateContactPoints(this.RequestData.NotificationEmail, string.Empty, ContactPointRoleBase.Blank, string.Empty);
            serviceProductFulfillment.ServiceProductFulfillmentDetail = this.CreateServiceProductFulfillmentDetail();

            return serviceProductFulfillment;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_FULFILLMENT_DETAIL container.
        /// </summary>
        /// <returns>A SERVICE_PRODUCT_FULFILLMENT_DETAIL container.</returns>
        protected SERVICE_PRODUCT_FULFILLMENT_DETAIL CreateServiceProductFulfillmentDetail()
        {
            var serviceProductFulfillmentDetail = new SERVICE_PRODUCT_FULFILLMENT_DETAIL();

            if (this.RequestData.RequestType != VOXRequestT.Get)
            {
                serviceProductFulfillmentDetail.ServiceRangeStartDate = ToMismoDate(this.DataLoan.m_convertLos.ToDateTimeString(this.RequestData.RequestDate));
            }

            if (this.RequestData.RequestType != VOXRequestT.Initial)
            {
                if (this.RequestData.IsSystemGeneratedRequest && this.RequestData.RequestType == VOXRequestT.Get)
                {
                    serviceProductFulfillmentDetail.VendorOrderIdentifier = ToMismoIdentifier(this.RequestData.GeneratedOrderNumber);
                }
                else
                {
                    serviceProductFulfillmentDetail.VendorOrderIdentifier = ToMismoIdentifier(this.RequestData.PreviousOrder?.OrderNumber);
                }
            }

            return serviceProductFulfillmentDetail;
        }

        /// <summary>
        /// Creates a SERVICE_PRODUCT_REQUEST container.
        /// </summary>
        /// <param name="orderToRequest">The raw order metadata that will be placed.</param>
        /// <returns>A SERVICE_PRODUCT_REQUEST container.</returns>
        protected SERVICE_PRODUCT_REQUEST CreateServiceProductRequest(VOXRequestOrder orderToRequest)
        {
            var serviceProductRequest = new SERVICE_PRODUCT_REQUEST();
            serviceProductRequest.ServiceProductDetail = this.CreateServiceProductDetail(orderToRequest);

            return serviceProductRequest;
        }

        /// <summary>
        /// Creates a TERMS_OF_LOAN container.
        /// </summary>
        /// <returns>A TERMS_OF_LOAN container.</returns>
        protected TERMS_OF_LOAN CreateTermsOfLoan()
        {
            var termsOfLoan = new TERMS_OF_LOAN();
            termsOfLoan.LoanPurposeType = ToMismoEnum(LoanPurposeBase.Other);
            termsOfLoan.LoanPurposeTypeOtherDescription = ToMismoString("EmployeesApplicationForCredit");

            return termsOfLoan;
        }

        /// <summary>
        /// Creates a VERIFICATION_OF_INCOME container.
        /// </summary>
        /// <param name="isSelfEmployed">Indicates whether the borrower is self-employed.</param>
        /// <returns>A VERIFICATION_OF_INCOME container.</returns>
        protected VERIFICATION_OF_INCOME CreateVerificationOfIncome(bool isSelfEmployed = false)
        {
            var verificationOfIncome = new VERIFICATION_OF_INCOME();
            verificationOfIncome.VerificationOfIncomeRequest = this.CreateVerificationOfIncomeRequest(isSelfEmployed);

            return verificationOfIncome;
        }

        /// <summary>
        /// Creates a VERIFICATION_OF_INCOME_REQUEST container.
        /// </summary>
        /// <param name="isSelfEmployed">Indicates whether the borrower is self-employed.</param>
        /// <returns>A VERIFICATION_OF_INCOME_REQUEST container.</returns>
        protected VERIFICATION_OF_INCOME_REQUEST CreateVerificationOfIncomeRequest(bool isSelfEmployed)
        {
            var verificationOfIncomeRequest = new VERIFICATION_OF_INCOME_REQUEST();
            verificationOfIncomeRequest.Extension = this.CreateVerificationOfIncomeRequestExtension(isSelfEmployed);

            return verificationOfIncomeRequest;
        }

        /// <summary>
        /// Creates a VERIFICATION_OF_INCOME_REQUEST_EXTENSION container.
        /// </summary>
        /// <param name="isSelfEmployed">Indicates whether the borrower is self-employed.</param>
        /// <returns>A VERIFICATION_OF_INCOME_REQUEST_EXTENSION.</returns>
        protected VERIFICATION_OF_INCOME_REQUEST_EXTENSION CreateVerificationOfIncomeRequestExtension(bool isSelfEmployed)
        {
            var extension = new VERIFICATION_OF_INCOME_REQUEST_EXTENSION();

            string action = this.RequestData.RequestType != VOXRequestT.Get ? "Order" : "Get";

            string verificationType = string.Empty;
            VOEOrder previousOrder = null;
            if (this.RequestData.RequestType == VOXRequestT.Initial)
            {
                if (isSelfEmployed)
                {
                    verificationType = "SelfEmployed";
                }
                else if (this.RequestData.ShouldVerifyIncome)
                {
                    verificationType = "EmploymentPlusIncome";
                }
                else
                {
                    verificationType = "Employment";
                }
            }
            else if (this.RequestData.RequestType == VOXRequestT.Refresh)
            {
                verificationType = "ReVerify";
                previousOrder = this.RequestData.PreviousOrder as VOEOrder;
            }

            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                var mclExtension = new MCL_VERIFICATION_OF_INCOME_REQUEST_EXTENSION();
                mclExtension.VerificationOfIncomeRequestActionType = ToMismoString(action);
                mclExtension.EmploymentVerificationType = ToMismoString(verificationType);
                if (!string.IsNullOrEmpty(previousOrder?.Last4Ssn))
                {
                    mclExtension.SocialSecurityNumberLastFourDigits = ToMismoString(previousOrder.Last4Ssn);
                }

                if (this.RequestData.RequestType != VOXRequestT.Initial)
                {
                    if (this.RequestData.IsSystemGeneratedRequest && this.RequestData.RequestType == VOXRequestT.Get)
                    {
                        mclExtension.PreviousFileNumber = ToMismoString(this.RequestData.GeneratedOrderNumber);
                    }
                    else
                    {
                        mclExtension.PreviousFileNumber = ToMismoString(this.RequestData.PreviousOrder?.OrderNumber);
                    }
                }

                extension.Other = mclExtension;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                var lqbExtension = new LQB_VERIFICATION_OF_INCOME_REQUEST_EXTENSION();
                lqbExtension.VerificationOfIncomeRequestActionType = ToMismoString(action);
                lqbExtension.EmploymentVerificationType = ToMismoString(verificationType);
                if (!string.IsNullOrEmpty(previousOrder?.Last4Ssn))
                {
                    lqbExtension.SocialSecurityNumberLastFourDigits = ToMismoString(previousOrder.Last4Ssn);
                }

                if (this.RequestData.RequestType != VOXRequestT.Initial)
                {
                    lqbExtension.PreviousFileNumber = ToMismoString(this.RequestData.PreviousOrder.OrderNumber);
                }

                extension.Other = lqbExtension;
            }

            return extension;
        }
    }
}
