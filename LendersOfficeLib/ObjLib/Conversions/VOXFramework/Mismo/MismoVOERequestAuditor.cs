﻿namespace LendersOffice.Conversions.VOXFramework.Mismo
{
    using System.Linq;
    using Integration.VOXFramework;
    using LendersOffice.Common;
    using Mismo3Specification;
    using Mismo3Specification.Version4Schema;
    using VOXErrors = LendersOffice.Common.ErrorMessages.VOXErrors;

    /// <summary>
    /// Validates the data in a VOE request.
    /// </summary>
    public class MismoVOERequestAuditor : AbstractMismoVOXRequestAuditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MismoVOERequestAuditor"/> class.
        /// </summary>
        public MismoVOERequestAuditor() : base()
        {
        }

        /// <summary>
        /// Validates a loan container.
        /// </summary>
        /// <param name="loan">The loan container.</param>
        public void ValidateLoan(LOAN loan)
        {
            var sectionName = "General Loan Data";
            var fieldPath = @"/newlos/Status/General.aspx?";

            var loanIdentifier = loan?.LoanIdentifiers?.LoanIdentifierList?.FirstOrDefault();

            this.RecordErrorIfEmpty(
                sectionName,
                "Lender Case Number",
                $"{fieldPath}highlightId=sLenderCaseNum",
                loanIdentifier?.LoanIdentifier.Value,
                VOXErrors.Loan.MissingLenderCaseNumber);

            var termsOfLoan = loan?.TermsOfLoan;

            this.RecordErrorIfEmpty(
                sectionName,
                "Loan Purpose Type",
                string.Empty,
                termsOfLoan.LoanPurposeType.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Loan Purpose Other Description",
                string.Empty,
                termsOfLoan.LoanPurposeTypeOtherDescription.Value,
                VOXErrors.GenericError);
        }

        /// <summary>
        /// Validates an employer container.
        /// </summary>
        /// <param name="employer">The employer container.</param>
        /// <param name="employerName">The employer's name.</param>
        public void ValidateEmployer(EMPLOYER employer, string employerName)
        {
            var sectionName = $"Employer - {employerName}";
            var fieldPath = $"/newlos/BorrowerInfo.aspx?pg=1";

            var legalEntityDetail = employer?.LegalEntity?.LegalEntityDetail;

            this.RecordErrorIfEmpty(
                sectionName,
                "Name",
                fieldPath,
                legalEntityDetail?.FullName?.Value,
                VOXErrors.Employer.MissingName);

            var address = employer?.Address;

            this.RecordErrorIfEmpty(
                sectionName,
                "Address",
                fieldPath,
                address?.AddressLineText?.Value,
                VOXErrors.Employer.MissingAddress);

            this.RecordErrorIfEmpty(
                sectionName,
                "City",
                fieldPath,
                address?.CityName?.Value,
                VOXErrors.Employer.MissingCity);

            this.RecordErrorIfEmpty(
                sectionName,
                "State",
                fieldPath,
                address?.StateCode?.Value,
                VOXErrors.Employer.MissingState);

            this.RecordErrorIfEmpty(
                sectionName,
                "ZIP Code",
                fieldPath,
                address?.PostalCode?.Value,
                VOXErrors.Employer.MissingZip);

            var employment = employer?.Employment;

            this.RecordErrorIfEmpty(
                sectionName,
                "Employment Status",
                fieldPath,
                employment?.EmploymentStatusType?.Value,
                VOXErrors.Employer.MissingEmploymentStatus);
        }

        /// <summary>
        /// Validates a party container representing a borrower.
        /// </summary>
        /// <param name="party">The party container.</param>
        /// <param name="partyName">The party's name.</param>
        public void ValidateParty_Borrower(PARTY party, string partyName)
        {
            string sectionName = $"Borrower - {partyName}";
            var fieldPath = @"/newlos/BorrowerInfo.aspx?pg=0";

            var individualName = party?.Individual?.Name;

            this.RecordErrorIfEmpty(
                sectionName,
                "First Name",
                fieldPath,
                individualName?.FirstName?.Value,
                VOXErrors.Party.MissingFirstName);

            this.RecordErrorIfEmpty(
                sectionName,
                "Last Name",
                fieldPath,
                individualName?.LastName?.Value,
                VOXErrors.Party.MissingLastName);

            var roleDetail = party?.Roles?.RoleList?.FirstOrDefault()?.RoleDetail;

            this.RecordErrorIfEmpty(
                sectionName,
                "Role",
                fieldPath,
                roleDetail?.PartyRoleType?.Value,
                VOXErrors.GenericError);

            var taxpayerIdentifier = party?.TaxpayerIdentifiers?.TaxpayerIdentifierList?.FirstOrDefault();

            this.RecordErrorIfEmpty(
                sectionName,
                "Social Security Number",
                fieldPath,
                taxpayerIdentifier.TaxpayerIdentifierValue?.Value,
                VOXErrors.Party.MissingSsn);
        }

        /// <summary>
        /// Validates a tax preparer for a borrower.
        /// </summary>
        /// <param name="party">The tax preparer party container.</param>
        /// <param name="partyName">The name of the preparer.</param>
        /// <remarks>Tax Preparer parties will only be created for self-employed borrowers who
        /// do not do their own taxes.</remarks>
        public void ValidateParty_TaxPreparer(PARTY party, string partyName)
        {
            string sectionName = $"Tax Preparer - {partyName}";
            var fieldPath = string.Empty;

            var contactPointTelephone = party?.LegalEntity?.Contacts?.ContactList?.FirstOrDefault()
                ?.ContactPoints?.ContactPointList?.FirstOrDefault(
                    contact => !string.IsNullOrEmpty(contact?.ContactPointTelephone?.ContactPointTelephoneValue?.Value));

            this.RecordErrorIfEmpty(
                sectionName,
                "Phone Number",
                fieldPath,
                contactPointTelephone?.ContactPointTelephone?.ContactPointTelephoneValue?.Value,
                VOXErrors.GenericError);

            var legalEntityDetail = party?.LegalEntity?.LegalEntityDetail;

            this.RecordErrorIfEmpty(
                sectionName,
                "Company Name",
                fieldPath,
                legalEntityDetail?.FullName?.Value,
                VOXErrors.GenericError);

            var address = party?.Addresses?.AddressList?.FirstOrDefault();

            this.RecordErrorIfEmpty(
                sectionName,
                "Address",
                fieldPath,
                address?.AddressLineText?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "City",
                fieldPath,
                address?.CityName?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "State",
                fieldPath,
                address?.StateCode?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "ZIP Code",
                fieldPath,
                address?.PostalCode?.Value,
                VOXErrors.GenericError);
        }

        /// <summary>
        /// Validates a service container.
        /// </summary>
        /// <param name="service">The service container.</param>
        /// <param name="borrowerName">The name of the associated borrower.</param>
        /// <param name="requestType">The request type.</param>
        /// <param name="payloadType">The type of payload being exported.</param>
        /// <param name="includeCCPayment">Indicates whether a credit card payment is included.</param>
        public void ValidateService(SERVICE service, string borrowerName, VOXRequestT requestType, PayloadFormatT payloadType, bool includeCCPayment)
        {
            string sectionName = requestType == VOXRequestT.Initial ? $"Service Data - {borrowerName}" : "Service Data";
            var fieldPath = string.Empty;

            var requestExtension = service?.VerificationOfIncome?.VerificationOfIncomeRequest?.Extension?.Other;

            if (payloadType == PayloadFormatT.MCLSmartApi)
            {
                this.ValidateVerificationOfIncomeRequestExtension_Mcl(requestExtension, sectionName, fieldPath, requestType);
            }
            else if (payloadType == PayloadFormatT.LQBMismo)
            {
                this.ValidateVerificationOfIncomeRequestExtension_Lqb(requestExtension, sectionName, fieldPath, requestType);
            }

            var serviceProductDetail = service?.ServiceProduct?.ServiceProductRequest?.ServiceProductDetail;

            this.RecordErrorIfEmpty(
                sectionName,
                "Service Product Description",
                fieldPath,
                serviceProductDetail.ServiceProductDescription?.Value,
                VOXErrors.GenericError);

            if (requestType != VOXRequestT.Initial)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Service Product Identifier",
                    fieldPath,
                    serviceProductDetail.ServiceProductIdentifier?.Value,
                    VOXErrors.GenericError);
            }

            var serviceProductDetailExtension = serviceProductDetail?.Extension?.Other;

            if (payloadType == PayloadFormatT.MCLSmartApi)
            {
                this.ValidateServiceProductDetailExtension_Mcl(serviceProductDetailExtension, sectionName, fieldPath);
            }
            else if (payloadType == PayloadFormatT.LQBMismo)
            {
                this.ValidateServiceProductDetailExtension_Lqb(serviceProductDetailExtension, sectionName, fieldPath);
            }

            var serviceProductFulfillmentDetail = service?.ServiceProductFulfillment?.ServiceProductFulfillmentDetail;

            if (requestType == VOXRequestT.Initial)
            {
                var contactPointEmail = service?.ServiceProductFulfillment?.ContactPoints?.ContactPointList?.FirstOrDefault()?.ContactPointEmail;

                this.RecordErrorIfEmpty(
                    sectionName,
                    "Notification Email",
                    fieldPath,
                    contactPointEmail?.ContactPointEmailValue?.Value,
                    VOXErrors.GenericError);
            }
            else
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Vendor Order Identifier",
                    fieldPath,
                    serviceProductFulfillmentDetail?.VendorOrderIdentifier?.Value,
                    VOXErrors.GenericError);
            }

            if (requestType != VOXRequestT.Get)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Service Start Date",
                    fieldPath,
                    serviceProductFulfillmentDetail?.ServiceRangeStartDate?.Value,
                    VOXErrors.GenericError);
            }

            if (includeCCPayment)
            {
                var servicePayment = service?.ServicePayments?.ServicePaymentList?.FirstOrDefault();
                this.ValidateServicePayment(servicePayment, sectionName);
            }
        }

        /// <summary>
        /// Validates a verification of income request extension container using the MCL Smart API format.
        /// </summary>
        /// <param name="extension">The extension container.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldPath">The field path.</param>
        /// <param name="requestType">The request type.</param>
        private void ValidateVerificationOfIncomeRequestExtension_Mcl(OTHER_BASE extension, string sectionName, string fieldPath, VOXRequestT requestType)
        {
            var requestExtension = extension as MCL_VERIFICATION_OF_INCOME_REQUEST_EXTENSION;

            if (requestType != VOXRequestT.Get)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Employment Verification Type",
                    fieldPath,
                    requestExtension?.EmploymentVerificationType?.Value,
                    VOXErrors.GenericError);
            }

            this.RecordErrorIfEmpty(
                sectionName,
                "Verification Request Type",
                fieldPath,
                requestExtension?.VerificationOfIncomeRequestActionType?.Value,
                VOXErrors.GenericError);

            if (requestType == VOXRequestT.Refresh)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Previous File Number",
                    fieldPath,
                    requestExtension?.PreviousFileNumber?.Value,
                    VOXErrors.GenericError);
            }
        }

        /// <summary>
        /// Validates a verification of income request extension container using the LQB proprietary format.
        /// </summary>
        /// <param name="extension">The extension container.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldPath">The field path.</param>
        /// <param name="requestType">The request type.</param>
        private void ValidateVerificationOfIncomeRequestExtension_Lqb(OTHER_BASE extension, string sectionName, string fieldPath, VOXRequestT requestType)
        {
            var requestExtension = extension as LQB_VERIFICATION_OF_INCOME_REQUEST_EXTENSION;

            if (requestType != VOXRequestT.Get)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Employment Verification Type",
                    fieldPath,
                    requestExtension?.EmploymentVerificationType?.Value,
                    VOXErrors.GenericError);
            }

            this.RecordErrorIfEmpty(
                sectionName,
                "Verification Request Type",
                fieldPath,
                requestExtension?.VerificationOfIncomeRequestActionType?.Value,
                VOXErrors.GenericError);

            if (requestType == VOXRequestT.Refresh)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Previous File Number",
                    fieldPath,
                    requestExtension?.PreviousFileNumber?.Value,
                    VOXErrors.GenericError);
            }
        }

        /// <summary>
        /// Validates a service product detail extension using the MCL Smart API format.
        /// </summary>
        /// <param name="extension">The extension container.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldPath">The field path.</param>
        private void ValidateServiceProductDetailExtension_Mcl(OTHER_BASE extension, string sectionName, string fieldPath)
        {
            var serviceProductDetailExtension = extension as MCL_SERVICE_PRODUCT_DETAIL_EXTENSION;

            if (serviceProductDetailExtension.PreferredResponseFormatTypeSpecified)
            {
                this.RecordErrorIfEmpty(
                    sectionName,
                    "Preferred Response Format",
                    fieldPath,
                    serviceProductDetailExtension.PreferredResponseFormatType?.Value,
                    VOXErrors.GenericError);
            }

            if (serviceProductDetailExtension.ServicePreferredResponseFormatsSpecified)
            {
                foreach (var preferredResponseFormat in (serviceProductDetailExtension.ServicePreferredResponseFormats?.ServicePreferredResponseFormat).CoalesceWithEmpty())
                {
                    this.RecordErrorIfEmpty(
                        sectionName,
                        "Preferred Response Format",
                        fieldPath,
                        preferredResponseFormat?.ServicePreferredResponseFormatDetail?.PreferredResponseFormatType?.Value,
                        VOXErrors.GenericError);
                }
            }
        }

        /// <summary>
        /// Validates a service product detail extension using the LQB proprietary format.
        /// </summary>
        /// <param name="extension">The extension container.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="fieldPath">The field path.</param>
        private void ValidateServiceProductDetailExtension_Lqb(OTHER_BASE extension, string sectionName, string fieldPath)
        {
            var serviceProductDetailExtension = extension as LQB_SERVICE_PRODUCT_DETAIL_EXTENSION;

            this.RecordErrorIfEmpty(
                sectionName,
                "Preferred Response Format",
                fieldPath,
                serviceProductDetailExtension.PreferredResponseFormatType?.Value,
                VOXErrors.GenericError);
        }
    }
}
