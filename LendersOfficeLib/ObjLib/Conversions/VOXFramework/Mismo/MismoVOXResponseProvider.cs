﻿namespace LendersOffice.Conversions.VOXFramework.Mismo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using System.Xml.Serialization;
    using Common;
    using DataAccess;
    using Integration.VOXFramework;
    using Mismo3Specification.Version4Schema;

    /// <summary>
    /// Parses a VOX response using the MCL Smart API format.
    /// </summary>
    public class MismoVOXResponseProvider : AbstractVOXResponseProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MismoVOXResponseProvider"/> class.
        /// </summary>
        /// <param name="response">The response string.</param>
        /// <param name="requestData">The request configuration.</param>
        public MismoVOXResponseProvider(string response, AbstractVOXRequestData requestData)
            : base(response, requestData)
        {
            XmlAttributeOverrides overrides = null;
            if (this.RequestData.PayloadFormatT == PayloadFormatT.MCLSmartApi)
            {
                overrides = ExtensionNamespaces.MCLOverrides;
            }
            else if (this.RequestData.PayloadFormatT == PayloadFormatT.LQBMismo)
            {
                overrides = ExtensionNamespaces.LQBOverrides;
            }

            this.LogResponse(response);
            this.Response = (MESSAGE)SerializationHelper.XmlDeserialize(response, typeof(MESSAGE), overrides);
            this.ParseResponse();
        }

        /// <summary>
        /// Gets or sets the response XML root.
        /// </summary>
        /// <value>The response XML root.</value>
        public new MESSAGE Response { get; protected set; }

        /// <summary>
        /// Logs the response.
        /// </summary>
        /// <param name="response">The response string, if needed.</param>
        protected override void LogResponse(string response = null)
        {
            if (string.IsNullOrEmpty(response))
            {
                return;
            }

            XElement root = XElement.Parse(response);
            var descendants = root.Descendants();

            var documents = descendants.Where(el => el.Name.LocalName.Equals("EmbeddedContentXML", StringComparison.OrdinalIgnoreCase));
            foreach (var doc in documents)
            {
                doc.Value = "[Removed]";
            }

            VOXUtilities.LogPayload(root.ToString(SaveOptions.DisableFormatting), true);
        }

        /// <summary>
        /// Parses the response XML.
        /// </summary>
        protected override void ParseResponse()
        {
            DEAL_SETS dealSets = this.Response?.DealSets;
            DEAL deal = dealSets?.DealSetList?.FirstOrDefault()?.Deals?.DealList?.FirstOrDefault();

            var transactionIds = new HashSet<Guid>();
            foreach (SERVICE serviceContainer in (deal?.Services?.ServiceList).CoalesceWithEmpty())
            {
                VOXServiceResponse serviceResponse = this.ParseServiceResponse(serviceContainer);
                transactionIds.Add(serviceResponse.TransactionId);
                serviceResponse.AllTransactionIds = transactionIds;
                this.ServiceResponses.Add(serviceResponse);
            }

            // Note: We currently only support populating verified assets for responses with a single SERVICE element.
            //  In the future, we may want to add support to resolve XLinks to match ASSETs with SERVICEs. 
            //  For now, "Get results" requests should always have a single SERVICE in the response.
            if (RequestData.ServiceType == VOXServiceT.VOA_VOD && this.ServiceResponses.Count == 1)
            {
                var returnedAssets = new List<VerifiedAsset>();
                foreach (ASSET returnedAsset in (deal?.Assets?.AssetList).CoalesceWithEmpty())
                {
                    if (returnedAsset.AssetDetail != null)
                    {
                        returnedAssets.Add(new VerifiedAsset(
                            accountNumber: returnedAsset.AssetDetail.AssetAccountIdentifier.Value,
                            accountBalance: decimal.Parse(returnedAsset.AssetDetail.AssetCashOrMarketValueAmount.Value),
                            financialInstitution: returnedAsset.AssetDetail.AssetDescription.Value,
                            accountTypeMismo: returnedAsset.AssetDetail.AssetType?.EnumValue ?? default(AssetBase)));
                    }
                }

                if (returnedAssets.Any())
                {
                    this.ServiceResponses.Single().ReturnedAssets = returnedAssets;
                }
            }

            if (this.ServiceResponses.Count == 0)
            {
                var errors = dealSets?.DealSetServices?.DealSetServiceList?.FirstOrDefault()
                    ?.Errors?.ErrorList;

                if (errors != null && errors.Any())
                {
                    foreach (var error in errors)
                    {
                        var errorMessage = error?.ErrorMessages?.ErrorMessageList?.FirstOrDefault();
                        if (errorMessage != null)
                        {
                            this.Errors.Add($"{errorMessage.ErrorMessageCategoryCode.Value}: {errorMessage.ErrorMessageText.Value}");
                        }
                    }
                }
                else
                {
                    this.Errors.Add("The response could not be parsed.");
                }
            }
        }

        /// <summary>
        /// Parses and processes a service response from the vendor..
        /// </summary>
        /// <param name="service">The service container to process.</param>
        /// <returns>A processed service response.</returns>
        private VOXServiceResponse ParseServiceResponse(SERVICE service)
        {
            string vendorOrderId = service?.ServiceProductFulfillment?.ServiceProductFulfillmentDetail?.VendorOrderIdentifier?.Value;
            if (string.IsNullOrEmpty(vendorOrderId))
            {
                this.Errors.Add("The vendor did not return an Order ID.");
            }

            Guid transactionId = Guid.Empty;
            SERVICE_PRODUCT_REQUEST serviceProductRequest = service?.ServiceProduct?.ServiceProductRequest;
            if (!Guid.TryParse(serviceProductRequest?.ServiceProductDetail?.ServiceProductIdentifier?.Value, out transactionId))
            {
                this.Errors.Add("The vendor did not echo back a valid order ID.");
            }

            DEAL deal = this.Response?.DealSets?.DealSetList?.FirstOrDefault()?.Deals?.DealList?.FirstOrDefault();
            List<DuValidationReferenceNumberVoxData> validationReferenceNumbers = this.GetValidationReferenceNumbers(serviceProductRequest, service.XlinkLabel, deal);

            HashSet<string> vendorReferenceIds = this.GetVendorReferenceIds(serviceProductRequest);

            var statusList = service?.Statuses?.StatusList;
            var voxStatuses = statusList.Select(mismoStatus =>
                new VOXStatus(
                    this.ParseOrderStatus(mismoStatus.StatusCode.Value),
                    mismoStatus?.StatusDescription?.Value,
                    (mismoStatus?.Extension?.Other as MCL_STATUS_EXTENSION)?.StatusDateTime?.Value?.ToNullable<DateTime>(DateTime.TryParse) ?? DateTime.Now))
                .OrderBy(s => s.StatusDateTime).ToList();

            var statusContainer = service.Statuses?.StatusList?.FirstOrDefault();
            var status = this.ParseOrderStatus(statusContainer?.StatusCode?.Value);
            var statusDescription = statusContainer?.StatusDescription?.Value;
            List<byte[]> documentBytes = null;

            switch (this.RequestData.RequestType)
            {
                case VOXRequestT.Initial:
                case VOXRequestT.Refresh:
                    if (this.RequestData.ServiceType == VOXServiceT.VOA_VOD)
                    {
                        if (status != VOXOrderStatusT.Pending && status != VOXOrderStatusT.Complete)
                        {
                            status = VOXOrderStatusT.Error;
                        }
                    }
                    else if (this.RequestData.ServiceType == VOXServiceT.VOE)
                    {
                        if (status != VOXOrderStatusT.Pending && status != VOXOrderStatusT.NoHit)
                        {
                            status = VOXOrderStatusT.Error;
                        }
                    }

                    break;
                case VOXRequestT.Get:
                    if (status == VOXOrderStatusT.Complete)
                    {
                        documentBytes = this.GetDocumentsForService(service.XlinkLabel);
                    }

                    break;
                default:
                    throw new UnhandledEnumException(this.RequestData.RequestType);
            }

            if (status == VOXOrderStatusT.Error)
            {
                this.Errors.Add(statusDescription);
            }

            bool isInProgress = false;
            var serviceProductFulfillmentDetailExtension = service?.ServiceProductFulfillment?.ServiceProductFulfillmentDetail?.Extension?.Other as MCL_SERVICE_PRODUCT_FULFILLMENT_DETAIL_EXTENSION;
            if (this.RequestData.ServiceType == VOXServiceT.VOA_VOD  && this.RequestData.RequestType == VOXRequestT.Get && serviceProductFulfillmentDetailExtension != null)
            {
                isInProgress = serviceProductFulfillmentDetailExtension.ReportStatus?.EnumValue == Mismo3Specification.Version4Schema.MCL.ReportStatusEnum.InProgress;
            }

            return new VOXServiceResponse(vendorOrderId, transactionId, voxStatuses, documentBytes, validationReferenceNumbers, vendorReferenceIds, isInProgress);
        }

        /// <summary>
        /// Gets the vendor reference numbers.
        /// </summary>
        /// <param name="serviceProductRequest">The SERVICE_PRODUCT_REQUEST container.</param>
        /// <returns>The set of vendor reference ids. May be empty.</returns>
        private HashSet<string> GetVendorReferenceIds(SERVICE_PRODUCT_REQUEST serviceProductRequest)
        {
            HashSet<string> vendorReferenceIds = new HashSet<string>();
            var extension = serviceProductRequest?.Extension?.Other as MCL_SERVICE_PRODUCT_REQUEST_EXTENSION;
            foreach (var serviceProductIdentifier in (extension?.ServiceProductIdentifiers?.ServiceProductIdentifier).CoalesceWithEmpty())
            {
                if (serviceProductIdentifier?.ServiceProductIdentifierDetail?.ProductIdentifierType?.EnumValue == Mismo3Specification.Version4Schema.MCL.ProductIdentifierTypeEnum.VendorReferenceNumber)
                {
                    vendorReferenceIds.Add(serviceProductIdentifier.ServiceProductIdentifierDetail.ProductIdentifier.Value);
                }
            }

            return vendorReferenceIds;
        }

        /// <summary>
        /// Retrieves the necessary data to construct the validation reference numbers from the service order.
        /// </summary>
        /// <param name="serviceProductRequest">The <c>SERVICE_PRODUCT_REQUEST</c> element for this order.</param>
        /// <param name="serviceLabel">The <c>label</c> attribute of the <c>SERVICE</c> element for this order.</param>
        /// <param name="deal">The <c>DEAL</c> element containing all the relevant data for this order.</param>
        /// <returns>The list of validation reference number data contained in the response.</returns>
        private List<DuValidationReferenceNumberVoxData> GetValidationReferenceNumbers(SERVICE_PRODUCT_REQUEST serviceProductRequest, string serviceLabel, DEAL deal)
        {
            IReadOnlyCollection<string> partyLabelsForService = deal?.Relationships?.RelationshipList
                ?.Where(r => r.Arcrole == "urn:fdc:Meridianlink.com:2017:mortgage/PARTY_IsVerifiedBy_SERVICE" && r.To == serviceLabel)
                ?.Select(r => r.From).ToList();
            IEnumerable<PARTY> partiesInService = deal?.Parties?.PartyList?.Where(p => partyLabelsForService.Contains(p.XlinkLabel)) ?? Enumerable.Empty<PARTY>();

            var validationReferenceNumbers = new List<DuValidationReferenceNumberVoxData>();
            IEnumerable<MCL_SERVICE_PRODUCT_IDENTIFIER_DETAIL> details = (serviceProductRequest?.Extension?.Other as MCL_SERVICE_PRODUCT_REQUEST_EXTENSION)
                ?.ServiceProductIdentifiers?.ServiceProductIdentifier
                ?.Select(serviceProductIdentifier => serviceProductIdentifier?.ServiceProductIdentifierDetail) ?? Enumerable.Empty<MCL_SERVICE_PRODUCT_IDENTIFIER_DETAIL>();
            if (!details.Any())
            {
                return validationReferenceNumbers;
            }

            Dictionary<string, string> mclSourceIdentifierMapping = new Dictionary<string, string>
            {
                { "1", "MeridianLink" },
                { "2", "DataVerify" },
                { "3", "VeriTax" },
                { "4", "NCS" },
                { "5", "IncoCheck" },
                { "6", "UCS" },
                { "7", "Formfree" },
                { "9", "Finicity" }
            };

            foreach (var serviceProductIdentifierDetail in details)
            {
                string providerName = mclSourceIdentifierMapping.GetValueOrNull(serviceProductIdentifierDetail.SourceIdentifier.Value);
                var provider = DataAccess.FannieMae.DuServiceProviders.RetrieveByProviderName(providerName);
                if (provider.HasValue)
                {
                    validationReferenceNumbers.Add(new DuValidationReferenceNumberVoxData(
                        provider.Value,
                        serviceProductIdentifierDetail.ProductIdentifier.Value,
                        partiesInService.Select(p => Tuple.Create(p.Individual?.Name?.FirstName?.Value, p.Individual?.Name?.LastName?.Value))));
                }
            }

            return validationReferenceNumbers;
        }

        /// <summary>
        /// Retrieves the documents associated with a completed order.
        /// </summary>
        /// <param name="serviceLabel">The xlink label of the completed service.</param>
        /// <returns>A list of returned documents.</returns>
        private List<byte[]> GetDocumentsForService(string serviceLabel)
        {
            var documentList = this.Response?.DocumentSets?.DocumentSetList?.FirstOrDefault()
                ?.Documents?.DocumentList?.Cast<DOCUMENT>();
            var relationships = this.Response?.DealSets?.DealSetList?.First()
                ?.Deals?.DealList?.First()
                ?.Relationships?.RelationshipList
                ?.Where(r => r.Arcrole.Contains("DOCUMENT_IsOutputFrom_SERVICE") & r.To.Equals(serviceLabel));

            var documentLabels = relationships?.Select(r => r.From);
            var documentBytes = new List<byte[]>();

            if (documentList == null || !documentList.Any() || documentLabels == null || !documentLabels.Any())
            {
                this.Errors.Add("No documents were returned with a completed order.");
                return documentBytes;
            }

            foreach (var document in documentList.Where(d => documentLabels.Contains(d.XlinkLabel)))
            {
                documentBytes.Add(this.ParseDocumentResponse(document));
            }

            return documentBytes;
        }

        /// <summary>
        /// Parses a byte array from a document response.
        /// </summary>
        /// <param name="document">The document container.</param>
        /// <returns>The document received converted to bytes.</returns>
        private byte[] ParseDocumentResponse(DOCUMENT document)
        {
            var xmlContent = document?.Views?.ViewList?.FirstOrDefault()
                ?.ViewFiles?.ViewFileList?.FirstOrDefault()
                ?.ForeignObject?.EmbeddedContentXML?.Value;

            return string.IsNullOrEmpty(xmlContent) ? null : Convert.FromBase64String(xmlContent);
        }

        /// <summary>
        /// Parses the order status.
        /// </summary>
        /// <param name="status">The order status.</param>
        /// <returns>An order status enum.</returns>
        private VOXOrderStatusT ParseOrderStatus(string status)
        {
            try
            {
                switch (status.ToLower())
                {
                    case "active": // active == complete on the backend, but the UI will show active if appropriate
                    case "completed":
                        return VOXOrderStatusT.Complete;
                    case "pending":
                    case "processing":
                    case "new":
                    case "alert":
                        return VOXOrderStatusT.Pending;
                    case "canceled":
                        return VOXOrderStatusT.Canceled;
                    case "error":
                        return VOXOrderStatusT.Error;
                    case "nohit":
                        return VOXOrderStatusT.NoHit;
                    default:
                        throw new ArgumentException($"\"{status}\" is not a valid order status.");
                }
            }
            catch (ArgumentException exc)
            {
                this.Errors.Add(exc.Message);
                return VOXOrderStatusT.Error;
            }
        }
    }
}
