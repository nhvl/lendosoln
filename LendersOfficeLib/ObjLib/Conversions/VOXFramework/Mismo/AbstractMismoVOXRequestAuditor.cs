﻿namespace LendersOffice.Conversions.VOXFramework.Mismo
{
    using System.Linq;
    using LendersOffice.Conversions.Templates;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using Mismo3Specification.Version4Schema;
    using ObjLib.Conversions.VOXFramework;
    using VOXErrors = LendersOffice.Common.ErrorMessages.VOXErrors;

    /// <summary>
    /// An abstract class that validates a VOX payload and provides an object containing any errors.
    /// </summary>
    public abstract class AbstractMismoVOXRequestAuditor : AbstractIntegrationRequestAuditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractMismoVOXRequestAuditor"/> class.
        /// </summary>
        public AbstractMismoVOXRequestAuditor()
            : base()
        {
        }

        /// <summary>
        /// Validates an about version container. This will throw an exception instead of auditing.
        /// </summary>
        /// <param name="aboutVersion">The about version container.</param>
        public void ValidateAboutVersion(ABOUT_VERSION aboutVersion)
        {
            if (string.IsNullOrEmpty(aboutVersion?.DataVersionIdentifier?.Value))
            {
                throw new DeveloperException(ErrorMessage.BadConfiguration);
            }
        }

        /// <summary>
        /// Validates a document container.
        /// </summary>
        /// <param name="document">The document container.</param>
        /// <param name="borrowerName">The borrower name.</param>
        public void ValidateDocument(DOCUMENT document, string borrowerName)
        {
            string sectionName = $"Borrower Authorization - {borrowerName}";
            string fieldPath = string.Empty;

            var foreignObject = document?.Views?.ViewList?.FirstOrDefault()?.ViewFiles?.ViewFileList?.FirstOrDefault()?.ForeignObject;

            this.RecordErrorIfEmpty(
                sectionName,
                "PDF Data",
                fieldPath,
                foreignObject?.EmbeddedContentXML?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Character Encoding Set",
                fieldPath,
                foreignObject?.CharacterEncodingSetType?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "MIME Type",
                fieldPath,
                foreignObject?.MIMETypeIdentifier?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Object Encoding Type",
                fieldPath,
                foreignObject?.ObjectEncodingType?.Value,
                VOXErrors.GenericError);

            var documentClass = document?.DocumentClassification?.DocumentClasses?.DocumentClassList?.FirstOrDefault();

            this.RecordErrorIfEmpty(
                sectionName,
                "Document Type",
                fieldPath,
                documentClass?.DocumentType?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Document Type Other Description",
                fieldPath,
                documentClass?.DocumentTypeOtherDescription?.Value,
                VOXErrors.GenericError);
        }

        /// <summary>
        /// Validates a service payment. Since all VOX types use the same SERVICE_PAYMENT container, this
        /// is generic and is intended to be called from ValidateService() in derived classes.
        /// </summary>
        /// <param name="servicePayment">The service payment container.</param>
        /// <param name="sectionName">The audit section name.</param>
        protected void ValidateServicePayment(SERVICE_PAYMENT servicePayment, string sectionName)
        {
            string fieldPath = string.Empty;

            this.RecordErrorIfEmpty(
                sectionName,
                "Billing Address",
                fieldPath,
                servicePayment?.Address?.AddressLineText?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Billing City",
                fieldPath,
                servicePayment?.Address?.CityName?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Billing State",
                fieldPath,
                servicePayment?.Address?.StateCode?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Billing ZIP Code",
                fieldPath,
                servicePayment?.Address?.PostalCode?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Billing First Name",
                fieldPath,
                servicePayment?.Name?.FirstName?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Billing Middle Name",
                fieldPath,
                servicePayment?.Address?.AddressLineText?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Billing Last Name",
                fieldPath,
                servicePayment?.Name?.LastName?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Credit Card Number",
                fieldPath,
                servicePayment?.ServicePaymentDetail?.ServicePaymentAccountIdentifier?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Credit Card Expiration Date",
                fieldPath,
                servicePayment?.ServicePaymentDetail?.ServicePaymentCreditAccountExpirationDate?.Value,
                VOXErrors.GenericError);

            this.RecordErrorIfEmpty(
                sectionName,
                "Credit Card CVV",
                fieldPath,
                servicePayment?.ServicePaymentDetail?.ServicePaymentSecondaryCreditAccountIdentifier?.Value,
                VOXErrors.GenericError);
        }
    }
}
