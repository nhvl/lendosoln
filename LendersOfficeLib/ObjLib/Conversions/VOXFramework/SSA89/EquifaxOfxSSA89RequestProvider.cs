﻿namespace LendersOffice.Conversions.VOXFramework.SSA89
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Common;
    using DataAccess;
    using EDocs;
    using Integration.VOXFramework;
    using LendersOffice.Integration.Templates;
    using ObjLib.Conversions.VOXFramework.SSA89;
    using OpenFinancialExchange;
    using OpenFinancialExchange.Request;

    /// <summary>
    /// Request provider for SSA-89 using Equifax OFX payload.
    /// </summary>
    public class EquifaxOfxSSA89RequestProvider : IVOXRequestProvider
    {
        /// <summary>
        /// Value for SONRQ/LANGUAGE.
        /// </summary>
        public const string SonrqLanguage = "ENG";

        /// <summary>
        /// Value for SONRQ/APPID.
        /// </summary>
        public const string SonrqAppid = "LENDINGQB";

        /// <summary>
        /// Concat value for SONRQ/USERID.
        /// </summary>
        public const string SonrqUseridConcat = "@50005";

        /// <summary>
        /// Value for EIVIDENTITYCHECKTRNRQ/PLATFORM.
        /// </summary>
        public const string EividentitychecktrnrqPlatform = "9k";

        /// <summary>
        /// Value for ATTACHEDFILE/ATTACHMENTENCODING.
        /// </summary>
        public const string AttachedfileAttachmentEncoding = "BASE64";

        /// <summary>
        /// Boundary used for the multipart request.
        /// </summary>
        public const string MultipartBoundary = "--=C642062B70FB498299F0F50D2A190B3C";

        /// <summary>
        /// The data loan to use.
        /// </summary>
        private CPageData dataLoan;

        /// <summary>
        /// The data app to use. We can grab this early on since we are only targetting a single borrower, and thus a single app.
        /// </summary>
        private CAppData dataApp;

        /// <summary>
        /// The OFX message.
        /// </summary>
        private Lazy<OFX> ofxMessage;

        /// <summary>
        /// The SSA-89 request data.
        /// </summary>
        private SSA89RequestData requestData;

        /// <summary>
        /// Gets the actual orders from the request data that will be sent to the vendor.
        /// </summary>
        /// <value>The orders that will be sent to the vendor.</value>
        private VOXRequestOrder requestOrder;

        /// <summary>
        /// Auditor for SSA-89 Equifax OFX.
        /// </summary>
        private EquifaxOfxSSA89RequestAuditor auditor;

        /// <summary>
        /// Info for the doc that was loaded up for this export.
        /// </summary>
        private DocumentInRequestInfo docInfo = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquifaxOfxSSA89RequestProvider"/> class.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        /// <param name="requestOrder">The request order.</param>
        public EquifaxOfxSSA89RequestProvider(SSA89RequestData requestData, VOXRequestOrder requestOrder)
        {
            this.requestData = requestData;
            this.dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.requestData.LoanId, typeof(EquifaxOfxSSA89RequestProvider));
            this.dataLoan.InitLoad();

            this.dataApp = this.dataLoan.GetAppData(this.requestData.AppIdForChosenBorrower);
            this.dataApp.BorrowerModeT = this.requestData.IsChosenBorrowerACoborrower ? E_BorrowerModeT.Coborrower : E_BorrowerModeT.Borrower;

            this.ofxMessage = new Lazy<OFX>(this.CreateOFX);
            this.auditor = new EquifaxOfxSSA89RequestAuditor();
            this.requestOrder = requestOrder;
        }

        /// <summary>
        /// Gets the VOX server to send the request. Boundary obtained from Equifax OFX documentation.
        /// </summary>
        /// <value>The VOX server to send the request.</value>
        public VOXServer Server => new EquifaxOfxVoxServer(MultipartBoundary);

        /// <summary>
        /// Audits the request.
        /// </summary>
        /// <returns>The VOX audit results.</returns>
        public IntegrationAuditResult AuditRequest()
        {
            OFX ofxMessage = this.ofxMessage.Value;
            return this.auditor.Results;
        }

        /// <summary>
        /// Serializes the payload to an XML string.
        /// </summary>
        /// <returns>The payload string.</returns>
        public string SerializeRequest()
        {
            OFX ofx = this.ofxMessage.Value;
            XmlWriterSettings settings = new XmlWriterSettings()
            {
                Encoding = new UTF8Encoding(encoderShouldEmitUTF8Identifier: false),
                OmitXmlDeclaration = false
            };

            string xml;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                using (XmlWriter writer = XmlWriter.Create(stream, settings))
                {
                    this.WriteOfxProcessingInstruction(
                        writer,
                        ofxHeader: "200",
                        version: "201",
                        security: "NONE",
                        oldFileUid: "NONE",
                        newFileUid: "NONE");

                    ofx.WriteXml(writer);
                }

                byte[] bytes = stream.GetBuffer();
                xml = Encoding.UTF8.GetString(bytes, 0, (int)stream.Position);
            }

            MimeMultipartContent content = new MimeMultipartContent();
            content.Boundary = MultipartBoundary;

            MultipartPart xmlPart = new MultipartPart();
            xmlPart.Content = xml;
            xmlPart.Headers.Add("Content-Type", "application/x-ofx");
            content.Parts.Add(xmlPart);

            if (this.docInfo != null)
            {
                MultipartPart docPart = new MultipartPart();
                docPart.Headers.Add("Content-Type", "application/pdf");
                docPart.Headers.Add("Content-Transfer-Encoding", "base64");
                docPart.Headers.Add("Content-Location", this.docInfo.DocumentName);
                docPart.Content = Convert.ToBase64String(File.ReadAllBytes(this.docInfo.TempPath));
                content.Parts.Add(docPart);
            }

            return content.ToString();
        }

        /// <summary>
        /// Logs the request.
        /// </summary>
        /// <param name="request">The request string.</param>
        public void LogRequest(string request = null)
        {
            if (string.IsNullOrEmpty(request))
            {
                return;
            }

            var loggingContent = MimeMultipartContent.Parse(request, MultipartBoundary);
            var xmlPart = loggingContent.Parts.ElementAtOrDefault(0);
            XElement root = XElement.Parse(xmlPart.Content);
            var descendants = root.Descendants();

            var passwords = descendants.Where(el => el.Name.LocalName.Equals("USERPASS", StringComparison.OrdinalIgnoreCase));
            foreach (var password in passwords)
            {
                password.Value = "*****";
            }

            var ssns = descendants.Where(el => el.Name.LocalName.Equals("SSN", StringComparison.OrdinalIgnoreCase));
            foreach (var ssn in ssns)
            {
                ssn.Value = "***-**-****";
            }

            xmlPart.Content = root.ToString(SaveOptions.DisableFormatting);

            var documentPart = loggingContent.Parts.ElementAtOrDefault(1);
            if (documentPart != null)
            {
                documentPart.Content = "[Removed]";
            }

            VOXUtilities.LogPayload(loggingContent.ToString(), false);
        }

        /// <summary>
        /// Write OFX processing instructions.
        /// </summary>
        /// <param name="writer">The XML writer.</param>
        /// <param name="ofxHeader">The OFX header.</param>
        /// <param name="version">Version number.</param>
        /// <param name="security">Security attribute.</param>
        /// <param name="oldFileUid">Old file UID.</param>
        /// <param name="newFileUid">New file UID.</param>
        private void WriteOfxProcessingInstruction(XmlWriter writer, string ofxHeader, string version, string security, string oldFileUid, string newFileUid)
        {
            string instructionText = $"OFXHEADER=\"{ofxHeader}\" VERSION=\"{version}\" SECURITY=\"{security}\" OLDFILEUID=\"{oldFileUid}\" NEWFILEUID=\"{newFileUid}\"";
            writer.WriteProcessingInstruction("OFX", instructionText);
        }

        /// <summary>
        /// Creates the main OFX element.
        /// </summary>
        /// <returns>The main OFX element.</returns>
        private OFX CreateOFX()
        {
            var ofx = new OFX();
            ofx.SIGNONMSGSRQV1 = this.CreateSIGNONMSGSRQV1();
            ofx.EIVVERMSGSRQV1 = this.CreateEIVVERMSGSRQV1();

            return ofx;
        }

        /// <summary>
        /// Creates the SIGNONMSGSRQV1 element.
        /// </summary>
        /// <returns>The SIGNONMSGSRQV1 element.</returns>
        private SIGNONMSGSRQV1 CreateSIGNONMSGSRQV1()
        {
            var signonmsgsrqv1 = new SIGNONMSGSRQV1();
            signonmsgsrqv1.SONRQ = this.CreateSONRQ();

            return signonmsgsrqv1;
        }

        /// <summary>
        /// Creates the SONRQ element.
        /// </summary>
        /// <returns>The SONRQ element.</returns>
        private SONRQ CreateSONRQ()
        {
            var sonrq = new SONRQ();
            sonrq.DTCLIENT = DateTime.Now.ToString("yyyyMMddHHmmss");
            sonrq.USERID = this.requestData.Username + SonrqUseridConcat;
            sonrq.USERPASS = this.requestData.Password;
            sonrq.LANGUAGE = SonrqLanguage;
            sonrq.APPID = SonrqAppid;
            sonrq.APPVER = DateTime.Now.Year.ToString();

            this.auditor.ValidateSONRQ(sonrq);

            return sonrq;
        }

        /// <summary>
        /// Create the EIVVERMSGSRQV1 element based on request type.
        /// </summary>
        /// <returns>The EIVVERMSGSRQV1 element.</returns>
        private EIVVERMSGSRQV1 CreateEIVVERMSGSRQV1()
        {
            var eivvermsgsrqv1 = new EIVVERMSGSRQV1();

            var requestType = this.requestData.RequestType;
            var isRetrieve = this.requestData.IsRetrieveGetRequest;
            if (requestType == VOXRequestT.Initial)
            {
                eivvermsgsrqv1.EIVIDENTITYCHECKTRNRQ = this.CreateEIVIDENTITYCHECKTRNRQ();
            }
            else if (requestType == VOXRequestT.Get)
            {
                if (!isRetrieve)
                {
                    eivvermsgsrqv1.EIVOFFLINEVERIFICATIONSTATUSTRNRQ = this.CreateEIVOFFLINEVERIFICATIONSTATUSTRNRQ();
                }
                else
                {
                    eivvermsgsrqv1.EIVIDENTITYCHECKRETRIEVERESULTTRNRQ = this.CreateEIVIDENTITYCHECKRETRIEVERESULTTRNRQ();
                }
            }
            else
            {
                throw new UnhandledEnumException(requestType, "Refresh not supported.");
            }

            return eivvermsgsrqv1;
        }

        /// <summary>
        /// Creates the EIVIDENTITYCHECKTRNRQ element.
        /// </summary>
        /// <returns>The EIVIDENTITYCHECKTRNRQ element.</returns>
        private EIVIDENTITYCHECKTRNRQ CreateEIVIDENTITYCHECKTRNRQ()
        {
            var eividentitychecktrnrq = new EIVIDENTITYCHECKTRNRQ();
            eividentitychecktrnrq.TRNUID = this.dataLoan.sLenderCaseNum;
            eividentitychecktrnrq.CLTCOOKIE = this.dataLoan.sLId.ToString("N");
            eividentitychecktrnrq.PLATFORM = EividentitychecktrnrqPlatform;
            eividentitychecktrnrq.ENDUSER = this.requestData.Principal.DisplayName;
            eividentitychecktrnrq.TRNPURPOSE = this.CreateTRNPURPOSE();

            if (this.requestData.UsedServiceCredential != null && this.requestData.UsedServiceCredential.IsBranchCredential)
            {
                var branchId = this.requestData.UsedServiceCredential.AssociatedBranchId;
                var brokerId = this.requestData.UsedServiceCredential.AssociatedBrokerId;
                var branch = new Admin.BranchDB(branchId.Value, brokerId);
                branch.Retrieve();
                eividentitychecktrnrq.INTERMEDIARY = branch.DisplayNm;
            }
            else if (this.requestData.UsedServiceCredential != null && this.requestData.UsedServiceCredential.IsLenderCredential)
            {
                var brokerId = this.requestData.UsedServiceCredential.AssociatedBrokerId;
                var broker = Admin.BrokerDB.RetrieveById(brokerId);
                eividentitychecktrnrq.INTERMEDIARY = broker.Name;
            }
            else
            {
                eividentitychecktrnrq.INTERMEDIARY = this.dataLoan.Branch.DisplayNm;
            }

            this.auditor.ValidateEIVIDENTITYCHECKTRNRQ(eividentitychecktrnrq);

            eividentitychecktrnrq.EIVIDENTITYCHECKRQ = this.CreateEIVIDENTITYCHECKRQ(this.requestOrder);
            return eividentitychecktrnrq;
        }

        /// <summary>
        /// Creates the TRNPURPOSE element.
        /// </summary>
        /// <returns>The TRNPURPOSE element.</returns>
        private TRNPURPOSE CreateTRNPURPOSE()
        {
            var trnpurpose = new TRNPURPOSE();
            trnpurpose.CODE = E_PPCODE.PPCREDIT;

            return trnpurpose;
        }

        /// <summary>
        /// Creates the EIVIDENTITYCHECKRQ element.
        /// </summary>
        /// <param name="requestOrder">The request order..</param>
        /// <returns>The EIVIDENTITYCHECKRQ element.</returns>
        private EIVIDENTITYCHECKRQ CreateEIVIDENTITYCHECKRQ(VOXRequestOrder requestOrder)
        {
            var eividentitycheckrq = new EIVIDENTITYCHECKRQ();
            eividentitycheckrq.FIRSTNAME = this.dataApp.aFirstNm;
            eividentitycheckrq.LASTNAME = this.dataApp.aLastNm;
            eividentitycheckrq.SSN = this.dataApp.aSsn.Replace("-", string.Empty);
            eividentitycheckrq.DATEBIRTH = this.dataApp.aDob_rep;
            eividentitycheckrq.IMMUTABLEID = requestOrder.TransactionId.ToString("N");
            eividentitycheckrq.LOANID = this.dataLoan.sLenderCaseNum;
            eividentitycheckrq.ADDR1 = this.dataApp.aAddr;
            eividentitycheckrq.CITY = this.dataApp.aCity;
            eividentitycheckrq.STATE = this.dataApp.aState;
            eividentitycheckrq.POSTALCODE = this.dataApp.aZip;
            string phoneNumber;
            Tools.ToPhoneFormat(this.dataApp.aHPhone, @"^(\d{3})(\d{3})(\d{4})(\d*)$", @"$1-$2-$3$4", out phoneNumber);
            eividentitycheckrq.PHONENUMBER = phoneNumber;
            eividentitycheckrq.CREDITHEADER = string.Empty;

            this.auditor.ValidateEIVIDENTITYCHECKRQ(eividentitycheckrq, this.dataApp.aNm);

            eividentitycheckrq.SIGNEDSSAAUTHFORM = this.CreateSIGNEDSSAAUTHFORM();
            return eividentitycheckrq;
        }

        /// <summary>
        /// Creates the SIGNEDSSAAUTHFORM element.
        /// </summary>
        /// <returns>The SIGNEDSSAAUTHFORM element.</returns>
        private SIGNEDSSAAUTHFORM CreateSIGNEDSSAAUTHFORM()
        {
            var signedssaauthform = new SIGNEDSSAAUTHFORM();
            signedssaauthform.VERIFICATIONATTACHMENT = this.CreateVERIFICATIONATTACHMENT();

            return signedssaauthform;
        }

        /// <summary>
        /// Creates the VERIFICATIONATTACHMENT element.
        /// </summary>
        /// <returns>The VERIFICATIONATTACHMENT element.</returns>
        private VERIFICATIONATTACHMENT CreateVERIFICATIONATTACHMENT()
        {
            var verificationattachment = new VERIFICATIONATTACHMENT();
            verificationattachment.VERIFICATIONATTACHMENTTYPE = this.CreateVERIFICATIONATTACHMENTTYPE();
            verificationattachment.ATTACHEDFILES = this.CreateATTACHEDFILES();

            return verificationattachment;
        }

        /// <summary>
        /// Creates the VERIFICATIONATTACHMENTTYPE element.
        /// </summary>
        /// <returns>The VERIFICATIONATTACHMENTTYPE element.</returns>
        private VERIFICATIONATTACHMENTTYPE CreateVERIFICATIONATTACHMENTTYPE()
        {
            var verificationattachmenttype = new VERIFICATIONATTACHMENTTYPE();

            verificationattachmenttype.ATTACHMENTTYPE = E_VERIFICATIONATTACHMENTTYPE.SSAAUTHFORM;

            return verificationattachmenttype;
        }

        /// <summary>
        /// Creates the ATTACHEDFILES element.
        /// </summary>
        /// <returns>The ATTACHEDFILES element.</returns>
        private ATTACHEDFILES CreateATTACHEDFILES()
        {
            var attachedfiles = new ATTACHEDFILES();
            attachedfiles.ATTACHEDFILEList.Add(this.CreateATTACHEDFILE());

            this.auditor.ValidateATTACHEDFILES(attachedfiles);
            return attachedfiles;
        }

        /// <summary>
        /// Creates the ATTACHEDFILE element.
        /// </summary>
        /// <returns>The ATTACHEDFILE element.</returns>
        private ATTACHEDFILE CreateATTACHEDFILE()
        {
            var attachedfile = new ATTACHEDFILE();
            this.CreateDocumentInfo();

            attachedfile.ATTACHMENTNAME = this.docInfo.DocumentName;
            attachedfile.ATTACHMENTENCODING = AttachedfileAttachmentEncoding;
            return attachedfile;
        }

        /// <summary>
        /// Bundles up doc info for this request for easy access later one.
        /// </summary>
        private void CreateDocumentInfo()
        {
            DocumentInRequestInfo docInfo = new DocumentInRequestInfo();
            var repo = EDocumentRepository.GetUserRepository();
            var authDoc = this.requestData.BorrowerAuthDocs.FirstOrDefault();
            var document = repo.GetDocumentById(authDoc.DocId);
            string tempPath;
            try
            {
                tempPath = document.GetPDFTempFile_Current();
            }
            catch (FileNotFoundException)
            {
                tempPath = document.GetPDFTempFile_Original();
            }

            docInfo.DocumentName = "Document.pdf";
            docInfo.TempPath = tempPath;

            this.docInfo = docInfo;
        }

        /// <summary>
        /// Creates the EIVOFFLINEVERIFICATIONSTATUSTRNRQ element.
        /// </summary>
        /// <returns>The EIVOFFLINEVERIFICATIONSTATUSTRNRQ element.</returns>
        private EIVOFFLINEVERIFICATIONSTATUSTRNRQ CreateEIVOFFLINEVERIFICATIONSTATUSTRNRQ()
        {
            var eivofflineverificationstatustrnrq = new EIVOFFLINEVERIFICATIONSTATUSTRNRQ();

            eivofflineverificationstatustrnrq.TRNUID = this.dataLoan.sLenderCaseNum;
            eivofflineverificationstatustrnrq.CLTCOOKIE = this.dataLoan.sLId.ToString("N");
            eivofflineverificationstatustrnrq.EIVOFFLINEVERIFICATIONSTATUSRQ = this.CreateEIVOFFLINEVERIFICATIONSTATUSRQ();

            this.auditor.ValidateEIVOFFLINEVERIFICATIONSTATUSTRNRQ(eivofflineverificationstatustrnrq);

            return eivofflineverificationstatustrnrq;
        }

        /// <summary>
        /// Creates the EIVOFFLINEVERIFICATIONSTATUSRQ element.
        /// </summary>
        /// <returns>The EIVOFFLINEVERIFICATIONSTATUSRQ element.</returns>
        private EIVOFFLINEVERIFICATIONSTATUSRQ CreateEIVOFFLINEVERIFICATIONSTATUSRQ()
        {
            var eivofflineverificationstatusrq = new EIVOFFLINEVERIFICATIONSTATUSRQ();
            eivofflineverificationstatusrq.SRVRTID = this.requestData?.PreviousOrder?.OrderNumber;
            this.auditor.ValidateEIVOFFLINEVERIFICATIONSTATUSRQ(eivofflineverificationstatusrq);

            return eivofflineverificationstatusrq;
        }

        /// <summary>
        /// Creates the EIVIDENTITYCHECKRETRIEVERESULTTRNRQ element.
        /// </summary>
        /// <returns>The EIVIDENTITYCHECKRETRIEVERESULTTRNRQ element.</returns>
        private EIVIDENTITYCHECKRETRIEVERESULTTRNRQ CreateEIVIDENTITYCHECKRETRIEVERESULTTRNRQ()
        {
            var eividentitycheckretrieveresulttrnrq = new EIVIDENTITYCHECKRETRIEVERESULTTRNRQ();
            eividentitycheckretrieveresulttrnrq.TRNUID = this.dataLoan.sLenderCaseNum;
            eividentitycheckretrieveresulttrnrq.CLTCOOKIE = this.dataLoan.sLId.ToString("N");
            eividentitycheckretrieveresulttrnrq.TRNPURPOSE = this.CreateTRNPURPOSE();

            this.auditor.ValidateEIVIDENTITYCHECKRETRIEVERESULTTRNRQ(eividentitycheckretrieveresulttrnrq);

            eividentitycheckretrieveresulttrnrq.EIVIDENTITYCHECKRETRIEVERESULTRQ = this.CreateEIVIDENTITYCHECKRETRIEVERESULTRQ();
            return eividentitycheckretrieveresulttrnrq;
        }

        /// <summary>
        /// Creates the EIVIDENTITYCHECKRETRIEVERESULTRQ element.
        /// </summary>
        /// <returns>The EIVIDENTITYCHECKRETRIEVERESULTRQ element.</returns>
        private EIVIDENTITYCHECKRETRIEVERESULTRQ CreateEIVIDENTITYCHECKRETRIEVERESULTRQ()
        {
            var eividentitycheckretrieveresultrq = new EIVIDENTITYCHECKRETRIEVERESULTRQ();
            eividentitycheckretrieveresultrq.SRVRTID = this.requestData?.PreviousOrder?.OrderNumber;
            this.auditor.ValidateEIVIDENTITYCHECKRETRIEVERESULTRQ(eividentitycheckretrieveresultrq);

            return eividentitycheckretrieveresultrq;
        }

        /// <summary>
        /// Class that holds info for the document that should go in the request.
        /// </summary>
        private class DocumentInRequestInfo
        {
            /// <summary>
            /// Gets or sets the document name.
            /// </summary>
            /// <value>The document name.</value>
            public string DocumentName
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the temp path to the document.
            /// </summary>
            /// <value>The temp path.</value>
            public string TempPath
            {
                get;
                set;
            }
        }
    }
}
