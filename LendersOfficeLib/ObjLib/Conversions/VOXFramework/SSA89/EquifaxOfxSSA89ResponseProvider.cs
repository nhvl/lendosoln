﻿namespace LendersOffice.ObjLib.Conversions.VOXFramework.SSA89
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using Common;
    using DataAccess;
    using Integration.VOXFramework;
    using LendersOffice.Conversions.VOXFramework;
    using OpenFinancialExchange;
    using OpenFinancialExchange.Response;

    /// <summary>
    /// Parses the response for Equifax Ofx SSA89 requests.
    /// </summary>
    public class EquifaxOfxSSA89ResponseProvider : AbstractVOXResponseProvider
    {
        /// <summary>
        ///  The multipart response.
        /// </summary>
        private MimeMultipartContent multipartResponse;

        /// <summary>
        /// The xml portion of the response.
        /// </summary>
        private OFX ofxXml;

        /// <summary>
        /// The order from the request.
        /// </summary>
        private VOXRequestOrder requestOrder;

        /// <summary>
        /// Initializes a new instance of the <see cref="EquifaxOfxSSA89ResponseProvider"/> class.
        /// </summary>
        /// <param name="responseData">The response string.</param>
        /// <param name="requestData">The request configuration.</param>
        /// <param name="requestOrder">The request order.</param>
        public EquifaxOfxSSA89ResponseProvider(VOXServerResponseData responseData, SSA89RequestData requestData, VOXRequestOrder requestOrder)
            : base(responseData.Content, requestData)
        {
            var boundary = MimeMultipartContent.ExtractBoundary(responseData.Headers["Content-Type"].Single());
            this.multipartResponse = MimeMultipartContent.Parse(responseData.Content, boundary);
            this.LogResponse();
            this.requestOrder = requestOrder;
            this.ParseResponse();
        }

        /// <summary>
        /// Logs the response.
        /// </summary>
        /// <param name="response">The response string.</param>
        protected override void LogResponse(string response = null)
        {
            var xmlPart = this.multipartResponse.Parts.FirstOrDefault(part => part.Headers.ContainsKey("Content-Type") && part.Headers["Content-Type"].Contains("application/x-ofx", StringComparison.OrdinalIgnoreCase));
            string xmlContent = null;
            if (xmlPart != null)
            {
                xmlContent = xmlPart.Content;
                XElement root = XElement.Parse(xmlContent);
                var descendants = root.Descendants();

                var ssns = descendants.Where(el => el.Name.LocalName.Equals("SSN", StringComparison.OrdinalIgnoreCase));
                foreach (var ssn in ssns)
                {
                    ssn.Value = "***-**-****";
                }

                xmlPart.Content = root.ToString(SaveOptions.DisableFormatting);
            }

            var documentPart = this.multipartResponse.Parts.FirstOrDefault(part => part.Headers.ContainsKey("Content-Type") && part.Headers["Content-Type"].Contains("application/pdf", StringComparison.OrdinalIgnoreCase));
            string documentContent = null;
            if (documentPart != null)
            {
                documentContent = documentPart.Content;
                documentPart.Content = "[Removed]";
            }

            VOXUtilities.LogPayload(this.multipartResponse.ToString(), true);

            if (!string.IsNullOrEmpty(documentContent))
            {
                documentPart.Content = documentContent;
            }

            if (!string.IsNullOrEmpty(xmlContent))
            {
                xmlPart.Content = xmlContent;
            }
        }

        /// <summary>
        /// Parses the response and creates the VOXServiceResponses.
        /// </summary>
        protected override void ParseResponse()
        {
            var xmlPart = this.multipartResponse.Parts.FirstOrDefault(part => part.Headers.ContainsKey("Content-Type") && part.Headers["Content-Type"].Contains("application/x-ofx", StringComparison.OrdinalIgnoreCase));
            if (xmlPart == null)
            {
                this.Errors.Add("No response received.");
            }

            this.ofxXml = (OFX)SerializationHelper.XmlDeserialize(xmlPart.Content, typeof(OFX));

            SSA89ServiceResponse serviceResponse;
            switch (this.RequestData.RequestType)
            {
                case VOXRequestT.Initial:
                    serviceResponse = this.ParseInitial();
                    break;
                case VOXRequestT.Get:
                    if (((SSA89RequestData)this.RequestData).IsRetrieveGetRequest)
                    {
                        serviceResponse = this.ParseRetrieve();
                    }
                    else
                    {
                        serviceResponse = this.ParseGetStatus();
                    }

                    break;
                case VOXRequestT.Refresh:
                    throw new UnhandledCaseException(this.RequestData.RequestType.ToString(), "Refresh not implemented.");
                default:
                    throw new UnhandledEnumException(this.RequestData.RequestType);
            }

            this.ServiceResponses.Add(serviceResponse);
        }

        /// <summary>
        /// Parses the response for an initial order.
        /// </summary>
        /// <returns>The response for the initial order.</returns>
        private SSA89ServiceResponse ParseInitial()
        {
            VOXOrderStatusT status;
            string statusDescription;

            Guid transactionId = this.requestOrder.TransactionId;
            string vendorOrderId = this.ParseVendorOrderId(this.ofxXml?.EIVVERMSGSRSV1);

            var responseStatus = this.ParseStatus(this.ofxXml?.SIGNONMSGSRSV1, this.ofxXml?.EIVVERMSGSRSV1?.EIVIDENTITYCHECKTRNRS?.STATUS);
            if (responseStatus == null || !responseStatus.Item1.HasValue ||
                responseStatus.Item1.Value == E_STATUSCODE.Error || string.IsNullOrEmpty(vendorOrderId))
            {
                status = VOXOrderStatusT.Error;
                statusDescription = responseStatus.Item2 ?? "Unable to retrieve status.";
                this.Errors.Add(responseStatus.Item2);
            }
            else
            {
                status = VOXOrderStatusT.Pending;
                statusDescription = responseStatus.Item2;
            }
            
            return new SSA89ServiceResponse(vendorOrderId, transactionId, status, statusDescription, null, null);
        }

        /// <summary>
        /// Parses the response for a STATUS get request.
        /// </summary>
        /// <returns>The service response.</returns>
        private SSA89ServiceResponse ParseGetStatus()
        {
            VOXOrderStatusT status;
            string statusDescription;

            Guid transactionId = this.requestOrder.TransactionId;
            string vendorOrderId = this.RequestData.PreviousOrder.OrderNumber;

            var responseStatus = this.ParseStatus(this.ofxXml?.SIGNONMSGSRSV1, this.ofxXml?.EIVVERMSGSRSV1?.EIVOFFLINEVERIFICATIONSTATUSTRNRS?.STATUS);
            if (responseStatus == null || !responseStatus.Item1.HasValue ||
                responseStatus.Item1.Value == E_STATUSCODE.Error)
            {
                status = VOXOrderStatusT.Error;
                statusDescription = responseStatus.Item2 ?? "Unable to retrieve status.";
                return new SSA89ServiceResponse(vendorOrderId, transactionId, status, statusDescription, null, null);
            }
            else
            {
                statusDescription = responseStatus.Item2;

                var identityCheck = this.ofxXml?.EIVVERMSGSRSV1?.EIVOFFLINEVERIFICATIONSTATUSTRNRS?.EIVOFFLINEVERIFICATIONSTATUSRS?.IDENTITYCHECKINFO?.IDENTITYCHECKSTATE;
                var deathMasterStatus = this.ParseDmStatus(identityCheck?.DMSTATUS);
                var ssaStatus = this.ParseSsaStatus(identityCheck?.SSASTATUS);
                var ofacStatus = this.ParseOfacStatus(identityCheck?.OFACSTATUS);
                var creditHeaderStatus = this.ParseChStatus(identityCheck?.CHSTATUS);

                status = VOXOrderStatusT.Complete;
                if (deathMasterStatus == SSA89SubproductStatus.Pending ||
                    ssaStatus == SSA89SubproductStatus.Pending ||
                    ofacStatus == SSA89SubproductStatus.Pending ||
                    creditHeaderStatus == SSA89SubproductStatus.Pending)
                {
                    status = VOXOrderStatusT.Pending;
                }

                var serviceResponse = new SSA89ServiceResponse(vendorOrderId, transactionId, status, statusDescription, null, null);
                serviceResponse.DmStatus = deathMasterStatus;
                serviceResponse.SsaStatus = ssaStatus;
                serviceResponse.OfacStatus = ofacStatus;
                serviceResponse.ChStatus = creditHeaderStatus;
                return serviceResponse;
            }
        }

        /// <summary>
        /// Parses the RETRIEVE response.
        /// </summary>
        /// <returns>The service response with information from the response.</returns>
        private SSA89ServiceResponse ParseRetrieve()
        {
            VOXOrderStatusT status;
            string statusDescription;

            Guid transactionId = this.requestOrder.TransactionId;
            string vendorOrderId = this.RequestData.PreviousOrder.OrderNumber;

            var responseStatus = this.ParseStatus(this.ofxXml?.SIGNONMSGSRSV1, this.ofxXml?.EIVVERMSGSRSV1?.EIVIDENTITYCHECKRETRIEVERESULTTRNRS?.STATUS);
            if (responseStatus == null || !responseStatus.Item1.HasValue ||
                responseStatus.Item1.Value == E_STATUSCODE.Error)
            {
                status = VOXOrderStatusT.Error;
                statusDescription = responseStatus.Item2 ?? "Unable to retrieve status.";
                return new SSA89ServiceResponse(vendorOrderId, transactionId, status, statusDescription, null, null);
            }
            else
            {
                statusDescription = responseStatus.Item2;

                var orderData = this.ofxXml?.EIVVERMSGSRSV1?.EIVIDENTITYCHECKRETRIEVERESULTTRNRS?.EIVIDENTITYCHECKRETRIEVERESULTRS?.ORDERDATA;
                var identityCheck = orderData?.IDENTITYCHECKSTATE;
                
                var deathMasterStatus = this.ParseDmStatus(identityCheck?.DMSTATUS);
                var ssaStatus = this.ParseSsaStatus(identityCheck?.SSASTATUS);
                var ofacStatus = this.ParseOfacStatus(identityCheck?.OFACSTATUS);
                var creditHeaderStatus = this.ParseChStatus(identityCheck?.CHSTATUS);

                status = VOXOrderStatusT.Complete;
                if (deathMasterStatus == SSA89SubproductStatus.Pending ||
                    ssaStatus == SSA89SubproductStatus.Pending ||
                    ofacStatus == SSA89SubproductStatus.Pending ||
                    creditHeaderStatus == SSA89SubproductStatus.Pending)
                {
                    status = VOXOrderStatusT.Pending;
                }

                DateTime? retDate = status == VOXOrderStatusT.Complete ? (this.ParseSsaRetDate(orderData) ?? DateTime.Now) : (DateTime?)null;
                var ssaFormAuthorized = this.ParseSsaFormAuthorized(orderData);
                var ssaFormRejReason = this.ParseSsaFormRejReason(orderData);
                var document = this.ParseDocument();
                
                var serviceResponse = new SSA89ServiceResponse(vendorOrderId, transactionId, status, statusDescription, document == null ? null : new List<byte[]>() { document }, null);
                serviceResponse.DmStatus = deathMasterStatus;
                serviceResponse.SsaStatus = ssaStatus;
                serviceResponse.OfacStatus = ofacStatus;
                serviceResponse.ChStatus = creditHeaderStatus;
                serviceResponse.DateCompleted = retDate;
                serviceResponse.RequestFormAccepted = ssaFormAuthorized;
                serviceResponse.RequestFormRejectionReason = ssaFormRejReason;
                
                return serviceResponse;
            }
        }

        /// <summary>
        /// Parses the EIVVERMSGSRSV1 for the vendor order id. This is only available in a response from an Initial request.
        /// </summary>
        /// <param name="eivvermsgrsrv1">The EIVVERMSGRSV1 element in the response.</param>
        /// <returns>The order id or null if not available. This will add an Error if the order id is not found.</returns>
        private string ParseVendorOrderId(EIVVERMSGSRSV1 eivvermsgrsrv1)
        {
            return eivvermsgrsrv1?.EIVIDENTITYCHECKTRNRS?.EIVIDENTITYCHECKRS?.SRVRTID ?? string.Empty;
        }

        /// <summary>
        /// Parses the response for the order status and status description.
        /// </summary>
        /// <param name="signonmsgsrsv1">The SIGNONMSGRSV1 element in the response.</param>
        /// <param name="eivvermsgsrsv1Status">The STATUS element in the response. This element is in a different container depending on the response.</param>
        /// <returns>Tuple. First item is the status code, second is the message.</returns>
        private Tuple<E_STATUSCODE?, string> ParseStatus(SIGNONMSGSRSV1 signonmsgsrsv1, STATUS eivvermsgsrsv1Status)
        {
            var status = signonmsgsrsv1?.SONRS?.STATUS?.CODE;
            var message = signonmsgsrsv1?.SONRS?.STATUS?.MESSAGE;
            var severity = signonmsgsrsv1?.SONRS?.STATUS?.SEVERITY ?? string.Empty;
            if (status == null)
            {
                // If the the above isn't present, we'll check the STATUS element in the EIVVERMSGSRSV1 element.
                status = eivvermsgsrsv1Status?.CODE;
                message = eivvermsgsrsv1Status?.MESSAGE;
                severity = eivvermsgsrsv1Status?.SEVERITY ?? string.Empty;
            }

            if (status == null)
            {
                this.Errors.Add("Unable to retrieve status.");
                return null;
            }

            string fullMessage = severity;
            if (!string.IsNullOrEmpty(message) && !string.IsNullOrEmpty(fullMessage))
            {
                fullMessage += $": {message}";
            }
            else
            {
                fullMessage += message ?? string.Empty;
            }
            
            return new Tuple<E_STATUSCODE?, string>(status, fullMessage);
        }

        /// <summary>
        /// Parses the DM status.
        /// </summary>
        /// <param name="deathMasterStatus">The DMSTATUS element.</param>
        /// <returns>The vox order status.</returns>
        private SSA89SubproductStatus ParseDmStatus(DMSTATUS deathMasterStatus)
        {
            if (deathMasterStatus == null || deathMasterStatus?.OFFLINESTATETYPE == null)
            {
                this.Errors.Add("Unable to retrieve Death Master status.");
                return SSA89SubproductStatus.Error;
            }

            switch (deathMasterStatus.OFFLINESTATETYPE)
            {
                case E_DMSTATUS.NOTORDERED:
                    return SSA89SubproductStatus.NotOrdered;
                case E_DMSTATUS.PENDING:
                    return SSA89SubproductStatus.Pending;
                case E_DMSTATUS.PRESENT:
                    return SSA89SubproductStatus.Present;
                case E_DMSTATUS.NOTPRESENT:
                    return SSA89SubproductStatus.NotPresent;
                default:
                    throw new UnhandledEnumException(deathMasterStatus.OFFLINESTATETYPE);
            }
        }

        /// <summary>
        /// Parses the SSA status.
        /// </summary>
        /// <param name="ssaStatus">The SSASTATUS element.</param>
        /// <returns>The VOX order status.</returns>
        private SSA89SubproductStatus ParseSsaStatus(SSASTATUS ssaStatus)
        {
            if (ssaStatus == null || ssaStatus?.OFFLINESTATETYPE == null)
            {
                this.Errors.Add("Unable to retrieve Social Security Administration status.");
                return SSA89SubproductStatus.Error;
            }

            switch (ssaStatus.OFFLINESTATETYPE)
            {
                case E_SSASTATUS.NOTORDERED:
                    return SSA89SubproductStatus.NotOrdered;
                case E_SSASTATUS.PENDING:
                    return SSA89SubproductStatus.Pending;
                case E_SSASTATUS.MATCH:
                    return SSA89SubproductStatus.Match;
                case E_SSASTATUS.NOMATCH:
                    return SSA89SubproductStatus.NoMatch;
                default:
                    throw new UnhandledEnumException(ssaStatus.OFFLINESTATETYPE);
            }
        }

        /// <summary>
        /// Parses the OFAC status.
        /// </summary>
        /// <param name="ofacStatus">The OFAC status element.</param>
        /// <returns>The VOX order status.</returns>
        private SSA89SubproductStatus ParseOfacStatus(OFACSTATUS ofacStatus)
        {
            if (ofacStatus == null || ofacStatus?.OFFLINESTATETYPE == null)
            {
                this.Errors.Add("Unable to retrieve Office of Foreign Assets status.");
                return SSA89SubproductStatus.Error;
            }

            switch (ofacStatus.OFFLINESTATETYPE)
            {
                case E_OFACSTATUS.NOTORDERED:
                    return SSA89SubproductStatus.NotOrdered;
                case E_OFACSTATUS.PENDING:
                    return SSA89SubproductStatus.Pending;
                case E_OFACSTATUS.PRESENT:
                    return SSA89SubproductStatus.Present;
                case E_OFACSTATUS.NOTPRESENT:
                    return SSA89SubproductStatus.NotPresent;
                default:
                    throw new UnhandledEnumException(ofacStatus.OFFLINESTATETYPE);
            }
        }

        /// <summary>
        /// Parses the CH status.
        /// </summary>
        /// <param name="creditHeaderStatus">The CH status element.</param>
        /// <returns>The VOX order status.</returns>
        private SSA89SubproductStatus ParseChStatus(CHSTATUS creditHeaderStatus)
        {
            if (creditHeaderStatus == null || creditHeaderStatus?.OFFLINESTATETYPE == null)
            {
                this.Errors.Add("Unable to retrieve Credit Header status.");
                return SSA89SubproductStatus.Error;
            }

            switch (creditHeaderStatus.OFFLINESTATETYPE)
            {
                case E_CHSTATUS.NOTORDERED:
                    return SSA89SubproductStatus.NotOrdered;
                case E_CHSTATUS.PENDING:
                    return SSA89SubproductStatus.Pending;
                case E_CHSTATUS.RETURNED:
                    return SSA89SubproductStatus.Returned;
                default:
                    throw new UnhandledEnumException(creditHeaderStatus.OFFLINESTATETYPE);
            }
        }

        /// <summary>
        /// Parses the SSARETDATE element for the return date.
        /// </summary>
        /// <param name="orderData">The ORDERDATA element.</param>
        /// <returns>The DateTime if available. Null otherwise.</returns>
        private DateTime? ParseSsaRetDate(ORDERDATA orderData)
        {
            if (orderData == null)
            {
                return null;
            }

            string dateString = orderData?.SSARETDATE;
            DateTime date;

            // They use 24 hour format.
            string ofxFormat = "yyyMMddHHmmss"; 
            if (!DateTime.TryParseExact(dateString, ofxFormat, null, System.Globalization.DateTimeStyles.None, out date))
            {
                return null;
            }

            return date;
        }

        /// <summary>
        /// Parses the SSAFORMAUTHORIZED element.
        /// </summary>
        /// <param name="orderData">The ORDERDATA element.</param>
        /// <returns>True if Yes, false if No, null if not present or Undefined.</returns>
        private bool? ParseSsaFormAuthorized(ORDERDATA orderData)
        {
            if (orderData == null || orderData?.SSAFORMAUTHORIZED == null || orderData.SSAFORMAUTHORIZED == E_YESNO.Undefined)
            {
                return null;
            }

            return orderData?.SSAFORMAUTHORIZED == E_YESNO.YES ? true : false;
        }

        /// <summary>
        /// Parses the SSAFORMREJREASON element.
        /// </summary>
        /// <param name="orderData">The order data.</param>
        /// <returns>The form rejection reason. Null if it doesn't exist.</returns>
        private string ParseSsaFormRejReason(ORDERDATA orderData)
        {
            if (orderData == null || orderData?.SSAFORMREJREASON == null)
            {
                return null;
            }

            return orderData?.SSAFORMREJREASON;
        }

        /// <summary>
        /// Parses the document from the MIME response.
        /// </summary>
        /// <returns>Null if not present, the bytes otherwise.</returns>
        private byte[] ParseDocument()
        {
            var documentPart = this.multipartResponse.Parts.FirstOrDefault(part => part.Headers.ContainsKey("Content-Type") && part.Headers["Content-Type"].Contains("application/pdf", StringComparison.OrdinalIgnoreCase));
            if (documentPart == null)
            {
                this.Errors.Add("No document returned");
                return null;
            }

            return Convert.FromBase64String(documentPart.Content);
        }
    }
}
