﻿namespace LendersOffice.ObjLib.Conversions.VOXFramework.SSA89
{
    using LendersOffice.Conversions.Templates;
    using LendersOffice.Conversions.VOXFramework.SSA89;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Exceptions;
    using OpenFinancialExchange;
    using OpenFinancialExchange.Request;
    using VOXErrors = LendersOffice.Common.ErrorMessages.VOXErrors;

    /// <summary>
    /// Validator for SSA89 using Equifax OFX payload.
    /// </summary>
    public class EquifaxOfxSSA89RequestAuditor : AbstractIntegrationRequestAuditor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EquifaxOfxSSA89RequestAuditor"/> class.
        /// </summary>
        public EquifaxOfxSSA89RequestAuditor() : base()
        {
        }

        /// <summary>
        /// Validates the SONRQ element.
        /// </summary>
        /// <param name="sonrq">The element to validate.</param>
        public void ValidateSONRQ(SONRQ sonrq)
        {
            this.ThrowIfEmpty("DTCLIENT", sonrq?.DTCLIENT);
            this.ThrowIfEmpty("USERID", sonrq?.USERID);
            this.ThrowIfEmpty("USERPASS", sonrq?.USERPASS);
            this.ThrowIfEmpty("APPVER", sonrq?.APPVER);
            this.ThrowIfEmptyOrNotMatching("LANGUAGE", sonrq?.LANGUAGE, EquifaxOfxSSA89RequestProvider.SonrqLanguage);
            this.ThrowIfEmptyOrNotMatching("APPID", sonrq?.APPID, EquifaxOfxSSA89RequestProvider.SonrqAppid);
        }

        /// <summary>
        /// Validates the EIVIDENTITYCHECKTRNRQ element.
        /// </summary>
        /// <param name="eividentitychecktrnrq">The element to validate.</param>
        public void ValidateEIVIDENTITYCHECKTRNRQ(EIVIDENTITYCHECKTRNRQ eividentitychecktrnrq)
        {
            var sectionName = "General Loan Data";
            var fieldPath = @"/newlos/Status/General.aspx?";

            this.RecordErrorIfEmpty(
                sectionName, 
                "Lender Case Number",
                $"{fieldPath}highlightId=sLenderCaseNum", 
                eividentitychecktrnrq?.TRNUID,
                VOXErrors.Loan.MissingLenderCaseNumber);

            var otherDataSection = "Other Data";
            this.RecordErrorIfEmpty(
                otherDataSection,
                "User Display Name",
                string.Empty,
                eividentitychecktrnrq?.ENDUSER,
                VOXErrors.Misc.MissingUserDisplayName);

            this.ThrowIfEmptyOrNotMatching("PLATFORM", eividentitychecktrnrq?.PLATFORM, EquifaxOfxSSA89RequestProvider.EividentitychecktrnrqPlatform);
            this.ThrowIfEmpty("INTERMEDIARY", eividentitychecktrnrq?.INTERMEDIARY);
            if (eividentitychecktrnrq?.TRNPURPOSE?.CODE != OpenFinancialExchange.E_PPCODE.PPCREDIT)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("TRNPURPOSE/CODE is not PPCREDIT"));
            }
        }

        /// <summary>
        /// Validates the ATTACHEDFILES element.
        /// </summary>
        /// <param name="attachedfiles">The element to validate.</param>
        public void ValidateATTACHEDFILES(ATTACHEDFILES attachedfiles)
        {
            foreach (var attachedFile in attachedfiles?.ATTACHEDFILEList)
            {
                this.ThrowIfEmpty("ATTACHMENTNAME", attachedFile?.ATTACHMENTNAME);
                this.ThrowIfEmptyOrNotMatching("ATTACHMENTENCODING", attachedFile?.ATTACHMENTENCODING, EquifaxOfxSSA89RequestProvider.AttachedfileAttachmentEncoding);
            }
        }

        /// <summary>
        /// Validates the EIVIDENTITYCHECKRQ element.
        /// </summary>
        /// <param name="eividentitycheckrq">The element to validate.</param>
        /// <param name="partyName">The party's name.</param>
        public void ValidateEIVIDENTITYCHECKRQ(EIVIDENTITYCHECKRQ eividentitycheckrq, string partyName)
        {
            string sectionName = $"Borrower - {partyName}";
            var fieldPath = @"/newlos/BorrowerInfo.aspx?pg=0";

            this.RecordErrorIfEmpty(
                sectionName,
                "First Name",
                fieldPath,
                eividentitycheckrq?.FIRSTNAME,
                VOXErrors.Party.MissingFirstName);

            this.RecordErrorIfEmpty(
                sectionName,
                "Last Name",
                fieldPath,
                eividentitycheckrq?.LASTNAME,
                VOXErrors.Party.MissingLastName);

            this.RecordErrorIfEmpty(
                sectionName,
                "Social Security Number",
                fieldPath,
                eividentitycheckrq?.SSN,
                VOXErrors.Party.MissingSsn);

            this.RecordErrorIfEmpty(
                sectionName,
                "Date of Birth",
                fieldPath,
                eividentitycheckrq?.DATEBIRTH,
                VOXErrors.Party.MissingDateOfBirth);
        }

        /// <summary>
        /// Validates the EIVOFFLINEVERIFICATIONSTATUSRQ element.
        /// </summary>
        /// <param name="element">The EIVOFFLINEVERIFICATIONSTATUSRQ element.</param>
        public void ValidateEIVOFFLINEVERIFICATIONSTATUSRQ(EIVOFFLINEVERIFICATIONSTATUSRQ element)
        {
            this.ThrowIfEmpty("SRVRTID", element?.SRVRTID);
        }

        /// <summary>
        /// Validates the EIVOFFLINEVERIFICATIONSTATUSTRNRQ element.
        /// </summary>
        /// <param name="element">The element to validate.</param>
        public void ValidateEIVOFFLINEVERIFICATIONSTATUSTRNRQ(EIVOFFLINEVERIFICATIONSTATUSTRNRQ element)
        {
            this.ThrowIfEmpty("TRNUID", element?.TRNUID);
            this.ThrowIfEmpty("CLTCOOKIE", element?.CLTCOOKIE);
        }

        /// <summary>
        /// Validates the EIVIDENTITYCHECKRETRIEVERESULTRQ element.
        /// </summary>
        /// <param name="element">The element to validate.</param>
        public void ValidateEIVIDENTITYCHECKRETRIEVERESULTRQ(EIVIDENTITYCHECKRETRIEVERESULTRQ element)
        {
            this.ThrowIfEmpty("SRVRTID", element?.SRVRTID);
        }

        /// <summary>
        /// Validates the EIVIDENTITYCHECKRETRIEVERESULTTRNRQ element.
        /// </summary>
        /// <param name="element">The element to validate.</param>
        public void ValidateEIVIDENTITYCHECKRETRIEVERESULTTRNRQ(EIVIDENTITYCHECKRETRIEVERESULTTRNRQ element)
        {
            this.ThrowIfEmpty("TRNUID", element?.TRNUID);
            this.ThrowIfEmpty("CLTCOOKIE", element?.CLTCOOKIE);
            if (element.TRNPURPOSE.CODE != E_PPCODE.PPCREDIT)
            {
                throw new DeveloperException(ErrorMessage.SystemError, new SimpleContext("TRNPURPOSE.CODE is not PPCREDIT"));
            }
        }
    }
}
