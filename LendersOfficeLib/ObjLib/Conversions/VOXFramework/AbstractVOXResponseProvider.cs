﻿namespace LendersOffice.Conversions.VOXFramework
{
    using System.Collections.Generic;
    using System.Linq;
    using Integration.VOXFramework;

    /// <summary>
    /// A base class exposing information that should be retrieved from a VOX response.
    /// </summary>
    public abstract class AbstractVOXResponseProvider
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AbstractVOXResponseProvider"/> class.
        /// </summary>
        /// <param name="response">The response string.</param>
        /// <param name="requestData">The request configuration.</param>
        public AbstractVOXResponseProvider(string response, AbstractVOXRequestData requestData)
        {
            this.ResponseString = response;
            this.RequestData = requestData;
        }

        /// <summary>
        /// Gets a value indicating whether the response provider contains a response.
        /// </summary>
        /// <value>A boolean indicating whether the response provider contains a response.</value>
        public bool HasResponse
        {
            get
            {
                return !string.IsNullOrEmpty(this.ResponseString);
            }
        }

        /// <summary>
        /// Gets the response string.
        /// </summary>
        /// <value>The response string.</value>
        public virtual string Response
        {
            get
            {
                return this.ResponseString;
            }
        }

        /// <summary>
        /// Gets a value indicating whether errors were encountered while parsing.
        /// </summary>
        /// <value>Indicates whether values were encountered while parsing.</value>
        public bool HasErrors
        {
            get
            {
                return this.Errors.Any();
            }
        }

        /// <summary>
        /// Gets or sets a list of errors encountered while parsing the response.
        /// </summary>
        /// <value>A list of errors encountered while parsing the response.</value>
        public List<string> Errors { get; set; } = new List<string>();

        /// <summary>
        /// Gets or sets service response data from the vendor.
        /// </summary>
        /// <value>Service response data from the vendor.</value>
        public List<VOXServiceResponse> ServiceResponses { get; set; } = new List<VOXServiceResponse>();

        /// <summary>
        /// Gets or sets the string response from the vendor.
        /// </summary>
        /// <value>The string response from the vendor.</value>
        protected string ResponseString { get; set; }

        /// <summary>
        /// Gets or sets the configuration data for the request.
        /// </summary>
        /// <value>The configuration data for the request.</value>
        protected AbstractVOXRequestData RequestData { get; set; }

        /// <summary>
        /// Parses the response string and exposes the data.
        /// </summary>
        protected abstract void ParseResponse();

        /// <summary>
        /// Logs the response.
        /// </summary>
        /// <param name="response">The string response, if it is needed or easier to deal with.</param>
        protected abstract void LogResponse(string response = null);
    }
}
