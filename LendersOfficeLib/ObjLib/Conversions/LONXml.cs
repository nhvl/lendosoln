namespace LendersOffice.Conversions.LON
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Task;

    public class LoanLONXmlWriter
	{
        private const string LOG_CONTEXT = "LoanLONXmlWriter";

		public LoanLONXmlWriter()
		{
		}

        public void Write(XmlWriter writer, CPageData loanData)
        {
            CAppData appData = loanData.GetAppData(0);

            writer.WriteStartElement("LOANSTATUS");
            writer.WriteElementString("STRKEYID", loanData.sLId.ToString());
            writer.WriteElementString("STRLOANNUMBER", loanData.sLNm);
            writer.WriteElementString("STRLQBBRANCHNAME", loanData.BranchNm);
            writer.WriteElementString("STRLQBBRANCHCODE", loanData.BranchCode);
            writer.WriteElementString("STRLQBBDIVISION", loanData.Division);
            writer.WriteElementString("STRLQBCHANNEL", Tools.GetDescription(loanData.sBranchChannelT));
            writer.WriteElementString("STRLQBCORRESPONDENTPROCESS", Tools.GetDescription(loanData.sCorrespondentProcessT));
            writer.WriteElementString("STRLOANSTATUS", loanData.sStatusT_rep);
            writer.WriteElementString("ISVALID", loanData.IsValid.ToString());
            writer.WriteElementString("NUMLOANAMOUNT", loanData.sFinalLAmt_rep);
            writer.WriteElementString("STRLOANPROGRAM", loanData.sLpTemplateNm);
            writer.WriteElementString("DATOPENED", loanData.sOpenedD_rep);
            writer.WriteElementString("STROPENEDCOMMENTS", loanData.sOpenedN);
            writer.WriteElementString("DATSUBMITTED", loanData.sSubmitD_rep);
            writer.WriteElementString("STRSUBMITTEDCOMMENTS", loanData.sSubmitN);
            writer.WriteElementString("DATAPPROVED", loanData.sApprovD_rep);
            writer.WriteElementString("STRAPPROVEDCOMMENTS", loanData.sApprovN);
            writer.WriteElementString("DATDOCUMENTS", loanData.sDocsD_rep);
            writer.WriteElementString("STRDOCUMENTSCOMMENTS", loanData.sDocsN);
            writer.WriteElementString("DATFUNDED", loanData.sFundD_rep);
            writer.WriteElementString("STRFUNDEDCOMMENTS", loanData.sFundN);
            writer.WriteElementString("DATRECORDED", loanData.sRecordedD_rep);
            writer.WriteElementString("STRRECORDEDCOMMENTS", loanData.sRecordedN);
            writer.WriteElementString("DATDENIED", loanData.sRejectD_rep);
            writer.WriteElementString("STRDENIEDCOMMENTS", loanData.sRejectN);
            writer.WriteElementString("DATSUSPENDED", loanData.sSuspendedD_rep);
            writer.WriteElementString("STRSUSPENDEDCOMMENTS", loanData.sSuspendedN);
            writer.WriteElementString("DATCANCELLED", loanData.sCanceledD_rep);
            writer.WriteElementString("STRCANCELLEDCOMMENTS", loanData.sCanceledN);
            writer.WriteElementString("STRCLOSEDCOMMENTS", loanData.sClosedN);
            writer.WriteElementString("DATCLOSEDATE", loanData.sClosedD_rep);
            writer.WriteElementString("NUMLOCKRATE", loanData.sNoteIR_rep);
            writer.WriteElementString("DATLOCKDATE", loanData.sRLckdD_rep);
            writer.WriteElementString("DATLOCKEXPIREDATE", loanData.sRLckdExpiredD_rep);
            writer.WriteElementString("DATFIRSTPAYMENTDATE", loanData.sSchedDueD1_rep);
            writer.WriteElementString("DATESTIMATEDCLOSEDATE", loanData.sEstCloseD_rep);
            writer.WriteElementString("STRESTIMATEDCLOSECOMMENTS", loanData.sEstCloseN);


            writer.WriteElementString("STRFNAME", appData.aBFirstNm);
            writer.WriteElementString("STRLNAME", appData.aBLastNm);
            writer.WriteElementString("STRCO_FNAME", appData.aCFirstNm);
            writer.WriteElementString("STRCO_LNAME", appData.aCLastNm);


            writer.WriteElementString("DATAPPRAISAL1", loanData.sApprRprtOd_rep);
            writer.WriteElementString("DATAPPRAISAL3", loanData.sApprRprtRd_rep);
            writer.WriteElementString("DATCREDREP1", appData.aCrOd_rep);
            writer.WriteElementString("DATCREDREP3", appData.aCrRd_rep);
            writer.WriteElementString("DATTITLEREP1", loanData.sPrelimRprtOd_rep);
            writer.WriteElementString("DATTITLEREP3", loanData.sPrelimRprtRd_rep);


            if (loanData.sU1DocStatDesc.Length > 0)
            {
                writer.WriteElementString("STRITEM2DESC", loanData.sU1DocStatDesc);
                writer.WriteElementString("DATITEM2DAT1", loanData.sU1DocStatOd_rep);
                writer.WriteElementString("DATITEM2DAT3", loanData.sU1DocStatRd_rep);
            }
            if (loanData.sU2DocStatDesc.Length > 0)
            {
                writer.WriteElementString("STRITEM3DESC", loanData.sU2DocStatDesc);
                writer.WriteElementString("DATITEM3DAT1", loanData.sU2DocStatOd_rep);
                writer.WriteElementString("DATITEM3DAT3", loanData.sU2DocStatRd_rep);
            }
            if (loanData.sU3DocStatDesc.Length > 0)
            {
                writer.WriteElementString("STRITEM4DESC", loanData.sU3DocStatDesc);
                writer.WriteElementString("DATITEM4DAT1", loanData.sU3DocStatOd_rep);
                writer.WriteElementString("DATITEM4DAT3", loanData.sU3DocStatRd_rep);
            }
            if (loanData.sU4DocStatDesc.Length > 0)
            {
                writer.WriteElementString("STRITEM5DESC", loanData.sU4DocStatDesc);
                writer.WriteElementString("DATITEM5DAT1", loanData.sU4DocStatOd_rep);
                writer.WriteElementString("DATITEM5DAT3", loanData.sU4DocStatRd_rep);
            }
            if (loanData.sU5DocStatDesc.Length > 0)
            {
                writer.WriteElementString("STRITEM6DESC", loanData.sU5DocStatDesc);
                writer.WriteElementString("DATITEM6DAT1", loanData.sU5DocStatOd_rep);
                writer.WriteElementString("DATITEM6DAT3", loanData.sU5DocStatRd_rep);
            }

            Guid sBrokerId = loanData.sBrokerId;
            BrokerDB brokerDB = BrokerDB.RetrieveById(sBrokerId);
            if (brokerDB.IsUseNewTaskSystem)
            {
                List<Task> conditions = Task.GetActiveConditionsByLoanId(sBrokerId, loanData.sLId, false);
                for (int i = 1; i <= conditions.Count && i <= 13; i++)

                {
                    Task currentCondition = conditions[i - 1];
                    writer.WriteElementString("STRCOND" + i + "DESC", currentCondition.TaskSubject);
                    if (currentCondition.TaskStatus == E_TaskStatus.Closed && currentCondition.TaskClosedDate.HasValue)
                    {
                        writer.WriteElementString("DATCOND" + i + "DAT1", currentCondition.TaskClosedDate.Value.ToShortDateString());
                    }
                }
            }
            else
            {
                for (int i = 1; i <= loanData.GetCondRecordCount() && i <= 13; i++)
                {
                    CCondFieldsObsolete fields = loanData.GetCondFieldsObsolete(i - 1);
                    writer.WriteElementString("STRCOND" + i + "DESC", fields.CondDesc);
                    if (fields.IsDone)
                        writer.WriteElementString("DATCOND" + i + "DAT1", fields.DoneDate.ToStringWithDefaultFormatting()); // DT - this should probably use the _rep...
                }
            }
            writer.WriteElementString("STREMAIL", appData.aBEmail);
            writer.WriteElementString("STRPROPADDRESS", loanData.sSpAddr);
            writer.WriteElementString("STRPROPCITY", loanData.sSpCity);
            writer.WriteElementString("STRPROPSTATE", loanData.sSpState);
            writer.WriteElementString("STRPROPZIP", loanData.sSpZip);
            writer.WriteElementString("STRADDRESS1", appData.aBAddr);
            writer.WriteElementString("STRCITY", appData.aBCity);
            writer.WriteElementString("STRSTATE", appData.aBState);
            writer.WriteElementString("STRZIP", appData.aBZip);

            bool bLoanOfficer = false;
            bool bProcessor = false;
            bool bSellingAgent = false;

            WriteLoanAgent(writer, loanData.sLId, sBrokerId, ref bLoanOfficer, ref bProcessor, ref bSellingAgent);

            // 08/20/03-Binh-Overriding roles are located here

            for (int i = 0; i < loanData.GetAgentRecordCount(); i++)
            {
                CAgentFields fields = loanData.GetAgentFields(i);
                switch (fields.AgentRoleT)
                {
                    case E_AgentRoleT.SellingAgent:
                        if (!bSellingAgent)
                        {
                            writer.WriteElementString("STRSAGENTNAME", fields.AgentName);
                            writer.WriteElementString("STRSAGENTEMAIL", fields.EmailAddr);
                            writer.WriteElementString("STRSAGENTPHONE", fields.Phone);
                            writer.WriteElementString("STRSAGENTFAX", fields.FaxNum);
                            writer.WriteElementString("STRSAGENTADDRESS", fields.StreetAddr);
                            writer.WriteElementString("STRSAGENTCITY", fields.City);
                            writer.WriteElementString("STRSAGENTSTATE", fields.State);
                            writer.WriteElementString("STRSAGENTZIP", fields.Zip);
                            writer.WriteElementString("STRSAGENTCOMPNAME", fields.CompanyName);
                        }
                        break;
                    case E_AgentRoleT.ListingAgent:
                        writer.WriteElementString("STRLAGENTNAME", fields.AgentName);
                        writer.WriteElementString("STRLAGENTEMAIL", fields.EmailAddr);
                        writer.WriteElementString("STRLAGENTPHONE", fields.Phone);
                        writer.WriteElementString("STRLAGENTFAX", fields.FaxNum);
                        writer.WriteElementString("STRLAGENTADDRESS", fields.StreetAddr);
                        writer.WriteElementString("STRLAGENTCITY", fields.City);
                        writer.WriteElementString("STRLAGENTSTATE", fields.State);
                        writer.WriteElementString("STRLAGENTZIP", fields.Zip);
                        writer.WriteElementString("STRLAGENTCOMPNAME", fields.CompanyName);
                        break;
                    case E_AgentRoleT.LoanOfficer:
                        if (!bLoanOfficer)
                        {
                            writer.WriteElementString("STRLOANOFFICERNAME", fields.AgentName);
                        }
                        break;
                    case E_AgentRoleT.Processor:
                        if (!bProcessor)
                        {
                            writer.WriteElementString("STRLOANPROCESSORNAME", fields.AgentName);
                            writer.WriteElementString("STRLOANPROCESSORPHONE", fields.Phone);
                            writer.WriteElementString("STRLOANPROCESSORFAX", fields.FaxNum);
                            writer.WriteElementString("STRLOANPROCESSOREMAIL", fields.EmailAddr);
                        }
                        break;
                    case E_AgentRoleT.Appraiser:
                        writer.WriteElementString("STRAPPRAISERNAME", fields.AgentName);
                        writer.WriteElementString("STRAPPRAISERPHONE", fields.Phone);
                        writer.WriteElementString("STRAPPRAISERCOMPANY", fields.CompanyName);
                        writer.WriteElementString("STRAPPRAISEREMAIL", fields.EmailAddr);
                        break;
                    case E_AgentRoleT.Builder:
                        writer.WriteElementString("STRBUILDERNAME", fields.AgentName);
                        writer.WriteElementString("STRBUILDERCOMPANY", fields.CompanyName);
                        writer.WriteElementString("STRBUILDERPHONE", fields.Phone);
                        writer.WriteElementString("STRBUILDERADDRESS", fields.StreetAddr);
                        writer.WriteElementString("STRBUILDERCITYSTATEZIP", fields.CityStateZip);
                        writer.WriteElementString("STRBUILDERFAX", fields.FaxNum);
                        writer.WriteElementString("STRBUILDEREMAIL", fields.EmailAddr);
                        break;
                    case E_AgentRoleT.Escrow:
                        writer.WriteElementString("STRESCROWNUMBER", fields.CaseNum);
                        writer.WriteElementString("STRESCROWNAME", fields.AgentName);
                        writer.WriteElementString("STRESCROWCOMPANY", fields.CompanyName);
                        writer.WriteElementString("STRESCROWPHONE", fields.Phone);
                        writer.WriteElementString("STRESCROWADDRESS", fields.StreetAddr);
                        writer.WriteElementString("STRESCROWCITYSTATEZIP", fields.CityStateZip);
                        writer.WriteElementString("STRESCROWFAX", fields.FaxNum);
                        writer.WriteElementString("STRESCROWEMAIL", fields.EmailAddr);
                        break;
                    case E_AgentRoleT.Title:
                        writer.WriteElementString("STRTITLEOFFICER", fields.AgentName);
                        writer.WriteElementString("STRTITLECOMPANY", fields.CompanyName);
                        writer.WriteElementString("STRTITLEPHONE", fields.Phone);
                        writer.WriteElementString("STRTITLEADDRESS", fields.StreetAddr);
                        writer.WriteElementString("STRTITLECITYSTATEZIP", fields.CityStateZip);
                        writer.WriteElementString("STRTITLEFAX", fields.FaxNum);
                        writer.WriteElementString("STRTITLENUMBER", fields.CaseNum);
                        writer.WriteElementString("STRTITLEEMAIL", fields.EmailAddr);
                        break;
                    case E_AgentRoleT.BuyerAttorney:
                        writer.WriteElementString("STRBUYERATTYNAME", fields.AgentName);
                        writer.WriteElementString("STRBUYERATTYCOMPANY", fields.CompanyName);
                        writer.WriteElementString("STRBUYERATTYPHONE", fields.Phone);
                        writer.WriteElementString("STRBUYERATTYADDRESS", fields.StreetAddr);
                        writer.WriteElementString("STRBUYERATTYCITYSTATEZIP", fields.CityStateZip);
                        writer.WriteElementString("STRBUYERATTYFAX", fields.FaxNum);
                        writer.WriteElementString("STRBUYERATTYEMAIL", fields.EmailAddr);
                        break;
                    case E_AgentRoleT.BuyerAgent:
                        writer.WriteElementString("STRBAGENTCOMPANY", fields.CompanyName);
                        writer.WriteElementString("STRBAGENTNAME", fields.AgentName);
                        writer.WriteElementString("STRBAGENTADDRESS", fields.StreetAddr);
                        writer.WriteElementString("STRBAGENTPHONE", fields.Phone);
                        writer.WriteElementString("STRBAGENTFAX", fields.FaxNum);
                        writer.WriteElementString("STRBAGENTCITY", fields.City);
                        writer.WriteElementString("STRBAGENTSTATE", fields.State);
                        writer.WriteElementString("STRBAGENTZIP", fields.Zip);
                        writer.WriteElementString("STRBAGENTEMAIL", fields.EmailAddr);
                        break;
                }
            }



            IPreparerFields interviewer = loanData.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            // Needed for StateWide Processing center. 
            writer.WriteElementString("STRBROKERCOMPANY", interviewer.CompanyName);
            writer.WriteElementString("STRBROKERNAME", interviewer.PreparerName);
            writer.WriteElementString("STRINTERVIEWERPHONE", interviewer.Phone);
            writer.WriteElementString("STRBROKERADDRESS", interviewer.StreetAddr);
            writer.WriteElementString("STRBROKERCITY", interviewer.City);
            writer.WriteElementString("STRBROKERSTATE", interviewer.State);
            writer.WriteElementString("STRBROKERZIP", interviewer.Zip);
            writer.WriteElementString("STRBROKERCITYSTATEZIP", interviewer.CityStateZip);
            writer.WriteElementString("STRBROKERPHONE", interviewer.PhoneOfCompany);
            writer.WriteElementString("STRBROKERFAX", interviewer.FaxOfCompany);

            writer.WriteElementString("STRSOCSECNUM", appData.aBSsn);
            writer.WriteElementString("NUMMONTHLYPAYMENT", loanData.sMonthlyPmt_rep);
            writer.WriteElementString("NUMMONTHLYPIPAYMENT", loanData.sProThisMPmt_rep);
            ////sBrokerLockOriginatorPriceBrokComp1PcPrice is not subject to losConvert: Expunge % sign.
            writer.WriteElementString("NUMLOCKPRICE", loanData.sBrokerLockOriginatorPriceBrokComp1PcPrice.Replace("%", ""));
            writer.WriteElementString("NUMAPPRAISEDVALUE", loanData.sApprVal_rep);
            writer.WriteElementString("NUMLTV", loanData.sLtvR_rep);
            writer.WriteElementString("NUMCLTV", loanData.sCltvR_rep);

            writer.WriteEndElement();   // LOANSTATUS
        }

        public static void WriteLoanAgent(XmlWriter writer, Guid loanId, Guid sBrokerId, ref bool bLoanOfficer, ref bool bProcessor, ref bool bSellingAgent)
        {
            string sSQL = "SELECT RoleDesc AS Role, RoleModifiableDesc, UserFirstNm + ' ' + UserLastNm AS EmployeeName, Email, Phone, e.NmlsIdentifier, a.LoginNm " +
                "FROM Loan_User_Assignment l JOIN Role r ON l.RoleID = r.RoleID JOIN Employee e ON l.EmployeeID = e.EmployeeID LEFT JOIN ALL_USER a ON e.EmployeeUserId = a.UserId " +
                "WHERE l.sLId = @LoanId";

            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanId", loanId)
                                        };

            bool loanOfficerFlag = bLoanOfficer;
            bool processorFlag = bProcessor;
            bool sellingAgentFlag = bSellingAgent;

            Action<IDataReader> readHandler = delegate(IDataReader reader)
            {
                while (reader.Read())
                {
                    string role = (string)reader["Role"];
                    string employeeName = (string)reader["EmployeeName"];
                    if (role == "Agent")
                    {
                        writer.WriteElementString("STRLOANOFFICERNAME", employeeName);
                        writer.WriteElementString("STROFFICERNMLS", (string)reader["NmlsIdentifier"]);

                        var loginNm = reader["LoginNm"];

                        if (loginNm != DBNull.Value)
                        {
                            writer.WriteElementString("STRLQBUSERNAME", (string)loginNm);
                        }
                        else
                        {
                            writer.WriteElementString("STRLQBUSERNAME", "");
                        }
                        loanOfficerFlag = true;
                    }
                    else if (role == "Processor")
                    {
                        writer.WriteElementString("STRLOANPROCESSORNAME", employeeName);
                        writer.WriteElementString("STRLOANPROCESSORPHONE", (string)reader["Phone"]);
                        writer.WriteElementString("STRLOANPROCESSOREMAIL", (string)reader["Email"]);
                        processorFlag = true;
                    }
                    else if (role == "RealEstateAgent")
                    {
                        writer.WriteElementString("STRSAGENTNAME", employeeName);
                        writer.WriteElementString("STRSAGENTEMAIL", (string)reader["Email"]);
                        writer.WriteElementString("STRSAGENTPHONE", (string)reader["Phone"]);
                        sellingAgentFlag = true;
                    }
                }
            };

            DBSelectUtility.ProcessDBData(sBrokerId, sSQL, null, parameters, readHandler);

            bLoanOfficer = loanOfficerFlag;
            bProcessor = processorFlag;
            bSellingAgent = sellingAgentFlag;
        }
    }

    public class LoanLONXmlReader 
    {
        public LoanLONXmlReader() 
        {
        }

        public void Read(Guid sLId, XmlDocument doc) 
        {
            CPageData dataLoan = new CLoanLONXmlWriterData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            // Only update the interviewer section in 1003 pg 3 for now.
            // Only update data existed in XML
            XmlElement el = null;
            IPreparerFields interviewer = dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.CreateNew);

            CAgentFields loanOfficer = dataLoan.GetAgentOfRole(E_AgentRoleT.LoanOfficer, E_ReturnOptionIfNotExist.CreateNew);
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBROKERCOMPANY");
            if (null != el) 
            {
                interviewer.CompanyName = el.InnerText;
                loanOfficer.CompanyName = el.InnerText;
            }

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBROKERNAME");
            if (null != el) 
            {
                interviewer.PreparerName = el.InnerText;
                loanOfficer.AgentName = el.InnerText;
            }

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRINTERVIEWERPHONE");
            if (null != el) 
            {
                interviewer.Phone = el.InnerText;
                loanOfficer.Phone = el.InnerText;;
            }

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBROKERADDRESS");
            if (null != el) 
            {
                interviewer.StreetAddr = el.InnerText;
                loanOfficer.StreetAddr = el.InnerText;
            }

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBROKERCITY");
            if (null != el) 
            {
                interviewer.City = el.InnerText;
                loanOfficer.City = el.InnerText;
            }

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBROKERSTATE");
            if (null != el) 
            {
                interviewer.State = el.InnerText;
                loanOfficer.State = el.InnerText;
            }

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBROKERZIP");
            if (null != el) 
            {
                interviewer.Zip = el.InnerText;
                loanOfficer.Zip = el.InnerText;
            }
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBROKERPHONE");
            if (null != el) 
            {
                interviewer.PhoneOfCompany = el.InnerText;
                loanOfficer.PhoneOfCompany = el.InnerText;
            }

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBROKERFAX");
            if (null != el) 
            {
                interviewer.FaxOfCompany = el.InnerText;
                loanOfficer.FaxOfCompany = el.InnerText;
            }

            CAgentFields buyerAgent = dataLoan.GetAgentOfRole(E_AgentRoleT.BuyerAgent, E_ReturnOptionIfNotExist.CreateNew);
            bool hasBuyerAgent = false;

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTCOMPANY");
            if (null != el) 
            {
                buyerAgent.CompanyName = el.InnerText;
                hasBuyerAgent = true;
            }

            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTNAME");
            if (null != el) 
            {
                buyerAgent.AgentName = el.InnerText;
                hasBuyerAgent = true;
            }
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTADDRESS");
            if (null != el) 
            {
                buyerAgent.StreetAddr = el.InnerText;
                hasBuyerAgent = true;
            }
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTPHONE");
            if (null != el) 
            {
                buyerAgent.Phone = el.InnerText;
                hasBuyerAgent = true;
            }
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTFAX");
            if (null != el) 
            {
                buyerAgent.FaxNum = el.InnerText;
                hasBuyerAgent = true; 
            }
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTCITY");
            if (null != el) 
            {
                buyerAgent.City = el.InnerText;
                hasBuyerAgent = true;
            }
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTSTATE");
            if (null != el) 
            {
                buyerAgent.State = el.InnerText;
                hasBuyerAgent = true;
            }
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTZIP");
            if (null != el) 
            {
                buyerAgent.Zip =el.InnerText;
                hasBuyerAgent = true; 
            }
            el = (XmlElement) doc.SelectSingleNode("//LOANSTATUS/STRBAGENTEMAIL");
            if (null != el) 
            {
                buyerAgent.EmailAddr = el.InnerText;
                hasBuyerAgent = true;
            }
            if (hasBuyerAgent) 
            {
                buyerAgent.Update();
            }
            interviewer.Update();
            loanOfficer.Update();
            
            dataLoan.Save();
        }

    }
}
