using System;
using System.Collections;
using System.IO;

using LendersOffice.Common;
using LendersOffice.Conversions.MORNETPlus;

namespace LendersOffice.Conversions.MORNETPlus32
{
	public class MORNETPlus32Document : MORNETPlusDocument
	{
        private const string ENVELOPE_CONTROL_NUMBER = "0";
        private const string TRANSACTION_CONTROL_NUMBER = "1";
        private const string TRANSACTION_ID = "T100099-002";

        private MORNETPlusDataset m_1003;
        private MORNETPlusDataset m_additionalCaseData;
        private MORNETPlusDataset m_loanProductData;
        private MORNETPlusDataset m_governmentLoanData;
        private MORNETPlusDataset m_communityLendingData;
        private MORNETPlusDataset m_appraisalData; 
        private MORNETPlusDataset m_floodData; 

        #region Segment Definitions
        private static SegmentDefinition[] s_1003Definitions = {
        new SegmentDefinition("00A", 1, 4, 5, 5),
        new SegmentDefinition("01A", 1, 4, 6, 86, 116, 131, 146, 153, 156, 158, 238, 317),
        new SegmentDefinition("02A", 1, 4, 54, 89, 91, 96, 100, 103, 105, 185, 188),
        new SegmentDefinition("PAI", 1, 4, 15, 55, 65),
        new SegmentDefinition("02B", 1, 4, 6, 8, 88, 89, 149, 150, 157),
        new SegmentDefinition("02C", 1, 4, 63),
        new SegmentDefinition("02D", 1, 4, 8, 23, 38, 53, 68, 70, 150, 151, 165),
        new SegmentDefinition("02E", 1, 4, 6, 21, 100),
        new SegmentDefinition("03A", 1, 4, 6, 15, 50, 85, 120, 124, 134, 137, 139, 140, 142, 143, 152, 160, 239),
        new SegmentDefinition("03B", 1, 4, 13, 15),
        new SegmentDefinition("03C", 1, 4, 13, 15, 65, 100, 102, 107, 111, 112, 114, 116, 165),
        new SegmentDefinition("04A", 1, 4, 13, 48, 83, 118, 120, 125, 129, 130, 132, 134, 136, 161, 170),
        new SegmentDefinition("04B", 1, 4, 13, 48, 83, 118, 120, 125, 129, 130, 131, 139, 147, 162, 187, 196),
        new SegmentDefinition("05H", 1, 4, 13, 14, 16, 30),
        new SegmentDefinition("05I", 1, 4, 13, 15, 29),
        // OPM 20691 - Temporarily un-retire this segment until Fannie Mae officially no longer accepts it
        // OPM 20691 7/14/10 - re-retiring this segment because it is officially not supported.  Leaving the line commented in case an unkown situation arises related to this segment.
        // OPM 55950 9/2/10 - re-un-retiring the segment because some LOS have not upgraded their system to
        // handle this segment that has been retired for 2 years now and had a grace period even before that.
        new SegmentDefinition("06A", 1, 4, 13, 48, 62), 
        new SegmentDefinition("06B", 1, 4, 13, 43, 58, 72),
        new SegmentDefinition("06C", 1, 4, 13, 16, 51, 86, 121, 123, 128, 132, 162, 177, 184, 264, 265, 266),
        new SegmentDefinition("06D", 1, 4, 13, 43, 47, 61),
        new SegmentDefinition("06F", 1, 4, 13, 16, 31, 34, 93),
        new SegmentDefinition("06G", 1, 4, 13, 48, 83, 85, 90, 94, 95, 97, 112, 127, 142, 157, 172, 187, 188, 189, 191, 205),
        new SegmentDefinition("06H", 1, 4, 13, 48, 83, 118, 153, 182),
        new SegmentDefinition("06L", 1, 4, 13, 15, 50, 85, 120, 122, 127, 131, 161, 176, 179, 194, 195, 197, 198, 199, 200,200),
        new SegmentDefinition("06S", 1, 4, 13, 16, 30),
        new SegmentDefinition("07A", 1, 4, 19, 34, 49, 64, 79, 94, 109, 124, 139, 154, 168),
        new SegmentDefinition("07B", 1, 4, 6, 20),
        new SegmentDefinition("08A", 1, 4, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28),
        new SegmentDefinition("08B", 1, 4, 13, 15, 269),
        new SegmentDefinition("09A", 1, 4, 13, 20),
        new SegmentDefinition("10A", 1, 4, 13, 14, 15, 45, 45),
        new SegmentDefinition("10B", 1, 4, 5, 65, 73, 83, 118, 153, 188, 223, 225, 230, 233),
        new SegmentDefinition("10R", 1, 4, 13, 14)
                                                               };

        private static SegmentDefinition[] s_additionalCaseDataDefinitions = {
        new SegmentDefinition("99B", 1, 4, 5, 7, 22, 29, 31, 34, 94, 129, 144, 145),
        new SegmentDefinition("ADS", 1, 4, 39, 88),
        new SegmentDefinition("SCA", 1, 4, 7, 10, 17)
                                                                             };

        private static SegmentDefinition[] s_loanProductDataDefinitions = {
        new SegmentDefinition("LNC", 1, 4, 5, 6, 8, 10, 12, 14, 16, 18, 25, 26, 27, 28, 29, 36, 43, 58, 59, 67, 75, 82, 85, 90, 91, 91),
        new SegmentDefinition("PID", 1, 4, 34, 49, 53),
        new SegmentDefinition("PCH", 1, 4, 7, 8, 10, 11, 12, 13),
        new SegmentDefinition("ARM", 1, 4, 11, 13, 20, 26),
        new SegmentDefinition("PAJ", 1, 4, 8, 11, 14, 15, 22, 37, 44, 59, 61),
        new SegmentDefinition("RAJ", 1, 4, 8, 11, 14, 15, 22, 29, 31),
        new SegmentDefinition("BUA", 1, 4, 7, 10, 17, 18, 19, 19)
                                                                          };

        private static SegmentDefinition[] s_governmentLoanDataDefinitions = {
        new SegmentDefinition("IDA", 1, 4, 26),
        new SegmentDefinition("LEA", 1, 4, 24, 44, 54, 66),
        new SegmentDefinition("GOA", 1, 4, 5, 20, 35, 50, 57, 72, 79, 94, 101, 102, 136),
        new SegmentDefinition("GOB", 1, 4, 17, 32, 39, 54, 61, 63),
        new SegmentDefinition("GOC", 1, 4, 5, 20, 27, 34, 40),
        new SegmentDefinition("GOD", 1, 4, 13, 28, 43, 58, 73, 88, 103, 118, 132),
        new SegmentDefinition("GOE", 1, 4, 13, 23, 26, 29, 32, 32)
                                                                             };

        private static SegmentDefinition[] s_communityLendingDefinitions = {
        new SegmentDefinition("LMD", 1, 4, 44, 84, 85, 86, 101, 116, 130)
                                                                           };


        private static SegmentDefinition s_envelopeHeaderDefinition = new SegmentDefinition("EH", 1, 4, 10, 35, 46, 54);
        private static SegmentDefinition s_envelopeTrailerDefinition = new SegmentDefinition("ET", 1, 4, 12);
        private static SegmentDefinition s_transactionHeaderDefinition = new SegmentDefinition("TH", 1, 4, 15, 23);
        private static SegmentDefinition s_transactionTrailerDefinition = new SegmentDefinition("TT", 1, 4, 12);
        private static SegmentDefinition s_transactionProcessionDefinition = new SegmentDefinition("TPI", 1, 4, 9, 11, 41, 41);

        private static SegmentDefinition s_MDF_6Header = new SegmentDefinition("000", 1, 4, 7, 12, 12);
        private static SegmentDefinition s_MDF_Header = new SegmentDefinition("000", 1, 4, 7, 11);
        #endregion

        public override void Initialize() 
        {
            m_1003                 = new MORNETPlusDataset(s_1003Definitions);
            m_additionalCaseData   = new MORNETPlusDataset(s_additionalCaseDataDefinitions);
            m_loanProductData      = new MORNETPlusDataset(s_loanProductDataDefinitions);
            m_governmentLoanData   = new MORNETPlusDataset(s_governmentLoanDataDefinitions);
            m_communityLendingData = new MORNETPlusDataset(s_communityLendingDefinitions);
            m_appraisalData        = null;
            m_floodData            = null;

        }

        protected override void Process() 
        {
            Initialize();

            ParseEnvelopeHeader();
            ParseTransactionHeader();
            ParseTransactionProcessingInformation();

            Parse(s_MDF_6Header,  "1", "3.20", m_1003);
            // 10/11/2007 dd - FNMA can return these segments in any order.
            while (true) 
            {
                string line = GetNextLine();
                if (s_MDF_Header.IsValid(line)) 
                {
                    string fileType = s_MDF_Header.Get(line, "000-020");
                    PushLineBack();
                    if (fileType == "70") 
                    {
                        Parse(s_MDF_Header,  "70", "3.20", m_additionalCaseData);
                    } 
                    else if (fileType == "11") 
                    {
                        Parse(s_MDF_Header,  "11", "3.20", m_loanProductData);
                    } 
                    else if (fileType == "20") 
                    {
                        Parse(s_MDF_Header,  "20", "3.20", m_governmentLoanData);
                    } 
                    else if (fileType == "30") 
                    {
                        Parse(s_MDF_Header,  "30", "3.20", m_communityLendingData);
                    } 
                    else 
                    {
                        PushLineBack();
                        break;
                    }
                } 
                else 
                {
                    PushLineBack();
                    break; // Break out of the loop.
                }

            }
            ParseTransactionTrailer();
            ParseEnvelopeTrailer();
        }
        private void ParseEnvelopeHeader() 
        {
            string line = GetNextLine();

            if (!s_envelopeHeaderDefinition.IsValid(line)) 
            {
                throw ConstructError("Invalid Envelope Header.", line);
            }
            sFannieInstitutionId = s_envelopeHeaderDefinition.Get(line, "EH-020");
            Log("Header: Envelope Control Number = " + s_envelopeHeaderDefinition.Get(line, "EH-050"));
        }
        private void ParseTransactionHeader() 
        { 
            string line = GetNextLine();

            if (!s_transactionHeaderDefinition.IsValid(line)) 
            {
                throw ConstructError("Invalid Transaction Header.", line);
            }

            Log("Header: Transaction Control Number = " + s_transactionHeaderDefinition.Get(line, "TH-020"));

        }

        private void ParseTransactionProcessingInformation() 
        { 
            string line = GetNextLine();
            if (!s_transactionProcessionDefinition.IsValid(line)) 
            {
                throw ConstructError("Invalid Transaction Processing Information.", line);
            }
            sDuCaseId = s_transactionProcessionDefinition.Get(line, "TPI-040");
        }

        private void Parse(SegmentDefinition header, string fileType, string fileVersion, MORNETPlusDataset ds) 
        {
            Log("*** Start Parse 000 segment. FileType = " + fileType);
            string line = GetNextLine();
            if (header.IsValid(line)) 
            {
                string _fileType = header.Get(line, "000-020");
                string _fileVersion = header.Get(line, "000-030");
				if (_fileType == fileType && _fileVersion == fileVersion) 
				{
					while((line = GetNextLine()) != null) 
					{
						if (!ds.Add(line))
							break;
					}
				}
				else
				{
					while (( line = GetNextLine() ) != null && ds.IsPartOfDataSet( line ) ) ; //eat up all the bad lines 
					ds.Skipped = true ; 
				}
            }
            PushLineBack();
            Log("*** End Parse 000 segment. FileType = " + fileType);
        }

        private void ParseTransactionTrailer() 
        { 
            string line = GetNextLine();

            if (!s_transactionTrailerDefinition.IsValid(line)) 
            {
                PushLineBack();
            }
        }
        private void ParseEnvelopeTrailer() 
        { 
            string line = GetNextLine();

            if (!s_envelopeTrailerDefinition.IsValid(line)) 
            {
                PushLineBack();

            }
        }

        public MORNETPlusSegment EnvelopeHeader 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_envelopeHeaderDefinition);
                segment["EH-020"] = sFannieInstitutionId; // 1/22/2009 dd - Export FannieMae Lender Institution Id if provider
                segment["EH-030"] = "";
                segment["EH-040"] = "";
                segment["EH-050"] = ENVELOPE_CONTROL_NUMBER;

                return segment;
            }
        }
        public MORNETPlusSegment TransactionHeader 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_transactionHeaderDefinition);
                segment["TH-020"] = TRANSACTION_ID;
                segment["TH-030"] = TRANSACTION_CONTROL_NUMBER;
                return segment;
            }
        }
        public MORNETPlusSegment TransactionProcessingInfo 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_transactionProcessionDefinition);
                bool isResubmit = sDuCaseId != ""; // 5/10/2005 dd - If sDuCaseId is not empty then set isResubmit=true

                segment["TPI-020"] = " 1.00";
                segment["TPI-030"] = "01";
                segment["TPI-040"] = sDuCaseId;
                segment["TPI-050"] = isResubmit ? "R" : "N";

                return segment;
            }
        }
        public MORNETPlusSegment TransactionTrailer 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_transactionTrailerDefinition);
                segment["TT-020"] = TRANSACTION_CONTROL_NUMBER;
                return segment;
            }
        }
        public MORNETPlusSegment EnvelopeTrailer 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_envelopeTrailerDefinition);
                segment["ET-020"] = ENVELOPE_CONTROL_NUMBER;
                return segment;
            }
        }

        public MORNETPlusSegment _1003Header 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_MDF_6Header);
                segment["000-020"] = "1"; // 1 = 1003
                segment["000-030"] = "3.20"; // File Version
                segment["000-040"] = "W"; // W = Working copy

                return segment;
            }
        }
        public override MORNETPlusDataset _1003 
        {
            get { return m_1003; }
        }

        public MORNETPlusSegment AdditionalCaseDataHeader 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_MDF_Header);
                segment["000-020"] = "70"; // 70 = Additional Case Data
                segment["000-030"] = "3.20"; // File Version

                return segment;
            }
        }

        public override MORNETPlusDataset AdditionalCaseData 
        {
            get { return m_additionalCaseData; }
        }

        public MORNETPlusSegment LoanProductDataHeader 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_MDF_Header);
                segment["000-020"] = "11"; // 11 = Loan Product Data
                segment["000-030"] = "3.20"; // File Version

                return segment;
            }
        }
        public override MORNETPlusDataset LoanProductData 
        {
            get { return m_loanProductData; }
        }
        public MORNETPlusSegment GovernmentLoanDataHeader 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_MDF_Header);
                segment["000-020"] = "20"; // 20 = Government Loan Data
                segment["000-030"] = "3.20"; // File Version
                return segment;
            }
        }
        public override MORNETPlusDataset GovernmentLoanData 
        {
            get { return m_governmentLoanData; }
        }
        public MORNETPlusSegment CommunityLendingDataHeader 
        {
            get 
            {
                MORNETPlusSegment segment = new MORNETPlusSegment(s_MDF_Header);
                segment["000-020"] = "30"; // 30 = Community Lending Data
                segment["000-030"] = "3.20"; // File Version
                return segment;
            }
        }
        public override MORNETPlusDataset CommunityLendingData 
        {
            get { return m_communityLendingData; }
        }
        public override MORNETPlusDataset AppraisalData 
        {
            get { return m_appraisalData; }
        }
        public override MORNETPlusDataset FloodData 
        {
            get { return m_floodData; }
        }
	}
}
