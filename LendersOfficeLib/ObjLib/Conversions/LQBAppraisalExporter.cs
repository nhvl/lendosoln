﻿namespace LendersOffice.ObjLib.Conversions
{
    using System;
    using DataAccess;
    using global::LQBAppraisal;
    using global::LQBAppraisal.LQBAppraisalRequest;
    using LendersOffice.Integration.Appraisals;

    public static class LQBAppraisalExporter
    {
        public static LQBAppraisalRequest CreateAccountInfoRequest(AppraisalVendorCredentials credentials, Guid loanIdentifier, string vendorName)
        {
            var request = new LQBAppraisalRequest();
            request.AccountID = credentials.AccountId;
            request.Username = credentials.UserName;
            request.Password = credentials.Password;
            request.AppraisalOrder = null;
            request.AccountInfoRequest.RequestBillingMethods = E_YesNo.Yes;
            request.AccountInfoRequest.RequestProducts = E_YesNo.Yes;

            
            if (string.Equals(vendorName, "LenderX", StringComparison.OrdinalIgnoreCase)
                && loanIdentifier != null && loanIdentifier != Guid.Empty)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependency(loanIdentifier, typeof(LQBAppraisalExporter));
                dataLoan.InitLoad();
                request.AccountInfoRequest.LoanType = ToLoanType(dataLoan.sLT);
                request.AccountInfoRequest.IntendedUse = ToIntendedUse(dataLoan.sLPurposeT);
            }

            return request;
        }
        public static LQBAppraisalRequest CreateAppraisalOrderRequest(AppraisalVendorCredentials credentials, AppraisalOrder order)
        {
            var request = new LQBAppraisalRequest();
            request.AccountID = credentials.AccountId;
            request.Username = credentials.UserName;
            request.Password = credentials.Password;
            request.AccountInfoRequest = null;

            request.AppraisalOrder = order;

            return request;
        }

        /// <summary>
        /// Maps the loan type to the corresponding value in the appraisal schema.
        /// </summary>
        /// <param name="loanType">The loan type.</param>
        /// <returns>The corresponding value in the appraisal schema.</returns>
        public static E_LoanInfoLoanType ToLoanType(E_sLT loanType)
        {
            switch (loanType)
            {
                case E_sLT.Conventional:
                    return E_LoanInfoLoanType.Conventional;
                case E_sLT.FHA:
                    return E_LoanInfoLoanType.FHA;
                case E_sLT.UsdaRural:
                    return E_LoanInfoLoanType.USDARuralHousing;
                case E_sLT.VA:
                    return E_LoanInfoLoanType.VA;
                case E_sLT.Other:
                    return E_LoanInfoLoanType.Other;
                default:
                    throw new UnhandledEnumException(loanType);
            }
        }

        /// <summary>
        /// Maps the loan purpose to the corresponding value in the appraisal schema.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose.</param>
        /// <returns>The corresponding value in the appraisal schema.</returns>
        public static E_LoanInfoIntendedUse ToIntendedUse(E_sLPurposeT loanPurpose)
        {
            switch(loanPurpose)
            {
                case E_sLPurposeT.Construct:
                    return E_LoanInfoIntendedUse.Construction;
                case E_sLPurposeT.ConstructPerm:
                    return E_LoanInfoIntendedUse.ConstructionPerm;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                    return E_LoanInfoIntendedUse.FHAStreamlineRefi;
                case E_sLPurposeT.Purchase:
                    return E_LoanInfoIntendedUse.Purchase;
                case E_sLPurposeT.Refin:
                    return E_LoanInfoIntendedUse.RefiRateTerm;
                case E_sLPurposeT.RefinCashout:
                    return E_LoanInfoIntendedUse.RefinanceCashout;
                case E_sLPurposeT.VaIrrrl:
                    return E_LoanInfoIntendedUse.VAIRRRL;
                case E_sLPurposeT.HomeEquity:
                case E_sLPurposeT.Other:
                    return E_LoanInfoIntendedUse.Other;
                default:
                    throw new UnhandledEnumException(loanPurpose);
            }
        }
    }
}
