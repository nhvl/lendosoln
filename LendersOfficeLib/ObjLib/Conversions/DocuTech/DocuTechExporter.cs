﻿namespace LendersOffice.Conversions.DocuTech
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using DataAccess;
    using global::DocuTech;
    using LendersOffice.Admin;
    using LendersOffice.com.conformx.www;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using LendersOffice.Common;

    public class DocuTechExporter
    {
        public static bool Export(Guid sLId, out string url, out string errorMessages, AbstractUserPrincipal principal)
        {
            BrokerDB db = BrokerDB.RetrieveById(principal.BrokerId);

            BrokerDB.AutoLoginDocuTechInfo info = db.DocuTechInfo;

            if (info == null)
            {
                url = "";
                errorMessages = "Docutech Information is incorrect.";
                return false;
            }

            return Export(sLId, info.UserName, info.Password.Value, info.PackageType, out url, out errorMessages, principal);
        }

        public static bool Export(Guid sLId, string userName, string password, E_DocumentsPackageType packageType, out string url, out string errorMessage, AbstractUserPrincipal principal)
        {
            bool hasError = true;

            url = string.Empty;
            errorMessage = "Unable to export to DocuTech. Contact customer support.";

            EngineWS engine = new EngineWS();
            engine.Url = DocuTechExportUrl(userName);
            
            DocuTechExporter exporter = new DocuTechExporter(sLId, packageType);
            string xml = null;
            try
            {
                xml = exporter.Export(userName, password, packageType, principal);
            }
            catch (FeeDiscrepancyException e)
            {
                errorMessage = e.UserMessage;
                return true;
            }

            string responseXml = engine.Process(xml);
            if (responseXml.IndexOf("EXCEPTIONS") >= 0)
            {
                hasError = true;

                Exceptions exception = new Exceptions();
                exception.ReadXml(XmlReader.Create(new StringReader(responseXml)));

                if (exception.AuthenticationExceptionList.Count > 0)
                {
                    errorMessage = "Invalid user name and password.";
                } 
            }
            else
            {
                EMortgagePackage emortgagePackage = new EMortgagePackage();
                emortgagePackage.ReadXml(XmlReader.Create(new StringReader(responseXml)));

                foreach (var key in emortgagePackage.KeyList)
                {
                    if (key.Name == "URL")
                    {
                        url = key.Value;
                        hasError = false;
                        errorMessage = string.Empty;
                        break;
                    }
                }
            }

            if (hasError)
            {
                Tools.LogInfo("DocuTech: " + Environment.NewLine + xml + Environment.NewLine + "Response:" + Environment.NewLine + responseXml);
            }
            else if (packageType == E_DocumentsPackageType.AdverseAction)
            {
                exporter.SaveHMDACreditDenialInfo();
            }

            return hasError;
        }

        private IGFEArchive m_gfea = null;
        private CPageData m_dataLoan = null;
        bool m_use2015DataLayer = false;
        bool m_exportTridData = false;

        public string sLNm
        {
            get;
            private set;
        }

        public Guid sLId
        {
            get;
            private set;
        }

        public DocuTechExporter(Guid sLId, E_DocumentsPackageType packageType)
        {
            m_dataLoan = GetPageData(sLId);
            m_dataLoan.SetFormatTarget(FormatTarget.DocuTech);
            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            m_exportTridData = m_dataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID;
            m_use2015DataLayer = m_dataLoan.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy;

            if (m_use2015DataLayer)
            {
                if (packageType == E_DocumentsPackageType.InitialDisclosure && m_dataLoan.sLastDisclosedClosingCostArchive != null)
                {
                    m_dataLoan.ApplyClosingCostArchive(m_dataLoan.sLastDisclosedClosingCostArchive);
                }
            }
            else if (m_gfea == null)
            {
                m_gfea = m_dataLoan.ExtractStaticGFEArchive();
            }

            this.sLNm = m_dataLoan.sLNm;
            this.sLId = sLId;
        }

        private CPageData GetPageData(Guid loanid)
        {
            return CPageData.CreateUsingSmartDependency(loanid, typeof(DocuTechExporter));
        }

        private void SaveHMDACreditDenialInfo()
        {
            m_dataLoan.sHmdaDeniedFormDoneD_rep = System.DateTime.Today.ToString("d");
            m_dataLoan.sHmdaDeniedFormDone = true;
            m_dataLoan.sHmdaDeniedFormDoneBy = PrincipalFactory.CurrentPrincipal.DisplayName;
            m_dataLoan.Save();
        }

        public string Export(string userName, string password, E_DocumentsPackageType packageType, AbstractUserPrincipal principal)
        {
            DocumentRequestImport _documentRequestImport = CreateDocumentRequestImport(userName, password, packageType);

            byte[] bytes = null;
            int contentLength = 0;
            using (MemoryStream stream = new MemoryStream(5000))
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII))
                {
                    _documentRequestImport.WriteXml(writer);
                    writer.Flush();
                    contentLength = (int)stream.Position;
                }
                bytes = stream.GetBuffer();
            }

            string xml = System.Text.Encoding.ASCII.GetString(bytes, 0, contentLength);

            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            if (db.IsSeamlessConformXEnabled)
            {
                //Replace Loan with Generic Mismo Loan xml
                string genericMismoLoanXml = LendersOffice.Conversions.GenericMismoClosing26.GenericMismoClosing26Exporter.ExportCustom(m_dataLoan.sLId, "DocuTech", principal);
                xml = System.Text.RegularExpressions.Regex.Replace(xml, "(<MAIN>)[\\w\\W]*(</MAIN>)", string.Format("$1{0}$2", genericMismoLoanXml));

                //Remove Custom ConformX
                xml = System.Text.RegularExpressions.Regex.Replace(xml, "<CUSTOM>[\\w\\W]*</CUSTOM>", "");

                //Add LOXml
                bool bypassFieldSecurity = true;
                string LOXmlToInsert = LOFormatExporter.Export( m_dataLoan.sLId,
                                                                BrokerUserPrincipal.CurrentPrincipal,
                                                                Integration.DocumentVendor.VendorConfig.LOXmlExportFields,
                                                                FormatTarget.MismoClosing,
                                                                bypassFieldSecurity);
                LOXmlToInsert = LOXmlToInsert.Replace("LOXmlFormat version=\"1.0\"", "LOXml") // replace LOXmlFormat tag with LOXml
                                            .Replace("LOXmlFormat", "LOXml")
                                            .Replace("<loan>", "") //remove loan tags
                                            .Replace("</loan>", "");
                xml = xml.Insert(xml.IndexOf("</MAIN>"), LOXmlToInsert);//insert LOXml
            }
            return xml;
        }

        internal static string DocuTechExportUrl(string userName)
        {
            //case 70087. Send all the requests from the test account to the staging server.
            AbstractUserPrincipal principal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (principal != null)
            {
                foreach (string testLogin in ConstAppDavid.DocuTech_TestLQBLogins)
                {
                    if (principal.LoginNm.Equals(testLogin, StringComparison.OrdinalIgnoreCase))
                    {
                        return ConstAppDavid.DocuTech_TestUrl;
                    }
                }
            }

            foreach (string testUserName in ConstAppDavid.DocuTech_TestUserAccounts)
            {
                if (userName.Equals(testUserName, StringComparison.OrdinalIgnoreCase))
                {
                    return ConstAppDavid.DocuTech_TestUrl;
                }
            }

            BrokerDB db = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            if (db.IsDocuTechStageEnabled && !db.IsDocuTechEnabled) // use stage, provided production DocuTech is not enabled
                return ConstAppDavid.DocuTech_TestUrl;

            return ConstAppDavid.DocuTech_ProductionUrl;
        }

        private DocumentRequestImport CreateDocumentRequestImport(string userName, string password, E_DocumentsPackageType packageType)
        {
            DocumentRequestImport _DocumentRequestImport = new DocumentRequestImport();

            // _DocumentRequestImport.Version = ;
            // _DocumentRequestImport.PopulatingSystemName = ;
            // _DocumentRequestImport.PopulatingSystemVersion = ;
            // _DocumentRequestImport.PopulatingSystemLightsOn = ;

            _DocumentRequestImport.User = CreateUser(userName, password);
            _DocumentRequestImport.EMortgagePackage = CreateEMortgagePackage();
            _DocumentRequestImport.RequestOptions = CreateRequestOptions(packageType);
            return _DocumentRequestImport;

        }

        private RequestOptions CreateRequestOptions(E_DocumentsPackageType packageType)
        {
            RequestOptions _RequestOptions = new RequestOptions();

            // _RequestOptions.IncludeXmlSignatureMetaFile = ;

            _RequestOptions.DocumentsList.Add(CreateDocuments(packageType));
            // _RequestOptions.DocumentFormatList.Add(CreateDocumentFormat());
            // _RequestOptions.DeliveryFormat = CreateDeliveryFormat();
            // _RequestOptions.DataChecks = CreateDataChecks();
            return _RequestOptions;

        }

        private DataChecks CreateDataChecks()
        {
            DataChecks _DataChecks = new DataChecks();

            // _DataChecks.MissingFieldCheck = ;
            // _DataChecks.IgnoreOptionalFields = ;
            // _DataChecks.BreakOnException = ;

            return _DataChecks;

        }

        private DeliveryFormat CreateDeliveryFormat()
        {
            DeliveryFormat _DeliveryFormat = new DeliveryFormat();

            // _DeliveryFormat.BundlingOption = ;
            // _DeliveryFormat.CompressionType = ;

            return _DeliveryFormat;

        }

        private global::DocuTech.DocumentFormat CreateDocumentFormat()
        {
            global::DocuTech.DocumentFormat _DocumentFormat = new global::DocuTech.DocumentFormat();

            // _DocumentFormat.Type = ;

            // _DocumentFormat.SmartDocType = CreateSmartDocType();
            return _DocumentFormat;

        }

        private SmartDocType CreateSmartDocType()
        {
            SmartDocType _SmartDocType = new SmartDocType();

            // _SmartDocType.Level = ;

            return _SmartDocType;

        }

        private Documents CreateDocuments(E_DocumentsPackageType packageType)
        {
            Documents _Documents = new Documents();

            _Documents.PackageType = packageType;

            // _Documents.DocumentList.Add(CreateDocument());
            return _Documents;

        }

        private Document CreateDocument()
        {
            Document _Document = new Document();

            // _Document.Id = ;
            // _Document.Name = ;
            // _Document.Blank = ;
            // _Document.Sequence = ;
            // _Document.Copies = ;

            return _Document;

        }

        private EMortgagePackage CreateEMortgagePackage()
        {
            EMortgagePackage _EMortgagePackage = new EMortgagePackage();

            _EMortgagePackage.Id = ConstAppDavid.DocuTech_SystemDocumentIdentifier;
            // _EMortgagePackage.MISMOVersionIdentifier = ;

            // _EMortgagePackage.KeyList.Add(CreateKey());
            // _EMortgagePackage.EmbeddedFile = CreateEmbeddedFile();
            _EMortgagePackage.SmartDocument = CreateSmartDocument();

            return _EMortgagePackage;

        }

        private SmartDocument CreateSmartDocument()
        {
            SmartDocument _SmartDocument = new SmartDocument();

            // _SmartDocument.MISMOVersionIdentifier = ;
            _SmartDocument.PopulatingSystemDocumentIdentifier = ConstAppDavid.DocuTech_SystemDocumentIdentifier;
            _SmartDocument.PackageIdentifier = " ";
            _SmartDocument.DTC_PopulatingSystemIdentifier = "LendingQB";

            _SmartDocument.Data = CreateSmartDocumentData();
            return _SmartDocument;

        }

        private SmartDocumentData CreateSmartDocumentData()
        {
            SmartDocumentData _SmartDocumentData = new SmartDocumentData();

            _SmartDocumentData.Main = CreateSmartDocumentDataMain();
            _SmartDocumentData.Custom = CreateCustom();

            return _SmartDocumentData;

        }

        private Custom CreateCustom()
        {
            Custom _Custom = new Custom();


            // _Custom.UserData = CreateUserData();
            _Custom.ConformX = CreateConformX();
            return _Custom;

        }

        private ConformX CreateConformX()
        {
            ConformX _ConformX = new ConformX();

            int nApps = m_dataLoan.nApps;
            int borrowerSequenceId = 0;
            for (int nAppIndex = 0; nAppIndex < nApps; nAppIndex++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(nAppIndex);
                if (!dataApp.aBIsValidNameSsn && !dataApp.aCIsValidNameSsn)
                {
                    continue; // Skip if there is no borrower & no coborrower.
                }
                if (dataApp.aBIsValidNameSsn)
                {
                    borrowerSequenceId++;
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    _ConformX.CxBorrowerList.Add(CreateCxBorrower(borrowerSequenceId, dataApp));
                }
                if (dataApp.aCIsValidNameSsn)
                {
                    borrowerSequenceId++;
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    _ConformX.CxBorrowerList.Add(CreateCxBorrower(borrowerSequenceId, dataApp));
                }
            }
            // _ConformX.AdverseAction = CreateAdverseAction();
            // _ConformX.CxEscrow = CreateCxEscrow();
            // _ConformX.CxAssignment = CreateCxAssignment();
            _ConformX.CxLoan = CreateCxLoan();
            // _ConformX.CxProperty = CreateCxProperty();
            // _ConformX.CxTitle = CreateCxTitle();
            // _ConformX.CxHeloc = CreateCxHeloc();
            // _ConformX.CxRespaFee = CreateCxRespaFee();
            // _ConformX.CxInvestor = CreateCxInvestor();
            // _ConformX.CxLender = CreateCxLender();
            // _ConformX.CxNote = CreateCxNote();
            // _ConformX.CxSellerList.Add(CreateCxSeller());
            // _ConformX.CxConsolidation = CreateCxConsolidation();
            // _ConformX.CxPreviousLoan = CreateCxPreviousLoan();
            // _ConformX.CxBroker = CreateCxBroker();
            // _ConformX.Austraila = CreateAustraila();

            CAgentFields processor = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Processor, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            _ConformX.CxAgent = CreateCxAgent(E_CxAgentType.Processor, processor.AgentName);
            // _ConformX.CxCompliance = CreateCxCompliance();
            // _ConformX.CxCreditScoreList.Add(CreateCxCreditScore());
            // _ConformX.CxDocTrigger = CreateCxDocTrigger();
            // _ConformX.CxEsignature = CreateCxEsignature();
            // _ConformX.CxProcessing = CreateCxProcessing();
            _ConformX.CxRefinance = CreateCxRefinance();
            // _ConformX.CxShipping = CreateCxShipping();
            // _ConformX.CxStateSpecificInfo = CreateCxStateSpecificInfo();
            // _ConformX.Underwriter = CreateUnderwriter();
            // _ConformX.UserDefined = CreateUserDefined();
            //_ConformX.Los = CreateLos();
            return _ConformX;

        }

        //private Los CreateLos()
        //{
        //    Los _Los = new Los();
        //    _Los.PopulatingSystemRefId = ConstAppDavid.DocuTech_SystemDocumentIdentifier;
        //    return _Los;
        //}
        private UserDefined CreateUserDefined()
        {
            UserDefined _UserDefined = new UserDefined();

            // _UserDefined.SequenceIdentifier = ;
            // _UserDefined.Value = ;

            return _UserDefined;

        }

        private Underwriter CreateUnderwriter()
        {
            Underwriter _Underwriter = new Underwriter();


            // _Underwriter.ContactDetail = CreateUnderwriterContactDetail();
            return _Underwriter;

        }

        private UnderwriterContactDetail CreateUnderwriterContactDetail()
        {
            UnderwriterContactDetail _UnderwriterContactDetail = new UnderwriterContactDetail();

            // _UnderwriterContactDetail.Name = ;
            // _UnderwriterContactDetail.UnderwriterComments = ;

            return _UnderwriterContactDetail;

        }

        private CxStateSpecificInfo CreateCxStateSpecificInfo()
        {
            CxStateSpecificInfo _CxStateSpecificInfo = new CxStateSpecificInfo();

            // _CxStateSpecificInfo.SubjectToKansasUniformCommercialCreditCode = ;
            // _CxStateSpecificInfo.DerivationClauseDocumentType = ;
            // _CxStateSpecificInfo.NonPara27PurchaseMoneyIndicator = ;
            // _CxStateSpecificInfo.DerivationRecordingInfoType = ;
            // _CxStateSpecificInfo.TexasHomeEquitySection50LoanIndicator = ;
            // _CxStateSpecificInfo.AttorneysFees = ;
            // _CxStateSpecificInfo.JudicialDistrict = ;
            // _CxStateSpecificInfo.TrusteeResidentOf = ;
            // _CxStateSpecificInfo.AttorneysClosingConditions = ;

            return _CxStateSpecificInfo;

        }

        private CxShipping CreateCxShipping()
        {
            CxShipping _CxShipping = new CxShipping();

            // _CxShipping.AbaRoutingNumber = ;
            // _CxShipping.AmccAccountNumber = ;

            return _CxShipping;

        }

        private CxRefinance CreateCxRefinance()
        {
            CxRefinance _CxRefinance = new CxRefinance();

            _CxRefinance.Cltv = m_dataLoan.sCltvR_rep;
            // _CxRefinance.ExistingHclIndicator = ;
            // _CxRefinance.MonthsSincePreviousRefinance = ;
            // _CxRefinance.RefinancePercentPenalty = ;

            return _CxRefinance;

        }

        private CxProcessing CreateCxProcessing()
        {
            CxProcessing _CxProcessing = new CxProcessing();

            // _CxProcessing.PrintToProcessingCenterIndicator = ;

            return _CxProcessing;

        }

        private CxEsignature CreateCxEsignature()
        {
            CxEsignature _CxEsignature = new CxEsignature();

            // _CxEsignature.ESignIndicator = ;

            return _CxEsignature;

        }

        private CxDocTrigger CreateCxDocTrigger()
        {
            CxDocTrigger _CxDocTrigger = new CxDocTrigger();

            // _CxDocTrigger._4506 = ;

            return _CxDocTrigger;

        }

        private CxCreditScore CreateCxCreditScore(E_CxCreditScoreCreditRepositorySourceType type, string score)
        {
            CxCreditScore _CxCreditScore = new CxCreditScore();
            _CxCreditScore.Value = score;
            _CxCreditScore.CreditRepositorySourceType = type;
            // _CxCreditScore.SequenceIdentifier = ;
            // _CxCreditScore.RangeMax = ;
            // _CxCreditScore.RangeMin = ;

            return _CxCreditScore;

        }

        private CxCompliance CreateCxCompliance()
        {
            CxCompliance _CxCompliance = new CxCompliance();

            // _CxCompliance.HclIndicator = ;
            // _CxCompliance.PredatoryLendingIndicator = ;
            // _CxCompliance.RateSpread = ;
            // _CxCompliance.Section32Indicator = ;

            return _CxCompliance;

        }

        private CxAgent CreateCxAgent(E_CxAgentType type, string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            CxAgent _CxAgent = new CxAgent();

            _CxAgent.Type = type;
            _CxAgent.UnparsedName = name;

            return _CxAgent;

        }

        private Austraila CreateAustraila()
        {
            Austraila _Austraila = new Austraila();


            // _Austraila.SecurityProperty = CreateSecurityProperty();
            return _Austraila;

        }

        private SecurityProperty CreateSecurityProperty()
        {
            SecurityProperty _SecurityProperty = new SecurityProperty();

            // _SecurityProperty.SequenceIdentifier = ;
            // _SecurityProperty.SecurityStreetName = ;

            return _SecurityProperty;

        }

        private CxBroker CreateCxBroker()
        {
            CxBroker _CxBroker = new CxBroker();

            // _CxBroker.FeePercent = ;
            // _CxBroker.LicenseNumber = ;
            // _CxBroker.MortgageBrokerServices = ;

            return _CxBroker;

        }

        private CxPreviousLoan CreateCxPreviousLoan()
        {
            CxPreviousLoan _CxPreviousLoan = new CxPreviousLoan();

            // _CxPreviousLoan.Amount = ;
            // _CxPreviousLoan.InterestRatePercent = ;
            // _CxPreviousLoan.PaymentAmount = ;

            return _CxPreviousLoan;

        }

        private CxConsolidation CreateCxConsolidation()
        {
            CxConsolidation _CxConsolidation = new CxConsolidation();


            // _CxConsolidation.CxMortgage = CreateCxMortgage();
            return _CxConsolidation;

        }

        private CxMortgage CreateCxMortgage()
        {
            CxMortgage _CxMortgage = new CxMortgage();

            // _CxMortgage.SequenceIdentifier = ;
            // _CxMortgage.MortgageGivenByName = ;
            // _CxMortgage.MortgageInFavorOf = ;
            // _CxMortgage.MortgageDate = ;
            // _CxMortgage.NoteDate = ;
            // _CxMortgage.OriginalPrincipalAmount = ;
            // _CxMortgage.UnpaidPrincipalAmount = ;
            // _CxMortgage.AssignedToName = ;
            // _CxMortgage.AssignedToDate = ;
            // _CxMortgage.AmountAdvanced = ;
            // _CxMortgage.GivenBy = ;

            // _CxMortgage.CxRecordingList.Add(CreateCxRecording());
            return _CxMortgage;

        }

        private CxRecording CreateCxRecording()
        {
            CxRecording _CxRecording = new CxRecording();

            // _CxRecording.Type = ;
            // _CxRecording.RecordedAtName = ;
            // _CxRecording.RecordingJurisdictionType = ;
            // _CxRecording.RecordedDate = ;
            // _CxRecording.RecordingJurisdictionName = ;
            // _CxRecording.MortgageState = ;
            // _CxRecording.AssignedToName = ;
            // _CxRecording.AssignedToDate = ;

            return _CxRecording;

        }

        private CxSeller CreateCxSeller()
        {
            CxSeller _CxSeller = new CxSeller();

            // _CxSeller.SequenceIdentifier = ;
            // _CxSeller.SocialSecurityNumber = ;
            // _CxSeller.Vesting = ;

            return _CxSeller;

        }

        private CxNote CreateCxNote()
        {
            CxNote _CxNote = new CxNote();

            // _CxNote.NoteEndorsement = ;

            return _CxNote;

        }

        private CxLender CreateCxLender()
        {
            CxLender _CxLender = new CxLender();

            // _CxLender.NameExtention = ;
            // _CxLender.LenderIsA = ;
            // _CxLender.VaIdNumber = ;
            // _CxLender.UnparsedAddress = ;
            // _CxLender.UnparsedMailingAddress = ;
            // _CxLender.NameAndExtension = ;
            // _CxLender.LenderApprovedMortgageeNationalHousingActIndicator = ;
            // _CxLender.LenderIsANationalBankIndicator = ;
            // _CxLender.LenderVaOfficeNumber = ;
            // _CxLender.SearchCode = ;

            // _CxLender.CxCorporateOfficerList.Add(CreateCxCorporateOfficer());
            return _CxLender;

        }

        private CxCorporateOfficer CreateCxCorporateOfficer()
        {
            CxCorporateOfficer _CxCorporateOfficer = new CxCorporateOfficer();

            // _CxCorporateOfficer.SequenceIdentifier = ;
            // _CxCorporateOfficer.Title = ;
            // _CxCorporateOfficer.FirstName = ;
            // _CxCorporateOfficer.MiddleName = ;
            // _CxCorporateOfficer.LastName = ;

            return _CxCorporateOfficer;

        }

        private CxInvestor CreateCxInvestor()
        {
            CxInvestor _CxInvestor = new CxInvestor();

            // _CxInvestor.ExistsUnderTheLawsOfState = ;
            // _CxInvestor.Id = ;
            // _CxInvestor.IsTamiIndicator = ;

            return _CxInvestor;

        }

        private CxRespaFee CreateCxRespaFee()
        {
            CxRespaFee _CxRespaFee = new CxRespaFee();

            // _CxRespaFee.SpecifiedHudLineNumber = ;
            // _CxRespaFee.PremiumYears = ;
            // _CxRespaFee.PlusAmount = ;
            // _CxRespaFee.FeeDescription = ;

            return _CxRespaFee;

        }

        private CxHeloc CreateCxHeloc()
        {
            CxHeloc _CxHeloc = new CxHeloc();

            // _CxHeloc.Abstracting = ;
            // _CxHeloc.Administration = ;
            // _CxHeloc.AgreementAllowsSubAccount = ;
            // _CxHeloc.AmountPaidToCapRate = ;
            // _CxHeloc.AnnualCreditLine = ;
            // _CxHeloc.AnnualMaintenance = ;
            // _CxHeloc.AnnualMaintFee = ;
            // _CxHeloc.Application = ;
            // _CxHeloc.Appraisal = ;
            // _CxHeloc.AppraisalReview = ;
            // _CxHeloc.Attorney = ;
            // _CxHeloc.AutoCalculateDiscountPercentIndicator = ;
            // _CxHeloc.AutoPmtsAccountNumber = ;
            // _CxHeloc.AutoPmtsIncreaseDprAfterTermination = ;
            // _CxHeloc.BillingRightsNotice = ;
            // _CxHeloc.Broker = ;
            // _CxHeloc.BrokerOrigination = ;
            // _CxHeloc.Check = ;
            // _CxHeloc.CityCountyTaxStamps = ;
            // _CxHeloc.ClerkJudicialDistrict = ;
            // _CxHeloc.ClosingAgent = ;
            // _CxHeloc.CommissionToTrusteeAmount = ;
            // _CxHeloc.CommissionToTrusteePercent = ;
            // _CxHeloc.Courier = ;
            // _CxHeloc.CreditLimit = ;
            // _CxHeloc.CreditLine = ;
            // _CxHeloc.CreditReport = ;
            // _CxHeloc.CurrentIndexRate = ;
            // _CxHeloc.DailyBalMin = ;
            // _CxHeloc.DiscountedAprAtOpening = ;
            // _CxHeloc.DiscountedAprInEffectMonths = ;
            // _CxHeloc.DiscountedAprInEffectPeriods = ;
            // _CxHeloc.DiscountedDailyPeriodicRatePercentAtOpening = ;
            // _CxHeloc.DocIdNumber = ;
            // _CxHeloc.DocumentaryTaxOrleansParish = ;
            // _CxHeloc.Documentation = ;
            // _CxHeloc.DocumentPreparation = ;
            // _CxHeloc.DocumentPreparationandNotarization = ;
            // _CxHeloc.DrawPeriodType = ;
            // _CxHeloc.Escrow = ;
            // _CxHeloc.FloodCertification = ;
            // _CxHeloc.Funding = ;
            // _CxHeloc.HazardInsurance = ;
            // _CxHeloc.HeImportantTermsPoints = ;
            // _CxHeloc.HomeEquityBrokerFees = ;
            // _CxHeloc.InitialPercentRate = ;
            // _CxHeloc.InitialPercentRateType = ;
            // _CxHeloc.Inspection = ;
            // _CxHeloc.InspectionAppraisalReview = ;
            // _CxHeloc.Intangible = ;
            // _CxHeloc.InvestorDocuments = ;
            // _CxHeloc.LargeQuarter = ;
            // _CxHeloc.LateFeeAmount = ;
            // _CxHeloc.LenderFees = ;
            // _CxHeloc.LoanDiscount = ;
            // _CxHeloc.LoanOriginationFeeAsAPercent = ;
            // _CxHeloc.MarginPercent = ;
            // _CxHeloc.MaxAttorneyHourlyRate = ;
            // _CxHeloc.MaxiumInterestPayment = ;
            // _CxHeloc.MinAttorneyHourlyRate = ;
            // _CxHeloc.Miscellaneous = ;
            // _CxHeloc.MunicipalLienCertificate = ;
            // _CxHeloc.Notary = ;
            // _CxHeloc.OpeningApr = ;
            // _CxHeloc.Origination = ;
            // _CxHeloc.OriginationDeferred = ;
            // _CxHeloc.OtherDocumentAddendum = ;
            // _CxHeloc.OtherDocumentAddendumDescription = ;
            // _CxHeloc.OvernightDelivery = ;
            // _CxHeloc.PaymentAmountCalculationType = ;
            // _CxHeloc.PaymentMethodForLineOfCredit = ;
            // _CxHeloc.Points = ;
            // _CxHeloc.Processing = ;
            // _CxHeloc.ProcessingFeePercent = ;
            // _CxHeloc.PropertyJudicialDistrict = ;
            // _CxHeloc.Reconveyance = ;
            // _CxHeloc.Recording = ;
            // _CxHeloc.RecordingAndTransfer = ;
            // _CxHeloc.RecordingTax = ;
            // _CxHeloc.RepaymentPeriodType = ;
            // _CxHeloc.ReturnPayment = ;
            // _CxHeloc.SettlementClosing = ;
            // _CxHeloc.SmallQuarter = ;
            // _CxHeloc.StampTax = ;
            // _CxHeloc.StopPayment = ;
            // _CxHeloc.SubAccountCreditLimit = ;
            // _CxHeloc.SubEscrow = ;
            // _CxHeloc.SubEscrowFullEscrow = ;
            // _CxHeloc.Survey = ;
            // _CxHeloc.Taxes = ;
            // _CxHeloc.TaxService = ;
            // _CxHeloc.ThirdPartyFeeMaximumAmount = ;
            // _CxHeloc.ThirdPartyFeeMinimumAmount = ;
            // _CxHeloc.TitleExamination = ;
            // _CxHeloc.TitleGuaranty = ;
            // _CxHeloc.TitleGuarantyInsurance = ;
            // _CxHeloc.TitleInsurance = ;
            // _CxHeloc.TitleInsuranceSearch = ;
            // _CxHeloc.TitleSearch = ;
            // _CxHeloc.TotalPoints = ;
            // _CxHeloc.Transfer = ;
            // _CxHeloc.TransferMunicipalLien = ;
            // _CxHeloc.Underwriting = ;
            // _CxHeloc.ProceedsArePurchaseMoneyCheck = ;
            // _CxHeloc.RealEstateDeedOfTrustcheckbox = ;
            // _CxHeloc.AmountFinanced = ;
            // _CxHeloc.AnnualPercentageRate = ;
            // _CxHeloc.DirectDeposit = ;
            // _CxHeloc.FinalPaymentAmount = ;
            // _CxHeloc.FinanceChargeAmount = ;
            // _CxHeloc.EstimatedInitialAmountOfPayment = ;
            // _CxHeloc.NewBeneficiaryName = ;
            // _CxHeloc.NewCreditLimit = ;
            // _CxHeloc.NewEstimatedClosingDate = ;
            // _CxHeloc.EstimatedNumberOfPayments = ;
            // _CxHeloc.OldBeneficiaryName = ;
            // _CxHeloc.TotalOfPayments = ;
            // _CxHeloc.IntroductoryRelationshipRewardsDiscount = ;
            // _CxHeloc.IntroductoryOpeningApr = ;
            // _CxHeloc.IntroductoryDirectDepositDiscount = ;

            // _CxHeloc.CxHelocFeeList.Add(CreateCxHelocFee());
            // _CxHeloc.CxHelocFeeSummary = CreateCxHelocFeeSummary();
            // _CxHeloc.CxImportantTermsList.Add(CreateCxImportantTerms());
            // _CxHeloc.FinanceCharge = CreateFinanceCharge();
            // _CxHeloc.PriorDeedOrMortgage = CreatePriorDeedOrMortgage();
            // _CxHeloc.PropertyLocation = CreatePropertyLocation();
            return _CxHeloc;

        }

        private PropertyLocation CreatePropertyLocation()
        {
            PropertyLocation _PropertyLocation = new PropertyLocation();

            // _PropertyLocation.Range = ;
            // _PropertyLocation.Section = ;
            // _PropertyLocation.Township = ;

            return _PropertyLocation;

        }

        private PriorDeedOrMortgage CreatePriorDeedOrMortgage()
        {
            PriorDeedOrMortgage _PriorDeedOrMortgage = new PriorDeedOrMortgage();

            // _PriorDeedOrMortgage.RecordedBook = ;
            // _PriorDeedOrMortgage.RecordedDate = ;
            // _PriorDeedOrMortgage.RecordedPage = ;
            // _PriorDeedOrMortgage.RecordingNumber = ;

            return _PriorDeedOrMortgage;

        }

        private FinanceCharge CreateFinanceCharge()
        {
            FinanceCharge _FinanceCharge = new FinanceCharge();

            // _FinanceCharge.BuyDownAmount = ;
            // _FinanceCharge.BuyDownRate = ;
            // _FinanceCharge.BuyDownReductionInDpr = ;
            // _FinanceCharge.MaxAprIfRateCapped = ;
            // _FinanceCharge.MinimumAprRate = ;
            // _FinanceCharge.RateCap = ;

            return _FinanceCharge;

        }

        private CxImportantTerms CreateCxImportantTerms()
        {
            CxImportantTerms _CxImportantTerms = new CxImportantTerms();

            // _CxImportantTerms.Name = ;
            // _CxImportantTerms.Amt = ;
            // _CxImportantTerms.Pct = ;

            return _CxImportantTerms;

        }

        private CxHelocFeeSummary CreateCxHelocFeeSummary()
        {
            CxHelocFeeSummary _CxHelocFeeSummary = new CxHelocFeeSummary();

            // _CxHelocFeeSummary.BorrowerOtherFeePaidAmount = ;
            // _CxHelocFeeSummary.BrokerFinanceChargesAmount = ;
            // _CxHelocFeeSummary.LenderOtherFeePaidAmount = ;

            return _CxHelocFeeSummary;

        }

        private CxHelocFee CreateCxHelocFee()
        {
            CxHelocFee _CxHelocFee = new CxHelocFee();

            // _CxHelocFee.Type = ;
            // _CxHelocFee.SequenceIdentifier = ;
            // _CxHelocFee.Description = ;
            // _CxHelocFee.Amount = ;

            return _CxHelocFee;

        }

        private CxTitle CreateCxTitle()
        {
            CxTitle _CxTitle = new CxTitle();

            // _CxTitle.ComprehensiveEndorsementIndicator = ;
            // _CxTitle.OtherEndorsementDescription = ;
            // _CxTitle.ThreeRandFiveEndorsementIndicator = ;
            // _CxTitle.SixArmEndorsementIndicator = ;
            // _CxTitle.FourCondoEndorsementIndicator = ;
            // _CxTitle.NegativeAmortizationEndorsementIndicator = ;
            // _CxTitle.PlannedUnitDevelopmentEndorsementIndicator = ;
            // _CxTitle.EightOneEnvironmentEndorsementIndicator = ;
            // _CxTitle.LocationEndorsementIndicator = ;
            // _CxTitle.OtherEndorsementIndicator = ;
            // _CxTitle.UnparsedAddress = ;

            return _CxTitle;

        }

        private CxProperty CreateCxProperty()
        {
            CxProperty _CxProperty = new CxProperty();

            // _CxProperty.PuDorCondoName = ;
            // _CxProperty.NewConstruction = ;
            // _CxProperty.AssessorsThirdParcelIdentifier = ;
            // _CxProperty.UnparsedAddress = ;
            // _CxProperty.ApartmentNumber = ;
            // _CxProperty.CoopCompanyName = ;
            // _CxProperty.CoopSharesOwned = ;

            // _CxProperty.CxBuilder = CreateCxBuilder();
            // _CxProperty.CxMobileHome = CreateCxMobileHome();
            // _CxProperty.CxInspector = CreateCxInspector();
            // _CxProperty.SurveyInfo = CreateSurveyInfo();
            return _CxProperty;

        }

        private SurveyInfo CreateSurveyInfo()
        {
            SurveyInfo _SurveyInfo = new SurveyInfo();

            // _SurveyInfo.SurveyCondition = ;
            // _SurveyInfo.SurveyDate = ;
            // _SurveyInfo.SurveyName = ;

            return _SurveyInfo;

        }

        private CxInspector CreateCxInspector()
        {
            CxInspector _CxInspector = new CxInspector();

            // _CxInspector.FaxNumber = ;
            // _CxInspector.Name = ;
            // _CxInspector.Phone = ;

            return _CxInspector;

        }

        private CxMobileHome CreateCxMobileHome()
        {
            CxMobileHome _CxMobileHome = new CxMobileHome();

            // _CxMobileHome.Make = ;
            // _CxMobileHome.Condition = ;
            // _CxMobileHome.SerialNumber = ;
            // _CxMobileHome.Year = ;
            // _CxMobileHome.HouseLength = ;
            // _CxMobileHome.HouseWidth = ;
            // _CxMobileHome.Description = ;
            // _CxMobileHome.HudPlateNumber = ;
            // _CxMobileHome.LandLotDescription = ;
            // _CxMobileHome.Model = ;

            return _CxMobileHome;

        }

        private CxBuilder CreateCxBuilder()
        {
            CxBuilder _CxBuilder = new CxBuilder();

            // _CxBuilder.Street = ;
            // _CxBuilder.City = ;
            // _CxBuilder.ContactName = ;
            // _CxBuilder.Name = ;
            // _CxBuilder.Phone = ;
            // _CxBuilder.State = ;
            // _CxBuilder.PostalCode = ;
            // _CxBuilder.ContactTitle = ;
            // _CxBuilder.ContactFax = ;

            return _CxBuilder;

        }

        private CxLoan CreateCxLoan()
        {
            CxLoan _CxLoan = new CxLoan();

            // _CxLoan.FinalBalloonPaymentAmount = ;
            // _CxLoan.MortgageInsuranceAnnualPremium = ;
            // _CxLoan.PrepaymentPenaltyAmount = ;
            // _CxLoan.FactaDate = ;
            _CxLoan.LockDate = m_dataLoan.sRLckdD_rep;
            // _CxLoan.DiscountPointsChargedPct = ;
            // _CxLoan.HardPrepaymentPenaltyMonths = ;
            // _CxLoan.MonthsAppliedToPrepaymentPenaltyFee = ;
            // _CxLoan.PaymentFirstRecastMonths = ;
            // _CxLoan.PaymentSubsequentRecastMonths = ;
            // _CxLoan.FinankcedPremiumPercent = ;
            // _CxLoan.PeriodicPaymentAdjustmentFloor = ;
            // _CxLoan.FailureToCloseReasons = ;
            // _CxLoan.FhaSuffix = ;
            // _CxLoan.LoanNumber = ;
            // _CxLoan.LoanOriginationatorName = ;
            // _CxLoan.ServicingLoanNumber = ;
            // _CxLoan.SubprimeIndicator = ;
            // _CxLoan.TilSecurityTypeOtherDescription = ;
            // _CxLoan.TimelyPaymentRewards = ;
            // _CxLoan.AmountFinancedItemizationIndicator = ;
            // _CxLoan.DocumentsToUse = ;
            // _CxLoan.CemaIndicator = ;
            // _CxLoan.Calculate901UsingMarginPlusIndexIndicator = ;
            // _CxLoan.BiweeklyConversionIndictor = ;
            // _CxLoan.LotIndicator = ;
            // _CxLoan.CreditLifeRequiredIndicator = ;
            // _CxLoan.InvestorArmDisclosureAvailabilityIndicator = ;
            // _CxLoan.LastPaymentChangeIsFullyAmortizedIndicator = ;
            // _CxLoan.Advantage90Indicator = ;
            // _CxLoan.FactaIndicator = ;
            // _CxLoan.AdjustableRatePeriodicCap = ;
            // _CxLoan.ArmIndex = ;
            // _CxLoan.HelocProgram = ;
            // _CxLoan.LifeFloorSetting = ;
            // _CxLoan.RehabilitationOrRenovationIndicator = ;
            // _CxLoan.LoanTableFundedInInvestorsNameIndicator = ;
            // _CxLoan.IncludeOddDaysTimeIndicator = ;
            // _CxLoan.CommunityParticipatesInNfipIndicator = ;
            // _CxLoan.PrepaymentMonthsOrPercentIndicator = ;
            // _CxLoan.TieredPrepaymentPenalty = ;

            // _CxLoan.CxBuydown = CreateCxBuydown();
            // _CxLoan.CxConstructionInfo = CreateCxConstructionInfo();
            // _CxLoan.CxDualAmortization = CreateCxDualAmortization();
            // _CxLoan.CxSecondMortgage = CreateCxSecondMortgage();
            // _CxLoan.CxPayment = CreateCxPayment();
            // _CxLoan.ClosingDocuments = CreateCxClosingDocuments();
            // _CxLoan.CxArmDisclosure = CreateCxArmDisclosure();
            _CxLoan.InitialDisclosures = CreateInitialDisclosures();
            return _CxLoan;

        }

        private InitialDisclosures CreateInitialDisclosures()
        {
            InitialDisclosures _InitialDisclosures = new InitialDisclosures();

            // _InitialDisclosures.ArmMaxPaymentAmount = ;
            // _InitialDisclosures.ArmTerms = ;
            // _InitialDisclosures.BasisOfRateChange = ;
            // _InitialDisclosures.CommitExpirationDate = ;
            // _InitialDisclosures.CommitExpirationNumberofDays = ;
            // _InitialDisclosures.CommittmentLetterWaiverReason = ;
            _InitialDisclosures.DebtToIncomeLevel = m_dataLoan.sQualBottomR_rep;
            // _InitialDisclosures.FailureToQualifyReasons = ;
            // _InitialDisclosures.FinalBalloonPaymentAmount = ;
            // _InitialDisclosures.FinalBalloonPaymentInterestPortion = ;
            // _InitialDisclosures.FinalBalloonPaymentPrincipalPortion = ;
            // _InitialDisclosures.IndexEstablishedOtherText = ;
            // _InitialDisclosures.IndexPublishOtherText = ;
            // _InitialDisclosures.InitialDisclosureFirstRateChangePaymentNumber = ;
            // _InitialDisclosures.LockInDays = ;
            // _InitialDisclosures.LockInFee = ;
            // _InitialDisclosures.LockInFeeRefundableConditions = ;
            // _InitialDisclosures.LockInOptions = ;
            // _InitialDisclosures.MaximumPoints = ;
            // _InitialDisclosures.MinimumPoints = ;
            // _InitialDisclosures.NumberofProcessingDays = ;
            // _InitialDisclosures.OvernightDocumentDeliveryServicesFee = ;
            // _InitialDisclosures.PreviousLoanDate = ;
            // _InitialDisclosures.PreviousLoanDebtToIncomeLevel = ;
            // _InitialDisclosures.PreviousLoanLoanToValue = ;
            // _InitialDisclosures.PreviousLoanTerm = ;
            // _InitialDisclosures.RegulatorAgencyCity = ;
            // _InitialDisclosures.RegulatorAgencyName = ;
            // _InitialDisclosures.RegulatorAgencyState = ;
            // _InitialDisclosures.RegulatorAgencyStreet = ;
            // _InitialDisclosures.RegulatorAgencyZip = ;
            // _InitialDisclosures.RequiredLockDaysPriortoSettlement = ;
            // _InitialDisclosures.UffiLevel = ;
            // _InitialDisclosures.PreviousLoanPurposeType = ;
            // _InitialDisclosures.OpenOrClosedEndSelection = ;
            // _InitialDisclosures.PreviousLoanPrePayPenaltyIndicator = ;
            // _InitialDisclosures.IndexPublishPeriod = ;
            // _InitialDisclosures.LockInFeeRefundableIndicator = ;
            // _InitialDisclosures.IndexEstablishedBy = ;
            // _InitialDisclosures.BalloonConditionalRighttoRefinanceIndicator = ;
            // _InitialDisclosures.FeeCreditLifeDisabilityPaidIndicator = ;
            // _InitialDisclosures.DisclosureStatementIndicator = ;
            // _InitialDisclosures.UffiIndicator = ;
            // _InitialDisclosures.LenderWillSetRateAt = ;
            // _InitialDisclosures.LenderWillSetRateIndicator = ;
            // _InitialDisclosures.RateCanChangeIndicator = ;

            // _InitialDisclosures.ConstructionLoan = CreateConstructionLoan();
            // _InitialDisclosures.RequiredServiceProviders = CreateRequiredServiceProviders();
            // _InitialDisclosures.TaxService = CreateTaxService();
            return _InitialDisclosures;

        }

        private TaxService CreateTaxService()
        {
            TaxService _TaxService = new TaxService();

            // _TaxService.TaxServiceVendorBankCode = ;
            // _TaxService.TaxServiceVendorCity = ;
            // _TaxService.TaxServiceVendorContactFirstName = ;
            // _TaxService.TaxServiceVendorContactLastName = ;
            // _TaxService.TaxServiceVendorContactMiddleName = ;
            // _TaxService.TaxServiceVendorContactSuffix = ;
            // _TaxService.TaxServiceVendorName = ;
            // _TaxService.TaxServiceVendorPhone = ;
            // _TaxService.TaxServiceVendorState = ;
            // _TaxService.TaxServiceVendorStreet = ;
            // _TaxService.TaxServiceVendorZip = ;

            return _TaxService;

        }

        private RequiredServiceProviders CreateRequiredServiceProviders()
        {
            RequiredServiceProviders _RequiredServiceProviders = new RequiredServiceProviders();

            // _RequiredServiceProviders.SequenceIdentifier = ;
            // _RequiredServiceProviders.City = ;
            // _RequiredServiceProviders.CompanyName = ;
            // _RequiredServiceProviders.Phone = ;
            // _RequiredServiceProviders.RelationshipToLender = ;
            // _RequiredServiceProviders.State = ;
            // _RequiredServiceProviders.Street = ;
            // _RequiredServiceProviders.Zip = ;

            return _RequiredServiceProviders;

        }

        private ConstructionLoan CreateConstructionLoan()
        {
            ConstructionLoan _ConstructionLoan = new ConstructionLoan();

            // _ConstructionLoan.ConstructionToPermFloorRate = ;
            // _ConstructionLoan.LoansInProcessHouseAccountAmount = ;
            // _ConstructionLoan.TotalConstructionCost = ;
            // _ConstructionLoan.PreliminaryDrawAmount = ;

            return _ConstructionLoan;

        }

        private CxArmDisclosure CreateCxArmDisclosure()
        {
            CxArmDisclosure _CxArmDisclosure = new CxArmDisclosure();

            // _CxArmDisclosure.ArmDisclosurePrintingPreferenceForClosingPackageType = ;
            // _CxArmDisclosure.ArmDisclosurePrintingPreferenceForInitialPackageType = ;
            // _CxArmDisclosure.EffectiveDate = ;
            // _CxArmDisclosure.InterestRate = ;
            // _CxArmDisclosure.LifeTimeCapPercent = ;

            return _CxArmDisclosure;

        }

        private CxClosingDocuments CreateCxClosingDocuments()
        {
            CxClosingDocuments _CxClosingDocuments = new CxClosingDocuments();

            // _CxClosingDocuments.DateSecurityInstrumentRecorded = ;

            // _CxClosingDocuments.HardshipLetter = CreateHardshipLetter();
            // _CxClosingDocuments.MortgageBroker = CreateCxMortgageBroker();
            // _CxClosingDocuments.Note = CreateNote();
            // _CxClosingDocuments.ReturnDocsForFinalProcessing = CreateReturnDocsForFinalProcessing();
            // _CxClosingDocuments.RightToCancel = CreateRightToCancel();
            return _CxClosingDocuments;

        }

        private RightToCancel CreateRightToCancel()
        {
            RightToCancel _RightToCancel = new RightToCancel();

            // _RightToCancel.DtcLoanIsOriginalLenderRefiIndicator = ;

            return _RightToCancel;

        }

        private ReturnDocsForFinalProcessing CreateReturnDocsForFinalProcessing()
        {
            ReturnDocsForFinalProcessing _ReturnDocsForFinalProcessing = new ReturnDocsForFinalProcessing();

            // _ReturnDocsForFinalProcessing.CompanyStreetAddress = ;
            // _ReturnDocsForFinalProcessing.CompanyContactName = ;
            // _ReturnDocsForFinalProcessing.CompanyCity = ;
            // _ReturnDocsForFinalProcessing.CompanyName = ;
            // _ReturnDocsForFinalProcessing.CompanyState = ;
            // _ReturnDocsForFinalProcessing.CompanyPostalCode = ;

            return _ReturnDocsForFinalProcessing;

        }

        private Note CreateNote()
        {
            Note _Note = new Note();

            // _Note.UseAlCautionNotice = ;

            return _Note;

        }

        private CxMortgageBroker CreateCxMortgageBroker()
        {
            CxMortgageBroker _CxMortgageBroker = new CxMortgageBroker();

            // _CxMortgageBroker.DtcType = ;

            return _CxMortgageBroker;

        }

        private HardshipLetter CreateHardshipLetter()
        {
            HardshipLetter _HardshipLetter = new HardshipLetter();

            // _HardshipLetter.IncludeHardshipIndicator = ;

            return _HardshipLetter;

        }

        private CxPayment CreateCxPayment()
        {
            CxPayment _CxPayment = new CxPayment();

            // _CxPayment.TotalAmountIncludingEscrow = ;

            return _CxPayment;

        }

        private CxSecondMortgage CreateCxSecondMortgage()
        {
            CxSecondMortgage _CxSecondMortgage = new CxSecondMortgage();

            // _CxSecondMortgage.InterestRate = ;
            // _CxSecondMortgage.LoanTerm = ;
            // _CxSecondMortgage.PaymentAmount = ;
            // _CxSecondMortgage.LienHolder = ;
            // _CxSecondMortgage.LienTrustee = ;

            return _CxSecondMortgage;

        }

        private CxDualAmortization CreateCxDualAmortization()
        {
            CxDualAmortization _CxDualAmortization = new CxDualAmortization();

            // _CxDualAmortization.DualAmortizationIndicator = ;
            // _CxDualAmortization.FullyAmortizeAfterMonths = ;
            // _CxDualAmortization.AmortizationTermMonths = ;

            return _CxDualAmortization;

        }

        private CxConstructionInfo CreateCxConstructionInfo()
        {
            CxConstructionInfo _CxConstructionInfo = new CxConstructionInfo();

            // _CxConstructionInfo.LoanMaturityDate = ;
            // _CxConstructionInfo.InterestRate = ;
            // _CxConstructionInfo.LoanTerm = ;
            // _CxConstructionInfo.FirstPaymentDate = ;

            return _CxConstructionInfo;

        }

        private CxBuydown CreateCxBuydown()
        {
            CxBuydown _CxBuydown = new CxBuydown();

            // _CxBuydown.DepositorName = ;
            // _CxBuydown.TotalBorrowerAmount = ;
            // _CxBuydown.TotalLenderAmount = ;
            // _CxBuydown.YearOneMonthlyAmount = ;
            // _CxBuydown.YearTwoMonthlyAmount = ;
            // _CxBuydown.YearThreeMonthlyAmount = ;
            // _CxBuydown.BuydownOptionType = ;

            // _CxBuydown.CxBuydownPeriodList.Add(CreateCxBuydownPeriod());
            return _CxBuydown;

        }

        private CxBuydownPeriod CreateCxBuydownPeriod()
        {
            CxBuydownPeriod _CxBuydownPeriod = new CxBuydownPeriod();

            // _CxBuydownPeriod.SequenceIdentifier = ;
            // _CxBuydownPeriod.NumberOfMonths = ;
            // _CxBuydownPeriod.Rate = ;

            return _CxBuydownPeriod;

        }

        private CxBorrower CreateCxBorrower(int sequenceId, CAppData dataApp)
        {
            CxBorrower _CxBorrower = new CxBorrower();

            // _CxBorrower.BorrowerType = ;
            _CxBorrower.SequenceIdentifier = sequenceId.ToString();
            // _CxBorrower.NonPurchasingSpouseIndicator = ;
            // _CxBorrower.UnparsedMailingAddress = ;
            // _CxBorrower.BorrowerAppraisalCopyIndicator = ;
            // _CxBorrower.BorrowerCompleteInsurerCity = ;
            // _CxBorrower.BorrowerCompleteInsurerName = ;
            // _CxBorrower.BorrowerCompleteInsurerPhone = ;
            // _CxBorrower.BorrowerCompleteInsurerState = ;
            // _CxBorrower.BorrowerCompleteInsurerStreet = ;
            // _CxBorrower.BorrowerCompleteInsurerZip = ;
            // _CxBorrower.BorrowerHasSpouseOnLoan = ;
            // _CxBorrower.BorrowerToObtainOwnersTitleInsIndicator = ;
            // _CxBorrower.ChooseInsurerAgentAttorneyIndicator = ;
            // _CxBorrower.FicoDate = ;
            // _CxBorrower.SpouseName = ;

            // _CxBorrower.CxTrust = CreateCxTrust();
            // _CxBorrower.CxNonPurchasingSpouse = CreateCxNonPurchasingSpouse();
            // _CxBorrower.CxVa = CreateCxVa();
            _CxBorrower.CxCreditScoreList.Add(CreateCxCreditScore(E_CxCreditScoreCreditRepositorySourceType.Equifax, dataApp.aEquifaxScore_rep));
            _CxBorrower.CxCreditScoreList.Add(CreateCxCreditScore(E_CxCreditScoreCreditRepositorySourceType.Experian, dataApp.aExperianScore_rep));
            _CxBorrower.CxCreditScoreList.Add(CreateCxCreditScore(E_CxCreditScoreCreditRepositorySourceType.TransUnion, dataApp.aTransUnionScore_rep));

            return _CxBorrower;

        }

        private CxVa CreateCxVa()
        {
            CxVa _CxVa = new CxVa();

            // _CxVa.LandAcquiredInSeparateTransactionDate = ;
            // _CxVa.AdditionalSecurityDescription = ;
            // _CxVa.ApproximateAnnualSpecialAssessmentPaymentAmount = ;
            // _CxVa.CompleteWhenAuthorizedByCertificateIndicator = ;
            // _CxVa.ContractPriceExceedsCrv = ;
            // _CxVa.InsuranceType = ;
            // _CxVa.AnnualMaintainanceAssessment = ;
            // _CxVa.NonRealtyAcquiredWithLoanProceedsDescription = ;
            // _CxVa.CertificationOfEligibilityIndicator = ;
            // _CxVa.VeteranIntendsToOccupyHomeIndicator = ;
            // _CxVa.VeteranOnActiveDutySpouseWillOccupyIndicator = ;
            // _CxVa.VeteranPreviouslyOccupiedPropertyIndicator = ;
            // _CxVa.VeteranOnActiveDutySpousePreviouslyOccupiedPropertyIndicator = ;
            // _CxVa.ProcedureType = ;
            // _CxVa.TotalUnpaidSpecailAssessments = ;
            // _CxVa.VestingType = ;
            // _CxVa.VestingTypeOtherDescription = ;
            // _CxVa.LoanProceedsWithholdingDepositedInAccountType = ;
            // _CxVa.LoanProceedsWithholdingAmount = ;
            // _CxVa.ApprovedUnderwriter = ;
            // _CxVa.BorrowerAwareContractPriceExceedsCrv = ;
            // _CxVa.EstateInPropertyOtherDescription = ;
            // _CxVa.LienType = ;
            // _CxVa.OtherLienTypeDescription = ;
            // _CxVa.PurchasePriceOfLandAcquiredInSeparate = ;
            // _CxVa.SocialSecurityNumber = ;
            // _CxVa.VeteranFirstName = ;
            // _CxVa.VeteranGender = ;
            // _CxVa.VeteranLastName = ;
            // _CxVa.VeteranMiddleName = ;
            // _CxVa.VetEthnicity = ;
            // _CxVa.CoborrowerEthnicity = ;

            // _CxVa.CxVaAgentList.Add(CreateCxVaAgent());
            // _CxVa.CxVaRelative = CreateCxVaRelative();
            return _CxVa;

        }

        private CxVaRelative CreateCxVaRelative()
        {
            CxVaRelative _CxVaRelative = new CxVaRelative();

            // _CxVaRelative.City = ;
            // _CxVaRelative.FirstName = ;
            // _CxVaRelative.LastName = ;
            // _CxVaRelative.MiddleName = ;
            // _CxVaRelative.Phone = ;
            // _CxVaRelative.State = ;
            // _CxVaRelative.Street = ;
            // _CxVaRelative.PostalCode = ;

            return _CxVaRelative;

        }

        private CxVaAgent CreateCxVaAgent()
        {
            CxVaAgent _CxVaAgent = new CxVaAgent();

            // _CxVaAgent.SequenceIdentifier = ;
            // _CxVaAgent.FullAddress = ;
            // _CxVaAgent.Function = ;
            // _CxVaAgent.Name = ;

            return _CxVaAgent;

        }

        private CxNonPurchasingSpouse CreateCxNonPurchasingSpouse()
        {
            CxNonPurchasingSpouse _CxNonPurchasingSpouse = new CxNonPurchasingSpouse();

            // _CxNonPurchasingSpouse.UnparsedName = ;
            // _CxNonPurchasingSpouse.FirstName = ;
            // _CxNonPurchasingSpouse.MiddleName = ;
            // _CxNonPurchasingSpouse.LastName = ;
            // _CxNonPurchasingSpouse.Suffix = ;

            return _CxNonPurchasingSpouse;

        }

        private CxTrust CreateCxTrust()
        {
            CxTrust _CxTrust = new CxTrust();

            // _CxTrust.BeneficiaryName = ;
            // _CxTrust.Date = ;
            // _CxTrust.Name = ;
            // _CxTrust.SignatureLineText = ;
            // _CxTrust.State = ;
            // _CxTrust.TrustEnabledIndicator = ;

            // _CxTrust.TrusteeList.Add(CreateCxTrustee());
            // _CxTrust.SettlorList.Add(CreateSettlor());
            return _CxTrust;

        }

        private Settlor CreateSettlor()
        {
            Settlor _Settlor = new Settlor();

            // _Settlor.SequenceIdentifier = ;
            // _Settlor.Name = ;

            return _Settlor;

        }

        private CxTrustee CreateCxTrustee()
        {
            CxTrustee _CxTrustee = new CxTrustee();

            // _CxTrustee.SequenceIdentifier = ;
            // _CxTrustee.Name = ;

            return _CxTrustee;

        }

        private CxAssignment CreateCxAssignment()
        {
            CxAssignment _CxAssignment = new CxAssignment();

            // _CxAssignment.Phone = ;
            // _CxAssignment.Date = ;
            // _CxAssignment.LoanNumber = ;
            // _CxAssignment.CorporateOfficer1 = ;
            // _CxAssignment.CorporateOfficer2 = ;

            // _CxAssignment.CxNotary = CreateCxNotary();
            return _CxAssignment;

        }

        private CxNotary CreateCxNotary()
        {
            CxNotary _CxNotary = new CxNotary();

            // _CxNotary.OfficerUnparsedName = ;
            // _CxNotary.OfficerTitle = ;
            // _CxNotary.RelationShip = ;
            // _CxNotary.FirstName = ;
            // _CxNotary.MiddleName = ;
            // _CxNotary.LastName = ;
            // _CxNotary.RelationShip2 = ;
            // _CxNotary.ResidingAt = ;

            return _CxNotary;

        }

        private CxEscrow CreateCxEscrow()
        {
            CxEscrow _CxEscrow = new CxEscrow();

            // _CxEscrow.ScheduleBItems = ;
            // _CxEscrow.FloodCommunityDescription = ;
            // _CxEscrow.TaxMessage = ;
            // _CxEscrow.TotalOfMonthlyAmounts = ;
            // _CxEscrow.FloodInsuranceRequired = ;
            // _CxEscrow.EscrowHoldbackAgent = ;
            // _CxEscrow.EscrowHoldbackFailurePercentage = ;

            // _CxEscrow.CxEscrowItemList.Add(CreateCxEscrowItem());
            // _CxEscrow.CxHoldbackItemList.Add(CreateCxHoldbackItem());
            return _CxEscrow;

        }

        private CxHoldbackItem CreateCxHoldbackItem()
        {
            CxHoldbackItem _CxHoldbackItem = new CxHoldbackItem();

            // _CxHoldbackItem.SequenceIdentifier = ;
            // _CxHoldbackItem.BorrowersCost = ;
            // _CxHoldbackItem.CompletionDate = ;
            // _CxHoldbackItem.ContractorName = ;
            // _CxHoldbackItem.TotalCost = ;
            // _CxHoldbackItem.Description = ;
            // _CxHoldbackItem.PercentOfTotalCost = ;
            // _CxHoldbackItem.SellersCost = ;

            return _CxHoldbackItem;

        }

        private CxEscrowItem CreateCxEscrowItem()
        {
            CxEscrowItem _CxEscrowItem = new CxEscrowItem();

            // _CxEscrowItem.ItemType = ;
            // _CxEscrowItem.PolicyCoverageAmount = ;
            // _CxEscrowItem.AmountLastPaid = ;
            // _CxEscrowItem.BuyerExpense = ;
            // _CxEscrowItem.DelinquentDate = ;
            // _CxEscrowItem.PeriodicPaymentType = ;
            // _CxEscrowItem.NonBuyerExpensePaidBy = ;
            // _CxEscrowItem.PolicyNumber = ;
            // _CxEscrowItem.PrepaidFinanceChargeIndicator = ;
            // _CxEscrowItem.NonBuyerExpense = ;
            // _CxEscrowItem.DateLastPaid = ;
            // _CxEscrowItem.Phone = ;
            // _CxEscrowItem.SellerPaysFromDate = ;
            // _CxEscrowItem.SellerPaysToDate = ;
            // _CxEscrowItem.FullAddress = ;
            // _CxEscrowItem.EffectiveDate = ;
            // _CxEscrowItem.DtcOtherEscrowGroupNumber = ;

            return _CxEscrowItem;

        }

        private AdverseAction CreateAdverseAction()
        {
            AdverseAction _AdverseAction = new AdverseAction();

            // _AdverseAction.CreditAgencyPhoneTollFree = ;
            // _AdverseAction.BasedOnAgencyIndicator = ;
            // _AdverseAction.BasedOnOtherIndicator = ;
            // _AdverseAction.MailedDate = ;
            // _AdverseAction.MailingstatusIndicator = ;

            // _AdverseAction.Credit = CreateCredit();
            // _AdverseAction.EmploymentStatus = CreateEmploymentStatus();
            // _AdverseAction.Income = CreateIncome();
            // _AdverseAction.Other = CreateOther();
            // _AdverseAction.Residency = CreateResidency();
            return _AdverseAction;

        }

        private Residency CreateResidency()
        {
            Residency _Residency = new Residency();

            // _Residency.LengthOfResidenceIndicator = ;
            // _Residency.TemporaryResidenceIndicator = ;
            // _Residency.UnableToVerifyResidenceIndicator = ;

            return _Residency;

        }

        private Other CreateOther()
        {
            Other _Other = new Other();

            // _Other.CreditApplicationIncompleteIndicator = ;
            // _Other.InsufficientFundsToCloseIndicator = ;
            // _Other.OtherDenialTermsText = ;
            // _Other.OtherTermsText = ;
            // _Other.UnacceptableAppraisalIndicator = ;
            // _Other.UnacceptableLeaseholdEstateIndicator = ;
            // _Other.UnacceptablePropertyIndicator = ;
            // _Other.CollateralNotSufficientIndicator = ;
            // _Other.NeverOnRequestedTermsIndicator = ;
            // _Other.OtherDenialTermsTextIndicator = ;

            return _Other;

        }

        private Income CreateIncome()
        {
            Income _Income = new Income();

            // _Income.InsufficientIncomeForCreditIndicator = ;
            // _Income.UnabletoVerifyIncomeIndicator = ;
            // _Income.ExcessiveObligationsToIncomeIndicator = ;

            return _Income;

        }

        private EmploymentStatus CreateEmploymentStatus()
        {
            EmploymentStatus _EmploymentStatus = new EmploymentStatus();

            // _EmploymentStatus.LengthOfEmploymentIndicator = ;
            // _EmploymentStatus.TempOrIrregularEmploymentIndicator = ;
            // _EmploymentStatus.UnableToVerifyEmploymentIndicator = ;
            // _EmploymentStatus.UnableToVerifyReferencesIndicatorIndicator = ;

            return _EmploymentStatus;

        }

        private Credit CreateCredit()
        {
            Credit _Credit = new Credit();

            // _Credit.BankruptcyInPastOrPresentIndicator = ;
            // _Credit.DelinquentWithOtherIndicator = ;
            // _Credit.ExcessiveObligationsIndicator = ;
            // _Credit.GarnishForecloseRepoJudgementIndicator = ;
            // _Credit.InsufficientCreditIndicator = ;
            // _Credit.InsufficientDataPropertyIndicator = ;
            // _Credit.InsufficientReferencesProvidedIndicator = ;
            // _Credit.LackOfCashReservesIndicator = ;
            // _Credit.LimitedCreditExperienceIndicator = ;
            // _Credit.NoCreditIndicator = ;
            // _Credit.PoorCreditWithUsIndicator = ;
            // _Credit.UnacceptableReferenceTypeIndicator = ;
            // _Credit.UnacceptablePaymentPreviousMortgageIndicator = ;
            // _Credit.CreditApplicationWithdrawnIndicator = ;

            return _Credit;

        }

        private UserData CreateUserData()
        {
            UserData _UserData = new UserData();


            // _UserData.DataList.Add(CreateData());
            return _UserData;

        }

        private Data CreateData()
        {
            Data _Data = new Data();

            // _Data.Id = ;
            // _Data.Value = ;

            return _Data;

        }

        private SmartDocumentDataMain CreateSmartDocumentDataMain()
        {
            SmartDocumentDataMain _SmartDocumentDataMain = new SmartDocumentDataMain();

            _SmartDocumentDataMain.Loan = CreateLoan();
            return _SmartDocumentDataMain;

        }

        private Loan CreateLoan()
        {
            Loan _Loan = new Loan();

            // _Loan.MismoVersionIdentifier = ;

            _Loan.Application = CreateApplication();
            _Loan.ClosingDocuments = CreateClosingDocuments();
            return _Loan;

        }

        private ClosingDocuments CreateClosingDocuments()
        {
            ClosingDocuments _ClosingDocuments = new ClosingDocuments();


            // _ClosingDocuments.AllongeToNote = CreateAllongeToNote();
            // _ClosingDocuments.Beneficiary = CreateBeneficiary();
            _ClosingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.ClosingAgent));
            _ClosingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.Escrow));
            _ClosingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.Title));
            _ClosingDocuments.ClosingAgentList.Add(CreateClosingAgent(E_AgentRoleT.LoanOfficer));

            _ClosingDocuments.ClosingInstructionsList.Add(CreateClosingInstructions());
            _ClosingDocuments.CompensationList.Add(CreateCompensation());
            // _ClosingDocuments.CosignerList.Add(CreateCosigner());
            _ClosingDocuments.EscrowAccountDetailList.Add(CreateEscrowAccountDetail());
            // _ClosingDocuments.Execution = CreateExecution();
            _ClosingDocuments.Investor = CreateInvestor();
            _ClosingDocuments.Lender = CreateLender();
            // _ClosingDocuments.LenderBranch = CreateLenderBranch();
            _ClosingDocuments.LoanDetails = CreateLoanDetails();
            // _ClosingDocuments.LossPayeeList.Add(CreateLossPayee());
            _ClosingDocuments.MortgageBroker = CreateMortgageBroker();
            // _ClosingDocuments.PaymentDetails = CreatePaymentDetails();
            // _ClosingDocuments.PayoffList.Add(CreatePayoff());
            _ClosingDocuments.RecordableDocumentList.Add(CreateRecordableDocument());
            // _ClosingDocuments.RespaHudDetailList.Add(CreateRespaHudDetail());
            // _ClosingDocuments.RespaServicingData = CreateRespaServicingData();
            // _ClosingDocuments.RespaSummary = CreateRespaSummary();
            // _ClosingDocuments.SellerList.Add(CreateSeller());
            // _ClosingDocuments.ServicerList.Add(CreateServicer());
            return _ClosingDocuments;

        }

        private Servicer CreateServicer()
        {
            Servicer _Servicer = new Servicer();

            // _Servicer.Name = ;
            // _Servicer.StreetAddress = ;
            // _Servicer.StreetAddress2 = ;
            // _Servicer.City = ;
            // _Servicer.State = ;
            // _Servicer.PostalCode = ;
            // _Servicer.Country = ;
            // _Servicer.DaysOfTheWeekDescription = ;
            // _Servicer.DirectInquiryToDescription = ;
            // _Servicer.HoursOfTheDayDescription = ;
            // _Servicer.InquiryTelephoneNumber = ;
            // _Servicer.PaymentAcceptanceEndDate = ;
            // _Servicer.PaymentAcceptanceStartDate = ;
            // _Servicer.TransferEffectiveDate = ;
            // _Servicer.Type = ;
            // _Servicer.TypeOtherDescription = ;
            // _Servicer.NonPersonEntityIndicator = ;

            // _Servicer.ContactDetail = CreateContactDetail();
            // _Servicer.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            // _Servicer.QualifiedWrittenRequestMailTo = CreateQualifiedWrittenRequestMailTo();
            return _Servicer;

        }

        private QualifiedWrittenRequestMailTo CreateQualifiedWrittenRequestMailTo()
        {
            QualifiedWrittenRequestMailTo _QualifiedWrittenRequestMailTo = new QualifiedWrittenRequestMailTo();

            // _QualifiedWrittenRequestMailTo.StreetAddress = ;
            // _QualifiedWrittenRequestMailTo.StreetAddress2 = ;
            // _QualifiedWrittenRequestMailTo.City = ;
            // _QualifiedWrittenRequestMailTo.State = ;
            // _QualifiedWrittenRequestMailTo.PostalCode = ;
            // _QualifiedWrittenRequestMailTo.Country = ;

            return _QualifiedWrittenRequestMailTo;

        }

        private NonPersonEntityDetail CreateNonPersonEntityDetail()
        {
            NonPersonEntityDetail _NonPersonEntityDetail = new NonPersonEntityDetail();

            // _NonPersonEntityDetail.OrganizationType = ;
            // _NonPersonEntityDetail.OrganizationTypeOtherDescription = ;
            // _NonPersonEntityDetail.OrganizedUnderTheLawsOfJurisdictionName = ;
            // _NonPersonEntityDetail.SuccessorClauseTextDescription = ;
            // _NonPersonEntityDetail.TaxIdentificationNumberIdentifier = ;
            // _NonPersonEntityDetail.OrganizationLicensingTypeDescription = ;
            // _NonPersonEntityDetail.MersOrganizationIdentifier = ;

            // _NonPersonEntityDetail.AuthorizedRepresentativeList.Add(CreateAuthorizedRepresentative());
            return _NonPersonEntityDetail;

        }

        private AuthorizedRepresentative CreateAuthorizedRepresentative()
        {
            AuthorizedRepresentative _AuthorizedRepresentative = new AuthorizedRepresentative();

            // _AuthorizedRepresentative.UnparsedName = ;
            // _AuthorizedRepresentative.TitleDescription = ;
            // _AuthorizedRepresentative.AuthorizedToSignIndicator = ;

            // _AuthorizedRepresentative.ContactDetail = CreateContactDetail();
            return _AuthorizedRepresentative;

        }

        private ContactDetail CreateContactDetail(string name, string phone, string fax, string email)
        {
            ContactDetail _ContactDetail = new ContactDetail();

            _ContactDetail.Name = name;
            // _ContactDetail.DtcTitle = ;

            _ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, E_ContactPointType.Phone, phone));
            _ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, E_ContactPointType.Fax, fax));
            _ContactDetail.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, E_ContactPointType.Email, email));

            return _ContactDetail;

        }

        private ContactPoint CreateContactPoint(E_ContactPointRoleType roleType, E_ContactPointType type, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            ContactPoint _ContactPoint = new ContactPoint();

            // _ContactPoint.PreferenceIndicator = ;
            _ContactPoint.RoleType = roleType;
            _ContactPoint.Type = type;
            // _ContactPoint.TypeOtherDescription = ;
            _ContactPoint.Value = value;

            return _ContactPoint;

        }

        private Seller CreateSeller()
        {
            Seller _Seller = new Seller();

            // _Seller.FirstName = ;
            // _Seller.MiddleName = ;
            // _Seller.LastName = ;
            // _Seller.NameSuffix = ;
            // _Seller.StreetAddress = ;
            // _Seller.StreetAddress2 = ;
            // _Seller.City = ;
            // _Seller.State = ;
            // _Seller.PostalCode = ;
            // _Seller.Country = ;
            // _Seller.SequenceIdentifier = ;
            // _Seller.NonPersonEntityIndicator = ;
            // _Seller.UnparsedName = ;

            // _Seller.ContactDetail = CreateContactDetail();
            // _Seller.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _Seller;

        }

        private RespaSummary CreateRespaSummary()
        {
            RespaSummary _RespaSummary = new RespaSummary();

            // _RespaSummary.DisclosedTotalSalesPriceAmount = ;
            // _RespaSummary.TotalAmountFinancedAmount = ;
            // _RespaSummary.TotalAprFeesAmount = ;
            // _RespaSummary.TotalNonAprFeesAmount = ;
            // _RespaSummary.TotalPaidToOthersAmount = ;
            // _RespaSummary.TotalDepositedReservesAmount = ;
            // _RespaSummary.TotalFinanceChargeAmount = ;
            // _RespaSummary.TotalNetBorrowerFeesAmount = ;
            // _RespaSummary.TotalNetProceedsForFundingAmount = ;
            // _RespaSummary.TotalNetSellerFeesAmount = ;
            // _RespaSummary.TotalOfAllPaymentsAmount = ;
            // _RespaSummary.TotalPrepaidFinanceChargeAmount = ;
            // _RespaSummary.Apr = ;
            // _RespaSummary.TotalFilingRecordingFeeAmount = ;

            // _RespaSummary.TotalFeesPaidToList.Add(CreateTotalFeesPaidTo());
            // _RespaSummary.TotalFeesPaidByList.Add(CreateTotalFeesPaidBy());
            return _RespaSummary;

        }

        private TotalFeesPaidBy CreateTotalFeesPaidBy()
        {
            TotalFeesPaidBy _TotalFeesPaidBy = new TotalFeesPaidBy();

            // _TotalFeesPaidBy.Type = ;
            // _TotalFeesPaidBy.TypeOtherDescription = ;
            // _TotalFeesPaidBy.TypeAmount = ;

            return _TotalFeesPaidBy;

        }

        private TotalFeesPaidTo CreateTotalFeesPaidTo()
        {
            TotalFeesPaidTo _TotalFeesPaidTo = new TotalFeesPaidTo();

            // _TotalFeesPaidTo.Type = ;
            // _TotalFeesPaidTo.TypeOtherDescription = ;
            // _TotalFeesPaidTo.TypeAmount = ;

            return _TotalFeesPaidTo;

        }

        private RespaServicingData CreateRespaServicingData()
        {
            RespaServicingData _RespaServicingData = new RespaServicingData();

            // _RespaServicingData.AreAbleToServiceIndicator = ;
            // _RespaServicingData.AssignSellOrTransferSomeServicingIndicator = ;
            // _RespaServicingData.DoNotServiceIndicator = ;
            // _RespaServicingData.ExpectToAssignSellOrTransferPercentOfServicingIndicator = ;
            // _RespaServicingData.ExpectToAssignSellOrTransferPercent = ;
            // _RespaServicingData.ExpectToRetainAllServicingIndicator = ;
            // _RespaServicingData.ExpectToSellAllServicingIndicator = ;
            // _RespaServicingData.FirstTransferYear = ;
            // _RespaServicingData.FirstTransferYearValue = ;
            // _RespaServicingData.HaveNotDecidedToServiceIndicator = ;
            // _RespaServicingData.HaveNotServicedInPastThreeYearsIndicator = ;
            // _RespaServicingData.HavePreviouslyAssignedServicingIndicator = ;
            // _RespaServicingData.MayAssignServicingIndicator = ;
            // _RespaServicingData.PresentlyIntendToAssignSellOrTransferServicingIndicator = ;
            // _RespaServicingData.SecondTransferYear = ;
            // _RespaServicingData.SecondTransferYearValue = ;
            // _RespaServicingData.ThirdTransferYear = ;
            // _RespaServicingData.ThirdTransferYearValue = ;
            // _RespaServicingData.ThisEstimateIncludesAssignmentsSalesOrTransfersIndicator = ;
            // _RespaServicingData.ThisInformationDoesIncludeAssignmentsSalesOrTransfersIndicator = ;
            // _RespaServicingData.ThisIsOurRecordOfTransferringServicingIndicator = ;
            // _RespaServicingData.TwelveMonthPeriodTransferPercent = ;
            // _RespaServicingData.WillNotServiceIndicator = ;
            // _RespaServicingData.WillServiceIndicator = ;

            return _RespaServicingData;

        }

        private RespaHudDetail CreateRespaHudDetail()
        {
            RespaHudDetail _RespaHudDetail = new RespaHudDetail();

            // _RespaHudDetail.SpecifiedHudLineNumber = ;
            // _RespaHudDetail.LineItemAmount = ;
            // _RespaHudDetail.LineItemDescription = ;
            // _RespaHudDetail.LineItemPaidByType = ;
            // _RespaHudDetail.Hud1SettlementAgentUnparsedName = ;
            // _RespaHudDetail.Hud1SettlementAgentUnparsedAddress = ;
            // _RespaHudDetail.Hud1SettlementDate = ;
            // _RespaHudDetail.Hud1LenderUnparsedName = ;
            // _RespaHudDetail.Hud1LenderUnparsedAddress = ;
            // _RespaHudDetail.Hud1CashToOrFromBorrowerIndicator = ;
            // _RespaHudDetail.Hud1CashToOrFromSellerIndicator = ;
            // _RespaHudDetail.Hud1ConventionalInsuredIndicator = ;
            // _RespaHudDetail.Hud1FileNumberIdentifier = ;
            // _RespaHudDetail.Hud1LineItemToDate = ;
            // _RespaHudDetail.Hud1LineItemFromDate = ;

            return _RespaHudDetail;

        }

        private RecordableDocument CreateRecordableDocument()
        {
            RecordableDocument _RecordableDocument = new RecordableDocument();

            // _RecordableDocument.Type = ;
            // _RecordableDocument.TypeOtherDescription = ;
            // _RecordableDocument.SequenceIdentifier = ;

            // _RecordableDocument.AssignFrom = CreateAssignFrom();
            // _RecordableDocument.AssignTo = CreateAssignTo();
            // _RecordableDocument.Default = CreateDefault();
            // _RecordableDocument.NotaryList.Add(CreateNotary());
            // _RecordableDocument.PreparedBy = CreatePreparedBy();
            // _RecordableDocument.AssociatedDocument = CreateAssociatedDocument();
            // _RecordableDocument.ReturnToList.Add(CreateReturnTo());
            // _RecordableDocument.Riders = CreateRiders();
            // _RecordableDocument.SecurityInstrument = CreateSecurityInstrument();
            _RecordableDocument.TrusteeList.Add(CreateTrustee());
            // _RecordableDocument.WitnessList.Add(CreateWitness());
            return _RecordableDocument;

        }

        private Witness CreateWitness()
        {
            Witness _Witness = new Witness();

            // _Witness.SequenceIdentifier = ;
            // _Witness.UnparsedName = ;

            return _Witness;

        }

        private Trustee CreateTrustee()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Trustee, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (string.IsNullOrEmpty(agent.CompanyName))
            {
                return null;
            }
            Trustee _Trustee = new Trustee();

            _Trustee.UnparsedName = agent.CompanyName;
            _Trustee.StreetAddress = agent.StreetAddr;
            // _Trustee.StreetAddress2 = ;
            _Trustee.City = agent.City;
            _Trustee.State = agent.State;
            _Trustee.PostalCode = agent.Zip;
            // _Trustee.Country = ;
            // _Trustee.County = ;
            // _Trustee.Type = ;
            // _Trustee.TypeOtherDescription = ;
            _Trustee.NonPersonEntityIndicator = E_YesNoIndicator.Y;

            // _Trustee.ContactDetail = CreateContactDetail();
            // _Trustee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _Trustee;

        }

        private SecurityInstrument CreateSecurityInstrument()
        {
            SecurityInstrument _SecurityInstrument = new SecurityInstrument();

            // _SecurityInstrument.AssumptionFeeAmount = ;
            // _SecurityInstrument.AttorneyFeeMinimumAmount = ;
            // _SecurityInstrument.AttorneyFeePercent = ;
            // _SecurityInstrument.MaximumPrincipalIndebtednessAmount = ;
            // _SecurityInstrument.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = ;
            // _SecurityInstrument.NoteHolderName = ;
            // _SecurityInstrument.OtherFeesAmount = ;
            // _SecurityInstrument.OtherFeesDescription = ;
            // _SecurityInstrument.OweltyOfPartitionIndicator = ;
            // _SecurityInstrument.PropertyLongLegalPageNumberDescription = ;
            // _SecurityInstrument.PurchaseMoneyIndicator = ;
            // _SecurityInstrument.RealPropertyImprovedOrToBeImprovedIndicator = ;
            // _SecurityInstrument.RealPropertyImprovementsNotCoveredIndicator = ;
            // _SecurityInstrument.RecordingRequestedByName = ;
            // _SecurityInstrument.RenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator = ;
            // _SecurityInstrument.TaxSerialNumberIdentifier = ;
            // _SecurityInstrument.TrusteeFeePercent = ;
            // _SecurityInstrument.VendorsLienIndicator = ;
            // _SecurityInstrument.VendorsLienDescription = ;
            // _SecurityInstrument.VestingDescription = ;

            return _SecurityInstrument;

        }

        private Riders CreateRiders()
        {
            Riders _Riders = new Riders();

            // _Riders.AdjustableRateRiderIndicator = ;
            // _Riders.BalloonRiderIndicator = ;
            // _Riders.BiweeklyPaymentRiderIndicator = ;
            // _Riders.CondominiumRiderIndicator = ;
            // _Riders.GraduatedPaymentRiderIndicator = ;
            // _Riders.GrowingEquityRiderIndicator = ;
            // _Riders.NonOwnerOccupancyRiderIndicator = ;
            // _Riders.OneToFourFamilyRiderIndicator = ;
            // _Riders.OtherRiderDescription = ;
            // _Riders.OtherRiderIndicator = ;
            // _Riders.PlannedUnitDevelopmentRiderIndicator = ;
            // _Riders.RateImprovementRiderIndicator = ;
            // _Riders.RehabilitationLoanRiderIndicator = ;
            // _Riders.SecondHomeRiderIndicator = ;
            // _Riders.VaRiderIndicator = ;

            return _Riders;

        }

        private ReturnTo CreateReturnTo()
        {
            ReturnTo _ReturnTo = new ReturnTo();

            // _ReturnTo.UnparsedName = ;
            // _ReturnTo.TitleDescription = ;
            // _ReturnTo.StreetAddress = ;
            // _ReturnTo.StreetAddress2 = ;
            // _ReturnTo.City = ;
            // _ReturnTo.State = ;
            // _ReturnTo.PostalCode = ;
            // _ReturnTo.Country = ;
            // _ReturnTo.County = ;
            // _ReturnTo.ElectronicRoutingMethodType = ;
            // _ReturnTo.ElectronicRoutingAddress = ;
            // _ReturnTo.StateFipsCode = ;
            // _ReturnTo.CountyFipsCode = ;
            // _ReturnTo.CountryCode = ;
            // _ReturnTo.NonPersonEntityIndicator = ;

            // _ReturnTo.ContactDetail = CreateContactDetail();
            // _ReturnTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _ReturnTo;

        }

        private AssociatedDocument CreateAssociatedDocument()
        {
            AssociatedDocument _AssociatedDocument = new AssociatedDocument();

            // _AssociatedDocument.Type = ;
            // _AssociatedDocument.TypeOtherDescription = ;
            // _AssociatedDocument.BookNumber = ;
            // _AssociatedDocument.BookType = ;
            // _AssociatedDocument.BookTypeOtherDescription = ;
            // _AssociatedDocument.Number = ;
            // _AssociatedDocument.TitleDescription = ;
            // _AssociatedDocument.InstrumentNumber = ;
            // _AssociatedDocument.PageNumber = ;
            // _AssociatedDocument.VolumeNumber = ;
            // _AssociatedDocument.RecordingDate = ;
            // _AssociatedDocument.RecordingJurisdictionName = ;
            // _AssociatedDocument.CountyOfRecordationName = ;
            // _AssociatedDocument.OfficeOfRecordationName = ;
            // _AssociatedDocument.StateOfRecordationName = ;

            return _AssociatedDocument;

        }

        private PreparedBy CreatePreparedBy()
        {
            PreparedBy _PreparedBy = new PreparedBy();

            // _PreparedBy.UnparsedName = ;
            // _PreparedBy.TitleDescription = ;
            // _PreparedBy.StreetAddress = ;
            // _PreparedBy.StreetAddress2 = ;
            // _PreparedBy.City = ;
            // _PreparedBy.State = ;
            // _PreparedBy.PostalCode = ;
            // _PreparedBy.Country = ;
            // _PreparedBy.County = ;
            // _PreparedBy.ElectronicRoutingMethodType = ;
            // _PreparedBy.ElectronicRoutingAddress = ;
            // _PreparedBy.TelephoneNumber = ;
            // _PreparedBy.StateFipsCode = ;
            // _PreparedBy.CountyFipsCode = ;
            // _PreparedBy.CountryCode = ;
            // _PreparedBy.NonPersonEntityIndicator = ;

            // _PreparedBy.ContactDetail = CreateContactDetail();
            return _PreparedBy;

        }

        private Notary CreateNotary()
        {
            Notary _Notary = new Notary();

            // _Notary.AppearanceDate = ;
            // _Notary.AppearedBeforeNamesDescription = ;
            // _Notary.AppearedBeforeTitlesDescription = ;
            // _Notary.City = ;
            // _Notary.CommissionBondNumberIdentifier = ;
            // _Notary.CommissionCity = ;
            // _Notary.CommissionCounty = ;
            // _Notary.CommissionExpirationDate = ;
            // _Notary.CommissionNumberIdentifier = ;
            // _Notary.CommissionState = ;
            // _Notary.County = ;
            // _Notary.PostalCode = ;
            // _Notary.State = ;
            // _Notary.StreetAddress = ;
            // _Notary.StreetAddress2 = ;
            // _Notary.TitleDescription = ;
            // _Notary.UnparsedName = ;

            // _Notary.CertificateList.Add(CreateCertificate());
            // _Notary.DataVersionList.Add(CreateDataVersion());
            return _Notary;

        }

        private DataVersion CreateDataVersion()
        {
            DataVersion _DataVersion = new DataVersion();

            // _DataVersion.Name = ;
            // _DataVersion.Number = ;

            return _DataVersion;

        }

        private Certificate CreateCertificate()
        {
            Certificate _Certificate = new Certificate();

            // _Certificate.SigningDate = ;
            // _Certificate.SigningCounty = ;
            // _Certificate.SigningState = ;
            // _Certificate.SignerFirstName = ;
            // _Certificate.SignerMiddleName = ;
            // _Certificate.SignerLastName = ;
            // _Certificate.SignerNameSuffix = ;
            // _Certificate.SignerUnparsedName = ;
            // _Certificate.SignerTitleDescription = ;
            // _Certificate.SignerCompanyName = ;

            // _Certificate.SignerIdentification = CreateSignerIdentification();
            return _Certificate;

        }

        private SignerIdentification CreateSignerIdentification()
        {
            SignerIdentification _SignerIdentification = new SignerIdentification();

            // _SignerIdentification.Type = ;
            // _SignerIdentification.Description = ;

            return _SignerIdentification;

        }

        private Default CreateDefault()
        {
            Default _Default = new Default();

            // _Default.AcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = ;
            // _Default.ApplicationFeesAmount = ;
            // _Default.ClosingPreparationFeesAmount = ;
            // _Default.LendingInstitutionPostOfficeBoxAddress = ;

            return _Default;

        }

        private AssignTo CreateAssignTo()
        {
            AssignTo _AssignTo = new AssignTo();

            // _AssignTo.UnparsedName = ;
            // _AssignTo.StreetAddress = ;
            // _AssignTo.StreetAddress2 = ;
            // _AssignTo.City = ;
            // _AssignTo.State = ;
            // _AssignTo.PostalCode = ;
            // _AssignTo.Country = ;
            // _AssignTo.County = ;
            // _AssignTo.CountyFipsCode = ;
            // _AssignTo.NonPersonEntityIndicator = ;

            // _AssignTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _AssignTo;

        }

        private AssignFrom CreateAssignFrom()
        {
            AssignFrom _AssignFrom = new AssignFrom();

            // _AssignFrom.UnparsedName = ;
            // _AssignFrom.StreetAddress = ;
            // _AssignFrom.StreetAddress2 = ;
            // _AssignFrom.City = ;
            // _AssignFrom.State = ;
            // _AssignFrom.PostalCode = ;
            // _AssignFrom.Country = ;
            // _AssignFrom.County = ;
            // _AssignFrom.CountyFipsCode = ;
            // _AssignFrom.SigningOfficialName = ;
            // _AssignFrom.SigningOfficialTitleDescription = ;
            // _AssignFrom.NonPersonEntityIndicator = ;

            // _AssignFrom.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _AssignFrom;

        }

        private Payoff CreatePayoff()
        {
            Payoff _Payoff = new Payoff();

            // _Payoff.AccountNumberIdentifier = ;
            // _Payoff.Amount = ;
            // _Payoff.PerDiemAmount = ;
            // _Payoff.SequenceIdentifier = ;
            // _Payoff.SpecifiedHudLineNumber = ;
            // _Payoff.ThroughDate = ;

            // _Payoff.Payee = CreatePayee();
            return _Payoff;

        }

        private Payee CreatePayee()
        {
            Payee _Payee = new Payee();

            // _Payee.UnparsedName = ;
            // _Payee.StreetAddress = ;
            // _Payee.StreetAddress2 = ;
            // _Payee.City = ;
            // _Payee.State = ;
            // _Payee.PostalCode = ;
            // _Payee.Country = ;
            // _Payee.County = ;
            // _Payee.NonPersonEntityIndicator = ;

            // _Payee.ContactDetail = CreateContactDetail();
            // _Payee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _Payee;

        }

        private PaymentDetails CreatePaymentDetails()
        {
            PaymentDetails _PaymentDetails = new PaymentDetails();


            // _PaymentDetails.AmortizationScheduleList.Add(CreateAmortizationSchedule());
            // _PaymentDetails.PaymentScheduleList.Add(CreatePaymentSchedule());
            return _PaymentDetails;

        }

        private PaymentSchedule CreatePaymentSchedule()
        {
            PaymentSchedule _PaymentSchedule = new PaymentSchedule();

            // _PaymentSchedule.PaymentSequenceIdentifier = ;
            // _PaymentSchedule.TotalNumberOfPaymentsCount = ;
            // _PaymentSchedule.PaymentAmount = ;
            // _PaymentSchedule.PaymentStartDate = ;
            // _PaymentSchedule.PaymentVaryingToAmount = ;

            return _PaymentSchedule;

        }

        private AmortizationSchedule CreateAmortizationSchedule()
        {
            AmortizationSchedule _AmortizationSchedule = new AmortizationSchedule();

            // _AmortizationSchedule.EndingBalanceAmount = ;
            // _AmortizationSchedule.InterestRatePercent = ;
            // _AmortizationSchedule.LoanToValuePercent = ;
            // _AmortizationSchedule.PaymentAmount = ;
            // _AmortizationSchedule.PaymentNumber = ;
            // _AmortizationSchedule.MiPaymentAmount = ;
            // _AmortizationSchedule.PortionOfPaymentDistributedToInterestAmount = ;
            // _AmortizationSchedule.PortionOfPaymentDistributedToPrincipalAmount = ;

            return _AmortizationSchedule;

        }

        private MortgageBroker CreateMortgageBroker()
        {
            MortgageBroker _MortgageBroker = new MortgageBroker();

            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);


            _MortgageBroker.UnparsedName = preparer.CompanyName;
            _MortgageBroker.StreetAddress = preparer.StreetAddr;
            // _MortgageBroker.StreetAddress2 = ;
            _MortgageBroker.City = preparer.City;
            _MortgageBroker.State = preparer.State;
            _MortgageBroker.PostalCode = preparer.Zip;
            // _MortgageBroker.Country = ;
            // _MortgageBroker.County = ;
            _MortgageBroker.LicenseNumberIdentifier = preparer.CompanyLoanOriginatorIdentifier;
            // _MortgageBroker.NonPersonEntityIndicator = ;

            // _MortgageBroker.ContactDetail = CreateContactDetail();
            // _MortgageBroker.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _MortgageBroker;

        }

        private LossPayee CreateLossPayee()
        {
            LossPayee _LossPayee = new LossPayee();

            // _LossPayee.UnparsedName = ;
            // _LossPayee.StreetAddress = ;
            // _LossPayee.StreetAddress2 = ;
            // _LossPayee.City = ;
            // _LossPayee.State = ;
            // _LossPayee.PostalCode = ;
            // _LossPayee.Country = ;
            // _LossPayee.County = ;
            // _LossPayee.Type = ;
            // _LossPayee.TypeOtherDescription = ;
            // _LossPayee.NonPersonEntityIndicator = ;

            // _LossPayee.ContactDetail = CreateContactDetail();
            // _LossPayee.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _LossPayee;

        }

        private LoanDetails CreateLoanDetails()
        {
            LoanDetails _LoanDetails = new LoanDetails();

            // _LoanDetails.ClosingDate = ;

            _LoanDetails.DisbursementDate = m_use2015DataLayer ? m_dataLoan.sConsummationD_rep : m_gfea.sConsummationD_rep;
            // _LoanDetails.DocumentOrderClassificationType = ;
            _LoanDetails.DocumentPreparationDate = m_dataLoan.sDocsD_rep;
            // _LoanDetails.FundByDate = ;
            _LoanDetails.OriginalLtvRatioPercent = m_dataLoan.sLtvR_rep;
            _LoanDetails.LockExpirationDate = m_dataLoan.sRLckdExpiredD_rep;
            //_LoanDetails.RescissionDate = ;

            _LoanDetails.InterimInterest = CreateInterimInterest();
            // _LoanDetails.RequestToRescind = CreateRequestToRescind();
            _LoanDetails.GfeDetail = CreateGfeDetail();
            return _LoanDetails;

        }

        private GfeDetail CreateGfeDetail()
        {
            GfeDetail gfeDetail = new GfeDetail();
            gfeDetail.GfeInterestRateAvailableThroughDate = m_use2015DataLayer ? m_dataLoan.sGfeNoteIRAvailTillD_DateTime : m_gfea.sGfeNoteIRAvailTillD_DateTime;
            gfeDetail.GfeSettlementChargesAvailableThroughDate = m_use2015DataLayer ? m_dataLoan.sGfeEstScAvailTillD_DateTime : m_gfea.sGfeEstScAvailTillD_DateTime;
            gfeDetail.GfeRateLockPeriodDaysCount = m_use2015DataLayer ? m_dataLoan.sGfeRateLockPeriod_rep : m_gfea.sGfeRateLockPeriod_rep;
            gfeDetail.GfeRateLockMinimumDaysPriorToSettlementCount = m_use2015DataLayer ? m_dataLoan.sGfeLockPeriodBeforeSettlement_rep : m_gfea.sGfeLockPeriodBeforeSettlement_rep;
            gfeDetail.GfeCreditOrChargeForChosenInterestRateType = ToMismo(m_use2015DataLayer ? m_dataLoan.sGfeCreditChargeT : m_gfea.sGfeCreditChargeT);

            return gfeDetail;
        }

        private E_GfeDetailGfeCreditOrChargeForChosenInterestRateType ToMismo(E_sGfeCreditChargeT sGfeCreditChargeT)
        {
            switch (sGfeCreditChargeT)
            {
                case E_sGfeCreditChargeT.CreditChargeIncludeInOrigination:
                    return E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.CreditOrChargeIncludedInOriginationCharge;
                case E_sGfeCreditChargeT.ReceiveCredit:
                    return E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.BorrowerCredit;
                case E_sGfeCreditChargeT.PayCharge:
                    return E_GfeDetailGfeCreditOrChargeForChosenInterestRateType.BorrowerCharge;
                default:
                    throw new UnhandledEnumException(sGfeCreditChargeT);
            }
        }

        private RequestToRescind CreateRequestToRescind()
        {
            RequestToRescind _RequestToRescind = new RequestToRescind();

            // _RequestToRescind.TransactionDate = ;
            // _RequestToRescind.NotificationUnparsedName = ;
            // _RequestToRescind.NotificationStreetAddress = ;
            // _RequestToRescind.NotificationStreetAddress2 = ;
            // _RequestToRescind.NotificationCity = ;
            // _RequestToRescind.NotificationState = ;
            // _RequestToRescind.NotificationPostalCode = ;
            // _RequestToRescind.NotificationCountry = ;

            return _RequestToRescind;

        }

        private InterimInterest CreateInterimInterest()
        {
            InterimInterest _InterimInterest = new InterimInterest();

            // _InterimInterest.PaidFromDate = ;
            // _InterimInterest.PaidThroughDate = ;
            // _InterimInterest.PerDiemPaymentOptionType = ;

            string daysInYear = m_use2015DataLayer ? m_dataLoan.sDaysInYr_rep : m_gfea.sDaysInYr_rep;
            if (daysInYear == "365")
            {
                _InterimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._365;
            }
            else if (daysInYear == "360")
            {
                _InterimInterest.PerDiemCalculationMethodType = E_InterimInterestPerDiemCalculationMethodType._360;
            }

            if (m_use2015DataLayer)
            {
                BorrowerClosingCostFee interimInterestFee = (BorrowerClosingCostFee)m_dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sIPia, PrincipalFactory.CurrentPrincipal.BrokerId, false);
                if (interimInterestFee.IsValidFee())
                {
                    _InterimInterest.TotalPerDiemAmount = interimInterestFee.TotalAmount_rep;
                    _InterimInterest.PaidNumberOfDays = m_dataLoan.sIPiaDy_rep;
                    _InterimInterest.SinglePerDiemAmount = m_dataLoan.sIPerDay_rep;
                }
            }
            else
            {
                if (m_dataLoan.sSettlementChargesExportSource == E_SettlementChargesExportSource.GFE)
                {
                    _InterimInterest.PaidNumberOfDays = m_gfea.sIPiaDy_rep;
                    _InterimInterest.SinglePerDiemAmount = m_gfea.sIPerDay_rep;
                    _InterimInterest.TotalPerDiemAmount = m_gfea.sIPia_rep;
                    _InterimInterest.DisclosureType = E_InterimInterestDisclosureType.GFE;
                }
                else if (m_dataLoan.sSettlementChargesExportSource == E_SettlementChargesExportSource.SETTLEMENT)
                {
                    _InterimInterest.PaidNumberOfDays = m_dataLoan.sSettlementIPiaDy_rep;
                    _InterimInterest.DisclosureType = E_InterimInterestDisclosureType.HUD1;
                    _InterimInterest.TotalPerDiemAmount = m_dataLoan.sSettlementIPia_rep;
                }
                else
                {
                    throw new UnhandledEnumException(m_dataLoan.sSettlementChargesExportSource);
                }
            }

            return _InterimInterest;
        }

        private LenderBranch CreateLenderBranch()
        {
            LenderBranch _LenderBranch = new LenderBranch();

            // _LenderBranch.UnparsedName = ;
            // _LenderBranch.StreetAddress = ;
            // _LenderBranch.StreetAddress2 = ;
            // _LenderBranch.City = ;
            // _LenderBranch.State = ;
            // _LenderBranch.PostalCode = ;
            // _LenderBranch.Country = ;
            // _LenderBranch.County = ;

            // _LenderBranch.ContactDetail = CreateContactDetail();
            return _LenderBranch;

        }

        private Lender CreateLender()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            Lender _Lender = new Lender();


            _Lender.UnparsedName = agent.CompanyName;
            _Lender.StreetAddress = agent.StreetAddr;
            // _Lender.StreetAddress2 = ;
            _Lender.City = agent.City;
            _Lender.State = agent.State;
            _Lender.PostalCode = agent.Zip;
            // _Lender.Country = ;
            // _Lender.County = ;
            // _Lender.DocumentsOrderedByName = ;
            // _Lender.FunderName = ;
            // _Lender.NonPersonEntityIndicator = ;

            _Lender.ContactDetail = CreateContactDetail(m_dataLoan.sEmployeeLoanRepName, m_dataLoan.sEmployeeLoanRepPhone, m_dataLoan.sEmployeeLoanRepFax, m_dataLoan.sEmployeeLoanRepEmail);
            // _Lender.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _Lender;

        }

        private Investor CreateInvestor()
        {
            Investor _Investor = new Investor();

            _Investor.UnparsedName = m_dataLoan.sInvestorLockLpInvestorNm;
            // _Investor.StreetAddress = ;
            // _Investor.StreetAddress2 = ;
            // _Investor.City = ;
            // _Investor.State = ;
            // _Investor.PostalCode = ;
            // _Investor.Country = ;
            // _Investor.County = ;
            // _Investor.NonPersonEntityIndicator = ;

            // _Investor.ContactDetail = CreateContactDetail();
            // _Investor.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _Investor;

        }

        private Execution CreateExecution()
        {
            Execution _Execution = new Execution();

            // _Execution.City = ;
            // _Execution.State = ;
            // _Execution.County = ;
            // _Execution.Date = ;

            return _Execution;

        }

        private EscrowAccountDetail CreateEscrowAccountDetail()
        {
            EscrowAccountDetail _EscrowAccountDetail = new EscrowAccountDetail();

            AggregateEscrowAccount escrowAccount = m_dataLoan.sAggregateEscrowAccount;
            _EscrowAccountDetail.InitialBalanceAmount = escrowAccount.InitialDeposit_rep;
            // _EscrowAccountDetail.MinimumBalanceAmount = ;
            // _EscrowAccountDetail.TotalMonthlyPitiAmount = ;

            // _EscrowAccountDetail.EscrowAccountActivityList.Add(CreateEscrowAccountActivity());
            return _EscrowAccountDetail;

        }

        private EscrowAccountActivity CreateEscrowAccountActivity()
        {
            EscrowAccountActivity _EscrowAccountActivity = new EscrowAccountActivity();

            // _EscrowAccountActivity.CurrentBalanceAmount = ;
            // _EscrowAccountActivity.DisbursementMonth = ;
            // _EscrowAccountActivity.DisbursementSequenceIdentifier = ;
            // _EscrowAccountActivity.DisbursementYear = ;
            // _EscrowAccountActivity.PaymentDescriptionType = ;
            // _EscrowAccountActivity.PaymentFromEscrowAccountAmount = ;
            // _EscrowAccountActivity.PaymentToEscrowAccountAmount = ;

            return _EscrowAccountActivity;

        }

        private Cosigner CreateCosigner()
        {
            Cosigner _Cosigner = new Cosigner();

            // _Cosigner.TitleDescription = ;
            // _Cosigner.UnparsedName = ;

            return _Cosigner;

        }

        private Compensation CreateCompensation()
        {
            Compensation _Compensation = new Compensation();

            if (m_use2015DataLayer)
            {
                BorrowerClosingCostFee compensation = (BorrowerClosingCostFee)m_dataLoan.sClosingCostSet.GetFirstFeeByLegacyType(E_LegacyGfeFieldT.sGfeOriginatorCompF, PrincipalFactory.CurrentPrincipal.BrokerId, false);
                _Compensation.Amount = compensation.TotalAmount_rep;
            }
            else
            {
                _Compensation.Amount = m_gfea.sGfeOriginatorCompF_rep;
            }

            //_Compensation.PaidByType;
            _Compensation.PaidToType = E_CompensationPaidToType.Broker;
            //_Compensation.Percent;
            _Compensation.Type =  E_CompensationType.BrokerCompensation;
            // _Compensation.TypeOtherDescription = ;

            return _Compensation;

        }

        private ClosingInstructions CreateClosingInstructions()
        {
            ClosingInstructions _ClosingInstructions = new ClosingInstructions();

            #region Conditions
            StringBuilder sb = new StringBuilder();
            Guid brokerId = ((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal).BrokerId;
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);

            if (brokerDB.IsUseNewTaskSystem)
            {
                List<Task> tasks = Task.GetActiveConditionsByLoanId(brokerId, m_dataLoan.sLId, false);
                foreach (Task task in tasks)
                {
                    if (task.TaskStatus == E_TaskStatus.Closed)
                    {
                        continue; //skip complete condition
                    }
                    sb.AppendLine(task.TaskSubject);
                }
            }
            else
            {

                LoanConditionSetObsolete ldSet = new LoanConditionSetObsolete();

                if (brokerDB.HasLenderDefaultFeatures)
                {
                    ldSet.Retrieve(m_dataLoan.sLId, false, false, false);

                    foreach (CLoanConditionObsolete condition in ldSet)
                    {
                        if (condition.CondStatus == E_CondStatus.Done)
                            continue; // Skip complete condition.

                        sb.AppendLine(condition.CondDesc);
                        //closingInstructions.ConditionList.Add(CreateCondition(condition.CondDesc));
                    }
                }
                else
                {
                    int count = m_dataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete condition = m_dataLoan.GetCondFieldsObsolete(i);
                        if (condition.IsDone)
                            continue;

                        sb.AppendLine(condition.CondDesc);
                        //closingInstructions.ConditionList.Add(CreateCondition(condition.CondDesc));
                    }
                }
            }

            #endregion
            _ClosingInstructions.ConsolidatedClosingConditionsDescription = sb.ToString();
            // _ClosingInstructions.SpecialFloodHazardAreaIndictor = ;
            // _ClosingInstructions.LeadBasedPaintCertificationRequiredIndicator = ;
            _ClosingInstructions.PropertyTaxMessageDescription = m_dataLoan.sPropertyTaxMessageDescription;
            _ClosingInstructions.PreliminaryTitleReportDate = m_dataLoan.sPrelimRprtRd_rep;
            // _ClosingInstructions.TitleReportItemsDescription = ;
            _ClosingInstructions.TitleReportRequiredEndorsementsDescription = m_dataLoan.sRequiredEndorsements;
            // _ClosingInstructions.TermiteReportRequiredIndicator = ;
            // _ClosingInstructions.HoursDocumentsNeededPriorToDisbursementCount = ;
            // _ClosingInstructions.FundingCutoffTime = ;
            // _ClosingInstructions.SpecialFloodHazardAreaIndicator = ;

            // _ClosingInstructions.ConditionList.Add(CreateCondition());
            return _ClosingInstructions;

        }

        private Condition CreateCondition()
        {
            Condition _Condition = new Condition();

            // _Condition.Description = ;
            // _Condition.MetIndicator = ;
            // _Condition.SequenceIdentifier = ;
            // _Condition.WaivedIndicator = ;

            return _Condition;

        }

        private ClosingAgent CreateClosingAgent(E_AgentRoleT agentRole)
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(agentRole, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return null;
            }
            ClosingAgent _ClosingAgent = new ClosingAgent();

            _ClosingAgent.UnparsedName = agent.CompanyName;
            _ClosingAgent.StreetAddress = agent.StreetAddr;
            // _ClosingAgent.StreetAddress2 = ;
            _ClosingAgent.City = agent.City;
            _ClosingAgent.State = agent.State;
            _ClosingAgent.PostalCode = agent.Zip;
            // _ClosingAgent.Country = ;
            // _ClosingAgent.County = ;
            _ClosingAgent.OrderNumberIdentifier = agent.CaseNum;
            if (agentRole == E_AgentRoleT.LoanOfficer)
            {
                _ClosingAgent.Type = E_ClosingAgentType.Other;
                _ClosingAgent.TypeOtherDescription = "LenderLoanOfficer";
            }
            else
            {
                _ClosingAgent.Type = ToMismoClosingAgentType(agentRole);
            }
            // _ClosingAgent.TypeOtherDescription = ;
            // _ClosingAgent.NonPersonEntityIndicator = ;

            _ClosingAgent.ContactDetail = CreateContactDetail(agent.AgentName, agent.Phone, agent.FaxNum, agent.EmailAddr);
            // _ClosingAgent.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _ClosingAgent;

        }
        private E_ClosingAgentType ToMismoClosingAgentType(E_AgentRoleT agentRole)
        {
            switch (agentRole)
            {
                case E_AgentRoleT.ClosingAgent: return E_ClosingAgentType.ClosingAgent;
                case E_AgentRoleT.Escrow: return E_ClosingAgentType.EscrowCompany;
                case E_AgentRoleT.Title: return E_ClosingAgentType.TitleCompany;
                default:
                    throw new UnhandledEnumException(agentRole);

            }
        }

        private Beneficiary CreateBeneficiary()
        {
            Beneficiary _Beneficiary = new Beneficiary();

            // _Beneficiary.UnparsedName = ;
            // _Beneficiary.StreetAddress = ;
            // _Beneficiary.StreetAddress2 = ;
            // _Beneficiary.City = ;
            // _Beneficiary.State = ;
            // _Beneficiary.PostalCode = ;
            // _Beneficiary.Country = ;
            // _Beneficiary.County = ;
            // _Beneficiary.NonPersonEntityIndicator = ;

            // _Beneficiary.ContactDetail = CreateContactDetail();
            // _Beneficiary.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _Beneficiary;

        }

        private AllongeToNote CreateAllongeToNote()
        {
            AllongeToNote _AllongeToNote = new AllongeToNote();

            // _AllongeToNote.Date = ;
            // _AllongeToNote.ExecutedByDescription = ;
            // _AllongeToNote.InFavorOfDescription = ;
            // _AllongeToNote.PayToTheOrderOfDescription = ;
            // _AllongeToNote.WithoutRecourseDescription = ;

            // _AllongeToNote.AuthorizedRepresentative = CreateAuthorizedRepresentative();
            return _AllongeToNote;

        }

        private Application CreateApplication()
        {
            Application _Application = new Application();

            _Application.LoanPurpose = CreateLoanPurpose();
            _Application.Property = CreateProperty();

            _Application.AdditionalCaseData = CreateAdditionalCaseData();

           
            _Application.EscrowAccountSummary = CreateEscrowAccountSummary();
            _Application.InterviewerInformation = CreateInterviewerInformation();
            _Application.LoanProductData = CreateLoanProductData();
            _Application.MortgageTerms = CreateMortgageTerms();
            _Application.LoanOriginatorList.Add(CreateLoanOriginator());
            // _Application.DataInformation = CreateDataInformation();
            // _Application.AffordableLending = CreateAffordableLending();
            // _Application.DownPaymentList.Add(CreateDownPayment());

            _Application.GovernmentLoan = CreateGovernmentLoan();
            _Application.Mers = CreateMers();

            // _Application.GovernmentReporting = CreateGovernmentReporting();
            // _Application.LoanQualification = CreateLoanQualification();
            _Application.MiData = CreateMiData();
            _Application.TransactionDetail = CreateTransactionDetail();

            int nApps = m_dataLoan.nApps;
            int borrowerSequenceId = 0;
            for (int nAppIndex = 0; nAppIndex < nApps; nAppIndex++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(nAppIndex);
                if (!dataApp.aBIsValidNameSsn && !dataApp.aCIsValidNameSsn)
                {
                    continue; // Skip if there is no borrower & no coborrower.
                }

                if (dataApp.aBIsValidNameSsn)
                {
                    borrowerSequenceId++;
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    _Application.BorrowerList.Add(CreateBorrower(borrowerSequenceId, dataApp));
                }

                _Application.TitleHolderList.Add(CreateTitleHolder(dataApp.aTitleNm1));
                _Application.TitleHolderList.Add(CreateTitleHolder(dataApp.aTitleNm2));

                #region Add Assets
                IAssetCollection assetCollection = dataApp.aAssetCollection;
                _Application.AssetList.Add(CreateAsset(assetCollection.GetBusinessWorth(false), "BOR" + borrowerSequenceId));
                _Application.AssetList.Add(CreateAsset(assetCollection.GetLifeInsurance(false), "BOR" + borrowerSequenceId));
                _Application.AssetList.Add(CreateAsset(assetCollection.GetRetirement(false), "BOR" + borrowerSequenceId));
                _Application.AssetList.Add(CreateAsset(assetCollection.GetCashDeposit1(false), "BOR" + borrowerSequenceId));
                _Application.AssetList.Add(CreateAsset(assetCollection.GetCashDeposit2(false), "BOR" + borrowerSequenceId));
                for (int i = 0; i < assetCollection.CountRegular; i++)
                {
                    _Application.AssetList.Add(CreateAsset(assetCollection.GetRegularRecordAt(i), "BOR" + borrowerSequenceId));
                }
                #endregion

                #region Add Liabilities
                ILiaCollection liaCollection = dataApp.aLiaCollection;
                _Application.LiabilityList.Add(CreateLiability(liaCollection.GetAlimony(false), "BOR" + borrowerSequenceId));
                _Application.LiabilityList.Add(CreateLiability(liaCollection.GetChildSupport(false), "BOR" + borrowerSequenceId));
                _Application.LiabilityList.Add(CreateLiability(liaCollection.GetJobRelated1(false), "BOR" + borrowerSequenceId));
                _Application.LiabilityList.Add(CreateLiability(liaCollection.GetJobRelated2(false), "BOR" + borrowerSequenceId));
                for (int i = 0; i < liaCollection.CountRegular; i++)
                {
                    _Application.LiabilityList.Add(CreateLiability(liaCollection.GetRegularRecordAt(i), "BOR" + borrowerSequenceId));
                }
                #endregion

                #region Add REO Properties
                var reCollection = dataApp.aReCollection;
                for (int i = 0; i < reCollection.CountRegular; i++)
                {
                    _Application.ReoPropertyList.Add(CreateReoProperty(reCollection.GetRegularRecordAt(i), dataApp, "BOR" + borrowerSequenceId));
                }
                #endregion

                if (dataApp.aCIsValidNameSsn)
                {
                    borrowerSequenceId++;
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    _Application.BorrowerList.Add(CreateBorrower(borrowerSequenceId, dataApp));
                }
            }

            if (m_use2015DataLayer)
            {
                PopulateEscrowList_2015DataLayer(_Application.EscrowList);
                PopulateRespaFeeList_2015DataLayer(_Application.RespaFeeList);
                PopulateProposedHousingExpenseList_2015DataLayer(_Application.ProposedHousingExpenseList);
            }
            else
            {
                PopulateEscrowList(_Application.EscrowList);
                PopulateRespaFeeList(_Application.RespaFeeList);
                PopulateProposedHousingExpenseList(_Application.ProposedHousingExpenseList);
            }

            return _Application;
        }

        /// <summary>
        /// Populates the Escrow list with GFE or TRID data using the 2015 data layer.
        /// </summary>
        /// <param name="escrowList">The escrow list to be populated.</param>
        private void PopulateEscrowList_2015DataLayer(List<Escrow> escrowList)
        {
            foreach (var expense in m_dataLoan.sHousingExpenses.ExpensesToUse)
            {
                if (expense.IsEscrowedAtClosing.Equals(E_TriState.Yes))
                {
                    escrowList.Add(CreateEscrow_2015DataLayer(expense, m_dataLoan.sClosingCostSet));
                }
            }
        }

        /// <summary>
        /// Creates an Escrow with GFE or TRID data using the 2015 data layer.
        /// </summary>
        /// <param name="expense">The expense information.</param>
		/// <param name="closingCostSet">The closing cost set with live data.</param>
        /// <returns>An <see cref="Escrow" /> object populated with data.</returns>
        private Escrow CreateEscrow_2015DataLayer(BaseHousingExpense expense, BorrowerClosingCostSet closingCostSet)
        {
            Escrow escrow = new Escrow();

            escrow.AnnualPaymentAmount = expense.AnnualAmt_rep;
            escrow.CollectedNumberOfMonthsCount = expense.ReserveMonths_rep;

            // 8/24/2015 bs - Case 222479 Get the correct HUDLine from fee type
            Guid feeTypeId = ClosingCostSetUtils.Get1000FeeTypeIdFromAssignedHousingExpense(expense);
            BorrowerClosingCostFee fee = (BorrowerClosingCostFee)closingCostSet.FindFeeByTypeId(feeTypeId);
            if (expense.HousingExpenseType != E_HousingExpenseTypeT.Unassigned)
            {
                escrow.SpecifiedHud1LineNumber = fee.HudLine.ToString();
            }
            else
            {
                escrow.SpecifiedHud1LineNumber = expense.LineNumAsString;
            }

            escrow.ItemType = ToMismo(expense.HousingExpenseType);
            escrow.AccountSummary = CreateAccountSummary(expense.PrepaidMonths);
            
            if (escrow.ItemType == E_EscrowItemType.Other)
            {
                escrow.ItemTypeOtherDescription = expense.ExpenseDescription;
            }

            BorrowerClosingCostFee prepaidFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionF);
            if (prepaidFee != null)
            {
                escrow.PremiumAmount = expense.PrepaidAmt_rep;
                escrow.PremiumDurationMonthsCount = expense.PrepaidMonths_rep;

                var firstPayment = prepaidFee.Payments.FirstOrDefault();
                if (firstPayment != null)
                {
                    escrow.PremiumPaidByType = ToMismo_2015DataLayerEscrowPremiumPaidBy(firstPayment.PaidByT);
                }
            }

            escrow.MonthlyPaymentAmount = expense.MonthlyAmtTotal_rep;
            escrow.CollectedNumberOfMonthsCount = expense.ReserveMonths_rep;
            escrow.AnnualPaymentAmount = expense.AnnualAmt_rep;

            BorrowerClosingCostFee reserveFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionG);
            if (reserveFee != null)
            {
                var firstPayment = reserveFee.Payments.FirstOrDefault();
                if (firstPayment != null)
                {
                    escrow.PaidByType = ToMismo_2015DataLayerEscrowPaidBy(firstPayment.PaidByT);
                }
            }

            int numberOfPayments = 1;
            if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                foreach (var disbursement in expense.ActualDisbursementsList)
                {
                    escrow.PaymentsList.Add(CreateEscrowPayments_2015DataLayer(disbursement, numberOfPayments++));
                }
            }
            else
            {
                foreach (var disbursement in expense.ProjectedDisbursements.OrderBy(o => o.DueDate))
                {
                    escrow.PaymentsList.Add(CreateEscrowPayments_2015DataLayer(disbursement, numberOfPayments++));
                }
            }

            return escrow;
        }

        /// <summary>
        /// Creates an escrow payment with GFE or TRID data using the 2015 data layer.
        /// </summary>
        /// <param name="disbursement">The payment information.</param>
        /// <returns>A <see cref="Payments" /> object populated with data.</returns>
        private Payments CreateEscrowPayments_2015DataLayer(SingleDisbursement disbursement, int sequenceIdentifier)
        {
            var payments = new Payments();
            payments.SequenceIdentifier = sequenceIdentifier.ToString();
            payments.DueDate = disbursement.DueDate_rep;
            payments.PaymentAmount = disbursement.DisbursementAmt_rep;

            return payments;
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid an escrow premium.
        /// </summary>
        /// <param name="paidBy">The entity who paid the escrow premium.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidByType"/> value.</returns>
        public E_EscrowPremiumPaidByType ToMismo_2015DataLayerEscrowPremiumPaidBy(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_EscrowPremiumPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_EscrowPremiumPaidByType.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_EscrowPremiumPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                case E_ClosingCostFeePaymentPaidByT.Other:
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return E_EscrowPremiumPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid an escrow reserve.
        /// </summary>
        /// <param name="paidBy">The entity who paid the escrow reserve.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidByType"/> value.</returns>
        public E_EscrowPaidByType ToMismo_2015DataLayerEscrowPaidBy(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_EscrowPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_EscrowPaidByType.LenderPremium;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_EscrowPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                case E_ClosingCostFeePaymentPaidByT.Other:
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return E_EscrowPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Retrieves the escrow type corresponding to a housing expense type.
        /// </summary>
        /// <param name="type">The housing expense type.</param>
        /// <returns>The corresponding <see cref="E_EscrowItemType"/> value.</returns>
        public E_EscrowItemType ToMismo(E_HousingExpenseTypeT type)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.HazardInsurance:
                    return E_EscrowItemType.HazardInsurance;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return E_EscrowItemType.FloodInsurance;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return E_EscrowItemType.WindstormInsurance;
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return E_EscrowItemType.SchoolPropertyTax;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                case E_HousingExpenseTypeT.GroundRent:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.Unassigned:
                    return E_EscrowItemType.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Retrieves the entity who paid an escrow fee.
        /// </summary>
        /// <param name="paidBy">The entity who paid an escrow fee.</param>
        /// <returns>The corresponding <see cref="E_EscrowPaidByType"/> value.</returns>
        public E_EscrowPaidByType ToMismo(E_DisbursementPaidByT? paidBy)
        {
            switch (paidBy)
            {
                case E_DisbursementPaidByT.Borrower:
                    return E_EscrowPaidByType.Buyer;
                case E_DisbursementPaidByT.Lender:
                    return E_EscrowPaidByType.LenderPremium;
                case E_DisbursementPaidByT.Seller:
                    return E_EscrowPaidByType.Seller;
                case E_DisbursementPaidByT.EscrowImpounds:
                case null:
                    return E_EscrowPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Populates the RESPA fee list with GFE or TRID data using the 2015 data layer.
        /// </summary>
        /// <param name="respaFeeList">The RESPA fee list to populate.</param>
        private void PopulateRespaFeeList_2015DataLayer(List<RespaFee> respaFeeList)
        {
            Func<BaseClosingCostFee, bool> borrowerSetFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(
                                                                        m_dataLoan.sClosingCostSet,
                                                                        false,
                                                                        m_dataLoan.sOriginatorCompensationPaymentSourceT,
                                                                        m_dataLoan.sDisclosureRegulationT,
                                                                        m_dataLoan.sGfeIsTPOTransaction);

            foreach (BorrowerClosingCostFee closingCost in m_dataLoan.sClosingCostSet.GetFees(borrowerSetFilter))
            {
                respaFeeList.Add(CreateRespaFee_2015DataLayer(closingCost));
            }
        }

        /// <summary>
        /// Creates a GFE or TRID fee using the 2015 data layer.
        /// </summary>
        /// <param name="closingCost">The fee data.</param>
        /// <returns>A populated <see cref="RespaFee"/> object.</returns>
        private RespaFee CreateRespaFee_2015DataLayer(BorrowerClosingCostFee closingCost)
        {
            var fee = new RespaFee();

            fee.SpecifiedHudLineNumber = closingCost.HudLine_rep;
            fee.Type = ToMismo(closingCost.MismoFeeT);
            fee.TypeOtherDescription = closingCost.Description;
            fee.PaidToType = ToMismo(closingCost.Beneficiary);
            fee.PaidTo = CreatePaidTo(closingCost.BeneficiaryDescription);
            fee.GfeAggregationType = ToMismo(closingCost.GfeSectionT);
            fee.GfeDisclosedAmount = closingCost.TotalAmount_rep;

            if (m_exportTridData)
            {
                fee.ResponsiblePartyType = ToMismo(closingCost.GfeResponsiblePartyT);
                fee.BorrowerChosenProviderIndicator = closingCost.DidShop ? E_YesNoIndicator.Y : E_YesNoIndicator.N;
            }

            foreach (BorrowerClosingCostFeePayment payment in closingCost.Payments)
            {
                fee.PaymentList.Add(CreateRespaFeePayment_2015DataLayer(payment));
            }

            return fee;
        }

        /// <summary>
        /// Creates a GFE or TRID fee payment using the 2015 data layer.
        /// </summary>
        /// <param name="payment">The fee payment data.</param>
        /// <returns></returns>
        private Payment CreateRespaFeePayment_2015DataLayer(BorrowerClosingCostFeePayment payment)
        {
            var respaPayment = new Payment();
            respaPayment.Amount = payment.Amount_rep;
            respaPayment.IncludedInAprIndicator = payment.IsIncludedInApr(m_dataLoan.GetLiveLoanVersion()) ? E_YesNoIndicator.Y : E_YesNoIndicator.N;
            respaPayment.PaidOutsideOfClosingIndicator = payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing ? E_YesNoIndicator.Y : E_YesNoIndicator.N;
            respaPayment.PaidByType = ToMismo(payment.PaidByT);
            
            return respaPayment;
        }

        /// <summary>
        /// Retrieves the RESPA fee type.
        /// </summary>
        /// <param name="feeType">The closing cost fee type.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeType"/> value.</returns>
        private E_RespaFeeType ToMismo(E_ClosingCostFeeMismoFeeT feeType)
        {
            switch (feeType)
            {
                case E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee:
                    return E_RespaFeeType._203KArchitecturalAndEngineeringFee;
                case E_ClosingCostFeeMismoFeeT._203KConsultantFee:
                    return E_RespaFeeType._203KConsultantFee;
                case E_ClosingCostFeeMismoFeeT._203KDiscountOnRepairs:
                    return E_RespaFeeType._203KDiscountOnRepairs;
                case E_ClosingCostFeeMismoFeeT._203KInspectionFee:
                    return E_RespaFeeType._203KInspectionFee;
                case E_ClosingCostFeeMismoFeeT._203KPermits:
                    return E_RespaFeeType._203KPermits;
                case E_ClosingCostFeeMismoFeeT._203KSupplementalOriginationFee:
                    return E_RespaFeeType._203KSupplementalOriginationFee;
                case E_ClosingCostFeeMismoFeeT._203KTitleUpdate:
                    return E_RespaFeeType._203KTitleUpdate;
                case E_ClosingCostFeeMismoFeeT.AbstractOrTitleSearchFee:
                    return E_RespaFeeType.AbstractOrTitleSearchFee;
                case E_ClosingCostFeeMismoFeeT.AmortizationFee:
                    return E_RespaFeeType.AmortizationFee;
                case E_ClosingCostFeeMismoFeeT.ApplicationFee:
                    return E_RespaFeeType.ApplicationFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFee:
                    return E_RespaFeeType.AppraisalFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentFee:
                    return E_RespaFeeType.AssignmentFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee:
                    return E_RespaFeeType.AssignmentRecordingFee;
                case E_ClosingCostFeeMismoFeeT.AssumptionFee:
                    return E_RespaFeeType.AssumptionFee;
                case E_ClosingCostFeeMismoFeeT.AttorneyFee:
                    return E_RespaFeeType.AttorneyFee;
                case E_ClosingCostFeeMismoFeeT.BondReviewFee:
                    return E_RespaFeeType.BondReviewFee;
                case E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CityDeedTaxStampFee:
                    return E_RespaFeeType.CityCountyDeedTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee:
                case E_ClosingCostFeeMismoFeeT.CityMortgageTaxStampFee:
                    return E_RespaFeeType.CityCountyMortgageTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.CLOAccessFee:
                    return E_RespaFeeType.CLOAccessFee;
                case E_ClosingCostFeeMismoFeeT.CommitmentFee:
                    return E_RespaFeeType.CommitmentFee;
                case E_ClosingCostFeeMismoFeeT.CopyFaxFee:
                    return E_RespaFeeType.CopyFaxFee;
                case E_ClosingCostFeeMismoFeeT.CourierFee:
                    return E_RespaFeeType.CourierFee;
                case E_ClosingCostFeeMismoFeeT.CreditReportFee:
                    return E_RespaFeeType.CreditReportFee;
                case E_ClosingCostFeeMismoFeeT.DeedRecordingFee:
                    return E_RespaFeeType.DeedRecordingFee;
                case E_ClosingCostFeeMismoFeeT.DocumentaryStampFee:
                    return E_RespaFeeType.DocumentaryStampFee;
                case E_ClosingCostFeeMismoFeeT.DocumentPreparationFee:
                    return E_RespaFeeType.DocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.EscrowWaiverFee:
                    return E_RespaFeeType.EscrowWaiverFee;
                case E_ClosingCostFeeMismoFeeT.FloodCertification:
                    return E_RespaFeeType.FloodCertification;
                case E_ClosingCostFeeMismoFeeT.GeneralCounselFee:
                    return E_RespaFeeType.GeneralCounselFee;
                case E_ClosingCostFeeMismoFeeT.InspectionFee:
                    return E_RespaFeeType.InspectionFee;
                case E_ClosingCostFeeMismoFeeT.LoanDiscountPoints:
                    return E_RespaFeeType.LoanDiscountPoints;
                case E_ClosingCostFeeMismoFeeT.LoanOriginationFee:
                    return E_RespaFeeType.LoanOriginationFee;
                case E_ClosingCostFeeMismoFeeT.ModificationFee:
                    return E_RespaFeeType.ModificationFee;
                case E_ClosingCostFeeMismoFeeT.MortgageBrokerFee:
                    return E_RespaFeeType.MortgageBrokerFee;
                case E_ClosingCostFeeMismoFeeT.MortgageRecordingFee:
                    return E_RespaFeeType.MortgageRecordingFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateFee:
                    return E_RespaFeeType.MunicipalLienCertificateFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee:
                    return E_RespaFeeType.MunicipalLienCertificateRecordingFee;
                case E_ClosingCostFeeMismoFeeT.NewLoanAdministrationFee:
                    return E_RespaFeeType.NewLoanAdministrationFee;
                case E_ClosingCostFeeMismoFeeT.NotaryFee:
                    return E_RespaFeeType.NotaryFee;
                case E_ClosingCostFeeMismoFeeT.PestInspectionFee:
                    return E_RespaFeeType.PestInspectionFee;
                case E_ClosingCostFeeMismoFeeT.ProcessingFee:
                    return E_RespaFeeType.ProcessingFee;
                case E_ClosingCostFeeMismoFeeT.RealEstateCommission:
                case E_ClosingCostFeeMismoFeeT.RealEstateCommissionSellersBroker:
                    return E_RespaFeeType.RealEstateCommission;
                case E_ClosingCostFeeMismoFeeT.RedrawFee:
                    return E_RespaFeeType.RedrawFee;
                case E_ClosingCostFeeMismoFeeT.ReinspectionFee:
                    return E_RespaFeeType.ReinspectionFee;
                case E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee:
                    return E_RespaFeeType.ReleaseRecordingFee;
                case E_ClosingCostFeeMismoFeeT.RuralHousingFee:
                    return E_RespaFeeType.RuralHousingFee;
                case E_ClosingCostFeeMismoFeeT.SettlementOrClosingFee:
                    return E_RespaFeeType.SettlementOrClosingFee;
                case E_ClosingCostFeeMismoFeeT.StateDeedTaxStampFee:
                    return E_RespaFeeType.StateDeedTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee:
                    return E_RespaFeeType.StateMortgageTaxStampFee;
                case E_ClosingCostFeeMismoFeeT.SurveyFee:
                    return E_RespaFeeType.SurveyFee;
                case E_ClosingCostFeeMismoFeeT.TaxRelatedServiceFee:
                    return E_RespaFeeType.TaxRelatedServiceFee;
                case E_ClosingCostFeeMismoFeeT.TitleExaminationFee:
                    return E_RespaFeeType.TitleExaminationFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceBinderFee:
                    return E_RespaFeeType.TitleInsuranceBinderFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceFee:
                case E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium:
                case E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium:
                    return E_RespaFeeType.TitleInsuranceFee;
                case E_ClosingCostFeeMismoFeeT.Undefined:
                    return E_RespaFeeType.Undefined;
                case E_ClosingCostFeeMismoFeeT.UnderwritingFee:
                    return E_RespaFeeType.UnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation:
                case E_ClosingCostFeeMismoFeeT.WireTransferFee:
                case E_ClosingCostFeeMismoFeeT.TitleEndorsementFee:
                case E_ClosingCostFeeMismoFeeT.SubordinationFee:
                case E_ClosingCostFeeMismoFeeT.SigningAgentFee:
                case E_ClosingCostFeeMismoFeeT.PayoffRequestFee:
                case E_ClosingCostFeeMismoFeeT.MERSRegistrationFee:
                case E_ClosingCostFeeMismoFeeT.EscrowServiceFee:
                case E_ClosingCostFeeMismoFeeT.ElectronicDocumentDeliveryFee:
                case E_ClosingCostFeeMismoFeeT.ClosingProtectionLetterFee:
                case E_ClosingCostFeeMismoFeeT.CertificationFee:
                case E_ClosingCostFeeMismoFeeT.BondFee:
                case E_ClosingCostFeeMismoFeeT.BankruptcyMonitoringFee:
                case E_ClosingCostFeeMismoFeeT.AppraisalFieldReviewFee:
                case E_ClosingCostFeeMismoFeeT.AppraisalDeskReviewFee:
                case E_ClosingCostFeeMismoFeeT.Other:
                // OPM 227381 - MISMO 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.AppraisalManagementCompanyFee:
                case E_ClosingCostFeeMismoFeeT.AsbestosInspectionFee:
                case E_ClosingCostFeeMismoFeeT.AutomatedUnderwritingFee:
                case E_ClosingCostFeeMismoFeeT.AVMFee:
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationDues:
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationDues:
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.CreditDisabilityInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditLifeInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditPropertyInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.CreditUnemploymentInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.DebtCancellationInsurancePremium:
                case E_ClosingCostFeeMismoFeeT.DeedPreparationFee:
                case E_ClosingCostFeeMismoFeeT.DisasterInspectionFee:
                case E_ClosingCostFeeMismoFeeT.DryWallInspectionFee:
                case E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee:
                case E_ClosingCostFeeMismoFeeT.EnvironmentalInspectionFee:
                case E_ClosingCostFeeMismoFeeT.FilingFee:
                case E_ClosingCostFeeMismoFeeT.FoundationInspectionFee:
                case E_ClosingCostFeeMismoFeeT.HeatingCoolingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.HighCostMortgageCounselingFee:
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationDues:
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationSpecialAssessment:
                case E_ClosingCostFeeMismoFeeT.HomeWarrantyFee:
                case E_ClosingCostFeeMismoFeeT.LeadInspectionFee:
                case E_ClosingCostFeeMismoFeeT.LendersAttorneyFee:
                case E_ClosingCostFeeMismoFeeT.LoanLevelPriceAdjustment:
                case E_ClosingCostFeeMismoFeeT.ManualUnderwritingFee:
                case E_ClosingCostFeeMismoFeeT.ManufacturedHousingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.MIUpfrontPremium:
                case E_ClosingCostFeeMismoFeeT.MoldInspectionFee:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeCountyOrParish:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeMunicipal:
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeState:
                case E_ClosingCostFeeMismoFeeT.PlumbingInspectionFee:
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyPreparationFee:
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyRecordingFee:
                case E_ClosingCostFeeMismoFeeT.PreclosingVerificationControlFee:
                case E_ClosingCostFeeMismoFeeT.PropertyInspectionWaiverFee:
                case E_ClosingCostFeeMismoFeeT.PropertyTaxStatusResearchFee:
                case E_ClosingCostFeeMismoFeeT.RadonInspectionFee:
                case E_ClosingCostFeeMismoFeeT.RateLockFee:
                case E_ClosingCostFeeMismoFeeT.ReconveyanceFee:
                case E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination:
                case E_ClosingCostFeeMismoFeeT.RecordingFeeTotal:
                case E_ClosingCostFeeMismoFeeT.RepairsFee:
                case E_ClosingCostFeeMismoFeeT.RoofInspectionFee:
                case E_ClosingCostFeeMismoFeeT.SepticInspectionFee:
                case E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee:
                case E_ClosingCostFeeMismoFeeT.StateTitleInsuranceFee:
                case E_ClosingCostFeeMismoFeeT.StructuralInspectionFee:
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownAdministrationFee:
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownPoints:
                case E_ClosingCostFeeMismoFeeT.TitleCertificationFee:
                case E_ClosingCostFeeMismoFeeT.TitleClosingFee:
                case E_ClosingCostFeeMismoFeeT.TitleDocumentPreparationFee:
                case E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee:
                case E_ClosingCostFeeMismoFeeT.TitleNotaryFee:
                case E_ClosingCostFeeMismoFeeT.TitleServicesSalesTax:
                case E_ClosingCostFeeMismoFeeT.TitleUnderwritingIssueResolutionFee:
                case E_ClosingCostFeeMismoFeeT.TransferTaxTotal:
                case E_ClosingCostFeeMismoFeeT.VerificationOfAssetsFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfEmploymentFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfIncomeFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfResidencyStatusFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxpayerIdentificationFee:
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxReturnFee:
                case E_ClosingCostFeeMismoFeeT.WaterTestingFee:
                case E_ClosingCostFeeMismoFeeT.WellInspectionFee:
                case E_ClosingCostFeeMismoFeeT.VAFundingFee:
                case E_ClosingCostFeeMismoFeeT.MIInitialPremium:
                case E_ClosingCostFeeMismoFeeT.ChosenInterestRateCreditOrChargeTotal:
                case E_ClosingCostFeeMismoFeeT.OurOriginationChargeTotal:
                case E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal:
                    return E_RespaFeeType.Other;
                default:
                    throw new UnhandledEnumException(feeType);
            }
        }

        /// <summary>
        /// Retrieves the entity who receives payment of a fee.
        /// </summary>
        /// <param name="role">The agent who receives payment for a fee.</param>
        /// <returns>The corresponding <see cref="E_RespaFeePaidToType"/> value.</returns>
        private E_RespaFeePaidToType ToMismo(E_AgentRoleT role)
        {
            var agent = m_dataLoan.GetAgentOfRole(role, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            if (Enum.Equals(role, E_AgentRoleT.Broker))
            {
                return E_RespaFeePaidToType.Broker;
            }
            else if (Enum.Equals(role, E_AgentRoleT.Investor))
            {
                return E_RespaFeePaidToType.Investor;
            }
            else if (Enum.Equals(role, E_AgentRoleT.Lender))
            {
                return E_RespaFeePaidToType.Lender;
            }
            else if (agent.IsLender)
            {
                return E_RespaFeePaidToType.Lender;
            }
            else if (agent.IsOriginator)
            {
                return E_RespaFeePaidToType.Broker;
            }
            else
            {
                return E_RespaFeePaidToType.Other;
            }
        }

        /// <summary>
        /// Retrieves the entity responsible for a fee.
        /// </summary>
        /// <param name="type">The entity responsible for a fee.</param>
        /// <returns>The corresponding <see cref="E_RespaFeeResponsiblePartyType"/> value.</returns>
        private E_RespaFeeResponsiblePartyType ToMismo(E_GfeResponsiblePartyT type)
        {
            switch (type)
            {
                case E_GfeResponsiblePartyT.Buyer:
                    return E_RespaFeeResponsiblePartyType.Buyer;
                case E_GfeResponsiblePartyT.Seller:
                    return E_RespaFeeResponsiblePartyType.Seller;
                case E_GfeResponsiblePartyT.Lender:
                case E_GfeResponsiblePartyT.Broker:
                case E_GfeResponsiblePartyT.LeaveBlank:
                    return E_RespaFeeResponsiblePartyType.Undefined;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Retrieves the entity who makes payments on a fee.
        /// </summary>
        /// <param name="paidBy">The entity who makes payments.</param>
        /// <returns>The corresponding <see cref="E_PaymentPaidByType"/> value.</returns>
        private E_PaymentPaidByType ToMismo(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return E_PaymentPaidByType.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return E_PaymentPaidByType.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return E_PaymentPaidByType.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return E_PaymentPaidByType.Seller;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return E_PaymentPaidByType.Undefined;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Populates the proposed housing expense list with 2015 data layer information.
        /// </summary>
        /// <param name="proposedHousingExpenseList">The list to be populated.</param>
        private void PopulateProposedHousingExpenseList_2015DataLayer(List<ProposedHousingExpense> proposedHousingExpenseList)
        {

            // 8/14/2015 bs - Add P&I Mortgage Payment and MI to proposed housing expense. OPM 219295 
            proposedHousingExpenseList.Add(CreateProposedHousingExpenseFromLoanData_2015DataLayer(m_dataLoan.sProFirstMPmt_rep, E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest));
            proposedHousingExpenseList.Add(CreateProposedHousingExpenseFromLoanData_2015DataLayer(m_dataLoan.sProMIns_rep, E_ProposedHousingExpenseHousingExpenseType.MI));

            foreach (var housingExpense in m_dataLoan.sHousingExpenses.AssignedExpenses)
            {
                proposedHousingExpenseList.Add(CreateProposedHousingExpense_2015DataLayer(housingExpense));
            }

            foreach (var housingExpense in m_dataLoan.sHousingExpenses.UnassignedExpenses)
            {
                proposedHousingExpenseList.Add(CreateProposedHousingExpense_2015DataLayer(housingExpense));
            }
        }

        /// <summary>
        /// Creates a proposed housing expense using 2015 data layer information.
        /// </summary>
        /// <param name="expense">The housing expense.</param>
        /// <returns>A populated <see cref="ProposedHousingExpense"/> object.</returns>
        private ProposedHousingExpense CreateProposedHousingExpense_2015DataLayer(BaseHousingExpense expense)
        {
            var proposedHousingExpense = new ProposedHousingExpense();
            proposedHousingExpense.PaymentAmount = expense.MonthlyAmtTotal_rep;
            proposedHousingExpense.HousingExpenseType = ToMismo_2015DataLayer(expense.HousingExpenseType);

            return proposedHousingExpense;
        }

        /// <summary>
        /// Creates a <see cref="ProposedHousingExpense" /> using loan data.
        /// </summary>
        /// <param name="expense">The housing expense data.</param>
        /// <returns>A <see cref="ProposedHousingExpense" /> object.</returns>
        private ProposedHousingExpense CreateProposedHousingExpenseFromLoanData_2015DataLayer(string monthlyPayment, E_ProposedHousingExpenseHousingExpenseType housingExpenseType)
        {
            var proposedHousingExpense = new ProposedHousingExpense();
            proposedHousingExpense.PaymentAmount = monthlyPayment;
            proposedHousingExpense.HousingExpenseType = housingExpenseType;

            return proposedHousingExpense;
        }

        /// <summary>
        /// Retrieves the proposed housing expense type.
        /// </summary>
        /// <param name="type">The housing expense type.</param>
        /// <returns>The corresponding <see cref="E_ProposedHousingExpenseHousingExpenseType"/> value.</returns>
        private E_ProposedHousingExpenseHousingExpenseType ToMismo_2015DataLayer(E_HousingExpenseTypeT type)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.GroundRent:
                    return E_ProposedHousingExpenseHousingExpenseType.GroundRent;
                case E_HousingExpenseTypeT.HazardInsurance:
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return E_ProposedHousingExpenseHousingExpenseType.HazardInsurance;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                    return E_ProposedHousingExpenseHousingExpenseType.RealEstateTax;
                case E_HousingExpenseTypeT.WindstormInsurance:
                case E_HousingExpenseTypeT.SchoolTaxes:
                case E_HousingExpenseTypeT.FloodInsurance:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                    return E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense;
                case E_HousingExpenseTypeT.Unassigned:
                    return E_ProposedHousingExpenseHousingExpenseType.Undefined;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        private void PopulateProposedHousingExpenseList(List<ProposedHousingExpense> proposedHousingExpenseList)
        {
            var presentHousingExpenseList = new[] {
                new { ExpenseType = E_ProposedHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, Amount=m_dataLoan.sProFirstMPmt_rep}
                , new { ExpenseType = E_ProposedHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, Amount = m_dataLoan.sProSecondMPmt_rep}
                , new { ExpenseType = E_ProposedHousingExpenseHousingExpenseType.HazardInsurance, Amount = m_gfea.sProHazIns_rep}
                , new { ExpenseType = E_ProposedHousingExpenseHousingExpenseType.RealEstateTax, Amount = m_dataLoan.sProRealETx_rep}
                , new { ExpenseType = E_ProposedHousingExpenseHousingExpenseType.MI, Amount = m_gfea.sProMIns_rep}
                , new { ExpenseType = E_ProposedHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, Amount=m_dataLoan.sProHoAssocDues_rep}
                , new { ExpenseType = E_ProposedHousingExpenseHousingExpenseType.OtherHousingExpense, Amount = m_dataLoan.sProOHExp_rep}
            };

            foreach (var expense in presentHousingExpenseList)
            {
                proposedHousingExpenseList.Add(CreateProposedHousingExpense(expense.ExpenseType, expense.Amount));
            }
        }

        private void PopulateEscrowList(List<Escrow> escrowList)
        {
            escrowList.Add(CreateEscrow("1002", E_EscrowItemType.HazardInsurance, "", m_dataLoan.sProHazIns, m_gfea.sHazInsRsrvMon_rep, m_gfea.sHazInsRsrvProps));
            escrowList.Add(CreateEscrow("1003", E_EscrowItemType.DTC_MortgageInsurance, "", m_dataLoan.sProMIns, m_gfea.sMInsRsrvMon_rep, m_gfea.sMInsRsrvProps));
            escrowList.Add(CreateEscrow("1004", E_EscrowItemType.CountyPropertyTax, "", m_dataLoan.sProRealETx, m_gfea.sRealETxRsrvMon_rep, m_gfea.sRealETxRsrvProps));
            escrowList.Add(CreateEscrow("1005", E_EscrowItemType.SchoolPropertyTax, "", m_dataLoan.sProSchoolTx, m_gfea.sSchoolTxRsrvMon_rep, m_gfea.sSchoolTxRsrvProps));
            escrowList.Add(CreateEscrow("1006", E_EscrowItemType.FloodInsurance, "", m_dataLoan.sProFloodIns, m_gfea.sFloodInsRsrvMon_rep, m_gfea.sFloodInsRsrvProps));
            escrowList.Add(CreateEscrow("1007", E_EscrowItemType.Other, m_gfea.s1006ProHExpDesc, m_dataLoan.s1006ProHExp, m_gfea.s1006RsrvMon_rep, m_gfea.s1006RsrvProps));
            escrowList.Add(CreateEscrow("1008", E_EscrowItemType.Other, m_gfea.s1007ProHExpDesc, m_dataLoan.s1007ProHExp, m_gfea.s1007RsrvMon_rep, m_gfea.s1007RsrvProps));
            escrowList.Add(CreateEscrow("1010", E_EscrowItemType.Other, m_gfea.sU3RsrvDesc, m_dataLoan.sProU3Rsrv, m_gfea.sU3RsrvMon_rep, m_gfea.sU3RsrvProps));
            escrowList.Add(CreateEscrow("1011", E_EscrowItemType.Other, m_gfea.sU4RsrvDesc, m_dataLoan.sProU4Rsrv, m_gfea.sU4RsrvMon_rep, m_gfea.sU4RsrvProps));
        }

        private void PopulateRespaFeeList(List<RespaFee> respaFeeList)
        {
            bool isSettlement = m_dataLoan.sSettlementChargesExportSource == E_SettlementChargesExportSource.SETTLEMENT;
            bool isDisclosedGfe = isSettlement && m_dataLoan.sIncludeGfeDataForDocMagicComparison;

            #region 800 Items
            respaFeeList.Add(CreateRespaFee("801", "LOAN ORIGINATION FEE",
                isSettlement ? m_dataLoan.sSettlementLOrigF_rep : m_gfea.sLOrigF_rep,
                isSettlement ? m_dataLoan.sSettlementLOrigFPc_rep : m_gfea.sLOrigFPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sLOrigFProps, m_dataLoan.sSettlementLOrigFProps) : m_gfea.sLOrigFProps,
                "",
                isDisclosedGfe ? m_gfea.sLOrigF_rep : "",
                E_GfeSectionT.B1
                ));

            respaFeeList.Add(CreateRespaFee("802", "DISCOUNT POINT",
                isSettlement ? m_dataLoan.sSettlementLDiscnt_rep : m_gfea.sLDiscnt_rep,
                isSettlement ? m_dataLoan.sSettlementLDiscntPc_rep : m_gfea.sLDiscntPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sGfeDiscountPointFProps, m_dataLoan.sSettlementLDiscntProps) : m_gfea.sGfeDiscountPointFProps, //isSettlement ? LosConvert.GfeItemProps_CopyAprFrom(m_dataLoan.sLDiscntProps, m_dataLoan.sSettlementLDiscntProps) : m_dataLoan.sLDiscntProps,
                "",
                isDisclosedGfe ? m_gfea.sLDiscnt_rep : "",
                E_GfeSectionT.B2
                ));
            respaFeeList.Add(CreateRespaFee("804", "APPRAISAL FEE",
                isSettlement ? m_dataLoan.sSettlementApprF_rep : m_gfea.sApprF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sApprFProps, m_dataLoan.sSettlementApprFProps) : m_gfea.sApprFProps,
                m_gfea.sApprFPaidTo,
                isDisclosedGfe ? m_gfea.sApprF_rep : "",
                E_GfeSectionT.B3
                ));
            respaFeeList.Add(CreateRespaFee("805", "CREDIT REPORT",
                isSettlement ? m_dataLoan.sSettlementCrF_rep : m_gfea.sCrF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sCrFProps, m_dataLoan.sSettlementCrFProps) : m_gfea.sCrFProps,
                m_gfea.sCrFPaidTo,
                isDisclosedGfe ? m_gfea.sCrF_rep : "",
                E_GfeSectionT.B3
                ));
            respaFeeList.Add(CreateRespaFee("806", "TAX SERVICE FEE",
                isSettlement ? m_dataLoan.sSettlementTxServF_rep : m_gfea.sTxServF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sTxServFProps, m_dataLoan.sSettlementTxServFProps) : m_gfea.sTxServFProps,
                m_gfea.sTxServFPaidTo,
                isDisclosedGfe ? m_gfea.sTxServF_rep : "",
                E_GfeSectionT.B3
                ));
            respaFeeList.Add(CreateRespaFee("807", "FLOOD CERTIFICATION",
                isSettlement ? m_dataLoan.sSettlementFloodCertificationF_rep : m_gfea.sFloodCertificationF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sFloodCertificationFProps, m_dataLoan.sSettlementFloodCertificationFProps) : m_gfea.sFloodCertificationFProps,
                m_gfea.sFloodCertificationFPaidTo,
                isDisclosedGfe ? m_gfea.sFloodCertificationF_rep : "",
                E_GfeSectionT.B3
                ));
            respaFeeList.Add(CreateRespaFee("808", "MORTGAGE BROKER FEE",
                isSettlement ? m_dataLoan.sSettlementMBrokF_rep : m_gfea.sMBrokF_rep,
                isSettlement ? m_dataLoan.sSettlementMBrokFPc_rep : m_gfea.sMBrokFPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sMBrokFProps, m_dataLoan.sSettlementMBrokFProps) : m_gfea.sMBrokFProps,
                "",
                isDisclosedGfe ? m_gfea.sMBrokF_rep : "",
                E_GfeSectionT.B1
                ));
            respaFeeList.Add(CreateRespaFee("809", "LENDER'S INSPECTION FEE",
                isSettlement ? m_dataLoan.sSettlementInspectF_rep : m_gfea.sInspectF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sInspectFProps, m_dataLoan.sSettlementInspectFProps) : m_gfea.sInspectFProps,
                m_gfea.sInspectFPaidTo,
                isDisclosedGfe ? m_gfea.sInspectF_rep : "",
                E_GfeSectionT.B1
                ));
            respaFeeList.Add(CreateRespaFee("810", "PROCESSING FEE",
                isSettlement ? m_dataLoan.sSettlementProcF_rep : m_gfea.sProcF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sProcFProps, m_dataLoan.sSettlementProcFProps) : m_gfea.sProcFProps,
                m_gfea.sProcFPaidTo,
                isDisclosedGfe ? m_gfea.sProcF_rep : "",
                E_GfeSectionT.B1
                ));
            respaFeeList.Add(CreateRespaFee("811", "UNDERWRITING FEE",
                isSettlement ? m_dataLoan.sSettlementUwF_rep : m_gfea.sUwF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sUwFProps, m_dataLoan.sSettlementUwFProps) : m_gfea.sUwFProps,
                m_gfea.sUwFPaidTo,
                isDisclosedGfe ? m_gfea.sUwF_rep : "",
                E_GfeSectionT.B1
                ));
            respaFeeList.Add(CreateRespaFee("812", "WIRE TRANSFER",
                isSettlement ? m_dataLoan.sSettlementWireF_rep : m_gfea.sWireF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sWireFProps, m_dataLoan.sSettlementWireFProps) : m_gfea.sWireFProps,
                m_gfea.sWireFPaidTo,
                isDisclosedGfe ? m_gfea.sWireF_rep : "",
                E_GfeSectionT.B1
                ));
            respaFeeList.Add(CreateRespaFee("813",
                isSettlement ? m_dataLoan.sSettlement800U1FDesc : m_gfea.s800U1FDesc,
                isSettlement ? m_dataLoan.sSettlement800U1F_rep : m_gfea.s800U1F_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U1FProps, m_dataLoan.sSettlement800U1FProps) : m_gfea.s800U1FProps,
                m_gfea.s800U1FPaidTo,
                isDisclosedGfe ? m_gfea.s800U1F_rep : "",
                isSettlement ? m_dataLoan.s800U1FSettlementSection : m_gfea.s800U1FGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("814",
                isSettlement ? m_dataLoan.sSettlement800U2FDesc : m_gfea.s800U2FDesc,
                isSettlement ? m_dataLoan.sSettlement800U2F_rep : m_gfea.s800U2F_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U2FProps, m_dataLoan.sSettlement800U2FProps) : m_gfea.s800U2FProps,
                m_gfea.s800U2FPaidTo,
                isDisclosedGfe ? m_gfea.s800U2F_rep : "",
                isSettlement ? m_dataLoan.s800U2FSettlementSection : m_gfea.s800U2FGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("815",
                isSettlement ? m_dataLoan.sSettlement800U3FDesc : m_gfea.s800U3FDesc,
                isSettlement ? m_dataLoan.sSettlement800U3F_rep : m_gfea.s800U3F_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U3FProps, m_dataLoan.sSettlement800U3FProps) : m_gfea.s800U3FProps,
                m_gfea.s800U3FPaidTo,
                isDisclosedGfe ? m_gfea.s800U3F_rep : "",
                isSettlement ? m_dataLoan.s800U3FSettlementSection : m_gfea.s800U3FGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("816",
                isSettlement ? m_dataLoan.sSettlement800U4FDesc : m_gfea.s800U4FDesc,
                isSettlement ? m_dataLoan.sSettlement800U4F_rep : m_gfea.s800U4F_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U4FProps, m_dataLoan.sSettlement800U4FProps) : m_gfea.s800U4FProps,
                m_gfea.s800U4FPaidTo,
                isDisclosedGfe ? m_gfea.s800U4F_rep : "",
                isSettlement ? m_dataLoan.s800U4FSettlementSection : m_gfea.s800U4FGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("817",
                isSettlement ? m_dataLoan.sSettlement800U5FDesc : m_gfea.s800U5FDesc,
                isSettlement ? m_dataLoan.sSettlement800U5F_rep : m_gfea.s800U5F_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s800U5FProps, m_dataLoan.sSettlement800U5FProps) : m_gfea.s800U5FProps,
                m_gfea.s800U5FPaidTo,
                isDisclosedGfe ? m_gfea.s800U5F_rep : "",
                isSettlement ? m_dataLoan.s800U5FSettlementSection : m_gfea.s800U5FGfeSection
                ));
            #endregion

            #region 900 Items
            //sHazInsPia
            respaFeeList.Add(CreateRespaFee("903", "HOMEOWNER'S INSURANCE",
                isSettlement ? m_dataLoan.sSettlementHazInsPia_rep : m_gfea.sHazInsPia_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sHazInsPiaProps, m_dataLoan.sSettlementHazInsPiaProps) : m_gfea.sHazInsPiaProps,
                "",
                isDisclosedGfe ? m_gfea.sHazInsPia_rep : "",
                E_GfeSectionT.B11
                ));

            respaFeeList.Add(CreateRespaFee("904",
                isSettlement ? m_dataLoan.sSettlement904PiaDesc : m_gfea.s904PiaDesc,
                isSettlement ? m_dataLoan.sSettlement904Pia_rep : m_gfea.s904Pia_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s904PiaProps, m_dataLoan.sSettlement904PiaProps) : m_gfea.s904PiaProps,
                "",
                isDisclosedGfe ? m_gfea.s904Pia_rep : "",
                isSettlement ? m_dataLoan.s904PiaSettlementSection : m_gfea.s904PiaGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("906",
                isSettlement ? m_dataLoan.sSettlement900U1PiaDesc : m_gfea.s900U1PiaDesc,
                isSettlement ? m_dataLoan.sSettlement900U1Pia_rep : m_gfea.s900U1Pia_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.s900U1PiaProps, m_dataLoan.sSettlement900U1PiaProps) : m_gfea.s900U1PiaProps,
                "",
                isDisclosedGfe ? m_gfea.s900U1Pia_rep : "",
                isSettlement ? m_dataLoan.s900U1PiaSettlementSection : m_gfea.s900U1PiaGfeSection
                ));
            #endregion

            #region 1100 Title Charges
            respaFeeList.Add(CreateRespaFee("1102", "CLOSING/ESCROW FEE",
                isSettlement ? m_dataLoan.sSettlementEscrowF_rep : m_gfea.sEscrowF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sEscrowFProps, m_dataLoan.sSettlementEscrowFProps) : m_gfea.sEscrowFProps,
                m_gfea.sEscrowFTable,
                isDisclosedGfe ? m_gfea.sEscrowF_rep : "",
                isSettlement ? m_dataLoan.sEscrowFSettlementSection : m_gfea.sEscrowFGfeSection
                    ));

            respaFeeList.Add(CreateRespaFee("1103", "OWNER'S TITLE INSURANCE",
                isSettlement ? m_dataLoan.sSettlementOwnerTitleInsF_rep : m_gfea.sOwnerTitleInsF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sOwnerTitleInsProps, m_dataLoan.sSettlementOwnerTitleInsFProps) : m_gfea.sOwnerTitleInsProps,
                m_gfea.sOwnerTitleInsPaidTo,
                isDisclosedGfe ? m_gfea.sOwnerTitleInsF_rep : "",
                E_GfeSectionT.B5
                ));
            respaFeeList.Add(CreateRespaFee("1104", "LENDER'S TITLE INSURANCE",
                isSettlement ? m_dataLoan.sSettlementTitleInsF_rep : m_gfea.sTitleInsF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sTitleInsFProps, m_dataLoan.sSettlementTitleInsFProps) : m_gfea.sTitleInsFProps,
                m_gfea.sTitleInsFTable,
                isDisclosedGfe ? m_gfea.sTitleInsF_rep : "",
                isSettlement ? m_dataLoan.sTitleInsFSettlementSection : m_dataLoan.sTitleInsFGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("1109", "DOC PREPARATION FEE",
                isSettlement ? m_dataLoan.sSettlementDocPrepF_rep : m_gfea.sDocPrepF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sDocPrepFProps, m_dataLoan.sSettlementDocPrepFProps) : m_gfea.sDocPrepFProps,
                m_gfea.sDocPrepFPaidTo,
                isDisclosedGfe ? m_gfea.sDocPrepF_rep : "",
                isSettlement ? m_dataLoan.sDocPrepFSettlementSection : m_gfea.sDocPrepFGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("1110", "NOTARY FEES",
                m_gfea.sNotaryF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sNotaryFProps, m_dataLoan.sSettlementNotaryFProps) : m_gfea.sNotaryFProps,
                m_gfea.sNotaryFPaidTo,
                isDisclosedGfe ? m_gfea.sNotaryF_rep : "",
                isSettlement ? m_dataLoan.sNotaryFSettlementSection : m_gfea.sNotaryFGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("1111", "ATTORNEY FEES",
                m_gfea.sAttorneyF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sAttorneyFProps, m_dataLoan.sSettlementAttorneyFProps) : m_gfea.sAttorneyFProps,
                m_gfea.sAttorneyFPaidTo,
                isDisclosedGfe ? m_gfea.sAttorneyF_rep : "",
                isSettlement ? m_dataLoan.sAttorneyFSettlementSection : m_gfea.sAttorneyFGfeSection
                ));
            respaFeeList.Add(CreateRespaFee("1112",
                isSettlement ? m_dataLoan.sSettlementU1TcDesc : m_gfea.sU1TcDesc,
                isSettlement ? m_dataLoan.sSettlementU1Tc_rep : m_gfea.sU1Tc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1TcProps, m_dataLoan.sSettlementU1TcProps) : m_gfea.sU1TcProps,
                m_gfea.sU1TcPaidTo,
                isDisclosedGfe ? m_gfea.sU1Tc_rep : "",
                isSettlement ? m_dataLoan.sU1TcSettlementSection : m_gfea.sU1TcGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1113",
                isSettlement ? m_dataLoan.sSettlementU2TcDesc : m_gfea.sU2TcDesc,
                isSettlement ? m_dataLoan.sSettlementU2Tc_rep : m_gfea.sU2Tc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2TcProps, m_dataLoan.sSettlementU2TcProps) : m_gfea.sU2TcProps,
                m_gfea.sU2TcPaidTo,
                isDisclosedGfe ? m_gfea.sU2Tc_rep : "",
                isSettlement ? m_dataLoan.sU2TcSettlementSection : m_gfea.sU2TcGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1114",
                isSettlement ? m_dataLoan.sSettlementU3TcDesc : m_gfea.sU3TcDesc,
                isSettlement ? m_dataLoan.sSettlementU3Tc_rep : m_gfea.sU3Tc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3TcProps, m_dataLoan.sSettlementU3TcProps) : m_gfea.sU3TcProps,
                m_gfea.sU3TcPaidTo,
                isDisclosedGfe ? m_gfea.sU3Tc_rep : "",
                isSettlement ? m_dataLoan.sU3TcSettlementSection : m_gfea.sU3TcGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1115",
                isSettlement ? m_dataLoan.sSettlementU4TcDesc : m_gfea.sU4TcDesc,
                isSettlement ? m_dataLoan.sSettlementU4Tc_rep : m_gfea.sU4Tc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU4TcProps, m_dataLoan.sSettlementU4TcProps) : m_gfea.sU4TcProps,
                m_gfea.sU4TcPaidTo,
                isDisclosedGfe ? m_gfea.sU4Tc_rep : "",
                isSettlement ? m_dataLoan.sU4TcSettlementSection : m_gfea.sU4TcGfeSection
                ));
            #endregion

            #region 1200 Government Recording & Transfer Charges
            respaFeeList.Add(CreateRespaFee("1202", "RECORDING FEES",
                isSettlement ? m_dataLoan.sSettlementRecF_rep : m_gfea.sRecF_rep,
                isSettlement ? m_dataLoan.sSettlementRecFPc_rep : m_gfea.sRecFPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sRecFProps, m_dataLoan.sSettlementRecFProps) : m_gfea.sRecFProps,
                m_gfea.sRecFDesc,
                isDisclosedGfe ? m_gfea.sRecF_rep : "",
                E_GfeSectionT.B7
                ));
            respaFeeList.Add(CreateRespaFee("1204", "CITY TAX/STAMPS",
                isSettlement ? m_dataLoan.sSettlementCountyRtc_rep : m_gfea.sCountyRtc_rep,
                isSettlement ? m_dataLoan.sSettlementCountyRtcPc_rep : m_gfea.sCountyRtcPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sCountyRtcProps, m_dataLoan.sSettlementCountyRtcProps) : m_gfea.sCountyRtcProps,
                m_gfea.sCountyRtcDesc,
                isDisclosedGfe ? m_gfea.sCountyRtc_rep : "",
                E_GfeSectionT.B8
                ));
            respaFeeList.Add(CreateRespaFee("1205", "STATE TAX/STAMPS",
                isSettlement ? m_dataLoan.sSettlementStateRtc_rep : m_gfea.sStateRtc_rep,
                isSettlement ? m_dataLoan.sSettlementStateRtcPc_rep : m_gfea.sStateRtcPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sStateRtcProps, m_dataLoan.sSettlementStateRtcProps) : m_gfea.sStateRtcProps,
                m_gfea.sStateRtcDesc,
                isDisclosedGfe ? m_gfea.sStateRtc_rep : "",
                E_GfeSectionT.B8
                ));
            respaFeeList.Add(CreateRespaFee("1206",
                isSettlement ? m_dataLoan.sSettlementU1GovRtcDesc : m_gfea.sU1GovRtcDesc,
                isSettlement ? m_dataLoan.sSettlementU1GovRtc_rep : m_gfea.sU1GovRtc_rep,
                isSettlement ? m_dataLoan.sSettlementU1GovRtcPc_rep : m_gfea.sU1GovRtcPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1GovRtcProps, m_dataLoan.sSettlementU1GovRtcProps) : m_gfea.sU1GovRtcProps,
                m_gfea.sU1GovRtcPaidTo,
                isDisclosedGfe ? m_gfea.sU1GovRtc_rep : "",
                isSettlement ? m_dataLoan.sU1GovRtcSettlementSection : m_gfea.sU1GovRtcGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1207",
                isSettlement ? m_dataLoan.sSettlementU2GovRtcDesc : m_gfea.sU2GovRtcDesc,
                isSettlement ? m_dataLoan.sSettlementU2GovRtc_rep : m_gfea.sU2GovRtc_rep,
                isSettlement ? m_dataLoan.sSettlementU2GovRtcPc_rep : m_gfea.sU2GovRtcPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2GovRtcProps, m_dataLoan.sSettlementU2GovRtcProps) : m_gfea.sU2GovRtcProps,
                m_gfea.sU2GovRtcPaidTo,
                isDisclosedGfe ? m_gfea.sU2GovRtc_rep : "",
                isSettlement ? m_dataLoan.sU2GovRtcSettlementSection : m_gfea.sU2GovRtcGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1208",
                isSettlement ? m_dataLoan.sSettlementU3GovRtcDesc : m_gfea.sU3GovRtcDesc,
                isSettlement ? m_dataLoan.sSettlementU3GovRtc_rep : m_gfea.sU3GovRtc_rep,
                isSettlement ? m_dataLoan.sSettlementU3GovRtcPc_rep : m_gfea.sU3GovRtcPc_rep,
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3GovRtcProps, m_dataLoan.sSettlementU3GovRtcProps) : m_gfea.sU3GovRtcProps,
                m_gfea.sU3GovRtcPaidTo,
                isDisclosedGfe ? m_gfea.sU3GovRtc_rep : "",
                isSettlement ? m_dataLoan.sU3GovRtcSettlementSection : m_gfea.sU3GovRtcGfeSection
                ));

            #endregion

            #region 1300 Additional Settlement Charges
            respaFeeList.Add(CreateRespaFee("1302", "PEST INSPECTION",
                isSettlement ? m_dataLoan.sSettlementPestInspectF_rep : m_gfea.sPestInspectF_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sPestInspectFProps, m_dataLoan.sSettlementPestInspectFProps) : m_gfea.sPestInspectFProps,
                m_gfea.sPestInspectPaidTo,
                isDisclosedGfe ? m_gfea.sPestInspectF_rep : "",
                E_GfeSectionT.B6
                ));
            respaFeeList.Add(CreateRespaFee("1303",
                isSettlement ? m_dataLoan.sSettlementU1ScDesc : m_gfea.sU1ScDesc,
                isSettlement ? m_dataLoan.sSettlementU1Sc_rep : m_gfea.sU1Sc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU1ScProps, m_dataLoan.sSettlementU1ScProps) : m_gfea.sU1ScProps,
                m_gfea.sU1ScPaidTo,
                isDisclosedGfe ? m_gfea.sU1Sc_rep : "",
                isSettlement ? m_dataLoan.sU1ScSettlementSection : m_gfea.sU1ScGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1304",
                isSettlement ? m_dataLoan.sSettlementU2ScDesc : m_gfea.sU2ScDesc,
                isSettlement ? m_dataLoan.sSettlementU2Sc_rep : m_gfea.sU2Sc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU2ScProps, m_dataLoan.sSettlementU2ScProps) : m_gfea.sU2ScProps,
                m_gfea.sU2ScPaidTo,
                isDisclosedGfe ? m_gfea.sU2Sc_rep : "",
                isSettlement ? m_dataLoan.sU2ScSettlementSection : m_gfea.sU2ScGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1305",
                isSettlement ? m_dataLoan.sSettlementU3ScDesc : m_gfea.sU3ScDesc,
                isSettlement ? m_dataLoan.sSettlementU3Sc_rep : m_gfea.sU3Sc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU3ScProps, m_dataLoan.sSettlementU3ScProps) : m_gfea.sU3ScProps,
                m_gfea.sU3ScPaidTo,
                isDisclosedGfe ? m_gfea.sU3Sc_rep : "",
                isSettlement ? m_dataLoan.sU3ScSettlementSection : m_gfea.sU3ScGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1306",
                isSettlement ? m_dataLoan.sSettlementU4ScDesc : m_gfea.sU4ScDesc,
                isSettlement ? m_dataLoan.sSettlementU4Sc_rep : m_gfea.sU4Sc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU4ScProps, m_dataLoan.sSettlementU4ScProps) : m_gfea.sU4ScProps,
                m_gfea.sU4ScPaidTo,
                isDisclosedGfe ? m_gfea.sU4Sc_rep : "",
                isSettlement ? m_dataLoan.sU4ScSettlementSection : m_gfea.sU4ScGfeSection
                ));

            respaFeeList.Add(CreateRespaFee("1307",
                isSettlement ? m_dataLoan.sSettlementU5ScDesc : m_gfea.sU5ScDesc,
                isSettlement ? m_dataLoan.sSettlementU5Sc_rep : m_gfea.sU5Sc_rep,
                "",
                isSettlement ? LosConvert.GfeItemProps_CopyGfeOnlyFieldsFrom(m_dataLoan.sU5ScProps, m_dataLoan.sSettlementU5ScProps) : m_gfea.sU5ScProps,
                m_gfea.sU5ScPaidTo,
                isDisclosedGfe ? m_gfea.sU5Sc_rep : "",
                isSettlement ? m_dataLoan.sU5ScSettlementSection : m_gfea.sU5ScGfeSection
                ));
            #endregion
        }

        private LoanOriginator CreateLoanOriginator()
        {
            LoanOriginator _LoanOriginator = new LoanOriginator();

            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            _LoanOriginator.NationwideMortgageLicensingSystemAssignedIdentifier = preparer.LoanOriginatorIdentifier;
            _LoanOriginator.LicenseNumberIdentifier = preparer.LicenseNumOfAgent;
            return _LoanOriginator;
        }
        private E_BorrowerMaritalStatusType ToMismo(E_aBMaritalStatT aBMaritalStatT)
        {
            switch (aBMaritalStatT)
            {
                case E_aBMaritalStatT.Married: return E_BorrowerMaritalStatusType.Married;
                case E_aBMaritalStatT.NotMarried: return E_BorrowerMaritalStatusType.Unmarried;
                case E_aBMaritalStatT.Separated: return E_BorrowerMaritalStatusType.Separated;
                case E_aBMaritalStatT.LeaveBlank: return E_BorrowerMaritalStatusType.NotProvided;
                default:
                    return E_BorrowerMaritalStatusType.Undefined;
            }
        }
        private E_BorrowerPrintPositionType ToMismo(E_BorrowerModeT BorrowerModeT)
        {
            switch (BorrowerModeT)
            {
                case E_BorrowerModeT.Borrower: return E_BorrowerPrintPositionType.Borrower;
                case E_BorrowerModeT.Coborrower: return E_BorrowerPrintPositionType.CoBorrower;
                default:
                    return E_BorrowerPrintPositionType.Undefined;
            }
        }
        private E_CurrentIncomeIncomeType ToMismo(E_aOIDescT e_aOIDescT)
        {
            E_CurrentIncomeIncomeType type = E_CurrentIncomeIncomeType.Undefined;
            switch (e_aOIDescT)
            {
                case E_aOIDescT.Other:
                case E_aOIDescT.CapitalGains:
                case E_aOIDescT.EmploymentRelatedAssets:
                case E_aOIDescT.ForeignIncome:
                case E_aOIDescT.RoyaltyPayment:
                case E_aOIDescT.SeasonalIncome:
                case E_aOIDescT.TemporaryLeave:
                case E_aOIDescT.TipIncome:
                case E_aOIDescT.BoarderIncome:
                case E_aOIDescT.MortgageCreditCertificate:
                case E_aOIDescT.TrailingCoBorrowerIncome:
                case E_aOIDescT.AccessoryUnitIncome:
                case E_aOIDescT.NonBorrowerHouseholdIncome:
                case E_aOIDescT.ContractBasis:
                case E_aOIDescT.DefinedContributionPlan:
                case E_aOIDescT.HousingAllowance:
                case E_aOIDescT.MiscellaneousIncome:
                case E_aOIDescT.WorkersCompensation:
                    type = E_CurrentIncomeIncomeType.OtherTypesOfIncome;
                    break;
                case E_aOIDescT.PublicAssistance:
                    type = E_CurrentIncomeIncomeType.PublicAssistance;
                    break;
                case E_aOIDescT.MilitaryBasePay:
                    type = E_CurrentIncomeIncomeType.MilitaryBasePay;
                    break;
                case E_aOIDescT.MilitaryRationsAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryRationsAllowance;
                    break;
                case E_aOIDescT.MilitaryFlightPay:
                    type = E_CurrentIncomeIncomeType.MilitaryFlightPay;
                    break;
                case E_aOIDescT.MilitaryHazardPay:
                    type = E_CurrentIncomeIncomeType.MilitaryHazardPay;
                    break;
                case E_aOIDescT.MilitaryClothesAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryClothesAllowance;
                    break;
                case E_aOIDescT.MilitaryQuartersAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryQuartersAllowance;
                    break;
                case E_aOIDescT.MilitaryPropPay:
                    type = E_CurrentIncomeIncomeType.MilitaryPropPay;
                    break;
                case E_aOIDescT.MilitaryOverseasPay:
                    type = E_CurrentIncomeIncomeType.MilitaryOverseasPay;
                    break;
                case E_aOIDescT.MilitaryCombatPay:
                    type = E_CurrentIncomeIncomeType.MilitaryCombatPay;
                    break;
                case E_aOIDescT.MilitaryVariableHousingAllowance:
                    type = E_CurrentIncomeIncomeType.MilitaryVariableHousingAllowance;
                    break;
                case E_aOIDescT.AlimonyChildSupport:
                case E_aOIDescT.Alimony:
                case E_aOIDescT.ChildSupport:
                    type = E_CurrentIncomeIncomeType.AlimonyChildSupport;
                    break;
                case E_aOIDescT.NotesReceivableInstallment:
                    type = E_CurrentIncomeIncomeType.NotesReceivableInstallment;
                    break;
                case E_aOIDescT.PensionRetirement:
                    type = E_CurrentIncomeIncomeType.Pension;
                    break;
                case E_aOIDescT.SocialSecurityDisability:
                case E_aOIDescT.SocialSecurity:
                case E_aOIDescT.Disability:
                    type = E_CurrentIncomeIncomeType.SocialSecurity;
                    break;
                case E_aOIDescT.RealEstateMortgageDifferential:
                    type = E_CurrentIncomeIncomeType.MortgageDifferential;
                    break;
                case E_aOIDescT.Trust:
                    type = E_CurrentIncomeIncomeType.Trust;
                    break;
                case E_aOIDescT.UnemploymentWelfare:
                    type = E_CurrentIncomeIncomeType.Unemployment;
                    break;
                case E_aOIDescT.AutomobileExpenseAccount:
                    type = E_CurrentIncomeIncomeType.AutomobileExpenseAccount;
                    break;
                case E_aOIDescT.FosterCare:
                    type = E_CurrentIncomeIncomeType.FosterCare;
                    break;
                case E_aOIDescT.VABenefitsNonEducation:
                    type = E_CurrentIncomeIncomeType.VABenefitsNonEducational;
                    break;
                case E_aOIDescT.SubjPropNetCashFlow:
                    type = E_CurrentIncomeIncomeType.SubjectPropertyNetCashFlow;
                    break;
                case E_aOIDescT.HousingChoiceVoucher:
                    type = E_CurrentIncomeIncomeType.PublicAssistance;
                    break;
                default:
                    throw new UnhandledEnumException(e_aOIDescT);
            }
            return type;
        }

        private Borrower CreateBorrower(int sequenceId, CAppData dataApp)
        {
            Borrower _Borrower = new Borrower();

            _Borrower.BorrowerId = "BOR" + sequenceId;
            // _Borrower.JointAssetBorrowerId = ;
            _Borrower.ApplicationSignedDate = m_dataLoan.sAppSubmittedD_rep;
            // _Borrower.JointAssetLiabilityReportingType = ;
            // _Borrower.CreditReportIdentifier = ;
            // _Borrower.BorrowerNonObligatedIndicator = ;
            // _Borrower.NonPersonEntityIndicator = ;
            // _Borrower.RelationshipTitleType = ;
            // _Borrower.RelationshipTitleTypeOtherDescription = ;

            _Borrower.FirstName = dataApp.aFirstNm;
            _Borrower.MiddleName = dataApp.aMidNm;
            _Borrower.LastName = dataApp.aLastNm;
            _Borrower.NameSuffix = dataApp.aSuffix;
            _Borrower.AgeAtApplicationYears = dataApp.aAge_rep;
            _Borrower.BirthDate = dataApp.aDob_rep;
            _Borrower.HomeTelephoneNumber = dataApp.aHPhone;
            _Borrower.PrintPositionType = ToMismo(dataApp.BorrowerModeT);
            _Borrower.Ssn = dataApp.aSsn;
            _Borrower.DependentCount = dataApp.aDependNum_rep;
            _Borrower.MaritalStatusType = ToMismo(dataApp.aMaritalStatT);
            _Borrower.SchoolingYears = dataApp.aSchoolYrs_rep;
            _Borrower.SequenceIdentifier = sequenceId.ToString();
            _Borrower.UnparsedName = dataApp.aNm;

            _Borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Base, dataApp.aBaseI_rep));
            _Borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Overtime, dataApp.aOvertimeI_rep));
            _Borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Bonus, dataApp.aBonusesI_rep));
            _Borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.Commissions, dataApp.aCommisionI_rep));
            _Borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.DividendsInterest, dataApp.aDividendI_rep));
            _Borrower.CurrentIncomeList.Add(CreateCurrentIncome(E_CurrentIncomeIncomeType.NetRentalIncome, dataApp.aNetRentI1003_rep));

            foreach (var otherIncome in dataApp.aOtherIncomeList)
            {
                if ((otherIncome.IsForCoBorrower && dataApp.BorrowerModeT == E_BorrowerModeT.Coborrower) || (!otherIncome.IsForCoBorrower && dataApp.BorrowerModeT == E_BorrowerModeT.Borrower))
                {
                    _Borrower.CurrentIncomeList.Add(CreateCurrentIncome(ToMismo(OtherIncome.Get_aOIDescT(otherIncome.Desc)), dataApp.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep)));
                }
            }

            string[] dependentAges = dataApp.aDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':');
            foreach (var p in dependentAges)
            {
                if (string.IsNullOrEmpty(p) == false)
                {
                    _Borrower.DependentList.Add(CreateDependent(p));
                }
            }


            _Borrower.MailTo = CreateMailTo(dataApp);
            _Borrower.ResidenceList.Add(CreateResidence(dataApp.aAddr, dataApp.aCity, dataApp.aState, dataApp.aBZip, dataApp.aAddrT, dataApp.aAddrYrs, true));
            _Borrower.ResidenceList.Add(CreateResidence(dataApp.aPrev1Addr, dataApp.aPrev1City, dataApp.aPrev1State, dataApp.aPrev1Zip, (E_aBAddrT)dataApp.aPrev1AddrT, dataApp.aPrev1AddrYrs, false));
            _Borrower.ResidenceList.Add(CreateResidence(dataApp.aPrev2Addr, dataApp.aPrev2City, dataApp.aPrev2State, dataApp.aPrev2Zip, (E_aBAddrT)dataApp.aPrev2AddrT, dataApp.aPrev2AddrYrs, false));

            _Borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Work, E_ContactPointType.Phone, dataApp.aBusPhone));
            _Borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Mobile, E_ContactPointType.Phone, dataApp.aCellPhone));
            _Borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, E_ContactPointType.Fax, dataApp.aFax));
            _Borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, E_ContactPointType.Email, dataApp.aEmail));
            _Borrower.ContactPointList.Add(CreateContactPoint(E_ContactPointRoleType.Home, E_ContactPointType.Phone, dataApp.aHPhone));


            _Borrower.Declaration = CreateDeclaration(dataApp);

            IEmpCollection empCollection = dataApp.aEmpCollection;
            _Borrower.EmployerList.Add(CreateEmployer(empCollection.GetPrimaryEmp(false)));

            foreach (IRegularEmploymentRecord record in empCollection.GetSubcollection(true, E_EmpGroupT.Previous))
            {
                _Borrower.EmployerList.Add(CreateEmployer(record));
            }

            _Borrower.GovernmentMonitoring = CreateGovernmentMonitoring(dataApp);

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                // Only add present housing for Borrower.
                _Borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.Rent, dataApp.aPresRent_rep));
                _Borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.FirstMortgagePrincipalAndInterest, dataApp.aPres1stM_rep));
                _Borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.OtherMortgageLoanPrincipalAndInterest, dataApp.aPresOFin_rep));
                _Borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.HazardInsurance, dataApp.aPresHazIns_rep));
                _Borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.RealEstateTax, dataApp.aPresRealETx_rep));
                _Borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.MI, dataApp.aPresMIns_rep));
                _Borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.HomeownersAssociationDuesAndCondominiumFees, dataApp.aPresHoAssocDues_rep));
                _Borrower.PresentHousingExpenseList.Add(CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType.OtherHousingExpense, dataApp.aPresOHExp_rep));
            }

            // _Borrower.AliasList.Add(CreateAlias());
            // _Borrower.FhaVaBorrower = CreateFhaVaBorrower();
            // _Borrower.SummaryList.Add(CreateSummary());
            // _Borrower.VaBorrower = CreateVaBorrower();
            // _Borrower.FhaBorrower = CreateFhaBorrower();
            // _Borrower.NearestLivingRelative = CreateNearestLivingRelative();
            // _Borrower.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            // _Borrower.PowerOfAttorney = CreatePowerOfAttorney();

            return _Borrower;

        }

        private PowerOfAttorney CreatePowerOfAttorney()
        {
            PowerOfAttorney _PowerOfAttorney = new PowerOfAttorney();

            // _PowerOfAttorney.UnparsedName = ;
            // _PowerOfAttorney.TitleDescription = ;
            // _PowerOfAttorney.SigningCapacityTextDescription = ;

            return _PowerOfAttorney;

        }

        private NearestLivingRelative CreateNearestLivingRelative()
        {
            NearestLivingRelative _NearestLivingRelative = new NearestLivingRelative();

            // _NearestLivingRelative.City = ;
            // _NearestLivingRelative.Name = ;
            // _NearestLivingRelative.PostalCode = ;
            // _NearestLivingRelative.RelationshipDescription = ;
            // _NearestLivingRelative.State = ;
            // _NearestLivingRelative.StreetAddress = ;
            // _NearestLivingRelative.StreetAddress2 = ;
            // _NearestLivingRelative.TelephoneNumber = ;

            return _NearestLivingRelative;

        }

        private FhaBorrower CreateFhaBorrower()
        {
            FhaBorrower _FhaBorrower = new FhaBorrower();

            // _FhaBorrower.CertificationOwnOtherPropertyIndicator = ;
            // _FhaBorrower.CertificationLeadPaintIndicator = ;
            // _FhaBorrower.CertificationOwn4OrMoreDwellingsIndicator = ;
            // _FhaBorrower.CertificationPropertySoldCity = ;
            // _FhaBorrower.CertificationPropertySoldPostalCode = ;
            // _FhaBorrower.CertificationOriginalMortgageAmount = ;
            // _FhaBorrower.CertificationSalesPriceAmount = ;
            // _FhaBorrower.CertificationRentalIndicator = ;
            // _FhaBorrower.CertificationPropertyToBeSoldIndicator = ;
            // _FhaBorrower.CertificationPropertySoldState = ;
            // _FhaBorrower.CertificationPropertySoldStreetAddress = ;

            return _FhaBorrower;

        }

        private VaBorrower CreateVaBorrower()
        {
            VaBorrower _VaBorrower = new VaBorrower();

            // _VaBorrower.VaCoBorrowerNonTaxableIncomeAmount = ;
            // _VaBorrower.VaCoBorrowerTaxableIncomeAmount = ;
            // _VaBorrower.VaFederalTaxAmount = ;
            // _VaBorrower.VaLocalTaxAmount = ;
            // _VaBorrower.VaPrimaryBorrowerNonTaxableIncomeAmount = ;
            // _VaBorrower.VaPrimaryBorrowerTaxableIncomeAmount = ;
            // _VaBorrower.VaSocialSecurityTaxAmount = ;
            // _VaBorrower.VaStateTaxAmount = ;
            // _VaBorrower.CertificationOccupancyType = ;

            return _VaBorrower;

        }

        private Summary CreateSummary()
        {
            Summary _Summary = new Summary();

            // _Summary.Amount = ;
            // _Summary.AmountType = ;

            return _Summary;

        }

        private PresentHousingExpense CreatePresentHousingExpense(E_PresentHousingExpenseHousingExpenseType type, string amount)
        {
            if (amount == "" || amount == "0.00" || amount == "$0.00")
            {
                return null;
            }
            PresentHousingExpense _PresentHousingExpense = new PresentHousingExpense();

            _PresentHousingExpense.HousingExpenseType = type;
            _PresentHousingExpense.PaymentAmount = amount;

            return _PresentHousingExpense;

        }

        private GovernmentMonitoring CreateGovernmentMonitoring(CAppData dataApp)
        {
            GovernmentMonitoring _GovernmentMonitoring = new GovernmentMonitoring();

            // _GovernmentMonitoring.OtherRaceNationalOriginDescription = ;
            // _GovernmentMonitoring.RaceNationalOriginType = ;

            _GovernmentMonitoring.HmdaEthnicityType = ToMismo(dataApp.aHispanicTFallback);
            _GovernmentMonitoring.GenderType = ToMismo(dataApp.aGenderFallback);
            _GovernmentMonitoring.RaceNationalOriginRefusalIndicator = ToMismo(dataApp.aNoFurnish);

            if ((dataApp.aIsAmericanIndian))
                _GovernmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.AmericanIndianOrAlaskaNative));

            if ((dataApp.aIsAsian))
                _GovernmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.Asian));

            if ((dataApp.aIsBlack))
                _GovernmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.BlackOrAfricanAmerican));

            if ((dataApp.aIsPacificIslander))
                _GovernmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.NativeHawaiianOrOtherPacificIslander));

            if ((dataApp.aIsWhite))
                _GovernmentMonitoring.HmdaRaceList.Add(CreateHmdaRace(E_HmdaRaceType.White));

            return _GovernmentMonitoring;

        }
        private E_GovernmentMonitoringGenderType ToMismo(E_GenderT e_GenderT)
        {
            switch (e_GenderT)
            {
                case E_GenderT.Female: return E_GovernmentMonitoringGenderType.Female;
                case E_GenderT.Male: return E_GovernmentMonitoringGenderType.Male;
                case E_GenderT.LeaveBlank: return E_GovernmentMonitoringGenderType.Undefined;
                case E_GenderT.NA: return E_GovernmentMonitoringGenderType.NotApplicable;
                case E_GenderT.Unfurnished: return E_GovernmentMonitoringGenderType.InformationNotProvidedUnknown;
                default:
                    throw new UnhandledEnumException(e_GenderT);
            }
        }
        private E_GovernmentMonitoringHmdaEthnicityType ToMismo(E_aHispanicT e_aHispanicT)
        {
            switch (e_aHispanicT)
            {
                case E_aHispanicT.Hispanic: return E_GovernmentMonitoringHmdaEthnicityType.HispanicOrLatino;
                case E_aHispanicT.LeaveBlank: return E_GovernmentMonitoringHmdaEthnicityType.Undefined;
                case E_aHispanicT.NotHispanic: return E_GovernmentMonitoringHmdaEthnicityType.NotHispanicOrLatino;
                default:
                    throw new UnhandledEnumException(e_aHispanicT);
            }
        }
        private HmdaRace CreateHmdaRace(E_HmdaRaceType type)
        {
            HmdaRace _HmdaRace = new HmdaRace();

            _HmdaRace.Type = type;

            return _HmdaRace;

        }

        private FhaVaBorrower CreateFhaVaBorrower()
        {
            FhaVaBorrower _FhaVaBorrower = new FhaVaBorrower();

            // _FhaVaBorrower.CaivrsIdentifier = ;
            // _FhaVaBorrower.FnmBankruptcyCount = ;
            // _FhaVaBorrower.FnmBorrowerCreditRating = ;
            // _FhaVaBorrower.FnmCreditReportScoreType = ;
            // _FhaVaBorrower.FnmForeclosureCount = ;
            // _FhaVaBorrower.VeteranStatusIndicator = ;
            // _FhaVaBorrower.CertificationSalesPriceExceedsAppraisedValueType = ;

            return _FhaVaBorrower;

        }

        private Employer CreateEmployer(IRegularEmploymentRecord record)
        {
            if (null == record)
            {
                return null;
            }
            Employer _Employer = new Employer();

            _Employer.Name = record.EmplrNm;
            _Employer.StreetAddress = record.EmplrAddr;
            _Employer.City = record.EmplrCity;
            _Employer.State = record.EmplrState;
            _Employer.PostalCode = record.EmplrZip;
            _Employer.TelephoneNumber = record.EmplrBusPhone;
            // _Employer.CurrentEmploymentMonthsOnJob = ;
            // _Employer.CurrentEmploymentTimeInLineOfWorkYears = ;
            // _Employer.CurrentEmploymentYearsOnJob = ;
            _Employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            _Employer.EmploymentCurrentIndicator = ToMismo(record.IsCurrent);
            _Employer.EmploymentPositionDescription = record.JobTitle;
            _Employer.EmploymentPrimaryIndicator = ToMismo(record.IsPrimaryEmp);
            _Employer.IncomeEmploymentMonthlyAmount = record.MonI_rep;
            _Employer.PreviousEmploymentEndDate = record.EmplmtEndD_rep;
            _Employer.PreviousEmploymentStartDate = record.EmplmtStartD_rep;

            return _Employer;

        }

        private Employer CreateEmployer(IPrimaryEmploymentRecord record)
        {
            if (null == record)
            {
                return null;
            }
            Employer _Employer = new Employer();

            _Employer.Name = record.EmplrNm;
            _Employer.StreetAddress = record.EmplrAddr;
            _Employer.City = record.EmplrCity;
            _Employer.State = record.EmplrState;
            _Employer.PostalCode = record.EmplrZip;
            _Employer.TelephoneNumber = record.EmplrBusPhone;
            _Employer.CurrentEmploymentMonthsOnJob = record.EmplmtLenInMonths_rep;
            _Employer.CurrentEmploymentTimeInLineOfWorkYears = record.EmplmtLenInYrs_rep;
            _Employer.CurrentEmploymentYearsOnJob = record.EmplmtLenInYrs_rep;
            _Employer.EmploymentBorrowerSelfEmployedIndicator = ToMismo(record.IsSelfEmplmt);
            _Employer.EmploymentCurrentIndicator = E_YesNoIndicator.Y;
            _Employer.EmploymentPositionDescription = record.JobTitle;
            _Employer.EmploymentPrimaryIndicator = E_YesNoIndicator.Y;

            return _Employer;

        }

        private Dependent CreateDependent(string age)
        {
            Dependent _Dependent = new Dependent();

            _Dependent.AgeYears = age;

            return _Dependent;

        }

        private Declaration CreateDeclaration(CAppData dataApp)
        {
            Declaration _Declaration = new Declaration();

            _Declaration.AlimonyChildSupportObligationIndicator = ToMismoFromDeclaration(dataApp.aDecAlimony);
            _Declaration.BankruptcyIndicator = ToMismoFromDeclaration(dataApp.aDecBankrupt);
            _Declaration.BorrowedDownPaymentIndicator = ToMismoFromDeclaration(dataApp.aDecBorrowing);
            _Declaration.CoMakerEndorserOfNoteIndicator = ToMismoFromDeclaration(dataApp.aDecEndorser);
            _Declaration.LoanForeclosureOrJudgementIndicator = ToMismoFromDeclaration(dataApp.aDecObligated);
            _Declaration.OutstandingJudgementsIndicator = ToMismoFromDeclaration(dataApp.aDecJudgment);
            _Declaration.PartyToLawsuitIndicator = ToMismoFromDeclaration(dataApp.aDecLawsuit);
            _Declaration.PresentlyDelinquentIndicator = ToMismoFromDeclaration(dataApp.aDecDelinquent);
            _Declaration.PriorPropertyTitleType = ToMismo(dataApp.aDecPastOwnedPropTitleT);
            _Declaration.PriorPropertyUsageType = ToMismo(dataApp.aDecPastOwnedPropT);
            _Declaration.PropertyForeclosedPastSevenYearsIndicator = ToMismoFromDeclaration(dataApp.aDecForeclosure);
            _Declaration.BorrowerFirstTimeHomebuyerIndicator = ToMismo(DetermineFirstTimeHomeBuyerValue(dataApp));

            switch (dataApp.aDecPastOwnership)
            {
                case "Y":
                    _Declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.Yes;
                    break;
                case "N":
                    _Declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.No;
                    break;
                default:
                    _Declaration.HomeownerPastThreeYearsType = E_DeclarationHomeownerPastThreeYearsType.Undefined;
                    break;

            }
            switch (dataApp.aDecOcc)
            {
                case "Y":
                    _Declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.Yes;
                    break;
                case "N":
                    _Declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.No;
                    break;
                default:
                    _Declaration.IntentToOccupyType = E_DeclarationIntentToOccupyType.Undefined;
                    break;
            }
            if (dataApp.aDecCitizen.ToUpper() == "Y")
            {
                _Declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.USCitizen;

            }

            else if (dataApp.aDecResidency.ToUpper() == "Y")
            {
                _Declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.PermanentResidentAlien;
            }
            else
            {
                _Declaration.CitizenshipResidencyType = E_DeclarationCitizenshipResidencyType.Unknown;
            }
            // _Declaration.ExplanationList.Add(CreateExplanation());
            return _Declaration;

        }
        private E_DeclarationPriorPropertyTitleType ToMismo(E_aBDecPastOwnedPropTitleT e_aBDecPastOwnedPropTitleT)
        {
            switch (e_aBDecPastOwnedPropTitleT)
            {
                case E_aBDecPastOwnedPropTitleT.Empty: return E_DeclarationPriorPropertyTitleType.Undefined;
                case E_aBDecPastOwnedPropTitleT.O: return E_DeclarationPriorPropertyTitleType.JointWithOtherThanSpouse;
                case E_aBDecPastOwnedPropTitleT.S: return E_DeclarationPriorPropertyTitleType.Sole;
                case E_aBDecPastOwnedPropTitleT.SP: return E_DeclarationPriorPropertyTitleType.JointWithSpouse;
                default:
                    throw new UnhandledEnumException(e_aBDecPastOwnedPropTitleT);
            }
        }
        private E_DeclarationPriorPropertyUsageType ToMismo(E_aBDecPastOwnedPropT e_aBDecPastOwnedPropT)
        {
            switch (e_aBDecPastOwnedPropT)
            {
                case E_aBDecPastOwnedPropT.Empty: return E_DeclarationPriorPropertyUsageType.Undefined;
                case E_aBDecPastOwnedPropT.IP: return E_DeclarationPriorPropertyUsageType.Investment;
                case E_aBDecPastOwnedPropT.PR: return E_DeclarationPriorPropertyUsageType.PrimaryResidence;
                case E_aBDecPastOwnedPropT.SH: return E_DeclarationPriorPropertyUsageType.SecondaryResidence;
                default:
                    throw new UnhandledEnumException(e_aBDecPastOwnedPropT);
            }
        }

        // OPM 41047
        private bool DetermineFirstTimeHomeBuyerValue(CAppData dataApp)
        {
            if (m_dataLoan.sLT == E_sLT.FHA || m_dataLoan.sLT == E_sLT.VA)
            {
                return dataApp.aHas1stTimeBuyerTri == E_TriState.Yes;
            }
            else
            {
                return m_dataLoan.sHas1stTimeBuyer;
            }
        }
        private E_YesNoIndicator ToMismo(bool p)
        {
            return p ? E_YesNoIndicator.Y : E_YesNoIndicator.N;
        }
        private E_YesNoIndicator ToMismoFromDeclaration(string p)
        {
            switch (p.ToUpper())
            {
                case "Y": return E_YesNoIndicator.Y;
                case "N": return E_YesNoIndicator.N;
                default: return E_YesNoIndicator.Undefined;
            }
        }

        private Explanation CreateExplanation()
        {
            Explanation _Explanation = new Explanation();

            // _Explanation.Description = ;
            // _Explanation.Type = ;

            return _Explanation;

        }

        private CurrentIncome CreateCurrentIncome(E_CurrentIncomeIncomeType type, string monthlyTotalAmount)
        {
            if (monthlyTotalAmount == "" || monthlyTotalAmount == "0.00")
            {
                return null;
            }
            CurrentIncome _CurrentIncome = new CurrentIncome();

            _CurrentIncome.IncomeType = type;
            _CurrentIncome.MonthlyTotalAmount = monthlyTotalAmount;

            return _CurrentIncome;

        }
        private E_ResidenceBorrowerResidencyBasisType ToMismo(E_aBAddrT type)
        {
            switch (type)
            {
                case E_aBAddrT.Own: return E_ResidenceBorrowerResidencyBasisType.Own;
                case E_aBAddrT.Rent: return E_ResidenceBorrowerResidencyBasisType.Rent;
                case E_aBAddrT.LeaveBlank: return E_ResidenceBorrowerResidencyBasisType.Undefined;
                case E_aBAddrT.LivingRentFree: return E_ResidenceBorrowerResidencyBasisType.LivingRentFree; // OPM 46198
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        private Residence CreateResidence(string address, string city, string state, string zip, E_aBAddrT type, string years, bool current)
        {
            if (string.IsNullOrEmpty(address) && string.IsNullOrEmpty(city) && string.IsNullOrEmpty(zip) && string.IsNullOrEmpty(state))
            {
                return null;
            }
            Residence _Residence = new Residence();

            YearMonthParser parser = new YearMonthParser();
            parser.Parse(years);

            _Residence.StreetAddress = address;
            _Residence.City = city;
            _Residence.State = state;
            _Residence.PostalCode = zip;
            _Residence.BorrowerResidencyDurationMonths = parser.NumberOfMonths.ToString();
            _Residence.BorrowerResidencyDurationYears = parser.NumberOfYears.ToString();
            _Residence.BorrowerResidencyBasisType = ToMismo(type);
            _Residence.BorrowerResidencyType = current ? E_ResidenceBorrowerResidencyType.Current : E_ResidenceBorrowerResidencyType.Prior;

            // _Residence.Country = ;

            return _Residence;

        }

        private MailTo CreateMailTo(CAppData dataApp)
        {
            MailTo _MailTo = new MailTo();

            // _MailTo.StreetAddress2 = ;
            // _MailTo.Country = ;
            // _MailTo.AddressSameAsPropertyIndicator = ;

            _MailTo.StreetAddress = dataApp.aAddrMail;
            _MailTo.City = dataApp.aCityMail;
            _MailTo.State = dataApp.aStateMail;
            _MailTo.PostalCode = dataApp.aZipMail;

            return _MailTo;

        }

        private Alias CreateAlias()
        {
            Alias _Alias = new Alias();

            // _Alias.FirstName = ;
            // _Alias.LastName = ;
            // _Alias.MiddleName = ;
            // _Alias.NameSuffix = ;
            // _Alias.SequenceIdentifier = ;
            // _Alias.Type = ;
            // _Alias.TypeOtherDescription = ;
            // _Alias.UnparsedName = ;
            // _Alias.CreditorName = ;
            // _Alias.AccountIdentifier = ;

            return _Alias;

        }

        private TransactionDetail CreateTransactionDetail()
        {
            TransactionDetail _TransactionDetail = new TransactionDetail();

            // _TransactionDetail.SubordinateLienHelocAmount = ;
            // _TransactionDetail.FreReserveAmount = ;

            _TransactionDetail.AlterationsImprovementsAndRepairsAmount = m_dataLoan.sAltCost_rep;
            _TransactionDetail.BorrowerPaidDiscountPointsTotalAmount = m_dataLoan.sLDiscnt1003_rep;
            _TransactionDetail.EstimatedClosingCostsAmount = m_dataLoan.sTotEstCcNoDiscnt1003_rep;
            _TransactionDetail.MiAndFundingFeeFinancedAmount = m_dataLoan.sFfUfmipFinanced_rep;
            _TransactionDetail.MiAndFundingFeeTotalAmount = m_dataLoan.sFfUfmip1003_rep;
            _TransactionDetail.PrepaidItemsEstimatedAmount = m_dataLoan.sTotEstPp1003_rep;
            _TransactionDetail.PurchasePriceAmount = m_dataLoan.sPurchPrice_rep;

            E_sLPurposeT loanPurpose = m_use2015DataLayer ? m_dataLoan.sLPurposeT : m_gfea.sLPurposeT;
            if (loanPurpose != E_sLPurposeT.Purchase)
            {
                _TransactionDetail.RefinanceIncludingDebtsToBePaidOffAmount = m_dataLoan.sRefPdOffAmt1003_rep;
            }
            _TransactionDetail.SalesConcessionAmount = m_dataLoan.sFHASalesConcessions_rep;
            _TransactionDetail.SellerPaidClosingCostsAmount = m_dataLoan.sTotCcPbs_rep;
            _TransactionDetail.SubordinateLienAmount = m_dataLoan.sONewFinBal_rep;
            _TransactionDetail.FreReservesAmount = m_dataLoan.sFredieReservesAmt_rep;

            _TransactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit1Desc, m_dataLoan.sOCredit1Amt_rep));
            _TransactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit2Desc, m_dataLoan.sOCredit2Amt_rep));
            _TransactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit3Desc, m_dataLoan.sOCredit3Amt_rep));
            _TransactionDetail.PurchaseCreditList.Add(CreatePurchaseCredit(m_dataLoan.sOCredit4Desc, m_dataLoan.sOCredit4Amt_rep));

            return _TransactionDetail;

        }

        private PurchaseCredit CreatePurchaseCredit(string desc, string amount)
        {
            if (null == amount || "" == amount || "0.00" == amount || null == desc)
            {
                return null;
            }
            PurchaseCredit _PurchaseCredit = new PurchaseCredit();

            switch (desc.ToLower())
            {
                case "cash deposit on sales contract":
                    _PurchaseCredit.Type = E_PurchaseCreditType.EarnestMoney;
                    break;
                case "employer assisted housing":
                    _PurchaseCredit.Type = E_PurchaseCreditType.EmployerAssistedHousing;
                    break;
                case "lease purchase fund":
                    _PurchaseCredit.Type = E_PurchaseCreditType.LeasePurchaseFund;
                    break;
                case "relocation funds":
                    _PurchaseCredit.Type = E_PurchaseCreditType.RelocationFunds;
                    break;
                case "seller credit":
                    _PurchaseCredit.SourceType = E_PurchaseCreditSourceType.PropertySeller;
                    _PurchaseCredit.Type = E_PurchaseCreditType.Other;
                    break;
                case "lender credit":
                    _PurchaseCredit.SourceType = E_PurchaseCreditSourceType.Lender;
                    _PurchaseCredit.Type = E_PurchaseCreditType.Other;
                    break;

                default:
                    _PurchaseCredit.Type = E_PurchaseCreditType.Other;
                    break;
            }
            _PurchaseCredit.Amount = amount;

            return _PurchaseCredit;

        }

        private TitleHolder CreateTitleHolder(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            TitleHolder _TitleHolder = new TitleHolder();

            _TitleHolder.Name = name;
            // _TitleHolder.LandTrustType = ;

            return _TitleHolder;

        }

        private RespaFee CreateRespaFee(string hudLineNumber, string desription, string amount, string percent, int props, string paidTo, string gfeDisclosedAmount, E_GfeSectionT gfeSectionT)
        {
            if (string.IsNullOrEmpty(amount) || amount == "0.00")
            {
                return null;
            }
            RespaFee _RespaFee = new RespaFee();

            // _RespaFee.RespaSectionClassificationType = ;
            // _RespaFee.RequiredProviderOfServiceIndicator = ;
            // _RespaFee.ResponsiblePartyType = ;
            _RespaFee.SpecifiedHudLineNumber = hudLineNumber;
            // _RespaFee.Type = ;
            _RespaFee.TypeOtherDescription = desription;
            if (LosConvert.GfeItemProps_ToBr(props))
            {
                _RespaFee.PaidToType = E_RespaFeePaidToType.Broker;
            }
            else
            {
                if (string.IsNullOrEmpty(paidTo))
                {
                    _RespaFee.PaidToType = E_RespaFeePaidToType.Lender;
                }
            }
            // _RespaFee.PaidToTypeOtherDescription = ;
            _RespaFee.GfeDisclosedAmount = gfeDisclosedAmount;
            _RespaFee.PaymentList.Add(CreatePayment(amount, percent, props));
            // _RespaFee.RequiredServiceProvider = CreateRequiredServiceProvider();
            _RespaFee.PaidTo = CreatePaidTo(paidTo);
            _RespaFee.GfeAggregationType = ToMismo(gfeSectionT);
            return _RespaFee;

        }

        private E_RespaFeeGfeAggregationType ToMismo(E_GfeSectionT gfeSectionT)
        {
            switch (gfeSectionT)
            {
                case E_GfeSectionT.B1:
                    return E_RespaFeeGfeAggregationType.OurOriginationCharge;
                case E_GfeSectionT.B2:
                    return E_RespaFeeGfeAggregationType.ChosenInterestRateCreditOrCharge;
                case E_GfeSectionT.B3:
                    return E_RespaFeeGfeAggregationType.RequiredServicesLenderSelected;
                case E_GfeSectionT.B4:
                    return E_RespaFeeGfeAggregationType.TitleServices;
                case E_GfeSectionT.B5:
                    return E_RespaFeeGfeAggregationType.OwnersTitleInsurance;
                case E_GfeSectionT.B6:
                    return E_RespaFeeGfeAggregationType.RequiredServicesYouCanShopFor;
                case E_GfeSectionT.B7:
                    return E_RespaFeeGfeAggregationType.GovernmentRecordingCharges;
                case E_GfeSectionT.B8:
                    return E_RespaFeeGfeAggregationType.TransferTaxes;
                case E_GfeSectionT.B9:
                case E_GfeSectionT.B10:
                case E_GfeSectionT.B11:
                case E_GfeSectionT.NotApplicable:
                case E_GfeSectionT.LeaveBlank:

                    return E_RespaFeeGfeAggregationType.Undefined;
                default:
                    throw new UnhandledEnumException(gfeSectionT);
            }
        }

        private PaidTo CreatePaidTo(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            PaidTo _PaidTo = new PaidTo();

            _PaidTo.Name = name;
            // _PaidTo.StreetAddress = ;
            // _PaidTo.StreetAddress2 = ;
            // _PaidTo.City = ;
            // _PaidTo.State = ;
            // _PaidTo.PostalCode = ;
            // _PaidTo.Country = ;
            // _PaidTo.County = ;
            // _PaidTo.NonPersonEntityIndicator = ;

            // _PaidTo.ContactDetail = CreateContactDetail();
            // _PaidTo.NonPersonEntityDetail = CreateNonPersonEntityDetail();
            return _PaidTo;

        }

        private RequiredServiceProvider CreateRequiredServiceProvider()
        {
            RequiredServiceProvider _RequiredServiceProvider = new RequiredServiceProvider();

            // _RequiredServiceProvider.City = ;
            // _RequiredServiceProvider.Name = ;
            // _RequiredServiceProvider.NatureOfRelationshipDescription = ;
            // _RequiredServiceProvider.PostalCode = ;
            // _RequiredServiceProvider.State = ;
            // _RequiredServiceProvider.StreetAddress = ;
            // _RequiredServiceProvider.StreetAddress2 = ;
            // _RequiredServiceProvider.TelephoneNumber = ;

            return _RequiredServiceProvider;

        }

        private Payment CreatePayment(string amount, string percent, int props)
        {
            Payment _Payment = new Payment();

            // _Payment.AllowableFhaClosingCostIndicator = ;
            _Payment.Amount = amount;
            // _Payment.CollectedByType = ;

            _Payment.IncludedInAprIndicator = ToMismo(LosConvert.GfeItemProps_AprAndPdByBorrower(props));
            // _Payment.NetDueAmount = ;
            int paidBy = LosConvert.GfeItemProps_Payer(props);
            if (paidBy == 0 || paidBy == 1)
            {
                _Payment.PaidByType = E_PaymentPaidByType.Buyer;
            }
            else if (paidBy == 2)
            {
                _Payment.PaidByType = E_PaymentPaidByType.Seller;
            }
            else if (paidBy == 3)
            {
                _Payment.PaidByType = E_PaymentPaidByType.Lender;
            }
            else if (paidBy == 4)
            {
                _Payment.PaidByType = E_PaymentPaidByType.ThirdParty;
            }
            // _Payment.PaidByType = ;
            // _Payment.PaidByTypeThirdPartyName = ;
            _Payment.PaidOutsideOfClosingIndicator = ToMismo(LosConvert.GfeItemProps_Poc(props));
            _Payment.Percent = percent;
            // _Payment.ProcessType = ;
            // _Payment.Section32Indicator = ;

            return _Payment;

        }

        private ReoProperty CreateReoProperty(IRealEstateOwned field, CAppData dataApp, string borrowerId)
        {
            ReoProperty _ReoProperty = new ReoProperty();

            _ReoProperty.ReoId = "REO_" + field.RecordId.ToString("N");
            _ReoProperty.BorrowerId = borrowerId;
            
            Guid[] matchedLiabilities = dataApp.FindMatchedLiabilities(field.RecordId);
            string str = "";
            foreach (Guid id in matchedLiabilities)
            {
                str += "LIA_" + id.ToString("N") + " ";
            }
            if (matchedLiabilities.Length > 0)
            {
                _ReoProperty.LiabilityId = str.TrimWhitespaceAndBOM();
            }
            _ReoProperty.StreetAddress = field.Addr;
            _ReoProperty.City = field.City;
            _ReoProperty.State = field.State;
            _ReoProperty.PostalCode = field.Zip;
            _ReoProperty.DispositionStatusType = ToMismo(field.StatT);
            _ReoProperty.GsePropertyType = ToMismo(field.TypeT);
            _ReoProperty.LienInstallmentAmount = field.MPmt_rep;
            _ReoProperty.LienUpbAmount = field.MAmt_rep;
            _ReoProperty.MaintenanceExpenseAmount = field.HExp_rep;
            _ReoProperty.MarketValueAmount = field.Val_rep;
            _ReoProperty.RentalIncomeGrossAmount = field.GrossRentI_rep;
            _ReoProperty.RentalIncomeNetAmount = field.NetRentI_rep;
            _ReoProperty.SubjectIndicator = ToMismo(field.IsSubjectProp);
            // _ReoProperty.StreetAddress2 = ;
            // _ReoProperty.CurrentResidenceIndicator = ;

            return _ReoProperty;

        }
        private E_ReoPropertyGsePropertyType ToMismo(E_ReoTypeT type)
        {
            switch (type)
            {
                case E_ReoTypeT._2_4Plx: return E_ReoPropertyGsePropertyType.TwoToFourUnitProperty;
                case E_ReoTypeT.ComNR: return E_ReoPropertyGsePropertyType.CommercialNonResidential;
                case E_ReoTypeT.ComR: return E_ReoPropertyGsePropertyType.HomeAndBusinessCombined;
                case E_ReoTypeT.Condo: return E_ReoPropertyGsePropertyType.Condominium;
                case E_ReoTypeT.Coop: return E_ReoPropertyGsePropertyType.Cooperative;
                case E_ReoTypeT.Farm: return E_ReoPropertyGsePropertyType.Farm;
                case E_ReoTypeT.Land: return E_ReoPropertyGsePropertyType.Land;
                case E_ReoTypeT.Mixed: return E_ReoPropertyGsePropertyType.MixedUseResidential;
                case E_ReoTypeT.Mobil: return E_ReoPropertyGsePropertyType.ManufacturedMobileHome;
                case E_ReoTypeT.Multi: return E_ReoPropertyGsePropertyType.MultifamilyMoreThanFourUnits;
                case E_ReoTypeT.SFR: return E_ReoPropertyGsePropertyType.SingleFamily;
                case E_ReoTypeT.Town: return E_ReoPropertyGsePropertyType.Townhouse;
                case E_ReoTypeT.Other: return E_ReoPropertyGsePropertyType.Undefined;
                default:
                    return E_ReoPropertyGsePropertyType.Undefined;
            }
        }

        private E_ReoPropertyDispositionStatusType ToMismo(E_ReoStatusT e_ReoStatusT)
        {
            switch (e_ReoStatusT)
            {
                case E_ReoStatusT.PendingSale: return E_ReoPropertyDispositionStatusType.PendingSale;
                case E_ReoStatusT.Rental: return E_ReoPropertyDispositionStatusType.RetainForRental;
                case E_ReoStatusT.Residence: return E_ReoPropertyDispositionStatusType.RetainForPrimaryOrSecondaryResidence;
                case E_ReoStatusT.Sale: return E_ReoPropertyDispositionStatusType.Sold;
                default:
                    return E_ReoPropertyDispositionStatusType.Undefined;

            }
        }

        private ProposedHousingExpense CreateProposedHousingExpense(E_ProposedHousingExpenseHousingExpenseType housingExpenseType, string amount)
        {
            if (string.IsNullOrEmpty(amount) || amount == "0.00" || amount == "$0.00") 
            {
                return null;
            }
            ProposedHousingExpense _ProposedHousingExpense = new ProposedHousingExpense();

            _ProposedHousingExpense.HousingExpenseType = housingExpenseType;
            _ProposedHousingExpense.PaymentAmount = amount;

            return _ProposedHousingExpense;

        }

        private Property CreateProperty()
        {
            Property _Property = new Property();

            // _Property.StreetAddress2 = ;
            _Property.AssessorsParcelIdentifier = m_dataLoan.sAssessorsParcelId;
            // _Property.AssessorsSecondParcelIdentifier = ;
            // _Property.BuildingStatusType = ;
            // _Property.AcquiredDate = ;
            // _Property.PlannedUnitDevelopmentIndicator = ;
            // _Property.AcreageNumber = ;
            // _Property.Country = ;

            _Property.StreetAddress = m_dataLoan.sSpAddr;
            _Property.City = m_dataLoan.sSpCity;
            _Property.State = m_dataLoan.sSpState;
            _Property.PostalCode = m_dataLoan.sSpZip;
            _Property.FinancedNumberOfUnits = m_dataLoan.sUnitsNum_rep;
            _Property.StructureBuiltYear = m_dataLoan.sYrBuilt;
            _Property.County = m_dataLoan.sSpCounty;

            _Property.LegalDescriptionList.Add(CreateLegalDescription());

            // _Property.ParsedStreetAddress = CreateParsedStreetAddress();
            // _Property.ValuationList.Add(CreateValuation());
            _Property.Details = CreateDetails();
            // _Property.HomeownersAssociation = CreateHomeownersAssociation();

            return _Property;

        }

        private HomeownersAssociation CreateHomeownersAssociation()
        {
            HomeownersAssociation _HomeownersAssociation = new HomeownersAssociation();

            // _HomeownersAssociation.Name = ;
            // _HomeownersAssociation.StreetAddress = ;
            // _HomeownersAssociation.StreetAddress2 = ;
            // _HomeownersAssociation.City = ;
            // _HomeownersAssociation.State = ;
            // _HomeownersAssociation.PostalCode = ;
            // _HomeownersAssociation.Country = ;
            // _HomeownersAssociation.County = ;

            // _HomeownersAssociation.ContactDetail = CreateContactDetail();
            return _HomeownersAssociation;

        }

        private Details CreateDetails()
        {
            if (string.IsNullOrEmpty(m_dataLoan.sProjNm))
            {
                return null;
            }

            Details _Details = new Details();

            // _Details.CondominiumIndicator = ;
            // _Details.CondominiumPudDeclarationsDescription = ;
            _Details.NfipFloodZoneIdentifier = m_dataLoan.sNfipFloodZoneId;
            // _Details.JudicialDistrictName = ;
            // _Details.JudicialDivisionName = ;
            // _Details.ManufacturedHomeIndicator = ;
            // _Details.OneToFourFamilyIndicator = ;
            _Details.ProjectName = m_dataLoan.sProjNm;
            // _Details.RecordingJurisdictionName = ;
            // _Details.RecordingJurisdictionType = ;
            // _Details.RecordingJurisdictionTypeOtherDescription = ;
            // _Details.PropertyUnincorporatedAreaName = ;

            return _Details;

        }

        private Valuation CreateValuation()
        {
            Valuation _Valuation = new Valuation();

            // _Valuation.MethodType = ;
            // _Valuation.MethodTypeOtherDescription = ;

            // _Valuation.AppraiserList.Add(CreateAppraiser());
            return _Valuation;

        }

        private Appraiser CreateAppraiser()
        {
            Appraiser _Appraiser = new Appraiser();

            // _Appraiser.Name = ;
            // _Appraiser.CompanyName = ;
            // _Appraiser.LicenseIdentifier = ;
            // _Appraiser.LicenseState = ;

            return _Appraiser;

        }

        private ParsedStreetAddress CreateParsedStreetAddress()
        {
            ParsedStreetAddress _ParsedStreetAddress = new ParsedStreetAddress();

            // _ParsedStreetAddress.ApartmentOrUnit = ;
            // _ParsedStreetAddress.DirectionPrefix = ;
            // _ParsedStreetAddress.DirectionSuffix = ;
            // _ParsedStreetAddress.BuildingNumber = ;
            // _ParsedStreetAddress.HouseNumber = ;
            // _ParsedStreetAddress.MilitaryApoFpo = ;
            // _ParsedStreetAddress.PostOfficeBox = ;
            // _ParsedStreetAddress.RuralRoute = ;
            // _ParsedStreetAddress.StreetName = ;
            // _ParsedStreetAddress.StreetSuffix = ;

            return _ParsedStreetAddress;

        }

        private LegalDescription CreateLegalDescription()
        {
            LegalDescription _LegalDescription = new LegalDescription();

            _LegalDescription.TextDescription = m_dataLoan.sSpLegalDesc;
            // _LegalDescription.Type = ;
            // _LegalDescription.TypeOtherDescription = ;

            return _LegalDescription;

        }

        private MortgageTerms CreateMortgageTerms()
        {
            MortgageTerms _MortgageTerms = new MortgageTerms();

            // _MortgageTerms.ArmTypeDescription = ;
            // _MortgageTerms.PaymentRemittanceDay = ;
            // _MortgageTerms.LendersContactPrimaryTelephoneNumber = ;

            _MortgageTerms.AgencyCaseIdentifier = m_dataLoan.sAgencyCaseNum;
            _MortgageTerms.BaseLoanAmount = m_dataLoan.sLAmtCalc_rep;
            _MortgageTerms.BorrowerRequestedLoanAmount = m_use2015DataLayer ? m_dataLoan.sFinalLAmt_rep : m_gfea.sFinalLAmt_rep;
            _MortgageTerms.LenderCaseIdentifier = m_dataLoan.sLenderCaseNum;
            _MortgageTerms.LoanAmortizationTermMonths = m_use2015DataLayer ? m_dataLoan.sTerm_rep : m_gfea.sTerm_rep;
            _MortgageTerms.LoanAmortizationType = ToMismo(m_use2015DataLayer ? m_dataLoan.sFinMethT : m_gfea.sFinMethT);
            _MortgageTerms.MortgageType = ToMismo(m_dataLoan.sLT);
            _MortgageTerms.OtherAmortizationTypeDescription = m_dataLoan.sFinMethDesc;
            _MortgageTerms.OtherMortgageTypeDescription = m_dataLoan.sLTODesc;
            _MortgageTerms.RequestedInterestRatePercent = m_use2015DataLayer ? m_dataLoan.sNoteIR_rep : m_gfea.sNoteIR_rep;
            _MortgageTerms.NoteRatePercent = m_use2015DataLayer ? m_dataLoan.sNoteIR_rep : m_gfea.sNoteIR_rep;
            _MortgageTerms.LenderLoanIdentifier = m_dataLoan.sLNm;
            _MortgageTerms.OriginalLoanAmount = m_use2015DataLayer ? m_dataLoan.sFinalLAmt_rep : m_gfea.sFinalLAmt_rep;
            _MortgageTerms.LoanEstimatedClosingDate = m_use2015DataLayer ? m_dataLoan.sEstCloseD_rep : m_gfea.sEstCloseD_rep;
            _MortgageTerms.CaseNumberAssignmentDate = m_dataLoan.sCaseAssignmentD_rep;

            return _MortgageTerms;

        }
        private E_MortgageTermsMortgageType ToMismo(E_sLT sLT)
        {
            switch (sLT)
            {
                case E_sLT.Conventional: return E_MortgageTermsMortgageType.Conventional;
                case E_sLT.FHA: return E_MortgageTermsMortgageType.FHA;
                case E_sLT.VA: return E_MortgageTermsMortgageType.VA;
                case E_sLT.UsdaRural: return E_MortgageTermsMortgageType.FarmersHomeAdministration;
                case E_sLT.Other: return E_MortgageTermsMortgageType.Other;
                default:
                    throw new UnhandledEnumException(sLT);
            }
        }
        private E_MortgageTermsLoanAmortizationType ToMismo(E_sFinMethT sFinMethT)
        {
            switch (sFinMethT)
            {
                case E_sFinMethT.Fixed: return E_MortgageTermsLoanAmortizationType.Fixed;
                case E_sFinMethT.ARM: return E_MortgageTermsLoanAmortizationType.AdjustableRate;
                case E_sFinMethT.Graduated: return E_MortgageTermsLoanAmortizationType.GraduatedPaymentMortgage;
                default:
                    throw new UnhandledEnumException(sFinMethT);
            }
        }
        private MiData CreateMiData()
        {
            MiData _MiData = new MiData();

            // _MiData.BorrowerMiTerminationDate = ;
            // _MiData.MiScheduledTerminationDate = ;
            // _MiData.MiFhaUpfrontPremiumAmount = ;
            // _MiData.MiCertificateIdentifier = ;
            // _MiData.MiCollectedNumberOfMonthsCount = ;
            // _MiData.MiCompanyName = ;
            // _MiData.MiCushionNumberOfMonthsCount = ;
            // _MiData.ScheduledAmortizationMidpointDate = ;
            // _MiData.MiDurationType = ;
            // _MiData.MiEscrowIncludedInAggregateIndicator = ;
            // _MiData.MiInitialPremiumAmount = ;
            // _MiData.MiInitialPremiumAtClosingType = ;
            // _MiData.MiInitialPremiumRateDurationMonths = ;
            // _MiData.MiInitialPremiumRatePercent = ;
            _MiData.MiLtvCutoffPercent = m_dataLoan.sProMInsCancelLtv_rep;
            // _MiData.MiLtvCutoffType = ;
            // _MiData.MiPremiumFinancedIndicator = ;
            // _MiData.MiPremiumFromClosingAmount = ;
            // _MiData.MiPremiumPaymentType = ;
            // _MiData.MiPremiumRefundableType = ;
            // _MiData.MiPremiumTermMonths = ;
            // _MiData.MiRenewalCalculationType = ;
            // _MiData.MiSourceType = ;

            // _MiData.MiPremiumTax = CreateMiPremiumTax();
            _MiData.MiRenewalPremiumList.Add(CreateMiRenewalPremium(E_MiRenewalPremiumSequence.First, m_dataLoan.sProMInsR_rep, m_dataLoan.sProMInsMon_rep));
            _MiData.MiRenewalPremiumList.Add(CreateMiRenewalPremium(E_MiRenewalPremiumSequence.Second, m_dataLoan.sProMInsR2_rep, m_dataLoan.sProMIns2Mon_rep));
            // _MiData.PaidTo = CreatePaidTo();
            return _MiData;

        }

        private MiRenewalPremium CreateMiRenewalPremium(E_MiRenewalPremiumSequence sequence, string rate, string term)
        {
            if (string.IsNullOrEmpty(rate) || rate == "0.000")
            {
                return null;
            }
            MiRenewalPremium _MiRenewalPremium = new MiRenewalPremium();

            // _MiRenewalPremium.MonthlyPaymentAmount = ;
            // _MiRenewalPremium.MonthlyPaymentRoundingType = ;
            _MiRenewalPremium.Rate = rate;
            _MiRenewalPremium.RateDurationMonths = term;
            _MiRenewalPremium.Sequence = sequence;

            return _MiRenewalPremium;

        }

        private MiPremiumTax CreateMiPremiumTax()
        {
            MiPremiumTax _MiPremiumTax = new MiPremiumTax();

            // _MiPremiumTax.CodeAmount = ;
            // _MiPremiumTax.CodePercent = ;
            // _MiPremiumTax.CodeType = ;

            return _MiPremiumTax;

        }

        private Mers CreateMers()
        {
            if (string.IsNullOrEmpty(m_dataLoan.sMersMin))
            {
                return null;
            }
            Mers _Mers = new Mers();

            _Mers.MersMinNumber = m_dataLoan.sMersMin;
            // _Mers.MersOriginalMortgageeOfRecordIndicator = ;
            // _Mers.MersMortgageeOfRecordIndicator = ;
            // _Mers.MersRegistrationDate = ;
            // _Mers.MersRegistrationIndicator = ;
            // _Mers.MersRegistrationStatusType = ;
            // _Mers.MersRegistrationStatusTypeOtherDescription = ;
            // _Mers.MersTaxNumberIdentifier = ;

            return _Mers;

        }

        private LoanQualification CreateLoanQualification()
        {
            LoanQualification _LoanQualification = new LoanQualification();

            // _LoanQualification.AdditionalBorrowerAssetsConsideredIndicator = ;
            // _LoanQualification.AdditionalBorrowerAssetsNotConsideredIndicator = ;

            return _LoanQualification;

        }

        private LoanPurpose CreateLoanPurpose()
        {
            LoanPurpose _LoanPurpose = new LoanPurpose();

            CAppData dataApp = m_dataLoan.GetAppData(0);

            _LoanPurpose.GseTitleMannerHeldDescription = dataApp.aManner;
            _LoanPurpose.Type = ToMismo(m_use2015DataLayer ? m_dataLoan.sLPurposeT : m_gfea.sLPurposeT);
            _LoanPurpose.OtherLoanPurposeDescription = m_dataLoan.sOLPurposeDesc;
            _LoanPurpose.PropertyLeaseholdExpirationDate = m_dataLoan.sLeaseHoldExpireD_rep;
            _LoanPurpose.PropertyRightsType = ToMismo(m_dataLoan.sEstateHeldT);
            _LoanPurpose.PropertyUsageType = ToMismo(dataApp.aOccT);

            _LoanPurpose.ConstructionRefinanceData = CreateConstructionRefinanceData();
            return _LoanPurpose;

        }
        private E_LoanPurposeType ToMismo(E_sLPurposeT sLPurposeT)
        {
            switch (sLPurposeT)
            {
                case E_sLPurposeT.Construct: return E_LoanPurposeType.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm: return E_LoanPurposeType.ConstructionToPermanent;
                case E_sLPurposeT.Other: return E_LoanPurposeType.Other;
                case E_sLPurposeT.Purchase: return E_LoanPurposeType.Purchase;
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    return E_LoanPurposeType.Refinance;
                default:
                    throw new UnhandledEnumException(sLPurposeT);

            }
        }
        private E_LoanPurposePropertyUsageType ToMismo(E_aOccT aOccT)
        {
            switch (aOccT)
            {
                case E_aOccT.PrimaryResidence: return E_LoanPurposePropertyUsageType.PrimaryResidence;
                case E_aOccT.SecondaryResidence: return E_LoanPurposePropertyUsageType.SecondHome;
                case E_aOccT.Investment: return E_LoanPurposePropertyUsageType.Investor;
                default:
                    throw new UnhandledEnumException(aOccT);
            }
        }

        private E_LoanPurposePropertyRightsType ToMismo(E_sEstateHeldT sEstateHeldT)
        {
            switch (sEstateHeldT)
            {
                case E_sEstateHeldT.FeeSimple: return E_LoanPurposePropertyRightsType.FeeSimple;
                case E_sEstateHeldT.LeaseHold: return E_LoanPurposePropertyRightsType.Leasehold;
                default:
                    throw new UnhandledEnumException(sEstateHeldT);
            }
        }
        private ConstructionRefinanceData CreateConstructionRefinanceData()
        {
            ConstructionRefinanceData _ConstructionRefinanceData = new ConstructionRefinanceData();
            E_sLPurposeT loanPurpose = m_use2015DataLayer ? m_dataLoan.sLPurposeT : m_gfea.sLPurposeT;
            switch (loanPurpose)
            {
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    _ConstructionRefinanceData.GseRefinancePurposeType = ToMismoGseRefinancePurposeType(m_dataLoan.sRefPurpose);
                    _ConstructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sSpAcqYr;
                    _ConstructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sSpLien_rep;
                    _ConstructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sSpOrigC_rep;
                    _ConstructionRefinanceData.RefinanceImprovementCostsAmount = m_dataLoan.sSpImprovC_rep;
                    _ConstructionRefinanceData.RefinanceImprovementsType = ToMismo(m_dataLoan.sSpImprovTimeFrameT);
                    _ConstructionRefinanceData.FreCashOutAmount = m_dataLoan.sProdCashoutAmt_rep;
                    _ConstructionRefinanceData.RefinanceProposedImprovementsDescription = m_dataLoan.sSpImprovDesc;
                    _ConstructionRefinanceData.SecondaryFinancingRefinanceIndicator = ToMismo(m_dataLoan.sPayingOffSubordinate);
                    break;
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:

                    _ConstructionRefinanceData.ConstructionImprovementCostsAmount = m_dataLoan.sLotImprovC_rep;
                    _ConstructionRefinanceData.ConstructionPurposeType = loanPurpose == E_sLPurposeT.Construct ? E_ConstructionRefinanceDataConstructionPurposeType.ConstructionOnly : E_ConstructionRefinanceDataConstructionPurposeType.ConstructionToPermanent;
                    _ConstructionRefinanceData.LandEstimatedValueAmount = m_dataLoan.sLotVal_rep;
                    _ConstructionRefinanceData.PropertyAcquiredYear = m_dataLoan.sLotAcqYr;
                    _ConstructionRefinanceData.PropertyExistingLienAmount = m_dataLoan.sLotLien_rep;
                    _ConstructionRefinanceData.PropertyOriginalCostAmount = m_dataLoan.sLotOrigC_rep;
                    break;
            };

            // _ConstructionRefinanceData.LandOriginalCostAmount = ;
            // _ConstructionRefinanceData.ConstructionPeriodInterestRatePercent = ;
            // _ConstructionRefinanceData.ConstructionPeriodNumberOfMonthsCount = ;
            // _ConstructionRefinanceData.FnmSecondMortgageFinancingOriginalPropertyIndicator = ;
            // _ConstructionRefinanceData.StructuralAlterationsConventionalAmount = ;
            // _ConstructionRefinanceData.NonStructuralAlterationsConventionalAmount = ;

            return _ConstructionRefinanceData;

        }
        private E_ConstructionRefinanceDataRefinanceImprovementsType ToMismo(E_sSpImprovTimeFrameT sSpImprovTimeFrameT)
        {
            switch (sSpImprovTimeFrameT)
            {
                case E_sSpImprovTimeFrameT.LeaveBlank: return E_ConstructionRefinanceDataRefinanceImprovementsType.Unknown;
                case E_sSpImprovTimeFrameT.Made: return E_ConstructionRefinanceDataRefinanceImprovementsType.Made;
                case E_sSpImprovTimeFrameT.ToBeMade: return E_ConstructionRefinanceDataRefinanceImprovementsType.ToBeMade;
                default:
                    throw new UnhandledEnumException(sSpImprovTimeFrameT);
            }
        }
        private E_ConstructionRefinanceDataGseRefinancePurposeType ToMismoGseRefinancePurposeType(string desc)
        {
            switch (desc.ToLower())
            {
                case "no cash-out rate/term": return E_ConstructionRefinanceDataGseRefinancePurposeType.ChangeInRateTerm;
                case "limited cash-out rate/term": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutLimited;
                case "cash-out/home improvement": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutHomeImprovement;
                case "cash-out/debt consolidation": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutDebtConsolidation;
                case "cash-out/other": return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                default:
                    E_sLPurposeT loanPurpose = m_use2015DataLayer ? m_dataLoan.sLPurposeT : m_gfea.sLPurposeT;
                    if (loanPurpose == E_sLPurposeT.Refin || loanPurpose == E_sLPurposeT.FhaStreamlinedRefinance || loanPurpose == E_sLPurposeT.VaIrrrl
                        || loanPurpose == E_sLPurposeT.HomeEquity)
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.NoCashOutOther;
                    else if (loanPurpose == E_sLPurposeT.RefinCashout)
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.CashOutOther;
                    else
                        return E_ConstructionRefinanceDataGseRefinancePurposeType.Undefined;

            }
        }

        private LoanProductData CreateLoanProductData()
        {
            LoanProductData _LoanProductData = new LoanProductData();


            _LoanProductData.Arm = CreateArm();
            _LoanProductData.LoanFeatures = CreateLoanFeatures();
            _LoanProductData.InterestOnly = CreateInterestOnly();
            _LoanProductData.PaymentAdjustmentList.Add(CreatePaymentAdjustment());
            _LoanProductData.RateAdjustmentList.Add(CreateRateAdjustment());

            // _LoanProductData.BuydownList.Add(CreateBuydown());
            _LoanProductData.Heloc = CreateHeloc();
            // _LoanProductData.PrepaymentPenaltyList.Add(CreatePrepaymentPenalty());
            return _LoanProductData;

        }

        private PrepaymentPenalty CreatePrepaymentPenalty()
        {
            PrepaymentPenalty _PrepaymentPenalty = new PrepaymentPenalty();

            // _PrepaymentPenalty.Percent = ;
            // _PrepaymentPenalty.PeriodSequenceIdentifier = ;
            // _PrepaymentPenalty.TermMonths = ;
            // _PrepaymentPenalty.TextDescription = ;

            return _PrepaymentPenalty;

        }

        private InterestOnly CreateInterestOnly()
        {
            InterestOnly _InterestOnly = new InterestOnly();

            _InterestOnly.MonthlyPaymentAmount = m_dataLoan.sProThisMPmt_rep;
            _InterestOnly.TermMonthsCount = m_use2015DataLayer ? m_dataLoan.sIOnlyMon_rep : m_gfea.sIOnlyMon_rep;

            return _InterestOnly;

        }

        private Heloc CreateHeloc()
        {
            Heloc _Heloc = new Heloc();

            // _Heloc.AnnualFeeAmount = ;
            // _Heloc.CreditCardAccountIdentifier = ;
            // _Heloc.CreditCardIndicator = ;
            // _Heloc.DailyPeriodicInterestRateCalculationType = ;
            // _Heloc.DailyPeriodicInterestRatePercent = ;
            // _Heloc.DrawPeriodMonthsCount = ;
            // _Heloc.FirstLienBookNumber = ;
            // _Heloc.FirstLienDate = ;
            // _Heloc.FirstLienHolderName = ;
            // _Heloc.FirstLienIndicator = ;
            // _Heloc.FirstLienInstrumentNumber = ;
            // _Heloc.FirstLienPageNumber = ;
            // _Heloc.FirstLienPrincipalBalanceAmount = ;
            // _Heloc.FirstLienRecordedDate = ;
            // _Heloc.InitialAdvanceAmount = ;
            // _Heloc.MaximumAprRate = ;
            // _Heloc.MinimumAdvanceAmount = ;
            // _Heloc.MinimumPaymentAmount = ;
            // _Heloc.MinimumPaymentPercent = ;
            // _Heloc.RepayPeriodMonthsCount = ;
            // _Heloc.ReturnedCheckChargeAmount = ;
            // _Heloc.StopPaymentChargeAmount = ;
            // _Heloc.TeaserTermEndDate = ;
            // _Heloc.TeaserTermMonthsCount = ;
            // _Heloc.TerminationFeeAmount = ;
            // _Heloc.TerminationPeriodMonthsCount = ;
            _Heloc.Dtc_HTCltv = m_dataLoan.sHcltvR_rep;

            return _Heloc;

        }

        private RateAdjustment CreateRateAdjustment()
        {
            RateAdjustment _RateAdjustment = new RateAdjustment();

            _RateAdjustment.FirstRateAdjustmentMonths = m_use2015DataLayer ? m_dataLoan.sRAdj1stCapMon_rep : m_gfea.sRAdj1stCapMon_rep;
            _RateAdjustment.SubsequentCapPercent = m_use2015DataLayer ? m_dataLoan.sRAdjCapR_rep : m_gfea.sRAdjCapR_rep;
            _RateAdjustment.SubsequentRateAdjustmentMonths = m_use2015DataLayer ? m_dataLoan.sRAdjCapMon_rep : m_gfea.sRAdjCapMon_rep;
            _RateAdjustment.FirstChangeCapRate = m_dataLoan.sR1stCapR_rep;
            _RateAdjustment.InitialCapPercent = m_use2015DataLayer ? m_dataLoan.sRAdj1stCapR_rep : m_gfea.sRAdj1stCapR_rep;

            // _RateAdjustment.CalculationType = ;
            // _RateAdjustment.DurationMonths = ;
            // _RateAdjustment.Percent = ;
            // _RateAdjustment.PeriodNumber = ;
            // _RateAdjustment.FirstChangeFloorPercent = ;
            // _RateAdjustment.FirstChangeFloorRate = ;
            // _RateAdjustment.FirstRateAdjustmentDate = ;

            return _RateAdjustment;

        }

        private PaymentAdjustment CreatePaymentAdjustment()
        {
            PaymentAdjustment _PaymentAdjustment = new PaymentAdjustment();

            // _PaymentAdjustment.FirstPaymentAdjustmentMonths = ;
            // _PaymentAdjustment.Amount = ;
            // _PaymentAdjustment.CalculationType = ;
            // _PaymentAdjustment.DurationMonths = ;
            // _PaymentAdjustment.Percent = ;
            // _PaymentAdjustment.PeriodicCapAmount = ;
            // _PaymentAdjustment.SubsequentPaymentAdjustmentMonths = ;
            // _PaymentAdjustment.FirstPaymentAdjustmentDate = ;
            // _PaymentAdjustment.LastPaymentAdjustmentDate = ;
            _PaymentAdjustment.PeriodicCapPercent = m_use2015DataLayer ? m_dataLoan.sPmtAdjCapR_rep : m_gfea.sPmtAdjCapR_rep;
            _PaymentAdjustment.PeriodNumber = m_use2015DataLayer ? m_dataLoan.sPmtAdjCapMon_rep : m_gfea.sPmtAdjCapMon_rep;

            return _PaymentAdjustment;

        }

        private LoanFeatures CreateLoanFeatures()
        {
            LoanFeatures _LoanFeatures = new LoanFeatures();

            // _LoanFeatures.BuydownTemporarySubsidyIndicator = ;
            // _LoanFeatures.CounselingConfirmationIndicator = ;
            // _LoanFeatures.DownPaymentOptionType = ;
            // _LoanFeatures.FnmProductPlanIdentifier = ;
            // _LoanFeatures.FreOfferingIdentifier = ;
            // _LoanFeatures.HelocMaximumBalanceAmount = ;
            // _LoanFeatures.HelocInitialAdvanceAmount = ;
            // _LoanFeatures.LenderSelfInsuredIndicator = ;
            // _LoanFeatures.LoanClosingStatusType = ;
            _LoanFeatures.LoanDocumentationType = ToMismo(m_dataLoan.sProdDocT);
            // _LoanFeatures.LoanRepaymentType = ;
            // _LoanFeatures.LoanScheduledClosingDate = ;
            // _LoanFeatures.MiCertificationStatusType = ;
            // _LoanFeatures.MiCompanyNameType = ;
            // _LoanFeatures.MiCoveragePercent = ;
            // _LoanFeatures.NameDocumentsDrawnInType = ;
            // _LoanFeatures.FullPrepaymentPenaltyOptionType = ;
            // _LoanFeatures.PrepaymentPenaltyTermMonths = ;
            // _LoanFeatures.PrepaymentRestrictionIndicator = ;
            // _LoanFeatures.ProductDescription = ;
            // _LoanFeatures.ServicingTransferStatusType = ;
            // _LoanFeatures.ConformingIndicator = ;
            // _LoanFeatures.DemandFeatureIndicator = ;
            // _LoanFeatures.EstimatedPrepaidDaysPaidByType = ;
            // _LoanFeatures.EstimatedPrepaidDaysPaidByOtherTypeDescription = ;
            // _LoanFeatures.GraduatedPaymentMultiplierFactor = ;
            // _LoanFeatures.LoanMaturityDate = ;
            // _LoanFeatures.PaymentFrequencyTypeOtherDescription = ;
            // _LoanFeatures.TimelyPaymentRateReductionIndicator = ;
            // _LoanFeatures.TimelyPaymentRateReductionPercent = ;
            // _LoanFeatures.CounselingConfirmationType = ;
            // _LoanFeatures.InitialPaymentRatePercent = ;
            // _LoanFeatures.RefundableApplicationFeeIndicator = ;
            // _LoanFeatures.GrowingEquityLoanPayoffYearsCount = ;

            _LoanFeatures.BalloonIndicator = ToMismo(m_dataLoan.sBalloonPmt);
            _LoanFeatures.BalloonLoanMaturityTermMonths = m_use2015DataLayer ? m_dataLoan.sDue_rep : m_gfea.sDue_rep;
            _LoanFeatures.EscrowWaiverIndicator = ToMismo(m_dataLoan.sWillEscrowBeWaived);
            _LoanFeatures.GsePropertyType = ToMismo(m_dataLoan.sGseSpT);
            _LoanFeatures.InterestOnlyTerm = m_use2015DataLayer ? m_dataLoan.sIOnlyMon_rep : m_gfea.sIOnlyMon_rep;
            _LoanFeatures.LienPriorityType = ToMismo(m_dataLoan.sLienPosT);
            _LoanFeatures.NegativeAmortizationLimitPercent = m_use2015DataLayer ? m_dataLoan.sPmtAdjMaxBalPc_rep : m_gfea.sPmtAdjMaxBalPc_rep;
            _LoanFeatures.PaymentFrequencyType = E_LoanFeaturesPaymentFrequencyType.Monthly;
            _LoanFeatures.ProductName = m_use2015DataLayer ? m_dataLoan.sLpTemplateNm : m_gfea.sLpTemplateNm;
            _LoanFeatures.ScheduledFirstPaymentDate = m_use2015DataLayer ? m_dataLoan.sSchedDueD1_rep : m_gfea.sSchedDueD1_rep;
            _LoanFeatures.RequiredDepositIndicator = ToMismo(m_dataLoan.sAprIncludesReqDeposit);
            _LoanFeatures.EstimatedPrepaidDays = m_use2015DataLayer ? m_dataLoan.sIPiaDy_rep : m_gfea.sIPiaDy_rep;
            _LoanFeatures.LoanOriginalMaturityTermMonths = m_use2015DataLayer ? m_dataLoan.sDue_rep : m_gfea.sDue_rep;
            _LoanFeatures.OriginalPrincipalAndInterestPaymentAmount = m_dataLoan.sProThisMPmt_rep;
            _LoanFeatures.OriginalBalloonTermMonths = m_use2015DataLayer ? m_dataLoan.sTerm_rep : m_gfea.sTerm_rep;
            switch (m_dataLoan.sAssumeLT)
            {
                case E_sAssumeLT.May:
                    _LoanFeatures.AssumabilityIndicator = E_YesNoIndicator.Y;
                    _LoanFeatures.ConditionsToAssumabilityIndicator = E_YesNoIndicator.N;
                    break;
                case E_sAssumeLT.MayNot:
                    _LoanFeatures.AssumabilityIndicator = E_YesNoIndicator.N;
                    break;
                case E_sAssumeLT.MaySubjectToCondition:
                    _LoanFeatures.AssumabilityIndicator = E_YesNoIndicator.Y;
                    _LoanFeatures.ConditionsToAssumabilityIndicator = E_YesNoIndicator.Y;
                    break;
            }

            switch (m_dataLoan.sPrepmtPenaltyT)
            {
                case E_sPrepmtPenaltyT.May:
                    _LoanFeatures.PrepaymentPenaltyIndicator = E_YesNoIndicator.Y;
                    break;
                case E_sPrepmtPenaltyT.WillNot:
                    _LoanFeatures.PrepaymentPenaltyIndicator = E_YesNoIndicator.N;
                    break;
                default:
                    _LoanFeatures.PrepaymentPenaltyIndicator = E_YesNoIndicator.Undefined;
                    break;
            }
            switch (m_dataLoan.sPrepmtRefundT)
            {
                case E_sPrepmtRefundT.May:
                    _LoanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YesNoIndicator.Y;
                    break;
                case E_sPrepmtRefundT.WillNot:
                    _LoanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YesNoIndicator.N;
                    break;
                default:
                    _LoanFeatures.PrepaymentFinanceChargeRefundableIndicator = E_YesNoIndicator.Undefined;
                    break;

            }

            _LoanFeatures.LateCharge = CreateLateCharge();
            // _LoanFeatures.NotePayTo = CreateNotePayTo();
            return _LoanFeatures;

        }

        private E_LoanFeaturesLoanDocumentationType ToMismo(E_sProdDocT sProdDocT)
        {
            switch (sProdDocT)
            {
                case E_sProdDocT.Full:
                    return E_LoanFeaturesLoanDocumentationType.FullDocumentation;
                case E_sProdDocT.Alt:
                    return E_LoanFeaturesLoanDocumentationType.Alternative;
                case E_sProdDocT.Streamline:
                    return E_LoanFeaturesLoanDocumentationType.StreamlineRefinance;
                case E_sProdDocT.SISA:
                    return E_LoanFeaturesLoanDocumentationType.NoVerificationOfStatedIncomeOrAssets;

                case E_sProdDocT.Light:
                    return E_LoanFeaturesLoanDocumentationType.Reduced;
                case E_sProdDocT.SIVA:
                case E_sProdDocT.VISA:
                case E_sProdDocT.NIVA:
                case E_sProdDocT.NINA:
                case E_sProdDocT.NISA:
                case E_sProdDocT.NINANE:
                case E_sProdDocT.NIVANE:
                case E_sProdDocT.VINA:
                    return E_LoanFeaturesLoanDocumentationType.Undefined; // Don't know how to map these type.
                case E_sProdDocT._12MoPersonalBankStatements:
                case E_sProdDocT._24MoPersonalBankStatements:
                case E_sProdDocT._12MoBusinessBankStatements:
                case E_sProdDocT._24MoBusinessBankStatements:
                case E_sProdDocT.OtherBankStatements:
                case E_sProdDocT._1YrTaxReturns:
                case E_sProdDocT.Voe:
                    return E_LoanFeaturesLoanDocumentationType.Alternative;
                case E_sProdDocT.AssetUtilization:
                case E_sProdDocT.DebtServiceCoverage:
                    return E_LoanFeaturesLoanDocumentationType.NoRatio;
                case E_sProdDocT.NoIncome:
                    return E_LoanFeaturesLoanDocumentationType.NoDocumentation;
                default:
                    throw new UnhandledEnumException(sProdDocT);
            }
            throw new NotImplementedException();
        }
        private E_LoanFeaturesLienPriorityType ToMismo(E_sLienPosT sLienPosT)
        {
            switch (sLienPosT)
            {
                case E_sLienPosT.First: return E_LoanFeaturesLienPriorityType.FirstLien;
                case E_sLienPosT.Second: return E_LoanFeaturesLienPriorityType.SecondLien;
                default:
                    throw new UnhandledEnumException(sLienPosT);
            }
        }

        private E_LoanFeaturesGsePropertyType ToMismo(E_sGseSpT sGseSpT)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.LeaveBlank: return E_LoanFeaturesGsePropertyType.Undefined;
                case E_sGseSpT.Attached: return E_LoanFeaturesGsePropertyType.Attached;
                case E_sGseSpT.Condominium: return E_LoanFeaturesGsePropertyType.Condominium;
                case E_sGseSpT.Cooperative: return E_LoanFeaturesGsePropertyType.Cooperative;
                case E_sGseSpT.Detached: return E_LoanFeaturesGsePropertyType.Detached;
                case E_sGseSpT.DetachedCondominium: return E_LoanFeaturesGsePropertyType.DetachedCondominium;
                case E_sGseSpT.HighRiseCondominium: return E_LoanFeaturesGsePropertyType.HighRiseCondominium;
                case E_sGseSpT.ManufacturedHomeCondominium: return E_LoanFeaturesGsePropertyType.ManufacturedHomeCondominium;
                case E_sGseSpT.ManufacturedHomeMultiwide: return E_LoanFeaturesGsePropertyType.ManufacturedHousingMultiWide;
                case E_sGseSpT.ManufacturedHousing: return E_LoanFeaturesGsePropertyType.ManufacturedHousing;
                case E_sGseSpT.ManufacturedHousingSingleWide: return E_LoanFeaturesGsePropertyType.ManufacturedHousingSingleWide;
                case E_sGseSpT.Modular: return E_LoanFeaturesGsePropertyType.Modular;
                case E_sGseSpT.PUD: return E_LoanFeaturesGsePropertyType.PUD;
                default:
                    throw new UnhandledEnumException(sGseSpT);
            }
        }

        private NotePayTo CreateNotePayTo()
        {
            NotePayTo _NotePayTo = new NotePayTo();

            // _NotePayTo.UnparsedName = ;
            // _NotePayTo.StreetAddress = ;
            // _NotePayTo.StreetAddress2 = ;
            // _NotePayTo.City = ;
            // _NotePayTo.State = ;
            // _NotePayTo.PostalCode = ;
            // _NotePayTo.Country = ;

            return _NotePayTo;

        }

        private LateCharge CreateLateCharge()
        {
            LateCharge _LateCharge = new LateCharge();

            // _LateCharge.Amount = ;
            _LateCharge.GracePeriod = m_dataLoan.sLateDays;
            // _LateCharge.LoanPaymentAmount = ;
            // _LateCharge.MaximumAmount = ;
            // _LateCharge.MinimumAmount = ;
            string lateCharge = m_dataLoan.sLateChargePc.Replace("%", ""); // 3/18/2011 dd - DocuTech only accept valid value.
            _LateCharge.Rate = lateCharge;
            //_LateCharge.Type = ;

            return _LateCharge;

        }

        private Buydown CreateBuydown()
        {
            Buydown _Buydown = new Buydown();

            // _Buydown.BaseDateType = ;
            // _Buydown.ChangeFrequencyMonths = ;
            // _Buydown.ContributorType = ;
            // _Buydown.ContributorTypeOtherDescription = ;
            // _Buydown.DurationMonths = ;
            // _Buydown.IncreaseRatePercent = ;
            // _Buydown.LenderFundingIndicator = ;
            // _Buydown.OriginalBalanceAmount = ;
            // _Buydown.PermanentIndicator = ;
            // _Buydown.SubsidyCalculationType = ;
            // _Buydown.TotalSubsidyAmount = ;

            // _Buydown.ContributorList.Add(CreateContributor());
            // _Buydown.SubsidyScheduleList.Add(CreateSubsidySchedule());
            return _Buydown;

        }

        private SubsidySchedule CreateSubsidySchedule()
        {
            SubsidySchedule _SubsidySchedule = new SubsidySchedule();

            // _SubsidySchedule.AdjustmentPercent = ;
            // _SubsidySchedule.PeriodIdentifier = ;
            // _SubsidySchedule.PeriodicPaymentEffectiveDate = ;
            // _SubsidySchedule.PeriodicPaymentSubsidyAmount = ;
            // _SubsidySchedule.PeriodicTerm = ;

            return _SubsidySchedule;

        }

        private Contributor CreateContributor()
        {
            Contributor _Contributor = new Contributor();

            // _Contributor.Amount = ;
            // _Contributor.Percent = ;
            // _Contributor.RoleType = ;
            // _Contributor.RoleTypeOtherDescription = ;

            return _Contributor;

        }

        private Arm CreateArm()
        {
            var financingMethod = m_use2015DataLayer ? m_dataLoan.sFinMethT : m_gfea.sFinMethT;
            if (financingMethod != E_sFinMethT.ARM)
            {
                return null;
            }
            Arm _Arm = new Arm();

            // _Arm.IndexCurrentValuePercent = ;
            // _Arm.IndexType = ;
            // _Arm.ConversionOptionIndicator = ;
            // _Arm.PaymentAdjustmentLifetimeCapAmount = ;
            // _Arm.PaymentAdjustmentLifetimeCapPercent = ;
            // _Arm.LifetimeCapRate = ;
            // _Arm.InterestRateRoundingFactor = ;
            // _Arm.InterestRateRoundingType = ;
            // _Arm.FnmTreasuryYieldForCurrentIndexDivisorNumber = ;
            // _Arm.FnmTreasuryYieldForIndexDivisorNumber = ;

            // _Arm.ConversionOption = CreateConversionOption();

            _Arm.IndexMarginPercent = m_dataLoan.sRAdjMarginR_rep;
            _Arm.QualifyingRatePercent = m_dataLoan.sQualIR_rep;
            _Arm.RateAdjustmentLifetimeCapPercent = m_use2015DataLayer ? m_dataLoan.sRAdjLifeCapR_rep : m_gfea.sRAdjLifeCapR_rep;
            _Arm.LifetimeFloorPercent = m_dataLoan.sRAdjFloorR_rep;
            return _Arm;

        }

        private ConversionOption CreateConversionOption()
        {
            ConversionOption _ConversionOption = new ConversionOption();

            // _ConversionOption.EndingChangeDatePeriodDescription = ;
            // _ConversionOption.FeeAmount = ;
            // _ConversionOption.NoteTermGreaterThanFifteenYearsAdditionalPercent = ;
            // _ConversionOption.NoteTermLessThanFifteenYearsAdditionalPercent = ;
            // _ConversionOption.PeriodEndDate = ;
            // _ConversionOption.PeriodStartDate = ;
            // _ConversionOption.StartingChangeDatePeriodDescription = ;

            return _ConversionOption;

        }

        private Liability CreateLiability(ILiabilityAlimony alimony, string borrowerId)
        {
            if (null == alimony)
            {
                return null;
            }
            Liability liability = new Liability();
            liability.Id = "LIA_" + alimony.RecordId.ToString("N");
            liability.BorrowerId = borrowerId;
            liability.Type = E_LiabilityType.Alimony;
            liability.AlimonyOwedToName = alimony.OwedTo;
            liability.RemainingTermMonths = alimony.RemainMons_rep;
            liability.MonthlyPaymentAmount = alimony.Pmt_rep;

            return liability;
        }

        private Liability CreateLiability(ILiabilityChildSupport childSupport, string borrowerId)
        {
            if (null == childSupport)
            {
                return null;
            }
            Liability liability = new Liability();
            liability.Id = "LIA_" + childSupport.RecordId.ToString("N");
            liability.BorrowerId = borrowerId;
            liability.Type = E_LiabilityType.ChildSupport;
            liability.RemainingTermMonths = childSupport.RemainMons_rep;
            liability.MonthlyPaymentAmount = childSupport.Pmt_rep;

            return liability;
        }

        private Liability CreateLiability(ILiabilityJobExpense jobExpense, string borrowerId)
        {
            if (null == jobExpense)
            {
                return null;
            }

            Liability liability = new Liability();
            liability.Id = "LIA_" + jobExpense.RecordId.ToString("N");
            liability.BorrowerId = borrowerId;
            liability.Type = E_LiabilityType.JobRelatedExpenses;
            liability.HolderName = jobExpense.ExpenseDesc;
            liability.MonthlyPaymentAmount = jobExpense.Pmt_rep;

            return liability;
        }
        private Liability CreateLiability(ILiabilityRegular field, string borrowerId)
        {
            Liability _Liability = new Liability();
            _Liability.Id = "LIA_" + field.RecordId.ToString("N");
            _Liability.BorrowerId = borrowerId;
            if (field.DebtT == E_DebtRegularT.Mortgage && field.MatchedReRecordId != Guid.Empty)
            {
                _Liability.ReoId = "REO_" + field.MatchedReRecordId.ToString("N");
            }
            _Liability.HolderStreetAddress = field.ComAddr;
            _Liability.HolderCity = field.ComCity;
            _Liability.HolderState = field.ComState;
            _Liability.HolderPostalCode = field.ComZip;
            // _Liability.AlimonyOwedToName = ;
            _Liability.AccountIdentifier = field.AccNum.Value;
            if (field.DebtT == E_DebtRegularT.Mortgage)
            {
                _Liability.ExclusionIndicator = ToMismo(field.WillBePdOff);
            }
            else
            {
                _Liability.ExclusionIndicator = ToMismo(field.NotUsedInRatio);
            }
            _Liability.HolderName = field.ComNm;
            _Liability.MonthlyPaymentAmount = field.Pmt_rep;
            _Liability.PayoffStatusIndicator = ToMismo(field.WillBePdOff);
            // _Liability.PayoffWithCurrentAssetsIndicator = ;
            _Liability.RemainingTermMonths = field.RemainMons_rep;
            _Liability.Type = ToMismo(field.DebtT);
            _Liability.UnpaidBalanceAmount = field.Bal_rep;
            // _Liability.SubjectLoanResubordinationIndicator = ;

            return _Liability;

        }
        private E_LiabilityType ToMismo(E_DebtRegularT e_DebtRegularT)
        {
            switch (e_DebtRegularT)
            {
                case E_DebtRegularT.Installment: return E_LiabilityType.Installment;
                case E_DebtRegularT.Mortgage: return E_LiabilityType.MortgageLoan;
                case E_DebtRegularT.Open: return E_LiabilityType.Open30DayChargeAccount;
                case E_DebtRegularT.Other: return E_LiabilityType.OtherLiability;
                case E_DebtRegularT.Revolving: return E_LiabilityType.Revolving;
                default:
                    return E_LiabilityType.Undefined;
            }
        }
        private InterviewerInformation CreateInterviewerInformation()
        {
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            InterviewerInformation _InterviewerInformation = new InterviewerInformation();

            // _InterviewerInformation.InterviewerApplicationSignedDate = ;

            _InterviewerInformation.InterviewersEmployerStreetAddress = preparer.StreetAddr;
            _InterviewerInformation.InterviewersEmployerCity = preparer.City;
            _InterviewerInformation.InterviewersEmployerState = preparer.State;
            _InterviewerInformation.InterviewersEmployerPostalCode = preparer.Zip;
            _InterviewerInformation.InterviewersTelephoneNumber = preparer.Phone;
            _InterviewerInformation.ApplicationTakenMethodType = ToMismo(m_dataLoan.GetAppData(0).aIntrvwrMethodT);
            _InterviewerInformation.InterviewersEmployerName = preparer.CompanyName;
            _InterviewerInformation.InterviewersName = preparer.PreparerName;

            return _InterviewerInformation;

        }
        private E_InterviewerInformationApplicationTakenMethodType ToMismo(E_aIntrvwrMethodT aIntrvwrMethodT)
        {
            switch (aIntrvwrMethodT)
            {
                case E_aIntrvwrMethodT.LeaveBlank: return E_InterviewerInformationApplicationTakenMethodType.Undefined;
                case E_aIntrvwrMethodT.FaceToFace: return E_InterviewerInformationApplicationTakenMethodType.FaceToFace;
                case E_aIntrvwrMethodT.ByMail: return E_InterviewerInformationApplicationTakenMethodType.Mail;
                case E_aIntrvwrMethodT.ByTelephone: return E_InterviewerInformationApplicationTakenMethodType.Telephone;
                case E_aIntrvwrMethodT.Internet: return E_InterviewerInformationApplicationTakenMethodType.Internet;
                default:
                    throw new UnhandledEnumException(aIntrvwrMethodT);
            }
        }
        private GovernmentReporting CreateGovernmentReporting()
        {
            GovernmentReporting _GovernmentReporting = new GovernmentReporting();

            // _GovernmentReporting.HmdaPurposeOfLoanType = ;
            // _GovernmentReporting.HmdaPreapprovalType = ;
            // _GovernmentReporting.HmdaHoepaLoanStatusIndicator = ;
            // _GovernmentReporting.HmdaRateSpreadPercent = ;

            return _GovernmentReporting;

        }

        private GovernmentLoan CreateGovernmentLoan()
        {
            if (m_dataLoan.sLT != E_sLT.FHA && m_dataLoan.sLT != E_sLT.VA)
            {
                return null;
            }
            GovernmentLoan _GovernmentLoan = new GovernmentLoan();


            _GovernmentLoan.FhaLoan = CreateFhaLoan();
            // _GovernmentLoan.FhaVaLoan = CreateFhaVaLoan();
            if (m_dataLoan.sLT == E_sLT.VA)
            {
                _GovernmentLoan.VaLoan = CreateVaLoan();
            }
            return _GovernmentLoan;

        }

        private VaLoan CreateVaLoan()
        {
            VaLoan _VaLoan = new VaLoan();

            CAppData dataApp = m_dataLoan.GetAppData(0);

            // _VaLoan.VaBorrowerCoBorrowerMarriedIndicator = ;
            _VaLoan.BorrowerFundingFeePercent = m_dataLoan.sFfUfmipR_rep;
            _VaLoan.VaEntitlementAmount = dataApp.aVaEntitleAmt_rep;
            _VaLoan.VaMaintenanceExpenseMonthlyAmount = m_dataLoan.sVaProMaintenancePmt_rep;
            _VaLoan.VaResidualIncomeAmount = dataApp.aVaFamilySupportBal_rep;
            _VaLoan.VaUtilityExpenseMonthlyAmount = m_dataLoan.sVaProUtilityPmt_rep;
            _VaLoan.VaEntitlementCodeIdentifier = dataApp.aVaEntitleCode;
            // _VaLoan.VaHouseholdSizeCount = ;

            return _VaLoan;

        }

        private FhaVaLoan CreateFhaVaLoan()
        {
            FhaVaLoan _FhaVaLoan = new FhaVaLoan();

            // _FhaVaLoan.BorrowerPaidFhaVaClosingCostsAmount = ;
            // _FhaVaLoan.BorrowerPaidFhaVaClosingCostsPercent = ;
            // _FhaVaLoan.GovernmentMortgageCreditCertificateAmount = ;
            // _FhaVaLoan.GovernmentRefinanceType = ;
            // _FhaVaLoan.OtherPartyPaidFhaVaClosingCostsAmount = ;
            // _FhaVaLoan.OtherPartyPaidFhaVaClosingCostsPercent = ;
            // _FhaVaLoan.PropertyEnergyEfficientHomeIndicator = ;
            // _FhaVaLoan.SellerPaidFhaVaClosingCostsPercent = ;
            // _FhaVaLoan.OriginatorIdentifier = ;

            return _FhaVaLoan;

        }
        private E_FhaLoanSectionOfActType ToMismoLoanSectionOfAct(string type)
        {
            switch (type.ToLower())
            {
                case "203(b)": return E_FhaLoanSectionOfActType._203B;
                case "203(b)2": return E_FhaLoanSectionOfActType._203B2;
                case "203(b)/251": return E_FhaLoanSectionOfActType._203B251;
                case "203(k)": return E_FhaLoanSectionOfActType._203K;
                case "203(k)/251": return E_FhaLoanSectionOfActType._203K251;
                case "234(c)": return E_FhaLoanSectionOfActType._234C;
                case "234(c)/251": return E_FhaLoanSectionOfActType._234C251;
                default: return E_FhaLoanSectionOfActType.Undefined;
            }
        }
        private FhaLoan CreateFhaLoan()
        {
            FhaLoan _FhaLoan = new FhaLoan();

            if (m_dataLoan.sLT == E_sLT.FHA)
            {
                _FhaLoan.FhaEnergyRelatedRepairsOrImprovementsAmount = m_dataLoan.sFHAEnergyEffImprov_rep;
                _FhaLoan.FhaMiPremiumRefundAmount = m_dataLoan.sFHA203kFHAMipRefund_rep;
                _FhaLoan.FhaUpfrontMiPremiumPercent = m_dataLoan.sFfUfmipR_rep;
                _FhaLoan.SectionOfActType = ToMismoLoanSectionOfAct(m_dataLoan.sFHAHousingActSection);

            }
            _FhaLoan.LenderIdentifier = m_dataLoan.sFHALenderIdCode;
            _FhaLoan.SponsorIdentifier = m_dataLoan.sFHASponsorAgentIdCode;

            // _FhaLoan.BorrowerFinancedFhaDiscountPointsAmount = ;
            // _FhaLoan.FhaAlimonyLiabilityTreatmentType = ;
            // _FhaLoan.FhaCoverageRenewalRatePercent = ;
            // _FhaLoan.FhaGeneralServicesAdminstrationCodeIdentifier = ;
            // _FhaLoan.FhaLimitedDenialParticipationIdentifier = ;
            // _FhaLoan.FhaRefinanceInterestOnExistingLienAmount = ;
            // _FhaLoan.FhaRefinanceOriginalExistingFhaCaseIdentifier = ;
            // _FhaLoan.FhaRefinanceOriginalExistingUpFrontMipAmount = ;
            // _FhaLoan.FhaUnderwriterComputerizedHomesUnderwritingSystemIdentifier = ;
            // _FhaLoan.HudAdequateAvailableAssetsIndicator = ;
            // _FhaLoan.HudAdequateEffectiveIncomeIndicator = ;
            // _FhaLoan.HudCreditCharacteristicsIndicator = ;
            // _FhaLoan.HudStableEffectiveIncomeIndicator = ;

            return _FhaLoan;

        }

        private EscrowAccountSummary CreateEscrowAccountSummary()
        {
            EscrowAccountSummary _EscrowAccountSummary = new EscrowAccountSummary();

            _EscrowAccountSummary.EscrowAggregateAccountingAdjustmentAmount = m_use2015DataLayer ? m_dataLoan.sAggregateAdjRsrv_rep : m_gfea.sAggregateAdjRsrv_rep;
            // _EscrowAccountSummary.EscrowCushionNumberOfMonthsCount = ;

            return _EscrowAccountSummary;

        }

        private Escrow CreateEscrow(string hudItem, E_EscrowItemType itemType, string otherDescription, decimal monthlyPremium, string monthsReserve, int itemProps)
        {
            if (monthlyPremium == 0)
            {
                return null;
            }

            Escrow _Escrow = new Escrow();

            decimal annualPremium = monthlyPremium * 12;

            _Escrow.AnnualPaymentAmount = m_dataLoan.m_convertLos.ToMoneyString(annualPremium, FormatDirection.ToRep);
            _Escrow.CollectedNumberOfMonthsCount = monthsReserve;
            _Escrow.ItemType = itemType;
            _Escrow.SpecifiedHud1LineNumber = hudItem;
            if (itemType == E_EscrowItemType.Other)
            {

                _Escrow.ItemTypeOtherDescription = otherDescription;
            }

            E_EscrowPaidByType paidBy;
            switch(LosConvert.GfeItemProps_Payer(itemProps))
            {
                case 0: // paid by borrower, out of pocket.
                case 1: // paid by borrower but he/she will finance it.
                    paidBy = E_EscrowPaidByType.Buyer;
                    break;
                case 2: // paid by seller
                    paidBy = E_EscrowPaidByType.Seller;
                    break;
                case 3: // paid by lender
                case 4: // paid by broker
                    paidBy = E_EscrowPaidByType.LenderPremium;
                    break;
                default:
                    Tools.LogError("Unable to determine paid by type.");
                    paidBy = E_EscrowPaidByType.Undefined;
                    break;
            }

            _Escrow.PaidByType = paidBy;
            
            _Escrow.MonthlyPaymentAmount = m_dataLoan.m_convertLos.ToMoneyString(monthlyPremium, FormatDirection.ToRep); ;

            int colIndex = -1;
            if (itemType == E_EscrowItemType.HazardInsurance)
            {
                colIndex = 1;
            }
            else if (itemType == E_EscrowItemType.DTC_MortgageInsurance)
            {
                colIndex = 2; // MI
            }
            else if (itemType == E_EscrowItemType.CountyPropertyTax)
            {
                colIndex = 0; // PROPERTY TAX
            }
            else if (itemType == E_EscrowItemType.SchoolPropertyTax)
            {
                colIndex = 4;
            }
            else if (itemType == E_EscrowItemType.FloodInsurance)
            {
                colIndex = 3; //FLOOD
            }
            else if (itemType == E_EscrowItemType.Other)
            {
                if (hudItem == "1007")
                {
                    colIndex = 5;
                }
                else if (hudItem == "1008")
                {
                    colIndex = 6;
                }
                else if (hudItem == "1010")
                {
                    colIndex = 7;
                }
                else if (hudItem == "1011")
                {
                    colIndex = 8;
                }
            }

            bool hasSchedDueD1 = false;
            DateTime sSchedDueD1 = DateTime.MinValue;

            try
            {
                sSchedDueD1 = m_dataLoan.sSchedDueD1.DateTimeForComputation;
                hasSchedDueD1 = true;
            }
            catch (CBaseException)
            {
                // It is okay to swallow because sSchedDueD1 is missing.
            }
            catch (FormatException)
            {
                // It is okay to swallow because sSchedDueD1 is missing.
            }
            if (hasSchedDueD1 && colIndex >= 0)
            {
                int[,] sInitialEscrowAcc = m_dataLoan.sInitialEscrowAcc;
                int index = 1;
                _Escrow.AccountSummary = CreateAccountSummary(sInitialEscrowAcc[0, colIndex]); // Set Cushion.
                for (int offset = 0; offset < 12; offset++)
                {
                    DateTime dueDate = sSchedDueD1.AddMonths(offset);
                    int nMonths = sInitialEscrowAcc[dueDate.Month, colIndex];
                    if (nMonths > 0)
                    {

                        decimal amount = (decimal)nMonths * monthlyPremium;
                        _Escrow.PaymentsList.Add(CreatePayments(m_dataLoan.m_convertLos.ToDateTimeString(dueDate), m_dataLoan.m_convertLos.ToMoneyString(amount, FormatDirection.ToRep), index.ToString()));
                        index++;
                    }
                }
            }
            // _Escrow.DueDate = ;
            // _Escrow.ItemTypeOtherDescription = ;
            // _Escrow.MonthlyPaymentRoundingType = ;
            // _Escrow.PaidByType = ;
            // _Escrow.PaymentFrequencyType = ;
            // _Escrow.PaymentFrequencyTypeOtherDescription = ;
            // _Escrow.PremiumAmount = ;
            // _Escrow.PremiumPaidByType = ;
            // _Escrow.PremiumPaymentType = ;
            // _Escrow.PremiumDurationMonthsCount = ;
            // _Escrow.SpecifiedHud1LineNumber = ;
            // _Escrow.DtcTotalMonthlyPaymentAmount = ;
            // _Escrow.DtcMiPrePaidIndicator = ;
            // _Escrow.DtcOtherEscrowGroupNumber = ;
            // _Escrow.DtcEscrowIsTaxIndicator = ;

            // _Escrow.AccountSummary = CreateAccountSummary();
            // _Escrow.PaymentsList.Add(CreatePayments());
            // _Escrow.PaidTo = CreatePaidTo();
            return _Escrow;

        }

        private Payments CreatePayments(string dueDate, string paymentAmount, string sequenceIdentifier)
        {
            Payments _Payments = new Payments();

            _Payments.DueDate = dueDate;
            _Payments.PaymentAmount = paymentAmount;
            _Payments.SequenceIdentifier = sequenceIdentifier;

            return _Payments;

        }

        private AccountSummary CreateAccountSummary(int cushionAmount)
        {
            AccountSummary _AccountSummary = new AccountSummary();

            // _AccountSummary.EscrowAggregateAccountingAdjustmentAmount = ;
            _AccountSummary.EscrowCushionNumberOfMonthsCount = cushionAmount.ToString();

            return _AccountSummary;

        }

        private DownPayment CreateDownPayment()
        {
            DownPayment _DownPayment = new DownPayment();

            // _DownPayment.Amount = ;
            // _DownPayment.SourceDescription = ;
            // _DownPayment.Type = ;

            return _DownPayment;

        }
        private Asset CreateAsset(IAssetRetirement retirement, string borrowerId)
        {
            if (retirement == null || retirement.IsValid == false)
            {
                return null;
            }
            Asset asset = new Asset();
            asset.BorrowerId = borrowerId;
            asset.Type = E_AssetType.RetirementFund;
            asset.CashOrMarketValueAmount = retirement.Val_rep;
            return asset;
        }
        private Asset CreateAsset(IAssetBusiness business, string borrowerId)
        {
            if (null == business || business.IsValid == false)
            {
                return null;
            }
            Asset _Asset = new Asset();
            _Asset.BorrowerId = borrowerId;
            _Asset.Type = E_AssetType.NetWorthOfBusinessOwned;
            _Asset.CashOrMarketValueAmount = business.Val_rep;
            return _Asset;
        }
        private Asset CreateAsset(IAssetLifeInsurance lifeIns, string borrowerId)
        {
            if (null == lifeIns || lifeIns.IsValid == false)
            {
                return null;
            }
            Asset asset = new Asset();
            asset.BorrowerId = borrowerId;
            asset.Type = E_AssetType.LifeInsurance;
            asset.CashOrMarketValueAmount = lifeIns.Val_rep;
            asset.LifeInsuranceFaceValueAmount = lifeIns.FaceVal_rep;
            return asset;
        }
        private Asset CreateAsset(IAssetCashDeposit cashDeposit, string borrowerId)
        {
            if (null == cashDeposit || cashDeposit.IsValid == false)
            {
                return null;
            }
            Asset asset = new Asset();
            asset.BorrowerId = borrowerId;
            asset.Type = E_AssetType.EarnestMoneyCashDepositTowardPurchase;
            asset.CashOrMarketValueAmount = cashDeposit.Val_rep;
            asset.HolderName = cashDeposit.Desc;
            return asset;
        }
        private Asset CreateAsset(IAssetRegular assetField, string borrowerId)
        {
            Asset _Asset = new Asset();

            _Asset.BorrowerId = borrowerId;
            _Asset.AccountIdentifier = assetField.AccNum.Value;
            _Asset.CashOrMarketValueAmount = assetField.Val_rep;
            _Asset.Type = ToMismo(assetField.AssetT);
            _Asset.HolderName = assetField.ComNm;
            _Asset.HolderStreetAddress = assetField.StAddr;
            _Asset.HolderCity = assetField.City;
            _Asset.HolderState = assetField.State;
            _Asset.HolderPostalCode = assetField.Zip;
            if (assetField.AssetT == E_AssetRegularT.Auto)
            {
                _Asset.AutomobileMakeDescription = assetField.Desc;
            }
            // _Asset.AutomobileModelYear = ;
            // _Asset.LifeInsuranceFaceValueAmount = ;
            // _Asset.OtherAssetTypeDescription = ;
            // _Asset.StockBondMutualFundShareCount = ;
            // _Asset.VerifiedIndicator = ;

            return _Asset;

        }
        private E_AssetType ToMismo(E_AssetRegularT assetRegularT)
        {
            switch (assetRegularT)
            {
                case E_AssetRegularT.Auto: return E_AssetType.Automobile;
                case E_AssetRegularT.Bonds: return E_AssetType.Bond;
                case E_AssetRegularT.Checking: return E_AssetType.CheckingAccount;
                case E_AssetRegularT.GiftFunds: return E_AssetType.GiftsNotDeposited;
                case E_AssetRegularT.Savings: return E_AssetType.SavingsAccount;
                case E_AssetRegularT.Stocks: return E_AssetType.Stock;
                case E_AssetRegularT.OtherIlliquidAsset: return E_AssetType.OtherNonLiquidAssets;
                case E_AssetRegularT.OtherLiquidAsset: return E_AssetType.OtherLiquidAssets;
                case E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets: return E_AssetType.PendingNetSaleProceedsFromRealEstateAssets;
                case E_AssetRegularT.GiftEquity: return E_AssetType.GiftsTotal;
                default:
                    return E_AssetType.Undefined;
            }
        }
        private AffordableLending CreateAffordableLending()
        {
            AffordableLending _AffordableLending = new AffordableLending();

            // _AffordableLending.FnmCommunityLendingProductName = ;
            // _AffordableLending.FnmCommunityLendingProductType = ;
            // _AffordableLending.FnmCommunityLendingProductTypeOtherDescription = ;
            // _AffordableLending.FnmCommunitySecondsIndicator = ;
            // _AffordableLending.FnmNeighborsMortgageEligibilityIndicator = ;
            // _AffordableLending.FreAffordableProgramIdentifier = ;
            // _AffordableLending.HudIncomeLimitAdjustmentFactor = ;
            // _AffordableLending.HudLendingIncomeLimitAmount = ;
            // _AffordableLending.HudMedianIncomeAmount = ;
            // _AffordableLending.MsaIdentifier = ;

            return _AffordableLending;

        }

        private AdditionalCaseData CreateAdditionalCaseData()
        {
            AdditionalCaseData _AdditionalCaseData = new AdditionalCaseData();


            // _AdditionalCaseData.MortgageScoreList.Add(CreateMortgageScore());
            _AdditionalCaseData.TransmittalData = CreateTransmittalData();
            return _AdditionalCaseData;

        }

        private TransmittalData CreateTransmittalData()
        {
            IPreparerFields preparer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            TransmittalData _TransmittalData = new TransmittalData();
            _TransmittalData.PropertyAppraisedValueAmount = m_dataLoan.sApprVal_rep;
            _TransmittalData.RateLockPeriodDays = m_dataLoan.sRLckdDays_rep;
            _TransmittalData.LoanOriginationCompanyID = preparer.CompanyLoanOriginatorIdentifier;
            // _TransmittalData.ArmsLengthIndicator = ;
            // _TransmittalData.BelowMarketSubordinateFinancingIndicator = ;
            // _TransmittalData.BuydownRatePercent = ;
            // _TransmittalData.CaseStateType = ;
            // _TransmittalData.CreditReportAuthorizationIndicator = ;
            // _TransmittalData.CurrentFirstMortgageHolderType = ;
            // _TransmittalData.LenderBranchIdentifier = ;
            // _TransmittalData.LenderRegistrationIdentifier = ;
            // _TransmittalData.PropertyEstimatedValueAmount = ;
            // _TransmittalData.LoanOriginationSystemLoanIdentifier = ;
            _TransmittalData.InvestorLoanIdentifier = m_dataLoan.sInvestorLockLoanNum;
            // _TransmittalData.InvestorInstitutionIdentifier = ;
            // _TransmittalData.CommitmentReferenceIdentifier = ;
            // _TransmittalData.ConcurrentOriginationIndicator = ;
            // _TransmittalData.ConcurrentOriginationLenderIndicator = ;
            // _TransmittalData.RateLockType = ;
            // _TransmittalData.RateLockRequestedExtensionDays = ;
            // _TransmittalData.DtcAusRecommendation = ;
            // _TransmittalData.DtcAusType = ;
            // _TransmittalData.DtcAusTypeOtherDesc = ;
            // _TransmittalData.DtcChannel = ;
            // _TransmittalData.DtcContractNumber = ;
            // _TransmittalData.DtcDuCaseIdLpAusKey = ;
            // _TransmittalData.DtcEnhancedFeeIndicator = ;
            // _TransmittalData.DtcHomeownershipEducationCertificateInFileIndicator = ;
            // _TransmittalData.DtcInterestedPartyContributions = ;
            // _TransmittalData.DtcLpDocClass = ;
            // _TransmittalData.DtcMortgageOriginatorType = ;
            // _TransmittalData.DtcNoOfMonthsReserves = ;
            // _TransmittalData.DtcRepresentativeCreditIndicatorScore = ;
            // _TransmittalData.DtcRiskAssessment = ;
            // _TransmittalData.DtcSourceOfFunds = ;
            // _TransmittalData.DtcVerifiedAssets = ;

            return _TransmittalData;

        }

        private MortgageScore CreateMortgageScore()
        {
            MortgageScore _MortgageScore = new MortgageScore();

            // _MortgageScore.Date = ;
            // _MortgageScore.Type = ;
            // _MortgageScore.TypeOtherDescription = ;
            // _MortgageScore.Value = ;

            return _MortgageScore;

        }

        private DataInformation CreateDataInformation()
        {
            DataInformation _DataInformation = new DataInformation();


            // _DataInformation.DataVersionList.Add(CreateDataVersion());
            return _DataInformation;

        }

        private EmbeddedFile CreateEmbeddedFile()
        {
            EmbeddedFile _EmbeddedFile = new EmbeddedFile();

            // _EmbeddedFile.Document = ;
            // _EmbeddedFile.Id = ;
            // _EmbeddedFile.Type = ;
            // _EmbeddedFile.Version = ;
            // _EmbeddedFile.Name = ;
            // _EmbeddedFile.EncodingType = ;
            // _EmbeddedFile.Description = ;
            // _EmbeddedFile.MimeType = ;

            return _EmbeddedFile;

        }

        private Key CreateKey()
        {
            Key _Key = new Key();

            // _Key.Name = ;
            // _Key.Value = ;

            return _Key;

        }

        private User CreateUser(string userName, string password)
        {
            User _User = new User();

            _User.UserName = userName;
            _User.Password = password;

            return _User;

        }
    }
}
