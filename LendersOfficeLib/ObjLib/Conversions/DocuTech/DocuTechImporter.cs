﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Admin;
using DocuTech;
using LendersOffice.com.conformx.www;
using LendersOffice.Conversions.DocuTech;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOffice.Conversions.DocuTech
{
    public class DocuTechImporter
    {
        public static bool Import(Guid sLId, out string response, AbstractUserPrincipal principal)
        {
            DocuTechImporter importer = new DocuTechImporter(sLId, E_DocumentsPackageType.InitialDisclosure);
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DocuTechImporter));
            BrokerDB.AutoLoginDocuTechInfo info = dataLoan.BrokerDB.DocuTechInfo;
            if (info == null)
            {
                response = "Docutech Auto-Login is not properly configured.";
                return true;
            }
            return importer.Import(info.UserName, info.Password.Value, info.PackageType, out response, principal);
        }

        public static bool Import(Guid sLId, string userName, string password, E_DocumentsPackageType packageType, out string response, AbstractUserPrincipal principal)
        {
            DocuTechImporter importer = new DocuTechImporter(sLId, packageType);
            return importer.Import(userName, password, packageType, out response, principal);
        }

        private bool Import(string userName, string password, E_DocumentsPackageType packageType, out string response, AbstractUserPrincipal principal)
        {
            StringBuilder log = new StringBuilder();
            log.AppendLine("DocuTech GetLoanData: retrieving DocuTech PostID...");
            try
            {
                string postID = "";
                EngineWS engine = new EngineWS();
                engine.Url = DocuTechExporter.DocuTechExportUrl(userName);

                string xmlRequest = null;
                try
                {
                    xmlRequest = m_exporter.Export(userName, password, packageType, principal);
                }
                catch (FeeDiscrepancyException e)
                {
                    response = e.UserMessage;
                    log.AppendLine("There was a discrepancy between Settlement Charge fees and GFE fees, aborting.");
                    log.AppendLine(response);
                    Tools.LogInfo(log.ToString());
                    return true;
                }
                log.AppendLine("DocuTech: " +Environment.NewLine+ xmlRequest);
                log.AppendLine(Environment.NewLine + "Response:" + Environment.NewLine);
                string responseXml = engine.Process(xmlRequest);
                log.AppendLine(responseXml);

                if (responseXml.Contains("EXCEPTIONS"))
                {
                    response = "Error: There were errors exporting to DocuTech for the PostID.";
                }
                else
                {
                    EMortgagePackage emortgagePackage = new EMortgagePackage();
                    emortgagePackage.ReadXml(System.Xml.XmlReader.Create(new System.IO.StringReader(responseXml)));
                    postID = emortgagePackage.PostId;

                    log.AppendLine(Environment.NewLine);
                    log.AppendLine("Attempting to import LoanData from DocuTech.");
                    log.AppendLine("EngineURL: '" + engine.Url + "'");
                    log.AppendLine("UserName: '" + userName + "'");
                    log.AppendLine("PostID: '" + postID + "'");
                    log.AppendLine(Environment.NewLine);

                    response = engine.GetLoanData(userName, password, postID);

                    log.AppendLine("Response from DocuTech: ");
                    log.AppendLine(response);
                }
                Tools.LogInfo(log.ToString());
                return response.StartsWith("Error:");
            }
            catch (Exception exc)
            {
                Tools.LogError("DocuTech GetLoanData: Error occurred while attempting to collect LoanData from DocuTech" + Environment.NewLine + log.ToString(), exc);
                response = exc.GetType().ToString() + " occurred. Check PB Logs.";
                return true;
            }
        }



        public static bool Import(Guid sLId, string postID, out string response)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(DocuTechImporter));
            BrokerDB.AutoLoginDocuTechInfo info = dataLoan.BrokerDB.DocuTechInfo;
            if (info == null)
            {
                response = "Docutech Auto-Login is not properly configured.";
                return true;
            }
            return Import(info.UserName, info.Password.Value, postID, out response);
        }

        public static bool Import(string userName, string password, string postID, out string response)
        {
            StringBuilder log = new StringBuilder();
            log.AppendLine("DocuTech GetLoanData: ");
            EngineWS engine = new EngineWS();
            engine.Url = DocuTechExporter.DocuTechExportUrl(userName);
            log.AppendLine("Attempting to import LoanData from DocuTech.");
            log.AppendLine("EngineURL: '" + engine.Url + "'");
            log.AppendLine("UserName: '" + userName + "'");
            log.AppendLine("PostID: '" + postID + "'");
            log.AppendLine(Environment.NewLine);

            response = engine.GetLoanData(userName, password, postID);
            log.AppendLine("Response from DocuTech: ");
            log.AppendLine(response);
            Tools.LogInfo(log.ToString());

            return response.StartsWith("Error:");
        }

        DocuTechExporter m_exporter;
        public DocuTechImporter(Guid sLId, E_DocumentsPackageType packageType)
        {
            m_exporter = new DocuTechExporter(sLId, packageType);
        }
    }
}
