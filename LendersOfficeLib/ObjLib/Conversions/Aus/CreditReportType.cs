﻿namespace LendersOffice.ObjLib.Conversions.Aus
{
    using DataAccess;

    /// <summary>
    /// Enumerates the options for getting a credit report with DU.
    /// </summary>
    public enum CreditReportType
    {
        /// <summary>
        /// Blank or unknown value.
        /// </summary>
        [OrderedDescription("")]
        Blank = 0,

        /// <summary>
        /// Order a new report.
        /// </summary>
        [OrderedDescription("Order a new credit report from a credit provider")]
        OrderNew = 1,

        /// <summary>
        /// Reissue a previous report using a CRA reissue ID.
        /// </summary>
        [OrderedDescription("Reissue an existing credit report from a credit provider")]
        Reissue = 2,

        /// <summary>
        /// Use a previous report that the AUS has on file.
        /// </summary>
        [OrderedDescription("Use credit report from previous submission")]
        UsePrevious = 3,

        /// <summary>
        /// For LPA: run without a credit report.
        /// </summary>
        [OrderedDescription("Run without credit report")]
        NoReport = 4
    }
}
