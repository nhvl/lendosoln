﻿namespace LendersOffice.ObjLib.Conversions.Aus
{
    /// <summary>
    /// An enumeration of the kinds of responses to send to the seamless AUS (DU and LPA) UI.
    /// </summary>
    public enum AusResponseStatus
    {
        /// <summary>
        /// An error has occurred in the response or processing in our code.
        /// </summary>
        Error,

        /// <summary>
        /// A login error occurred, which may require some additional handling.
        /// </summary>
        LoginError,

        /// <summary>
        /// The loan has audit errors that should be fixed before re-submitting the AUS request.
        /// </summary>
        AuditFailure,

        /// <summary>
        /// The order is processing and should be checked back on later.
        /// </summary>
        Processing,

        /// <summary>
        /// The order is finished successfully and has a response.
        /// </summary>
        Done
    }
}
