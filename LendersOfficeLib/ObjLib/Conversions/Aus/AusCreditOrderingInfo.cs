﻿namespace LendersOffice.ObjLib.Conversions.Aus
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using Integration.LoanProductAdvisor;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.Security;

    /// <summary>
    /// Holds ViewModel data which represents the common AUS credit information
    /// for whether to request a new credit report, use one on file, or reissue an existing report.
    /// </summary>
    public class AusCreditOrderingInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AusCreditOrderingInfo"/> class with no values initialized.
        /// </summary>
        /// <remarks>
        /// Allows deserialization.
        /// </remarks>
        public AusCreditOrderingInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AusCreditOrderingInfo"/> class.
        /// </summary>
        /// <param name="loanData">The loan data object to use in creating the model.</param>
        /// <param name="user">The user requesting the page.</param>
        /// <param name="aus">The type of AUS the credit is being ordered for.</param>
        public AusCreditOrderingInfo(CPageData loanData, AbstractUserPrincipal user, AUSType aus)
        {
            this.CraUserId = user.DuAutoCraId;
            this.HasCraAutoId = !string.IsNullOrEmpty(user.DuAutoCraId);

            // Loan Product Advisor (formerly Loan Prospector) doesn't use CRA credentials
            this.HasCraAutoLogin = aus == AUSType.LoanProspector || !string.IsNullOrEmpty(user.DuAutoCraLogin);
            this.HasCraAutoPassword = aus == AUSType.LoanProspector || !string.IsNullOrEmpty(user.DuAutoCraPassword);

            this.Applications = loanData.Apps.Select(app => new AusCreditAppData(app.aBNm, app.aCNm, app.aCreditReportId, app.aAppId));
            this.CreditReportOption = this.Applications.Any(app => !string.IsNullOrEmpty(app.CraResubmitId)) ? CreditReportType.UsePrevious : CreditReportType.OrderNew;

            if (aus == AUSType.LoanProspector)
            {
                var craInfo = LoadCraFromCreditReportForFreddie(loanData, user.BrokerId);
                this.CraProviderId = craInfo.Item1;

                if (string.IsNullOrEmpty(this.CraProviderId) && FileDBTools.DoesFileExist(E_FileDB.Normal, LoanSubmitRequest.FreddieCraKey(loanData.sLId)))
                {
                    var savedOrderInfo = LoanSubmitRequest.GetTemporaryCraInformation(loanData.sLId);
                    this.CraProviderId = savedOrderInfo.CraProviderId;
                }
            }
        }

        /// <summary>
        /// Gets or sets the ID of the credit provider selected on the dropdown.
        /// </summary>
        /// <value>CRA provider ID.</value>
        public string CraProviderId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user has an auto-login CRA ID in LendingQB.
        /// If true, the CRA selection will be disabled.
        /// </summary>
        /// <value>Whether the user has CRA auto-ID.</value>
        public bool HasCraAutoId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user has an auto-login CRA login name in LendingQB.
        /// If true, the CRA login field will be disabled.
        /// </summary>
        /// <value>Whether the user has CRA auto-login.</value>
        public bool HasCraAutoLogin
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user has an auto-login CRA login password in LendingQB.
        /// If true, the CRA password field will be disabled.
        /// </summary>
        /// <value>Whether the user has CRA auto-password.</value>
        public bool HasCraAutoPassword
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the selected Credit Report Option.
        /// </summary>
        /// <value>
        /// The credit report option chosen.
        /// </value>
        public CreditReportType CreditReportOption
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the automatic CRA user ID to use for logging in to the CRA.
        /// Comes from <see cref="AbstractUserPrincipal.DuAutoCraLogin"/>.
        /// </summary>
        /// <value>The saved CRA user ID for DU login.</value>
        public string CraUserId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the CRA password to use for authenticating with the CRA.
        /// Should only come from UI submissions, not be sent to the browser.
        /// </summary>
        /// <value>CRA password.</value>
        public string CraPassword
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a list of information from applications for displaying on the login page.
        /// </summary>
        /// <value>App data to show on the login page.</value>
        public IEnumerable<AusCreditAppData> Applications { get; set; }

        /// <summary>
        /// Loads credit report agency information from the last credit report on the loan file.
        /// </summary>
        /// <param name="loan">The loan file to get the credit report from.</param>
        /// <param name="brokerId">The broker ID, used for loading from the DB.</param>
        /// <returns>A Tuple with the first value being the last credit report's CRA ID for Freddie Mac, and the second value being a list of borrower information including report IDs.</returns>
        public static Tuple<string, List<TmpBorrower>> LoadCraFromCreditReportForFreddie(CPageData loan, Guid brokerId)
        {
            List<TmpBorrower> borrowerList = new List<TmpBorrower>();
            string lastCra = string.Empty;
            foreach (CAppData app in loan.Apps)
            {
                string borrowerName = app.aBNm_aCNm;

                string borrowerId = "B" + app.aAppId.ToString("N");
                SqlParameter[] parameters = { new SqlParameter("@ApplicationID", app.aAppId) };
                string referenceNumber = string.Empty;
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "RetrieveCreditReport", parameters))
                {
                    if (reader.Read())
                    {
                        referenceNumber = (string)reader["ExternalFileID"];
                        if (reader["CrAccProxyId"] is DBNull)
                        {
                            lastCra = reader["ComId"].ToString();
                        }
                        else
                        {
                            lastCra = reader["CrAccProxyId"].ToString();
                        }
                    }
                }

                borrowerList.Add(new TmpBorrower(borrowerId, borrowerName, referenceNumber));
            }

            return new Tuple<string, List<TmpBorrower>>(LoanProspectorCreditReportingCompanyMapping.GetLpCrcCode(lastCra), borrowerList);
        }

        /// <summary>
        /// A temporary borrower object to store information from a credit report.
        /// </summary>
        public class TmpBorrower
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="TmpBorrower"/> class.
            /// </summary>
            /// <param name="id">The borrower's ID.</param>
            /// <param name="name">The borrower's name.</param>
            /// <param name="reference">The borrower's credit reference.</param>
            public TmpBorrower(string id, string name, string reference)
            {
                this.BorrowerID = id;
                this.BorrowerName = name;
                this.Reference = reference;
            }

            /// <summary>
            /// Gets the borrower's ID.
            /// </summary>
            public string BorrowerID { get; private set; }

            /// <summary>
            /// Gets the borrower name.
            /// </summary>
            public string BorrowerName { get; private set; }

            /// <summary>
            /// Gets the borrower's credit reference number.
            /// </summary>
            public string Reference { get; private set; }
        }

        /// <summary>
        /// ViewModel data which represents AUS credit data from an app on the loan file.
        /// </summary>
        public class AusCreditAppData
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="AusCreditAppData"/> class.
            /// </summary>
            /// <param name="borrowerName">Name of the borrower on the application.</param>
            /// <param name="coborrowerName">Name of the co-borrower on the application.</param>
            /// <param name="resubmitId">The credit report resubmission ID.</param>
            /// <param name="appId">The application id of the application represented.</param>
            /// <param name="reorderApp">Whether the app's credit should be re-ordered.</param>
            public AusCreditAppData(string borrowerName, string coborrowerName, string resubmitId, Guid appId, bool reorderApp = false)
            {
                this.BorrowerName = borrowerName;
                this.CoborrowerName = coborrowerName;
                this.CraResubmitId = resubmitId;
                this.ApplicationId = appId;
                this.ReorderApplicationCredit = reorderApp;
            }

            /// <summary>
            /// Gets or sets the name of the borrower.
            /// </summary>
            /// <value>Borrower name.</value>
            public string BorrowerName
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the name of the co-borrower.
            /// </summary>
            /// <value>Co-borrower name.</value>
            public string CoborrowerName
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the resubmission ID to get an updated credit report from the CRA.
            /// </summary>
            /// <value>CRA resubmission ID.</value>
            public string CraResubmitId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets the application ID for identifying the application in the loan file.
            /// </summary>
            /// <value>Application identifier.</value>
            public Guid ApplicationId
            {
                get; set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the application has been selected to reorder credit (for LPA).
            /// </summary>
            /// <value>Whether to reorder credit for the application.</value>
            public bool ReorderApplicationCredit
            {
                get; set;
            }
        }
    }
}
