﻿namespace LendersOffice.Conversions.Mismo3
{
    /// <summary>
    /// Indicates whether a party represents a person or a legal entity.
    /// </summary>
    public enum EntityType
    {
        /// <summary>
        /// Represents a person.
        /// </summary>
        Individual,

        /// <summary>
        /// Represents a company or other legal entity.
        /// </summary>
        LegalEntity
    }
}
