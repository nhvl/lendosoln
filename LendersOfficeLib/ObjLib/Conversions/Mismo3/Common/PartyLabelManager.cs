﻿namespace LendersOffice.Conversions.Mismo3
{
    using System;
    using System.Collections.Generic;
    using System.Web.UI;
    using DataAccess;

    /// <summary>
    /// Manages a set of labels related to the parties of a loan.
    /// </summary>
    public class PartyLabelManager
    {
        /// <summary>
        /// Stores a set of labels related to loan parties. The key is the label itself, while
        /// the value is a <see cref="Pair" /> holding the <see cref="EntityType" /> and role.
        /// </summary>
        private Dictionary<string, Pair> labels;

        /// <summary>
        /// Initializes a new instance of the <see cref="PartyLabelManager" /> class.
        /// </summary>
        public PartyLabelManager()
        {
            this.labels = new Dictionary<string, Pair>();
        }

        /// <summary>
        /// Gets the labels as a read only dictionary.
        /// </summary>
        public IReadOnlyDictionary<string, Pair> Labels
        {
            get
            {
                return this.labels;
            }
        }

        /// <summary>
        /// Stores a new label in the label manager.
        /// </summary>
        /// <param name="label">The label to be stored.</param>
        /// <param name="role">The role of the party holding the label.</param>
        /// <param name="entityType">Indicates whether the party is an individual or legal entity.</param>
        public void StoreLabel(string label, Enum role, EntityType entityType)
        {
            this.ValidateRoleEnumType(role);
            this.StoreLabel(label, role.ToString(), entityType);
        }

        /// <summary>
        /// Stores a new label in the label manager.
        /// </summary>
        /// <param name="label">The label to be stored.</param>
        /// <param name="role">The role or type of the party holding the label.</param>
        /// <param name="entityType">Indicates whether the party is an individual or legal entity.</param>
        public void StoreLabel(string label, string role, EntityType entityType)
        {
            this.ValidateLabel(label);
            this.labels.Add(label, new Pair(role, entityType));
        }

        /// <summary>
        /// Retrieves the set of labels that correspond to the given entity type and role.
        /// </summary>
        /// <param name="role">The role that returned labels should have.</param>
        /// <param name="entityType">Indicates whether returned labels should be individuals or legal entities.</param>
        /// <returns>A list of labels that meet the criteria.</returns>
        public List<string> RetrieveLabels(Enum role, EntityType entityType)
        {
            this.ValidateRoleEnumType(role);
            return this.RetrieveLabels(role.ToString(), entityType);
        }

        /// <summary>
        /// Retrieves the set of labels that correspond to the given entity type and role.
        /// </summary>
        /// <param name="role">The role that returned labels should have.</param>
        /// <param name="entityType">Indicates whether returned labels should be individuals or legal entities.</param>
        /// <returns>A list of labels that meet the criteria.</returns>
        public List<string> RetrieveLabels(string role, EntityType entityType)
        {
            var labelSet = new List<string>();
            foreach (var kvp in this.labels)
            {
                if (string.Equals((string)kvp.Value.First, role, StringComparison.OrdinalIgnoreCase) &&
                    Enum.Equals((EntityType)kvp.Value.Second, entityType))
                {
                    labelSet.Add(kvp.Key);
                }
            }

            return labelSet;
        }

        /// <summary>
        /// Ensures the given label is unique in the label manager.
        /// </summary>
        /// <param name="label">The label to validate.</param>
        /// <exception cref="ArgumentException">Throws if the label already exists.</exception>
        private void ValidateLabel(string label)
        {
            if (this.labels.ContainsKey(label))
            {
                throw new ArgumentException("The given label already exists.");
            }
        }

        /// <summary>
        /// Ensures the given role is of type <see cref="E_AgentRoleT" /> or <see cref="Mismo3Specification.Version3Schema.PartyRoleBase" /> or <see cref="Mismo3Specification.Version4Schema.PartyRoleBase" />.
        /// </summary>
        /// <param name="role">The role to validate.</param>
        /// <exception cref="ArgumentException">Throws if the role is not an expected type.</exception>
        private void ValidateRoleEnumType(Enum role)
        {
            if (!(role is E_AgentRoleT) && !(role is Mismo3Specification.Version3Schema.PartyRoleBase) && !(role is Mismo3Specification.Version4Schema.PartyRoleBase))
            {
                throw new ArgumentException("The role must be of type E_AgentRoleT or PartyRoleBase.");
            }
        }
    }
}