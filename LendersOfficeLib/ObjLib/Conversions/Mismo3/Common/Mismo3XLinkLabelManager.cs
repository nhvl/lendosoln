﻿namespace LendersOffice.Conversions.Mismo3
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// This class contains all XLink labels used in the MISMO 3.4.
    /// </summary>
    public class Mismo3XLinkLabelManager
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Mismo3XLinkLabelManager"/> class.
        /// </summary>
        public Mismo3XLinkLabelManager()
        {
            this.PartyLabels = new PartyLabelManager();
            this.ReoAssetLabels = new Dictionary<string, string>();
            this.LoanLabels = new Dictionary<Guid, string>();
        }

        /// <summary>
        /// Gets a PARTY label manager.
        /// </summary>
        public PartyLabelManager PartyLabels { get; private set; }

        /// <summary>
        /// Gets a dictionary of REO labels.
        /// </summary>
        public Dictionary<string, string> ReoAssetLabels { get; private set; }

        /// <summary>
        /// Gets a dictionary of LOAN labels.
        /// </summary>
        public Dictionary<Guid, string> LoanLabels { get; private set; }
    }
}
