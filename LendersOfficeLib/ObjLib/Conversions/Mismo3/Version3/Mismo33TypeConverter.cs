﻿namespace LendersOffice.Conversions.Mismo3.Version3
{
    using System;
    using System.Reflection;
    using System.Text;
    using System.Xml.Serialization;
    using DataAccess;
    using Mismo3Specification;
    using Mismo3Specification.Version3Schema;
    using static Mismo3Specification.Mismo3Constants.TypeRegexes;
    using static Mismo3Specification.Mismo3Utilities;

    /// <summary>
    /// A collection of static methods to convert datalayer information into serializable MISMO elements.
    /// </summary>
    public static class MismoTypeConverter
    {
        /// <summary>
        /// Creates a MISMOAmount object with the given decimal and sensitivity values. If the
        /// currency URI is known, use the overloaded method with that value.
        /// </summary>
        /// <param name="amount">The decimal value for this amount object.</param>
        /// <param name="isSensitiveData">Indicates whether this element has sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOAmount object populated with the given data.</returns>
        public static MISMOAmount ToMismoAmount(string amount, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (string.IsNullOrEmpty(amount))
            {
                return null;
            }

            if (!MismoAmountRegex.IsMatch(amount))
            {
                throw new CBaseException(Mismo3Errors.InvalidFieldFormat, Mismo3Errors.InvalidFieldFormat + " ToMismoAmount: amount = " + amount);
            }

            //// Truncate fractions of a cent.
            if (FractionalCentsRegex.IsMatch(amount))
            {
                int extraCentsIndex = amount.IndexOf('.') + 3;
                amount = amount.Substring(0, extraCentsIndex);
            }

            var mismoAmount = new MISMOAmount();
            mismoAmount.Value = amount;
            mismoAmount.IsSensitive = isSensitiveData;
            mismoAmount.DisplaySensitive = displaySensitive;

            return mismoAmount;
        }

        /// <summary>
        /// Creates a MISMOAmount object with the given values, including the currency URI.
        /// </summary>
        /// <param name="amount">The decimal value for the amount object.</param>
        /// <param name="currencyURI">Indicates the currency type.</param>
        /// <param name="isSensitiveData">Indicates whether this element has sensitive data.</param>
        /// <returns>A MISMOAmount object populated with the given data.</returns>
        public static MISMOAmount ToMismoAmount(string amount, string currencyURI, bool isSensitiveData = false)
        {
            var mismoAmount = ToMismoAmount(amount, isSensitiveData);
            mismoAmount.CurrencyURI = currencyURI;

            return mismoAmount;
        }

        /// <summary>
        /// Creates a MISMODate object with the given values.
        /// </summary>
        /// <param name="date">The date value for the object, formatted for MISMO.</param>
        /// <param name="isSensitiveData">Indicates whether this element has sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMODate object populated with the given data.</returns>
        public static MISMODate ToMismoDate(string date, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (string.IsNullOrEmpty(date))
            {
                return null;
            }

            //// MISMODate_Base
            if (!MismoDateRegex.IsMatch(date))
            {
                throw new CBaseException(Mismo3Errors.InvalidFieldFormat, Mismo3Errors.InvalidFieldFormat + " ToMismoDate: date = " + date);
            }

            var mismoDate = new MISMODate();
            mismoDate.Value = date;
            mismoDate.IsSensitive = isSensitiveData;
            mismoDate.DisplaySensitive = displaySensitive;

            return mismoDate;
        }

        /// <summary>
        /// Creates a MISMOCount object with the given values.
        /// </summary>
        /// <param name="countValue">The count value as an integer.</param>
        /// <param name="convertor">The convertor to use to convert the given value to a count string.</param>
        /// <param name="isSensitiveData">Indicates whether this element contains sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOCount object populated with the given data.</returns>
        public static MISMOCount ToMismoCount(int countValue, LosConvert convertor, bool isSensitiveData = false, bool displaySensitive = true)
        {
            return ToMismoCount(convertor.ToCountString(countValue), null, isSensitiveData, displaySensitive);
        }

        /// <summary>
        /// Creates a MISMOCount object with the given values.
        /// </summary>
        /// <param name="countValue">The count value as an integer.</param>
        /// <param name="convertor">The convertor to use to convert the given value to a count string.</param>
        /// <param name="isSensitiveData">Indicates whether this element contains sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOCount object populated with the given data. Null if no int value.</returns>
        public static MISMOCount ToMismoCount(int? countValue, LosConvert convertor, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (!countValue.HasValue)
            {
                return null;
            }

            return ToMismoCount(countValue.Value, convertor, isSensitiveData, displaySensitive);
        }

        /// <summary>
        /// Creates a MISMOCount object with the given values.
        /// </summary>
        /// <param name="countValue">The count value, represented by an integer.</param>
        /// <param name="convertor">The convertor to use to convert the given value to a count string.
        /// Pass null if the given value was generated by LosConvert::ToCountString, such as is done in the data-layer for counts.</param>
        /// <param name="isSensitiveData">Indicates whether this element contains sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOCount object populated with the given data.</returns>
        public static MISMOCount ToMismoCount(string countValue, LosConvert convertor, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (string.IsNullOrEmpty(countValue))
            {
                return null;
            }

            int count;

            if (!int.TryParse(countValue, out count))
            {
                if (convertor != null && ConvertDecimalCountToInt(countValue, out count))
                {
                    countValue = convertor.ToCountString(count);
                }
                else
                {
                    return null;
                }
            }

            if (count < 0)
            {
                return null;
            }

            var mismoCount = new MISMOCount();
            mismoCount.Value = countValue;
            mismoCount.IsSensitive = isSensitiveData;
            mismoCount.DisplaySensitive = displaySensitive;

            return mismoCount;
        }

        /// <summary>
        /// Creates a MISMOCode object with the given values. If the effective date
        /// and owner URI are known, use the overloaded method with those values.
        /// </summary>
        /// <param name="code">The value of the MISMO code.</param>
        /// <param name="isSensitiveData">Indicates whether this element contains sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOCode object populated with the given data.</returns>
        public static MISMOCode ToMismoCode(string code, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (string.IsNullOrEmpty(code))
            {
                return null;
            }

            var mismoCode = new MISMOCode();
            mismoCode.Value = EnforceMISMOStringMaxLength(code);
            mismoCode.IsSensitive = isSensitiveData;
            mismoCode.DisplaySensitive = displaySensitive;

            return mismoCode;
        }

        /// <summary>
        /// Creates a MISMOCode object with the given values, including the effective
        /// date and owner URI.
        /// </summary>
        /// <param name="code">The value of the MISMO code.</param>
        /// <param name="codeEffectiveDate">The effective date of the code.</param>
        /// <param name="codeOwnerURI">A URI representing the owner of the code.</param>
        /// <param name="isSensitiveData">Indicates whether this element contains sensitive data.</param>
        /// <returns>A MISMOCode object populated with the given data.</returns>
        public static MISMOCode ToMismoCode(string code, string codeEffectiveDate, string codeOwnerURI, bool isSensitiveData = false)
        {
            var mismoCode = ToMismoCode(code, isSensitiveData);
            mismoCode.CodeEffectiveDate = codeEffectiveDate;
            mismoCode.CodeOwnerURI = codeOwnerURI;

            return mismoCode;
        }

        /// <summary>
        /// Creates a MISMODateTime object with the given values.
        /// </summary>
        /// <param name="dateTime">The date/time value for the object, formatted for MISMO.</param>
        /// <param name="ignoreTimeSegmentIndicator">Set to true if the value does not include time data.</param>
        /// <param name="isSensitiveData">Indicates whether this element has sensitive data.</param>
        /// <returns>A MISMODateTime object populated with the given data.</returns>
        public static MISMODatetime ToMismoDatetime(string dateTime, bool ignoreTimeSegmentIndicator, bool isSensitiveData = false)
        {
            if (string.IsNullOrEmpty(dateTime))
            {
                return null;
            }

            //// 6/2/2015 BB - 213975 MISMODatetime_Base
            if (!MismoDateTimeRegex.IsMatch(dateTime))
            {
                throw new CBaseException(Mismo3Errors.InvalidFieldFormat, Mismo3Errors.InvalidFieldFormat + " ToMismoDatetime: dateTime = " + dateTime);
            }

            var mismoDateTime = new MISMODatetime();
            mismoDateTime.Value = dateTime;
            mismoDateTime.IsSensitive = isSensitiveData;
            mismoDateTime.IgnoreTimeSegmentIndicator = ignoreTimeSegmentIndicator;

            return mismoDateTime;
        }

        /// <summary>
        /// Creates a MISMOString object with the given values. To set the lang attribute, use
        /// the overloaded method with that value.
        /// </summary>
        /// <param name="stringValue">The string value represented by this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOString object populated with the given data.</returns>
        public static MISMOString ToMismoString(string stringValue, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (string.IsNullOrEmpty(stringValue))
            {
                return null;
            }

            var mismoString = new MISMOString();
            mismoString.Value = EnforceMISMOStringMaxLength(stringValue);
            mismoString.IsSensitive = isSensitiveData;
            mismoString.DisplaySensitive = displaySensitive;

            return mismoString;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MISMOString" /> class.
        /// </summary>
        /// <param name="stringValue">The string value represented by this element.</param>
        /// <param name="language">The ISO 639-1 two character code value representing the language of the text.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOString object populated with the given data.</returns>
        public static MISMOString ToMismoString(string stringValue, string language, bool isSensitiveData = false)
        {
            var mismoString = ToMismoString(stringValue, isSensitiveData);
            mismoString.Lang = language;

            return mismoString;
        }

        /// <summary>
        /// Creates a MISMODay object with the given values.
        /// </summary>
        /// <param name="day">The day value for the object, formatted for MISMO.</param>
        /// <param name="isSensitiveData">Indicates whether this element has sensitive data.</param>
        /// <returns>A MISMODay object populated with the given data.</returns>
        public static MISMODay ToMismoDay(string day, bool isSensitiveData = false)
        {
            if (string.IsNullOrEmpty(day))
            {
                return null;
            }

            //// xsd:gDay, e.g. ---24 or ---24+13:00
            if (!MismoDayRegex.IsMatch(day))
            {
                throw new CBaseException(Mismo3Errors.InvalidFieldFormat, Mismo3Errors.InvalidFieldFormat + " ToMismoDay: day = " + day);
            }

            var mismoDay = new MISMODay();
            mismoDay.Value = day;
            mismoDay.IsSensitive = isSensitiveData;

            return mismoDay;
        }

        /// <summary>
        /// Creates a MISMOIdentifier object with the given values. If the effective date 
        /// and owner URI attributes are known, use the overloaded constructor with those values.
        /// </summary>
        /// <param name="identifier">The identifier held in this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOIdentifier object populated with the given data.</returns>
        public static MISMOIdentifier ToMismoIdentifier(string identifier, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (string.IsNullOrEmpty(identifier))
            {
                return null;
            }

            var mismoIdentifier = new MISMOIdentifier();
            mismoIdentifier.Value = EnforceMISMOStringMaxLength(identifier);
            mismoIdentifier.IsSensitive = isSensitiveData;
            mismoIdentifier.DisplaySensitive = displaySensitive;

            return mismoIdentifier;
        }

        /// <summary>
        /// Creates a MISMOIdentifier object with the given values, including the effective date
        /// and the owner URI.
        /// </summary>
        /// <param name="identifier">The identifier held in this element.</param>
        /// <param name="effectiveDate">The effective date of the identifier.</param>
        /// <param name="ownerURI">The owner of the identifier.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOIdentifier object populated with the given data.</returns>
        public static MISMOIdentifier ToMismoIdentifier(string identifier, string effectiveDate, string ownerURI, bool isSensitiveData = false)
        {
            var mismoIdentifier = ToMismoIdentifier(identifier, isSensitiveData);

            if (mismoIdentifier != null)
            {
                mismoIdentifier.IdentifierEffectiveDate = effectiveDate;
                mismoIdentifier.IdentifierOwnerURI = ownerURI;
            }

            return mismoIdentifier;
        }

        /// <summary>
        /// Creates a MISMOIndicator object with the given values.
        /// </summary>
        /// <param name="indicatorValue">The boolean value held in this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOIndicator object populated with the given data.</returns>
        public static MISMOIndicator ToMismoIndicator(bool indicatorValue, bool isSensitiveData = false, bool displaySensitive = true)
        {
            var mismoIndicator = new MISMOIndicator();
            mismoIndicator.booleanValue = indicatorValue;
            mismoIndicator.IsSensitive = isSensitiveData;
            mismoIndicator.DisplaySensitive = displaySensitive;

            return mismoIndicator;
        }

        /// <summary>
        /// Creates a MISMOIndicator object, parsing the boolean value from a string.
        /// </summary>
        /// <param name="indicatorValue">Case insensitive. "True" or "Y" map to true, "False" or "N" map to false.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOIndicator object populated with the given data.</returns>
        public static MISMOIndicator ToMismoIndicator(string indicatorValue, bool isSensitiveData = false)
        {
            if (string.IsNullOrEmpty(indicatorValue))
            {
                return null;
            }

            bool value;
            if (indicatorValue.Equals("true", StringComparison.OrdinalIgnoreCase)
                || indicatorValue.Equals("Y", StringComparison.OrdinalIgnoreCase)
                || indicatorValue.Equals("Yes", StringComparison.OrdinalIgnoreCase)
                || indicatorValue.Equals("1", StringComparison.OrdinalIgnoreCase))
            {
                value = true;
            }
            else if (indicatorValue.Equals("false", StringComparison.OrdinalIgnoreCase)
                || indicatorValue.Equals("N", StringComparison.OrdinalIgnoreCase)
                || indicatorValue.Equals("No", StringComparison.OrdinalIgnoreCase)
                || indicatorValue.Equals("0", StringComparison.OrdinalIgnoreCase))
            {
                value = false;
            }
            else
            {
                return null;
            }

            var mismoIndicator = ToMismoIndicator(value, isSensitiveData);
            return mismoIndicator;
        }

        /// <summary>
        /// Decays a TriState to a simple boolean.
        /// </summary>
        /// <param name="triState">The TriState boolean value held in this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOIndicator object representing the TriState boolean.</returns>
        public static MISMOIndicator ToMismoIndicator(E_TriState triState, bool isSensitiveData = false)
        {
            bool value;
            switch (triState)
            {
                case E_TriState.Yes:
                    value = true;
                    break;
                case E_TriState.No:
                    value = false;
                    break;
                case E_TriState.Blank:
                    return null;
                default:
                    throw new UnhandledEnumException(triState);
            }

            var mismoIndicator = ToMismoIndicator(value, isSensitiveData);
            return mismoIndicator;
        }

        /// <summary>
        /// Creates a MISMOMonth object with the given values.
        /// </summary>
        /// <param name="month">The month value for the object, formatted for MISMO.</param>
        /// <param name="isSensitiveData">Indicates whether this element has sensitive data.</param>
        /// <returns>A MISMOMonth object populated with the given data.</returns>
        public static MISMOMonth ToMismoMonth(string month, bool isSensitiveData = false)
        {
            if (string.IsNullOrEmpty(month))
            {
                return null;
            }

            //// xsd:gDay, e.g. --06 or --06+13:00
            if (!MismoMonthRegex.IsMatch(month))
            {
                throw new CBaseException(Mismo3Errors.InvalidFieldFormat, Mismo3Errors.InvalidFieldFormat + " ToMismoMonth: month = " + month);
            }

            var mismoMonth = new MISMOMonth();
            mismoMonth.Value = month;
            mismoMonth.IsSensitive = isSensitiveData;

            return mismoMonth;
        }

        /// <summary>
        /// Creates a MISMONumeric object with the given values.
        /// </summary>
        /// <param name="numericValue">The numeric value held in this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMONumeric object populated with the given data.</returns>
        public static MISMONumeric ToMismoNumeric(string numericValue, bool isSensitiveData = false)
        {
            if (string.IsNullOrEmpty(numericValue))
            {
                return null;
            }

            //// xsd:decimal, e.g. -1.23, 12678967.543233, +100000.00, 210
            if (!MismoNumericRegex.IsMatch(numericValue))
            {
                throw new CBaseException(Mismo3Errors.InvalidFieldFormat, Mismo3Errors.InvalidFieldFormat + " ToMismoNumeric: numericValue = " + numericValue);
            }

            var mismoNumeric = new MISMONumeric();
            mismoNumeric.Value = numericValue;
            mismoNumeric.IsSensitive = isSensitiveData;

            return mismoNumeric;
        }

        /// <summary>
        /// Creates a MISMONumericString object with the given values. Constraint: digits only, i.e. pattern must match (\d*).
        /// </summary>
        /// <param name="numericString">The numeric string value held in this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMONumericString object populated with the given data.</returns>
        public static MISMONumericString ToMismoNumericString(string numericString, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (string.IsNullOrEmpty(numericString))
            {
                return null;
            }

            //// Strip out any non-numeric characters. This is used for phone numbers, ssn, etc., to convey the digits w/o dash or other characters.
            var mismoNumericString = new MISMONumericString();
            mismoNumericString.Value = NonNumericCharacterRegex.Replace(numericString, string.Empty);
            mismoNumericString.IsSensitive = isSensitiveData;
            mismoNumericString.DisplaySensitive = displaySensitive;

            return mismoNumericString;
        }

        /// <summary>
        /// Creates a MISMOValue object with the given values. If the algorithm URI
        /// and lang attributes are known, use the overloaded constructor with those values.
        /// </summary>
        /// <param name="value">The value held by this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOValue object populated with the given data.</returns>
        public static MISMOValue ToMismoValue(string value, bool isSensitiveData = false, bool displaySensitive = true)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            var mismoValue = new MISMOValue();
            mismoValue.Value = EnforceMISMOStringMaxLength(value);
            mismoValue.IsSensitive = isSensitiveData;
            mismoValue.DisplaySensitive = displaySensitive;

            return mismoValue;
        }

        /// <summary>
        /// Creates a MISMOValue object with the given values. If the algorithm URI
        /// and lang attributes are known, use the overloaded constructor with those values.
        /// </summary>
        /// <param name="value">The value held by this element.</param>
        /// <param name="algorithmURI">The algorithm URI for this value.</param>
        /// <param name="language">The ISO 639-1 two character code value representing the language of the text.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOValue object populated with the given data.</returns>
        public static MISMOValue ToMismoValue(string value, string algorithmURI, string language, bool isSensitiveData = false)
        {
            var mismoValue = ToMismoValue(value, isSensitiveData);
            mismoValue.AlgorithmURI = algorithmURI;
            mismoValue.Lang = language;

            return mismoValue;
        }

        /// <summary>
        /// Creates a MISMOPercent object with the given values.
        /// </summary>
        /// <param name="percent">The percentage value held by this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A MISMOPercent object populated with the given data.</returns>
        public static MISMOPercent ToMismoPercent(string percent, bool isSensitiveData = false, bool displaySensitive = true)
        {
            decimal dec;
            if (string.IsNullOrEmpty(percent) || !decimal.TryParse(percent, out dec))
            {
                return null;
            }

            //// xsd:decimal, e.g. -1.23, 12678967.543233, +100000.00, 210
            if (!MismoPercentRegex.IsMatch(percent))
            {
                throw new CBaseException(Mismo3Errors.InvalidFieldFormat, Mismo3Errors.InvalidFieldFormat + " ToMismoPercent: percent = " + percent);
            }

            var mismoPercent = new MISMOPercent();
            mismoPercent.Value = percent;
            mismoPercent.IsSensitive = isSensitiveData;
            mismoPercent.DisplaySensitive = displaySensitive;

            return mismoPercent;
        }

        /// <summary>
        /// Creates a MISMOPercent object with the given values.
        /// </summary>
        /// <param name="percent">The percentage value held by this element.</param>
        /// <param name="convertor">The convertor to use to convert the given value to a percent string.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOPercent object populated with the given data.</returns>
        public static MISMOPercent ToMismoPercent(string percent, LosConvert convertor, bool isSensitiveData = false)
        {
            decimal convertedDecimal;
            if (string.IsNullOrWhiteSpace(percent) || !decimal.TryParse(convertor.SanitizedDecimalString(percent), out convertedDecimal))
            {
                return null;
            }

            string rateString = convertor.ToRateString(convertedDecimal);
            return ToMismoPercent(rateString, isSensitiveData);
        }

        /// <summary>
        /// Creates a MISMOSequenceNumber object with the given values.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number held in this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOSequenceNumber object populated with the given data.</returns>
        public static MISMOSequenceNumber ToMismoSequenceNumber(int sequenceNumber, bool isSensitiveData = false)
        {
            var mismoSequenceNumber = new MISMOSequenceNumber();
            mismoSequenceNumber.intValue = sequenceNumber;
            mismoSequenceNumber.IsSensitive = isSensitiveData;

            return mismoSequenceNumber;
        }

        /// <summary>
        /// Creates a MISMOTime object with the given values.
        /// </summary>
        /// <param name="time">The time value held by this element, formatted to MISMO standards.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOTime object populated with the given data.</returns>
        public static MISMOTime ToMismoTime(string time, bool isSensitiveData = false)
        {
            if (string.IsNullOrEmpty(time))
            {
                return null;
            }

            //// xsd:time, e.g. 05:00:00-03:00
            if (!MismoTimeRegex.IsMatch(time))
            {
                throw new CBaseException(Mismo3Errors.InvalidFieldFormat, Mismo3Errors.InvalidFieldFormat + " ToMismoTime: time = " + time);
            }

            var mismoTime = new MISMOTime();
            mismoTime.Value = time;
            mismoTime.IsSensitive = isSensitiveData;

            return mismoTime;
        }

        /// <summary>
        /// Creates a MISMOYear object with the given values.
        /// </summary>
        /// <param name="year">The year value held by this element, formatted to MISMO standards.</param>
        /// <param name="fieldName">The name of the LendingQB field being converted to MISMO.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <param name="isLenderExportMode">If in lender export mode, this method will not throw if year value is in an invalid format.</param>
        /// <returns>A MISMOYear object populated with the given data.</returns>
        public static MISMOYear ToMismoYear(string year, string fieldName, bool isSensitiveData = false, bool isLenderExportMode = false)
        {
            if (string.IsNullOrEmpty(year))
            {
                return null;
            }

            // sg 09/10/2015 - OPM 228509: Extract year from given date string
            DateTime parsedDate = new DateTime();
            if (DateTime.TryParse(year, new System.Globalization.CultureInfo("en-US"), System.Globalization.DateTimeStyles.NoCurrentDateDefault, out parsedDate) && parsedDate.Year > 1)
            {
                year = parsedDate.Year.ToString();
            }

            //// xsd:gYear, e.g. 2015
            if (!isLenderExportMode && !MismoYearRegex.IsMatch(year))
            {
                string errorMessage = Mismo3Errors.InvalidYearFormat(fieldName, year);
                throw new CBaseException(errorMessage, errorMessage);
            }

            var mismoYear = new MISMOYear();
            mismoYear.Value = year;
            mismoYear.IsSensitive = isSensitiveData;

            return mismoYear;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MISMOXML" /> class.
        /// </summary>
        /// <param name="data">The string value represented by this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOXML object populated with the given data.</returns>
        public static MISMOXML ToMismoXml(string data, bool isSensitiveData = false)
        {
            if (string.IsNullOrEmpty(data))
            {
                return null;
            }

            var mismoXml = new MISMOXML();
            var bytes = Encoding.UTF8.GetBytes(data);
            mismoXml.Value = Convert.ToBase64String(bytes);
            mismoXml.IsSensitive = isSensitiveData;

            return mismoXml;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MISMOXML" /> class.
        /// </summary>
        /// <param name="data">The string value represented by this element.</param>
        /// <param name="isSensitiveData">Indicates whether this element holds sensitive data.</param>
        /// <returns>A MISMOXML object populated with the given data.</returns>
        public static MISMOXML ToMismoXml(byte[] data, bool isSensitiveData = false)
        {
            if (data == null || data.Length == 0)
            {
                return null;
            }

            var mismoXml = new MISMOXML();
            mismoXml.Value = Convert.ToBase64String(data);
            mismoXml.IsSensitive = isSensitiveData;

            return mismoXml;
        }

        /// <summary>
        /// A method for simple encapsulation of MISMO Enum types. Takes an <see cref="Enum"/> value and creates a serializable <see cref="MISMOEnum{TEnum}"/> containing that value.
        /// If enumValue's <see cref="XmlEnumAttribute"/> has Name="" (Blank), this method returns null. 
        /// Throws <see cref="ArgumentException"/> if TEnum is not an Enum type, or if the enumValue parameter does not have <see cref="XmlEnumAttribute"/>.
        /// </summary>
        /// <param name="enumValue">The enumerated value to encapsulate in a <see cref="MISMOEnum{TEnum}"/>. Must be of an <see cref="Enum"/> type and have <see cref="XmlEnumAttribute"/> defined. </param>
        /// <param name="isSensitive">Whether or not the MISMO data point is sensitive.</param>
        /// <param name="displaySensitive">Whether or not the SensitiveIndicator attribute should be serialized for this element.</param>
        /// <typeparam name="TEnum">An <see cref="Enum"/> value.</typeparam>
        /// <returns>A <see cref="MISMOEnum{TEnum}"/></returns>
        public static MISMOEnum<TEnum> ToMismoEnum<TEnum>(TEnum enumValue, bool isSensitive = false, bool displaySensitive = true) where TEnum : struct
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("Mismo33Convert.cs:ToMismoEnum: Type of TEnum must be an Enum type. Type was " + typeof(TEnum));
            }

            XmlEnumAttribute attribute = typeof(TEnum).GetMember(enumValue.ToString())[0].GetCustomAttribute(typeof(XmlEnumAttribute)) as XmlEnumAttribute;

            if (attribute == null)
            {
                throw new ArgumentException("Mismo33Convert.cs:ToMismoEnum: Enum passed with no XmlEnumAttribute.");
            }
            else if (string.IsNullOrEmpty(attribute.Name))
            {
                return null;
            }

            return new MISMOEnum<TEnum>()
            {
                enumValue = enumValue,
                IsSensitive = isSensitive,
                DisplaySensitive = displaySensitive
            };
        }
    }
}
