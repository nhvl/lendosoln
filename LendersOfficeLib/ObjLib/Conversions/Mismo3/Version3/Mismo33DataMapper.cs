﻿// <copyright file="Mismo33DataMapper.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara, Isaac Ribakoff
//  Date:   January 15, 2015
// </summary>
namespace LendersOffice.Conversions.Mismo3.Version3
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.Core.Construction;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using Mismo3Specification.Version3Schema;
    using ObjLib.Hmda;
    using ObjLib.TRID2;
    using static MismoTypeConverter;

    /// <summary>
    /// Contains methods to convert LQB data into MISMO format.
    /// </summary>
    public static class Mismo33DataMapper
    {
        /// <summary>
        /// Creates an address enumeration object holding the given address type.
        /// </summary>
        /// <param name="addressType">The address type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{AddressBase}"/> object.</returns>
        public static MISMOEnum<AddressBase> ToAddressEnum(AddressBase addressType)
        {
            if (addressType == AddressBase.Blank)
            {
                return null;
            }

            var addressEnum = new MISMOEnum<AddressBase>();
            addressEnum.enumValue = addressType;
            addressEnum.IsSensitive = false;

            return addressEnum;
        }

        /// <summary>
        /// Creates an <see cref="MISMOEnum{AdjustmentRuleBase}" /> object from the given MISMO value.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to be converted.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{AdjustmentRuleBase}"/> object.</returns>
        public static MISMOEnum<AdjustmentRuleBase> ToAdjustmentRuleEnum(AdjustmentRuleBase mismoValue, bool displaySensitive = true)
        {
            if (mismoValue == AdjustmentRuleBase.Blank)
            {
                return null;
            }

            var adjustmentRuleEnum = new MISMOEnum<AdjustmentRuleBase>();
            adjustmentRuleEnum.enumValue = mismoValue;
            adjustmentRuleEnum.IsSensitive = false;
            adjustmentRuleEnum.DisplaySensitive = displaySensitive;

            return adjustmentRuleEnum;
        }

        /// <summary>
        /// Maps the prior loan type to an amortization base enum.
        /// </summary>
        /// <param name="amortType">The amortization type.</param>
        /// <param name="displaySensitive">Whether the element's sensitive indicator attribute should be serialized.</param>
        /// <returns>The AmortizationBase enum.</returns>
        public static MISMOEnum<AmortizationBase> ToAmortizationEnum(E_sLoanBeingRefinancedAmortizationT amortType, bool displaySensitive = true)
        {
            MISMOEnum<AmortizationBase> amortizationEnum = new MISMOEnum<AmortizationBase>();
            amortizationEnum.IsSensitive = false;
            amortizationEnum.DisplaySensitive = displaySensitive;
            switch (amortType)
            {
                case E_sLoanBeingRefinancedAmortizationT.Adjustable:
                    amortizationEnum.enumValue = AmortizationBase.AdjustableRate;
                    break;
                case E_sLoanBeingRefinancedAmortizationT.Fixed:
                    amortizationEnum.enumValue = AmortizationBase.Fixed;
                    break;
                case E_sLoanBeingRefinancedAmortizationT.LeaveBlank:
                    amortizationEnum.enumValue = AmortizationBase.Blank;
                    break;
                default:
                    throw new UnhandledEnumException(amortType);
            }

            return amortizationEnum;
        }

        /// <summary>
        /// Creates an <see cref="MISMOEnum{AmortizationBase}" /> object from the given LQB value and sensitivity indicator.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to MISMO.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{AmortizationBase}" /> object.</returns>
        public static MISMOEnum<AmortizationBase> ToAmortizationEnum(E_sFinMethT lqbValue, bool displaySensitive = true)
        {
            var amortizationEnum = new MISMOEnum<AmortizationBase>();
            amortizationEnum.enumValue = GetAmortizationBaseValue(lqbValue);
            amortizationEnum.IsSensitive = false;
            amortizationEnum.DisplaySensitive = displaySensitive;

            return amortizationEnum;
        }

        /// <summary>
        /// Converts an LQB enum into the appropriate AmortizationBase enumerated value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to an Amortization enumeration.</param>
        /// <returns>An AmortizationBase enumerated value.</returns>
        public static AmortizationBase GetAmortizationBaseValue(E_sFinMethT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sFinMethT.ARM:
                    return AmortizationBase.AdjustableRate;
                case E_sFinMethT.Fixed:
                    return AmortizationBase.Fixed;
                case E_sFinMethT.Graduated:
                    return AmortizationBase.GraduatedPaymentARM;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates an application taken method enumeration object from the given method.
        /// </summary>
        /// <param name="method">The application taken method.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ApplicationTakenMethodBase}" /> object.</returns>
        public static MISMOEnum<ApplicationTakenMethodBase> ToApplicationTakenMethodEnum(E_aIntrvwrMethodT method)
        {
            if (method == E_aIntrvwrMethodT.LeaveBlank)
            {
                return null;
            }

            var applicationTakenMethodEnum = new MISMOEnum<ApplicationTakenMethodBase>();
            applicationTakenMethodEnum.enumValue = GetApplicationTakenMethodBaseValue(method);
            applicationTakenMethodEnum.IsSensitive = false;

            return applicationTakenMethodEnum;
        }

        /// <summary>
        /// Retrieves the corresponding application taken method enumeration from the given method.
        /// </summary>
        /// <param name="method">The application taken method.</param>
        /// <returns>The corresponding value.</returns>
        public static ApplicationTakenMethodBase GetApplicationTakenMethodBaseValue(E_aIntrvwrMethodT method)
        {
            switch (method)
            {
                case E_aIntrvwrMethodT.ByMail:
                    return ApplicationTakenMethodBase.Mail;
                case E_aIntrvwrMethodT.ByTelephone:
                    return ApplicationTakenMethodBase.Telephone;
                case E_aIntrvwrMethodT.FaceToFace:
                    return ApplicationTakenMethodBase.FaceToFace;
                case E_aIntrvwrMethodT.Internet:
                    return ApplicationTakenMethodBase.Internet;
                default:
                    throw new UnhandledEnumException(method);
            }
        }

        /// <summary>
        /// Creates an asset enumeration object from the given asset type.
        /// </summary>
        /// <param name="assetType">The type of asset.</param>
        /// <returns>A serializable <see cref="MISMOEnum{AssetBase}" /> object.</returns>
        public static MISMOEnum<AssetBase> ToAssetEnum(AssetBase assetType)
        {
            var assetEnum = new MISMOEnum<AssetBase>();
            assetEnum.enumValue = assetType;
            assetEnum.IsSensitive = false;

            return assetEnum;
        }

        /// <summary>
        /// Creates an asset enumeration object from the given asset type.
        /// </summary>
        /// <param name="assetType">The type of asset.</param>
        /// <returns>A serializable <see cref="MISMOEnum{AssetBase}" /> object.</returns>
        public static MISMOEnum<AssetBase> ToAssetEnum(E_AssetRegularT assetType)
        {
            var assetEnum = new MISMOEnum<AssetBase>();
            assetEnum.enumValue = GetAssetBaseValue(assetType);
            assetEnum.IsSensitive = false;

            return assetEnum;
        }

        /// <summary>
        /// Retrieves the asset enumeration value corresponding to the given asset type.
        /// </summary>
        /// <param name="assetType">The type of asset.</param>
        /// <returns>The corresponding asset enumeration value.</returns>
        public static AssetBase GetAssetBaseValue(E_AssetRegularT assetType)
        {
            switch (assetType)
            {
                case E_AssetRegularT.Auto:
                    return AssetBase.Automobile;
                case E_AssetRegularT.Bonds:
                    return AssetBase.Bond;
                case E_AssetRegularT.BridgeLoanNotDeposited:
                    return AssetBase.BridgeLoanNotDeposited;
                case E_AssetRegularT.CertificateOfDeposit:
                    return AssetBase.CertificateOfDepositTimeDeposit;
                case E_AssetRegularT.Checking:
                    return AssetBase.CheckingAccount;
                case E_AssetRegularT.EmployerAssistance:
                    return AssetBase.Other;
                case E_AssetRegularT.GiftEquity:
                    return AssetBase.Other;
                case E_AssetRegularT.GiftFunds:
                    return AssetBase.GiftsNotDeposited;
                case E_AssetRegularT.Grant:
                    return AssetBase.GrantsNotDeposited;
                case E_AssetRegularT.IndividualDevelopmentAccount:
                    return AssetBase.Other;
                case E_AssetRegularT.LeasePurchaseCredit:
                    return AssetBase.Other;
                case E_AssetRegularT.MoneyMarketFund:
                    return AssetBase.MoneyMarketFund;
                case E_AssetRegularT.MutualFunds:
                    return AssetBase.MutualFund;
                case E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets:
                    return AssetBase.PendingNetSaleProceedsFromRealEstateAssets;
                case E_AssetRegularT.ProceedsFromSaleOfNonRealEstateAsset:
                    return AssetBase.Other;
                case E_AssetRegularT.ProceedsFromUnsecuredLoan:
                    return AssetBase.Other;
                case E_AssetRegularT.Savings:
                    return AssetBase.SavingsAccount;
                case E_AssetRegularT.SecuredBorrowedFundsNotDeposit:
                    return AssetBase.BridgeLoanNotDeposited;
                case E_AssetRegularT.StockOptions:
                    return AssetBase.Other;
                case E_AssetRegularT.Stocks:
                    return AssetBase.Stock;
                case E_AssetRegularT.SweatEquity:
                    return AssetBase.Other;
                case E_AssetRegularT.TradeEquityFromPropertySwap:
                    return AssetBase.Other;
                case E_AssetRegularT.TrustFunds:
                    return AssetBase.TrustAccount;
                case E_AssetRegularT.OtherIlliquidAsset:
                case E_AssetRegularT.OtherLiquidAsset:
                case E_AssetRegularT.OtherPurchaseCredit:
                    return AssetBase.Other;
                default:
                    throw new UnhandledEnumException(assetType);
            }
        }

        /// <summary>
        /// Converts the automated underwriting result to a string description.
        /// </summary>
        /// <param name="result">The automated underwriting result.</param>
        /// <returns>The string description of the result as a MISMOString.</returns>
        public static MISMOString ToAutomatedUnderwritingRecommendationDescription(E_sProd3rdPartyUwResultT result)
        {
            return ToMismoString(GetAutomatedUnderwritingRecommendationDescription(result));
        }

        /// <summary>
        /// Creates an attachment type object from the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{AttachmentBase}" /> object.</returns>
        public static MISMOEnum<AttachmentBase> ToAttachmentEnum(E_sGseSpT propertyType)
        {
            var attachmentEnum = new MISMOEnum<AttachmentBase>();
            attachmentEnum.enumValue = GetAttachmentBaseValue(propertyType);
            attachmentEnum.IsSensitive = false;

            if (attachmentEnum.enumValue == AttachmentBase.Blank)
            {
                return null;
            }

            return attachmentEnum;
        }

        /// <summary>
        /// Gets the attachment type value corresponding to the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding attachment base value.</returns>
        public static AttachmentBase GetAttachmentBaseValue(E_sGseSpT propertyType)
        {
            switch (propertyType)
            {
                case E_sGseSpT.Attached:
                case E_sGseSpT.Condominium:
                    return AttachmentBase.Attached;
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.Detached:
                case E_sGseSpT.DetachedCondominium:
                    return AttachmentBase.Detached;
                case E_sGseSpT.HighRiseCondominium:
                    return AttachmentBase.Attached;
                case E_sGseSpT.LeaveBlank:
                    return AttachmentBase.Blank;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return AttachmentBase.Detached;
                default:
                    throw new UnhandledEnumException(propertyType);
            }
        }

        /// <summary>
        /// Retrieves the string result corresponding to the given automated underwriting result.
        /// </summary>
        /// <param name="result">The automated underwriting result.</param>
        /// <returns>The string description of the result.</returns>
        public static string GetAutomatedUnderwritingRecommendationDescription(E_sProd3rdPartyUwResultT result)
        {
            switch (result)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible: 
                    return "ApproveEligible";
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible: 
                    return "ApproveIneligible";
                case E_sProd3rdPartyUwResultT.DU_EAIEligible: 
                    return "EAIEligible";
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible: 
                    return "EAIIEligible";
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible: 
                    return "EAIIIEligible";
                case E_sProd3rdPartyUwResultT.DU_ReferEligible: 
                    return "ReferEligible";
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible: 
                    return "ReferIneligible";
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                    return "ReferWithCautionIV";
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                    return "Accept";
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible: 
                    return "Refer";
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible: 
                    return "ReferWCaution";
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                    return "Accept";
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                    return "CautionEligibleForAMinus";
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                    return "C1Caution";
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                    return "Unknown";
                case E_sProd3rdPartyUwResultT.NA:
                    return string.Empty;
                case E_sProd3rdPartyUwResultT.OutOfScope:
                    return "OutofScope";
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible: 
                    return "ApproveEligible";
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible: 
                    return "ApproveIneligible";
                case E_sProd3rdPartyUwResultT.Total_ReferEligible: 
                    return "ReferEligible";
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible: 
                    return "ReferIneligible";
                default:
                    throw new UnhandledEnumException(result);
            }
        }

        /// <summary>
        /// Converts the automated underwriting result for a VA loan to a string description.
        /// </summary>
        /// <param name="result">The VA risk.</param>
        /// <returns>The string description of the result as a MISMOString.</returns>
        public static MISMOString ToAutomatedUnderwritingRecommendationDescription(E_sVaRiskT result)
        {
            return ToMismoString(GetAutomatedUnderwritingRecommendationDescription(result));
        }

        /// <summary>
        /// Retrieves the recommendation description based on the given VA risk.
        /// </summary>
        /// <param name="result">The VA risk.</param>
        /// <returns>The string description of the result.</returns>
        public static string GetAutomatedUnderwritingRecommendationDescription(E_sVaRiskT result)
        {
            switch (result)
            {
                case E_sVaRiskT.LeaveBlank:
                    return string.Empty;
                case E_sVaRiskT.Approve:
                    return "Approve";
                case E_sVaRiskT.Refer:
                    return "Refer";
                default:
                    throw new UnhandledEnumException(result);
            }
        }

        /// <summary>
        /// Creates an automated underwriting system enumeration object based on the indicators in the loan.
        /// </summary>
        /// <param name="isDesktopUnderwriter">Indicates whether the automated system is a desktop underwriter.</param>
        /// <param name="isLoanProspector">Indicates whether the automated system is a loan prospector.</param>
        /// <param name="isOther">Indicates whether the automated system is something else.</param>
        /// <returns>A serializable <see cref="MISMOEnum{AutomatedUnderwritingSystemBase}" /> object.</returns>
        public static MISMOEnum<AutomatedUnderwritingSystemBase> ToAutomatedUnderwritingSystemEnum(bool isDesktopUnderwriter, bool isLoanProspector, bool isOther)
        {
            var automatedUnderwritingSystemEnum = new MISMOEnum<AutomatedUnderwritingSystemBase>();
            automatedUnderwritingSystemEnum.enumValue = GetAutomatedUnderwritingSystemBaseValue(isDesktopUnderwriter, isLoanProspector, isOther);
            automatedUnderwritingSystemEnum.IsSensitive = false;

            return automatedUnderwritingSystemEnum;
        }

        /// <summary>
        /// Retrieves the system type corresponding to the indicators in the loan.
        /// </summary>
        /// <param name="isDesktopUnderwriter">Indicates whether the automated system is a desktop underwriter.</param>
        /// <param name="isLoanProspector">Indicates whether the automated system is a loan prospector.</param>
        /// <param name="isOther">Indicates whether the automated system is something else.</param>
        /// <returns>The corresponding system type.</returns>
        public static AutomatedUnderwritingSystemBase GetAutomatedUnderwritingSystemBaseValue(bool isDesktopUnderwriter, bool isLoanProspector, bool isOther)
        {
            if (isDesktopUnderwriter)
            {
                return AutomatedUnderwritingSystemBase.DesktopUnderwriter;
            }
            else if (isLoanProspector)
            {
                return AutomatedUnderwritingSystemBase.LoanProspector;
            }
            else if (isOther)
            {
                return AutomatedUnderwritingSystemBase.Other;
            }
            else
            {
                return AutomatedUnderwritingSystemBase.Blank;
            }
        }

        /// <summary>
        /// Creates a MISMO value holding the given automated underwriting system result value.
        /// </summary>
        /// <param name="isAccepted">Indicates whether the loan is rated Accepted.</param>
        /// <param name="isRefer">Indicates whether the loan is rated Refer.</param>
        /// <returns>A serializable MISMOValue with the given value.</returns>
        public static MISMOValue ToAutomatedUnderwritingSystemResultValue(bool isAccepted, bool isRefer)
        {
            return ToMismoValue(GetAutomatedUnderwritingSystemResultValue(isAccepted, isRefer));
        }

        /// <summary>
        /// Retrieves the automated underwriting system result value based on the result fields for the loan.
        /// </summary>
        /// <param name="isAccepted">Indicates whether the loan is rated Accepted.</param>
        /// <param name="isRefer">Indicates whether the loan is rated Refer.</param>
        /// <returns>The corresponding result value.</returns>
        public static string GetAutomatedUnderwritingSystemResultValue(bool isAccepted, bool isRefer)
        {
            if (isAccepted)
            {
                return "AA";
            }
            else if (isRefer)
            {
                return "Refer";
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{BorrowerClassificationBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{BorrowerClassificationBase}" /> object.</returns>
        public static MISMOEnum<BorrowerClassificationBase> ToBorrowerClassificationEnum(E_BorrowerModeT lqbValue, bool displaySensitive = true)
        {
            var borrowerClassificationEnum = new MISMOEnum<BorrowerClassificationBase>();
            borrowerClassificationEnum.enumValue = GetBorrowerClassificationBaseValue(lqbValue);
            borrowerClassificationEnum.IsSensitive = false;
            borrowerClassificationEnum.DisplaySensitive = displaySensitive;

            return borrowerClassificationEnum;
        }

        /// <summary>
        /// Retrieves the BorrowerClassification value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable BorrowerClassification value.</returns>
        public static BorrowerClassificationBase GetBorrowerClassificationBaseValue(E_BorrowerModeT lqbValue)
        {
            switch (lqbValue)
            {
                case E_BorrowerModeT.Borrower:
                    return BorrowerClassificationBase.Primary;
                case E_BorrowerModeT.Coborrower:
                    return BorrowerClassificationBase.Secondary;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{BorrowerRelationshipTitleBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{BorrowerRelationshipTitleBase}" /> object.</returns>
        public static MISMOEnum<BorrowerRelationshipTitleBase> ToBorrowerRelationshipTitleEnum(E_aRelationshipTitleT lqbValue)
        {
            if (lqbValue == E_aRelationshipTitleT.LeaveBlank)
            {
                return null;
            }

            var borrowerRelationshipTitleEnum = new MISMOEnum<BorrowerRelationshipTitleBase>();
            borrowerRelationshipTitleEnum.enumValue = GetBorrowerRelationshipTitleBaseValue(lqbValue);
            borrowerRelationshipTitleEnum.IsSensitive = false;

            return borrowerRelationshipTitleEnum;
        }

        /// <summary>
        /// Retrieves the BorrowerRelationshipTitle value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding BorrowerRelationshipTitle value.</returns>
        public static BorrowerRelationshipTitleBase GetBorrowerRelationshipTitleBaseValue(E_aRelationshipTitleT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aRelationshipTitleT.AHusbandAndWife:
                case E_aRelationshipTitleT.HusbandAndWife:
                    return BorrowerRelationshipTitleBase.AHusbandAndWife;
                case E_aRelationshipTitleT.AMarriedMan:
                    return BorrowerRelationshipTitleBase.AMarriedMan;
                case E_aRelationshipTitleT.AMarriedPerson:
                    return BorrowerRelationshipTitleBase.AMarriedPerson;
                case E_aRelationshipTitleT.AMarriedWoman:
                    return BorrowerRelationshipTitleBase.AMarriedWoman;
                case E_aRelationshipTitleT.AnUnmarriedMan:
                    return BorrowerRelationshipTitleBase.AnUnmarriedMan;
                case E_aRelationshipTitleT.AnUnmarriedPerson:
                    return BorrowerRelationshipTitleBase.AnUnmarriedPerson;
                case E_aRelationshipTitleT.AnUnmarriedWoman:
                    return BorrowerRelationshipTitleBase.AnUnmarriedWoman;
                case E_aRelationshipTitleT.DomesticPartners:
                    return BorrowerRelationshipTitleBase.AsDomesticPartners;
                case E_aRelationshipTitleT.ASingleMan:
                    return BorrowerRelationshipTitleBase.ASingleMan;
                case E_aRelationshipTitleT.ASinglePerson:
                    return BorrowerRelationshipTitleBase.ASinglePerson;
                case E_aRelationshipTitleT.ASingleWoman:
                    return BorrowerRelationshipTitleBase.ASingleWoman;
                case E_aRelationshipTitleT.AWidow:
                    return BorrowerRelationshipTitleBase.AWidow;
                case E_aRelationshipTitleT.AWidower:
                    return BorrowerRelationshipTitleBase.AWidower;
                case E_aRelationshipTitleT.AWifeAndHusband:
                case E_aRelationshipTitleT.WifeAndHusband:
                    return BorrowerRelationshipTitleBase.AWifeAndHusband;
                case E_aRelationshipTitleT.HerHusband:
                    return BorrowerRelationshipTitleBase.HerHusband;
                case E_aRelationshipTitleT.HisWife:
                    return BorrowerRelationshipTitleBase.HisWife;
                case E_aRelationshipTitleT.NotApplicable:
                    return BorrowerRelationshipTitleBase.NotApplicable;
                case E_aRelationshipTitleT.AMarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AMarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ChiefExecutiveOfficer:
                case E_aRelationshipTitleT.GeneralPartner:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityProperty:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityPropertyWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenants:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenantsWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsByTheEntirety:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsInCommon:
                case E_aRelationshipTitleT.HusbandAndWifeAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.HusbandAndWifeTenancyByTheEntirety:
                case E_aRelationshipTitleT.PersonalGuarantor:
                case E_aRelationshipTitleT.President:
                case E_aRelationshipTitleT.Secretary:
                case E_aRelationshipTitleT.TenancyByEntirety:
                case E_aRelationshipTitleT.TenantsByTheEntirety:
                case E_aRelationshipTitleT.Treasurer:
                case E_aRelationshipTitleT.Trustee:
                case E_aRelationshipTitleT.VicePresident:
                case E_aRelationshipTitleT.WifeAndHusbandAsCommunityProperty:
                case E_aRelationshipTitleT.WifeAndHusbandAsJointTenants:
                case E_aRelationshipTitleT.WifeAndHusbandAsTenantsInCommon:
                case E_aRelationshipTitleT.Other:
                    return BorrowerRelationshipTitleBase.Other;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates a borrower residency enumeration object based on the given residency value.
        /// </summary>
        /// <param name="currentResident">Indicates whether the borrower currently resides at this residence.</param>
        /// <returns>A serializable <see cref="MISMOEnum{BorrowerResidencyBase}" /> object.</returns>
        public static MISMOEnum<BorrowerResidencyBase> ToBorrowerResidencyEnum(BorrowerResidencyBase currentResident)
        {
            var borrowerResidencyEnum = new MISMOEnum<BorrowerResidencyBase>();
            borrowerResidencyEnum.enumValue = currentResident;
            borrowerResidencyEnum.IsSensitive = false;

            return borrowerResidencyEnum;
        }

        /// <summary>
        /// Creates a borrower residency basis enumeration object containing the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for their current residence.</param>
        /// <returns>A serializable <see cref="MISMOEnum{BorrowerResidencyBasisBase}" /> object.</returns>
        public static MISMOEnum<BorrowerResidencyBasisBase> ToBorrowerResidencyBasisEnum(E_aBAddrT residencyType)
        {
            var borrowerResidencyBasisEnum = new MISMOEnum<BorrowerResidencyBasisBase>();
            borrowerResidencyBasisEnum.enumValue = GetBorrowerResidencyBasisBaseValue(residencyType);
            borrowerResidencyBasisEnum.IsSensitive = false;

            return borrowerResidencyBasisEnum;
        }

        /// <summary>
        /// Retrieves the borrower residency basis value corresponding to the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for their current residence.</param>
        /// <returns>The corresponding borrower residency basis value.</returns>
        public static BorrowerResidencyBasisBase GetBorrowerResidencyBasisBaseValue(E_aBAddrT residencyType)
        {
            switch (residencyType)
            {
                case E_aBAddrT.Own:
                    return BorrowerResidencyBasisBase.Own;
                case E_aBAddrT.Rent:
                    return BorrowerResidencyBasisBase.Rent;
                case E_aBAddrT.LivingRentFree:
                    return BorrowerResidencyBasisBase.LivingRentFree;
                case E_aBAddrT.LeaveBlank:
                    return BorrowerResidencyBasisBase.Unknown;
                default:
                    throw new UnhandledEnumException(residencyType);
            }
        }

        /// <summary>
        /// Creates a borrower residency basis enumeration object containing the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for a past residence.</param>
        /// <returns>A serializable <see cref="MISMOEnum{BorrowerResidencyBasisBase}" /> object.</returns>
        public static MISMOEnum<BorrowerResidencyBasisBase> ToBorrowerResidencyBasisEnum(E_aBPrev1AddrT residencyType)
        {
            var borrowerResidencyBasisEnum = new MISMOEnum<BorrowerResidencyBasisBase>();
            borrowerResidencyBasisEnum.enumValue = GetBorrowerResidencyBasisBaseValue(residencyType);
            borrowerResidencyBasisEnum.IsSensitive = false;

            return borrowerResidencyBasisEnum;
        }

        /// <summary>
        /// Retrieves the borrower residency basis value corresponding to the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for their past residence.</param>
        /// <returns>The corresponding borrower residency basis value.</returns>
        public static BorrowerResidencyBasisBase GetBorrowerResidencyBasisBaseValue(E_aBPrev1AddrT residencyType)
        {
            switch (residencyType)
            {
                case E_aBPrev1AddrT.Own:
                    return BorrowerResidencyBasisBase.Own;
                case E_aBPrev1AddrT.Rent:
                    return BorrowerResidencyBasisBase.Rent;
                case E_aBPrev1AddrT.LivingRentFree:
                    return BorrowerResidencyBasisBase.LivingRentFree;
                case E_aBPrev1AddrT.LeaveBlank:
                    return BorrowerResidencyBasisBase.Unknown;
                default:
                    throw new UnhandledEnumException(residencyType);
            }
        }

        /// <summary>
        /// Creates a borrower residency basis enumeration object containing the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for their past residence.</param>
        /// <returns>A serializable <see cref="MISMOEnum{BorrowerResidencyBasisBase}" /> object.</returns>
        public static MISMOEnum<BorrowerResidencyBasisBase> ToBorrowerResidencyBasisEnum(E_aBPrev2AddrT residencyType)
        {
            var borrowerResidencyBasisEnum = new MISMOEnum<BorrowerResidencyBasisBase>();
            borrowerResidencyBasisEnum.enumValue = GetBorrowerResidencyBasisBaseValue(residencyType);
            borrowerResidencyBasisEnum.IsSensitive = false;

            return borrowerResidencyBasisEnum;
        }

        /// <summary>
        /// Retrieves the borrower residency basis value corresponding to the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for their past residence.</param>
        /// <returns>The corresponding borrower residency basis value.</returns>
        public static BorrowerResidencyBasisBase GetBorrowerResidencyBasisBaseValue(E_aBPrev2AddrT residencyType)
        {
            switch (residencyType)
            {
                case E_aBPrev2AddrT.Own:
                    return BorrowerResidencyBasisBase.Own;
                case E_aBPrev2AddrT.Rent:
                    return BorrowerResidencyBasisBase.Rent;
                case E_aBPrev2AddrT.LivingRentFree:
                    return BorrowerResidencyBasisBase.LivingRentFree;
                case E_aBPrev2AddrT.LeaveBlank:
                    return BorrowerResidencyBasisBase.Unknown;
                default:
                    throw new UnhandledEnumException(residencyType);
            }
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{BuydownBaseDateBase}" /> for a buydown based on the given base date.
        /// </summary>
        /// <param name="buydownBaseDate">The base date of the buydown.</param>
        /// <returns>A serializable <see cref="MISMOEnum{BuydownBaseDateBase}" /> object.</returns>
        public static MISMOEnum<BuydownBaseDateBase> ToBuydownBaseDateEnum(BuydownBaseDateBase buydownBaseDate)
        {
            var buydownBaseDateEnum = new MISMOEnum<BuydownBaseDateBase>();
            buydownBaseDateEnum.enumValue = buydownBaseDate;
            buydownBaseDateEnum.IsSensitive = false;

            return buydownBaseDateEnum;
        }

        /// <summary>
        /// Creates a serializable enum representing the start period for first-year analysis.
        /// </summary>
        /// <param name="startFrom">The point from which analysis will begin.</param>
        /// <returns>A serializable enum.</returns>
        public static MISMOEnum<LqbCdEscrowFirstYearAnalysisStartFromBase> ToLqbCdEscrowFirstYearAnalysisStartFromEnum(TridClosingDisclosureCalculateEscrowAccountFromT startFrom)
        {
            if (startFrom == TridClosingDisclosureCalculateEscrowAccountFromT.NA)
            {
                return null;
            }

            var cdEscrowFirstYearAnalysisStartFromBase = new MISMOEnum<LqbCdEscrowFirstYearAnalysisStartFromBase>();
            cdEscrowFirstYearAnalysisStartFromBase.enumValue = GetLqbCdEscrowFirstYearAnalysisStartFromBaseValue(startFrom);
            cdEscrowFirstYearAnalysisStartFromBase.IsSensitive = false;

            return cdEscrowFirstYearAnalysisStartFromBase;
        }

        /// <summary>
        /// Retrieves the serializable value corresponding to the start period for first-year analysis.
        /// </summary>
        /// <param name="startFrom">The point from which analysis will begin.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbCdEscrowFirstYearAnalysisStartFromBase GetLqbCdEscrowFirstYearAnalysisStartFromBaseValue(TridClosingDisclosureCalculateEscrowAccountFromT startFrom)
        {
            switch (startFrom)
            {
                case TridClosingDisclosureCalculateEscrowAccountFromT.Consummation:
                    return LqbCdEscrowFirstYearAnalysisStartFromBase.Consummation;
                case TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate:
                    return LqbCdEscrowFirstYearAnalysisStartFromBase.FirstPayment;
                default:
                    throw new UnhandledEnumException(startFrom);
            }
        }

        /// <summary>
        /// Creates a serializable CertificateOfTitleEnum object from the given type.
        /// </summary>
        /// <param name="type">The given certificate of title type.</param>
        /// <returns>A serializable CertificateOfTitleEnum object.</returns>
        public static MISMOEnum<CertificateOfTitleBase> ToCertificateOfTitleEnum(E_ManufacturedHomeCertificateofTitleT type)
        {
            if (type == E_ManufacturedHomeCertificateofTitleT.LeaveBlank)
            {
                return null;
            }

            var certificateOfTitleEnum = new MISMOEnum<CertificateOfTitleBase>();
            certificateOfTitleEnum.enumValue = GetCertificateOfTitleBaseValue(type);
            certificateOfTitleEnum.IsSensitive = false;

            return certificateOfTitleEnum;
        }

        /// <summary>
        /// Creates a serializable <see cref="MISMOEnum{CharacterEncodingSetBase}"/>
        /// indicating the encoding used by a file.
        /// </summary>
        /// <param name="encodingSet">The encoding set.</param>
        /// <returns>A serializable <see cref="MISMOEnum{CharacterEncodingSetBase}"/> object.</returns>
        public static MISMOEnum<CharacterEncodingSetBase> ToCharacterEncodingSetEnum(CharacterEncodingSetBase encodingSet)
        {
            var characterEncodingSetBase = new MISMOEnum<CharacterEncodingSetBase>();
            characterEncodingSetBase.enumValue = encodingSet;
            characterEncodingSetBase.IsSensitive = false;

            return characterEncodingSetBase;
        }

        /// <summary>
        /// Retrieves the certificate of title base value corresponding to the given type.
        /// </summary>
        /// <param name="type">The given certificate of title type.</param>
        /// <returns>The corresponding value.</returns>
        public static CertificateOfTitleBase GetCertificateOfTitleBaseValue(E_ManufacturedHomeCertificateofTitleT type)
        {
            switch (type)
            {
                case E_ManufacturedHomeCertificateofTitleT.HomeIsNotCoveredOrCertIsAttached:
                    return CertificateOfTitleBase.HomeIsNotCoveredOrCertIsAttached;
                case E_ManufacturedHomeCertificateofTitleT.HomeIsNotCoveredOrNotAbleToProduceCert:
                    return CertificateOfTitleBase.HomeIsNotCoveredOrNotAbleToProduceCert;
                case E_ManufacturedHomeCertificateofTitleT.HomeShallBeCoveredByCertificateOfTitle:
                    return CertificateOfTitleBase.HomeShallBeCoveredByCertificateOfTitle;
                case E_ManufacturedHomeCertificateofTitleT.ManufacturersCertificateHasBeenEliminated:
                    return CertificateOfTitleBase.ManufacturersCertificateHasBeenEliminated;
                case E_ManufacturedHomeCertificateofTitleT.ManufacturersCertificateShallBeEliminated:
                    return CertificateOfTitleBase.ManufacturersCertificateShallBeEliminated;
                case E_ManufacturedHomeCertificateofTitleT.TitleCertificateHasBeenEliminated:
                    return CertificateOfTitleBase.TitleCertificateHasBeenEliminated;
                case E_ManufacturedHomeCertificateofTitleT.TitleCertificateShallBeEliminated:
                    return CertificateOfTitleBase.TitleCertificateShallBeEliminated;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Creates a serializable <see cref="MISMOEnum{CitizenshipResidencyBase}" /> object from a borrower application.
        /// </summary>
        /// <param name="isResident">Indicates whether the borrower is a permanent resident alien.</param>
        /// <param name="isCitizen">Indicates whether the borrower is a citizen.</param>
        /// <returns>A serializable <see cref="MISMOEnum{CitizenshipResidencyBase}" /> object.</returns>
        public static MISMOEnum<CitizenshipResidencyBase> ToCitizenshipResidencyEnum(string isResident, string isCitizen)
        {
            var citizenshipResidencyEnum = new MISMOEnum<CitizenshipResidencyBase>();
            citizenshipResidencyEnum.enumValue = GetCitizenshipResidencyBaseValue(isResident, isCitizen);
            citizenshipResidencyEnum.IsSensitive = false;

            return citizenshipResidencyEnum;
        }

        /// <summary>
        /// Retrieves the CitizenshipResidency value corresponding to two LQB values.
        /// </summary>
        /// <param name="isResident">Indicates whether the borrower is a permanent resident alien.</param>
        /// <param name="isCitizen">Indicates whether the borrower is a citizen.</param>
        /// <returns>The corresponding CitizenshipResidency value.</returns>
        public static CitizenshipResidencyBase GetCitizenshipResidencyBaseValue(string isResident, string isCitizen)
        {
            if (string.IsNullOrWhiteSpace(isResident) && string.IsNullOrWhiteSpace(isCitizen))
            {
                return CitizenshipResidencyBase.Blank;
            }
            else if (string.Equals(isCitizen, "Y"))
            {
                return CitizenshipResidencyBase.USCitizen;
            }
            else if (string.Equals(isResident, "Y"))
            {
                return CitizenshipResidencyBase.PermanentResidentAlien;
            }
            else
            {
                return CitizenshipResidencyBase.NonResidentAlien;
            }
        }

        /// <summary>
        /// Creates an Escrow Absence Reason enumeration object with the given reason for lack of an escrow account.
        /// </summary>
        /// <param name="reason">The reason for lack of an escrow account.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>An <see cref="MISMOEnum{EscrowAbsenceReasonBase}" /> object with the reason for lack of an escrow account.</returns>
        public static MISMOEnum<EscrowAbsenceReasonBase> ToEscrowAbsenceReasonEnum(E_sNonMIHousingExpensesNotEscrowedReasonT reason, bool isSensitiveData = false)
        {
            var absenceReason = new MISMOEnum<EscrowAbsenceReasonBase>();
            absenceReason.enumValue = GetEscrowAbsenceReasonBase(reason);
            absenceReason.IsSensitive = isSensitiveData;

            if (absenceReason.enumValue == EscrowAbsenceReasonBase.Blank)
            {
                return null;
            }

            return absenceReason;
        }

        /// <summary>
        /// Converts the given reason for lack of an escrow account to the corresponding MISMO type.
        /// </summary>
        /// <param name="reason">The reason for lack of an escrow account.</param>
        /// <returns>The EscrowAbsenceReasonBase corresponding to the reason for lack of an escrow account.</returns>
        public static EscrowAbsenceReasonBase GetEscrowAbsenceReasonBase(E_sNonMIHousingExpensesNotEscrowedReasonT reason)
        {
            switch (reason)
            {
                case E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined:
                    return EscrowAbsenceReasonBase.BorrowerDeclined;
                case E_sNonMIHousingExpensesNotEscrowedReasonT.LenderDidNotOffer:
                    return EscrowAbsenceReasonBase.LenderDoesNotOffer;
                case E_sNonMIHousingExpensesNotEscrowedReasonT.Unknown:
                    return EscrowAbsenceReasonBase.Blank;
                default:
                    throw new UnhandledEnumException(reason);
            }
        }

        /// <summary>
        /// Creates an escrow lender requirement enumeration object with the given reason for having an escrow account.
        /// </summary>
        /// <param name="reason">The reason for having an escrow account.</param>
        /// <param name="hasEscrow">Indicates whether the loan has an escrow account.</param>
        /// <returns>An <see cref="MISMOEnum{EscrowAccountLenderRequirementBase}"/> object with the reason for having an escrow account.</returns>
        public static MISMOEnum<EscrowAccountLenderRequirementBase> ToEscrowAccountLenderRequirementEnum(E_sNonMIHousingExpensesEscrowedReasonT reason, bool hasEscrow)
        {
            var escrowRequirementEnum = new MISMOEnum<EscrowAccountLenderRequirementBase>();
            escrowRequirementEnum.enumValue = GetEscrowAccountLenderRequirementBaseValue(reason, hasEscrow);
            escrowRequirementEnum.IsSensitive = false;

            if (escrowRequirementEnum.enumValue == EscrowAccountLenderRequirementBase.Blank)
            {
                return null;
            }

            return escrowRequirementEnum;
        }

        /// <summary>
        /// Converts the given reason for having an escrow account to the corresponding MISMO type.
        /// </summary>
        /// <param name="reason">The reason for having an escrow account.</param>
        /// <param name="hasEscrow">Indicates whether the loan has an escrow account.</param>
        /// <returns>The MISMO value corresponding to the reason for having an escrow account.</returns>
        public static EscrowAccountLenderRequirementBase GetEscrowAccountLenderRequirementBaseValue(E_sNonMIHousingExpensesEscrowedReasonT reason, bool hasEscrow)
        {
            if (reason == E_sNonMIHousingExpensesEscrowedReasonT.LenderRequired)
            {
                return EscrowAccountLenderRequirementBase.EscrowsRequiredByLender;
            }
            else if (reason == E_sNonMIHousingExpensesEscrowedReasonT.Unknown && hasEscrow)
            {
                return EscrowAccountLenderRequirementBase.Blank;
            }
            else
            {
                return EscrowAccountLenderRequirementBase.EscrowsWaivedByLender;
            }
        }

        /// <summary>
        /// Creates an Escrow Item enumeration object with the given item type.
        /// </summary>
        /// <param name="type">The escrow item type.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <returns>A serializable <see cref="MISMOEnum{EscrowItemBase}" /> object.</returns>
        public static MISMOEnum<EscrowItemBase> ToEscrowItemEnum(EscrowItemBase type, string displayLabelText)
        {
            if (type == EscrowItemBase.Blank)
            {
                return null;
            }

            var escrowItemEnum = new MISMOEnum<EscrowItemBase>();
            escrowItemEnum.enumValue = type;
            escrowItemEnum.IsSensitive = false;
            escrowItemEnum.DisplayLabelText = displayLabelText;

            return escrowItemEnum;
        }

        /// <summary>
        /// Creates an Escrow Item enumeration object from the given expense type.
        /// </summary>
        /// <param name="type">The housing expense type.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <returns>A serializable <see cref="MISMOEnum{EscrowItemBase}" /> object.</returns>
        public static MISMOEnum<EscrowItemBase> ToEscrowItemEnum(E_HousingExpenseTypeT type, string displayLabelText)
        {
            return ToEscrowItemEnum(GetEscrowItemBaseValue(type), displayLabelText);
        }

        /// <summary>
        /// Creates an escrow item enumeration object from the given tax or expense type.
        /// </summary>
        /// <param name="taxType">The tax type.</param>
        /// <param name="escrowType">The expense type.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <returns>A serializable <see cref="MISMOEnum{EscrowItemBase}" /> object.</returns>
        public static MISMOEnum<EscrowItemBase> ToEscrowItemEnum(E_TaxTableTaxT taxType, E_HousingExpenseTypeT escrowType, string displayLabelText)
        {
            var typeBasedOnTax = GetEscrowItemBaseValue_Tax(taxType);

            if (typeBasedOnTax != EscrowItemBase.Other)
            {
                return ToEscrowItemEnum(typeBasedOnTax, displayLabelText);
            }
            else
            {
                return ToEscrowItemEnum(escrowType, displayLabelText);
            }
        }

        /// <summary>
        /// Retrieves the escrow item type corresponding to a tax type.
        /// </summary>
        /// <param name="taxType">A tax type.</param>
        /// <returns>The corresponding escrow item type.</returns>
        public static EscrowItemBase GetEscrowItemBaseValue_Tax(E_TaxTableTaxT taxType)
        {
            switch (taxType)
            {
                case E_TaxTableTaxT.Borough:
                case E_TaxTableTaxT.City:
                    return EscrowItemBase.CityPropertyTax;
                case E_TaxTableTaxT.County:
                    return EscrowItemBase.CountyPropertyTax;
                case E_TaxTableTaxT.FireDist:
                case E_TaxTableTaxT.LocalImprovementDist:
                case E_TaxTableTaxT.MunicipalUtilDist:
                case E_TaxTableTaxT.SpecialAssessmentDist:
                case E_TaxTableTaxT.WasteFeeDist:
                    return EscrowItemBase.DistrictPropertyTax;
                case E_TaxTableTaxT.School:
                    return EscrowItemBase.SchoolPropertyTax;
                case E_TaxTableTaxT.Town:
                    return EscrowItemBase.TownPropertyTax;
                case E_TaxTableTaxT.Township:
                    return EscrowItemBase.TownshipPropertyTax;
                case E_TaxTableTaxT.Village:
                    return EscrowItemBase.VillagePropertyTax;
                case E_TaxTableTaxT.LeaveBlank:
                case E_TaxTableTaxT.Miscellaneous:
                case E_TaxTableTaxT.Utility:
                case E_TaxTableTaxT.Water_Irrigation:
                case E_TaxTableTaxT.Water_Sewer:
                    return EscrowItemBase.Other;
                default:
                    throw new UnhandledEnumException(taxType);
            }
        }

        /// <summary>
        /// Retrieves the Escrow Item type corresponding to the expense type.
        /// </summary>
        /// <param name="type">The housing expense type.</param>
        /// <returns>The corresponding <see cref="EscrowItemBase" /> enumeration.</returns>
        public static EscrowItemBase GetEscrowItemBaseValue(E_HousingExpenseTypeT type)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.HazardInsurance:
                    return EscrowItemBase.HazardInsurance;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return EscrowItemBase.FloodInsurance;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return EscrowItemBase.WindstormInsurance;
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return EscrowItemBase.HomeownersInsurance;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                    return EscrowItemBase.PropertyTax;
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return EscrowItemBase.SchoolPropertyTax;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return EscrowItemBase.HomeownersAssociationDues;
                case E_HousingExpenseTypeT.GroundRent:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.Unassigned:
                    return EscrowItemBase.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Creates an escrow item payment timing enumeration object from the given disbursement payment type.
        /// </summary>
        /// <param name="timing">The timing of the payment.</param>
        /// <returns>A serializable <see cref="MISMOEnum{EscrowItemPaymentTimingBase}" /> object.</returns>
        public static MISMOEnum<EscrowItemPaymentTimingBase> ToEscrowItemPaymentTimingEnum(E_GfeClosingCostFeePaymentTimingT timing)
        {
            var escrowItemPaymentTimingEnum = new MISMOEnum<EscrowItemPaymentTimingBase>();
            escrowItemPaymentTimingEnum.enumValue = GetEscrowItemPaymentTimingBaseValue(timing);
            escrowItemPaymentTimingEnum.IsSensitive = false;

            if (escrowItemPaymentTimingEnum.enumValue == EscrowItemPaymentTimingBase.Blank)
            {
                return null;
            }

            return escrowItemPaymentTimingEnum;
        }

        /// <summary>
        /// Retrieves the escrow item payment timing value corresponding to the given disbursement payment type.
        /// </summary>
        /// <param name="timing">The timing of the payment.</param>
        /// <returns>The corresponding escrow item payment timing value.</returns>
        public static EscrowItemPaymentTimingBase GetEscrowItemPaymentTimingBaseValue(E_GfeClosingCostFeePaymentTimingT timing)
        {
            switch (timing)
            {
                case E_GfeClosingCostFeePaymentTimingT.AtClosing:
                    return EscrowItemPaymentTimingBase.AtClosing;
                case E_GfeClosingCostFeePaymentTimingT.LeaveBlank:
                    return EscrowItemPaymentTimingBase.Blank;
                case E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing:
                    return EscrowItemPaymentTimingBase.BeforeClosing;
                default:
                    throw new UnhandledEnumException(timing);
            }
        }

        /// <summary>
        /// Creates an escrow item payment paid by enumeration object.
        /// </summary>
        /// <param name="paidBy">The entity who made the payment.</param>
        /// <returns>A serializable <see cref="MISMOEnum{EscrowItemPaymentPaidByBase}" /> object.</returns>
        public static MISMOEnum<EscrowItemPaymentPaidByBase> ToEscrowItemPaymentPaidByEnum(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            var escrowItemPaymentPaidByEnum = new MISMOEnum<EscrowItemPaymentPaidByBase>();
            escrowItemPaymentPaidByEnum.enumValue = GetEscrowItemPaymentPaidByBaseValue(paidBy);
            escrowItemPaymentPaidByEnum.IsSensitive = false;

            return escrowItemPaymentPaidByEnum;
        }

        /// <summary>
        /// Retrieves the escrow item payment paid by value.
        /// </summary>
        /// <param name="paidBy">The entity who made the payment.</param>
        /// <returns>The corresponding paid by value.</returns>
        public static EscrowItemPaymentPaidByBase GetEscrowItemPaymentPaidByBaseValue(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return EscrowItemPaymentPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return EscrowItemPaymentPaidByBase.Broker;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return EscrowItemPaymentPaidByBase.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return EscrowItemPaymentPaidByBase.Seller;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return EscrowItemPaymentPaidByBase.ThirdParty;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Creates an <see cref="MISMOEnum{EscrowPaidByBase}" /> object with the type of entity that pays the escrow amount.
        /// </summary>
        /// <param name="paidBy">The type of entity that pays the escrow amount.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>An <see cref="MISMOEnum{EscrowPaidByBase}" /> object with the type of entity that pays the escrow amount.</returns>
        public static MISMOEnum<EscrowPaidByBase> ToEscrowPaidByEnum(E_ClosingCostFeePaymentPaidByT paidBy, bool isSensitiveData = false)
        {
            var paidByEnum = new MISMOEnum<EscrowPaidByBase>();
            paidByEnum.enumValue = GetEscrowPaidByBase(paidBy);
            paidByEnum.IsSensitive = isSensitiveData;

            if (paidByEnum.enumValue == EscrowPaidByBase.Blank)
            {
                return null;
            }

            return paidByEnum;
        }

        /// <summary>
        /// Converts the given paid by indicator to the corresponding EscrowPaidByBase.
        /// </summary>
        /// <param name="paidBy">The type of entity that pays the escrow amount.</param>
        /// <returns>The EscrowPaidByBase corresponding to the given paid by type.</returns>
        public static EscrowPaidByBase GetEscrowPaidByBase(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return EscrowPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return EscrowPaidByBase.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return EscrowPaidByBase.Blank;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return EscrowPaidByBase.LenderPremium;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return EscrowPaidByBase.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return EscrowPaidByBase.Seller;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Creates an escrow payment frequency enumeration object from the given interval.
        /// </summary>
        /// <param name="frequency">The payment interval.</param>
        /// <returns>A serializable <see cref="MISMOEnum{EscrowPaymentFrequencyBase}" /> object.</returns>
        public static MISMOEnum<EscrowPaymentFrequencyBase> ToEscrowPaymentFrequencyEnum(E_DisbursementRepIntervalT frequency)
        {
            var escrowPaymentFrequencyEnum = new MISMOEnum<EscrowPaymentFrequencyBase>();
            escrowPaymentFrequencyEnum.enumValue = GetEscrowPaymentFrequencyBaseValue(frequency);
            escrowPaymentFrequencyEnum.IsSensitive = false;

            return escrowPaymentFrequencyEnum;
        }

        /// <summary>
        /// Retrieves the escrow payment frequency value corresponding to the disbursement interval.
        /// </summary>
        /// <param name="frequency">The payment interval.</param>
        /// <returns>The corresponding escrow payment frequency value.</returns>
        public static EscrowPaymentFrequencyBase GetEscrowPaymentFrequencyBaseValue(E_DisbursementRepIntervalT frequency)
        {
            switch (frequency)
            {
                case E_DisbursementRepIntervalT.Annual:
                case E_DisbursementRepIntervalT.AnnuallyInClosingMonth:
                    return EscrowPaymentFrequencyBase.Annual;
                case E_DisbursementRepIntervalT.Monthly:
                    return EscrowPaymentFrequencyBase.Monthly;
                default:
                    throw new UnhandledEnumException(frequency);
            }
        }

        /// <summary>
        /// Creates an <see cref="MISMOEnum{EscrowPremiumPaidByBase}" /> object with the type of entity that pays the escrow amount.
        /// </summary>
        /// <param name="paidBy">The type of entity that pays the escrow amount.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>An <see cref="MISMOEnum{EscrowPremiumPaidByBase}" /> object with the type of entity that pays the escrow amount.</returns>
        public static MISMOEnum<EscrowPremiumPaidByBase> ToEscrowPremiumPaidByEnum(E_ClosingCostFeePaymentPaidByT paidBy, bool isSensitiveData = false)
        {
            var paidByEnum = new MISMOEnum<EscrowPremiumPaidByBase>();
            paidByEnum.enumValue = GetEscrowPremiumPaidByBase(paidBy);
            paidByEnum.IsSensitive = isSensitiveData;

            if (paidByEnum.enumValue == EscrowPremiumPaidByBase.Blank)
            {
                return null;
            }

            return paidByEnum;
        }

        /// <summary>
        /// Converts the given paid by indicator to the corresponding EscrowPremiumPaidByBase.
        /// </summary>
        /// <param name="paidBy">The type of entity that pays the escrow amount.</param>
        /// <returns>The EscrowPremiumPaidByBase corresponding to the given paid by type.</returns>
        public static EscrowPremiumPaidByBase GetEscrowPremiumPaidByBase(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return EscrowPremiumPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return EscrowPremiumPaidByBase.Blank;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return EscrowPremiumPaidByBase.Lender;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return EscrowPremiumPaidByBase.Blank;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return EscrowPremiumPaidByBase.Seller;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Creates an <see cref="MISMOEnum{EscrowPremiumPaymentBase}" /> with the timing of the escrow payment, e.g. at closing | outside of closing.
        /// </summary>
        /// <param name="timing">The timing of the escrow payment.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>An <see cref="MISMOEnum{EscrowPremiumPaymentBase}" /> with the timing of the escrow payment, e.g. at closing | outside of closing.</returns>
        public static MISMOEnum<EscrowPremiumPaymentBase> ToEscrowPremiumPaymentEnum(E_GfeClosingCostFeePaymentTimingT timing, bool isSensitiveData = false)
        {
            var paymentTiming = new MISMOEnum<EscrowPremiumPaymentBase>();
            paymentTiming.enumValue = GetEscrowPremiumPaymentBase(timing);
            paymentTiming.IsSensitive = isSensitiveData;

            if (paymentTiming.enumValue == EscrowPremiumPaymentBase.Blank)
            {
                return null;
            }

            return paymentTiming;
        }

        /// <summary>
        /// Converts the given escrow payment timing to the corresponding EscrowPremiumPaymentBase.
        /// </summary>
        /// <param name="timing">The timing of the escrow payment.</param>
        /// <returns>The EscrowPremiumPaymentBase that corresponds to the given escrow payment timing.</returns>
        public static EscrowPremiumPaymentBase GetEscrowPremiumPaymentBase(E_GfeClosingCostFeePaymentTimingT timing)
        {
            switch (timing)
            {
                case E_GfeClosingCostFeePaymentTimingT.AtClosing:
                    return EscrowPremiumPaymentBase.CollectAtClosing;
                case E_GfeClosingCostFeePaymentTimingT.LeaveBlank:
                    return EscrowPremiumPaymentBase.Blank;
                case E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing:
                    return EscrowPremiumPaymentBase.PaidOutsideOfClosing;
                default:
                    throw new UnhandledEnumException(timing);
            }
        }

        /// <summary>
        /// Creates an escrow premium rate percent basis enumeration object from the given percent basis.
        /// </summary>
        /// <param name="percentBasis">The percent basis.</param>
        /// <returns>A serializable <see cref="MISMOEnum{EscrowPremiumRatePercentBasisBase}" /> object.</returns>
        public static MISMOEnum<EscrowPremiumRatePercentBasisBase> ToEscrowPremiumRatePercentBasisEnum(E_PercentBaseT percentBasis)
        {
            var escrowPremiumRatePercentBasisEnum = new MISMOEnum<EscrowPremiumRatePercentBasisBase>();
            escrowPremiumRatePercentBasisEnum.enumValue = GetEscrowPremiumRatePercentBasisBaseValue(percentBasis);
            escrowPremiumRatePercentBasisEnum.IsSensitive = false;

            return escrowPremiumRatePercentBasisEnum;
        }

        /// <summary>
        /// Retrieves the escrow premium rate percent basis value corresponding to the given percent basis.
        /// </summary>
        /// <param name="percentBasis">The percent basis.</param>
        /// <returns>The corresponding escrow premium rate percent basis value.</returns>
        public static EscrowPremiumRatePercentBasisBase GetEscrowPremiumRatePercentBasisBaseValue(E_PercentBaseT percentBasis)
        {
            switch (percentBasis)
            {    
                case E_PercentBaseT.AppraisalValue:
                    return EscrowPremiumRatePercentBasisBase.PropertyValuationAmount;
                case E_PercentBaseT.LoanAmount:
                    return EscrowPremiumRatePercentBasisBase.NoteAmount;
                case E_PercentBaseT.SalesPrice:
                    return EscrowPremiumRatePercentBasisBase.PurchasePriceAmount;
                case E_PercentBaseT.TotalLoanAmount:
                    return EscrowPremiumRatePercentBasisBase.TotalLoanAmount;
                case E_PercentBaseT.OriginalCost:
                case E_PercentBaseT.AverageOutstandingBalance:
                case E_PercentBaseT.DecliningRenewalsAnnually:
                case E_PercentBaseT.DecliningRenewalsMonthly:
                case E_PercentBaseT.AllYSP:
                    return EscrowPremiumRatePercentBasisBase.Other;
                default:
                    throw new UnhandledEnumException(percentBasis);
            }
        }

        /// <summary>
        /// Converts an LQB value to a serializable EstimatedPrepaidDaysPaidBy enumeration.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{EstimatedPrepaidDaysPaidByBase}" /> object.</returns>
        public static MISMOEnum<EstimatedPrepaidDaysPaidByBase> ToEstimatedPrepaidDaysPaidByEnum(int lqbValue)
        {
            var estimatedPrepaidDaysPaidByEnum = new MISMOEnum<EstimatedPrepaidDaysPaidByBase>();
            estimatedPrepaidDaysPaidByEnum.enumValue = GetEstimatedPrepaidDaysPaidByBaseValue(lqbValue);
            estimatedPrepaidDaysPaidByEnum.IsSensitive = false;

            return estimatedPrepaidDaysPaidByEnum;
        }

        /// <summary>
        /// Retrieves the EstimatedPrepaidDaysPaidBy enumerated value corresponding to an LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding EstimatedPrepaidDaysPaidBy enumerated value.</returns>
        public static EstimatedPrepaidDaysPaidByBase GetEstimatedPrepaidDaysPaidByBaseValue(int lqbValue)
        {
            switch (LosConvert.GfeItemProps_Payer(lqbValue))
            {
                case LosConvert.BORR_PAID_OUTOFPOCKET:
                case LosConvert.BORR_PAID_FINANCED:
                    return EstimatedPrepaidDaysPaidByBase.Buyer;
                case LosConvert.SELLER_PAID:
                    return EstimatedPrepaidDaysPaidByBase.Seller;
                case LosConvert.LENDER_PAID:
                    return EstimatedPrepaidDaysPaidByBase.Lender;
                case LosConvert.BROKER_PAID:
                    return EstimatedPrepaidDaysPaidByBase.Other;
                default:
                    throw new ArgumentException("Unhandled value='" + LosConvert.GfeItemProps_Payer(lqbValue).ToString() + "' for sIPiaProps");
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{EmploymentClassificationBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{EmploymentClassificationBase}" /> object.</returns>
        public static MISMOEnum<EmploymentClassificationBase> ToEmploymentClassificationEnum(bool lqbValue)
        {
            var employmentClassificationEnum = new MISMOEnum<EmploymentClassificationBase>();
            employmentClassificationEnum.enumValue = GetEmploymentClassificationBaseValue(lqbValue);
            employmentClassificationEnum.IsSensitive = false;

            return employmentClassificationEnum;
        }

        /// <summary>
        /// Retrieves the EmploymentClassification value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding EmploymentClassification value.</returns>
        public static EmploymentClassificationBase GetEmploymentClassificationBaseValue(bool lqbValue)
        {
            if (lqbValue)
            {
                return EmploymentClassificationBase.Primary;
            }
            else
            {
                return EmploymentClassificationBase.Secondary;
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{EmploymentStatusBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{EmploymentStatusBase}" /> object.</returns>
        public static MISMOEnum<EmploymentStatusBase> ToEmploymentStatusEnum(bool lqbValue)
        {
            var employmentStatusEnum = new MISMOEnum<EmploymentStatusBase>();
            employmentStatusEnum.enumValue = GetEmploymentStatusBaseValue(lqbValue);
            employmentStatusEnum.IsSensitive = false;

            return employmentStatusEnum;
        }

        /// <summary>
        /// Retrieves the EmploymentStatus value corresponding to a given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding EmploymentStatus value.</returns>
        public static EmploymentStatusBase GetEmploymentStatusBaseValue(bool lqbValue)
        {
            if (lqbValue)
            {
                return EmploymentStatusBase.Current;
            }
            else
            {
                return EmploymentStatusBase.Previous;
            }
        }

        /// <summary>
        /// Creates an expense enumeration object from the given expense type.
        /// </summary>
        /// <param name="expenseType">The expense type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ExpenseBase}" /> object.</returns>
        public static MISMOEnum<ExpenseBase> ToExpenseEnum(ExpenseBase expenseType)
        {
            var expenseEnum = new MISMOEnum<ExpenseBase>();
            expenseEnum.enumValue = expenseType;
            expenseEnum.IsSensitive = false;

            return expenseEnum;
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{FundsSourceBase}" /> object from the given LQB value and sensitivity indicator.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to MISMO.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FundsSourceBase}" /> object.</returns>
        public static MISMOEnum<FundsSourceBase> ToFundsSourceEnum(string lqbValue)
        {
            var fundsSourceEnum = new MISMOEnum<FundsSourceBase>();
            fundsSourceEnum.enumValue = GetFundsSourceBaseValue(lqbValue);
            fundsSourceEnum.IsSensitive = false;

            return fundsSourceEnum;
        }

        /// <summary>
        /// Converts an LQB string into the appropriate FundsSourceBase enumerated value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to a FundsSourceBase enumeration.</param>
        /// <returns>A FundsSourceBase enumerated value.</returns>
        public static FundsSourceBase GetFundsSourceBaseValue(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return FundsSourceBase.Blank;
            }
            else if (string.Equals(lqbValue, "FHA - Gift - Source N/A", StringComparison.OrdinalIgnoreCase))
            {
                return FundsSourceBase.Unknown;
            }
            else if (string.Equals(lqbValue, "FHA - Gift - Source Relative", StringComparison.OrdinalIgnoreCase))
            {
                return FundsSourceBase.Relative;
            }
            else if (string.Equals(lqbValue, "FHA - Gift - Source Employer", StringComparison.OrdinalIgnoreCase))
            {
                return FundsSourceBase.Employer;
            }
            else if (string.Equals(lqbValue, "FHA - Gift - Source Nonprofit/Religious/Community - Seller Funded", StringComparison.OrdinalIgnoreCase))
            {
                return FundsSourceBase.PropertySeller;
            }
            else
            {
                return FundsSourceBase.Other;
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{GenderBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{GenderBase}" /> object.</returns>
        public static MISMOEnum<GenderBase> ToGenderEnum(E_GenderT lqbValue)
        {
            if (lqbValue == E_GenderT.LeaveBlank)
            {
                return null;
            }

            var genderEnum = new MISMOEnum<GenderBase>();
            genderEnum.enumValue = GetGenderBaseValue(lqbValue);
            genderEnum.IsSensitive = false;

            return genderEnum;
        }

        /// <summary>
        /// Retrieves the Gender enumeration corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding Gender value.</returns>
        public static GenderBase GetGenderBaseValue(E_GenderT lqbValue)
        {
            switch (lqbValue)
            {
                case E_GenderT.Female:
                    return GenderBase.Female;
                case E_GenderT.Male:
                    return GenderBase.Male;
                case E_GenderT.Unfurnished:
                    return GenderBase.InformationNotProvidedUnknown;
                case E_GenderT.NA:
                    return GenderBase.NotApplicable;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates a serializeble enum object representing a gender.
        /// </summary>
        /// <param name="gender">The gender on the loan application.</param>
        /// <param name="interviewMethod">The application interview method.</param>
        /// <returns>A serializeble enum object.</returns>
        public static MISMOEnum<HMDAGenderBase> ToHMDAGenderEnum(E_GenderT gender, E_aIntrvwrMethodT interviewMethod)
        {
            if (gender == E_GenderT.LeaveBlank)
            {
                return null;
            }

            var genderEnum = new MISMOEnum<HMDAGenderBase>();
            genderEnum.enumValue = GetHmdaGenderBaseValue(gender, interviewMethod);
            genderEnum.IsSensitive = false;

            if (genderEnum.enumValue == HMDAGenderBase.LeaveBlank)
            {
                return null;
            }

            return genderEnum;
        }

        /// <summary>
        /// Determines the HMDA gender value that corresponds to the gender value on the loan.
        /// </summary>
        /// <param name="gender">The gender on the loan application.</param>
        /// <param name="interviewMethod">The application interview method.</param>
        /// <returns>The corresponding HMDA gender value.</returns>
        public static HMDAGenderBase GetHmdaGenderBaseValue(E_GenderT gender, E_aIntrvwrMethodT interviewMethod)
        {
            if (gender == E_GenderT.MaleAndFemale || gender == E_GenderT.MaleFemaleNotFurnished)
            {
                return HMDAGenderBase.ApplicantHasSelectedBothMaleAndFemale;
            }
            else if (gender == E_GenderT.Female || gender == E_GenderT.FemaleAndNotFurnished)
            {
                return HMDAGenderBase.Female;
            }
            else if (gender == E_GenderT.Male || gender == E_GenderT.MaleAndNotFurnished)
            {
                return HMDAGenderBase.Male;
            }
            else if (gender == E_GenderT.NA)
            {
                return HMDAGenderBase.NotApplicable;
            }
            else if ((gender == E_GenderT.LeaveBlank || gender == E_GenderT.Unfurnished)
                && interviewMethod != E_aIntrvwrMethodT.LeaveBlank && interviewMethod != E_aIntrvwrMethodT.FaceToFace)
            {
                return HMDAGenderBase.InformationNotProvidedUnknown;
            }
            else
            {
                return HMDAGenderBase.LeaveBlank;
            }
        }

        /// <summary>
        /// Creates a serializable enum specifying a HMDA gender.
        /// </summary>
        /// <param name="sex">The applicant's sex.</param>
        /// <returns>A serializable enum object.</returns>
        public static MISMOEnum<LqbHmdaGenderBase> ToHmdaGenderEnum(Hmda_Sex sex)
        {
            if (sex == Hmda_Sex.Blank)
            {
                return null;
            }

            var genderEnum = new MISMOEnum<LqbHmdaGenderBase>();
            genderEnum.enumValue = GetHmdaGenderBaseValue(sex);
            genderEnum.IsSensitive = false;

            return genderEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to an applicant's sex.
        /// </summary>
        /// <param name="sex">The applicant's sex.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaGenderBase GetHmdaGenderBaseValue(Hmda_Sex sex)
        {
            switch (sex)
            {
                case Hmda_Sex.BothMaleFemale:
                    return LqbHmdaGenderBase.ApplicantHasSelectedBothMaleAndFemale;
                case Hmda_Sex.Female:
                    return LqbHmdaGenderBase.Female;
                case Hmda_Sex.Male:
                    return LqbHmdaGenderBase.Male;
                case Hmda_Sex.NotApplicable:
                    return LqbHmdaGenderBase.NotApplicable;
                case Hmda_Sex.NotProvided:
                    return LqbHmdaGenderBase.InformationNotProvidedUnknown;
                case Hmda_Sex.NoCoApplicant:
                    return LqbHmdaGenderBase.NoCoApplicant;
                default:
                    throw new UnhandledEnumException(sex);
            }
        }

        /// <summary>
        /// Creates a serializable object indicating whether the applicant refused to
        /// specify their gender.
        /// </summary>
        /// <param name="gender">The gender on the loan application.</param>
        /// <returns>A serializeble indicator.</returns>
        public static MISMOIndicator ToHMDAGenderRefusalIndicator(E_GenderT gender)
        {
            var indicator = new MISMOIndicator();

            indicator.booleanValue = gender == E_GenderT.FemaleAndNotFurnished
                || gender == E_GenderT.MaleAndNotFurnished
                || gender == E_GenderT.MaleFemaleNotFurnished
                || gender == E_GenderT.Unfurnished;
            indicator.IsSensitive = false;

            return indicator;
        }

        /// <summary>
        /// Gets a list of races associated with a borrower. There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of races.</returns>
        public static List<HMDARaceBase> GetHmdaRaceBaseValues(CAppData appData)
        {
            var races = new List<HMDARaceBase>();

            if (appData.aIsAmericanIndian)
            {
                races.Add(HMDARaceBase.AmericanIndianOrAlaskaNative);
            }

            if (appData.aIsAsian)
            {
                races.Add(HMDARaceBase.Asian);
            }

            if (appData.aIsBlack)
            {
                races.Add(HMDARaceBase.BlackOrAfricanAmerican);
            }

            if (appData.aIsPacificIslander)
            {
                races.Add(HMDARaceBase.NativeHawaiianOrOtherPacificIslander);
            }

            if (appData.aIsWhite)
            {
                races.Add(HMDARaceBase.White);
            }

            if (appData.aDoesNotWishToProvideRace)
            {
                races.Add(HMDARaceBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication);
            }

            return races;
        }

        /// <summary>
        /// Gets a list of asian race designations associated with the borrower.
        /// There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of asian race designations.</returns>
        public static List<HMDARaceDesignationBase> GetHmdaRaceDesignationBaseValues_Asian(CAppData appData)
        {
            var designations = new List<HMDARaceDesignationBase>();

            if (appData.aIsAsianIndian)
            {
                designations.Add(HMDARaceDesignationBase.AsianIndian);
            }

            if (appData.aIsChinese)
            {
                designations.Add(HMDARaceDesignationBase.Chinese);
            }

            if (appData.aIsFilipino)
            {
                designations.Add(HMDARaceDesignationBase.Filipino);
            }

            if (appData.aIsJapanese)
            {
                designations.Add(HMDARaceDesignationBase.Japanese);
            }

            if (appData.aIsKorean)
            {
                designations.Add(HMDARaceDesignationBase.Korean);
            }

            if (appData.aIsVietnamese)
            {
                designations.Add(HMDARaceDesignationBase.Vietnamese);
            }

            if (appData.aIsOtherAsian)
            {
                designations.Add(HMDARaceDesignationBase.OtherAsian);
            }

            return designations;
        }

        /// <summary>
        /// Gets a list of pacific islander race designations for the borrower.
        /// There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of pacific islander race designations.</returns>
        public static List<HMDARaceDesignationBase> GetHmdaRaceDesignationBaseValues_PacificIslander(CAppData appData)
        {
            var designations = new List<HMDARaceDesignationBase>();

            if (appData.aIsNativeHawaiian)
            {
                designations.Add(HMDARaceDesignationBase.NativeHawaiian);
            }

            if (appData.aIsGuamanianOrChamorro)
            {
                designations.Add(HMDARaceDesignationBase.GuamanianOrChamorro);
            }

            if (appData.aIsSamoan)
            {
                designations.Add(HMDARaceDesignationBase.Samoan);
            }

            if (appData.aIsOtherPacificIslander)
            {
                designations.Add(HMDARaceDesignationBase.OtherPacificIslander);
            }

            return designations;
        }

        /// <summary>
        /// Gets a list of ethnicity origins for the borrower.
        /// There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of ethnicity origins.</returns>
        public static List<HMDAEthnicityOriginBase> GetHmdaEthnicityOriginBaseValues(CAppData appData)
        {
            var origins = new List<HMDAEthnicityOriginBase>();

            if (appData.aIsCuban)
            {
                origins.Add(HMDAEthnicityOriginBase.Cuban);
            }

            if (appData.aIsMexican)
            {
                origins.Add(HMDAEthnicityOriginBase.Mexican);
            }

            if (appData.aIsOtherHispanicOrLatino)
            {
                origins.Add(HMDAEthnicityOriginBase.Other);
            }

            if (appData.aIsPuertoRican)
            {
                origins.Add(HMDAEthnicityOriginBase.PuertoRican);
            }

            return origins;
        }

        /// <summary>
        /// Gets a list of ethnicities for the borrower. There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of ethnicities.</returns>
        public static List<HMDAEthnicityBase> GetHmdaEthnicityBaseValues(CAppData appData)
        {
            var ethnicities = new List<HMDAEthnicityBase>();

            if (appData.aHispanicT == E_aHispanicT.Hispanic || appData.aHispanicT == E_aHispanicT.BothHispanicAndNotHispanic)
            {
                ethnicities.Add(HMDAEthnicityBase.HispanicOrLatino);
            }

            if (appData.aDoesNotWishToProvideEthnicity
                && appData.aInterviewMethodT != E_aIntrvwrMethodT.LeaveBlank
                && appData.aInterviewMethodT != E_aIntrvwrMethodT.FaceToFace)
            {
                ethnicities.Add(HMDAEthnicityBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication);
            }

            if (appData.aHispanicT == E_aHispanicT.NotHispanic || appData.aHispanicT == E_aHispanicT.BothHispanicAndNotHispanic)
            {
                ethnicities.Add(HMDAEthnicityBase.NotHispanicOrLatino);
            }

            return ethnicities;
        }

        /// <summary>
        /// Creates an object representing the type of a given integrated disclosure cash to close item.
        /// </summary>
        /// <param name="type">The item type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntegratedDisclosureCashToCloseItemBase}" /> object.</returns>
        public static MISMOEnum<IntegratedDisclosureCashToCloseItemBase> ToIntegratedDisclosureCashToCloseItemEnum(IntegratedDisclosureCashToCloseItemBase type)
        {
            var integratedDisclosureCashToCloseItemEnum = new MISMOEnum<IntegratedDisclosureCashToCloseItemBase>();
            integratedDisclosureCashToCloseItemEnum.enumValue = type;
            integratedDisclosureCashToCloseItemEnum.IsSensitive = false;

            return integratedDisclosureCashToCloseItemEnum;
        }

        /// <summary>
        /// Creates an object representing the payment type for a given integrated disclosure cash to close item.
        /// </summary>
        /// <param name="type">The item payment type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntegratedDisclosureCashToCloseItemPaymentBase}" /> object.</returns>
        public static MISMOEnum<IntegratedDisclosureCashToCloseItemPaymentBase> ToIntegratedDisclosureCashToCloseItemPaymentEnum(IntegratedDisclosureCashToCloseItemPaymentBase type)
        {
            var integratedDisclosureCashToCloseItemPaymentEnum = new MISMOEnum<IntegratedDisclosureCashToCloseItemPaymentBase>();

            integratedDisclosureCashToCloseItemPaymentEnum.enumValue = type;
            integratedDisclosureCashToCloseItemPaymentEnum.IsSensitive = false;

            return integratedDisclosureCashToCloseItemPaymentEnum;
        }

        /// <summary>
        /// Indicates whether an Integrated Disclosure document is a Loan Estimate or Closing Disclosure.
        /// </summary>
        /// <param name="type">The integrated disclosure type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntegratedDisclosureDocumentBase}" /> object.</returns>
        public static MISMOEnum<IntegratedDisclosureDocumentBase> ToIntegratedDisclosureDocumentEnum(IntegratedDisclosureDocumentBase type)
        {
            var integratedDisclosureDocumentEnum = new MISMOEnum<IntegratedDisclosureDocumentBase>();
            integratedDisclosureDocumentEnum.enumValue = type;
            integratedDisclosureDocumentEnum.IsSensitive = false;

            return integratedDisclosureDocumentEnum;
        }

        /// <summary>
        /// Creates an object representing the TRID Home Equity Loan indicator.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose.</param>
        /// <param name="isLineOfCredit">Indicates whether the loan is a line of credit.</param>
        /// <returns>A serializable <see cref="MISMOIndicator"/> element.</returns>
        public static MISMOIndicator ToIntegratedDisclosureHomeEquityLoanIndicator(E_sLPurposeT loanPurpose, bool isLineOfCredit)
        {
            var indicator = new MISMOIndicator();
            indicator.booleanValue = GetIntegratedDisclosureHomeEquityLoanIndicatorValue(loanPurpose, isLineOfCredit);
            indicator.IsSensitive = false;

            return indicator;
        }

        /// <summary>
        /// Determines whether the loan is a home equity loan.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose.</param>
        /// <param name="isLineOfCredit">Indicates whether the loan is a line of credit.</param>
        /// <returns>A boolean indicating whether the loan is a home equity loan.</returns>
        public static bool GetIntegratedDisclosureHomeEquityLoanIndicatorValue(E_sLPurposeT loanPurpose, bool isLineOfCredit)
        {
            if (loanPurpose == E_sLPurposeT.HomeEquity)
            {
                return true;
            }
            else
            {
                if (isLineOfCredit && loanPurpose != E_sLPurposeT.Purchase
                    && loanPurpose != E_sLPurposeT.Construct && loanPurpose != E_sLPurposeT.ConstructPerm)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Creates a loan originator fee payment credit enumeration object with the given payment credit value.
        /// </summary>
        /// <param name="paymentCredit">Describes the payment credit.</param>
        /// <returns>A serializable <see cref="MISMOEnum{GFELoanOriginatorFeePaymentCreditBase}" /> object.</returns>
        public static MISMOEnum<GFELoanOriginatorFeePaymentCreditBase> ToGfeLoanOriginatorFeePaymentCreditEnum(string paymentCredit)
        {
            var gfeLoanOriginatorFeePaymentCreditEnum = new MISMOEnum<GFELoanOriginatorFeePaymentCreditBase>();
            gfeLoanOriginatorFeePaymentCreditEnum.enumValue = GetGfeLoanOriginatorFeePaymentCreditBaseValue(paymentCredit);
            gfeLoanOriginatorFeePaymentCreditEnum.IsSensitive = false;

            return gfeLoanOriginatorFeePaymentCreditEnum;
        }

        /// <summary>
        /// Retrieves the payment credit enumeration corresponding to the given payment credit value.
        /// </summary>
        /// <param name="paymentCredit">Describes the payment credit.</param>
        /// <returns>The corresponding value.</returns>
        public static GFELoanOriginatorFeePaymentCreditBase GetGfeLoanOriginatorFeePaymentCreditBaseValue(string paymentCredit)
        {
            if (string.Equals("AmountsPaidByOrInBehalfOfBorrower", paymentCredit, StringComparison.OrdinalIgnoreCase))
            {
                return GFELoanOriginatorFeePaymentCreditBase.AmountsPaidByOrInBehalfOfBorrower;
            }
            else if (string.Equals("ChosenInterestRateCreditOrCharge", paymentCredit, StringComparison.OrdinalIgnoreCase))
            {
                return GFELoanOriginatorFeePaymentCreditBase.ChosenInterestRateCreditOrCharge;
            }
            else
            {
                return GFELoanOriginatorFeePaymentCreditBase.Other;
            }
        }

        /// <summary>
        /// Creates a GFE section identifier enumeration object based on the given section.
        /// </summary>
        /// <param name="section">The GFE section.</param>
        /// <returns>A serializable <see cref="MISMOEnum{GFESectionIdentifierBase}" /> object.</returns>
        public static MISMOEnum<GFESectionIdentifierBase> ToGfeSectionIdentifierEnum(GFESectionIdentifierBase section)
        {
            var gfeSectionIdentifierEnum = new MISMOEnum<GFESectionIdentifierBase>();
            gfeSectionIdentifierEnum.enumValue = section;
            gfeSectionIdentifierEnum.IsSensitive = false;

            return gfeSectionIdentifierEnum;
        }

        /// <summary>
        /// Converts an LQB Mortgage Pool GovernmentBodnFinancingProgramType into a MismoEnum.
        /// </summary>
        /// <param name="programType">The LQB-supported value of the program type.</param>
        /// <param name="displaySensitive">Whether or not to serialize the output element's sensitiveIndicator attribute.</param>
        /// <returns>A serializable <see cref="MISMOEnum{GovernmentBondFinancingProgramBase}"/>.</returns>
        public static MISMOEnum<GovernmentBondFinancingProgramBase> ToGovernmentBondFinancingProgramEnum(E_GovernmentBondFinancingProgramType programType, bool displaySensitive = true)
        {
            GovernmentBondFinancingProgramBase baseValue;

            switch (programType)
            {
                case E_GovernmentBondFinancingProgramType.LeaveBlank:
                    return null;
                case E_GovernmentBondFinancingProgramType.BuilderBond:
                    baseValue = GovernmentBondFinancingProgramBase.BuilderBond;
                    break;
                case E_GovernmentBondFinancingProgramType.ConsolidatedBond:
                    baseValue = GovernmentBondFinancingProgramBase.ConsolidatedBond;
                    break;
                case E_GovernmentBondFinancingProgramType.FinalBond:
                    baseValue = GovernmentBondFinancingProgramBase.FinalBond;
                    break;
                default:
                    throw new UnhandledEnumException(programType);
            }

            return ToMismoEnum(baseValue, displaySensitive: displaySensitive);
        }

        /// <summary>
        /// Creates an enumeration object indicating the government refinance type.
        /// </summary>
        /// <param name="refinanceType">The government refinance type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{GovernmentRefinanceBase}"/> object.</returns>
        public static MISMOEnum<GovernmentRefinanceBase> ToGovernmentRefinanceEnum(GovernmentRefinanceBase refinanceType)
        {
            var governmentRefinanceEnum = new MISMOEnum<GovernmentRefinanceBase>();
            governmentRefinanceEnum.enumValue = refinanceType;
            governmentRefinanceEnum.IsSensitive = false;

            return governmentRefinanceEnum;
        }

        /// <summary>
        /// Generates an enumeration object for document type.
        /// </summary>
        /// <param name="docType">The doc type.</param>
        /// <returns>The mismo enum for doc type.</returns>
        public static MISMOEnum<LqbDocumentTypeNameBase> ToLqbDocumentTypeNameBase(E_sProdDocT docType)
        {
            var docTypeNameBase = new MISMOEnum<LqbDocumentTypeNameBase>();
            docTypeNameBase.IsSensitive = false;
            switch (docType)
            {
                case E_sProdDocT.Alt:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.Alt;
                    break;
                case E_sProdDocT.AssetUtilization:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.AssetUtilization;
                    break;
                case E_sProdDocT.DebtServiceCoverage:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.DebtServiceCoverage;
                    break;
                case E_sProdDocT.Full:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.Full;
                    break;
                case E_sProdDocT.Light:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.Light;
                    break;
                case E_sProdDocT.NINA:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.NINA;
                    break;
                case E_sProdDocT.NINANE:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.NINANE;
                    break;
                case E_sProdDocT.NISA:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.NISA;
                    break;
                case E_sProdDocT.NIVA:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.NIVA;
                    break;
                case E_sProdDocT.NIVANE:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.NIVANE;
                    break;
                case E_sProdDocT.NoIncome:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.NoIncome;
                    break;
                case E_sProdDocT.OtherBankStatements:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.OtherBankStatements;
                    break;
                case E_sProdDocT.SISA:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.SISA;
                    break;
                case E_sProdDocT.SIVA:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.SIVA;
                    break;
                case E_sProdDocT.Streamline:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.Streamline;
                    break;
                case E_sProdDocT.VINA:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.VINA;
                    break;
                case E_sProdDocT.VISA:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.VISA;
                    break;
                case E_sProdDocT.Voe:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase.Voe;
                    break;
                case E_sProdDocT._12MoBusinessBankStatements:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase._12MoBusinessBankStatements;
                    break;
                case E_sProdDocT._12MoPersonalBankStatements:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase._12MoPersonalBankStatements;
                    break;
                case E_sProdDocT._1YrTaxReturns:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase._1YrTaxReturns;
                    break;
                case E_sProdDocT._24MoBusinessBankStatements:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase._24MoBusinessBankStatements;
                    break;
                case E_sProdDocT._24MoPersonalBankStatements:
                    docTypeNameBase.enumValue = LqbDocumentTypeNameBase._24MoPersonalBankStatements;
                    break;
                default:
                    throw new UnhandledEnumException(docType);
            }

            return docTypeNameBase;
        }

        /// <summary>
        /// Creates an enumeration object indicating the loan state type.
        /// </summary>
        /// <param name="loanStateType">The loan state type.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LoanStateBase}"/> object.</returns>
        public static MISMOEnum<LoanStateBase> ToLoanStateEnum(LoanStateBase loanStateType, bool displaySensitive = true)
        {
            if (loanStateType == LoanStateBase.Blank)
            {
                return null;
            }

            var loanStateEnum = new MISMOEnum<LoanStateBase>();
            loanStateEnum.enumValue = loanStateType;
            loanStateEnum.IsSensitive = false;
            loanStateEnum.DisplaySensitive = displaySensitive;

            return loanStateEnum;
        }

        /// <summary>
        /// Maps VA appraisal type to mismoe VA appraisal base.
        /// </summary>
        /// <param name="appraisalType">The appraisal type.</param>
        /// <returns>The equivalent appraisal base.</returns>
        public static MISMOEnum<VAAppraisalBase> ToVaAppraisalBaseEnum(E_sVaApprT appraisalType)
        {
            MISMOEnum<VAAppraisalBase> appraisalBase = new MISMOEnum<VAAppraisalBase>();
            appraisalBase.DisplaySensitive = false;
            switch (appraisalType)
            {
                case E_sVaApprT.SingleProp:
                    appraisalBase.enumValue = VAAppraisalBase.SingleProperty;
                    break;
                case E_sVaApprT.MasterCrvCase:
                    appraisalBase.enumValue = VAAppraisalBase.MasterCertificateOfReasonableValueCase;
                    break;
                case E_sVaApprT.LappLenderAppr:
                    appraisalBase.enumValue = VAAppraisalBase.LenderAppraisal;
                    break;
                case E_sVaApprT.ManufacturedHome:
                    appraisalBase.enumValue = VAAppraisalBase.ManufacturedHome;
                    break;
                case E_sVaApprT.HudVaConversion:
                    appraisalBase.enumValue = VAAppraisalBase.HUDConversion;
                    break;
                case E_sVaApprT.PropMgmtCase:
                    appraisalBase.enumValue = VAAppraisalBase.PropertyManagementCase;
                    break;
                case E_sVaApprT.LeaveBlank:
                    appraisalBase.enumValue = VAAppraisalBase.Blank;
                    break;
                default:
                    throw new UnhandledEnumException(appraisalType);
            }

            return appraisalBase;
        }

        /// <summary>
        /// Creates an enumeration object indicating the government refinance type. Used for GNMA PDD.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose.</param>
        /// <param name="hasAppraisal">Whether the loan has an appraisal on file.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{GovernmentRefinanceBase}"/> object.</returns>
        public static MISMOEnum<GovernmentRefinanceBase> ToGovernmentRefinanceEnum(E_sLPurposeT loanPurpose, bool hasAppraisal, bool displaySensitive = true)
        {
            var governmentRefinanceEnum = new MISMOEnum<GovernmentRefinanceBase>();
            governmentRefinanceEnum.enumValue = ToGovernmentRefinanceBaseValue(loanPurpose, hasAppraisal);

            if (governmentRefinanceEnum.enumValue == GovernmentRefinanceBase.Blank)
            {
                return null;
            }

            governmentRefinanceEnum.IsSensitive = false;
            governmentRefinanceEnum.DisplaySensitive = displaySensitive;

            return governmentRefinanceEnum;
        }

        /// <summary>
        /// Determines the Government Refinance enum value corresponding to a loan's purpose and whether it has an appraisal.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose.</param>
        /// <param name="hasAppraisal">Whether the loan has an appraisal on file.</param>
        /// <returns>A GovernmentRefinanceBase enum value.</returns>
        public static GovernmentRefinanceBase ToGovernmentRefinanceBaseValue(E_sLPurposeT loanPurpose, bool hasAppraisal)
        {
            if (loanPurpose == E_sLPurposeT.VaIrrrl)
            {
                return GovernmentRefinanceBase.InterestRateReductionRefinanceLoan;
            }
            else if (loanPurpose == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                if (hasAppraisal)
                {
                    return GovernmentRefinanceBase.StreamlineWithAppraisal;
                }
                else
                {
                    return GovernmentRefinanceBase.StreamlineWithoutAppraisal;
                }
            }
            else
            {
                return GovernmentRefinanceBase.FullDocumentation;
            }
        }

        /// <summary>
        /// Creates an enumeration object indicating the GSE property type.
        /// </summary>
        /// <param name="propertyType">The GSE property type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{GsePropertyBase}"/> object.</returns>
        public static MISMOEnum<GsePropertyBase> ToGsePropertyEnum(E_ReoTypeT propertyType)
        {
            var gsePropertyEnum = new MISMOEnum<GsePropertyBase>();
            gsePropertyEnum.enumValue = GetGsePropertyBaseValue(propertyType);
            if (gsePropertyEnum.enumValue.Equals(GsePropertyBase.Blank))
            {
                return null;
            }

            gsePropertyEnum.IsSensitive = false;

            return gsePropertyEnum;
        }

        /// <summary>
        /// Retrieves the MISMO value corresponding to the GSE property type.
        /// </summary>
        /// <param name="propertyType">The GSE property type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static GsePropertyBase GetGsePropertyBaseValue(E_ReoTypeT propertyType)
        {
            switch (propertyType)
            {
                case E_ReoTypeT._2_4Plx: 
                    return GsePropertyBase.TwoToFourUnitProperty;
                case E_ReoTypeT.ComNR: 
                    return GsePropertyBase.CommercialNonResidential;
                case E_ReoTypeT.ComR: 
                    return GsePropertyBase.HomeAndBusinessCombined;
                case E_ReoTypeT.Condo: 
                    return GsePropertyBase.Condominium;
                case E_ReoTypeT.Coop: 
                    return GsePropertyBase.Cooperative;
                case E_ReoTypeT.Farm: 
                    return GsePropertyBase.Farm;
                case E_ReoTypeT.Land: 
                    return GsePropertyBase.Land;
                case E_ReoTypeT.Mixed: 
                    return GsePropertyBase.MixedUseResidential;
                case E_ReoTypeT.Mobil: 
                    return GsePropertyBase.ManufacturedMobileHome;
                case E_ReoTypeT.Multi: 
                    return GsePropertyBase.MultifamilyMoreThanFourUnits;
                case E_ReoTypeT.SFR: 
                    return GsePropertyBase.SingleFamily;
                case E_ReoTypeT.Town: 
                    return GsePropertyBase.Townhouse;
                case E_ReoTypeT.Other: 
                case E_ReoTypeT.LeaveBlank:
                    return GsePropertyBase.Blank;
                default:
                    throw new UnhandledEnumException(propertyType);
            }
        }

        /// <summary>
        /// Creates a Hazard Insurance Coverage enumeration object based on the given housing expense type.
        /// </summary>
        /// <param name="expenseType">The housing expense type associated with the insurance policy.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HazardInsuranceCoverageBase}" /> object.</returns>
        public static MISMOEnum<HazardInsuranceCoverageBase> ToHazardInsuranceCoverageEnum(E_HousingExpenseTypeT expenseType)
        {
            var hazardInsuranceCoverageEnum = new MISMOEnum<HazardInsuranceCoverageBase>();
            hazardInsuranceCoverageEnum.enumValue = GetHazardInsuranceCoverageBase(expenseType);
            hazardInsuranceCoverageEnum.IsSensitive = false;

            return hazardInsuranceCoverageEnum.enumValue == HazardInsuranceCoverageBase.Blank ? null : hazardInsuranceCoverageEnum;
        }

        /// <summary>
        /// Gets the Hazard Insurance Coverage type corresponding to the given housing expense type.
        /// </summary>
        /// <param name="expenseType">The housing expense type associated with the insurance policy.</param>
        /// <returns>The Hazard Insurance Coverage type corresponding to the given housing expense type.</returns>
        public static HazardInsuranceCoverageBase GetHazardInsuranceCoverageBase(E_HousingExpenseTypeT expenseType)
        {
            switch (expenseType)
            {
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return HazardInsuranceCoverageBase.Homeowners;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return HazardInsuranceCoverageBase.Flood;
                case E_HousingExpenseTypeT.GroundRent:
                    return HazardInsuranceCoverageBase.Blank;
                case E_HousingExpenseTypeT.HazardInsurance:
                    return HazardInsuranceCoverageBase.Hazard;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.RealEstateTaxes:
                case E_HousingExpenseTypeT.SchoolTaxes:
                case E_HousingExpenseTypeT.Unassigned:
                    return HazardInsuranceCoverageBase.Blank;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return HazardInsuranceCoverageBase.Wind;
                default:
                    throw new UnhandledEnumException(expenseType);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{HMDAEthnicityBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HMDAEthnicityBase}" /> object.</returns>
        public static MISMOEnum<HMDAEthnicityBase> ToHmdaEthnicityEnum(E_aHispanicT lqbValue)
        {
            if (lqbValue == E_aHispanicT.LeaveBlank)
            {
                return null;
            }

            var hmdaEthnicityEnum = new MISMOEnum<HMDAEthnicityBase>();
            hmdaEthnicityEnum.enumValue = GetHmdaEthnicityBaseValue(lqbValue);
            hmdaEthnicityEnum.IsSensitive = false;

            return hmdaEthnicityEnum;
        }

        /// <summary>
        /// Retrieves the HMDAEthnicity enumeration corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding HMDAEthnicity value.</returns>
        public static HMDAEthnicityBase GetHmdaEthnicityBaseValue(E_aHispanicT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aHispanicT.Hispanic:
                    return HMDAEthnicityBase.HispanicOrLatino;
                case E_aHispanicT.NotHispanic:
                    return HMDAEthnicityBase.NotHispanicOrLatino;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates a serializable MISMO enum containing an applicant's ethnicity or ethnicity origin.
        /// </summary>
        /// <param name="ethnicity">The applicant's ethnicity or ethnicity origin.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<LqbHmdaEthnicityAndOriginBase> ToLqbHmdaEthnicityAndOriginEnum(Hmda_Ethnicity ethnicity)
        {
            if (ethnicity == Hmda_Ethnicity.Blank)
            {
                return null;
            }

            var ethnicityEnum = new MISMOEnum<LqbHmdaEthnicityAndOriginBase>();
            ethnicityEnum.enumValue = GetLqbHmdaEthnicityAndOriginBaseValue(ethnicity);
            ethnicityEnum.IsSensitive = false;

            return ethnicityEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to an applicant's ethnicity or ethnicity origin.
        /// </summary>
        /// <param name="ethnicity">The applicant's ethnicity or ethnicity origin.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaEthnicityAndOriginBase GetLqbHmdaEthnicityAndOriginBaseValue(Hmda_Ethnicity ethnicity)
        {
            switch (ethnicity)
            {
                case Hmda_Ethnicity.Cuban:
                    return LqbHmdaEthnicityAndOriginBase.Cuban;
                case Hmda_Ethnicity.HispanicOrLatino:
                    return LqbHmdaEthnicityAndOriginBase.HispanicOrLatino;
                case Hmda_Ethnicity.Mexican:
                    return LqbHmdaEthnicityAndOriginBase.Mexican;
                case Hmda_Ethnicity.NotApplicable:
                    return LqbHmdaEthnicityAndOriginBase.NotApplicable;
                case Hmda_Ethnicity.NotHispanicOrLatino:
                    return LqbHmdaEthnicityAndOriginBase.NotHispanicOrLatino;
                case Hmda_Ethnicity.NotProvided:
                    return LqbHmdaEthnicityAndOriginBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
                case Hmda_Ethnicity.OtherHispanicOrLatino:
                    return LqbHmdaEthnicityAndOriginBase.Other;
                case Hmda_Ethnicity.PuertoRican:
                    return LqbHmdaEthnicityAndOriginBase.PuertoRican;
                case Hmda_Ethnicity.NoCoApplicant:
                    return LqbHmdaEthnicityAndOriginBase.NoCoApplicant;
                default:
                    throw new UnhandledEnumException(ethnicity);
            }
        }

        /// <summary>
        /// Creates an HMDA preapproval enumeration object from the given preapproval status.
        /// </summary>
        /// <param name="preapproval">The preapproval request status.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HMDAPreapprovalBase}" /> object, or null if the loan has no preapproval status.</returns>
        public static MISMOEnum<HMDAPreapprovalBase> ToHmdaPreapprovalEnum(E_sHmdaPreapprovalT preapproval)
        {
            if (preapproval == E_sHmdaPreapprovalT.LeaveBlank)
            {
                return null;
            }

            var hmdaPreapprovalEnum = new MISMOEnum<HMDAPreapprovalBase>();
            hmdaPreapprovalEnum.enumValue = GetHmdaPreapprovalBaseValue(preapproval);
            hmdaPreapprovalEnum.IsSensitive = false;

            return hmdaPreapprovalEnum;
        }

        /// <summary>
        /// Retrieves the HMDA preapproval value corresponding to the given preapproval request status.
        /// </summary>
        /// <param name="preapproval">The preapproval request status.</param>
        /// <returns>The corresponding HMDA preapproval value.</returns>
        public static HMDAPreapprovalBase GetHmdaPreapprovalBaseValue(E_sHmdaPreapprovalT preapproval)
        {
            switch (preapproval)
            {
                case E_sHmdaPreapprovalT.NotApplicable:
                    return HMDAPreapprovalBase.NotApplicable;
                case E_sHmdaPreapprovalT.PreapprovalNotRequested:
                    return HMDAPreapprovalBase.PreapprovalWasNotRequested;
                case E_sHmdaPreapprovalT.PreapprovalRequested:
                    return HMDAPreapprovalBase.PreapprovalWasRequested;
                default:
                    throw new UnhandledEnumException(preapproval);
            }
        }

        /// <summary>
        /// Creates a MISMO enum containing a purchaser type.
        /// </summary>
        /// <param name="purchaserType">The loan purchaser type.</param>
        /// <returns>A MISMO enum.</returns>
        public static MISMOEnum<LqbHmdaPurchaserBase> ToHmdaPurchaserEnum(HmdaPurchaser2015T purchaserType)
        {
            if (purchaserType == HmdaPurchaser2015T.LeaveBlank)
            {
                return null;
            }

            var purchaserEnum = new MISMOEnum<LqbHmdaPurchaserBase>();
            purchaserEnum.enumValue = GetHmdaPurchaserBaseValue(purchaserType);
            purchaserEnum.IsSensitive = false;

            return purchaserEnum;
        }

        /// <summary>
        /// Gets the purchaser type value.
        /// </summary>
        /// <param name="purchaserType">The loan purchaser type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaPurchaserBase GetHmdaPurchaserBaseValue(HmdaPurchaser2015T purchaserType)
        {
            switch (purchaserType)
            {
                case HmdaPurchaser2015T.AffiliateInstitution:
                    return LqbHmdaPurchaserBase.AffiliateInstitution;
                case HmdaPurchaser2015T.CommercialBankSavingsBankSavingsAssociation:
                    return LqbHmdaPurchaserBase.CommercialOrSavingsBank;
                case HmdaPurchaser2015T.CreditUnionMortgageCompanyFinanceCompany:
                    return LqbHmdaPurchaserBase.CreditUnionMortgageCompanyFinanceCompany;
                case HmdaPurchaser2015T.LifeInsCreditUnion:
                    return LqbHmdaPurchaserBase.LifeInsuranceCreditUnion;
                case HmdaPurchaser2015T.LifeInsuranceCompany:
                    return LqbHmdaPurchaserBase.LifeInsuranceCompany;
                case HmdaPurchaser2015T.FannieMae:
                    return LqbHmdaPurchaserBase.FannieMae;
                case HmdaPurchaser2015T.FarmerMac:
                    return LqbHmdaPurchaserBase.FarmerMac;
                case HmdaPurchaser2015T.FreddieMac:
                    return LqbHmdaPurchaserBase.FreddieMac;
                case HmdaPurchaser2015T.GinnieMae:
                    return LqbHmdaPurchaserBase.GinnieMae;
                case HmdaPurchaser2015T.NA:
                    return LqbHmdaPurchaserBase.NotApplicable;
                case HmdaPurchaser2015T.PrivateSecuritizer:
                    return LqbHmdaPurchaserBase.PrivateSecuritization;
                case HmdaPurchaser2015T.Other:
                    return LqbHmdaPurchaserBase.Other;
                default:
                    throw new UnhandledEnumException(purchaserType);
            }
        }

        /// <summary>
        /// Creates an HMDA loan purpose enumeration object from the given loan purpose.
        /// </summary>
        /// <param name="loanPurpose">The purpose of the loan.</param>
        /// <param name="reportedAsHomeImprovement">Indicates whether the purpose of the loan is to finance a home improvement.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HMDAPurposeOfLoanBase}" /> object.</returns>
        public static MISMOEnum<HMDAPurposeOfLoanBase> ToHmdaPurposeOfLoanEnum(E_sLPurposeT loanPurpose, bool reportedAsHomeImprovement)
        {
            var hmdaPurposeOfLoanEnum = new MISMOEnum<HMDAPurposeOfLoanBase>();
            hmdaPurposeOfLoanEnum.enumValue = GetHmdaPurposeOfLoanBaseValue(loanPurpose, reportedAsHomeImprovement);
            hmdaPurposeOfLoanEnum.IsSensitive = false;

            if (hmdaPurposeOfLoanEnum.enumValue == HMDAPurposeOfLoanBase.Blank)
            {
                return null;
            }

            return hmdaPurposeOfLoanEnum;
        }

        /// <summary>
        /// Retrieves the HMDA loan purpose value corresponding to the LQB loan purpose.
        /// </summary>
        /// <param name="loanPurpose">The purpose of the loan.</param>
        /// <param name="reportedAsHomeImprovement">Indicates whether the purpose of the loan is to finance a home improvement.</param>
        /// <returns>The corresponding HMDA loan purpose value.</returns>
        public static HMDAPurposeOfLoanBase GetHmdaPurposeOfLoanBaseValue(E_sLPurposeT loanPurpose, bool reportedAsHomeImprovement)
        {
            switch (loanPurpose)
            {
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    return HMDAPurposeOfLoanBase.HomePurchase;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.HomeEquity:
                    return reportedAsHomeImprovement ? HMDAPurposeOfLoanBase.HomeImprovement : HMDAPurposeOfLoanBase.Refinancing;
                case E_sLPurposeT.Other:
                    return HMDAPurposeOfLoanBase.Blank;
                case E_sLPurposeT.Purchase:
                    return HMDAPurposeOfLoanBase.HomePurchase;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.VaIrrrl:
                    return reportedAsHomeImprovement ? HMDAPurposeOfLoanBase.HomeImprovement : HMDAPurposeOfLoanBase.Refinancing;
                default:
                    throw new UnhandledEnumException(loanPurpose);
            }
        }

        /// <summary>
        /// Converts a MISMO value into a serializable <see cref="MISMOEnum{HMDARaceBase}" /> object.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HMDARaceBase}" /> object.</returns>
        public static MISMOEnum<HMDARaceBase> ToHmdaRaceEnum(HMDARaceBase mismoValue)
        {
            var hmdaRaceEnum = new MISMOEnum<HMDARaceBase>();
            hmdaRaceEnum.enumValue = mismoValue;
            hmdaRaceEnum.IsSensitive = false;

            return hmdaRaceEnum;
        }

        /// <summary>
        /// Creates a serializable MISMO enum containing an applicant's race or race designation.
        /// </summary>
        /// <param name="race">An applicant's race or race designation.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<LqbHmdaRaceAndDesignationBase> ToLqbHmdaRaceAndDesignationEnum(Hmda_Race race)
        {
            if (race == Hmda_Race.Blank)
            {
                return null;
            }

            var raceEnum = new MISMOEnum<LqbHmdaRaceAndDesignationBase>();
            raceEnum.enumValue = GetLqbHmdaRaceAndDesignationBaseValue(race);
            raceEnum.IsSensitive = false;

            return raceEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to an applicant's race or race designation.
        /// </summary>
        /// <param name="race">An applicant's race or race designation.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaRaceAndDesignationBase GetLqbHmdaRaceAndDesignationBaseValue(Hmda_Race race)
        {
            switch (race)
            {
                case Hmda_Race.AmericanIndianOrAlaskanNative:
                    return LqbHmdaRaceAndDesignationBase.AmericanIndianOrAlaskaNative;
                case Hmda_Race.Asian:
                    return LqbHmdaRaceAndDesignationBase.Asian;
                case Hmda_Race.AsianIndian:
                    return LqbHmdaRaceAndDesignationBase.AsianIndian;
                case Hmda_Race.BlackOrAfricanAmerican:
                    return LqbHmdaRaceAndDesignationBase.BlackOrAfricanAmerican;
                case Hmda_Race.Chinese:
                    return LqbHmdaRaceAndDesignationBase.Chinese;
                case Hmda_Race.Filipino:
                    return LqbHmdaRaceAndDesignationBase.Filipino;
                case Hmda_Race.GuamanianOrChamorro:
                    return LqbHmdaRaceAndDesignationBase.GuamanianOrChamorro;
                case Hmda_Race.Japanese:
                    return LqbHmdaRaceAndDesignationBase.Japanese;
                case Hmda_Race.Korean:
                    return LqbHmdaRaceAndDesignationBase.Korean;
                case Hmda_Race.NativeHawaiian:
                    return LqbHmdaRaceAndDesignationBase.NativeHawaiian;
                case Hmda_Race.NativeHawaiianOrOtherPacificIslander:
                    return LqbHmdaRaceAndDesignationBase.NativeHawaiianOrOtherPacificIslander;
                case Hmda_Race.NotApplicable:
                    return LqbHmdaRaceAndDesignationBase.NotApplicable;
                case Hmda_Race.NotProvided:
                    return LqbHmdaRaceAndDesignationBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
                case Hmda_Race.OtherAsian:
                    return LqbHmdaRaceAndDesignationBase.OtherAsian;
                case Hmda_Race.OtherPacificIslander:
                    return LqbHmdaRaceAndDesignationBase.OtherPacificIslander;
                case Hmda_Race.Samoan:
                    return LqbHmdaRaceAndDesignationBase.Samoan;
                case Hmda_Race.Vietnamese:
                    return LqbHmdaRaceAndDesignationBase.Vietnamese;
                case Hmda_Race.White:
                    return LqbHmdaRaceAndDesignationBase.White;
                case Hmda_Race.NoCoApplicant:
                    return LqbHmdaRaceAndDesignationBase.NoCoApplicant;
                default:
                    throw new UnhandledEnumException(race);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{HomeownerPastThreeYearsBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HomeownerPastThreeYearsBase}" /> object.</returns>
        public static MISMOEnum<HomeownerPastThreeYearsBase> ToHomeownerPastThreeYearsEnum(string lqbValue)
        {
            var homeownerPastThreeYearsEnum = new MISMOEnum<HomeownerPastThreeYearsBase>();
            homeownerPastThreeYearsEnum.enumValue = GetHomeownerPastThreeYearsBaseValue(lqbValue);
            homeownerPastThreeYearsEnum.IsSensitive = false;

            return homeownerPastThreeYearsEnum;
        }

        /// <summary>
        /// Retrieves the HomeownerPastThreeYears value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding HomeownerPastThreeYears value.</returns>
        public static HomeownerPastThreeYearsBase GetHomeownerPastThreeYearsBaseValue(string lqbValue)
        {
            if (string.Equals(lqbValue, "Y", StringComparison.OrdinalIgnoreCase))
            {
                return HomeownerPastThreeYearsBase.Yes;
            }
            else if (string.Equals(lqbValue, "N", StringComparison.OrdinalIgnoreCase))
            {
                return HomeownerPastThreeYearsBase.No;
            }
            else
            {
                return HomeownerPastThreeYearsBase.Unknown;
            }
        }

        /// <summary>
        /// Converts a MISMO value into a <see cref="MISMOEnum{HousingExpensePaymentAmountBase}" /> object.
        /// </summary>
        /// <param name="mismoValue">The HousingExpensePaymentAmountBase value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HousingExpensePaymentAmountBase}" />.</returns>
        public static MISMOEnum<HousingExpensePaymentAmountBase> ToHousingExpensePaymentAmountEnum(HousingExpensePaymentAmountBase mismoValue)
        {
            var housingExpensePaymentAmountEnum = new MISMOEnum<HousingExpensePaymentAmountBase>();
            housingExpensePaymentAmountEnum.enumValue = mismoValue;
            housingExpensePaymentAmountEnum.IsSensitive = false;

            return housingExpensePaymentAmountEnum;
        }

        /// <summary>
        /// Converts a MISMO value into a <see cref="MISMOEnum{HousingExpenseBase}" /> object.
        /// </summary>
        /// <param name="mismoValue">The HousingExpenseBase value to be converted.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HousingExpenseBase}" /> object.</returns>
        public static MISMOEnum<HousingExpenseBase> ToHousingExpenseEnum(HousingExpenseBase mismoValue, bool isSensitiveData = false)
        {
            var housingExpenseEnum = new MISMOEnum<HousingExpenseBase>();
            housingExpenseEnum.enumValue = mismoValue;
            housingExpenseEnum.IsSensitive = isSensitiveData;

            return housingExpenseEnum;
        }

        /// <summary>
        /// Converts an LQB value into a <see cref="MISMOEnum{HELOCDailyPeriodicInterestRateCalculationBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{HELOCDailyPeriodicInterestRateCalculationBase}" /> object, or null if the LQB value is empty.</returns>
        public static MISMOEnum<HELOCDailyPeriodicInterestRateCalculationBase> ToHelocDailyPeriodicInterestRateCalculationEnum(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return null;
            }

            var helocDailyPeriodicInterestRateCalculationEnum = new MISMOEnum<HELOCDailyPeriodicInterestRateCalculationBase>();
            helocDailyPeriodicInterestRateCalculationEnum.enumValue = GetHelocDailyPeriodicInterestRateCalculationBaseValue(lqbValue);
            helocDailyPeriodicInterestRateCalculationEnum.IsSensitive = false;

            return helocDailyPeriodicInterestRateCalculationEnum;
        }

        /// <summary>
        /// Retrieves the HELOCDailyPeriodicInterestRateCalculation value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding HELOCDailyPeriodicInterestRateCalculation value.</returns>
        public static HELOCDailyPeriodicInterestRateCalculationBase GetHelocDailyPeriodicInterestRateCalculationBaseValue(string lqbValue)
        {
            switch (lqbValue)
            {
                case "360":
                    return HELOCDailyPeriodicInterestRateCalculationBase.Item360;
                case "365":
                    return HELOCDailyPeriodicInterestRateCalculationBase.Item365;
                default:
                    throw new ArgumentException("Unhandled value='" + lqbValue + "' for sDaysInYr");
            }
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{HELOCTeaserRateBase}" /> object with the HELOC teaser rate type corresponding to the initial rate type on the loan.
        /// </summary>
        /// <param name="rateType">The initial rate type on the HELOC.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A <see cref="MISMOEnum{HELOCTeaserRateBase}" /> object with the HELOC teaser rate type corresponding to the initial rate type on the loan.</returns>
        public static MISMOEnum<HELOCTeaserRateBase> ToHelocTeaserRateEnum(E_sInitialRateT rateType, bool isSensitiveData = false)
        {
            var teaserRateEnum = new MISMOEnum<HELOCTeaserRateBase>();
            teaserRateEnum.enumValue = GetHelocTeaserRateBase(rateType);
            teaserRateEnum.IsSensitive = isSensitiveData;

            if (teaserRateEnum.enumValue == HELOCTeaserRateBase.Blank)
            {
                return null;
            }

            return teaserRateEnum;
        }

        /// <summary>
        /// Converts the given initial rate type to a HELOC teaser rate type.
        /// </summary>
        /// <param name="rateType">The initial rate type on the HELOC.</param>
        /// <returns>The HELOC teaser rate type corresponding to the given initial rate type.</returns>
        public static HELOCTeaserRateBase GetHelocTeaserRateBase(E_sInitialRateT rateType)
        {
            switch (rateType)
            {
                case E_sInitialRateT.DiscountToMargin:
                    return HELOCTeaserRateBase.Adjustable;
                case E_sInitialRateT.FixedRate:
                    return HELOCTeaserRateBase.Fixed;
                case E_sInitialRateT.LeaveBlank:
                    return HELOCTeaserRateBase.Blank;
                case E_sInitialRateT.NoDiscount:
                    return HELOCTeaserRateBase.Fixed;
                default:
                    throw new UnhandledEnumException(rateType);
            }
        }

        /// <summary>
        /// Creates an indicator determining whether the loan is higher priced.
        /// </summary>
        /// <param name="pricedType">The high priced mortgage type.</param>
        /// <returns>A <see cref="MISMOIndicator"/> object.</returns>
        public static MISMOIndicator ToHigherPricedMortgageLoanIndicator(E_HighPricedMortgageT pricedType)
        {
            if (pricedType == E_HighPricedMortgageT.None || pricedType == E_HighPricedMortgageT.Unknown)
            {
                return null;
            }

            var mismoIndicator = new MISMOIndicator();
            mismoIndicator.booleanValue = pricedType == E_HighPricedMortgageT.HigherPricedQm
                || pricedType == E_HighPricedMortgageT.HPML;
            mismoIndicator.IsSensitive = false;

            return mismoIndicator;
        }

        /// <summary>
        /// Converts the agent type to real estate agent type.
        /// </summary>
        /// <param name="agentType">The agent type.</param>
        /// <returns>The RealEstateAgentBase corresponding to the agent type.</returns>
        public static RealEstateAgentBase GetRealEstateAgentBase(E_AgentRoleT agentType)
        {
            switch (agentType)
            {
                case E_AgentRoleT.AppraisalManagementCompany:
                case E_AgentRoleT.Appraiser:
                case E_AgentRoleT.Bank:
                case E_AgentRoleT.Broker:
                case E_AgentRoleT.BrokerProcessor:
                case E_AgentRoleT.BrokerRep:
                case E_AgentRoleT.Builder:
                    return RealEstateAgentBase.Blank;
                case E_AgentRoleT.BuyerAgent:
                    return RealEstateAgentBase.Selling;
                case E_AgentRoleT.BuyerAttorney:
                case E_AgentRoleT.CallCenterAgent:
                case E_AgentRoleT.ClosingAgent:
                case E_AgentRoleT.CollateralAgent:
                case E_AgentRoleT.CreditAuditor:
                case E_AgentRoleT.CreditReport:
                case E_AgentRoleT.CreditReportAgency2:
                case E_AgentRoleT.CreditReportAgency3:
                case E_AgentRoleT.DisclosureDesk:
                case E_AgentRoleT.DocDrawer:
                case E_AgentRoleT.ECOA:
                case E_AgentRoleT.Escrow:
                case E_AgentRoleT.ExternalPostCloser:
                case E_AgentRoleT.ExternalSecondary:
                case E_AgentRoleT.FairHousingLending:
                case E_AgentRoleT.FloodProvider:
                case E_AgentRoleT.Funder:
                case E_AgentRoleT.HazardInsurance:
                case E_AgentRoleT.HomeInspection:
                case E_AgentRoleT.HomeOwnerAssociation:
                case E_AgentRoleT.HomeOwnerInsurance:
                case E_AgentRoleT.Insuring:
                case E_AgentRoleT.Investor:
                case E_AgentRoleT.JuniorProcessor:
                case E_AgentRoleT.JuniorUnderwriter:
                case E_AgentRoleT.LegalAuditor:
                case E_AgentRoleT.Lender:
                    return RealEstateAgentBase.Blank;
                case E_AgentRoleT.ListingAgent:
                    return RealEstateAgentBase.Listing;
                case E_AgentRoleT.LoanOfficer:
                case E_AgentRoleT.LoanOfficerAssistant:
                case E_AgentRoleT.LoanOpener:
                case E_AgentRoleT.LoanPurchasePayee:
                case E_AgentRoleT.Manager:
                case E_AgentRoleT.MarketingLead:
                case E_AgentRoleT.Mortgagee:
                case E_AgentRoleT.MortgageInsurance:
                case E_AgentRoleT.Other:
                case E_AgentRoleT.PestInspection:
                case E_AgentRoleT.PostCloser:
                case E_AgentRoleT.Processor:
                case E_AgentRoleT.PropertyManagement:
                case E_AgentRoleT.Purchaser:
                case E_AgentRoleT.QCCompliance:
                    return RealEstateAgentBase.Blank;
                case E_AgentRoleT.Realtor:
                    return RealEstateAgentBase.Other;
                case E_AgentRoleT.Secondary:
                case E_AgentRoleT.Seller:
                case E_AgentRoleT.SellerAttorney:
                    return RealEstateAgentBase.Blank;
                case E_AgentRoleT.SellingAgent:
                    return RealEstateAgentBase.Selling;
                case E_AgentRoleT.Servicing:
                case E_AgentRoleT.Shipper:
                case E_AgentRoleT.Subservicer:
                case E_AgentRoleT.Surveyor:
                case E_AgentRoleT.Title:
                case E_AgentRoleT.TitleUnderwriter:
                case E_AgentRoleT.Trustee:
                case E_AgentRoleT.Underwriter:
                    return RealEstateAgentBase.Blank;
                default:
                    throw new UnhandledEnumException(agentType);
            }
        }

        /// <summary>
        /// Converts a visual observation indicator to a MISMO indicator.
        /// </summary>
        /// <param name="collectionStatus">Indicates whether HMDA information was obtained by visual observation or surname.</param>
        /// <returns>A MISMO indicator with the corresponding value.</returns>
        public static MISMOEnum<LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase> ToHmdaDataCollectedByObservationOrSurnameEnum(VisualObservationSurnameCollection collectionStatus)
        {
            if (collectionStatus == VisualObservationSurnameCollection.Blank)
            {
                return null;
            }

            var collectionEnum = new MISMOEnum<LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase>();
            collectionEnum.enumValue = GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(collectionStatus);
            collectionEnum.IsSensitive = false;

            return collectionEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the collection status.
        /// </summary>
        /// <param name="collectionStatus">The data collection status.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(VisualObservationSurnameCollection collectionStatus)
        {
            switch (collectionStatus)
            {
                case VisualObservationSurnameCollection.CollectedOnBasisOfVisualObservationOrSurname:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.CollectedBasedOnVisualObservationOrSurname;
                case VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.NotCollectedBasedOnVisualObservationOrSurname;
                case VisualObservationSurnameCollection.NotApplicable:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.NotApplicable;
                case VisualObservationSurnameCollection.NoCoApplicant:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.NoCoApplicant;
                default:
                    throw new UnhandledEnumException(collectionStatus);
            }
        }

        /// <summary>
        /// Converts an interest-only indicator to a MISMO indicator.
        /// </summary>
        /// <param name="hasInterestOnlyPayments">Indicates whether a loan has interest-only payments.</param>
        /// <returns>A MISMO indicator with the corresponding value.</returns>
        public static MISMOIndicator ToInterestOnlyPaymentsIndicator(sHmdaInterestOnlyPaymentT hasInterestOnlyPayments)
        {
            if (hasInterestOnlyPayments == sHmdaInterestOnlyPaymentT.LeaveBlank)
            {
                return null;
            }

            var indicator = new MISMOIndicator();
            indicator.booleanValue = hasInterestOnlyPayments == sHmdaInterestOnlyPaymentT.InterestOnlyPayments;
            indicator.IsSensitive = false;

            return indicator;
        }

        /// <summary>
        /// Converts a balloon indicator to a MISMO indicator.
        /// </summary>
        /// <param name="hasBalloonPayment">Indicates whether a loan has a balloon payment.</param>
        /// <returns>A MISMO indicator with the corresponding value.</returns>
        public static MISMOIndicator ToBalloonPaymentIndicator(sHmdaBalloonPaymentT hasBalloonPayment)
        {
            if (hasBalloonPayment == sHmdaBalloonPaymentT.LeaveBlank)
            {
                return null;
            }

            var indicator = new MISMOIndicator();
            indicator.booleanValue = hasBalloonPayment == sHmdaBalloonPaymentT.BalloonPayment;
            indicator.IsSensitive = false;

            return indicator;
        }

        /// <summary>
        /// Converts a negative amortization indicator to a MISMO indicator.
        /// </summary>
        /// <param name="hasNegativeAmortization">Indicates whether a loan has negative amortization.</param>
        /// <returns>A MISMO indicator with the corrsponding value.</returns>
        public static MISMOIndicator ToNegativeAmortizationIndicator(sHmdaNegativeAmortizationT hasNegativeAmortization)
        {
            if (hasNegativeAmortization == sHmdaNegativeAmortizationT.LeaveBlank)
            {
                return null;
            }

            var indicator = new MISMOIndicator();
            indicator.booleanValue = hasNegativeAmortization == sHmdaNegativeAmortizationT.NegativeAmortization;
            indicator.IsSensitive = false;

            return indicator;
        }

        /// <summary>
        /// Converts a non-amortizing features indicator to a MISMO indicator.
        /// </summary>
        /// <param name="hasOtherNonAmortizingFeatures">Indicates whether a loan has non-amortizing features.</param>
        /// <returns>A MISMO indicator with the corresponding value.</returns>
        public static MISMOIndicator ToOtherNonAmortizingFeaturesIndicator(sHmdaOtherNonAmortFeatureT hasOtherNonAmortizingFeatures)
        {
            if (hasOtherNonAmortizingFeatures == sHmdaOtherNonAmortFeatureT.LeaveBlank)
            {
                return null;
            }

            var indicator = new MISMOIndicator();
            indicator.booleanValue = hasOtherNonAmortizingFeatures == sHmdaOtherNonAmortFeatureT.OtherNonFullyAmortizingFeatures;
            indicator.IsSensitive = false;

            return indicator;
        }

        /// <summary>
        /// Converts a reverse mortgage indicator to a MISMO indicator.
        /// </summary>
        /// <param name="isReverseMortgage">Indicates whether a loan is a reverse mortgage.</param>
        /// <returns>A MISMO indicator with the corresponding value.</returns>
        public static MISMOIndicator ToReverseMortgageIndicator(sHmdaReverseMortgageT isReverseMortgage)
        {
            if (isReverseMortgage == sHmdaReverseMortgageT.LeaveBlank)
            {
                return null;
            }

            var indicator = new MISMOIndicator();
            indicator.booleanValue = isReverseMortgage == sHmdaReverseMortgageT.ReverseMortgage;
            indicator.IsSensitive = false;

            return indicator;
        }

        /// <summary>
        /// Converts a business purpose indicator to a MISMO indicator.
        /// </summary>
        /// <param name="hasBusinessPurpose">Indicates whether a loan is made for a business purpose.</param>
        /// <returns>A MISMO indicator with the corresponding value.</returns>
        public static MISMOIndicator ToBusinessPurposeIndicator(sHmdaBusinessPurposeT hasBusinessPurpose)
        {
            if (hasBusinessPurpose == sHmdaBusinessPurposeT.LeaveBlank)
            {
                return null;
            }

            var indicator = new MISMOIndicator();
            indicator.booleanValue = hasBusinessPurpose == sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial;
            indicator.IsSensitive = false;

            return indicator;
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{RealEstateAgentBase}" /> object based on the given agent type.
        /// </summary>
        /// <param name="agentType">The agent type.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A <see cref="MISMOEnum{RealEstateAgentBase}" /> object based on the given agent type.</returns>
        public static MISMOEnum<RealEstateAgentBase> ToRealEstateAgentEnum(E_AgentRoleT agentType, bool isSensitiveData = false)
        {
            var agent = new MISMOEnum<RealEstateAgentBase>();
            agent.enumValue = GetRealEstateAgentBase(agentType);
            agent.IsSensitive = isSensitiveData;

            if (agent.enumValue == RealEstateAgentBase.Blank)
            {
                return null;
            }

            return agent;
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{RealEstateAgentBase}" /> object based on the given agent type.
        /// </summary>
        /// <param name="agentType">The agent type.</param>
        /// <returns>A <see cref="MISMOEnum{RealEstateAgentBase}" /> object based on the given agent type.</returns>
        public static MISMOEnum<RealEstateAgentBase> ToRealEstateAgentEnum(RealEstateAgentBase agentType)
        {
            var realEstateAgentEnum = new MISMOEnum<RealEstateAgentBase>();
            realEstateAgentEnum.enumValue = agentType;
            realEstateAgentEnum.IsSensitive = false;

            return realEstateAgentEnum;
        }

        /// <summary>
        /// Converts an LQB value into a <see cref="MISMOEnum{RefinanceCashOutDeterminationBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <param name="loanPurpose">The purpose of the loan.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{RefinanceCashOutDeterminationBase}" /> object.</returns>
        public static MISMOEnum<RefinanceCashOutDeterminationBase> ToRefinanceCashOutDeterminationEnum(string lqbValue, E_sLPurposeT loanPurpose, bool displaySensitive = true)
        {
            var refinanceCashOutDeterminationEnum = new MISMOEnum<RefinanceCashOutDeterminationBase>();
            refinanceCashOutDeterminationEnum.enumValue = GetRefinanceCashOutDeterminationBaseValue(lqbValue, loanPurpose);
            refinanceCashOutDeterminationEnum.IsSensitive = false;
            refinanceCashOutDeterminationEnum.DisplaySensitive = displaySensitive;

            return refinanceCashOutDeterminationEnum;
        }

        /// <summary>
        /// Retrieves the RefinanceCashOutDetermination value corresponding to the given LQB value, or attempts to deduce from the loan purpose.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <param name="loanPurpose">The purpose of the loan.</param>
        /// <returns>The corresponding value.</returns>
        public static RefinanceCashOutDeterminationBase GetRefinanceCashOutDeterminationBaseValue(string lqbValue, E_sLPurposeT loanPurpose)
        {
            if (string.Equals("No Cash-Out Rate/Term", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinanceCashOutDeterminationBase.NoCashOut;
            }
            else if (string.Equals("Limited Cash-Out Rate/Term", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinanceCashOutDeterminationBase.LimitedCashOut;
            }
            else if (string.Equals("Cash-Out/Home Improvement", lqbValue, StringComparison.OrdinalIgnoreCase) ||
                     string.Equals("Cash-Out/Debt Consolidation", lqbValue, StringComparison.OrdinalIgnoreCase) ||
                     string.Equals("Cash-Out/Other", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinanceCashOutDeterminationBase.CashOut;
            }
            else if (loanPurpose == E_sLPurposeT.Refin ||
                     loanPurpose == E_sLPurposeT.FhaStreamlinedRefinance ||
                     loanPurpose == E_sLPurposeT.VaIrrrl)
            {
                return RefinanceCashOutDeterminationBase.NoCashOut;
            }
            else if (loanPurpose == E_sLPurposeT.RefinCashout || loanPurpose == E_sLPurposeT.HomeEquity)
            {
                return RefinanceCashOutDeterminationBase.CashOut;
            }
            else
            {
                return RefinanceCashOutDeterminationBase.Unknown;
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{RefinancePrimaryPurposeBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{RefinancePrimaryPurposeBase}" /> object.</returns>
        public static MISMOEnum<RefinancePrimaryPurposeBase> ToRefinancePrimaryPurposeEnum(string lqbValue)
        {
            var refinancePrimaryPurposeEnum = new MISMOEnum<RefinancePrimaryPurposeBase>();
            refinancePrimaryPurposeEnum.enumValue = GetRefinancePrimaryPurposeBaseValue(lqbValue);
            refinancePrimaryPurposeEnum.IsSensitive = false;

            return refinancePrimaryPurposeEnum;
        }

        /// <summary>
        /// Retrieves the RefinancePrimaryPurpose value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding RefinancePrimaryPurpose value.</returns>
        public static RefinancePrimaryPurposeBase GetRefinancePrimaryPurposeBaseValue(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return RefinancePrimaryPurposeBase.Unspecified;
            }
            else if (string.Equals("Cash-Out/Home Improvement", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinancePrimaryPurposeBase.HomeImprovement;
            }
            else if (string.Equals("Cash-Out/Debt Consolidation", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinancePrimaryPurposeBase.DebtConsolidation;
            }
            else
            {
                return RefinancePrimaryPurposeBase.Other;
            }
        }

        /// <summary>
        /// Creates a relationship vesting enumeration object based on the given title holding manner.
        /// </summary>
        /// <param name="manner">The manner in which the title will be held.</param>
        /// <returns>A serializable <see cref="MISMOEnum{RelationshipVestingBase}" /> object.</returns>
        public static MISMOEnum<RelationshipVestingBase> ToRelationshipVestingEnum(string manner)
        {
            var relationshipVestingEnum = new MISMOEnum<RelationshipVestingBase>();
            relationshipVestingEnum.enumValue = GetRelationshipVestingBaseValue(manner);
            relationshipVestingEnum.IsSensitive = false;

            return relationshipVestingEnum;
        }

        /// <summary>
        /// Retrieves the relationship vesting value corresponding to the given title holding manner.
        /// </summary>
        /// <param name="manner">The manner in which the title will be held.</param>
        /// <returns>The corresponding relationship vesting value.</returns>
        public static RelationshipVestingBase GetRelationshipVestingBaseValue(string manner)
        {
            if (string.IsNullOrEmpty(manner))
            {
                return RelationshipVestingBase.Unassigned;
            }
            else if (string.Equals(manner, "As Community Property", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.CommunityProperty;
            }
            else if (string.Equals(manner, "All as Joint Tenants", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "As Joint Tenants", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Wife as Joint Tenants", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Husband", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Wife", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Spouse and Spouse", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Wife and Husband", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Wife and Wife", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.JointTenants;
            }
            else if (string.Equals(manner, "All as Tenants in Common", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "As Tenants in Common", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Wife as Tenants in Common", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.TenantsInCommon;
            }
            else if (string.Equals(manner, "As Tenants by the Entirety", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.TenantsByTheEntirety;
            }
            else if (string.Equals(manner, "As Joint Tenants with Right of Survivorship", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Wife as Joint Tenants with Right of Survivorship", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.JointTenantsWithRightOfSurvivorship;
            }
            else if (string.Equals(manner, "Single Man", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Single Woman", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Unmarried Man", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Unmarried Woman", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Married Man", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Married Woman", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.Individual;
            }
            else
            {
                return RelationshipVestingBase.Other;
            }
        }

        /// <summary>
        /// Creates a new closing adjustment item enumeration object with the given item.
        /// </summary>
        /// <param name="type">The closing adjustment item type.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ClosingAdjustmentItemBase}" /> object.</returns>
        public static MISMOEnum<ClosingAdjustmentItemBase> ToClosingAdjustmentItemEnum(ClosingAdjustmentItemBase type, string displayLabelText, bool isSensitiveData = false)
        {
            if (type == ClosingAdjustmentItemBase.Blank)
            {
                return null;
            }

            var closingAdjustmentItemEnum = new MISMOEnum<ClosingAdjustmentItemBase>();
            closingAdjustmentItemEnum.enumValue = type;
            closingAdjustmentItemEnum.IsSensitive = isSensitiveData;
            closingAdjustmentItemEnum.DisplayLabelText = displayLabelText;

            return closingAdjustmentItemEnum;
        }

        /// <summary>
        /// Gets the closing adjustment item base value corresponding to the adjustment type.
        /// </summary>
        /// <param name="adjustmentType">The adjustment type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static ClosingAdjustmentItemBase GetClosingAdjustmentItemBaseValue(E_AdjustmentT adjustmentType)
        {
            switch (adjustmentType)
            {
                case E_AdjustmentT.FuelCosts:
                    return ClosingAdjustmentItemBase.FuelCosts;
                case E_AdjustmentT.Gift:
                    return ClosingAdjustmentItemBase.Gift;
                case E_AdjustmentT.Grant:
                    return ClosingAdjustmentItemBase.Grant;
                case E_AdjustmentT.ProceedsOfSubordinateLiens:
                    return ClosingAdjustmentItemBase.ProceedsOfSubordinateLiens;
                case E_AdjustmentT.RebateCredit:
                    return ClosingAdjustmentItemBase.RebateCredit;
                case E_AdjustmentT.RelocationFunds:
                    return ClosingAdjustmentItemBase.RelocationFunds;
                case E_AdjustmentT.RentFromSubjectProperty:
                    return ClosingAdjustmentItemBase.RentFromSubjectProperty;
                case E_AdjustmentT.RepairCompletionEscrowHoldback:
                    return ClosingAdjustmentItemBase.RepairCompletionEscrowHoldback;
                case E_AdjustmentT.Repairs:
                    return ClosingAdjustmentItemBase.Repairs;
                case E_AdjustmentT.SatisfactionOfSubordinateLien:
                    return ClosingAdjustmentItemBase.SatisfactionOfSubordinateLien;
                case E_AdjustmentT.SellerCredit:
                    return ClosingAdjustmentItemBase.SellerCredit;
                case E_AdjustmentT.SellersEscrowAssumption:
                    return ClosingAdjustmentItemBase.SellersEscrowAssumption;
                case E_AdjustmentT.SellersMortgageInsuranceAssumption:
                    return ClosingAdjustmentItemBase.SellersMortgageInsuranceAssumption;
                case E_AdjustmentT.Services:
                    return ClosingAdjustmentItemBase.Services;
                case E_AdjustmentT.SubordinateFinancingProceeds:
                    return ClosingAdjustmentItemBase.SubordinateFinancingProceeds;
                case E_AdjustmentT.SweatEquity:
                    return ClosingAdjustmentItemBase.SweatEquity;
                case E_AdjustmentT.TenantSecurityDeposit:
                    return ClosingAdjustmentItemBase.TenantSecurityDeposit;
                case E_AdjustmentT.TradeEquity:
                    return ClosingAdjustmentItemBase.TradeEquity;
                case E_AdjustmentT.UnpaidUtilityEscrowHoldback:
                    return ClosingAdjustmentItemBase.UnpaidUtilityEscrowHoldback;
                case E_AdjustmentT.Blank:
                case E_AdjustmentT.BorrowerPaidFees:
                case E_AdjustmentT.BuydownFund:
                case E_AdjustmentT.CommitmentOriginationFee:
                case E_AdjustmentT.EmployerAssistedHousing:
                case E_AdjustmentT.FederalAgencyFundingFeeRefund:
                case E_AdjustmentT.LeasePurchaseFund:
                case E_AdjustmentT.MIPremiumRefund:
                case E_AdjustmentT.Other:
                case E_AdjustmentT.SellersReserveAccountAssumption:
                case E_AdjustmentT.TitlePremiumAdjustment:
                case E_AdjustmentT.TradeEquityCredit:
                case E_AdjustmentT.TradeEquityFromPropertySwap:
                case E_AdjustmentT.GiftOfEquity:
                case E_AdjustmentT.PrincipalReductionToReduceCashToBorrower:
                case E_AdjustmentT.PrincipalReductionToCureToleranceViolation:
                    return ClosingAdjustmentItemBase.Other;
                default:
                    throw UnhandledEnumException.GenericException("Unhandled enum exception in E_AdjustmentT");
            }
        }

        /// <summary>
        /// Gets the TRID subsection for a closing adjustment paid to a borrower.
        /// </summary>
        /// <param name="adjustmentType">The adjustment type.</param>
        /// <returns>The corresponding subsection value.</returns>
        public static IntegratedDisclosureSubsectionBase GetIntegratedDisclosureSubsectionBaseValue_AdjustmentPaidToBorrower(E_AdjustmentT adjustmentType)
        {
            if (adjustmentType == E_AdjustmentT.Gift
                || adjustmentType == E_AdjustmentT.Grant
                || adjustmentType == E_AdjustmentT.RebateCredit
                || adjustmentType == E_AdjustmentT.Other)
            {
                return IntegratedDisclosureSubsectionBase.OtherCredits;
            }
            else
            {
                return IntegratedDisclosureSubsectionBase.Adjustments;
            }
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{ClosingAdjustmentItemPaidByBase}" /> object.
        /// </summary>
        /// <param name="paidBy">The type of entity that the adjustment is paid by.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ClosingAdjustmentItemPaidByBase}" /> object.</returns>
        public static MISMOEnum<ClosingAdjustmentItemPaidByBase> ToClosingAdjustmentItemPaidByEnum(E_PartyT paidBy, bool isSensitiveData = false)
        {
            var paidByEnum = new MISMOEnum<ClosingAdjustmentItemPaidByBase>();
            paidByEnum.enumValue = GetClosingAdjustmentItemPaidByBase(paidBy);
            paidByEnum.IsSensitive = isSensitiveData;

            if (paidByEnum.enumValue == ClosingAdjustmentItemPaidByBase.Blank)
            {
                return null;
            }

            return paidByEnum;
        }

        /// <summary>
        /// Converts the given party type to a ClosingAdjustmentItemPaidByBase.
        /// </summary>
        /// <param name="partyType">The type of party that will/has paid the adjustment amount.</param>
        /// <returns>The ClosingAdjustmentItemPaidByBase corresponding to the given party type.</returns>
        public static ClosingAdjustmentItemPaidByBase GetClosingAdjustmentItemPaidByBase(E_PartyT partyType)
        {
            switch (partyType)
            {
                case E_PartyT.Blank:
                    return ClosingAdjustmentItemPaidByBase.Blank;
                case E_PartyT.Borrower:
                    return ClosingAdjustmentItemPaidByBase.Buyer;
                case E_PartyT.BorrowersEmployer:
                case E_PartyT.BorrowersFriend:
                case E_PartyT.BorrowersParent:
                case E_PartyT.BorrowersRelative:
                case E_PartyT.BuilderOrDeveloper:
                case E_PartyT.FederalAgency:
                    return ClosingAdjustmentItemPaidByBase.ThirdParty;
                case E_PartyT.Lender:
                    return ClosingAdjustmentItemPaidByBase.Lender;
                case E_PartyT.Other:
                case E_PartyT.RealEstateAgent:
                    return ClosingAdjustmentItemPaidByBase.ThirdParty;
                case E_PartyT.Seller:
                    return ClosingAdjustmentItemPaidByBase.Seller;
                default:
                    throw new UnhandledEnumException(partyType);
            }
        }

        /// <summary>
        /// Creates a new closing agent enumeration object with the given closing agent type.
        /// </summary>
        /// <param name="agentType">The closing agent type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ClosingAgentBase}" /> object.</returns>
        public static MISMOEnum<ClosingAgentBase> ToClosingAgentEnum(ClosingAgentBase agentType)
        {
            var closingAgentEnum = new MISMOEnum<ClosingAgentBase>();
            closingAgentEnum.enumValue = agentType;
            closingAgentEnum.IsSensitive = false;

            return closingAgentEnum;
        }

        /// <summary>
        /// Converts an LQB value to a <see cref="MISMOEnum{ConstructionLoanBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ConstructionLoanBase}" /> object.</returns>
        public static MISMOEnum<ConstructionLoanBase> ToConstructionLoanEnum(E_sLPurposeT lqbValue)
        {
            var constructionLoanEnum = new MISMOEnum<ConstructionLoanBase>();
            constructionLoanEnum.enumValue = GetConstructionLoanBaseValue(lqbValue);
            constructionLoanEnum.IsSensitive = false;
            if (constructionLoanEnum.enumValue == ConstructionLoanBase.Blank)
            {
                return null;
            }
            
            return constructionLoanEnum;
        }

        /// <summary>
        /// Retrieves the ConstructionLoan enumeration corresponding to an LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding ConstructionLoan value.</returns>
        public static ConstructionLoanBase GetConstructionLoanBaseValue(E_sLPurposeT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sLPurposeT.Construct:
                    return ConstructionLoanBase.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm:
                    return ConstructionLoanBase.ConstructionToPermanent;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.HomeEquity:
                case E_sLPurposeT.Purchase:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.Other:
                    return ConstructionLoanBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Generates a MISMO enum containing a construction loan calculation method.
        /// </summary>
        /// <param name="calcType">The calculation method.</param>
        /// <returns>A MISMO enum.</returns>
        public static MISMOEnum<ConstructionLoanEstimatedInterestCalculationMethodBase> ToConstructionLoanEstimatedInterestCalculationMethodEnum(ConstructionIntCalcType calcType)
        {
            if (calcType == ConstructionIntCalcType.Blank)
            {
                return null;
            }

            var calculationMethodEnum = new MISMOEnum<ConstructionLoanEstimatedInterestCalculationMethodBase>();
            calculationMethodEnum.enumValue = GetConstructionLoanEstimatedInterestCalculationMethodBaseValue(calcType);
            calculationMethodEnum.IsSensitive = false;
            calculationMethodEnum.DisplaySensitive = true;

            return calculationMethodEnum;
        }

        /// <summary>
        /// Maps LendingQB data to a construction loan calculation method.
        /// </summary>
        /// <param name="calcType">The LendingQB calculation method.</param>
        /// <returns>A MISMO construction loan calculation method.</returns>
        public static ConstructionLoanEstimatedInterestCalculationMethodBase GetConstructionLoanEstimatedInterestCalculationMethodBaseValue(ConstructionIntCalcType calcType)
        {
            switch (calcType)
            {
                case ConstructionIntCalcType.FullCommitment:
                    return ConstructionLoanEstimatedInterestCalculationMethodBase.FullLoanCommitment;
                case ConstructionIntCalcType.HalfCommitment:
                    return ConstructionLoanEstimatedInterestCalculationMethodBase.HalfLoanCommitment;
                default:
                    throw new UnhandledEnumException(calcType);
            }
        }

        /// <summary>
        /// Generates a MISMO enum containing a construction loan purpose.
        /// </summary>
        /// <param name="purpose">The LendingQB construction purpose.</param>
        /// <returns>A MISMO enum.</returns>
        public static MISMOEnum<LqbConstructionPurposeBase> ToLqbConstructionPurposeEnum(ConstructionPurpose purpose)
        {
            if (purpose == ConstructionPurpose.Blank)
            {
                return null;
            }

            var purposeEnum = new MISMOEnum<LqbConstructionPurposeBase>();
            purposeEnum.enumValue = GetLqbConstructionPurposeBaseValue(purpose);
            purposeEnum.IsSensitive = false;
            purposeEnum.DisplaySensitive = true;

            return purposeEnum;
        }

        /// <summary>
        /// Maps LendingQB data to a MISMO construction purpose.
        /// </summary>
        /// <param name="purpose">The LendingQB construction purpose.</param>
        /// <returns>A MISMO construction purpose.</returns>
        public static LqbConstructionPurposeBase GetLqbConstructionPurposeBaseValue(ConstructionPurpose purpose)
        {
            switch (purpose)
            {
                case ConstructionPurpose.ConstructionAndLotPurchase:
                    return LqbConstructionPurposeBase.ConstructionAndLotPurchase;
                case ConstructionPurpose.ConstructionAndLotRefinance:
                    return LqbConstructionPurposeBase.ConstructionAndLotRefinance;
                case ConstructionPurpose.ConstructionOnOwnedLot:
                    return LqbConstructionPurposeBase.Construction;
                default:
                    throw new UnhandledEnumException(purpose);
            }
        }

        /// <summary>
        /// Generates a MISMO enum containing a construction loan amortization type.
        /// </summary>
        /// <param name="amortType">The amortization type.</param>
        /// <returns>A MISMO enum.</returns>
        public static MISMOEnum<LqbConstructionLoanAmortizationBase> ToLqbConstructionLoanAmortizationEnum(E_sFinMethT amortType)
        {
            var amortTypeEnum = new MISMOEnum<LqbConstructionLoanAmortizationBase>();
            amortTypeEnum.enumValue = GetLqbConstructionLoanAmortizationBaseValue(amortType);
            amortTypeEnum.IsSensitive = false;
            amortTypeEnum.DisplaySensitive = true;

            return amortTypeEnum;
        }

        /// <summary>
        /// Maps LendingQB data to a MISMO construction loan amortization type.
        /// </summary>
        /// <param name="amortType">The amortization type.</param>
        /// <returns>A MISMO construction loan amortization type.</returns>
        public static LqbConstructionLoanAmortizationBase GetLqbConstructionLoanAmortizationBaseValue(E_sFinMethT amortType)
        {
            switch (amortType)
            {
                case E_sFinMethT.ARM:
                    return LqbConstructionLoanAmortizationBase.AdjustableRate;
                case E_sFinMethT.Fixed:
                    return LqbConstructionLoanAmortizationBase.Fixed;
                default:
                    throw new UnhandledEnumException(amortType);
            }
        }

        /// <summary>
        /// Generates a MISMO enum containing an interest accrual type for the construction phase of a construction loan.
        /// </summary>
        /// <param name="accrualType">The interest accrual type.</param>
        /// <returns>A MISMO enum.</returns>
        public static MISMOEnum<LqbConstructionPhaseInterestAccrualBase> ToLqbConstructionPhaseInterestAccrualEnum(ConstructionPhaseIntAccrual accrualType)
        {
            if (accrualType == ConstructionPhaseIntAccrual.Blank)
            {
                return null;
            }

            var accrualTypeEnum = new MISMOEnum<LqbConstructionPhaseInterestAccrualBase>();
            accrualTypeEnum.enumValue = GetLqbConstructionPhaseInterestAccrualBaseValue(accrualType);
            accrualTypeEnum.IsSensitive = false;
            accrualTypeEnum.DisplaySensitive = true;

            return accrualTypeEnum;
        }

        /// <summary>
        /// Maps LendingQB data to a MISMO construction phase interest accrual type.
        /// </summary>
        /// <param name="accrualType">The interest accrual type.</param>
        /// <returns>A MISMO construction phase interest accrual type.</returns>
        public static LqbConstructionPhaseInterestAccrualBase GetLqbConstructionPhaseInterestAccrualBaseValue(ConstructionPhaseIntAccrual accrualType)
        {
            switch (accrualType)
            {
                case ConstructionPhaseIntAccrual.ActualDays_365_360:
                    return LqbConstructionPhaseInterestAccrualBase.Daily365_360;
                case ConstructionPhaseIntAccrual.ActualDays_365_365:
                    return LqbConstructionPhaseInterestAccrualBase.Daily365_365;
                case ConstructionPhaseIntAccrual.Monthly_360_360:
                    return LqbConstructionPhaseInterestAccrualBase.Monthly360_360;
                default:
                    throw new UnhandledEnumException(accrualType);
            }
        }

        /// <summary>
        /// Creates a construction method enumeration object based on the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ConstructionMethodBase}" /> object.</returns>
        public static MISMOEnum<ConstructionMethodBase> ToConstructionMethodEnum(E_sGseSpT propertyType, bool displaySensitive = true)
        {
            var constructionMethodEnum = new MISMOEnum<ConstructionMethodBase>();
            constructionMethodEnum.enumValue = GetConstructionMethodBaseValue(propertyType);
            constructionMethodEnum.IsSensitive = false;
            constructionMethodEnum.DisplaySensitive = displaySensitive;

            return constructionMethodEnum;
        }

        /// <summary>
        /// Gets the construction method value corresponding to the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding construction method value.</returns>
        public static ConstructionMethodBase GetConstructionMethodBaseValue(E_sGseSpT propertyType)
        {
            if (propertyType == E_sGseSpT.ManufacturedHousing
                || propertyType == E_sGseSpT.ManufacturedHomeCondominium
                || propertyType == E_sGseSpT.ManufacturedHomeMultiwide
                || propertyType == E_sGseSpT.ManufacturedHousingSingleWide)
            {
                return ConstructionMethodBase.Manufactured;
            }
            else if (propertyType == E_sGseSpT.Modular)
            {
                return ConstructionMethodBase.Modular;
            }
            else
            {
                return ConstructionMethodBase.Other;
            }
        }

        /// <summary>
        /// Creates a construction method enumeration object.
        /// </summary>
        /// <param name="constructionMethod">The construction method type.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<ConstructionMethodBase> ToConstructionMethodEnum(ConstructionMethodBase constructionMethod)
        {
            var constructionMethodEnum = new MISMOEnum<ConstructionMethodBase>();
            constructionMethodEnum.enumValue = constructionMethod;
            constructionMethodEnum.IsSensitive = false;

            return constructionMethodEnum;
        }

        /// <summary>
        /// Creates a construction method enumeration object based on the given property type.
        /// </summary>
        /// <param name="constructionMethod">The construction method type.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ConstructionMethodBase}" /> object.</returns>
        public static MISMOEnum<ConstructionMethodBase> ToConstructionMethodEnum(ConstructionMethod constructionMethod, bool displaySensitive = true)
        {
            if (constructionMethod == ConstructionMethod.Blank)
            {
                return null;
            }

            var constructionMethodEnum = new MISMOEnum<ConstructionMethodBase>();
            constructionMethodEnum.enumValue = GetConstructionMethodBaseValue(constructionMethod);
            constructionMethodEnum.IsSensitive = false;
            constructionMethodEnum.DisplaySensitive = displaySensitive;

            return constructionMethodEnum;
        }

        /// <summary>
        /// Gets the construction method value corresponding to the given property type.
        /// </summary>
        /// <param name="constructionMethod">The construction method type.</param>
        /// <returns>The corresponding construction method value.</returns>
        public static ConstructionMethodBase GetConstructionMethodBaseValue(ConstructionMethod constructionMethod)
        {
            switch (constructionMethod)
            {
                case ConstructionMethod.Manufactured:
                    return ConstructionMethodBase.Manufactured;
                case ConstructionMethod.MobileHome:
                    return ConstructionMethodBase.MobileHome;
                case ConstructionMethod.Modular:
                    return ConstructionMethodBase.Modular;
                case ConstructionMethod.OnFrameModular:
                    return ConstructionMethodBase.OnFrameModular;
                case ConstructionMethod.SiteBuilt:
                    return ConstructionMethodBase.SiteBuilt;
                case ConstructionMethod.Other:
                    return ConstructionMethodBase.Other;
                default:
                    throw new UnhandledEnumException(constructionMethod);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{ConstructionStatusBase}" /> object.
        /// </summary>
        /// <param name="buildingStatus">The building status type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ConstructionStatusBase}" /> object.</returns>
        public static MISMOEnum<ConstructionStatusBase> ToConstructionStatusEnum(E_sBuildingStatusT buildingStatus)
        {
            if (buildingStatus == E_sBuildingStatusT.LeaveBlank)
            {
                return null;
            }

            var constructionStatusEnum = new MISMOEnum<ConstructionStatusBase>();
            constructionStatusEnum.enumValue = GetConstructionStatusBaseValue(buildingStatus);
            constructionStatusEnum.IsSensitive = false;

            return constructionStatusEnum;
        }

        /// <summary>
        /// Retrieves the ConstructionStatus value corresponding to the given LQB value.
        /// </summary>
        /// <param name="buildingStatus">The building status type.</param>
        /// <returns>The corresponding ConstructionStatusBase value.</returns>
        public static ConstructionStatusBase GetConstructionStatusBaseValue(E_sBuildingStatusT buildingStatus)
        {
            switch (buildingStatus)
            {
                case E_sBuildingStatusT.Existing:
                    return ConstructionStatusBase.Existing;
                case E_sBuildingStatusT.Proposed:
                    return ConstructionStatusBase.Proposed;
                case E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab:
                    return ConstructionStatusBase.SubjectToAlterationImprovementRepairAndRehabilitation;
                case E_sBuildingStatusT.SubstantiallyRehabilitated:
                    return ConstructionStatusBase.SubstantiallyRehabilitated;
                case E_sBuildingStatusT.UnderConstruction:
                    return ConstructionStatusBase.UnderConstruction;
                default:
                    throw new UnhandledEnumException(buildingStatus);
            }
        }

        /// <summary>
        /// Creates a serializable enum containing a construction phase interest payment type.
        /// </summary>
        /// <param name="paymentTiming">The interest payment timing during the construction phase.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<ConstructionPhaseInterestPaymentBase> ToConstructionPhaseInterestPaymentEnum(ConstructionPhaseIntPaymentTiming paymentTiming)
        {
            if (paymentTiming == ConstructionPhaseIntPaymentTiming.Blank)
            {
                return null;
            }

            var interestPaymentEnum = new MISMOEnum<ConstructionPhaseInterestPaymentBase>();
            interestPaymentEnum.enumValue = GetConstructionPhaseInterestPaymentBaseValue(paymentTiming);
            interestPaymentEnum.IsSensitive = false;
            return interestPaymentEnum;
        }

        /// <summary>
        /// Maps a construction phase interest payment type to a MISMO value.
        /// </summary>
        /// <param name="paymentTiming">The interest payment timing during the construction phase.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static ConstructionPhaseInterestPaymentBase GetConstructionPhaseInterestPaymentBaseValue(ConstructionPhaseIntPaymentTiming paymentTiming)
        {
            switch (paymentTiming)
            {
                case ConstructionPhaseIntPaymentTiming.InterestPaidAtEndOfConstruction:
                    return ConstructionPhaseInterestPaymentBase.InterestPaidAtEndOfConstruction;
                case ConstructionPhaseIntPaymentTiming.InterestPaidPeriodically:
                    return ConstructionPhaseInterestPaymentBase.InterestPaidPeriodically;
                case ConstructionPhaseIntPaymentTiming.Other:
                    return ConstructionPhaseInterestPaymentBase.Other;
                default:
                    throw new UnhandledEnumException(paymentTiming);
            }
        }

        /// <summary>
        /// Creates a serializable enum containing a construction phase interest payment frequency type.
        /// </summary>
        /// <param name="paymentFrequency">The interest payment frequency during the construction phase.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<ConstructionPhaseInterestPaymentFrequencyBase> ToConstructionPhaseInterestPaymentFrequencyEnum(ConstructionPhaseIntPaymentFrequency paymentFrequency)
        {
            if (paymentFrequency == ConstructionPhaseIntPaymentFrequency.Blank)
            {
                return null;
            }

            var paymentFrequencyEnum = new MISMOEnum<ConstructionPhaseInterestPaymentFrequencyBase>();
            paymentFrequencyEnum.enumValue = GetConstructionPhaseInterestPaymentFrequencyBaseValue(paymentFrequency);
            paymentFrequencyEnum.IsSensitive = false;
            return paymentFrequencyEnum;
        }

        /// <summary>
        /// Maps a construction phase interest payment frequency enum to a MISMO value.
        /// </summary>
        /// <param name="paymentFrequency">The interest payment frequency during the construction phase.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static ConstructionPhaseInterestPaymentFrequencyBase GetConstructionPhaseInterestPaymentFrequencyBaseValue(ConstructionPhaseIntPaymentFrequency paymentFrequency)
        {
            switch (paymentFrequency)
            {
                case ConstructionPhaseIntPaymentFrequency.Biweekly:
                    return ConstructionPhaseInterestPaymentFrequencyBase.Biweekly;
                case ConstructionPhaseIntPaymentFrequency.Monthly:
                    return ConstructionPhaseInterestPaymentFrequencyBase.Monthly;
                case ConstructionPhaseIntPaymentFrequency.Other:
                    return ConstructionPhaseInterestPaymentFrequencyBase.Other;
                default:
                    throw new UnhandledEnumException(paymentFrequency);
            }
        }

        /// <summary>
        /// Creates a serializable enum containing a construction-to-permanent closing type.
        /// </summary>
        /// <param name="closingType">The closing type for a construction-to-permanent loan.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<ConstructionToPermanentClosingBase> ToConstructionToPermanentClosingEnum(ConstructionToPermanentClosingType closingType)
        {
            if (closingType == ConstructionToPermanentClosingType.Blank)
            {
                return null;
            }

            var closingTypeEnum = new MISMOEnum<ConstructionToPermanentClosingBase>();
            closingTypeEnum.enumValue = GetConstructionToPermanentClosingBaseValue(closingType);
            closingTypeEnum.IsSensitive = false;
            return closingTypeEnum;
        }

        /// <summary>
        /// Maps a construction-to-permanent closing type to a MISMO value.
        /// </summary>
        /// <param name="closingType">The closing type for a construction-to-permanent loan.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static ConstructionToPermanentClosingBase GetConstructionToPermanentClosingBaseValue(ConstructionToPermanentClosingType closingType)
        {
            switch (closingType)
            {
                case ConstructionToPermanentClosingType.OneClosing:
                    return ConstructionToPermanentClosingBase.OneClosing;
                case ConstructionToPermanentClosingType.TwoClosing:
                    return ConstructionToPermanentClosingBase.TwoClosing;
                case ConstructionToPermanentClosingType.Other:
                    return ConstructionToPermanentClosingBase.Other;
                default:
                    throw new UnhandledEnumException(closingType);
            }
        }

        /// <summary>
        /// Creates a MISMO enum containing a lot owner.
        /// </summary>
        /// <param name="lotOwner">The lot owner.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<LqbLotOwnerBase> ToLqbLotOwnerEnum(LotOwnerType lotOwner)
        {
            if (lotOwner == LotOwnerType.Blank)
            {
                return null;
            }

            var lotOwnerEnum = new MISMOEnum<LqbLotOwnerBase>();
            lotOwnerEnum.enumValue = GetLqbLotOwnerBaseValue(lotOwner);
            lotOwnerEnum.IsSensitive = false;

            return lotOwnerEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the lot owner.
        /// </summary>
        /// <param name="lotOwner">The lot owner.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbLotOwnerBase GetLqbLotOwnerBaseValue(LotOwnerType lotOwner)
        {
            switch (lotOwner)
            {
                case LotOwnerType.Borrower:
                    return LqbLotOwnerBase.Borrower;
                case LotOwnerType.Builder:
                    return LqbLotOwnerBase.Builder;
                case LotOwnerType.Other:
                    return LqbLotOwnerBase.Other;
                default:
                    throw new UnhandledEnumException(lotOwner);
            }
        }

        /// <summary>
        /// Creates a MISMO enum containing a construction disclosure type.
        /// </summary>
        /// <param name="disclosureType">The construction disclosure type.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<LqbConstructionDisclosureBase> ToLqbConstructionDisclosureEnum(ConstructionDisclosureType disclosureType)
        {
            if (disclosureType == ConstructionDisclosureType.Blank)
            {
                return null;
            }

            var disclosureTypeEnum = new MISMOEnum<LqbConstructionDisclosureBase>();
            disclosureTypeEnum.enumValue = GetLqbConstructionDisclosureBaseValue(disclosureType);
            disclosureTypeEnum.IsSensitive = false;

            return disclosureTypeEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the construction disclosure type.
        /// </summary>
        /// <param name="disclosureType">The construction disclosure type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbConstructionDisclosureBase GetLqbConstructionDisclosureBaseValue(ConstructionDisclosureType disclosureType)
        {
            switch (disclosureType)
            {
                case ConstructionDisclosureType.Combined:
                    return LqbConstructionDisclosureBase.Combined;
                case ConstructionDisclosureType.Separate:
                    return LqbConstructionDisclosureBase.Separate;
                default:
                    throw new UnhandledEnumException(disclosureType);
            }
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{ToMismo33ContactPointRoleBase}" /> object from given ContactPointRoleBase enumeration.
        /// </summary>
        /// <param name="contactPointRoleBase">The specified ContactPointRoleBase enumeration.</param>
        /// <returns><see cref="MISMOEnum{ToMismo33ContactPointRoleBase}" /> object with specified ContactPointRoleBase enumeration.</returns>
        public static MISMOEnum<ContactPointRoleBase> ToContactPointRoleEnum(ContactPointRoleBase contactPointRoleBase)
        {
            var contactPointRoleEnum = new MISMOEnum<ContactPointRoleBase>();
            contactPointRoleEnum.enumValue = contactPointRoleBase;
            contactPointRoleEnum.IsSensitive = true;
            return contactPointRoleEnum;
        }

        /// <summary>
        /// Creates a credit repository source enumeration object based on the given agency.
        /// </summary>
        /// <param name="agency">The credit reporting agency.</param>
        /// <returns>A serializable <see cref="MISMOEnum{CreditRepositorySourceBase}" /> object.</returns>
        public static MISMOEnum<CreditRepositorySourceBase> ToCreditRepositorySourceEnum(CreditRepositorySourceBase agency)
        {
            var creditRepositorySourceEnum = new MISMOEnum<CreditRepositorySourceBase>();
            creditRepositorySourceEnum.enumValue = agency;
            creditRepositorySourceEnum.IsSensitive = true;

            return creditRepositorySourceEnum;
        }

        /// <summary>
        /// Creates a container holding a credit score model.
        /// </summary>
        /// <param name="model">The applicant's credit model.</param>
        /// <returns>A serializable MISMO enum object.</returns>
        public static MISMOEnum<CreditScoreModelNameBase> ToCreditScoreModelNameEnum(E_CreditScoreModelT model)
        {
            if (model == E_CreditScoreModelT.LeaveBlank)
            {
                return null;
            }

            var creditScoreModelNameEnum = new MISMOEnum<CreditScoreModelNameBase>();
            creditScoreModelNameEnum.enumValue = GetCreditScoreModelNameBaseValue(model);
            creditScoreModelNameEnum.IsSensitive = true;

            return creditScoreModelNameEnum;
        }

        /// <summary>
        /// Gets the MISMO credit model value corresponding to the applicant's credit model.
        /// </summary>
        /// <param name="model">The applicant's credit model.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static CreditScoreModelNameBase GetCreditScoreModelNameBaseValue(E_CreditScoreModelT model)
        {
            switch (model)
            {
                case E_CreditScoreModelT.Beacon09MortgageIndustryOption:
                    return CreditScoreModelNameBase.Beacon09MortgageIndustryOption;
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02781:
                    return CreditScoreModelNameBase.EquifaxBankruptcyNavigatorIndex02781;
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02782:
                    return CreditScoreModelNameBase.EquifaxBankruptcyNavigatorIndex02782;
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02783:
                    return CreditScoreModelNameBase.EquifaxBankruptcyNavigatorIndex02783;
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02784:
                    return CreditScoreModelNameBase.EquifaxBankruptcyNavigatorIndex02784;
                case E_CreditScoreModelT.EquifaxBeacon:
                    return CreditScoreModelNameBase.EquifaxBeacon;
                case E_CreditScoreModelT.EquifaxBeacon5:
                    return CreditScoreModelNameBase.EquifaxBeacon50;
                case E_CreditScoreModelT.EquifaxBeacon5Auto:
                    return CreditScoreModelNameBase.EquifaxBeacon50Auto;
                case E_CreditScoreModelT.EquifaxBeacon5BankCard:
                    return CreditScoreModelNameBase.EquifaxBeacon50BankCard;
                case E_CreditScoreModelT.EquifaxBeacon5Installment:
                    return CreditScoreModelNameBase.EquifaxBeacon50Installment;
                case E_CreditScoreModelT.EquifaxBeacon5PersonalFinance:
                    return CreditScoreModelNameBase.EquifaxBeacon50PersonalFinance;
                case E_CreditScoreModelT.EquifaxBeaconAuto:
                    return CreditScoreModelNameBase.EquifaxBeaconAuto;
                case E_CreditScoreModelT.EquifaxBeaconBankcard:
                    return CreditScoreModelNameBase.EquifaxBeaconBankcard;
                case E_CreditScoreModelT.EquifaxBeaconInstallment:
                    return CreditScoreModelNameBase.EquifaxBeaconInstallment;
                case E_CreditScoreModelT.EquifaxBeaconPersonalFinance:
                    return CreditScoreModelNameBase.EquifaxBeaconPersonalFinance;
                case E_CreditScoreModelT.EquifaxDAS:
                    return CreditScoreModelNameBase.EquifaxDAS;
                case E_CreditScoreModelT.EquifaxEnhancedBeacon:
                    return CreditScoreModelNameBase.EquifaxEnhancedBeacon;
                case E_CreditScoreModelT.EquifaxEnhancedDAS:
                    return CreditScoreModelNameBase.EquifaxEnhancedDAS;
                case E_CreditScoreModelT.EquifaxMarketMax:
                    return CreditScoreModelNameBase.EquifaxMarketMax;
                case E_CreditScoreModelT.EquifaxMortgageScore:
                    return CreditScoreModelNameBase.EquifaxMortgageScore;
                case E_CreditScoreModelT.EquifaxPinnacle:
                    return CreditScoreModelNameBase.EquifaxPinnacle;
                case E_CreditScoreModelT.EquifaxPinnacle2:
                    return CreditScoreModelNameBase.EquifaxPinnacle20;
                case E_CreditScoreModelT.EquifaxVantageScore:
                    return CreditScoreModelNameBase.EquifaxVantageScore;
                case E_CreditScoreModelT.EquifaxVantageScore3:
                    return CreditScoreModelNameBase.EquifaxVantageScore30;
                case E_CreditScoreModelT.ExperianFairIsaac:
                    return CreditScoreModelNameBase.ExperianFairIsaac;
                case E_CreditScoreModelT.ExperianFairIsaacAdvanced:
                    return CreditScoreModelNameBase.ExperianFairIsaacAdvanced;
                case E_CreditScoreModelT.ExperianFairIsaacAdvanced2:
                    return CreditScoreModelNameBase.ExperianFairIsaacAdvanced20;
                case E_CreditScoreModelT.ExperianFairIsaacAuto:
                    return CreditScoreModelNameBase.ExperianFairIsaacAuto;
                case E_CreditScoreModelT.ExperianFairIsaacBankcard:
                    return CreditScoreModelNameBase.ExperianFairIsaacBankcard;
                case E_CreditScoreModelT.ExperianFairIsaacInstallment:
                    return CreditScoreModelNameBase.ExperianFairIsaacInstallment;
                case E_CreditScoreModelT.ExperianFairIsaacPersonalFinance:
                    return CreditScoreModelNameBase.ExperianFairIsaacPersonalFinance;
                case E_CreditScoreModelT.ExperianFICOClassicV3:
                    return CreditScoreModelNameBase.ExperianFICOClassicV3;
                case E_CreditScoreModelT.ExperianMDSBankruptcyII:
                    return CreditScoreModelNameBase.ExperianMDSBankruptcyII;
                case E_CreditScoreModelT.ExperianNewNationalEquivalency:
                    return CreditScoreModelNameBase.ExperianNewNationalEquivalency;
                case E_CreditScoreModelT.ExperianNewNationalRisk:
                    return CreditScoreModelNameBase.ExperianNewNationalRisk;
                case E_CreditScoreModelT.ExperianOldNationalRisk:
                    return CreditScoreModelNameBase.ExperianOldNationalRisk;
                case E_CreditScoreModelT.ExperianScorexPLUS:
                    return CreditScoreModelNameBase.ExperianScorexPLUS;
                case E_CreditScoreModelT.ExperianVantageScore:
                    return CreditScoreModelNameBase.ExperianVantageScore;
                case E_CreditScoreModelT.ExperianVantageScore3:
                    return CreditScoreModelNameBase.ExperianVantageScore30;
                case E_CreditScoreModelT.FICOExpansionScore:
                    return CreditScoreModelNameBase.FICOExpansionScore;
                case E_CreditScoreModelT.FICORiskScoreClassic04:
                    return CreditScoreModelNameBase.FICORiskScoreClassic04;
                case E_CreditScoreModelT.FICORiskScoreClassic98:
                    return CreditScoreModelNameBase.FICORiskScoreClassic98;
                case E_CreditScoreModelT.FICORiskScoreClassicAuto98:
                    return CreditScoreModelNameBase.FICORiskScoreClassicAuto98;
                case E_CreditScoreModelT.FICORiskScoreClassicBankcard98:
                    return CreditScoreModelNameBase.FICORiskScoreClassicBankcard98;
                case E_CreditScoreModelT.FICORiskScoreClassicInstallmentLoan98:
                    return CreditScoreModelNameBase.FICORiskScoreClassicInstallmentLoan98;
                case E_CreditScoreModelT.FICORiskScoreClassicPersonalFinance98:
                    return CreditScoreModelNameBase.FICORiskScoreClassicPersonalFinance98;
                case E_CreditScoreModelT.FICORiskScoreNextGen00:
                    return CreditScoreModelNameBase.FICORiskScoreNextGen00;
                case E_CreditScoreModelT.FICORiskScoreNextGen03:
                    return CreditScoreModelNameBase.FICORiskScoreNextGen03;
                case E_CreditScoreModelT.TransUnionDelphi:
                    return CreditScoreModelNameBase.TransUnionDelphi;
                case E_CreditScoreModelT.TransUnionEmpirica:
                    return CreditScoreModelNameBase.TransUnionEmpirica;
                case E_CreditScoreModelT.TransUnionEmpiricaAuto:
                    return CreditScoreModelNameBase.TransUnionEmpiricaAuto;
                case E_CreditScoreModelT.TransUnionEmpiricaBankcard:
                    return CreditScoreModelNameBase.TransUnionEmpiricaBankcard;
                case E_CreditScoreModelT.TransUnionEmpiricaInstallment:
                    return CreditScoreModelNameBase.TransUnionEmpiricaInstallment;
                case E_CreditScoreModelT.TransUnionEmpiricaPersonalFinance:
                    return CreditScoreModelNameBase.TransUnionEmpiricaPersonalFinance;
                case E_CreditScoreModelT.TransUnionNewDelphi:
                    return CreditScoreModelNameBase.TransUnionNewDelphi;
                case E_CreditScoreModelT.TransUnionPrecision:
                    return CreditScoreModelNameBase.TransUnionPrecision;
                case E_CreditScoreModelT.TransUnionPrecision03:
                    return CreditScoreModelNameBase.TransUnionPrecision03;
                case E_CreditScoreModelT.TransUnionVantageScore:
                    return CreditScoreModelNameBase.TransUnionVantageScore;
                case E_CreditScoreModelT.TransUnionVantageScore30:
                    return CreditScoreModelNameBase.TransUnionVantageScore30;
                case E_CreditScoreModelT.VantageScore2:
                case E_CreditScoreModelT.VantageScore3:
                case E_CreditScoreModelT.MoreThanOneCreditScoringModel:
                case E_CreditScoreModelT.Other:
                    return CreditScoreModelNameBase.Other;
                default:
                    throw new UnhandledEnumException(model);
            }
        }

        /// <summary>
        /// Creates a MISMO enum containing a credit repository source.
        /// </summary>
        /// <param name="creditSource">The credit bureau.</param>
        /// <returns>A MISMO enum.</returns>
        public static MISMOEnum<CreditRepositorySourceBase> ToCreditRepositorySourceEnum(E_aDecisionCreditSourceT creditSource)
        {
            var sourceEnum = new MISMOEnum<CreditRepositorySourceBase>();
            sourceEnum.enumValue = GetCreditRepositorySourceBaseValue(creditSource);
            sourceEnum.IsSensitive = true;

            return sourceEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the credit bureau.
        /// </summary>
        /// <param name="creditSource">The credit bureau.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static CreditRepositorySourceBase GetCreditRepositorySourceBaseValue(E_aDecisionCreditSourceT creditSource)
        {
            switch (creditSource)
            {
                case E_aDecisionCreditSourceT.Equifax:
                    return CreditRepositorySourceBase.Equifax;
                case E_aDecisionCreditSourceT.Experian:
                    return CreditRepositorySourceBase.Experian;
                case E_aDecisionCreditSourceT.TransUnion:
                    return CreditRepositorySourceBase.TransUnion;
                case E_aDecisionCreditSourceT.Other:
                    return CreditRepositorySourceBase.Other;
                default:
                    throw new UnhandledEnumException(creditSource);
            }
        }

        /// <summary>
        /// Creates a creditor servicing of loan enumeration object based on the given indicator.
        /// </summary>
        /// <param name="lenderMayTransferLoan">Indicates whether it is possible that the lender will transfer some ownership of the loan.</param>
        /// <param name="lenderIntendsToServiceLoan">Indicates whether the creditor intends to service the loan.</param>
        /// <returns>A serializable <see cref="MISMOEnum{CreditorServicingOfLoanStatementBase}" /> object.</returns>
        public static MISMOEnum<CreditorServicingOfLoanStatementBase> ToCreditorServicingOfLoanStatementEnum(bool lenderMayTransferLoan, bool lenderIntendsToServiceLoan)
        {
            var creditorServicingOfLoanStatementEnum = new MISMOEnum<CreditorServicingOfLoanStatementBase>();

            if (lenderMayTransferLoan)
            {
                creditorServicingOfLoanStatementEnum.enumValue = CreditorServicingOfLoanStatementBase.CreditorMayAssignSellOrTransferServicingOfLoan;
            }
            else
            {
                creditorServicingOfLoanStatementEnum.enumValue = 
                    lenderIntendsToServiceLoan
                    ? CreditorServicingOfLoanStatementBase.CreditorIntendsToServiceLoan
                    : CreditorServicingOfLoanStatementBase.CreditorIntendsToTransferServicingOfLoan;
            }

            creditorServicingOfLoanStatementEnum.IsSensitive = false;

            return creditorServicingOfLoanStatementEnum;
        }

        /// <summary>
        /// Creates an object indicating the type of document.
        /// </summary>
        /// <param name="baseValue">The document base value for the object.</param>
        /// <returns>A serializable <see cref="MISMOEnum{DocumentBase}" /> object.</returns>
        public static MISMOEnum<DocumentBase> ToDocumentEnum(DocumentBase baseValue)
        {
            var documentEnum = new MISMOEnum<DocumentBase>();
            documentEnum.enumValue = baseValue;
            documentEnum.IsSensitive = false;

            return documentEnum;
        }

        /// <summary>
        /// Creates an object indicating the type of a fee.
        /// </summary>
        /// <param name="type">The type of the fee.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <returns>A serializable <see cref="MISMOEnum{FeeBase}" /> object.</returns>
        public static MISMOEnum<FeeBase> ToFeeEnum(E_ClosingCostFeeMismoFeeT type, string displayLabelText)
        {
            var feeEnum = new MISMOEnum<FeeBase>();
            feeEnum.enumValue = GetFeeBaseValue(type);
            feeEnum.IsSensitive = false;
            feeEnum.DisplayLabelText = displayLabelText;

            return feeEnum;
        }

        /// <summary>
        /// Retrieves the MISMO fee type corresponding to the closing cost type.
        /// </summary>
        /// <param name="type">The type of the closing cost fee.</param>
        /// <returns>The corresponding <see cref="FeeBase" /> type.</returns>
        public static FeeBase GetFeeBaseValue(E_ClosingCostFeeMismoFeeT type)
        {
            switch (type)
            {
                case E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee:
                    return FeeBase.Item203KArchitecturalAndEngineeringFee;
                case E_ClosingCostFeeMismoFeeT._203KConsultantFee:
                    return FeeBase.Item203KConsultantFee;
                case E_ClosingCostFeeMismoFeeT._203KDiscountOnRepairs:
                    return FeeBase.Item203KDiscountOnRepairs;
                case E_ClosingCostFeeMismoFeeT._203KInspectionFee:
                    return FeeBase.Item203KInspectionFee;
                case E_ClosingCostFeeMismoFeeT._203KPermits:
                    return FeeBase.Item203KPermits;
                case E_ClosingCostFeeMismoFeeT._203KSupplementalOriginationFee:
                    return FeeBase.Item203KSupplementalOriginationFee;
                case E_ClosingCostFeeMismoFeeT._203KTitleUpdate:
                    return FeeBase.Item203KTitleUpdate;
                case E_ClosingCostFeeMismoFeeT.AmortizationFee:
                    return FeeBase.AmortizationFee;
                case E_ClosingCostFeeMismoFeeT.ApplicationFee:
                    return FeeBase.ApplicationFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalDeskReviewFee:
                    return FeeBase.AppraisalDeskReviewFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFee:
                    return FeeBase.AppraisalFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFieldReviewFee:
                    return FeeBase.AppraisalFieldReviewFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentFee:
                    return FeeBase.AssignmentFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee:
                    return FeeBase.RecordingFeeForAssignment;
                case E_ClosingCostFeeMismoFeeT.AssumptionFee:
                    return FeeBase.AssumptionFee;
                case E_ClosingCostFeeMismoFeeT.AttorneyFee:
                    return FeeBase.AttorneyFee;
                case E_ClosingCostFeeMismoFeeT.BankruptcyMonitoringFee:
                    return FeeBase.BankruptcyMonitoringFee;
                case E_ClosingCostFeeMismoFeeT.BondFee:
                    return FeeBase.BondFee;
                case E_ClosingCostFeeMismoFeeT.BondReviewFee:
                    return FeeBase.BondReviewFee;
                case E_ClosingCostFeeMismoFeeT.CertificationFee:
                    return FeeBase.CertificationFee;
                case E_ClosingCostFeeMismoFeeT.CityDeedTaxStampFee:
                    return FeeBase.TaxStampForCityDeed;
                case E_ClosingCostFeeMismoFeeT.CityMortgageTaxStampFee:
                    return FeeBase.TaxStampForCityMortgage;
                case E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee:
                    return FeeBase.TaxStampForCountyDeed;
                case E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee:
                    return FeeBase.TaxStampForCountyMortgage;
                case E_ClosingCostFeeMismoFeeT.CLOAccessFee:
                    return FeeBase.CLOAccessFee;
                case E_ClosingCostFeeMismoFeeT.ClosingProtectionLetterFee:
                    return FeeBase.TitleClosingProtectionLetterFee;
                case E_ClosingCostFeeMismoFeeT.CommitmentFee:
                    return FeeBase.CommitmentFee;
                case E_ClosingCostFeeMismoFeeT.CopyFaxFee:
                    return FeeBase.CopyOrFaxFee;
                case E_ClosingCostFeeMismoFeeT.CourierFee:
                    return FeeBase.CourierFee;
                case E_ClosingCostFeeMismoFeeT.CreditReportFee:
                    return FeeBase.CreditReportFee;
                case E_ClosingCostFeeMismoFeeT.DeedRecordingFee:
                    return FeeBase.RecordingFeeForDeed;
                case E_ClosingCostFeeMismoFeeT.DocumentaryStampFee:
                    return FeeBase.DocumentaryStampFee;
                case E_ClosingCostFeeMismoFeeT.DocumentPreparationFee:
                    return FeeBase.DocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.ElectronicDocumentDeliveryFee:
                    return FeeBase.ElectronicDocumentDeliveryFee;
                case E_ClosingCostFeeMismoFeeT.EscrowServiceFee:
                    return FeeBase.EscrowServiceFee;
                case E_ClosingCostFeeMismoFeeT.EscrowWaiverFee:
                    return FeeBase.EscrowWaiverFee;
                case E_ClosingCostFeeMismoFeeT.FloodCertification:
                    return FeeBase.FloodCertification;
                case E_ClosingCostFeeMismoFeeT.GeneralCounselFee:
                    return FeeBase.GeneralCounselFee;
                case E_ClosingCostFeeMismoFeeT.InspectionFee:
                    return FeeBase.HomeInspectionFee; 
                case E_ClosingCostFeeMismoFeeT.LoanDiscountPoints:
                    return FeeBase.LoanDiscountPoints;
                case E_ClosingCostFeeMismoFeeT.LoanOriginationFee:
                    return FeeBase.LoanOriginationFee;
                case E_ClosingCostFeeMismoFeeT.MERSRegistrationFee:
                    return FeeBase.MERSRegistrationFee;
                case E_ClosingCostFeeMismoFeeT.ModificationFee:
                    return FeeBase.ModificationFee;
                case E_ClosingCostFeeMismoFeeT.MortgageBrokerFee:
                    return FeeBase.MortgageBrokerFee;
                case E_ClosingCostFeeMismoFeeT.MortgageRecordingFee:
                    return FeeBase.RecordingFeeForMortgage;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateFee:
                    return FeeBase.MunicipalLienCertificateFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee:
                    return FeeBase.RecordingFeeForMunicipalLienCertificate;
                case E_ClosingCostFeeMismoFeeT.NewLoanAdministrationFee:
                    return FeeBase.NewLoanAdministrationFee;
                case E_ClosingCostFeeMismoFeeT.NotaryFee:
                    return FeeBase.NotaryFee;
                case E_ClosingCostFeeMismoFeeT.PayoffRequestFee:
                    return FeeBase.PayoffRequestFee;
                case E_ClosingCostFeeMismoFeeT.PestInspectionFee:
                    return FeeBase.PestInspectionFee;
                case E_ClosingCostFeeMismoFeeT.ProcessingFee:
                    return FeeBase.ProcessingFee;
                case E_ClosingCostFeeMismoFeeT.RealEstateCommission:
                    return FeeBase.RealEstateCommissionBuyersBroker;
                case E_ClosingCostFeeMismoFeeT.RedrawFee:
                    return FeeBase.RedrawFee;
                case E_ClosingCostFeeMismoFeeT.ReinspectionFee:
                    return FeeBase.ReinspectionFee;
                case E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee:
                    return FeeBase.RecordingFeeForRelease;
                case E_ClosingCostFeeMismoFeeT.SettlementOrClosingFee:
                    return FeeBase.SettlementFee;
                case E_ClosingCostFeeMismoFeeT.SigningAgentFee:
                    return FeeBase.SigningAgentFee;
                case E_ClosingCostFeeMismoFeeT.StateDeedTaxStampFee:
                    return FeeBase.TaxStampForStateDeed;
                case E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee:
                    return FeeBase.TaxStampForStateMortgage;
                case E_ClosingCostFeeMismoFeeT.SubordinationFee:
                    return FeeBase.SubordinationFee;
                case E_ClosingCostFeeMismoFeeT.SurveyFee:
                    return FeeBase.SurveyFee;
                case E_ClosingCostFeeMismoFeeT.TaxRelatedServiceFee:
                    return FeeBase.TaxRelatedServiceFee;
                case E_ClosingCostFeeMismoFeeT.TitleEndorsementFee:
                    return FeeBase.TitleEndorsementFee;
                case E_ClosingCostFeeMismoFeeT.TitleExaminationFee:
                    return FeeBase.TitleExaminationFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceBinderFee:
                    return FeeBase.TitleInsuranceBinderFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceFee:
                    return FeeBase.TitleInsuranceFee;
                case E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium:
                    return FeeBase.TitleLendersCoveragePremium;
                case E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium:
                    return FeeBase.TitleOwnersCoveragePremium;
                case E_ClosingCostFeeMismoFeeT.UnderwritingFee:
                    return FeeBase.UnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.WireTransferFee:
                    return FeeBase.WireTransferFee;
                case E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation:
                    return FeeBase.LoanOriginatorCompensation;
                case E_ClosingCostFeeMismoFeeT.RuralHousingFee:
                    return FeeBase.USDARuralDevelopmentGuaranteeFee;
                //// OPM 227381 - MISMO 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.AppraisalManagementCompanyFee:
                    return FeeBase.AppraisalManagementCompanyFee;
                case E_ClosingCostFeeMismoFeeT.AsbestosInspectionFee:
                    return FeeBase.AsbestosInspectionFee;
                case E_ClosingCostFeeMismoFeeT.AutomatedUnderwritingFee:
                    return FeeBase.AutomatedUnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.AVMFee:
                    return FeeBase.AVMFee;
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationDues:
                    return FeeBase.CondominiumAssociationDues;
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationSpecialAssessment:
                    return FeeBase.CondominiumAssociationSpecialAssessment;
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationDues:
                    return FeeBase.CooperativeAssociationDues;
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationSpecialAssessment:
                    return FeeBase.CooperativeAssociationSpecialAssessment;
                case E_ClosingCostFeeMismoFeeT.CreditDisabilityInsurancePremium:
                    return FeeBase.CreditDisabilityInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.CreditLifeInsurancePremium:
                    return FeeBase.CreditLifeInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.CreditPropertyInsurancePremium:
                    return FeeBase.CreditPropertyInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.CreditUnemploymentInsurancePremium:
                    return FeeBase.CreditUnemploymentInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.DebtCancellationInsurancePremium:
                    return FeeBase.DebtCancellationInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.DeedPreparationFee:
                    return FeeBase.DeedPreparationFee;
                case E_ClosingCostFeeMismoFeeT.DisasterInspectionFee:
                    return FeeBase.DisasterInspectionFee;
                case E_ClosingCostFeeMismoFeeT.DryWallInspectionFee:
                    return FeeBase.DryWallInspectionFee;
                case E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee:
                    return FeeBase.ElectricalInspectionFee;
                case E_ClosingCostFeeMismoFeeT.EnvironmentalInspectionFee:
                    return FeeBase.EnvironmentalInspectionFee;
                case E_ClosingCostFeeMismoFeeT.FilingFee:
                    return FeeBase.FilingFee;
                case E_ClosingCostFeeMismoFeeT.FoundationInspectionFee:
                    return FeeBase.FoundationInspectionFee;
                case E_ClosingCostFeeMismoFeeT.HeatingCoolingInspectionFee:
                    return FeeBase.HeatingCoolingInspectionFee;
                case E_ClosingCostFeeMismoFeeT.HighCostMortgageCounselingFee:
                    return FeeBase.HighCostMortgageCounselingFee;
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationDues:
                    return FeeBase.HomeownersAssociationDues;
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationSpecialAssessment:
                    return FeeBase.HomeownersAssociationSpecialAssessment;
                case E_ClosingCostFeeMismoFeeT.HomeWarrantyFee:
                    return FeeBase.HomeWarrantyFee;
                case E_ClosingCostFeeMismoFeeT.LeadInspectionFee:
                    return FeeBase.LeadInspectionFee;
                case E_ClosingCostFeeMismoFeeT.LendersAttorneyFee:
                    return FeeBase.LendersAttorneyFee;
                case E_ClosingCostFeeMismoFeeT.LoanLevelPriceAdjustment:
                    return FeeBase.LoanLevelPriceAdjustment;
                case E_ClosingCostFeeMismoFeeT.ManualUnderwritingFee:
                    return FeeBase.ManualUnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.ManufacturedHousingInspectionFee:
                    return FeeBase.ManufacturedHousingInspectionFee;
                case E_ClosingCostFeeMismoFeeT.MIUpfrontPremium:
                    return FeeBase.MIUpfrontPremium;
                case E_ClosingCostFeeMismoFeeT.MoldInspectionFee:
                    return FeeBase.MoldInspectionFee;
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeCountyOrParish:
                    return FeeBase.MortgageSurchargeCountyOrParish;
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeMunicipal:
                    return FeeBase.MortgageSurchargeMunicipal;
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeState:
                    return FeeBase.MortgageSurchargeState;
                case E_ClosingCostFeeMismoFeeT.PlumbingInspectionFee:
                    return FeeBase.PlumbingInspectionFee;
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyPreparationFee:
                    return FeeBase.PowerOfAttorneyPreparationFee;
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyRecordingFee:
                    return FeeBase.PowerOfAttorneyRecordingFee;
                case E_ClosingCostFeeMismoFeeT.PreclosingVerificationControlFee:
                    return FeeBase.PreclosingVerificationControlFee;
                case E_ClosingCostFeeMismoFeeT.PropertyInspectionWaiverFee:
                    return FeeBase.PropertyInspectionWaiverFee;
                case E_ClosingCostFeeMismoFeeT.PropertyTaxStatusResearchFee:
                    return FeeBase.PropertyTaxStatusResearchFee;
                case E_ClosingCostFeeMismoFeeT.RadonInspectionFee:
                    return FeeBase.RadonInspectionFee;
                case E_ClosingCostFeeMismoFeeT.RateLockFee:
                    return FeeBase.RateLockFee;
                case E_ClosingCostFeeMismoFeeT.ReconveyanceFee:
                    return FeeBase.ReconveyanceFee;
                case E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination:
                    return FeeBase.RecordingFeeForSubordination;
                case E_ClosingCostFeeMismoFeeT.RecordingFeeTotal:
                    return FeeBase.RecordingFeeTotal;
                case E_ClosingCostFeeMismoFeeT.RepairsFee:
                    return FeeBase.RepairsFee;
                case E_ClosingCostFeeMismoFeeT.RoofInspectionFee:
                    return FeeBase.RoofInspectionFee;
                case E_ClosingCostFeeMismoFeeT.SepticInspectionFee:
                    return FeeBase.SepticInspectionFee;
                case E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee:
                    return FeeBase.SmokeDetectorInspectionFee;
                case E_ClosingCostFeeMismoFeeT.StateTitleInsuranceFee:
                    return FeeBase.StateTitleInsuranceFee;
                case E_ClosingCostFeeMismoFeeT.StructuralInspectionFee:
                    return FeeBase.StructuralInspectionFee;
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownAdministrationFee:
                    return FeeBase.TemporaryBuydownAdministrationFee;
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownPoints:
                    return FeeBase.TemporaryBuydownPoints;
                case E_ClosingCostFeeMismoFeeT.TitleCertificationFee:
                    return FeeBase.TitleCertificationFee;
                case E_ClosingCostFeeMismoFeeT.TitleClosingFee:
                    return FeeBase.TitleClosingFee;
                case E_ClosingCostFeeMismoFeeT.TitleDocumentPreparationFee:
                    return FeeBase.TitleDocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee:
                    return FeeBase.TitleFinalPolicyShortFormFee;
                case E_ClosingCostFeeMismoFeeT.TitleNotaryFee:
                    return FeeBase.TitleNotaryFee;
                case E_ClosingCostFeeMismoFeeT.TitleServicesSalesTax:
                    return FeeBase.TitleServicesSalesTax;
                case E_ClosingCostFeeMismoFeeT.TitleUnderwritingIssueResolutionFee:
                    return FeeBase.TitleUnderwritingIssueResolutionFee;
                case E_ClosingCostFeeMismoFeeT.TransferTaxTotal:
                    return FeeBase.TransferTaxTotal;
                case E_ClosingCostFeeMismoFeeT.VerificationOfAssetsFee:
                    return FeeBase.VerificationOfAssetsFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfEmploymentFee:
                    return FeeBase.VerificationOfEmploymentFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfIncomeFee:
                    return FeeBase.VerificationOfIncomeFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfResidencyStatusFee:
                    return FeeBase.VerificationOfResidencyStatusFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxpayerIdentificationFee:
                    return FeeBase.VerificationOfTaxpayerIdentificationFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxReturnFee:
                    return FeeBase.VerificationOfTaxReturnFee;
                case E_ClosingCostFeeMismoFeeT.WaterTestingFee:
                    return FeeBase.WaterTestingFee;
                case E_ClosingCostFeeMismoFeeT.WellInspectionFee:
                    return FeeBase.WellInspectionFee;
                //// OPM 227381 - Both MISMO 2.6 and 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.VAFundingFee:
                    return FeeBase.VAFundingFee;
                case E_ClosingCostFeeMismoFeeT.MIInitialPremium:
                    return FeeBase.MIInitialPremium;
                case E_ClosingCostFeeMismoFeeT.ChosenInterestRateCreditOrChargeTotal:
                    return FeeBase.ChosenInterestRateCreditOrChargeTotal;
                case E_ClosingCostFeeMismoFeeT.OurOriginationChargeTotal:
                    return FeeBase.OurOriginationChargeTotal;
                case E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal:
                    return FeeBase.TitleServicesFeeTotal;
                case E_ClosingCostFeeMismoFeeT.RealEstateCommissionSellersBroker:
                    return FeeBase.RealEstateCommissionSellersBroker;
                case E_ClosingCostFeeMismoFeeT.AbstractOrTitleSearchFee:
                case E_ClosingCostFeeMismoFeeT.Other:
                case E_ClosingCostFeeMismoFeeT.Undefined:
                    return FeeBase.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Creates an object specifying the entity who will receive payment for a service.
        /// </summary>
        /// <param name="agent">The entity who will receive payment for a service.</param>
        /// <param name="affiliate">True if the entity is an affiliate of either the lender or the originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the entity is a third party. Otherwise false.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FeePaidToBase}" /> object.</returns>
        public static MISMOEnum<FeePaidToBase> ToFeePaidToEnum(CAgentFields agent, bool affiliate, bool thirdParty)
        {
            if (!agent.IsValid || agent.RecordId == Guid.Empty || agent.IsNewRecord)
            {
                return null;
            }

            return ToFeePaidToEnum(GetFeePaidToBaseValue(agent.AgentRoleT, agent.IsLender, agent.IsOriginator, affiliate, thirdParty));
        }

        /// <summary>
        /// Creates an object specifying the entity who will receive payment for a service.
        /// For use when only the agent type is available, such as when a fee object has a beneficiary type but not an agent record.
        /// </summary>
        /// <param name="agentType">The type of entity who will receive payment for a service.</param>
        /// <param name="affiliate">True if the entity is an affiliate of either the lender or the originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the entity to which the fee is paid is a third party. Otherwise false.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FeePaidToBase}" /> object.</returns>
        public static MISMOEnum<FeePaidToBase> ToFeePaidToEnum(E_AgentRoleT agentType, bool affiliate, bool thirdParty)
        {
            //// Assumes false for parameters that would be gathered from the agent record, if there were an associated agent.
            return ToFeePaidToEnum(GetFeePaidToBaseValue(agentType, false, false, affiliate, thirdParty));
        }

        /// <summary>
        /// Creates an object specifying the entity who will receive payment for a service.
        /// </summary>
        /// <param name="paidTo">The type of entity who will receive payment for a service.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FeePaidToBase}" /> object.</returns>
        public static MISMOEnum<FeePaidToBase> ToFeePaidToEnum(FeePaidToBase paidTo)
        {
            var feePaidToEnum = new MISMOEnum<FeePaidToBase>();
            feePaidToEnum.enumValue = paidTo;
            feePaidToEnum.IsSensitive = false;

            return feePaidToEnum;
        }

        /// <summary>
        /// Retrieves the enumerated value corresponding to the entity who will receive payment for a service.
        /// </summary>
        /// <param name="agentType">The type of entity who will receive payment for a service.</param>
        /// <param name="lender">True if the entity is the lender. Otherwise false.</param>
        /// <param name="originator">True if the entity is the originator. Otherwise false.</param>
        /// <param name="affiliate">True if the entity is an affiliate of either the lender or originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the agent is a third party. Otherwise false.</param>
        /// <returns>The corresponding value.</returns>
        public static FeePaidToBase GetFeePaidToBaseValue(E_AgentRoleT agentType, bool lender, bool originator, bool affiliate, bool thirdParty)
        {
            if (agentType == E_AgentRoleT.Broker)
            {
                return FeePaidToBase.Broker;
            }
            else if (agentType == E_AgentRoleT.Investor)
            {
                return FeePaidToBase.Investor;
            }
            else if (agentType == E_AgentRoleT.Lender)
            {
                return FeePaidToBase.Lender;
            }
            else if (lender)
            {
                return FeePaidToBase.Lender;
            }
            else if (originator)
            {
                return FeePaidToBase.Broker;
            }
            else if (affiliate)
            {
                return FeePaidToBase.AffiliateProvider;
            }
            else if (thirdParty)
            {
                return FeePaidToBase.ThirdPartyProvider;
            }
            else
            {
                return FeePaidToBase.Other;
            }
        }

        /// <summary>
        /// Creates an object indicating who paid the fee payment.
        /// </summary>
        /// <param name="paidBy">The entity who paid the fee payment.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FeePaymentPaidByBase}" /> object.</returns>
        public static MISMOEnum<FeePaymentPaidByBase> ToFeePaymentPaidByEnum(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            if (paidBy == E_ClosingCostFeePaymentPaidByT.LeaveBlank)
            {
                return null;
            }

            var feePaymentPaidByEnum = new MISMOEnum<FeePaymentPaidByBase>();
            feePaymentPaidByEnum.enumValue = GetFeePaymentPaidByBaseValue(paidBy);
            feePaymentPaidByEnum.IsSensitive = false;

            return feePaymentPaidByEnum;
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid the fee payment.
        /// </summary>
        /// <param name="paidBy">The entity who paid the fee payment.</param>
        /// <returns>The corresponding <see cref="FeePaymentPaidByBase" /> value.</returns>
        public static FeePaymentPaidByBase GetFeePaymentPaidByBaseValue(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return FeePaymentPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return FeePaymentPaidByBase.Broker;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return FeePaymentPaidByBase.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return FeePaymentPaidByBase.Seller;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return FeePaymentPaidByBase.ThirdParty;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{FeePaymentResponsiblePartyBase}" /> object with the given responsible party type.
        /// </summary>
        /// <param name="responsible">The type of party responsible for paying the fee.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A <see cref="MISMOEnum{FeePaymentResponsiblePartyBase}" /> object with the given responsible party type.</returns>
        public static MISMOEnum<FeePaymentResponsiblePartyBase> ToFeePaymentResponsiblePartyEnum(FeePaymentResponsiblePartyBase responsible, bool isSensitiveData = false)
        {
            if (responsible == FeePaymentResponsiblePartyBase.Blank)
            {
                return null;
            }

            var responsiblePartyEnum = new MISMOEnum<FeePaymentResponsiblePartyBase>();
            responsiblePartyEnum.enumValue = responsible;
            responsiblePartyEnum.IsSensitive = isSensitiveData;

            return responsiblePartyEnum;
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{FeePaymentResponsiblePartyBase}" /> object with the given responsible party type.
        /// </summary>
        /// <param name="responsible">The type of party responsible for paying the fee.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A <see cref="MISMOEnum{FeePaymentResponsiblePartyBase}" /> object with the given responsible party type.</returns>
        public static MISMOEnum<FeePaymentResponsiblePartyBase> ToFeePaymentResponsiblePartyEnum(E_GfeResponsiblePartyT responsible, bool isSensitiveData = false)
        {
            return ToFeePaymentResponsiblePartyEnum(GetFeePaymentResponsiblePartyBaseValue(responsible), isSensitiveData);
        }

        /// <summary>
        /// Converts the given responsible party type to the corresponding FeePaymentResponsiblePartyBase.
        /// </summary>
        /// <param name="responsible">The type of party responsible for paying the fee.</param>
        /// <returns>The FeePaymentResponsiblePartyBase corresponding to the given responsible party type.</returns>
        public static FeePaymentResponsiblePartyBase GetFeePaymentResponsiblePartyBaseValue(E_GfeResponsiblePartyT responsible)
        {
            switch (responsible)
            {
                case E_GfeResponsiblePartyT.Buyer:
                    return FeePaymentResponsiblePartyBase.Buyer;
                case E_GfeResponsiblePartyT.LeaveBlank:
                    return FeePaymentResponsiblePartyBase.Blank;
                case E_GfeResponsiblePartyT.Seller:
                    return FeePaymentResponsiblePartyBase.Seller;
                case E_GfeResponsiblePartyT.Lender:
                    return FeePaymentResponsiblePartyBase.Lender;
                case E_GfeResponsiblePartyT.Broker:
                    return FeePaymentResponsiblePartyBase.Broker;
                default:
                    throw new UnhandledEnumException(responsible);
            }
        }

        /// <summary>
        /// Creates an object indicating the fee percent basis type.
        /// </summary>
        /// <param name="percentBasis">The percent base type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FeePercentBasisBase}" /> object.</returns>
        public static MISMOEnum<FeePercentBasisBase> ToFeePercentBasisEnum(E_PercentBaseT percentBasis)
        {
            var feePercentBasisEnum = new MISMOEnum<FeePercentBasisBase>();
            feePercentBasisEnum.enumValue = GetFeePercentBasisBaseValue(percentBasis);
            feePercentBasisEnum.IsSensitive = false;

            return feePercentBasisEnum;
        }

        /// <summary>
        /// Retrieves the fee percent basis type corresponding to the given percent base.
        /// </summary>
        /// <param name="percentBasis">The percent base type.</param>
        /// <returns>The corresponding <see cref="FeePercentBasisBase" /> value.</returns>
        public static FeePercentBasisBase GetFeePercentBasisBaseValue(E_PercentBaseT percentBasis)
        {
            switch (percentBasis)
            {
                case E_PercentBaseT.AppraisalValue:
                    return FeePercentBasisBase.PropertyAppraisedValueAmount;
                case E_PercentBaseT.LoanAmount:
                    return FeePercentBasisBase.BaseLoanAmount;
                case E_PercentBaseT.TotalLoanAmount:
                    return FeePercentBasisBase.OriginalLoanAmount;
                case E_PercentBaseT.SalesPrice:
                    return FeePercentBasisBase.PurchasePriceAmount;
                case E_PercentBaseT.OriginalCost:
                case E_PercentBaseT.AverageOutstandingBalance:
                case E_PercentBaseT.DecliningRenewalsAnnually:
                case E_PercentBaseT.DecliningRenewalsMonthly:
                case E_PercentBaseT.AllYSP:
                    return FeePercentBasisBase.Other;
                default:
                    throw new UnhandledEnumException(percentBasis);
            }
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{FundsBase}" /> object from the given LQB value and sensitivity indicator.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to MISMO.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FundsBase}" /> object.</returns>
        public static MISMOEnum<FundsBase> ToFundsEnum(string lqbValue)
        {
            return ToFundsEnum(GetFundsBaseValue(lqbValue));
        }

        /// <summary>
        /// Creates a fundsEnum object from the given funds indicator.
        /// </summary>
        /// <param name="funds">The funds type indicator.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FundsBase}" /> object.</returns>
        public static MISMOEnum<FundsBase> ToFundsEnum(FundsBase funds)
        {
            if (funds == FundsBase.Blank)
            {
                return null;
            }

            var fundsEnum = new MISMOEnum<FundsBase>();
            fundsEnum.enumValue = funds;
            fundsEnum.IsSensitive = false;

            return fundsEnum;
        }

        /// <summary>
        /// Converts an LQB string into the appropriate FundsBase enumerated value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to a FundsBase enumeration.</param>
        /// <returns>A FundsBase enumerated value.</returns>
        public static FundsBase GetFundsBaseValue(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return FundsBase.Blank;
            }
            else if (string.Equals(lqbValue, "Checking/Savings", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.CheckingSavings;
            }
            else if (string.Equals(lqbValue, "Deposit on Sales Contract", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.DepositOnSalesContract;
            }
            else if (string.Equals(lqbValue, "Equity on Sold Property", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.EquityOnSoldProperty;
            }
            else if (string.Equals(lqbValue, "Equity from Pending Sale", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.EquityOnPendingSale;
            }
            else if (string.Equals(lqbValue, "Equity from Subject Property", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.EquityOnSubjectProperty;
            }
            else if (string.Equals(lqbValue, "Stocks & Bonds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.StocksAndBonds;
            }
            else if (string.Equals(lqbValue, "Lot Equity", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.LotEquity;
            }
            else if (string.Equals(lqbValue, "Bridge Loan", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.BridgeLoan;
            }
            else if (string.Equals(lqbValue, "Unsecured Borrowed Funds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.UnsecuredBorrowedFunds;
            }
            else if (string.Equals(lqbValue, "Trust Funds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.TrustFunds;
            }
            else if (string.Equals(lqbValue, "Retirement Funds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.RetirementFunds;
            }
            else if (string.Equals(lqbValue, "Rent with Option to Purchase", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.RentWithOptionToPurchase;
            }
            else if (string.Equals(lqbValue, "Life Insurance Cash Value", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.LifeInsuranceCashValue;
            }
            else if (string.Equals(lqbValue, "Sale of Chattel", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.SaleOfChattel;
            }
            else if (string.Equals(lqbValue, "Trade Equity", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.TradeEquity;
            }
            else if (string.Equals(lqbValue, "Sweat Equity", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.SweatEquity;
            }
            else if (string.Equals(lqbValue, "Cash on Hand", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.CashOnHand;
            }
            else if (string.Equals(lqbValue, "Secured Borrowed Funds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.SecuredLoan;
            }
            else if (string.Equals(lqbValue, "Forgivable Secured Loan", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.ForgivableSecuredLoan;
            }
            else if (string.Equals(lqbValue, "Gift Funds", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source N/A", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Relative", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Government Assistance", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Employer", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Nonprofit/Religious/Community - Seller Funded", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Nonprofit/Religious/Community - Non-Seller Funded", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.GiftFunds;
            }
            else
            {
                // For any custom option or "Other Type of Down Payment"
                return FundsBase.Other;
            }
        }

        /// <summary>
        /// Creates a serializable IdentityDocumentEnum object based on the given identification type.
        /// </summary>
        /// <param name="idType">The identification type.</param>
        /// <param name="idTypeOtherDesc">A description of the identification.</param>
        /// <returns>A serializable IdentityDocumentEnum object.</returns>
        public static MISMOEnum<IdentityDocumentBase> ToIdentityDocumentEnum(E_IdType idType, out string idTypeOtherDesc)
        {
            if (idType == E_IdType.Blank)
            {
                idTypeOtherDesc = string.Empty;
                return null;
            }

            var identityDocumentEnum = new MISMOEnum<IdentityDocumentBase>();
            identityDocumentEnum.enumValue = GetIdentityDocumentBaseValue(idType, out idTypeOtherDesc);
            identityDocumentEnum.IsSensitive = true;

            return identityDocumentEnum;
        }

        /// <summary>
        /// Retrieves the IdentityDocument value corresponding to the given type.
        /// </summary>
        /// <param name="idType">The identification type.</param>
        /// <param name="idTypeOtherDesc">A description of the identification.</param>
        /// <returns>The corresponding IdentityDocument value.</returns>
        public static IdentityDocumentBase GetIdentityDocumentBaseValue(E_IdType idType, out string idTypeOtherDesc)
        {
            idTypeOtherDesc = string.Empty;

            switch (idType)
            {
                case E_IdType.DriversLicense:
                    return IdentityDocumentBase.DriversLicense;
                case E_IdType.GreenCard:
                    idTypeOtherDesc = "Green Card";
                    return IdentityDocumentBase.Other;
                case E_IdType.ImmigrationCard:
                    idTypeOtherDesc = "Immigration Card";
                    return IdentityDocumentBase.Other;
                case E_IdType.MilitaryId:
                    return IdentityDocumentBase.MilitaryIdentification;
                case E_IdType.OtherDocument:
                    return IdentityDocumentBase.Other;
                case E_IdType.Passport:
                    return IdentityDocumentBase.Passport;
                case E_IdType.StateId:
                    return IdentityDocumentBase.StateIdentification;
                case E_IdType.Visa:
                    return IdentityDocumentBase.Visa;
                default:
                    throw new UnhandledEnumException(idType);
            }
        }

        /// <summary>
        /// Creates a serializable IdentityDocumentIssuedByEnum object based on the given identification type.
        /// </summary>
        /// <param name="idType">The type of identification.</param>
        /// <param name="issuedByOtherDesc">A description of the issuer.</param>
        /// <returns>A serializable IdentityDocumentIssuedByEnum object.</returns>
        public static MISMOEnum<IdentityDocumentIssuedByBase> ToIdentityDocumentIssuedByEnum(E_IdType idType, out string issuedByOtherDesc)
        {
            if (idType == E_IdType.Blank || idType == E_IdType.GreenCard || idType == E_IdType.OtherDocument)
            {
                issuedByOtherDesc = string.Empty;
                return null;
            }

            var identityDocumentIssuedByEnum = new MISMOEnum<IdentityDocumentIssuedByBase>();
            identityDocumentIssuedByEnum.enumValue = GetIdentityDocumentIssuedByBaseValue(idType, out issuedByOtherDesc);
            identityDocumentIssuedByEnum.IsSensitive = true;

            return identityDocumentIssuedByEnum;
        }

        /// <summary>
        /// Retrieves the IdentityDocumentIssuedBy value corredponding to the identification type.
        /// </summary>
        /// <param name="idType">The type of identification.</param>
        /// <param name="issuedByOtherDesc">A description of the issuer.</param>
        /// <returns>The corresponding IdentityDocumentIssuedBy value.</returns>
        public static IdentityDocumentIssuedByBase GetIdentityDocumentIssuedByBaseValue(E_IdType idType, out string issuedByOtherDesc)
        {
            issuedByOtherDesc = string.Empty;

            switch (idType)
            {
                case E_IdType.DriversLicense:
                case E_IdType.StateId:
                    return IdentityDocumentIssuedByBase.StateProvince;
                case E_IdType.Passport:
                case E_IdType.MilitaryId:
                case E_IdType.ImmigrationCard:
                    return IdentityDocumentIssuedByBase.Country;
                case E_IdType.Visa:
                    issuedByOtherDesc = "Government Branch";
                    return IdentityDocumentIssuedByBase.Other;
                case E_IdType.GreenCard:
                case E_IdType.OtherDocument:
                default:
                    throw new UnhandledEnumException(idType);
            }
        }

        /// <summary>
        /// Converts an IncomeBase value to a serializable <see cref="MISMOEnum{IncomeBase}" /> object.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IncomeBase}" /> object.</returns>
        public static MISMOEnum<IncomeBase> ToIncomeEnum(IncomeBase mismoValue)
        {
            var incomeEnum = new MISMOEnum<IncomeBase>();
            incomeEnum.enumValue = mismoValue;
            incomeEnum.IsSensitive = false;

            return incomeEnum;
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{IncomeBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IncomeBase}" /> object.</returns>
        public static MISMOEnum<IncomeBase> ToIncomeEnum(E_aOIDescT lqbValue)
        {
            var incomeEnum = new MISMOEnum<IncomeBase>();
            incomeEnum.enumValue = GetIncomeBaseValue(lqbValue);
            incomeEnum.IsSensitive = false;

            return incomeEnum;
        }

        /// <summary>
        /// Retrieves the Income value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding Income value.</returns>
        public static IncomeBase GetIncomeBaseValue(E_aOIDescT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aOIDescT.AlimonyChildSupport:
                    return IncomeBase.ChildSupport;
                case E_aOIDescT.AutomobileExpenseAccount:
                    return IncomeBase.AutomobileExpenseAccount;
                case E_aOIDescT.BoarderIncome:
                    return IncomeBase.BoarderIncome;
                case E_aOIDescT.SocialSecurityDisability:
                    return IncomeBase.Disability;
                case E_aOIDescT.FosterCare:
                    return IncomeBase.FosterCare;
                case E_aOIDescT.MilitaryBasePay:
                    return IncomeBase.MilitaryBasePay;
                case E_aOIDescT.MilitaryClothesAllowance:
                    return IncomeBase.MilitaryClothesAllowance;
                case E_aOIDescT.MilitaryCombatPay:
                    return IncomeBase.MilitaryCombatPay;
                case E_aOIDescT.MilitaryFlightPay:
                    return IncomeBase.MilitaryFlightPay;
                case E_aOIDescT.MilitaryHazardPay:
                    return IncomeBase.MilitaryHazardPay;
                case E_aOIDescT.MilitaryOverseasPay:
                    return IncomeBase.MilitaryOverseasPay;
                case E_aOIDescT.MilitaryPropPay:
                    return IncomeBase.MilitaryPropPay;
                case E_aOIDescT.MilitaryQuartersAllowance:
                    return IncomeBase.MilitaryQuartersAllowance;
                case E_aOIDescT.MilitaryRationsAllowance:
                    return IncomeBase.MilitaryRationsAllowance;
                case E_aOIDescT.MilitaryVariableHousingAllowance:
                    return IncomeBase.MilitaryVariableHousingAllowance;
                case E_aOIDescT.MortgageCreditCertificate:
                    return IncomeBase.MortgageCreditCertificate;
                case E_aOIDescT.RealEstateMortgageDifferential:
                    return IncomeBase.MortgageDifferential;
                case E_aOIDescT.NotesReceivableInstallment:
                    return IncomeBase.NotesReceivableInstallment;
                case E_aOIDescT.PensionRetirement:
                    return IncomeBase.Pension;
                case E_aOIDescT.SubjPropNetCashFlow:
                    return IncomeBase.SubjectPropertyNetCashFlow;
                case E_aOIDescT.TrailingCoBorrowerIncome:
                    return IncomeBase.TrailingCoBorrowerIncome;
                case E_aOIDescT.Trust:
                    return IncomeBase.Trust;
                case E_aOIDescT.UnemploymentWelfare:
                    return IncomeBase.Unemployment;
                case E_aOIDescT.VABenefitsNonEducation:
                    return IncomeBase.VABenefitsNonEducational;
                case E_aOIDescT.NonBorrowerHouseholdIncome:
                    return IncomeBase.NonBorrowerContribution;
                case E_aOIDescT.HousingChoiceVoucher:
                    return IncomeBase.PublicAssistance;
                case E_aOIDescT.SocialSecurity:
                    return IncomeBase.SocialSecurity;
                case E_aOIDescT.Disability:
                    return IncomeBase.Disability;
                case E_aOIDescT.Alimony:
                    return IncomeBase.Alimony;
                case E_aOIDescT.ChildSupport:
                    return IncomeBase.ChildSupport;
                case E_aOIDescT.ContractBasis:
                    return IncomeBase.ContractBasis;
                case E_aOIDescT.DefinedContributionPlan:
                    return IncomeBase.DefinedContributionPlan;
                case E_aOIDescT.HousingAllowance:
                    return IncomeBase.HousingAllowance;
                case E_aOIDescT.MiscellaneousIncome:
                    return IncomeBase.MiscellaneousIncome;
                case E_aOIDescT.PublicAssistance:
                    return IncomeBase.PublicAssistance;
                case E_aOIDescT.WorkersCompensation:
                    return IncomeBase.WorkersCompensation;
                case E_aOIDescT.AccessoryUnitIncome:
                case E_aOIDescT.CapitalGains:
                case E_aOIDescT.EmploymentRelatedAssets:
                case E_aOIDescT.ForeignIncome:
                case E_aOIDescT.RoyaltyPayment:
                case E_aOIDescT.SeasonalIncome:
                case E_aOIDescT.TemporaryLeave:
                case E_aOIDescT.TipIncome:
                case E_aOIDescT.Other:
                    return IncomeBase.Other;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Converts an Index enumerated value into a serializable <see cref="MISMOEnum{IndexBase}" /> object.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to place in the object.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IndexBase}" /> object.</returns>
        public static MISMOEnum<IndexBase> ToIndexEnum(IndexBase mismoValue, bool displaySensitive = true)
        {
            var indexEnum = new MISMOEnum<IndexBase>();
            indexEnum.enumValue = mismoValue;
            indexEnum.IsSensitive = false;
            indexEnum.DisplaySensitive = displaySensitive;

            return indexEnum;
        }

        /// <summary>
        /// Converts a mortgage pool's index type enumerated value into a serializable <see cref="MISMOEnum{IndexBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to serialize.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IndexBase}" /> object.</returns>
        public static MISMOEnum<IndexBase> ToIndexEnum(E_MortgagePoolIndexT lqbValue, bool displaySensitive = true)
        {
            switch (lqbValue)
            {
                case E_MortgagePoolIndexT.CMT:
                    return ToIndexEnum(IndexBase.ConstantMaturityTreasury, displaySensitive);
                case E_MortgagePoolIndexT.LIBOR:
                    return ToIndexEnum(IndexBase.LIBOR, displaySensitive);
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates an <see cref="MISMOEnum{IndexSourceBase}" /> object with the index associated with the adjustable interest rate on the loan, based on the intermediate adjustment period and the Fannie or Freddie index.
        /// </summary>
        /// <param name="fnmaIndexType">The Fannie Mae ARM index.</param>
        /// <param name="freddieIndexType">The Freddie Mac ARM index.</param>
        /// <param name="rateAdjustmentPeriod">The intermediate adjustment period for the adjustable interest rate.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>An <see cref="MISMOEnum{IndexSourceBase}" /> object with the index associated with the adjustable interest rate on the loan.</returns>
        public static MISMOEnum<IndexSourceBase> ToIndexSourceEnum(E_sArmIndexT fnmaIndexType, E_sFreddieArmIndexT freddieIndexType, int rateAdjustmentPeriod, bool isSensitiveData = false)
        {
            var indexSourceEnum = new MISMOEnum<IndexSourceBase>();
            indexSourceEnum.enumValue = GetIndexSourceBase(fnmaIndexType, freddieIndexType, rateAdjustmentPeriod);
            indexSourceEnum.IsSensitive = isSensitiveData;

            if (indexSourceEnum.enumValue == IndexSourceBase.Blank)
            {
                return null;
            }

            return indexSourceEnum;
        }

        /// <summary>
        /// Converts the given intermediate rate adjustment period and the Fannie or Freddie index to the MISMO defined index type.
        /// </summary>
        /// <param name="fnmaIndexType">The Fannie Mae ARM index.</param>
        /// <param name="freddieIndexType">The Freddie Mac ARM index.</param>
        /// <param name="rateAdjustmentPeriod">The intermediate adjustment period for the adjustable interest rate.</param>
        /// <returns>An IndexSourceBase object with the MISMO defined index type corresponding to the given rate adjustment period and indices.</returns>
        public static IndexSourceBase GetIndexSourceBase(E_sArmIndexT fnmaIndexType, E_sFreddieArmIndexT freddieIndexType, int rateAdjustmentPeriod)
        {
            switch (fnmaIndexType)
            {
                case E_sArmIndexT.DailyCDRate:
                    break;
                case E_sArmIndexT.EleventhDistrictCOF:
                    return IndexSourceBase.FHLBEleventhDistrictMonthlyCostOfFundsIndex;
                case E_sArmIndexT.FannieMaeLIBOR:
                case E_sArmIndexT.LeaveBlank:
                case E_sArmIndexT.MonthlyAvgCMT:
                    break;
                case E_sArmIndexT.NationalMonthlyMedianCostOfFunds:
                    return IndexSourceBase.NationalMonthlyMedianCostOfFundsIndexOTS;
                case E_sArmIndexT.TBillDailyValue:
                    break;
                case E_sArmIndexT.WallStreetJournalLIBOR:
                    if (rateAdjustmentPeriod == 6)
                    {
                        return IndexSourceBase.SixMonthLIBOR_WSJDaily;
                    }
                    else if (rateAdjustmentPeriod == 12)
                    {
                        return IndexSourceBase.LIBOROneYearWSJDaily;
                    }

                    break;
                case E_sArmIndexT.WeeklyAvgCDRate:
                    break;
                case E_sArmIndexT.WeeklyAvgCMT:
                    if (rateAdjustmentPeriod == 12)
                    {
                        return IndexSourceBase.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
                    }
                    else if (rateAdjustmentPeriod == 36)
                    {
                        return IndexSourceBase.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
                    }
                    else if (rateAdjustmentPeriod == 60)
                    {
                        return IndexSourceBase.WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15;
                    }
                    else if (rateAdjustmentPeriod == 120)
                    {
                        return IndexSourceBase.WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15;
                    }

                    break;
                case E_sArmIndexT.WeeklyAvgPrimeRate:
                case E_sArmIndexT.WeeklyAvgSMTI:
                case E_sArmIndexT.WeeklyAvgTAABD:
                case E_sArmIndexT.WeeklyAvgTAAI:
                    break;
                default:
                    throw new UnhandledEnumException(fnmaIndexType);
            }

            switch (freddieIndexType)
            {
                case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds:
                    return IndexSourceBase.FHLBEleventhDistrictMonthlyCostOfFundsIndex;
                case E_sFreddieArmIndexT.LeaveBlank:
                    return IndexSourceBase.Blank;
                case E_sFreddieArmIndexT.LIBOR:
                    if (rateAdjustmentPeriod == 6)
                    {
                        return IndexSourceBase.SixMonthLIBOR_WSJDaily;
                    }
                    else if (rateAdjustmentPeriod == 12)
                    {
                        return IndexSourceBase.LIBOROneYearWSJDaily;
                    }

                    break;
                case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds:
                    return IndexSourceBase.NationalMonthlyMedianCostOfFundsIndexOTS;
                case E_sFreddieArmIndexT.OneYearTreasury:
                    return IndexSourceBase.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
                case E_sFreddieArmIndexT.Other:
                case E_sFreddieArmIndexT.SixMonthTreasury:
                    break;
                case E_sFreddieArmIndexT.ThreeYearTreasury:
                    return IndexSourceBase.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
                default:
                    throw new UnhandledEnumException(freddieIndexType);
            }

            return IndexSourceBase.Other; 
        }

        /// <summary>
        /// Creates an integrated disclosure section enumeration object from the given section.
        /// </summary>
        /// <param name="section">The integrated disclosure section.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntegratedDisclosureSectionBase}" /> object.</returns>
        public static MISMOEnum<IntegratedDisclosureSectionBase> ToIntegratedDisclosureSectionEnum(IntegratedDisclosureSectionBase section)
        {
            if (section == IntegratedDisclosureSectionBase.Blank)
            {
                return null;
            }

            var integratedDisclosureSectionEnum = new MISMOEnum<IntegratedDisclosureSectionBase>();
            integratedDisclosureSectionEnum.enumValue = section;
            integratedDisclosureSectionEnum.IsSensitive = false;

            return integratedDisclosureSectionEnum;
        }

        /// <summary>
        /// Creates an object representing the integrated disclosure section for a given fee.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure.</param>
        /// <param name="section">The closing cost details section.</param>
        /// <param name="canShop">The "Can Shop" value for the fee, for determining "B or C" value.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntegratedDisclosureSectionBase}" /> object.</returns>
        public static MISMOEnum<IntegratedDisclosureSectionBase> ToIntegratedDisclosureSectionEnum(IntegratedDisclosureDocumentBase documentType, E_IntegratedDisclosureSectionT section, bool canShop = false)
        {
            return ToIntegratedDisclosureSectionEnum(GetIntegratedDisclosureSectionBaseValue(documentType, section, canShop));
        }

        /// <summary>
        /// Gets the integrated disclosure section type based on the type of disclosure (loan estimate | closing disclosure) and the given closing cost details section of the disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure.</param>
        /// <param name="sectionType">The closing cost details section.</param>
        /// <param name="canShop">The "Can Shop" value for the fee, for determining "B or C" value. </param>
        /// <returns>The <see cref="IntegratedDisclosureSectionBase" /> value corresponding to the given closing cost details section of the given disclosure.</returns>
        public static IntegratedDisclosureSectionBase GetIntegratedDisclosureSectionBaseValue(IntegratedDisclosureDocumentBase documentType, E_IntegratedDisclosureSectionT sectionType, bool canShop = false)
        {
            if (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure)
            {
                return IntegratedDisclosureSectionBase.Blank;
            }

            bool estimate = documentType == IntegratedDisclosureDocumentBase.LoanEstimate;

            switch (sectionType)
            {
                case E_IntegratedDisclosureSectionT.LeaveBlank:
                    return IntegratedDisclosureSectionBase.Blank;
                case E_IntegratedDisclosureSectionT.SectionA:
                    return IntegratedDisclosureSectionBase.OriginationCharges;
                case E_IntegratedDisclosureSectionT.SectionB:
                    return estimate ? IntegratedDisclosureSectionBase.ServicesYouCannotShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidNotShopFor;
                case E_IntegratedDisclosureSectionT.SectionBorC:
                    // sg 10/08/2015 - Case 224832 - If canShop is checked, return Section C logic, if not checked, return Section B logic.
                    if (canShop)
                    {
                        return estimate ? IntegratedDisclosureSectionBase.ServicesYouCanShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidShopFor;
                    }
                    else
                    {
                        return estimate ? IntegratedDisclosureSectionBase.ServicesYouCannotShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidNotShopFor;
                    }

                case E_IntegratedDisclosureSectionT.SectionC:
                    return estimate ? IntegratedDisclosureSectionBase.ServicesYouCanShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidShopFor;
                case E_IntegratedDisclosureSectionT.SectionD:
                    return IntegratedDisclosureSectionBase.TotalLoanCosts;
                case E_IntegratedDisclosureSectionT.SectionE:
                    return IntegratedDisclosureSectionBase.TaxesAndOtherGovernmentFees;
                case E_IntegratedDisclosureSectionT.SectionF:
                    return IntegratedDisclosureSectionBase.Prepaids;
                case E_IntegratedDisclosureSectionT.SectionG:
                    return IntegratedDisclosureSectionBase.InitialEscrowPaymentAtClosing;
                case E_IntegratedDisclosureSectionT.SectionH:
                    return IntegratedDisclosureSectionBase.OtherCosts;
                default:
                    throw new UnhandledEnumException(sectionType);
            }
        }

        /// <summary>
        /// Creates an integrated disclosure subsection enumeration object from the given subsection.
        /// </summary>
        /// <param name="subsection">The integrated disclosure subsection.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntegratedDisclosureSubsectionBase}" /> object.</returns>
        public static MISMOEnum<IntegratedDisclosureSubsectionBase> ToIntegratedDisclosureSubsectionEnum(IntegratedDisclosureSubsectionBase subsection)
        {
            if (subsection == IntegratedDisclosureSubsectionBase.Blank)
            {
                return null;
            }

            var integratedDisclosureSubsectionEnum = new MISMOEnum<IntegratedDisclosureSubsectionBase>();
            integratedDisclosureSubsectionEnum.enumValue = subsection;
            integratedDisclosureSubsectionEnum.IsSensitive = false;

            return integratedDisclosureSubsectionEnum;
        }

        /// <summary>
        /// Creates an integrated disclosure subsection paid by enumeration object that indicates the responsible party (e.g. buyer | seller | other).
        /// </summary>
        /// <param name="paidBy">The type of party that has / will make the payment.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntegratedDisclosureSubsectionPaidByBase}" /> object.</returns>
        public static MISMOEnum<IntegratedDisclosureSubsectionPaidByBase> ToIntegratedDisclosureSubsectionPaidByEnum(IntegratedDisclosureSubsectionPaidByBase paidBy)
        {
            if (paidBy == IntegratedDisclosureSubsectionPaidByBase.Blank)
            {
                return null;
            }

            var integratedDisclosureSubsectionPaidByEnum = new MISMOEnum<IntegratedDisclosureSubsectionPaidByBase>();
            integratedDisclosureSubsectionPaidByEnum.enumValue = paidBy;
            integratedDisclosureSubsectionPaidByEnum.IsSensitive = false;

            return integratedDisclosureSubsectionPaidByEnum;
        }

        /// <summary>
        /// Creates an integrated disclosure subsection payment timing enumeration object that indicates whether the payment will be at closing or prior to.
        /// </summary>
        /// <param name="timing">The timing of the payment, i.e. at closing | prior to.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntegratedDisclosureSubsectionPaymentTimingBase}" /> object.</returns>
        public static MISMOEnum<IntegratedDisclosureSubsectionPaymentTimingBase> ToIntegratedDisclosureSubsectionPaymentTimingEnum(IntegratedDisclosureSubsectionPaymentTimingBase timing)
        {
            if (timing == IntegratedDisclosureSubsectionPaymentTimingBase.Blank)
            {
                return null;
            }

            var integratedDisclosureSubsectionPaymentTimingEnum = new MISMOEnum<IntegratedDisclosureSubsectionPaymentTimingBase>();
            integratedDisclosureSubsectionPaymentTimingEnum.enumValue = timing;
            integratedDisclosureSubsectionPaymentTimingEnum.IsSensitive = false;

            return integratedDisclosureSubsectionPaymentTimingEnum;
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{IntentToOccupyBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{IntentToOccupyBase}" /> object.</returns>
        public static MISMOEnum<IntentToOccupyBase> ToIntentToOccupyEnum(string lqbValue)
        {
            var intentToOccupyEnum = new MISMOEnum<IntentToOccupyBase>();
            intentToOccupyEnum.enumValue = GetIntentToOccupyBaseValue(lqbValue);
            intentToOccupyEnum.IsSensitive = false;

            return intentToOccupyEnum;
        }

        /// <summary>
        /// Retrieves the IntentToOccupy value corresponding to a given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding IntentToOccupy value.</returns>
        public static IntentToOccupyBase GetIntentToOccupyBaseValue(string lqbValue)
        {
            if (string.Equals(lqbValue, "Y", StringComparison.OrdinalIgnoreCase))
            {
                return IntentToOccupyBase.Yes;
            }
            else if (string.Equals(lqbValue, "N", StringComparison.OrdinalIgnoreCase))
            {
                return IntentToOccupyBase.No;
            }
            else
            {
                return IntentToOccupyBase.Unknown;
            }
        }

        /// <summary>
        /// Converts an LQB enumerated value into a serializable <see cref="MISMOEnum{InterestRateRoundingBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value for conversion.</param>
        /// <returns>A serializable <see cref="MISMOEnum{InterestRateRoundingBase}" /> object.</returns>
        public static MISMOEnum<InterestRateRoundingBase> ToInterestRateRoundingEnum(E_sRAdjRoundT lqbValue)
        {
            var interestRateRoundingEnum = new MISMOEnum<InterestRateRoundingBase>();
            interestRateRoundingEnum.enumValue = GetInterestRateRoundingValue(lqbValue);
            interestRateRoundingEnum.IsSensitive = false;

            return interestRateRoundingEnum;
        }

        /// <summary>
        /// Retrieves the InterestRateRoundingBase value corresponding to an enumerated value from LQB.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding InterestRateRoundingBase value.</returns>
        public static InterestRateRoundingBase GetInterestRateRoundingValue(E_sRAdjRoundT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sRAdjRoundT.Down:
                    return InterestRateRoundingBase.Down;
                case E_sRAdjRoundT.Normal:
                    return InterestRateRoundingBase.Nearest;
                case E_sRAdjRoundT.Up:
                    return InterestRateRoundingBase.Up;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{JointAssetLiabilityReportingBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{JointAssetLiabilityReportingBase}" /> object.</returns>
        public static MISMOEnum<JointAssetLiabilityReportingBase> ToJointAssetLiabilityReportingEnum(bool lqbValue)
        {
            var jointAssetLiabilityReportingEnum = new MISMOEnum<JointAssetLiabilityReportingBase>();
            jointAssetLiabilityReportingEnum.enumValue = GetJointAssetLiabilityReportingBaseValue(lqbValue);
            jointAssetLiabilityReportingEnum.IsSensitive = false;

            return jointAssetLiabilityReportingEnum;
        }

        /// <summary>
        /// Retrieves the JointAssetLiabilityReporting value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding JointAssetLiabilityReporting value.</returns>
        public static JointAssetLiabilityReportingBase GetJointAssetLiabilityReportingBaseValue(bool lqbValue)
        {
            return lqbValue ? JointAssetLiabilityReportingBase.NotJointly : JointAssetLiabilityReportingBase.Jointly;
        }

        /// <summary>
        /// Creates a serializable <see cref="MISMOEnum{LegalEntityBase}" /> container with the given seller type.
        /// </summary>
        /// <param name="sellerType">The seller's legal entity type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LegalEntityBase}" /> container.</returns>
        public static MISMOEnum<LegalEntityBase> ToLegalEntityEnum(E_SellerEntityType sellerType)
        {
            if (sellerType == E_SellerEntityType.Undefined)
            {
                return null;
            }

            var legalEntityEnum = new MISMOEnum<LegalEntityBase>();
            legalEntityEnum.enumValue = GetLegalEntityBaseValue(sellerType);
            legalEntityEnum.IsSensitive = false;

            return legalEntityEnum;
        }

        /// <summary>
        /// Retrieves the LegalEntityBase value corresponding to the given seller type.
        /// </summary>
        /// <param name="sellerType">The seller's legal entity type.</param>
        /// <returns>The corresponding LegalEntityBase value.</returns>
        public static LegalEntityBase GetLegalEntityBaseValue(E_SellerEntityType sellerType)
        {
            switch (sellerType)
            {
                case E_SellerEntityType.Corporation:
                    return LegalEntityBase.Corporation;
                case E_SellerEntityType.GeneralPartnership:
                    return LegalEntityBase.Partnership;
                case E_SellerEntityType.LimitedLiabilityCompany:
                    return LegalEntityBase.LimitedLiabilityCompany;
                case E_SellerEntityType.LimitedPartnership:
                    return LegalEntityBase.LimitedPartnership;
                case E_SellerEntityType.SoleProprietor:
                    return LegalEntityBase.SoleProprietorship;
                case E_SellerEntityType.Company:
                case E_SellerEntityType.LimitedLiabilityCorporation:
                    return LegalEntityBase.Other;
                default:
                    throw new UnhandledEnumException(sellerType);
            }
        }

        /// <summary>
        /// Converts Liability type to MISMO compliant value.
        /// </summary>
        /// <param name="debtType">Liability type, as represented in liability editor.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <returns>Returns MISMO compliant liability type.</returns>
        public static MISMOEnum<LiabilityBase> ToLiabilityEnum(E_DebtRegularT debtType, string displayLabelText)
        {
            MISMOEnum<LiabilityBase> liabilityEnum = new MISMOEnum<LiabilityBase>();
            liabilityEnum.DisplayLabelText = displayLabelText;
            liabilityEnum.enumValue = GetLiabilityBaseValue(debtType);

            return liabilityEnum;
        }

        /// <summary>
        /// Gets the MISMO liability type corresponding to a datalayer regular debt type.
        /// </summary>
        /// <param name="debtType">The regular debt type.</param>
        /// <returns>The corresponding MISMO liability type.</returns>
        public static LiabilityBase GetLiabilityBaseValue(E_DebtRegularT debtType)
        {
            switch (debtType)
            {
                case E_DebtRegularT.Heloc:
                    return LiabilityBase.HELOC;
                case E_DebtRegularT.Installment:
                    return LiabilityBase.Installment;
                case E_DebtRegularT.Lease:
                    return LiabilityBase.LeasePayment;
                case E_DebtRegularT.Mortgage:
                    return LiabilityBase.MortgageLoan;
                case E_DebtRegularT.Open:
                    return LiabilityBase.Open30DayChargeAccount;
                case E_DebtRegularT.Revolving:
                    return LiabilityBase.Revolving;
                case E_DebtRegularT.SeparateMaintenance:
                case E_DebtRegularT.OtherExpenseLiability:
                case E_DebtRegularT.Other:
                    return LiabilityBase.Other;
                default:
                    throw new UnhandledEnumException(debtType);
            }
        }

        /// <summary>
        /// Creates a liability enumeration object from the given liability type.
        /// </summary>
        /// <param name="liabilityType">The liability type.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <returns>A serializable <see cref="MISMOEnum{LiabilityBase}" /> object.</returns>
        public static MISMOEnum<LiabilityBase> ToLiabilityEnum(LiabilityBase liabilityType, string displayLabelText)
        {
            var liabilityEnum = new MISMOEnum<LiabilityBase>();
            liabilityEnum.enumValue = liabilityType;
            liabilityEnum.IsSensitive = false;
            liabilityEnum.DisplayLabelText = displayLabelText;

            return liabilityEnum;
        }

        /// <summary>
        /// Creates a liability exclusion indicator object from the given debt type.
        /// </summary>
        /// <param name="debtType">The type of debt represented by the liability.</param>
        /// <param name="willBePaidOff">Indicates whether the liability will be paid off.</param>
        /// <param name="notUsedInRatio">Indicates whether the liability is used to calculate the debt ratios.</param>
        /// <returns>A serializable MISMOIndicator object.</returns>
        public static MISMOIndicator ToLiabilityExclusionIndicator(E_DebtRegularT debtType, bool willBePaidOff, bool notUsedInRatio)
        {
            if (debtType == E_DebtRegularT.Mortgage)
            {
                return ToMismoIndicator(willBePaidOff);
            }
            else
            {
                return ToMismoIndicator(notUsedInRatio);
            }
        }

        /// <summary>
        /// Creates an indicator specifying whether the buyer will pay off a liability before closing with current assets.
        /// </summary>
        /// <param name="timing">Indicates when the buyer will pay off the liability before closing with current assets.</param>
        /// <returns>A serializable MISMOIndicator object.</returns>
        public static MISMOIndicator ToLiabilityPayoffWithCurrentAssetsIndicator(E_Timing timing)
        {
            if (timing == E_Timing.Blank)
            {
                return null;
            }

            var mismoIndicator = new MISMOIndicator();
            mismoIndicator.IsSensitive = true;
            mismoIndicator.booleanValue = timing == E_Timing.Before_Closing;

            return mismoIndicator;
        }

        /// <summary>
        /// Converts an LQB enumerated value into a serializable <see cref="MISMOEnum{LienPriorityBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LienPriorityBase}" /> object.</returns>
        public static MISMOEnum<LienPriorityBase> ToLienPriorityEnum(E_sLienPosT lqbValue)
        {
            var lienPriorityEnum = new MISMOEnum<LienPriorityBase>();
            lienPriorityEnum.enumValue = GetLienPriorityValue(lqbValue);
            lienPriorityEnum.IsSensitive = false;

            return lienPriorityEnum;
        }

        /// <summary>
        /// Retrieves the MISMO enumeration that corresponds to the LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding MISMO LienPriority enumeration.</returns>
        public static LienPriorityBase GetLienPriorityValue(E_sLienPosT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sLienPosT.First:
                    return LienPriorityBase.FirstLien;
                case E_sLienPosT.Second:
                    return LienPriorityBase.SecondLien;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Converts a datalayer lien position into a serializable MISMO value.
        /// </summary>
        /// <param name="position">The lien position.</param>
        /// <returns>A serializable MISMO value.</returns>
        public static MISMOEnum<LienPriorityBase> ToLienPriorityEnum(E_sLoanBeingRefinancedLienPosT position)
        {
            if (position == E_sLoanBeingRefinancedLienPosT.LeaveBlank)
            {
                return null;
            }

            var lienPriorityEnum = new MISMOEnum<LienPriorityBase>();
            lienPriorityEnum.enumValue = GetLienPriorityValue(position);
            lienPriorityEnum.IsSensitive = false;

            return lienPriorityEnum;
        }

        /// <summary>
        /// Retrieves the MISMO value corresponding to a datalayer lien position.
        /// </summary>
        /// <param name="position">A datalayer lien position.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LienPriorityBase GetLienPriorityValue(E_sLoanBeingRefinancedLienPosT position)
        {
            switch (position)
            {
                case E_sLoanBeingRefinancedLienPosT.LeaveBlank:
                    return LienPriorityBase.Blank;
                case E_sLoanBeingRefinancedLienPosT.First:
                    return LienPriorityBase.FirstLien;
                case E_sLoanBeingRefinancedLienPosT.Second:
                    return LienPriorityBase.SecondLien;
                default:
                    throw new UnhandledEnumException(position);
            }
        }

        /// <summary>
        /// Converts a LoanAmortizationPeriod enumerated value into a serializable object.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to convert.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LoanAmortizationPeriodBase}" /> object.</returns>
        public static MISMOEnum<LoanAmortizationPeriodBase> ToLoanAmortizationPeriodEnum(LoanAmortizationPeriodBase mismoValue)
        {
            var loanAmortizationPeriodEnum = new MISMOEnum<LoanAmortizationPeriodBase>();
            loanAmortizationPeriodEnum.enumValue = mismoValue;
            loanAmortizationPeriodEnum.IsSensitive = false;

            return loanAmortizationPeriodEnum;
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{LoanConsiderationDisclosureStatementBase}" /> object based on the given statement type.
        /// </summary>
        /// <param name="statementType">The type of consideration or disclosure statement.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LoanConsiderationDisclosureStatementBase}" /> object.</returns>
        public static MISMOEnum<LoanConsiderationDisclosureStatementBase> ToLoanConsiderationDisclosureStatementEnum(LoanConsiderationDisclosureStatementBase statementType)
        {
            var statement = new MISMOEnum<LoanConsiderationDisclosureStatementBase>();
            statement.enumValue = statementType;
            statement.IsSensitive = false;

            return statement;
        }

        /// <summary>
        /// Converts a MISMO LoanIdentifier value into a serializable <see cref="MISMOEnum{LoanIdentifierBase}" /> object.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to convert.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LoanIdentifierBase}" /> object.</returns>
        public static MISMOEnum<LoanIdentifierBase> ToLoanIdentifierEnum(LoanIdentifierBase mismoValue, bool displaySensitive = true)
        {
            var loanIdentifierEnum = new MISMOEnum<LoanIdentifierBase>();
            loanIdentifierEnum.enumValue = mismoValue;
            loanIdentifierEnum.IsSensitive = false;
            loanIdentifierEnum.DisplaySensitive = displaySensitive;

            return loanIdentifierEnum;
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{LoanLevelCreditScoreSelectionMethodBase}" /> object with the given selection method.
        /// </summary>
        /// <param name="method">The loan-level credit score selection method.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A <see cref="MISMOEnum{LoanLevelCreditScoreSelectionMethodBase}" /> object with the given selection method.</returns>
        public static MISMOEnum<LoanLevelCreditScoreSelectionMethodBase> ToLoanLevelCreditScoreSelectionMethodEnum(LoanLevelCreditScoreSelectionMethodBase method, bool isSensitiveData = false)
        {
            if (method == LoanLevelCreditScoreSelectionMethodBase.Blank)
            {
                return null;
            }

            var methodEnum = new MISMOEnum<LoanLevelCreditScoreSelectionMethodBase>();
            methodEnum.enumValue = method;
            methodEnum.IsSensitive = isSensitiveData;

            return methodEnum;
        }

        /// <summary>
        /// Converts a LoanMaturityPeriod enumerated value into a serializable object.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to convert.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LoanMaturityPeriodBase}" /> object.</returns>
        public static MISMOEnum<LoanMaturityPeriodBase> ToLoanMaturityPeriodEnum(LoanMaturityPeriodBase mismoValue, bool displaySensitive = true)
        {
            var loanMaturityPeriodEnum = new MISMOEnum<LoanMaturityPeriodBase>();
            loanMaturityPeriodEnum.enumValue = mismoValue;
            loanMaturityPeriodEnum.IsSensitive = false;
            loanMaturityPeriodEnum.DisplaySensitive = displaySensitive;

            return loanMaturityPeriodEnum;
        }

        /// <summary>
        /// Converts an LQB branch channel into a Loan Origination Channel Enum for serialization.
        /// </summary>
        /// <param name="branchChannel">The LQB value to convert.</param>
        /// <returns>A serializable channel enum for serialization.</returns>
        public static MISMOEnum<LoanOriginationChannelBase> ToLoanOriginationChannelEnum(E_BranchChannelT branchChannel)
        {
            var loanOriginationChannelEnum = new MISMOEnum<LoanOriginationChannelBase>();

            switch (branchChannel)
            {
                case E_BranchChannelT.Retail:
                    loanOriginationChannelEnum.enumValue = LoanOriginationChannelBase.Retail;
                    break;
                case E_BranchChannelT.Wholesale:
                    loanOriginationChannelEnum.enumValue = LoanOriginationChannelBase.Wholesale;
                    break;
                case E_BranchChannelT.Correspondent:
                    loanOriginationChannelEnum.enumValue = LoanOriginationChannelBase.Correspondent;
                    break;
                case E_BranchChannelT.Broker:
                    loanOriginationChannelEnum.enumValue = LoanOriginationChannelBase.BrokeredOut;
                    break;
                case E_BranchChannelT.Blank:
                    return null;
                default:
                    throw new UnhandledEnumException(branchChannel);
            }

            return loanOriginationChannelEnum;
        }

        /// <summary>
        /// Creates a loan originator enumeration object based on the given originator type.
        /// </summary>
        /// <param name="originatorType">The loan originator type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LoanOriginatorBase}" /> object.</returns>
        public static MISMOEnum<LoanOriginatorBase> ToLoanOriginatorEnum(E_BranchChannelT originatorType)
        {
            var loanOriginatorEnum = new MISMOEnum<LoanOriginatorBase>();
            loanOriginatorEnum.enumValue = GetLoanOriginatorBaseValue(originatorType);
            loanOriginatorEnum.IsSensitive = false;

            return loanOriginatorEnum;
        }

        /// <summary>
        /// Retrieves the loan originator value corresponding to the given originator type.
        /// </summary>
        /// <param name="originatorType">The loan originator type.</param>
        /// <returns>The corresponding loan originator value.</returns>
        public static LoanOriginatorBase GetLoanOriginatorBaseValue(E_BranchChannelT originatorType)
        {
            switch (originatorType)
            {
                case E_BranchChannelT.Broker:
                case E_BranchChannelT.Wholesale:
                    return LoanOriginatorBase.Broker;
                case E_BranchChannelT.Correspondent:
                    return LoanOriginatorBase.Correspondent;
                case E_BranchChannelT.Blank:
                case E_BranchChannelT.Retail:
                    return LoanOriginatorBase.Lender;
                default:
                    throw new UnhandledEnumException(originatorType);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{LoanPurposeBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <param name="isHeloc">Indicates whether the loan is a HELOC.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LoanPurposeBase}" /> object.</returns>
        public static MISMOEnum<LoanPurposeBase> ToLoanPurposeEnum(E_sLPurposeT lqbValue, bool isHeloc, bool displaySensitive = false)
        {
            var loanPurposeEnum = new MISMOEnum<LoanPurposeBase>();
            loanPurposeEnum.enumValue = GetLoanPurposeBaseValue(lqbValue, isHeloc);
            loanPurposeEnum.IsSensitive = false;
            loanPurposeEnum.DisplaySensitive = displaySensitive;

            return loanPurposeEnum;
        }

        /// <summary>
        /// Converts an LQB NMLS Loan Purpose value into a serializable <see cref="MISMOEnum{LoanPurposeBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>A serializable <see cref="MISMOEnum{LoanPurposeBase}" /> object.</returns>
        public static MISMOEnum<LoanPurposeBase> ToLoanPurposeEnum(E_NMLSLoanPurposeT lqbValue)
        {
            var loanPurposeEnum = new MISMOEnum<LoanPurposeBase>();

            switch (lqbValue)
            {
                case E_NMLSLoanPurposeT.Purchase:
                    loanPurposeEnum.enumValue = LoanPurposeBase.Purchase;
                    break;
                case E_NMLSLoanPurposeT.RefinanceCashOutRefinance:
                case E_NMLSLoanPurposeT.RefinanceOtherUnknown:
                case E_NMLSLoanPurposeT.RefinanceRateTerm:
                case E_NMLSLoanPurposeT.RefinanceRestructure:
                    loanPurposeEnum.enumValue = LoanPurposeBase.Refinance;
                    break;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }

            loanPurposeEnum.IsSensitive = false;

            return loanPurposeEnum;
        }

        /// <summary>
        /// Retrieves the LoanPurpose enumeration corresponding to a given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <param name="isHeloc">Indicates whether the loan is a HELOC.</param>
        /// <returns>The corresponding LoanPurpose value.</returns>
        public static LoanPurposeBase GetLoanPurposeBaseValue(E_sLPurposeT lqbValue, bool isHeloc)
        {
            if (isHeloc)
            {
                return LoanPurposeBase.Other;
            }

            switch (lqbValue)
            {
                case E_sLPurposeT.Purchase:
                    return LoanPurposeBase.Purchase;
                case E_sLPurposeT.Other:
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    return LoanPurposeBase.Other;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.HomeEquity:
                case E_sLPurposeT.VaIrrrl:
                    return LoanPurposeBase.Refinance;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{ManufacturedHomeConditionDescriptionBase}" /> object with the condition type of the home.
        /// </summary>
        /// <param name="condition">New or Used.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A <see cref="MISMOEnum{ManufacturedHomeConditionDescriptionBase}" /> object.</returns>
        public static MISMOEnum<ManufacturedHomeConditionDescriptionBase> ToManufacturedHomeConditionDescriptionEnum(E_ManufacturedHomeConditionT condition, bool isSensitiveData = false)
        {
            var conditionEnum = new MISMOEnum<ManufacturedHomeConditionDescriptionBase>();
            conditionEnum.enumValue = GetManufacturedHomeConditionDescriptionBase(condition);
            conditionEnum.IsSensitive = isSensitiveData;

            if (conditionEnum.enumValue == ManufacturedHomeConditionDescriptionBase.Blank)
            {
                return null;
            }

            return conditionEnum;
        }

        /// <summary>
        /// Converts the given manufactured home condition type to the corresponding ManufacturedHomeConditionDescriptionBase.
        /// </summary>
        /// <param name="condition">New or Used.</param>
        /// <returns>The ManufacturedHomeConditionDescriptionBase corresponding to the given condition.</returns>
        public static ManufacturedHomeConditionDescriptionBase GetManufacturedHomeConditionDescriptionBase(E_ManufacturedHomeConditionT condition)
        {
            switch (condition)
            {
                case E_ManufacturedHomeConditionT.LeaveBlank:
                    return ManufacturedHomeConditionDescriptionBase.Blank;
                case E_ManufacturedHomeConditionT.New:
                    return ManufacturedHomeConditionDescriptionBase.New;
                case E_ManufacturedHomeConditionT.Used:
                    return ManufacturedHomeConditionDescriptionBase.Used;
                default:
                    throw new UnhandledEnumException(condition);
            }
        }

        /// <summary>
        /// Converts an LQB value into a <see cref="MISMOEnum{ManufacturedHomeWidthBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ManufacturedHomeWidthBase}" /> object.</returns>
        public static MISMOEnum<ManufacturedHomeWidthBase> ToManufacturedHomeWidthEnum(E_sGseSpT lqbValue)
        {
            if (lqbValue != E_sGseSpT.ManufacturedHomeMultiwide && lqbValue != E_sGseSpT.ManufacturedHousingSingleWide)
            {
                return null;
            }

            var manufacturedHomeWidthEnum = new MISMOEnum<ManufacturedHomeWidthBase>();
            manufacturedHomeWidthEnum.enumValue = GetManufacturedHomeWidthBaseValue(lqbValue);
            manufacturedHomeWidthEnum.IsSensitive = false;

            return manufacturedHomeWidthEnum;
        }

        /// <summary>
        /// Retrieves the ManufacturedHomeWidth value that corresponds to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding ManufacturedHomeWidth value.</returns>
        public static ManufacturedHomeWidthBase GetManufacturedHomeWidthBaseValue(E_sGseSpT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sGseSpT.ManufacturedHomeMultiwide:
                    return ManufacturedHomeWidthBase.MultiWide;
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return ManufacturedHomeWidthBase.SingleWide;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{MaritalStatusBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MaritalStatusBase}" /> object.</returns>
        public static MISMOEnum<MaritalStatusBase> ToMaritalStatusEnum(E_aBMaritalStatT lqbValue)
        {
            if (lqbValue == E_aBMaritalStatT.LeaveBlank)
            {
                return null;
            }

            var maritalStatusEnum = new MISMOEnum<MaritalStatusBase>();
            maritalStatusEnum.enumValue = GetMaritalStatusBaseValue(lqbValue);
            maritalStatusEnum.IsSensitive = false;

            return maritalStatusEnum;
        }

        /// <summary>
        /// Retrieves the MaritalStatus enumeration corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding MaritalStatus value.</returns>
        public static MaritalStatusBase GetMaritalStatusBaseValue(E_aBMaritalStatT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aBMaritalStatT.Married:
                    return MaritalStatusBase.Married;
                case E_aBMaritalStatT.NotMarried:
                    return MaritalStatusBase.Unmarried;
                case E_aBMaritalStatT.Separated:
                    return MaritalStatusBase.Separated;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates an MI company name enumeration object with the name of the given mortgage insurance provider.
        /// </summary>
        /// <param name="company">The mortgage insurance provider.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MICompanyNameBase}" /> object.</returns>
        public static MISMOEnum<MICompanyNameBase> ToMICompanyNameEnum(E_sMiCompanyNmT company, bool isSensitiveData = false)
        {
            var micompanyNameEnum = new MISMOEnum<MICompanyNameBase>();
            micompanyNameEnum.enumValue = GetMICompanyNameBaseValue(company);
            micompanyNameEnum.IsSensitive = isSensitiveData;

            return micompanyNameEnum;
        }

        /// <summary>
        /// Determines the MI company name for the given company.
        /// </summary>
        /// <param name="company">The mortgage insurance provider.</param>
        /// <returns>The MI company name for the given company.</returns>
        public static MICompanyNameBase GetMICompanyNameBaseValue(E_sMiCompanyNmT company)
        {
            switch (company)
            {
                case E_sMiCompanyNmT.Amerin:
                case E_sMiCompanyNmT.Arch:
                case E_sMiCompanyNmT.CAHLIF:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.CMG:
                case E_sMiCompanyNmT.CMGPre94:
                    return MICompanyNameBase.CMG;
                case E_sMiCompanyNmT.Commonwealth:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.Essent:
                    return MICompanyNameBase.Essent;
                case E_sMiCompanyNmT.FHA:
                    return MICompanyNameBase.Blank;
                case E_sMiCompanyNmT.Genworth:
                    return MICompanyNameBase.Genworth;
                case E_sMiCompanyNmT.LeaveBlank:
                    return MICompanyNameBase.Blank;
                case E_sMiCompanyNmT.MDHousing:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.MGIC:
                    return MICompanyNameBase.MGIC;
                case E_sMiCompanyNmT.MIF:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.NationalMI:
                    return MICompanyNameBase.NationalMI;
                case E_sMiCompanyNmT.PMI:
                    return MICompanyNameBase.PMI;
                case E_sMiCompanyNmT.Radian:
                    return MICompanyNameBase.Radian;
                case E_sMiCompanyNmT.RMIC:
                case E_sMiCompanyNmT.RMICNC:
                    return MICompanyNameBase.RMIC;
                case E_sMiCompanyNmT.SONYMA:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.Triad:
                    return MICompanyNameBase.Triad;
                case E_sMiCompanyNmT.UnitedGuaranty:
                    return MICompanyNameBase.UGI;
                case E_sMiCompanyNmT.USDA:
                case E_sMiCompanyNmT.VA:
                    return MICompanyNameBase.Blank;
                case E_sMiCompanyNmT.Verex:
                case E_sMiCompanyNmT.WiscMtgAssr:
                case E_sMiCompanyNmT.MassHousing:
                    return MICompanyNameBase.Other;
                default:
                    throw new UnhandledEnumException(company);
            }
        }

        /// <summary>
        /// Creates an MI duration enumeration object from the given duration.
        /// </summary>
        /// <param name="duration">The MI duration.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIDurationBase}" /> object.</returns>
        public static MISMOEnum<MIDurationBase> ToMiDurationEnum(E_sProdConvMIOptionT duration)
        {
            var mortgageInsuranceDurationEnum = new MISMOEnum<MIDurationBase>();
            mortgageInsuranceDurationEnum.enumValue = GetMiDurationBaseValue(duration);
            mortgageInsuranceDurationEnum.IsSensitive = false;

            return mortgageInsuranceDurationEnum;
        }

        /// <summary>
        /// Retrieves the MI duration value corresponding to the given duration.
        /// </summary>
        /// <param name="duration">The MI duration.</param>
        /// <returns>The corresponding MI duration enumeration.</returns>
        public static MIDurationBase GetMiDurationBaseValue(E_sProdConvMIOptionT duration)
        {
            switch (duration)
            {
                case E_sProdConvMIOptionT.NoMI:
                case E_sProdConvMIOptionT.Blank:
                    return MIDurationBase.NotApplicable;
                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                    return MIDurationBase.PeriodicMonthly;
                case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                    return MIDurationBase.SplitPremium;
                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                case E_sProdConvMIOptionT.LendPaidSinglePrem:
                    return MIDurationBase.SingleLifeOfLoan;
                default:
                    throw new UnhandledEnumException(duration);
            }
        }

        /// <summary>
        /// Creates a serializable <see cref="MISMOEnum{MilitaryBranchBase}"/> object from the given branch identifier.
        /// </summary>
        /// <param name="militaryBranch">Identifies the military branch.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MilitaryBranchBase}"/> object.</returns>
        public static MISMOEnum<MilitaryBranchBase> ToMilitaryBranchEnum(string militaryBranch)
        {
            if (string.IsNullOrWhiteSpace(militaryBranch))
            {
                return null;
            }

            var militaryBranchEnum = new MISMOEnum<MilitaryBranchBase>();
            militaryBranchEnum.enumValue = GetMilitaryBranchBaseValue(militaryBranch);
            militaryBranchEnum.IsSensitive = false;

            return militaryBranchEnum;
        }

        /// <summary>
        /// Retrieves the <see cref="MilitaryBranchBase"/> value corresponding to the given identifier.
        /// </summary>
        /// <param name="militaryBranch">Identifies the military branch.</param>
        /// <returns>The corresponding <see cref="MilitaryBranchBase"/> value.</returns>
        public static MilitaryBranchBase GetMilitaryBranchBaseValue(string militaryBranch)
        {
            if (string.Equals(militaryBranch, "AirForce", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.AirForce;
            }
            else if (string.Equals(militaryBranch, "Army", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.Army;
            }
            else if (string.Equals(militaryBranch, "Marines", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.Marines;
            }
            else if (string.Equals(militaryBranch, "Navy", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.Navy;
            }
            else if (string.Equals(militaryBranch, "AirNationalGuard", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.AirNationalGuard;
            }
            else if (string.Equals(militaryBranch, "ArmyNationalGuard", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.ArmyNationalGuard;
            }
            else if (string.Equals(militaryBranch, "ArmyReserves", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.ArmyReserves;
            }
            else if (string.Equals(militaryBranch, "CoastGuard", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.CoastGuard;
            }
            else if (string.Equals(militaryBranch, "MarinesReserves", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.MarinesReserves;
            }
            else if (string.Equals(militaryBranch, "NavyReserves", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.NavyReserves;
            }
            else
            {
                return MilitaryBranchBase.Blank;
            }
        }

        /// <summary>
        /// Maps the VA Service Branch type to a MilitaryBranchBase.
        /// </summary>
        /// <param name="serviceBranchType">The VA Service Branch type.</param>
        /// <returns>The matching MilitaryBranchBase.</returns>
        public static MISMOEnum<MilitaryBranchBase> ToMilitaryBranchEnum(E_aVaServiceBranchT serviceBranchType)
        {
            var branchBase = new MISMOEnum<MilitaryBranchBase>();
            branchBase.IsSensitive = false;
            switch (serviceBranchType)
            {
                case E_aVaServiceBranchT.Army:
                    branchBase.enumValue = MilitaryBranchBase.Army;
                    break;
                case E_aVaServiceBranchT.Navy:
                    branchBase.enumValue = MilitaryBranchBase.Navy;
                    break;
                case E_aVaServiceBranchT.AirForce:
                    branchBase.enumValue = MilitaryBranchBase.AirForce;
                    break;
                case E_aVaServiceBranchT.Marine:
                    branchBase.enumValue = MilitaryBranchBase.Marines;
                    break;
                case E_aVaServiceBranchT.CoastGuard:
                    branchBase.enumValue = MilitaryBranchBase.CoastGuard;
                    break;
                case E_aVaServiceBranchT.Other:
                    branchBase.enumValue = MilitaryBranchBase.Other;
                    break;
                case E_aVaServiceBranchT.LeaveBlank:
                    branchBase.enumValue = MilitaryBranchBase.Blank;
                    break;
                default:
                    throw new UnhandledEnumException(serviceBranchType);
            }

            return branchBase;
        }

        /// <summary>
        /// Creates a serializable <see cref="MISMOEnum{MilitaryStatusBase}"/> object with the given data.
        /// </summary>
        /// <param name="onActiveDuty">Indicates whether the borrower is currently on active duty.</param>
        /// <param name="startDate">The start date of the borrower's military service.</param>
        /// <param name="endDate">The end date of the borrower's military service.</param>
        /// <param name="isActiveServiceLine">Indicates whether the data is from an active service or military reserve line.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MilitaryStatusBase}"/> object.</returns>
        public static MISMOEnum<MilitaryStatusBase> ToMilitaryStatusEnum(E_TriState onActiveDuty, string startDate, string endDate, bool isActiveServiceLine)
        {
            var militaryStatusEnum = new MISMOEnum<MilitaryStatusBase>();
            militaryStatusEnum.enumValue = GetMilitaryStatusBaseValue(onActiveDuty, startDate, endDate, isActiveServiceLine);
            militaryStatusEnum.IsSensitive = false;

            return militaryStatusEnum;
        }

        /// <summary>
        /// Retrieves the <see cref="MilitaryStatusBase"/> corresponding to the given application data.
        /// </summary>
        /// <param name="onActiveDuty">Indicates whether the borrower is currently on active duty.</param>
        /// <param name="startDate">The start date of the borrower's military service.</param>
        /// <param name="endDate">The end date of the borrower's military service.</param>
        /// <param name="isActiveServiceLine">Indicates whether the data is from an active service or military reserve line.</param>
        /// <returns>The corresponding <see cref="MilitaryStatusBase"/> value.</returns>
        public static MilitaryStatusBase GetMilitaryStatusBaseValue(E_TriState onActiveDuty, string startDate, string endDate, bool isActiveServiceLine)
        {
            bool startDateIsValid = !string.IsNullOrWhiteSpace(startDate);
            bool endDateIsValid = !string.IsNullOrWhiteSpace(endDate);

            if (startDateIsValid && !endDateIsValid)
            {
                if (isActiveServiceLine)
                {
                    if (onActiveDuty == E_TriState.Yes)
                    {
                        return MilitaryStatusBase.ActiveDuty;
                    }
                }
                else
                {
                    return MilitaryStatusBase.ReserveNationalGuardNotSubjectToActivation;
                }
            }
            else if (startDateIsValid && endDateIsValid)
            {
                return MilitaryStatusBase.Separated;
            }

            return MilitaryStatusBase.Blank;
        }

        /// <summary>
        /// Maps the VA Military Status type to MilitaryStatusBase.
        /// </summary>
        /// <param name="militaryStatusType">The VA Military Status type.</param>
        /// <returns>The MilitaryStatusBase.</returns>
        public static MISMOEnum<MilitaryStatusBase> ToMilitaryStatusEnum(E_aVaMilitaryStatT militaryStatusType)
        {
            var statusBase = new MISMOEnum<MilitaryStatusBase>();
            statusBase.IsSensitive = false;
            switch (militaryStatusType)
            {
                case E_aVaMilitaryStatT.SeparatedFromService:
                    statusBase.enumValue = MilitaryStatusBase.Separated;
                    break;
                case E_aVaMilitaryStatT.InService:
                    statusBase.enumValue = MilitaryStatusBase.ActiveDuty;
                    break;
                case E_aVaMilitaryStatT.LeaveBlank:
                    statusBase.enumValue = MilitaryStatusBase.Blank;
                    break;
                default:
                    throw new UnhandledEnumException(militaryStatusType);
            }

            return statusBase;
        }

        /// <summary>
        /// Creates an MI premium calculation enumeration object from the given percent base type.
        /// </summary>
        /// <param name="type">The percent base type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIPremiumCalculationBase}" /> object.</returns>
        public static MISMOEnum<MIPremiumCalculationBase> ToMIPremiumCalculationEnum(E_PercentBaseT type)
        {
            return ToMIPremiumCalculationEnum(GetMIPremiumCalculationBaseValue(type));
        }

        /// <summary>
        /// Creates an MI premium calculation enumeration object from the given percent base type.
        /// </summary>
        /// <param name="type">The percent base type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIPremiumCalculationBase}" /> object.</returns>
        public static MISMOEnum<MIPremiumCalculationBase> ToMIPremiumCalculationEnum(MIPremiumCalculationBase type)
        {
            if (type == MIPremiumCalculationBase.Blank)
            {
                return null;
            }

            var premiumCalculationEnum = new MISMOEnum<MIPremiumCalculationBase>();
            premiumCalculationEnum.enumValue = type;
            premiumCalculationEnum.IsSensitive = false;

            return premiumCalculationEnum;
        }

        /// <summary>
        /// Retrieves the MI premium calculation type corresponding to the given percent base.
        /// </summary>
        /// <param name="type">The percent base type.</param>
        /// <returns>The corresponding <see cref="MIPremiumCalculationBase" /> value.</returns>
        public static MIPremiumCalculationBase GetMIPremiumCalculationBaseValue(E_PercentBaseT type)
        {
            switch (type)
            {
                case E_PercentBaseT.AppraisalValue:
                case E_PercentBaseT.LoanAmount:
                case E_PercentBaseT.SalesPrice:
                case E_PercentBaseT.TotalLoanAmount:
                    return MIPremiumCalculationBase.Constant;
                case E_PercentBaseT.AverageOutstandingBalance:
                case E_PercentBaseT.DecliningRenewalsAnnually:
                case E_PercentBaseT.DecliningRenewalsMonthly:
                    return MIPremiumCalculationBase.Declining;
                case E_PercentBaseT.AllYSP:
                case E_PercentBaseT.OriginalCost:
                    return MIPremiumCalculationBase.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Creates an enumeration object corresponding to the type of mortgage insurance.
        /// </summary>
        /// <param name="escrowed">True if the mortgage insurance premium is escrowed. Otherwise false.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIPremiumPaymentBase}" /> object.</returns>
        public static MISMOEnum<MIPremiumPaymentBase> ToMIPremiumPaymentEnum(bool escrowed, bool isSensitiveData = false)
        {
            var mipremiumPaymentEnum = new MISMOEnum<MIPremiumPaymentBase>();
            mipremiumPaymentEnum.enumValue = GetMIPremiumPaymentBaseValue(escrowed);
            mipremiumPaymentEnum.IsSensitive = isSensitiveData;

            return mipremiumPaymentEnum;
        }

        /// <summary>
        /// Determines how the mortgage insurance premium payment type based on whether the premiums are escrowed.
        /// </summary>
        /// <param name="escrowed">True if the mortgage insurance premiums are escrowed. Otherwise false.</param>
        /// <returns>A serializable <see cref="MIPremiumPaymentBase" /> object.</returns>
        public static MIPremiumPaymentBase GetMIPremiumPaymentBaseValue(bool escrowed)
        {
            return escrowed ? MIPremiumPaymentBase.Escrowed : MIPremiumPaymentBase.Blank;
        }

        /// <summary>
        /// Creates an enumeration object showing who paid the MI premium.
        /// </summary>
        /// <param name="paidBy">The entity who paid the MI premium.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIPremiumPaymentPaidByBase}" /> object.</returns>
        public static MISMOEnum<MIPremiumPaymentPaidByBase> ToMIPremiumPaymentPaidByEnum(E_sMiInsuranceT paidBy)
        {
            var premiumPaymentPaidByEnum = new MISMOEnum<MIPremiumPaymentPaidByBase>();
            premiumPaymentPaidByEnum.enumValue = GetMIPremiumPaymentPaidByBaseValue(paidBy);
            premiumPaymentPaidByEnum.IsSensitive = false;

            return premiumPaymentPaidByEnum;
        }

        /// <summary>
        /// Retrieves the entity who paid the MI premium.
        /// </summary>
        /// <param name="paidBy">The entity who paid the MI premium.</param>
        /// <returns>The value representing the entity who paid the MI premium.</returns>
        public static MIPremiumPaymentPaidByBase GetMIPremiumPaymentPaidByBaseValue(E_sMiInsuranceT paidBy)
        {
            switch (paidBy)
            {
                case E_sMiInsuranceT.BorrowerPaid:
                    return MIPremiumPaymentPaidByBase.Buyer;
                case E_sMiInsuranceT.LenderPaid:
                    return MIPremiumPaymentPaidByBase.Lender;
                case E_sMiInsuranceT.InvestorPaid:
                case E_sMiInsuranceT.None:
                    return MIPremiumPaymentPaidByBase.Other;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Creates an enumeration object with the given mortgage insurance premium period.
        /// </summary>
        /// <param name="period">The premium period type.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIPremiumPeriodBase}" /> object.</returns>
        public static MISMOEnum<MIPremiumPeriodBase> ToMIPremiumPeriodEnum(MIPremiumPeriodBase period, bool isSensitiveData = false)
        {
            var mipremiumPeriodEnum = new MISMOEnum<MIPremiumPeriodBase>();
            mipremiumPeriodEnum.enumValue = period;
            mipremiumPeriodEnum.IsSensitive = isSensitiveData;

            return mipremiumPeriodEnum;
        }

        /// <summary>
        /// Creates an enumeration object with the refund-ability of the mortgage insurance premium.
        /// </summary>
        /// <param name="refundable">True if the premium is refundable. Otherwise false.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIPremiumRefundableBase}" /> object.</returns>
        public static MISMOEnum<MIPremiumRefundableBase> ToMIPremiumRefundableEnum(bool refundable, bool isSensitiveData = false)
        {
            var mipremiumRefundableEnum = new MISMOEnum<MIPremiumRefundableBase>();
            mipremiumRefundableEnum.enumValue = GetMIPremiumRefundableBaseValue(refundable);
            mipremiumRefundableEnum.IsSensitive = isSensitiveData;

            return mipremiumRefundableEnum;
        }

        /// <summary>
        /// Determines the MI premium refundable base value from the given indicator.
        /// </summary>
        /// <param name="refundable">True if the premium is refundable. Otherwise false.</param>
        /// <returns>The corresponding MI premium refundable base value.</returns>
        public static MIPremiumRefundableBase GetMIPremiumRefundableBaseValue(bool refundable)
        {
            return refundable ? MIPremiumRefundableBase.Refundable : MIPremiumRefundableBase.NotRefundable;
        }

        /// <summary>
        /// Creates an enumeration object with the given mortgage insurance premiums sequence.
        /// </summary>
        /// <param name="premiumSequence">The sequence of the premium among the list of premiums.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIPremiumSequenceBase}" /> object.</returns>
        public static MISMOEnum<MIPremiumSequenceBase> ToMIPremiumSequenceEnum(MIPremiumSequenceBase premiumSequence, bool isSensitiveData = false)
        {
            var mipremiumSequenceEnum = new MISMOEnum<MIPremiumSequenceBase>();
            mipremiumSequenceEnum.enumValue = premiumSequence;
            mipremiumSequenceEnum.IsSensitive = isSensitiveData;

            return mipremiumSequenceEnum;
        }

        /// <summary>
        /// Creates an enumeration object with the source of the mortgage insurance payment, based on the given insurance type.
        /// </summary>
        /// <param name="mitype">The type of mortgage insurance, e.g. lender-paid or borrower-paid.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MIPremiumSourceBase}" /> object.</returns>
        public static MISMOEnum<MIPremiumSourceBase> ToMIPremiumSourceEnum(E_sMiInsuranceT mitype, bool isSensitiveData = false)
        {
            var mipremiumSourceEnum = new MISMOEnum<MIPremiumSourceBase>();
            mipremiumSourceEnum.enumValue = GetMIPremiumSourceBaseValue(mitype);
            mipremiumSourceEnum.IsSensitive = isSensitiveData;

            return mipremiumSourceEnum;
        }

        /// <summary>
        /// Determines the mortgage insurance premium source from the given mortgage insurance type.
        /// </summary>
        /// <param name="mitype">The type of mortgage insurance, e.g. lender-paid or borrower-paid.</param>
        /// <returns>The corresponding MIPremiumSourceBase value.</returns>
        public static MIPremiumSourceBase GetMIPremiumSourceBaseValue(E_sMiInsuranceT mitype)
        {
            switch (mitype)
            {
                case E_sMiInsuranceT.BorrowerPaid:
                    return MIPremiumSourceBase.Borrower;
                case E_sMiInsuranceT.InvestorPaid:
                    return MIPremiumSourceBase.Other;
                case E_sMiInsuranceT.LenderPaid:
                    return MIPremiumSourceBase.Lender;
                case E_sMiInsuranceT.None:
                    return MIPremiumSourceBase.Blank;
                default:
                    throw new UnhandledEnumException(mitype);
            }
        }

        /// <summary>
        /// Creates an enumeration object with the type of mortgage insurance provider, e.g. FHA | PMI.
        /// </summary>
        /// <param name="loanType">The loan type, e.g. FHA | Conventional.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MISourceBase}" /> object.</returns>
        public static MISMOEnum<MISourceBase> ToMISourceEnum(E_sLT loanType, bool isSensitiveData = false)
        {
            var misourceEnum = new MISMOEnum<MISourceBase>();
            misourceEnum.enumValue = GetMISourceBaseValue(loanType);
            misourceEnum.IsSensitive = isSensitiveData;

            return misourceEnum;
        }

        /// <summary>
        /// Determines the MISourceBase value from the given loan type.
        /// </summary>
        /// <param name="loanType">The loan type, e.g. FHA | Conventional.</param>
        /// <returns>The corresponding MISourceBase value.</returns>
        public static MISourceBase GetMISourceBaseValue(E_sLT loanType)
        {
            switch (loanType)
            {
                case E_sLT.Conventional:
                    return MISourceBase.PMI;
                case E_sLT.FHA:
                    return MISourceBase.FHA;
                case E_sLT.Other:
                    return MISourceBase.PMI;
                case E_sLT.UsdaRural:
                    return MISourceBase.USDA;
                case E_sLT.VA:
                    return MISourceBase.Blank;
                default:
                    throw new UnhandledEnumException(loanType);
            }
        }

        /// <summary>
        /// Creates a serializable enumeration object with the government mortgage program.
        /// </summary>
        /// <param name="loanType">The LQB loan type value.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MortgageProgramBase}"/> object.</returns>
        public static MISMOEnum<MortgageProgramBase> ToMortgageProgramEnum(E_sLT loanType, bool displaySensitive = true)
        {
            var mortgageProgramTypeEnum = new MISMOEnum<MortgageProgramBase>();
            mortgageProgramTypeEnum.enumValue = GetMortgageProgramBaseValue(loanType);
            mortgageProgramTypeEnum.IsSensitive = false;
            mortgageProgramTypeEnum.DisplaySensitive = displaySensitive;

            if (mortgageProgramTypeEnum.enumValue == MortgageProgramBase.Blank)
            {
                return null;
            }

            return mortgageProgramTypeEnum;
        }

        /// <summary>
        /// Determines the MortgageProgramBase value (for Ginnie Mae) from the loan type.
        /// </summary>
        /// <param name="loanType">The LQB loan type value.</param>
        /// <returns>The corresponding MortgageProgramBase value.</returns>
        public static MortgageProgramBase GetMortgageProgramBaseValue(E_sLT loanType)
        {
            switch (loanType)
            {
                case E_sLT.FHA:
                    return MortgageProgramBase.FHASingleFamily;
                case E_sLT.VA:
                    return MortgageProgramBase.VAGuaranteedInsured;
                case E_sLT.UsdaRural:
                    return MortgageProgramBase.SingleFamilyRHS;
                default:
                    throw new UnhandledEnumException(loanType);
            }
        }

        /// <summary>
        /// Maps the prior loan type to mortgage enum.
        /// </summary>
        /// <param name="priorLoanType">The prior loan type.</param>
        /// <returns>The MortgageBase enum.</returns>
        public static MISMOEnum<MortgageBase> ToMortgageEnum(E_sVaPriorLoanT priorLoanType)
        {
            MISMOEnum<MortgageBase> mortgageEnum = new MISMOEnum<MortgageBase>();
            mortgageEnum.IsSensitive = false;
            switch (priorLoanType)
            {
                case E_sVaPriorLoanT.FhaFixed:
                case E_sVaPriorLoanT.FhaArm:
                    mortgageEnum.enumValue = MortgageBase.FHA;
                    break;
                case E_sVaPriorLoanT.ConventionalFixed:
                case E_sVaPriorLoanT.ConventionalArm:
                case E_sVaPriorLoanT.ConventionalInterestOnly:
                    mortgageEnum.enumValue = MortgageBase.Conventional;
                    break;
                case E_sVaPriorLoanT.VaFixed:
                case E_sVaPriorLoanT.VaArm:
                    mortgageEnum.enumValue = MortgageBase.VA;
                    break;
                case E_sVaPriorLoanT.Other:
                    mortgageEnum.enumValue = MortgageBase.Other;
                    break;
                case E_sVaPriorLoanT.LeaveBlank:
                    mortgageEnum.enumValue = MortgageBase.Blank;
                    break;
                default:
                    throw new UnhandledEnumException(priorLoanType);
            }

            return mortgageEnum;
        }

        /// <summary>
        /// Converts an LQB value to a serializable <see cref="MISMOEnum{MortgageBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{MortgageBase}" /> object.</returns>
        public static MISMOEnum<MortgageBase> ToMortgageEnum(E_sLT lqbValue, bool displaySensitive = true)
        {
            var mortgageEnum = new MISMOEnum<MortgageBase>();
            mortgageEnum.enumValue = GetMortgageBaseValue(lqbValue);
            mortgageEnum.IsSensitive = false;
            mortgageEnum.DisplaySensitive = displaySensitive;

            return mortgageEnum;
        }

        /// <summary>
        /// Retrieves the MortgageBase value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding MortgageBase value.</returns>
        public static MortgageBase GetMortgageBaseValue(E_sLT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sLT.Conventional:
                    return MortgageBase.Conventional;
                case E_sLT.FHA:
                    return MortgageBase.FHA;
                case E_sLT.Other:
                    return MortgageBase.Other;
                case E_sLT.UsdaRural:
                    return MortgageBase.USDARuralDevelopment;
                case E_sLT.VA:
                    return MortgageBase.VA;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates a serializable MISMO enum containing a mortgage type.
        /// </summary>
        /// <param name="loanType">The loan type.</param>
        /// <returns>A serializable MISMO enum with the corresponding value.</returns>
        public static MISMOEnum<MortgageBase> ToMortgageEnum(sHmdaLoanTypeT loanType)
        {
            if (loanType == sHmdaLoanTypeT.LeaveBlank)
            {
                return null;
            }

            var mortgageEnum = new MISMOEnum<MortgageBase>();
            mortgageEnum.enumValue = GetMortgageBaseValue(loanType);
            mortgageEnum.IsSensitive = false;

            return mortgageEnum;
        }

        /// <summary>
        /// Gets the MISMO mortgage type value corresponding to the loan type.
        /// </summary>
        /// <param name="loanType">The loan type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static MortgageBase GetMortgageBaseValue(sHmdaLoanTypeT loanType)
        {
            switch (loanType)
            {
                case sHmdaLoanTypeT.Conventional:
                    return MortgageBase.Conventional;
                case sHmdaLoanTypeT.FHA:
                    return MortgageBase.FHA;
                case sHmdaLoanTypeT.USDA:
                    return MortgageBase.USDARuralDevelopment;
                case sHmdaLoanTypeT.VA:
                    return MortgageBase.VA;
                default:
                    throw new UnhandledEnumException(loanType);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{NFIPCommunityParticipationStatusBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{NFIPCommunityParticipationStatusBase}" /> object.</returns>
        public static MISMOEnum<NFIPCommunityParticipationStatusBase> ToNFIPCommunityParticipationStatusEnum(string lqbValue)
        {
            var nfipCommunityParticipationStatusEnum = new MISMOEnum<NFIPCommunityParticipationStatusBase>();
            nfipCommunityParticipationStatusEnum.enumValue = GetNFIPCommunityParticipationStatusBaseValue(lqbValue);
            nfipCommunityParticipationStatusEnum.IsSensitive = false;

            return nfipCommunityParticipationStatusEnum;
        }

        /// <summary>
        /// Retrieves the NFIPCommunityParticipationStatus value corresponding to a given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding NFIPCommunityParticipationStatus value.</returns>
        public static NFIPCommunityParticipationStatusBase GetNFIPCommunityParticipationStatusBaseValue(string lqbValue)
        {
            if (string.Equals("E", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.Emergency;
            }
            else if (string.Equals("N", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.NonParticipating;
            }
            else if (string.Equals("P", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.Probation;
            }
            else if (string.Equals("R", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.Regular;
            }
            else if (string.Equals("S", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.Suspended;
            }
            else
            {
                return NFIPCommunityParticipationStatusBase.Unknown;
            }
        }

        /// <summary>
        /// Creates a serializable <see cref="MISMOEnum{ObjectEncodingBase}"/>
        /// indicating the encoding used by an object.
        /// </summary>
        /// <param name="encoding">The encoding type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ObjectEncodingBase}"/> object.</returns>
        public static MISMOEnum<ObjectEncodingBase> ToObjectEncodingEnum(ObjectEncodingBase encoding)
        {
            var characterEncodingSetBase = new MISMOEnum<ObjectEncodingBase>();
            characterEncodingSetBase.enumValue = encoding;
            characterEncodingSetBase.IsSensitive = false;

            return characterEncodingSetBase;
        }

        /// <summary>
        /// Creates an owned property disposition status enumeration object based on the given property status.
        /// </summary>
        /// <param name="status">The owned property status.</param>
        /// <returns>A serializable <see cref="MISMOEnum{OwnedPropertyDispositionStatusBase}" /> object.</returns>
        public static MISMOEnum<OwnedPropertyDispositionStatusBase> ToOwnedPropertyDispositionStatusEnum(E_ReoStatusT status)
        {
            switch (status)
            {
                case E_ReoStatusT.PendingSale:
                    return ToOwnedPropertyDispositionStatusEnum(OwnedPropertyDispositionStatusBase.PendingSale);
                case E_ReoStatusT.Rental:
                    return ToOwnedPropertyDispositionStatusEnum(OwnedPropertyDispositionStatusBase.RetainForRental);
                case E_ReoStatusT.Residence:
                    return ToOwnedPropertyDispositionStatusEnum(OwnedPropertyDispositionStatusBase.RetainForPrimaryOrSecondaryResidence);
                case E_ReoStatusT.Sale:
                    return ToOwnedPropertyDispositionStatusEnum(OwnedPropertyDispositionStatusBase.Sold);
                default:
                    throw new UnhandledEnumException(status);
            }
        }

        /// <summary>
        /// Creates an owned property disposition status enumeration object based on the given OwnedPropertyDispositionStatusBase enumeration.
        /// </summary>
        /// <param name="ownedPropertyDispositionBase">The OwnedPropertyDispositionStatusBase enumeration.</param>
        /// <returns>A serializable <see cref="MISMOEnum{OwnedPropertyDispositionStatusBase}" /> object.</returns>
        public static MISMOEnum<OwnedPropertyDispositionStatusBase> ToOwnedPropertyDispositionStatusEnum(OwnedPropertyDispositionStatusBase ownedPropertyDispositionBase)
        {
            var ownedPropertyDispositionStatusEnum = new MISMOEnum<OwnedPropertyDispositionStatusBase>();
            ownedPropertyDispositionStatusEnum.enumValue = ownedPropertyDispositionBase;
            ownedPropertyDispositionStatusEnum.IsSensitive = false;

            return ownedPropertyDispositionStatusEnum;
        }

        /// <summary>
        /// Creates a serializable <see cref="MISMOEnum{PoolClassBase}" /> object from the given issue type code string.
        /// </summary>
        /// <param name="issueTypeCode">The issue type code from the mortgage pool.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable Pool Class enumeration.</returns>
        public static MISMOEnum<PoolClassBase> ToPoolClassEnum(string issueTypeCode, bool displaySensitive = true)
        {
            PoolClassBase poolClass;

            if (issueTypeCode.Equals("X", StringComparison.OrdinalIgnoreCase))
            {
                poolClass = PoolClassBase.GNMAI;
            }
            else if (issueTypeCode.Equals("C", StringComparison.OrdinalIgnoreCase))
            {
                poolClass = PoolClassBase.GNMAII;
            }
            else if (issueTypeCode.Equals("M", StringComparison.OrdinalIgnoreCase))
            {
                poolClass = PoolClassBase.GNMAII;
            }
            else
            {
                poolClass = PoolClassBase.Blank;
            }

            return ToMismoEnum(poolClass, displaySensitive: displaySensitive);
        }

        /// <summary>
        /// Creates a pooling method enumeration object based on the LQB MortgagePoolAmortMeth enumeration.
        /// </summary>
        /// <param name="poolAmortizationMethod">The LQB enum representation of the pool amortization method.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PoolingMethodBase}"/> object.</returns>
        public static MISMOEnum<PoolingMethodBase> ToPoolingMethodEnum(E_MortgagePoolAmortMethT poolAmortizationMethod, bool displaySensitive = true)
        {
            PoolingMethodBase poolingMethod;
            switch (poolAmortizationMethod)
            {
                case E_MortgagePoolAmortMethT.ConcurrentDate:
                    poolingMethod = PoolingMethodBase.ConcurrentDate;
                    break;
                case E_MortgagePoolAmortMethT.InternalReserve:
                    poolingMethod = PoolingMethodBase.InternalReserve;
                    break;
                default:
                    throw new UnhandledEnumException(poolAmortizationMethod);
            }

            return ToMismoEnum(poolingMethod, displaySensitive: displaySensitive);
        }

        /// <summary>
        /// Converts an LQB pool amortization method value into a pooling method base value enumeration.
        /// </summary>
        /// <param name="poolAmortizationMethod">The LQB pool amortization value.</param>
        /// <returns>The corresponding <see cref="PoolingMethodBase"/> value.</returns>
        public static PoolingMethodBase ToPoolingMethodBaseValue(E_MortgagePoolAmortMethT poolAmortizationMethod)
        {
            switch (poolAmortizationMethod)
            {
                case E_MortgagePoolAmortMethT.ConcurrentDate:
                    return PoolingMethodBase.ConcurrentDate;
                case E_MortgagePoolAmortMethT.InternalReserve:
                    return PoolingMethodBase.InternalReserve;
                default:
                    throw new UnhandledEnumException(poolAmortizationMethod);
            }
        }
        
        /// <summary>
        /// Creates a serializable <see cref="MISMOEnum{PoolStructureBase}" /> object from the given issue type code string.
        /// </summary>
        /// <param name="issueTypeCode">The issue type code from the mortgage pool.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable Pool Structure enumeration.</returns>
        public static MISMOEnum<PoolStructureBase> ToPoolStructureEnum(string issueTypeCode, bool displaySensitive = true)
        {
            PoolStructureBase poolStructure;
            if (issueTypeCode.Equals("X", StringComparison.OrdinalIgnoreCase))
            {
                poolStructure = PoolStructureBase.SingleIssuer;
            }
            else if (issueTypeCode.Equals("C", StringComparison.OrdinalIgnoreCase))
            {
                poolStructure = PoolStructureBase.MultipleIssuer;
            }
            else if (issueTypeCode.Equals("M", StringComparison.OrdinalIgnoreCase))
            {
                poolStructure = PoolStructureBase.MultipleIssuer;
            }
            else
            {
                poolStructure = PoolStructureBase.Blank;
            }

            return ToMismoEnum(poolStructure, displaySensitive: displaySensitive);
        }

        /// <summary>
        /// Converts the issue type code string into a Pool Structure enumeration for Ginnie Mae's PDD.
        /// </summary>
        /// <param name="issueTypeCode">The issue type code from the mortgage pool.</param>
        /// <returns>The corresponding serializable pool structure enumeration.</returns>
        public static PoolStructureBase ToPoolStructureBaseValue(string issueTypeCode)
        {
            if (issueTypeCode.Equals("X", StringComparison.OrdinalIgnoreCase))
            {
                return PoolStructureBase.SingleIssuer;
            }
            else if (issueTypeCode.Equals("C", StringComparison.OrdinalIgnoreCase))
            {
                return PoolStructureBase.MultipleIssuer;
            }
            else if (issueTypeCode.Equals("M", StringComparison.OrdinalIgnoreCase))
            {
                return PoolStructureBase.MultipleIssuer;
            }
            else
            {
                return PoolStructureBase.Blank;
            }
        }

        /// <summary>
        /// Creates a preferred response format enumeration object based on the given PreferredResponseFormatBase enumeration.
        /// </summary>
        /// <param name="preferredResponseFormatBase">The PreferredResponseFormatBase enumeration.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PreferredResponseFormatBase}" /> object.</returns>
        public static MISMOEnum<PreferredResponseFormatBase> ToPreferredResponseFormatEnum(PreferredResponseFormatBase preferredResponseFormatBase)
        {
            var preferredResponseFormatEnum = new MISMOEnum<PreferredResponseFormatBase>();
            preferredResponseFormatEnum.enumValue = preferredResponseFormatBase;
            preferredResponseFormatEnum.IsSensitive = false;

            return preferredResponseFormatEnum;
        }

        /// <summary>
        /// Creates a preferred response method enumeration object based on the given PreferredResponseMethodBase enumeration.
        /// </summary>
        /// <param name="preferredResponseMethodBase">The PreferredResponseMethodBase enumeration.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PreferredResponseMethodBase}" /> object.</returns>
        public static MISMOEnum<PreferredResponseMethodBase> ToPreferredResponseMethodEnum(PreferredResponseMethodBase preferredResponseMethodBase)
        {
            var preferredResponseMethodEnum = new MISMOEnum<PreferredResponseMethodBase>();
            preferredResponseMethodEnum.enumValue = preferredResponseMethodBase;
            preferredResponseMethodEnum.IsSensitive = false;

            return preferredResponseMethodEnum;
        }

        /// <summary>
        /// Creates an object indicating the type of a prepaid fee.
        /// </summary>
        /// <param name="defaultSystemClosingCostFeeType">The <see cref="DefaultSystemClosingCostFee" /> type of the prepaid fee.</param>
        /// <param name="taxType">The tax type associated with the prepaid. Pass the "leave blank" type if the prepaid is not a tax.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PrepaidItemBase}" /> object.</returns>
        public static MISMOEnum<PrepaidItemBase> ToPrepaidItemEnum(Guid defaultSystemClosingCostFeeType, E_TaxTableTaxT taxType, string displayLabelText, bool isSensitiveData = false)
        {
            var prepaidItemEnum = new MISMOEnum<PrepaidItemBase>();
            prepaidItemEnum.enumValue = GetPrepaidItemBaseValue(defaultSystemClosingCostFeeType, taxType);
            prepaidItemEnum.IsSensitive = isSensitiveData;
            prepaidItemEnum.DisplayLabelText = displayLabelText;

            return prepaidItemEnum;
        }

        /// <summary>
        /// Retrieves the MISMO prepaid item type corresponding to the closing cost type.
        /// </summary>
        /// <param name="defaultSystemClosingCostFeeType">The <see cref="DefaultSystemClosingCostFee" /> type of the prepaid fee.</param>
        /// <param name="taxType">The tax type associated with the prepaid. Pass the "leave blank" type if the prepaid is not a tax.</param>
        /// <returns>The corresponding <see cref="PrepaidItemBase" /> type.</returns>
        public static PrepaidItemBase GetPrepaidItemBaseValue(Guid defaultSystemClosingCostFeeType, E_TaxTableTaxT taxType)
        {
            if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId)
            {
                return PrepaidItemBase.HomeownersInsurancePremium;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
            {
                return PrepaidItemBase.PrepaidInterest;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId)
            {
                return PrepaidItemBase.FloodInsurancePremium;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId)
            {
                return PrepaidItemBase.Other;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId)
            {
                return PrepaidItemBase.HazardInsurancePremium;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId)
            {
                return PrepaidItemBase.HomeownersAssociationDues;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
            {
                return PrepaidItemBase.MortgageInsurancePremium;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId)
            {
                switch (taxType)
                {
                    case E_TaxTableTaxT.Borough:
                        return PrepaidItemBase.BoroughPropertyTax;
                    case E_TaxTableTaxT.City:
                        return PrepaidItemBase.CityPropertyTax;
                    case E_TaxTableTaxT.County:
                        return PrepaidItemBase.CountyPropertyTax;
                    case E_TaxTableTaxT.FireDist:
                        return PrepaidItemBase.DistrictPropertyTax;
                    case E_TaxTableTaxT.LeaveBlank:
                        return PrepaidItemBase.Other;
                    case E_TaxTableTaxT.LocalImprovementDist:
                        return PrepaidItemBase.DistrictPropertyTax;
                    case E_TaxTableTaxT.Miscellaneous:
                        return PrepaidItemBase.Other;
                    case E_TaxTableTaxT.MunicipalUtilDist:
                    case E_TaxTableTaxT.School:
                    case E_TaxTableTaxT.SpecialAssessmentDist:
                        return PrepaidItemBase.DistrictPropertyTax;
                    case E_TaxTableTaxT.Town:
                        return PrepaidItemBase.TownPropertyTax;
                    case E_TaxTableTaxT.Township:
                    case E_TaxTableTaxT.Utility:
                    case E_TaxTableTaxT.Village:
                        return PrepaidItemBase.Other;
                    case E_TaxTableTaxT.WasteFeeDist:
                        return PrepaidItemBase.DistrictPropertyTax;
                    case E_TaxTableTaxT.Water_Irrigation:
                    case E_TaxTableTaxT.Water_Sewer:
                        return PrepaidItemBase.Other;
                    default:
                        throw new UnhandledEnumException(taxType);
                }
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId)
            {
                return PrepaidItemBase.WindAndStormInsurancePremium;
            }

            return PrepaidItemBase.Other;
        }

        /// <summary>
        /// Creates an object indicating who paid the prepaid fee payment.
        /// </summary>
        /// <param name="paidBy">The entity who paid the prepaid fee payment.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PrepaidItemPaymentPaidByBase}" /> object.</returns>
        public static MISMOEnum<PrepaidItemPaymentPaidByBase> ToPrepaidItemPaymentPaidByEnum(E_ClosingCostFeePaymentPaidByT paidBy, bool isSensitiveData = false)
        {
            if (paidBy == E_ClosingCostFeePaymentPaidByT.LeaveBlank)
            {
                return null;
            }

            var paidByEnum = new MISMOEnum<PrepaidItemPaymentPaidByBase>();
            paidByEnum.enumValue = GetPrepaidItemPaymentPaidByBaseValue(paidBy);
            paidByEnum.IsSensitive = isSensitiveData;

            return paidByEnum;
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid the prepaid fee payment.
        /// </summary>
        /// <param name="paidBy">The entity who paid the prepaid fee payment.</param>
        /// <returns>The corresponding <see cref="PrepaidItemPaymentPaidByBase" /> value.</returns>
        public static PrepaidItemPaymentPaidByBase GetPrepaidItemPaymentPaidByBaseValue(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return PrepaidItemPaymentPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return PrepaidItemPaymentPaidByBase.Broker;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return PrepaidItemPaymentPaidByBase.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return PrepaidItemPaymentPaidByBase.Seller;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return PrepaidItemPaymentPaidByBase.ThirdParty;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Creates a prepaid item payment timing object from the given payment time.
        /// </summary>
        /// <param name="paymentTime">The payment time of a prepaid item.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PrepaidItemPaymentTimingBase}" /> object.</returns>
        public static MISMOEnum<PrepaidItemPaymentTimingBase> ToPrepaidItemPaymentTimingEnum(E_GfeClosingCostFeePaymentTimingT paymentTime)
        {
            var prepaidItemPaymentTimingEnum = new MISMOEnum<PrepaidItemPaymentTimingBase>();
            prepaidItemPaymentTimingEnum.enumValue = GetPrepaidItemPaymentTimingBaseValue(paymentTime);
            prepaidItemPaymentTimingEnum.IsSensitive = false;

            if (prepaidItemPaymentTimingEnum.enumValue == PrepaidItemPaymentTimingBase.Blank)
            {
                return null;
            }

            return prepaidItemPaymentTimingEnum;
        }

        /// <summary>
        /// Determines the prepaid item payment timing based on the given closing cost fee payment timing.
        /// </summary>
        /// <param name="paymentTime">The closing cost fee payment timing, e.g. at closing | outside of closing.</param>
        /// <returns>The prepaid item payment timing based on the given closing cost fee payment timing.</returns>
        public static PrepaidItemPaymentTimingBase GetPrepaidItemPaymentTimingBaseValue(E_GfeClosingCostFeePaymentTimingT paymentTime)
        {
            switch (paymentTime)
            {
                case E_GfeClosingCostFeePaymentTimingT.AtClosing:
                    return PrepaidItemPaymentTimingBase.AtClosing;
                case E_GfeClosingCostFeePaymentTimingT.LeaveBlank:
                    return PrepaidItemPaymentTimingBase.Blank;
                case E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing:
                    return PrepaidItemPaymentTimingBase.BeforeClosing;
                default:
                    throw new UnhandledEnumException(paymentTime);
            }
        }

        /// <summary>
        /// Creates a per diem calculation method object from the given days in the calculation year.
        /// </summary>
        /// <param name="daysInYear">The days in the calculation year: should be 360 or 365.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PrepaidItemPerDiemCalculationMethodBase}" /> object.</returns>
        public static MISMOEnum<PrepaidItemPerDiemCalculationMethodBase> ToPrepaidItemPerDiemCalculationMethodEnum(int daysInYear)
        {
            var prepaidItemPerDiemCalculationMethodEnum = new MISMOEnum<PrepaidItemPerDiemCalculationMethodBase>();
            prepaidItemPerDiemCalculationMethodEnum.enumValue = GetPrepaidItemPerDiemCalculationMethodBaseValue(daysInYear);
            prepaidItemPerDiemCalculationMethodEnum.IsSensitive = false;

            return prepaidItemPerDiemCalculationMethodEnum;
        }

        /// <summary>
        /// Retrieves the per diem calculation method object corresponding to the given days in the calculation year.
        /// </summary>
        /// <param name="daysInYear">The days in the calculation year: should be 360 or 365.</param>
        /// <returns>The corresponding value.</returns>
        public static PrepaidItemPerDiemCalculationMethodBase GetPrepaidItemPerDiemCalculationMethodBaseValue(int daysInYear)
        {
            if (daysInYear == 360)
            {
                return PrepaidItemPerDiemCalculationMethodBase.Item360;
            }
            else if (daysInYear == 365)
            {
                return PrepaidItemPerDiemCalculationMethodBase.Item365;
            }

            return PrepaidItemPerDiemCalculationMethodBase.Other;
        }

        /// <summary>
        /// Creates a parcel identification enumeration object from the given identifier type.
        /// </summary>
        /// <param name="parcelIdentifier">The identifier type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ParcelIdentificationBase}" /> object.</returns>
        public static MISMOEnum<ParcelIdentificationBase> ToParcelIdentificationEnum(ParcelIdentificationBase parcelIdentifier)
        {
            var parcelIdentificationEnum = new MISMOEnum<ParcelIdentificationBase>();
            parcelIdentificationEnum.enumValue = parcelIdentifier;
            parcelIdentificationEnum.IsSensitive = false;

            return parcelIdentificationEnum;
        }

        /// <summary>
        /// Creates a partial payment application method enumeration object from the given indicator.
        /// </summary>
        /// <param name="partialPaymentsAccepted">Indicates whether a partial payment is accepted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PartialPaymentApplicationMethodBase}"/> object.</returns>
        public static MISMOEnum<PartialPaymentApplicationMethodBase> ToPartialPaymentApplicationMethodEnum(E_sTRIDPartialPaymentAcceptanceT partialPaymentsAccepted)
        {
            if (partialPaymentsAccepted == E_sTRIDPartialPaymentAcceptanceT.NotAccepted)
            {
                return null;
            }

            var partialPaymentApplicationMethodEnum = new MISMOEnum<PartialPaymentApplicationMethodBase>();
            partialPaymentApplicationMethodEnum.enumValue = GetPartialPaymentApplicationMethodBaseValue(partialPaymentsAccepted);
            partialPaymentApplicationMethodEnum.IsSensitive = false;

            return partialPaymentApplicationMethodEnum;
        }

        /// <summary>
        /// Retrieves the partial payment application method corresponding to the given method.
        /// </summary>
        /// <param name="partialPaymentsAccepted">The partial payment application method as represented in LendingQB.</param>
        /// <returns>THe corresponding MISMO partial payment application method.</returns>
        public static PartialPaymentApplicationMethodBase GetPartialPaymentApplicationMethodBaseValue(E_sTRIDPartialPaymentAcceptanceT partialPaymentsAccepted)
        {
            switch (partialPaymentsAccepted)
            {
                case E_sTRIDPartialPaymentAcceptanceT.AcceptedAndApplied:
                    return PartialPaymentApplicationMethodBase.ApplyPartialPayment;
                case E_sTRIDPartialPaymentAcceptanceT.AcceptedButNotApplied:
                    return PartialPaymentApplicationMethodBase.HoldUntilCompleteAmount;
                default:
                    throw new UnhandledEnumException(partialPaymentsAccepted);
            }
        }

        /// <summary>
        /// Creates a party role enumeration object from the given role.
        /// </summary>
        /// <param name="role">The role of the party.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PartyRoleBase}" /> object.</returns>
        public static MISMOEnum<PartyRoleBase> ToPartyRoleEnum(PartyRoleBase role, bool displaySensitive = true)
        {
            var partyRoleEnum = new MISMOEnum<PartyRoleBase>();
            partyRoleEnum.enumValue = role;
            partyRoleEnum.IsSensitive = false;
            partyRoleEnum.DisplaySensitive = displaySensitive;

            return partyRoleEnum;
        }

        /// <summary>
        /// Retrieves the party role type corresponding to the borrower type.
        /// </summary>
        /// <param name="borrowerType">The type of a borrower on an application.</param>
        /// <returns>The <see cref="PartyRoleBase" /> value corresponding to the borrower type.</returns>
        public static PartyRoleBase GetPartyRoleBaseValue(E_aTypeT borrowerType)
        {
            switch (borrowerType)
            {
                case E_aTypeT.CoSigner:
                    return PartyRoleBase.Cosigner;
                case E_aTypeT.Individual:
                    return PartyRoleBase.Borrower;
                case E_aTypeT.NonTitleSpouse:
                    return PartyRoleBase.NonTitleSpouse;
                case E_aTypeT.TitleOnly:
                    return PartyRoleBase.TitleHolder;
                default:
                    throw new UnhandledEnumException(borrowerType);
            }
        }

        /// <summary>
        /// Retrieves the party role type corresponding to the given agent role.
        /// </summary>
        /// <param name="agentRole">The agent role to be mapped to MISMO.</param>
        /// <param name="agentRoleDescription">The description of the agent's role if agentRole is set to Other.</param>
        /// <param name="entityType">Indicates whether the agent is an individual or legal entity.</param>
        /// <param name="tpoTransaction">Indicates whether the loan is originated by a third-party.</param>
        /// <param name="branchChannel">The branch channel of the loan.</param>
        /// <param name="otherDescription">Will be populated with the party role if the agent does not map directly.</param>
        /// <returns>The <see cref="PartyRoleBase" /> value corresponding to the given agent role.</returns>
        public static PartyRoleBase GetPartyRoleBaseValue(E_AgentRoleT agentRole, string agentRoleDescription, EntityType entityType, bool tpoTransaction, E_BranchChannelT branchChannel, out string otherDescription)
        {
            otherDescription = string.Empty;

            switch (agentRole)
            {
                case E_AgentRoleT.Appraiser:
                    return PartyRoleBase.Appraiser;
                case E_AgentRoleT.Broker:
                case E_AgentRoleT.BrokerRep:
                    return PartyRoleBase.MortgageBroker;
                case E_AgentRoleT.BrokerProcessor:
                    if (branchChannel == E_BranchChannelT.Wholesale)
                    {
                        otherDescription = "BrokerProcessor";
                        return PartyRoleBase.Other;
                    }

                    return PartyRoleBase.LoanProcessor;

                case E_AgentRoleT.Builder:
                    return PartyRoleBase.Builder;
                case E_AgentRoleT.BuyerAgent:
                    return PartyRoleBase.RealEstateAgent;
                case E_AgentRoleT.BuyerAttorney:
                    return PartyRoleBase.Attorney;
                case E_AgentRoleT.ClosingAgent:
                case E_AgentRoleT.Escrow:
                    return PartyRoleBase.ClosingAgent;
                case E_AgentRoleT.FloodProvider:
                    return PartyRoleBase.FloodCertificateProvider;
                case E_AgentRoleT.HazardInsurance:
                    return entityType == EntityType.Individual ? PartyRoleBase.HazardInsuranceAgent : PartyRoleBase.HazardInsuranceCompany;
                case E_AgentRoleT.HomeOwnerAssociation:
                    return PartyRoleBase.HomeownersAssociation;
                case E_AgentRoleT.HomeOwnerInsurance:
                    return entityType == EntityType.Individual ? PartyRoleBase.HazardInsuranceAgent : PartyRoleBase.HazardInsuranceCompany;
                case E_AgentRoleT.Investor:
                    return PartyRoleBase.Investor;
                case E_AgentRoleT.Lender:
                    return PartyRoleBase.Lender;
                case E_AgentRoleT.ListingAgent:
                    return PartyRoleBase.RealEstateAgent;
                case E_AgentRoleT.LoanOfficer:
                    return tpoTransaction ? PartyRoleBase.MortgageBroker : PartyRoleBase.LoanOfficer;
                case E_AgentRoleT.LoanPurchasePayee:
                    return PartyRoleBase.Payee;
                case E_AgentRoleT.MortgageInsurance:
                    return PartyRoleBase.MICompany;
                case E_AgentRoleT.Processor:
                    return PartyRoleBase.LoanProcessor;
                case E_AgentRoleT.Realtor:
                    return PartyRoleBase.RealEstateAgent;
                case E_AgentRoleT.Seller:
                    return PartyRoleBase.PropertySeller;
                case E_AgentRoleT.SellerAttorney:
                    return PartyRoleBase.Attorney;
                case E_AgentRoleT.SellingAgent:
                    return PartyRoleBase.RealEstateAgent;
                case E_AgentRoleT.Servicing:
                case E_AgentRoleT.Subservicer:
                    return PartyRoleBase.Servicer;
                case E_AgentRoleT.Title:
                    return PartyRoleBase.TitleCompany;
                case E_AgentRoleT.TitleUnderwriter:
                    return PartyRoleBase.TitleUnderwriter;
                case E_AgentRoleT.Trustee:
                    return PartyRoleBase.Trustee;
                case E_AgentRoleT.Underwriter:
                    return PartyRoleBase.LoanUnderwriter;
                case E_AgentRoleT.Bank:
                case E_AgentRoleT.CallCenterAgent:
                case E_AgentRoleT.CollateralAgent:
                case E_AgentRoleT.CreditAuditor:
                case E_AgentRoleT.CreditReport:
                case E_AgentRoleT.CreditReportAgency2:
                case E_AgentRoleT.CreditReportAgency3:
                case E_AgentRoleT.DisclosureDesk:
                case E_AgentRoleT.DocDrawer:
                case E_AgentRoleT.ECOA:
                case E_AgentRoleT.ExternalPostCloser:
                case E_AgentRoleT.ExternalSecondary:
                case E_AgentRoleT.FairHousingLending:
                case E_AgentRoleT.Funder:
                case E_AgentRoleT.HomeInspection:
                case E_AgentRoleT.Insuring:
                case E_AgentRoleT.JuniorProcessor:
                case E_AgentRoleT.JuniorUnderwriter:
                case E_AgentRoleT.LegalAuditor:
                case E_AgentRoleT.LoanOfficerAssistant:
                case E_AgentRoleT.LoanOpener:
                case E_AgentRoleT.Manager:
                case E_AgentRoleT.MarketingLead:
                case E_AgentRoleT.Mortgagee:
                case E_AgentRoleT.PestInspection:
                case E_AgentRoleT.PostCloser:
                case E_AgentRoleT.PropertyManagement:
                case E_AgentRoleT.Purchaser:
                case E_AgentRoleT.QCCompliance:
                case E_AgentRoleT.Secondary:
                case E_AgentRoleT.Shipper:
                case E_AgentRoleT.Surveyor:
                case E_AgentRoleT.AppraisalManagementCompany:
                case E_AgentRoleT.Referral:
                    otherDescription = agentRole.ToString();
                    return PartyRoleBase.Other;
                case E_AgentRoleT.Other:
                    // OPM 236992: Enum Parse will succeed if input string is an int, even if it does not correspond to a valid enumeration.
                    PartyRoleBase partyRoleType;
                    if (Enum.TryParse<PartyRoleBase>(agentRoleDescription, out partyRoleType) && Enum.IsDefined(typeof(PartyRoleBase), partyRoleType))
                    {
                        return partyRoleType;
                    }

                    otherDescription = agentRoleDescription;
                    return PartyRoleBase.Other;
                default:
                    throw new UnhandledEnumException(agentRole);
            }
        }

        /// <summary>
        /// Converts a PaymentFrequency enumerated value into a serializable object.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to convert.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PaymentFrequencyBase}" /> object.</returns>
        public static MISMOEnum<PaymentFrequencyBase> ToPaymentFrequencyEnum(PaymentFrequencyBase mismoValue)
        {
            var paymentFrequencyEnum = new MISMOEnum<PaymentFrequencyBase>();
            paymentFrequencyEnum.enumValue = mismoValue;
            paymentFrequencyEnum.IsSensitive = false;

            return paymentFrequencyEnum;
        }

        /// <summary>
        /// Converts an LQB enumerated value representing a prepayment penalty into a MISMO indicator.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable MISMOIndicator object, or null if the LQB field was left blank.</returns>
        public static MISMOIndicator ToPrepaymentPenaltyIndicator(E_sPrepmtPenaltyT lqbValue)
        {
            if (lqbValue == E_sPrepmtPenaltyT.LeaveBlank)
            {
                return null;
            }

            // May maps to true, Will Not maps to false.
            return ToMismoIndicator(lqbValue == E_sPrepmtPenaltyT.May);
        }

        /// <summary>
        /// Converts a PrepaymentPenaltyPeriod enumerated value into a serializable object.
        /// </summary>
        /// <param name="mismoValue">The MISMO value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PrepaymentPenaltyPeriodBase}" /> object.</returns>
        public static MISMOEnum<PrepaymentPenaltyPeriodBase> ToPrepaymentPenaltyPeriodEnum(PrepaymentPenaltyPeriodBase mismoValue)
        {
            var prepaymentPenaltyPeriodEnum = new MISMOEnum<PrepaymentPenaltyPeriodBase>();
            prepaymentPenaltyPeriodEnum.enumValue = mismoValue;
            prepaymentPenaltyPeriodEnum.IsSensitive = false;

            return prepaymentPenaltyPeriodEnum;
        }

        /// <summary>
        /// Converts an LQB enumerated value representing a prepayment finance charge indicator into a MISMO indicator.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable MISMOIndicator object, or null if the LQB field was left blank.</returns>
        public static MISMOIndicator ToPrepaymentFinanceChargeRefundableIndicator(E_sPrepmtRefundT lqbValue)
        {
            if (lqbValue == E_sPrepmtRefundT.LeaveBlank)
            {
                return null;
            }

            // May maps to true, Will Not maps to false.
            return ToMismoIndicator(lqbValue == E_sPrepmtRefundT.May);
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{PriorPropertyTitleBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PriorPropertyTitleBase}" /> object.</returns>
        public static MISMOEnum<PriorPropertyTitleBase> ToPriorPropertyTitleEnum(E_aBDecPastOwnedPropTitleT lqbValue)
        {
            var priorPropertyTitleEnum = new MISMOEnum<PriorPropertyTitleBase>();
            priorPropertyTitleEnum.enumValue = GetPriorPropertyTitleBaseValue(lqbValue);
            priorPropertyTitleEnum.IsSensitive = false;

            return priorPropertyTitleEnum;
        }

        /// <summary>
        /// Retrieves the PriorPropertyTitle value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding PriorPropertyTitle value.</returns>
        public static PriorPropertyTitleBase GetPriorPropertyTitleBaseValue(E_aBDecPastOwnedPropTitleT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aBDecPastOwnedPropTitleT.Empty:
                    return PriorPropertyTitleBase.Blank;
                case E_aBDecPastOwnedPropTitleT.O:
                    return PriorPropertyTitleBase.JointWithOtherThanSpouse;
                case E_aBDecPastOwnedPropTitleT.SP:
                    return PriorPropertyTitleBase.JointWithSpouse;
                case E_aBDecPastOwnedPropTitleT.S:
                    return PriorPropertyTitleBase.Sole;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{PriorPropertyUsageBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PriorPropertyUsageBase}" /> object.</returns>
        public static MISMOEnum<PriorPropertyUsageBase> ToPriorPropertyUsageEnum(E_aBDecPastOwnedPropT lqbValue)
        {
            var priorPropertyUsageEnum = new MISMOEnum<PriorPropertyUsageBase>();
            priorPropertyUsageEnum.enumValue = GetPriorPropertyUsageBaseValue(lqbValue);
            priorPropertyUsageEnum.IsSensitive = false;

            return priorPropertyUsageEnum;
        }

        /// <summary>
        /// Retrieves the PriorPropertyUsage value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding PriorPropertyUsage value.</returns>
        public static PriorPropertyUsageBase GetPriorPropertyUsageBaseValue(E_aBDecPastOwnedPropT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aBDecPastOwnedPropT.Empty:
                    return PriorPropertyUsageBase.Blank;
                case E_aBDecPastOwnedPropT.IP:
                    return PriorPropertyUsageBase.Investment;
                case E_aBDecPastOwnedPropT.PR:
                    return PriorPropertyUsageBase.PrimaryResidence;
                case E_aBDecPastOwnedPropT.SH:
                    return PriorPropertyUsageBase.SecondHome;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates a project attachment enumeration object based on the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProjectAttachmentBase}" /> object.</returns>
        public static MISMOEnum<ProjectAttachmentBase> ToProjectAttachmentEnum(E_sGseSpT propertyType)
        {
            var projectAttachmentEnum = new MISMOEnum<ProjectAttachmentBase>();
            projectAttachmentEnum.enumValue = GetProjectAttachmentBaseValue(propertyType);
            projectAttachmentEnum.IsSensitive = false;

            if (projectAttachmentEnum.enumValue == ProjectAttachmentBase.Blank)
            {
                return null;
            }

            return projectAttachmentEnum;
        }

        /// <summary>
        /// Gets the project attachment type value corresponding to the given property property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding project attachment base value.</returns>
        public static ProjectAttachmentBase GetProjectAttachmentBaseValue(E_sGseSpT propertyType)
        {
            switch (propertyType)
            {
                case E_sGseSpT.Attached:
                case E_sGseSpT.Condominium:
                    return ProjectAttachmentBase.Attached;
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.Detached:
                case E_sGseSpT.DetachedCondominium:
                    return ProjectAttachmentBase.Detached;
                case E_sGseSpT.HighRiseCondominium:
                    return ProjectAttachmentBase.Attached;
                case E_sGseSpT.LeaveBlank:
                    return ProjectAttachmentBase.Blank;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return ProjectAttachmentBase.Detached;
                default:
                    throw new UnhandledEnumException(propertyType);
            }
        }

        /// <summary>
        /// Creates a project design enumeration object based on the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProjectDesignBase}" /> object.</returns>
        public static MISMOEnum<ProjectDesignBase> ToProjectDesignEnum(E_sGseSpT propertyType)
        {
            var projectDesignEnum = new MISMOEnum<ProjectDesignBase>();
            projectDesignEnum.enumValue = GetProjectDesignBaseValue(propertyType);
            projectDesignEnum.IsSensitive = false;

            return projectDesignEnum;
        }

        /// <summary>
        /// Retrieves the project design value corresponding to the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding project design value.</returns>
        public static ProjectDesignBase GetProjectDesignBaseValue(E_sGseSpT propertyType)
        {
            if (propertyType == E_sGseSpT.HighRiseCondominium)
            {
                return ProjectDesignBase.HighriseProject;
            }
            else
            {
                return ProjectDesignBase.Other;
            }
        }

        /// <summary>
        /// Gets the land use field for an REO, from the REO type.
        /// </summary>
        /// <param name="reoType">The REO's type from the dropdown.</param>
        /// <returns>The land use corresponding to the REO type, or null if no mapping or blank.</returns>
        public static MISMOEnum<LandUseBase> ToLandUseEnum(E_ReoTypeT reoType)
        {
            var landUseEnum = new MISMOEnum<LandUseBase>();
            landUseEnum.enumValue = GetLandUseBaseValue(reoType);
            if (landUseEnum.enumValue.Equals(LandUseBase.Blank))
            {
                return null;
            }

            landUseEnum.IsSensitive = false;

            return landUseEnum;
        }

        /// <summary>
        /// Maps LQB REO type to MISMO Land Use.
        /// </summary>
        /// <param name="reoType">The REO type to be mapped.</param>
        /// <returns>Corresponding land use enum, or blank if not mapped.</returns>
        public static LandUseBase GetLandUseBaseValue(E_ReoTypeT reoType)
        {
            if (reoType == E_ReoTypeT.ComNR)
            {
                return LandUseBase.Commercial;
            }
            else if (reoType == E_ReoTypeT.ComR)
            {
                return LandUseBase.Income;
            }
            else if (reoType == E_ReoTypeT.Farm)
            {
                return LandUseBase.Agricultural;
            }
            else if (reoType == E_ReoTypeT.Land)
            {
                return LandUseBase.Vacant;
            }
            else if (reoType == E_ReoTypeT.Mixed)
            {
                return LandUseBase.Residential;
            }
            else
            {
                return LandUseBase.Blank;
            }
        }

        /// <summary>
        /// Creates a project design enumeration object based on REO type.
        /// </summary>
        /// <param name="reoType">The REO type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProjectDesignBase}"/> based on the REO type, or null if REO type is unmapped.</returns>
        public static MISMOEnum<ProjectDesignBase> ToProjectDesignEnum(E_ReoTypeT reoType)
        {
            var projectDesignEnum = new MISMOEnum<ProjectDesignBase>();
            projectDesignEnum.enumValue = GetProjectDesignBaseValue(reoType);
            if (projectDesignEnum.enumValue == ProjectDesignBase.Blank)
            {
                return null;
            }

            projectDesignEnum.IsSensitive = false;

            return projectDesignEnum;
        }

        /// <summary>
        /// Maps REO type to project design enumeration.
        /// </summary>
        /// <param name="reoType">The REO type.</param>
        /// <returns>The mapped <see cref="ProjectDesignBase"/> enumeration, or Blank if unmapped.</returns>
        public static ProjectDesignBase GetProjectDesignBaseValue(E_ReoTypeT reoType)
        {
            if (reoType == E_ReoTypeT.Town)
            {
                return ProjectDesignBase.TownhouseRowhouse;
            }
            else
            {
                return ProjectDesignBase.Blank;
            }
        }

        /// <summary>
        /// Creates a projected payment calculation period term enumeration object based on the given term.
        /// </summary>
        /// <param name="term">The calculation period term, e.g. yearly.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProjectedPaymentCalculationPeriodTermBase}" /> object.</returns>
        public static MISMOEnum<ProjectedPaymentCalculationPeriodTermBase> ToProjectedPaymentCalculationPeriodTermEnum(ProjectedPaymentCalculationPeriodTermBase term)
        {
            var termEnum = new MISMOEnum<ProjectedPaymentCalculationPeriodTermBase>();
            termEnum.enumValue = term;
            termEnum.IsSensitive = false;

            return termEnum;
        }

        /// <summary>
        /// Creates a projected payment escrowed enumeration object indicating whether the payment is escrowed.
        /// </summary>
        /// <param name="escrowed">True if the projected payment is escrowed. Otherwise false.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProjectedPaymentEscrowedBase}" /> object.</returns>
        public static MISMOEnum<ProjectedPaymentEscrowedBase> ToProjectedPaymentEscrowedEnum(bool escrowed)
        {
            var projectedPaymentEscrowedEnum = new MISMOEnum<ProjectedPaymentEscrowedBase>();
            projectedPaymentEscrowedEnum.enumValue = escrowed ? ProjectedPaymentEscrowedBase.Escrowed : ProjectedPaymentEscrowedBase.NotEscrowed;
            projectedPaymentEscrowedEnum.IsSensitive = false;

            return projectedPaymentEscrowedEnum;
        }

        /// <summary>
        /// Gets the ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase value corresponding to the type of the associated cost/expense.
        /// </summary>
        /// <param name="costType">The cost type, e.g. property taxes, hazard insurance, etc.</param>
        /// <returns>The ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase value based on the cost type.</returns>
        public static ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase GetProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase(E_HousingExpenseTypeT costType)
        {
            switch (costType)
            {
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                case E_HousingExpenseTypeT.FloodInsurance:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.HomeownersInsurance;
                case E_HousingExpenseTypeT.GroundRent:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.GroundRent;
                case E_HousingExpenseTypeT.HazardInsurance:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.HomeownersInsurance;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.HomeownersAssociationDues;
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.RealEstateTaxes:
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.PropertyTaxes;
                case E_HousingExpenseTypeT.Unassigned:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.Other;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.HomeownersInsurance;
                default:
                    throw new UnhandledEnumException(costType);
            }
        }

        /// <summary>
        /// Creates a projected payment estimated taxes/insurance assessment component enumeration object based on the type of the associated cost/expense.
        /// </summary>
        /// <param name="costType">The cost type, e.g. property taxes, hazard insurance, etc.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase}" /> object.</returns>
        public static MISMOEnum<ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase> ToProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentEnum(E_HousingExpenseTypeT costType)
        {
            var projectedPaymentEstTIAComponentEnum = new MISMOEnum<ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase>();
            projectedPaymentEstTIAComponentEnum.enumValue = GetProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase(costType);
            projectedPaymentEstTIAComponentEnum.IsSensitive = false;

            return projectedPaymentEstTIAComponentEnum;
        }

        /// <summary>
        /// Creates a project legal structure enumeration object based on the given real estate owned type.
        /// </summary>
        /// <param name="reoType">The real estate owned type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProjectLegalStructureBase}" /> object.</returns>
        public static MISMOEnum<ProjectLegalStructureBase> ToProjectLegalStructureEnum(E_ReoTypeT reoType)
        {
            var projectLegalStructureEnum = new MISMOEnum<ProjectLegalStructureBase>();
            projectLegalStructureEnum.enumValue = GetProjectLegalStructureBaseValue(reoType);
            if (projectLegalStructureEnum.enumValue.Equals(ProjectLegalStructureBase.Blank))
            {
                return null;
            }

            projectLegalStructureEnum.IsSensitive = false;

            return projectLegalStructureEnum;
        }

        /// <summary>
        /// Creates a project legal structure enumeration object based on the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProjectLegalStructureBase}" /> object.</returns>
        public static MISMOEnum<ProjectLegalStructureBase> ToProjectLegalStructureEnum(E_sGseSpT propertyType)
        {
            var projectLegalStructureEnum = new MISMOEnum<ProjectLegalStructureBase>();
            projectLegalStructureEnum.enumValue = GetProjectLegalStructureBaseValue(propertyType);
            projectLegalStructureEnum.IsSensitive = false;

            return projectLegalStructureEnum;
        }

        /// <summary>
        /// Gets the project legal structure value corresponding to the given real estate owned type.
        /// </summary>
        /// <param name="reoType">The real estate owned type.</param>
        /// <returns>The corresponding ProjectLegalStructureBase value.</returns>
        public static ProjectLegalStructureBase GetProjectLegalStructureBaseValue(E_ReoTypeT reoType)
        {
            if (reoType == E_ReoTypeT.Condo)
            {
                return ProjectLegalStructureBase.Condominium;
            }
            else if (reoType == E_ReoTypeT.Coop)
            {
                return ProjectLegalStructureBase.Cooperative;
            }
            else
            {
                return ProjectLegalStructureBase.Blank;
            }
        }

        /// <summary>
        /// Gets the project legal structure value corresponding to the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding ProjectLegalStructureBase value.</returns>
        public static ProjectLegalStructureBase GetProjectLegalStructureBaseValue(E_sGseSpT propertyType)
        {
            if (propertyType == E_sGseSpT.Condominium ||
                propertyType == E_sGseSpT.DetachedCondominium ||
                propertyType == E_sGseSpT.HighRiseCondominium ||
                propertyType == E_sGseSpT.ManufacturedHomeCondominium)
            {
                return ProjectLegalStructureBase.Condominium;
            }
            else if (propertyType == E_sGseSpT.Cooperative)
            {
                return ProjectLegalStructureBase.Cooperative;
            }
            else
            {
                return ProjectLegalStructureBase.Unknown;
            }
        }

        /// <summary>
        /// Converts an LQB enumeration into a serializable <see cref="MISMOEnum{PropertyEstateBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PropertyEstateBase}" /> object.</returns>
        public static MISMOEnum<PropertyEstateBase> ToPropertyEstateEnum(E_sEstateHeldT lqbValue)
        {
            var propertyEstateEnum = new MISMOEnum<PropertyEstateBase>();
            propertyEstateEnum.enumValue = GetPropertyEstateBaseValue(lqbValue);
            propertyEstateEnum.IsSensitive = false;

            return propertyEstateEnum;
        }

        /// <summary>
        /// Retrieves the PropertyEstate value that corresponds to the LQB enumeration.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding PropertyEstateBase value.</returns>
        public static PropertyEstateBase GetPropertyEstateBaseValue(E_sEstateHeldT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sEstateHeldT.FeeSimple:
                    return PropertyEstateBase.FeeSimple;
                case E_sEstateHeldT.LeaseHold:
                    return PropertyEstateBase.Leasehold;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Converts an LQB enumeration into a serializable <see cref="MISMOEnum{PropertyInspectionBase}"/> object.
        /// </summary>
        /// <param name="inspectionType">The inspection type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PropertyInspectionBase}"/> object.</returns>
        public static MISMOEnum<PropertyInspectionBase> ToPropertyInspectionEnum(E_sPropertyInspectionT inspectionType)
        {
            var propertyInspectionEnum = new MISMOEnum<PropertyInspectionBase>();
            propertyInspectionEnum.enumValue = GetPropertyInspectionBaseValue(inspectionType);
            propertyInspectionEnum.IsSensitive = false;

            return propertyInspectionEnum;
        }

        /// <summary>
        /// Maps the property inspection type to MISMO.
        /// </summary>
        /// <param name="inspectionType">The property inspection type as represented in LendingQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static PropertyInspectionBase GetPropertyInspectionBaseValue(E_sPropertyInspectionT inspectionType)
        {
            switch (inspectionType)
            {
                case E_sPropertyInspectionT.ExteriorAndInterior:
                    return PropertyInspectionBase.ExteriorAndInterior;
                case E_sPropertyInspectionT.ExteriorOnly:
                    return PropertyInspectionBase.ExteriorOnly;
                case E_sPropertyInspectionT.None:
                    return PropertyInspectionBase.None;
                case E_sPropertyInspectionT.Other:
                    return PropertyInspectionBase.Other;
                default:
                    throw new UnhandledEnumException(inspectionType);
            }
        }

        /// <summary>
        /// Creates a property usage enumeration object based on the occupancy type.
        /// </summary>
        /// <param name="occupancyType">The occupancy type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PropertyUsageBase}" /> object.</returns>
        public static MISMOEnum<PropertyUsageBase> ToPropertyUsageEnum(E_aOccT occupancyType)
        {
            var propertyUsageEnum = new MISMOEnum<PropertyUsageBase>();
            propertyUsageEnum.enumValue = GetPropertyUsageBaseValue(occupancyType);
            propertyUsageEnum.IsSensitive = false;

            return propertyUsageEnum;
        }

        /// <summary>
        /// Retrieves the property usage value corresponding to the occupancy type.
        /// </summary>
        /// <param name="occupancyType">The occupancy type.</param>
        /// <returns>The corresponding property usage value.</returns>
        public static PropertyUsageBase GetPropertyUsageBaseValue(E_aOccT occupancyType)
        {
            switch (occupancyType)
            {
                case E_aOccT.Investment:
                    return PropertyUsageBase.Investment;
                case E_aOccT.PrimaryResidence:
                    return PropertyUsageBase.PrimaryResidence;
                case E_aOccT.SecondaryResidence:
                    return PropertyUsageBase.SecondHome;
                default:
                    throw new UnhandledEnumException(occupancyType);
            }
        }

        /// <summary>
        /// Creates a property usage enumeration object based on the occupancy type.
        /// </summary>
        /// <param name="occupancyType">The occupancy type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PropertyUsageBase}" /> object.</returns>
        public static MISMOEnum<PropertyUsageBase> ToPropertyUsageEnum(E_sOccT occupancyType)
        {
            var propertyUsageEnum = new MISMOEnum<PropertyUsageBase>();
            propertyUsageEnum.enumValue = GetPropertyUsageBaseValue(occupancyType);
            propertyUsageEnum.IsSensitive = false;

            return propertyUsageEnum;
        }

        /// <summary>
        /// Retrieves the property usage value corresponding to the occupancy type.
        /// </summary>
        /// <param name="occupancyType">The occupancy type.</param>
        /// <returns>The corresponding property usage value.</returns>
        public static PropertyUsageBase GetPropertyUsageBaseValue(E_sOccT occupancyType)
        {
            switch (occupancyType)
            {
                case E_sOccT.Investment:
                    return PropertyUsageBase.Investment;
                case E_sOccT.PrimaryResidence:
                    return PropertyUsageBase.PrimaryResidence;
                case E_sOccT.SecondaryResidence:
                    return PropertyUsageBase.SecondHome;
                default:
                    throw new UnhandledEnumException(occupancyType);
            }
        }

        /// <summary>
        /// Converts an LQB value to a serializable <see cref="MISMOEnum{PropertyValuationFormBase}" /> object.
        /// </summary>
        /// <param name="form">The type of form used for appraisal.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PropertyValuationFormBase}"/> object.</returns>
        public static MISMOEnum<PropertyValuationFormBase> ToPropertyValuationFormEnum(E_sSpAppraisalFormT form)
        {
            var propertyValuationFormEnum = new MISMOEnum<PropertyValuationFormBase>();
            propertyValuationFormEnum.enumValue = GetPropertyValuationFormBaseValue(form);
            propertyValuationFormEnum.IsSensitive = false;

            return propertyValuationFormEnum;
        }

        /// <summary>
        /// Maps the LendingQB appraisal valuation form type to MISMO.
        /// </summary>
        /// <param name="form">The valuation form as represented in LendingQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static PropertyValuationFormBase GetPropertyValuationFormBaseValue(E_sSpAppraisalFormT form)
        {
            switch (form)
            {
                case E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport:
                    return PropertyValuationFormBase.AppraisalUpdateAndOrCompletionReport;
                case E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport:
                    return PropertyValuationFormBase.DesktopUnderwriterPropertyInspectionReport;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport:
                    return PropertyValuationFormBase.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport:
                    return PropertyValuationFormBase.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport:
                    return PropertyValuationFormBase.ExteriorOnlyInspectionResidentialAppraisalReport;
                case E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport:
                    return PropertyValuationFormBase.IndividualCondominiumUnitAppraisalReport;
                case E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport:
                    return PropertyValuationFormBase.IndividualCooperativeInterestAppraisalReport;
                case E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability:
                    return PropertyValuationFormBase.LoanProspectorConditionAndMarketability;
                case E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport:
                    return PropertyValuationFormBase.ManufacturedHomeAppraisalReport;
                case E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport:
                    return PropertyValuationFormBase.OneUnitResidentialAppraisalFieldReviewReport;
                case E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport:
                    return PropertyValuationFormBase.SmallResidentialIncomePropertyAppraisalReport;
                case E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal:
                    return PropertyValuationFormBase.TwoToFourUnitResidentialAppraisalFieldReviewReport;
                case E_sSpAppraisalFormT.UniformResidentialAppraisalReport:
                    return PropertyValuationFormBase.UniformResidentialAppraisalReport;
                case E_sSpAppraisalFormT.Blank:
                case E_sSpAppraisalFormT.Other:
                    return PropertyValuationFormBase.Other;
                default:
                    throw new UnhandledEnumException(form);
            }
        }

        /// <summary>
        /// Converts an LQB value to a serializable <see cref="MISMOEnum{PropertyValuationMethodBase}" /> object.
        /// </summary>
        /// <param name="valuationMethod">The valuation method as represented in LQB.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PropertyValuationMethodBase}" /> object.</returns>
        public static MISMOEnum<PropertyValuationMethodBase> ToPropertyValuationMethodEnum(E_sSpValuationMethodT valuationMethod)
        {
            var propertyValuationMethodEnum = new MISMOEnum<PropertyValuationMethodBase>();
            propertyValuationMethodEnum.enumValue = GetPropertyValuationMethodBaseValue(valuationMethod);
            propertyValuationMethodEnum.IsSensitive = false;

            return propertyValuationMethodEnum;
        }

        /// <summary>
        /// Maps the LendingQB valuation method to MISMO.
        /// </summary>
        /// <param name="valuationMethod">The valuation method as represented in LendingQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static PropertyValuationMethodBase GetPropertyValuationMethodBaseValue(E_sSpValuationMethodT valuationMethod)
        {
            switch (valuationMethod)
            {
                case E_sSpValuationMethodT.AutomatedValuationModel:
                    return PropertyValuationMethodBase.AutomatedValuationModel;
                case E_sSpValuationMethodT.DesktopAppraisal:
                    return PropertyValuationMethodBase.DesktopAppraisal;
                case E_sSpValuationMethodT.DriveBy:
                    return PropertyValuationMethodBase.DriveBy;
                case E_sSpValuationMethodT.FullAppraisal:
                    return PropertyValuationMethodBase.FullAppraisal;
                case E_sSpValuationMethodT.PriorAppraisalUsed:
                    return PropertyValuationMethodBase.PriorAppraisalUsed;
                case E_sSpValuationMethodT.None:
                    return PropertyValuationMethodBase.None;
                case E_sSpValuationMethodT.FieldReview:
                case E_sSpValuationMethodT.DeskReview:
                case E_sSpValuationMethodT.LeaveBlank:
                case E_sSpValuationMethodT.Other:
                    return PropertyValuationMethodBase.Other;
                default:
                    throw new UnhandledEnumException(valuationMethod);
            }
        }

        /// <summary>
        /// Creates a proration item enumeration object from the given item type.
        /// </summary>
        /// <param name="item">The proration item type.</param>
        /// <param name="displayLabelText">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <returns>A serializable <see cref="MISMOEnum{ProrationItemBase}" /> object.</returns>
        public static MISMOEnum<ProrationItemBase> ToProrationItemEnum(ProrationItemBase item, string displayLabelText)
        {
            if (item == ProrationItemBase.Blank)
            {
                return null;
            }

            var prorationItemEnum = new MISMOEnum<ProrationItemBase>();
            prorationItemEnum.enumValue = item;
            prorationItemEnum.IsSensitive = false;
            prorationItemEnum.DisplayLabelText = displayLabelText;

            return prorationItemEnum;
        }

        /// <summary>
        /// Returns the proration/assessment type based on the given property type.
        /// </summary>
        /// <param name="propertyType">The type of subject property.</param>
        /// <returns>A ProrationItemBase with the assessment type corresponding to the given property type.</returns>
        public static ProrationItemBase GetProrationItemBase_Assessment(E_sGseSpT propertyType)
        {
            switch (propertyType)
            {
                case E_sGseSpT.Attached:
                    return ProrationItemBase.HomeownersAssociationSpecialAssessment;
                case E_sGseSpT.Condominium:
                    return ProrationItemBase.CondominiumAssociationSpecialAssessment;
                case E_sGseSpT.Cooperative:
                    return ProrationItemBase.CooperativeAssociationSpecialAssessment;
                case E_sGseSpT.Detached:
                    return ProrationItemBase.HomeownersAssociationSpecialAssessment;
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                    return ProrationItemBase.CondominiumAssociationSpecialAssessment;
                case E_sGseSpT.LeaveBlank:
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return ProrationItemBase.HomeownersAssociationSpecialAssessment;
                default:
                    throw new UnhandledEnumException(propertyType);
            }
        }

        /// <summary>
        /// Creates a purchase credit enumeration object from the given description.
        /// </summary>
        /// <param name="description">A description of the purchase credit.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PurchaseCreditBase}" /> object.</returns>
        public static MISMOEnum<PurchaseCreditBase> ToPurchaseCreditEnum(string description)
        {
            var purchaseCreditEnum = new MISMOEnum<PurchaseCreditBase>();
            purchaseCreditEnum.enumValue = GetPurchaseCreditBaseValue(description);
            purchaseCreditEnum.IsSensitive = false;

            return purchaseCreditEnum;
        }

        /// <summary>
        /// Retrieves the purchase credit value corresponding to the given description.
        /// </summary>
        /// <param name="description">A description of the purchase credit.</param>
        /// <returns>The corresponding value.</returns>
        public static PurchaseCreditBase GetPurchaseCreditBaseValue(string description)
        {
            if (string.Equals("cash deposit on sales contract", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.EarnestMoney;
            }
            else if (string.Equals("employer assisted housing", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.EmployerAssistedHousing;
            }
            else if (string.Equals("lease purchase fund", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.LeasePurchaseFund;
            }
            else if (string.Equals("relocation funds", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.RelocationFunds;
            }
            else if (string.Equals("mi premium refund", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.MIPremiumRefund;
            }
            else if (string.Equals("buydown fund", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.BuydownFund;
            }
            else if (string.Equals("commitment origination fee", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.CommitmentOriginationFee;
            }
            else if (string.Equals("federal agency funding fee refund", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.FederalAgencyFundingFeeRefund;
            }
            else if (string.Equals("gift of equity", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.GiftOfEquity;
            }
            else if (string.Equals("sweat equity", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.SweatEquity;
            }
            else if (string.Equals("trade equity", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.TradeEquity;
            }
            else
            {
                return PurchaseCreditBase.Other;
            }
        }

        /// <summary>
        /// Creates a purchase credit source object from the given description.
        /// </summary>
        /// <param name="description">A description of the purchase credit.</param>
        /// <returns>A serializable <see cref="MISMOEnum{PurchaseCreditSourceBase}" /> object.</returns>
        public static MISMOEnum<PurchaseCreditSourceBase> ToPurchaseCreditSourceEnum(string description)
        {
            var purchaseCreditSourceEnum = new MISMOEnum<PurchaseCreditSourceBase>();
            purchaseCreditSourceEnum.enumValue = GetPurchaseCreditSourceBaseValue(description);
            purchaseCreditSourceEnum.IsSensitive = false;

            return purchaseCreditSourceEnum;
        }

        /// <summary>
        /// Retrieves the purchase credit source value corresponding to the given description.
        /// </summary>
        /// <param name="description">A description of the purchase credit.</param>
        /// <returns>The corresponding purchase credit source value.</returns>
        public static PurchaseCreditSourceBase GetPurchaseCreditSourceBaseValue(string description)
        {
            if (string.Equals("lender credit", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditSourceBase.Lender;
            }
            else if (string.Equals("seller credit", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditSourceBase.PropertySeller;
            }
            else if (string.Equals("earnest deposit", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditSourceBase.BorrowerPaidOutsideClosing;
            }
            else
            {
                return PurchaseCreditSourceBase.Other;
            }
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{QualifiedMortgageBase}" /> object with the type of standards used to determine Qualified Mortgage eligibility. 
        /// </summary>
        /// <param name="eligibleByLoanPurchaseAgency">True if the loan is eligible for purchase or guarantee by a qualifying agency under $$QM$$.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A <see cref="MISMOEnum{QualifiedMortgageBase}" /> object with the type of standards used to determine Qualified Mortgage eligibility.</returns>
        public static MISMOEnum<QualifiedMortgageBase> ToQualifiedMortgageEnum(bool eligibleByLoanPurchaseAgency, bool isSensitiveData = false)
        {
            var qualifiedMortgage = new MISMOEnum<QualifiedMortgageBase>();
            qualifiedMortgage.enumValue = eligibleByLoanPurchaseAgency ? QualifiedMortgageBase.TemporaryAgencyGSE : QualifiedMortgageBase.Standard;
            qualifiedMortgage.IsSensitive = isSensitiveData;

            return qualifiedMortgage;
        }

        /// <summary>
        /// Creates an indicater determining whether the loan is an eligible qualified mortgage under Regulation Z.
        /// </summary>
        /// <param name="qualifiedStatus">The QM status.</param>
        /// <returns>A MISMO Indicator showing the qualified mortgage status.</returns>
        public static MISMOIndicator ToQualifiedMortgageIndicator(E_sQMStatusT qualifiedStatus)
        {
            if (qualifiedStatus == E_sQMStatusT.Blank)
            {
                return null;
            }

            var qualifiedIndicator = new MISMOIndicator();
            qualifiedIndicator.booleanValue = qualifiedStatus != E_sQMStatusT.Ineligible;
            qualifiedIndicator.IsSensitive = false;

            return qualifiedIndicator;
        }

        /// <summary>
        /// Creates a <see cref="MISMOEnum{QualifiedMortgageTemporaryGSEBase}" /> object based on the type of the given agency, by which the loan is eligible for purchase under $$QM$$.
        /// </summary>
        /// <param name="agency">The agency by which the loan is eligible for purchase.</param>
        /// <param name="isSensitiveData">Indicates whether the information is sensitive.</param>
        /// <returns>A <see cref="MISMOEnum{QualifiedMortgageTemporaryGSEBase}" /> object.</returns>
        public static MISMOEnum<QualifiedMortgageTemporaryGSEBase> ToQualifiedMortgageTemporaryGSEEnum(E_sQMLoanPurchaseAgency agency, bool isSensitiveData = false)
        {
            var gse = new MISMOEnum<QualifiedMortgageTemporaryGSEBase>();
            gse.enumValue = GetQualifiedMortgageTemporaryGSEBase(agency);
            gse.IsSensitive = isSensitiveData;

            if (gse.enumValue == QualifiedMortgageTemporaryGSEBase.Blank)
            {
                return null;
            }

            return gse;
        }

        /// <summary>
        /// Converts the given agency type to the corresponding QualifiedMortgageTemporaryGSEBase, e.g. Fannie Mae or Freddie Mac.
        /// </summary>
        /// <param name="agency">The agency by which the loan is eligible for purchase.</param>
        /// <returns>The QualifiedMortgageTemporaryGSEBase that corresponds to the given agency type.</returns>
        public static QualifiedMortgageTemporaryGSEBase GetQualifiedMortgageTemporaryGSEBase(E_sQMLoanPurchaseAgency agency)
        {
            switch (agency)
            {
                case E_sQMLoanPurchaseAgency.Blank:
                    return QualifiedMortgageTemporaryGSEBase.Blank;
                case E_sQMLoanPurchaseAgency.FannieMae:
                    return QualifiedMortgageTemporaryGSEBase.FannieMae;
                case E_sQMLoanPurchaseAgency.FHA:
                    return QualifiedMortgageTemporaryGSEBase.Blank;
                case E_sQMLoanPurchaseAgency.FreddieMac:
                    return QualifiedMortgageTemporaryGSEBase.FreddieMac;
                case E_sQMLoanPurchaseAgency.USDA:
                case E_sQMLoanPurchaseAgency.VA:
                    return QualifiedMortgageTemporaryGSEBase.Blank;
                default:
                    throw new UnhandledEnumException(agency);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{RefinanceImprovementsBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>A serializable <see cref="MISMOEnum{RefinanceImprovementsBase}" /> object, or null if the LQB value is blank.</returns>
        public static MISMOEnum<RefinanceImprovementsBase> ToRefinanceImprovementsEnum(E_sSpImprovTimeFrameT lqbValue)
        {
            if (lqbValue == E_sSpImprovTimeFrameT.LeaveBlank)
            {
                return null;
            }

            var refinanceImprovementsEnum = new MISMOEnum<RefinanceImprovementsBase>();
            refinanceImprovementsEnum.enumValue = GetRefinanceImprovementsBaseValue(lqbValue);
            refinanceImprovementsEnum.IsSensitive = false;

            return refinanceImprovementsEnum;
        }

        /// <summary>
        /// Retrieves the RefinanceImprovementsBase value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding RefinanceImprovementBase value.</returns>
        public static RefinanceImprovementsBase GetRefinanceImprovementsBaseValue(E_sSpImprovTimeFrameT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sSpImprovTimeFrameT.Made:
                    return RefinanceImprovementsBase.Made;
                case E_sSpImprovTimeFrameT.ToBeMade:
                    return RefinanceImprovementsBase.ToBeMade;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Converts an LQB value into a serializable <see cref="MISMOEnum{SectionOfActBase}" /> object.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable <see cref="MISMOEnum{SectionOfActBase}" /> object.</returns>
        public static MISMOEnum<SectionOfActBase> ToSectionOfActEnum(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return null;
            }

            var sectionOfActEnum = new MISMOEnum<SectionOfActBase>();
            sectionOfActEnum.enumValue = GetSectionOfActBaseValue(lqbValue);
            sectionOfActEnum.IsSensitive = false;

            return sectionOfActEnum;
        }

        /// <summary>
        /// Retrieves a value that indicates whether the lender intends to service or transfer the loan.
        /// </summary>
        /// <param name="lenderIntendsToRetainLoan">Indicates whether the lender intends to retain the loan for servicing.</param>
        /// <returns>A serializable <see cref="MISMOEnum{ServicingTransferStatusBase}"/> object.</returns>
        public static MISMOEnum<ServicingTransferStatusBase> ToServicingTransferStatusEnum(bool lenderIntendsToRetainLoan)
        {
            var servicingTransferStatusEnum = new MISMOEnum<ServicingTransferStatusBase>();
            servicingTransferStatusEnum.enumValue = lenderIntendsToRetainLoan ? ServicingTransferStatusBase.Retained : ServicingTransferStatusBase.Released;
            servicingTransferStatusEnum.IsSensitive = false;

            return servicingTransferStatusEnum;
        }

        /// <summary>
        /// Retrieves the SectionOfAct value that corresponds to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding SectionOfAct value.</returns>
        public static SectionOfActBase GetSectionOfActBaseValue(string lqbValue)
        {
            if (string.Equals("184", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item184;
            }
            else if (string.Equals("203(b)", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203B;
            }
            else if (string.Equals("203(b)2", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203B2;
            }
            else if (string.Equals("203(b)/251", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203B251;
            }
            else if (string.Equals("203(h)", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203H;
            }
            else if (string.Equals("203(k)", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203K;
            }
            else if (string.Equals("203(k)/251", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203K251;
            }
            else if (string.Equals("234(c)", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item234C;
            }
            else if (string.Equals("234(c)/251", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item234C251;
            }
            else if (string.Equals("248", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item248;
            }
            else if (string.Equals("251", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item251;
            }
            else if (string.Equals("255", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item255;
            }
            else
            {
                return SectionOfActBase.Other;
            }
        }

        /// <summary>
        /// Creates a summary amount type enumeration object specifying the type of a borrower's
        /// asset or liability.
        /// </summary>
        /// <param name="type">The type of a borrower's asset or liability.</param>
        /// <returns>A serializable <see cref="MISMOEnum{SummaryAmountBase}" /> object.</returns>
        public static MISMOEnum<SummaryAmountBase> ToSummaryAmountEnum(SummaryAmountBase type)
        {
            var summaryAmountEnum = new MISMOEnum<SummaryAmountBase>();
            summaryAmountEnum.enumValue = type;
            summaryAmountEnum.IsSensitive = false;

            return summaryAmountEnum;
        }

        /// <summary>
        /// Creates an object mapping a tax type to a tax authority enumerated value.
        /// </summary>
        /// <param name="taxType">The tax type.</param>
        /// <returns>A serializable <see cref="MISMOEnum{TaxAuthorityBase}" /> object.</returns>
        public static MISMOEnum<TaxAuthorityBase> ToTaxAuthorityEnum(E_TaxTableTaxT taxType)
        {
            var taxAuthorityEnum = new MISMOEnum<TaxAuthorityBase>();
            taxAuthorityEnum.enumValue = GetTaxAuthorityBaseValue(taxType);
            taxAuthorityEnum.IsSensitive = false;

            return taxAuthorityEnum;
        }

        /// <summary>
        /// Retrieves the tax authority value corresponding to the given tax type.
        /// </summary>
        /// <param name="taxType">The tax type.</param>
        /// <returns>The corresponding tax authority value.</returns>
        public static TaxAuthorityBase GetTaxAuthorityBaseValue(E_TaxTableTaxT taxType)
        {
            switch (taxType)
            {
                case E_TaxTableTaxT.Borough:
                    return TaxAuthorityBase.BoroughTax;
                case E_TaxTableTaxT.City:
                    return TaxAuthorityBase.CityTax;
                case E_TaxTableTaxT.County:
                    return TaxAuthorityBase.CountyTax;
                case E_TaxTableTaxT.FireDist:
                    return TaxAuthorityBase.FireOrPoliceTax;
                case E_TaxTableTaxT.LocalImprovementDist:
                    return TaxAuthorityBase.ImprovementTax;
                case E_TaxTableTaxT.School:
                    return TaxAuthorityBase.SchoolDistrictTax;
                case E_TaxTableTaxT.SpecialAssessmentDist:
                    return TaxAuthorityBase.SpecialAssessment;
                case E_TaxTableTaxT.Town:
                    return TaxAuthorityBase.TownTax;
                case E_TaxTableTaxT.Township:
                    return TaxAuthorityBase.TownshipTax;
                case E_TaxTableTaxT.Utility:
                case E_TaxTableTaxT.MunicipalUtilDist:
                    return TaxAuthorityBase.UtilityDistrictTax;
                case E_TaxTableTaxT.Village:
                    return TaxAuthorityBase.VillageTax;
                case E_TaxTableTaxT.Water_Irrigation:
                    return TaxAuthorityBase.WaterControlTax;
                case E_TaxTableTaxT.WasteFeeDist:
                    return TaxAuthorityBase.SanitationTax;
                case E_TaxTableTaxT.Water_Sewer:
                    return TaxAuthorityBase.WaterOrSewerDistrictTax;
                case E_TaxTableTaxT.LeaveBlank:
                case E_TaxTableTaxT.Miscellaneous:
                    return TaxAuthorityBase.Other;
                default:
                    throw new UnhandledEnumException(taxType);                    
            }
        }

        /// <summary>
        /// Creates a taxpayer identifier enumeration object based on the given type.
        /// </summary>
        /// <param name="type">The taxpayer identification type.</param>
        /// <param name="displaySensitive">Indicates whether this element's sensitive indicator attribute should be serialized.</param>
        /// <returns>A serializable <see cref="MISMOEnum{TaxpayerIdentifierBase}" /> object.</returns>
        public static MISMOEnum<TaxpayerIdentifierBase> ToTaxpayerIdentifierEnum(TaxpayerIdentifierBase type, bool displaySensitive = true)
        {
            var taxpayerIdentifierEnum = new MISMOEnum<TaxpayerIdentifierBase>();
            taxpayerIdentifierEnum.enumValue = type;
            taxpayerIdentifierEnum.IsSensitive = true;
            taxpayerIdentifierEnum.DisplaySensitive = displaySensitive;

            return taxpayerIdentifierEnum;
        }

        /// <summary>
        /// Creates a TRID target regulation version enumeration object.
        /// </summary>
        /// <param name="version">The TRID regulation version.</param>
        /// <returns>A serializable enum object.</returns>
        public static MISMOEnum<LqbTridTargetRegulationVersionBase> ToLqbTridTargetRegulationVersionEnum(TridTargetRegulationVersionT version)
        {
            if (version == TridTargetRegulationVersionT.Blank)
            {
                return null;
            }

            var tridTargetRegulationVersionEnum = new MISMOEnum<LqbTridTargetRegulationVersionBase>();
            tridTargetRegulationVersionEnum.enumValue = GetLqbTridTargetRegulationVersionBaseValue(version);
            tridTargetRegulationVersionEnum.IsSensitive = false;

            return tridTargetRegulationVersionEnum;
        }

        /// <summary>
        /// Gets the TRID regulation serialization enum value.
        /// </summary>
        /// <param name="version">The TRID version.</param>
        /// <returns>The corresponding serialization value.</returns>
        public static LqbTridTargetRegulationVersionBase GetLqbTridTargetRegulationVersionBaseValue(TridTargetRegulationVersionT version)
        {
            switch (version)
            {
                case TridTargetRegulationVersionT.TRID2015:
                    return LqbTridTargetRegulationVersionBase.TRID2015;
                case TridTargetRegulationVersionT.TRID2017:
                    return LqbTridTargetRegulationVersionBase.TRID2017;
                default:
                    throw new UnhandledEnumException(version);
            }
        }

        /// <summary>
        /// Creates a trust classification enumeration object based on the given classification.
        /// </summary>
        /// <param name="classification">The trust classification.</param>
        /// <returns>A serializable <see cref="MISMOEnum{TrustClassificationBase}" /> object.</returns>
        public static MISMOEnum<TrustClassificationBase> ToTrustClassificationEnum(TrustClassificationBase classification)
        {
            var trustClassificationEnum = new MISMOEnum<TrustClassificationBase>();
            trustClassificationEnum.enumValue = classification;
            trustClassificationEnum.IsSensitive = false;

            return trustClassificationEnum;
        }

        /// <summary>
        /// Converts LQB values into a serializable <see cref="MISMOEnum{VABorrowerCertificationOccupancyBase}" /> object.
        /// </summary>
        /// <param name="isHome">Indicates whether the property will be occupied as a home.</param>
        /// <param name="isHomeForSpouse">Indicates whether the property will be occupied by a spouse.</param>
        /// <param name="isPrevAsHome">Indicates whether the property was previously occupied as a home.</param>
        /// <param name="isPrevAsHomeForSpouse">Indicates whether the property was previously occupied by a spouse.</param>
        /// <returns>A serializable <see cref="MISMOEnum{VABorrowerCertificationOccupancyBase}" /> object.</returns>
        public static MISMOEnum<VABorrowerCertificationOccupancyBase> ToVABorrowerCertificationOccupancyEnum(bool isHome, bool isHomeForSpouse, bool isPrevAsHome, bool isPrevAsHomeForSpouse)
        {
            var borrowerCertificationOccupancyEnum = new MISMOEnum<VABorrowerCertificationOccupancyBase>();
            borrowerCertificationOccupancyEnum.enumValue = GetVABorrowerCertificationOccupancyBaseValue(isHome, isHomeForSpouse, isPrevAsHome, isPrevAsHomeForSpouse);
            borrowerCertificationOccupancyEnum.IsSensitive = false;

            if (borrowerCertificationOccupancyEnum.enumValue == VABorrowerCertificationOccupancyBase.Blank)
            {
                return null;
            }
            else
            {
                return borrowerCertificationOccupancyEnum;
            }
        }

        /// <summary>
        /// Retrieves the VABorrowerCertificationOccupancy value corresponding to the LQB values in the application.
        /// </summary>
        /// <param name="isHome">Indicates whether the property will be occupied as a home.</param>
        /// <param name="isHomeForSpouse">Indicates whether the property will be occupied by a spouse.</param>
        /// <param name="isPrevAsHome">Indicates whether the property was previously occupied as a home.</param>
        /// <param name="isPrevAsHomeForSpouse">Indicates whether the property was previously occupied by a spouse.</param>
        /// <returns>The corresponding VABorrowerCertificationOccupancy value.</returns>
        public static VABorrowerCertificationOccupancyBase GetVABorrowerCertificationOccupancyBaseValue(bool isHome, bool isHomeForSpouse, bool isPrevAsHome, bool isPrevAsHomeForSpouse)
        {
            if (isHome)
            {
                return VABorrowerCertificationOccupancyBase.A;
            }
            else if (isHomeForSpouse)
            {
                return VABorrowerCertificationOccupancyBase.B;
            }
            else if (isPrevAsHome)
            {
                return VABorrowerCertificationOccupancyBase.C;
            }
            else if (isPrevAsHomeForSpouse)
            {
                return VABorrowerCertificationOccupancyBase.D;
            }
            else
            {
                return VABorrowerCertificationOccupancyBase.Blank;
            }
        }

        /// <summary>
        /// Creates a MISMO enumeration representing a VA loan procedure type.
        /// </summary>
        /// <param name="procedure">The loan procedure type.</param>
        /// <returns>A serializable MISMO enumeration.</returns>
        public static MISMOEnum<VALoanProcedureBase> ToVALoanProcedureEnum(VALoanProcedureBase procedure)
        {
            var loanProcedure = new MISMOEnum<VALoanProcedureBase>();
            loanProcedure.enumValue = procedure;
            loanProcedure.IsSensitive = false;

            return loanProcedure;
        }

        /// <summary>
        /// Converts LQB values into a serializable <see cref="MISMOEnum{FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase}" /> object.
        /// </summary>
        /// <param name="awareAtContractSigning">Indicates whether the borrower was aware of the property value at contract signing.</param>
        /// <param name="notAwareAtContractSigning">Indicates whether the borrower was not aware of the property value at contract signing.</param>
        /// <returns>A <see cref="MISMOEnum{FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase}"/> object with value calculated from the inputs.</returns>
        public static MISMOEnum<FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase> ToFHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueEnum(bool awareAtContractSigning, bool notAwareAtContractSigning)
        {
            var borrowerCertificationSalesPriceExceedsAppraisedValueEnum = new MISMOEnum<FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase>();
            borrowerCertificationSalesPriceExceedsAppraisedValueEnum.enumValue = GetFHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBaseValue(awareAtContractSigning, notAwareAtContractSigning);
            borrowerCertificationSalesPriceExceedsAppraisedValueEnum.IsSensitive = false;

            if (borrowerCertificationSalesPriceExceedsAppraisedValueEnum.enumValue == FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.Blank)
            {
                return null;
            }
            else
            {
                return borrowerCertificationSalesPriceExceedsAppraisedValueEnum;
            }
        }

        /// <summary>
        /// Maps LQB values to a base value for a VA "Borrower Certification Sales Price Exceeds Appraised Value" enumerated type.
        /// </summary>
        /// <param name="awareAtContractSigning">Indicates whether the borrower was aware of the property value at contract signing.</param>
        /// <param name="notAwareAtContractSigning">Indicates whether the borrower was not aware of the property value at contract signing.</param>
        /// <returns>An enumerated value based on the input.</returns>
        public static FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase GetFHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBaseValue(bool awareAtContractSigning, bool notAwareAtContractSigning)
        {
            if (awareAtContractSigning)
            {
                return FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.A;
            }
            else if (notAwareAtContractSigning)
            {
                return FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.B;
            }
            else
            {
                return FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.Blank;
            }
        }

        /// <summary>
        /// Converts an LQB cash to close calculation method enumeration to a serializable value.
        /// </summary>
        /// <param name="calculationMethod">The LQB enumeration value.</param>
        /// <returns>A serializable <see cref="MISMOEnum{CashToCloseCalculationMethodBase}"/>.</returns>
        public static MISMOEnum<CashToCloseCalculationMethodBase> ToCalculationMethodType(E_CashToCloseCalcMethodT calculationMethod)
        {
            var calculationMethodEnum = new MISMOEnum<CashToCloseCalculationMethodBase>();

            switch (calculationMethod)
            {
                case E_CashToCloseCalcMethodT.Standard:
                    calculationMethodEnum.enumValue = CashToCloseCalculationMethodBase.Standard;
                    break;
                case E_CashToCloseCalcMethodT.Alternative:
                    calculationMethodEnum.enumValue = CashToCloseCalculationMethodBase.Alternative;
                    break;
                default:
                    throw new UnhandledEnumException(calculationMethod);
            }

            calculationMethodEnum.IsSensitive = false;

            return calculationMethodEnum;
        }

        /// <summary>
        /// Retrieves the VA title and vesting value based on the names listed on the title.
        /// </summary>
        /// <param name="vestingTypes">A list of vestingTypes on the title for each application.</param>
        /// <returns>The corresponding <see cref="MISMOEnum{VATitleVestingBase}"/> value.</returns>
        public static MISMOEnum<VATitleVestingBase> ToVATitleVestingEnum(List<E_aVaVestTitleT> vestingTypes)
        {
            var titleVestingEnum = new MISMOEnum<VATitleVestingBase>();

            VATitleVestingBase titleVestingBaseValue;
            if (vestingTypes.Any(type => type == E_aVaVestTitleT.VeteranAndSpouse))
            {
                // If any apps include a spouse, we override with this value.
                titleVestingBaseValue = VATitleVestingBase.VeteranAndSpouse;
            }
            else if (vestingTypes.Count == 1 && vestingTypes.First() == E_aVaVestTitleT.Veteran)
            {
                titleVestingBaseValue = VATitleVestingBase.Veteran;
            }
            else if (vestingTypes.Count(type => type == E_aVaVestTitleT.Veteran) > 1)
            {
                titleVestingBaseValue = VATitleVestingBase.JointTwoOrMoreVeterans;
            }
            else
            {
                titleVestingBaseValue = VATitleVestingBase.Other;
            }

            titleVestingEnum.enumValue = titleVestingBaseValue;
            titleVestingEnum.IsSensitive = false;
            return titleVestingEnum;
        }

        /// <summary>
        /// Creates a serializable MISMO enum containining a construction method.
        /// </summary>
        /// <param name="method">The construction method.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<ConstructionMethodBase> ToConstructionMethodEnum(sHmdaConstructionMethT method)
        {
            if (method == sHmdaConstructionMethT.LeaveBlank)
            {
                return null;
            }

            var constructionMethodEnum = new MISMOEnum<ConstructionMethodBase>();
            constructionMethodEnum.enumValue = GetConstructionMethodBaseValue(method);
            constructionMethodEnum.IsSensitive = false;

            return constructionMethodEnum;
        }

        /// <summary>
        /// Gets the construction method type.
        /// </summary>
        /// <param name="method">The construction method.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static ConstructionMethodBase GetConstructionMethodBaseValue(sHmdaConstructionMethT method)
        {
            switch (method)
            {
                case sHmdaConstructionMethT.ManufacturedHome:
                    return ConstructionMethodBase.Manufactured;
                case sHmdaConstructionMethT.SiteBuilt:
                    return ConstructionMethodBase.SiteBuilt;
                default:
                    throw new UnhandledEnumException(method);
            }
        }

        /// <summary>
        /// Creates a MISMO enum containing the HOEPA status.
        /// </summary>
        /// <param name="hoepaStatus">The HOEPA status of the loan.</param>
        /// <returns>A MISMO enum with the corresponding value.</returns>
        public static MISMOEnum<LqbHmdaHoepaLoanStatusBase> ToLqbHmdaHoepaLoanStatusEnum(sHmdaHoepaStatusT hoepaStatus)
        {
            if (hoepaStatus == sHmdaHoepaStatusT.LeaveBlank)
            {
                return null;
            }

            var statusEnum = new MISMOEnum<LqbHmdaHoepaLoanStatusBase>();
            statusEnum.enumValue = GetLqbHmdaHoepaLoanStatusBaseValue(hoepaStatus);
            statusEnum.IsSensitive = false;

            return statusEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the HOEPA status.
        /// </summary>
        /// <param name="hoepaStatus">The loan's HOEPA status.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaHoepaLoanStatusBase GetLqbHmdaHoepaLoanStatusBaseValue(sHmdaHoepaStatusT hoepaStatus)
        {
            switch (hoepaStatus)
            {
                case sHmdaHoepaStatusT.HighCost:
                    return LqbHmdaHoepaLoanStatusBase.HighCost;
                case sHmdaHoepaStatusT.NotHighCost:
                    return LqbHmdaHoepaLoanStatusBase.NotHighCost;
                case sHmdaHoepaStatusT.NA:
                    return LqbHmdaHoepaLoanStatusBase.NotApplicable;
                default:
                    throw new UnhandledEnumException(hoepaStatus);
            }
        }

        /// <summary>
        /// Creates a serializable MISMO enum containing the lien priority.
        /// </summary>
        /// <param name="lienPriority">The lien type.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<LqbLienPriorityBase> ToLqbLienPriorityEnum(E_sHmdaLienT lienPriority)
        {
            if (lienPriority == E_sHmdaLienT.LeaveBlank)
            {
                return null;
            }

            var lienPriorityEnum = new MISMOEnum<LqbLienPriorityBase>();
            lienPriorityEnum.enumValue = GetLqbLienPriorityBaseValue(lienPriority);
            lienPriorityEnum.IsSensitive = false;

            return lienPriorityEnum;
        }

        /// <summary>
        /// Gets the MISMO lien priority.
        /// </summary>
        /// <param name="lienPriority">The lien type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbLienPriorityBase GetLqbLienPriorityBaseValue(E_sHmdaLienT lienPriority)
        {
            switch (lienPriority)
            {
                case E_sHmdaLienT.FirstLien:
                    return LqbLienPriorityBase.FirstLien;
                case E_sHmdaLienT.SubordinateLien:
                    return LqbLienPriorityBase.SubordinateLien;
                case E_sHmdaLienT.NotSecuredByLien:
                    return LqbLienPriorityBase.NotSecuredByLien;
                case E_sHmdaLienT.NotApplicablePurchaseLoan:
                    return LqbLienPriorityBase.NotApplicable;
                default:
                    throw new UnhandledEnumException(lienPriority);
            }
        }

        /// <summary>
        /// Creates an extension enum object with a credit score model name.
        /// </summary>
        /// <param name="modelType">The credit score model type.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<LqbHmdaCreditScoreModelNameBase> ToLqbCreditScoreModelNameEnum(sHmdaCreditScoreModelT modelType)
        {
            if (modelType == sHmdaCreditScoreModelT.LeaveBlank)
            {
                return null;
            }

            var modelEnum = new MISMOEnum<LqbHmdaCreditScoreModelNameBase>();
            modelEnum.enumValue = GetLqbCreditScoreModelNameBaseValue(modelType);
            modelEnum.IsSensitive = true;

            return modelEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the credit score model type.
        /// </summary>
        /// <param name="modelType">The credit score model type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaCreditScoreModelNameBase GetLqbCreditScoreModelNameBaseValue(sHmdaCreditScoreModelT modelType)
        {
            switch (modelType)
            {
                case sHmdaCreditScoreModelT.EquifaxBeacon5:
                    return LqbHmdaCreditScoreModelNameBase.EquifaxBeacon5;
                case sHmdaCreditScoreModelT.ExperianFairIsaac:
                    return LqbHmdaCreditScoreModelNameBase.ExperianFairIsaac;
                case sHmdaCreditScoreModelT.FicoRiskScoreClassic04:
                    return LqbHmdaCreditScoreModelNameBase.FicoRiskScoreClassic04;
                case sHmdaCreditScoreModelT.FicoRiskScoreClassic98:
                    return LqbHmdaCreditScoreModelNameBase.FicoRiskScoreClassic98;
                case sHmdaCreditScoreModelT.MoreThanOneScoringModel:
                    return LqbHmdaCreditScoreModelNameBase.MoreThanOneScoringModel;
                case sHmdaCreditScoreModelT.NotApplicable:
                    return LqbHmdaCreditScoreModelNameBase.NotApplicable;
                case sHmdaCreditScoreModelT.Other:
                    return LqbHmdaCreditScoreModelNameBase.Other;
                case sHmdaCreditScoreModelT.VantageScore2:
                    return LqbHmdaCreditScoreModelNameBase.VantageScore2;
                case sHmdaCreditScoreModelT.VantageScore3:
                    return LqbHmdaCreditScoreModelNameBase.VantageScore3;
                case sHmdaCreditScoreModelT.NoCoApplicant:
                    return LqbHmdaCreditScoreModelNameBase.NoCoApplicant;
                default:
                    throw new UnhandledEnumException(modelType);
            }
        }

        /// <summary>
        /// Creates a MISMO enum containing a property interest.
        /// </summary>
        /// <param name="interestType">The property interest for land on which manufactured home sits.</param>
        /// <returns>A MISMO enum.</returns>
        public static MISMOEnum<LqbManufacturedHomeLandPropertyInterestBase> ToLqbManufacturedHomeLandPropertyInterestEnum(sHmdaManufacturedInterestT interestType)
        {
            if (interestType == sHmdaManufacturedInterestT.LeaveBlank)
            {
                return null;
            }

            var interestTypeEnum = new MISMOEnum<LqbManufacturedHomeLandPropertyInterestBase>();
            interestTypeEnum.enumValue = GetLqbManufacturedHomeLandPropertyInterestBaseValue(interestType);
            interestTypeEnum.IsSensitive = false;

            return interestTypeEnum;
        }

        /// <summary>
        /// Gets the property interest value.
        /// </summary>
        /// <param name="interestType">The property interest type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbManufacturedHomeLandPropertyInterestBase GetLqbManufacturedHomeLandPropertyInterestBaseValue(sHmdaManufacturedInterestT interestType)
        {
            switch (interestType)
            {
                case sHmdaManufacturedInterestT.DirectOwnership:
                    return LqbManufacturedHomeLandPropertyInterestBase.DirectOwnership;
                case sHmdaManufacturedInterestT.IndirectOwnership:
                    return LqbManufacturedHomeLandPropertyInterestBase.IndirectOwnership;
                case sHmdaManufacturedInterestT.NA:
                    return LqbManufacturedHomeLandPropertyInterestBase.NotApplicable;
                case sHmdaManufacturedInterestT.PaidLeasehold:
                    return LqbManufacturedHomeLandPropertyInterestBase.PaidLeasehold;
                case sHmdaManufacturedInterestT.UnpaidLeasehold:
                    return LqbManufacturedHomeLandPropertyInterestBase.UnpaidLeasehold;
                default:
                    throw new UnhandledEnumException(interestType);
            }
        }

        /// <summary>
        /// Creates a MISMO enum containing the secured property type.
        /// </summary>
        /// <param name="securedPropertyType">The manufactured home property type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static MISMOEnum<LqbManufacturedHomeSecuredPropertyBase> ToLqbManufacturedHomeSecuredPropertyEnum(sHmdaManufacturedTypeT securedPropertyType)
        {
            if (securedPropertyType == sHmdaManufacturedTypeT.LeaveBlank)
            {
                return null;
            }

            var securedPropertyTypeEnum = new MISMOEnum<LqbManufacturedHomeSecuredPropertyBase>();
            securedPropertyTypeEnum.enumValue = GetLqbManufacturedHomeSecuredPropertyBaseValue(securedPropertyType);
            securedPropertyTypeEnum.IsSensitive = false;

            return securedPropertyTypeEnum;
        }

        /// <summary>
        /// Gets the secured property value.
        /// </summary>
        /// <param name="securedPropertyType">The manufactured home property type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbManufacturedHomeSecuredPropertyBase GetLqbManufacturedHomeSecuredPropertyBaseValue(sHmdaManufacturedTypeT securedPropertyType)
        {
            switch (securedPropertyType)
            {
                case sHmdaManufacturedTypeT.ManufacturedHomeAndLand:
                    return LqbManufacturedHomeSecuredPropertyBase.ManufacturedHomeAndLand;
                case sHmdaManufacturedTypeT.ManufacturedHomeAndNotLand:
                    return LqbManufacturedHomeSecuredPropertyBase.ManufacturedHomeAndNotLand;
                case sHmdaManufacturedTypeT.NA:
                    return LqbManufacturedHomeSecuredPropertyBase.NotApplicable;
                default:
                    throw new UnhandledEnumException(securedPropertyType);
            }
        }

        /// <summary>
        /// Creates a MISMO enum containing the relied-on credit score mode.
        /// </summary>
        /// <param name="mode">The relied-on credit score mode.</param>
        /// <returns>A MISMO enum.</returns>
        public static MISMOEnum<LqbReliedOnCreditScoreModeBase> ToLqbReliedOnCreditScoreModeEnum(ReliedOnCreditScoreMode mode)
        {
            if (mode == ReliedOnCreditScoreMode.Blank)
            {
                return null;
            }

            var modeEnum = new MISMOEnum<LqbReliedOnCreditScoreModeBase>();
            modeEnum.enumValue = GetLqbReliedOnCreditScoreModeBaseValue(mode);
            modeEnum.IsSensitive = false;

            return modeEnum;
        }

        /// <summary>
        /// Gets the relied-on credit score mode.
        /// </summary>
        /// <param name="mode">The relied-on credit score mode.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbReliedOnCreditScoreModeBase GetLqbReliedOnCreditScoreModeBaseValue(ReliedOnCreditScoreMode mode)
        {
            switch (mode)
            {
                case ReliedOnCreditScoreMode.ApplicantIndividualScore:
                    return LqbReliedOnCreditScoreModeBase.ApplicantsIndividualScore;
                case ReliedOnCreditScoreMode.SingleScore:
                    return LqbReliedOnCreditScoreModeBase.SingleScore;
                default:
                    throw new UnhandledEnumException(mode);
            }
        }

        /// <summary>
        /// Creates a MISMO enum object containing an AUS type.
        /// </summary>
        /// <param name="aus">The AUS used to underwrite the loan.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<LqbHmdaAutomatedUnderwritingSystemBase> ToLqbHmdaAutomatedUnderwritingSystemEnum(AUSType aus)
        {
            if (aus == AUSType.Blank)
            {
                return null;
            }

            var ausEnum = new MISMOEnum<LqbHmdaAutomatedUnderwritingSystemBase>();
            ausEnum.enumValue = GetLqbHmdaAutomatedUnderwritingSystemBaseValue(aus);
            ausEnum.IsSensitive = false;

            return ausEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the AUS type.
        /// </summary>
        /// <param name="aus">The AUS used to underwrite the loan.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaAutomatedUnderwritingSystemBase GetLqbHmdaAutomatedUnderwritingSystemBaseValue(AUSType aus)
        {
            switch (aus)
            {
                case AUSType.DesktopUnderwriter:
                    return LqbHmdaAutomatedUnderwritingSystemBase.DesktopUnderwriter;
                case AUSType.GUS:
                    return LqbHmdaAutomatedUnderwritingSystemBase.GuaranteedUnderwritingSystem;
                case AUSType.LoanProspector:
                    return LqbHmdaAutomatedUnderwritingSystemBase.LoanProspector;
                case AUSType.TotalScorecard:
                    return LqbHmdaAutomatedUnderwritingSystemBase.TotalScorecard;
                case AUSType.NotApplicable:
                    return LqbHmdaAutomatedUnderwritingSystemBase.NotApplicable;
                case AUSType.Other:
                    return LqbHmdaAutomatedUnderwritingSystemBase.Other;
                default:
                    throw new UnhandledEnumException(aus);
            }
        }

        /// <summary>
        /// Creates a MISMO object containing an AUS result.
        /// </summary>
        /// <param name="result">The AUS result.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static MISMOEnum<LqbHmdaAutomatedUnderwritingSystemResult> ToLqbHmdaAutomatedUnderwritingSystemResultEnum(AUSResult result)
        {
            if (result == AUSResult.Blank)
            {
                return null;
            }

            var resultEnum = new MISMOEnum<LqbHmdaAutomatedUnderwritingSystemResult>();
            resultEnum.enumValue = GetLqbHmdaAutomatedUnderwritingSystemResultBaseValue(result);
            resultEnum.IsSensitive = false;

            return resultEnum;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to an AUS result.
        /// </summary>
        /// <param name="result">The AUS result.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static LqbHmdaAutomatedUnderwritingSystemResult GetLqbHmdaAutomatedUnderwritingSystemResultBaseValue(AUSResult result)
        {
            switch (result)
            {
                case AUSResult.Accept:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Accept;
                case AUSResult.ApproveEligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ApproveEligible;
                case AUSResult.ApproveIneligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ApproveIneligible;
                case AUSResult.Caution:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Caution;
                case AUSResult.Eligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Eligible;
                case AUSResult.Error:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Error;
                case AUSResult.Incomplete:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Incomplete;
                case AUSResult.Ineligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Ineligible;
                case AUSResult.Invalid:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Invalid;
                case AUSResult.OutOfScope:
                    return LqbHmdaAutomatedUnderwritingSystemResult.OutOfScope;
                case AUSResult.Refer:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Refer;
                case AUSResult.ReferCaution:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ReferCaution;
                case AUSResult.ReferEligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ReferEligible;
                case AUSResult.ReferIneligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ReferIneligible;
                case AUSResult.UnableToDetermine:
                    return LqbHmdaAutomatedUnderwritingSystemResult.UnableToDetermine;
                case AUSResult.Other:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Other;
                case AUSResult.NotApplicable:
                    return LqbHmdaAutomatedUnderwritingSystemResult.NotApplicable;
                default:
                    throw new UnhandledEnumException(result);
            }
        }

        /// <summary>
        /// Creates a MISMO enum indicating the ATR type.
        /// </summary>
        /// <param name="isExemptFromAtr">Indicates whether the loan is exempt from ATR.</param>
        /// <param name="qmStatus">The QM status of the loan.</param>
        /// <returns>A serializable MISMO enum.</returns>
        public static MISMOEnum<AbilityToRepayMethodBase> ToAbilityToRepayMethodEnum(bool isExemptFromAtr, E_sQMStatusT qmStatus)
        {
            if (!isExemptFromAtr && qmStatus == E_sQMStatusT.Blank)
            {
                return null;
            }

            var mismoEnum = new MISMOEnum<AbilityToRepayMethodBase>();
            mismoEnum.enumValue = GetAbilityToRepayMethodBaseValue(isExemptFromAtr, qmStatus);
            mismoEnum.IsSensitive = false;
            return mismoEnum;
        }

        /// <summary>
        /// Gets the MISMO value indicating the ATR type.
        /// </summary>
        /// <param name="isExemptFromAtr">Indicates whether the loan is exempt from ATR.</param>
        /// <param name="qmStatus">The QM status of the loan.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static AbilityToRepayMethodBase GetAbilityToRepayMethodBaseValue(bool isExemptFromAtr, E_sQMStatusT qmStatus)
        {
            if (isExemptFromAtr)
            {
                return AbilityToRepayMethodBase.Exempt;
            }

            switch (qmStatus)
            {
                case E_sQMStatusT.Eligible:
                    return AbilityToRepayMethodBase.QualifiedMortgage;
                case E_sQMStatusT.Ineligible:
                    return AbilityToRepayMethodBase.General;
                case E_sQMStatusT.ProvisionallyEligible:
                    return AbilityToRepayMethodBase.QualifiedMortgage;
                default:
                    throw new UnhandledEnumException(qmStatus);
            }
        }

        /// <summary>
        /// Retrieves the underwriting decision date for the given loan.
        /// </summary>
        /// <param name="approvalDate">The approval date for the loan.</param>
        /// <param name="approvalDateString">A string representation of the approval date.</param>
        /// <param name="rejectionDate">The rejection date for the loan.</param>
        /// <param name="rejectionDateString">A string representation of the rejection date.</param>
        /// <returns>The underwriting decision date.</returns>
        public static string GetUnderwritingDecisionDate(CDateTime approvalDate, string approvalDateString, CDateTime rejectionDate, string rejectionDateString)
        {
            if (approvalDate.IsValid)
            {
                return (approvalDate.CompareTo(rejectionDate, true) >= 0) ? approvalDateString : rejectionDateString;
            }
            else if (rejectionDate.IsValid)
            {
                return rejectionDateString;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Sets the disclosure section for Section K adjustments based on the current document package
        /// type and the calculation method for the package.
        /// </summary>
        /// <param name="isClosingPackage">Indicates whether a closing package is being requested.</param>
        /// <param name="loanEstimateCalculationType">The LE calculation type (sTRIDLoanEstimateCashToCloseCalcMethodT).</param>
        /// <param name="closingDisclosureCalculationType">The CD calculation type (sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodD).</param>
        /// <returns>An IntegratedDisclosureSectionBase enum.</returns>
        public static IntegratedDisclosureSectionBase GetDisclosureSectionForSectionKAdjustments(bool isClosingPackage, E_CashToCloseCalcMethodT loanEstimateCalculationType, E_CashToCloseCalcMethodT closingDisclosureCalculationType)
        {
            var disclosureSectionForSectionK = IntegratedDisclosureSectionBase.PayoffsAndPayments;

            if ((isClosingPackage && closingDisclosureCalculationType == E_CashToCloseCalcMethodT.Standard)
                || (!isClosingPackage && loanEstimateCalculationType == E_CashToCloseCalcMethodT.Standard))
            {
                disclosureSectionForSectionK = IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing;
            }

            return disclosureSectionForSectionK;
        }

        /// <summary>
        /// Indicates whether TRID 2017 subordinate financing data should be exported.
        /// </summary>
        /// <param name="lienPosition">The lien position.</param>
        /// <param name="isOtherFinancingNew">Indicates whether other financing is new.</param>
        /// <param name="concurrentSubordinateFinancing">The amount of concurrent subordinate financing.</param>
        /// <param name="regulationVersion">The loan regulation version.</param>
        /// <returns>A boolean indicating whether TRID 2017 subordinate financing data should be exported.</returns>
        public static bool ShouldSendTrid2017SubordinateFinancingData(E_sLienPosT lienPosition, bool isOtherFinancingNew, decimal concurrentSubordinateFinancing, TridTargetRegulationVersionT regulationVersion)
        {
            if (regulationVersion == TridTargetRegulationVersionT.TRID2017)
            {
                if (lienPosition == E_sLienPosT.First)
                {
                    if (isOtherFinancingNew && concurrentSubordinateFinancing > 0)
                    {
                        return true;
                    }
                }
                else if (lienPosition == E_sLienPosT.Second)
                {
                    return true;
                }
            }

            return false;
        }
    }
}