﻿// <copyright file="Mismo33RequestProvider.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara, Isaac Ribakoff
//  Date:   January 13, 2015
// </summary>
namespace LendersOffice.Conversions.Mismo3.Version3
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.Core.Construction;
    using DataAccess.Trust;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.ObjLib.Rolodex;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using Mismo3Specification;
    using Mismo3Specification.Version3Schema;
    using ObjLib;
    using ObjLib.DocMagicLib;
    using ObjLib.TRID2;
    using static Mismo33DataMapper;
    using static Mismo3Specification.Mismo3Utilities;
    using static MismoTypeConverter;

    /// <summary>
    /// Serializes a MISMO 3.3 request to an XML string which can be used in various integrations.
    /// </summary>
    public class Mismo33RequestProvider
    {
        /// <summary>
        /// Holds the lazy instance of the select statement provider for this class.
        /// </summary>
        private static Lazy<CSelectStatementProvider> lazySelectStatementProvider = new Lazy<CSelectStatementProvider>(CreateSelectStatementProvider, System.Threading.LazyThreadSafetyMode.ExecutionAndPublication);

        /// <summary>
        /// Holds the data of the loan associated with the request.
        /// </summary>
        private CPageData loanData;

        /// <summary>
        /// Holds a copy of the loan estimate data-set from the archive.
        /// </summary>
        private CPageData loanEstimateArchiveData;

        /// <summary>
        /// Holds a copy of the closing disclosure data-set from the archive.
        /// </summary>
        private CPageData closingDisclosureArchiveData;
        
        /// <summary>
        /// Indicates whether an integrated disclosure archive has been loaded.
        /// </summary>
        private bool hasArchive;

        /// <summary>
        /// Indicates whether an integrated disclosure archive should be omitted.
        /// </summary>
        private bool shouldOmitArchive;

        /// <summary>
        /// Holds the data of the primary application.
        /// </summary>
        private CAppData primaryApp;

        /// <summary>
        /// A collection of sequence number counters for particular container types.
        /// </summary>
        private Mismo33Counters counters;

        /// <summary>
        /// A list of relationships that specify how object A relates to object B, where neither object contains the other. E.G. Party objects for borrowers on an application.
        /// </summary>
        private List<RELATIONSHIP> allRelationships;

        /// <summary>
        /// The $$xlink$$ labels for the loan objects in the DEAL.
        /// </summary>
        private Dictionary<string, string> loanLabel;
        
        /// <summary>
        /// The $$xlink$$ labels for real estate owned ASSET objects in the DEAL.
        /// </summary>
        private Dictionary<string, string> reoAssetLabel;

        /// <summary>
        /// A manager for the $$xlink$$ labels related to each party in the loan.
        /// </summary>
        private PartyLabelManager partyLabels;

        /// <summary>
        /// An instance of the StateInfo object with US states, counties, and FIPS codes.
        /// </summary>
        private StateInfo statesAndTerritories;

        /// <summary>
        /// An employee database object with the user account information.
        /// </summary>
        private EmployeeDB user = null;

        /// <summary>
        /// The lender's account ID with the vendor to receive the MISMO message. Or their LQB customer code if an account ID is not required by the vendor.
        /// </summary>
        private string vendorAccountID;

        /// <summary>
        /// The transaction ID associated with the MISMO message.
        /// </summary>
        private string transactionID;

        /// <summary>
        /// True if the <see cref="INTEGRATED_DISCLOSURE"/> container ought to be extended to include fee information. Otherwise false.
        /// </summary>
        private bool extendIntegratedDisclosure;

        /// <summary>
        /// True if this request is for a closing package. Otherwise false.
        /// </summary>
        private bool isClosingPackage;

        /// <summary>
        /// Tells which vendor is the target of the request, for the purposes of vendor-specific conditions.
        /// </summary>
        private E_DocumentVendor vendor;

        /// <summary>
        /// A list of fees to exclude from the payload. Varies by vendor.
        /// </summary>
        private List<Guid> feeIgnoreList;

        /// <summary>
        /// True if request provider is operating in lender export mode.
        /// If true, prevents certain exceptions from being thrown.
        /// </summary>
        private bool isLenderExportMode;

        /// <summary>
        /// Initializes a new instance of the <see cref="Mismo33RequestProvider" /> class, which
        /// is capable of creating and serializing a request.
        /// </summary>
        /// <param name="loanIdentifier">The unique identifier for the loan to be exported.</param>
        /// <param name="vendorAccountID">The lender's account ID with the vendor to receive the MISMO message. Null/empty if there is no target vendor or if they do not require a lender-level account ID.</param>
        /// <param name="transactionID">The transaction ID associated with the message. Null/empty if there is no such ID.</param>
        /// <param name="docPackage">The document package, if this is a document framework request. Null otherwise.</param>
        /// <param name="includeIntegratedDisclosureExtension">True if the export should include an extension to the <see cref="INTEGRATED_DISCLOSURE"/> container with fee information. Otherwise false.</param>
        /// <param name="useTemporaryArchive">True if the temporary archive should be used in place of the last disclosed archive. Only expected to be true for seamless.</param>
        /// <param name="principal">Abstract User Principal object.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="isLenderExportMode">True if in lender export mode.</param>
        protected Mismo33RequestProvider(Guid loanIdentifier, string vendorAccountID, string transactionID, VendorConfig.DocumentPackage docPackage, bool includeIntegratedDisclosureExtension, bool useTemporaryArchive, AbstractUserPrincipal principal, E_DocumentVendor vendor = E_DocumentVendor.UnknownOtherNone, bool isLenderExportMode = false)
        {
            if (docPackage == null)
            {
                docPackage = VendorConfig.DocumentPackage.InvalidPackage;
            }

            this.loanData = this.InitPageData(loanIdentifier);

            if (this.loanData.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                this.loanEstimateArchiveData = this.InitPageData(loanIdentifier);

                this.closingDisclosureArchiveData = this.InitPageData(loanIdentifier);

                if (useTemporaryArchive)
                {
                    this.loanData.ApplyTemporaryArchive(principal);
                }
                else if (!docPackage.IsOfType(VendorConfig.PackageType.Closing) && !docPackage.IsUndefinedType && this.loanData.sHasLastDisclosedLoanEstimateArchive)
                {
                    this.loanData.ApplyClosingCostArchive(this.loanData.sLastDisclosedClosingCostArchive);
                }
            }
            
            this.isClosingPackage = docPackage.IsOfType(VendorConfig.PackageType.Closing)
                || (vendor == E_DocumentVendor.UnknownOtherNone && this.loanData.sHasClosingDisclosureArchive);

            this.primaryApp = this.loanData.GetAppData(0);
            this.primaryApp.BorrowerModeT = E_BorrowerModeT.Borrower;

            this.counters = new Mismo33Counters();
            this.allRelationships = new List<RELATIONSHIP>();
            this.loanLabel = new Dictionary<string, string>();
            this.partyLabels = new PartyLabelManager();
            this.reoAssetLabel = new Dictionary<string, string>();
            this.statesAndTerritories = StateInfo.Instance;
            this.hasArchive = false;

            this.vendorAccountID = string.IsNullOrEmpty(vendorAccountID) ? this.loanData.BrokerDB.CustomerCode : vendorAccountID;
            this.transactionID = transactionID;
            this.extendIntegratedDisclosure = includeIntegratedDisclosureExtension;
            this.vendor = vendor;

            this.isLenderExportMode = isLenderExportMode;

            // 4/23/2015 BB - If the MISMO is being called by our automation, e.g. compliance queue, then there is only a hard-coded system user.
            if (PrincipalFactory.CurrentPrincipal != null && !(PrincipalFactory.CurrentPrincipal is SystemUserPrincipal))
            {
                this.user = new EmployeeDB(PrincipalFactory.CurrentPrincipal.EmployeeId, PrincipalFactory.CurrentPrincipal.BrokerId);
                this.user.Retrieve();
            }

            this.PopulateFeeIgnoreList();
        }

        /// <summary>
        /// The select statement provider for this class.
        /// </summary>
        public static CSelectStatementProvider SelectStatementProvider => lazySelectStatementProvider.Value;

        /// <summary>
        /// Converts LQB values from the loan data to an FHA/VA loan purpose type.
        /// </summary>
        /// <remarks>
        /// Converts a set of booleans into a single enum, without checking them for mutual exclusivity. This may lead to unexpected behavior.
        /// </remarks>
        /// <param name="loanData">The loan data from which to pull the LQB values.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FHA_VALoanPurposeBase}"/>.</returns>
        public static MISMOEnum<FHA_VALoanPurposeBase> ToFHA_VALoanPurposeType(CPageData loanData)
        {
            var loanPurposeEnum = new MISMOEnum<FHA_VALoanPurposeBase>();

            if (loanData.sFHAPurposeIsPurchaseExistHome)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.PurchaseExistingHomePreviouslyOccupied;
            }
            else if (loanData.sFHAPurposeIsFinanceImprovement)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.FinanceImprovementsToExistingProperty;
            }
            else if (loanData.sFHAPurposeIsRefinance)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.Refinance;
            }
            else if (loanData.sFHAPurposeIsPurchaseNewCondo)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.PurchaseNewCondominiumUnit;
            }
            else if (loanData.sFHAPurposeIsPurchaseExistCondo)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.PurchaseExistingCondominiumUnit;
            }
            else if (loanData.sFHAPurposeIsPurchaseNewHome)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.PurchaseExistingHomeNotPreviouslyOccupied;
            }
            else if (loanData.sFHAPurposeIsConstructHome)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.ConstructHome;
            }
            else if (loanData.sFHAPurposeIsFinanceCoopPurchase)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.FinanceCooperativePurchase;
            }
            else if (loanData.sFHAPurposeIsPurchaseManufacturedHome)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.PurchasePermanentlySitedManufacturedHome;
            }
            else if (loanData.sFHAPurposeIsManufacturedHomeAndLot)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.PurchasePermanentlySitedManufacturedHomeAndLot;
            }
            else if (loanData.sFHAPurposeIsRefiManufacturedHomeToBuyLot)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.RefinancePermanentlySitedManufacturedHomeToBuyLot;
            }
            else if (loanData.sFHAPurposeIsRefiManufacturedHomeOrLotLoan)
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.RefinancePermanentlySitedManufacturedHomeLotLoan;
            }
            else
            {
                loanPurposeEnum.enumValue = FHA_VALoanPurposeBase.Blank;
                return null;
            }

            loanPurposeEnum.IsSensitive = false;

            return loanPurposeEnum;
        }

        /// <summary>
        /// Entry point for the MISMO 3.3 Exporter. Serializes the request to a string.
        /// </summary>
        /// <param name="loanIdentifier">The unique identifier for the loan to be exported.</param>
        /// <param name="vendorAccountID">The lender's account ID with the vendor to receive the MISMO message. Null/empty if there is no target vendor or if they do not require a lender-level account ID.</param>
        /// <param name="transactionID">The transaction ID associated with the message. Null/empty if there is no such ID.</param>
        /// <param name="docPackage">The document package, if this is a document framework request. Null otherwise.</param>
        /// <param name="includeIntegratedDisclosureExtension">True if the export should include an extension to the <see cref="INTEGRATED_DISCLOSURE"/> container with fee information. Otherwise false.</param>
        /// <param name="principal">Abstract User Principal object.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="useTemporaryArchive">True if the temporary archive should be used in place of the last disclosed archive. Only expected to be true for seamless.</param>
        /// <param name="useMismo3DefaultNamespace">Indicates whether to use the MISMO 3 default namespace for the MEESAGE element.</param>
        /// <param name="isLenderExportMode">True if in lender export mode.</param>
        /// <returns>A string representation of the MISMO 3.3 request.</returns>
        public static string SerializeMessage(
            Guid loanIdentifier, 
            string vendorAccountID, 
            string transactionID, 
            VendorConfig.DocumentPackage docPackage,  
            bool includeIntegratedDisclosureExtension,
            AbstractUserPrincipal principal,
            E_DocumentVendor vendor = E_DocumentVendor.UnknownOtherNone,
            bool useTemporaryArchive = false,
            bool useMismo3DefaultNamespace = false,
            bool isLenderExportMode = false)
        {
            Mismo33RequestProvider provider = new Mismo33RequestProvider(loanIdentifier, vendorAccountID, transactionID, docPackage, includeIntegratedDisclosureExtension, useTemporaryArchive, principal, vendor, isLenderExportMode);
            
            return Mismo33RequestProvider.SerializeMessage(provider, useMismo3DefaultNamespace);
        }

        /// <summary>
        /// Creates a container holding a set of taxpayer identifiers.
        /// </summary>
        /// <param name="identifier">The taxpayer identifier number.</param>
        /// <param name="type">The taxpayer identifier type.</param>
        /// <param name="displaySensitive">Indicates whether the contained taxpayer identifier and identifier type should display their "SensitiveIndicator" attribute.</param>
        /// <returns>A TAXPAYER_IDENTIFIERS container.</returns>
        public static TAXPAYER_IDENTIFIERS CreateTaxpayerIdentifiers(string identifier, TaxpayerIdentifierBase type, bool displaySensitive = true)
        {
            var taxpayerIdentifiers = new TAXPAYER_IDENTIFIERS();
            taxpayerIdentifiers.TaxpayerIdentifier.Add(CreateTaxpayerIdentifier(identifier, type, 1, displaySensitive));

            return taxpayerIdentifiers;
        }

        /// <summary>
        /// Creates a container to hold up to three of a borrower's credit scores.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <param name="version">The current loan version.</param>
        /// <returns>A CREDIT_SCORES container.</returns>
        public static CREDIT_SCORES CreateCreditScores(CAppData appData, LoanVersionT version)
        {
            var creditScores = new CREDIT_SCORES();
            var isOnOrBeyondVersion26 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(version, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums);

            creditScores.CreditScore.Add(CreateCreditScore(
                scoreCreatedDateRep: appData.aEquifaxCreatedD_rep,
                scoreValueRep: appData.aEquifaxScore_rep,
                scorePercentile: appData.aEquifaxPercentile_rep,
                agencyFactors: appData.aEquifaxFactors,
                agency: CreditRepositorySourceBase.Equifax,
                model: isOnOrBeyondVersion26 ? appData.aEquifaxModelT : default(E_CreditScoreModelT?),
                modelOtherDescription: isOnOrBeyondVersion26 ? appData.aEquifaxModelTOtherDescription : null,
                sequence: 1));

            creditScores.CreditScore.Add(CreateCreditScore(
                scoreCreatedDateRep: appData.aExperianCreatedD_rep,
                scoreValueRep: appData.aExperianScore_rep,
                scorePercentile: appData.aExperianPercentile_rep,
                agencyFactors: appData.aExperianFactors,
                agency: CreditRepositorySourceBase.Experian,
                model: isOnOrBeyondVersion26 ? appData.aExperianModelT : default(E_CreditScoreModelT?),
                modelOtherDescription: isOnOrBeyondVersion26 ? appData.aExperianModelTOtherDescription : null,
                sequence: creditScores.CreditScore.Count(c => c != null) + 1));

            creditScores.CreditScore.Add(CreateCreditScore(
                scoreCreatedDateRep: appData.aTransUnionCreatedD_rep,
                scoreValueRep: appData.aTransUnionScore_rep,
                scorePercentile: appData.aTransUnionPercentile_rep,
                agencyFactors: appData.aTransUnionFactors,
                agency: CreditRepositorySourceBase.TransUnion,
                model: isOnOrBeyondVersion26 ? appData.aTransUnionModelT : default(E_CreditScoreModelT?),
                modelOtherDescription: isOnOrBeyondVersion26 ? appData.aTransUnionModelTOtherDescription : null,
                sequence: creditScores.CreditScore.Count(c => c != null) + 1));

            return creditScores;
        }

        /// <summary>
        /// Creates a container representing a credit score from a specific reporting agency.
        /// </summary>
        /// <param name="scoreValueRep">The numeric value of the credit score, as a string.</param>
        /// <param name="scorePercentile">The credit score percentile.</param>
        /// <param name="sequence">The sequence number of the credit score among the list of scores.</param>
        /// <param name="scoreCreatedDateRep">The creation date of the credit score, as a string.</param>
        /// <param name="agencyFactors">The credit reporting agency's factors behind the score.</param>
        /// <param name="agency">The credit reporting agency.</param>
        /// <param name="model">The credit scoring model type.</param>
        /// <param name="modelOtherDescription">The credit scoring model other description.</param>
        /// <returns>A CREDIT_SCORE container.</returns>
        public static CREDIT_SCORE CreateCreditScore(string scoreValueRep, string scorePercentile, int sequence, string scoreCreatedDateRep = null, string agencyFactors = null, CreditRepositorySourceBase agency = CreditRepositorySourceBase.Blank, E_CreditScoreModelT? model = null, string modelOtherDescription = null)
        {
            var creditScore = new CREDIT_SCORE();
            creditScore.SequenceNumber = sequence;

            if (!string.IsNullOrEmpty(scoreValueRep))
            {
                creditScore.CreditScoreDetail = CreateCreditScoreDetail(agency, scoreCreatedDateRep, scoreValueRep, scorePercentile, model, modelOtherDescription);
                creditScore.CreditScoreFactors = CreateCreditScoreFactors(agencyFactors);
            }

            return creditScore;
        }

        /// <summary>
        /// Serializes the message for the specified <paramref name="provider"/>.
        /// </summary>
        /// <param name="provider">The provider to generate the message.</param>
        /// <param name="useMismo3DefaultNamespace">Indicates whether to use the MISMO 3 default namespace for the MEESAGE element.</param>
        /// <returns>A string representation of the MISMO 3.3 request.</returns>
        protected static string SerializeMessage(Mismo33RequestProvider provider, bool useMismo3DefaultNamespace = false)
        {
            var message = provider.CreateMessage();

            if (useMismo3DefaultNamespace)
            {
                return SerializationHelper.XmlSerialize(message, false, "http://www.mismo.org/residential/2009/schemas");
            }
            else
            {
                return SerializationHelper.XmlSerializeStripDefaultNamespace(message, false);
            }
        }

        /// <summary>
        /// Initializes a <see cref="CPageData" /> object with data for the given loan file.
        /// Will also load the dependencies required for accessing the loan estimate/closing disclosure archive, subject to the archive parameter.
        /// </summary>
        /// <param name="loanIdentifier">The unique identifier for the loan to be exported.</param>
        /// <returns>An initialized CPageData object.</returns>
        protected virtual CPageData InitPageData(Guid loanIdentifier)
        {
            CPageData dataContainer = new CFullAccessPageData(loanIdentifier, nameof(Mismo33RequestProvider), SelectStatementProvider);
            dataContainer.InitLoad();
            dataContainer.SetFormatTarget(FormatTarget.MismoClosing);
            dataContainer.ByPassFieldSecurityCheck = true;
            return dataContainer;
        }

        /// <summary>
        /// Creates the <see cref="CSelectStatementProvider"/> for the fields used by this class.
        /// </summary>
        /// <returns>An instance of <see cref="CSelectStatementProvider"/> for loading the loan for this class.</returns>
        private static CSelectStatementProvider CreateSelectStatementProvider()
        {
            // 2018-03 tj - I tested the fields, since it seemed noteworthy that we might not need to load the archive fields all the time.
            // Right now, the only difference between the fully expanded SelectStatementProvider field list for just requestProviderFields
            // versus the Union of the three lists (archiveCdFields, archiveLeFields, requestProviderFields) is just one field:
            // sPdByBorrowerU6F.  As such, I think it's worth just simplifying everything to use the same provider.  I'm still keeping the
            // archives in this list so this is resilient to changes, but the performance cost of just loading all of them does not appear
            // to be high at this point.
            var archiveCdFields = ClosingCostArchive.GetLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.ClosingDisclosure);
            var archiveLeFields = ClosingCostArchive.GetLoanPropertyList(ClosingCostArchive.E_ClosingCostArchiveType.LoanEstimate);
            var requestProviderFields = CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(Mismo33RequestProvider));
            return CSelectStatementProvider.GetProviderForTargets(archiveCdFields.Union(archiveLeFields.Concat(requestProviderFields)), expandTriggersIfNeeded: false);
        }

        /// <summary>
        /// Creates ADDRESS container based on specified street address, city, state, zip code, and county.
        /// </summary>
        /// <param name="streetAddr">The street address.</param>
        /// <param name="city">The specified city.</param>
        /// <param name="state">The specified state abbreviation.</param>
        /// <param name="zip">The specified postal zip code.</param>
        /// <param name="county">The specified county.</param>
        /// <param name="countyFIPSCode">The $$FIPS$$ code for the county or blank/null if unknown.</param>
        /// <param name="stateFullName">The full name of the given U.S. state.</param>
        /// <param name="type">The address type.</param>
        /// <param name="sequenceNumber">The sequence number for this element.</param>
        /// <returns>Returns ADDRESS container with specified street address, city, state, and zip code.</returns>
        private static ADDRESS CreateAddress(string streetAddr, string city, string state, string zip, string county, string countyFIPSCode, string stateFullName, AddressBase type, int? sequenceNumber = null)
        {
            var address = new ADDRESS();
            address.AddressLineText = ToMismoString(streetAddr);
            address.AddressType = ToAddressEnum(type);
            address.CityName = ToMismoString(city);
            address.CountryCode = ToMismoCode(StateInfo.USACountryCode);
            address.CountryName = ToMismoString(StateInfo.USACountryName);
            address.CountyCode = ToMismoCode(countyFIPSCode);
            address.CountyName = ToMismoString(county);
            address.PostalCode = ToMismoCode(zip);
            address.StateCode = ToMismoCode(state);
            address.StateName = ToMismoString(stateFullName);

            if (sequenceNumber != null)
            {
                address.SequenceNumber = sequenceNumber.Value;
            }

            return address;
        }

        /// <summary>
        /// Creates ADDRESSES container holding one ADDRESS sub-container populated with provider's address information. 
        /// </summary>
        /// <param name="provider">The settlement service provider object from which ADDRESS information is populated.</param>
        /// <returns>Returns ADDRESSES container holding one ADDRESS sub-container populated with provider's address information.</returns>
        private static ADDRESSES CreateAddresses(SettlementServiceProvider provider)
        {
            if (provider == null)
            {
                return null;
            }

            var addresses = new ADDRESSES();
            addresses.Address.Add(
                CreateAddress(
                provider.StreetAddress,
                provider.City,
                provider.State,
                provider.Zip,
                string.Empty,
                string.Empty,
                string.Empty,
                AddressBase.Blank,
                1));

            return addresses;
        }

        /// <summary>
        /// Creates ADDRESSES container holding one ADDRESS sub-container populated with agent's address information. 
        /// </summary>
        /// <param name="agent">The agent object from which ADDRESS information is populated.</param>
        /// <returns>Returns ADDRESSES container holding one ADDRESS sub-container populated with agent's address information.</returns>
        private static ADDRESSES CreateAddresses(CAgentFields agent)
        {
            if (agent == null || !agent.IsValid || agent.IsNewRecord)
            {
                return null;
            }

            var addresses = new ADDRESSES();
            addresses.Address.Add(
                CreateAddress(
                agent.StreetAddr,
                agent.City,
                agent.State,
                agent.Zip,
                agent.County,
                string.Empty,
                string.Empty,
                AddressBase.Blank,
                1));

            return addresses;
        }

        /// <summary>
        /// Creates a CONTACT container holding contactName, email, fax, and phone.
        /// </summary>
        /// <param name="agent">Agent contact holding phone, fax, and email information.</param>
        /// <param name="contactRoleType">Enumeration representing the type of contact (work/home/mobile/etc...) provided.</param>
        /// <param name="entity">The type of entity associated with the contact.</param>
        /// <param name="sequence">The sequence number among the list of contacts.</param>
        /// <returns>Returns a CONTACT container holding contactName, email, fax, and phone.</returns>
        private static CONTACT CreateContact(CAgentFields agent, ContactPointRoleBase contactRoleType, EntityType entity, int sequence)
        {
            var contact = new CONTACT();
            contact.ContactPoints = CreateContactPoints(agent, contactRoleType, entity);
            contact.SequenceNumber = sequence;

            if (entity == EntityType.Individual)
            {
                contact.Name = CreateName(agent.AgentName, isLegalEntity: false);
            }

            return contact;
        }

        /// <summary>
        /// Creates a CONTACT container holding contactName and phone.
        /// </summary>
        /// <param name="provider">Settlement service provider holding phone and name information.</param>
        /// <param name="sequence">The sequence number among the list of contacts.</param>
        /// <returns>Returns a CONTACT container holding contactName and phone.</returns>
        private static CONTACT CreateContact(SettlementServiceProvider provider, int sequence)
        {
            var contact = new CONTACT();
            contact.ContactPoints = CreateContactPoints(provider);
            contact.SequenceNumber = sequence;

            contact.Name = CreateName(provider.ContactName, isLegalEntity: false);

            return contact;
        }

        /// <summary>
        /// Creates a telephone contact point with the given contact information.
        /// </summary>
        /// <param name="phone">A phone number.</param>
        /// <param name="contactRoleType">The role of the contact.</param>
        /// <param name="sequence">The sequence number for the contact point.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        private static CONTACT_POINT CreateContactPoint_Phone(string phone, ContactPointRoleBase contactRoleType, int sequence)
        {
            if (string.IsNullOrEmpty(phone))
            {
                return null;
            }

            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequence;
            contactPoint.ContactPointTelephone = new CONTACT_POINT_TELEPHONE();
            contactPoint.ContactPointTelephone.ContactPointTelephoneValue = ToMismoNumericString(phone, isSensitiveData: true);

            if (contactRoleType != ContactPointRoleBase.Blank)
            {
                contactPoint.ContactPointDetail = new CONTACT_POINT_DETAIL();
                contactPoint.ContactPointDetail.ContactPointRoleType = ToContactPointRoleEnum(contactRoleType);
            }

            return contactPoint;
        }

        /// <summary>
        /// Creates a fax contact point with the given contact information.
        /// </summary>
        /// <param name="fax">A fax number.</param>
        /// <param name="contactRoleType">The role of the contact.</param>
        /// <param name="sequence">The sequence number for the contact point.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        private static CONTACT_POINT CreateContactPoint_Fax(string fax, ContactPointRoleBase contactRoleType, int sequence)
        {
            if (string.IsNullOrEmpty(fax))
            {
                return null;
            }

            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequence;
            contactPoint.ContactPointTelephone = new CONTACT_POINT_TELEPHONE();
            contactPoint.ContactPointTelephone.ContactPointFaxValue = ToMismoNumericString(fax, isSensitiveData: true);

            if (contactRoleType != ContactPointRoleBase.Blank)
            {
                contactPoint.ContactPointDetail = new CONTACT_POINT_DETAIL();
                contactPoint.ContactPointDetail.ContactPointRoleType = ToContactPointRoleEnum(contactRoleType);
            }

            return contactPoint;
        }

        /// <summary>
        /// Creates an email contact point with the given contact information.
        /// </summary>
        /// <param name="email">An email address.</param>
        /// <param name="contactRoleType">The role of the contact.</param>
        /// <param name="sequence">The sequence number for the contact point.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        private static CONTACT_POINT CreateContactPoint_Email(string email, ContactPointRoleBase contactRoleType, int sequence)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }

            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequence;
            contactPoint.ContactPointEmail = new CONTACT_POINT_EMAIL();
            contactPoint.ContactPointEmail.ContactPointEmailValue = ToMismoValue(email, isSensitiveData: true);

            if (contactRoleType != ContactPointRoleBase.Blank)
            {
                contactPoint.ContactPointDetail = new CONTACT_POINT_DETAIL();
                contactPoint.ContactPointDetail.ContactPointRoleType = ToContactPointRoleEnum(contactRoleType);
            }

            return contactPoint;
        }

        /// <summary>
        /// Creates CONTACT_POINTS container with single CONTACT_POINT holding agent information.
        /// </summary>
        /// <param name="agent">Agent object holding contact information.</param>
        /// <param name="contactRoleType">Enumeration representing the type of contact (work/home/mobile/etc...) provided.</param>
        /// <param name="entity">The type of entity associated with the contact points.</param>
        /// <returns>Returns a CONTACT_POINTS container with single CONTACT_POINT holding agent information.</returns>
        private static CONTACT_POINTS CreateContactPoints(CAgentFields agent, ContactPointRoleBase contactRoleType, EntityType entity)
        {
            if (agent == null || !agent.IsValid || agent.IsNewRecord)
            {
                return null;
            }

            var contactPoints = new CONTACT_POINTS();
            string phone = string.Empty;
            string fax = string.Empty;
            string email = string.Empty;

            if (entity == EntityType.Individual)
            {
                phone = agent.Phone;
                fax = agent.FaxNum;
                email = agent.EmailAddr;
            }
            else
            {
                phone = agent.PhoneOfCompany;
                fax = agent.FaxOfCompany;
            }

            contactPoints.ContactPoint.Add(CreateContactPoint_Phone(phone, contactRoleType, 1));
            contactPoints.ContactPoint.Add(CreateContactPoint_Fax(fax, contactRoleType, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            contactPoints.ContactPoint.Add(CreateContactPoint_Email(email, contactRoleType, contactPoints.ContactPoint.Count(pt => pt != null) + 1));

            return contactPoints;
        }

        /// <summary>
        /// Creates CONTACT_POINTS container with single CONTACT_POINT holding settlement service provider information.
        /// </summary>
        /// <param name="provider">Settlement service provider object holding contact information.</param>
        /// <returns>Returns a CONTACT_POINTS container with single CONTACT_POINT holding agent information.</returns>
        private static CONTACT_POINTS CreateContactPoints(SettlementServiceProvider provider)
        {
            if (provider == null)
            {
                return null;
            }

            var contactPoints = new CONTACT_POINTS();
            string phone = provider.Phone;

            contactPoints.ContactPoint.Add(CreateContactPoint_Phone(phone, ContactPointRoleBase.Work, 1));

            return contactPoints;
        }

        /// <summary>
        /// Creates a CONTACTS container with one CONTACT sub-container holding contactName, email, fax, and phone.
        /// </summary>
        /// <param name="agent">Agent object holding contact name, phone, fax, and email.</param>
        /// <param name="contactRoleType">Enumeration representing the type of contact (work/home/mobile/etc...) provided.</param>
        /// <param name="entity">The type of entity associated with the contacts.</param>
        /// <returns>Returns a CONTACTS container with one CONTACT sub-container holding contactName, email, fax, and phone.</returns>
        private static CONTACTS CreateContacts(CAgentFields agent, ContactPointRoleBase contactRoleType, EntityType entity)
        {
            var contacts = new CONTACTS();
            contacts.Contact.Add(CreateContact(agent, contactRoleType, entity, 1));
            return contacts;
        }

        /// <summary>
        /// Creates a CONTACTS container with one CONTACT sub-container holding contactName and phone.
        /// </summary>
        /// <param name="provider">Settlement service provider object holding contact name, phone, fax, and email.</param>
        /// <returns>Returns a CONTACTS container with one CONTACT sub-container holding contactName and phone.</returns>
        private static CONTACTS CreateContacts(SettlementServiceProvider provider)
        {
            var contacts = new CONTACTS();
            contacts.Contact.Add(CreateContact(provider, 1));
            return contacts;
        }

        /// <summary>
        /// Creates a container holding data on a fee.
        /// </summary>
        /// <param name="closingFee">An instance of a closing cost fee.</param>
        /// <param name="estimatedAmount">The estimated amount of the fee.</param>
        /// <param name="documentType">Indicates the TRID document type the fees are associated with.</param>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <param name="availableProviders">The available settlement service providers for the loan.</param>
        /// <param name="creditDiscloseLocation">The document that will disclose the lender credit.</param>
        /// <param name="sequence">The sequence number of the fee among the list of fees.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="liveLoanVersion">The live (not archived) version of the loan.</param>
        /// <param name="loanType">The loan type.</param>
        /// <returns>A FEE container.</returns>
        private static FEE CreateFee(BorrowerClosingCostFee closingFee, string estimatedAmount, IntegratedDisclosureDocumentBase documentType, CAgentFields beneficiaryContact, AvailableSettlementServiceProviders availableProviders, E_LenderCreditDiscloseLocationT creditDiscloseLocation, int sequence, E_DocumentVendor vendor, LoanVersionT liveLoanVersion, E_sLT loanType)
        {
            var fee = new FEE();
            fee.SequenceNumber = sequence;
            fee.FeeDetail = CreateFeeDetail(closingFee, estimatedAmount, documentType, beneficiaryContact, loanType, vendor);
            fee.FeePaidTo = CreatePaidTo(beneficiaryContact, closingFee.BeneficiaryDescription, closingFee.BeneficiaryAgentId);
            fee.FeePayments = CreateFeePayments(closingFee.Payments, closingFee.Percent_rep, closingFee.IsFhaAllowable, closingFee.QmAmount > 0.00m, documentType, creditDiscloseLocation, vendor, liveLoanVersion);

            if (beneficiaryContact != null && !closingFee.CanShop)
            {
                fee.RequiredServiceProvider = CreateRequiredServiceProvider(beneficiaryContact);
            }

            fee.IdentifiedServiceProviders = CreateIdentifiedServiceProviders(
                availableProviders.GetProvidersForFeeTypeId(closingFee.ClosingCostFeeTypeId));

            return fee;
        }

        /// <summary>
        /// Creates a container to hold all instances of FEE.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="estimatedFeeSet">The corresponding estimated fee set.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="documentType">Indicates the TRID document type the fees are associated with.</param>
        /// <param name="creditDiscloseLocation">The document that will disclose the lender credit.</param>
        /// <param name="ignoreList">A list of fees that shouldn't be exported.</param>
        /// <returns>A FEES container.</returns>
        private static FEES CreateFees(CPageData loanData, BorrowerClosingCostSet estimatedFeeSet, E_DocumentVendor vendor, IntegratedDisclosureDocumentBase documentType, E_LenderCreditDiscloseLocationT creditDiscloseLocation, List<Guid> ignoreList)
        {
            var fees = new FEES();

            var sellerFeeIds = loanData.sSellerResponsibleClosingCostSet.GetFees(null).Select(f => f.UniqueId);
            bool shouldAlwaysIncludeOCompFee = documentType == IntegratedDisclosureDocumentBase.LoanEstimate && loanData.BrokerDB.IncludeLenderPaidOriginatorCompensationInMismo33Payload;
            
            CombinedClosingCostSet actualFeeSet = loanData.GetCombinedClosingCostSet(shouldAlwaysIncludeOCompFee);
            foreach (BorrowerClosingCostFee closingFee in actualFeeSet.GetFees(null))
            {
                bool isIgnoredFee = ignoreList != null
                    && ignoreList.Any(id => id == (closingFee.SourceFeeTypeId ?? closingFee.ClosingCostFeeTypeId));

                bool isZeroFee = closingFee.TotalAmount == 0M
                    && closingFee.Payments.All(payment => payment.Amount == 0M);

                bool isExcludedForIDS = vendor == E_DocumentVendor.IDS
                    && documentType == IntegratedDisclosureDocumentBase.LoanEstimate
                    && sellerFeeIds.Contains(closingFee.UniqueId);

                if (isIgnoredFee || isZeroFee || isExcludedForIDS)
                {
                    continue;
                }

                CAgentFields beneficiaryContact = null;
                if (closingFee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = loanData.GetAgentFields(closingFee.BeneficiaryAgentId);
                }

                var estimatedFee = (BorrowerClosingCostFee)estimatedFeeSet?.FindFeeByTypeId(closingFee.ClosingCostFeeTypeId);
                var estimatedAmount = estimatedFee?.TotalAmount_rep ?? string.Empty;

                FEE fee = CreateFee(closingFee, estimatedAmount, documentType, beneficiaryContact, loanData.sAvailableSettlementServiceProviders, creditDiscloseLocation, fees.Fee.Count(f => f != null) + 1, vendor, loanData.GetLiveLoanVersion(), loanData.sLT);
                fees.Fee.Add(fee);
            }

            return fees;
        }

        /// <summary>
        /// Creates a container holding details on a closing cost fee.
        /// </summary>
        /// <param name="closingFee">An individual closing cost fee.</param>
        /// <param name="estimatedAmount">The estimated amount of the fee.</param>
        /// <param name="documentType">Indicates the TRID document type the fees are associated with.</param>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <param name="loanType">The loan type.</param>
        /// <param name="vendor">The vendor associated with the request.</param>
        /// <returns>A FEE_DETAIL container.</returns>
        private static FEE_DETAIL CreateFeeDetail(BorrowerClosingCostFee closingFee, string estimatedAmount, IntegratedDisclosureDocumentBase documentType, CAgentFields beneficiaryContact, E_sLT loanType, E_DocumentVendor vendor)
        {
            var feeDetail = new FEE_DETAIL();
            feeDetail.BorrowerChosenProviderIndicator = ToMismoIndicator(closingFee.DidShop);

            feeDetail.FeeEstimatedTotalAmount = ToMismoAmount(estimatedAmount);
            feeDetail.FeeActualTotalAmount = ToMismoAmount(closingFee.TotalAmount_rep);

            MISMOString paidToTypeOtherDescription = null;

            feeDetail.FeePaidToType = GetFeePaidToType(
                beneficiaryContact,
                closingFee.IsAffiliate,
                closingFee.IsThirdParty,
                closingFee.BeneficiaryType,
                closingFee.Beneficiary,
                closingFee.BeneficiaryDescription,
                out paidToTypeOtherDescription);

            if (paidToTypeOtherDescription != null)
            {
                feeDetail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
            }

            string feeDescription = closingFee.Description;
            if (closingFee.IsTitleFee)
            {
                feeDescription = "TITLE - " + feeDescription;
            }
            
            feeDetail.FeeDescription = ToMismoString(feeDescription);
            feeDetail.FeePercentBasisType = ToFeePercentBasisEnum(closingFee.PercentBaseT);

            if (feeDetail.FeePercentBasisType != null && feeDetail.FeePercentBasisType.enumValue == FeePercentBasisBase.Other)
            {
                feeDetail.FeePercentBasisTypeOtherDescription = ToMismoString(GetXmlEnumName(closingFee.PercentBaseT));
            }

            feeDetail.FeeSpecifiedFixedAmount = ToMismoAmount(closingFee.BaseAmount_rep);
            feeDetail.FeeSpecifiedHUD1LineNumberValue = ToMismoValue(closingFee.HudLine_rep);
            feeDetail.FeeTotalPercent = ToMismoPercent(closingFee.Percent_rep);
            feeDetail.FeeType = ToFeeEnum(closingFee.MismoFeeT, feeDescription);

            string description = closingFee.MismoFeeT == E_ClosingCostFeeMismoFeeT.Undefined || closingFee.MismoFeeT == E_ClosingCostFeeMismoFeeT.Other ? closingFee.Description : GetXmlEnumName(closingFee.MismoFeeT);
            if (feeDetail != null && feeDetail.FeeType.enumValue == FeeBase.Other)
            {
                feeDetail.FeeTypeOtherDescription = ToMismoString(description);
            }

            feeDetail.IntegratedDisclosureSectionType = ToIntegratedDisclosureSectionEnum(documentType, closingFee.GetTRIDSectionBasedOnDisclosure(documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure), closingFee.CanShop);

            if (feeDetail.IntegratedDisclosureSectionType != null && feeDetail.IntegratedDisclosureSectionType.enumValue == IntegratedDisclosureSectionBase.Other)
            {
                feeDetail.IntegratedDisclosureSectionTypeOtherDescription = ToMismoString(GetXmlEnumName(closingFee.GetTRIDSectionBasedOnDisclosure(documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure)));
            }

            feeDetail.OptionalCostIndicator = ToMismoIndicator(closingFee.IsOptional);
            feeDetail.RegulationZPointsAndFeesIndicator = ToMismoIndicator(closingFee.IsIncludedInQm);
            feeDetail.RequiredProviderOfServiceIndicator = ToMismoIndicator(!closingFee.CanShop);

            if (closingFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId && loanType == E_sLT.UsdaRural)
            {
                feeDetail.FeeType = ToFeeEnum(E_ClosingCostFeeMismoFeeT.RuralHousingFee, displayLabelText: "Rural Housing Fee");
            }
            else if (closingFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId && vendor == E_DocumentVendor.DocuTech)
            {
                feeDetail.FeeSpecifiedHUD1LineNumberValue = ToMismoValue("895");
            }

            feeDetail.Extension = CreateFeeDetailExtension(closingFee.Dflp, closingFee.OriginalDescription, closingFee.ClosingCostFeeTypeId, closingFee.CanShop, closingFee.DidShop);

            return feeDetail;
        }

        /// <summary>
        /// Creates an "EXTENSION" container with extra information about a fee detail.
        /// </summary>
        /// <param name="dflp">Whether this fee has the DFLP checkbox checked.</param>
        /// <param name="originalDescription">The unmodified description of the fee.</param>
        /// <param name="feeTypeId">The unique closing cost type ID.</param>
        /// <param name="canShop">A boolean value indicating whether the borrower can shop for this fee.</param>
        /// <param name="didShop">A boolean value indicating whether the borrower did shop for this fee.</param>
        /// <returns>A <see cref="FEE_DETAIL_EXTENSION"/> container.</returns>
        private static FEE_DETAIL_EXTENSION CreateFeeDetailExtension(bool dflp, string originalDescription, Guid feeTypeId, bool canShop, bool didShop)
        {
            var extension = new FEE_DETAIL_EXTENSION();
            extension.Other = CreateLQBFeeDetailExtension(dflp, originalDescription, feeTypeId, canShop, didShop);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LQB "OTHER" container with extra information about a fee detail.
        /// </summary>
        /// <param name="dflp">Whether this fee has the DFLP checkbox checked.</param>
        /// <param name="originalDescription">The unmodified description of the fee.</param>
        /// <param name="feeTypeId">The unique closing cost type ID.</param>
        /// <param name="canShop">A boolean value indicating whether the borrower can shop for this fee.</param>
        /// <param name="didShop">A boolean value indicating whether the borrower did shop for this fee.</param>
        /// <returns>A <see cref="LQB_FEE_DETAIL_EXTENSION"/> container.</returns>
        private static LQB_FEE_DETAIL_EXTENSION CreateLQBFeeDetailExtension(bool dflp, string originalDescription, Guid feeTypeId, bool canShop, bool didShop)
        {
            var extension = new LQB_FEE_DETAIL_EXTENSION();
            extension.DFLPIndicator = ToMismoIndicator(dflp);
            extension.OriginalDescription = ToMismoString(originalDescription);
            extension.ClosingCostFeeTypeID = ToMismoString(feeTypeId.ToString());
            extension.CanShop = ToMismoIndicator(canShop);
            extension.DidShop = ToMismoIndicator(didShop);

            return extension;
        }

        /// <summary>
        /// Creates a FeePaidToEnum object with the paid-to type based on the provided agent information.
        /// </summary>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <param name="affiliate">True if the entity imposing the fee is an affiliate of either the lender or the originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the entity imposing the fee is a third party. Otherwise false.</param>
        /// <param name="beneficiaryType">A string representation of the agent type of the fee beneficiary.</param>
        /// <param name="beneficiary">The agent type of the fee beneficiary.</param>
        /// <param name="beneficiaryDescription">A description of the fee beneficiary.</param>
        /// <param name="otherDescription">The target <see cref="MISMOString"/> to contain the paid-to type other description, if the paid-to type resolves to other. Otherwise it will be null.</param>
        /// <returns>A FeePaidToEnum object with the paid-to type based on the provided agent information.</returns>
        private static MISMOEnum<FeePaidToBase> GetFeePaidToType(CAgentFields beneficiaryContact, bool affiliate, bool thirdParty, string beneficiaryType, E_AgentRoleT beneficiary, string beneficiaryDescription, out MISMOString otherDescription)
        {
            MISMOEnum<FeePaidToBase> feePaidToType = null;
            otherDescription = null;

            if (beneficiaryContact != null)
            {
                feePaidToType = ToFeePaidToEnum(beneficiaryContact, affiliate, thirdParty);

                if (feePaidToType != null && feePaidToType.enumValue == FeePaidToBase.Other)
                {
                    otherDescription = ToMismoString(beneficiaryType);
                }
            }

            //// If the fee PaidTo is set w/o having an associated agent, such as set from the fee setup, then map it.
            if (feePaidToType == null)
            {
                feePaidToType = ToFeePaidToEnum(beneficiary, affiliate, thirdParty);

                if (feePaidToType != null && feePaidToType.enumValue == FeePaidToBase.Other)
                {
                    string description = string.IsNullOrEmpty(beneficiaryDescription) ? GetXmlEnumName(beneficiary) : beneficiaryDescription;

                    otherDescription = ToMismoString(description);
                }
            }

            return feePaidToType;
        }

        /// <summary>
        /// Creates a container holding information on fees. This method is used at the loan level, for live data, and at the loan estimate | closing disclosure level (given that extensions are requested).
        /// This method is static to guarantee that neither it nor any sub-methods mix live data with archived data.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="documentType">Indicates the TRID document type the fees are associated with.</param>
        /// <param name="creditDiscloseLocation">The document that will disclose the lender credit.</param>
        /// <param name="feeIgnoreList">A list of fees to exclude from the export.</param>
        /// <returns>A FEE_INFORMATION container.</returns>
        private static FEE_INFORMATION CreateFeeInformation(CPageData loanData, E_DocumentVendor vendor, IntegratedDisclosureDocumentBase documentType, E_LenderCreditDiscloseLocationT creditDiscloseLocation, List<Guid> feeIgnoreList)
        {
            var feeInformation = new FEE_INFORMATION();

            BorrowerClosingCostSet estimatedFeeSet = null;
            if (loanData.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017
                && loanData.sAppliedCCArchiveLinkedArchiveId != Guid.Empty)
            {
                // If there is a linked pair of archives, the archive applied to the loan contains the
                // actual fee amounts while the linked archive contains the tolerance data.
                var linkedArchive = loanData.GetClosingCostArchiveById(loanData.sAppliedCCArchiveLinkedArchiveId);
                estimatedFeeSet = linkedArchive?.GetClosingCostSet(loanData.m_convertLos);
            }

            if (estimatedFeeSet == null)
            {
                estimatedFeeSet = loanData.LastDisclosedLoanEstimateArchive?.GetClosingCostSet(loanData.m_convertLos);
            }

            feeInformation.Fees = CreateFees(loanData, estimatedFeeSet, vendor, documentType, creditDiscloseLocation, feeIgnoreList);
            feeInformation.FeesSummary = CreateFeesSummary(loanData, documentType);

            return feeInformation;
        }

        /// <summary>
        /// Creates a container holding data on a fee payment.
        /// </summary>
        /// <param name="payment">An individual payment for a fee.</param>
        /// <param name="percent">The payment percent.</param>
        /// <param name="allowableFHA">True if the fee is an allowable FHA closing cost. Otherwise false.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <param name="sequence">The sequence number of the payment among the list of payments.</param>
        /// <param name="documentType">The type of document the fees will appear on.</param>
        /// <param name="creditDiscloseLocation">Indicates which form the lender credit is disclosed on.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="liveLoanVersion">The live (not archived) version of the loan.</param>
        /// <returns>A FEE_PAYMENT container.</returns>
        private static FEE_PAYMENT CreateFeePayment(BorrowerClosingCostFeePayment payment, string percent, bool allowableFHA, bool qm, int sequence, IntegratedDisclosureDocumentBase documentType, E_LenderCreditDiscloseLocationT creditDiscloseLocation, E_DocumentVendor vendor, LoanVersionT liveLoanVersion)
        {
            var feePayment = new FEE_PAYMENT();
            feePayment.FeeActualPaymentAmount = ToMismoAmount(payment.Amount_rep);
            feePayment.FeePaymentAllowableFHAClosingCostIndicator = ToMismoIndicator(allowableFHA);
            feePayment.FeePaymentPaidByType = ToFeePaymentPaidByEnum(payment.GetPaidByType(creditDiscloseLocation, documentType != IntegratedDisclosureDocumentBase.LoanEstimate));
            feePayment.FeePaymentPaidOutsideOfClosingIndicator = ToMismoIndicator(payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing);
            feePayment.FeePaymentPercent = ToMismoPercent(percent);
            feePayment.FeePaymentResponsiblePartyType = ToFeePaymentResponsiblePartyEnum(payment.ResponsiblePartyT);
            feePayment.PaymentFinancedIndicator = ToMismoIndicator(payment.IsFinanced);
            feePayment.PaymentIncludedInAPRIndicator = vendor.Equals(E_DocumentVendor.DocsOnDemand) ? 
                ToMismoIndicator(payment.IsAPRRaw) :
                ToMismoIndicator(payment.IsIncludedInApr(liveLoanVersion));
            feePayment.RegulationZPointsAndFeesIndicator = ToMismoIndicator(qm);
            feePayment.Extension = CreateFeePaymentExtension(payment.Id.ToString());
            feePayment.SequenceNumber = sequence;

            return feePayment;
        }

        /// <summary>
        /// Creates an extension container holding data on a fee payment.
        /// </summary>
        /// <param name="paymentId">The fee payment ID.</param>
        /// <returns>A serializable <see cref="FEE_PAYMENT_EXTENSION"/> container.</returns>
        private static FEE_PAYMENT_EXTENSION CreateFeePaymentExtension(string paymentId)
        {
            var extension = new FEE_PAYMENT_EXTENSION();
            extension.Other = CreateLqbFeePaymentExtension(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container holding data on a fee payment.
        /// </summary>
        /// <param name="paymentId">The fee payment ID.</param>
        /// <returns>A serializable <see cref="LQB_FEE_PAYMENT_EXTENSION"/> container.</returns>
        private static LQB_FEE_PAYMENT_EXTENSION CreateLqbFeePaymentExtension(string paymentId)
        {
            var extension = new LQB_FEE_PAYMENT_EXTENSION();
            extension.Id = ToMismoString(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a container to hold all instances of FEE_PAYMENT.
        /// </summary>
        /// <param name="payments">The set of payments for a fee.</param>
        /// <param name="percent">The payment percent.</param>
        /// <param name="allowableFHA">True if the fee is an allowable FHA closing cost. Otherwise false.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <param name="documentType">The type of document the fees will appear on.</param>
        /// <param name="creditDiscloseLocation">The document that will disclose the lender credit.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="liveLoanVersion">The live (not archived) version of the loan.</param>
        /// <returns>A FEE_PAYMENTS container.</returns>
        private static FEE_PAYMENTS CreateFeePayments(IEnumerable<LoanClosingCostFeePayment> payments, string percent, bool allowableFHA, bool qm, IntegratedDisclosureDocumentBase documentType, E_LenderCreditDiscloseLocationT creditDiscloseLocation, E_DocumentVendor vendor, LoanVersionT liveLoanVersion)
        {
            var feePayments = new FEE_PAYMENTS();

            foreach (BorrowerClosingCostFeePayment payment in payments.Where(p => p.Amount != 0.00m))
            {
                feePayments.FeePayment.Add(CreateFeePayment(payment, percent, allowableFHA, qm, feePayments.FeePayment.Count(f => f != null) + 1, documentType, creditDiscloseLocation, vendor, liveLoanVersion));
            }

            return feePayments;
        }

        /// <summary>
        /// Creates a container to hold a summary of the loan fees.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="documentType">Indicates the TRID document type the fees are associated with.</param>
        /// <returns>A FEES_SUMMARY container.</returns>
        private static FEES_SUMMARY CreateFeesSummary(CPageData loanData, IntegratedDisclosureDocumentBase documentType)
        {
            var feesSummary = new FEES_SUMMARY();
            feesSummary.FeeSummaryDetail = CreateFeeSummaryDetail(loanData, documentType);

            return feesSummary;
        }

        /// <summary>
        /// Creates a container holding details on the summarized loan fees.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="documentType">Indicates the TRID document type the fees are associated with.</param>
        /// <returns>A FEE_SUMMARY_DETAIL container.</returns>
        private static FEE_SUMMARY_DETAIL CreateFeeSummaryDetail(CPageData loanData, IntegratedDisclosureDocumentBase documentType)
        {
            var feeSummaryDetail = new FEE_SUMMARY_DETAIL();

            feeSummaryDetail.APRPercent = ToMismoPercent(loanData.sApr_rep);
            feeSummaryDetail.FeeSummaryDisclosedTotalSalesPriceAmount = ToMismoAmount(loanData.sPurchPrice_rep);
            feeSummaryDetail.FeeSummaryTotalAmountFinancedAmount = ToMismoAmount(loanData.sFinancedAmt_rep);
            feeSummaryDetail.FeeSummaryTotalAPRFeesAmount = ToMismoAmount(loanData.sAprIncludedCc_rep);
            feeSummaryDetail.FeeSummaryTotalDepositedReservesAmount = ToMismoAmount(loanData.sGfeInitialImpoundDeposit_rep);
            feeSummaryDetail.FeeSummaryTotalFinanceChargeAmount = ToMismoAmount(loanData.sFinCharge_rep);
            feeSummaryDetail.FeeSummaryTotalInterestPercent = ToMismoPercent(
                documentType == IntegratedDisclosureDocumentBase.LoanEstimate
                ? loanData.sCFPBTotalInterestPercentLE_rep
                : loanData.sCFPBTotalInterestPercent_rep);

            // OPM 247161 - UCD Support.
            feeSummaryDetail.FeeSummaryTotalOfAllPaymentsAmount = ToMismoAmount(loanData.sTRIDTotalOfPayments_rep);

            feeSummaryDetail.Extension = CreateFeeSummaryDetailExtension(loanData);

            return feeSummaryDetail;
        }

        /// <summary>
        /// Creates an extension container for the fee summary details.
        /// </summary>
        /// <param name="loanData">A loan object.</param>
        /// <returns>An extension container.</returns>
        private static FEE_SUMMARY_DETAIL_EXTENSION CreateFeeSummaryDetailExtension(CPageData loanData)
        {
            var extension = new FEE_SUMMARY_DETAIL_EXTENSION();
            extension.Other = CreateLqbFeeSummaryDetailExtension(loanData);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container for the fee summary details.
        /// </summary>
        /// <param name="loanData">A loan object.</param>
        /// <returns>An extension container.</returns>
        private static LQB_FEE_SUMMARY_DETAIL_EXTENSION CreateLqbFeeSummaryDetailExtension(CPageData loanData)
        {
            var extension = new LQB_FEE_SUMMARY_DETAIL_EXTENSION();
            extension.LenderCreditCanOffsetAprIndicator = ToMismoIndicator(loanData.sGfeLenderCreditFProps_Apr);

            return extension;
        }

        /// <summary>
        /// Creates a container holding data about an index rule.
        /// </summary>
        /// <param name="index">The adjustable rate mortgage index.</param>
        /// <param name="freddieIndex">The Freddie Mac index.</param>
        /// <param name="adjustmentCapMonths">The adjustment period (months) for the adjustable rate.</param>
        /// <param name="indexName">The name of the ARM index.</param>
        /// <param name="indexLeadDays_rep">The number of days prior to an interest rate effective date used to determine the date for the index value.</param>
        /// <param name="sequence">The sequence number of the index rule among the set of rules.</param>
        /// <returns>An INDEX_RULE container populated with data.</returns>
        private static INDEX_RULE CreateIndexRule(E_sArmIndexT index, E_sFreddieArmIndexT freddieIndex, int adjustmentCapMonths, string indexName, string indexLeadDays_rep, int sequence)
        {
            var indexRule = new INDEX_RULE();

            indexRule.IndexSourceType = ToIndexSourceEnum(index, freddieIndex, adjustmentCapMonths);

            if (indexRule.IndexSourceType != null && indexRule.IndexSourceType.enumValue == IndexSourceBase.Other)
            {
                if (index != E_sArmIndexT.LeaveBlank)
                {
                    indexRule.IndexSourceTypeOtherDescription = ToMismoString(GetXmlEnumName(index));
                }
                else if (freddieIndex != E_sFreddieArmIndexT.LeaveBlank)
                {
                    indexRule.IndexSourceTypeOtherDescription = ToMismoString(GetXmlEnumName(freddieIndex));
                }
            }

            indexRule.IndexType = ToIndexEnum(IndexBase.Other);
            indexRule.IndexTypeOtherDescription = ToMismoString(indexName);
            indexRule.InterestAndPaymentAdjustmentIndexLeadDaysCount = ToMismoCount(indexLeadDays_rep, null);
            indexRule.SequenceNumber = sequence;

            return indexRule;
        }

        /// <summary>
        /// Creates a container to hold instances of INDEX_RULE.
        /// </summary>
        /// <param name="index">The adjustable rate mortgage index.</param>
        /// <param name="freddieIndex">The Freddie Mac index.</param>
        /// <param name="adjustmentCapMonths">The adjustment period (months) for the adjustable rate.</param>
        /// <param name="indexName">The name of the ARM index.</param>
        /// <param name="indexLeadDays_rep">The number of days prior to an interest rate effective date used to determine the date for the index value.</param>
        /// <returns>An INDEX_RULES container.</returns>
        private static INDEX_RULES CreateIndexRules(E_sArmIndexT index, E_sFreddieArmIndexT freddieIndex, int adjustmentCapMonths, string indexName, string indexLeadDays_rep)
        {
            var indexRules = new INDEX_RULES();
            indexRules.IndexRule.Add(CreateIndexRule(index, freddieIndex, adjustmentCapMonths, indexName, indexLeadDays_rep, 1));

            return indexRules;
        }

        /// <summary>
        /// Creates an container holding data on an interest-only rule.
        /// </summary>
        /// <param name="interestOnlyMonths">The number of months during which the loan will have an interest only payment.</param>
        /// <param name="iomonthsCount_rep">The number of months the loan remains interest only.</param>
        /// <param name="iomonthlyPaymentAmount_rep">The monthly interest only payment amount.</param>
        /// <returns>An INTEREST_ONLY container populated with data.</returns>
        private static INTEREST_ONLY CreateInterestOnly(int interestOnlyMonths, string iomonthsCount_rep, string iomonthlyPaymentAmount_rep)
        {
            if (interestOnlyMonths == 0)
            {
                return null;
            }

            var interestOnly = new INTEREST_ONLY();
            interestOnly.InterestOnlyTermMonthsCount = ToMismoCount(iomonthsCount_rep, null);
            interestOnly.InterestOnlyMonthlyPaymentAmount = ToMismoAmount(iomonthlyPaymentAmount_rep);

            return interestOnly;
        }

        /// <summary>
        /// Creates a container holding data on an interest rate adjustment.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>An INTEREST_RATE_ADJUSTMENT populated with data.</returns>
        private static INTEREST_RATE_ADJUSTMENT CreateInterestRateAdjustment(CPageData loanData)
        {
            var interestRateAdjustment = new INTEREST_RATE_ADJUSTMENT();
            interestRateAdjustment.InterestRateLifetimeAdjustmentRule = CreateInterestRateLifetimeAdjustmentRule(loanData);
            interestRateAdjustment.IndexRules = CreateIndexRules(loanData.sArmIndexT, loanData.sFreddieArmIndexT, loanData.sRAdjCapMon, loanData.sArmIndexNameVstr, loanData.sArmIndexLeadDays_rep);
            interestRateAdjustment.InterestRatePerChangeAdjustmentRules = CreateInterestRatePerChangeAdjustmentRules(loanData);

            return interestRateAdjustment;
        }

        /// <summary>
        /// Creates a container holding data on a lifetime interest rate adjustment rule.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>An INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE populated with data.</returns>
        private static INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE CreateInterestRateLifetimeAdjustmentRule(CPageData loanData)
        {
            var interestRateLifetimeAdjustmentRule = new INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE();
            interestRateLifetimeAdjustmentRule.CeilingRatePercent = ToMismoPercent(loanData.sRLifeCapR_rep);
            interestRateLifetimeAdjustmentRule.FloorRatePercent = ToMismoPercent(loanData.sRAdjFloorR_rep);
            interestRateLifetimeAdjustmentRule.InterestRateRoundingPercent = ToMismoPercent(loanData.sRAdjRoundToR_rep);
            interestRateLifetimeAdjustmentRule.InterestRateRoundingType = ToInterestRateRoundingEnum(loanData.sRAdjRoundT);
            interestRateLifetimeAdjustmentRule.MarginRatePercent = ToMismoPercent(loanData.sRAdjMarginR == 0m ? "0.00" : loanData.sRAdjMarginR_rep);
            interestRateLifetimeAdjustmentRule.MaximumIncreaseRatePercent = ToMismoPercent(loanData.sRAdjLifeCapR_rep);
            interestRateLifetimeAdjustmentRule.FirstRateChangeMonthsCount = ToMismoCount(loanData.sRAdj1stCapMon_rep, null);

            return interestRateLifetimeAdjustmentRule;
        }

        /// <summary>
        /// Creates a container holding rule data on either a first or subsequent rate change.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="sequence">The sequence of the rule among the list of interest rate adjustment rules.</param>
        /// <returns>An INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE container populated with data.</returns>
        private static INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE CreateInterestRatePerChangeAdjustmentRule(CPageData loanData, int sequence)
        {
            var rule = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE();
            rule.SequenceNumber = sequence;

            if (sequence == 1)
            {
                rule.AdjustmentRuleType = ToAdjustmentRuleEnum(AdjustmentRuleBase.First);
                rule.PerChangeCeilingRatePercent = ToMismoPercent(loanData.sR1stCapR_rep);
                rule.PerChangeRateAdjustmentEffectiveMonthsCount = ToMismoCount(loanData.sRAdj1stCapMon_rep, null);
                rule.PerChangeRateAdjustmentFrequencyMonthsCount = ToMismoCount(loanData.sRAdj1stCapMon_rep, null);
                rule.PerChangeMaximumIncreaseRatePercent = ToMismoPercent(loanData.sLimitsOnInterestRateFirstChange_rep);
            }
            else
            {
                rule.AdjustmentRuleType = ToAdjustmentRuleEnum(AdjustmentRuleBase.Subsequent);
                rule.PerChangeCeilingRatePercent = ToMismoPercent(loanData.sRLifeCapR_rep);
                rule.PerChangeRateAdjustmentEffectiveMonthsCount = ToMismoCount(loanData.sRAdjCapMon_rep, null);
                rule.PerChangeRateAdjustmentFrequencyMonthsCount = ToMismoCount(loanData.sRAdjCapMon_rep, null);
                rule.PerChangeMaximumIncreaseRatePercent = ToMismoPercent(loanData.sLimitsOnInterestRateSubsequentChanges_rep);
            }

            rule.PerChangeFloorRatePercent = ToMismoPercent(loanData.m_convertLos.ToRateString(Math.Max(loanData.sNoteIR - loanData.sRAdj1stCapR, loanData.sRAdjFloorR)));

            return rule;
        }

        /// <summary>
        /// Creates a container that holds all instances of INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>An INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES container.</returns>
        private static INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES CreateInterestRatePerChangeAdjustmentRules(CPageData loanData)
        {
            var interestRatePerChangeAdjustmentRules = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES();
            interestRatePerChangeAdjustmentRules.InterestRatePerChangeAdjustmentRule.Add(CreateInterestRatePerChangeAdjustmentRule(loanData, 1));
            interestRatePerChangeAdjustmentRules.InterestRatePerChangeAdjustmentRule.Add(CreateInterestRatePerChangeAdjustmentRule(loanData, 2));

            return interestRatePerChangeAdjustmentRules;
        }

        /// <summary>
        /// Creates LEGAL_ENTITY container with contactName, companyName, email, fax, phone provided.
        /// </summary>
        /// <param name="agent">Agent object containing contact name, company name, phone, fax, and email.</param>
        /// <param name="companyName">The company name of the legal entity.</param>
        /// <param name="contactRoleType">Enumeration representing the type of contact (work/home/mobile/etc...) provided.</param>
        /// <returns>Returns a LEGAL_ENTITY container with contactName, companyName, email, fax, phone provided.</returns>
        private static LEGAL_ENTITY CreateLegalEntity(CAgentFields agent, string companyName, ContactPointRoleBase contactRoleType)
        {
            LEGAL_ENTITY legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(companyName);
            legalEntity.Contacts = CreateContacts(agent, contactRoleType, EntityType.LegalEntity);

            return legalEntity;
        }

        /// <summary>
        /// Creates LEGAL_ENTITY container with contactName, companyName, phone provided.
        /// </summary>
        /// <param name="provider">SettlementServiceProvider object containing contact name, company name, phone.</param>
        /// <returns>Returns a LEGAL_ENTITY container with contactName, companyName, phone provided.</returns>
        private static LEGAL_ENTITY CreateLegalEntity(SettlementServiceProvider provider)
        {
            LEGAL_ENTITY legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(provider.CompanyName);
            legalEntity.Contacts = CreateContacts(provider);

            return legalEntity;
        }

        /// <summary>
        /// Creates a basic LEGAL_ENTITY container holding just a company name.
        /// </summary>
        /// <param name="companyName">The company name.</param>
        /// <returns>A LEGAL_ENTITY container.</returns>
        private static LEGAL_ENTITY CreateLegalEntity(string companyName)
        {
            var legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(companyName);

            return legalEntity;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY_DETAIL container.
        /// </summary>
        /// <param name="companyName">Company name to be populated.</param>
        /// <param name="mersOrganizationId">The MERSOrganizationIdentifier to be populated.</param>
        /// <param name="legalEntityType">The type of Legal Entity, if applicable.</param>
        /// <returns>Returns LEGAL_ENTITY_DETAIL container.</returns>
        private static LEGAL_ENTITY_DETAIL CreateLegalEntityDetail(string companyName, string mersOrganizationId = null, LegalEntityBase legalEntityType = LegalEntityBase.Blank)
        {
            var legalEntityDetail = new LEGAL_ENTITY_DETAIL();
            legalEntityDetail.FullName = ToMismoString(companyName);
            legalEntityDetail.MERSOrganizationIdentifier = ToMismoIdentifier(mersOrganizationId);

            return legalEntityDetail;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY_DETAIL for a property seller.
        /// </summary>
        /// <param name="companyName">Company name to be populated.</param>
        /// <param name="sellerType">The type of property seller.</param>
        /// <returns>A LEGAL_ENTITY_DETAIL container.</returns>
        private static LEGAL_ENTITY_DETAIL CreateLegalEntityDetail(string companyName, E_SellerEntityType sellerType)
        {
            var legalEntityDetail = new LEGAL_ENTITY_DETAIL();
            legalEntityDetail.FullName = ToMismoString(companyName);

            if (sellerType != E_SellerEntityType.Undefined)
            {
                legalEntityDetail.LegalEntityType = ToLegalEntityEnum(sellerType);

                if (legalEntityDetail.LegalEntityType.enumValue == LegalEntityBase.Other)
                {
                    legalEntityDetail.LegalEntityTypeOtherDescription = ToMismoString(GetXmlEnumName(sellerType));
                }
            }

            return legalEntityDetail;
        }

        /// <summary>
        /// Creates NAME object with full name specified.
        /// </summary>
        /// <param name="fullName">The full name.</param>
        /// <param name="isLegalEntity">Indicates whether the name is for a legal entity.</param>
        /// <returns>NAME object with full name specified.</returns>
        private static NAME CreateName(string fullName, bool isLegalEntity)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                return null;
            }

            if (!isLegalEntity)
            {
                try
                {
                    // If the name is not for a legal entity, we attempt to parse it.
                    var parser = new CommonLib.Name();
                    parser.ParseName(fullName);

                    return CreateName(parser.FirstName, parser.MiddleName, parser.LastName, parser.Suffix, fullName);
                }
                catch (NullReferenceException)
                {
                    // Unable to parse the name, let the default behavior occur instead.
                }
            }

            var name = new NAME();
            name.FullName = ToMismoString(fullName);
            return name;
        }

        /// <summary>
        /// Creates a PAID_TO container with information regarding the company of the given agent, the recipient of a fee/payable. 
        /// </summary>
        /// <param name="paidToContact">The contact record associated with the entity to which the fee is paid.</param>
        /// <param name="companyName">The name of the company or legal entity to which the fee is paid.</param>
        /// <param name="beneficiaryAgentId">
        /// The ID of the <see cref="CAgentFields"/> 
        /// entry in the loan's contact list which is the beneficiary of the payment.
        /// </param>
        /// <returns>A PAID_TO container with information regarding the company of the given agent, the recipient of a fee/payable.</returns>
        private static PAID_TO CreatePaidTo(CAgentFields paidToContact, string companyName, Guid beneficiaryAgentId)
        {
            bool validContact = paidToContact != null && paidToContact.IsValid && !paidToContact.IsNewRecord;

            if (!validContact && string.IsNullOrEmpty(companyName))
            {
                return null;
            }

            var paidTo = new PAID_TO();

            if (validContact)
            {
                var addresses = CreateAddresses(paidToContact);

                if (addresses.Address != null && addresses.Address.Count(a => a != null) > 0)
                {
                    paidTo.Address = addresses.Address.Where(a => a != null).FirstOrDefault();
                }

                paidTo.LegalEntity = CreateLegalEntity(paidToContact, string.IsNullOrEmpty(companyName) ? paidToContact.CompanyName : companyName, ContactPointRoleBase.Work);
                paidTo.Extension = new PAID_TO_EXTENSION
                {
                    Other = new LQB_PAID_TO_EXTENSION
                    {
                        BeneficiaryAgentId = ToMismoIdentifier(beneficiaryAgentId.ToString())
                    }
                };
            }
            else
            {
                paidTo.LegalEntity = CreateLegalEntity(companyName);
            }

            return paidTo;
        }

        /// <summary>
        /// Creates a container holding a set of partial payments.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>A PARTIAL_PAYMENTS object.</returns>
        private static PARTIAL_PAYMENTS CreatePartialPayments(CPageData loanData)
        {
            var partialPayments = new PARTIAL_PAYMENTS();
            partialPayments.PartialPayment.Add(CreatePartialPayment(loanData.sTRIDPartialPaymentAcceptanceT));

            return partialPayments;
        }

        /// <summary>
        /// Creates a container holding data on a partial payment.
        /// </summary>
        /// <param name="partialPaymentsAccepted">Indicates whether partial payments are accepted.</param>
        /// <returns>A PARTIAL_PAYMENT container.</returns>
        private static PARTIAL_PAYMENT CreatePartialPayment(E_sTRIDPartialPaymentAcceptanceT partialPaymentsAccepted)
        {
            var partialPayment = new PARTIAL_PAYMENT();
            partialPayment.PartialPaymentApplicationMethodType = ToPartialPaymentApplicationMethodEnum(partialPaymentsAccepted);
            partialPayment.SequenceNumber = 1;

            return partialPayment;
        }

        /// <summary>
        /// Creates a container holding information on the mortgage payment.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="isClosingPackage">Indicates whether a closing package is being generated.</param>
        /// <returns>A PAYMENT object.</returns>
        private static PAYMENT CreatePayment(CPageData loanData, bool isClosingPackage)
        {
            var payment = new PAYMENT();
            payment.PaymentComponentBreakouts = CreatePaymentComponentBreakouts(loanData);
            payment.PaymentRule = CreatePaymentRule(loanData.sSchedDueD1_rep, loanData.sProThisMPmt_rep, loanData.sTRIDPartialPaymentAcceptanceT, isClosingPackage);

            if (isClosingPackage)
            {
                payment.PartialPayments = CreatePartialPayments(loanData);
            }

            return payment;
        }

        /// <summary>
        /// Creates a container holding information on a payment component breakout.
        /// </summary>
        /// <param name="pipaymentAmount_rep">The principal and interest amount that is part of the total payment being reported.</param>
        /// <param name="sequence">The sequence number of the payment component breakout among the list of breakouts.</param>
        /// <returns>A PAYMENT_COMPONENT_BREAKOUT container.</returns>
        private static PAYMENT_COMPONENT_BREAKOUT CreatePaymentComponentBreakout(string pipaymentAmount_rep, int sequence)
        {
            var paymentComponentBreakout = new PAYMENT_COMPONENT_BREAKOUT();
            paymentComponentBreakout.SequenceNumber = sequence;
            paymentComponentBreakout.PaymentComponentBreakoutDetail = CreatePaymentComponentBreakoutDetail(pipaymentAmount_rep);

            return paymentComponentBreakout;
        }

        /// <summary>
        /// Creates a container to hold all instances of PAYMENT_COMPONENT_BREAKOUTS.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>A PAYMENT_COMPONENT_BREAKOUTS container.</returns>
        private static PAYMENT_COMPONENT_BREAKOUTS CreatePaymentComponentBreakouts(CPageData loanData)
        {
            var paymentComponentBreakouts = new PAYMENT_COMPONENT_BREAKOUTS();
            paymentComponentBreakouts.PaymentComponentBreakout.Add(CreatePaymentComponentBreakout(loanData.sProThisMPmt_rep, 1));

            return paymentComponentBreakouts;
        }

        /// <summary>
        /// Creates a container holding data on a payment component breakout.
        /// </summary>
        /// <param name="pipaymentAmount_rep">The principal and interest amount that is part of the total payment being reported.</param>
        /// <returns>A PAYMENT_COMPONENT_BREAKOUT_DETAIL populated with data.</returns>
        private static PAYMENT_COMPONENT_BREAKOUT_DETAIL CreatePaymentComponentBreakoutDetail(string pipaymentAmount_rep)
        {
            var paymentComponentBreakoutDetail = new PAYMENT_COMPONENT_BREAKOUT_DETAIL();
            paymentComponentBreakoutDetail.PrincipalAndInterestPaymentAmount = ToMismoAmount(pipaymentAmount_rep);

            return paymentComponentBreakoutDetail;
        }

        /// <summary>
        /// Creates a container holding data on a payment rule.
        /// </summary>
        /// <param name="scheduledFirstPaymentDate_rep">The date of the first scheduled mortgage payment to be made by the borrower under the terms of the mortgage.</param>
        /// <param name="initialPIPaymentAmount_rep">The dollar amount of the principal and interest payment as stated on the Note.</param>
        /// <param name="partialPaymentsAccepted">Indicates whether partial payments are accepted.</param>
        /// <param name="isClosingPackage">Indicates whether a closing package is currently being exported.</param>
        /// <returns>A PAYMENT_RULE object populated with data.</returns>
        private static PAYMENT_RULE CreatePaymentRule(string scheduledFirstPaymentDate_rep, string initialPIPaymentAmount_rep, E_sTRIDPartialPaymentAcceptanceT partialPaymentsAccepted, bool isClosingPackage)
        {
            var paymentRule = new PAYMENT_RULE();
            paymentRule.PaymentFrequencyType = ToPaymentFrequencyEnum(PaymentFrequencyBase.Monthly);
            paymentRule.ScheduledFirstPaymentDate = ToMismoDate(scheduledFirstPaymentDate_rep);
            paymentRule.InitialPrincipalAndInterestPaymentAmount = ToMismoAmount(initialPIPaymentAmount_rep);

            if (isClosingPackage)
            {
                paymentRule.PartialPaymentAllowedIndicator = ToMismoIndicator(partialPaymentsAccepted != E_sTRIDPartialPaymentAcceptanceT.NotAccepted);
            }

            return paymentRule;
        }

        /// <summary>
        /// Creates a container holding information on a principal and interest payment lifetime adjustment rule.
        /// </summary>
        /// <param name="paymentMax_rep">The stated maximum principal and interest payment amount allowed over the life of the loan.</param>
        /// <param name="paymentMaxEarliestEffectiveMonthsCount_rep">The minimum number of months before the maximum payment amount to which the principal and interest payment can increase over the life of the loan could be reached.</param>
        /// <param name="firstPIChangeDate_rep">The date of the first scheduled principal and interest payment change.</param>
        /// <param name="canPaymentIncrease">True if the principal and interest payment can increase over the life of the loan. False if it cannot.</param>
        /// <param name="firstPIPaymentChangeMonthsCount_rep">The number of months after origination in which the first payment adjustment occurs.</param>
        /// <returns>A PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE.</returns>
        private static PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE CreatePrincipalAndInterestPaymentLifetimeAdjustmentRule(string paymentMax_rep, string paymentMaxEarliestEffectiveMonthsCount_rep, string firstPIChangeDate_rep, bool canPaymentIncrease, string firstPIPaymentChangeMonthsCount_rep)
        {
            var principalAndInterestPaymentLifetimeAdjustmentRule = new PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE();
            principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentMaximumAmount = ToMismoAmount(paymentMax_rep);
            principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount = ToMismoCount(paymentMaxEarliestEffectiveMonthsCount_rep, null);
            principalAndInterestPaymentLifetimeAdjustmentRule.FirstPrincipalAndInterestPaymentChangeDate = ToMismoDate(firstPIChangeDate_rep);

            if (canPaymentIncrease)
            {
                principalAndInterestPaymentLifetimeAdjustmentRule.FirstPrincipalAndInterestPaymentChangeMonthsCount = ToMismoCount(firstPIPaymentChangeMonthsCount_rep, null);
            }

            return principalAndInterestPaymentLifetimeAdjustmentRule;
        }

        /// <summary>
        /// Creates an IDENTIFIED_SERVICE_PROVIDERS element with an IDENTIFIED_SERVICE_PROVIDER sub-container for each supplied provider.
        /// </summary>
        /// <param name="providers">The settlement service providers to include.</param>
        /// <returns>An IDENTIFIED_SERVICE_PROVIDERS element with an IDENTIFIED_SERVICE_PROVIDER sub-container for each supplied provider.</returns>
        private static IDENTIFIED_SERVICE_PROVIDERS CreateIdentifiedServiceProviders(IEnumerable<SettlementServiceProvider> providers)
        {
            if (providers == null || !providers.Any())
            {
                return null;
            }

            var identifiedServiceProviders = new IDENTIFIED_SERVICE_PROVIDERS();

            foreach (var provider in providers)
            {
                int sequence = identifiedServiceProviders.IdentifiedServiceProvider.Count(p => p != null) + 1;

                identifiedServiceProviders.IdentifiedServiceProvider.Add(
                    CreateIdentifiedServiceProvider(provider, sequence));
            }

            return identifiedServiceProviders;
        }

        /// <summary>
        /// Creates an IDENTIFIED_SERVICE_PROVIDER container with information
        /// from a service provider identified on the Settlement Service
        /// Provider List page.
        /// </summary>
        /// <param name="provider">The settlement service provider.</param>
        /// <param name="sequence">The sequence number for this provider.</param>
        /// <returns>
        /// A IDENTIFIED_SERVICE_PROVIDER container with information from a
        /// service provider identified on the Settlement Service Provider
        /// List page.
        /// </returns>
        private static IDENTIFIED_SERVICE_PROVIDER CreateIdentifiedServiceProvider(SettlementServiceProvider provider, int sequence)
        {
            if (provider == null)
            {
                return null;
            }

            var element = new IDENTIFIED_SERVICE_PROVIDER();
            var addresses = CreateAddresses(provider);
            element.SequenceNumber = sequence;

            if (addresses.Address != null && addresses.Address.Count(a => a != null) > 0)
            {
                element.Address = addresses.Address.Where(a => a != null).FirstOrDefault();
            }

            element.LegalEntity = CreateLegalEntity(provider);

            return element;
        }

        /// <summary>
        /// Creates a container holding data on a rate or payment change.
        /// </summary>
        /// <param name="indexRatePercent_rep">The index value used to calculate the interest rate percent for this adjustment.</param>
        /// <param name="sequence">The sequence number of the payment change occurrence among the list of occurrences.</param>
        /// <returns>A RATE_OR_PAYMENT_CHANGE_OCCURRENCE populated with data.</returns>
        private static RATE_OR_PAYMENT_CHANGE_OCCURRENCE CreateRateOrPaymentChangeOccurrence(string indexRatePercent_rep, int sequence)
        {
            var rateOrPaymentChangeOccurrence = new RATE_OR_PAYMENT_CHANGE_OCCURRENCE();
            rateOrPaymentChangeOccurrence.SequenceNumber = sequence;
            rateOrPaymentChangeOccurrence.AdjustmentChangeIndexRatePercent = ToMismoPercent(indexRatePercent_rep);

            return rateOrPaymentChangeOccurrence;
        }

        /// <summary>
        /// Creates a container that can hold multiple instances of RATE_OR_PAYMENT_CHANGE_OCCURRENCE.
        /// </summary>
        /// <param name="indexRatePercent_rep">The index value used to calculate the interest rate percent for this adjustment.</param>
        /// <returns>A RATE_OR_PAYMENT_CHANGE_OCCURRENCES container.</returns>
        private static RATE_OR_PAYMENT_CHANGE_OCCURRENCES CreateRateOrPaymentChangeOccurrences(string indexRatePercent_rep)
        {
            var rateOrPaymentChangeOccurrences = new RATE_OR_PAYMENT_CHANGE_OCCURRENCES();
            rateOrPaymentChangeOccurrences.RateOrPaymentChangeOccurrence.Add(CreateRateOrPaymentChangeOccurrence(indexRatePercent_rep, 1));

            return rateOrPaymentChangeOccurrences;
        }

        /// <summary>
        /// Creates a REQUIRED_SERVICE_PROVIDER container with information regarding the company of the given agent, a service provider required by the lender. 
        /// </summary>
        /// <param name="agent">The contact record associated with the service provider.</param>
        /// <returns>A REQUIRED_SERVICE_PROVIDER container with information regarding the company of the given agent, a service provider required by the lender.</returns>
        private static REQUIRED_SERVICE_PROVIDER CreateRequiredServiceProvider(CAgentFields agent)
        {
            if (!agent.IsValid || agent.IsNewRecord)
            {
                return null;
            }

            var provider = new REQUIRED_SERVICE_PROVIDER();
            var addresses = CreateAddresses(agent);

            if (addresses.Address != null && addresses.Address.Count(a => a != null) > 0)
            {
                provider.Address = addresses.Address.Where(a => a != null).FirstOrDefault();
            }

            provider.LegalEntity = CreateLegalEntity(agent, agent.CompanyName, ContactPointRoleBase.Work);
            provider.RequiredServiceProviderDetail = CreateRequiredServiceProviderDetail(agent);

            return provider;
        }

        /// <summary>
        /// Creates a REQUIRED_SERVICE_PROVIDER_DETAIL container with information regarding the service provider's relationship with the lender.
        /// </summary>
        /// <param name="provider">The agent record of the service provider.</param>
        /// <returns>A REQUIRED_SERVICE_PROVIDER_DETAIL container with information regarding the service provider's relationship with the lender.</returns>
        private static REQUIRED_SERVICE_PROVIDER_DETAIL CreateRequiredServiceProviderDetail(CAgentFields provider)
        {
            var detail = new REQUIRED_SERVICE_PROVIDER_DETAIL();
            string relationship = string.Empty;

            if (provider.IsLender)
            {
                relationship = "This company is the lender";
            }
            else if (provider.IsLenderAssociation)
            {
                relationship = "Associate of the lender";
            }
            else if (provider.IsLenderRelative)
            {
                relationship = "Relative of the lender";
            }
            else if (provider.HasLenderRelationship)
            {
                relationship = "Has an employment, franchise or other business relationship with the lender";
            }
            else if (provider.HasLenderAccountLast12Months)
            {
                relationship = "Within the last 12 months, the provider has maintained an account with the lender or had an outstanding loan or credit arrangement with the lender";
            }
            else if (provider.IsUsedRepeatlyByLenderLast12Months)
            {
                relationship = "Within the last 12 months, the lender has repeatedly used or required borrowers to use the services of this provider";
            }
            else if (provider.IsLenderAffiliate)
            {
                relationship = "Affiliate of the lender";
            }

            if (string.IsNullOrEmpty(relationship))
            {
                return null;
            }

            detail.RequiredServiceProviderNatureOfRelationshipDescription = ToMismoString(relationship);

            return detail;
        }

        /// <summary>
        /// Creates a container holding data on the terms of a loan.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="vendor">The vendor to receive the MISMO payload.</param>
        /// <param name="helocEnabled">True if the HELOC feature is enabled for the lender. Otherwise false.</param>
        /// <returns>A TERMS_OF_LOAN object populated with data.</returns>
        private static TERMS_OF_LOAN CreateTermsOfLoan(CPageData loanData, E_DocumentVendor vendor, bool helocEnabled)
        {
            var termsOfLoan = new TERMS_OF_LOAN();
            termsOfLoan.LienPriorityType = ToLienPriorityEnum(loanData.sLienPosT);
            termsOfLoan.BaseLoanAmount = ToMismoAmount(loanData.sIsLineOfCredit ? loanData.sCreditLineAmt_rep : loanData.sLAmtCalc_rep);

            termsOfLoan.MortgageType = ToMortgageEnum(loanData.sLT);

            if (termsOfLoan.MortgageType != null && termsOfLoan.MortgageType.enumValue == MortgageBase.Other)
            {
                termsOfLoan.MortgageTypeOtherDescription = ToMismoString(loanData.sLTODesc);
            }

            // sg 10/20/2015 - OPM 229518: DocMagic specific expected values for HELOC
            if (vendor == E_DocumentVendor.DocMagic && loanData.sIsLineOfCredit)
            {
                termsOfLoan.MortgageType = ToMortgageEnum(E_sLT.Other);
                termsOfLoan.MortgageTypeOtherDescription = ToMismoString("HELOC");
            }

            termsOfLoan.NoteRatePercent = ToMismoPercent(string.IsNullOrEmpty(loanData.sNoteIR_rep) ? "0.00" : loanData.sNoteIR_rep);
            termsOfLoan.NoteAmount = ToMismoAmount(loanData.sFinalLAmt_rep);

            bool isHeloc = helocEnabled && loanData.sIsLineOfCredit;
            if (IsConstructionLoan(loanData.sLPurposeT))
            {
                termsOfLoan.LoanPurposeType = ToLoanPurposeEnum(loanData.sNMLSLoanPurposeT);
            }
            else
            {
                termsOfLoan.LoanPurposeType = ToLoanPurposeEnum(loanData.sLPurposeT, isHeloc);
            }

            termsOfLoan.LoanPurposeTypeOtherDescription = ToMismoString(isHeloc ? "HELOC" : loanData.sOLPurposeDesc);

            if (loanData.sFinMethT == E_sFinMethT.ARM)
            {
                termsOfLoan.DisclosedIndexRatePercent = ToMismoPercent(loanData.sRAdjIndexR_rep);
            }

            return termsOfLoan;
        }

        /// <summary>
        /// Returns a value indicating whether this loan is a construction loan. Used for determining whether to send the CONSTRUCTION element and setting ConstructionLoanIndicator = True.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose, e.g. Purchase, Refi, ConstructPerm, etc.</param>
        /// <returns>A value indicating whether this loan is a construction loan.</returns>
        private static bool IsConstructionLoan(E_sLPurposeT loanPurpose)
        {
            return loanPurpose == E_sLPurposeT.Construct || loanPurpose == E_sLPurposeT.ConstructPerm;
        }

        /// <summary>
        /// Creates a container holding data on a borrower's social security number.
        /// </summary>
        /// <param name="identifier">The taxpayer identifier number.</param>
        /// <param name="type">The taxpayer identifier type.</param>
        /// <param name="sequenceNumber">The sequence number for this element.</param>
        /// <param name="displaySensitive">Indicates whether the contained taxpayer identifier and identifier type should display their "SensitiveIndicator" attribute.</param>
        /// <returns>A TAXPAYER_IDENTIFIER container.</returns>
        private static TAXPAYER_IDENTIFIER CreateTaxpayerIdentifier(string identifier, TaxpayerIdentifierBase type, int sequenceNumber, bool displaySensitive = true)
        {
            var taxpayerIdentifier = new TAXPAYER_IDENTIFIER();
            taxpayerIdentifier.TaxpayerIdentifierValue = ToMismoNumericString(identifier, isSensitiveData: true, displaySensitive: displaySensitive);
            taxpayerIdentifier.TaxpayerIdentifierType = ToTaxpayerIdentifierEnum(type, displaySensitive: displaySensitive);
            taxpayerIdentifier.SequenceNumber = sequenceNumber;

            return taxpayerIdentifier;
        }
        
        /// <summary>
        /// Creates a container holding data on a specific credit score.
        /// </summary>
        /// <param name="agency">The credit reporting agency.</param>
        /// <param name="date">The issue date of the credit report.</param>
        /// <param name="score">The credit score.</param>
        /// <param name="percentile">The credit score percentile.</param>
        /// <param name="model">The credit scoring model type.</param>
        /// <param name="modelOtherDescription">The credit scoring model other description.</param>
        /// <returns>A CREDIT_SCORE_DETAIL container.</returns>
        private static CREDIT_SCORE_DETAIL CreateCreditScoreDetail(CreditRepositorySourceBase agency, string date, string score, string percentile, E_CreditScoreModelT? model, string modelOtherDescription)
        {
            var creditScoreDetail = new CREDIT_SCORE_DETAIL();
            creditScoreDetail.CreditRepositorySourceType = ToCreditRepositorySourceEnum(agency);
            creditScoreDetail.CreditScoreDate = ToMismoDate(date, isSensitiveData: true);
            creditScoreDetail.CreditScoreValue = ToMismoValue(score, isSensitiveData: true);
            creditScoreDetail.CreditScoreRankPercentileValue = ToMismoValue(percentile, isSensitiveData: true);

            if (model.HasValue)
            {
                creditScoreDetail.CreditScoreModelNameType = ToCreditScoreModelNameEnum(model.Value);

                if (creditScoreDetail.CreditScoreModelNameType?.enumValue == CreditScoreModelNameBase.Other)
                {
                    string otherDescription = model.Value == E_CreditScoreModelT.Other ? modelOtherDescription : EnumUtilities.GetDescription(model.Value);
                    creditScoreDetail.CreditScoreModelNameTypeOtherDescription = ToMismoString(otherDescription);
                }
            }

            return creditScoreDetail;
        }

        /// <summary>
        /// Creates a container holding factors of a borrower's credit score.
        /// </summary>
        /// <param name="factors">A list of factors delimited by newlines.</param>
        /// <returns>A CREDIT_SCORE_FACTORS container.</returns>
        private static CREDIT_SCORE_FACTORS CreateCreditScoreFactors(string factors)
        {
            if (string.IsNullOrEmpty(factors))
            {
                return null;
            }

            var creditScoreFactors = new CREDIT_SCORE_FACTORS();

            foreach (string factor in factors.Split(Environment.NewLine.ToCharArray()))
            {
                if (string.IsNullOrEmpty(factor))
                {
                    continue;
                }

                creditScoreFactors.CreditScoreFactor.Add(CreateCreditScoreFactor(factor, creditScoreFactors.CreditScoreFactor.Count(c => c != null) + 1));
            }

            return creditScoreFactors;
        }

        /// <summary>
        /// Creates a container holding a single credit score factor.
        /// </summary>
        /// <param name="factor">The credit score factor.</param>
        /// <param name="sequence">The sequence number of the credit score factor among the list of factors.</param>
        /// <returns>A CREDIT_SCORE_FACTOR container.</returns>
        private static CREDIT_SCORE_FACTOR CreateCreditScoreFactor(string factor, int sequence)
        {
            var creditScoreFactor = new CREDIT_SCORE_FACTOR();
            creditScoreFactor.CreditScoreFactorText = ToMismoString(factor, isSensitiveData: true);
            creditScoreFactor.SequenceNumber = sequence;

            return creditScoreFactor;
        }

        /// <summary>
        /// Creates NAME object with individual names specified.
        /// </summary>
        /// <param name="first">The first name.</param>
        /// <param name="middle">The middle name or initial.</param>
        /// <param name="last">The last name.</param>
        /// <param name="suffix">The suffix for the name.</param>
        /// <param name="fullName">The unparsed full name.</param>
        /// <returns>A NAME object populated with the specified names.</returns>
        private static NAME CreateName(string first, string middle, string last, string suffix, string fullName)
        {
            NAME name = new NAME();
            name.FirstName = ToMismoString(first);
            name.FullName = ToMismoString(fullName);
            name.MiddleName = ToMismoString(middle);
            name.LastName = ToMismoString(last);
            name.SuffixName = ToMismoString(suffix);

            return name;
        }

        /// <summary>
        /// Creates a container holding data on index rules for a construction loan.
        /// </summary>
        /// <returns>An INDEX_RULES container.</returns>
        private INDEX_RULES CreateIndexRules_Construction()
        {
            var indexRules = new INDEX_RULES();
            indexRules.IndexRule.Add(this.CreateIndexRule_Construction());

            return indexRules;
        }

        /// <summary>
        /// Creates a container holding data on an index rule for a construction loan.
        /// </summary>
        /// <returns>An INDEX_RULE container.</returns>
        private INDEX_RULE CreateIndexRule_Construction()
        {
            var indexRule = new INDEX_RULE();
            indexRule.IndexType = ToIndexEnum(IndexBase.Other);
            indexRule.IndexTypeOtherDescription = ToMismoString(this.loanData.sConstructionArmIndexNameVstr);

            indexRule.SequenceNumber = 1;
            return indexRule;
        }

        /// <summary>
        /// Creates a container holding data on an interest rate adjustment for a construction ARM.
        /// </summary>
        /// <returns>An INTEREST_RATE_ADJUSTMENT container.</returns>
        private INTEREST_RATE_ADJUSTMENT CreateInterestRateAdjustment_Construction()
        {
            var interestRateAdjustment = new INTEREST_RATE_ADJUSTMENT();
            interestRateAdjustment.InterestRatePerChangeAdjustmentRules = this.CreateInterestRatePerChangeAdjustmentRules_Construction();
            interestRateAdjustment.InterestRateLifetimeAdjustmentRule = this.CreateInterestRateLifetimeAdjustmentRule_Construction();
            interestRateAdjustment.IndexRules = this.CreateIndexRules_Construction();

            return interestRateAdjustment;
        }

        /// <summary>
        /// Creates a container holding data on interest rate adjustment rules for a construction ARM.
        /// </summary>
        /// <returns>An INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES container.</returns>
        private INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES CreateInterestRatePerChangeAdjustmentRules_Construction()
        {
            var adjustmentRules = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES();
            adjustmentRules.InterestRatePerChangeAdjustmentRule.Add(this.CreateInterestRatePerChangeAdjustmentRule_Construction(sequenceNumber: 1));
            adjustmentRules.InterestRatePerChangeAdjustmentRule.Add(this.CreateInterestRatePerChangeAdjustmentRule_Construction(sequenceNumber: 2));

            return adjustmentRules;
        }

        /// <summary>
        /// Creates a container holding interest rate adjustment rules for a construction ARM.
        /// </summary>
        /// <param name="sequenceNumber">The container's sequence number.</param>
        /// <returns>An INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE container.</returns>
        private INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE CreateInterestRatePerChangeAdjustmentRule_Construction(int sequenceNumber)
        {
            var adjustmentRule = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE();

            if (sequenceNumber == 1)
            {
                adjustmentRule.AdjustmentRuleType = ToAdjustmentRuleEnum(AdjustmentRuleBase.First);
                adjustmentRule.PerChangeMaximumIncreaseRatePercent = ToMismoPercent(this.loanData.sConstructionRAdj1stCapR_rep);
                adjustmentRule.PerChangeRateAdjustmentFrequencyMonthsCount = ToMismoCount(this.loanData.sConstructionRAdj1stCapMon_rep, this.loanData.m_convertLos);
            }
            else
            {
                adjustmentRule.AdjustmentRuleType = ToAdjustmentRuleEnum(AdjustmentRuleBase.Subsequent);
                adjustmentRule.PerChangeMaximumIncreaseRatePercent = ToMismoPercent(this.loanData.sConstructionRAdjCapR_rep);
                adjustmentRule.PerChangeRateAdjustmentFrequencyMonthsCount = ToMismoCount(this.loanData.sConstructionRAdjCapMon_rep, this.loanData.m_convertLos);
            }

            return adjustmentRule;
        }

        /// <summary>
        /// Creates a container holding data on a lifetime adjustment rule for a construction ARM.
        /// </summary>
        /// <returns>An INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE container.</returns>
        private INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE CreateInterestRateLifetimeAdjustmentRule_Construction()
        {
            var adjustmentRule = new INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE();
            adjustmentRule.MarginRatePercent = ToMismoPercent(this.loanData.sConstructionRAdjMarginR_rep);
            adjustmentRule.MaximumIncreaseRatePercent = ToMismoPercent(this.loanData.sConstructionRAdjLifeCapR_rep);
            adjustmentRule.FloorRatePercent = ToMismoPercent(this.loanData.sConstructionRAdjFloorR_rep);
            adjustmentRule.InterestRateRoundingType = ToInterestRateRoundingEnum(this.loanData.sConstructionRAdjRoundT);
            adjustmentRule.InterestRateRoundingPercent = ToMismoPercent(this.loanData.sConstructionRAdjRoundToR_rep);

            return adjustmentRule;
        }

        /// <summary>
        /// Create a TERMS_OF_LOAN container for the related loan.
        /// </summary>
        /// <returns>The TERMS_OF_LOAN container.</returns>
        private TERMS_OF_LOAN CreateTermsOfLoanForRelatedLoan()
        {
            var termsOfLoan = new TERMS_OF_LOAN();
            termsOfLoan.MortgageType = ToMortgageEnum(this.loanData.sVaPriorLoanT);
            termsOfLoan.NoteRatePercent = ToMismoPercent(this.loanData.sLoanBeingRefinancedInterestRate_rep);
            termsOfLoan.LienPriorityType = ToLienPriorityEnum(this.loanData.sLoanBeingRefinancedLienPosT);
            return termsOfLoan;
        }

        /// <summary>
        /// Creates a MATURITY container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private MATURITY CreateMaturity_RelatedLoan()
        {
            MATURITY maturity = new MATURITY();
            maturity.MaturityOccurrences = this.CreateMaturityOccurences_RelatedLoan();

            return maturity;
        }

        /// <summary>
        /// Creates a MATURITY_OCCURENCES container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private MATURITY_OCCURRENCES CreateMaturityOccurences_RelatedLoan()
        {
            MATURITY_OCCURRENCES maturityOccurences = new MATURITY_OCCURRENCES();
            maturityOccurences.MaturityOccurrence.Add(this.CreateMaturityOccurence_RelatedLoan());

            return maturityOccurences;
        }

        /// <summary>
        /// Creates a MATURITY_OCCURENCE container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private MATURITY_OCCURRENCE CreateMaturityOccurence_RelatedLoan()
        {
            MATURITY_OCCURRENCE maturityOccurence = new MATURITY_OCCURRENCE();
            maturityOccurence.LoanRemainingMaturityTermMonthsCount = ToMismoCount(this.loanData.sLoanBeingRefinancedRemainingTerm_rep, this.loanData.m_convertLos);

            return maturityOccurence;
        }

        /// <summary>
        /// Creates a FEE_INFORMATION container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private FEE_INFORMATION CreateFeeInformation_RelatedLoan()
        {
            FEE_INFORMATION feeInformation = new FEE_INFORMATION();
            feeInformation.FeesSummary = this.CreateFeeSummary_RelatedLoan();

            return feeInformation;
        }

        /// <summary>
        /// Creates a FEES_SUMMARY container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private FEES_SUMMARY CreateFeeSummary_RelatedLoan()
        {
            FEES_SUMMARY feeSummary = new FEES_SUMMARY();
            feeSummary.FeeSummaryDetail = this.CreateFeeSummaryDetail_RelatedLoan();

            return feeSummary;
        }

        /// <summary>
        /// Creates a FEE_SUMMARY_DETAIL container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private FEE_SUMMARY_DETAIL CreateFeeSummaryDetail_RelatedLoan()
        {
            FEE_SUMMARY_DETAIL feeSummaryDetail = new FEE_SUMMARY_DETAIL();
            feeSummaryDetail.FeeSummaryTotalOfAllPaymentsAmount = ToMismoAmount(this.loanData.sLoanBeingRefinancedTotalOfPayments_rep);

            return feeSummaryDetail;
        }

        /// <summary>
        /// Creates a LTV container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private LTV CreateLtv_RelatedLoan()
        {
            LTV ltv = new LTV();
            ltv.LTVRatioPercent = ToMismoPercent(this.loanData.sLoanBeingRefinancedLtvR_rep);

            return ltv;
        }

        /// <summary>
        /// Creates a LOAN_DETAIL container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private LOAN_DETAIL CreateLoanDetail_RelatedLoan()
        {
            LOAN_DETAIL loanDetail = new LOAN_DETAIL();
            loanDetail.Extension = this.CreateLoanDetailExtension_RelatedLoan();

            return loanDetail;
        }

        /// <summary>
        /// Creates a LOAN_DETAIL_EXTENSION container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private LOAN_DETAIL_EXTENSION CreateLoanDetailExtension_RelatedLoan()
        {
            LOAN_DETAIL_EXTENSION loanDetailExtension = new LOAN_DETAIL_EXTENSION();
            loanDetailExtension.Other = this.CreateLqbLoanDetailExtension_RelatedLoan();

            return loanDetailExtension;
        }

        /// <summary>
        /// Creates a LQB_LOAN_DETAIL_EXTENSION container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private LQB_LOAN_DETAIL_EXTENSION CreateLqbLoanDetailExtension_RelatedLoan()
        {
            LQB_LOAN_DETAIL_EXTENSION extension = new LQB_LOAN_DETAIL_EXTENSION();
            extension.LoanBeingRefinancedUsedToConstructAlterRepairIndicator = ToMismoIndicator(this.loanData.sLoanBeingRefinancedUsedToConstructAlterRepair);

            return extension;
        }

        /// <summary>
        /// Creates a PAYMENT container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private PAYMENT CreatePayment_RelatedLoan()
        {
            PAYMENT payment = new PAYMENT();
            payment.PaymentSummary = this.CreatePaymentSummary_RelatedLoan();

            return payment;
        }

        /// <summary>
        /// Creates a PAYMENT_SUMMARY container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private PAYMENT_SUMMARY CreatePaymentSummary_RelatedLoan()
        {
            PAYMENT_SUMMARY paymentSummary = new PAYMENT_SUMMARY();
            paymentSummary.ScheduledUPBAmount = ToMismoAmount(this.loanData.sSpLien_rep);

            return paymentSummary;
        }

        /// <summary>
        /// Creates a container holding data on an adjustment.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>An ADJUSTMENT populated with data.</returns>
        private ADJUSTMENT CreateAdjustment(CPageData loanData)
        {
            if (loanData.sFinMethT != E_sFinMethT.ARM)
            {
                return null;
            }

            var adjustment = new ADJUSTMENT();
            adjustment.InterestRateAdjustment = CreateInterestRateAdjustment(loanData);
            adjustment.RateOrPaymentChangeOccurrences = CreateRateOrPaymentChangeOccurrences(loanData.sRAdjIndexR_rep);
            adjustment.PrincipalAndInterestPaymentAdjustment = this.CreatePrincipalAndInterestPaymentAdjustment(loanData);

            return adjustment;
        }

        /// <summary>
        /// Creates a container holding data on an adjustment for a construction ARM.
        /// </summary>
        /// <returns>An ADJUSTMENT container.</returns>
        private ADJUSTMENT CreateAdjustment_Construction()
        {
            var adjustment = new ADJUSTMENT();
            adjustment.InterestRateAdjustment = this.CreateInterestRateAdjustment_Construction();
            adjustment.RateOrPaymentChangeOccurrences = this.CreateRateOrPaymentChangeOccurrences_Construction();

            return adjustment;
        }

        /// <summary>
        /// Creates a container holding sub-elements with information on principal and interest payment adjustments.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>A PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT.</returns>
        private PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT CreatePrincipalAndInterestPaymentAdjustment(CPageData loanData)
        {
            var principalAndInterestPaymentAdjustment = new PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT();
            principalAndInterestPaymentAdjustment.PrincipalAndInterestPaymentPerChangeAdjustmentRules = this.CreatePrincipalAndInterestPaymentPerChangeAdjustmentRules(loanData);

            principalAndInterestPaymentAdjustment.PrincipalAndInterestPaymentLifetimeAdjustmentRule = CreatePrincipalAndInterestPaymentLifetimeAdjustmentRule(
                                                                                                        loanData.sTRIDPIPmtMaxEver_rep,
                                                                                                        loanData.sPIPmtMaxEverStartAfterMon_rep,
                                                                                                        loanData.sRAdj1stD_rep,
                                                                                                        loanData.sGfeCanPaymentIncrease,
                                                                                                        loanData.sPIPmtFirstChangeAfterMon_rep);

            return principalAndInterestPaymentAdjustment;
        }

        /// <summary>
        /// Creates a container that holds all instances of PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>A PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES container.</returns>
        private PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES CreatePrincipalAndInterestPaymentPerChangeAdjustmentRules(CPageData loanData)
        {
            var principalAndInterestPaymentPerChangeAdjustmentRules = new PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES();
            principalAndInterestPaymentPerChangeAdjustmentRules.PrincipalAndInterestPaymentPerChangeAdjustmentRule.Add(this.CreatePrincipalAndInterestPaymentPerChangeAdjustmentRule(loanData, 1));
            principalAndInterestPaymentPerChangeAdjustmentRules.PrincipalAndInterestPaymentPerChangeAdjustmentRule.Add(this.CreatePrincipalAndInterestPaymentPerChangeAdjustmentRule(loanData, 2));

            return principalAndInterestPaymentPerChangeAdjustmentRules;
        }

        /// <summary>
        /// Creates a container holding data on a particular principal and interest payment change rule.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="sequence">The sequence number of the adjustment rule among the set of rules.</param>
        /// <returns>A PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE container.</returns>
        private PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE CreatePrincipalAndInterestPaymentPerChangeAdjustmentRule(CPageData loanData, int sequence)
        {
            var rule = new PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE();
            rule.SequenceNumber = sequence;

            if (sequence == 1)
            {
                rule.AdjustmentRuleType = ToAdjustmentRuleEnum(AdjustmentRuleBase.First);
                rule.PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount = ToMismoCount(loanData.sPIPmtFirstChangeAfterMon_rep, null);
                rule.PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent = ToMismoPercent(loanData.sRAdj1stCapR_rep);

                if (loanData.sGfeCanPaymentIncrease && loanData.sPIPmtFirstChangeAfterMon.HasValue)
                {
                    try
                    {
                        int index = loanData.sPIPmtFirstChangeAfterMon.Value + 1;
                        AmortItem bestAmortItem = loanData.GetAmortTable(E_AmortizationScheduleT.BestCase, false).Items[index];
                        AmortItem worstAmortItem = loanData.GetAmortTable(E_AmortizationScheduleT.WorstCase, false).Items[index];

                        rule.PerChangeMinimumPrincipalAndInterestPaymentAmount = ToMismoAmount(loanData.m_convertLos.ToMoneyString(bestAmortItem.PaymentWOMI, FormatDirection.ToRep));

                        if (bestAmortItem.PaymentWOMI == worstAmortItem.PaymentWOMI)
                        {
                            rule.PerChangeMaximumPrincipalAndInterestPaymentAmount = rule.PerChangeMinimumPrincipalAndInterestPaymentAmount;
                        }
                        else
                        {
                            rule.PerChangeMaximumPrincipalAndInterestPaymentAmount = ToMismoAmount(loanData.m_convertLos.ToMoneyString(worstAmortItem.PaymentWOMI, FormatDirection.ToRep));
                        }
                    }
                    catch (AmortTableInvalidArgumentException) when (this.isLenderExportMode)
                    {
                        rule.PerChangeMinimumPrincipalAndInterestPaymentAmount = null;
                        rule.PerChangeMaximumPrincipalAndInterestPaymentAmount = null;
                    }
                }
            }
            else
            {
                rule.AdjustmentRuleType = ToAdjustmentRuleEnum(AdjustmentRuleBase.Subsequent);

                if (loanData.sGfeCanPaymentIncrease)
                {
                    rule.PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount = ToMismoCount(loanData.sTRIDPIPmtSubsequentAdjustFreqMon_rep, null);
                }

                rule.PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent = ToMismoPercent(loanData.sRAdjCapR_rep);
            }

            return rule;
        }

        /// <summary>
        /// Creates a container holding rate or payment change occurrences for a construction ARM.
        /// </summary>
        /// <returns>A RATE_OR_PAYMENT_CHANGE_OCCURRENCES container.</returns>
        private RATE_OR_PAYMENT_CHANGE_OCCURRENCES CreateRateOrPaymentChangeOccurrences_Construction()
        {
            var occurrences = new RATE_OR_PAYMENT_CHANGE_OCCURRENCES();
            occurrences.RateOrPaymentChangeOccurrence.Add(this.CreateRateOrPaymentChangeOccurrence_Construction());

            return occurrences;
        }

        /// <summary>
        /// Creates a container holding a rate or payment change occurrence for a construction ARM.
        /// </summary>
        /// <returns>A RATE_OR_PAYMENT_CHANGE_OCCURRENCE container.</returns>
        private RATE_OR_PAYMENT_CHANGE_OCCURRENCE CreateRateOrPaymentChangeOccurrence_Construction()
        {
            var occurrence = new RATE_OR_PAYMENT_CHANGE_OCCURRENCE();
            occurrence.AdjustmentChangeIndexRatePercent = ToMismoPercent(this.loanData.sConstructionRAdjIndexR_rep);
            occurrence.SequenceNumber = 1;

            return occurrence;
        }

        /// <summary>
        /// Creates a container representing the Truth In Lending document.
        /// </summary>
        /// <returns>A TIL_DISCLOSURE container.</returns>
        private TIL_DISCLOSURE CreateTilDisclosure()
        {
            var tilDisclosure = new TIL_DISCLOSURE();
            tilDisclosure.TilDisclosureDetail = this.CreateTilDisclosureDetail();

            return tilDisclosure;
        }

        /// <summary>
        /// Applies a loan estimate or closing disclosure archive to the associated loan data object.
        /// Note: This method will be replaced when OPM 214209 is complete. That one will allow an archive to be associated with the relevant meta-data from the general status page. The current method here will grab the wrong archive under certain circumstances, but it's the best we can do for now for testing purposes.
        /// </summary>
        /// <param name="docType">The type of integrated disclosure, e.g. loan estimate | closing disclosure.</param>
        /// <param name="archiveID">The GUID associated with the archive.</param>
        /// <remarks>For Loan Estimates, we check specifically for the Disclosed status instead of using the IsDisclosed property because the LE dates should NOT include LEs that were disclosed as part of a Closing Disclosure.</remarks>
        private void ApplyIntegratedDisclosureArchive(DocumentBase docType, Guid archiveID)
        {
            this.hasArchive = false;
            this.shouldOmitArchive = false;

            if (archiveID == null || archiveID == Guid.Empty || this.loanEstimateArchiveData == null || this.closingDisclosureArchiveData == null)
            {
                return;
            }

            if (docType == DocumentBase.LoanEstimate && this.loanEstimateArchiveData.sClosingCostArchive.Count != 0)
            {
                try
                {
                    var archive = this.loanEstimateArchiveData.sClosingCostArchive.FirstOrDefault(a => a.Id == archiveID);
                    if (archive == null)
                    {
                        return;
                    }
                    else if (archive.Status != ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed)
                    {
                        this.shouldOmitArchive = true;
                        return;
                    }

                    this.loanEstimateArchiveData.ApplyClosingCostArchive(archiveID);
                    this.hasArchive = true;
                }
                catch (ArgumentNullException)
                {
                    return;
                }
            }
            else if (docType == DocumentBase.ClosingDisclosure && this.closingDisclosureArchiveData.sClosingCostArchive.Count != 0)
            {
                var archive = this.closingDisclosureArchiveData.sClosingCostArchive.FirstOrDefault(a => a.Id == archiveID);
                if (archive == null)
                {
                    return;
                }
                else if (!archive.IsDisclosed)
                {
                    this.shouldOmitArchive = true;
                    return;
                }

                try
                {
                    this.closingDisclosureArchiveData.ApplyClosingCostArchive(archiveID);
                    this.hasArchive = true;
                }
                catch (ArgumentNullException)
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Creates ADDRESSES container holding one ADDRESS sub-container populated with information from Common Library Address object.
        /// </summary>
        /// <param name="address">Common Library Address object used to populate ADDRESS sub-container.</param>
        /// <param name="addressType">The type of address. E.G. current | mailing | etc.</param>
        /// <param name="countyName">The name of the county.</param>
        /// <param name="countyFIPSCode">The FIPS code assigned to the county.</param>
        /// <returns>Returns an ADDRESSES container holding one ADDRESS sub-container populated with information from Common Library Address object.</returns>
        private ADDRESSES CreateAddresses(CommonLib.Address address, AddressBase addressType, string countyName, string countyFIPSCode)
        {
            return CreateAddresses(
                address.StreetAddress, 
                address.City, 
                address.State, 
                address.Zipcode, 
                addressType,
                countyName, 
                countyFIPSCode);
        }

        /// <summary>
        /// Creates ADDRESSES container holding one ADDRESS sub-container.
        /// </summary>
        /// <param name="streetAddress">The street address.</param>
        /// <param name="city">The city listed in the address.</param>
        /// <param name="state">The state listed in the address.</param>
        /// <param name="zip">The ZIP code listed in the address.</param>
        /// <param name="addressType">The type of address. E.G. current | mailing | etc.</param>
        /// <param name="countyName">The name of the county.</param>
        /// <param name="countyFIPSCode">The FIPS code assigned to the county.</param>
        /// <returns>An ADDRESSES container holding one ADDRESS sub-container.</returns>
        private ADDRESSES CreateAddresses(string streetAddress, string city, string state, string zip, AddressBase addressType, string countyName, string countyFIPSCode)
        {
            var addresses = new ADDRESSES();
            addresses.Address.Add(CreateAddress(
                streetAddress,
                city,
                state,
                zip,
                countyName,
                countyFIPSCode,
                this.statesAndTerritories.GetStateName(state),
                addressType,
                1));

            return addresses;
        }

        /// <summary>
        /// Creates a container holding an alias.
        /// </summary>
        /// <param name="aliasString">The alias represented by the container.</param>
        /// <param name="sequence">The sequence number of the alias among the list of aliases.</param>
        /// <returns>An ALIAS container.</returns>
        private ALIAS CreateAlias(string aliasString, int sequence)
        {
            var alias = new ALIAS();
            alias.SequenceNumber = sequence;
            alias.Name = CreateName(aliasString, isLegalEntity: false);

            return alias;
        }

        /// <summary>
        /// Creates a container with a set of aliases. ALIAS containers will be created to
        /// hold one alias each.
        /// </summary>
        /// <param name="aliasArray">The set of aliases.</param>
        /// <returns>An ALIASES container.</returns>
        private ALIASES CreateAliases(string[] aliasArray)
        {
            var aliases = new ALIASES();
            
            foreach (string alias in aliasArray)
            {
                aliases.Alias.Add(this.CreateAlias(alias, aliases.Alias.Count(a => a != null) + 1));
            }

            return aliases;
        }

        /// <summary>
        /// Creates a container holding data about an allonge to a note.
        /// </summary>
        /// <returns>An ALLONGE_TO_NOTE container.</returns>
        private ALLONGE_TO_NOTE CreateAllongeToNote()
        {
            var allongeToNote = new ALLONGE_TO_NOTE();
            allongeToNote.AllongeToNotePayToTheOrderOfDescription = ToMismoString(this.loanData.sDocMagicTransferToInvestorCode);

            return allongeToNote;
        }

        /// <summary>
        /// Creates a container holding data on the amortization of a loan.
        /// </summary>
        /// <param name="isForSubjectLoan">Whether this AMORTIZATION container is meant for the subject loan or not.</param>
        /// <returns>An AMORTIZATION container.</returns>
        private AMORTIZATION CreateAmortization(bool isForSubjectLoan)
        {
            var amortization = new AMORTIZATION();
            amortization.AmortizationRule = isForSubjectLoan ? this.CreateAmortizationRule() : this.CreateAmortizationRule_RelatedLoan();

            return amortization;
        }

        /// <summary>
        /// Creates an AMORTIZATION_RULE container for the related loan.
        /// </summary>
        /// <returns>The AMORTIZATION_RULE container.</returns>
        private AMORTIZATION_RULE CreateAmortizationRule_RelatedLoan()
        {
            var amortizationRule = new AMORTIZATION_RULE();
            amortizationRule.AmortizationType = ToAmortizationEnum(this.loanData.sLoanBeingRefinancedAmortizationT);

            return amortizationRule;
        }

        /// <summary>
        /// Creates a container holding data on the rules governing amortization.
        /// </summary>
        /// <returns>An AMORTIZATION_RULE populated with data.</returns>
        private AMORTIZATION_RULE CreateAmortizationRule()
        {
            var amortizationRule = new AMORTIZATION_RULE();
            amortizationRule.AmortizationType = ToAmortizationEnum(this.loanData.sFinMethT);
            amortizationRule.AmortizationTypeOtherDescription = ToMismoString(this.loanData.sFinMethDesc);
            amortizationRule.LoanAmortizationPeriodCount = ToMismoCount(this.loanData.sTerm_rep, null);
            amortizationRule.LoanAmortizationPeriodType = ToLoanAmortizationPeriodEnum(LoanAmortizationPeriodBase.Month);

            return amortizationRule;
        }

        /// <summary>
        /// Creates a container to hold all instances of ASSET.
        /// </summary>
        /// <returns>An ASSETS container.</returns>
        private ASSETS CreateAssets()
        {
            var assets = new ASSETS();

            int appsCount = this.loanData.nApps;
            for (int appIndex = 0; appIndex < appsCount; appIndex++)
            {
                CAppData appData = this.loanData.GetAppData(appIndex);

                IAssetCollection assetCollection = appData.aAssetCollection;

                var retirement = assetCollection.GetRetirement(false);
                if (retirement != null && retirement.Val > 0)
                {
                    assets.Asset.Add(this.CreateAsset(retirement.Val_rep, AssetBase.RetirementFund, null, null, appData.aBMismoId));
                }

                var business = assetCollection.GetBusinessWorth(false);
                if (business != null && business.Val > 0)
                {
                    assets.Asset.Add(this.CreateAsset(business.Val_rep, AssetBase.NetWorthOfBusinessOwned, null, null, appData.aBMismoId));
                }

                var lifeInsurance = assetCollection.GetLifeInsurance(false);
                if (lifeInsurance != null && lifeInsurance.Val > 0)
                {
                    assets.Asset.Add(this.CreateAsset(lifeInsurance.Val_rep, AssetBase.LifeInsurance, lifeInsurance.FaceVal_rep, null, appData.aBMismoId));
                }

                var deposit1 = assetCollection.GetCashDeposit1(false);
                if (deposit1 != null && deposit1.Val > 0)
                {
                    assets.Asset.Add(this.CreateAsset(deposit1.Val_rep, AssetBase.EarnestMoneyCashDepositTowardPurchase, null, deposit1.Desc, appData.aBMismoId));
                }

                var deposit2 = assetCollection.GetCashDeposit2(false);
                if (deposit2 != null && deposit2.Val > 0)
                {
                    assets.Asset.Add(this.CreateAsset(deposit2.Val_rep, AssetBase.EarnestMoneyCashDepositTowardPurchase, null, deposit2.Desc, appData.aBMismoId));
                }

                for (int i = 0; i < assetCollection.CountRegular; i++)
                {
                    assets.Asset.Add(this.CreateAsset(assetCollection.GetRegularRecordAt(i), appData.aBMismoId, appData.aCMismoId, appData.aCIsValidNameSsn));
                }

                var ownedPropertyCollection = appData.aReCollection;
                for (int i = 0; i < ownedPropertyCollection.CountRegular; i++)
                {
                    assets.Asset.Add(this.CreateAsset(ownedPropertyCollection.GetRegularRecordAt(i), appData));
                }
            }

            return assets;
        }

        /// <summary>
        /// Creates a container representing a specific asset.
        /// </summary>
        /// <param name="marketValue">The market or cash value of the asset.</param>
        /// <param name="type">The type of asset.</param>
        /// <param name="faceValue">The face value of the asset.</param>
        /// <param name="holder">The name of the asset holder.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <returns>An ASSET container.</returns>
        private ASSET CreateAsset(string marketValue, AssetBase type, string faceValue, string holder, string borrowerLabel)
        {
            var asset = new ASSET();
            asset.AssetDetail = this.CreateAssetDetail(marketValue, type, faceValue);
            asset.AssetHolder = this.CreateAssetHolder(holder, null, null, null, null, isLegalEntity: true);
            asset.SequenceNumber = this.counters.Assets;
            asset.label = "Asset" + asset.SequenceNumberSerialized;

            this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.label, borrowerLabel));

            return asset;
        }

        /// <summary>
        /// Creates a container representing a regular asset.
        /// </summary>
        /// <param name="regularAsset">An object holding information on the asset.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <param name="coborrowerLabel">The $$xlink$$ label for the co-borrower (role).</param>
        /// <param name="coborrowerIsValidSSN">Co borrower valid SSN indicator from app data. Used for determining whether to add RELATIONSHIP.</param>
        /// <returns>An ASSET container.</returns>
        private ASSET CreateAsset(IAssetRegular regularAsset, string borrowerLabel, string coborrowerLabel, bool coborrowerIsValidSSN)
        {
            var asset = new ASSET();
            asset.AssetHolder = this.CreateAssetHolder(regularAsset.ComNm, regularAsset.StAddr, regularAsset.City, regularAsset.State, regularAsset.Zip, isLegalEntity: true);
            asset.AssetDetail = this.CreateAssetDetail(regularAsset);
            asset.SequenceNumber = this.counters.Assets;
            asset.label = "Asset" + asset.SequenceNumberSerialized;

            if (regularAsset.OwnerT == E_AssetOwnerT.Borrower || regularAsset.OwnerT == E_AssetOwnerT.Joint)
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.label, borrowerLabel));
            }
            
            if (coborrowerIsValidSSN && (regularAsset.OwnerT == E_AssetOwnerT.CoBorrower || regularAsset.OwnerT == E_AssetOwnerT.Joint))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.label, coborrowerLabel));
            }

            return asset;
        }

        /// <summary>
        /// Creates a container representing a real estate owned asset.
        /// </summary>
        /// <param name="reo">The real estate owned object.</param>
        /// <param name="appData">The borrower application.</param>
        /// <returns>An ASSET container.</returns>
        private ASSET CreateAsset(IRealEstateOwned reo, CAppData appData)
        {
            var asset = new ASSET();
            asset.AssetDetail = this.CreateAssetDetail(reo);
            asset.OwnedProperty = this.CreateOwnedProperty(reo);

            if (reo.ReOwnerT == E_ReOwnerT.Borrower || reo.ReOwnerT == E_ReOwnerT.Joint)
            {
                asset.AssetHolder = this.CreateAssetHolder(appData.aBNm, appData.aBAddr, appData.aBCity, appData.aBState, appData.aBZip, isLegalEntity: false);
            }
            else
            {
                asset.AssetHolder = this.CreateAssetHolder(appData.aCNm, appData.aCAddr, appData.aCCity, appData.aCState, appData.aCZip, isLegalEntity: false);
            }

            asset.SequenceNumber = this.counters.Assets;
            asset.label = "Asset" + asset.SequenceNumberSerialized;
            if (reo.ReOwnerT == E_ReOwnerT.Borrower || reo.ReOwnerT == E_ReOwnerT.Joint)
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.label, appData.aBMismoId));
            }

            if (appData.aCIsValidNameSsn && (reo.ReOwnerT == E_ReOwnerT.CoBorrower || reo.ReOwnerT == E_ReOwnerT.Joint))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.label, appData.aCMismoId));
            }

            this.reoAssetLabel.Add(reo.RecordId.ToString("D"), asset.label);

            return asset;
        }

        /// <summary>
        /// Creates a container with details on an asset.
        /// </summary>
        /// <param name="regularAsset">An object holding information on the asset.</param>
        /// <returns>An ASSET_DETAIL container.</returns>
        private ASSET_DETAIL CreateAssetDetail(IAssetRegular regularAsset)
        {
            var assetDetail = new ASSET_DETAIL();
            assetDetail.AssetAccountIdentifier = ToMismoIdentifier(regularAsset.AccNum.Value);
            assetDetail.AssetAccountInNameOfDescription = ToMismoString(regularAsset.AccNm);
            assetDetail.AssetCashOrMarketValueAmount = ToMismoAmount(regularAsset.Val_rep);
            assetDetail.AssetType = ToAssetEnum(regularAsset.AssetT);

            if (assetDetail.AssetType != null && assetDetail.AssetType.enumValue == AssetBase.Other)
            {
                if (regularAsset.AssetT.EqualsOneOf(E_AssetRegularT.OtherLiquidAsset, E_AssetRegularT.OtherIlliquidAsset))
                {
                    assetDetail.AssetTypeOtherDescription = ToMismoString(regularAsset.OtherTypeDesc);
                }
                else
                {
                    // We now support some values that are not represented in MISMO 3.3.
                    // If the type is not intended to be other, populate the other description with the actual type used. 
                    assetDetail.AssetTypeOtherDescription = ToMismoString(regularAsset.AssetT.ToString());
                }
            }

            if (assetDetail.AssetType != null && assetDetail.AssetType.enumValue == AssetBase.Automobile)
            {
                assetDetail.AutomobileMakeDescription = ToMismoString(regularAsset.Desc);
            }

            return assetDetail;
        }

        /// <summary>
        /// Creates a container with details on an asset.
        /// </summary>
        /// <param name="reo">An object holding information on the real-estate owned asset.</param>
        /// <returns>An ASSET_DETAIL container.</returns>
        private ASSET_DETAIL CreateAssetDetail(IRealEstateOwned reo)
        {
            var assetDetail = new ASSET_DETAIL();
            assetDetail.AssetCashOrMarketValueAmount = ToMismoAmount(reo.Val_rep);
            assetDetail.AssetNetValueAmount = ToMismoAmount(reo.NetValue_rep);
            assetDetail.AssetType = ToAssetEnum(AssetBase.RealEstateOwned);

            return assetDetail;
        }

        /// <summary>
        /// Creates a container holding details on an asset.
        /// </summary>
        /// <param name="marketValue">The market value of the asset.</param>
        /// <param name="type">The type of asset.</param>
        /// <param name="faceValue">The face value of the asset.</param>
        /// <returns>An ASSET_DETAIL container.</returns>
        private ASSET_DETAIL CreateAssetDetail(string marketValue, AssetBase type, string faceValue)
        {
            var assetDetail = new ASSET_DETAIL();
            assetDetail.AssetCashOrMarketValueAmount = ToMismoAmount(marketValue);
            assetDetail.AssetType = ToAssetEnum(type);

            if (assetDetail.AssetType != null && assetDetail.AssetType.enumValue == AssetBase.LifeInsurance)
            {
                assetDetail.LifeInsuranceFaceValueAmount = ToMismoAmount(faceValue);
            }
            
            return assetDetail;
        }

        /// <summary>
        /// Creates a container holding information on the asset's owner.
        /// </summary>
        /// <param name="name">The name of the asset holder.</param>
        /// <param name="streetAddress">The street address of the asset holder.</param>
        /// <param name="city">The city of the asset holder.</param>
        /// <param name="state">The state of the asset holder.</param>
        /// <param name="zip">The ZIP code of the asset holder.</param>
        /// <param name="isLegalEntity">Indicates whether the asset holder is a legal entity.</param>
        /// <returns>An ASSET_HOLDER container.</returns>
        private ASSET_HOLDER CreateAssetHolder(string name, string streetAddress, string city, string state, string zip, bool isLegalEntity)
        {
            var assetHolder = new ASSET_HOLDER();
            assetHolder.Name = CreateName(name, isLegalEntity);
            
            assetHolder.Address = CreateAddress(
                streetAddress, 
                city, 
                state, 
                zip, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(state),
                AddressBase.Blank, 
                1);

            return assetHolder;
        }

        /// <summary>
        /// Creates a container holding information about the assumption option of the loan.
        /// </summary>
        /// <returns>An ASSUMABILITY container.</returns>
        private ASSUMABILITY CreateAssumability()
        {
            if (this.loanData.sAssumeLT == E_sAssumeLT.LeaveBlank || this.loanData.sAssumeLT == E_sAssumeLT.MayNot)
            {
                return null;
            }

            var assumability = new ASSUMABILITY();
            assumability.AssumabilityRule = this.CreateAssumabilityRule();

            return assumability;
        }

        /// <summary>
        /// Creates a container holding data on the rules of the assumption option.
        /// </summary>
        /// <returns>An ASSUMABILITY_RULE container populated with data.</returns>
        private ASSUMABILITY_RULE CreateAssumabilityRule()
        {
            if (this.loanData.sAssumeLT == E_sAssumeLT.LeaveBlank || this.loanData.sAssumeLT == E_sAssumeLT.MayNot)
            {
                return null;
            }

            var assumabilityRule = new ASSUMABILITY_RULE();
            
            // MaySubjectToCondition maps to true, May maps to false.
            bool conditions = this.loanData.sAssumeLT == E_sAssumeLT.MaySubjectToCondition;
            assumabilityRule.ConditionsToAssumabilityIndicator = ToMismoIndicator(conditions);

            return assumabilityRule;
        }

        /// <summary>
        /// Creates a container with information about the automated underwriting process.
        /// </summary>
        /// <param name="sequence">The sequence number of the automated underwriting object.</param>
        /// <returns>An AUTOMATED_UNDERWRITING container.</returns>
        private AUTOMATED_UNDERWRITING CreateAutomatedUnderwriting(int sequence)
        {
            var automatedUnderwriting = new AUTOMATED_UNDERWRITING();
            automatedUnderwriting.SequenceNumber = sequence;
            automatedUnderwriting.AutomatedUnderwritingCaseIdentifier = ToMismoIdentifier(string.IsNullOrEmpty(this.loanData.sDuCaseId) ? this.loanData.sLpAusKey : this.loanData.sDuCaseId);
            automatedUnderwriting.AutomatedUnderwritingDecisionDatetime = ToMismoDatetime(GetUnderwritingDecisionDate(this.loanData.sApprovD, this.loanData.sApprovD_rep_WithTime, this.loanData.sRejectD, this.loanData.sRejectD_rep_WithTime), false);
            automatedUnderwriting.AutomatedUnderwritingSystemResultValue = ToAutomatedUnderwritingSystemResultValue(this.loanData.sFHARatedAcceptedByTotalScorecard, this.loanData.sFHARatedReferByTotalScorecard);
            automatedUnderwriting.AutomatedUnderwritingSystemType = ToAutomatedUnderwritingSystemEnum(this.loanData.sIsDuUw, this.loanData.sIsLpUw, this.loanData.sIsOtherUw);

            if (automatedUnderwriting.AutomatedUnderwritingSystemType != null && automatedUnderwriting.AutomatedUnderwritingSystemType.enumValue == AutomatedUnderwritingSystemBase.Other)
            {
                automatedUnderwriting.AutomatedUnderwritingSystemTypeOtherDescription = ToMismoString(this.loanData.sOtherUwDesc);
            }

            if (this.loanData.sLT == E_sLT.VA)
            {
                automatedUnderwriting.AutomatedUnderwritingRecommendationDescription = ToAutomatedUnderwritingRecommendationDescription(this.loanData.sVaRiskT);
            }
            else
            {
                automatedUnderwriting.AutomatedUnderwritingRecommendationDescription = ToAutomatedUnderwritingRecommendationDescription(this.loanData.sProd3rdPartyUwResultT);
            }

            if (!automatedUnderwriting.ShouldSerialize)
            {
                return null;
            }

            return automatedUnderwriting;
        }

        /// <summary>
        /// Creates a container to hold a UCD delivery casefile.
        /// </summary>
        /// <param name="ucdCaseFileId">The UCD casefile ID.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>An AUTOMATED_UNDERWRITING container.</returns>
        private AUTOMATED_UNDERWRITING CreateAutomatedUnderwriting_UcdDelivery(string ucdCaseFileId, int sequenceNumber)
        {
            var automatedUnderwriting = new AUTOMATED_UNDERWRITING();
            automatedUnderwriting.AutomatedUnderwritingSystemType = ToMismoEnum(AutomatedUnderwritingSystemBase.Other);
            automatedUnderwriting.AutomatedUnderwritingSystemTypeOtherDescription = ToMismoString("UCD");
            automatedUnderwriting.AutomatedUnderwritingCaseIdentifier = ToMismoIdentifier(ucdCaseFileId);
            automatedUnderwriting.SequenceNumber = sequenceNumber;

            return automatedUnderwriting;
        }

        /// <summary>
        /// Creates a container to hold all instances of automated underwriting processes.
        /// </summary>
        /// <returns>An AUTOMATED_UNDERWRITINGS container.</returns>
        private AUTOMATED_UNDERWRITINGS CreateAutomatedUnderwritings()
        {
            var automatedUnderwritings = new AUTOMATED_UNDERWRITINGS();
            automatedUnderwritings.AutomatedUnderwriting.Add(this.CreateAutomatedUnderwriting(automatedUnderwritings.AutomatedUnderwriting.Count(au => au != null) + 1));

            var latestUcdDelivery = this.loanData.sUcdDeliveryCollection.OrderByDescending(ucd => ucd.DateOfDelivery.DateTimeForComputationWithTime).FirstOrDefault();
            if (latestUcdDelivery != null)
            {
                automatedUnderwritings.AutomatedUnderwriting.Add(this.CreateAutomatedUnderwriting_UcdDelivery(latestUcdDelivery.CaseFileId, automatedUnderwritings.AutomatedUnderwriting.Count(au => au != null) + 1));
            }

            return automatedUnderwritings;
        }

        /// <summary>
        /// Creates a container holding a primary billing address for the loan.
        /// </summary>
        /// <returns>A BILLING_ADDRESS container.</returns>
        private BILLING_ADDRESS CreateBillingAddress()
        {
            var billingAddress = new BILLING_ADDRESS();
            billingAddress.Address = CreateAddress(
                this.primaryApp.aBAddrPost, 
                this.primaryApp.aBCityPost, 
                this.primaryApp.aBStatePost, 
                this.primaryApp.aBZipPost, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(this.primaryApp.aBStatePost),
                AddressBase.Mailing, 
                1);

            return billingAddress;
        }

        /// <summary>
        /// Creates a container representing a borrower.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A BORROWER container.</returns>
        private BORROWER CreateBorrower(CAppData appData)
        {
            var borrower = new BORROWER();
            borrower.BorrowerDetail = this.CreateBorrowerDetail(appData);
            borrower.Counseling = this.CreateCounseling(appData);
            borrower.CurrentIncome = this.CreateCurrentIncome(appData);
            borrower.CreditScores = CreateCreditScores(appData, this.loanData.sLoanVersionT);
            borrower.Dependents = this.CreateDependents(appData);
            borrower.Declaration = this.CreateDeclaration(appData);
            borrower.Employers = this.CreateEmployers(appData);
            borrower.GovernmentBorrower = this.CreateGovernmentBorrower(appData);
            borrower.GovernmentMonitoring = this.CreateGovernmentMonitoring(appData);
            borrower.Residences = this.CreateResidences(appData);
            borrower.HousingExpenses = this.CreateHousingExpenses_BorrowerContainer(appData);

            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                borrower.Summaries = this.CreateSummaries(appData);

                if (this.loanData.sLT == E_sLT.VA)
                {
                    borrower.MilitaryServices = this.CreateMilitaryServices(appData);
                }
            }

            if (appData.aMismoId == "B0" || appData.aMismoId == "C0")
            {
                borrower.NearestLivingRelative = this.CreateNearestLivingRelative(appData.aMismoId == "B0");
            }

            return borrower;
        }

        /// <summary>
        /// Creates a container holding data about a borrower.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A BORROWER_DETAIL container.</returns>
        private BORROWER_DETAIL CreateBorrowerDetail(CAppData appData)
        {
            var borrowerDetail = new BORROWER_DETAIL();
            borrowerDetail.BorrowerAgeAtApplicationYearsCount = ToMismoCount(appData.aAge_rep, null);
            borrowerDetail.BorrowerApplicationSignedDate = ToMismoDate(this.loanData.sAppSubmittedD_rep);
            borrowerDetail.BorrowerBirthDate = ToMismoDate(appData.aDob_rep);
            borrowerDetail.BorrowerClassificationType = ToBorrowerClassificationEnum(appData.BorrowerModeT);
            borrowerDetail.CreditReportIdentifier = ToMismoIdentifier(appData.aCreditReportId, isSensitiveData: true);
            borrowerDetail.BorrowerMailToAddressSameAsPropertyIndicator = ToMismoIndicator(appData.aAddrMailSourceT == E_aAddrMailSourceT.SubjectPropertyAddress ||
                                                                                                            (appData.aAddrMailSourceT == E_aAddrMailSourceT.PresentAddress && appData.aBorrowerAddressSameAsSubjectProperty));
            borrowerDetail.BorrowerRelationshipTitleType = ToBorrowerRelationshipTitleEnum(appData.aRelationshipTitleT);

            if (borrowerDetail.BorrowerRelationshipTitleType != null && borrowerDetail.BorrowerRelationshipTitleType.enumValue == BorrowerRelationshipTitleBase.Other)
            {
                // If the relationship title is set to "Other", use the description field, otherwise
                // use the mapped description of the relationship title setting.
                borrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = ToMismoString(
                    appData.aRelationshipTitleT == E_aRelationshipTitleT.Other ? 
                        appData.aRelationshipTitleOtherDesc : 
                        CAppBase.aRelationshipTitleT_Map_rep(appData.aRelationshipTitleT));
            }

            borrowerDetail.DependentCount = ToMismoCount(string.IsNullOrEmpty(appData.aDependNum_rep) ? "0" : appData.aDependNum_rep, null);
            borrowerDetail.JointAssetLiabilityReportingType = ToJointAssetLiabilityReportingEnum(appData.aAsstLiaCompletedNotJointly);
            borrowerDetail.MaritalStatusType = ToMaritalStatusEnum(appData.aMaritalStatT);
            borrowerDetail.SchoolingYearsCount = ToMismoCount(appData.aSchoolYrs_rep, null);
            borrowerDetail.Extension = this.CreateBorrowerDetailExtension(appData);
            return borrowerDetail;
        }

        /// <summary>
        /// Creates a container holding extended details about a borrower.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A BORROWER_DETAIL_EXTENSION container.</returns>
        private BORROWER_DETAIL_EXTENSION CreateBorrowerDetailExtension(CAppData appData)
        {
            var borrowerDetailExtension = new BORROWER_DETAIL_EXTENSION();
            borrowerDetailExtension.Other = new LQB_BORROWER_DETAIL_EXTENSION();
            borrowerDetailExtension.Other.FinancialInstitutionMemberIdentifier = ToMismoIdentifier(appData.BorrowerModeT == E_BorrowerModeT.Borrower ? appData.aBCoreSystemId : appData.aCCoreSystemId, isSensitiveData: true);
            return borrowerDetailExtension;
        }

        /// <summary>
        /// Creates a container with information on a buydown.
        /// Logic adapted from the DocMagicMismoExporter class; OPM 61315.
        /// </summary>
        /// <returns>A BUYDOWN container.</returns>
        private BUYDOWN CreateBuydown()
        {
            var buydown = new BUYDOWN();
            var buydownRows = new[]
            {
                //// R = Rate Reduction, Mon = Term (mths)
                new 
                { 
                    R = this.loanData.sBuydwnR1, 
                    R_rep = this.loanData.sBuydwnR1_rep, 
                    Mon = this.loanData.sBuydwnMon1, 
                    Mon_rep = this.loanData.sBuydwnMon1_rep, 
                    Num = "1"
                },
                
                new 
                { 
                    R = this.loanData.sBuydwnR2, 
                    R_rep = this.loanData.sBuydwnR2_rep, 
                    Mon = this.loanData.sBuydwnMon2, 
                    Mon_rep = this.loanData.sBuydwnMon2_rep, 
                    Num = "2"
                },

                new 
                { 
                    R = this.loanData.sBuydwnR3, 
                    R_rep = this.loanData.sBuydwnR3_rep, 
                    Mon = this.loanData.sBuydwnMon3, 
                    Mon_rep = this.loanData.sBuydwnMon3_rep, 
                    Num = "3"
                },

                new 
                { 
                    R = this.loanData.sBuydwnR4, 
                    R_rep = this.loanData.sBuydwnR4_rep, 
                    Mon = this.loanData.sBuydwnMon4, 
                    Mon_rep = this.loanData.sBuydwnMon4_rep, 
                    Num = "4"
                },

                new 
                { 
                    R = this.loanData.sBuydwnR5, 
                    R_rep = this.loanData.sBuydwnR5_rep, 
                    Mon = this.loanData.sBuydwnMon5, 
                    Mon_rep = this.loanData.sBuydwnMon5_rep, 
                    Num = "5"
                }
            };

            //// Get nonempty rows. A row is nonzero if both fields have a nonzero value.
            var buydownRowsFiltered = buydownRows.Where((row) => row.R != 0 && row.Mon != 0);

            if (buydownRowsFiltered.Count() == 0)
            {
                return null;
            }

            bool isIncreaseRatePercentConstant = true;
            bool isChangeFrequencyMonthsConstant = true;
            decimal increaseRatePercent = buydownRowsFiltered.First().R;
            int changeFrequencyMonths = buydownRowsFiltered.First().Mon;
            int durationMonths = 0;

            buydown.BuydownSchedules = new BUYDOWN_SCHEDULES();
            foreach (var row in buydownRowsFiltered)
            {
                buydown.BuydownSchedules.BuydownSchedule.Add(this.CreateBuydownSchedule(row.R_rep, row.Num, row.Mon_rep, buydown.BuydownSchedules.BuydownSchedule.Count(b => b != null) + 1));

                durationMonths += row.Mon;

                //// Detect increase rate percent: iff the value in sBuydwnR1 through sBuydwnR5 drops at the same rate
                if (row.R != increaseRatePercent)
                {
                    isIncreaseRatePercentConstant = false;
                }

                //// Detect change frequency months: iff the value in sBuydwnMon1 through sBuydwnMon5 remains constant
                if (row.Mon != changeFrequencyMonths)
                {
                    isChangeFrequencyMonthsConstant = false;
                }
            }

            buydown.BuydownRule = new BUYDOWN_RULE();
            buydown.BuydownRule.BuydownBaseDateType = ToBuydownBaseDateEnum(BuydownBaseDateBase.FirstPaymentDate);
            buydown.BuydownRule.BuydownDurationMonthsCount = ToMismoCount(durationMonths, this.loanData.m_convertLos);
            
            if (isChangeFrequencyMonthsConstant)
            {
                buydown.BuydownRule.BuydownChangeFrequencyMonthsCount = ToMismoCount(changeFrequencyMonths, this.loanData.m_convertLos);
            }

            if (isIncreaseRatePercentConstant)
            {
                buydown.BuydownRule.BuydownIncreaseRatePercent = ToMismoPercent(this.loanData.m_convertLos.ToRateString(increaseRatePercent));
            }
            
            return buydown;
        }

        /// <summary>
        /// Creates a container holding information on a period of the buydown.
        /// </summary>
        /// <param name="adjustmentPercent">The rate adjustment for this period.</param>
        /// <param name="periodIdentifier">A sequence identifier for all periods of the buydown.</param>
        /// <param name="periodicTerm">The months/number of payments in this period.</param>
        /// <param name="sequence">The sequence number of the schedule among the list of schedules.</param>
        /// <returns>A BUYDOWN_SCHEDULE container.</returns>
        private BUYDOWN_SCHEDULE CreateBuydownSchedule(string adjustmentPercent, string periodIdentifier, string periodicTerm, int sequence)
        {
            var buydownSchedule = new BUYDOWN_SCHEDULE();
            buydownSchedule.SequenceNumber = sequence;
            buydownSchedule.BuydownScheduleAdjustmentPercent = ToMismoPercent(adjustmentPercent);
            buydownSchedule.BuydownSchedulePeriodIdentifier = ToMismoIdentifier(periodIdentifier);
            buydownSchedule.BuydownSchedulePeriodicPaymentsCount = ToMismoCount(periodicTerm, this.loanData.m_convertLos);

            return buydownSchedule;
        }

        /// <summary>
        /// Creates a CASH_TO_CLOSE_ITEM container with a cash to close item from the disclosure given by the document type.
        /// </summary>
        /// <param name="docType">Distinguishes between the Loan Estimate and Closing Disclosure.</param>
        /// <param name="itemType">The type of cash to close item.</param>
        /// <param name="estimatedAmount">The estimated amount of cash.</param>
        /// <param name="finalAmount">If the document is a closing disclosure then this is the final cash amount. Otherwise null/empty string.</param>
        /// <param name="paymentType">The type of payment (from or to borrower) for this item when the amount is positive.</param>
        /// <param name="sequenceNumber">The item sequence among the list of items.</param>
        /// <returns>A CASH_TO_CLOSE_ITEM container with a cash to close item.</returns>
        private CASH_TO_CLOSE_ITEM CreateCashToCloseItem(DocumentBase docType, IntegratedDisclosureCashToCloseItemBase itemType, string estimatedAmount, string finalAmount, IntegratedDisclosureCashToCloseItemPaymentBase paymentType, int sequenceNumber)
        {
            var cashToCloseItem = new CASH_TO_CLOSE_ITEM();

            decimal? parsedEstimatedAmount = estimatedAmount.ToNullable<decimal>(decimal.TryParse);
            decimal? parsedFinalAmount = finalAmount.ToNullable<decimal>(decimal.TryParse);
            string estimatedAmountToSend = estimatedAmount;
            string finalAmountToSend = finalAmount;
            IntegratedDisclosureCashToCloseItemPaymentBase paymentTypeToSend = paymentType;

            if (docType == DocumentBase.ClosingDisclosure && parsedEstimatedAmount.HasValue && parsedFinalAmount.HasValue)
            {
                if (parsedFinalAmount.Value < 0)
                {
                    finalAmountToSend = this.loanData.m_convertLos.ToMoneyString(-1 * parsedFinalAmount.Value, FormatDirection.ToRep);
                    estimatedAmountToSend = this.loanData.m_convertLos.ToMoneyString(-1 * parsedEstimatedAmount.Value, FormatDirection.ToRep);
                    paymentTypeToSend = paymentType != IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower ? IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower : IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower;
                }

                cashToCloseItem.IntegratedDisclosureCashToCloseItemFinalAmount = ToMismoAmount(finalAmountToSend);
            }
            else if (docType == DocumentBase.LoanEstimate && parsedEstimatedAmount.HasValue)
            {
                if (parsedEstimatedAmount.Value < 0)
                {
                    estimatedAmountToSend = this.loanData.m_convertLos.ToMoneyString(-1 * parsedEstimatedAmount.Value, FormatDirection.ToRep);
                    paymentTypeToSend = paymentType != IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower ? IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower : IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower;
                }
            }
            else
            {
                return null;
            }

            cashToCloseItem.IntegratedDisclosureCashToCloseItemEstimatedAmount = ToMismoAmount(estimatedAmountToSend);
            cashToCloseItem.IntegratedDisclosureCashToCloseItemPaymentType = ToIntegratedDisclosureCashToCloseItemPaymentEnum(paymentTypeToSend);
            
            cashToCloseItem.IntegratedDisclosureCashToCloseItemType = ToIntegratedDisclosureCashToCloseItemEnum(itemType);
            cashToCloseItem.SequenceNumber = sequenceNumber;

            return cashToCloseItem;
        }

        /// <summary>
        /// Creates a CASH_TO_CLOSE_ITEMS container with a list of cash to close items from the archived disclosure given by the document type.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>A CASH_TO_CLOSE_ITEMS container with a list of cash to close items.</returns>
        private CASH_TO_CLOSE_ITEMS CreateCashToCloseItems(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var cashToCloseItems = new CASH_TO_CLOSE_ITEMS();

            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
            {
                cashToCloseItems.Extension = this.CreateCashToCloseItemsExtension(this.loanEstimateArchiveData.sTRIDLoanEstimateCashToCloseCalcMethodT);
                cashToCloseItems.CashToCloseItem.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.LoanEstimate,
                        IntegratedDisclosureCashToCloseItemBase.LoanCostsAndOtherCostsTotal,
                        estimatedAmount: this.loanEstimateArchiveData.sTRIDLoanEstimateTotalAllCosts_rep,
                        finalAmount: null,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: 1));

                cashToCloseItems.CashToCloseItem.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.LoanEstimate,
                        IntegratedDisclosureCashToCloseItemBase.LenderCredits,
                        estimatedAmount: this.loanEstimateArchiveData.sTRIDLoanEstimateLenderCredits_rep,
                        finalAmount: null,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                        sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                cashToCloseItems.CashToCloseItem.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.LoanEstimate,
                        IntegratedDisclosureCashToCloseItemBase.TotalClosingCosts,
                        estimatedAmount: this.loanEstimateArchiveData.sTRIDLoanEstimateTotalClosingCosts_rep,
                        finalAmount: null,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                if (this.loanEstimateArchiveData.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard)
                {
                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.ClosingCostsFinanced,
                            estimatedAmount: this.loanEstimateArchiveData.sTRIDLoanEstimateBorrowerFinancedClosingCosts_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.FundsFromBorrower,
                            estimatedAmount: this.loanEstimateArchiveData.sTRIDLoanEstimateDownPaymentOrFundsFromBorrower_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.Deposit,
                            estimatedAmount: this.loanEstimateArchiveData.sTRIDCashDeposit_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.FundsForBorrower,
                            estimatedAmount: this.loanEstimateArchiveData.sTRIDLoanEstimateFundsForBorrower_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.SellerCredits,
                            estimatedAmount: this.loanEstimateArchiveData.sTRIDLoanEstimateSellerCredits_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.AdjustmentsAndOtherCredits,
                            estimatedAmount: this.loanEstimateArchiveData.sTRIDAdjustmentsAndOtherCredits_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));
                }
                else if (this.loanEstimateArchiveData.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative)
                {
                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.LoanAmount,
                            estimatedAmount: this.loanEstimateArchiveData.sFinalLAmt_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.TotalPayoffsAndPayments,
                            estimatedAmount: this.loanEstimateArchiveData.sTRIDTotalPayoffsAndPayments_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));
                }

                cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.CashToCloseTotal,
                            estimatedAmount: this.loanEstimateArchiveData.sTRIDLoanEstimateCashToClose_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));
            }
            else
            {
                ////ClosingDisclosure
                cashToCloseItems.Extension = this.CreateCashToCloseItemsExtension(this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT);
                cashToCloseItems.CashToCloseItem.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.LoanCostsAndOtherCostsTotal,
                        estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateTotalClosingCostsExcludingLenderCredit_rep,
                        finalAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureTotalClosingCostsExcludingLenderCredit_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: 1));

                cashToCloseItems.CashToCloseItem.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.LenderCredits,
                        estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateLenderCredit_rep,
                        finalAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureGeneralLenderCredits_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                        sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                cashToCloseItems.CashToCloseItem.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.TotalClosingCosts,
                        estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateTotalClosingCosts_rep,
                        finalAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureTotalClosingCosts_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                //// 5/11/2015 BB - Estimated amount is hard-coded to $0.00 on the closing disclosure page. It would be better to migrate this value to the data layer at some point.
                cashToCloseItems.CashToCloseItem.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.ClosingCostsPaidBeforeClosing,
                        estimatedAmount: "0.00",
                        finalAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureClosingCostsPaidBeforeClosing_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                if (this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard)
                {
                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.ClosingCostsFinanced,
                            estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateBorrowerFinancedClosingCosts_rep,
                            finalAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureBorrowerFinancedClosingCosts_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.FundsFromBorrower,
                            estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateDownPaymentOrFundsFromBorrower_rep,
                            finalAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureDownPaymentOrFundsFromBorrower_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.Deposit,
                            estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashDeposit_rep,
                            finalAmount: this.closingDisclosureArchiveData.sTRIDCashDeposit_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.FundsForBorrower,
                            estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateFundsForBorrower_rep,
                            finalAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureFundsForBorrower_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.SellerCredits,
                            estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateSellerCredits_rep,
                            finalAmount: this.closingDisclosureArchiveData.sTRIDSellerCredits_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.AdjustmentsAndOtherCredits,
                            estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateAdjustmentsAndOtherCredits_rep,
                            finalAmount: this.closingDisclosureArchiveData.sTRIDAdjustmentsAndOtherCredits_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));
                }
                else if (this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative)
                {
                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.LoanAmount,
                            estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateFinalLAmt_rep,
                            finalAmount: this.closingDisclosureArchiveData.sFinalLAmt_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));

                    cashToCloseItems.CashToCloseItem.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.TotalPayoffsAndPayments,
                            estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateTotalPayoffsAndPayments_rep,
                            finalAmount: this.closingDisclosureArchiveData.sTRIDTotalPayoffsAndPayments_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));
                }

                cashToCloseItems.CashToCloseItem.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.CashToCloseTotal,
                        estimatedAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToClose_rep,
                        finalAmount: this.closingDisclosureArchiveData.sTRIDClosingDisclosureCashToClose_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: cashToCloseItems.CashToCloseItem.Count(item => item != null) + 1));
            }

            return cashToCloseItems;
        }

        /// <summary>
        /// Creates an extension to the CASH_TO_CLOSE_ITEMS container with additional LQB and/or MISMO information. 
        /// </summary>
        /// <param name="calculationMethod">The LQB calculation method for cash to close items. Appears in the "OTHER" (LendingQB-proprietary) subcontainer.</param>
        /// <returns>A serializable <see cref="CASH_TO_CLOSE_ITEMS_EXTENSION"/> container.</returns>
        private CASH_TO_CLOSE_ITEMS_EXTENSION CreateCashToCloseItemsExtension(E_CashToCloseCalcMethodT calculationMethod)
        {
            // Build CASH_TO_CLOSE_ITEMS_EXTENSION/OTHER/CASH_TO_CLOSE_DETAIL/CalculationMethodType from inside out
            var cashToCloseDetail = new LQB_CASH_TO_CLOSE_DETAIL()
            {
                CalculationMethodType = ToCalculationMethodType(calculationMethod)
            };

            var other = new LQB_CASH_TO_CLOSE_ITEMS_EXTENSION()
            {
                LqbCashToCloseDetail = cashToCloseDetail
            };

            return new CASH_TO_CLOSE_ITEMS_EXTENSION()
            {
                Other = other
            };
        }

        /// <summary>
        /// Creates a proprietary LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE container from a change of circumstance archive.
        /// </summary>
        /// <param name="archive">A closing cost change of circumstance archive.</param>
        /// <param name="sequence">The sequence number of the change of circumstance among the list of changes.</param>
        /// <returns>A proprietary LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE container.</returns>
        private LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE CreateChangeOfCircumstanceOccurrence(ClosingCostCoCArchive archive, int sequence)
        {
            var coc = new LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE();
            coc.SequenceNumber = sequence;

            coc.AssociatedFeeChanges = this.CreateCOCAssociatedFeeChanges(archive);
            coc.AssociatedLoanValueChanges = this.CreateCOCAssociatedLoanValueChanges(archive);
            coc.ChangeOfCircumstanceOccurrenceDetail = this.CreateChangeOfCircumstanceOccurrenceDetail(archive);

            return coc;
        }

        /// <summary>
        /// Creates a proprietary LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE_DETAIL container from a change of circumstance archive.
        /// </summary>
        /// <param name="archive">A closing cost change of circumstance archive.</param>
        /// <returns>A proprietary LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE_DETAIL container.</returns>
        private LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE_DETAIL CreateChangeOfCircumstanceOccurrenceDetail(ClosingCostCoCArchive archive)
        {
            var detail = new LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCE_DETAIL();
            detail.ChangeExplanationText = ToMismoString(archive.CircumstanceChangeExplanation);
            detail.CircumstanceChangeDate = ToMismoDate(this.loanData.m_convertLos.ToDateTimeVarCharString(archive.CircumstanceChangeD));

            //// This belongs in the data-layer. The COC object should have rep fields that handle format conversion for each date, etc.
            DateTime archiveDate = DateTime.MinValue;
            DateTime.TryParse(archive.DateArchived, out archiveDate);

            if (archiveDate != DateTime.MinValue)
            {
                detail.COCArchivedDatetime = ToMismoDatetime(this.loanData.m_convertLos.ToDateTimeString(archiveDate, true), ignoreTimeSegmentIndicator: false);
            }

            if (archive.ComplianceCOCCategory != ComplianceEagle.CEagleChangedCircumstanceCategory.Blank)
            {
                detail.ComplianceCOCCategory = ToMismoString(GetXmlEnumName(archive.ComplianceCOCCategory));
            }

            detail.RedisclosureDate = ToMismoDate(this.loanData.m_convertLos.ToDateTimeVarCharString(archive.RedisclosureD));

            return detail;
        }

        /// <summary>
        /// Creates a proprietary LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCES container with one occurrence per change of circumstance archive.
        /// </summary>
        /// <returns>A proprietary LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCES container.</returns>
        private LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCES CreateChangeOfCircumstanceOccurrences()
        {
            var cocs = new LQB_CHANGE_OF_CIRCUMSTANCE_OCCURRENCES();

            foreach (var archive in this.loanData.sClosingCostCoCArchive)
            {
                cocs.ChangeOfCircumstanceOccurrence.Add(this.CreateChangeOfCircumstanceOccurrence(archive, cocs.ChangeOfCircumstanceOccurrence.Count(c => c != null) + 1));
            }

            return cocs;
        }

        /// <summary>
        /// Creates a proprietary LQB_ASSOCIATED_FEE_CHANGES container with the fees associated with the given change of circumstance archive.
        /// </summary>
        /// <param name="archive">A change of circumstance archive.</param>
        /// <returns>A proprietary LQB_ASSOCIATED_FEE_CHANGES container.</returns>
        private LQB_ASSOCIATED_FEE_CHANGES CreateCOCAssociatedFeeChanges(ClosingCostCoCArchive archive)
        {
            var changes = new LQB_ASSOCIATED_FEE_CHANGES();

            foreach (var fee in archive.Fees)
            {
                changes.AssociatedFeeChange.Add(this.CreateCOCAssociatedFeeChange(fee.Description, fee.FeeId, fee.Value, changes.AssociatedFeeChange.Count(c => c != null) + 1));
            }

            return changes;
        }

        /// <summary>
        /// Creates a proprietary LQB_ASSOCIATED_FEE_CHANGE container with a fee associated with a change of circumstance archive.
        /// </summary>
        /// <param name="description">The fee description.</param>
        /// <param name="feeID">The fee identifier.</param>
        /// <param name="amount">The fee dollar amount.</param>
        /// <param name="sequence">The sequence number of the fee change among the list of changes.</param>
        /// <returns>A proprietary LQB_ASSOCIATED_FEE_CHANGE container.</returns>
        private LQB_ASSOCIATED_FEE_CHANGE CreateCOCAssociatedFeeChange(string description, Guid feeID, decimal amount, int sequence)
        {
            var change = new LQB_ASSOCIATED_FEE_CHANGE();
            change.FeeAmount = ToMismoAmount(this.loanData.m_convertLos.ToMoneyString(amount, FormatDirection.ToRep));
            change.FeeDescription = ToMismoString(description);
            change.FeeIdentifier = ToMismoIdentifier(feeID.ToString("D"));
            change.SequenceNumber = sequence;

            return change;
        }

        /// <summary>
        /// Creates a proprietary LQB_ASSOCIATED_LOAN_VALUE_CHANGES container with the fields associated with the given change of circumstance archive.
        /// </summary>
        /// <param name="archive">A change of circumstance archive.</param>
        /// <returns>A proprietary LQB_ASSOCIATED_LOAN_VALUE_CHANGES container.</returns>
        private LQB_ASSOCIATED_LOAN_VALUE_CHANGES CreateCOCAssociatedLoanValueChanges(ClosingCostCoCArchive archive)
        {
            var changes = new LQB_ASSOCIATED_LOAN_VALUE_CHANGES();

            foreach (var field in archive.LoanValues)
            {
                changes.AssociatedLoanValueChange.Add(this.CreateCOCAssociatedLoanValueChange(field.Description, field.FieldId, field.Value, changes.AssociatedLoanValueChange.Count(c => c != null) + 1));
            }

            return changes;
        }

        /// <summary>
        /// Creates a proprietary LQB_ASSOCIATED_LOAN_VALUE_CHANGE container with a field associated with a change of circumstance archive.
        /// </summary>
        /// <param name="description">The field description.</param>
        /// <param name="fieldID">The field identifier.</param>
        /// <param name="value">The field decimal value.</param>
        /// <param name="sequence">The sequence number of the field change among the list of changes.</param>
        /// <returns>A proprietary LQB_ASSOCIATED_LOAN_VALUE_CHANGE container.</returns>
        private LQB_ASSOCIATED_LOAN_VALUE_CHANGE CreateCOCAssociatedLoanValueChange(string description, string fieldID, decimal value, int sequence)
        {
            var change = new LQB_ASSOCIATED_LOAN_VALUE_CHANGE();
            change.FieldDescription = ToMismoString(description);
            change.FieldName = ToMismoString(fieldID);
            change.SequenceNumber = sequence;
            change.Value = ToMismoNumeric(this.loanData.m_convertLos.ToDecimalString(value, FormatDirection.ToRep));

            return change;
        }

        /// <summary>
        /// Creates a CENSUS_INFORMATION object with the census tract of the subject property.
        /// </summary>
        /// <returns>A CENSUS_INFORMATION object with the census tract of the subject property.</returns>
        private CENSUS_INFORMATION CreateCensusInformation()
        {
            var info = new CENSUS_INFORMATION();
            info.CensusTractIdentifier = ToMismoIdentifier(this.loanData.sHmdaCensusTract);

            return info;
        }

        /// <summary>
        /// Creates a CLOSING_ADJUSTMENT_ITEM container with description, value, integrated disclosure type and line number specified.
        /// </summary>
        /// <param name="section">The loan estimate or closing disclosure section that includes the adjustment.</param>
        /// <param name="subsection">The loan estimate or closing disclosure subsection that the adjustment belongs to, if any. Pass blank if not applicable.</param>
        /// <param name="adjItemType">The adjustment type.</param>
        /// <param name="adjItemTypeOtherDescription">A verbal description of the adjustment.</param>
        /// <param name="amount">The adjustment amount. Requires a <code>rep or LOSConverted</code> value.</param>
        /// <param name="paidBy">The type of entity that pays/paid the adjustment amount.</param>
        /// <param name="paidOutsideOfClosing">Indicates whether the adjustment is paid outside of closing.</param>
        /// <param name="sequence">The sequence number of the adjustment among the list of adjustments.</param>
        /// <param name="excludeFromLeCd">Indicates whether the adjustment item should be excluded from the LE/CD.</param>
        /// <param name="lqbAdjustmentType">The adjustment type in LendingQB.</param>
        /// <param name="populateExcludeFromLeCdOverride">Whether the ExcludeFromLECDForThisLien field will be populated. Will default to <see cref="ShouldSendTrid2017SubordinateFinancingData"/> if not provided.</param>
        /// <returns>Returns a CLOSING_ADJUSTMENT_ITEM container with description, value, integrated disclosure type and line number specified.</returns>
        private CLOSING_ADJUSTMENT_ITEM CreateClosingAdjustmentItem(
                                            IntegratedDisclosureSectionBase section,
                                            IntegratedDisclosureSubsectionBase subsection,
                                            ClosingAdjustmentItemBase adjItemType,
                                            string adjItemTypeOtherDescription,
                                            string amount,
                                            E_PartyT paidBy,
                                            bool paidOutsideOfClosing,
                                            int sequence,
                                            bool? excludeFromLeCd = null,
                                            E_AdjustmentT lqbAdjustmentType = E_AdjustmentT.Blank,
                                            bool? populateExcludeFromLeCdOverride = null)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var closingAdjustmentItem = new CLOSING_ADJUSTMENT_ITEM();
            closingAdjustmentItem.ClosingAdjustmentItemDetail = this.CreateClosingAdjustmentItemDetail(
                section,
                subsection,
                adjItemType,
                adjItemTypeOtherDescription,
                amount,
                paidBy,
                paidOutsideOfClosing,
                excludeFromLeCd,
                lqbAdjustmentType,
                populateExcludeFromLeCdOverride);
            closingAdjustmentItem.SequenceNumber = sequence;

            return closingAdjustmentItem;
        }

        /// <summary>
        /// Creates a CLOSING_ADJUSTMENT_ITEM_DETAIL with information pertaining to a closing adjustment.
        /// </summary>
        /// <param name="section">The loan estimate or closing disclosure section that includes the adjustment.</param>
        /// <param name="subsection">The loan estimate or closing disclosure subsection that the adjustment belongs to, if any. Pass blank if not applicable.</param>
        /// <param name="adjItemType">The adjustment type.</param>
        /// <param name="adjItemTypeOtherDescription">A verbal description of the adjustment.</param>
        /// <param name="amount">The adjustment amount. Requires a <code>rep or LOSConverted</code> value.</param>
        /// <param name="paidBy">The type of entity that pays/paid the adjustment amount.</param>
        /// <param name="paidOutsideOfClosing">Indicates whether the closing adjustment is paid outside of closing.</param>
        /// <param name="excludeFromLeCd">Indicates whether the adjustment item should be excluded from the LE/CD.</param>
        /// <param name="lqbAdjustmentType">The adjustment type in LendingQB.</param>
        /// <param name="populateExcludeFromLeCdOverride">Whether the ExcludeFromLECDForThisLien field will be populated. Will default to <see cref="ShouldSendTrid2017SubordinateFinancingData"/> if not provided.</param>
        /// <returns>A CLOSING_ADJUSTMENT_ITEM_DETAIL with information pertaining to a closing adjustment.</returns>
        private CLOSING_ADJUSTMENT_ITEM_DETAIL CreateClosingAdjustmentItemDetail(
                                                    IntegratedDisclosureSectionBase section,
                                                    IntegratedDisclosureSubsectionBase subsection,
                                                    ClosingAdjustmentItemBase adjItemType,
                                                    string adjItemTypeOtherDescription,
                                                    string amount,
                                                    E_PartyT paidBy,
                                                    bool paidOutsideOfClosing,
                                                    bool? excludeFromLeCd,
                                                    E_AdjustmentT lqbAdjustmentType,
                                                    bool? populateExcludeFromLeCdOverride)
        {
            var detail = new CLOSING_ADJUSTMENT_ITEM_DETAIL();
            detail.ClosingAdjustmentItemAmount = ToMismoAmount(amount);
            detail.ClosingAdjustmentItemPaidByType = ToClosingAdjustmentItemPaidByEnum(paidBy);
            detail.ClosingAdjustmentItemType = ToClosingAdjustmentItemEnum(adjItemType, adjItemTypeOtherDescription);
            detail.ClosingAdjustmentItemPaidOutsideOfClosingIndicator = ToMismoIndicator(paidOutsideOfClosing);

            if (detail.ClosingAdjustmentItemType != null && detail.ClosingAdjustmentItemType.enumValue == ClosingAdjustmentItemBase.Other)
            {
                detail.ClosingAdjustmentItemTypeOtherDescription = ToMismoString(adjItemTypeOtherDescription);
            }

            detail.IntegratedDisclosureSectionType = ToIntegratedDisclosureSectionEnum(section);
            detail.IntegratedDisclosureSubsectionType = ToIntegratedDisclosureSubsectionEnum(subsection);

            detail.Extension = this.ToClosingAdjustmentItemDetailExtension(lqbAdjustmentType, excludeFromLeCd, populateExcludeFromLeCdOverride);

            return detail;
        }

        /// <summary>
        /// Creates a container extending the closing adjustment item details.
        /// </summary>
        /// <param name="adjustmentType">The type of adjustment.</param>
        /// <param name="excludeFromLeCd">Indicates whether the adjustment item should be excluded from the LE/CD.</param>
        /// <param name="populateExcludeFromLeCdOverride">Whether the ExcludeFromLECDForThisLien field will be populated. Will default to <see cref="ShouldSendTrid2017SubordinateFinancingData"/> if not provided.</param>
        /// <returns>A serializable container.</returns>
        private CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION ToClosingAdjustmentItemDetailExtension(E_AdjustmentT adjustmentType, bool? excludeFromLeCd, bool? populateExcludeFromLeCdOverride)
        {
            var extension = new CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION();
            extension.Other = this.ToLqbClosingAdjustmentItemDetailExtension(adjustmentType, excludeFromLeCd, populateExcludeFromLeCdOverride);

            return extension;
        }

        /// <summary>
        /// Creates a container with LQB-specific data for the closing adjustment item details.
        /// </summary>
        /// <param name="adjustmentType">The type of adjustment.</param>
        /// <param name="excludeFromLeCd">Indicates whether the adjustment item should be excluded from the LE/CD.</param>
        /// <param name="populateExcludeFromLeCdOverride">Whether the ExcludeFromLECDForThisLien field will be populated. Will default to <see cref="ShouldSendTrid2017SubordinateFinancingData"/> if not provided.</param>
        /// <returns>A serializeable container.</returns>
        private LQB_CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION ToLqbClosingAdjustmentItemDetailExtension(E_AdjustmentT adjustmentType, bool? excludeFromLeCd, bool? populateExcludeFromLeCdOverride)
        {
            var extension = new LQB_CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION();

            if (adjustmentType == E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)
            {
                extension.AdjustmentIsPrincipalReduction = ToMismoIndicator(true);
                extension.PrincipalReductionForToleranceCure = ToMismoIndicator(false);
            }
            else if (adjustmentType == E_AdjustmentT.PrincipalReductionToCureToleranceViolation)
            {
                extension.AdjustmentIsPrincipalReduction = ToMismoIndicator(true);
                extension.PrincipalReductionForToleranceCure = ToMismoIndicator(true);
            }

            if (excludeFromLeCd.HasValue)
            {
                bool shouldInclude = populateExcludeFromLeCdOverride ?? ShouldSendTrid2017SubordinateFinancingData(this.loanData.sLienPosT, this.loanData.sIsOFinNew, this.loanData.sConcurSubFin, this.loanData.sTridTargetRegulationVersionT);
                if (shouldInclude)
                {
                    extension.ExcludeFromLECDForThisLien = ToMismoIndicator(excludeFromLeCd.Value);
                }
            }

            return extension;
        }

        /// <summary>
        /// Creates CLOSING_ADJUSTMENT_ITEMS container with CLOSING_ADJUSTMENT_ITEM sub-container populated with loan file information.
        /// </summary>
        /// <returns>Returns a CLOSING_ADJUSTMENT_ITEMS container with CLOSING_ADJUSTMENT_ITEM sub-container populated with loan file information.</returns>
        private CLOSING_ADJUSTMENT_ITEMS CreateClosingAdjustmentItems()
        {
            var adjItems = new CLOSING_ADJUSTMENT_ITEMS();

            //// Section K. Due from Borrower at Closing
            adjItems.ClosingAdjustmentItem.Add(
                this.CreateClosingAdjustmentItem(
                GetDisclosureSectionForSectionKAdjustments(this.isClosingPackage, this.loanData.sTRIDLoanEstimateCashToCloseCalcMethodT, this.loanData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.Other,
                "Sale Price of Any Personal Property Included in Sale",
                this.loanData.sGrossDueFromBorrPersonalProperty_rep,
                E_PartyT.Borrower,
                paidOutsideOfClosing: false,
                sequence: 1));

            adjItems.ClosingAdjustmentItem.Add(
                this.CreateClosingAdjustmentItem(
                GetDisclosureSectionForSectionKAdjustments(this.isClosingPackage, this.loanData.sTRIDLoanEstimateCashToCloseCalcMethodT, this.loanData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                IntegratedDisclosureSubsectionBase.Adjustments,
                ClosingAdjustmentItemBase.Repairs,
                this.loanData.sTRIDClosingDisclosureAltCostDescription,
                this.loanData.sTRIDClosingDisclosureAltCost_rep,
                E_PartyT.Borrower,
                paidOutsideOfClosing: false,
                sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1));

            var adjList = this.loanData.sAdjustmentList.GetEnumeratorWithoutSellerCredit() ?? new List<Adjustment>();

            foreach (Adjustment adj in adjList.Where(a => a.IsPaidFromBorrower && a.Amount > 0.00m))
            {
                adjItems.ClosingAdjustmentItem.Add(
                    this.CreateClosingAdjustmentItem(
                    GetDisclosureSectionForSectionKAdjustments(this.isClosingPackage, this.loanData.sTRIDLoanEstimateCashToCloseCalcMethodT, this.loanData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                    IntegratedDisclosureSubsectionBase.Adjustments,
                    ClosingAdjustmentItemBase.Other,
                    adj.Description,
                    adj.Amount_rep,
                    adj.PaidFromParty,
                    adj.POC,
                    lqbAdjustmentType: adj.AdjustmentType,
                    excludeFromLeCd: !adj.IncludeAdjustmentInLeCdForThisLien,
                    sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1));
            }

            //// K - AdjustmentsForItemsPaidBySellerInAdvance -> see the prorations

            //// Section M. Due to Seller at Closing
            adjItems.ClosingAdjustmentItem.Add(
                this.CreateClosingAdjustmentItem(
                IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.Other,
                "Sale Price of Any Personal Property Included in Sale",
                this.loanData.sGrossDueFromBorrPersonalProperty_rep,
                E_PartyT.Borrower,
                paidOutsideOfClosing: false,
                sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1));
            
            foreach (Adjustment adj in adjList.Where(a => a.IsPaidToSeller && a.Amount > 0.00m))
            {
                adjItems.ClosingAdjustmentItem.Add(
                    this.CreateClosingAdjustmentItem(
                    IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                    IntegratedDisclosureSubsectionBase.Blank,
                    ClosingAdjustmentItemBase.Other,
                    adj.Description,
                    adj.Amount_rep,
                    adj.PaidFromParty,
                    adj.POC,
                    excludeFromLeCd: !adj.IncludeAdjustmentInLeCdForThisLien,
                    lqbAdjustmentType: adj.AdjustmentType,
                    sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1));
            }

            //// M - AdjustmentsForItemsPaidBySellerInAdvance -> see the prorations

            //// Section L. Paid Already by or on Behalf of the Borrower at Closing
            //// Deposit -> see the closing cost funds

            bool populateExcludeFromLeCdForProceedsOfSubordinateLiensOverride = this.loanData.sLienPosT == E_sLienPosT.Second && 
                                                                                this.loanData.sIsOFinNew && 
                                                                                this.loanData.sRemain1stMBal > 0 && 
                                                                                this.loanData.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017;
            adjItems.ClosingAdjustmentItem.Add(
                this.CreateClosingAdjustmentItem(
                IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.ProceedsOfSubordinateLiens,
                "Other New Financing",
                this.loanData.sONewFinNetProceeds_rep,
                E_PartyT.Borrower,
                paidOutsideOfClosing: false,
                sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1,
                excludeFromLeCd: true,
                populateExcludeFromLeCdOverride: populateExcludeFromLeCdForProceedsOfSubordinateLiensOverride));

            Adjustment sellerCredit = this.loanData.sAdjustmentList.SellerCredit ?? new Adjustment();

            adjItems.ClosingAdjustmentItem.Add(
                this.CreateClosingAdjustmentItem(
                IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.SellerCredit,
                "Seller Credit",
                sellerCredit.Amount_rep,
                sellerCredit.PaidFromParty,
                sellerCredit.POC,
                excludeFromLeCd: !sellerCredit.IncludeAdjustmentInLeCdForThisLien,
                sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1));

            bool enablePredefinedAdjustmentMapping = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.loanData.GetLiveLoanVersion(), LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments);

            ////Other Credits & Adjustments
            foreach (Adjustment adj in adjList.Where(a => a.IsPaidToBorrower && a.Amount > 0.00m))
            {
                var subsection = GetIntegratedDisclosureSubsectionBaseValue_AdjustmentPaidToBorrower(adj.AdjustmentType);
                var adjItemType = ClosingAdjustmentItemBase.Other;

                if (enablePredefinedAdjustmentMapping)
                {
                    adjItemType = GetClosingAdjustmentItemBaseValue(adj.AdjustmentType);
                }

                adjItems.ClosingAdjustmentItem.Add(
                    this.CreateClosingAdjustmentItem(
                    IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                    subsection,
                    adjItemType,
                    adj.Description,
                    adj.Amount_rep,
                    adj.PaidFromParty,
                    adj.POC,
                    excludeFromLeCd: !adj.IncludeAdjustmentInLeCdForThisLien,
                    sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1,
                    lqbAdjustmentType: adj.AdjustmentType));
            }

            //// L - AdjustmentsForItemsUnpaidBySellerInAdvance -> see the prorations

            //// Section N. Due from Seller at Closing
            //// Excess Deposit -> see the closing cost funds

            adjItems.ClosingAdjustmentItem.Add(
                this.CreateClosingAdjustmentItem(
                IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.SellerCredit,
                "Seller Credit",
                sellerCredit.Amount_rep,
                sellerCredit.PaidFromParty,
                sellerCredit.POC,
                excludeFromLeCd: !sellerCredit.IncludeAdjustmentInLeCdForThisLien,
                sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1));

            foreach (Adjustment adj in adjList.Where(a => a.IsPaidFromSeller && a.Amount > 0.00m))
            {
                adjItems.ClosingAdjustmentItem.Add(
                    this.CreateClosingAdjustmentItem(
                    IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                    IntegratedDisclosureSubsectionBase.Blank,
                    ClosingAdjustmentItemBase.Other,
                    adj.Description,
                    adj.Amount_rep,
                    adj.PaidFromParty,
                    adj.POC,
                    excludeFromLeCd: !adj.IncludeAdjustmentInLeCdForThisLien,
                    sequence: adjItems.ClosingAdjustmentItem.Count(a => a != null) + 1,
                    lqbAdjustmentType: adj.AdjustmentType));
            }

            //// N - AdjustmentsForItemsUnpaidBySellerInAdvance -> see the prorations

            return adjItems.ClosingAdjustmentItem.Count(i => i != null) > 0 ? adjItems : null;
        }

        /// <summary>
        /// Creates CLOSING_AGENT container based on specified type and/or other description.
        /// </summary>
        /// <param name="closingAgentType">The closing agent type.</param>
        /// <param name="closingAgentTypeOtherDescription">The closing agent other description, used for other type.</param>
        /// <returns>Returns CLOSING_AGENT container with specified type and/or other description.</returns>
        private CLOSING_AGENT CreateClosingAgent(ClosingAgentBase closingAgentType, string closingAgentTypeOtherDescription)
        {
            var closingAgent = new CLOSING_AGENT();
            closingAgent.ClosingAgentType = ToClosingAgentEnum(closingAgentType);

            if (closingAgentType == ClosingAgentBase.Other)
            {
                closingAgent.ClosingAgentTypeOtherDescription = ToMismoString(closingAgentTypeOtherDescription);
            }

            return closingAgent;
        }

        /// <summary>
        /// Creates a CLOSING_COST_FUND_EXTENSION container.
        /// </summary>
        /// <param name="excludeFromLeCd">The value to populate the ExcludeFromLECDForThisLien property with. Null if no need to populate.</param>
        /// <returns>The container.</returns>
        private CLOSING_COST_FUND_EXTENSION CreateClosingCostFundExtension(bool? excludeFromLeCd)
        {
            CLOSING_COST_FUND_EXTENSION extension = new CLOSING_COST_FUND_EXTENSION();
            extension.Other = this.CreateLqbClosingCostFundExtension(excludeFromLeCd);

            return extension;
        }

        /// <summary>
        /// Creates the LQB_CLOSING_COST_FUND_EXTENSION container.
        /// </summary>
        /// <param name="excludeFromLeCd">The value to populate the ExcludeFromLECDForThisLien property with. Null if no need to populate.</param>
        /// <returns>The container.</returns>
        private LQB_CLOSING_COST_FUND_EXTENSION CreateLqbClosingCostFundExtension(bool? excludeFromLeCd)
        {
            LQB_CLOSING_COST_FUND_EXTENSION extension = new LQB_CLOSING_COST_FUND_EXTENSION();

            if (excludeFromLeCd.HasValue && ShouldSendTrid2017SubordinateFinancingData(this.loanData.sLienPosT, this.loanData.sIsOFinNew, this.loanData.sConcurSubFin, this.loanData.sTridTargetRegulationVersionT))
            {
                extension.ExcludeFromLECDForThisLien = ToMismoIndicator(excludeFromLeCd.Value);
            }

            return extension;
        }

        /// <summary>
        /// Creates a CLOSING_COST_FUND element with specified integrated disclosure section type, funds type, and dollar amount.
        /// </summary>
        /// <param name="section">The section of the loan estimate or closing disclosure on which the fund amount is displayed.</param>
        /// <param name="fundsType">Type of funds used.</param>
        /// <param name="amount">The dollar amount. Requires a <code>rep or LosConverted</code> value.</param>
        /// <param name="sequence">The sequence number of the fund among the list of funds.</param>
        /// <param name="excludeFromLeCd">The value to populate the ExcludeFromLECDForThisLien property with. Null if no need to populate.</param>
        /// <returns>Returns a CLOSING_COST_FUND element with specified integrated disclosure section type, funds type, and dollar amount.</returns>
        private CLOSING_COST_FUND CreateClosingCostFund(IntegratedDisclosureSectionBase section, FundsBase fundsType, string amount, int sequence, bool? excludeFromLeCd = null)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var closingCostFund = new CLOSING_COST_FUND();
            closingCostFund.ClosingCostFundAmount = ToMismoAmount(amount);
            closingCostFund.FundsType = ToFundsEnum(fundsType);
            closingCostFund.IntegratedDisclosureSectionType = ToIntegratedDisclosureSectionEnum(section);
            closingCostFund.SequenceNumber = sequence;
            closingCostFund.Extension = this.CreateClosingCostFundExtension(excludeFromLeCd);
            
            return closingCostFund;
        }

        /// <summary>
        /// Creates Closing Costs for loan.
        /// </summary>
        /// <returns>Returns a CLOSING_COSTS container populated according to the loan file.</returns>
        private CLOSING_COST_FUNDS CreateClosingCostFunds()
        {
            var closingCostFunds = new CLOSING_COST_FUNDS();

            closingCostFunds.ClosingCostFund.Add(
                this.CreateClosingCostFund(
                    IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                    FundsBase.DepositOnSalesContract,
                    this.loanData.sTotCashDeposit_rep,
                    sequence: 1,
                    excludeFromLeCd: this.loanData.sLienToIncludeCashDepositInTridDisclosures == ResponsibleLien.OtherLienTransaction));

            closingCostFunds.ClosingCostFund.Add(
                this.CreateClosingCostFund(
                    IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                    FundsBase.ExcessDeposit,
                    this.loanData.sReductionsDueToSellerExcessDeposit_rep,
                    closingCostFunds.ClosingCostFund.Count(f => f != null) + 1));

            return closingCostFunds;
        }

        /// <summary>
        /// Creates a container holding information about closing.
        /// </summary>
        /// <returns>A CLOSING_INFORMATION container.</returns>
        private CLOSING_INFORMATION CreateClosingInformation()
        {
            var closingInformation = new CLOSING_INFORMATION();
            closingInformation.ClosingAdjustmentItems = this.CreateClosingAdjustmentItems();
            closingInformation.ClosingCostFunds = this.CreateClosingCostFunds();
            closingInformation.ClosingInformationDetail = this.CreateClosingInformationDetail();
            closingInformation.ClosingInstruction = this.CreateClosingInstruction();
            //// closingInformation.Compensations = this.CreateCompensations();
            closingInformation.PrepaidItems = this.CreatePrepaidItems();
            closingInformation.ProrationItems = this.CreateProrationItems();

            return closingInformation;
        }

        /// <summary>
        /// Creates a container holding details about closing.
        /// </summary>
        /// <returns>A CLOSING_INFORMATION_DETAIL container populated with data.</returns>
        private CLOSING_INFORMATION_DETAIL CreateClosingInformationDetail()
        {
            var closingInformationDetail = new CLOSING_INFORMATION_DETAIL();
            
            closingInformationDetail.ClosingAgentOrderNumberIdentifier = ToMismoIdentifier(this.loanData.sSettlementAgentFileNum);
            closingInformationDetail.ClosingDate = ToMismoDate(this.loanData.sDocMagicClosingD_rep);
            closingInformationDetail.CurrentRateSetDate = ToMismoDate(this.loanData.sRLckdD_rep);
            closingInformationDetail.DisbursementDate = ToMismoDate(this.loanData.sDocMagicDisbursementD_rep);
            closingInformationDetail.DocumentPreparationDate = ToMismoDate(this.loanData.sDocMagicDocumentD_rep);
            closingInformationDetail.EstimatedPrepaidDaysCount = ToMismoCount(this.loanData.sIPiaDy_rep, null);
            closingInformationDetail.EstimatedPrepaidDaysPaidByType = ToEstimatedPrepaidDaysPaidByEnum(this.loanData.sIPiaProps);
            
            // LQB only sets EstimatedPrepaidDaysPaidByType to Other when the prepayment was made by the Broker.
            if (closingInformationDetail.EstimatedPrepaidDaysPaidByType != null && closingInformationDetail.EstimatedPrepaidDaysPaidByType.enumValue == EstimatedPrepaidDaysPaidByBase.Other)
            {
                closingInformationDetail.EstimatedPrepaidDaysPaidByTypeOtherDescription = ToMismoString("Broker");
            }
            
            closingInformationDetail.LoanEstimatedClosingDate = ToMismoDate(this.loanData.sDocMagicClosingD_rep);
            closingInformationDetail.LoanScheduledClosingDate = ToMismoDate(this.loanData.sDocMagicClosingD_rep);
            closingInformationDetail.RescissionDate = ToMismoDate(this.loanData.sDocMagicCancelD_rep);
            closingInformationDetail.ClosingDocumentsExpirationDate = ToMismoDate(this.loanData.sDocExpirationD_rep);

            closingInformationDetail.Extension = this.CreateClosingInformationDetailExtension();

            return closingInformationDetail;
        }

        /// <summary>
        /// Creates a container holding extension data on the closing process.
        /// </summary>
        /// <returns>A CLOSING_INFORMATION_DETAIL_EXTENSION container.</returns>
        private CLOSING_INFORMATION_DETAIL_EXTENSION CreateClosingInformationDetailExtension()
        {
            var extension = new CLOSING_INFORMATION_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBClosingDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates a container holding LendingQB extension values for CLOSING_INFORMATION_DETAIL.
        /// </summary>
        /// <returns>An LQB_CLOSING_INFORMATION_DETAIL_EXTENSION container.</returns>
        private LQB_CLOSING_INFORMATION_DETAIL_EXTENSION CreateLQBClosingDetailExtension()
        {
            var extension = new LQB_CLOSING_INFORMATION_DETAIL_EXTENSION();
            extension.EstimatedPrepaidDaysCount = ToMismoString(this.loanData.sIPiaDy_rep);
            extension.AdditionalInterestDaysRequiringConsentCount = ToMismoCount(
                string.IsNullOrEmpty(this.loanData.sAdditionalInterestDaysRequiringConsent_rep)
                    ? "0"
                    : this.loanData.sAdditionalInterestDaysRequiringConsent_rep,
                this.loanData.m_convertLos);

            return extension;
        }

        /// <summary>
        /// Creates a container holding Closing Instructions.
        /// </summary>
        /// <returns>Returns a container holding Closing Instructions.</returns>
        private CLOSING_INSTRUCTION CreateClosingInstruction()
        {
            CLOSING_INSTRUCTION closingInstruction = new CLOSING_INSTRUCTION();

            // Holds a list of conditions, including their description and a boolean indicating whether the
            // condition is complete.
            var conditionList = new List<Tuple<string, bool>>();

            if (this.loanData.BrokerDB.IsUseNewTaskSystem)
            {
                List<Task> tasks = Task.GetActiveConditionsByLoanId(this.loanData.sBrokerId, this.loanData.sLId, false);
                foreach (Task task in tasks)
                {
                    if (task.TaskStatus == E_TaskStatus.Closed)
                    {
                        continue; // Skip complete condition
                    }
                    
                    conditionList.Add(new Tuple<string, bool>(task.TaskSubject, task.TaskStatus != E_TaskStatus.Active));
                }
            }
            else
            {
                LoanConditionSetObsolete conditionSet = new LoanConditionSetObsolete();

                if (this.loanData.BrokerDB.HasLenderDefaultFeatures)
                {
                    conditionSet.Retrieve(this.loanData.sLId, false, false, false);

                    foreach (CLoanConditionObsolete condition in conditionSet)
                    {
                        if (condition.CondStatus == E_CondStatus.Done)
                        {
                            continue; // Skip complete condition.
                        }

                        conditionList.Add(new Tuple<string, bool>(condition.CondDesc, condition.CondStatus != E_CondStatus.Active));
                    }
                }
                else
                {
                    int count = this.loanData.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete condition = this.loanData.GetCondFieldsObsolete(i);
                        if (condition.IsDone)
                        {
                            continue;
                        }

                        conditionList.Add(new Tuple<string, bool>(condition.CondDesc, condition.IsDone));
                    }
                }
            }

            closingInstruction.Conditions = this.CreateConditions(conditionList);
            closingInstruction.ClosingInstructionDetail = this.CreateClosingInstructionDetail();
            return closingInstruction;
        }

        /// <summary>
        /// Creates a container holding closing instruction detail.
        /// </summary>
        /// <returns>Returns a container holding closing instruction detail.</returns>
        private CLOSING_INSTRUCTION_DETAIL CreateClosingInstructionDetail()
        {
            CLOSING_INSTRUCTION_DETAIL closingInstructionDetail = new CLOSING_INSTRUCTION_DETAIL();
            if (this.primaryApp != null)
            {
                closingInstructionDetail.LeadBasedPaintCertificationRequiredIndicator = ToMismoIndicator(this.primaryApp.aFHABorrCertReceivedLeadPaintPoisonInfoTri);
            }

            closingInstructionDetail.ClosingInstructionsPropertyTaxMessageDescription = ToMismoString(this.loanData.sPropertyTaxMessageDescription);
            return closingInstructionDetail;
        }
        
        /// <summary>
        /// Creates a container that holds elements with information about the subject property.
        /// </summary>
        /// <param name="loanId">Loan identifier associated with the collateral.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A COLLATERAL container.</returns>
        private COLLATERAL CreateCollateral(Guid loanId, int sequenceNumber)
        {
            var collateral = new COLLATERAL();
            collateral.SubjectProperty = this.CreateSubjectProperty(1);
            collateral.SequenceNumber = sequenceNumber;
            collateral.label = "Collateral" + sequenceNumber;

            this.allRelationships.Add(this.CreateRelationship_ToLoan("COLLATERAL", collateral.label, ArcroleVerbPhrase.IsAssociatedWith));
            
            return collateral;
        }

        /// <summary>
        /// Creates a container that holds all instances of COLLATERAL.
        /// </summary>
        /// <returns>A COLLATERALS container.</returns>
        private COLLATERALS CreateCollaterals()
        {
            var collaterals = new COLLATERALS();
            collaterals.Collateral.Add(this.CreateCollateral(this.loanData.sLId, 1));
            return collaterals;
        }

        /// <summary>
        /// Creates a container holding information on a condition.
        /// </summary>
        /// <param name="condition">Indicates the description of a condition and whether it is complete.</param>
        /// <param name="sequence">The sequence number of the condition among the list of conditions.</param>
        /// <returns>A CONDITION container.</returns>
        private CONDITION CreateCondition(Tuple<string, bool> condition, int sequence)
        {
            CONDITION conditionElement = new CONDITION();
            conditionElement.SequenceNumber = sequence;
            conditionElement.ConditionDescription = ToMismoString(condition.Item1);
            conditionElement.ConditionMetIndicator = ToMismoIndicator(condition.Item2);
            return conditionElement;
        }

        /// <summary>
        /// Creates a container to hold all instances of CONDITIONS.
        /// </summary>
        /// <param name="conditionList">A list of conditions holding descriptions and a boolean
        /// indicating whether they are complete.</param>
        /// <returns>A CONDITIONS container.</returns>
        private CONDITIONS CreateConditions(IEnumerable<Tuple<string, bool>> conditionList)
        {
            CONDITIONS conditions = new CONDITIONS();
            foreach (var condition in conditionList)
            {
                conditions.Condition.Add(this.CreateCondition(condition, conditions.Condition.Count(c => c != null) + 1));
            }

            return conditions;
        }

        /// <summary>
        /// Creates a container holding data on a construction loan.
        /// </summary>
        /// <returns>A CONSTRUCTION container, or null if the loan is not a construction loan.</returns>
        private CONSTRUCTION CreateConstruction()
        {            
            var construction = new CONSTRUCTION();
            if (IsConstructionLoan(this.loanData.sLPurposeT))
            {
                construction.ConstructionLoanType = ToConstructionLoanEnum(this.loanData.sLPurposeT);
            }

            construction.ConstructionImprovementCostsAmount = ToMismoAmount(this.loanData.sLotImprovC_rep);
            construction.ConstructionPeriodInterestRatePercent = ToMismoPercent(this.loanData.sConstructionPeriodIR_rep);
            construction.ConstructionPeriodNumberOfMonthsCount = ToMismoCount(this.loanData.sConstructionPeriodMon_rep, null);
            construction.ConstructionLoanEstimatedInterestCalculationMethodType = ToConstructionLoanEstimatedInterestCalculationMethodEnum(this.loanData.sConstructionIntCalcT);
            construction.LandOriginalCostAmount = ToMismoAmount(this.loanData.sLotOrigC_rep);
            construction.LandEstimatedValueAmount = ToMismoAmount(this.loanData.sPresentValOfLot_rep);
            construction.ConstructionLoanInterestReserveAmount = ToMismoAmount(this.loanData.sIntReserveAmt_rep);

            // The construction purpose will be blank if the SMF migrations have not run to populate the new
            // construction data. If this is the case, don't serialize any of the new datapoints.
            if (this.loanData.sConstructionPurposeT != ConstructionPurpose.Blank)
            {
                construction.ConstructionPhaseInterestPaymentType = ToConstructionPhaseInterestPaymentEnum(this.loanData.sConstructionPhaseIntPaymentTimingT);
                construction.ConstructionPhaseInterestPaymentFrequencyType = ToConstructionPhaseInterestPaymentFrequencyEnum(this.loanData.sConstructionPhaseIntPaymentFrequencyT);
                construction.ConstructionToPermanentClosingType = ToConstructionToPermanentClosingEnum(this.loanData.sConstructionToPermanentClosingT);

                construction.Extension = this.CreateConstructionExtension();
            }

            return construction;
        }

        /// <summary>
        /// Creates an extension container with data on a construction loan.
        /// </summary>
        /// <returns>A CONSTRUCTION_EXTENSION container.</returns>
        private CONSTRUCTION_EXTENSION CreateConstructionExtension()
        {
            var extension = new CONSTRUCTION_EXTENSION();

            if (this.loanData.sConstructionAmortT == E_sFinMethT.ARM)
            {
                extension.Mismo = this.CreateMismoConstructionExtension();
            }

            extension.Other = this.CreateLqbConstructionExtension();

            return extension;
        }

        /// <summary>
        /// Creates a MISMO extension with data on a construction loan.
        /// </summary>
        /// <returns>A MISMO_CONSTRUCTION_EXTENSION container.</returns>
        private MISMO_CONSTRUCTION_EXTENSION CreateMismoConstructionExtension()
        {
            var extension = new MISMO_CONSTRUCTION_EXTENSION();
            extension.Adjustment = this.CreateAdjustment_Construction();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LQB extension with data on a construction loan.
        /// </summary>
        /// <returns>An LQB_CONSTRUCTION_EXTENSION container.</returns>
        private LQB_CONSTRUCTION_EXTENSION CreateLqbConstructionExtension()
        {
            var extension = new LQB_CONSTRUCTION_EXTENSION();
            extension.ConstructionPurposeType = ToLqbConstructionPurposeEnum(this.loanData.sConstructionPurposeT);
            extension.ConstructionLoanAmortizationType = ToLqbConstructionLoanAmortizationEnum(this.loanData.sConstructionAmortT);
            extension.ConstructionInitialAdvanceAmount = ToMismoAmount(this.loanData.sConstructionInitialAdvanceAmt_rep);
            extension.ConstructionRequiredInterestReserveIndicator = ToMismoIndicator(this.loanData.sIsIntReserveRequired);
            extension.ConstructionPhaseInterestAccrualType = ToLqbConstructionPhaseInterestAccrualEnum(this.loanData.sConstructionPhaseIntAccrualT);
            extension.ConstructionLoanDate = ToMismoDate(this.loanData.sConstructionLoanD_rep);
            extension.ConstructionPeriodEndDate = ToMismoDate(this.loanData.sConstructionPeriodEndD_rep);
            extension.ConstructionFirstPaymentDueDate = ToMismoDate(this.loanData.sConstructionFirstPaymentD_rep);
            extension.LandCostAmount = ToMismoAmount(this.loanData.sLandCost_rep);
            extension.LotValueAmount = ToMismoAmount(this.loanData.sLotVal_rep);
            extension.LotAcquiredDate = ToMismoDate(this.loanData.sLotAcquiredD_rep);
            extension.LotOwnerType = ToLqbLotOwnerEnum(this.loanData.sLotOwnerT);
            extension.SubsequentlyPaidFinanceChargeAmount = ToMismoAmount(this.loanData.sSubsequentlyPaidFinanceChargeAmt_rep);
            extension.ConstructionInterestAmount = ToMismoAmount(this.loanData.sConstructionIntAmount_rep);
            extension.ConstructionInterestAccrualDate = ToMismoDate(this.loanData.sConstructionIntAccrualD_rep);
            extension.ConstructionDisclosureType = ToLqbConstructionDisclosureEnum(this.loanData.sConstructionDiscT);

            return extension;
        }

        /// <summary>
        /// Creates a CONTACT container with a phone number and fax number.
        /// </summary>
        /// <param name="telephoneNumber">A phone number.</param>
        /// <param name="faxNumber">A fax number.</param>
        /// <param name="email">An email address.</param>
        /// <param name="contactName">A NAME object containing the name of a person associated with the contact.</param>
        /// <param name="contactPointType">The locale / type of the contact point, e.g. work | home | mobile.</param>
        /// <param name="sequence">The sequence number of the contact among the list of contacts.</param>
        /// <returns>A CONTACT container.</returns>
        private CONTACT CreateContact(string telephoneNumber, string faxNumber, string email, NAME contactName, ContactPointRoleBase contactPointType, int sequence)
        {
            var contact = new CONTACT();
            contact.ContactPoints = this.CreateContactPoints(telephoneNumber, faxNumber, email, contactPointType);
            contact.Name = contactName;
            contact.SequenceNumber = sequence;

            return contact;
        }

        /// <summary>
        /// Creates a CONTACT_POINTS container with phone and fax contact points.
        /// </summary>
        /// <param name="phone">A phone number.</param>
        /// <param name="faxNumber">A fax number.</param>
        /// <param name="email">An email address.</param>
        /// <param name="contactPointType">The locale / type of the contact point, e.g. work | home | mobile.</param>
        /// <returns>A CONTACT_POINTS container.</returns>
        private CONTACT_POINTS CreateContactPoints(string phone, string faxNumber, string email, ContactPointRoleBase contactPointType)
        {
            if (string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(faxNumber) && string.IsNullOrEmpty(email))
            {
                return null;
            }

            var contactPoints = new CONTACT_POINTS();

            if (!string.IsNullOrEmpty(phone))
            {
                contactPoints.ContactPoint.Add(CreateContactPoint_Phone(phone, contactPointType, 1));
            }

            if (!string.IsNullOrEmpty(faxNumber))
            {
                contactPoints.ContactPoint.Add(CreateContactPoint_Fax(faxNumber, contactPointType, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            }

            if (!string.IsNullOrEmpty(email))
            {
                contactPoints.ContactPoint.Add(CreateContactPoint_Email(email, contactPointType, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            }

            return contactPoints;
        }

        /// <summary>
        /// Creates CONTACT_POINTS element for given borrower.
        /// </summary>
        /// <param name="dataApp">Loan app from which contact information is populated.</param>
        /// <returns>CONTACT_POINTS object containing borrower data.</returns>
        private CONTACT_POINTS CreateContactPoints(CAppData dataApp)
        {
            var contactPoints = new CONTACT_POINTS();
            contactPoints.ContactPoint.Add(CreateContactPoint_Phone(dataApp.aBusPhone, ContactPointRoleBase.Work, 1));
            contactPoints.ContactPoint.Add(CreateContactPoint_Phone(dataApp.aCellPhone, ContactPointRoleBase.Mobile, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            contactPoints.ContactPoint.Add(CreateContactPoint_Phone(dataApp.aHPhone, ContactPointRoleBase.Home, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            contactPoints.ContactPoint.Add(CreateContactPoint_Fax(dataApp.aFax, ContactPointRoleBase.Home, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            contactPoints.ContactPoint.Add(CreateContactPoint_Email(dataApp.aEmail, ContactPointRoleBase.Blank, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            return contactPoints;
        }

        /// <summary>
        /// Creates CONTACT_POINTS element for given preparer object. 
        /// </summary>
        /// <param name="preparer">1003 preparer object.</param>
        /// <param name="useCompanyContactInfo">Boolean indicating whether to use company contact information.</param>
        /// <returns>CONTACT_POINTS object containing preparer information.</returns>
        private CONTACT_POINTS CreateContactPoints(IPreparerFields preparer, bool useCompanyContactInfo)
        {
            string email = preparer.EmailAddr;
            string fax = useCompanyContactInfo ? preparer.FaxOfCompany : preparer.FaxNum;
            string phone = useCompanyContactInfo ? preparer.PhoneOfCompany : preparer.Phone;
            var contactPoints = new CONTACT_POINTS();
            contactPoints.ContactPoint.Add(CreateContactPoint_Phone(phone, ContactPointRoleBase.Work, 1));
            contactPoints.ContactPoint.Add(CreateContactPoint_Fax(fax, ContactPointRoleBase.Work, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            contactPoints.ContactPoint.Add(CreateContactPoint_Email(email, ContactPointRoleBase.Work, contactPoints.ContactPoint.Count(pt => pt != null) + 1));
            return contactPoints;
        }

        /// <summary>
        /// Creates a CONTACTS container with a phone number and fax number.
        /// </summary>
        /// <param name="telephoneNumber">A phone number.</param>
        /// <param name="faxNumber">A fax number.</param>
        /// <param name="email">An email address.</param>
        /// <param name="contactName">A NAME object containing the name of a person associated with the contacts.</param>
        /// <param name="contactPointType">The locale / type of the contact point, e.g. work | home | mobile.</param>
        /// <returns>A CONTACTS container.</returns>
        private CONTACTS CreateContacts(string telephoneNumber, string faxNumber, string email, NAME contactName, ContactPointRoleBase contactPointType)
        {
            var contacts = new CONTACTS();
            contacts.Contact.Add(this.CreateContact(telephoneNumber, faxNumber, email, contactName, contactPointType, 1));

            return contacts;
        }

        /// <summary>
        /// Creates a container representing borrower counseling.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A <see cref="COUNSELING"/> container.</returns>
        private COUNSELING CreateCounseling(CAppData appData)
        {
            var counseling = new COUNSELING();
            counseling.CounselingDetail = this.CreateCounselingDetail(appData);

            return counseling;
        }

        /// <summary>
        /// Creates a container with details on borrower counseling.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A <see cref="COUNSELING_DETAIL"/> container.</returns>
        private COUNSELING_DETAIL CreateCounselingDetail(CAppData appData)
        {
            var counselingDetail = new COUNSELING_DETAIL();
            counselingDetail.HousingCounselingAgenciesListProvidedDate = ToMismoDate(this.loanData.sHomeownerCounselingOrganizationDisclosureD_rep);

            return counselingDetail;
        }

        /// <summary>
        /// Creates a container to hold current income items.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A CURRENT_INCOME container.</returns>
        private CURRENT_INCOME CreateCurrentIncome(CAppData appData)
        {
            var currentIncome = new CURRENT_INCOME();
            currentIncome.CurrentIncomeItems = this.CreateCurrentIncomeItems(appData);

            return currentIncome;
        }

        /// <summary>
        /// Creates a container holding data on a common type of current income.
        /// </summary>
        /// <param name="amount">The amount of the income.</param>
        /// <param name="type">The type of the income.</param>
        /// <param name="sequence">The sequence number of the income item among the list of items.</param>
        /// <returns>A CURRENT_INCOME_ITEM container.</returns>
        private CURRENT_INCOME_ITEM CreateCurrentIncomeItem(string amount, IncomeBase type, int sequence)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var currentIncomeItem = new CURRENT_INCOME_ITEM();
            currentIncomeItem.CurrentIncomeItemDetail = this.CreateCurrentIncomeItemDetail(amount, type);
            currentIncomeItem.SequenceNumber = sequence;

            return currentIncomeItem;
        }

        /// <summary>
        /// Creates a container holding data on an uncommon type of current income, which must be
        /// mapped to MISMO from the income description.
        /// </summary>
        /// <param name="amount">The amount of the income.</param>
        /// <param name="type">The type of the income.</param>
        /// <param name="sequence">The sequence number of the income item among the list of items.</param>
        /// <param name="otherDescription">The description for income type "Other".</param>
        /// <returns>A CURRENT_INCOME_ITEM container.</returns>
        private CURRENT_INCOME_ITEM CreateCurrentIncomeItem(string amount, E_aOIDescT type, int sequence, string otherDescription)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var currentIncomeItem = new CURRENT_INCOME_ITEM();
            currentIncomeItem.CurrentIncomeItemDetail = this.CreateCurrentIncomeItemDetail(amount, type, otherDescription);
            currentIncomeItem.SequenceNumber = sequence;

            return currentIncomeItem;
        }

        /// <summary>
        /// Creates a container to hold all instances of CURRENT_INCOME_ITEM, and populates it
        /// using types of income listed in the application data.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A CURRENT_INCOME_ITEMS container.</returns>
        private CURRENT_INCOME_ITEMS CreateCurrentIncomeItems(CAppData appData)
        {
            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower ? IsAmountStringBlankOrZero(appData.aBTotI_rep) : IsAmountStringBlankOrZero(appData.aCTotI_rep))
            {
                return null;
            }

            var currentIncomeItems = new CURRENT_INCOME_ITEMS();
            currentIncomeItems.CurrentIncomeItem.Add(this.CreateCurrentIncomeItem(appData.aBaseI_rep, IncomeBase.Base, 1));
            currentIncomeItems.CurrentIncomeItem.Add(this.CreateCurrentIncomeItem(appData.aOvertimeI_rep, IncomeBase.Overtime, currentIncomeItems.CurrentIncomeItem.Count(i => i != null) + 1));
            currentIncomeItems.CurrentIncomeItem.Add(this.CreateCurrentIncomeItem(appData.aBonusesI_rep, IncomeBase.Bonus, currentIncomeItems.CurrentIncomeItem.Count(i => i != null) + 1));
            currentIncomeItems.CurrentIncomeItem.Add(this.CreateCurrentIncomeItem(appData.aCommisionI_rep, IncomeBase.Commissions, currentIncomeItems.CurrentIncomeItem.Count(i => i != null) + 1));
            currentIncomeItems.CurrentIncomeItem.Add(this.CreateCurrentIncomeItem(appData.aDividendI_rep, IncomeBase.DividendsInterest, currentIncomeItems.CurrentIncomeItem.Count(i => i != null) + 1));
            currentIncomeItems.CurrentIncomeItem.Add(this.CreateCurrentIncomeItem(appData.aNetRentI1003_rep, IncomeBase.NetRentalIncome, currentIncomeItems.CurrentIncomeItem.Count(i => i != null) + 1));
            currentIncomeItems.CurrentIncomeItem.Add(this.CreateCurrentIncomeItem(appData.aSpPosCf_rep, IncomeBase.SubjectPropertyNetCashFlow, currentIncomeItems.CurrentIncomeItem.Count(i => i != null) + 1));

            foreach (OtherIncome otherIncome in appData.aOtherIncomeList)
            {
                if ((otherIncome.IsForCoBorrower && appData.BorrowerModeT == E_BorrowerModeT.Coborrower) || (!otherIncome.IsForCoBorrower && appData.BorrowerModeT == E_BorrowerModeT.Borrower))
                {
                    currentIncomeItems.CurrentIncomeItem.Add(this.CreateCurrentIncomeItem(appData.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep), OtherIncome.Get_aOIDescT(otherIncome.Desc), currentIncomeItems.CurrentIncomeItem.Count(i => i != null) + 1, otherIncome.Desc));
                }
            }

            return currentIncomeItems;
        }

        /// <summary>
        /// Creates a container with details on a common income item.
        /// </summary>
        /// <param name="amount">The amount of the income.</param>
        /// <param name="type">The type of the income.</param>
        /// <returns>A CURRENT_INCOME_ITEM_DETAIL container.</returns>
        private CURRENT_INCOME_ITEM_DETAIL CreateCurrentIncomeItemDetail(string amount, IncomeBase type)
        {
            var currentIncomeItemDetail = new CURRENT_INCOME_ITEM_DETAIL();
            currentIncomeItemDetail.CurrentIncomeMonthlyTotalAmount = ToMismoAmount(amount);
            currentIncomeItemDetail.IncomeType = ToIncomeEnum(type);

            return currentIncomeItemDetail;
        }

        /// <summary>
        /// Creates a container with details on an uncommon income item, which must be mapped from an LQB value.
        /// </summary>
        /// <param name="amount">The amount of the income.</param>
        /// <param name="type">The type of the income.</param>
        /// <param name="otherDescription">The description of the income if the type is "Other".</param>
        /// <returns>A CURRENT_INCOME_ITEM_DETAIL container.</returns>
        private CURRENT_INCOME_ITEM_DETAIL CreateCurrentIncomeItemDetail(string amount, E_aOIDescT type, string otherDescription)
        {
            var currentIncomeItemDetail = new CURRENT_INCOME_ITEM_DETAIL();
            currentIncomeItemDetail.CurrentIncomeMonthlyTotalAmount = ToMismoAmount(amount);
            currentIncomeItemDetail.IncomeType = ToIncomeEnum(type);
            if (currentIncomeItemDetail.IncomeType != null && currentIncomeItemDetail.IncomeType.enumValue == IncomeBase.Other)
            {
                currentIncomeItemDetail.IncomeTypeOtherDescription = ToMismoString(otherDescription);
            }

            return currentIncomeItemDetail;
        }

        /// <summary>
        /// Creates a DEAL which holds information about a particular transaction.
        /// </summary>
        /// <returns>A DEAL populated with loan information.</returns>
        private DEAL CreateDeal()
        {
            var deal = new DEAL();
            
            deal.SequenceNumber = 1; // Hardcoded since we only show data for one loan.
            deal.Loans = this.CreateLoans(); // 3/26/2015 BB - Do this first to populate the loanLabel dictionary that's used when generating the relationships for loan-level siblings.
            deal.Parties = this.CreateParties(); // Do this second for the seller liability relationships.
            deal.Assets = this.CreateAssets(); // 3/31/2015 IR - Do this prior to CreateLiabilities so that reoAssetLabel dictionary populates.
            deal.Collaterals = this.CreateCollaterals();
            deal.Expenses = this.CreateExpenses();
            deal.Liabilities = this.CreateLiabilities();

            //// Do this one last so that all relationships will populate.
            deal.Relationships = this.CreateRelationships();

            return deal;
        }

        /// <summary>
        /// Creates a DEALS container which holds a set of DEAL.
        /// </summary>
        /// <returns>A DEALS container.</returns>
        private DEALS CreateDeals()
        {
            var deals = new DEALS();
            deals.Deal.Add(this.CreateDeal());

            return deals;
        }

        /// <summary>
        /// Creates a DEAL_SET container which holds a DEALS group.
        /// </summary>
        /// <returns>A DEAL_SET container.</returns>
        private DEAL_SET CreateDealSet()
        {
            var dealSet = new DEAL_SET();
            dealSet.SequenceNumber = 1; // Hardcoded since we only show data for one loan.
            dealSet.Deals = this.CreateDeals();

            return dealSet;
        }

        /// <summary>
        /// Creates a DEAL_SETS container which holds all DEAL_SETs.
        /// </summary>
        /// <returns>A DEAL_SETS container.</returns>
        private DEAL_SETS CreateDealSets()
        {
            var dealSets = new DEAL_SETS();
            dealSets.DealSet.Add(this.CreateDealSet());

            return dealSets;
        }

        /// <summary>
        /// Creates a container representing a declaration.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A DECLARATION container.</returns>
        private DECLARATION CreateDeclaration(CAppData appData)
        {
            var declaration = new DECLARATION();
            declaration.DeclarationDetail = this.CreateDeclarationDetail(appData);

            return declaration;
        }

        /// <summary>
        /// Creates a container holding data on a declaration.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A DECLARATION_DETAIL container.</returns>
        private DECLARATION_DETAIL CreateDeclarationDetail(CAppData appData)
        {
            var declarationDetail = new DECLARATION_DETAIL();
            declarationDetail.AlimonyChildSupportObligationIndicator = ToMismoIndicator(appData.aDecAlimony);
            declarationDetail.BankruptcyIndicator = ToMismoIndicator(appData.aDecBankrupt);
            declarationDetail.BorrowedDownPaymentIndicator = ToMismoIndicator(appData.aDecBorrowing);

            if (this.loanData.sLT == E_sLT.FHA || this.loanData.sLT == E_sLT.VA)
            {
                declarationDetail.BorrowerFirstTimeHomebuyerIndicator = ToMismoIndicator(appData.aHas1stTimeBuyerTri == E_TriState.Yes);
            }
            else
            {
                declarationDetail.BorrowerFirstTimeHomebuyerIndicator = ToMismoIndicator(this.loanData.sHas1stTimeBuyer);
            }

            declarationDetail.CitizenshipResidencyType = ToCitizenshipResidencyEnum(appData.aDecResidency, appData.aDecCitizen);
            declarationDetail.CoMakerEndorserOfNoteIndicator = ToMismoIndicator(appData.aDecEndorser);
            declarationDetail.HomeownerPastThreeYearsType = ToHomeownerPastThreeYearsEnum(appData.aDecPastOwnership);
            declarationDetail.IntentToOccupyType = ToIntentToOccupyEnum(appData.aDecOcc);
            declarationDetail.LoanForeclosureOrJudgmentIndicator = ToMismoIndicator(appData.aDecObligated);
            declarationDetail.OutstandingJudgmentsIndicator = ToMismoIndicator(appData.aDecJudgment);
            declarationDetail.PartyToLawsuitIndicator = ToMismoIndicator(appData.aDecLawsuit);
            declarationDetail.PresentlyDelinquentIndicator = ToMismoIndicator(appData.aDecDelinquent);
            declarationDetail.PriorPropertyTitleType = ToPriorPropertyTitleEnum(appData.aDecPastOwnedPropTitleT);
            declarationDetail.PriorPropertyUsageType = ToPriorPropertyUsageEnum(appData.aDecPastOwnedPropT);
            declarationDetail.PropertyForeclosedPastSevenYearsIndicator = ToMismoIndicator(appData.aDecForeclosure);

            return declarationDetail;
        }

        /// <summary>
        /// Creates a container holding data on a dependent of the borrower.
        /// </summary>
        /// <param name="age">The age of the dependent.</param>
        /// <param name="sequence">The sequence number of the dependent among the list of dependents.</param>
        /// <returns>A DEPENDENT container.</returns>
        private DEPENDENT CreateDependent(string age, int sequence)
        {
            var dependent = new DEPENDENT();
            dependent.DependentAgeYearsCount = ToMismoCount(age, this.loanData.m_convertLos);
            dependent.SequenceNumber = sequence;
            
            return dependent;
        }

        /// <summary>
        /// Creates a container to hold all instances of DEPENDENT and populates it with
        /// a specific borrower's dependents.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A DEPENDENTS container.</returns>
        private DEPENDENTS CreateDependents(CAppData appData)
        {
            var dependents = new DEPENDENTS();
            string[] dependentAges = appData.aDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':');

            foreach (string age in dependentAges)
            {
                if (string.IsNullOrEmpty(age))
                {
                    continue;
                }

                dependents.Dependent.Add(this.CreateDependent(age, dependents.Dependent.Count(d => d != null) + 1));
            }

            return dependents;
        }

        /// <summary>
        /// Creates a container with a document class for the document given by the document type.
        /// </summary>
        /// <param name="docType">The document type.</param>
        /// <param name="sequence">The sequence number of the document class among the list of classes.</param>
        /// <returns>A DOCUMENT_CLASS container.</returns>
        private DOCUMENT_CLASS CreateDocumentClass(DocumentBase docType, int sequence)
        {
            var documentClass = new DOCUMENT_CLASS();
            if (this.vendor == E_DocumentVendor.DocMagic && (docType == DocumentBase.ClosingDisclosure || docType == DocumentBase.LoanEstimate))                 
            {
                documentClass.DocumentType = ToDocumentEnum(DocumentBase.Other);

                string prefix = string.Empty;                
                if (docType == DocumentBase.LoanEstimate)
                {
                    prefix = "LoanEstimate:";
                }
                else 
                { 
                    // if (docType == DocumentType.ClosingDisclosure)
                    prefix = "ClosingDisclosure:";
                }

                if ((docType == DocumentBase.LoanEstimate && this.loanEstimateArchiveData.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative) ||
                    (docType == DocumentBase.ClosingDisclosure && this.closingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative))
                {
                    documentClass.DocumentTypeOtherDescription = ToMismoString(prefix + "AlternateForm");
                }
                else 
                {
                    // E_CashToCloseCalcMethodT.Standard
                    documentClass.DocumentTypeOtherDescription = ToMismoString(prefix + "ModelForm");
                }
            }
            else
            {
                documentClass.DocumentType = ToDocumentEnum(docType);                
            }

            documentClass.SequenceNumber = sequence;

            return documentClass;
        }

        /// <summary>
        /// Creates a container with the classifications of a document.
        /// </summary>
        /// <param name="docType">The document type.</param>
        /// <returns>A DOCUMENT_CLASSES container.</returns>
        private DOCUMENT_CLASSES CreateDocumentClasses(DocumentBase docType)
        {
            var documentClasses = new DOCUMENT_CLASSES();
            documentClasses.DocumentClass.Add(this.CreateDocumentClass(docType, 1));

            return documentClasses;
        }

        /// <summary>
        /// Creates a container holding information on specific documents.
        /// </summary>
        /// <param name="docType">Indicates the type of document that should be created.</param>
        /// <param name="document">An instance of either <see cref="LoanEstimateDates" /> or <see cref="ClosingDisclosureDates" />, depending on the <see cref="DocumentBase" /> parameter.</param>
        /// <param name="currentLoanTRID">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued. Should also be false if the disclosure is not a TRID disclosure.</param>
        /// <param name="sequence">The sequence number of the data-set within the list of data-sets.</param>
        /// <returns>A DOCUMENT_SPECIFIC_DATA_SET container.</returns>
        private DOCUMENT_SPECIFIC_DATA_SET CreateDocumentSpecificDataSet(DocumentBase docType, object document, bool currentLoanTRID, int sequence)
        {
            var documentSpecificDataSet = new DOCUMENT_SPECIFIC_DATA_SET();

            documentSpecificDataSet.DocumentClasses = this.CreateDocumentClasses(docType);
            documentSpecificDataSet.SequenceNumber = sequence;

            if (docType == DocumentBase.ClosingDisclosure || docType == DocumentBase.LoanEstimate)
            {
                documentSpecificDataSet.IntegratedDisclosure = this.CreateIntegratedDisclosure(docType, document, currentLoanTRID);

                if (!documentSpecificDataSet.IntegratedDisclosureSpecified || 
                    !documentSpecificDataSet.IntegratedDisclosure.IntegratedDisclosureDetailSpecified ||
                    !documentSpecificDataSet.IntegratedDisclosure.IntegratedDisclosureDetail.IntegratedDisclosureIssuedDateSpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.LoanApplicationURLA)
            {
                documentSpecificDataSet.URLA = this.CreateUrla();

                if (!documentSpecificDataSet.URLASpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.SecurityInstrument)
            {
                // 4/23/2015 BB - These are both applicable to the deed of trust.
                documentSpecificDataSet.SecurityInstrument = this.CreateSecurityInstrument();
                documentSpecificDataSet.RecordingEndorsements = this.CreateRecordingEndorsements();
                documentSpecificDataSet.Execution = this.CreateExecution();

                if (!documentSpecificDataSet.SecurityInstrumentSpecified && !documentSpecificDataSet.RecordingEndorsementsSpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.TILDisclosure)
            {
                documentSpecificDataSet.TILDisclosure = this.CreateTilDisclosure();

                if (!documentSpecificDataSet.TILDisclosureSpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.GFE)
            {
                documentSpecificDataSet.GFE = this.CreateGfe();

                if (!documentSpecificDataSet.GFESpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.Note)
            {
                documentSpecificDataSet.Note = this.CreateNote();
                documentSpecificDataSet.Execution = this.CreateExecution();
            }

            return documentSpecificDataSet;
        }

        /// <summary>
        /// Creates a DOCUMENT_SPECIFIC_DATA_SET with an integrated disclosure data packet representing the current loan information to be disclosed.
        /// </summary>
        /// <param name="sequence">The sequence of the data-set among the list of sets.</param>
        /// <returns>A DOCUMENT_SPECIFIC_DATA_SET with an integrated disclosure data packet representing the current loan information to be disclosed.</returns>
        private DOCUMENT_SPECIFIC_DATA_SET CreateToBeDisclosed(int sequence)
        {
            this.hasArchive = true; //// Tricks the exporter into generating all of the integrated disclosure sub-containers.
            bool resetExtendIntegratedDisclosure = this.extendIntegratedDisclosure;
            this.extendIntegratedDisclosure = false; //// Omits the fee information extension that is meant only for disclosures that have already been issued.

            if (this.isClosingPackage)
            {
                CPageData resetCDArchive = this.closingDisclosureArchiveData;
                this.closingDisclosureArchiveData = this.loanData; //// Tricks the exporter into using the live loan data when generating the integrated disclosure sub-containers.

                var metadata = new ClosingDisclosureDatesMetadata(
                    this.closingDisclosureArchiveData.GetClosingCostArchiveById,
                    this.closingDisclosureArchiveData.GetConsumerDisclosureMetadataByConsumerId(),
                    this.closingDisclosureArchiveData.sLoanRescindableT,
                    this.closingDisclosureArchiveData.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

                ClosingDisclosureDates currentCDDates = ClosingDisclosureDates.Create(metadata);
                currentCDDates.IssuedDateLckd = true;
                currentCDDates.IssuedDate = System.DateTime.Now; //// Hardcodes the integrated disclosure issue date to the current date.
                if (this.closingDisclosureArchiveData.sDocMagicClosingD.IsValid)
                {
                    currentCDDates.LoanClosingDate = this.closingDisclosureArchiveData.sDocMagicClosingD.DateTimeForComputation;
                }

                var closingDisclosure = this.CreateDocumentSpecificDataSet(docType: DocumentBase.ClosingDisclosure, document: currentCDDates, currentLoanTRID: true, sequence: sequence);

                this.closingDisclosureArchiveData = resetCDArchive;
                this.hasArchive = false;
                this.extendIntegratedDisclosure = resetExtendIntegratedDisclosure;
                return closingDisclosure;
            }
            else
            {
                CPageData resetLEArchive = this.loanEstimateArchiveData;
                this.loanEstimateArchiveData = this.loanData;

                var metadata = new LoanEstimateDatesMetadata(
                    this.loanEstimateArchiveData.GetClosingCostArchiveById,
                    this.loanEstimateArchiveData.GetConsumerDisclosureMetadataByConsumerId(),
                    this.loanEstimateArchiveData.sLoanRescindableT);

                LoanEstimateDates currentLEDates = LoanEstimateDates.Create(metadata);
                currentLEDates.IssuedDateLckd = true;
                currentLEDates.IssuedDate = System.DateTime.Now;

                var loanEstimate = this.CreateDocumentSpecificDataSet(docType: DocumentBase.LoanEstimate, document: currentLEDates, currentLoanTRID: true, sequence: sequence);

                this.loanEstimateArchiveData = resetLEArchive;
                this.hasArchive = false;
                this.extendIntegratedDisclosure = resetExtendIntegratedDisclosure;
                return loanEstimate;
            }
        }

        /// <summary>
        /// Creates a container that holds all instances of DOCUMENT_SPECIFIC_DATA_SET.
        /// </summary>
        /// <returns>A DOCUMENT_SPECIFIC_DATA_SETS container.</returns>
        private DOCUMENT_SPECIFIC_DATA_SETS CreateDocumentSpecificDataSets()
        {
            var documentSpecificDataSets = new DOCUMENT_SPECIFIC_DATA_SETS();

            if (this.vendor != E_DocumentVendor.UnknownOtherNone)
            {
                documentSpecificDataSets.DocumentSpecificDataSet.Add(this.CreateToBeDisclosed(1));
            }

            if (this.vendor != E_DocumentVendor.DocMagic)
            {
                foreach (LoanEstimateDates loanEstimate in this.loanData.sLoanEstimateDatesInfo.LoanEstimateDatesList)
                {
                    this.ApplyIntegratedDisclosureArchive(DocumentBase.LoanEstimate, loanEstimate.ArchiveId);

                    if (!this.shouldOmitArchive)
                    {
                        documentSpecificDataSets.DocumentSpecificDataSet.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.LoanEstimate, document: loanEstimate, currentLoanTRID: false, sequence: documentSpecificDataSets.DocumentSpecificDataSet.Count(s => s != null) + 1));
                    }
                }

                foreach (ClosingDisclosureDates closingDisclosure in this.loanData.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
                {
                    this.ApplyIntegratedDisclosureArchive(DocumentBase.ClosingDisclosure, closingDisclosure.ArchiveId);

                    if (!this.shouldOmitArchive)
                    {
                        documentSpecificDataSets.DocumentSpecificDataSet.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.ClosingDisclosure, document: closingDisclosure, currentLoanTRID: false, sequence: documentSpecificDataSets.DocumentSpecificDataSet.Count(s => s != null) + 1));
                    }
                }
            }

            documentSpecificDataSets.DocumentSpecificDataSet.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.LoanApplicationURLA, document: null, currentLoanTRID: false, sequence: documentSpecificDataSets.DocumentSpecificDataSet.Count(s => s != null) + 1));
            documentSpecificDataSets.DocumentSpecificDataSet.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.SecurityInstrument, document: null, currentLoanTRID: false, sequence: documentSpecificDataSets.DocumentSpecificDataSet.Count(s => s != null) + 1));
            documentSpecificDataSets.DocumentSpecificDataSet.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.TILDisclosure, document: null, currentLoanTRID: false, sequence: documentSpecificDataSets.DocumentSpecificDataSet.Count(s => s != null) + 1));
            documentSpecificDataSets.DocumentSpecificDataSet.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.GFE, document: null, currentLoanTRID: false, sequence: documentSpecificDataSets.DocumentSpecificDataSet.Count(s => s != null) + 1));

            if (this.vendor == E_DocumentVendor.DocMagic)
            {
                documentSpecificDataSets.DocumentSpecificDataSet.Add(this.CreateDocumentSpecificDataSet(DocumentBase.Note, document: null, currentLoanTRID: false, sequence: documentSpecificDataSets.DocumentSpecificDataSet.Count(s => s != null) + 1));
            }

            return documentSpecificDataSets;
        }

        /// <summary>
        /// Creates a DOWN_PAYMENT holding information on the down payment for a loan.
        /// </summary>
        /// <param name="sequence">The sequence number of the down payment among the set of down payments.</param>
        /// <returns>A DOWN_PAYMENT populated with data.</returns>
        private DOWN_PAYMENT CreateDownPayment(int sequence)
        {
            var downPayment = new DOWN_PAYMENT();
            downPayment.DownPaymentAmount = ToMismoAmount(this.loanData.sEquityCalc_rep);
            
            MISMOEnum<FundsSourceBase> fundsSourceType = ToFundsSourceEnum(this.loanData.sDwnPmtSrc);
                        
            downPayment.FundsType = ToFundsEnum(this.loanData.sDwnPmtSrc);
            
            if (downPayment.FundsType != null && downPayment.FundsType.enumValue == FundsBase.Other)
            {
                downPayment.FundsTypeOtherDescription = ToMismoString(this.loanData.sDwnPmtSrc);
            }

            if (fundsSourceType != null)
            {
                downPayment.PropertySellerFundingIndicator = ToMismoIndicator(fundsSourceType.enumValue == FundsSourceBase.PropertySeller);
            }

            downPayment.SequenceNumber = sequence;

            return downPayment;
        }

        /// <summary>
        /// Creates a DOWN_PAYMENTS container which holds a set of down payments.
        /// </summary>
        /// <returns>A DOWN_PAYMENTS container.</returns>
        private DOWN_PAYMENTS CreateDownPayments()
        {
            var downPayments = new DOWN_PAYMENTS();
            downPayments.DownPayment.Add(this.CreateDownPayment(1));

            return downPayments;
        }

        /// <summary>
        /// Creates a container holding elements with information on a draw.
        /// </summary>
        /// <returns>A DRAW container.</returns>
        private DRAW CreateDraw()
        {
            if (!this.loanData.sIsLineOfCredit)
            {
                return null;
            }

            var draw = new DRAW();
            draw.DrawRule = this.CreateDrawRule();

            return draw;
        }

        /// <summary>
        /// Creates a container holding data on a draw rule.
        /// </summary>
        /// <returns>A DRAW_RULE container populated with data.</returns>
        private DRAW_RULE CreateDrawRule()
        {
            if (!this.loanData.sIsLineOfCredit)
            {
                return null;
            }

            var drawRule = new DRAW_RULE();
            ////drawRule.LoanDrawExpirationDate;
            ////drawRule.LoanDrawExtensionTermMonthsCount;
            drawRule.LoanDrawMaximumAmount = ToMismoAmount(this.loanData.sCreditLineAmt_rep);
            ////drawRule.LoanDrawMaximumTermMonthsCount;
            drawRule.LoanDrawMinimumAmount = ToMismoAmount(this.loanData.sHelocMinimumAdvanceAmt_rep);
            ////drawRule.LoanDrawMinimumInitialDrawAmount;
            ////drawRule.LoanDrawPeriodMaximumDrawCount;
            ////drawRule.LoanDrawStartPeriodMonthsCount;
            drawRule.LoanDrawTermMonthsCount = ToMismoCount(this.loanData.sHelocDraw_rep, null);

            return drawRule;
        }

        /// <summary>
        /// Creates a container representing a borrower's primary employer.
        /// </summary>
        /// <param name="record">The primary employer record.</param>
        /// <returns>An EMPLOYER container.</returns>
        private EMPLOYER CreateEmployer(IPrimaryEmploymentRecord record)
        {
            if (record == null)
            {
                return null;
            }

            var employer = new EMPLOYER();
            employer.SequenceNumber = this.counters.Employers;

            employer.LegalEntity = this.CreateLegalEntity(record.EmplrNm, record.EmplrBusPhone, record.EmplrFax, string.Empty, null, ContactPointRoleBase.Work);
            employer.Address = CreateAddress(
                record.EmplrAddr, 
                record.EmplrCity, 
                record.EmplrState, 
                record.EmplrZip, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(record.EmplrState), 
                AddressBase.Blank, 
                1);

            employer.Employment = this.CreateEmployment(record);

            return employer;
        }

        /// <summary>
        /// Creates a container representing one of a borrower's secondary employers.
        /// </summary>
        /// <param name="record">A secondary employer record.</param>
        /// <returns>An EMPLOYER container.</returns>
        private EMPLOYER CreateEmployer(IRegularEmploymentRecord record)
        {
            if (record == null)
            {
                return null;
            }

            var employer = new EMPLOYER();
            employer.SequenceNumber = this.counters.Employers;

            employer.LegalEntity = this.CreateLegalEntity(record.EmplrNm, record.EmplrBusPhone, record.EmplrFax, string.Empty, null, ContactPointRoleBase.Work);
            employer.Address = CreateAddress(
                record.EmplrAddr, 
                record.EmplrCity, 
                record.EmplrState, 
                record.EmplrZip, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(record.EmplrState),
                AddressBase.Blank, 
                1);

            employer.Employment = this.CreateEmployment(record);

            return employer;
        }

        /// <summary>
        /// Creates a container to hold all instances of EMPLOYER, and populates it with employer
        /// data from the borrower's application.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>An EMPLOYERS container.</returns>
        private EMPLOYERS CreateEmployers(CAppData appData)
        {
            var employers = new EMPLOYERS();
            employers.Employer.Add(this.CreateEmployer(appData.aEmpCollection.GetPrimaryEmp(false)));

            foreach (IRegularEmploymentRecord record in appData.aEmpCollection.GetSubcollection(true, E_EmpGroupT.Previous))
            {
                employers.Employer.Add(this.CreateEmployer(record));
            }

            return employers;
        }

        /// <summary>
        /// Creates a container holding data on a borrower's employment with a primary employer.
        /// </summary>
        /// <param name="record">The primary employer record.</param>
        /// <returns>An EMPLOYMENT container.</returns>
        private EMPLOYMENT CreateEmployment(IPrimaryEmploymentRecord record)
        {
            var employment = new EMPLOYMENT();
            employment.EmploymentMonthsOnJobCount = ToMismoCount(record.EmplmtLenInMonths_rep, null);
            employment.EmploymentYearsOnJobCount = ToMismoCount(record.EmplmtLenInYrs_rep, null);
            employment.EmploymentTimeInLineOfWorkYearsCount = ToMismoCount(record.ProfLenYears, null);
            employment.EmploymentTimeInLineOfWorkMonthsCount = ToMismoCount(record.ProfLenRemainderMonths, null);
            employment.EmploymentBorrowerSelfEmployedIndicator = ToMismoIndicator(record.IsSelfEmplmt);
            employment.EmploymentStatusType = ToEmploymentStatusEnum(record.IsCurrent);
            employment.EmploymentPositionDescription = ToMismoString(record.JobTitle);
            employment.EmploymentClassificationType = ToEmploymentClassificationEnum(record.IsPrimaryEmp);

            return employment;
        }

        /// <summary>
        /// Creates a container holding data on a borrower's employment with a secondary employer.
        /// </summary>
        /// <param name="record">A secondary employer record.</param>
        /// <returns>An EMPLOYMENT container.</returns>
        private EMPLOYMENT CreateEmployment(IRegularEmploymentRecord record)
        {
            var employment = new EMPLOYMENT();
            employment.EmploymentBorrowerSelfEmployedIndicator = ToMismoIndicator(record.IsSelfEmplmt);
            employment.EmploymentClassificationType = ToEmploymentClassificationEnum(record.IsPrimaryEmp);
            employment.EmploymentEndDate = ToMismoDate(record.EmplmtEndD_rep);
            employment.EmploymentMonthlyIncomeAmount = ToMismoAmount(record.MonI_rep);
            employment.EmploymentPositionDescription = ToMismoString(record.JobTitle);
            employment.EmploymentStartDate = ToMismoDate(record.EmplmtStartD_rep);
            employment.EmploymentStatusType = ToEmploymentStatusEnum(record.IsCurrent);

            return employment;
        }

        /// <summary>
        /// Creates ESCROW container with loan escrow detail provided.
        /// </summary>
        /// <returns>Returns ESCROW container with loan escrow detail provided.</returns>
        private ESCROW CreateEscrow()
        {
            var escrow = new ESCROW();
            escrow.EscrowDetail = this.CreateEscrowDetail();
            escrow.EscrowItems = this.CreateEscrowItems();
            return escrow;
        }

        /// <summary>
        /// Creates ESCROW_DETAIL container with loan escrow detail provided.
        /// </summary>
        /// <returns>Returns ESCROW_DETAIL container with loan escrow detail provided.</returns>
        private ESCROW_DETAIL CreateEscrowDetail()
        {
            var escrowDetail = new ESCROW_DETAIL();
            ////To-do: Isaac, implement initialescrowbalanceamount according to sClosingCostSet with proper version.
            ////escrowDetail.GFEDisclosedInitialEscrowBalanceAmount = null;
            escrowDetail.InitialEscrowDepositIncludesOtherDescription = ToMismoString(this.loanData.sGfeHasImpoundDepositDescription);
            escrowDetail.EscrowAggregateAccountingAdjustmentAmount = ToMismoAmount(this.loanData.sAggregateAdjRsrv_rep);
            escrowDetail.EscrowCushionNumberOfMonthsCount = ToMismoCount(this.loanData.sEscrowCushionMonths_rep, null) ?? ToMismoCount("0", null);
            escrowDetail.EscrowAccountMinimumBalanceAmount = ToMismoAmount(this.loanData.sAggrEscrowCushionRequired_rep);

            return escrowDetail;
        }

        /// <summary>
        /// Creates a container to hold all escrow expenses.
        /// </summary>
        /// <returns>An ESCROW_ITEMS container.</returns>
        private ESCROW_ITEMS CreateEscrowItems()
        {
            var escrowItems = new ESCROW_ITEMS();

            if (this.loanData.sMInsRsrvEscrowedTri == E_TriState.Yes && (this.loanData.sProMIns > 0.00m || this.loanData.sMInsRsrv > 0.00m))
            {
                escrowItems.EscrowItem.Add(this.CreateEscrowItem_MI());
            }

            foreach (BaseHousingExpense expense in this.loanData.sHousingExpenses.ExpensesToUse.Where(e => (e.AnnualAmt != 0.00m || e.MonthlyAmtTotal != 0.00m || e.ReserveAmt != 0.00m) && e.IsEscrowedAtClosing == E_TriState.Yes))
            {
                escrowItems.EscrowItem.Add(this.CreateEscrowItem(expense));
            }

            return escrowItems;
        }

        /// <summary>
        /// Creates a container holding data on a specific escrow item.
        /// </summary>
        /// <param name="expense">The escrowed expense.</param>
        /// <returns>An ESCROW_ITEM container.</returns>
        private ESCROW_ITEM CreateEscrowItem(BaseHousingExpense expense)
        {
            BorrowerClosingCostFee escrowFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionG);
            BorrowerClosingCostFee archivedFee = null;
            if (this.loanData.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017 && this.loanData.sAppliedCCArchiveLinkedArchiveId != Guid.Empty)
            {
                // If there is a linked pair of archives, the archive applied to the loan contains the
                // actual fee amounts while the linked archive contains the tolerance data.
                var linkedArchive = this.loanData.GetClosingCostArchiveById(this.loanData.sAppliedCCArchiveLinkedArchiveId);
                archivedFee = linkedArchive?.GetClosingCostSet(this.loanData.m_convertLos)?.GetFees(f => f.ClosingCostFeeTypeId == expense.GetClosingCostFee900Or1000Type(E_IntegratedDisclosureSectionT.SectionG))?.FirstOrDefault() as BorrowerClosingCostFee;
            }

            if (archivedFee == null)
            {
                archivedFee = this.loanData.LastDisclosedLoanEstimateArchive?.GetClosingCostSet(this.loanData.m_convertLos)?.GetFees(f => f.ClosingCostFeeTypeId == expense.GetClosingCostFee900Or1000Type(E_IntegratedDisclosureSectionT.SectionG))?.FirstOrDefault() as BorrowerClosingCostFee;
            }

            var escrowItem = new ESCROW_ITEM();
            escrowItem.EscrowItemDetail = this.CreateEscrowItemDetail(expense, escrowFee, archivedFee);
            escrowItem.SequenceNumber = this.counters.EscrowItems;

            if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                escrowItem.EscrowItemDisbursements = this.CreateEscrowItemDisbursements(expense.ActualDisbursementsList);
            }
            else
            {
                escrowItem.EscrowItemDisbursements = this.CreateEscrowItemDisbursements(expense.ProjectedDisbursements);
            }

            if (escrowFee != null)
            {
                CAgentFields beneficiaryContact = null;
                if (escrowFee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.loanData.GetAgentFields(escrowFee.BeneficiaryAgentId);
                }

                escrowItem.EscrowPaidTo = CreatePaidTo(beneficiaryContact, escrowFee.BeneficiaryDescription, escrowFee.BeneficiaryAgentId);
                escrowItem.EscrowItemPayments = this.CreateEscrowItemPayments(escrowFee.Payments, escrowFee.QmAmount > 0.00m);

                var identifiedProviders = this.loanData.sAvailableSettlementServiceProviders.GetProvidersForFeeTypeId(escrowFee.ClosingCostFeeTypeId);
                escrowItem.IdentifiedServiceProviders = CreateIdentifiedServiceProviders(identifiedProviders);
            }

            return escrowItem;
        }

        /// <summary>
        /// Creates an ESCROW_ITEM container with information on the mortgage insurance premium escrows.
        /// </summary>
        /// <returns>An ESCROW_ITEM container.</returns>
        private ESCROW_ITEM CreateEscrowItem_MI()
        {
            var escrowItem = new ESCROW_ITEM();
            var miEscrowFee = this.GetMortgageInsuranceEscrowFee();

            escrowItem.SequenceNumber = this.counters.EscrowItems;
            escrowItem.EscrowItemDetail = this.CreateEscrowItemDetail_MI(miEscrowFee);

            if (miEscrowFee != null)
            { 
                escrowItem.EscrowItemPayments = this.CreateEscrowItemPayments(miEscrowFee.Payments, miEscrowFee.QmAmount > 0.00m);
            }
            
            return escrowItem;
        }

        /// <summary>
        /// Creates a container with details on an escrowed expense.
        /// </summary>
        /// <param name="expense">The escrowed expense.</param>
        /// <param name="escrowFee">The fee associated with the escrow.</param>
        /// <param name="archivedFee">The archived fee associated with the escrow.</param>
        /// <returns>An ESCROW_ITEM_DETAIL container.</returns>
        private ESCROW_ITEM_DETAIL CreateEscrowItemDetail(BaseHousingExpense expense, BorrowerClosingCostFee escrowFee, BorrowerClosingCostFee archivedFee)
        {
            var escrowItemDetail = new ESCROW_ITEM_DETAIL();
            escrowItemDetail.EscrowAnnualPaymentAmount = ToMismoAmount(expense.AnnualAmt_rep);
            escrowItemDetail.EscrowCollectedNumberOfMonthsCount = ToMismoCount(expense.ReserveMonths_rep, null);
            escrowItemDetail.EscrowItemTotalReserveCollectedAtClosingAmount = ToMismoAmount(expense.ReserveAmt_rep);

            if (expense.IsTax)
            {
                escrowItemDetail.EscrowItemType = ToEscrowItemEnum(expense.TaxType, expense.HousingExpenseType, expense.DescriptionOrDefault);
            }
            else
            {
                escrowItemDetail.EscrowItemType = ToEscrowItemEnum(expense.HousingExpenseType, expense.DescriptionOrDefault);
            }

            if (escrowItemDetail.EscrowItemType != null && escrowItemDetail.EscrowItemType.enumValue == EscrowItemBase.Other)
            {
                escrowItemDetail.EscrowItemTypeOtherDescription = ToMismoString(expense.DescriptionOrDefault);
            }

            escrowItemDetail.EscrowMonthlyPaymentAmount = ToMismoAmount(expense.MonthlyAmtServicing_rep);
            escrowItemDetail.EscrowPaymentFrequencyType = ToEscrowPaymentFrequencyEnum(expense.DisbursementRepInterval);

            if (escrowItemDetail.EscrowPaymentFrequencyType != null && escrowItemDetail.EscrowPaymentFrequencyType.enumValue == EscrowPaymentFrequencyBase.Other)
            {
                escrowItemDetail.EscrowPaymentFrequencyTypeOtherDescription = ToMismoString(GetXmlEnumName(expense.DisbursementRepInterval));
            }

            escrowItemDetail.EscrowPremiumDurationMonthsCount = ToMismoCount(expense.PrepaidMonths_rep, null);
            escrowItemDetail.EscrowPremiumRatePercent = ToMismoPercent(expense.AnnualAmtCalcBasePerc_rep);
            escrowItemDetail.EscrowPremiumRatePercentBasisType = ToEscrowPremiumRatePercentBasisEnum(expense.AnnualAmtCalcBaseType);

            if (escrowItemDetail.EscrowPremiumRatePercentBasisType != null && escrowItemDetail.EscrowPremiumRatePercentBasisType.enumValue == EscrowPremiumRatePercentBasisBase.Other)
            {
                escrowItemDetail.EscrowPremiumRatePercentBasisTypeOtherDescription = ToMismoString(GetXmlEnumName(expense.AnnualAmtCalcBaseType));
            }

            escrowItemDetail.EscrowItemEstimatedTotalAmount = ToMismoAmount(archivedFee?.TotalAmount_rep);
            if (escrowFee != null)
            {
                if (escrowFee.Payments != null && escrowFee.Payments.Count(p => p != null && p.Amount > 0.00m) > 0)
                {
                    var payment = escrowFee.Payments.First(p => p != null && p.Amount > 0.00m);

                    escrowItemDetail.EscrowPaidByType = ToEscrowPaidByEnum(payment.GetPaidByType(this.loanData.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage));
                    escrowItemDetail.EscrowPremiumPaidByType = ToEscrowPremiumPaidByEnum(payment.GetPaidByType(this.loanData.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage));
                    escrowItemDetail.EscrowPremiumPaymentType = ToEscrowPremiumPaymentEnum(payment.GfeClosingCostFeePaymentTimingT);
                }

                escrowItemDetail.EscrowSpecifiedHUD1LineNumberValue = ToMismoValue(escrowFee.HudLine_rep);

                MISMOString paidToTypeOtherDescription = null;
                CAgentFields beneficiaryContact = null;
                if (escrowFee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.loanData.GetAgentFields(escrowFee.BeneficiaryAgentId);
                }

                escrowItemDetail.FeePaidToType = GetFeePaidToType(
                    beneficiaryContact, 
                    escrowFee.IsAffiliate, 
                    escrowFee.IsThirdParty, 
                    escrowFee.BeneficiaryType, 
                    escrowFee.Beneficiary, 
                    escrowFee.BeneficiaryDescription, 
                    out paidToTypeOtherDescription);

                if (paidToTypeOtherDescription != null)
                {
                    escrowItemDetail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
                }
            }

            var feeTypeId = escrowFee == null ? Guid.Empty : escrowFee.ClosingCostFeeTypeId;
            escrowItemDetail.Extension = this.CreateEscrowItemDetailExtension(expense, feeTypeId);

            return escrowItemDetail;
        }

        /// <summary>
        /// Creates an extension container with further details on an escrow item.
        /// </summary>
        /// <param name="expense">The expense object.</param>
        /// <param name="feeTypeId">The unique closing cost fee type associated with the escrow.</param>
        /// <returns>An ESCROW_ITEM_DETAIL container.</returns>
        private ESCROW_ITEM_DETAIL_EXTENSION CreateEscrowItemDetailExtension(BaseHousingExpense expense, Guid feeTypeId)
        {
            var escrowItemDetailExtension = new ESCROW_ITEM_DETAIL_EXTENSION();

            if (expense != null)
            {
                escrowItemDetailExtension.Mismo = this.CreateMismoEscrowItemDetailExtension(expense);
            }

            if (feeTypeId != Guid.Empty)
            {
                escrowItemDetailExtension.Other = this.CreateLQBEscrowItemDetailExtension(feeTypeId);
            }

            return escrowItemDetailExtension;
        }

        /// <summary>
        /// Creates an extension to the ESCROW_ITEM_DETAIL container with MISMO datapoints.
        /// </summary>
        /// <param name="expense">The housing expense.</param>
        /// <returns>A MISMO_ESCROW_ITEM_DETAIL_EXTENSION container.</returns>
        private MISMO_ESCROW_ITEM_DETAIL_EXTENSION CreateMismoEscrowItemDetailExtension(BaseHousingExpense expense)
        {
            var extension = new MISMO_ESCROW_ITEM_DETAIL_EXTENSION();
            extension.EscrowDetailCushionNumberOfMonths = ToMismoCount(expense.CushionMonths_rep, null);

            return extension;
        }

        /// <summary>
        /// Creates an extension to the ESCROW_ITEM_DETAIL container with proprietary datapoints.
        /// </summary>
        /// <param name="feeTypeId">The unique closing cost fee type associated with the escrow.</param>
        /// <returns>An LQB_ESCROW_ITEM_DETAIL_EXTENSION container.</returns>
        private LQB_ESCROW_ITEM_DETAIL_EXTENSION CreateLQBEscrowItemDetailExtension(Guid feeTypeId)
        {
            var extension = new LQB_ESCROW_ITEM_DETAIL_EXTENSION();

            if (feeTypeId != Guid.Empty)
            {
                extension.ClosingCostFeeTypeID = ToMismoString(feeTypeId.ToString());
            }

            return extension;
        }

        /// <summary>
        /// Creates a container with details on escrowed mortgage insurance premiums.
        /// </summary>
        /// <param name="miEscrowFee">The closing cost fee associated with the MI escrow row.</param>
        /// <returns>An ESCROW_ITEM_DETAIL container.</returns>
        private ESCROW_ITEM_DETAIL CreateEscrowItemDetail_MI(BorrowerClosingCostFee miEscrowFee)
        {
            var escrowItemDetail = new ESCROW_ITEM_DETAIL();
            escrowItemDetail.EscrowCollectedNumberOfMonthsCount = ToMismoCount(this.loanData.sMInsRsrvMon_rep, null);
            escrowItemDetail.EscrowItemTotalReserveCollectedAtClosingAmount = ToMismoAmount(this.loanData.sMInsRsrv_rep);
            escrowItemDetail.EscrowItemType = ToEscrowItemEnum(EscrowItemBase.MortgageInsurance, displayLabelText: "Mortgage Insurance");
            escrowItemDetail.EscrowMonthlyPaymentAmount = ToMismoAmount(this.loanData.sProMIns_rep);
            escrowItemDetail.EscrowPaymentFrequencyType = ToEscrowPaymentFrequencyEnum(E_DisbursementRepIntervalT.Monthly);
            escrowItemDetail.EscrowPremiumAmount = ToMismoAmount(this.loanData.sProMIns_rep);
            escrowItemDetail.EscrowPremiumRatePercent = ToMismoPercent(this.loanData.sProMInsR_rep);
            escrowItemDetail.EscrowPremiumRatePercentBasisType = ToEscrowPremiumRatePercentBasisEnum(this.loanData.sProMInsT);

            if (escrowItemDetail.EscrowPremiumRatePercentBasisType != null && escrowItemDetail.EscrowPremiumRatePercentBasisType.enumValue == EscrowPremiumRatePercentBasisBase.Other)
            {
                escrowItemDetail.EscrowPremiumRatePercentBasisTypeOtherDescription = ToMismoString(GetXmlEnumName(this.loanData.sProMInsT));
            }

            if (miEscrowFee != null)
            {
                if (miEscrowFee.Payments != null && miEscrowFee.Payments.Count(p => p != null && p.Amount > 0.00m) > 0)
                {
                    var payment = miEscrowFee.Payments.First(p => p != null && p.Amount > 0.00m);

                    escrowItemDetail.EscrowPaidByType = ToEscrowPaidByEnum(payment.GetPaidByType(this.loanData.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage));
                    escrowItemDetail.EscrowPremiumPaidByType = ToEscrowPremiumPaidByEnum(payment.GetPaidByType(this.loanData.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage));
                    escrowItemDetail.EscrowPremiumPaymentType = ToEscrowPremiumPaymentEnum(payment.GfeClosingCostFeePaymentTimingT);
                }
                
                escrowItemDetail.EscrowSpecifiedHUD1LineNumberValue = ToMismoValue(miEscrowFee.HudLine_rep);

                MISMOString paidToTypeOtherDescription = null;
                CAgentFields beneficiaryContact = null;
                if (miEscrowFee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.loanData.GetAgentFields(miEscrowFee.BeneficiaryAgentId);
                }

                escrowItemDetail.FeePaidToType = GetFeePaidToType(
                    beneficiaryContact,
                    miEscrowFee.IsAffiliate,
                    miEscrowFee.IsThirdParty, 
                    miEscrowFee.BeneficiaryType, 
                    miEscrowFee.Beneficiary, 
                    miEscrowFee.BeneficiaryDescription, 
                    out paidToTypeOtherDescription);

                if (paidToTypeOtherDescription != null)
                {
                    escrowItemDetail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
                }
            }

            var feeTypeId = miEscrowFee == null ? Guid.Empty : miEscrowFee.ClosingCostFeeTypeId;
            escrowItemDetail.Extension = this.CreateEscrowItemDetailExtension(null, feeTypeId);

            return escrowItemDetail;
        }

        /// <summary>
        /// Creates a container holding disbursements for an escrowed expense.
        /// </summary>
        /// <param name="payments">A list of projected or actual payments for the expense.</param>
        /// <param name="forceExportDisbursements">
        /// Normally disbursements will only be exported if they are to be paid by the escrow account.
        /// This can be overridden by setting this parameter to true.
        /// </param>
        /// <returns>An ESCROW_ITEM_DISBURSEMENTS container.</returns>
        private ESCROW_ITEM_DISBURSEMENTS CreateEscrowItemDisbursements(ICollection<SingleDisbursement> payments, bool forceExportDisbursements = false)
        {
            var escrowItemDisbursements = new ESCROW_ITEM_DISBURSEMENTS();

            foreach (SingleDisbursement payment in payments.OrderBy(p => p.DueDate))
            {
                if (forceExportDisbursements || payment.PaidBy == E_DisbursementPaidByT.EscrowImpounds)
                {
                    escrowItemDisbursements.EscrowItemDisbursement.Add(this.CreateEscrowItemDisbursement(payment, escrowItemDisbursements.EscrowItemDisbursement.Count(e => e != null) + 1));
                }
            }

            return escrowItemDisbursements.EscrowItemDisbursement.Count(e => e != null) > 0 ? escrowItemDisbursements : null;
        }

        /// <summary>
        /// Creates a container holding data on a single disbursement for an escrowed expense.
        /// </summary>
        /// <param name="payment">A projected or actual payment for the expense.</param>
        /// <param name="sequence">The sequence number of the disbursement among the list of disbursements.</param>
        /// <returns>An ESCROW_ITEM_DISBURSEMENT container.</returns>
        private ESCROW_ITEM_DISBURSEMENT CreateEscrowItemDisbursement(SingleDisbursement payment, int sequence)
        {
            var escrowItemDisbursement = new ESCROW_ITEM_DISBURSEMENT();
            escrowItemDisbursement.EscrowItemDisbursementDate = ToMismoDate(payment.DueDate_rep);
            escrowItemDisbursement.EscrowItemDisbursementAmount = ToMismoAmount(payment.DisbursementAmt_rep);
            escrowItemDisbursement.EscrowItemDisbursementTermMonthsCount = ToMismoCount(payment.CoveredMonths_rep, null);
            escrowItemDisbursement.EscrowItemDisbursedDate = ToMismoDate(payment.PaidDate_rep);
            escrowItemDisbursement.SequenceNumber = sequence;
            
            return escrowItemDisbursement;
        }

        /// <summary>
        /// Creates a container holding a list of payments for an escrowed expense.
        /// </summary>
        /// <param name="payments">The set of payments for an escrow.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <returns>An ESCROW_ITEM_PAYMENTS container.</returns>
        private ESCROW_ITEM_PAYMENTS CreateEscrowItemPayments(IEnumerable<LoanClosingCostFeePayment> payments, bool qm)
        {
            var escrowItemPayments = new ESCROW_ITEM_PAYMENTS();

            foreach (BorrowerClosingCostFeePayment payment in payments.Where(p => p.Amount > 0.00m))
            {
                escrowItemPayments.EscrowItemPayment.Add(this.CreateEscrowItemPayment(payment, qm, escrowItemPayments.EscrowItemPayment.Count(p => p != null) + 1));
            }

            return escrowItemPayments;
        }

        /// <summary>
        /// Creates a container holding data on a specific payment for an escrowed expense.
        /// </summary>
        /// <param name="payment">An escrow payment.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <param name="sequence">The sequence number of the payment among the list of payments.</param>
        /// <returns>An ESCROW_ITEM_PAYMENT container.</returns>
        private ESCROW_ITEM_PAYMENT CreateEscrowItemPayment(BorrowerClosingCostFeePayment payment, bool qm, int sequence)
        {
            var escrowItemPayment = new ESCROW_ITEM_PAYMENT();

            escrowItemPayment.EscrowItemActualPaymentAmount = ToMismoAmount(payment.Amount_rep);
            escrowItemPayment.EscrowItemPaymentPaidByType = ToEscrowItemPaymentPaidByEnum(payment.GetPaidByType(this.loanData.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage));

            if (escrowItemPayment.EscrowItemPaymentPaidByType != null && escrowItemPayment.EscrowItemPaymentPaidByType.enumValue == EscrowItemPaymentPaidByBase.Other)
            {
                escrowItemPayment.EscrowItemPaymentPaidByTypeOtherDescription = ToMismoString(GetXmlEnumName(payment.GetPaidByType(this.loanData.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage)));
            }

            escrowItemPayment.EscrowItemPaymentTimingType = ToEscrowItemPaymentTimingEnum(payment.GfeClosingCostFeePaymentTimingT);

            if (escrowItemPayment.EscrowItemPaymentTimingType != null && escrowItemPayment.EscrowItemPaymentTimingType.enumValue == EscrowItemPaymentTimingBase.Other)
            {
                escrowItemPayment.EscrowItemPaymentTimingTypeOtherDescription = ToMismoString(GetXmlEnumName(payment.GfeClosingCostFeePaymentTimingT));
            }

            escrowItemPayment.PaymentFinancedIndicator = ToMismoIndicator(payment.IsFinanced);
            escrowItemPayment.PaymentIncludedInAPRIndicator = ToMismoIndicator(payment.IsIncludedInApr(this.loanData.GetLiveLoanVersion()));
            escrowItemPayment.RegulationZPointsAndFeesIndicator = ToMismoIndicator(qm);
            escrowItemPayment.Extension = this.CreateEscrowItemPaymentExtension(payment.Id.ToString());
            escrowItemPayment.SequenceNumber = sequence;

            return escrowItemPayment;
        }

        /// <summary>
        /// Creates an extension container holding data on an escrow item payment.
        /// </summary>
        /// <param name="paymentId">The escrow item payment ID.</param>
        /// <returns>A serializable <see cref="ESCROW_ITEM_PAYMENT_EXTENSION"/> container.</returns>
        private ESCROW_ITEM_PAYMENT_EXTENSION CreateEscrowItemPaymentExtension(string paymentId)
        {
            var extension = new ESCROW_ITEM_PAYMENT_EXTENSION();
            extension.Other = this.CreateLqbEscrowItemPaymentExtension(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container holding data on an escrow item payment.
        /// </summary>
        /// <param name="paymentId">The escrow item payment ID.</param>
        /// <returns>A serializable <see cref="LQB_ESCROW_ITEM_PAYMENT_EXTENSION"/> container.</returns>
        private LQB_ESCROW_ITEM_PAYMENT_EXTENSION CreateLqbEscrowItemPaymentExtension(string paymentId)
        {
            var extension = new LQB_ESCROW_ITEM_PAYMENT_EXTENSION();
            extension.Id = ToMismoString(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST container with a set of property costs from the archived disclosure given by the document type.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST container with a list of property costs.</returns>
        private ESTIMATED_PROPERTY_COST CreateEstimatedPropertyCost(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var propertyCost = new ESTIMATED_PROPERTY_COST();
            propertyCost.EstimatedPropertyCostComponents = this.CreateEstimatedPropertyCostComponents(documentType);
            propertyCost.EstimatedPropertyCostDetail = this.CreateEstimatedPropertyCostDetail(documentType);

            return propertyCost;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST_COMPONENT container with information pertaining to the given housing expense/cost.
        /// </summary>
        /// <param name="costType">The type of expense/cost.</param>
        /// <param name="escrowed">True if the cost is escrowed. Otherwise false.</param>
        /// <param name="description">A description of the cost.</param>
        /// <param name="annualAmount_rep">The annual amount of the cost (as a string).</param>
        /// <param name="sequenceNumber">The sequence number for the cost.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST_COMPONENT container with information pertaining to the given housing expense/cost.</returns>
        private ESTIMATED_PROPERTY_COST_COMPONENT CreateEstimatedPropertyCostComponent(E_HousingExpenseTypeT costType, bool escrowed, string description, string annualAmount_rep, int sequenceNumber)
        {
            var component = new ESTIMATED_PROPERTY_COST_COMPONENT();
            component.ProjectedPaymentEscrowedType = ToProjectedPaymentEscrowedEnum(escrowed);
            component.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType = ToProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentEnum(costType);

            if (component.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType.enumValue == ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.Other)
            {
                component.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription = ToMismoString(description);
            }

            component.SequenceNumber = sequenceNumber;

            component.Extension = this.CreateEstimatedPropertyCostComponentExtension(annualAmount_rep, description);

            return component;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container.
        /// </summary>
        /// <param name="annualAmount_rep">The annual amount of the cost (as a string).</param>
        /// <param name="description">The description of the cost.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container.</returns>
        private ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION CreateEstimatedPropertyCostComponentExtension(string annualAmount_rep, string description)
        {
            var extension = new ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION();

            extension.Other = this.CreateLQBEstimatedPropertyCostComponentExtension(annualAmount_rep, description);

            return extension;
        }

        /// <summary>
        /// Creates an LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container to contain extra information.
        /// </summary>
        /// <param name="annualAmount_rep">The annual amount of the cost.</param>
        /// <param name="description">The description of the cost.</param>
        /// <returns>An LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container.</returns>
        private LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION CreateLQBEstimatedPropertyCostComponentExtension(string annualAmount_rep, string description)
        {
            var lqbExtension = new LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION();

            lqbExtension.ExpenseDescription = ToMismoString(description);

            lqbExtension.AnnualAmount = ToMismoAmount(annualAmount_rep);

            return lqbExtension;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST_COMPONENTS container with a set of cost components from the archived disclosure given by the document type.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST_COMPONENTS container with a set of cost components.</returns>
        private ESTIMATED_PROPERTY_COST_COMPONENTS CreateEstimatedPropertyCostComponents(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var propertyCostComponents = new ESTIMATED_PROPERTY_COST_COMPONENTS();

            int sequence = 0;
            foreach (BaseHousingExpense expense in (documentType == IntegratedDisclosureDocumentBase.LoanEstimate ? this.loanEstimateArchiveData : this.closingDisclosureArchiveData).sHousingExpenses.ExpensesToUse)
            {
                if (expense.MonthlyAmtTotal != 0)
                {
                    propertyCostComponents.EstimatedPropertyCostComponent.Add(
                        this.CreateEstimatedPropertyCostComponent(
                        expense.HousingExpenseType,
                        expense.IsEscrowedAtClosing == E_TriState.Yes, 
                        expense.DefaultExpenseDescription,
                        expense.AnnualAmt_rep,
                        ++sequence));
                }
            }

            return propertyCostComponents;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST_DETAIL container with information regarding the property costs from the archived disclosure given by the document type. 
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST_DETAIL container with information regarding the archived property costs.</returns>
        private ESTIMATED_PROPERTY_COST_DETAIL CreateEstimatedPropertyCostDetail(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var propertyCostDetail = new ESTIMATED_PROPERTY_COST_DETAIL();
            propertyCostDetail.ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount = ToMismoAmount(this.loanData.sMonthlyNonPINonMIExpenseTotalPITI_rep);

            return propertyCostDetail;
        }

        /// <summary>
        /// Creates a container holding information on the loan execution.
        /// </summary>
        /// <returns>An EXECUTION container.</returns>
        private EXECUTION CreateExecution()
        {
            var execution = new EXECUTION();

            execution.Address = CreateAddress(
                this.loanData.sExecutionLocationAddress,
                this.loanData.sExecutionLocationCity,
                this.loanData.sExecutionLocationState,
                this.loanData.sExecutionLocationZip,
                this.loanData.sExecutionLocationCounty,
                string.Empty,
                string.Empty,
                AddressBase.Blank);
            execution.ExecutionDetail = this.CreateExecutionDetail();

            return execution;
        }

        /// <summary>
        /// Creates a container holding information on the details of the loan execution.
        /// </summary>
        /// <returns>An EXECUTION_DETAIL container.</returns>
        private EXECUTION_DETAIL CreateExecutionDetail()
        {
            var executionDetail = new EXECUTION_DETAIL();
            executionDetail.ExecutionDate = ToMismoDate(this.loanData.sDocMagicClosingD_rep);
            return executionDetail;
        }

        /// <summary>
        /// Creates a container holding a list of exemptions.
        /// </summary>
        /// <returns>An EXEMPTIONS container.</returns>
        private EXEMPTIONS CreateExemptions()
        {
            var exemptions = new EXEMPTIONS();
            exemptions.Exemption.Add(this.CreateExemption());

            return exemptions;
        }

        /// <summary>
        /// Creates a container holding data on an exemption.
        /// </summary>
        /// <returns>An EXEMPTION container.</returns>
        private EXEMPTION CreateExemption()
        {
            var exemption = new EXEMPTION();
            exemption.AbilityToRepayExemptionCreditExtendedPurposeIndicator = ToMismoIndicator(this.loanData.sIsExemptFromAtr);
            exemption.SequenceNumber = 1;

            return exemption;
        }

        /// <summary>
        /// Creates EXPENSE container holding alimony information.
        /// </summary>
        /// <param name="alimony">Loan file's alimony object.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <returns>Returns an EXPENSE container holding alimony information.</returns>
        private EXPENSE CreateExpense(ILiabilityAlimony alimony, string borrowerLabel)
        {
            var expense = new EXPENSE();

            expense.AlimonyOwedToName = ToMismoString(alimony.OwedTo);
            expense.ExpenseMonthlyPaymentAmount = ToMismoAmount(alimony.Pmt_rep);
            expense.ExpenseRemainingTermMonthsCount = ToMismoCount(alimony.RemainMons_rep, null);
            expense.ExpenseType = ToExpenseEnum(ExpenseBase.Alimony);
            expense.SequenceNumber = this.counters.Expenses;
            expense.label = "Expense" + expense.SequenceNumberSerialized;

            this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "EXPENSE", "ROLE", expense.label, borrowerLabel));

            return expense;
        }

        /// <summary>
        /// Creates EXPENSE container holding child support information.
        /// </summary>
        /// <param name="childSupport">Loan file's child support object.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <returns>Returns an EXPENSE container holding child support information.</returns>
        private EXPENSE CreateExpense(ILiabilityChildSupport childSupport, string borrowerLabel)
        {
            var expense = new EXPENSE();

            expense.ExpenseDescription = ToMismoString(childSupport.OwedTo);
            expense.ExpenseMonthlyPaymentAmount = ToMismoAmount(childSupport.Pmt_rep);
            expense.ExpenseRemainingTermMonthsCount = ToMismoCount(childSupport.RemainMons_rep, null);
            expense.ExpenseType = ToExpenseEnum(ExpenseBase.ChildSupport);
            expense.SequenceNumber = this.counters.Expenses;
            expense.label = "Expense" + expense.SequenceNumberSerialized;

            this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "EXPENSE", "ROLE", expense.label, borrowerLabel));

            return expense;
        }

        /// <summary>
        /// Creates EXPENSE container holding job expense information.
        /// </summary>
        /// <param name="jobExpense">The job expense object.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <returns>Returns an EXPENSE container holding job expense information.</returns>
        private EXPENSE CreateExpense(ILiabilityJobExpense jobExpense, string borrowerLabel)
        {
            var expense = new EXPENSE();

            expense.ExpenseMonthlyPaymentAmount = ToMismoAmount(jobExpense.Pmt_rep);
            expense.ExpenseType = ToExpenseEnum(ExpenseBase.JobRelatedExpenses);
            expense.ExpenseDescription = ToMismoString(jobExpense.ExpenseDesc);
            expense.SequenceNumber = this.counters.Expenses;
            expense.label = "Expense" + expense.SequenceNumberSerialized;

            this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "EXPENSE", "ROLE", expense.label, borrowerLabel));

            return expense;
        }

        /// <summary>
        /// Creates EXPENSES container holding alimony and job-related expenses for all applicants.
        /// </summary>
        /// <returns>Returns EXPENSES container holding alimony and job-related expenses for all applicants.</returns>
        private EXPENSES CreateExpenses()
        {
            EXPENSES expenses = new EXPENSES();
            var validApps = this.loanData.Apps.Where(app => app.aBIsValidNameSsn || app.aCIsValidNameSsn);
            foreach (CAppData appData in validApps)
            {
                ILiaCollection liabilityCollection = appData.aLiaCollection;
                ////Create Alimony expense
                var alimony = liabilityCollection.GetAlimony(false);
                if (alimony != null)
                {
                    expenses.Expense.Add(this.CreateExpense(alimony, appData.aBMismoId));
                }

                var childSupport = liabilityCollection.GetChildSupport(false);
                if (childSupport != null)
                {
                    expenses.Expense.Add(this.CreateExpense(childSupport, appData.aBMismoId));
                }

                ////Create job-related expenses
                ILiabilityJobExpense[] jobExpenses = { liabilityCollection.GetJobRelated1(false), liabilityCollection.GetJobRelated2(false) };
                expenses.Expense.AddRange(jobExpenses.Where(jobExpense => jobExpense != null).Select(jobExpense => this.CreateExpense(jobExpense, appData.aBMismoId)));
            }

            return expenses;
        }

        /// <summary>
        /// Creates a FIPS_INFORMATION container with Federal Information Processing Standards codes associated with the subject property.
        /// </summary>
        /// <returns>A FIPS_INFORMATION container with Federal Information Processing Standards codes associated with the subject property.</returns>
        private FIPS_INFORMATION CreateFipsInformation()
        {
            var info = new FIPS_INFORMATION();
            info.FIPSCountryCode = ToMismoCode(StateInfo.USACountryCode);
            info.FIPSCountyCode = ToMismoCode(this.loanData.sHmdaCountyCode);
            info.FIPSStateAlphaCode = ToMismoCode(this.loanData.sSpState);
            info.FIPSStateNumericCode = ToMismoCode(this.loanData.sHmdaStateCode);

            return info;
        }

        /// <summary>
        /// Creates a container holding Flood Determination information for subject property.
        /// </summary>
        /// <returns>Returns a container holding Flood Determination information for subject property.</returns>
        private FLOOD_DETERMINATION CreateFloodDetermination()
        {
            FLOOD_DETERMINATION floodDetermination = new FLOOD_DETERMINATION();
            floodDetermination.FloodDeterminationDetail = this.CreateFloodDeterminationDetail();
            return floodDetermination;
        }

        /// <summary>
        /// Creates a container holding details on a flood determination.
        /// </summary>
        /// <returns>A FLOOD_DETERMINATION_DETAIL container.</returns>
        private FLOOD_DETERMINATION_DETAIL CreateFloodDeterminationDetail()
        {
            var floodDeterminationDetail = new FLOOD_DETERMINATION_DETAIL();
            floodDeterminationDetail.NFIPCommunityIdentifier = ToMismoIdentifier(this.loanData.sFloodCertificationCommunityNum);
            floodDeterminationDetail.NFIPFloodZoneIdentifier = ToMismoIdentifier(this.loanData.sNfipFloodZoneId);
            floodDeterminationDetail.FloodCertificationIdentifier = ToMismoIdentifier(this.loanData.sFloodCertId);
            floodDeterminationDetail.FloodDeterminationLifeOfLoanIndicator = ToMismoIndicator(this.loanData.sFloodCertificationIsLOLUpgraded);
            floodDeterminationDetail.FloodPartialIndicator = ToMismoIndicator(this.loanData.sNfipIsPartialZone);
            floodDeterminationDetail.FloodProductCertifyDate = ToMismoDate(this.loanData.sFloodCertificationDeterminationD_rep);
            floodDeterminationDetail.NFIPCommunityName = ToMismoString(this.loanData.sFloodHazardCommunityDesc);
            floodDeterminationDetail.NFIPCommunityParticipationStatusType = ToNFIPCommunityParticipationStatusEnum(this.loanData.sFloodCertificationParticipationStatus);
            floodDeterminationDetail.NFIPMapIdentifier = ToMismoIdentifier(this.loanData.sFloodCertificationMapNum + this.loanData.sFloodCertificationPanelNums + this.loanData.sFloodCertificationPanelSuffix);
            floodDeterminationDetail.NFIPMapPanelDate = ToMismoDate(this.loanData.sFloodCertificationMapD_rep);
            floodDeterminationDetail.NFIPMapPanelIdentifier = ToMismoIdentifier(this.loanData.sFloodCertificationPanelNums);
            floodDeterminationDetail.SpecialFloodHazardAreaIndicator = ToMismoIndicator(this.loanData.sFloodCertificationIsInSpecialArea);

            var floodInsuranceExpense = this.loanData.sHousingExpenses.GetExpense(E_HousingExpenseTypeT.FloodInsurance);
            if (floodInsuranceExpense != null)
            {
                floodDeterminationDetail.NFIPCommunityParticipationStartDate = ToMismoDate(floodInsuranceExpense.PolicyActivationD_rep);
            }

            return floodDeterminationDetail;
        }

        /// <summary>
        /// Creates a container holding a set of foreclosures.
        /// </summary>
        /// <returns>A FORECLOSURES container.</returns>
        private FORECLOSURES CreateForeclosures()
        {
            var foreclosures = new FORECLOSURES();
            foreclosures.Foreclosure.Add(this.CreateForeclosure());

            return foreclosures;
        }

        /// <summary>
        /// Creates a container representing a foreclosure.
        /// </summary>
        /// <returns>A FORECLOSURE container.</returns>
        private FORECLOSURE CreateForeclosure()
        {
            var foreclosure = new FORECLOSURE();
            foreclosure.SequenceNumber = 1;
            foreclosure.ForeclosureDetail = this.CreateForeclosureDetail();

            return foreclosure;
        }

        /// <summary>
        /// Creates a container holding details on a foreclosure.
        /// </summary>
        /// <returns>A FORECLOSURE_DETAIL container.</returns>
        private FORECLOSURE_DETAIL CreateForeclosureDetail()
        {
            var foreclosureDetail = new FORECLOSURE_DETAIL();

            foreclosureDetail.DeficiencyRightsPreservedIndicator = ToMismoIndicator(!this.loanData.sTRIDPropertyIsInNonRecourseLoanState);

            return foreclosureDetail;
        }

        /// <summary>
        /// Creates a container holding a general identifier.
        /// </summary>
        /// <returns>A GENERAL_IDENTIFIER container.</returns>
        private GENERAL_IDENTIFIER CreateGeneralIdentifier()
        {
            var generalIdentifier = new GENERAL_IDENTIFIER();
            generalIdentifier.MSAIdentifier = ToMismoIdentifier(this.loanData.sHmdaMsaNum);

            return generalIdentifier;
        }

        /// <summary>
        /// Creates a container representing the Good Faith Estimate document.
        /// </summary>
        /// <returns>A GFE container.</returns>
        private GFE CreateGfe()
        {
            var gfe = new GFE();
            gfe.GfeDetail = this.CreateGfeDetail();
            gfe.GfeSectionSummaries = this.CreateGfeSectionSummaries();

            return gfe;
        }

        /// <summary>
        /// Creates a container holding details on the Good Faith Estimate.
        /// </summary>
        /// <returns>A GFE_DETAIL container.</returns>
        private GFE_DETAIL CreateGfeDetail()
        {
            var gfeDetail = new GFE_DETAIL();
            gfeDetail.GFEDisclosureDate = ToMismoDate(this.loanData.sGfeTilPrepareDate_rep);
            gfeDetail.GFEInterestRateAvailableThroughDate = ToMismoDate(this.loanData.sIsRateLocked ? this.loanData.sRLckdExpiredD_rep : this.loanData.sGfeNoteIRAvailTillD_Date);
            gfeDetail.GFELoanOriginatorFeePaymentCreditType = ToGfeLoanOriginatorFeePaymentCreditEnum(this.loanData.sGFELoanOriginatorFeePaymentCreditType);
            gfeDetail.GFERateLockMinimumDaysPriorToSettlementCount = ToMismoCount(this.loanData.sGfeLockPeriodBeforeSettlement_rep, null);
            gfeDetail.GFERateLockPeriodDaysCount = ToMismoCount(this.loanData.sGfeRateLockPeriod_rep, null);
            gfeDetail.GFESettlementChargesAvailableThroughDate = ToMismoDate(this.loanData.sGfeEstScAvailTillD_Date);

            return gfeDetail;
        }

        /// <summary>
        /// Creates a container summarizing the amount of a particular GFE section.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="section">The GFE section.</param>
        /// <param name="sequence">The sequence number of the section summary among the list of summaries.</param>
        /// <returns>A GFE_SECTION_SUMMARY container.</returns>
        private GFE_SECTION_SUMMARY CreateGfeSectionSummary(string amount, GFESectionIdentifierBase section, int sequence)
        {
            var gfeSectionSummary = new GFE_SECTION_SUMMARY();
            gfeSectionSummary.GFESectionDisclosedTotalAmount = ToMismoAmount(amount);
            gfeSectionSummary.GFESectionIdentifierType = ToGfeSectionIdentifierEnum(section);
            gfeSectionSummary.SequenceNumber = sequence;

            return gfeSectionSummary;
        }

        /// <summary>
        /// Creates a container holding summaries of various GFE sections.
        /// </summary>
        /// <returns>A GFE_SECTION_SUMMARIES container.</returns>
        private GFE_SECTION_SUMMARIES CreateGfeSectionSummaries()
        {
            var gfeSectionSummaries = new GFE_SECTION_SUMMARIES();
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeAdjOriginationCharge_rep, GFESectionIdentifierBase.A, 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeTotalEstimateSettlementCharge_rep, GFESectionIdentifierBase.AB, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeTotalOtherSettlementServiceFee_rep, GFESectionIdentifierBase.B, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeOriginationF_rep, GFESectionIdentifierBase.One, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sLDiscnt_rep, GFESectionIdentifierBase.Two, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeRequiredServicesTotalFee_rep, GFESectionIdentifierBase.Three, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeLenderTitleTotalFee_rep, GFESectionIdentifierBase.Four, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeOwnerTitleTotalFee_rep, GFESectionIdentifierBase.Five, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeServicesYouShopTotalFee_rep, GFESectionIdentifierBase.Six, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeGovtRecTotalFee_rep, GFESectionIdentifierBase.Seven, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeTransferTaxTotalFee_rep, GFESectionIdentifierBase.Eight, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeInitialImpoundDeposit_rep, GFESectionIdentifierBase.Nine, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeDailyInterestTotalFee_rep, GFESectionIdentifierBase.Ten, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeHomeOwnerInsuranceTotalFee_rep, GFESectionIdentifierBase.Eleven, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            gfeSectionSummaries.GfeSectionSummary.Add(this.CreateGfeSectionSummary(this.loanData.sGfeNotApplicableF_rep, GFESectionIdentifierBase.Other, gfeSectionSummaries.GfeSectionSummary.Count(g => g != null) + 1));
            
            return gfeSectionSummaries;
        }

        /// <summary>
        /// Creates a container holding data about a borrower who is taking an FHA or VA loan.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A GOVERNMENT_BORROWER container.</returns>
        private GOVERNMENT_BORROWER CreateGovernmentBorrower(CAppData appData)
        {
            if (this.loanData.sLT != E_sLT.FHA && this.loanData.sLT != E_sLT.VA)
            {
                return null;
            }

            var governmentBorrower = new GOVERNMENT_BORROWER();
            governmentBorrower.CAIVRSIdentifier = ToMismoIdentifier(appData.BorrowerModeT == E_BorrowerModeT.Borrower ? appData.aFHABCaivrsNum : appData.aFHACCaivrsNum);

            // FHABorrowerCertification... used for both FHA and VA
            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower && (this.loanData.sLT == E_sLT.FHA || this.loanData.sLT == E_sLT.VA))
            {
                governmentBorrower.FHABorrowerCertificationOriginalMortgageAmount = ToMismoAmount(appData.aFHABorrCertOtherPropOrigMAmt_rep);
                governmentBorrower.FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator = ToMismoIndicator(appData.aFHABorrCertOwnMoreThan4DwellingsTri);
                governmentBorrower.FHABorrowerCertificationOwnOtherPropertyIndicator = ToMismoIndicator(appData.aFHABorrCertOwnOrSoldOtherFHAPropTri);
                governmentBorrower.FHABorrowerCertificationPropertySoldCityName = ToMismoString(appData.aFHABorrCertOtherPropCity);
                governmentBorrower.FHABorrowerCertificationPropertySoldPostalCode = ToMismoCode(appData.aFHABorrCertOtherPropZip);
                governmentBorrower.FHABorrowerCertificationPropertySoldStateName = ToMismoString(appData.aFHABorrCertOtherPropState);
                governmentBorrower.FHABorrowerCertificationPropertySoldStreetAddressLineText = ToMismoString(appData.aFHABorrCertOtherPropStAddr);
                governmentBorrower.FHABorrowerCertificationPropertyToBeSoldIndicator = ToMismoIndicator(appData.aFHABorrCertOtherPropToBeSoldTri);
                governmentBorrower.FHABorrowerCertificationSalesPriceAmount = ToMismoAmount(appData.aFHABorrCertOtherPropSalesPrice_rep);
                governmentBorrower.FHABorrowerCertificationLeadPaintIndicator = ToMismoIndicator(appData.aFHABorrCertReceivedLeadPaintPoisonInfoTri);
                governmentBorrower.FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType = ToFHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueEnum(appData.aFHABorrCertInformedPropValAwareAtContractSigning, appData.aFHABorrCertInformedPropValNotAwareAtContractSigning);
            }

            // VA only
            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower && this.loanData.sLT == E_sLT.VA)
            {
                governmentBorrower.VABorrowerCertificationOccupancyType = ToVABorrowerCertificationOccupancyEnum(appData.aFHABorrCertOccIsAsHome, appData.aFHABorrCertOccIsAsHomeForActiveSpouse, appData.aFHABorrCertOccIsAsHomePrev, appData.aFHABorrCertOccIsAsHomePrevForActiveSpouse);
                governmentBorrower.VACoBorrowerNonTaxableIncomeAmount = ToMismoAmount(appData.aVaCONetI_rep);
                governmentBorrower.VAFederalTaxAmount = ToMismoAmount(this.loanData.m_convertLos.ToMoneyString(appData.aVaBFedITax + appData.aVaCFedITax, FormatDirection.ToRep));
                governmentBorrower.VALocalTaxAmount = ToMismoAmount(this.loanData.m_convertLos.ToMoneyString(appData.aVaBOITax + appData.aVaCOITax, FormatDirection.ToRep));
                governmentBorrower.VAPrimaryBorrowerNonTaxableIncomeAmount = ToMismoAmount(appData.aVaBONetI_rep);
                governmentBorrower.VASocialSecurityTaxAmount = ToMismoAmount(this.loanData.m_convertLos.ToMoneyString(appData.aVaBSsnTax + appData.aVaCSsnTax, FormatDirection.ToRep));
                governmentBorrower.VAStateTaxAmount = ToMismoAmount(this.loanData.m_convertLos.ToMoneyString(appData.aVaBStateITax + appData.aVaCStateITax, FormatDirection.ToRep));
            }

            return governmentBorrower;
        }

        /// <summary>
        /// Creates a container holding information on a VA or FHA loan.
        /// </summary>
        /// <returns>A GOVERNMENT_LOAN container.</returns>
        private GOVERNMENT_LOAN CreateGovernmentLoan()
        {
            if (this.loanData.sLT != E_sLT.FHA && this.loanData.sLT != E_sLT.VA)
            {
                return null;
            }

            var governmentLoan = new GOVERNMENT_LOAN();

            if (this.loanData.sLT == E_sLT.FHA)
            {
                governmentLoan.FHA_MIPremiumRefundAmount = ToMismoAmount(this.loanData.sFHA203kFHAMipRefund_rep);
                governmentLoan.FHAAssignmentDate = ToMismoDate(this.loanData.sCaseAssignmentD_rep);
                governmentLoan.FHACoverageRenewalRatePercent = ToMismoPercent(this.loanData.sProMInsR_rep);
                governmentLoan.FHAEnergyRelatedRepairsOrImprovementsAmount = ToMismoAmount(this.loanData.sFHAEnergyEffImprov_rep);
                governmentLoan.FHAUpfrontPremiumAmount = ToMismoAmount(this.loanData.sFfUfmip1003_rep);
                governmentLoan.FHAUpfrontPremiumPercent = ToMismoPercent(this.loanData.sFfUfmipR_rep);
                governmentLoan.FHALoanSponsorIdentifier = ToMismoIdentifier(this.loanData.sFHASponsorAgentIdCode);
                governmentLoan.FHALoanLenderIdentifier = ToMismoIdentifier(this.loanData.sFHALenderIdCode);
                governmentLoan.SectionOfActType = ToSectionOfActEnum(this.loanData.sFHAHousingActSection);
                governmentLoan.FHAEndorsementDate = ToMismoDate(this.loanData.sFhaEndorsementD_rep);

                if (governmentLoan.SectionOfActType?.enumValue == SectionOfActBase.Other)
                {
                    governmentLoan.SectionOfActTypeOtherDescription = ToMismoString(this.loanData.sFHAHousingActSection);
                }
            }

            if (this.loanData.sLT == E_sLT.VA)
            {
                governmentLoan.BorrowerFundingFeePercent = ToMismoPercent(this.loanData.sFfUfmipR_rep);
                governmentLoan.VAEntitlementAmount = ToMismoAmount(this.primaryApp.aVaEntitleAmt_rep);
                governmentLoan.VAEntitlementIdentifier = ToMismoIdentifier(this.primaryApp.aVaEntitleCode);
                governmentLoan.VAMaintenanceExpenseMonthlyAmount = ToMismoAmount(this.loanData.sVaProMaintenancePmt_rep);
                governmentLoan.VAResidualIncomeAmount = ToMismoAmount(this.primaryApp.aVaFamilySupportBal_rep);
                governmentLoan.VAUtilityExpenseMonthlyAmount = ToMismoAmount(this.loanData.sVaProUtilityPmt_rep);
                governmentLoan.VAResidualIncomeGuidelineAmount = ToMismoAmount(this.primaryApp.aVaFamilySuportGuidelineAmt_rep);
                governmentLoan.FHALoanSponsorIdentifier = ToMismoIdentifier(this.loanData.sVASponsorAgentIdCode);
                governmentLoan.FHALoanLenderIdentifier = ToMismoIdentifier(this.loanData.sVALenderIdCode);

                var vestingTypes = new List<E_aVaVestTitleT>();
                foreach (var app in this.loanData.Apps)
                {
                    vestingTypes.Add(app.aVaVestTitleT);
                }

                governmentLoan.VATitleVestingType = ToVATitleVestingEnum(vestingTypes);

                if (this.loanData.sVaIsAutoProc)
                {
                    governmentLoan.VALoanProcedureType = ToVALoanProcedureEnum(VALoanProcedureBase.Automatic);
                }
                else if (this.loanData.sVaIsAutoIrrrlProc)
                {
                    governmentLoan.VALoanProcedureType = ToVALoanProcedureEnum(VALoanProcedureBase.AutomaticInterestRateReductionRefinanceLoan);
                }
                else if (this.loanData.sVaIsPriorApprovalProc)
                {
                    governmentLoan.VALoanProcedureType = ToVALoanProcedureEnum(VALoanProcedureBase.VAPriorApproval);
                }

                bool anyMarriedVeteransOnApplication = false;
                foreach (var app in this.loanData.Apps)
                {
                    if (app.aBMaritalStatT == E_aBMaritalStatT.Married && app.aCMaritalStatT == E_aCMaritalStatT.Married
                        && (app.aBIsVeteran || app.aCIsVeteran))
                    {
                        anyMarriedVeteransOnApplication = true;
                        break;
                    }
                }

                governmentLoan.VABorrowerCoBorrowerMarriedIndicator = ToMismoIndicator(anyMarriedVeteransOnApplication);

                if (governmentLoan.VATitleVestingType.enumValue == VATitleVestingBase.Other)
                {
                    governmentLoan.VATitleVestingTypeOtherDescription = ToMismoString(this.primaryApp.aVaVestTitleODesc);
                }
            }

            bool previousVAHomeLoanIndicatorVal = false;
            foreach (CAppData appdata in this.loanData.Apps)
            {
                if (appdata.aVaBorrCertHadVaLoanTri == E_TriState.Yes)
                {
                    previousVAHomeLoanIndicatorVal = true;
                }
            }

            governmentLoan.PreviousVAHomeLoanIndicator = ToMismoIndicator(previousVAHomeLoanIndicatorVal);
            governmentLoan.FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier = ToMismoIdentifier(this.loanData.sFHAAddendumUnderwriterChumsId);
            governmentLoan.GovernmentRefinanceType = ToGovernmentRefinanceEnum(this.loanData.sLPurposeT, this.loanData.sHasAppraisal);
            governmentLoan.Extension = this.CreateGovernmentLoanExtension();
            governmentLoan.VAAppraisalType = ToVaAppraisalBaseEnum(this.loanData.sVaApprT);

            return governmentLoan;
        }

        /// <summary>
        /// Creates a container holding extra information about a government loan.
        /// </summary>
        /// <returns>A <see cref="GOVERNMENT_LOAN_EXTENSION"/> container.</returns>
        private GOVERNMENT_LOAN_EXTENSION CreateGovernmentLoanExtension()
        {
            var extension = new GOVERNMENT_LOAN_EXTENSION();

            extension.Other = this.CreateLQBGovernmentLoanExtension();

            return extension;
        }

        /// <summary>
        /// Creates an LQB proprietary container with extra information about a government loan.
        /// </summary>
        /// <returns>A proprietary <see cref="LQB_GOVERNMENT_LOAN_EXTENSION"/>.</returns>
        private LQB_GOVERNMENT_LOAN_EXTENSION CreateLQBGovernmentLoanExtension()
        {
            var lqbExtension = new LQB_GOVERNMENT_LOAN_EXTENSION();

            lqbExtension.FHA_VALoanPurposeType = Mismo33RequestProvider.ToFHA_VALoanPurposeType(this.loanData);

            return lqbExtension;
        }

        /// <summary>
        /// Creates a container to hold all demographic data on a borrower.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A GOVERNMENT_MONITORING container.</returns>
        private GOVERNMENT_MONITORING CreateGovernmentMonitoring(CAppData appData)
        {
            var governmentMonitoring = new GOVERNMENT_MONITORING();
            governmentMonitoring.GovernmentMonitoringDetail = this.CreateGovernmentMonitoringDetail(appData);
            governmentMonitoring.HmdaRaces = this.CreateHmdaRaces(appData);

            governmentMonitoring.Extension = this.CreateGovernmentMonitoringExtension(appData);

            return governmentMonitoring;
        }

        /// <summary>
        /// Creates a container holding extended government monitoring information.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A GOVERNMENT_MONITORING_EXTENSION container.</returns>
        private GOVERNMENT_MONITORING_EXTENSION CreateGovernmentMonitoringExtension(CAppData appData)
        {
            var governmentMonitoringExtension = new GOVERNMENT_MONITORING_EXTENSION();
            governmentMonitoringExtension.Other = this.CreateLqbGovernmentMonitoringExtension(appData);

            return governmentMonitoringExtension;
        }

        /// <summary>
        /// Creates an extension container holding HMDA information.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>An LQB_GOVERNMENT_MONITORING_EXTENSION container.</returns>
        private LQB_GOVERNMENT_MONITORING_EXTENSION CreateLqbGovernmentMonitoringExtension(CAppData appData)
        {
            var lqbExtension = new LQB_GOVERNMENT_MONITORING_EXTENSION();

            lqbExtension.GenderType = ToHMDAGenderEnum(appData.aGender, appData.aInterviewMethodT);
            lqbExtension.HMDAGenderRefusalIndicator = ToHMDAGenderRefusalIndicator(appData.aGender);
            lqbExtension.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator = ToMismoIndicator(appData.aSexCollectedByObservationOrSurname);

            lqbExtension.HMDAEthnicities = this.CreateLqbHmdaEthnicities(appData);
            lqbExtension.HMDAEthnicityOrigins = this.CreateLqbHmdaEthnicityOrigins(appData);
            lqbExtension.HMDAEthnicityRefusalIndicator = ToMismoIndicator(appData.aDoesNotWishToProvideEthnicity);
            lqbExtension.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator = ToMismoIndicator(appData.aEthnicityCollectedByObservationOrSurname);

            lqbExtension.HMDARaces = this.CreateLqbHmdaRaces(appData);
            lqbExtension.HMDARaceRefusalIndicator = ToMismoIndicator(appData.aDoesNotWishToProvideRace);
            lqbExtension.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator = ToMismoIndicator(appData.aRaceCollectedByObservationOrSurname);

            lqbExtension.ApplicationTakenMethodType = ToApplicationTakenMethodEnum(appData.aInterviewMethodT);

            lqbExtension.DecisionCreditScore = this.CreateLqbDecisionCreditScore(appData.aDecisionCreditScore_rep, appData.aDecisionCreditSourceT, appData.aDecisionCreditModelName);

            return lqbExtension;
        }

        /// <summary>
        /// Creates a container holding a decision credit score.
        /// </summary>
        /// <param name="score">The credit score.</param>
        /// <param name="creditSource">The credit bureau.</param>
        /// <param name="modelName">The credit model name.</param>
        /// <returns>An LQB_DECISION_CREDIT_SCORE container.</returns>
        private LQB_DECISION_CREDIT_SCORE CreateLqbDecisionCreditScore(string score, E_aDecisionCreditSourceT creditSource, string modelName)
        {
            if (string.IsNullOrEmpty(score))
            {
                return null;
            }

            var decisionScore = new LQB_DECISION_CREDIT_SCORE();
            decisionScore.DecisionCreditScoreValue = ToMismoValue(score);
            decisionScore.DecisionCreditRepositorySourceType = ToCreditRepositorySourceEnum(creditSource);
            decisionScore.DecisionCreditModelName = ToMismoString(modelName);

            return decisionScore;
        }

    /// <summary>
    /// Creates an extension container holding data on a borrower's races.
    /// </summary>
    /// <param name="appData">The borrower data.</param>
    /// <returns>An LQB_HMDA_RACES container.</returns>
    private LQB_HMDA_RACES CreateLqbHmdaRaces(CAppData appData)
        {
            var hmdaRaces = new LQB_HMDA_RACES();
            List<HMDARaceBase> races = GetHmdaRaceBaseValues(appData);

            foreach (var race in races)
            {
                if (race == HMDARaceBase.Asian
                    || race == HMDARaceBase.NativeHawaiianOrOtherPacificIslander
                    || race == HMDARaceBase.AmericanIndianOrAlaskaNative)
                {
                    // These are handled separately.
                    continue;
                }

                hmdaRaces.HMDARace.Add(this.CreateLqbHmdaRace(race, appData, includeRaceType: true));
            }

            bool hasAsianRace = races.Contains(HMDARaceBase.Asian);
            var asianDesignations = GetHmdaRaceDesignationBaseValues_Asian(appData);
            if (hasAsianRace || asianDesignations.Any() || !string.IsNullOrEmpty(appData.aOtherAsianDescription))
            {
                hmdaRaces.HMDARace.Add(this.CreateLqbHmdaRace(HMDARaceBase.Asian, appData, hasAsianRace, asianDesignations, appData.aOtherAsianDescription));
            }

            bool hasPacificIslanderRace = races.Contains(HMDARaceBase.NativeHawaiianOrOtherPacificIslander);
            var pacificIslanderDesignations = GetHmdaRaceDesignationBaseValues_PacificIslander(appData);
            if (hasPacificIslanderRace || pacificIslanderDesignations.Any() || !string.IsNullOrEmpty(appData.aOtherPacificIslanderDescription))
            {
                hmdaRaces.HMDARace.Add(this.CreateLqbHmdaRace(HMDARaceBase.NativeHawaiianOrOtherPacificIslander, appData, hasPacificIslanderRace, pacificIslanderDesignations, appData.aOtherPacificIslanderDescription));
            }

            bool hasNativeAmericanRace = races.Contains(HMDARaceBase.AmericanIndianOrAlaskaNative);
            bool hasNativeAmericanDescription = !string.IsNullOrEmpty(appData.aOtherAmericanIndianDescription);
            if (hasNativeAmericanRace || hasNativeAmericanDescription)
            {
                hmdaRaces.HMDARace.Add(this.CreateLqbHmdaRace(HMDARaceBase.AmericanIndianOrAlaskaNative, appData, hasNativeAmericanRace));
            }

            return hmdaRaces;
        }

        /// <summary>
        /// Creates a container holding information on a borrower race.
        /// </summary>
        /// <param name="race">The race to populate.</param>
        /// <param name="appData">The borrower data.</param>
        /// <param name="includeRaceType">Indicates whether to include the HMDA_RACE_DETAIL element.</param>
        /// <param name="designations">Designations for the race, if applicable.</param>
        /// <param name="designationOtherDescription">The catchall description for any supplementary race designations.</param>
        /// <returns>An LQB_HMDA_RACE container.</returns>
        private LQB_HMDA_RACE CreateLqbHmdaRace(HMDARaceBase race, CAppData appData, bool includeRaceType, List<HMDARaceDesignationBase> designations = null, string designationOtherDescription = null)
        {
            var hmdaRace = new LQB_HMDA_RACE();

            if (includeRaceType || race == HMDARaceBase.AmericanIndianOrAlaskaNative)
            {
                hmdaRace.HMDARaceDetail = this.CreateLqbHmdaRaceDetail(race, appData, includeRaceType);
            }

            if ((designations != null && designations.Any()) || !string.IsNullOrEmpty(designationOtherDescription))
            {
                hmdaRace.HMDARaceDesignations = this.CreateLqbHmdaRaceDesignations(race, designations, designationOtherDescription);
            }

            return hmdaRace;
        }

        /// <summary>
        /// Creates a container with details on a borrower's race.
        /// </summary>
        /// <param name="race">The borrower's race.</param>
        /// <param name="appData">The borrower data.</param>
        /// <param name="includeRaceType">Indicates whether to include the race type datapoint.</param>
        /// <returns>An LQB_HMDA_RACE_DETAIL container.</returns>
        private LQB_HMDA_RACE_DETAIL CreateLqbHmdaRaceDetail(HMDARaceBase race, CAppData appData, bool includeRaceType)
        {
            var hmdaRaceDetail = new LQB_HMDA_RACE_DETAIL();

            if (includeRaceType)
            {
                hmdaRaceDetail.HMDARaceType = ToMismoEnum(race);
            }

            if (race == HMDARaceBase.AmericanIndianOrAlaskaNative && !string.IsNullOrEmpty(appData.aOtherAmericanIndianDescription))
            {
                hmdaRaceDetail.HMDARaceTypeAdditionalDescription = ToMismoString(appData.aOtherAmericanIndianDescription);
            }

            return hmdaRaceDetail;
        }

        /// <summary>
        /// Creates a container containing a borrower's race designations.
        /// </summary>
        /// <param name="race">The race to populate.</param>
        /// <param name="designationCollection">Designations for the race.</param>
        /// <param name="designationOtherDescription">The catchall description for any supplementary race designations.</param>
        /// <returns>An LQB_HMDA_RACE_DESIGNATIONS container.</returns>
        private LQB_HMDA_RACE_DESIGNATIONS CreateLqbHmdaRaceDesignations(HMDARaceBase race, List<HMDARaceDesignationBase> designationCollection, string designationOtherDescription)
        {
            var hmdaRaceDesignations = new LQB_HMDA_RACE_DESIGNATIONS();

            // Special handling for the race designation other descriptions, we should send them
            // regardless of whether there's an Other value to pair with.
            if (!string.IsNullOrEmpty(designationOtherDescription))
            {
                HMDARaceDesignationBase? designation = null;

                if (designationCollection.Contains(HMDARaceDesignationBase.OtherAsian))
                {
                    designation = HMDARaceDesignationBase.OtherAsian;
                    designationCollection.Remove(designation.Value);
                }

                if (designationCollection.Contains(HMDARaceDesignationBase.OtherPacificIslander))
                {
                    designation = HMDARaceDesignationBase.OtherPacificIslander;
                    designationCollection.Remove(designation.Value);
                }

                hmdaRaceDesignations.HMDARaceDesignation.Add(
                    this.CreateLqbHmdaRaceDesignation(race, designation, designationOtherDescription));
            }

            foreach (var designation in designationCollection)
            {
                hmdaRaceDesignations.HMDARaceDesignation.Add(this.CreateLqbHmdaRaceDesignation(race, designation));
            }

            return hmdaRaceDesignations;
        }

        /// <summary>
        /// Creates a container with information on a race designation.
        /// </summary>
        /// <param name="race">The borrower's race.</param>
        /// <param name="designation">The race designation.</param>
        /// <param name="otherDescription">An other description, if applicable.</param>
        /// <returns>An LQB_HMDA_RACE_DESIGNATION container.</returns>
        private LQB_HMDA_RACE_DESIGNATION CreateLqbHmdaRaceDesignation(HMDARaceBase race, HMDARaceDesignationBase? designation, string otherDescription = null)
        {
            var hmdaRaceDesignation = new LQB_HMDA_RACE_DESIGNATION();

            if (designation.HasValue)
            {
                hmdaRaceDesignation.HMDARaceDesignationType = ToMismoEnum(designation.Value);
            }

            if (!string.IsNullOrEmpty(otherDescription))
            {
                if (race == HMDARaceBase.Asian)
                {
                    hmdaRaceDesignation.HMDARaceDesignationTypeOtherAsianDescription = ToMismoString(otherDescription);
                }
                else if (race == HMDARaceBase.NativeHawaiianOrOtherPacificIslander)
                {
                    hmdaRaceDesignation.HMDARaceDesignationTypeOtherPacificIslanderDescription = ToMismoString(otherDescription);
                }
            }

            return hmdaRaceDesignation;
        }

        /// <summary>
        /// Creates a container holding a borrower's ethnicity origins.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>An LQB_HMDA_ETHNICITY_ORIGINS container.</returns>
        private LQB_HMDA_ETHNICITY_ORIGINS CreateLqbHmdaEthnicityOrigins(CAppData appData)
        {
            var hmdaEthnicityOrigins = new LQB_HMDA_ETHNICITY_ORIGINS();
            List<HMDAEthnicityOriginBase> originCollection = GetHmdaEthnicityOriginBaseValues(appData);

            if (!originCollection.Any() && string.IsNullOrEmpty(appData.aOtherHispanicOrLatinoDescription))
            {
                return null;
            }

            // Special handling for the other description, we should send it regardless of
            // whether the set of ethnicity origins contains Other.
            HMDAEthnicityOriginBase? otherOrigin = null;

            if (originCollection.Contains(HMDAEthnicityOriginBase.Other))
            {
                otherOrigin = HMDAEthnicityOriginBase.Other;
                originCollection.Remove(otherOrigin.Value);
            }

            hmdaEthnicityOrigins.HMDAEthnicityOrigin.Add(
                this.CreateLqbHmdaEthnicityOrigin(otherOrigin, appData.aOtherHispanicOrLatinoDescription));

            foreach (var origin in originCollection)
            {
                hmdaEthnicityOrigins.HMDAEthnicityOrigin.Add(this.CreateLqbHmdaEthnicityOrigin(origin));
            }

            return hmdaEthnicityOrigins;
        }

        /// <summary>
        /// Creates a container holding a borrower's ethnicity origin.
        /// </summary>
        /// <param name="origin">The ethnicity origin.</param>
        /// <param name="otherDescription">An other description, if applicable.</param>
        /// <returns>An LQB_HMDA_ETHNICITY_ORIGIN container.</returns>
        private LQB_HMDA_ETHNICITY_ORIGIN CreateLqbHmdaEthnicityOrigin(HMDAEthnicityOriginBase? origin, string otherDescription = null)
        {
            var hmdaEthnicityOrigin = new LQB_HMDA_ETHNICITY_ORIGIN();

            if (origin.HasValue)
            {
                hmdaEthnicityOrigin.HMDAEthnicityOriginType = ToMismoEnum(origin.Value);
            }

            if (!string.IsNullOrEmpty(otherDescription))
            {
                hmdaEthnicityOrigin.HMDAEthnicityOriginTypeOtherDescription = ToMismoString(otherDescription);
            }

            return hmdaEthnicityOrigin;
        }

        /// <summary>
        /// Creates a container holding a borrower's ethnicities.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>An LQB_HMDA_ETHNICITIES container.</returns>
        private LQB_HMDA_ETHNICITIES CreateLqbHmdaEthnicities(CAppData appData)
        {
            var hmdaEthnicities = new LQB_HMDA_ETHNICITIES();
            List<HMDAEthnicityBase> ethnicities = GetHmdaEthnicityBaseValues(appData);

            if (!ethnicities.Any())
            {
                return null;
            }

            foreach (var ethnicity in ethnicities)
            {
                hmdaEthnicities.HmdaEthnicity.Add(this.CreateLqbHmdaEthnicity(ethnicity));
            }

            return hmdaEthnicities;
        }

        /// <summary>
        /// Creates a container holding a borrower's ethnicity.
        /// </summary>
        /// <param name="ethnicity">The ethnicity to populate.</param>
        /// <returns>An LQB_HMDA_ETHNICITY container.</returns>
        private LQB_HMDA_ETHNICITY CreateLqbHmdaEthnicity(HMDAEthnicityBase ethnicity)
        {
            var hmdaEthnicity = new LQB_HMDA_ETHNICITY();
            hmdaEthnicity.HMDAEthnicityType = ToMismoEnum(ethnicity);

            return hmdaEthnicity;
        }

        /// <summary>
        /// Creates a container holding non-race demographic data on a borrower.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A GOVERNMENT_MONITORING_DETAIL container.</returns>
        private GOVERNMENT_MONITORING_DETAIL CreateGovernmentMonitoringDetail(CAppData appData)
        {
            var governmentMonitoringDetail = new GOVERNMENT_MONITORING_DETAIL();
            governmentMonitoringDetail.GenderType = ToGenderEnum(appData.aGenderFallback);
            governmentMonitoringDetail.HMDAEthnicityType = ToHmdaEthnicityEnum(appData.aHispanicTFallback);
            governmentMonitoringDetail.RaceNationalOriginRefusalIndicator = ToMismoIndicator(appData.aNoFurnish);

            return governmentMonitoringDetail;
        }

        /// <summary>
        /// Creates a container holding a set of high cost mortgage information.
        /// </summary>
        /// <returns>A HIGH_COST_MORTGAGES container.</returns>
        private HIGH_COST_MORTGAGES CreateHighCostMortgages()
        {
            var highCostMortgages = new HIGH_COST_MORTGAGES();
            highCostMortgages.HighCostMortgage.Add(this.CreateHighCostMortgage());

            return highCostMortgages;
        }

        /// <summary>
        /// Creates a container holding information on a high cost mortgage.
        /// </summary>
        /// <returns>A HIGH_COST_MORTGAGE container.</returns>
        private HIGH_COST_MORTGAGE CreateHighCostMortgage()
        {
            var highCostMortgage = new HIGH_COST_MORTGAGE();
            highCostMortgage.SequenceNumber = 1;

            highCostMortgage.AveragePrimeOfferRatePercent = ToMismoPercent(this.loanData.sQMAveragePrimeOfferR_rep);
            highCostMortgage.RegulationZExcludedBonaFideDiscountPointsIndicator = ToMismoIndicator(this.loanData.sGfeDiscountPointFProps_BF);
            highCostMortgage.RegulationZExcludedBonaFideDiscountPointsPercent = ToMismoPercent(this.loanData.sExcludedBonaFideDiscountPointsPc_rep);
            highCostMortgage.RegulationZTotalLoanAmount = ToMismoAmount(this.loanData.sQMLAmt_rep);
            highCostMortgage.RegulationZTotalPointsAndFeesAmount = ToMismoAmount(this.loanData.sQMTotFeeAmt_rep);
            highCostMortgage.RegulationZTotalPointsAndFeesPercent = ToMismoPercent(this.loanData.sQMTotFeePc_rep);

            return highCostMortgage;
        }

        /// <summary>
        /// Creates a container holding data about an HMDA loan.
        /// </summary>
        /// <returns>An HMDA_LOAN container.</returns>
        private HMDA_LOAN CreateHmdaLoan()
        {
            var hmdaLoan = new HMDA_LOAN();
            hmdaLoan.HMDA_HOEPALoanStatusIndicator = ToMismoIndicator(this.loanData.sHmdaReportAsHoepaLoan);
            hmdaLoan.HMDAPreapprovalType = ToHmdaPreapprovalEnum(this.loanData.sHmdaPreapprovalT);
            hmdaLoan.HMDAPurposeOfLoanType = ToHmdaPurposeOfLoanEnum(this.loanData.sLPurposeT, this.loanData.sHmdaReportAsHomeImprov);
            hmdaLoan.HMDARateSpreadPercent = ToMismoPercent(this.loanData.sHmdaAprRateSpread, this.loanData.m_convertLos);

            hmdaLoan.Extension = this.CreateHmdaLoanExtension();

            return hmdaLoan;
        }

        /// <summary>
        /// Creates an extension to the HMDA_LOAN container.
        /// </summary>
        /// <returns>An extension to the HMDA_LOAN container.</returns>
        private HMDA_LOAN_EXTENSION CreateHmdaLoanExtension()
        {
            var extension = new HMDA_LOAN_EXTENSION();
            extension.Other = this.CreateLqbHmdaLoanExtension();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LQB extension to the HMDA_LOAN container.
        /// </summary>
        /// <returns>A proprietary LQB extension to the HMDA_LOAN container.</returns>
        private LQB_HMDA_LOAN_EXTENSION CreateLqbHmdaLoanExtension()
        {
            var extension = new LQB_HMDA_LOAN_EXTENSION();
            extension.HmdaLarData = this.CreateLqbHmdaLarData();
            extension.HmdaReliedOn = this.CreateLqbHmdaReliedOn();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension element containing HMDA LAR data.
        /// </summary>
        /// <returns>A container holding HMDA LAR data.</returns>
        /// <remarks>Some datapoints use the MISMO 3.4 types as those contain HMDA data that is not captured in 3.3.</remarks>
        private LQB_HMDA_LAR_DATA CreateLqbHmdaLarData()
        {
            var hmdaLar = new LQB_HMDA_LAR_DATA();

            hmdaLar.GlobalLegalEntityIdentifier = ToMismoString(this.loanData.sLegalEntityIdentifier);
            hmdaLar.LoanIdentifier = ToMismoString(this.loanData.sUniversalLoanIdentifier);
            hmdaLar.ApplicationReceivedDate = ToMismoString(this.loanData.sHmdaApplicationDate_rep);
            hmdaLar.MortgageType = ToMortgageEnum(this.loanData.sHmdaLoanTypeT);
            hmdaLar.HMDAPurposeOfLoanType = Version4.TypeBuilder.ToMismoEnum(
                Version4.CommonBuilder.GetHmdaPurposeOfLoanBaseValue(this.loanData.sHmdaLoanPurposeT));
            hmdaLar.HMDAPreapprovalType = ToHmdaPreapprovalEnum(this.loanData.sHmdaPreapprovalT);
            hmdaLar.ConstructionMethodType = ToConstructionMethodEnum(this.loanData.sHmdaConstructionMethT);
            hmdaLar.PropertyUsageType = ToPropertyUsageEnum(this.loanData.sOccT);
            hmdaLar.BorrowerRequestedLoanAmount = ToMismoString(this.loanData.sHmdaLoanAmount_rep);
            hmdaLar.HMDADispositionType = Version4.TypeBuilder.ToMismoEnum(
                Version4.CommonBuilder.GetHmdaDispositionBaseValue(this.loanData.sHmdaActionTakenT));
            hmdaLar.HMDADispositionDate = ToMismoString(this.loanData.sHmdaActionD_rep);

            hmdaLar.AddressLineText = ToMismoString(this.loanData.sHmdaSpAddr);
            hmdaLar.CityName = ToMismoString(this.loanData.sHmdaSpCity);
            hmdaLar.StateCode = ToMismoString(this.loanData.sHmdaSpState);
            hmdaLar.PostalCode = ToMismoString(this.loanData.sHmdaSpZip);
            hmdaLar.FIPSCountyCode = ToMismoString(this.loanData.sHmda2018CountyCode);
            hmdaLar.CensusTractIdentifier = ToMismoString(this.loanData.sHmda2018CensusTract);

            hmdaLar.BorrowerHMDAEthnicityType1 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaBEthnicity1T);
            hmdaLar.BorrowerHMDAEthnicityType2 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaBEthnicity2T);
            hmdaLar.BorrowerHMDAEthnicityType3 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaBEthnicity3T);
            hmdaLar.BorrowerHMDAEthnicityType4 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaBEthnicity4T);
            hmdaLar.BorrowerHMDAEthnicityType5 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaBEthnicity5T);
            hmdaLar.BorrowerHMDAEthnicityTypeOtherDescription = ToMismoString(this.loanData.sHmdaBEthnicityOtherDescription);

            hmdaLar.CoborrowerHMDAEthnicityType1 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaCEthnicity1T);
            hmdaLar.CoborrowerHMDAEthnicityType2 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaCEthnicity2T);
            hmdaLar.CoborrowerHMDAEthnicityType3 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaCEthnicity3T);
            hmdaLar.CoborrowerHMDAEthnicityType4 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaCEthnicity4T);
            hmdaLar.CoborrowerHMDAEthnicityType5 = ToLqbHmdaEthnicityAndOriginEnum(this.loanData.sHmdaCEthnicity5T);
            hmdaLar.CoborrowerHMDAEthnicityTypeOtherDescription = ToMismoString(this.loanData.sHmdaCEthnicityOtherDescription);

            hmdaLar.BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType = ToHmdaDataCollectedByObservationOrSurnameEnum(this.loanData.sHmdaBEthnicityCollectedByObservationOrSurname);
            hmdaLar.CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType = ToHmdaDataCollectedByObservationOrSurnameEnum(this.loanData.sHmdaCEthnicityCollectedByObservationOrSurname);

            hmdaLar.BorrowerHMDARaceType1 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaBRace1T);
            hmdaLar.BorrowerHMDARaceType2 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaBRace2T);
            hmdaLar.BorrowerHMDARaceType3 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaBRace3T);
            hmdaLar.BorrowerHMDARaceType4 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaBRace4T);
            hmdaLar.BorrowerHMDARaceType5 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaBRace5T);
            hmdaLar.BorrowerHMDAEnrolledOrPrincipalTribe = ToMismoString(this.loanData.sHmdaBEnrolledOrPrincipalTribe);
            hmdaLar.BorrowerHMDARaceDesignationTypeOtherAsianDescription = ToMismoString(this.loanData.sHmdaBOtherAsianDescription);
            hmdaLar.BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescription = ToMismoString(this.loanData.sHmdaBOtherPacificIslanderDescription);

            hmdaLar.CoborrowerHMDARaceType1 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaCRace1T);
            hmdaLar.CoborrowerHMDARaceType2 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaCRace2T);
            hmdaLar.CoborrowerHMDARaceType3 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaCRace3T);
            hmdaLar.CoborrowerHMDARaceType4 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaCRace4T);
            hmdaLar.CoborrowerHMDARaceType5 = ToLqbHmdaRaceAndDesignationEnum(this.loanData.sHmdaCRace5T);
            hmdaLar.CoborrowerHMDAEnrolledOrPrincipalTribe = ToMismoString(this.loanData.sHmdaCEnrolledOrPrincipalTribe);
            hmdaLar.CoborrowerHMDARaceDesignationTypeOtherAsianDescription = ToMismoString(this.loanData.sHmdaCOtherAsianDescription);
            hmdaLar.CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescription = ToMismoString(this.loanData.sHmdaCOtherPacificIslanderDescription);

            hmdaLar.BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType = ToHmdaDataCollectedByObservationOrSurnameEnum(this.loanData.sHmdaBRaceCollectedByObservationOrSurname);
            hmdaLar.CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType = ToHmdaDataCollectedByObservationOrSurnameEnum(this.loanData.sHmdaCRaceCollectedByObservationOrSurname);

            hmdaLar.BorrowerHMDAGenderType = ToHmdaGenderEnum(this.loanData.sHmdaBSexT);
            hmdaLar.CoborrowerHMDAGenderType = ToHmdaGenderEnum(this.loanData.sHmdaCSexT);

            hmdaLar.BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType = ToHmdaDataCollectedByObservationOrSurnameEnum(this.loanData.sHmdaBSexCollectedByObservationOrSurname);
            hmdaLar.CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType = ToHmdaDataCollectedByObservationOrSurnameEnum(this.loanData.sHmdaCSexCollectedByObservationOrSurname);

            hmdaLar.BorrowerAgeAtApplicationYearsCount = ToMismoString(this.loanData.sHmdaBAge_rep);
            hmdaLar.CoborrowerAgeAtApplicationYearsCount = ToMismoString(this.loanData.sHmdaCAge_rep);

            hmdaLar.TotalMonthlyIncomeAmount = ToMismoString(this.loanData.sHmdaIncome_rep);
            hmdaLar.HMDAPurchaserType = ToHmdaPurchaserEnum(this.loanData.sHmdaPurchaser2015T);
            hmdaLar.HMDARateSpreadPercent = ToMismoString(this.loanData.sHmda2018AprRateSpread);
            hmdaLar.HMDAHOEPALoanStatusType = ToLqbHmdaHoepaLoanStatusEnum(this.loanData.sHmdaHoepaStatusT);
            hmdaLar.LienPriorityType = ToLqbLienPriorityEnum(this.loanData.sHmdaLienT);

            hmdaLar.BorrowerCreditScoreValue = ToMismoString(this.loanData.sHmdaBCreditScore_rep);
            hmdaLar.CoborrowerCreditScoreValue = ToMismoString(this.loanData.sHmdaCCreditScore_rep);
            hmdaLar.BorrowerCreditScoreModelNameType = ToLqbCreditScoreModelNameEnum(this.loanData.sHmdaBCreditScoreModelT);
            hmdaLar.BorrowerCreditScoreModelNameTypeOtherDescription = ToMismoString(this.loanData.sHmdaBCreditScoreModelOtherDescription);
            hmdaLar.CoborrowerCreditScoreModelNameType = ToLqbCreditScoreModelNameEnum(this.loanData.sHmdaCCreditScoreModelT);
            hmdaLar.CoborrowerCreditScoreModelNameTypeOtherDescription = ToMismoString(this.loanData.sHmdaCCreditScoreModelOtherDescription);

            hmdaLar.HMDAReasonForDenialType1 = Version4.TypeBuilder.ToMismoEnum(
                Version4.CommonBuilder.GetHmdaReasonForDenialBaseValue(this.loanData.sHmdaDenialReasonT1));
            hmdaLar.HMDAReasonForDenialType2 = Version4.TypeBuilder.ToMismoEnum(
                Version4.CommonBuilder.GetHmdaReasonForDenialBaseValue(this.loanData.sHmdaDenialReasonT2));
            hmdaLar.HMDAReasonForDenialType3 = Version4.TypeBuilder.ToMismoEnum(
                Version4.CommonBuilder.GetHmdaReasonForDenialBaseValue(this.loanData.sHmdaDenialReasonT3));
            hmdaLar.HMDAReasonForDenialType4 = Version4.TypeBuilder.ToMismoEnum(
                Version4.CommonBuilder.GetHmdaReasonForDenialBaseValue(this.loanData.sHmdaDenialReasonT4));
            hmdaLar.HMDAReasonForDenialTypeOtherDescription = ToMismoString(this.loanData.sHmdaDenialReasonOtherDescription);

            if (this.loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE)
            {
                hmdaLar.TotalPointsAndFeesAmount = ToMismoString(this.loanData.sHmdaTotalLoanCosts_rep);
            }
            else
            {
                hmdaLar.TotalLoanCostsAmount = ToMismoString(this.loanData.sHmdaTotalLoanCosts_rep);
            }

            hmdaLar.OriginationChargesAmount = ToMismoString(this.loanData.sHmdaOriginationCharge_rep);
            hmdaLar.DiscountPointsAmount = ToMismoString(this.loanData.sHmdaDiscountPoints_rep);
            hmdaLar.LenderCreditsAmount = ToMismoString(this.loanData.sHmdaLenderCredits_rep);
            hmdaLar.NoteRatePercent = ToMismoString(this.loanData.sHmdaInterestRate_rep);
            hmdaLar.PrepaymentPenaltyExpirationMonthsCount = ToMismoString(this.loanData.sHmdaPrepaymentTerm_rep);
            hmdaLar.TotalDebtExpenseRatioPercent = ToMismoString(this.loanData.sHmdaDebtRatio_rep);
            hmdaLar.CombinedLTVRatioPercent = ToMismoString(this.loanData.sHmdaCombinedRatio_rep);
            hmdaLar.LoanMaturityPeriodCount = ToMismoString(this.loanData.sHmdaTerm_rep);
            hmdaLar.FirstRateChangeMonthsCount = ToMismoString(this.loanData.sHmdaIntroductoryPeriod_rep);
            hmdaLar.BalloonPaymentIndicator = ToBalloonPaymentIndicator(this.loanData.sHmdaBalloonPaymentT);
            hmdaLar.InterestOnlyIndicator = ToInterestOnlyPaymentsIndicator(this.loanData.sHmdaInterestOnlyPaymentT);
            hmdaLar.NegativeAmortizationIndicator = ToNegativeAmortizationIndicator(this.loanData.sHmdaNegativeAmortizationT);
            hmdaLar.HMDAOtherNonAmortizingFeaturesIndicator = ToOtherNonAmortizingFeaturesIndicator(this.loanData.sHmdaOtherNonAmortFeatureT);

            hmdaLar.PropertyValuationAmount = ToMismoString(this.loanData.sHmdaPropertyValue_rep);
            hmdaLar.ManufacturedHomeSecuredPropertyType = ToLqbManufacturedHomeSecuredPropertyEnum(this.loanData.sHmdaManufacturedTypeT);
            hmdaLar.ManufacturedHomeLandPropertyInterestType = ToLqbManufacturedHomeLandPropertyInterestEnum(this.loanData.sHmdaManufacturedInterestT);
            hmdaLar.FinancedUnitCount = ToMismoString(this.loanData.sHmdaTotalUnits_rep);
            hmdaLar.AffordableUnitsCount = ToMismoString(this.loanData.sHmdaMultifamilyUnits_rep);

            hmdaLar.HMDAApplicationSubmissionType = Version4.TypeBuilder.ToMismoEnum(
                Version4.CommonBuilder.GetHmdaApplicationSubmissionBaseValue(this.loanData.sHmdaSubmissionApplicationT));
            hmdaLar.HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType = Version4.TypeBuilder.ToMismoEnum(
                Version4.CommonBuilder.GetHmdaCoveredLoanInitiallyPayableToReportingInstitutionStatusBaseValue(this.loanData.sHmdaInitiallyPayableToInstitutionT));
            hmdaLar.LoanOriginatorNMLSIdentifier = ToMismoString(this.loanData.sApp1003InterviewerLoanOriginatorIdentifier);

            hmdaLar.AutomatedUnderwritingSystemType1 = ToLqbHmdaAutomatedUnderwritingSystemEnum(this.loanData.sHmdaAutomatedUnderwritingSystem1T);
            hmdaLar.AutomatedUnderwritingSystemType2 = ToLqbHmdaAutomatedUnderwritingSystemEnum(this.loanData.sHmdaAutomatedUnderwritingSystem2T);
            hmdaLar.AutomatedUnderwritingSystemType3 = ToLqbHmdaAutomatedUnderwritingSystemEnum(this.loanData.sHmdaAutomatedUnderwritingSystem3T);
            hmdaLar.AutomatedUnderwritingSystemType4 = ToLqbHmdaAutomatedUnderwritingSystemEnum(this.loanData.sHmdaAutomatedUnderwritingSystem4T);
            hmdaLar.AutomatedUnderwritingSystemType5 = ToLqbHmdaAutomatedUnderwritingSystemEnum(this.loanData.sHmdaAutomatedUnderwritingSystem5T);
            hmdaLar.AutomatedUnderwritingSystemTypeOtherDescription = ToMismoString(this.loanData.sHmdaAutomatedUnderwritingSystemOtherDescription);

            hmdaLar.AutomatedUnderwritingSystemResultType1 = ToLqbHmdaAutomatedUnderwritingSystemResultEnum(this.loanData.sHmdaAutomatedUnderwritingSystemResult1T);
            hmdaLar.AutomatedUnderwritingSystemResultType2 = ToLqbHmdaAutomatedUnderwritingSystemResultEnum(this.loanData.sHmdaAutomatedUnderwritingSystemResult2T);
            hmdaLar.AutomatedUnderwritingSystemResultType3 = ToLqbHmdaAutomatedUnderwritingSystemResultEnum(this.loanData.sHmdaAutomatedUnderwritingSystemResult3T);
            hmdaLar.AutomatedUnderwritingSystemResultType4 = ToLqbHmdaAutomatedUnderwritingSystemResultEnum(this.loanData.sHmdaAutomatedUnderwritingSystemResult4T);
            hmdaLar.AutomatedUnderwritingSystemResultType5 = ToLqbHmdaAutomatedUnderwritingSystemResultEnum(this.loanData.sHmdaAutomatedUnderwritingSystemResult5T);
            hmdaLar.AutomatedUnderwritingSystemResultTypeOtherDescription = ToMismoString(this.loanData.sHmdaAutomatedUnderwritingSystemResultOtherDescription);

            hmdaLar.ReverseMortgageIndicator = ToReverseMortgageIndicator(this.loanData.sHmdaReverseMortgageT);
            hmdaLar.OpenEndCreditIndicator = ToMismoIndicator(this.loanData.sIsLineOfCredit);
            hmdaLar.HMDABusinessPurposeIndicator = ToBusinessPurposeIndicator(this.loanData.sHmdaBusinessPurposeT);

            return hmdaLar;
        }

        /// <summary>
        /// Creates a container holding HMDA relied-on data.
        /// </summary>
        /// <returns>An LQB_HMDA_RELIED_ON container.</returns>
        private LQB_HMDA_RELIED_ON CreateLqbHmdaReliedOn()
        {
            var reliedOn = new LQB_HMDA_RELIED_ON();

            reliedOn.TotalAnnualIncomeReliedOnIndicator = ToMismoIndicator(this.loanData.sHmdaIsIncomeReliedOn);
            reliedOn.ReliedOnTotalAnnualIncomeAmount = ToMismoAmount(this.loanData.sReliedOnTotalIncome_rep);

            reliedOn.DebtToIncomeRatioReliedOnIndicator = ToMismoIndicator(this.loanData.sHmdaIsDebtRatioReliedOn);
            reliedOn.ReliedOnDebtToIncomeRatioPercent = ToMismoPercent(this.loanData.sReliedOnDebtRatio_rep);

            reliedOn.CombinedLoanToValueRatioReliedOnIndicator = ToMismoIndicator(this.loanData.sHmdaIsCombinedRatioReliedOn);
            reliedOn.ReliedOnCombinedLoanToValueRatioPercent = ToMismoPercent(this.loanData.sReliedOnCombinedLTVRatio_rep);

            reliedOn.PropertyValueReliedOnIndicator = ToMismoIndicator(this.loanData.sHmdaIsPropertyValueReliedOn);
            reliedOn.ReliedOnPropertyValueAmount = ToMismoAmount(this.loanData.sReliedOnPropertyValue_rep);

            reliedOn.CreditScoreModeReliedOnIndicator = ToMismoIndicator(this.loanData.sHmdaIsCreditScoreReliedOn);
            reliedOn.ReliedOnCreditScoreModeType = ToLqbReliedOnCreditScoreModeEnum(this.loanData.sReliedOnCreditScoreModeT);

            return reliedOn;
        }

/// <summary>
/// Creates a container to hold all instances of a borrower's race.
/// </summary>
/// <param name="appData">The borrower's application data.</param>
/// <returns>An HMDA_RACES container.</returns>
private HMDA_RACES CreateHmdaRaces(CAppData appData)
        {
            var hmdaRaces = new HMDA_RACES();

            if (appData.aIsAmericanIndian)
            {
                hmdaRaces.HmdaRace.Add(this.CreateHmdaRace(HMDARaceBase.AmericanIndianOrAlaskaNative, 1));
            }

            if (appData.aIsAsian)
            {
                hmdaRaces.HmdaRace.Add(this.CreateHmdaRace(HMDARaceBase.Asian, hmdaRaces.HmdaRace.Count(h => h != null) + 1));
            }

            if (appData.aIsBlack)
            {
                hmdaRaces.HmdaRace.Add(this.CreateHmdaRace(HMDARaceBase.BlackOrAfricanAmerican, hmdaRaces.HmdaRace.Count(h => h != null) + 1));
            }

            if (appData.aIsPacificIslander)
            {
                hmdaRaces.HmdaRace.Add(this.CreateHmdaRace(HMDARaceBase.NativeHawaiianOrOtherPacificIslander, hmdaRaces.HmdaRace.Count(h => h != null) + 1));
            }

            if (appData.aIsWhite)
            {
                hmdaRaces.HmdaRace.Add(this.CreateHmdaRace(HMDARaceBase.White, hmdaRaces.HmdaRace.Count(h => h != null) + 1));
            }

            return hmdaRaces;
        }

        /// <summary>
        /// Creates a container holding one instance of a borrower's race.
        /// </summary>
        /// <param name="race">One specific race that describes the borrower.</param>
        /// <param name="sequence">The sequence number of the race among the list of races.</param>
        /// <returns>An HMDA_RACE container.</returns>
        private HMDA_RACE CreateHmdaRace(HMDARaceBase race, int sequence)
        {
            var hmdaRace = new HMDA_RACE();
            hmdaRace.HMDARaceType = ToHmdaRaceEnum(race);
            hmdaRace.SequenceNumber = sequence;

            return hmdaRace;
        }

        /// <summary>
        /// Creates a container to hold a set of hazard insurance policies.
        /// </summary>
        /// <returns>A HAZARD_INSURANCE container.</returns>
        private HAZARD_INSURANCES CreateHazardInsurances()
        {
            var hazardInsurances = new HAZARD_INSURANCES();

            foreach (BaseHousingExpense insuranceExpense in this.loanData.sHousingExpenses.ExpensesToUse.Where(e => e.IsInsurance))
            {
                hazardInsurances.HazardInsurance.Add(this.CreateHazardInsurance(insuranceExpense, hazardInsurances.HazardInsurance.Count(i => i != null) + 1));
            }

            return hazardInsurances;
        }

        /// <summary>
        /// Creates a container holding information on a hazard insurance policy.
        /// </summary>
        /// <param name="insuranceExpense">A <see cref="BaseHousingExpense"/> representing an insurance policy.</param>
        /// <param name="sequence">The sequence number of the hazard insurance among the list of hazard insurances.</param>
        /// <returns>A HAZARD_INSURANCE container.</returns>
        private HAZARD_INSURANCE CreateHazardInsurance(BaseHousingExpense insuranceExpense, int sequence)
        {
            if (!insuranceExpense.IsInsurance)
            {
                return null;
            }

            var hazardInsurance = new HAZARD_INSURANCE();
            hazardInsurance.SequenceNumber = sequence;
            hazardInsurance.HazardInsuranceCoverageType = ToHazardInsuranceCoverageEnum(insuranceExpense.HousingExpenseType);
            ////hazardInsurance.HazardInsuranceCoverageTypeOtherDescription;
            hazardInsurance.HazardInsuranceEffectiveDate = ToMismoDate(insuranceExpense.PolicyActivationD_rep);
            hazardInsurance.HazardInsuranceEscrowedIndicator = ToMismoIndicator(insuranceExpense.IsEscrowedAtClosing);
            ////hazardInsurance.HazardInsuranceNonStandardPolicyType;
            ////hazardInsurance.HazardInsuranceNonStandardPolicyTypeOtherDescription;
            ////hazardInsurance.HazardInsurancePolicyCancellationDate;

            var firstDisbursement = insuranceExpense.FirstDisbursement;

            if (firstDisbursement != null)
            {
                hazardInsurance.HazardInsuranceNextPremiumDueDate = ToMismoDate(firstDisbursement.DueDate_rep);
                hazardInsurance.HazardInsurancePremiumAmount = ToMismoAmount(firstDisbursement.DisbursementAmt_rep);
                hazardInsurance.HazardInsurancePremiumMonthsCount = ToMismoCount(firstDisbursement.CoveredMonths_rep, null);
            }

            ////hazardInsurance.HazardInsuranceReplacementValueIndicator;
            ////hazardInsurance.InsuranceRequiredIndicator;

            decimal coverageAmount = 0.00m;

            if (insuranceExpense.HousingExpenseType == E_HousingExpenseTypeT.CondoHO6Insurance)
            {
                hazardInsurance.HazardInsuranceCoverageAmount = ToMismoAmount(this.loanData.sCondoHO6InsCoverageAmt_rep);
                hazardInsurance.HazardInsuranceExpirationDate = ToMismoDate(this.loanData.sCondoHO6InsPolicyExpirationD_rep);
                hazardInsurance.HazardInsurancePolicyIdentifier = ToMismoIdentifier(this.loanData.sCondoHO6InsPolicyNum);
                coverageAmount = this.loanData.sCondoHO6InsCoverageAmt;
            }
            else if (insuranceExpense.HousingExpenseType == E_HousingExpenseTypeT.FloodInsurance)
            {
                ////hazardInsurance.FloodContractFeeAmount;

                hazardInsurance.HazardInsuranceCoverageAmount = ToMismoAmount(this.loanData.sFloodInsCoverageAmt_rep);
                hazardInsurance.HazardInsuranceExpirationDate = ToMismoDate(this.loanData.sFloodInsPolicyExpirationD_rep);
                hazardInsurance.HazardInsurancePolicyIdentifier = ToMismoIdentifier(this.loanData.sFloodInsPolicyNum);
                coverageAmount = this.loanData.sFloodInsCoverageAmt;
            }
            else if (insuranceExpense.HousingExpenseType == E_HousingExpenseTypeT.HazardInsurance)
            {
                hazardInsurance.HazardInsuranceCoverageAmount = ToMismoAmount(this.loanData.sHazInsCoverageAmt_rep);
                hazardInsurance.HazardInsuranceExpirationDate = ToMismoDate(this.loanData.sHazInsPolicyExpirationD_rep);
                hazardInsurance.HazardInsurancePolicyIdentifier = ToMismoIdentifier(this.loanData.sHazInsPolicyNum);
                coverageAmount = this.loanData.sHazInsCoverageAmt;
            }
            else if (insuranceExpense.HousingExpenseType == E_HousingExpenseTypeT.WindstormInsurance)
            {
                hazardInsurance.HazardInsuranceCoverageAmount = ToMismoAmount(this.loanData.sWindInsCoverageAmt_rep);
                hazardInsurance.HazardInsuranceExpirationDate = ToMismoDate(this.loanData.sWindInsPolicyExpirationD_rep);
                hazardInsurance.HazardInsurancePolicyIdentifier = ToMismoIdentifier(this.loanData.sWindInsPolicyNum);
                coverageAmount = this.loanData.sWindInsCoverageAmt;
            }

            return coverageAmount > 0.00m || (firstDisbursement != null && firstDisbursement.DisbursementAmt > 0.00m) ? hazardInsurance : null;
        }

        /// <summary>
        /// Creates a container holding HELOC information.
        /// </summary>
        /// <returns>A HELOC container, or null if the loan is not a HELOC.</returns>
        private HELOC CreateHeloc()
        {
            if (!this.loanData.BrokerDB.IsEnableHELOC || !this.loanData.sIsLineOfCredit)
            {
                return null;
            }

            var heloc = new HELOC();
            heloc.HELOCRule = this.CreateHelocRule();
            heloc.HELOCOccurrences = this.CreateHelocOccurrences();

            return heloc;
        }

        /// <summary>
        /// Creates a container holding data on a HELOC occurrence.
        /// </summary>
        /// <param name="sequence">The sequence number of the occurrence among the list of occurrences.</param>
        /// <returns>A HELOC_OCCURRENCE container populated with data.</returns>
        private HELOC_OCCURRENCE CreateHelocOccurrence(int sequence)
        {
            var helocOccurrence = new HELOC_OCCURRENCE();
            helocOccurrence.HELOCDailyPeriodicInterestRatePercent = ToMismoPercent(this.loanData.sDailyPeriodicR_rep);
            helocOccurrence.SequenceNumber = sequence;

            return helocOccurrence;
        }

        /// <summary>
        /// Creates a container holding information on a subordinate HELOC.
        /// </summary>
        /// <param name="currentMax">The maximum balance of the current HELOC.</param>
        /// <param name="subordinateAmount">The balance of the HELOC.</param>
        /// <param name="subordinateRate">The daily interest rate of the HELOC.</param>
        /// <param name="sequence">The sequence number of the occurrence among the list of occurrences.</param>
        /// <returns>A HELOC_OCCURRENCE container.</returns>
        private HELOC_OCCURRENCE CreateHelocOccurrence(string currentMax, string subordinateAmount, string subordinateRate, int sequence)
        {
            var helocOccurrence = new HELOC_OCCURRENCE();
            helocOccurrence.CurrentHELOCMaximumBalanceAmount = ToMismoAmount(currentMax);
            helocOccurrence.HELOCBalanceAmount = ToMismoAmount(subordinateAmount);
            helocOccurrence.HELOCDailyPeriodicInterestRatePercent = ToMismoPercent(subordinateRate);
            helocOccurrence.SequenceNumber = sequence;

            return helocOccurrence;
        }

        /// <summary>
        /// Creates a container holding all instances of HELOC_OCCURRENCE.
        /// </summary>
        /// <returns>A HELOC_OCCURRENCES container.</returns>
        private HELOC_OCCURRENCES CreateHelocOccurrences()
        {
            var helocOccurrences = new HELOC_OCCURRENCES();

            if (this.loanData.sIsOFinCreditLineInDrawPeriod)
            {
                // Add fields related to a subordinate HELOC.
                helocOccurrences.HELOCOccurrence.Add(this.CreateHelocOccurrence(this.loanData.sSubFin_rep, this.loanData.sConcurSubFin_rep, this.loanData.sSubFinIR_rep, 1));
            }
            else
            {
                helocOccurrences.HELOCOccurrence.Add(this.CreateHelocOccurrence(1));
            }

            return helocOccurrences;
        }

        /// <summary>
        /// Creates a container holding data on a HELOC.
        /// </summary>
        /// <returns>A HELOC_RULE populated with data.</returns>
        private HELOC_RULE CreateHelocRule()
        {
            var helocRule = new HELOC_RULE();
            helocRule.HELOCAnnualFeeAmount = ToMismoAmount(this.loanData.sHelocAnnualFee_rep);
            helocRule.HELOCCreditLineDrawAccessFeeAmount = ToMismoAmount(this.loanData.sHelocDrawFee_rep);
            helocRule.HELOCDailyPeriodicInterestRateCalculationType = ToHelocDailyPeriodicInterestRateCalculationEnum(this.loanData.sDaysInYr_rep);
            helocRule.HELOCInitialAdvanceAmount = ToMismoAmount(this.loanData.sLAmtCalc_rep);
            helocRule.HELOCMaximumAPRPercent = ToMismoPercent(this.loanData.sRLifeCapR_rep);
            helocRule.HELOCMaximumBalanceAmount = ToMismoAmount(this.loanData.sCreditLineAmt_rep);
            helocRule.HELOCMinimumAdvanceAmount = ToMismoAmount(this.loanData.sHelocMinimumAdvanceAmt_rep);
            helocRule.HELOCMinimumPaymentAmount = ToMismoAmount(this.loanData.sHelocMinimumPayment_rep);
            helocRule.HELOCMinimumPaymentPercent = ToMismoPercent(this.loanData.sHelocMinimumPaymentPc_rep);
            helocRule.HELOCRepayPeriodMonthsCount = ToMismoCount(this.loanData.sHelocRepay_rep, null);
            helocRule.HELOCReturnedCheckChargeAmount = ToMismoAmount(this.loanData.sHelocReturnedCheckFee_rep);
            helocRule.HELOCStopPaymentChargeAmount = ToMismoAmount(this.loanData.sHelocStopPaymentFee_rep);
            helocRule.HELOCTeaserMarginRatePercent = ToMismoPercent(this.loanData.sNoteIR_rep);
            helocRule.HELOCTeaserRateType = ToHelocTeaserRateEnum(this.loanData.sInitialRateT);
            helocRule.HELOCTeaserTermMonthsCount = ToMismoCount(this.loanData.sRAdj1stCapMon_rep, null);
            helocRule.HELOCTerminationFeeAmount = ToMismoAmount(this.loanData.sHelocTerminationFee_rep);
            helocRule.HELOCTerminationPeriodMonthsCount = ToMismoCount(this.loanData.sHelocTerminationFeePeriod_rep, null);

            return helocRule;
        }

        /// <summary>
        /// Creates a container holding information on a housing expense.
        /// </summary>
        /// <param name="amount">The amount of the housing expense.</param>
        /// <param name="paymentAmountType">The Amount type of the housing expense (such as "proposed").</param>
        /// <param name="expenseType">The type of housing expense.</param>
        /// <param name="sequence">The sequence number of the housing expense among the list of expenses.</param>
        /// <returns>A HOUSING_EXPENSE container populated with data.</returns>
        private HOUSING_EXPENSE CreateHousingExpense(string amount, HousingExpensePaymentAmountBase paymentAmountType, HousingExpenseBase expenseType, int sequence)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var housingExpense = new HOUSING_EXPENSE();
            housingExpense.HousingExpensePaymentAmount = ToMismoAmount(amount);
            housingExpense.HousingExpensePaymentAmountType = ToHousingExpensePaymentAmountEnum(paymentAmountType);
            housingExpense.HousingExpenseType = ToHousingExpenseEnum(expenseType);
            housingExpense.SequenceNumber = sequence;

            return housingExpense;
        }

        /// <summary>
        /// Creates the container for the proposed housing expenses - xpath //DEAL/LOANS/LOAN[1]/HOUSING_EXPENSES .  
        /// </summary>
        /// <returns>A HOUSING_EXPENSES container containing only the Proposed housing expenses.</returns>
        private HOUSING_EXPENSES CreateHousingExpenses_LoanContainer()
        {
            var housingExpenses = new HOUSING_EXPENSES();

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    this.loanData.sProMIns_rep,
                    HousingExpensePaymentAmountBase.Proposed,
                    HousingExpenseBase.MI,
                    1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    this.loanData.sProHazIns_rep,
                    HousingExpensePaymentAmountBase.Proposed,
                    HousingExpenseBase.HazardInsurance,
                    housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    this.loanData.sProSecondMPmt_rep,
                    HousingExpensePaymentAmountBase.Proposed,
                    HousingExpenseBase.OtherMortgageLoanPrincipalAndInterest,
                    housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    this.loanData.sProOHExp_rep,
                    HousingExpensePaymentAmountBase.Proposed,
                    HousingExpenseBase.Other,
                    housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    this.loanData.sProRealETx_rep,
                    HousingExpensePaymentAmountBase.Proposed,
                    HousingExpenseBase.RealEstateTax,
                    housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    this.loanData.sProHoAssocDues_rep,
                    HousingExpensePaymentAmountBase.Proposed,
                    HousingExpenseBase.HomeownersAssociationDuesAndCondominiumFees,
                    housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    this.loanData.sProFirstMPmt_rep,
                    HousingExpensePaymentAmountBase.Proposed,
                    HousingExpenseBase.FirstMortgagePrincipalAndInterest,
                    housingExpenses.HousingExpense.Count(h => h != null) + 1));

            return housingExpenses;
        }

        /// <summary>
        /// Creates the container for the present housing expenses - xpath //DEAL/PARTIES/PARTY/ROLES/ROLE[1]/BORROWER/HOUSING_EXPENSES .  
        /// </summary>
        /// <param name="appData">CAppData object from which to get the present housing expenses (borrower/co-borrower agnostic).</param>
        /// <returns>A HOUSING_EXPENSES container containing only the present housing expenses.</returns>
        private HOUSING_EXPENSES CreateHousingExpenses_BorrowerContainer(CAppData appData)
        {
            if (appData.BorrowerModeT == E_BorrowerModeT.Coborrower)
            {
                return null;
            }

            var housingExpenses = new HOUSING_EXPENSES();

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    appData.aPres1stM_rep,
                    HousingExpensePaymentAmountBase.Present,
                    HousingExpenseBase.FirstMortgagePrincipalAndInterest,
                housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    appData.aPresHazIns_rep,
                    HousingExpensePaymentAmountBase.Present,
                    HousingExpenseBase.HazardInsurance,
                housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    appData.aPresHoAssocDues_rep,
                    HousingExpensePaymentAmountBase.Present,
                    HousingExpenseBase.HomeownersAssociationDuesAndCondominiumFees,
                housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    appData.aPresMIns_rep,
                    HousingExpensePaymentAmountBase.Present,
                    HousingExpenseBase.MI,
                housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    appData.aPresOHExp_rep,
                    HousingExpensePaymentAmountBase.Present,
                    HousingExpenseBase.Other,
                housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    appData.aPresOFin_rep,
                    HousingExpensePaymentAmountBase.Present,
                    HousingExpenseBase.OtherMortgageLoanPrincipalAndInterest,
                housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    appData.aPresRealETx_rep,
                    HousingExpensePaymentAmountBase.Present,
                    HousingExpenseBase.RealEstateTax,
                housingExpenses.HousingExpense.Count(h => h != null) + 1));

            housingExpenses.HousingExpense.Add(
                this.CreateHousingExpense(
                    appData.aPresRent_rep,
                    HousingExpensePaymentAmountBase.Present,
                    HousingExpenseBase.Rent,
                housingExpenses.HousingExpense.Count(h => h != null) + 1));

            return housingExpenses;
        }

        /// <summary>
        /// Creates an IDENTIFICATION_VERIFICATION container that holds a set of identification details.
        /// </summary>
        /// <param name="appData">Information about a borrower.</param>
        /// <returns>An IDENTIFICATION_VERIFICATION container.</returns>
        private IDENTIFICATION_VERIFICATION CreateIdentificationVerification(CAppData appData)
        {
            var identificationVerification = new IDENTIFICATION_VERIFICATION();
            identificationVerification.IdentityDocumentations = this.CreateIdentityDocumentations(appData);

            return identificationVerification;
        }

        /// <summary>
        /// Creates an IDENTITY_DOCUMENTATIONS container holding a set of details on identification documents.
        /// </summary>
        /// <param name="appData">Information on a borrower.</param>
        /// <returns>An IDENTITY_DOCUMENTATIONS container.</returns>
        private IDENTITY_DOCUMENTATIONS CreateIdentityDocumentations(CAppData appData)
        {
            var identityDocumentations = new IDENTITY_DOCUMENTATIONS();

            if (appData.aIdT != E_IdType.Blank)
            {
                identityDocumentations.IdentityDocumentation.Add(
                    this.CreateIdentityDocumentation(
                        appData.aIdT,
                        appData.aIdNumber,
                        appData.aIdIssueD_rep,
                        appData.aIdExpireD_rep,
                        appData.aIdState,
                        appData.aIdCountry,
                        appData.aIdGovBranch,
                        appData.aIdOther));
            }

            if (appData.a2ndIdT != E_IdType.Blank)
            {
                identityDocumentations.IdentityDocumentation.Add(
                    this.CreateIdentityDocumentation(
                        appData.a2ndIdT,
                        appData.a2ndIdNumber,
                        appData.a2ndIdIssueD_rep,
                        appData.a2ndIdExpireD_rep,
                        appData.a2ndIdState,
                        appData.a2ndIdCountry,
                        appData.a2ndIdGovBranch,
                        appData.a2ndIdOther));
            }

            return identityDocumentations;
        }

        /// <summary>
        /// Creates an IDENTITY_DOCUMENTATION container holding information on a specific identification document.
        /// </summary>
        /// <param name="idType">The type of identification recorded.</param>
        /// <param name="idNumber">The identification number.</param>
        /// <param name="issueDate">The issue date.</param>
        /// <param name="expirationDate">The expiration date.</param>
        /// <param name="issuingState">The issuing state.</param>
        /// <param name="issuingCountry">The issuing country.</param>
        /// <param name="issuingGovernmentBranch">The issuing government branch.</param>
        /// <param name="otherInformation">Other information about the identification.</param>
        /// <returns>An IDENTITY_DOCUMENTATION container.</returns>
        private IDENTITY_DOCUMENTATION CreateIdentityDocumentation(
            E_IdType idType,
            string idNumber,
            string issueDate,
            string expirationDate,
            string issuingState,
            string issuingCountry,
            string issuingGovernmentBranch,
            string otherInformation)
        {
            var identityDocumentation = new IDENTITY_DOCUMENTATION();

            string idTypeOtherDesc;
            identityDocumentation.IdentityDocumentType = ToIdentityDocumentEnum(idType, out idTypeOtherDesc);
            identityDocumentation.IdentityDocumentExpirationDate = ToMismoDate(expirationDate, true);

            if (identityDocumentation.IdentityDocumentType.enumValue == IdentityDocumentBase.Other)
            {
                if (!string.IsNullOrWhiteSpace(idTypeOtherDesc))
                {
                    identityDocumentation.IdentityDocumentTypeOtherDescription = ToMismoString(idTypeOtherDesc, true);
                }
                else if (!string.IsNullOrWhiteSpace(otherInformation))
                {
                    identityDocumentation.IdentityDocumentTypeOtherDescription = ToMismoString(otherInformation, true);
                }
            }

            string issuedByOtherDesc;
            identityDocumentation.IdentityDocumentIssuedByType = ToIdentityDocumentIssuedByEnum(idType, out issuedByOtherDesc);

            if (!string.IsNullOrWhiteSpace(issuedByOtherDesc))
            {
                identityDocumentation.IdentityDocumentIssuedByTypeOtherDescription = ToMismoString(issuedByOtherDesc, true);
            }

            if (idType == E_IdType.DriversLicense || idType == E_IdType.Passport || idType == E_IdType.StateId || idType == E_IdType.OtherDocument)
            {
                identityDocumentation.IdentityDocumentIssuedDate = ToMismoDate(issueDate, true);
            }

            if (idType != E_IdType.OtherDocument)
            {
                identityDocumentation.IdentityDocumentIdentifier = ToMismoIdentifier(idNumber, true);
            }

            if (idType == E_IdType.DriversLicense || idType == E_IdType.StateId)
            {
                identityDocumentation.IdentityDocumentIssuedByStateCode = ToMismoCode(issuingState, true);
            }

            if (idType == E_IdType.Passport || idType == E_IdType.MilitaryId || idType == E_IdType.ImmigrationCard)
            {
                identityDocumentation.IdentityDocumentIssuedByCountryName = ToMismoString(issuingCountry, true);
            }

            if (idType == E_IdType.Visa)
            {
                identityDocumentation.IdentityDocumentIssuedByName = ToMismoString(issuingGovernmentBranch, true);
            }

            return identityDocumentation;
        }

        /// <summary>
        /// Creates a container holding data on an interest calculation.
        /// </summary>
        /// <returns>An INTEREST_CALCULATION container.</returns>
        private INTEREST_CALCULATION CreateInterestCalculation()
        {
            var interestCalculation = new INTEREST_CALCULATION();
            interestCalculation.InterestCalculationRules = this.CreateInterestCalculationRules();

            return interestCalculation;
        }

        /// <summary>
        /// Creates a container holding data on an interest calculation rule.
        /// </summary>
        /// <returns>An INTEREST_CALCULATION_RULE container.</returns>
        private INTEREST_CALCULATION_RULE CreateInterestCalculationRule()
        {
            var interestCalculationRule = new INTEREST_CALCULATION_RULE();
            interestCalculationRule.SequenceNumber = 1;
            interestCalculationRule.LoanInterestAccrualStartDate = ToMismoDate(this.loanData.sConsummationD_rep);

            return interestCalculationRule;
        }

        /// <summary>
        /// Creates a container holding data on a set of interest calculation rules.
        /// </summary>
        /// <returns>An INTEREST_CALCULATION_RULE container.</returns>
        private INTEREST_CALCULATION_RULES CreateInterestCalculationRules()
        {
            var interestCalculationRules = new INTEREST_CALCULATION_RULES();
            interestCalculationRules.InterestCalculationRule.Add(this.CreateInterestCalculationRule());

            return interestCalculationRules;
        }

        /// <summary>
        /// Creates a container that holds information on a property improvement.
        /// </summary>
        /// <returns>An IMPROVEMENT container.</returns>
        private IMPROVEMENT CreateImprovement()
        {
            var improvement = new IMPROVEMENT();
            improvement.Structure = this.CreateStructure();

            return improvement;
        }

        /// <summary>
        /// Creates a container holding information on an integrated disclosure.
        /// </summary>
        /// <param name="docType">Distinguishes between the Loan Estimate and Closing Disclosure.</param>
        /// <param name="document">An instance of either <see cref="LoanEstimateDates" /> or <see cref="ClosingDisclosureDates" />, depending on the <see cref="DocumentBase" /> parameter.</param>
        /// <param name="currentLoan">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued.</param>
        /// <returns>An INTEGRATED_DISCLOSURE container.</returns>
        private INTEGRATED_DISCLOSURE CreateIntegratedDisclosure(DocumentBase docType, object document, bool currentLoan)
        {
            var integratedDisclosure = new INTEGRATED_DISCLOSURE();
            IntegratedDisclosureDocumentBase documentType = IntegratedDisclosureDocumentBase.Blank;

            if (docType == DocumentBase.LoanEstimate)
            {
                documentType = IntegratedDisclosureDocumentBase.LoanEstimate;
                LoanEstimateDates loanEstimateDates = (LoanEstimateDates)document;

                integratedDisclosure.IntegratedDisclosureDetail = this.CreateIntegratedDisclosureDetail(documentType, loanEstimateDates.IssuedDate, loanEstimateDates.ReceivedDate, loanEstimateDates.UniqueId, loanEstimateDates.DeliveryMethod, loanEstimateDates.LastDisclosedTRIDLoanProductDescription, currentLoan);
            }
            else if (docType == DocumentBase.ClosingDisclosure)
            {
                documentType = IntegratedDisclosureDocumentBase.ClosingDisclosure;
                ClosingDisclosureDates closingDisclosureDates = (ClosingDisclosureDates)document;

                integratedDisclosure.IntegratedDisclosureDetail = this.CreateIntegratedDisclosureDetail(documentType, closingDisclosureDates.IssuedDate, closingDisclosureDates.ReceivedDate, closingDisclosureDates.UniqueId, closingDisclosureDates.DeliveryMethod, closingDisclosureDates.LastDisclosedTRIDLoanProductDescription, currentLoan);
            }
            else
            {
                return null;
            }

            integratedDisclosure.CashToCloseItems = this.CreateCashToCloseItems(documentType);
            integratedDisclosure.EstimatedPropertyCost = this.CreateEstimatedPropertyCost(documentType);
            integratedDisclosure.IntegratedDisclosureSectionSummaries = this.CreateIntegratedDisclosureSectionSummaries(documentType);
            integratedDisclosure.OtherLoanConsiderationsAndDisclosuresItems = this.CreateOtherLoanConsiderationsAndDisclosuresItems(documentType);
            integratedDisclosure.ProjectedPayments = this.CreateProjectedPayments(documentType);

            if (this.extendIntegratedDisclosure)
            {
                integratedDisclosure.Extension = this.CreateIntegratedDisclosureExtension(documentType);
            }

            return integratedDisclosure;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_EXTENSION container with information that is otherwise captured at the loan level, but not per loan estimate | closing disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_EXTENSION container.</returns>
        private INTEGRATED_DISCLOSURE_EXTENSION CreateIntegratedDisclosureExtension(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var extension = new INTEGRATED_DISCLOSURE_EXTENSION();
            extension.Mismo = new MISMO_INTEGRATED_DISCLOSURE_EXTENSION();

            var loanArchive = documentType == IntegratedDisclosureDocumentBase.LoanEstimate ? this.loanEstimateArchiveData : this.closingDisclosureArchiveData;
            extension.Mismo.FeeInformation = CreateFeeInformation(loanArchive, this.vendor, documentType, this.loanData.sLenderPaidFeeDiscloseLocationT, this.feeIgnoreList);

            if (this.vendor == E_DocumentVendor.UnknownOtherNone)
            {
                extension.Mismo.Adjustment = this.CreateAdjustment(loanArchive);
                extension.Mismo.InterestOnly = CreateInterestOnly(loanArchive.sIOnlyMon, loanArchive.sIOnlyMon_rep, loanArchive.sProThisMPmt_rep);
                extension.Mismo.Payment = CreatePayment(loanArchive, this.isClosingPackage);
                extension.Mismo.TermsOfLoan = CreateTermsOfLoan(loanArchive, this.vendor, this.loanData.BrokerDB.IsEnableHELOC);
            }

            return extension;
        }

        /// <summary>
        /// Creates a container holding details about an integrated disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <param name="issuedDate">The date the document was issued.</param>
        /// <param name="receivedDate">The date the document was received by the borrower.</param>
        /// <param name="disclosureId">The GUID of the Integrated Disclosure object.</param>
        /// <param name="deliveryMethod">The method used to deliver the disclosure to the borrower.</param>
        /// <param name="loanProduct">The loan product name.</param>
        /// <param name="currentLoan">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_DETAIL container.</returns>
        private INTEGRATED_DISCLOSURE_DETAIL CreateIntegratedDisclosureDetail(IntegratedDisclosureDocumentBase documentType, DateTime issuedDate, DateTime receivedDate, Guid disclosureId, E_DeliveryMethodT deliveryMethod, string loanProduct, bool currentLoan)
        {
            var integratedDisclosureDetail = new INTEGRATED_DISCLOSURE_DETAIL();
            integratedDisclosureDetail.IntegratedDisclosureDocumentType = ToIntegratedDisclosureDocumentEnum(documentType);
            integratedDisclosureDetail.IntegratedDisclosureIssuedDate = ToMismoDate(this.loanData.m_convertLos.ToDateTimeString(issuedDate));

            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate && this.hasArchive)
            {
                try
                {
                    integratedDisclosureDetail.FiveYearPrincipalReductionComparisonAmount = ToMismoAmount(this.loanEstimateArchiveData.sTRIDLoanEstimatePrincipalIn5Yrs_rep);
                    integratedDisclosureDetail.FiveYearTotalOfPaymentsComparisonAmount = ToMismoAmount(this.loanEstimateArchiveData.sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs_rep);
                }
                catch (CBaseException) when (this.isLenderExportMode)
                {
                    integratedDisclosureDetail.FiveYearPrincipalReductionComparisonAmount = null;
                    integratedDisclosureDetail.FiveYearTotalOfPaymentsComparisonAmount = null;
                }

                integratedDisclosureDetail.IntegratedDisclosureEstimatedClosingCostsExpirationDatetime = ToMismoDatetime(this.loanEstimateArchiveData.sGfeEstScAvailTillD_rep_WithTime, false);

                integratedDisclosureDetail.IntegratedDisclosureHomeEquityLoanIndicator = ToIntegratedDisclosureHomeEquityLoanIndicator(this.loanEstimateArchiveData.sLPurposeT, this.loanEstimateArchiveData.sIsLineOfCredit);
                integratedDisclosureDetail.IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount = ToMismoAmount(this.loanEstimateArchiveData.sProThisMPmt_rep);
                integratedDisclosureDetail.IntegratedDisclosureLoanProductDescription = ToMismoString(loanProduct);
            }
            else if (documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure && this.hasArchive)
            {
                integratedDisclosureDetail.FirstYearTotalEscrowPaymentAmount = ToMismoAmount(this.closingDisclosureArchiveData.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep);
                ////integratedDisclosureDetail.FirstYearTotalEscrowPaymentDescription;

                if (this.closingDisclosureArchiveData.sTridEscrowAccountExists)
                {
                    integratedDisclosureDetail.FirstYearTotalNonEscrowPaymentAmount = ToMismoAmount(this.closingDisclosureArchiveData.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep);
                    ////integratedDisclosureDetail.FirstYearTotalNonEscrowPaymentDescription;
                }
                else
                {
                    integratedDisclosureDetail.FirstYearTotalNonEscrowPaymentAmount = ToMismoAmount(this.closingDisclosureArchiveData.sClosingDisclosurePropertyCostsFirstYear_rep);
                    ////integratedDisclosureDetail.FirstYearTotalNonEscrowPaymentDescription;
                }

                integratedDisclosureDetail.IntegratedDisclosureHomeEquityLoanIndicator = ToIntegratedDisclosureHomeEquityLoanIndicator(this.closingDisclosureArchiveData.sLPurposeT, this.closingDisclosureArchiveData.sIsLineOfCredit);
                integratedDisclosureDetail.IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount = ToMismoAmount(this.closingDisclosureArchiveData.sProThisMPmt_rep);
                integratedDisclosureDetail.IntegratedDisclosureLoanProductDescription = ToMismoString(loanProduct);
            }

            integratedDisclosureDetail.Extension = this.CreateIntegratedDisclosureDetailExtension(documentType, disclosureId, receivedDate, deliveryMethod, currentLoan);

            return integratedDisclosureDetail;
        }

        /// <summary>
        /// Creates a container with extension data for INTEGRATED_DISCLOSURE_DETAIL.
        /// </summary>
        /// <param name="documentType">The Integrated Disclosure document type.</param>
        /// <param name="disclosureId">The GUID of the Integrated Disclosure object.</param>
        /// <param name="receivedDate">The date the document was received by the borrower.</param>
        /// <param name="deliveryMethod">The method used to deliver the disclosure to the borrower.</param>
        /// <param name="currentLoan">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_DETAIL_EXTENSION container.</returns>
        private INTEGRATED_DISCLOSURE_DETAIL_EXTENSION CreateIntegratedDisclosureDetailExtension(IntegratedDisclosureDocumentBase documentType, Guid disclosureId, DateTime receivedDate, E_DeliveryMethodT deliveryMethod, bool currentLoan)
        {
            var extension = new INTEGRATED_DISCLOSURE_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBIntegratedDisclosureDetailExtension(documentType, disclosureId, receivedDate, deliveryMethod, currentLoan);

            return extension;
        }

        /// <summary>
        /// Creates a container with extension data for INTEGRATED_DISCLOSURE_DETAIL.
        /// </summary>
        /// <param name="documentType">The Integrated Disclosure document type.</param>
        /// <param name="disclosureId">The GUID of the Integrated Disclosure object.</param>
        /// <param name="receivedDate">The date the document was received by the borrower.</param>
        /// <param name="deliveryMethod">The method used to deliver the disclosure to the borrower.</param>
        /// <param name="currentLoan">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued.</param>
        /// <returns>An LQB_INTEGRATED_DISCLOSURE_DETAIL_EXTENSION container.</returns>
        private LQB_INTEGRATED_DISCLOSURE_DETAIL_EXTENSION CreateLQBIntegratedDisclosureDetailExtension(IntegratedDisclosureDocumentBase documentType, Guid disclosureId, DateTime receivedDate, E_DeliveryMethodT deliveryMethod, bool currentLoan)
        {
            var extension = new LQB_INTEGRATED_DISCLOSURE_DETAIL_EXTENSION();
            extension.CurrentLoan = ToMismoIndicator(currentLoan);
            extension.LastDisclosed = ToMismoIndicator(false);
            extension.ReceivedDate = ToMismoDate(this.loanData.m_convertLos.ToDateTimeString(receivedDate));

            if (deliveryMethod != E_DeliveryMethodT.LeaveEmpty)
            {
                extension.DeliveryMethodType = ToMismoString(GetXmlEnumName(deliveryMethod));
            }

            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
            {
                extension.RefinanceIncludingDebtsToBePaidOffAmount = ToMismoAmount(this.loanEstimateArchiveData.sRefPdOffAmt1003_rep);
                extension.BorrowerRequestedLoanAmount = ToMismoAmount(this.loanEstimateArchiveData.sFinalLAmt_rep);
                extension.AlterationsImprovementsAndRepairsAmount = ToMismoAmount(this.loanEstimateArchiveData.sTRIDClosingDisclosureAltCost_rep);
                extension.LandOriginalCostAmount = ToMismoAmount(this.loanEstimateArchiveData.sLandCost_rep);

                if (disclosureId != Guid.Empty && !currentLoan
                    && this.loanData.GetLastDisclosedLoanEstimate() != null
                    && (disclosureId == this.loanData.GetLastDisclosedLoanEstimate().UniqueId
                    || this.loanData.sLoanEstimateDatesInfo.LoanEstimateDatesList.Count() == 1))
                {
                    extension.LastDisclosed = ToMismoIndicator(true);
                }
            }
            else if (documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure)
            {
                extension.RefinanceIncludingDebtsToBePaidOffAmount = ToMismoAmount(this.closingDisclosureArchiveData.sRefPdOffAmt1003_rep);
                extension.BorrowerRequestedLoanAmount = ToMismoAmount(this.closingDisclosureArchiveData.sFinalLAmt_rep);
                extension.AlterationsImprovementsAndRepairsAmount = ToMismoAmount(this.closingDisclosureArchiveData.sTRIDClosingDisclosureAltCost_rep);
                extension.LandOriginalCostAmount = ToMismoAmount(this.closingDisclosureArchiveData.sLandCost_rep);
                extension.ClosingDate = ToMismoDate(this.closingDisclosureArchiveData.sDocMagicClosingD_rep);
                extension.CdEscrowFirstYearAnalysisStartFromType = ToLqbCdEscrowFirstYearAnalysisStartFromEnum(this.closingDisclosureArchiveData.sTridClosingDisclosureCalculateEscrowAccountFromTCalculated);

                if (disclosureId != Guid.Empty && !currentLoan
                    && this.loanData.GetLastDisclosedClosingDisclosure() != null
                    && (disclosureId == this.loanData.GetLastDisclosedClosingDisclosure().UniqueId
                    || this.loanData.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Count() == 1))
                {
                    extension.LastDisclosed = ToMismoIndicator(true);
                }
            }

            return extension;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SECTION_SUMMARIES container with items A-N and other summary information from the Loan Estimate/Closing Disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SECTION_SUMMARIES container with summary information from the given integrated disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SECTION_SUMMARIES CreateIntegratedDisclosureSectionSummaries(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var summaries = new INTEGRATED_DISCLOSURE_SECTION_SUMMARIES();

            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
            {
                foreach (BorrowerClosingCostFeeSection feeSection in this.loanEstimateArchiveData.sClosingCostSet.GetViewBase(E_ClosingCostViewT.LoanClosingCost))
                {
                    //// A, B, C, E, F, G, H
                    summaries.IntegratedDisclosureSectionSummary.Add(
                        this.CreateIntegratedDisclosureSectionSummary(
                        documentType,
                        feeSection,
                        summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));
                }

                //// D, I, J
                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.loanEstimateArchiveData.sTRIDLoanEstimateTotalLoanCosts_rep,
                    IntegratedDisclosureSectionBase.TotalLoanCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.Blank,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.loanEstimateArchiveData.sTRIDLoanEstimateTotalOtherCosts_rep,
                    IntegratedDisclosureSectionBase.TotalOtherCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.Blank,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.loanEstimateArchiveData.sTRIDLoanEstimateTotalClosingCosts_rep,
                    IntegratedDisclosureSectionBase.TotalClosingCosts,
                    string.Empty,
                    this.loanEstimateArchiveData.sTRIDLoanEstimateTotalAllCosts_rep,
                    IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                var lenderCreditAmount = this.loanEstimateArchiveData.sTRIDLoanEstimateLenderCredits_Neg_rep;
                if (this.vendor == E_DocumentVendor.DocMagic && documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
                {
                    lenderCreditAmount = this.loanEstimateArchiveData.sTRIDLoanEstimateGeneralLenderCredits_Neg_rep;
                }

                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    string.Empty,
                    IntegratedDisclosureSectionBase.TotalClosingCosts,
                    string.Empty,
                    lenderCreditAmount,
                    IntegratedDisclosureSubsectionBase.LenderCredits,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));
            }
            else
            {
                //// Closing Disclosure
                //// A, B, C, E, F, G, H
                foreach (var feeSection in this.closingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.GetCompressedView(E_ClosingCostViewT.CombinedLenderTypeEstimate))
                {
                    summaries.IntegratedDisclosureSectionSummary.Add(
                        this.CreateIntegratedDisclosureSectionSummary(
                        documentType,
                        feeSection,
                        summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));
                }

                //// D, I, J
                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.closingDisclosureArchiveData.sTRIDClosingDisclosureTotalLoanCosts_rep,
                    IntegratedDisclosureSectionBase.TotalLoanCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.LoanCostsSubtotal,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.closingDisclosureArchiveData.sTRIDClosingDisclosureTotalOtherCosts_rep,
                    IntegratedDisclosureSectionBase.TotalOtherCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.OtherCostsSubtotal,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.closingDisclosureArchiveData.sTRIDClosingDisclosureTotalClosingCosts_rep,
                    IntegratedDisclosureSectionBase.TotalClosingCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    string.Empty,
                    IntegratedDisclosureSectionBase.TotalClosingCosts,
                    string.Empty,
                    this.closingDisclosureArchiveData.sTRIDNegativeClosingDisclosureGeneralLenderCredits_rep,
                    IntegratedDisclosureSubsectionBase.LenderCredits,
                    this.closingDisclosureArchiveData.sToleranceCure_rep,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));
                
                //// K, L
                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.closingDisclosureArchiveData.sTRIDTotalDueFromBorrowerAtClosing_rep,
                    GetDisclosureSectionForSectionKAdjustments(this.isClosingPackage, this.loanData.sTRIDLoanEstimateCashToCloseCalcMethodT, this.loanData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                summaries.IntegratedDisclosureSectionSummary.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.closingDisclosureArchiveData.sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing_rep,
                    IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.Blank,
                    string.Empty,
                    summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                //// M, N
                if (!this.closingDisclosureArchiveData.sIsRefinancing)
                {
                    summaries.IntegratedDisclosureSectionSummary.Add(
                        this.CreateIntegratedDisclosureSectionSummary(
                        documentType,
                        this.closingDisclosureArchiveData.sTRIDTotalDueToSellerAtClosing_rep,
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        string.Empty,
                        string.Empty,
                        IntegratedDisclosureSubsectionBase.Blank,
                        string.Empty,
                        summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));

                    summaries.IntegratedDisclosureSectionSummary.Add(
                        this.CreateIntegratedDisclosureSectionSummary(
                        documentType,
                        this.closingDisclosureArchiveData.sTRIDTotalDueFromSellerAtClosing_rep,
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        string.Empty,
                        string.Empty,
                        IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal,
                        string.Empty,
                        summaries.IntegratedDisclosureSectionSummary.Count(s => s != null) + 1));
                }
            }

            return summaries;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SECTION_SUMMARY container with information pertaining to a section of the Loan Estimate/Closing Disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <param name="feeSection">A <see cref="BorrowerClosingCostFeeSection" /> object with fee data from a section of the given integrated disclosure.</param>
        /// <param name="sequence">The sequence number of the summary within the list of summaries.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SECTION_SUMMARY container with information pertaining to a section of the Loan Estimate/Closing Disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SECTION_SUMMARY CreateIntegratedDisclosureSectionSummary(IntegratedDisclosureDocumentBase documentType, BorrowerClosingCostFeeSection feeSection, int sequence)
        {
            if (!this.hasArchive || feeSection.SectionType == E_IntegratedDisclosureSectionT.LeaveBlank || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var summary = new INTEGRATED_DISCLOSURE_SECTION_SUMMARY();
            summary.SequenceNumber = sequence;
            
            IntegratedDisclosureSectionBase section = GetIntegratedDisclosureSectionBaseValue(documentType, feeSection.SectionType);
            
            summary.IntegratedDisclosureSectionSummaryDetail = this.CreateIntegratedDisclosureSectionSummaryDetail(
                documentType == IntegratedDisclosureDocumentBase.LoanEstimate ? feeSection.TotalAmount_rep : feeSection.TotalAmountBorrPaid_rep, 
                section, 
                feeSection.SectionName, 
                string.Empty, 
                IntegratedDisclosureSubsectionBase.Blank, 
                string.Empty);

            ////summary.IntegratedDisclosureSubsectionPayments;

            return summary;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SECTION_SUMMARY container with information pertaining to a section of the Loan Estimate/Closing Disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <param name="sectionAmount">The section total amount.</param>
        /// <param name="section">The section type.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="subsectionAmount">The subsection total amount.</param>
        /// <param name="subsection">The subsection type.</param>
        /// <param name="toleranceCureAmount">The amount of the section total allocated to tolerance cures, if any.</param>
        /// <param name="sequence">The sequence number of the summary within the list of summaries.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SECTION_SUMMARY container with information pertaining to a section of the Loan Estimate/Closing Disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SECTION_SUMMARY CreateIntegratedDisclosureSectionSummary(IntegratedDisclosureDocumentBase documentType, string sectionAmount, IntegratedDisclosureSectionBase section, string sectionName, string subsectionAmount, IntegratedDisclosureSubsectionBase subsection, string toleranceCureAmount, int sequence)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var summary = new INTEGRATED_DISCLOSURE_SECTION_SUMMARY();
            summary.SequenceNumber = sequence;
            summary.IntegratedDisclosureSectionSummaryDetail = this.CreateIntegratedDisclosureSectionSummaryDetail(sectionAmount, section, sectionName, subsectionAmount, subsection, toleranceCureAmount);

            if (documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure)
            {
                summary.IntegratedDisclosureSubsectionPayments = this.CreateIntegratedDisclosureSubsectionPayments_CD(section, subsection);
            }
            else if (this.vendor == E_DocumentVendor.DocMagic)
            {
                //// 10/26/2015 BB case 229689
                summary.IntegratedDisclosureSubsectionPayments = this.CreateIntegratedDisclosureSubsectionPayments_LE(section, subsection, subsectionAmount);
            }

            return summary;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT container with information pertaining to a subsection payment amount on the integrated disclosure.
        /// </summary>
        /// <param name="paidBy">The type of party that has / will make the payment (e.g. buyer | seller | other).</param>
        /// <param name="amount">The payment amount.</param>
        /// <param name="timing">Whether the payment will be made at closing or prior to.</param>
        /// <param name="sequence">The sequence number of the payment among the list of payments.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT container with information pertaining to a subsection payment amount on the integrated disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT CreateIntegratedDisclosureSubsectionPayment(IntegratedDisclosureSubsectionPaidByBase paidBy, string amount, IntegratedDisclosureSubsectionPaymentTimingBase timing, int sequence)
        {
            var payment = new INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT();
            payment.IntegratedDisclosureSubsectionPaidByType = ToIntegratedDisclosureSubsectionPaidByEnum(paidBy);
            payment.IntegratedDisclosureSubsectionPaymentAmount = ToMismoAmount(amount);
            payment.IntegratedDisclosureSubsectionPaymentTimingType = ToIntegratedDisclosureSubsectionPaymentTimingEnum(timing);
            payment.SequenceNumber = sequence;

            return payment;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container with a list of payments pertaining to a subsection of a closing disclosure.
        /// </summary>
        /// <param name="section">The section type.</param>
        /// <param name="subsection">The subsection type.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container with a list of payments pertaining to a subsection of a closing disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS CreateIntegratedDisclosureSubsectionPayments_CD(IntegratedDisclosureSectionBase section, IntegratedDisclosureSubsectionBase subsection)
        {
            var payments = new INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS();
            payments.IntegratedDisclosureSubsectionPayment = new List<INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT>();

            Func<LoanClosingCostFee, bool> sectionPredicate;
            string amountAtClosing = string.Empty;
            string amountBeforeClosing = string.Empty;
            string sellerAmountAtClosing = string.Empty;
            string sellerAmountBeforeClosing = string.Empty;
            string amountPaidByOther = string.Empty;

            //// To-do: move these to the data-layer. Both the page and this/any export should pull these values from the data-layer rather than calculating them on the fly.
            if (section == IntegratedDisclosureSectionBase.TotalLoanCosts && subsection == IntegratedDisclosureSubsectionBase.LoanCostsSubtotal)
            {
                sectionPredicate = p =>
                p.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage) == E_IntegratedDisclosureSectionT.SectionA ||
                p.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage) == E_IntegratedDisclosureSectionT.SectionB ||
                p.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage) == E_IntegratedDisclosureSectionT.SectionC;

                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalLoanCostsOfBorrowerAtClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalLoanCostsOfBorrowerBeforeClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);

                amountAtClosing = this.closingDisclosureArchiveData.m_convertLos.ToMoneyString(this.closingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalLoanCostsOfBorrowerAtClosingFeesPred), FormatDirection.ToRep);
                amountBeforeClosing = this.closingDisclosureArchiveData.m_convertLos.ToMoneyString(this.closingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalLoanCostsOfBorrowerBeforeClosingFeesPred), FormatDirection.ToRep);
            }
            else if (section == IntegratedDisclosureSectionBase.TotalOtherCosts && subsection == IntegratedDisclosureSubsectionBase.OtherCostsSubtotal)
            {
                sectionPredicate = p =>
                p.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage) == E_IntegratedDisclosureSectionT.SectionE ||
                p.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage) == E_IntegratedDisclosureSectionT.SectionF ||
                p.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage) == E_IntegratedDisclosureSectionT.SectionG ||
                p.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage) == E_IntegratedDisclosureSectionT.SectionH;

                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalOtherBorrowerCostsAtClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalOtherBorrowerCostsBeforeClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);

                amountAtClosing = this.closingDisclosureArchiveData.m_convertLos.ToMoneyString(this.closingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalOtherBorrowerCostsAtClosingFeesPred), FormatDirection.ToRep);
                amountBeforeClosing = this.closingDisclosureArchiveData.m_convertLos.ToMoneyString(this.closingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalOtherBorrowerCostsBeforeClosingFeesPred), FormatDirection.ToRep);
            }
            else if (section == IntegratedDisclosureSectionBase.TotalClosingCosts && subsection == IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal)
            {
                sectionPredicate = p => p != null;

                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalAllCostsBorrowerAtClosingPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalAllCostsSellerBeforeClosingPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Seller);
                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalAllCostsOtherPred = p => (p.PaidByT != E_ClosingCostFeePaymentPaidByT.BorrowerFinance && p.PaidByT != E_ClosingCostFeePaymentPaidByT.Borrower && p.PaidByT != E_ClosingCostFeePaymentPaidByT.Seller);

                amountAtClosing = this.closingDisclosureArchiveData.m_convertLos.ToMoneyString(this.closingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalAllCostsBorrowerAtClosingPred), FormatDirection.ToRep);
                amountBeforeClosing = this.closingDisclosureArchiveData.sTRIDClosingDisclosureClosingCostsPaidBeforeClosing_rep;

                if (!this.closingDisclosureArchiveData.sIsRefinancing)
                {
                    sellerAmountAtClosing = this.closingDisclosureArchiveData.sTotalSellerPaidClosingCostsPaidAtClosing_rep;
                    sellerAmountBeforeClosing = this.closingDisclosureArchiveData.m_convertLos.ToMoneyString(this.closingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalAllCostsSellerBeforeClosingPred), FormatDirection.ToRep);
                }

                amountPaidByOther = this.closingDisclosureArchiveData.m_convertLos.ToMoneyString(this.closingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalAllCostsOtherPred), FormatDirection.ToRep);
            }
            else if (section == IntegratedDisclosureSectionBase.TotalClosingCosts && subsection == IntegratedDisclosureSubsectionBase.LenderCredits)
            {
                amountAtClosing = this.closingDisclosureArchiveData.sTRIDNegativeClosingDisclosureGeneralLenderCredits_rep;
            }
            else if (section == IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing && subsection == IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal)
            {
                amountAtClosing = this.closingDisclosureArchiveData.sTRIDClosingDisclosureBorrowerCostsPaidAtClosing_rep;
            }
            else if (section == IntegratedDisclosureSectionBase.DueFromSellerAtClosing && subsection == IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal)
            {
                amountAtClosing = this.closingDisclosureArchiveData.sTotalSellerPaidClosingCostsPaidAtClosing_rep;
            }
            else
            {
                return null;
            }
            //// End-of needs to be moved.

            payments.IntegratedDisclosureSubsectionPayment.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                IntegratedDisclosureSubsectionPaidByBase.Buyer,
                amountAtClosing,
                IntegratedDisclosureSubsectionPaymentTimingBase.AtClosing,
                1));

            if (!string.IsNullOrEmpty(amountBeforeClosing))
            {
                payments.IntegratedDisclosureSubsectionPayment.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                    IntegratedDisclosureSubsectionPaidByBase.Buyer,
                    amountBeforeClosing,
                    IntegratedDisclosureSubsectionPaymentTimingBase.BeforeClosing,
                    2));
            }

            if (!string.IsNullOrEmpty(sellerAmountAtClosing))
            {
                payments.IntegratedDisclosureSubsectionPayment.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                    IntegratedDisclosureSubsectionPaidByBase.Seller,
                    sellerAmountAtClosing,
                    IntegratedDisclosureSubsectionPaymentTimingBase.AtClosing,
                    payments.IntegratedDisclosureSubsectionPayment.Count(p => p != null) + 1));
            }

            if (!string.IsNullOrEmpty(sellerAmountBeforeClosing))
            {
                payments.IntegratedDisclosureSubsectionPayment.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                    IntegratedDisclosureSubsectionPaidByBase.Seller,
                    sellerAmountBeforeClosing,
                    IntegratedDisclosureSubsectionPaymentTimingBase.BeforeClosing,
                    payments.IntegratedDisclosureSubsectionPayment.Count(p => p != null) + 1));
            }

            if (!string.IsNullOrEmpty(amountPaidByOther))
            {
                payments.IntegratedDisclosureSubsectionPayment.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                    IntegratedDisclosureSubsectionPaidByBase.ThirdParty,
                    amountPaidByOther,
                    IntegratedDisclosureSubsectionPaymentTimingBase.Blank,
                    payments.IntegratedDisclosureSubsectionPayment.Count(p => p != null) + 1));
            }

            return payments;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container with a list of payments pertaining to a subsection of a loan estimate.
        /// </summary>
        /// <param name="section">The section type.</param>
        /// <param name="subsection">The subsection type.</param>
        /// <param name="subsectionAmount">The payment amount.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container with a list of payments pertaining to a subsection of a loan estimate.</returns>
        private INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS CreateIntegratedDisclosureSubsectionPayments_LE(IntegratedDisclosureSectionBase section, IntegratedDisclosureSubsectionBase subsection, string subsectionAmount)
        {
            if (section == IntegratedDisclosureSectionBase.Blank || subsection != IntegratedDisclosureSubsectionBase.LenderCredits)
            {
                return null;
            }

            var payments = new INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS();

            payments.IntegratedDisclosureSubsectionPayment.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                IntegratedDisclosureSubsectionPaidByBase.Buyer,
                subsectionAmount,
                IntegratedDisclosureSubsectionPaymentTimingBase.Blank,
                1));

            return payments;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL container with section and subsection totals from the closing cost details area of the integrated disclosure.
        /// </summary>
        /// <param name="sectionAmount">The section total amount.</param>
        /// <param name="section">The section type.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="subsectionAmount">The subsection total amount.</param>
        /// <param name="subsection">The subsection type.</param>
        /// <param name="toleranceCureAmount">The amount of the section total allocated to tolerance cures, if any.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL container with section and subsection totals.</returns>
        private INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL CreateIntegratedDisclosureSectionSummaryDetail(string sectionAmount, IntegratedDisclosureSectionBase section, string sectionName, string subsectionAmount, IntegratedDisclosureSubsectionBase subsection, string toleranceCureAmount)
        {
            if (section == IntegratedDisclosureSectionBase.Blank || (string.IsNullOrEmpty(sectionAmount) && string.IsNullOrEmpty(subsectionAmount)))
            {
                return null;
            }

            var summaryDetail = new INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL();

            if (!string.IsNullOrEmpty(sectionAmount))
            {
                summaryDetail.IntegratedDisclosureSectionTotalAmount = ToMismoAmount(sectionAmount);
            }

            summaryDetail.IntegratedDisclosureSectionType = ToIntegratedDisclosureSectionEnum(section);

            if (section == IntegratedDisclosureSectionBase.Other)
            {
                summaryDetail.IntegratedDisclosureSectionTypeOtherDescription = ToMismoString(sectionName);
            }

            if (!string.IsNullOrEmpty(subsectionAmount))
            {
                summaryDetail.IntegratedDisclosureSubsectionTotalAmount = ToMismoAmount(subsectionAmount);
            }

            if (subsection != IntegratedDisclosureSubsectionBase.Blank)
            {
                summaryDetail.IntegratedDisclosureSubsectionType = ToIntegratedDisclosureSubsectionEnum(subsection);
            }

            ////summaryDetail.IntegratedDisclosureSubsectionTypeOtherDescription;

            if (!string.IsNullOrEmpty(toleranceCureAmount))
            {
                summaryDetail.LenderCreditToleranceCureAmount = ToMismoAmount(toleranceCureAmount);
            }

            return summaryDetail;
        }

        /// <summary>
        /// Creates a container with information on an individual agent.
        /// </summary>
        /// <param name="agent">The agent to be converted to MISMO.</param>
        /// <param name="contactType">The type of the agent's contact info (Work, Home, etc.).</param>
        /// <returns>An INDIVIDUAL container.</returns>
        private INDIVIDUAL CreateIndividual(CAgentFields agent, ContactPointRoleBase contactType)
        {
            var individual = new INDIVIDUAL();

            var name = agent.AgentName;
            if (agent.AgentRoleT == E_AgentRoleT.Lender && string.IsNullOrEmpty(agent.AgentName))
            {
                name = this.loanData.sEmployeeLoanRepName;
            }

            individual.Name = CreateName(name, isLegalEntity: false);

            // Special case for Trustee
            if (agent.AgentRoleT == E_AgentRoleT.Trustee)
            {
                contactType = ContactPointRoleBase.Blank;
            }

            individual.ContactPoints = CreateContactPoints(agent, contactType, EntityType.Individual);

            return individual;
        }

        /// <summary>
        /// Creates a container representing an individual borrower.
        /// </summary>
        /// <param name="appData">The application holding the borrower data.</param>
        /// <returns>An INDIVIDUAL container representing a borrower.</returns>
        private INDIVIDUAL CreateIndividual(CAppData appData)
        {
            var individual = new INDIVIDUAL();

            if (appData.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                individual.Aliases = this.CreateAliases(appData.aBAliases.ToArray());
            }
            else
            {
                individual.Aliases = this.CreateAliases(appData.aCAliases.ToArray());
            }

            individual.Name = CreateName(appData.aFirstNm, appData.aMidNm, appData.aLastNm, appData.aSuffix, appData.aNm);
            individual.ContactPoints = this.CreateContactPoints(appData);
            individual.IdentificationVerification = this.CreateIdentificationVerification(appData);

            return individual;
        }

        /// <summary>
        /// Creates a container representing an individual trustee.
        /// </summary>
        /// <param name="trustee">The data for the trustee.</param>
        /// <returns>An INDIVIDUAL container.</returns>
        private INDIVIDUAL CreateIndividual(TrustCollection.Trustee trustee)
        {
            var individual = new INDIVIDUAL();
            individual.Name = CreateName(trustee.Name, isLegalEntity: false);
            individual.ContactPoints = this.CreateContactPoints(trustee.Phone, string.Empty, string.Empty, ContactPointRoleBase.Blank);

            return individual;
        }

        /// <summary>
        /// Creates a basic INDIVIDUAL container with a name and contact information.
        /// </summary>
        /// <param name="name">The name of the individual.</param>
        /// <param name="phone">The phone number of the individual.</param>
        /// <param name="fax">The fax number of the individual.</param>
        /// <param name="email">The email of the individual.</param>
        /// <param name="contactPointType">The type of contact point, home | work | etc.</param>
        /// <returns>An INDIVIDUAL container with a name and contact information.</returns>
        private INDIVIDUAL CreateIndividual(string name, string phone, string fax, string email, ContactPointRoleBase contactPointType)
        {
            var individual = new INDIVIDUAL();
            individual.Name = CreateName(name, isLegalEntity: false);
            individual.ContactPoints = CreateContactPoints(phone, fax, email, contactPointType);

            return individual;
        }

        /// <summary>
        /// Creates a basic INDIVIDUAL container with just a name.
        /// </summary>
        /// <param name="name">The individual's name.</param>
        /// <returns>An INDIVUDAL container with the individual's name.</returns>
        private INDIVIDUAL CreateIndividual(string name)
        {
            return this.CreateIndividual(name, string.Empty, string.Empty, string.Empty, ContactPointRoleBase.Blank);
        }

        /// <summary>
        /// Creates an INVESTOR_LOAN_INFORMATION container.
        /// </summary>
        /// <returns>An INVESTOR_LOAN_INFORMATION container.</returns>
        private INVESTOR_LOAN_INFORMATION CreateInvestorLoanInformation()
        {
            var investorLoanInformation = new INVESTOR_LOAN_INFORMATION();
            investorLoanInformation.Extension = this.CreateInvestorLoanInformationExtension();

            return investorLoanInformation;
        }

        /// <summary>
        /// Creates an extension holding investor loan information.
        /// </summary>
        /// <returns>An INVESTOR_LOAN_INFORMATION_EXTENSION container.</returns>
        private INVESTOR_LOAN_INFORMATION_EXTENSION CreateInvestorLoanInformationExtension()
        {
            var extension = new INVESTOR_LOAN_INFORMATION_EXTENSION();
            extension.Other = this.CreateLqbInvestorLoanInformationExtension();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container holding investor loan information.
        /// </summary>
        /// <returns>An LQB_INVESTOR_LOAN_INFORMATION_EXTENSION container.</returns>
        private LQB_INVESTOR_LOAN_INFORMATION_EXTENSION CreateLqbInvestorLoanInformationExtension()
        {
            var extension = new LQB_INVESTOR_LOAN_INFORMATION_EXTENSION();
            extension.InvestorProgramIdentifier = ToMismoIdentifier(this.loanData.sInvestorLockProgramId);

            return extension;
        }

        /// <summary>
        /// Creates a container holding nodes with information on late charges.
        /// </summary>
        /// <returns>A LATE_CHARGE container.</returns>
        private LATE_CHARGE CreateLateCharge()
        {
            var lateCharge = new LATE_CHARGE();
            lateCharge.LateChargeRule = this.CreateLateChargeRule();

            return lateCharge;
        }

        /// <summary>
        /// Creates a container holding data on the late charge rules.
        /// </summary>
        /// <returns>A LATE_CHARGE_RULE container.</returns>
        private LATE_CHARGE_RULE CreateLateChargeRule()
        {
            var lateChargeRule = new LATE_CHARGE_RULE();
            lateChargeRule.LateChargeGracePeriodDaysCount = ToMismoCount(this.loanData.sLateDays, this.loanData.m_convertLos);
            lateChargeRule.LateChargeRatePercent = ToMismoPercent(this.loanData.sLateChargePc.Replace("%", string.Empty));

            return lateChargeRule;
        }

        /// <summary>
        /// Creates a container holding a legal description.
        /// </summary>
        /// <param name="sequence">The sequence number of the legal description among the list of descriptions.</param>
        /// <returns>A LEGAL_DESCRIPTION container.</returns>
        private LEGAL_DESCRIPTION CreateLegalDescription(int sequence)
        {
            var legalDescription = new LEGAL_DESCRIPTION();
            legalDescription.UnparsedLegalDescriptions = this.CreateUnparsedLegalDescriptions();
            legalDescription.ParcelIdentifications = this.CreateParcelIdentifications();
            legalDescription.SequenceNumber = sequence;

            return legalDescription;
        }

        /// <summary>
        /// Creates a container to hold legal descriptions.
        /// </summary>
        /// <returns>A LEGAL_DESCRIPTIONS container.</returns>
        private LEGAL_DESCRIPTIONS CreateLegalDescriptions()
        {
            var legalDescriptions = new LEGAL_DESCRIPTIONS();
            legalDescriptions.LegalDescription.Add(this.CreateLegalDescription(1));

            return legalDescriptions;
        }

        /// <summary>
        /// Creates a container holding a company's name and telephone number.
        /// </summary>
        /// <param name="companyName">The name of the company.</param>
        /// <param name="telephoneNumber">The company's telephone number.</param>
        /// <param name="faxNumber">The company's fax number.</param>
        /// <param name="email">An email address associated with the company or a representative.</param>
        /// <param name="contactName">A NAME object containing the name of a person associated with the legal entity.</param>
        /// <param name="contactPointType">The locale / type of the contact point, e.g. work | home | mobile.</param>
        /// <param name="mersOrganizationId">The MERSOrganizationIdentifier to be populated.</param>
        /// <returns>A LEGAL_ENTITY container holding a company's name and telephone number.</returns>
        private LEGAL_ENTITY CreateLegalEntity(string companyName, string telephoneNumber, string faxNumber, string email, NAME contactName, ContactPointRoleBase contactPointType, string mersOrganizationId = null)
        {
            LEGAL_ENTITY legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(companyName, mersOrganizationId);

            if (!(string.IsNullOrEmpty(telephoneNumber) && string.IsNullOrEmpty(faxNumber) && string.IsNullOrEmpty(email)))
            {
                legalEntity.Contacts = this.CreateContacts(telephoneNumber, faxNumber, email, contactName, contactPointType);
            }

            return legalEntity;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY container for a property seller.
        /// </summary>
        /// <param name="companyName">The company name.</param>
        /// <param name="sellerType">The type of property seller.</param>
        /// <returns>A LEGAL_ENTITY container.</returns>
        private LEGAL_ENTITY CreateLegalEntity(string companyName, E_SellerEntityType sellerType)
        {
            var legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(companyName, sellerType);

            return legalEntity;
        }

        /// <summary>
        /// Creates LIABILITIES container populated with liabilities from loan file.
        /// </summary>
        /// <returns>Returns a LIABILITIES container populated with liabilities from loan file.</returns>
        private LIABILITIES CreateLiabilities()
        {
            LIABILITIES liabilities = new LIABILITIES();

            bool isPayoffManuallySpecified = this.loanData.sRefPdOffAmt1003Lckd && !IsAmountStringBlankOrZero(this.loanData.sRefPdOffAmt1003_rep);
            bool includeManualPayoff = isPayoffManuallySpecified && this.vendor != E_DocumentVendor.Simplifile;

            if (includeManualPayoff)
            {
                liabilities.Liability.Add(this.CreateLiabilityManualPayoff(this.loanData.sRefPdOffAmt1003_rep));
            }

            var validApps = this.loanData.Apps.Where(app => app.aBIsValidNameSsn || app.aCIsValidNameSsn);
            foreach (CAppData appData in validApps)
            {
                ILiaCollection liabilityCollection = appData.aLiaCollection;
                for (int recordIndex = 0; recordIndex < liabilityCollection.CountRegular; recordIndex++)
                {
                    ILiabilityRegular liability = liabilityCollection.GetRegularRecordAt(recordIndex);

                    IRealEstateOwned associatedReo = null;
                    if (liability.MatchedReRecordId != Guid.Empty)
                    {
                        associatedReo = appData.aReCollection.GetRegRecordOf(liability.MatchedReRecordId);
                    }

                    liabilities.Liability.Add(
                        this.CreateLiabilityBorrower(
                            liability, 
                            associatedReo, 
                            appData.aBMismoId, 
                            appData.aCMismoId, 
                            appData.aCIsValidNameSsn, 
                            includeManualPayoff));
                }
            }

            if ((!this.isClosingPackage && this.loanData.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative)
                || (this.isClosingPackage && this.loanData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative))
            {
                foreach (var adjustment in this.loanData.sAdjustmentList.Where(adj => adj.AmountToBorrower != 0 && (adj.IsPaidToBorrower || adj.IsPaidFromBorrower)))
                {
                    liabilities.Liability.Add(
                        this.CreateLiabilityAdjustmentsAndOtherCredits(
                            adjustment.Description, 
                            -adjustment.AmountToBorrower, 
                            adjustment.AdjustmentType,
                            !adjustment.IncludeAdjustmentInLeCdForThisLien, 
                            this.primaryApp.aBMismoId));
                }

                if (this.loanData.sONewFinNetProceeds != 0)
                {
                    liabilities.Liability.Add(
                        this.CreateLiabilityAdjustmentsAndOtherCredits(
                            "Subordinate financing",
                            -this.loanData.sONewFinNetProceeds,
                            null,
                            null,
                            this.primaryApp.aBMismoId));
                }

                if (this.loanData.sTRIDClosingDisclosureAltCost != 0)
                {
                    liabilities.Liability.Add(
                        this.CreateLiabilityAdjustmentsAndOtherCredits(
                            this.loanData.sTRIDClosingDisclosureAltCostDescription, 
                            this.loanData.sTRIDClosingDisclosureAltCost,
                            null,
                            null,
                            this.primaryApp.aBMismoId));
                }

                if (this.loanData.sLandCost != 0)
                {
                    liabilities.Liability.Add(
                        this.CreateLiabilityAdjustmentsAndOtherCredits(
                            "Land Cost", 
                            this.loanData.sLandCost,
                            null,
                            null,
                            this.primaryApp.aBMismoId));
                }
            }

            ////https://www.fanniemae.com/content/technology_requirements/ucd-closing-disclosure-reference-numbers-appendix-c-purchase.pdf
            //// \\megatron\lendersoffice\Integration\UCD\UCDEditedDraft.yml, 15.4.1, 15.5.1, 15.8.1

            liabilities.Liability.Add(
                this.CreateLiabilitySeller(
                    LiabilityBase.FirstPositionMortgageLien, 
                    string.Empty, 
                    this.loanData.sReductionsDueToSellerPayoffOf1stMtgLoan_rep));

            liabilities.Liability.Add(
                this.CreateLiabilitySeller(
                    LiabilityBase.SecondPositionMortgageLien,
                    string.Empty,
                    this.loanData.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep));

            return liabilities;
        }

        /// <summary>
        /// Creates LIABILITY container from borrower liability entry.
        /// </summary>
        /// <param name="liabilityData">The borrower's liability entry.</param>
        /// <param name="associatedReo">The real estate record associated with the liability, if one exists.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <param name="coborrowerLabel">The $$xlink$$ label for the co-borrower (role).</param>
        /// <param name="coborrowerIsValidSSN">Whether the co borrower's SSN is valid, from the app data (for determining whether to make a RELATIONSHIP with co borrower).</param>
        /// <param name="isPayoffManuallySpecifiedIn1003DOTLineD">Boolean indicating whether payoff is manually specified in Details of Transaction line d. If true, a separate liability element to handle the manual payoff will be created, so there should be no payoffs added to this liability element.</param>
        /// <returns>Returns a LIABILITY container populated with borrower liability entry information.</returns>
        private LIABILITY CreateLiabilityBorrower(ILiabilityRegular liabilityData, IRealEstateOwned associatedReo, string borrowerLabel, string coborrowerLabel, bool coborrowerIsValidSSN, bool isPayoffManuallySpecifiedIn1003DOTLineD)
        {
            if (this.vendor == E_DocumentVendor.DocMagic && liabilityData.PayoffTiming == E_Timing.Before_Closing && !this.loanData.BrokerDB.IncludeLiabilitiesPaidBeforeClosingInDocMagicMismo33Payload)
            {
                return null;
            }

            bool includePayoff = !isPayoffManuallySpecifiedIn1003DOTLineD && liabilityData.PayoffTiming != E_Timing.Before_Closing && 
                liabilityData.WillBePdOff && !IsAmountStringBlankOrZero(liabilityData.PayoffAmt_rep);
            var liability = new LIABILITY();

            liability.LiabilityDetail = this.CreateLiabilityDetail(liabilityData, associatedReo);
            liability.LiabilityHolder = this.CreateLiabilityHolder(liabilityData);
            liability.Payoff = includePayoff ? this.CreatePayoff_Borrower(liabilityData.PayoffAmt_rep) : null;
            liability.SequenceNumber = this.counters.Liabilities;
            liability.label = "Liability" + liability.SequenceNumberSerialized;

            if (liabilityData.OwnerT == E_LiaOwnerT.Borrower || liabilityData.OwnerT == E_LiaOwnerT.Joint)
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.label, borrowerLabel));
            }

            if (coborrowerIsValidSSN && (liabilityData.OwnerT == E_LiaOwnerT.CoBorrower || liabilityData.OwnerT == E_LiaOwnerT.Joint))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.label, coborrowerLabel));
            }

            string reoAssetXlinkLabel = string.Empty;
            this.reoAssetLabel.TryGetValue(liabilityData.MatchedReRecordId.ToString("D"), out reoAssetXlinkLabel);
            if (!string.IsNullOrEmpty(reoAssetXlinkLabel))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "LIABILITY", reoAssetXlinkLabel, liability.label));
            }

            return liability;
        }

        /// <summary>
        /// Creates a liability from an adjustment on the "Adjustments and other credits" list on the "Adjustments and Other Credits" page, for use with the alternate LE/CD form.
        /// </summary>
        /// <param name="description">The description for the LIABILITY instance.</param>
        /// <param name="amountFromBorrower">The amount for the liability.</param>
        /// <param name="adjustmentType">The adjustment type from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="excludeFromLECDForThisLien">The adjustment exclude from CE LE setting from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="borrowerLabel">The label of the borrower to whom the liability will belong.</param>
        /// <returns>A LIABILITY container with the amount from the borrower (negative if TO the borrower).</returns>
        private LIABILITY CreateLiabilityAdjustmentsAndOtherCredits(string description, decimal amountFromBorrower, E_AdjustmentT? adjustmentType, bool? excludeFromLECDForThisLien, string borrowerLabel)
        {
            var liability = new LIABILITY();

            liability.LiabilityDetail = new LIABILITY_DETAIL
            {
                LiabilityDescription = ToMismoString(description),
                LiabilityExclusionIndicator = ToMismoIndicator(true),
                LiabilityPayoffStatusIndicator = ToMismoIndicator(true),
            };

            liability.Payoff = this.CreatePayoff(this.loanData.m_convertLos.ToMoneyString(amountFromBorrower, FormatDirection.ToRep), adjustmentType, excludeFromLECDForThisLien, IntegratedDisclosureSectionBase.PayoffsAndPayments);

            liability.SequenceNumber = this.counters.Liabilities;
            liability.label = "Liability" + liability.SequenceNumberSerialized;

            this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.label, borrowerLabel));

            return liability;
        }

        /// <summary>
        /// Creates LIABILITY container holding overriding refinance payoff amount on 1003 Detail of Transaction, section VII d.
        /// </summary>
        /// <param name="payoffAmount">Amount of payoff.</param>
        /// <returns>Returns a LIABILITY container holding overriding refinance payoff amount on 1003 Detail of Transaction, section VII d.</returns>
        private LIABILITY CreateLiabilityManualPayoff(string payoffAmount)
        {
            var liability = new LIABILITY();

            liability.Payoff = this.CreatePayoff_Borrower(payoffAmount);
            liability.SequenceNumber = this.counters.Liabilities;
            liability.label = "Liability" + liability.SequenceNumberSerialized;

            this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.label, this.primaryApp.aBMismoId));

            return liability;
        }

        /// <summary>
        /// Creates LIABILITY container intended for SELLER party role.
        /// </summary>
        /// <param name="liabilityType">Specified type of liability.</param>
        /// <param name="liabilityTypeOtherDescription">Free-form description of other liability type.</param>
        /// <param name="payoffAmount">Amount of seller payoff.</param>
        /// <returns>Returns a LIABILITY container intended for SELLER party role.</returns>
        private LIABILITY CreateLiabilitySeller(LiabilityBase liabilityType, string liabilityTypeOtherDescription, string payoffAmount)
        {
            if (IsAmountStringBlankOrZero(payoffAmount) || (liabilityType == LiabilityBase.Other && string.IsNullOrEmpty(liabilityTypeOtherDescription)))
            {
                return null;
            }

            var liability = new LIABILITY();
            liability.LiabilityDetail = this.CreateLiabilityDetail(liabilityType, liabilityTypeOtherDescription);
            liability.Payoff = this.CreatePayoff_Seller(payoffAmount);
            liability.SequenceNumber = this.counters.Liabilities;
            liability.label = "Liability" + liability.SequenceNumberSerialized;

            foreach (var sellerLabel in this.partyLabels.RetrieveLabels(PartyRoleBase.PropertySeller, EntityType.Individual))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.label, sellerLabel));
            }
            
            return liability;
        }

        /// <summary>
        /// Creates LIABILITY_DETAIL container from borrower liability entry.
        /// </summary>
        /// <param name="liability">The borrower's liability entry.</param>
        /// <param name="associatedReo">The real estate record associated with the liability, if one exists.</param>
        /// <returns>Returns a LIABILITY_DETAIL container populated with borrower liability entry information.</returns>
        private LIABILITY_DETAIL CreateLiabilityDetail(ILiabilityRegular liability, IRealEstateOwned associatedReo)
        {
            var liabilityDetail = new LIABILITY_DETAIL();
            liabilityDetail.LiabilityAccountIdentifier = ToMismoIdentifier(liability.AccNum.Value, isSensitiveData: true);
            liabilityDetail.LiabilityMonthlyPaymentAmount = ToMismoAmount(liability.Pmt_rep, isSensitiveData: true);
            liabilityDetail.LiabilityPayoffStatusIndicator = ToMismoIndicator(liability.WillBePdOff, isSensitiveData: true);
            liabilityDetail.LiabilityRemainingTermMonthsCount = ToMismoCount(liability.RemainMons_rep, null, isSensitiveData: true);

            var liabilityType = GetLiabilityBaseValue(liability.DebtT);
            liabilityDetail.LiabilityType = ToLiabilityEnum(liabilityType, string.IsNullOrEmpty(liability.Desc) ? liabilityType.ToString() : liability.Desc);
            if ((liabilityDetail.LiabilityType?.enumValue ?? LiabilityBase.Blank) == LiabilityBase.Other)
            {
                liabilityDetail.LiabilityTypeOtherDescription = ToMismoString(string.IsNullOrEmpty(liability.Desc) ? liability.DebtT_rep : liability.Desc);
            }

            liabilityDetail.LiabilityUnpaidBalanceAmount = ToMismoAmount(liability.Bal_rep, isSensitiveData: true);
            liabilityDetail.LiabilityPayoffWithCurrentAssetsIndicator = ToLiabilityPayoffWithCurrentAssetsIndicator(liability.PayoffTiming);

            if (this.vendor == E_DocumentVendor.UnknownOtherNone)
            {
                liabilityDetail.LiabilityExclusionIndicator = ToLiabilityExclusionIndicator(liability.DebtT, liability.WillBePdOff, liability.NotUsedInRatio);
            }
            else
            {
                bool excludeLiabilityFromUnderwriting = false;
                if (liability.DebtT == E_DebtRegularT.Mortgage)
                {
                    excludeLiabilityFromUnderwriting = liability.ExcFromUnderwriting || (associatedReo != null && associatedReo.StatT == E_ReoStatusT.Sale);
                }
                else
                {
                    excludeLiabilityFromUnderwriting = liability.ExcFromUnderwriting;
                }

                liabilityDetail.LiabilityExclusionIndicator = ToMismoIndicator(excludeLiabilityFromUnderwriting, isSensitiveData: true);
            }

            return liabilityDetail;
        }

        /// <summary>
        /// Creates LIABILITY_DETAIL container with type and other-type description specified.
        /// </summary>
        /// <param name="liabilityType">The type of liability.</param>
        /// <param name="liabilityTypeOtherDescription">Free-form description of other liability type.</param>
        /// <returns>Returns a LIABILITY_DETAIL container with type and other-type description specified.</returns>
        private LIABILITY_DETAIL CreateLiabilityDetail(LiabilityBase liabilityType, string liabilityTypeOtherDescription)
        {
            string liabilityDisplayLabelText = liabilityTypeOtherDescription;
            if (liabilityType == LiabilityBase.FirstPositionMortgageLien)
            {
                liabilityDisplayLabelText = "Payoff of First Mortgage Loan";
            }
            else if (liabilityType == LiabilityBase.SecondPositionMortgageLien)
            {
                liabilityDisplayLabelText = "Payoff of Second Mortgage Loan";
            }

            var liabilityDetail = new LIABILITY_DETAIL()
                {
                    LiabilityType = ToLiabilityEnum(liabilityType, liabilityDisplayLabelText),
                    LiabilityTypeOtherDescription = liabilityType == LiabilityBase.Other ? ToMismoString(liabilityTypeOtherDescription) : null
                };
            return liabilityDetail;
        }

        /// <summary>
        /// Creates LIABILITY_HOLDER container from borrower liability entry.
        /// </summary>
        /// <param name="liability">The borrower's liability entry.</param>
        /// <returns>Returns a LIABILITY_HOLDER container populated with borrower liability entry information.</returns>
        private LIABILITY_HOLDER CreateLiabilityHolder(ILiabilityRegular liability)
        {
            var holder = new LIABILITY_HOLDER();
            holder.Address = 
                CreateAddress(
                liability.ComAddr, 
                liability.ComCity, 
                liability.ComState, 
                liability.ComZip, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(liability.ComState),
                AddressBase.Blank, 
                1);
            
            holder.Name = CreateName(liability.ComNm, isLegalEntity: true);

            return holder;
        }

        /// <summary>
        /// Creates a LICENSES container with the given state license and Nationwide Mortgage Licensing System license.
        /// </summary>
        /// <param name="licenseNumber">The state license.</param>
        /// <param name="nmlsLicense">The NMLS license.</param>
        /// <returns>A LICENSES container with the given state license and Nationwide Mortgage Licensing System license.</returns>
        private LICENSES CreateLicenses(string licenseNumber, string nmlsLicense)
        {
            LICENSES licenses = new LICENSES();
            licenses.License.Add(this.CreateLicense(nmlsLicense, nmls: true, fha_va: false, sequence: 1));
            licenses.License.Add(this.CreateLicense(licenseNumber, nmls: false, fha_va: false, sequence: licenses.License.Count(l => l != null) + 1));

            return licenses;
        }

        /// <summary>
        /// Creates a LICENSES container with the given state license, Nationwide Mortgage Licensing System license, and VA license.
        /// </summary>
        /// <param name="licenseNumber">The state license.</param>
        /// <param name="nmlsLicense">The NMLS license.</param>
        /// <param name="licenseVAFHA">The VA license.</param>
        /// <returns>A LICENSES container with the given state license, Nationwide Mortgage Licensing System license, and VA license.</returns>
        private LICENSES CreateLicenses(string licenseNumber, string nmlsLicense, string licenseVAFHA)
        {
            LICENSES licenses = this.CreateLicenses(licenseNumber, nmlsLicense);
            if (this.loanData.sLT == E_sLT.VA || this.loanData.sLT == E_sLT.FHA)
            {
                licenses.License.Add(this.CreateLicense(licenseVAFHA, nmls: false, fha_va: true, sequence: licenses.License.Count(l => l != null) + 1));
            }

            return licenses;
        }

        /// <summary>
        /// Creates a LICENSE container with the given license.
        /// </summary>
        /// <param name="licenseNumber">The license number.</param>
        /// <param name="nmls">True if the license is an NMLS license. Otherwise false.</param>
        /// <param name="fha_va">True if the license is an FHA or VA license. Otherwise false.</param>
        /// <param name="sequence">The sequence number of the license among the list of licenses.</param>
        /// <returns>A LICENSE container with the given license.</returns>
        private LICENSE CreateLicense(string licenseNumber, bool nmls, bool fha_va, int sequence)
        {
            if (string.IsNullOrEmpty(licenseNumber))
            {
                return null;
            }

            var license = new LICENSE();
            ////license.AppraiserLicense;

            if (nmls)
            {
                license.LicenseDetail = this.CreateLicenseDetail_NMLS(licenseNumber);
            }
            else if (fha_va)
            {
                license.LicenseDetail = this.CreateLicenseDetail_FHA_VA(licenseNumber);
            }
            else
            {
                license.LicenseDetail = this.CreateLicenseDetail_State(licenseNumber, this.loanData.sSpState);
            }
            
            ////license.PropertyLicense;
            license.SequenceNumber = sequence;

            return license;
        }

        /// <summary>
        /// Creates a LICENSE_DETAIL container with the given VA/FHA license.
        /// </summary>
        /// <param name="licenseNumber">The VA/FHA license.</param>
        /// <returns>A LICENSE_DETAIL container with the given VA/FHA license.</returns>
        private LICENSE_DETAIL CreateLicenseDetail_FHA_VA(string licenseNumber)
        {
            if (string.IsNullOrEmpty(licenseNumber))
            {
                return null;
            }

            var detail = new LICENSE_DETAIL();
            ////detail.LicenseAuthorityLevelType;
            ////detail.LicenseAuthorityLevelTypeOtherDescription;
            ////detail.LicenseExemptIndicator;
            ////detail.LicenseExpirationDate;
            detail.LicenseIdentifier = ToMismoIdentifier(licenseNumber, isSensitiveData: true);
            ////detail.LicenseIssueDate;
            if (this.loanData.sLT == E_sLT.FHA)
            {
                detail.LicenseIssuingAuthorityName = ToMismoString("FHA");
            }
            else if (this.loanData.sLT == E_sLT.VA)
            {
                detail.LicenseIssuingAuthorityName = ToMismoString("VA");
            }
            ////detail.LicenseIssuingAuthorityStateCode;
            ////detail.LicenseIssuingAuthorityStateName;

            return detail;
        }

        /// <summary>
        /// Creates a LICENSE_DETAIL container with the given NMLS license.
        /// </summary>
        /// <param name="licenseNumber">The NMLS license.</param>
        /// <returns>A LICENSE_DETAIL container with the given NMLS license.</returns>
        private LICENSE_DETAIL CreateLicenseDetail_NMLS(string licenseNumber)
        {
            if (string.IsNullOrEmpty(licenseNumber))
            {
                return null;
            }

            var detail = new LICENSE_DETAIL();
            ////detail.LicenseAuthorityLevelType;
            ////detail.LicenseAuthorityLevelTypeOtherDescription;
            ////detail.LicenseExemptIndicator;
            ////detail.LicenseExpirationDate;
            detail.LicenseIdentifier = ToMismoIdentifier(licenseNumber, null, "mortgage.nationwidelicensingsystem.org", isSensitiveData: true);
            ////detail.LicenseIssueDate;
            detail.LicenseIssuingAuthorityName = ToMismoString("NationwideMortgageLicensingSystemAndRegistry");
            ////detail.LicenseIssuingAuthorityStateCode;
            ////detail.LicenseIssuingAuthorityStateName;

            return detail;
        }

        /// <summary>
        /// Creates a LICENSE_DETAIL container with the given state license.
        /// </summary>
        /// <param name="licenseNumber">The state license.</param>
        /// <param name="state">The U.S. state for which the license was issued.</param>
        /// <returns>A LICENSE_DETAIL container with the given state license.</returns>
        private LICENSE_DETAIL CreateLicenseDetail_State(string licenseNumber, string state)
        {
            if (string.IsNullOrEmpty(licenseNumber))
            {
                return null;
            }

            var detail = new LICENSE_DETAIL();
            ////detail.LicenseAuthorityLevelType;
            ////detail.LicenseAuthorityLevelTypeOtherDescription;
            ////detail.LicenseExemptIndicator;
            ////detail.LicenseExpirationDate;
            detail.LicenseIdentifier = ToMismoIdentifier(licenseNumber, isSensitiveData: true);
            ////detail.LicenseIssueDate;
            ////detail.LicenseIssuingAuthorityName;
            detail.LicenseIssuingAuthorityStateCode = ToMismoCode(state);
            detail.LicenseIssuingAuthorityStateName = ToMismoString(this.statesAndTerritories.GetStateName(state));

            return detail;
        }

        /// <summary>
        /// Creates a LOAN which holds information about a particular loan.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number for the loan.</param>
        /// <param name="loanLabelKeySuffix">A string to append to the loan ID when adding an $$xlink$$ label to the loan label dictionary. E.G. ArchiveN if the loan object will contain archive data.</param>
        /// <returns>A LOAN populated with loan information.</returns>
        private LOAN CreateLoan_SubjectLoan(int sequenceNumber, string loanLabelKeySuffix)
        {
            var loan = new LOAN();
            loan.SequenceNumber = sequenceNumber;
            loan.label = "LOAN" + loan.SequenceNumberSerialized;

            this.loanLabel.Add(this.loanData.sLId.ToString("D") + loanLabelKeySuffix, loan.label);

            loan.Adjustment = this.CreateAdjustment(this.loanData);
            loan.Amortization = this.CreateAmortization(isForSubjectLoan: true);
            loan.Assumability = this.CreateAssumability();
            loan.BillingAddress = this.CreateBillingAddress();
            loan.Buydown = this.CreateBuydown();
            loan.ClosingInformation = this.CreateClosingInformation();
            loan.Construction = this.CreateConstruction();
            loan.DocumentSpecificDataSets = this.CreateDocumentSpecificDataSets();
            loan.Draw = this.CreateDraw();
            loan.Escrow = this.CreateEscrow();
            loan.FeeInformation = CreateFeeInformation(this.loanData, this.vendor, this.isClosingPackage ? IntegratedDisclosureDocumentBase.ClosingDisclosure : IntegratedDisclosureDocumentBase.LoanEstimate, this.loanData.sLenderPaidFeeDiscloseLocationT, this.feeIgnoreList);
            loan.Foreclosures = this.CreateForeclosures();
            loan.GovernmentLoan = this.CreateGovernmentLoan();
            loan.Heloc = this.CreateHeloc();
            loan.HighCostMortgages = this.CreateHighCostMortgages();
            loan.HMDALoan = this.CreateHmdaLoan();
            loan.HousingExpenses = this.CreateHousingExpenses_LoanContainer();
            loan.InterestCalculation = this.CreateInterestCalculation();
            loan.InterestOnly = CreateInterestOnly(this.loanData.sIOnlyMon, this.loanData.sIOnlyMon_rep, this.loanData.sProThisMPmt_rep);
            loan.InvestorLoanInformation = this.CreateInvestorLoanInformation();
            loan.LateCharge = this.CreateLateCharge();
            loan.LoanDetail = this.CreateLoanDetail();
            loan.LoanIdentifiers = this.CreateLoanIdentifiers();
            loan.LoanLevelCredit = this.CreateLoanLevelCredit();
            loan.LoanProduct = this.CreateLoanProduct();
            loan.Ltv = this.CreateLtv();
            loan.Maturity = this.CreateMaturity();
            loan.MERSRegistrations = this.CreateMersRegistrations();
            loan.MIData = this.CreateMiData();
            loan.NegativeAmortization = this.CreateNegativeAmortization();
            loan.Payment = CreatePayment(this.loanData, this.isClosingPackage);
            loan.PrepaymentPenalty = this.CreatePrepaymentPenalty();
            loan.PurchaseCredits = this.CreatePurchaseCredits();
            loan.Qualification = this.CreateQualification();
            loan.QualifiedMortgage = this.CreateQualifiedMortgage();
            loan.Refinance = this.CreateRefinance();
            loan.Rehabilitation = this.CreateRehabilitation();
            loan.TermsOfLoan = CreateTermsOfLoan(this.loanData, this.vendor, this.loanData.BrokerDB.IsEnableHELOC);
            loan.Underwriting = this.CreateUnderwriting();
            loan.Documentations = this.CreateDocumentations();
            if (this.vendor != E_DocumentVendor.IDS)
            {
                loan.LoanRoleType = LoanRoleBase.SubjectLoan;
            }

            if (!this.loanData.sIsRefinancing)
            {
                loan.DownPayments = this.CreateDownPayments();
            }

            if (this.loanData.BrokerDB.IsGFEandCoCVersioningEnabled && this.loanData.sClosingCostFeeVersionT != E_sClosingCostFeeVersionT.Legacy && this.vendor == E_DocumentVendor.UnknownOtherNone)
            {
                loan.Extension = this.CreateLoanExtension();
            }

            return loan;
        }

        /// <summary>
        /// Creates a LOAN container for the related loan.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>The LOAN container.</returns>
        private LOAN CreateLoan_RelatedLoan(int sequenceNumber)
        {
            var loan = new LOAN();
            loan.SequenceNumber = sequenceNumber;
            loan.LoanRoleType = LoanRoleBase.RelatedLoan;
            loan.TermsOfLoan = this.CreateTermsOfLoanForRelatedLoan();
            loan.Amortization = this.CreateAmortization(isForSubjectLoan: false);
            loan.Maturity = this.CreateMaturity_RelatedLoan();
            loan.FeeInformation = this.CreateFeeInformation_RelatedLoan();
            loan.Ltv = this.CreateLtv_RelatedLoan();
            loan.LoanDetail = this.CreateLoanDetail_RelatedLoan();
            loan.Payment = this.CreatePayment_RelatedLoan();

            return loan;
        }

        /// <summary>
        /// Creates a LOAN_EXTENSION container with a proprietary LQB data set.
        /// </summary>
        /// <returns>A LOAN_EXTENSION container.</returns>
        private LOAN_EXTENSION CreateLoanExtension()
        {
            var extension = new LOAN_EXTENSION();
            extension.Other = this.CreateLQBLoanExtension();

            return extension;
        }

        /// <summary>
        /// Creates an LQB_LOAN_EXTENSION container.
        /// </summary>
        /// <returns>An LQB_LOAN_EXTENSION container.</returns>
        private LQB_LOAN_EXTENSION CreateLQBLoanExtension()
        {
            var extension = new LQB_LOAN_EXTENSION();
            extension.ChangeOfCircumstanceOccurrences = this.CreateChangeOfCircumstanceOccurrences();

            return extension;
        }

        /// <summary>
        /// Creates a LOANS container which holds a set of LOAN.
        /// </summary>
        /// <returns>A LOANS container.</returns>
        private LOANS CreateLoans()
        {
            var loans = new LOANS();
            loans.Loan.Add(this.CreateLoan_SubjectLoan(1, string.Empty));

            if (this.loanData.sLT == E_sLT.VA && this.loanData.sIsRefinancing)
            {
                loans.Loan.Add(this.CreateLoan_RelatedLoan(2));
            }

            return loans;
        }

        /// <summary>
        /// Creates the DOCUMENTATIONS container.
        /// </summary>
        /// <returns>The container.</returns>
        private DOCUMENTATIONS CreateDocumentations()
        {
            DOCUMENTATIONS documentations = new DOCUMENTATIONS();
            documentations.Documentation = this.CreateDocumentationList();

            return documentations;
        }

        /// <summary>
        /// Creates a list of DOCUMENTATION containers.
        /// </summary>
        /// <returns>The list of containers.</returns>
        private List<DOCUMENTATION> CreateDocumentationList()
        {
            List<DOCUMENTATION> list = new List<DOCUMENTATION>();
            list.Add(this.CreateDocumentation());
            return list;
        }

        /// <summary>
        /// Creates a DOCUMENTATION container.
        /// </summary>
        /// <returns>The container.</returns>
        private DOCUMENTATION CreateDocumentation()
        {
            DOCUMENTATION doc = new DOCUMENTATION();
            doc.Extension = this.CreateDocumentationExtension();
            return doc;
        }

        /// <summary>
        /// Creates the DOCUMENTATION container extension.
        /// </summary>
        /// <returns>The container.</returns>
        private DOCUMENTATION_EXTENSION CreateDocumentationExtension()
        {
            DOCUMENTATION_EXTENSION extension = new DOCUMENTATION_EXTENSION();
            extension.Other = this.CreateLqbDocumentationExtension();

            return extension;
        }

        /// <summary>
        /// Creates LQB Documentation container extension.
        /// </summary>
        /// <returns>The extension container.</returns>
        private LQB_DOCUMENTATION_EXTENSION CreateLqbDocumentationExtension()
        {
            LQB_DOCUMENTATION_EXTENSION extension = new LQB_DOCUMENTATION_EXTENSION();
            extension.DocumentTypeName = ToLqbDocumentTypeNameBase(this.loanData.sProdDocT);

            return extension;
        }

        /// <summary>
        /// Creates a container holding data on the details of a loan.
        /// </summary>
        /// <returns>A LOAN_DETAIL container populated with data.</returns>
        private LOAN_DETAIL CreateLoanDetail()
        {
            var loanDetail = new LOAN_DETAIL();
            loanDetail.ApplicationReceivedDate = ToMismoDate(this.loanData.sAppReceivedByLenderD_rep);
            loanDetail.BalloonIndicator = ToMismoIndicator(this.loanData.sBalloonPmt);
            loanDetail.DemandFeatureIndicator = ToMismoIndicator(this.loanData.sHasDemandFeature);
            loanDetail.EscrowIndicator = ToMismoIndicator(this.loanData.sTridEscrowAccountExists);
            loanDetail.EscrowAccountLenderRequirementType = ToEscrowAccountLenderRequirementEnum(this.loanData.sNonMIHousingExpensesEscrowedReasonT, this.loanData.sTridEscrowAccountExists);
            loanDetail.EscrowAbsenceReasonType = ToEscrowAbsenceReasonEnum(this.loanData.sNonMIHousingExpensesNotEscrowedReasonT);

            if (loanDetail.EscrowAbsenceReasonType != null && loanDetail.EscrowAbsenceReasonType.enumValue == EscrowAbsenceReasonBase.Other)
            {
                loanDetail.EscrowAbsenceReasonTypeOtherDescription = ToMismoString(GetXmlEnumName(this.loanData.sNonMIHousingExpensesNotEscrowedReasonT));
            }

            loanDetail.PrepaymentPenaltyIndicator = ToPrepaymentPenaltyIndicator(this.loanData.sPrepmtPenaltyT);
            loanDetail.HELOCIndicator = ToMismoIndicator(this.loanData.BrokerDB.IsEnableHELOC && this.loanData.sIsLineOfCredit);
            loanDetail.BuydownTemporarySubsidyFundingIndicator = ToMismoIndicator(this.loanData.sHasTempBuydown);
            loanDetail.ServicingTransferStatusType = ToServicingTransferStatusEnum(this.loanData.sTRIDLoanEstimateLenderIntendsToServiceLoan);
            loanDetail.HigherPricedMortgageLoanIndicator = ToHigherPricedMortgageLoanIndicator(this.loanData.sHighPricedMortgageT);
            loanDetail.CreditorServicingOfLoanStatementType = ToCreditorServicingOfLoanStatementEnum(this.loanData.sMayAssignSellTransfer, this.loanData.sTRIDLoanEstimateLenderIntendsToServiceLoan);
            loanDetail.ConstructionLoanIndicator = ToMismoIndicator(IsConstructionLoan(this.loanData.sLPurposeT));
            loanDetail.TotalSubordinateFinancingAmount = ToMismoAmount(this.loanData.sONewFinBal_rep);
            loanDetail.LoanApprovalExpirationDate = ToMismoDate(this.loanData.sAppExpD_rep);
            loanDetail.WarehouseLenderIndicator = ToMismoIndicator(this.loanData.sWarehouseLenderRolodexId >= 0);
            loanDetail.QualifiedMortgageIndicator = ToQualifiedMortgageIndicator(this.loanData.sQMStatusT);
            loanDetail.TotalMortgagedPropertiesCount = ToMismoCount(this.loanData.sNumFinancedProperties_rep, null);
            loanDetail.ConvertibleIndicator = ToMismoIndicator(this.loanData.sIsConvertibleMortgage);
            loanDetail.PropertyInspectionWaiverIndicator = ToMismoIndicator(this.loanData.sSpValuationMethodT == E_sSpValuationMethodT.None && this.loanData.sSpGseCollateralProgramT == E_sSpGseCollateralProgramT.PropertyInspectionWaiver);

            if (this.loanData.sAssumeLT != E_sAssumeLT.LeaveBlank)
            {
                // MayNot maps to false, May or MaySubjectToCondition map to true.
                bool assumable = this.loanData.sAssumeLT != E_sAssumeLT.MayNot;
                loanDetail.AssumabilityIndicator = ToMismoIndicator(assumable);
            }

            loanDetail.Extension = this.CreateLoanDetailExtension();

            // OPM 247161 - UCD Support.
            loanDetail.LoanAmountIncreaseIndicator = ToMismoIndicator(this.loanData.sGfeCanLoanBalanceIncrease);
            loanDetail.InterestRateIncreaseIndicator = ToMismoIndicator(this.loanData.sGfeCanRateIncrease);
            loanDetail.PaymentIncreaseIndicator = ToMismoIndicator(this.loanData.sGfeCanPaymentIncrease);
            loanDetail.InterestOnlyIndicator = ToMismoIndicator(this.loanData.sIsIOnly);
            loanDetail.BalloonPaymentAmount = ToMismoAmount(this.loanData.sGfeBalloonPmt_rep);

            loanDetail.LoanFundingDate = ToMismoDate(this.loanData.sFundD_rep);

            return loanDetail;
        }

        /// <summary>
        /// Creates an EXTENSION container to put in the LOAN_DETAIL element.
        /// </summary>
        /// <returns>A LOAN_DETAIL_EXTENSION element to serialize to LOAN_DETAIL/EXTENSION.</returns>
        private LOAN_DETAIL_EXTENSION CreateLoanDetailExtension()
        {
            var extension = new LOAN_DETAIL_EXTENSION();

            extension.Other = this.CreateLQBLoanDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates the proprietary LQB OTHER container for holding information pertaining to the LOAN_DETAIL.
        /// </summary>
        /// <returns>An LQB_LOAN_DETAIL_EXTENSION object suitable to serialize to LOAN_DETAIL/EXTENSION/OTHER.</returns>
        private LQB_LOAN_DETAIL_EXTENSION CreateLQBLoanDetailExtension()
        {
            var lqbExtension = new LQB_LOAN_DETAIL_EXTENSION();

            lqbExtension.LoanOriginationChannelType = ToLoanOriginationChannelEnum(this.loanData.sBranchChannelT);
            lqbExtension.TridTargetRegulationVersionType = ToLqbTridTargetRegulationVersionEnum(this.loanData.sTridTargetRegulationVersionT);
            lqbExtension.IntentToProceedDate = ToMismoDate(this.loanData.sIntentToProceedD_rep);
            lqbExtension.NewConcurrentOtherFinancingIndicator = ToMismoIndicator(this.loanData.sIsNewConcurrentOtherFinancing);

            return lqbExtension;
        }

        /// <summary>
        /// Creates a container holding information on a loan identifier.
        /// </summary>
        /// <param name="identifier">The identifier string.</param>
        /// <param name="identifierType">The type of identifier.</param>
        /// <param name="sequenceNumber">The sequence number of the LOAN_IDENTIFIER element.</param>
        /// <param name="identifierTypeOtherDescription">The description for when <paramref name="identifierType"/> is <see cref="LoanIdentifierBase.Other"/>.</param>
        /// <returns>A LOAN_IDENTIFIER container populated with data.</returns>
        private LOAN_IDENTIFIER CreateLoanIdentifier(string identifier, LoanIdentifierBase identifierType, int sequenceNumber, string identifierTypeOtherDescription = null)
        {
            if (string.IsNullOrEmpty(identifier))
            {
                return null;
            }

            var loanIdentifier = new LOAN_IDENTIFIER();
            loanIdentifier.SequenceNumber = sequenceNumber;
            loanIdentifier.LoanIdentifier = ToMismoIdentifier(identifier);
            loanIdentifier.LoanIdentifierType = ToLoanIdentifierEnum(identifierType);
            if (loanIdentifier.LoanIdentifierType.enumValue == LoanIdentifierBase.Other)
            {
                loanIdentifier.LoanIdentifierTypeOtherDescription = ToMismoString(identifierTypeOtherDescription);
            }

            return loanIdentifier;
        }
        
        /// <summary>
        /// Creates a container that holds all instances of LOAN_IDENTIFIER.
        /// </summary>
        /// <returns>A LOAN_IDENTIFIERS container.</returns>
        private LOAN_IDENTIFIERS CreateLoanIdentifiers()
        {
            var loanIdentifiers = new LOAN_IDENTIFIERS();
            loanIdentifiers.LoanIdentifier.Add(this.CreateLoanIdentifier(this.loanData.sMersMin, LoanIdentifierBase.MERS_MIN, 1));
            loanIdentifiers.LoanIdentifier.Add(this.CreateLoanIdentifier(this.loanData.sAgencyCaseNum, LoanIdentifierBase.AgencyCase, loanIdentifiers.LoanIdentifier.Count(i => i != null) + 1));
            loanIdentifiers.LoanIdentifier.Add(this.CreateLoanIdentifier(this.loanData.sLenderCaseNum, LoanIdentifierBase.LenderCase, loanIdentifiers.LoanIdentifier.Count(i => i != null) + 1));
            loanIdentifiers.LoanIdentifier.Add(this.CreateLoanIdentifier(this.loanData.sLNm, LoanIdentifierBase.LenderLoan, loanIdentifiers.LoanIdentifier.Count(i => i != null) + 1));
            loanIdentifiers.LoanIdentifier.Add(this.CreateLoanIdentifier(this.loanData.sInvestorLockLoanNum, LoanIdentifierBase.InvestorLoan, loanIdentifiers.LoanIdentifier.Count(i => i != null) + 1));
            loanIdentifiers.LoanIdentifier.Add(this.CreateLoanIdentifier(this.loanData.sCoreLoanId, LoanIdentifierBase.Other, loanIdentifiers.LoanIdentifier.Count(i => i != null) + 1, "CoreSystemAccountNumber"));
            
            return loanIdentifiers;
        }

        /// <summary>
        /// Creates a LOAN_LEVEL_CREDIT container with information regarding the credit score used to qualify the loan.
        /// </summary>
        /// <returns>A LOAN_LEVEL_CREDIT container with information regarding the credit score used to qualify the loan.</returns>
        private LOAN_LEVEL_CREDIT CreateLoanLevelCredit()
        {
            var credit = new LOAN_LEVEL_CREDIT();
            credit.LoanLevelCreditDetail = this.CreateLoanLevelCreditDetail();

            return credit;
        }

        /// <summary>
        /// Creates a LOAN_LEVEL_CREDIT_DETAIL container with information regarding the credit score used to qualify the loan.
        /// </summary>
        /// <returns>A LOAN_LEVEL_CREDIT_DETAIL container with information regarding the credit score used to qualify the loan.</returns>
        private LOAN_LEVEL_CREDIT_DETAIL CreateLoanLevelCreditDetail()
        {
            var detail = new LOAN_LEVEL_CREDIT_DETAIL();
            detail.LoanLevelCreditScoreValue = ToMismoValue(this.loanData.sCreditScoreLpeQual_rep);

            if (detail.LoanLevelCreditScoreValue == null)
            {
                return null;
            }

            detail.LoanLevelCreditScoreSelectionMethodType = ToLoanLevelCreditScoreSelectionMethodEnum(LoanLevelCreditScoreSelectionMethodBase.MiddleOrLowerThenLowest);

            return detail;
        }

        /// <summary>
        /// Creates a container holding information on a loan product.
        /// </summary>
        /// <returns>A LOAN_PRODUCT container.</returns>
        private LOAN_PRODUCT CreateLoanProduct()
        {
            var loanProduct = new LOAN_PRODUCT();
            loanProduct.LoanProductDetail = this.CreateLoanProductDetail();
            loanProduct.Locks = this.CreateLocks();

            return loanProduct;
        }

        /// <summary>
        /// Creates a container holding details on a loan product.
        /// </summary>
        /// <returns>A LOAN_PRODUCT_DETAIL populated with data.</returns>
        private LOAN_PRODUCT_DETAIL CreateLoanProductDetail()
        {
            var loanProductDetail = new LOAN_PRODUCT_DETAIL();

            loanProductDetail.ProductName = ToMismoString(this.loanData.sLpTemplateNm);
            loanProductDetail.ProductIdentifier = ToMismoIdentifier(this.loanData.sLoanProductIdentifier);

            return loanProductDetail;
        }

        /// <summary>
        /// Creates a container holding a location identifier.
        /// </summary>
        /// <returns>A LOCATION_IDENTIFIER container.</returns>
        private LOCATION_IDENTIFIER CreateLocationIdentifier()
        {
            var locationIdentifier = new LOCATION_IDENTIFIER();
            locationIdentifier.CensusInformation = this.CreateCensusInformation();
            locationIdentifier.FipsInformation = this.CreateFipsInformation();
            locationIdentifier.GeneralIdentifier = this.CreateGeneralIdentifier();

            return locationIdentifier;
        }

        /// <summary>
        /// Creates a container holding data about a rate lock.
        /// </summary>
        /// <param name="sequence">The sequence number of the lock among the list of locks.</param>
        /// <returns>A LOCK container.</returns>
        private LOCK CreateLock(int sequence)
        {
            var rateLock = new LOCK();
            rateLock.LockExpirationDatetime = ToMismoDatetime(this.loanData.sTRIDLoanEstimateNoteIRAvailTillD_rep_WithTime, true);
            if (!(this.loanData.sTRIDLoanEstimateSetLockStatusMethodT == E_sTRIDLoanEstimateSetLockStatusMethodT.SetManually || this.loanData.sTRIDIsLoanEstimateRateLocked == false))
            {
                rateLock.LockDurationDaysCount = ToMismoCount(this.loanData.sRLckdDays_rep, null);
            }

            rateLock.LockDatetime = ToMismoDatetime(this.loanData.sRLckdD_rep_WithTime, false);
            rateLock.SequenceNumber = sequence;

            return rateLock;
        }

        /// <summary>
        /// Creates a container holding all instances of LOCK.
        /// </summary>
        /// <returns>A LOCKS container.</returns>
        private LOCKS CreateLocks()
        {
            var locks = new LOCKS();
            locks.Lock.Add(this.CreateLock(1));

            return locks;
        }

        /// <summary>
        /// Creates a container holding data on the loan-to-value ratio.
        /// </summary>
        /// <returns>An LTV container.</returns>
        private LTV CreateLtv()
        {
            var ltv = new LTV();
            ltv.LTVRatioPercent = ToMismoPercent(this.loanData.sLtvR_rep);

            return ltv;
        }

        /// <summary>
        /// Creates a container representing a manufactured home.
        /// </summary>
        /// <returns>A MANUFACTURED_HOME container.</returns>
        private MANUFACTURED_HOME CreateManufacturedHome()
        {
            if (!(this.loanData.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium
                || this.loanData.sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide
                || this.loanData.sGseSpT == E_sGseSpT.ManufacturedHousing
                || this.loanData.sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide))
            {
                return null;
            }

            var manufacturedHome = new MANUFACTURED_HOME();
            manufacturedHome.ManufacturedHomeDetail = this.CreateManufacturedHomeDetail();

            return manufacturedHome;
        }

        /// <summary>
        /// Creates a container holding details on a manufactured home.
        /// </summary>
        /// <returns>A MANUFACTURED_HOME_DETAIL container.</returns>
        private MANUFACTURED_HOME_DETAIL CreateManufacturedHomeDetail()
        {
            var detail = new MANUFACTURED_HOME_DETAIL();
            detail.LengthFeetNumber = ToMismoNumeric(this.loanData.sManufacturedHomeLengthFeet_rep);
            detail.ManufacturedHomeAttachedToFoundationIndicator = ToMismoIndicator(this.loanData.sManufacturedHomeAttachedToFoundation);
            detail.ManufacturedHomeConditionDescriptionType = ToManufacturedHomeConditionDescriptionEnum(this.loanData.sManufacturedHomeConditionT);
            detail.ManufacturedHomeManufacturerName = ToMismoString(this.loanData.sManufacturedHomeManufacturer);
            detail.ManufacturedHomeManufactureYear = ToMismoYear(this.loanData.sManufacturedHomeYear, "sManufacturedHomeYear", false, this.isLenderExportMode);
            detail.ManufacturedHomeModelIdentifier = ToMismoIdentifier(this.loanData.sManufacturedHomeModel);
            detail.ManufacturedHomeSerialNumberIdentifier = ToMismoIdentifier(this.loanData.sManufacturedHomeSerialNumber);
            detail.ManufacturedHomeWidthType = ToManufacturedHomeWidthEnum(this.loanData.sGseSpT);
            detail.WidthFeetNumber = ToMismoNumeric(this.loanData.sManufacturedHomeWidthFeet_rep);
            detail.ManufacturedHomeHUDDataPlateIdentifier = ToMismoIdentifier(this.loanData.sManufacturedHomeHUDLabelNumber);

            detail.Extension = this.CreateManufacturedHomeDetailExtension();

            return detail;
        }

        /// <summary>
        /// Creates a container holding extension data related to a manufactured home.
        /// </summary>
        /// <returns>A MANUFACTURED_HOME_DETAIL_EXTENSION container.</returns>
        private MANUFACTURED_HOME_DETAIL_EXTENSION CreateManufacturedHomeDetailExtension()
        {
            var extension = new MANUFACTURED_HOME_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBManufacturedHomeDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates a container holding proprietary datapoints related to a manufactured home.
        /// </summary>
        /// <returns>An LQB_MANUFACTURED_HOME_DETAIL_EXTENSION container.</returns>
        private LQB_MANUFACTURED_HOME_DETAIL_EXTENSION CreateLQBManufacturedHomeDetailExtension()
        {
            var extension = new LQB_MANUFACTURED_HOME_DETAIL_EXTENSION();
            extension.CertificateOfTitleType = ToCertificateOfTitleEnum(this.loanData.sManufacturedHomeCertificateofTitleT);

            return extension;
        }

        /// <summary>
        /// Creates a container holding information on the loan maturity.
        /// </summary>
        /// <returns>A MATURITY container.</returns>
        private MATURITY CreateMaturity()
        {
            var maturity = new MATURITY();
            maturity.MaturityRule = this.CreateMaturityRule();

            return maturity;
        }

        /// <summary>
        /// Creates a container holding the rules of the loan maturity.
        /// </summary>
        /// <returns>A MATURITY_RULE object populated with data.</returns>
        private MATURITY_RULE CreateMaturityRule()
        {
            var maturityRule = new MATURITY_RULE();
            maturityRule.LoanMaturityPeriodCount = ToMismoCount(this.loanData.sDue_rep, null);
            maturityRule.LoanMaturityPeriodType = ToLoanMaturityPeriodEnum(LoanMaturityPeriodBase.Month);
            maturityRule.LoanMaturityDate = ToMismoDate(this.loanData.sLoanMaturityD_rep);

            return maturityRule;
        }

        /// <summary>
        /// Creates a container holding data about a MERS registration.
        /// </summary>
        /// <param name="sequence">The sequence number of the MERS registration among the list of registrations.</param>
        /// <returns>A MERS_REGISTRATION container populated with data.</returns>
        private MERS_REGISTRATION CreateMersRegistration(int sequence)
        {
            var mersRegistration = new MERS_REGISTRATION();
            mersRegistration.SequenceNumber = sequence;
            mersRegistration.MERSOriginalMortgageeOfRecordIndicator = ToMismoIndicator(this.loanData.sMersIsOriginalMortgagee);

            return mersRegistration;
        }

        /// <summary>
        /// Creates a container to hold all instances of MERS_REGISTRATION.
        /// </summary>
        /// <returns>A MERS_REGISTRATIONS container.</returns>
        private MERS_REGISTRATIONS CreateMersRegistrations()
        {
            var mersRegistrations = new MERS_REGISTRATIONS();
            mersRegistrations.MersRegistration.Add(this.CreateMersRegistration(1));

            return mersRegistrations;
        }

        /// <summary>
        /// Creates a MESSAGE container which is the MISMO 3.3 root and holds DEAL_SETS.
        /// </summary>
        /// <returns>A MESSAGE container.</returns>
        private MESSAGE CreateMessage()
        {
            var message = new MESSAGE();
            message.MISMOReferenceModelIdentifier = "3.3.1[B306]";
            message.MISMOLogicalDataDictionaryIdentifier = "3.3.1[B306]";
            message.DealSets = this.CreateDealSets();

            return message;
        }

        /// <summary>
        /// Creates a MESSAGE_EXTENSION container with an LQB generic framework authentication ticket.
        /// </summary>
        /// <returns>A MESSAGE_EXTENSION object containing a generic framework authentication ticket.</returns>
        private MESSAGE_EXTENSION CreateMessageExtension()
        {
            MESSAGE_EXTENSION extension = new MESSAGE_EXTENSION();
            LQB_MESSAGE_EXTENSION authTicketExtension = new LQB_MESSAGE_EXTENSION();
            AuthenticationTicket ticket = new AuthenticationTicket();

            ticket.Content = AuthServiceHelper.GetGenericFrameworkUserAuthTicket(PrincipalFactory.CurrentPrincipal, this.loanData.sLRefNm, null).ToString();
            authTicketExtension.Ticket = ticket;
            authTicketExtension.TicketServiceDomain = new MISMOURL { Value = ConstStage.GenericFrameworkWebServiceDomain };
            extension.Other = authTicketExtension;

            return extension;
        }

        /// <summary>
        /// Creates a set of mortgage insurance data.
        /// </summary>
        /// <returns>An MI_DATA container.</returns>
        private MI_DATA CreateMiData()
        {
            if (this.loanData.sLT == E_sLT.VA)
            {
                return null;
            }

            var mortgageInsuranceData = new MI_DATA();
            mortgageInsuranceData.MiDataDetail = this.CreateMiDataDetail();
            mortgageInsuranceData.MiPremiums = this.CreateMiPremiums();

            return mortgageInsuranceData;
        }

        /// <summary>
        /// Details about mortgage insurance.
        /// </summary>
        /// <returns>An MI_DATA_DETAIL container.</returns>
        private MI_DATA_DETAIL CreateMiDataDetail()
        {
            Func<BaseClosingCostFee, bool> initialFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(this.loanData.sClosingCostSet, false, this.loanData.sOriginatorCompensationPaymentSourceT, this.loanData.sDisclosureRegulationT, this.loanData.sGfeIsTPOTransaction);

            Func<BaseClosingCostFee, bool> setFilter = fee => initialFilter(fee) && fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId;
            
            BorrowerClosingCostFee mifee = (BorrowerClosingCostFee)this.loanData.sClosingCostSet.GetFees(setFilter).FirstOrDefault();
            var mortgageInsuranceDataDetail = new MI_DATA_DETAIL();

            if (mifee != null)
            {
                MISMOString paidToTypeOtherDescription = null;
                CAgentFields beneficiaryContact = null;
                if (mifee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.loanData.GetAgentFields(mifee.BeneficiaryAgentId);
                }

                mortgageInsuranceDataDetail.FeePaidToType = GetFeePaidToType(
                    beneficiaryContact, 
                    mifee.IsAffiliate, 
                    mifee.IsThirdParty, 
                    mifee.BeneficiaryType, 
                    mifee.Beneficiary, 
                    mifee.BeneficiaryDescription, 
                    out paidToTypeOtherDescription);

                if (paidToTypeOtherDescription != null)
                {
                    mortgageInsuranceDataDetail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
                }
            }

            mortgageInsuranceDataDetail.LenderPaidMIInterestRateAdjustmentPercent = ToMismoPercent(this.loanData.sMiLenderPaidAdj_rep);
            mortgageInsuranceDataDetail.MI_LTVCutoffPercent = ToMismoPercent(string.IsNullOrEmpty(this.loanData.sProMInsCancelLtv_rep) ? "0.000" : this.loanData.sProMInsCancelLtv_rep);
            mortgageInsuranceDataDetail.MICertificateIdentifier = ToMismoIdentifier(this.loanData.sMiCertId);
            mortgageInsuranceDataDetail.MICollectedNumberOfMonthsCount = ToMismoCount(this.loanData.sMInsRsrvMon == 0 ? "0" : this.loanData.sMInsRsrvMon_rep, null);
            mortgageInsuranceDataDetail.MICushionNumberOfMonthsCount = ToMismoCount(this.loanData.GetCushionMonths(E_EscrowItemT.MortgageInsurance), this.loanData.m_convertLos);
            mortgageInsuranceDataDetail.MICompanyNameType = ToMICompanyNameEnum(this.loanData.sMiCompanyNmT);

            if (mortgageInsuranceDataDetail.MICompanyNameType.enumValue == MICompanyNameBase.Other)
            {
                mortgageInsuranceDataDetail.MICompanyNameTypeOtherDescription = ToMismoString(Tools.Get_sMiCompanyNmTFriendlyDisplay(this.loanData.sMiCompanyNmT));
            }
            
            mortgageInsuranceDataDetail.MICoveragePercent = ToMismoPercent(this.loanData.sMiLenderPaidCoverage_rep);
            mortgageInsuranceDataDetail.MIDurationType = ToMiDurationEnum(this.loanData.sProdConvMIOptionT);
            mortgageInsuranceDataDetail.MIEscrowedIndicator = ToMismoIndicator(this.loanData.sMInsRsrvEscrowedTri == E_TriState.Yes);

            string initialPremium = this.loanData.sFfUfmip1003_rep;
            string initialPremiumPercent = this.loanData.sFfUfmipR_rep;

            if (this.loanData.sMiInsuranceT == E_sMiInsuranceT.LenderPaid)
            {
                initialPremium = this.loanData.sLenderUfmip_rep;
                initialPremiumPercent = this.loanData.sLenderUfmipR_rep;
            }

            mortgageInsuranceDataDetail.MIInitialPremiumAmount = ToMismoAmount(initialPremium);
            mortgageInsuranceDataDetail.MIInitialPremiumRatePercent = ToMismoPercent(initialPremiumPercent);
            mortgageInsuranceDataDetail.MIPremiumFinancedAmount = ToMismoAmount(this.loanData.sFfUfmipFinanced_rep);
            mortgageInsuranceDataDetail.MIPremiumFinancedIndicator = ToMismoIndicator(this.loanData.sFfUfmipFinanced > 0);
            mortgageInsuranceDataDetail.MIPremiumFromClosingAmount = ToMismoAmount(this.loanData.sUfCashPd_rep);
            mortgageInsuranceDataDetail.MIPremiumPaymentType = ToMIPremiumPaymentEnum(this.loanData.sMInsRsrvEscrowedTri == E_TriState.Yes);
            mortgageInsuranceDataDetail.MIPremiumRefundableType = ToMIPremiumRefundableEnum(this.loanData.sUfmipIsRefundableOnProRataBasis);
            mortgageInsuranceDataDetail.MIPremiumSourceType = ToMIPremiumSourceEnum(this.loanData.sMiInsuranceT);

            if (mortgageInsuranceDataDetail.MIPremiumSourceType.enumValue == MIPremiumSourceBase.Other)
            {
                mortgageInsuranceDataDetail.MIPremiumSourceTypeOtherDescription = ToMismoString(GetXmlEnumName(this.loanData.sMiInsuranceT));
            }

            mortgageInsuranceDataDetail.MISourceType = ToMISourceEnum(this.loanData.sLT);
            
            return mortgageInsuranceDataDetail;
        }

        /// <summary>
        /// Creates a MILITARY_SERVICES container holding data on a borrower's periods of military service.
        /// </summary>
        /// <param name="appData">A set of application data.</param>
        /// <returns>A MILITARY_SERVICES container.</returns>
        private MILITARY_SERVICES CreateMilitaryServices(CAppData appData)
        {
            var militaryServices = new MILITARY_SERVICES();

            militaryServices.MilitaryService.Add(
                this.CreateMilitaryService(
                    appData.aVaServ1BranchNum,
                    appData.aVaServ1Num,
                    appData.aVaServ1StartD_rep,
                    appData.aVaServ1EndD_rep,
                    string.IsNullOrWhiteSpace(appData.aVaServedAltName) ? appData.aNm : appData.aVaServedAltName,
                    appData.aActiveMilitaryStatTri,
                    isActiveServiceLine: true));

            militaryServices.MilitaryService.Add(
                this.CreateMilitaryService(
                    appData.aVaServ2BranchNum,
                    appData.aVaServ2Num,
                    appData.aVaServ2StartD_rep,
                    appData.aVaServ2EndD_rep,
                    string.IsNullOrWhiteSpace(appData.aVaServedAltName) ? appData.aNm : appData.aVaServedAltName,
                    appData.aActiveMilitaryStatTri,
                    isActiveServiceLine: true));

            militaryServices.MilitaryService.Add(
                this.CreateMilitaryService(
                    appData.aVaServ3BranchNum,
                    appData.aVaServ3Num,
                    appData.aVaServ3StartD_rep,
                    appData.aVaServ3EndD_rep,
                    string.IsNullOrWhiteSpace(appData.aVaServedAltName) ? appData.aNm : appData.aVaServedAltName,
                    appData.aActiveMilitaryStatTri,
                    isActiveServiceLine: true));

            militaryServices.MilitaryService.Add(
                this.CreateMilitaryService(
                    appData.aVaServ4BranchNum,
                    appData.aVaServ4Num,
                    appData.aVaServ4StartD_rep,
                    appData.aVaServ4EndD_rep,
                    string.IsNullOrWhiteSpace(appData.aVaServedAltName) ? appData.aNm : appData.aVaServedAltName,
                    appData.aActiveMilitaryStatTri,
                    isActiveServiceLine: false));

            militaryServices.MilitaryService.Add(
                this.CreateMilitaryService(
                    appData.aVaServ5BranchNum,
                    appData.aVaServ5Num,
                    appData.aVaServ5StartD_rep,
                    appData.aVaServ5EndD_rep,
                    string.IsNullOrWhiteSpace(appData.aVaServedAltName) ? appData.aNm : appData.aVaServedAltName,
                    appData.aActiveMilitaryStatTri,
                    isActiveServiceLine: false));

            militaryServices.MilitaryService.Add(
                this.CreateMilitaryService(
                    appData.aVaServ6BranchNum,
                    appData.aVaServ6Num,
                    appData.aVaServ6StartD_rep,
                    appData.aVaServ6EndD_rep,
                    string.IsNullOrWhiteSpace(appData.aVaServedAltName) ? appData.aNm : appData.aVaServedAltName,
                    appData.aActiveMilitaryStatTri,
                    isActiveServiceLine: false));

            militaryServices.MilitaryService.Add(
                this.CreateMilitaryService(
                    appData.aVaServ7BranchNum,
                    appData.aVaServ7Num,
                    appData.aVaServ7StartD_rep,
                    appData.aVaServ7EndD_rep,
                    string.IsNullOrWhiteSpace(appData.aVaServedAltName) ? appData.aNm : appData.aVaServedAltName,
                    appData.aActiveMilitaryStatTri,
                    isActiveServiceLine: false));

            militaryServices.MilitaryService.Add(this.CreateMilitaryService(appData.aVaServiceBranchT, appData.aVaMilitaryStatT));

            return militaryServices;
        }

        /// <summary>
        /// Creates a MILITARY_SERVICE container holding data on one period of a borrower's service.
        /// </summary>
        /// <param name="branchNumber">The branch the borrower served in.</param>
        /// <param name="identifier">The borrower's unique identifier in the service.</param>
        /// <param name="startDate">The borrower's service start date.</param>
        /// <param name="endDate">The borrower's service end date.</param>
        /// <param name="name">The name the borrower served under.</param>
        /// <param name="onActiveDuty">Indicates whether the borrower is currently on active duty.</param>
        /// <param name="isActiveServiceLine">Indicates whether the data is from an active service or military reserve line.</param>
        /// <returns>A MILITARY_SERVICE container.</returns>
        private MILITARY_SERVICE CreateMilitaryService(
            string branchNumber, 
            string identifier, 
            string startDate, 
            string endDate, 
            string name,
            E_TriState onActiveDuty,
            bool isActiveServiceLine)
        {
            if (string.IsNullOrWhiteSpace(branchNumber) && string.IsNullOrWhiteSpace(identifier) &&
                string.IsNullOrWhiteSpace(startDate) && string.IsNullOrWhiteSpace(endDate))
            {
                return null;
            }

            var militaryService = new MILITARY_SERVICE();
            militaryService.MilitaryBranchType = ToMilitaryBranchEnum(branchNumber);
            militaryService.MilitaryServiceNumberIdentifier = ToMismoIdentifier(identifier);
            militaryService.MilitaryServiceFromDate = ToMismoDate(startDate);
            militaryService.MilitaryServiceToDate = ToMismoDate(endDate);
            militaryService.MilitaryServiceServedAsName = ToMismoString(name);
            militaryService.MilitaryStatusType = ToMilitaryStatusEnum(onActiveDuty, startDate, endDate, isActiveServiceLine);

            return militaryService;
        }

        /// <summary>
        /// Creates a MILITARY_SERVICE container using service branch type and military status type.
        /// </summary>
        /// <param name="serviceBranchType">The service branch type.</param>
        /// <param name="militaryStatusType">The military status type.</param>
        /// <returns>The MILITARY_SERVICE container.</returns>
        private MILITARY_SERVICE CreateMilitaryService(E_aVaServiceBranchT serviceBranchType, E_aVaMilitaryStatT militaryStatusType)
        {
            var militaryService = new MILITARY_SERVICE();
            militaryService.MilitaryBranchType = ToMilitaryBranchEnum(serviceBranchType);
            militaryService.MilitaryStatusType = ToMilitaryStatusEnum(militaryStatusType);

            return militaryService;
        }

        /// <summary>
        /// Creates a container to hold a set of mortgage insurance premiums.
        /// </summary>
        /// <returns>An MI_PREMIUMS container.</returns>
        private MI_PREMIUMS CreateMiPremiums()
        {
            var mortgageInsurancePremiums = new MI_PREMIUMS();

            if (this.loanData.sMiInsuranceT == E_sMiInsuranceT.LenderPaid)
            {
                mortgageInsurancePremiums.MiPremium.Add(this.CreateMiPremium_LPMI(1));
            }
            else
            {
                if (this.loanData.sProMInsMon > 0)
                {
                    mortgageInsurancePremiums.MiPremium.Add(this.CreateMiPremium(1, true));
                }

                if (this.loanData.sProMIns2Mon > 0)
                {
                    mortgageInsurancePremiums.MiPremium.Add(this.CreateMiPremium(mortgageInsurancePremiums.MiPremium.Count(p => p != null) + 1, false));
                }
            }

            return mortgageInsurancePremiums;
        }

        /// <summary>
        /// Creates a container to hold information on a mortgage insurance premium.
        /// </summary>
        /// <param name="sequence">The sequence number of the MI premium among the list of premiums.</param>
        /// <param name="monthly">True if the premium is the monthly/initial. False if it is the renewal.</param>
        /// <returns>An MI_PREMIUM container.</returns>
        private MI_PREMIUM CreateMiPremium(int sequence, bool monthly)
        {
            var mortgageInsurancePremium = new MI_PREMIUM();
            mortgageInsurancePremium.SequenceNumber = sequence;
            mortgageInsurancePremium.MiPremiumDetail = this.CreateMiPremiumDetail(monthly ? MIPremiumSequenceBase.First : MIPremiumSequenceBase.Second);

            return mortgageInsurancePremium;
        }

        /// <summary>
        /// Creates an MI_PREMIUM container with information regarding lender-paid mortgage insurance.
        /// </summary>
        /// <param name="sequence">The sequence number of the MI premium among the list of premiums.</param>
        /// <returns>An MI_PREMIUM container.</returns>
        private MI_PREMIUM CreateMiPremium_LPMI(int sequence)
        {
            var mortgageInsurancePremium = new MI_PREMIUM();
            mortgageInsurancePremium.SequenceNumber = sequence;
            mortgageInsurancePremium.MiPremiumDetail = this.CreateMiPremiumDetail_LPMI();
            mortgageInsurancePremium.MiPremiumPayments = this.CreateMiPremiumPayments_LPMI();

            return mortgageInsurancePremium;
        }

        /// <summary>
        /// Creates a container to hold details on a mortgage insurance premium.
        /// </summary>
        /// <param name="premiumSequence">The mortgage insurance premium sequence among the list of premiums.</param>
        /// <returns>An MI_PREMIUM_DETAIL container.</returns>
        private MI_PREMIUM_DETAIL CreateMiPremiumDetail(MIPremiumSequenceBase premiumSequence)
        {
            var mortgageInsurancePremiumDetail = new MI_PREMIUM_DETAIL();
            mortgageInsurancePremiumDetail.MIPremiumCalculationType = ToMIPremiumCalculationEnum(this.loanData.sProMInsT);

            if (mortgageInsurancePremiumDetail.MIPremiumCalculationType.enumValue == MIPremiumCalculationBase.Other)
            {
                mortgageInsurancePremiumDetail.MIPremiumCalculationTypeOtherDescription = ToMismoString(GetXmlEnumName(this.loanData.sProMInsT));
            }

            mortgageInsurancePremiumDetail.MIPremiumMonthlyPaymentAmount = ToMismoAmount(
                premiumSequence == MIPremiumSequenceBase.First ? this.loanData.sProMIns_rep : this.loanData.sProMIns2_rep);

            mortgageInsurancePremiumDetail.MIPremiumPeriodType = ToMIPremiumPeriodEnum(MIPremiumPeriodBase.Renewal);
            mortgageInsurancePremiumDetail.MIPremiumRateDurationMonthsCount = ToMismoCount(
                premiumSequence == MIPremiumSequenceBase.First ? this.loanData.sProMInsMon_rep : this.loanData.sProMIns2Mon_rep,
                null);

            mortgageInsurancePremiumDetail.MIPremiumRatePercent = ToMismoPercent(
                premiumSequence == MIPremiumSequenceBase.First ? this.loanData.sProMInsR_rep : this.loanData.sProMInsR2_rep);

            mortgageInsurancePremiumDetail.MIPremiumSequenceType = ToMIPremiumSequenceEnum(premiumSequence);
            
            return mortgageInsurancePremiumDetail;
        }

        /// <summary>
        /// Creates an MI_PREMIUM_DETAIL container with information regarding the lender-paid mortgage insurance premium.
        /// </summary>
        /// <returns>An MI_PREMIUM_DETAIL container.</returns>
        private MI_PREMIUM_DETAIL CreateMiPremiumDetail_LPMI()
        {
            var mortgageInsurancePremiumDetail = new MI_PREMIUM_DETAIL();

            mortgageInsurancePremiumDetail.MIPremiumCalculationType = ToMIPremiumCalculationEnum(MIPremiumCalculationBase.BaseLoanAmount);
            mortgageInsurancePremiumDetail.MIPremiumPeriodType = ToMIPremiumPeriodEnum(MIPremiumPeriodBase.Upfront);
            mortgageInsurancePremiumDetail.MIPremiumRatePercent = ToMismoPercent(this.loanData.sLenderUfmipR_rep);
            mortgageInsurancePremiumDetail.MIPremiumSequenceType = ToMIPremiumSequenceEnum(MIPremiumSequenceBase.First);

            return mortgageInsurancePremiumDetail;
        }

        /// <summary>
        /// Creates an MI_PREMIUM_PAYMENT container with information regarding the lender-paid mortgage insurance premium payment.
        /// </summary>
        /// <param name="sequence">The sequence of the payment among the list of payments.</param>
        /// <returns>An MI_PREMIUM_PAYMENT container.</returns>
        private MI_PREMIUM_PAYMENT CreateMiPremiumPayment_LPMI(int sequence)
        {
            var payment = new MI_PREMIUM_PAYMENT();
            payment.MIPremiumActualPaymentAmount = ToMismoAmount(this.loanData.sLenderUfmip_rep);
            payment.MIPremiumPaymentPaidByType = ToMIPremiumPaymentPaidByEnum(E_sMiInsuranceT.LenderPaid);
            payment.PaymentFinancedIndicator = ToMismoIndicator(false);
            payment.SequenceNumber = sequence;

            return payment;
        }

        /// <summary>
        /// Creates an MI_PREMIUM_PAYMENTS container with information regarding the lender-paid mortgage insurance premium payment.
        /// </summary>
        /// <returns>An MI_PREMIUM_PAYMENTS container.</returns>
        private MI_PREMIUM_PAYMENTS CreateMiPremiumPayments_LPMI()
        {
            var payments = new MI_PREMIUM_PAYMENTS();
            payments.MiPremiumPayment.Add(this.CreateMiPremiumPayment_LPMI(1));

            return payments;
        }

        /// <summary>
        /// Creates a NEAREST_LIVING_RELATIVE element.
        /// </summary>
        /// <param name="usePrimaryBorrowerInformation">True if borrower information should be used.
        /// False if coborrower information should be used.</param>
        /// <returns>A NEAREST_LIVING_RELATIVE element.</returns>
        private NEAREST_LIVING_RELATIVE CreateNearestLivingRelative(bool usePrimaryBorrowerInformation)
        {
            var nearestLivingRelative = new NEAREST_LIVING_RELATIVE();

            if (usePrimaryBorrowerInformation)
            {
                nearestLivingRelative.Name = CreateName(this.loanData.sFHAPropImprovBorrRelativeNm, isLegalEntity: false);
                nearestLivingRelative.NearestLivingRelativeDetail = this.CreateNearestLivingRelativeDetail(usePrimaryBorrowerInformation);

                nearestLivingRelative.ContactPoints = this.CreateContactPoints(
                    this.loanData.sFHAPropImprovBorrRelativePhone,
                    string.Empty,
                    string.Empty,
                    ContactPointRoleBase.Home);

                if (!string.IsNullOrEmpty(this.loanData.sFHAPropImprovBorrRelativeStreetAddress))
                {
                    nearestLivingRelative.Address = CreateAddress(
                        this.loanData.sFHAPropImprovBorrRelativeStreetAddress,
                        this.loanData.sFHAPropImprovBorrRelativeCity,
                        this.loanData.sFHAPropImprovBorrRelativeState,
                        this.loanData.sFHAPropImprovBorrRelativeZip,
                        string.Empty,
                        string.Empty,
                        this.statesAndTerritories.GetStateName(this.loanData.sFHAPropImprovBorrRelativeState),
                        AddressBase.Mailing,
                        sequenceNumber: 1);
                }
            }
            else
            {
                nearestLivingRelative.Name = CreateName(this.loanData.sFHAPropImprovCoborRelativeNm, isLegalEntity: false);
                nearestLivingRelative.NearestLivingRelativeDetail = this.CreateNearestLivingRelativeDetail(usePrimaryBorrowerInformation);

                nearestLivingRelative.ContactPoints = this.CreateContactPoints(
                    this.loanData.sFHAPropImprovCoborRelativePhone,
                    string.Empty,
                    string.Empty,
                    ContactPointRoleBase.Home);

                if (!string.IsNullOrEmpty(this.loanData.sFHAPropImprovCoborRelativeAddr))
                {
                    nearestLivingRelative.Address = CreateAddress(
                        this.loanData.sFHAPropImprovCoborRelativeStreetAddress,
                        this.loanData.sFHAPropImprovCoborRelativeCity,
                        this.loanData.sFHAPropImprovCoborRelativeState,
                        this.loanData.sFHAPropImprovCoborRelativeZip,
                        string.Empty,
                        string.Empty,
                        this.statesAndTerritories.GetStateName(this.loanData.sFHAPropImprovCoborRelativeState),
                        AddressBase.Mailing,
                        sequenceNumber: 1);
                }
            }

            return nearestLivingRelative;
        }

        /// <summary>
        /// Creates a NEAREST_LIVING_RELATIVE_DETAIL element.
        /// </summary>
        /// <param name="usePrimaryBorrowerInformation">True if borrower information should be used.
        /// False if coborrower information should be used.</param>
        /// <returns>A NEAREST_LIVING_RELATIVE_DETAIL element.</returns>
        private NEAREST_LIVING_RELATIVE_DETAIL CreateNearestLivingRelativeDetail(bool usePrimaryBorrowerInformation)
        {
            var nearestLivingRelativeDetail = new NEAREST_LIVING_RELATIVE_DETAIL();

            if (usePrimaryBorrowerInformation)
            {
                nearestLivingRelativeDetail.BorrowerNearestLivingRelativeRelationshipDescription = ToMismoString(this.loanData.sFHAPropImprovBorrRelativeRelationship);
            }
            else
            {
                nearestLivingRelativeDetail.BorrowerNearestLivingRelativeRelationshipDescription = ToMismoString(this.loanData.sFHAPropImprovCoborRelativeRelationship);
            }

            return nearestLivingRelativeDetail;
        }

        /// <summary>
        /// Creates a container holding information about negative amortization.
        /// </summary>
        /// <returns>A NEGATIVE_AMORTIZATION object.</returns>
        private NEGATIVE_AMORTIZATION CreateNegativeAmortization()
        {
            var negativeAmortization = new NEGATIVE_AMORTIZATION();
            negativeAmortization.NegativeAmortizationRule = this.CreateNegativeAmortizationRule();

            return negativeAmortization;
        }

        /// <summary>
        /// Creates a container holding data on the negative amortization rules.
        /// </summary>
        /// <returns>A NEGATIVE_AMORTIZATION_RULE object populated with data.</returns>
        private NEGATIVE_AMORTIZATION_RULE CreateNegativeAmortizationRule()
        {
            var negativeAmortizationRule = new NEGATIVE_AMORTIZATION_RULE();
            negativeAmortizationRule.NegativeAmortizationLimitPercent = ToMismoPercent(this.loanData.sPmtAdjMaxBalPc_rep);
            negativeAmortizationRule.NegativeAmortizationLimitMonthsCount = ToMismoCount(this.loanData.sPmtAdjRecastPeriodMon_rep, null);

            return negativeAmortizationRule;
        }

        /// <summary>
        /// Creates a container holding data on a note.
        /// </summary>
        /// <returns>A NOTE container.</returns>
        private NOTE CreateNote()
        {
            var note = new NOTE();
            note.AllongeToNote = this.CreateAllongeToNote();

            return note;
        }

        /// <summary>
        /// Creates a container with a Loan Disclosure or Consideration statement from the related integrated disclosure.
        /// </summary>
        /// <param name="statementType">The type of disclosure or consideration statement.</param>
        /// <param name="description">The text of the statement.</param>
        /// <param name="sequence">The sequence number of the statement among the list of statements.</param>
        /// <returns>An OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM object containing a consideration or disclosure statement from the related integrated disclosure.</returns>
        private OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase statementType, string description, int sequence)
        {
            var item = new OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM();
            item.LoanConsiderationDisclosureStatementDescription = ToMismoString(description);
            item.LoanConsiderationDisclosureStatementType = ToLoanConsiderationDisclosureStatementEnum(statementType);
            ////item.LoanConsiderationDisclosureStatementTypeOtherDescription;
            item.SequenceNumber = sequence;

            return item;
        }

        /// <summary>
        /// Creates a container with a list of Loan Disclosure and Consideration statements from the related integrated disclosure. 
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS object containing a list of consideration and disclosure statements from the related integrated disclosure.</returns>
        private OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS CreateOtherLoanConsiderationsAndDisclosuresItems(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var items = new OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS();
            string description = string.Empty;

            //// To-do: move these to the data-layer. Both the page and this/any export should pull these values from the data-layer rather than calculating them on the fly.
            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
            {
                description = "We may order an appraisal to determine the property's value and charge you for this appraisal. We will promptly give you a copy of any appraisal, even if your loan does not close. You can pay for an additional appraisal for your own use at your own cost.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Appraisal, description, 1));

                description = "If you sell or transfer this property to another person, we "; 
                description += (this.loanEstimateArchiveData.sAssumeLT == E_sAssumeLT.May || this.loanEstimateArchiveData.sAssumeLT == E_sAssumeLT.MaySubjectToCondition) ? 
                    "will allow, under certain conditions, this person to assume this loan on the original terms." : "will not allow assumption of this loan on the original terms.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Assumption, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                if (this.loanEstimateArchiveData.sReqPropIns)
                {
                    description = "This loan requires homeowner's insurance on the property, which you may obtain from a company of your choice that we find acceptable.";
                    items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.HomeownersInsurance, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));
                }

                description = string.Format("If your payment is more than {0} days late, we will charge a late fee of {1} of {2}.", this.loanEstimateArchiveData.sLateDays, this.loanEstimateArchiveData.sLateChargePc, this.loanEstimateArchiveData.sLateChargeBaseDesc);
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.LatePayment, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "Refinancing this loan will depend on your future financial situation, the property value, and market conditions. You may not be able to refinance this loan.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Refinance, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "We intend ";
                description += this.loanEstimateArchiveData.sTRIDLoanEstimateLenderIntendsToServiceLoan ? "to service your loan. If so, you will make payments to us." : "to transfer servicing of your loan.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Servicing, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "Certain State law protections against liability for any deficiency after foreclosure may be lost upon refinancing. The potential consequences of the loss of such protections include the consumer's liability for the unpaid balance. The consumer should consult an attorney for additional information.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.LiabilityAfterForeclosure, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));
            }
            else
            {
                //// Closing Disclosure
                description = "If you sell or transfer this property to another person, we ";
                description += (this.closingDisclosureArchiveData.sAssumeLT == E_sAssumeLT.May || this.closingDisclosureArchiveData.sAssumeLT == E_sAssumeLT.MaySubjectToCondition) ? "will allow, under certain conditions, this person to assume this loan on the original terms." : "will not allow assumption of this loan on the original terms.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Assumption, description, 1));

                description = "Your loan ";
                description += this.closingDisclosureArchiveData.sHasDemandFeature ? "has a demand feature which permits your lender to require early repayment of the loan." : "does not have a demand feature.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.DemandFeature, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = string.Format("If your payment is more than {0} days late, we will charge a late fee of {1} of {2}.", this.closingDisclosureArchiveData.sLateDays, this.closingDisclosureArchiveData.sLateChargePc, this.closingDisclosureArchiveData.sLateChargeBaseDesc);
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.LatePayment, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "Under your loan terms you ";
                if (this.closingDisclosureArchiveData.sTRIDNegativeAmortizationT == E_sTRIDNegativeAmortizationT.Scheduled)
                {
                    description += "are scheduled to make monthly payments that do not pay all of the interest due that month.";
                }
                else if (this.closingDisclosureArchiveData.sTRIDNegativeAmortizationT == E_sTRIDNegativeAmortizationT.Potential)
                {
                    description += "may have monthly payments that do not pay all of the interest due that month.";
                }
                else
                {
                    description += "do not have a negative amortization feature.";
                }

                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.NegativeAmortization, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "Your lender ";
                if (this.closingDisclosureArchiveData.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.AcceptedAndApplied)
                {
                    description += "may accept partial payments that are less than the full amount due (partial payments) and apply them to your loan.";
                }
                else if (this.closingDisclosureArchiveData.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.AcceptedButNotApplied)
                {
                    description += "may hold them in a separate account until you pay the rest of the payment, and then apply the full payment to your loan.";
                }
                else
                {
                    description += "does not accept partial payments.";
                }

                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.PartialPayment, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "You are granting a security interest in " + this.closingDisclosureArchiveData.sSpFullAddr;
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.SecurityInterest, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "For now, your loan ";
                if (this.closingDisclosureArchiveData.sTridEscrowAccountExists)
                {
                    description += "will have an escrow account to pay the property costs listed below.";
                }
                else
                {
                    description += "will not have an escrow account because ";
                    description += this.closingDisclosureArchiveData.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined ? "you declined it." : "your lender does not offer one.";
                }

                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.EscrowAccountCurrent, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "We may order an appraisal to determine the property's value and charge you for this appraisal. We will promptly give you a copy of any appraisal, even if your loan does not close. You can pay for an additional appraisal for your own use at your own cost.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Appraisal, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "See your note and security instrument for information about what happens if you fail to make your payments, what is a default on the loan, situations in which your lender can require early repayments of the loan, and the rules for making payments before they are due.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.ContractDetails, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "If your lender forecloses on this property and the foreclosure does not cover the amount of unpaid balance on this loan, ";
                description += this.closingDisclosureArchiveData.sTRIDPropertyIsInNonRecourseLoanState ? "state law may protect you from liability for the unpaid balance. If you refinance or take on any additional debt on this property, you may lose this protection and have to pay any debt remaining even after foreclosure. You may want to consult a lawyer for more information." : "state law does not protect you from liability for the unpaid balance.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.LiabilityAfterForeclosure, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "Refinancing this loan will depend on your future financial situation, the property value, and market conditions. You may not be able to refinance this loan.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Refinance, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));

                description = "If you borrow more than this property is worth, the interest on the loan amount above this property's fair market value is not deductible from your federal income taxes. You should consult a tax advisor for more information.";
                items.OtherLoanConsiderationsAndDisclosuresItem.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.TaxDeductions, description, items.OtherLoanConsiderationsAndDisclosuresItem.Count(i => i != null) + 1));
            }
            //// End-of needs to be moved.

            return items;
        }

        /// <summary>
        /// Creates a container representing an owned property.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <returns>An OWNED_PROPERTY container.</returns>
        private OWNED_PROPERTY CreateOwnedProperty(IRealEstateOwned reo)
        {
            var ownedProperty = new OWNED_PROPERTY();
            ownedProperty.Property = this.CreateProperty(reo, 1);
            ownedProperty.OwnedPropertyDetail = this.CreateOwnedPropertyDetail(reo);

            return ownedProperty;
        }

        /// <summary>
        /// Creates a container holding details on an owned property.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <returns>An OWNED_PROPERTY_DETAIL container.</returns>
        private OWNED_PROPERTY_DETAIL CreateOwnedPropertyDetail(IRealEstateOwned reo)
        {
            var ownedPropertyDetail = new OWNED_PROPERTY_DETAIL();
            ownedPropertyDetail.OwnedPropertyDispositionStatusType = ToOwnedPropertyDispositionStatusEnum(reo.StatT);
            ownedPropertyDetail.OwnedPropertyLienInstallmentAmount = ToMismoAmount(reo.MPmt_rep);
            ownedPropertyDetail.OwnedPropertyLienUPBAmount = ToMismoAmount(reo.MAmt_rep);
            ownedPropertyDetail.OwnedPropertyMaintenanceExpenseAmount = ToMismoAmount(reo.HExp_rep);
            ownedPropertyDetail.OwnedPropertyRentalIncomeGrossAmount = ToMismoAmount(reo.GrossRentI_rep);
            ownedPropertyDetail.OwnedPropertyRentalIncomeNetAmount = ToMismoAmount(reo.NetRentI_rep);
            if (reo.TypeT == E_ReoTypeT._2_4Plx)
            {
                ownedPropertyDetail.OwnedPropertyOwnedUnitCount = ToMismoCount(2, new LosConvert());
            }
            else if (reo.TypeT == E_ReoTypeT.Multi)
            {
                ownedPropertyDetail.OwnedPropertyOwnedUnitCount = ToMismoCount(5, new LosConvert());
            }

            ownedPropertyDetail.Extension = this.CreateOwnedPropertyDetailExtension(reo.TypeT);
            return ownedPropertyDetail;
        }

        /// <summary>
        /// Creates a container to add custom data points to the OWNED_PROPERTY_DETAIL container.
        /// </summary>
        /// <param name="propertyType">The property type.</param>
        /// <returns>An OWNED_PROPERTY_DETAIL_EXTENSION container.</returns>
        private OWNED_PROPERTY_DETAIL_EXTENSION CreateOwnedPropertyDetailExtension(E_ReoTypeT propertyType)
        {
            var extension = new OWNED_PROPERTY_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBOwnedPropertyDetailExtension(propertyType);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container to hold extra data on owned properties.
        /// </summary>
        /// <param name="propertyType">The property type.</param>
        /// <returns>An LQB_OWNED_PROPERTY_DETAIL_EXTENSION container.</returns>
        private LQB_OWNED_PROPERTY_DETAIL_EXTENSION CreateLQBOwnedPropertyDetailExtension(E_ReoTypeT propertyType)
        {
            var extension = new LQB_OWNED_PROPERTY_DETAIL_EXTENSION();
            extension.GsePropertyType = ToGsePropertyEnum(propertyType);

            return extension;
        }

        /// <summary>
        /// Creates a borrower payoff.
        /// </summary>
        /// <param name="payoffAmount">The payoff amount.</param>
        /// <returns>A PAYOFF element.</returns>
        private PAYOFF CreatePayoff_Borrower(string payoffAmount)
        {
            var disclosureSection = GetDisclosureSectionForSectionKAdjustments(
                    this.isClosingPackage,
                    this.loanData.sTRIDLoanEstimateCashToCloseCalcMethodT,
                    this.loanData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT);

            return this.CreatePayoff(
                payoffAmount,
                null,
                null,
                disclosureSection);
        }

        /// <summary>
        /// Creates a seller payoff.
        /// </summary>
        /// <param name="payoffAmount">The payoff amount.</param>
        /// <returns>A PAYOFF element.</returns>
        private PAYOFF CreatePayoff_Seller(string payoffAmount)
        {
            return this.CreatePayoff(
                payoffAmount, 
                null,
                null,
                IntegratedDisclosureSectionBase.DueFromSellerAtClosing);
        }

        /// <summary>
        /// Creates PAYOFF container with specified payoff amount.
        /// </summary>
        /// <param name="payoffAmount">The specified payoff amount.</param>
        /// <param name="adjustmentType">The adjustment type from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="excludeFromLECDForThisLien">The adjustment exclude from CE LE setting from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="section">The section identifier enum for the payoff.</param>
        /// <returns>Returns a PAYOFF container with specified payoff amount.</returns>
        private PAYOFF CreatePayoff(string payoffAmount, E_AdjustmentT? adjustmentType, bool? excludeFromLECDForThisLien, IntegratedDisclosureSectionBase section)
        {
            var payoff = new PAYOFF();
            payoff.PayoffAmount = ToMismoAmount(payoffAmount);
            payoff.IntegratedDisclosureSectionType = ToIntegratedDisclosureSectionEnum(section);
            payoff.Extension = this.CreatePayoffExtension(adjustmentType, excludeFromLECDForThisLien);
            return payoff;
        }

        /// <summary>
        /// Creates a container extending the PAYOFF element.
        /// </summary>
        /// <param name="adjustmentType">The adjustment type from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="excludeFromLECDForThisLien">The adjustment exclude from CE LE setting from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <returns>A PAYOFF_EXTENSION container.</returns>
        private PAYOFF_EXTENSION CreatePayoffExtension(E_AdjustmentT? adjustmentType, bool? excludeFromLECDForThisLien)
        {
            var extension = new PAYOFF_EXTENSION();
            extension.Other = this.CreateLqbPayoffExtension(adjustmentType, excludeFromLECDForThisLien);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LendingQB extension to the PAYOFF element.
        /// </summary>
        /// <param name="adjustmentType">The adjustment type from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="excludeFromLECDForThisLien">The adjustment exclude from CE LE setting from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <returns>An LQB_PAYOFF_EXTENSION container.</returns>
        private LQB_PAYOFF_EXTENSION CreateLqbPayoffExtension(E_AdjustmentT? adjustmentType, bool? excludeFromLECDForThisLien)
        {
            var extension = new LQB_PAYOFF_EXTENSION();

            if (adjustmentType.HasValue && excludeFromLECDForThisLien.HasValue)
            {
                if (ShouldSendTrid2017SubordinateFinancingData(this.loanData.sLienPosT, this.loanData.sIsOFinNew, this.loanData.sConcurSubFin, this.loanData.sTridTargetRegulationVersionT))
                {
                    extension.ExcludeFromLECDForThisLien = ToMismoIndicator(excludeFromLECDForThisLien.Value);
                }

                if (adjustmentType.Value == E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)
                {
                    extension.AdjustmentIsPrincipalReduction = ToMismoIndicator(true);
                    extension.PrincipalReductionForToleranceCure = ToMismoIndicator(false);
                }
                else if (adjustmentType.Value == E_AdjustmentT.PrincipalReductionToCureToleranceViolation)
                {
                    extension.AdjustmentIsPrincipalReduction = ToMismoIndicator(true);
                    extension.PrincipalReductionForToleranceCure = ToMismoIndicator(true);
                }
            }
            else
            {
                if (ShouldSendTrid2017SubordinateFinancingData(this.loanData.sLienPosT, this.loanData.sIsOFinNew, this.loanData.sConcurSubFin, this.loanData.sTridTargetRegulationVersionT))
                {
                    extension.ExcludeFromLECDForThisLien = ToMismoIndicator(this.loanData.sLienToPayoffTotDebt == ResponsibleLien.OtherLienTransaction);
                }
            }

            return extension;
        }

        /// <summary>
        /// Creates a PREFERRED_RESPONSE container with information regarding the intended response, e.g. XML format via HTTPS.
        /// </summary>
        /// <returns>A PREFERRED_RESPONSE container.</returns>
        private PREFERRED_RESPONSE CreatePreferredResponse()
        {
            var response = new PREFERRED_RESPONSE();
            response.PreferredResponseDestinationDescription = ToMismoString("Direct the response to LendingQB.");
            response.PreferredResponseFormatType = ToPreferredResponseFormatEnum(PreferredResponseFormatBase.XML);
            response.PreferredResponseMethodType = ToPreferredResponseMethodEnum(PreferredResponseMethodBase.HTTPS);
            response.SequenceNumber = 1;

            return response;
        }

        /// <summary>
        /// Creates a PREFERRED_RESPONSES container with a list of preferred responses.
        /// </summary>
        /// <returns>A PREFERRED_RESPONSES container.</returns>
        private PREFERRED_RESPONSES CreatePreferredResponses()
        {
            var responses = new PREFERRED_RESPONSES();
            responses.PreferredResponse.Add(this.CreatePreferredResponse());

            return responses;
        }

        /// <summary>
        /// Creates a PREPAID_ITEM container with information regarding the given prepaid fee.
        /// </summary>
        /// <param name="prepaidFee">The prepaid fee.</param>
        /// <param name="estimatedFee">The last disclosed estimate of the prepaid fee.</param>
        /// <param name="sequence">The sequence number of the prepaid item among the list of items.</param>
        /// <returns>A PREPAID_ITEM container with information regarding the given prepaid fee.</returns>
        private PREPAID_ITEM CreatePrepaidItem(BorrowerClosingCostFee prepaidFee, BorrowerClosingCostFee estimatedFee, int sequence)
        {
            CAgentFields beneficiaryContact = null;
            if (prepaidFee.BeneficiaryAgentId != Guid.Empty)
            {
                beneficiaryContact = this.loanData.GetAgentFields(prepaidFee.BeneficiaryAgentId);
            }

            var prepaid = new PREPAID_ITEM();

            prepaid.PrepaidItemDetail = this.CreatePrepaidItemDetail(prepaidFee, estimatedFee, beneficiaryContact);
            prepaid.PrepaidItemPaidTo = CreatePaidTo(beneficiaryContact, prepaidFee.BeneficiaryDescription, prepaidFee.BeneficiaryAgentId);
            prepaid.PrepaidItemPayments = this.CreatePrepaidItemPayments(prepaidFee.Payments, prepaidFee.IsApr, prepaidFee.QmAmount > 0.00m);

            if (beneficiaryContact != null && !prepaidFee.CanShop)
            {
                prepaid.RequiredServiceProvider = CreateRequiredServiceProvider(beneficiaryContact);
            }

            var identifiedProviders = this.loanData.sAvailableSettlementServiceProviders.GetProvidersForFeeTypeId(prepaidFee.ClosingCostFeeTypeId);
            prepaid.IdentifiedServiceProviders = CreateIdentifiedServiceProviders(identifiedProviders);

            prepaid.SequenceNumber = sequence;

            return prepaid;
        }

        /// <summary>
        /// Creates a PREPAID_ITEM_DETAIL with information regarding the given prepaid fee.
        /// </summary>
        /// <param name="prepaidFee">The prepaid fee.</param>
        /// <param name="estimatedPrepaidFee">The last disclosed estimate of the prepaid fee.</param>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <returns>A PREPAID_ITEM_DETAIL container with information regarding the given prepaid fee.</returns>
        private PREPAID_ITEM_DETAIL CreatePrepaidItemDetail(BorrowerClosingCostFee prepaidFee, BorrowerClosingCostFee estimatedPrepaidFee, CAgentFields beneficiaryContact)
        {
            var detail = new PREPAID_ITEM_DETAIL();

            detail.BorrowerChosenProviderIndicator = ToMismoIndicator(prepaidFee.DidShop);

            MISMOString paidToTypeOtherDescription = null;

            detail.FeePaidToType = GetFeePaidToType(
                beneficiaryContact, 
                prepaidFee.IsAffiliate, 
                prepaidFee.IsThirdParty, 
                prepaidFee.BeneficiaryType, 
                prepaidFee.Beneficiary, 
                prepaidFee.BeneficiaryDescription, 
                out paidToTypeOtherDescription);

            if (paidToTypeOtherDescription != null)
            {
                detail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
            }

            var documentType = this.isClosingPackage ? IntegratedDisclosureDocumentBase.ClosingDisclosure : IntegratedDisclosureDocumentBase.LoanEstimate;
            detail.IntegratedDisclosureSectionType = ToIntegratedDisclosureSectionEnum(documentType, prepaidFee.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage), prepaidFee.CanShop);
            
            if (detail.IntegratedDisclosureSectionType != null && detail.IntegratedDisclosureSectionType.enumValue == IntegratedDisclosureSectionBase.Other)
            {
                detail.IntegratedDisclosureSectionTypeOtherDescription = ToMismoString(GetXmlEnumName(prepaidFee.GetTRIDSectionBasedOnDisclosure(this.isClosingPackage)));
            }

            detail.PrepaidItemActualTotalAmount = ToMismoAmount(prepaidFee.TotalAmount_rep);

            if (estimatedPrepaidFee != null)
            {
                detail.PrepaidItemEstimatedTotalAmount = ToMismoAmount(estimatedPrepaidFee.TotalAmount_rep);
            }

            E_TaxTableTaxT taxType = E_TaxTableTaxT.LeaveBlank;

            if (prepaidFee.IsPrepaidFromExpensesFee)
            {
                var housingExpense = this.loanData.sHousingExpenses.GetExpenseFrom900Or1000FeeTypeId(prepaidFee.ClosingCostFeeTypeId);

                if (housingExpense != null)
                {
                    detail.PrepaidItemMonthlyAmount = ToMismoAmount(housingExpense.MonthlyAmtTotal_rep);
                    detail.PrepaidItemMonthsPaidCount = ToMismoCount(housingExpense.PrepaidMonths_rep, null);
                }

                taxType = housingExpense.TaxType;
            }
            else if ((prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId 
                        || prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId
                        || prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
                    && this.loanData.sMipPiaMon > 0)
            {
                detail.PrepaidItemMonthsPaidCount = ToMismoCount(this.loanData.sMipPiaMon_rep, null);
            }
            else if (prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
            {
                if (prepaidFee.NumberOfPeriods < 0 && this.vendor == E_DocumentVendor.DocMagic)
                {
                    // Since MISMOCount requires a positive value, for a negative interim interest term we take the absolute value.
                    detail.PrepaidItemNumberOfDaysCount = ToMismoCount(Math.Abs(prepaidFee.NumberOfPeriods), this.loanData.m_convertLos);
                    detail.FeePaidToType = ToFeePaidToEnum(FeePaidToBase.Other);
                    detail.FeePaidToTypeOtherDescription = ToMismoString("Borrower");
                }
                else
                {
                    detail.PrepaidItemNumberOfDaysCount = ToMismoCount(prepaidFee.NumberOfPeriods_rep, null);
                }

                detail.PrepaidItemPaidFromDate = ToMismoDate(this.loanData.sConsummationD_rep);
                
                if (this.loanData.sSchedDueD1.IsValid)
                {
                    detail.PrepaidItemPaidThroughDate = ToMismoDate(this.loanData.m_convertLos.ToDateTimeString(this.loanData.sSchedDueD1.DateTimeForComputation.AddMonths(-1)));
                }

                detail.PrepaidItemPerDiemAmount = ToMismoAmount(prepaidFee.BaseAmount_rep);
                detail.PrepaidItemPerDiemCalculationMethodType = ToPrepaidItemPerDiemCalculationMethodEnum(this.loanData.sDaysInYr);

                if (detail.PrepaidItemPerDiemCalculationMethodType != null && detail.PrepaidItemPerDiemCalculationMethodType.enumValue == PrepaidItemPerDiemCalculationMethodBase.Other)
                {
                    detail.PrepaidItemPerDiemCalculationMethodTypeOtherDescription = ToMismoString(this.loanData.sDaysInYr_rep);
                }
            }

            detail.PrepaidItemType = ToPrepaidItemEnum(prepaidFee.ClosingCostFeeTypeId, taxType, prepaidFee.Description);

            if (detail.PrepaidItemType != null && detail.PrepaidItemType.enumValue == PrepaidItemBase.Other)
            {
                detail.PrepaidItemTypeOtherDescription = ToMismoString(prepaidFee.Description);
            }
            
            detail.RegulationZPointsAndFeesIndicator = ToMismoIndicator(prepaidFee.IsIncludedInQm);
            detail.RequiredProviderOfServiceIndicator = ToMismoIndicator(!prepaidFee.CanShop);

            detail.Extension = this.CreatePrepaidItemDetailExtension(prepaidFee);

            return detail;
        }

        /// <summary>
        /// Creates an extension container with extra data about this prepaid item.
        /// </summary>
        /// <param name="prepaidFee">The fee representing the prepaid item.</param>
        /// <returns>A <see cref="PREPAID_ITEM_DETAIL_EXTENSION"/> container.</returns>
        private PREPAID_ITEM_DETAIL_EXTENSION CreatePrepaidItemDetailExtension(BorrowerClosingCostFee prepaidFee)
        {
            var extension = new PREPAID_ITEM_DETAIL_EXTENSION();

            extension.Other = this.CreateLQBPrepaidItemDetailExtension(prepaidFee);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LQB extension container with extra non-MISMO data.
        /// </summary>
        /// <param name="prepaidFee">The fee representing the prepaid item.</param>
        /// <returns>A <see cref="LQB_PREPAID_ITEM_DETAIL_EXTENSION"/> container.</returns>
        private LQB_PREPAID_ITEM_DETAIL_EXTENSION CreateLQBPrepaidItemDetailExtension(BorrowerClosingCostFee prepaidFee)
        {
            var extension = new LQB_PREPAID_ITEM_DETAIL_EXTENSION();
            extension.PrepaidItemSpecifiedHUD1LineNumberValue = ToMismoValue(prepaidFee.HudLine_rep);
            extension.ClosingCostFeeTypeID = ToMismoString(prepaidFee.ClosingCostFeeTypeId.ToString());

            if (prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
            {
                extension.PrepaidItemPerDiemUnroundedAmount = ToMismoNumeric(this.loanData.sIPerDay_rep);
            }

            return extension;
        }

        /// <summary>
        /// Creates a container holding a set of prepaid items.
        /// </summary>
        /// <returns>A PREPAID_ITEMS container.</returns>
        private PREPAID_ITEMS CreatePrepaidItems()
        {
            var prepaidItems = new PREPAID_ITEMS();
            var prepaidFees = this.GetPrepaidFees();

            if (!prepaidFees.Any())
            {
                return null;
            }

            BorrowerClosingCostSet estimatedFeeSet = null;
            if (this.loanData.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017
                && this.loanData.sAppliedCCArchiveLinkedArchiveId != Guid.Empty)
            {
                estimatedFeeSet = this.loanData.GetClosingCostArchiveById(this.loanData.sAppliedCCArchiveLinkedArchiveId)?.GetClosingCostSet(this.loanData.m_convertLos);
            }
            else
            {
                estimatedFeeSet = this.loanData.LastDisclosedLoanEstimateArchive?.GetClosingCostSet(this.loanData.m_convertLos);
            }

            foreach (BorrowerClosingCostFee fee in prepaidFees)
            {
                var estimatedFee = (BorrowerClosingCostFee)estimatedFeeSet?.FindFeeByTypeId(fee.ClosingCostFeeTypeId);
                prepaidItems.PrepaidItem.Add(this.CreatePrepaidItem(fee, estimatedFee, prepaidItems.PrepaidItem.Count(p => p != null) + 1));
            }

            return prepaidItems;
        }

        /// <summary>
        /// Creates a PREPAID_ITEM_PAYMENT container with information pertaining to a prepaid fee payment.
        /// </summary>
        /// <param name="payment">An individual payment for a prepaid fee.</param>
        /// <param name="apr">True if the prepaid fee is to be included in APR calculations. Otherwise false.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <param name="sequence">The sequence number of the payment among the list of payments.</param>
        /// <returns>A PREPAID_ITEM_PAYMENT container with information pertaining to a prepaid fee payment.</returns>
        private PREPAID_ITEM_PAYMENT CreatePrepaidItemPayment(BorrowerClosingCostFeePayment payment, bool apr, bool qm, int sequence)
        {
            var prepaidItemPayment = new PREPAID_ITEM_PAYMENT();

            prepaidItemPayment.PaymentFinancedIndicator = ToMismoIndicator(payment.IsFinanced);
            prepaidItemPayment.PaymentIncludedInAPRIndicator = ToMismoIndicator(apr);
            prepaidItemPayment.PrepaidItemActualPaymentAmount = ToMismoAmount(payment.Amount_rep);
            prepaidItemPayment.PrepaidItemPaymentPaidByType = ToPrepaidItemPaymentPaidByEnum(payment.GetPaidByType(this.loanData.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage));

            if (prepaidItemPayment.PrepaidItemPaymentPaidByType != null && prepaidItemPayment.PrepaidItemPaymentPaidByType.enumValue == PrepaidItemPaymentPaidByBase.Other)
            {
                prepaidItemPayment.PrepaidItemPaymentPaidByTypeOtherDescription = ToMismoString(GetXmlEnumName(payment.GetPaidByType(this.loanData.sLenderPaidFeeDiscloseLocationT, this.isClosingPackage)));
            }

            prepaidItemPayment.PrepaidItemPaymentTimingType = ToPrepaidItemPaymentTimingEnum(payment.GfeClosingCostFeePaymentTimingT);

            if (prepaidItemPayment.PrepaidItemPaymentTimingType != null && prepaidItemPayment.PrepaidItemPaymentTimingType.enumValue == PrepaidItemPaymentTimingBase.Other)
            {
                prepaidItemPayment.PrepaidItemPaymentTimingTypeOtherDescription = ToMismoString(GetXmlEnumName(payment.GfeClosingCostFeePaymentTimingT));
            }

            prepaidItemPayment.RegulationZPointsAndFeesIndicator = ToMismoIndicator(qm);
            prepaidItemPayment.Extension = this.CreatePrepaidItemPaymentExtension(payment.Id.ToString());
            prepaidItemPayment.SequenceNumber = sequence;

            return prepaidItemPayment;
        }

        /// <summary>
        /// Creates an extension container holding data on a prepaid item payment.
        /// </summary>
        /// <param name="paymentId">The prepaid item payment ID.</param>
        /// <returns>A serializable <see cref="PREPAID_ITEM_PAYMENT_EXTENSION"/> container.</returns>
        private PREPAID_ITEM_PAYMENT_EXTENSION CreatePrepaidItemPaymentExtension(string paymentId)
        {
            var extension = new PREPAID_ITEM_PAYMENT_EXTENSION();
            extension.Other = this.CreateLqbPrepaidItemPaymentExtension(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container holding data on a prepaid item payment.
        /// </summary>
        /// <param name="paymentId">The prepaid item payment ID.</param>
        /// <returns>A serializable <see cref="LQB_PREPAID_ITEM_PAYMENT_EXTENSION"/> container.</returns>
        private LQB_PREPAID_ITEM_PAYMENT_EXTENSION CreateLqbPrepaidItemPaymentExtension(string paymentId)
        {
            var extension = new LQB_PREPAID_ITEM_PAYMENT_EXTENSION();
            extension.Id = ToMismoString(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a PREPAID_ITEM_PAYMENTS container with the payments associated with a prepaid fee.
        /// </summary>
        /// <param name="payments">The set of payments for a prepaid fee.</param>
        /// <param name="apr">True if the prepaid fee is to be included in APR calculations. Otherwise false.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <returns>A PREPAID_ITEM_PAYMENTS container with the payments associated with a prepaid fee.</returns>
        private PREPAID_ITEM_PAYMENTS CreatePrepaidItemPayments(IEnumerable<LoanClosingCostFeePayment> payments, bool apr, bool qm)
        {
            var prepaidItemPayments = new PREPAID_ITEM_PAYMENTS();

            foreach (BorrowerClosingCostFeePayment payment in payments.Where(p => p.Amount > 0.00m))
            {
                prepaidItemPayments.PrepaidItemPayment.Add(this.CreatePrepaidItemPayment(payment, apr, qm, prepaidItemPayments.PrepaidItemPayment.Count(p => p != null) + 1));
            }

            return prepaidItemPayments;
        }

        /// <summary>
        /// Creates a container holding a parcel identification.
        /// </summary>
        /// <param name="sequence">The sequence number of the parcel identification among the list of identifications.</param>
        /// <returns>A PARCEL_IDENTIFICATION container.</returns>
        private PARCEL_IDENTIFICATION CreateParcelIdentification(int sequence)
        {
            var parcelIdentification = new PARCEL_IDENTIFICATION();
            parcelIdentification.ParcelIdentifier = ToMismoIdentifier(this.loanData.sAssessorsParcelId);
            parcelIdentification.ParcelIdentificationType = ToParcelIdentificationEnum(ParcelIdentificationBase.AssessorUnformattedIdentifier);
            parcelIdentification.SequenceNumber = sequence;

            return parcelIdentification;
        }

        /// <summary>
        /// Creates a container holding parcel identifications.
        /// </summary>
        /// <returns>A PARCEL_IDENTIFICATIONS container.</returns>
        private PARCEL_IDENTIFICATIONS CreateParcelIdentifications()
        {
            var parcelIdentifications = new PARCEL_IDENTIFICATIONS();
            parcelIdentifications.ParcelIdentification.Add(this.CreateParcelIdentification(1));

            return parcelIdentifications;
        }

        /// <summary>
        /// Creates a PARTIES container holding all party information for the loan file, and
        /// populates RELATIONSHIPS related to parties as necessary.
        /// </summary>
        /// <returns>A PARTIES container populated with all party information for the loan file.</returns>
        private PARTIES CreateParties()
        {
            var parties = new PARTIES();
            var borrowerNames = new List<string>();

            // Borrower Parties
            int appsCount = this.loanData.nApps;
            var borrowersByApplicationId = new Dictionary<Guid, string>();
            for (int borrowerIndex = 0; borrowerIndex < appsCount; borrowerIndex++)
            {
                CAppData appData = this.loanData.GetAppData(borrowerIndex);
                
                // Skip if there is no borrower or coborrower
                if (!appData.aBIsValidNameSsn && !appData.aCIsValidNameSsn)
                {
                    continue;
                }

                if (appData.aBIsValidNameSsn)
                {
                    appData.BorrowerModeT = E_BorrowerModeT.Borrower;
                    parties.Party.Add(this.CreateParty(appData));
                    parties.Party.Add(this.CreateParty_AttorneyInFact(appData.aBPowerOfAttorneyNm, appData.aBMismoId));

                    this.allRelationships.Add(this.CreateRelationship_ToLoan("ROLE", appData.aBMismoId, ArcroleVerbPhrase.IsAssociatedWith));
                    borrowerNames.Add(appData.aBNm);

                    borrowersByApplicationId.Add(appData.aAppId, appData.aBMismoId);
                }

                if (appData.aCIsValidNameSsn)
                {
                    appData.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    parties.Party.Add(this.CreateParty(appData));
                    parties.Party.Add(this.CreateParty_AttorneyInFact(appData.aCPowerOfAttorneyNm, appData.aCMismoId));

                    if (this.vendor != E_DocumentVendor.DocMagic || (appData.aCTypeT != E_aTypeT.TitleOnly && appData.aCTypeT != E_aTypeT.NonTitleSpouse))
                    {
                        this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.SharesSufficientAssetsAndLiabilitiesWith, "ROLE", "ROLE", appData.aBMismoId, appData.aCMismoId));
                    }

                    this.allRelationships.Add(this.CreateRelationship_ToLoan("ROLE", appData.aCMismoId, ArcroleVerbPhrase.IsAssociatedWith));
                    borrowerNames.Add(appData.aCNm);
                }
            }

            // TitleBorrower Parties
            foreach (var borrower in this.loanData.sTitleBorrowers)
            {
                if (borrower.FullName.TrimWhitespaceAndBOM().Length > 0 || borrower.SSN.Length > 0)
                {
                    PARTY titleOnlyBorrowerParty = this.CreateParty_TitleOnly(borrower);
                    parties.Party.Add(titleOnlyBorrowerParty);
                    parties.Party.Add(this.CreateParty_AttorneyInFact(borrower.POA, titleOnlyBorrowerParty.Roles.Role.Count > 0 ? titleOnlyBorrowerParty.Roles.Role[0].label : string.Empty));
                    borrowerNames.Add(borrower.FullName);

                    if (borrower.AssociatedApplicationId != Guid.Empty && borrowersByApplicationId.ContainsKey(borrower.AssociatedApplicationId))
                    {
                        this.allRelationships.Add(
                            this.CreateRelationship(
                                ArcroleVerbPhrase.SharesSufficientAssetsAndLiabilitiesWith, 
                                "ROLE", 
                                "ROLE", 
                                borrowersByApplicationId[borrower.AssociatedApplicationId], 
                                titleOnlyBorrowerParty.Roles.Role[0].label));
                    }
                }
            }

            // Agent Parties
            List<Guid> agentsToExclude = this.CompileExcludedAgents();
            for (int i = 0; i < this.loanData.GetAgentRecordCount(); i++)
            {
                CAgentFields agent = this.loanData.GetAgentFields(i);

                if (agentsToExclude.Contains(agent.RecordId))
                {
                    continue;
                }

                var individualAgent = this.CreateParty(agent, EntityType.Individual);
                var legalEntityAgent = this.CreateParty(agent, EntityType.LegalEntity);

                parties.Party.Add(individualAgent);
                parties.Party.Add(legalEntityAgent);

                if (individualAgent != null && legalEntityAgent != null)
                {
                    this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualAgent.Roles.Role[0].label, legalEntityAgent.Roles.Role[0].label));
                }
            }

            foreach (var supervisor in this.partyLabels.RetrieveLabels("AppraiserSupervisor", EntityType.Individual))
            {
                foreach (var appraiser in this.partyLabels.RetrieveLabels("Appraiser", EntityType.Individual))
                {
                    this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsSupervisedBy, "ROLE", "ROLE", appraiser, supervisor));
                }
            }

            parties.Party.AddRange(this.CreatePartyList_Disclosure());

            if (this.vendor == E_DocumentVendor.DocMagic || this.vendor == E_DocumentVendor.DocsOnDemand || this.vendor == E_DocumentVendor.DocuTech || this.vendor == E_DocumentVendor.SigniaDocuments)
            {
                // Each provider instance, unique or not, is associated with a different fee. Create a data structure that relates
                // each UNIQUE provider with a set of the descriptions and estimated amounts of their associated fees.
                var feeTypes = this.loanData.sAvailableSettlementServiceProviders.GetFeeTypeIds();
                var feesByProvider = new Dictionary<Guid, Tuple<SettlementServiceProvider, List<Tuple<string, decimal>>>>();
                foreach (var feeType in feeTypes)
                {
                    var providersForFee = this.loanData.sAvailableSettlementServiceProviders.GetProvidersForFeeTypeId(feeType);
                    foreach (var providerForFee in providersForFee)
                    {
                        // For any providers that are not loan contacts and don't have their own agent ID, we will assign a
                        // new GUID to prevent all the empty GUIDs from deduping against each other.
                        if (providerForFee.AgentId == Guid.Empty)
                        {
                            providerForFee.AgentId = Guid.NewGuid();
                        }

                        if (!feesByProvider.ContainsKey(providerForFee.AgentId))
                        {
                            feesByProvider[providerForFee.AgentId] = new Tuple<SettlementServiceProvider, List<Tuple<string, decimal>>>(providerForFee, new List<Tuple<string, decimal>>());
                        }

                        var fee = this.loanData.sClosingCostSet.FindFeeByTypeId(feeType);
                        feesByProvider[providerForFee.AgentId].Item2.Add(new Tuple<string, decimal>(fee.Description, providerForFee.EstimatedCostAmount));
                    }
                }

                foreach (var kvp in feesByProvider)
                {
                    parties.Party.Add(this.CreateParty_SSP(kvp.Value.Item1, kvp.Value.Item2));
                }
            }

            // Trust Parties
            parties.Party.Add(this.CreateParty_Trust());

            foreach (var settlor in this.loanData.sTrustCollection.Trustors)
            {
                if (!borrowerNames.Contains(settlor.Name) && !string.IsNullOrWhiteSpace(settlor.Name))
                {
                    parties.Party.Add(this.CreateParty_Settlor(settlor));
                }
            }

            foreach (var trustee in this.loanData.sTrustCollection.Trustees)
            {
                if (!borrowerNames.Contains(trustee.Name) && !string.IsNullOrWhiteSpace(trustee.Name))
                {
                    parties.Party.Add(this.CreateParty_Trustee(trustee));
                }
            }

            foreach (var trust in this.partyLabels.RetrieveLabels("Trust", EntityType.LegalEntity))
            {
                foreach (var settlor in this.partyLabels.RetrieveLabels("Settlor", EntityType.Individual))
                {
                    this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsSettlorFor, "ROLE", "ROLE", settlor, trust));
                }

                foreach (var trustee in this.partyLabels.RetrieveLabels("Trustee", EntityType.Individual))
                {
                    this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsTrusteeFor, "ROLE", "ROLE", trustee, trust));
                }
            }

            // Other Parties
            parties.Party.Add(this.CreateParty_Lender());
            parties.Party.Add(this.CreateParty_LQB());

            BranchDB lenderBranch = new BranchDB(this.loanData.sBranchId, this.loanData.sBrokerId);
            lenderBranch.Retrieve();
            parties.Party.Add(this.CreateParty(lenderBranch));

            var individualFHAAddendumSponsor = this.CreateParty_Preparer(this.loanData.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject), PartyRoleBase.FHASponsor, EntityType.Individual);
            var legalEntityFHAAddendumSponsor = this.CreateParty_Preparer(this.loanData.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject), PartyRoleBase.FHASponsor, EntityType.LegalEntity);
            parties.Party.Add(individualFHAAddendumSponsor);
            parties.Party.Add(legalEntityFHAAddendumSponsor);

            if (individualFHAAddendumSponsor != null && legalEntityFHAAddendumSponsor != null)
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualFHAAddendumSponsor.Roles.Role[0].label, legalEntityFHAAddendumSponsor.Roles.Role[0].label));
            }

            var individualInterviewer = this.CreateParty_Preparer(this.loanData.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject), PartyRoleBase.Interviewer, EntityType.Individual);
            var legalEntityInterviewer = this.CreateParty_Preparer(this.loanData.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject), PartyRoleBase.InterviewerEmployer, EntityType.LegalEntity);
            parties.Party.Add(individualInterviewer);
            parties.Party.Add(legalEntityInterviewer);

            if (individualInterviewer != null && legalEntityInterviewer != null)
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualInterviewer.Roles.Role[0].label, legalEntityInterviewer.Roles.Role[0].label));

                if (individualInterviewer.Roles.Role.Count > 1 && legalEntityInterviewer.Roles.Role.Count > 1)
                {
                    this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualInterviewer.Roles.Role[1].label, legalEntityInterviewer.Roles.Role[1].label));
                }
            }

            parties.Party.Add(this.CreateParty_WindInsurance());
            parties.Party.Add(this.CreateParty_WarehouseLender());
            parties.Party.Add(this.CreateParty(this.loanData.sCalcInvestorNm));

            // Add investor parties from Investor Info page.
            InvestorRolodexEntry loanInvestorInfo = this.loanData.sInvestorInfo;

            parties.Party.Add(this.CreatePartyFromInvestorInfo(
                PartyRoleBase.LossPayee,
                loanInvestorInfo.LossPayeeContactName,
                loanInvestorInfo.LossPayeePhone,
                loanInvestorInfo.LossPayeeAddress,
                loanInvestorInfo.LossPayeeCity,
                loanInvestorInfo.LossPayeeState,
                loanInvestorInfo.LossPayeeZip));

            // Create a variable because the ROLE xlink label is used in the next PARTY to create a relationship.
            PARTY mainInvestorParty = this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Investor,
                loanInvestorInfo.CompanyName,
                loanInvestorInfo.MainPhone,
                loanInvestorInfo.MainAddress,
                loanInvestorInfo.MainCity,
                loanInvestorInfo.MainState,
                loanInvestorInfo.MainZip);
            parties.Party.Add(mainInvestorParty);

            parties.Party.Add(this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Investor,
                loanInvestorInfo.MainContactName,
                loanInvestorInfo.MainPhone,
                loanInvestorInfo.MainAddress,
                loanInvestorInfo.MainCity,
                loanInvestorInfo.MainState,
                loanInvestorInfo.MainZip,
                isIndividual: true,
                companyLabel: mainInvestorParty == null ? null : mainInvestorParty.Roles.Role[0].label));

            parties.Party.Add(this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Other,
                loanInvestorInfo.NoteShipToContactName,
                loanInvestorInfo.NoteShipToPhone,
                loanInvestorInfo.NoteShipToAddress,
                loanInvestorInfo.NoteShipToCity,
                loanInvestorInfo.NoteShipToState,
                loanInvestorInfo.NoteShipToZip,
                roleTypeOtherDescription: "NoteShipTo"));

            parties.Party.Add(this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Other,
                loanInvestorInfo.PaymentToContactName,
                loanInvestorInfo.PaymentToPhone,
                loanInvestorInfo.PaymentToAddress,
                loanInvestorInfo.PaymentToCity,
                loanInvestorInfo.PaymentToState,
                loanInvestorInfo.PaymentToZip,
                roleTypeOtherDescription: "MailPmt"));

            InvestorRolodexEntry loanSubservicerInfo = this.loanData.sSubservicerInfo;
            PARTY mainSubservicerParty = this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Servicer,
                loanSubservicerInfo.CompanyName,
                loanSubservicerInfo.MainPhone,
                loanSubservicerInfo.MainAddress,
                loanSubservicerInfo.MainCity,
                loanSubservicerInfo.MainState,
                loanSubservicerInfo.MainZip,
                mersOrganizationId: loanSubservicerInfo.MERSOrganizationId);
            if (mainSubservicerParty != null)
            {
                mainSubservicerParty.Roles.Role[0].Servicer = this.CreateServicer(ServicerBase.Subservicer);
                parties.Party.Add(mainSubservicerParty);
            }

            PARTY individualSubservicerParty = this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Servicer,
                loanSubservicerInfo.MainContactName,
                loanSubservicerInfo.MainPhone,
                loanSubservicerInfo.MainAddress,
                loanSubservicerInfo.MainCity,
                loanSubservicerInfo.MainState,
                loanSubservicerInfo.MainZip,
                isIndividual: true,
                companyLabel: mainSubservicerParty == null ? null : mainSubservicerParty.Roles.Role[0].label);
            if (individualSubservicerParty != null)
            {
                individualSubservicerParty.Roles.Role[0].Servicer = this.CreateServicer(ServicerBase.Subservicer);
                parties.Party.Add(individualSubservicerParty);
            }

            return parties;
        }

        /// <summary>
        /// Creates a party representing a party from the Investor Info page.
        /// Creates a <see cref="ROLE"/>, <see cref="LEGAL_ENTITY"/>, and <see cref="ADDRESS"/> for the party.
        /// </summary>
        /// <param name="roleType">The role type of the party to create.</param>
        /// <param name="contactName">The name of the company, entered as "Contact Name" on the investor info page.</param>
        /// <param name="phoneNumber">The phone number for the company.</param>
        /// <param name="address">The street address for the company.</param>
        /// <param name="city">The city of the company's address.</param>
        /// <param name="state">The state code of the company's address.</param>
        /// <param name="zip">The zip code of the company's address.</param>
        /// <param name="roleTypeOtherDescription">The description for the role type if the role type is "Other".</param>
        /// <param name="isIndividual">Whether this party should contain an INDIVIDUAL container. Alternative is LEGAL_ENTITY container.</param>
        /// <param name="companyLabel">The label for the employing company for referencing with a relationship.</param>
        /// <param name="mersOrganizationId">The MERSOrganizationIdentifier to be populated.</param>
        /// <remarks>Added specifically for better IDS integration (case 242202).</remarks>
        /// <returns>A <see cref="PARTY"/> container with information about the investor entity for the loan file.</returns>
        private PARTY CreatePartyFromInvestorInfo(
            PartyRoleBase roleType,
            string contactName,
            string phoneNumber,
            string address,
            string city,
            string state,
            string zip,
            string roleTypeOtherDescription = null,
            bool isIndividual = false,
            string companyLabel = null,
            string mersOrganizationId = null)
        {
            if (string.IsNullOrEmpty(contactName))
            {
                return null;
            }

            ROLES roles = this.CreateRoles(roleType, roleTypeOtherDescription);
            if (isIndividual)
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", roles.Role[0].label, companyLabel));
            }

            return new PARTY
            {
                Roles = roles,
                Individual = isIndividual ? this.CreateIndividual(contactName) : null,
                LegalEntity = isIndividual ? null : this.CreateLegalEntity(contactName, phoneNumber, null, null, null, ContactPointRoleBase.Blank, mersOrganizationId),
                Addresses = CreateAddresses(
                    address,
                    city,
                    state,
                    zip,
                    AddressBase.Blank,
                    countyName: null,
                    countyFIPSCode: null),
                SequenceNumber = this.counters.Parties,
            };
        }

        /// <summary>
        /// Creates a party representing a power of attorney.
        /// </summary>
        /// <param name="name">The attorney name.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_AttorneyInFact(string name, string borrowerLabel)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            var party = new PARTY();
            party.Individual = new INDIVIDUAL();
            party.Individual.Name = CreateName(name, isLegalEntity: false);
            party.SequenceNumber = this.counters.Parties;

            var role = new ROLE();
            role.RoleDetail = new ROLE_DETAIL();
            role.RoleDetail.PartyRoleType = ToPartyRoleEnum(PartyRoleBase.AttorneyInFact);
            role.label = "Attorney" + borrowerLabel; //// Each borrower can have only one power of attorney, so the borrower MISMO label is used to create a unique id per attorney.
            role.SequenceNumber = 1;

            party.Roles = new ROLES();
            party.Roles.Role.Add(role);

            if (!string.IsNullOrEmpty(borrowerLabel))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsAttorneyInFactFor, "ROLE", "ROLE", role.label, borrowerLabel));
                this.allRelationships.Add(this.CreateRelationship_ToLoan("ROLE", role.label, ArcroleVerbPhrase.IsAssociatedWith));
            }

            return party;
        }

        /// <summary>
        /// Creates a container representing a preparer.
        /// </summary>
        /// <param name="preparer">An object with data on the preparer.</param>
        /// <param name="roleType">The role type for the party object.</param>
        /// <param name="entityType">Indicates whether this party represents a person or legal entity.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_Preparer(IPreparerFields preparer, PartyRoleBase roleType, EntityType entityType)
        {
            if (preparer.IsEmpty)
            {
                return null;
            }

            var party = new PARTY();

            if (entityType == EntityType.Individual)
            {
                party.Individual = new INDIVIDUAL();
                party.Individual.Name = CreateName(preparer.PreparerName, isLegalEntity: false);

                party.Individual.ContactPoints = new CONTACT_POINTS();
                party.Individual.ContactPoints.ContactPoint.Add(CreateContactPoint_Phone(preparer.Phone, ContactPointRoleBase.Work, 1));
                party.Individual.ContactPoints.ContactPoint.Add(CreateContactPoint_Fax(preparer.FaxNum, ContactPointRoleBase.Work, party.Individual.ContactPoints.ContactPoint.Count(pt => pt != null) + 1));
                party.Individual.ContactPoints.ContactPoint.Add(CreateContactPoint_Email(preparer.EmailAddr, ContactPointRoleBase.Work, party.Individual.ContactPoints.ContactPoint.Count(pt => pt != null) + 1));
            }
            else
            {
                party.LegalEntity = new LEGAL_ENTITY();
                party.LegalEntity.LegalEntityDetail = new LEGAL_ENTITY_DETAIL();
                party.LegalEntity.LegalEntityDetail.FullName = ToMismoString(preparer.CompanyName);
            }
            
            party.Addresses = new ADDRESSES();
            party.Addresses.Address.Add(
                CreateAddress(
                preparer.StreetAddr, 
                preparer.City, 
                preparer.State, 
                preparer.Zip, 
                string.Empty,
                string.Empty,
                this.statesAndTerritories.GetStateName(preparer.State),
                AddressBase.Blank, 
                1));

            party.SequenceNumber = this.counters.Parties;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.RoleDetail = new ROLE_DETAIL();
            role.RoleDetail.PartyRoleType = ToPartyRoleEnum(roleType);

            if (entityType == EntityType.Individual)
            {
                role.Licenses = this.CreateLicenses(preparer.LicenseNumOfAgent, preparer.LoanOriginatorIdentifier);
            }
            else
            {
                role.Licenses = this.CreateLicenses(preparer.LicenseNumOfCompany, preparer.CompanyLoanOriginatorIdentifier);
            }

            party.Roles = new ROLES();
            party.Roles.Role.Add(role);

            if (roleType == PartyRoleBase.Interviewer || roleType == PartyRoleBase.InterviewerEmployer)
            {
                party.Roles.Role.Add(this.CreateRole_LoanOriginator(preparer, entityType, party.Roles.Role.Count(r => r != null) + 1));
            }

            role.label = this.GeneratePartyLabel(preparer.AgentRoleT, entityType);

            return party;
        }

        /// <summary>
        /// Creates a PARTY container representing a title-only borrower.
        /// </summary>
        /// <param name="borrower">The borrower.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_TitleOnly(TitleBorrower borrower)
        {
            var party = new PARTY();
            string baseLabel = "TitleOnlyBorrower" + this.counters.TitleOnlyBorrowers;
            int borrowerRoleCounter = 0;

            party.Roles = new ROLES();
            party.Roles.Role.Add(this.CreateRole_TitleOnly(borrower, baseLabel, ++borrowerRoleCounter));

            foreach (var trustee in this.loanData.sTrustCollection.Trustees)
            {
                if (string.Equals(borrower.FullName, trustee.Name, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(borrower.FirstNm + " " + borrower.LastNm, trustee.Name, StringComparison.OrdinalIgnoreCase))
                {
                    string label = baseLabel + "_Trustee";
                    party.Roles.Role.Add(this.CreateRole_Trustee(trustee, label, ++borrowerRoleCounter));
                }
            }
            
            foreach (var settlor in this.loanData.sTrustCollection.Trustors)
            {
                if (string.Equals(borrower.FullName, settlor.Name, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(borrower.FirstNm + " " + borrower.LastNm, settlor.Name, StringComparison.OrdinalIgnoreCase))
                {
                    string label = baseLabel + "_Settlor";
                    party.Roles.Role.Add(this.CreateRole_Settlor(label, ++borrowerRoleCounter));
                }
            }

            party.Addresses = new ADDRESSES();
            party.Addresses.Address.Add(
                CreateAddress(
                borrower.Address, 
                borrower.City, 
                borrower.State, 
                borrower.Zip, 
                string.Empty, 
                string.Empty, 
                this.statesAndTerritories.GetStateName(borrower.State),
                AddressBase.Mailing, 
                1));

            party.Individual = new INDIVIDUAL();
            party.Individual.Name = CreateName(borrower.FirstNm, borrower.MidNm, borrower.LastNm, string.Empty, borrower.FullName);
            party.Individual.ContactPoints = this.CreateContactPoints(borrower.HPhone, string.Empty, borrower.Email, ContactPointRoleBase.Blank);
            party.Individual.Aliases = new ALIASES();

            party.TaxpayerIdentifiers = CreateTaxpayerIdentifiers(borrower.SSNForExport, TaxpayerIdentifierBase.SocialSecurityNumber);

            party.SequenceNumber = this.counters.Parties;

            foreach (string alias in borrower.Aliases)
            {
                if (string.IsNullOrEmpty(alias))
                {
                    continue;
                }
                else
                {
                    party.Individual.Aliases.Alias.Add(this.CreateAlias(alias, party.Individual.Aliases.Alias.Count(a => a != null) + 1));
                }
            }

            return party;
        }

        /// <summary>
        /// Creates a container representing a trust.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_Trust()
        {
            if (string.IsNullOrEmpty(this.loanData.sTrustName))
            {
                return null;
            }

            var party = new PARTY();
            party.LegalEntity = new LEGAL_ENTITY();
            party.LegalEntity.LegalEntityDetail = new LEGAL_ENTITY_DETAIL();
            party.LegalEntity.LegalEntityDetail.FullName = ToMismoString(this.loanData.sTrustName);
            party.SequenceNumber = this.counters.Parties;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.Trust = new TRUST();
            role.Trust.TrustEstablishedDate = ToMismoDate(this.loanData.sTrustAgreementD_rep);
            role.Trust.TrustEstablishedStateName = ToMismoString(this.loanData.sTrustState);
            role.Trust.TrustClassificationType = ToTrustClassificationEnum(TrustClassificationBase.LivingTrust);

            var roleDetail = new ROLE_DETAIL();
            roleDetail.PartyRoleType = ToPartyRoleEnum(PartyRoleBase.Trust);
            role.RoleDetail = roleDetail;

            role.label = this.GeneratePartyLabel("Trust", EntityType.LegalEntity);

            party.Roles = new ROLES();
            party.Roles.Role.Add(role);

            return party;
        }

        /// <summary>
        /// Creates a container representing a settlement service provider.
        /// </summary>
        /// <param name="provider">The service provider.</param>
        /// <param name="fees">Fees associated with the service provider.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_SSP(SettlementServiceProvider provider, List<Tuple<string, decimal>> fees)
        {
            var party = new PARTY();
            party.SequenceNumber = this.counters.Parties;
            party.Roles = this.CreateRoles_SSP(provider, fees);

            var agent = this.loanData.GetAgentFields(provider.AgentId);

            // If the agent comes back as a new record, that indicates that the AgentId does not
            // correspond to an agent for this loan, and we must default to using the SSP data.
            if (agent.IsNewRecord)
            {
                party.Addresses = CreateAddresses(provider);
                party.LegalEntity = this.CreateLegalEntity(
                    provider.CompanyName,
                    provider.Phone,
                    string.Empty,
                    string.Empty,
                    CreateName(provider.ContactName, isLegalEntity: false),
                    ContactPointRoleBase.Work);
            }
            else
            {
                party.Addresses = CreateAddresses(agent);
                party.LegalEntity = this.CreateLegalEntity(
                    agent.CompanyName,
                    agent.Phone,
                    string.Empty,
                    string.Empty,
                    CreateName(agent.AgentName, isLegalEntity: false),
                    ContactPointRoleBase.Work);
            }

            return party;
        }

        /// <summary>
        /// Creates a container representing a trust settlor.
        /// </summary>
        /// <param name="settlor">The settlor information.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_Settlor(TrustCollection.Trustor settlor)
        {
            var party = new PARTY();
            party.Individual = new INDIVIDUAL();
            party.Individual.Name = CreateName(settlor.Name, isLegalEntity: false);
            party.SequenceNumber = this.counters.Parties;

            party.Roles = new ROLES();
            string label = "Settlor" + this.counters.PartyLabels;
            party.Roles.Role.Add(this.CreateRole_Settlor(label, 1));

            return party;
        }

        /// <summary>
        /// Creates a container representing a trustee.
        /// </summary>
        /// <param name="trustee">The trustee information.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_Trustee(TrustCollection.Trustee trustee)
        {
            var party = new PARTY();
            party.Individual = this.CreateIndividual(trustee);

            party.Addresses = new ADDRESSES();
            party.Addresses.Address.Add(
                CreateAddress(
                trustee.Address.StreetAddress, 
                trustee.Address.City, 
                trustee.Address.State, 
                trustee.Address.PostalCode, 
                string.Empty, 
                string.Empty, 
                this.statesAndTerritories.GetStateName(trustee.Address.State),
                AddressBase.Blank, 
                1));

            party.SequenceNumber = this.counters.Parties;

            party.Roles = new ROLES();
            string label = "Trustee" + this.counters.PartyLabels;
            party.Roles.Role.Add(this.CreateRole_Trustee(trustee, label, 1));

            return party;
        }

        /// <summary>
        /// Creates a container representing a warehouse lender.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_WarehouseLender()
        {
            if (this.loanData.sWarehouseLenderRolodexId < 0)
            {
                return null;
            }

            var warehouseLender = WarehouseLenderRolodexEntry.Get(this.loanData.sBrokerId, this.loanData.sWarehouseLenderRolodexId);
            var party = new PARTY();
            party.LegalEntity = this.CreateLegalEntity(warehouseLender.WarehouseLenderName, string.Empty, string.Empty, string.Empty, null, ContactPointRoleBase.Work);
            party.Roles = this.CreateRoles(PartyRoleBase.WarehouseLender);
            party.SequenceNumber = this.counters.Parties;

            return party;
        }

        /// <summary>
        /// Creates a container representing a wind insurance company.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_WindInsurance()
        {
            if (string.IsNullOrEmpty(this.loanData.sWindInsCompanyNm))
            {
                return null;
            }

            PARTY party = new PARTY();
            party.LegalEntity = this.CreateLegalEntity(this.loanData.sWindInsCompanyNm, string.Empty, string.Empty, string.Empty, null, ContactPointRoleBase.Blank);
            party.SequenceNumber = this.counters.Parties;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.ClosingAgent, string.Empty);
            role.ClosingAgent = this.CreateClosingAgent(ClosingAgentBase.Other, "WindstormInsuranceAgent");
            role.label = this.GeneratePartyLabel("WindInsuranceCompany", EntityType.LegalEntity);

            party.Roles = new ROLES();
            party.Roles.Role.Add(role);
            return party;
        }

        /// <summary>
        /// Creates a list of parties representing the agents listed on either the Loan Estimate or Closing Disclosure.
        /// </summary>
        /// <returns>A list of PARTY containers.</returns>
        private List<PARTY> CreatePartyList_Disclosure()
        {
            var disclosureParties = new List<PARTY>();

            if (this.isClosingPackage)
            {
                disclosureParties.AddRange(this.CreatePartyList_ClosingDisclosure_Lender());
                disclosureParties.AddRange(this.CreatePartyList_ClosingDisclosure_MortgageBroker());
                disclosureParties.AddRange(this.CreatePartyList_ClosingDisclosure_RealEstateBrokerBuyer());
                disclosureParties.AddRange(this.CreatePartyList_ClosingDisclosure_RealEstateBrokerSeller());
                disclosureParties.AddRange(this.CreatePartyList_ClosingDisclosure_SettlementAgent());
            }
            else
            {
                var lenderParty = this.CreateParty_LoanEstimate_Lender();
                var loanOfficerParty = this.CreateParty_LoanEstimate_LoanOfficer();
                var brokerParty = this.CreateParty_LoanEstimate_MortgageBroker();

                if (loanOfficerParty != null)
                {
                    string employeeLabel = loanOfficerParty.Roles.Role[0].label;
                    string employerLabel = string.Empty;
                    
                    if (this.loanData.sGfeIsTPOTransaction && brokerParty != null)
                    {
                        employerLabel = brokerParty.Roles.Role[0].label;
                    }
                    else if (lenderParty != null)
                    {
                        employerLabel = lenderParty.Roles.Role[0].label;
                    }

                    if (!string.IsNullOrEmpty(employeeLabel) && !string.IsNullOrEmpty(employerLabel))
                    {
                        this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", employeeLabel, employerLabel));
                    }
                }

                disclosureParties.Add(lenderParty);
                disclosureParties.Add(loanOfficerParty);
                disclosureParties.Add(brokerParty);
            }

            // Seller info should always be added, but they will only be listed on the disclosure for CD exports.
            disclosureParties.AddRange(this.CreatePartyList_ClosingDisclosure_Sellers());

            return disclosureParties;
        }

        /// <summary>
        /// Creates a list of parties representing the Lender on the Closing Disclosure.
        /// </summary>
        /// <returns>A list of PARTY containers.</returns>
        private List<PARTY> CreatePartyList_ClosingDisclosure_Lender()
        {
            var lenderParties = new List<PARTY>();
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.loanData.sTRIDLenderName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = this.counters.Parties;

                legalEntityParty.LegalEntity = CreateLegalEntity(this.loanData.sTRIDLenderName);

                legalEntityParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDLenderAddr,
                    this.loanData.sTRIDLenderCity,
                    this.loanData.sTRIDLenderState,
                    this.loanData.sTRIDLenderZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.Lender,
                    EntityType.LegalEntity,
                    this.loanData.sTRIDLenderLicenseIDNum,
                    this.loanData.sTRIDLenderNMLSNum);
                legalEntityLabel = legalEntityParty.Roles.Role[0].label;

                lenderParties.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.loanData.sTRIDLenderContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = this.counters.Parties;

                individualParty.Individual = this.CreateIndividual(
                    this.loanData.sTRIDLenderContact,
                    this.loanData.sTRIDLenderContactPhoneNum,
                    string.Empty,
                    this.loanData.sTRIDLenderContactEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDLenderAddr,
                    this.loanData.sTRIDLenderCity,
                    this.loanData.sTRIDLenderState,
                    this.loanData.sTRIDLenderZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.Lender,
                    EntityType.Individual,
                    this.loanData.sTRIDLenderContactLicenseIDNum,
                    this.loanData.sTRIDLenderContactNMLSNum);
                individualLabel = individualParty.Roles.Role[0].label;

                lenderParties.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel));
            }

            return lenderParties;
        }

        /// <summary>
        /// Creates a list of parties representing the Mortgage Broker on the Closing Disclosure.
        /// </summary>
        /// <returns>A list of PARTY containers.</returns>
        private List<PARTY> CreatePartyList_ClosingDisclosure_MortgageBroker()
        {
            var mortgageBrokerParties = new List<PARTY>();
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.loanData.sTRIDMortgageBrokerName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = this.counters.Parties;

                legalEntityParty.LegalEntity = CreateLegalEntity(this.loanData.sTRIDMortgageBrokerName);

                legalEntityParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDMortgageBrokerAddr,
                    this.loanData.sTRIDMortgageBrokerCity,
                    this.loanData.sTRIDMortgageBrokerState,
                    this.loanData.sTRIDMortgageBrokerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.MortgageBroker,
                    EntityType.LegalEntity,
                    this.loanData.sTRIDMortgageBrokerLicenseIDNum,
                    this.loanData.sTRIDMortgageBrokerNMLSNum);
                legalEntityLabel = legalEntityParty.Roles.Role[0].label;

                mortgageBrokerParties.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.loanData.sTRIDMortgageBrokerContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = this.counters.Parties;

                individualParty.Individual = this.CreateIndividual(
                    this.loanData.sTRIDMortgageBrokerContact,
                    this.loanData.sTRIDMortgageBrokerPhoneNum,
                    string.Empty,
                    this.loanData.sTRIDMortgageBrokerEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDMortgageBrokerAddr,
                    this.loanData.sTRIDMortgageBrokerCity,
                    this.loanData.sTRIDMortgageBrokerState,
                    this.loanData.sTRIDMortgageBrokerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.MortgageBroker,
                    EntityType.Individual,
                    this.loanData.sTRIDMortgageBrokerContactLicenseIDNum,
                    this.loanData.sTRIDMortgageBrokerContactNMLSNum);
                individualLabel = individualParty.Roles.Role[0].label;

                mortgageBrokerParties.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel));
            }

            return mortgageBrokerParties;
        }

        /// <summary>
        /// Creates a list of parties representing the Settlement Agents on the Closing Disclosure.
        /// </summary>
        /// <returns>A list of PARTY containers.</returns>
        private List<PARTY> CreatePartyList_ClosingDisclosure_SettlementAgent()
        {
            var settlementAgentParties = new List<PARTY>();
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.loanData.sTRIDSettlementAgentName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = this.counters.Parties;

                legalEntityParty.LegalEntity = CreateLegalEntity(this.loanData.sTRIDSettlementAgentName);

                legalEntityParty.Addresses = this.CreateAddresses(
                    this.loanData.sTRIDSettlementAgentAddr,
                    this.loanData.sTRIDSettlementAgentCity,
                    this.loanData.sTRIDSettlementAgentState,
                    this.loanData.sTRIDSettlementAgentZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.ClosingAgent,
                    EntityType.LegalEntity,
                    this.loanData.sTRIDSettlementAgentLicenseIDNum,
                    this.loanData.sTRIDSettlementAgentNMLSNum);
                legalEntityLabel = legalEntityParty.Roles.Role[0].label;

                settlementAgentParties.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.loanData.sTRIDSettlementAgentContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = this.counters.Parties;

                individualParty.Individual = this.CreateIndividual(
                    this.loanData.sTRIDSettlementAgentContact,
                    this.loanData.sTRIDSettlementAgentPhoneNum,
                    string.Empty,
                    this.loanData.sTRIDSettlementAgentEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDSettlementAgentAddr,
                    this.loanData.sTRIDSettlementAgentCity,
                    this.loanData.sTRIDSettlementAgentState,
                    this.loanData.sTRIDSettlementAgentZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.ClosingAgent,
                    EntityType.Individual,
                    this.loanData.sTRIDSettlementAgentContactLicenseIDNum,
                    this.loanData.sTRIDSettlementAgentContactNMLSNum);
                individualLabel = individualParty.Roles.Role[0].label;

                settlementAgentParties.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel));
            }

            return settlementAgentParties;
        }

        /// <summary>
        /// Creates a list of parties representing the Real Estate Selling Agents on the Closing Disclosure.
        /// </summary>
        /// <returns>A list of PARTY containers.</returns>
        private List<PARTY> CreatePartyList_ClosingDisclosure_RealEstateBrokerBuyer()
        {
            var realEstateBrokerBuyerParties = new List<PARTY>();
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.loanData.sTRIDRealEstateBrokerBuyerName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = this.counters.Parties;

                legalEntityParty.LegalEntity = CreateLegalEntity(this.loanData.sTRIDRealEstateBrokerBuyerName);

                legalEntityParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDRealEstateBrokerBuyerAddr,
                    this.loanData.sTRIDRealEstateBrokerBuyerCity,
                    this.loanData.sTRIDRealEstateBrokerBuyerState,
                    this.loanData.sTRIDRealEstateBrokerBuyerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.RealEstateAgent,
                    EntityType.LegalEntity,
                    this.loanData.sTRIDRealEstateBrokerBuyerLicenseIDNum,
                    this.loanData.sTRIDRealEstateBrokerBuyerNMLSNum,
                    RealEstateAgentBase.Selling);
                legalEntityLabel = legalEntityParty.Roles.Role[0].label;

                realEstateBrokerBuyerParties.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.loanData.sTRIDRealEstateBrokerBuyerContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = this.counters.Parties;

                individualParty.Individual = this.CreateIndividual(
                    this.loanData.sTRIDRealEstateBrokerBuyerContact,
                    this.loanData.sTRIDRealEstateBrokerBuyerPhoneNum,
                    string.Empty,
                    this.loanData.sTRIDRealEstateBrokerBuyerEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDRealEstateBrokerBuyerAddr,
                    this.loanData.sTRIDRealEstateBrokerBuyerCity,
                    this.loanData.sTRIDRealEstateBrokerBuyerState,
                    this.loanData.sTRIDRealEstateBrokerBuyerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.RealEstateAgent,
                    EntityType.Individual,
                    this.loanData.sTRIDRealEstateBrokerBuyerContactLicenseIDNum,
                    this.loanData.sTRIDRealEstateBrokerBuyerContactNMLSNum,
                    RealEstateAgentBase.Selling);
                individualLabel = individualParty.Roles.Role[0].label;

                realEstateBrokerBuyerParties.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel));
            }

            return realEstateBrokerBuyerParties;
        }

        /// <summary>
        /// Creates a list of parties representing the Real Estate Listing Agents on the Closing Disclosure.
        /// </summary>
        /// <returns>A list of PARTY containers.</returns>
        private List<PARTY> CreatePartyList_ClosingDisclosure_RealEstateBrokerSeller()
        {
            var realEstateBrokerSellerParties = new List<PARTY>();
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.loanData.sTRIDRealEstateBrokerSellerName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = this.counters.Parties;

                legalEntityParty.LegalEntity = CreateLegalEntity(this.loanData.sTRIDRealEstateBrokerSellerName);

                legalEntityParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDRealEstateBrokerSellerAddr,
                    this.loanData.sTRIDRealEstateBrokerSellerCity,
                    this.loanData.sTRIDRealEstateBrokerSellerState,
                    this.loanData.sTRIDRealEstateBrokerSellerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.RealEstateAgent,
                    EntityType.LegalEntity,
                    this.loanData.sTRIDRealEstateBrokerSellerLicenseIDNum,
                    this.loanData.sTRIDRealEstateBrokerSellerNMLSNum,
                    RealEstateAgentBase.Listing);
                legalEntityLabel = legalEntityParty.Roles.Role[0].label;

                realEstateBrokerSellerParties.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.loanData.sTRIDRealEstateBrokerSellerContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = this.counters.Parties;

                individualParty.Individual = this.CreateIndividual(
                    this.loanData.sTRIDRealEstateBrokerSellerContact,
                    this.loanData.sTRIDRealEstateBrokerSellerPhoneNum,
                    string.Empty,
                    this.loanData.sTRIDRealEstateBrokerSellerEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.loanData.sTRIDRealEstateBrokerSellerAddr,
                    this.loanData.sTRIDRealEstateBrokerSellerCity,
                    this.loanData.sTRIDRealEstateBrokerSellerState,
                    this.loanData.sTRIDRealEstateBrokerSellerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.RealEstateAgent,
                    EntityType.Individual,
                    this.loanData.sTRIDRealEstateBrokerSellerContactLicenseIDNum,
                    this.loanData.sTRIDRealEstateBrokerSellerContactNMLSNum,
                    RealEstateAgentBase.Listing);
                individualLabel = individualParty.Roles.Role[0].label;

                realEstateBrokerSellerParties.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.allRelationships.Add(this.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel));
            }

            return realEstateBrokerSellerParties;
        }

        /// <summary>
        /// Creates a list of parties representing the Sellers on the Closing Disclosure.
        /// </summary>
        /// <returns>A list of PARTY containers.</returns>
        private List<PARTY> CreatePartyList_ClosingDisclosure_Sellers()
        {
            var sellerParties = new List<PARTY>();

            foreach (var seller in this.loanData.sSellerCollection.ListOfSellers)
            {
                var sellerParty = new PARTY();
                sellerParty.SequenceNumber = this.counters.Parties;

                if (seller.Type == E_SellerType.Individual)
                {
                    sellerParty.Individual = this.CreateIndividual(seller.Name);
                }
                else
                {
                    sellerParty.LegalEntity = this.CreateLegalEntity(seller.Name, seller.EntityType);
                }

                sellerParty.Addresses = CreateAddresses(
                    seller.Address.StreetAddress,
                    seller.Address.City,
                    seller.Address.State,
                    seller.Address.PostalCode,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                sellerParty.Roles = this.CreateRoles_Seller(
                    seller.Type == E_SellerType.Individual ? EntityType.Individual : EntityType.LegalEntity,
                    this.isClosingPackage);

                sellerParties.Add(sellerParty);
            }

            return sellerParties;
        }

        /// <summary>
        /// Creates a party representing the Lender on the Loan Estimate.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_LoanEstimate_Lender()
        {
            if (string.IsNullOrEmpty(this.loanData.sTRIDLenderName))
            {
                return null;
            }

            var party = new PARTY();
            party.SequenceNumber = this.counters.Parties;

            party.LegalEntity = this.CreateLegalEntity(
                this.loanData.sTRIDLenderName,
                this.loanData.sTRIDLenderPhoneNum, 
                string.Empty, 
                this.loanData.sTRIDLenderContactEmail, 
                null, 
                ContactPointRoleBase.Work);

            party.Addresses = CreateAddresses(
                this.loanData.sTRIDLenderAddr,
                this.loanData.sTRIDLenderCity,
                this.loanData.sTRIDLenderState,
                this.loanData.sTRIDLenderZip,
                AddressBase.Mailing,
                string.Empty,
                string.Empty);

            party.Roles = this.CreateRoles_DisclosureContact(
                PartyRoleBase.Lender,
                EntityType.LegalEntity,
                this.loanData.sTRIDLenderLicenseIDNum,
                this.loanData.sTRIDLenderNMLSNum);

            return party;
        }

        /// <summary>
        /// Creates a party representing the Loan Officer on the Loan Estimate.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_LoanEstimate_LoanOfficer()
        {
            if (string.IsNullOrEmpty(this.loanData.sTRIDLoanOfficerName))
            {
                return null;
            }

            var party = new PARTY();
            party.SequenceNumber = this.counters.Parties;

            party.Individual = this.CreateIndividual(
                this.loanData.sTRIDLoanOfficerName,
                this.loanData.sTRIDLoanOfficerPhoneNum,
                string.Empty,
                this.loanData.sTRIDLoanOfficerEmail,
                ContactPointRoleBase.Work);

            party.Roles = this.CreateRoles_DisclosureContact(
                PartyRoleBase.LoanOfficer,
                EntityType.Individual,
                this.loanData.sTRIDLoanOfficerLicenseIDNum,
                this.loanData.sTRIDLoanOfficerNMLSNum);

            var agentId = this.loanData.sTRIDLoanOfficerAgentId;
            if (agentId != Guid.Empty)
            {
                var loanOfficerAgent = this.loanData.GetAgentFields(agentId);
                if (loanOfficerAgent != null)
                {
                    party.PartyExtension = this.CreatePartyExtension(loanOfficerAgent, EntityType.Individual);
                }
            }

            return party;
        }

        /// <summary>
        /// Creates a party representing the Mortgage Broker on the Loan Estimate.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_LoanEstimate_MortgageBroker()
        {
            if (string.IsNullOrEmpty(this.loanData.sTRIDMortgageBrokerName))
            {
                return null;
            }

            var party = new PARTY();
            party.SequenceNumber = this.counters.Parties;

            party.LegalEntity = CreateLegalEntity(this.loanData.sTRIDMortgageBrokerName);

            party.Roles = this.CreateRoles_DisclosureContact(
                PartyRoleBase.MortgageBroker,
                EntityType.LegalEntity,
                this.loanData.sTRIDMortgageBrokerLicenseIDNum,
                this.loanData.sTRIDMortgageBrokerNMLSNum);

            return party;
        }

        /// <summary>
        /// Creates a PARTY container holding agent information if such an agent exists on the loan file.
        /// </summary>
        /// <param name="agent">Agent role from which party information is populated.</param>
        /// <param name="entityType">Indicates whether the agent is an individual or legal entity.</param>
        /// <returns>PARTY container with agent information if specified agent type exists on loan file, otherwise null.</returns>
        private PARTY CreateParty(CAgentFields agent, EntityType entityType)
        {
            // We allow AgentName to be blank if we're creating an Individual of role Lender because we can use the field sEmployeeLoanRepName.
            if (!agent.IsValid ||
                (string.IsNullOrEmpty(agent.AgentName) && entityType == EntityType.Individual && agent.AgentRoleT != E_AgentRoleT.Lender) ||
                (string.IsNullOrEmpty(agent.CompanyName) && entityType == EntityType.LegalEntity))
            {
                return null;
            }

            var party = new PARTY();
            party.Addresses = CreateAddresses(agent);
            party.Roles = this.CreateRoles(agent, entityType);
            party.SequenceNumber = this.counters.Parties;

            if (entityType == EntityType.Individual)
            {
                party.Individual = this.CreateIndividual(agent, ContactPointRoleBase.Work);
            }
            else
            {
                party.LegalEntity = CreateLegalEntity(agent, agent.CompanyName, ContactPointRoleBase.Work);
                party.TaxpayerIdentifiers = CreateTaxpayerIdentifiers(agent.TaxId, TaxpayerIdentifierBase.EmployerIdentificationNumber);
            }

            party.PartyExtension = this.CreatePartyExtension(agent, entityType);

            return party;
        }

        /// <summary>
        /// Creates a PARTY extension.
        /// </summary>
        /// <param name="agent">An agent record.</param>
        /// <param name="entityType">Indicates whether the agent's individual or legal entity data is being exported.</param>
        /// <returns>A PARTY extension.</returns>
        private PARTY_EXTENSION CreatePartyExtension(CAgentFields agent, EntityType entityType)
        {
            var extension = new PARTY_EXTENSION();
            extension.Other = this.CreateLqbPartyExtension(agent, entityType);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary PARTY extension.
        /// </summary>
        /// <param name="agent">An agent record.</param>
        /// <param name="entityType">Indicates whether the agent's individual or legal entity data is being exported.</param>
        /// <returns>A proprietary PARTY extension.</returns>
        private LQB_PARTY_EXTENSION CreateLqbPartyExtension(CAgentFields agent, EntityType entityType)
        {
            var extension = new LQB_PARTY_EXTENSION();

            if (agent.RecordId != Guid.Empty)
            {
                extension.AgentId = ToMismoIdentifier(agent.RecordId.ToString());
            }

            if (!string.IsNullOrEmpty(agent.CompanyId))
            {
                extension.CompanyIdentifier = ToMismoIdentifier(agent.CompanyId);
            }
            
            if (entityType == EntityType.Individual)
            {
                extension.EmployeeIdentifier = ToMismoIdentifier(agent.EmployeeIDInCompany);
            }

            return extension;
        }

        /// <summary>
        /// Creates a PARTY container holding information on a borrower or co-borrower.
        /// </summary>
        /// <param name="appData">The application holding the data.</param>
        /// <returns>A PARTY container populated with borrower data.</returns>
        private PARTY CreateParty(CAppData appData)
        {
            var party = new PARTY();
            party.Individual = this.CreateIndividual(appData);
            party.Roles = this.CreateRoles(appData);
            party.TaxpayerIdentifiers = CreateTaxpayerIdentifiers(appData.aSsn, TaxpayerIdentifierBase.SocialSecurityNumber);
            party.SequenceNumber = this.counters.Parties;

            party.PartyExtension = this.CreatePartyExtension(appData);
            
            return party;
        }

        /// <summary>
        /// Creates an extension to the PARTY element.
        /// </summary>
        /// <param name="appData">A borrower's application data.</param>
        /// <returns>An extension container.</returns>
        private PARTY_EXTENSION CreatePartyExtension(CAppData appData)
        {
            var extension = new PARTY_EXTENSION();
            extension.Other = this.CreateLqbPartyExtension(appData);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container for the PARTY element.
        /// </summary>
        /// <param name="appData">A borrower's application data.</param>
        /// <returns>A proprietary extension container.</returns>
        private LQB_PARTY_EXTENSION CreateLqbPartyExtension(CAppData appData)
        {
            var extension = new LQB_PARTY_EXTENSION();
            extension.ConsumerId = ToMismoIdentifier(appData.aConsumerId_rep);

            return extension;
        }

        /// <summary>
        /// Creates PARTY container with loan investor.
        /// </summary>
        /// <param name="investorName">Name of loan investor.</param>
        /// <returns>Returns PARTY container with loan investor.</returns>
        private PARTY CreateParty(string investorName)
        {
            PARTY party = new PARTY();
            party.LegalEntity = this.CreateLegalEntity(investorName, string.Empty, string.Empty, string.Empty, null, ContactPointRoleBase.Blank);
            party.SequenceNumber = this.counters.Parties;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.Investor, string.Empty);
            role.label = this.GeneratePartyLabel("Investor", EntityType.LegalEntity);

            party.Roles = new ROLES();
            party.Roles.Role.Add(role);

            return party;
        }

        /// <summary>
        /// Creates PARTY container with lender branch information.
        /// </summary>
        /// <param name="lenderBranch">The lender's branch used to populate PARTY container.</param>
        /// <returns>Returns PARTY container with lender branch information.</returns>
        private PARTY CreateParty(BranchDB lenderBranch)
        {
            PARTY party = new PARTY();
            party.Addresses = this.CreateAddresses(lenderBranch.Address, AddressBase.Current, string.Empty, string.Empty);
            party.LegalEntity = this.CreateLegalEntity(lenderBranch.DisplayNm, lenderBranch.Phone, lenderBranch.Fax, string.Empty, null, ContactPointRoleBase.Work);
            party.SequenceNumber = this.counters.Parties;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.LenderBranch, string.Empty);
            role.label = this.GeneratePartyLabel("LenderBranch", EntityType.LegalEntity);

            party.Roles = new ROLES();
            party.Roles.Role.Add(role);

            party.PartyExtension = this.CreatePartyExtension(lenderBranch);
            
            return party;
        }

        /// <summary>
        /// Creates a PARTY extension.
        /// </summary>
        /// <param name="lenderBranch">The lender branch data.</param>
        /// <returns>A PARTY extension.</returns>
        private PARTY_EXTENSION CreatePartyExtension(BranchDB lenderBranch)
        {
            var extension = new PARTY_EXTENSION();
            extension.Other = this.CreateLqbPartyExtension(lenderBranch);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary PARTY extension.
        /// </summary>
        /// <param name="lenderBranch">The lender branch data.</param>
        /// <returns>A proprietary PARTY extension.</returns>
        private LQB_PARTY_EXTENSION CreateLqbPartyExtension(BranchDB lenderBranch)
        {
            var extension = new LQB_PARTY_EXTENSION();
            extension.BranchCode = ToMismoCode(lenderBranch.BranchCode);

            return extension;
        }

        /// <summary>
        /// Creates a party container with information regarding the lender.
        /// </summary>
        /// <returns>A PARTY container with lender information.</returns>
        private PARTY CreateParty_Lender()
        {
            PARTY lenderParty = new PARTY();
            lenderParty.SequenceNumber = this.counters.Parties;
            lenderParty.Addresses = this.CreateAddresses(this.loanData.BrokerDB.Address, AddressBase.Current, string.Empty, string.Empty);

            NAME contactName = null;
            string contactEmail = string.Empty;
            if (this.user != null)
            {
                contactName = CreateName(this.user.FirstName, this.user.MiddleName, this.user.LastName, this.user.Suffix, this.user.FullName);
                contactEmail = this.user.Email;
            }

            lenderParty.PartyExtension = new PARTY_EXTENSION
            {
                Other = new LQB_PARTY_EXTENSION
                {
                    AgentId = ToMismoIdentifier(this.loanData.sBrokerId.ToString())
                }
            };

            lenderParty.LegalEntity = this.CreateLegalEntity(this.loanData.BrokerDB.Name, this.loanData.BrokerDB.Phone, this.loanData.BrokerDB.Fax, contactEmail, contactName, ContactPointRoleBase.Work);
            lenderParty.Roles = this.CreateRoles_RequestingParty();

            return lenderParty;
        }

        /// <summary>
        /// Creates a party container with information regarding LendingQB.
        /// </summary>
        /// <returns>A PARTY container with LQB information.</returns>
        private PARTY CreateParty_LQB()
        {
            PARTY lqbParty = new PARTY();
            lqbParty.SequenceNumber = this.counters.Parties;

            CommonLib.Address lqbAddress = ConstApp.GetLendingQBAddress();
            lqbParty.Addresses = this.CreateAddresses(lqbAddress, AddressBase.CorporateHeadquarters, ConstApp.lqbCounty, this.statesAndTerritories.GetFIPSCodeForCounty(lqbAddress.State, ConstApp.lqbCounty));

            NAME lqbName = new NAME();
            lqbName.FullName = ToMismoString("LendingQB Support");
            lqbName.FunctionalTitleDescription = ToMismoString("Technical Support");

            lqbParty.LegalEntity = this.CreateLegalEntity("LendingQB", ConstApp.lqbPhoneNumber, string.Empty, ConstStage.LQBIntegrationEmail, lqbName, ContactPointRoleBase.Work);
            lqbParty.Roles = this.CreateRoles_LQB();

            return lqbParty;
        }

        /// <summary>
        /// Creates a container holding party role identifiers for the lender.
        /// </summary>
        /// <returns>A set of party role identifiers.</returns>
        private PARTY_ROLE_IDENTIFIERS CreatePartyRoleIdentifiers_Lender()
        {
            var partyRoleIdentifiers = new PARTY_ROLE_IDENTIFIERS();
            partyRoleIdentifiers.PartyRoleIdentifier.Add(this.CreatePartyRoleIdentifier_Lender());

            return partyRoleIdentifiers;
        }

        /// <summary>
        /// Creates a party role identifier for the lender.
        /// </summary>
        /// <returns>A party role identifier.</returns>
        private PARTY_ROLE_IDENTIFIER CreatePartyRoleIdentifier_Lender()
        {
            var altLender = new DocMagicAlternateLender(this.loanData.BrokerDB.BrokerID, this.loanData.sDocMagicAlternateLenderId);

            var partyRoleIdentifier = new PARTY_ROLE_IDENTIFIER();
            partyRoleIdentifier.PartyRoleIdentifier = ToMismoIdentifier(altLender.DocMagicAlternateLenderInternalId, string.Empty, @"www.docmagic.com/altlenderidentifier");

            return partyRoleIdentifier;
        }

        /// <summary>
        /// Creates a container holding data about a prepayment penalty.
        /// </summary>
        /// <returns>A PREPAYMENT_PENALTY container populated with data.</returns>
        private PREPAYMENT_PENALTY CreatePrepaymentPenalty()
        {
            var prepaymentPenalty = new PREPAYMENT_PENALTY();
            prepaymentPenalty.PrepaymentPenaltyPerChangeRules = this.CreatePrepaymentPenaltyPerChangeRules();
            prepaymentPenalty.PrepaymentPenaltyLifetimeRule = this.CreatePrepaymentPenaltyLifetimeRule();

            return prepaymentPenalty;
        }

        /// <summary>
        /// Creates a container holding data about a lifetime prepayment penalty.
        /// </summary>
        /// <returns>A PREPAYMENT_PENALTY_LIFETIME_RULE object populated with data.</returns>
        private PREPAYMENT_PENALTY_LIFETIME_RULE CreatePrepaymentPenaltyLifetimeRule()
        {
            var prepaymentPenaltyLifetimeRule = new PREPAYMENT_PENALTY_LIFETIME_RULE();
            prepaymentPenaltyLifetimeRule.PrepaymentFinanceChargeRefundableIndicator = ToPrepaymentFinanceChargeRefundableIndicator(this.loanData.sPrepmtRefundT);
            prepaymentPenaltyLifetimeRule.PrepaymentPenaltyExpirationMonthsCount = ToMismoCount(this.loanData.sHardPlusSoftPrepmtPeriodMonths_rep, null);
            prepaymentPenaltyLifetimeRule.PrepaymentPenaltyMaximumLifeOfLoanAmount = ToMismoAmount(this.loanData.sGfeMaxPpmtPenaltyAmt_rep);

            return prepaymentPenaltyLifetimeRule;
        }

        /// <summary>
        /// Creates a container holding data about a prepayment penalty per change.
        /// </summary>
        /// <param name="sequence">The sequence number of the prepayment penalty per change rule among the list of rules.</param>
        /// <returns>A PREPAYMENT_PENALTY_PER_CHANGE_RULE object populated with data.</returns>
        private PREPAYMENT_PENALTY_PER_CHANGE_RULE CreatePrepaymentPenaltyPerChangeRule(int sequence)
        {
            var prepaymentPenaltyPerChangeRule = new PREPAYMENT_PENALTY_PER_CHANGE_RULE();
            prepaymentPenaltyPerChangeRule.SequenceNumber = sequence;
            prepaymentPenaltyPerChangeRule.PrepaymentPenaltyPeriodCount = ToMismoCount(this.loanData.sPrepmtPeriodMonths_rep, null);
            prepaymentPenaltyPerChangeRule.PrepaymentPenaltyPeriodType = ToPrepaymentPenaltyPeriodEnum(PrepaymentPenaltyPeriodBase.Month);
            prepaymentPenaltyPerChangeRule.PrepaymentPenaltyMaximumAmount = ToMismoAmount(this.loanData.sGfeMaxPpmtPenaltyAmt_rep);
            
            return prepaymentPenaltyPerChangeRule;
        }

        /// <summary>
        /// Creates a container holding all instances of PREPAYMENT_PENALTY_PER_CHANGE_RULE.
        /// </summary>
        /// <returns>A PREPAYMENT_PENALTY_PER_CHANGE_RULES container.</returns>
        private PREPAYMENT_PENALTY_PER_CHANGE_RULES CreatePrepaymentPenaltyPerChangeRules()
        {
            var prepaymentPenaltyPerChangeRules = new PREPAYMENT_PENALTY_PER_CHANGE_RULES();
            prepaymentPenaltyPerChangeRules.PrepaymentPenaltyPerChangeRule.Add(this.CreatePrepaymentPenaltyPerChangeRule(1));

            return prepaymentPenaltyPerChangeRules;
        }

        /// <summary>
        /// Creates a container holding data on a project.
        /// </summary>
        /// <returns>A PROJECT container.</returns>
        private PROJECT CreateProject()
        {
            var project = new PROJECT();
            project.ProjectDetail = this.CreateProjectDetail();

            return project;
        }

        /// <summary>
        /// Creates a container holding data on a project.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <returns>A PROJECT container.</returns>
        private PROJECT CreateProject(IRealEstateOwned reo)
        {
            var project = new PROJECT();
            project.ProjectDetail = this.CreateProjectDetail(reo);

            return project;
        }

        /// <summary>
        /// Creates a container holding details on a project.
        /// </summary>
        /// <returns>A PROJECT_DETAIL container.</returns>
        private PROJECT_DETAIL CreateProjectDetail()
        {
            var projectDetail = new PROJECT_DETAIL();
            projectDetail.ProjectName = ToMismoString(this.loanData.sProjNm);
            projectDetail.ProjectAttachmentType = ToProjectAttachmentEnum(this.loanData.sGseSpT);
            projectDetail.ProjectDesignType = ToProjectDesignEnum(this.loanData.sGseSpT);
            projectDetail.ProjectLegalStructureType = ToProjectLegalStructureEnum(this.loanData.sGseSpT);

            return projectDetail;
        }

        /// <summary>
        /// Creates a container holding details on a project.
        /// </summary>
        /// <param name="reo">The real estate owned object.</param>
        /// <returns>A PROJECT_DETAIL container.</returns>
        private PROJECT_DETAIL CreateProjectDetail(IRealEstateOwned reo)
        {
            var projectDetail = new PROJECT_DETAIL();
            projectDetail.ProjectLegalStructureType = ToProjectLegalStructureEnum(reo.TypeT);
            projectDetail.ProjectDesignType = ToProjectDesignEnum(reo.TypeT);

            return projectDetail;
        }

        /// <summary>
        /// Creates a container with amounts and other information pertaining to a projected payment from the related integrated disclosure.
        /// </summary>
        /// <param name="projectedPayment">A <see cref="TridProjectedPayment"/>.</param>
        /// <param name="sequence">The sequence number of the projected payment among the list of payments.</param>
        /// <returns>A PROJECTED_PAYMENT container with amounts and other information pertaining to a projected payment from the related integrated disclosure.</returns>
        private PROJECTED_PAYMENT CreateProjectedPayment(TridProjectedPayment projectedPayment, int sequence)
        {
            var payment = new PROJECTED_PAYMENT();
            payment.PaymentFrequencyType = ToPaymentFrequencyEnum(PaymentFrequencyBase.Monthly);
            payment.ProjectedPaymentCalculationPeriodEndNumber = ToMismoNumeric(projectedPayment.EndYear_rep);
            payment.ProjectedPaymentCalculationPeriodStartNumber = ToMismoNumeric(projectedPayment.StartYear_rep);
            payment.ProjectedPaymentCalculationPeriodTermType = ToProjectedPaymentCalculationPeriodTermEnum(ProjectedPaymentCalculationPeriodTermBase.Yearly);
            payment.ProjectedPaymentEstimatedEscrowPaymentAmount = ToMismoAmount(projectedPayment.Escrow_rep);
            payment.ProjectedPaymentEstimatedTotalMaximumPaymentAmount = ToMismoAmount(projectedPayment.PaymentMax_rep);
            payment.ProjectedPaymentEstimatedTotalMinimumPaymentAmount = ToMismoAmount(projectedPayment.PaymentMin_rep);
            payment.ProjectedPaymentMIPaymentAmount = ToMismoAmount(projectedPayment.MI_rep);
            payment.ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount = ToMismoAmount(projectedPayment.PIMax_rep);
            payment.ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount = ToMismoAmount(projectedPayment.PIMin_rep);
            payment.SequenceNumber = sequence;

            payment.Extension = this.CreateProjectedPaymentExtension(projectedPayment.HasInterestOnly);

            return payment;
        }

        /// <summary>
        /// Creates a PROJECTED_PAYMENT_EXTENSION container with information regarding the projected payment that is not captured by the MISMO standard.
        /// </summary>
        /// <param name="interestOnly">Indicates whether the projected payment includes an interest-only payment.</param>
        /// <returns>A PROJECTED_PAYMENT_EXTENSION container.</returns>
        private PROJECTED_PAYMENT_EXTENSION CreateProjectedPaymentExtension(bool interestOnly)
        {
            var extension = new PROJECTED_PAYMENT_EXTENSION();
            extension.Other = this.CreateLQBProjectedPaymentExtension(interestOnly);

            return extension;
        }

        /// <summary>
        /// Creates an LQB_PROJECTED_PAYMENT_EXTENSION container with proprietary data-points.
        /// </summary>
        /// <param name="interestOnly">Indicates whether the projected payment includes an interest-only payment.</param>
        /// <returns>An LQB_PROJECTED_PAYMENT_EXTENSION container.</returns>
        private LQB_PROJECTED_PAYMENT_EXTENSION CreateLQBProjectedPaymentExtension(bool interestOnly)
        {
            var extension = new LQB_PROJECTED_PAYMENT_EXTENSION();
            extension.InterestOnlyIndicator = ToMismoIndicator(interestOnly);

            return extension;
        }

        /// <summary>
        /// Creates a PROJECTED_PAYMENTS object with a list of projected payments as displayed on a Loan Estimate or Closing Disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>A PROJECTED_PAYMENTS object with a list of projected payments as displayed on a Loan Estimate or Closing Disclosure.</returns>
        private PROJECTED_PAYMENTS CreateProjectedPayments(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.hasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            List<TridProjectedPayment> payments;

            try
            {
                if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
                {
                    payments = this.loanEstimateArchiveData.sTridProjectedPayments;
                }
                else
                {
                    payments = this.closingDisclosureArchiveData.sTridProjectedPayments;
                }
            }
            catch (CBaseException) when (this.isLenderExportMode)
            {
                payments = null;
            }

            if (payments == null || payments.Count == 0)
            {
                return null;
            }

            var projectedPayments = new PROJECTED_PAYMENTS();

            foreach (TridProjectedPayment payment in payments)
            {
                projectedPayments.ProjectedPayment.Add(this.CreateProjectedPayment(payment, projectedPayments.ProjectedPayment.Count(p => p != null) + 1));
            }

            return projectedPayments;
        }

        /// <summary>
        /// Creates a PRORATION_ITEM container with the given amount, payment period, etc.
        /// </summary>
        /// <param name="section">The section of the loan estimate or closing disclosure in which the proration is displayed.</param>
        /// <param name="subsection">The subsection of the loan estimate or closing disclosure that corresponds to the proration.</param>
        /// <param name="prorationItemType">The type of proration.</param>
        /// <param name="proration">The proration item to create the container for. Not all values are used, only PaidFromDate, PaidToDate, and Description.</param>
        /// <param name="amount">The proration amount. Requires a <code>rep or LosConverted</code> value.</param>
        /// <param name="excludeFromLeCd">Indicates whether the proration item should be excluded from the LE/CD.</param>
        /// <param name="sequence">The sequence of the proration among the list of prorations.</param>
        /// <returns>A PRORATION_ITEM container with the given amount, payment period, etc.</returns>
        private PRORATION_ITEM CreateProrationItem(
                                                    IntegratedDisclosureSectionBase section,
                                                    IntegratedDisclosureSubsectionBase subsection,
                                                    ProrationItemBase prorationItemType,
                                                    Proration proration,
                                                    string amount,
                                                    bool excludeFromLeCd,
                                                    int sequence)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var prorationItem = new PRORATION_ITEM();
            prorationItem.IntegratedDisclosureSectionType = ToIntegratedDisclosureSectionEnum(section);
            prorationItem.IntegratedDisclosureSubsectionType = ToIntegratedDisclosureSubsectionEnum(subsection);
            prorationItem.ProrationItemAmount = ToMismoAmount(amount);
            prorationItem.ProrationItemPaidFromDate = ToMismoDate(proration.PaidFromDate_rep);
            prorationItem.ProrationItemPaidThroughDate = ToMismoDate(proration.PaidToDate_rep);
            prorationItem.ProrationItemType = ToProrationItemEnum(prorationItemType, proration.Description);
            prorationItem.ProrationItemTypeOtherDescription = ToMismoString(prorationItemType == ProrationItemBase.Other ? proration.Description : string.Empty);

            if (ShouldSendTrid2017SubordinateFinancingData(this.loanData.sLienPosT, this.loanData.sIsOFinNew, this.loanData.sConcurSubFin, this.loanData.sTridTargetRegulationVersionT))
            {
                prorationItem.Extension = this.CreateProrationItemExtension(excludeFromLeCd);
            }

            prorationItem.SequenceNumber = sequence;
            return prorationItem;
        }

        /// <summary>
        /// Creates a container extending the PRORATION_ITEM element.
        /// </summary>
        /// <param name="excludeFromLeCd">Indicates whether the proration item should be excluded from the LE/CD.</param>
        /// <returns>A PRORATION_ITEM_EXTENSION container.</returns>
        private PRORATION_ITEM_EXTENSION CreateProrationItemExtension(bool excludeFromLeCd)
        {
            var extension = new PRORATION_ITEM_EXTENSION();
            extension.Other = this.CreateLqbProrationItemExtension(excludeFromLeCd);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary container extending the PRORATION_ITEM element.
        /// </summary>
        /// <param name="excludeFromLeCd">Indicates whether the proration item should be excluded from the LE/CD.</param>
        /// <returns>An LQB_PRORATION_ITEM_EXTENSION container.</returns>
        private LQB_PRORATION_ITEM_EXTENSION CreateLqbProrationItemExtension(bool excludeFromLeCd)
        {
            var extension = new LQB_PRORATION_ITEM_EXTENSION();
            extension.ExcludeFromLECDForThisLien = ToMismoIndicator(excludeFromLeCd);

            return extension;
        }
        
        /// <summary>
        /// Creates a PRORATION_ITEMS container populated with the prorations as entered on the Adjustments and Other Credits page.
        /// </summary>
        /// <returns>Returns a PRORATION_ITEMS container.</returns>
        private PRORATION_ITEMS CreateProrationItems()
        {
            if (this.loanData.sProrationList == null)
            {
                return null;
            }

            var prorationItems = new PRORATION_ITEMS();
            var excludeProrationsFromLeCd = !this.loanData.sProrationList.IncludeProrationsInLeCdForThisLien;

            //// K. Due from Borrower at Closing - Adjustments for Items Paid by Seller in Advance
            //// And M. Due to Seller at Closing - Adjustments for Items Paid by Seller in Advance
            if (this.loanData.sProrationList.CityTownTaxes != null && this.loanData.sProrationList.CityTownTaxes.IsPaidFromBorrowerToSeller)
            {
                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.CityPropertyTax,
                        this.loanData.sProrationList.CityTownTaxes,
                        this.loanData.sProrationList.CityTownTaxes.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        1));

                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.CityPropertyTax,
                        this.loanData.sProrationList.CityTownTaxes,
                        this.loanData.sProrationList.CityTownTaxes.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));
            }

            if (this.loanData.sProrationList.CountyTaxes != null && this.loanData.sProrationList.CountyTaxes.IsPaidFromBorrowerToSeller)
            {
                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.CountyPropertyTax,
                        this.loanData.sProrationList.CountyTaxes,
                        this.loanData.sProrationList.CountyTaxes.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));

                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.CountyPropertyTax,
                        this.loanData.sProrationList.CountyTaxes,
                        this.loanData.sProrationList.CountyTaxes.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));
            }

            ProrationItemBase assessmentType = GetProrationItemBase_Assessment(this.loanData.sGseSpT);

            if (this.loanData.sProrationList.Assessments != null && this.loanData.sProrationList.Assessments.IsPaidFromBorrowerToSeller)
            {
                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        assessmentType,
                        this.loanData.sProrationList.Assessments,
                        this.loanData.sProrationList.Assessments.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));

                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        assessmentType,
                        this.loanData.sProrationList.Assessments,
                        this.loanData.sProrationList.Assessments.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));
            }

            var prorationList = this.loanData.sProrationList.GetCustomItemsEnumerable() ?? new List<Proration>();

            foreach (Proration proration in prorationList.Where(a => a.IsPaidFromBorrowerToSeller && a.Amount > 0.00m))
            {
                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.Other,
                        proration,
                        proration.Amount_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));

                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.Other,
                        proration,
                        proration.Amount_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));
            }

            //// L. Paid Already by or on Behalf of the Borrower at Closing - Adjustments for Items Unpaid by Seller in Advance
            //// And N. Due from Seller at Closing - Adjustments for Items Unpaid by Seller in Advance
            if (this.loanData.sProrationList.CityTownTaxes != null && this.loanData.sProrationList.CityTownTaxes.IsPaidFromSellerToBorrower)
            {
                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.CityPropertyTax,
                        this.loanData.sProrationList.CityTownTaxes,
                        this.loanData.sProrationList.CityTownTaxes.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));

                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.CityPropertyTax,
                        this.loanData.sProrationList.CityTownTaxes,
                        this.loanData.sProrationList.CityTownTaxes.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));
            }

            if (this.loanData.sProrationList.CountyTaxes != null && this.loanData.sProrationList.CountyTaxes.IsPaidFromSellerToBorrower)
            {
                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.CountyPropertyTax,
                        this.loanData.sProrationList.CountyTaxes,
                        this.loanData.sProrationList.CountyTaxes.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));

                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.CountyPropertyTax,
                        this.loanData.sProrationList.CountyTaxes,
                        this.loanData.sProrationList.CountyTaxes.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));
            }

            if (this.loanData.sProrationList.Assessments != null && this.loanData.sProrationList.Assessments.IsPaidFromSellerToBorrower)
            {
                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        assessmentType,
                        this.loanData.sProrationList.Assessments,
                        this.loanData.sProrationList.Assessments.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));

                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        assessmentType,
                        this.loanData.sProrationList.Assessments,
                        this.loanData.sProrationList.Assessments.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));
            }

            foreach (Proration proration in prorationList.Where(a => a.IsPaidFromSellerToBorrower && a.Amount > 0.00m))
            {
                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.Other,
                        proration,
                        proration.Amount_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));

                prorationItems.ProrationItem.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.Other,
                        proration,
                        proration.Amount_rep,
                        excludeProrationsFromLeCd,
                        prorationItems.ProrationItem.Count(p => p != null) + 1));
            }

            return prorationItems.ProrationItem.Count(i => i != null) > 0 ? prorationItems : null;
        }

        /// <summary>
        /// Creates a container representing a sales concession.
        /// </summary>
        /// <param name="sequence">The sequence number of the sales concession among the list of concessions.</param>
        /// <returns>A SALES_CONCESSION container.</returns>
        private SALES_CONCESSION CreateSalesConcession(int sequence)
        {
            var salesConcession = new SALES_CONCESSION();
            salesConcession.SequenceNumber = sequence;

            if (this.loanData.sLT == E_sLT.Conventional)
            {
                salesConcession.SalesConcessionAmount = ToMismoAmount(this.loanData.sFHASalesConcessions_rep);
            }

            return salesConcession;
        }

        /// <summary>
        /// Creates a container to hold all sales concessions.
        /// </summary>
        /// <returns>A SALES_CONCESSIONS container.</returns>
        private SALES_CONCESSIONS CreateSalesConcessions()
        {
            var salesConcessions = new SALES_CONCESSIONS();
            salesConcessions.SalesConcession.Add(this.CreateSalesConcession(1));

            return salesConcessions;
        }

        /// <summary>
        /// Creates a container representing a sales contract.
        /// </summary>
        /// <param name="sequence">The sequence number of the sales contract among the list of contracts.</param>
        /// <returns>A SALES_CONTRACT container.</returns>
        private SALES_CONTRACT CreateSalesContract(int sequence)
        {
            var salesContract = new SALES_CONTRACT();
            salesContract.SequenceNumber = sequence;
            salesContract.SalesConcessions = this.CreateSalesConcessions();
            salesContract.SalesContractDetail = this.CreateSalesContractDetail();

            return salesContract;
        }

        /// <summary>
        /// Creates a container with details on a sales contract.
        /// </summary>
        /// <returns>A SALES_CONTRACT_DETAIL container.</returns>
        private SALES_CONTRACT_DETAIL CreateSalesContractDetail()
        {
            var salesContractDetail = new SALES_CONTRACT_DETAIL();

            bool personalPropertyIncluded = this.loanData.sGrossDueFromBorrPersonalProperty != 0.00m;
            salesContractDetail.PersonalPropertyIncludedIndicator = ToMismoIndicator(personalPropertyIncluded);

            if (personalPropertyIncluded)
            {
                salesContractDetail.RealPropertyAmount = ToMismoAmount(this.loanData.sPurchPrice_rep);
                salesContractDetail.PersonalPropertyAmount = ToMismoAmount(this.loanData.sGrossDueFromBorrPersonalProperty_rep);
            }
            else
            {
                salesContractDetail.SalesContractAmount = ToMismoAmount(this.loanData.sPurchPrice_rep);
            }

            return salesContractDetail;
        }

        /// <summary>
        /// Creates a container to hold all sales contracts.
        /// </summary>
        /// <returns>A SALES_CONTRACTS container.</returns>
        private SALES_CONTRACTS CreateSalesContracts()
        {
            var salesContracts = new SALES_CONTRACTS();
            salesContracts.SalesContract.Add(this.CreateSalesContract(1));

            return salesContracts;
        }

        /// <summary>
        /// Creates a container representing the security instrument document.
        /// </summary>
        /// <returns>A SECURITY_INSTRUMENT container.</returns>
        private SECURITY_INSTRUMENT CreateSecurityInstrument()
        {
            var securityInstrument = new SECURITY_INSTRUMENT();
            securityInstrument.SecurityInstrumentDetail = this.CreateSecurityInstrumentDetail();
            ////securityInstrument.SecurityInstrumentRiders = this.CreateSecurityInstrumentRiders();

            return securityInstrument;
        }

        /// <summary>
        /// Creates a container holding details on the security instrument document.
        /// </summary>
        /// <returns>A SECURITY_INSTRUMENT_DETAIL container.</returns>
        private SECURITY_INSTRUMENT_DETAIL CreateSecurityInstrumentDetail()
        {
            var securityInstrumentDetail = new SECURITY_INSTRUMENT_DETAIL();
            securityInstrumentDetail.DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = ToMismoIndicator(this.loanData.sAcknowledgeCashAdvanceForNonHomestead);
            securityInstrumentDetail.SecurityInstrumentAttorneyFeePercent = ToMismoPercent(this.loanData.sSecInstrAttorneyFeesPc_rep);
            securityInstrumentDetail.SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = ToMismoIndicator(this.loanData.sNyPropertyStatementT == E_sNyPropertyStatementT.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator);
            securityInstrumentDetail.SecurityInstrumentOweltyOfPartitionIndicator = ToMismoIndicator(this.loanData.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.OweltyOfPartition);
            securityInstrumentDetail.SecurityInstrumentPurchaseMoneyIndicator = ToMismoIndicator(this.loanData.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.PurchaseMoney);
            securityInstrumentDetail.SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator = ToMismoIndicator(this.loanData.sNyPropertyStatementT == E_sNyPropertyStatementT.RealPropertyImprovedOrToBeImprovedIndicator);
            securityInstrumentDetail.SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator = ToMismoIndicator(this.loanData.sNyPropertyStatementT == E_sNyPropertyStatementT.RealPropertyImprovementsNotCoveredIndicator);
            securityInstrumentDetail.SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator = ToMismoIndicator(this.loanData.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.RenewalAndExtensionOfLiens);
            securityInstrumentDetail.SecurityInstrumentTrusteeFeePercent = ToMismoPercent(this.loanData.sTrusteeFeePc_rep);
            securityInstrumentDetail.SecurityInstrumentVestingDescription = ToMismoString(this.loanData.sVestingToRead);

            return securityInstrumentDetail;
        }

        /*
        private SECURITY_INSTRUMENT_RIDER CreateSecurityInstrumentRider(int sequence)
        {
            var rider = new SECURITY_INSTRUMENT_RIDER();
            rider.SecurityInstrumentRiderType;
            rider.SecurityInstrumentRiderTypeOtherDescription;
            rider.SequenceNumber = sequence;

            return rider;
        }

        /// <summary>
        /// Creates a SECURITY_INSTRUMENT_RIDERS container.
        /// </summary>
        /// <returns>A SECURITY_INSTRUMENT_RIDERS container.</returns>
        private SECURITY_INSTRUMENT_RIDERS CreateSecurityInstrumentRiders()
        {
            var riders = new SECURITY_INSTRUMENT_RIDERS();
            riders.SecurityInstrumentRider.Add(this.CreateSecurityInstrumentRider(1));

            return riders;
        }
        */

        /// <summary>
        /// Creates a container holding information on a structure.
        /// </summary>
        /// <returns>A STRUCTURE container.</returns>
        private STRUCTURE CreateStructure()
        {
            var structure = new STRUCTURE();
            structure.StructureDetail = this.CreateStructureDetail();

            return structure;
        }

        /// <summary>
        /// Creates a container holding details on a structure.
        /// </summary>
        /// <returns>A STRUCTURE_DETAIL container.</returns>
        private STRUCTURE_DETAIL CreateStructureDetail()
        {
            var structureDetail = new STRUCTURE_DETAIL();

            if (this.loanData.sGseSpT == E_sGseSpT.Condominium ||
                this.loanData.sGseSpT == E_sGseSpT.DetachedCondominium ||
                this.loanData.sGseSpT == E_sGseSpT.HighRiseCondominium ||
                this.loanData.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium)
            {
                structureDetail.StoriesNumber = ToMismoNumeric(this.loanData.sProdCondoStories_rep);
            }

            return structureDetail;
        }

        /// <summary>
        /// Creates a container that represents a property.
        /// </summary>
        /// <param name="sequence">The sequence number of the property among the list of subject properties.</param>
        /// <returns>A PROPERTY container.</returns>
        private PROPERTY CreateSubjectProperty(int sequence)
        {
            var property = new PROPERTY();
            property.Address = CreateAddress(
                this.loanData.sSpAddr, 
                this.loanData.sSpCity, 
                this.loanData.sSpState, 
                this.loanData.sSpZip, 
                this.loanData.sSpCounty, 
                this.loanData.sHmdaCountyCode,
                this.statesAndTerritories.GetStateName(this.loanData.sSpState),
                AddressBase.Blank, 
                1);

            property.FloodDetermination = this.CreateFloodDetermination();
            property.HazardInsurances = this.CreateHazardInsurances();
            property.Improvement = this.CreateImprovement();
            property.LegalDescriptions = this.CreateLegalDescriptions();
            property.LocationIdentifier = this.CreateLocationIdentifier();
            property.ManufacturedHome = this.CreateManufacturedHome();
            property.Project = this.CreateProject();
            property.PropertyDetail = this.CreatePropertyDetail();
            property.PropertyTaxes = this.CreatePropertyTaxes();
            property.PropertyTitle = this.CreatePropertyTitle();
            property.PropertyValuations = this.CreatePropertyValuations();

            property.PropertyUnits = this.CreatePropertyUnits();
            if (!this.loanData.sIsRefinancing || this.loanData.sGrossDueFromBorrPersonalProperty != 0.00m)
            {
                property.SalesContracts = this.CreateSalesContracts();
            }

            property.ValuationUseType = ValuationUseBase.SubjectProperty;
            property.SequenceNumber = sequence;

            return property;
        }

        /// <summary>
        /// Creates a PROPERTY_UNITS container.
        /// </summary>
        /// <returns>The PROPERTY_UNITS container.</returns>
        private PROPERTY_UNITS CreatePropertyUnits()
        {
            PROPERTY_UNITS propertyUnits = new PROPERTY_UNITS();
            propertyUnits.PropertyUnit.Add(this.CreatePropertyUnit());

            return propertyUnits;
        }

        /// <summary>
        /// Creates a PROPERTY_UNIT container.
        /// </summary>
        /// <returns>The PROPERTY_UNIT container.</returns>
        private PROPERTY_UNIT CreatePropertyUnit()
        {
            PROPERTY_UNIT propertyUnit = new PROPERTY_UNIT();
            propertyUnit.PropertyUnitDetail = this.CreatePropertyUnitDetail();

            return propertyUnit;
        }

        /// <summary>
        /// Creates a PROPERTY_UNIT_DETAIL container.
        /// </summary>
        /// <returns>The PROPERTY_UNIT_DETAIL container.</returns>
        private PROPERTY_UNIT_DETAIL CreatePropertyUnitDetail()
        {
            PROPERTY_UNIT_DETAIL propertyUnitDetail = new PROPERTY_UNIT_DETAIL();
            propertyUnitDetail.BedroomCount = ToMismoCount(this.loanData.sSpBedroomCountAsRoundedNonNegativeInt, this.loanData.m_convertLos);
            propertyUnitDetail.TotalRoomCount = ToMismoCount(this.loanData.sSpRoomCountAsRoundedNonNegativeInt, this.loanData.m_convertLos);

            return propertyUnitDetail;
        }

        /// <summary>
        /// Creates a container that represents a property.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <param name="sequence">The sequence number of the property among the list of real estate owned properties.</param>
        /// <returns>A PROPERTY container.</returns>
        private PROPERTY CreateProperty(IRealEstateOwned reo, int sequence)
        {
            var property = new PROPERTY();
            property.Address = CreateAddress(
                reo.Addr, 
                reo.City, 
                reo.State, 
                reo.Zip, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(reo.State),
                AddressBase.Blank, 
                1);

            property.PropertyDetail = this.CreatePropertyDetail(reo);
            property.SequenceNumber = sequence;

            if (reo.TypeT == E_ReoTypeT.Condo || reo.TypeT == E_ReoTypeT.Coop || reo.TypeT == E_ReoTypeT.Town)
            {
                property.Project = this.CreateProject(reo);
            }

            if (reo.IsSubjectProp)
            {
                property.ValuationUseType = ValuationUseBase.SubjectProperty;
            }

            return property;
        }

        /// <summary>
        /// Creates a container holding data about a property.
        /// </summary>
        /// <returns>A PROPERTY_DETAIL container.</returns>
        private PROPERTY_DETAIL CreatePropertyDetail()
        {
            var propertyDetail = new PROPERTY_DETAIL();
            propertyDetail.AttachmentType = ToAttachmentEnum(this.loanData.sGseSpT);

            if (this.loanData.sConstructionPurposeT != ConstructionPurpose.Blank && IsConstructionLoan(this.loanData.sLPurposeT))
            {
                propertyDetail.ConstructionMethodType = ToConstructionMethodEnum(this.loanData.sConstructionMethodT);
            }
            else
            {
                propertyDetail.ConstructionMethodType = ToConstructionMethodEnum(this.loanData.sGseSpT);
            }
            
            propertyDetail.FinancedUnitCount = ToMismoCount(this.loanData.sUnitsNum_rep, null);
            propertyDetail.PropertyEstateType = ToPropertyEstateEnum(this.loanData.sEstateHeldT);
            propertyDetail.PropertyEstimatedValueAmount = ToMismoAmount(this.loanData.sApprVal_rep);
            propertyDetail.PropertyGroundLeaseExpirationDate = ToMismoDate(this.loanData.sLeaseHoldExpireD_rep);
            propertyDetail.PropertyStructureBuiltYear = ToMismoYear(this.loanData.sYrBuilt, "sYrBuilt", false, this.isLenderExportMode);
            propertyDetail.PropertyUsageType = ToPropertyUsageEnum(this.primaryApp.aOccT);
            propertyDetail.PUDIndicator = ToMismoIndicator(this.loanData.sGseSpT == E_sGseSpT.PUD);

            // Construction loan data points
            if (this.loanData.sLPurposeT == E_sLPurposeT.Construct || this.loanData.sLPurposeT == E_sLPurposeT.ConstructPerm)
            {
                propertyDetail.PropertyAcquiredYear = ToMismoYear(this.loanData.sLotAcqYr, "sLotAcqYr", false, this.isLenderExportMode);
                propertyDetail.PropertyAcquiredDate = ToMismoDate(this.loanData.sLotAcquiredD_rep);
                propertyDetail.PropertyExistingLienAmount = ToMismoAmount(this.loanData.sLotLien_rep);
                propertyDetail.PropertyOriginalCostAmount = ToMismoAmount(this.loanData.sLotOrigC_rep);
                propertyDetail.ConstructionStatusType = ToConstructionStatusEnum(this.loanData.sBuildingStatusT);
            }

            // Refinance loan data points
            if (this.loanData.sIsRefinancing)
            {
                propertyDetail.PropertyAcquiredYear = ToMismoYear(this.loanData.sSpAcqYr, "sSpAcqYr", false, this.isLenderExportMode);
                propertyDetail.PropertyExistingLienAmount = ToMismoAmount(this.loanData.sSpLien_rep);
                propertyDetail.PropertyOriginalCostAmount = ToMismoAmount(this.loanData.sSpOrigC_rep);
            }
            
            return propertyDetail;
        }

        /// <summary>
        /// Creates a container holding data about a property.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <returns>A PROPERTY_DETAIL container.</returns>
        private PROPERTY_DETAIL CreatePropertyDetail(IRealEstateOwned reo)
        {
            var propertyDetail = new PROPERTY_DETAIL();
            propertyDetail.PropertyEstimatedValueAmount = ToMismoAmount(reo.Val_rep);
            propertyDetail.LandUseType = ToLandUseEnum(reo.TypeT);

            if (reo.TypeT == E_ReoTypeT.Mobil)
            {
                propertyDetail.ConstructionMethodType = ToConstructionMethodEnum(ConstructionMethodBase.Manufactured);
            }

            if (reo.TypeT == E_ReoTypeT.SFR)
            {
                propertyDetail.AttachmentType = ToAttachmentEnum(E_sGseSpT.Detached);
            }

            return propertyDetail;
        }

        /// <summary>
        /// Creates a container representing a property owner.
        /// </summary>
        /// <param name="appData">A borrower's application data.</param>
        /// <returns>A PROPERTY_OWNER container.</returns>
        private PROPERTY_OWNER CreatePropertyOwner(CAppData appData)
        {
            var propertyOwner = new PROPERTY_OWNER();
            propertyOwner.RelationshipVestingType = ToRelationshipVestingEnum(appData.aManner);

            if (propertyOwner.RelationshipVestingType != null && propertyOwner.RelationshipVestingType.enumValue == RelationshipVestingBase.Other)
            {
                propertyOwner.RelationshipVestingTypeOtherDescription = ToMismoString(appData.aManner);
            }

            return propertyOwner;
        }
        
        /// <summary>
        /// Creates a container holding a set of property taxes.
        /// </summary>
        /// <returns>A PROPERTY_TAXES container.</returns>
        private PROPERTY_TAXES CreatePropertyTaxes()
        {
            var propertyTaxes = new PROPERTY_TAXES();
            
            foreach (BaseHousingExpense expense in this.loanData.sHousingExpenses.ExpensesToUse.Where(e => e.IsTax && e.MonthlyAmtTotal != 0.00m))
            {
                propertyTaxes.PropertyTax.Add(this.CreatePropertyTax(expense));
            }

            return propertyTaxes;
        }

        /// <summary>
        /// Creates a container representing a property tax.
        /// </summary>
        /// <param name="tax">A <see cref="BaseHousingExpense"/> representing the tax.</param>
        /// <returns>A PROPERTY_TAX container.</returns>
        private PROPERTY_TAX CreatePropertyTax(BaseHousingExpense tax)
        {
            var propertyTax = new PROPERTY_TAX();
            propertyTax.PropertyTaxDetail = this.CreatePropertyTaxDetail(tax);
            propertyTax.SequenceNumber = this.counters.PropertyTaxes;
            propertyTax.Extension = this.CreatePropertyTaxExtension(tax);

            return propertyTax;
        }

        /// <summary>
        /// Creates a container holding details on a property tax.
        /// </summary>
        /// <param name="tax">A <see cref="BaseHousingExpense"/> representing the tax.</param>
        /// <returns>A PROPERTY_TAX_DETAIL container.</returns>
        private PROPERTY_TAX_DETAIL CreatePropertyTaxDetail(BaseHousingExpense tax)
        {
            //// 6/18/2015 BB - This container is meant to capture data on the appraisal, e.g. 1004, most of which we don't have. The actual tax monthlies/escrows are captured elsewhere. This container could probably be omitted, but it's fine to include what data we have.
            if (!tax.IsTax || tax.MonthlyAmtTotal == 0.00m)
            {
                return null;
            }

            var propertyTaxDetail = new PROPERTY_TAX_DETAIL();
            propertyTaxDetail.TaxAuthorityType = ToTaxAuthorityEnum(tax.TaxType);

            if (propertyTaxDetail.TaxAuthorityType.enumValue == TaxAuthorityBase.Other && tax.TaxType != E_TaxTableTaxT.LeaveBlank)
            {
                propertyTaxDetail.TaxAuthorityTypeOtherDescription = ToMismoString(GetXmlEnumName(tax.TaxType));
            }

            propertyTaxDetail.TaxEscrowedIndicator = ToMismoIndicator(tax.IsEscrowedAtClosing);

            return propertyTaxDetail;
        }

        /// <summary>
        /// Creates a PROPERTY_TAX_EXTENSION container.
        /// </summary>
        /// <param name="tax">The tax to export.</param>
        /// <returns>A PROPERTY_TAX_EXTENSION container.</returns>
        private PROPERTY_TAX_EXTENSION CreatePropertyTaxExtension(BaseHousingExpense tax)
        {
            var extension = new PROPERTY_TAX_EXTENSION();
            extension.Mismo = this.CreateMismoPropertyTaxExtension(tax);

            return extension;
        }

        /// <summary>
        /// Creates a MISMO_PROPERTY_TAX_EXTENSION container.
        /// </summary>
        /// <param name="tax">The tax to export.</param>
        /// <returns>A MISMO_PROPERTY_TAX_EXTENSION container.</returns>
        private MISMO_PROPERTY_TAX_EXTENSION CreateMismoPropertyTaxExtension(BaseHousingExpense tax)
        {
            var extension = new MISMO_PROPERTY_TAX_EXTENSION();

            var disbursementSet = tax.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements
                ? tax.ActualDisbursementsList
                : tax.ProjectedDisbursements;
            extension.EscrowItemDisbursements = this.CreateEscrowItemDisbursements(disbursementSet, forceExportDisbursements: true);

            return extension;
        }

        /// <summary>
        /// Creates Property Title container with title preliminary report date for subject property. 
        /// </summary>
        /// <returns>Returns Property Title container with title preliminary report date for subject property. </returns>
        private PROPERTY_TITLE CreatePropertyTitle()
        {
            PROPERTY_TITLE propertyTitle = new PROPERTY_TITLE();
            propertyTitle.TitlePreliminaryReportDate = ToMismoDate(this.loanData.sPrelimRprtDocumentD_rep);
            propertyTitle.TitleReportItemsDescription = ToMismoString(this.loanData.sTitleReportItemsDescription);
            propertyTitle.TitleReportRequiredEndorsementsDescription = ToMismoString(this.loanData.sRequiredEndorsements);
            return propertyTitle;
        }

        /// <summary>
        /// Creates a container holding information on a property valuation.
        /// </summary>
        /// <returns>A PROPERTY_VALUATION container.</returns>
        private PROPERTY_VALUATION CreatePropertyValuation()
        {
            var propertyValuation = new PROPERTY_VALUATION();
            propertyValuation.SequenceNumber = 1;
            propertyValuation.PropertyValuationDetail = this.CreatePropertyValuationDetail();
            propertyValuation.Extension = this.CreatePropertyValuationExtension();

            return propertyValuation;
        }

        /// <summary>
        /// Creates a container holding extended information on a property valuation.
        /// </summary>
        /// <returns>A PROPERTY_VALUATION_EXTENSION container.</returns>
        private PROPERTY_VALUATION_EXTENSION CreatePropertyValuationExtension()
        {
            var propertyValuationExtension = new PROPERTY_VALUATION_EXTENSION();
            propertyValuationExtension.Other = this.CreateLqbPropertyValuationExtension();

            return propertyValuationExtension;
        }

        /// <summary>
        /// Creates a container holding LQB-branded information on a property valuation.
        /// </summary>
        /// <returns>An LQB_PROPERTY_VALUATION_EXTENSION container.</returns>
        private LQB_PROPERTY_VALUATION_EXTENSION CreateLqbPropertyValuationExtension()
        {
            var extension = new LQB_PROPERTY_VALUATION_EXTENSION();
            extension.CuRiskScore = ToMismoString(this.loanData.sSpCuScore_rep);

            return extension;
        }

        /// <summary>
        /// Creates a container to hold all instances of PROPERTY_VALUATION.
        /// </summary>
        /// <returns>A PROPERTY_VALUATIONS container.</returns>
        private PROPERTY_VALUATIONS CreatePropertyValuations()
        {
            var propertyValuations = new PROPERTY_VALUATIONS();
            propertyValuations.PropertyValuation.Add(this.CreatePropertyValuation());

            return propertyValuations;
        }

        /// <summary>
        /// Creates a container holding details on a property valuation.
        /// </summary>
        /// <returns>A PROPERTY_VALUATION_DETAIL container.</returns>
        private PROPERTY_VALUATION_DETAIL CreatePropertyValuationDetail()
        {
            var propertyValuationDetail = new PROPERTY_VALUATION_DETAIL();
            propertyValuationDetail.PropertyValuationAmount = ToMismoAmount(this.loanData.sApprVal_rep);
            propertyValuationDetail.PropertyInspectionType = ToPropertyInspectionEnum(this.loanData.sPropertyInspectionT);
            propertyValuationDetail.PropertyValuationFormType = ToPropertyValuationFormEnum(this.loanData.sSpAppraisalFormT);
            propertyValuationDetail.PropertyValuationMethodType = ToPropertyValuationMethodEnum(this.loanData.sSpValuationMethodT);

            // OPM 247161 - UCD support.
            if (!string.IsNullOrEmpty(this.loanData.sSpAppraisalId))
            {
                propertyValuationDetail.AppraisalIdentifier = ToMismoIdentifier(this.loanData.sSpAppraisalId);
                propertyValuationDetail.AppraisalIdentifier.IdentifierOwnerURI = ConstStage.MismoAppraisalIdentifierOwnerURI;
            }

            if (this.loanData.sSpValuationMethodT == E_sSpValuationMethodT.FieldReview)
            {
                propertyValuationDetail.PropertyValuationMethodTypeOtherDescription = ToMismoString("FieldReview-FNM2000");
            }

            return propertyValuationDetail;
        }

        /// <summary>
        /// Creates a container representing a purchase credit.
        /// </summary>
        /// <param name="amount">The amount of the purchase credit.</param>
        /// <param name="description">A description of the purchase credit.</param>
        /// <param name="sequence">The sequence number of the purchase credit among the list of credits.</param>
        /// <returns>A PURCHASE_CREDIT container.</returns>
        private PURCHASE_CREDIT CreatePurchaseCredit(string amount, string description, int sequence)
        {
            if (IsAmountStringBlankOrZero(amount) ||
                string.IsNullOrEmpty(description))
            {
                return null;
            }

            var purchaseCredit = new PURCHASE_CREDIT();

            purchaseCredit.PurchaseCreditAmount = ToMismoAmount(amount);
            purchaseCredit.PurchaseCreditSourceType = ToPurchaseCreditSourceEnum(description);
            purchaseCredit.PurchaseCreditType = ToPurchaseCreditEnum(description);
            purchaseCredit.SequenceNumber = sequence;

            if (purchaseCredit.PurchaseCreditType != null && purchaseCredit.PurchaseCreditType.enumValue == PurchaseCreditBase.Other)
            {
                purchaseCredit.PurchaseCreditTypeOtherDescription = ToMismoString(description);
            }

            return purchaseCredit;
        }

        /// <summary>
        /// Creates a container to hold all purchase credits.
        /// </summary>
        /// <returns>A PURCHASE_CREDITS container.</returns>
        private PURCHASE_CREDITS CreatePurchaseCredits()
        {
            var purchaseCredits = new PURCHASE_CREDITS();
            purchaseCredits.PurchaseCredit.Add(this.CreatePurchaseCredit(this.loanData.sOCredit1Amt_rep, this.loanData.sOCredit1Desc, 1));
            purchaseCredits.PurchaseCredit.Add(this.CreatePurchaseCredit(this.loanData.sOCredit2Amt_rep, this.loanData.sOCredit2Desc, purchaseCredits.PurchaseCredit.Count(p => p != null) + 1));
            purchaseCredits.PurchaseCredit.Add(this.CreatePurchaseCredit(this.loanData.sOCredit3Amt_rep, this.loanData.sOCredit3Desc, purchaseCredits.PurchaseCredit.Count(p => p != null) + 1));
            purchaseCredits.PurchaseCredit.Add(this.CreatePurchaseCredit(this.loanData.sOCredit4Amt_rep, this.loanData.sOCredit4Desc, purchaseCredits.PurchaseCredit.Count(p => p != null) + 1));
            if (!this.isClosingPackage && this.loanData.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                var creditAmount = this.vendor == E_DocumentVendor.DocMagic ? this.loanData.sTRIDLoanEstimateGeneralLenderCredits_rep : this.loanData.sTRIDLoanEstimateLenderCredits_rep;
                purchaseCredits.PurchaseCredit.Add(this.CreatePurchaseCredit(creditAmount, this.loanData.sOCredit5Desc, purchaseCredits.PurchaseCredit.Count(p => p != null) + 1));
            }
            else
            {
                var creditAmount = this.vendor == E_DocumentVendor.DocMagic ? this.loanData.sTRIDClosingDisclosureGeneralLenderCredits_rep : this.loanData.sOCredit5Amt_rep;
                purchaseCredits.PurchaseCredit.Add(this.CreatePurchaseCredit(creditAmount, this.loanData.sOCredit5Desc, purchaseCredits.PurchaseCredit.Count(p => p != null) + 1));
            }

            return purchaseCredits;
        }
        
        /// <summary>
        /// Creates a container holding data on a qualification.
        /// </summary>
        /// <returns>A QUALIFICATION populated with data.</returns>
        private QUALIFICATION CreateQualification()
        {
            var qualification = new QUALIFICATION();
            qualification.QualifyingRatePercent = ToMismoPercent(this.loanData.sQualIR_rep);
            qualification.TotalVerifiedReservesAmount = ToMismoAmount(this.loanData.sFredieReservesAmt_rep);
            qualification.HousingExpenseRatioPercent = ToMismoPercent(this.loanData.sQualTopR_rep);

            if (this.loanData.sLT == E_sLT.VA)
            {
                qualification.TotalDebtExpenseRatioPercent = ToMismoPercent(this.primaryApp.aVaRatio_rep);
            }
            else
            {
                qualification.TotalDebtExpenseRatioPercent = ToMismoPercent(this.loanData.sQualBottomR_rep);
            }

            return qualification;
        }

        /// <summary>
        /// Creates a QUALIFIED_MORTGAGE container with information regarding the loan file's Qualified Mortgage eligibility.
        /// </summary>
        /// <returns>A QUALIFIED_MORTGAGE container with information regarding the loan file's Qualified Mortgage eligibility.</returns>
        private QUALIFIED_MORTGAGE CreateQualifiedMortgage()
        {
            var qualifiedMortgage = new QUALIFIED_MORTGAGE();
            qualifiedMortgage.QualifiedMortgageDetail = this.CreateQualifiedMortgageDetail();
            qualifiedMortgage.Exemptions = this.CreateExemptions();

            return qualifiedMortgage;
        }

        /// <summary>
        /// Creates a QUALIFIED_MORTGAGE_DETAIL container with information regarding the loan file's Qualified Mortgage eligibility.
        /// </summary>
        /// <returns>A QUALIFIED_MORTGAGE_DETAIL container with information regarding the loan file's Qualified Mortgage eligibility.</returns>
        private QUALIFIED_MORTGAGE_DETAIL CreateQualifiedMortgageDetail()
        {
            var detail = new QUALIFIED_MORTGAGE_DETAIL();

            decimal dti;
            if (decimal.TryParse(this.loanData.sQMQualBottom_rep, out dti))
            {
                detail.QualifiedMortgageHighestRateDebtExpenseRatioPercent = ToMismoPercent(this.loanData.sQMQualBottom_rep);
            }

            detail.QualifiedMortgageTotalPointsAndFeesThresholdAmount = ToMismoAmount(this.loanData.sQMMaxPointAndFeesAllowedAmt_rep);
            detail.QualifiedMortgageTotalPointsAndFeesThresholdPercent = ToMismoPercent(this.loanData.sQMMaxPointAndFeesAllowedPc_rep);
            detail.AbilityToRepayMethodType = ToAbilityToRepayMethodEnum(this.loanData.sIsExemptFromAtr, this.loanData.sQMStatusT);
            detail.QualifiedMortgageType = ToQualifiedMortgageEnum(this.loanData.sQMIsEligibleByLoanPurchaseAgency);

            if (detail.QualifiedMortgageType != null && detail.QualifiedMortgageType.enumValue == QualifiedMortgageBase.TemporaryAgencyGSE)
            {
                detail.QualifiedMortgageTemporaryGSEType = ToQualifiedMortgageTemporaryGSEEnum(this.loanData.sQMLoanPurchaseAgency);
            }

            return detail;
        }

        /// <summary>
        /// Creates a REAL_ESTATE_AGENT container based on the given agent type.
        /// </summary>
        /// <param name="agentType">The agent type.</param>
        /// <returns>A REAL_ESTATE_AGENT container.</returns>
        private REAL_ESTATE_AGENT CreateRealEstateAgent(E_AgentRoleT agentType)
        {
            var agent = new REAL_ESTATE_AGENT();
            agent.RealEstateAgentType = ToRealEstateAgentEnum(agentType);

            if (agent.RealEstateAgentType == null)
            {
                return null;
            }

            if (agent.RealEstateAgentType.enumValue == RealEstateAgentBase.Other)
            {
                agent.RealEstateAgentTypeOtherDescription = ToMismoString(GetXmlEnumName(agentType));
            }

            return agent;
        }

        /// <summary>
        /// Creates a REAL_ESTATE_AGENT container based on the given agent type.
        /// </summary>
        /// <param name="realEstateAgentType">The real estate agent type.</param>
        /// <returns>A REAL_ESTATE_AGENT container.</returns>
        private REAL_ESTATE_AGENT CreateRealEstateAgent(RealEstateAgentBase realEstateAgentType)
        {
            var agent = new REAL_ESTATE_AGENT();
            agent.RealEstateAgentType = ToRealEstateAgentEnum(realEstateAgentType);

            return agent;
        }

        /// <summary>
        /// Creates a container that holds information on a recording endorsement.
        /// </summary>
        /// <param name="sequence">The sequence number of the recording endorsement among the list of endorsements.</param>
        /// <returns>A RECORDING_ENDORSEMENT container.</returns>
        private RECORDING_ENDORSEMENT CreateRecordingEndorsement(int sequence)
        {
            var recordingEndorsement = new RECORDING_ENDORSEMENT();
            recordingEndorsement.RecordingEndorsementInformation = this.CreateRecordingEndorsementInformation();
            recordingEndorsement.SequenceNumber = sequence;

            return recordingEndorsement;
        }

        /// <summary>
        /// Creates a container that holds all instances of RECORDING_ENDORSEMENT.
        /// </summary>
        /// <returns>A RECORDING_ENDORSEMENTS container.</returns>
        private RECORDING_ENDORSEMENTS CreateRecordingEndorsements()
        {
            var recordingEndorsements = new RECORDING_ENDORSEMENTS();
            recordingEndorsements.RecordingEndorsement.Add(this.CreateRecordingEndorsement(1));

            return recordingEndorsements;
        }

        /// <summary>
        /// Creates a container holding data on a recording endorsement.
        /// </summary>
        /// <returns>A RECORDING_ENDORSEMENT_INFORMATION object.</returns>
        private RECORDING_ENDORSEMENT_INFORMATION CreateRecordingEndorsementInformation()
        {
            var recordingEndorsementInformation = new RECORDING_ENDORSEMENT_INFORMATION();
            recordingEndorsementInformation.CountyOfRecordationName = ToMismoString(this.loanData.sSpCounty);
            recordingEndorsementInformation.FirstPageNumberValue = ToMismoValue(this.loanData.sRecordingPageNum);
            recordingEndorsementInformation.InstrumentNumberIdentifier = ToMismoIdentifier(this.loanData.sRecordingInstrumentNum);
            recordingEndorsementInformation.RecordedDatetime = ToMismoDatetime(this.loanData.sRecordedD_rep_WithTime, false);
            recordingEndorsementInformation.RecordingEndorsementIdentifier = ToMismoIdentifier(this.loanData.sRecordingBookNum);
            recordingEndorsementInformation.StateOfRecordationName = ToMismoString(this.statesAndTerritories.GetStateName(this.loanData.sSpState));
            recordingEndorsementInformation.VolumeIdentifier = ToMismoIdentifier(this.loanData.sRecordingVolumeNum);

            return recordingEndorsementInformation;
        }

        /// <summary>
        /// Creates a container holding refinance information.
        /// </summary>
        /// <returns>A REFINANCE container, or null if the loan is not a refinance.</returns>
        private REFINANCE CreateRefinance()
        {
            if (!this.loanData.sIsRefinancing)
            {
                return null;
            }

            var refinance = new REFINANCE();
            refinance.RefinanceCashOutAmount = ToMismoAmount(this.loanData.sProdCashoutAmt_rep);
            refinance.SecondaryFinancingRefinanceIndicator = ToMismoIndicator(this.loanData.sPayingOffSubordinate);
            refinance.RefinanceCashOutDeterminationType = ToRefinanceCashOutDeterminationEnum(this.loanData.sRefPurpose, this.loanData.sLPurposeT);
            refinance.RefinancePrimaryPurposeType = ToRefinancePrimaryPurposeEnum(this.loanData.sRefPurpose);

            if (refinance.RefinancePrimaryPurposeType != null && refinance.RefinancePrimaryPurposeType.enumValue == RefinancePrimaryPurposeBase.Other)
            {
                refinance.RefinancePrimaryPurposeTypeOtherDescription = ToMismoString(this.loanData.sRefPurpose);
            }

            return refinance;
        }

        /// <summary>
        /// Creates a REHABILITATION container with information pertaining to FHA 203k loans.
        /// </summary>
        /// <returns>A REHABILITATION container with information pertaining to FHA 203k loans.</returns>
        private REHABILITATION CreateRehabilitation()
        {
            var rehabilitation = new REHABILITATION();

            if (!this.loanData.sIsRenovationLoan && (this.loanData.sLT != E_sLT.FHA || (this.loanData.sFHASpAfterImprovedVal == 0.00m && this.loanData.sFHA203kRepairCostReserveR == 0.00m)))
            {
                return null;
            }
            else if (this.loanData.sIsRenovationLoan)
            {
                rehabilitation.RehabilitationContingencyPercent = ToMismoPercent(this.loanData.sRehabContingencyPcFinalLAmt_rep);
                rehabilitation.RehabilitationEstimatedPropertyValueAmount = ToMismoAmount(this.loanData.sApprVal_rep);
                rehabilitation.RehabilitationLoanFundsAmount = ToMismoAmount(this.loanData.sTotalRenovationCosts_rep);
            }
            else
            {
                rehabilitation.RehabilitationContingencyPercent = ToMismoPercent(this.loanData.sFHA203kRepairCostReserveR_rep);
                rehabilitation.RehabilitationEstimatedPropertyValueAmount = ToMismoAmount(this.loanData.sFHASpAfterImprovedVal_rep);
            }

            return rehabilitation;
        }

        /// <summary>
        /// Creates a container to hold all instances of RELATIONSHIP.
        /// </summary>
        /// <returns>A RELATIONSHIPS container.</returns>
        private RELATIONSHIPS CreateRelationships()
        {
            RELATIONSHIPS relationships = new RELATIONSHIPS();

            foreach (RELATIONSHIP relation in this.allRelationships)
            {
                if (!string.IsNullOrWhiteSpace(relation.from) && !string.IsNullOrWhiteSpace(relation.to))
                {
                    relationships.Relationship.Add(relation);
                }
            }

            return relationships;
        }

        /// <summary>
        /// Creates a relationship container that describes the relationship between two objects.
        /// </summary>
        /// <param name="arcroleVerbPhrase">Describes the relationship, e.g. IsAssociatedWith, SharesSufficientAssetsAndLiabilitiesWith.</param>
        /// <param name="fromName">The name of the source object, e.g. ASSET.</param>
        /// <param name="toname">The name of the target object, e.g. ROLE as in borrower role.</param>
        /// <param name="fromLabel">Identifies the source object for the relationship, e.g. asset x.</param>
        /// <param name="tolabel">Identifies the target object for the relationship, e.g. borrower y.</param>
        /// <returns>A RELATIONSHIP container.</returns>
        private RELATIONSHIP CreateRelationship(ArcroleVerbPhrase arcroleVerbPhrase, string fromName, string toname, string fromLabel, string tolabel)
        {
            RELATIONSHIP relationship = new RELATIONSHIP();

            relationship.arcrole = string.Format("{0}{1}_{2}_{3}", Mismo3Constants.XlinkArcroleURN, fromName, GetXmlEnumName(arcroleVerbPhrase), toname);
            relationship.from = fromLabel;
            relationship.to = tolabel;
            relationship.SequenceNumber = this.counters.Relationships;

            return relationship;
        }

        /// <summary>
        /// Adds a relation between the given element and the parent loan.
        /// </summary>
        /// <param name="name">The name of the element.</param>
        /// <param name="label">The unique label for the element.</param>
        /// <param name="verbPhrase">The verb phrase to link the object and loan.</param>
        /// <param name="isDataPoint">True if the element is a datapoint, false if it is a container.</param>
        /// <returns>A RELATIONSHIP container.</returns>
        private RELATIONSHIP CreateRelationship_ToLoan(string name, string label, ArcroleVerbPhrase verbPhrase, bool isDataPoint = false)
        {
            string loanContainerName = "LOAN";
            string loanXlinkLabel = string.Empty;
            this.loanLabel.TryGetValue(this.loanData.sLId.ToString("D"), out loanXlinkLabel);

            if (string.IsNullOrEmpty(loanXlinkLabel))
            {
                return null;
            }

            // The container that has alphabetical precedence is listed first.
            // If the given element is a data point, however, it is always listed last.
            if (isDataPoint || loanContainerName.CompareTo(name) < 0)
            {
                return this.CreateRelationship(verbPhrase, "LOAN", name, loanXlinkLabel, label);
            }
            else
            {
                return this.CreateRelationship(verbPhrase, name, "LOAN", label, loanXlinkLabel);
            }
        }

        /// <summary>
        /// Creates a container with information pertaining to the person placing the order, or making the request of the vendor to receive the MISMO message.
        /// </summary>
        /// <returns>A REQUESTING_PARTY container.</returns>
        private REQUESTING_PARTY CreateRequestingParty()
        {
            var requestor = new REQUESTING_PARTY();
            requestor.InternalAccountIdentifier = ToMismoIdentifier(this.vendorAccountID, isSensitiveData: true);

            if (this.user != null)
            {
                requestor.RequestedByName = ToMismoString(this.user.FullName);
            }

            return requestor;
        }

        /// <summary>
        /// Creates a container representing a borrower's current residence.
        /// </summary>
        /// <param name="address">The street address of the residence.</param>
        /// <param name="city">The city in which the residence is located.</param>
        /// <param name="state">The state in which the residence is located.</param>
        /// <param name="zip">The ZIP code for the residence.</param>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <param name="sequence">The sequence number of the residence among the list of residences.</param>
        /// <returns>A RESIDENCE container.</returns>
        private RESIDENCE CreateResidence(string address, string city, string state, string zip, string years, string months, E_aBAddrT residencyType, BorrowerResidencyBase currentResidence, int sequence)
        {
            var residence = new RESIDENCE();
            residence.SequenceNumber = sequence;
            residence.ResidenceDetail = this.CreateResidenceDetail(years, months, residencyType, currentResidence);

            var addressType = currentResidence.Equals(BorrowerResidencyBase.Current) ? AddressBase.Current : AddressBase.Prior;
            residence.Address = CreateAddress(
                address, 
                city, 
                state, 
                zip, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(state),
                addressType, 
                1);

            return residence;
        }

        /// <summary>
        /// Creates a container representing a borrower's mailing residence. Serves as a container for the mailing address in this case.
        /// </summary>
        /// <param name="address">The street address of the residence.</param>
        /// <param name="city">The city in which the residence is located.</param>
        /// <param name="state">The state in which the residence is located.</param>
        /// <param name="zip">The ZIP code for the residence.</param>
        /// <param name="sequence">The sequence number of the residence among the list of residences.</param>
        /// <returns>A RESIDENCE container.</returns>
        private RESIDENCE CreateResidence_Mailing(string address, string city, string state, string zip, int sequence)
        {
            var residence = new RESIDENCE();
            residence.SequenceNumber = sequence;
            
            residence.Address = CreateAddress(
                address,
                city,
                state,
                zip,
                string.Empty,
                string.Empty,
                this.statesAndTerritories.GetStateName(state),
                AddressBase.Mailing,
                1);

            return residence;
        }

        /// <summary>
        /// Creates a container representing a borrower's current residence.
        /// </summary>
        /// <param name="address">The street address of the residence.</param>
        /// <param name="city">The city in which the residence is located.</param>
        /// <param name="state">The state in which the residence is located.</param>
        /// <param name="zip">The ZIP code for the residence.</param>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <param name="sequence">The sequence number of the residence among the list of residences.</param>
        /// <returns>A RESIDENCE container.</returns>
        private RESIDENCE CreateResidence(string address, string city, string state, string zip, string years, string months, E_aBPrev1AddrT residencyType, BorrowerResidencyBase currentResidence, int sequence)
        {
            var residence = new RESIDENCE();
            residence.SequenceNumber = sequence;
            residence.ResidenceDetail = this.CreateResidenceDetail(years, months, residencyType, currentResidence);

            var addressType = currentResidence.Equals(BorrowerResidencyBase.Current) ? AddressBase.Current : AddressBase.Prior;
            residence.Address = CreateAddress(
                address, 
                city, 
                state, 
                zip, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(state),
                addressType, 
                1);

            return residence;
        }

        /// <summary>
        /// Creates a container representing a borrower's current residence.
        /// </summary>
        /// <param name="address">The street address of the residence.</param>
        /// <param name="city">The city in which the residence is located.</param>
        /// <param name="state">The state in which the residence is located.</param>
        /// <param name="zip">The ZIP code for the residence.</param>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <param name="sequence">The sequence number of the residence among the list of residences.</param>
        /// <returns>A RESIDENCE container.</returns>
        private RESIDENCE CreateResidence(string address, string city, string state, string zip, string years, string months, E_aBPrev2AddrT residencyType, BorrowerResidencyBase currentResidence, int sequence)
        {
            var residence = new RESIDENCE();
            residence.SequenceNumber = sequence;
            residence.ResidenceDetail = this.CreateResidenceDetail(years, months, residencyType, currentResidence);

            var addressType = currentResidence.Equals(BorrowerResidencyBase.Current) ? AddressBase.Current : AddressBase.Prior;
            residence.Address = CreateAddress(
                address, 
                city, 
                state, 
                zip, 
                string.Empty, 
                string.Empty,
                this.statesAndTerritories.GetStateName(state),
                addressType, 
                1);

            return residence;
        }

        /// <summary>
        /// Creates a container holding the borrower's current and previous two residences.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A RESIDENCES container.</returns>
        private RESIDENCES CreateResidences(CAppData appData)
        {
            var residences = new RESIDENCES();

            residences.Residence.Add(this.CreateResidence(
                                                          appData.aAddr,
                                                          appData.aCity, 
                                                          appData.aState,
                                                          appData.aZip,
                                                          appData.aAddrWholeYears_rep,
                                                          appData.aAddrRemainderMonths_rep,
                                                          appData.aAddrT, 
                                                          BorrowerResidencyBase.Current, 
                                                          1));

            residences.Residence.Add(this.CreateResidence(
                                                          appData.aPrev1Addr,
                                                          appData.aPrev1City,
                                                          appData.aPrev1State,
                                                          appData.aPrev1Zip,
                                                          appData.aPrev1AddrWholeYears_rep,
                                                          appData.aPrev1AddrRemainderMonths_rep,
                                                          appData.aPrev1AddrT, 
                                                          BorrowerResidencyBase.Prior, 
                                                          residences.Residence.Count(r => r != null) + 1));

            residences.Residence.Add(this.CreateResidence(
                                                          appData.aPrev2Addr,
                                                          appData.aPrev2City,
                                                          appData.aPrev2State,
                                                          appData.aPrev2Zip,
                                                          appData.aPrev2AddrWholeYears_rep,
                                                          appData.aPrev2AddrRemainderMonths_rep,
                                                          appData.aPrev2AddrT, 
                                                          BorrowerResidencyBase.Prior, 
                                                          residences.Residence.Count(r => r != null) + 1));

            residences.Residence.Add(this.CreateResidence_Mailing(
                                                            appData.aAddrMail,
                                                            appData.aCityMail,
                                                            appData.aStateMail,
                                                            appData.aZipMail,
                                                            residences.Residence.Count(r => r != null) + 1));

            return residences;
        }

        /// <summary>
        /// Creates a container with detail about a borrower's current residence.
        /// </summary>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <returns>A RESIDENCE_DETAIL container.</returns>
        private RESIDENCE_DETAIL CreateResidenceDetail(string years, string months, E_aBAddrT residencyType, BorrowerResidencyBase currentResidence)
        {
            var residenceDetail = new RESIDENCE_DETAIL();
            residenceDetail.BorrowerResidencyDurationYearsCount = ToMismoCount(years, this.loanData.m_convertLos);
            residenceDetail.BorrowerResidencyDurationMonthsCount = ToMismoCount(months, this.loanData.m_convertLos);

            residenceDetail.BorrowerResidencyBasisType = ToBorrowerResidencyBasisEnum(residencyType);
            residenceDetail.BorrowerResidencyType = ToBorrowerResidencyEnum(currentResidence);

            return residenceDetail;
        }

        /// <summary>
        /// Creates a container with detail about a borrower's past residence.
        /// </summary>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <returns>A RESIDENCE_DETAIL container.</returns>
        private RESIDENCE_DETAIL CreateResidenceDetail(string years, string months, E_aBPrev1AddrT residencyType, BorrowerResidencyBase currentResidence)
        {
            var residenceDetail = new RESIDENCE_DETAIL();
            residenceDetail.BorrowerResidencyDurationYearsCount = ToMismoCount(years, this.loanData.m_convertLos);
            residenceDetail.BorrowerResidencyDurationMonthsCount = ToMismoCount(months, this.loanData.m_convertLos);

            residenceDetail.BorrowerResidencyBasisType = ToBorrowerResidencyBasisEnum(residencyType);
            residenceDetail.BorrowerResidencyType = ToBorrowerResidencyEnum(currentResidence);

            return residenceDetail;
        }

        /// <summary>
        /// Creates a container with detail about a borrower's past residence.
        /// </summary>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <returns>A RESIDENCE_DETAIL container.</returns>
        private RESIDENCE_DETAIL CreateResidenceDetail(string years, string months, E_aBPrev2AddrT residencyType, BorrowerResidencyBase currentResidence)
        {
            var residenceDetail = new RESIDENCE_DETAIL();
            residenceDetail.BorrowerResidencyDurationYearsCount = ToMismoCount(years, this.loanData.m_convertLos);
            residenceDetail.BorrowerResidencyDurationMonthsCount = ToMismoCount(months, this.loanData.m_convertLos);

            residenceDetail.BorrowerResidencyBasisType = ToBorrowerResidencyBasisEnum(residencyType);
            residenceDetail.BorrowerResidencyType = ToBorrowerResidencyEnum(currentResidence);

            return residenceDetail;
        }

        /// <summary>
        /// Creates a RETURN_TO with the preferred responses to the MISMO message.
        /// </summary>
        /// <returns>A RETURN_TO container.</returns>
        private RETURN_TO CreateReturnTo()
        {
            var returnTo = new RETURN_TO();
            returnTo.PreferredResponses = this.CreatePreferredResponses();

            return returnTo;
        }

        /// <summary>
        /// Creates ROLE container with agent information populated by agent role type.
        /// </summary>
        /// <param name="agent">The agent object.</param>
        /// <param name="entityType">Indicates whether the agent is an individual or legal entity.</param>
        /// <param name="sequenceNumber">The sequence number for the ROLE container.</param>
        /// <returns>Returns ROLE container with agent information populated by agent role type.</returns>
        private ROLE CreateRole(CAgentFields agent, EntityType entityType, int sequenceNumber)
        {
            var role = new ROLE();
            role.SequenceNumber = sequenceNumber;
            E_AgentRoleT agentRole = agent.AgentRoleT;

            if (entityType == EntityType.Individual)
            {
                role.Licenses = this.CreateLicenses(agent.LicenseNumOfAgent, agent.LoanOriginatorIdentifier);
            }
            else if (entityType == EntityType.LegalEntity && agentRole == E_AgentRoleT.Lender)
            {
                role.Licenses = this.CreateLicenses(agent.LicenseNumOfCompany, agent.CompanyLoanOriginatorIdentifier, this.loanData.sVALenderIdCode);
            }
            else
            {
                role.Licenses = this.CreateLicenses(agent.LicenseNumOfCompany, agent.CompanyLoanOriginatorIdentifier);
            }

            string otherDescription;
            var partyRoleType = GetPartyRoleBaseValue(agent.AgentRoleT, agent.OtherAgentRoleTDesc, entityType, this.loanData.sGfeIsTPOTransaction, this.loanData.sBranchChannelT, out otherDescription);
            role.RoleDetail = this.CreateRoleDetail(partyRoleType, otherDescription);

            if (partyRoleType == PartyRoleBase.ClosingAgent)
            {
                role.ClosingAgent = this.CreateClosingAgent(ClosingAgentBase.EscrowCompany, string.Empty);
            }
            else if (partyRoleType == PartyRoleBase.TitleCompany)
            {
                role.ClosingAgent = this.CreateClosingAgent(ClosingAgentBase.TitleCompany, string.Empty);
            }
            else if (partyRoleType == PartyRoleBase.RealEstateAgent)
            {
                role.RealEstateAgent = this.CreateRealEstateAgent(agent.AgentRoleT);
            }
            else if (partyRoleType == PartyRoleBase.Trustee)
            {
                role.Trustee = this.CreateTrustee(entityType);
            }

            string roleString = agent.AgentRoleT != E_AgentRoleT.Other ? agent.AgentRoleT.ToString() : agent.OtherAgentRoleTDesc;
            role.label = this.GeneratePartyLabel(roleString, entityType);

            return role;
        }

        /// <summary>
        /// Creates a basic role with a role type.
        /// </summary>
        /// <param name="roleType">The role type.</param>
        /// <param name="otherDescription">The description if the role type is Other.</param>
        /// <param name="sequenceNumber">The sequence number for the ROLE.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole(PartyRoleBase roleType, string otherDescription = "", int sequenceNumber = 1)
        {
            var role = new ROLE();
            role.RoleDetail = this.CreateRoleDetail(roleType, otherDescription);
            role.SequenceNumber = sequenceNumber;
            role.label = this.GeneratePartyLabel(roleType, EntityType.LegalEntity);

            return role;
        }

        /// <summary>
        /// Creates an extension container holding information on a service offered by a provider.
        /// </summary>
        /// <param name="canShop">Indicates whether the service can be shopped for.</param>
        /// <param name="description">A description of the service.</param>
        /// <param name="amount">The dollar amount of the service.</param>
        /// <returns>An LQB_PROVIDED_SERVICE container.</returns>
        private LQB_PROVIDED_SERVICE CreateLQBProvidedService(bool canShop, string description, string amount)
        {
            var providedService = new LQB_PROVIDED_SERVICE();
            providedService.ProvidedServiceCanShopForIndicator = ToMismoIndicator(canShop);
            providedService.ProvidedServiceDescription = ToMismoString(description);
            providedService.ProvidedServiceEstimatedCostAmount = ToMismoAmount(amount);

            return providedService;
        }

        /// <summary>
        /// Creates a role for a settlement service provider.
        /// </summary>
        /// <param name="fees">Fees associated with the service provider.</param>
        /// <param name="sequenceNumber">The sequence number for the ROLE element.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_SSP(List<Tuple<string, decimal>> fees, int sequenceNumber)
        {
            var role = new ROLE();
            role.SequenceNumber = sequenceNumber;

            var roleType = PartyRoleBase.ServiceProvider;
            role.RoleDetail = this.CreateRoleDetail(roleType, string.Empty);
            role.label = this.GeneratePartyLabel(roleType, EntityType.LegalEntity);

            if (fees != null && fees.Count() > 0)
            {
                role.Extension = this.CreateRoleExtension(fees);
            }

            return role;
        }

        /// <summary>
        /// An extension for the ROLE container.
        /// </summary>
        /// <param name="associatedFees">A list of fee descriptions/amounts associated with a provider.</param>
        /// <returns>A ROLE_EXTENSION container.</returns>
        private ROLE_EXTENSION CreateRoleExtension(List<Tuple<string, decimal>> associatedFees)
        {
            var extension = new ROLE_EXTENSION();
            extension.Other = this.CreateLQBRoleExtension(associatedFees);

            return extension;
        }

        /// <summary>
        /// A container holding extension data for the ROLE container.
        /// </summary>
        /// <param name="associatedFees">A list of fee descriptions/amounts associated with a provider.</param>
        /// <returns>An LQB_ROLE_EXTENSION container.</returns>
        private LQB_ROLE_EXTENSION CreateLQBRoleExtension(List<Tuple<string, decimal>> associatedFees)
        {
            var extension = new LQB_ROLE_EXTENSION();
            extension.ServiceProvider = this.CreateLQBServiceProvider(associatedFees);

            return extension;
        }

        /// <summary>
        /// An extension container holding information on a service provider.
        /// </summary>
        /// <param name="associatedFees">A list of fee descriptions/amounts associated with a provider.</param>
        /// <returns>An LQB_SERVICE_PROVIDER container.</returns>
        private LQB_SERVICE_PROVIDER CreateLQBServiceProvider(List<Tuple<string, decimal>> associatedFees)
        {
            var serviceProvider = new LQB_SERVICE_PROVIDER();
            serviceProvider.ProvidedServices = this.CreateLQBProvidedServices(associatedFees);

            return serviceProvider;
        }

        /// <summary>
        /// An extension container holding a collection of services offered by a service provider.
        /// </summary>
        /// <param name="associatedFees">A list of fee descriptions/amounts associated with a provider.</param>
        /// <returns>An LQB_PROVIDED_SERVICES.</returns>
        private LQB_PROVIDED_SERVICES CreateLQBProvidedServices(List<Tuple<string, decimal>> associatedFees)
        {
            var providedServices = new LQB_PROVIDED_SERVICES();

            foreach (var fee in associatedFees)
            {
                providedServices.ProvidedService.Add(this.CreateLQBProvidedService(
                    canShop: true, 
                    description: fee.Item1, 
                    amount: this.loanData.m_convertLos.ToDecimalString(fee.Item2, FormatDirection.ToRep)));
            }

            return providedServices;
        }

        /// <summary>
        /// Creates a borrower role.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <param name="sequenceNumber">The sequence number for the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_Borrower(CAppData appData, int sequenceNumber)
        {
            var role = new ROLE();

            role.RoleDetail = this.CreateRoleDetail(GetPartyRoleBaseValue(appData.aTypeT), string.Empty);
            role.Borrower = this.CreateBorrower(appData);
            role.label = appData.aMismoId;
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a borrower role for a Title-Only Borrower.
        /// </summary>
        /// <param name="borrower">The Title-Only Borrower.</param>
        /// <param name="label">The role label to use.</param>
        /// <param name="sequenceNumber">The sequence number of the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_TitleOnly(TitleBorrower borrower, string label, int sequenceNumber)
        {
            var role = new ROLE();
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.TitleHolder, string.Empty);
            role.Borrower = new BORROWER();
            role.Borrower.BorrowerDetail = new BORROWER_DETAIL();
            role.Borrower.BorrowerDetail.BorrowerRelationshipTitleType = ToBorrowerRelationshipTitleEnum(borrower.RelationshipTitleT);
            role.Borrower.BorrowerDetail.BorrowerBirthDate = ToMismoDate(this.loanData.m_convertLos.ToDateTimeString(borrower.Dob), isSensitiveData: true);

            role.PartyRoleIdentifiers = new PARTY_ROLE_IDENTIFIERS();
            var partyRoleIdentifier = new PARTY_ROLE_IDENTIFIER();
            partyRoleIdentifier.SequenceNumber = 1;
            partyRoleIdentifier.PartyRoleIdentifier = ToMismoIdentifier(borrower.SSNForExport, isSensitiveData: true);
            role.PartyRoleIdentifiers.PartyRoleIdentifier.Add(partyRoleIdentifier);
            
            if (role.Borrower.BorrowerDetail.BorrowerRelationshipTitleType != null && role.Borrower.BorrowerDetail.BorrowerRelationshipTitleType.enumValue == BorrowerRelationshipTitleBase.Other)
            {
                role.Borrower.BorrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = ToMismoString(CAppBase.aRelationshipTitleT_Map_rep(borrower.RelationshipTitleT));
            }
            else
            {
                role.Borrower.BorrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = ToMismoString(borrower.RelationshipTitleTOtherDesc);
            }

            role.SequenceNumber = sequenceNumber;
            role.label = this.GeneratePartyLabel(label, EntityType.Individual);

            return role;
        }

        /// <summary>
        /// Creates a role container representing the loan originator role.
        /// </summary>
        /// <param name="preparer">The 1003 preparer information.</param>
        /// <param name="entityType">The entity type for the associated party, i.e. individual or legal entity.</param>
        /// <param name="sequence">The sequence number of the role among the list of roles.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_LoanOriginator(IPreparerFields preparer, EntityType entityType, int sequence)
        {
            var role = new ROLE();
            role.LoanOriginator = new LOAN_ORIGINATOR();
            role.LoanOriginator.LoanOriginatorType = ToLoanOriginatorEnum(this.loanData.sBranchChannelT);
            role.SequenceNumber = sequence;

            string licenseNumber = entityType == EntityType.Individual ? preparer.LicenseNumOfAgent : preparer.LicenseNumOfCompany;
            string nmlsLicense = entityType == EntityType.Individual ? preparer.LoanOriginatorIdentifier : preparer.CompanyLoanOriginatorIdentifier;
            role.Licenses = this.CreateLicenses(licenseNumber, nmlsLicense);

            PartyRoleBase roleBase = entityType == EntityType.Individual ? PartyRoleBase.LoanOriginator : PartyRoleBase.LoanOriginationCompany;
            role.RoleDetail = new ROLE_DETAIL();
            role.RoleDetail.PartyRoleType = ToPartyRoleEnum(roleBase);

            role.label = this.GeneratePartyLabel(roleBase, entityType);

            return role;
        }

        /// <summary>
        /// Creates a property owner role.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <param name="sequenceNumber">The sequence number for the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_PropertyOwner(CAppData appData, int sequenceNumber)
        {
            var role = new ROLE();
            PartyRoleBase roleBase = PartyRoleBase.PropertyOwner;

            role.RoleDetail = this.CreateRoleDetail(roleBase, string.Empty);
            role.PropertyOwner = this.CreatePropertyOwner(appData);
            role.SequenceNumber = sequenceNumber;

            role.label = this.GeneratePartyLabel(roleBase, EntityType.Individual);

            return role;
        }

        /// <summary>
        /// Creates a role container representing the requesting party.
        /// </summary>
        /// <param name="sequence">The sequence number of the role among the list of roles.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_RequestingParty(int sequence)
        {
            var role = new ROLE();

            role.RequestingParty = this.CreateRequestingParty();
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.RequestingParty, string.Empty);
            role.label = this.GeneratePartyLabel(PartyRoleBase.RequestingParty, EntityType.LegalEntity);
            role.SequenceNumber = sequence;
            
            return role;
        }

        /// <summary>
        /// Creates a role container representing the return-to role.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number for the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_ReturnTo(int sequenceNumber)
        {
            var role = new ROLE();

            role.ReturnTo = this.CreateReturnTo();
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.ReturnTo, string.Empty);
            role.label = this.GeneratePartyLabel(PartyRoleBase.ReturnTo, EntityType.LegalEntity);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a role container representing the submitting party role.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number for the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_SubmittingParty(int sequenceNumber)
        {
            var role = new ROLE();

            role.SubmittingParty = this.CreateSubmittingParty();
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.SubmittingParty, string.Empty);
            role.label = this.GeneratePartyLabel(PartyRoleBase.SubmittingParty, EntityType.LegalEntity);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a container holding information on a trustee.
        /// </summary>
        /// <param name="trustee">The trustee details.</param>
        /// <param name="label">The label for this role.</param>
        /// <param name="sequenceNumber">The sequence number for this role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_Trustee(TrustCollection.Trustee trustee, string label, int sequenceNumber)
        {
            var role = new ROLE();
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.Trustee, string.Empty);
            role.Trustee = this.CreateTrustee(trustee);

            role.label = this.GeneratePartyLabel(label, EntityType.Individual);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a container holding information on a trust settlor.
        /// </summary>
        /// <param name="label">The label for this role.</param>
        /// <param name="sequenceNumber">The sequence number for this role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_Settlor(string label, int sequenceNumber)
        {
            var role = new ROLE();
            role.RoleDetail = this.CreateRoleDetail(PartyRoleBase.Settlor, string.Empty);

            role.label = this.GeneratePartyLabel(label, EntityType.Individual);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a role for a contact listed on the Loan Estimate or Closing Disclosure.
        /// </summary>
        /// <param name="roleType">The contact's role type.</param>
        /// <param name="entityType">Indicates whether the contact is an individual or legal entity.</param>
        /// <param name="licenseId">The contact's license number.</param>
        /// <param name="nmlsId">The contact's NMLS number.</param>
        /// <param name="sequenceNumber">The sequence number for the ROLE element.</param>
        /// <param name="realEstateAgentType">The Real Estate Agent Type. Can be omitted for non-Real Estate Agents.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_DisclosureContact(PartyRoleBase roleType, EntityType entityType, string licenseId, string nmlsId, int sequenceNumber, RealEstateAgentBase realEstateAgentType = RealEstateAgentBase.Blank)
        {
            var role = new ROLE();
            role.RoleDetail = this.CreateRoleDetail(roleType, string.Empty, includedInDisclosure: true);
            role.SequenceNumber = sequenceNumber;

            if (!string.IsNullOrEmpty(licenseId) || !string.IsNullOrEmpty(nmlsId))
            {
                role.Licenses = this.CreateLicenses(licenseId, nmlsId);
            }

            if (realEstateAgentType != RealEstateAgentBase.Blank)
            {
                role.RealEstateAgent = this.CreateRealEstateAgent(realEstateAgentType);
            }

            if (roleType == PartyRoleBase.ClosingAgent)
            {
                role.ClosingAgent = this.CreateClosingAgent(ClosingAgentBase.Other, "SettlementAgent");
            }

            if (this.vendor == E_DocumentVendor.DocMagic && roleType == PartyRoleBase.Lender && this.loanData.sDocMagicAlternateLenderId != Guid.Empty)
            {
                role.PartyRoleIdentifiers = this.CreatePartyRoleIdentifiers_Lender();
            }

            role.label = this.GeneratePartyLabel(roleType, entityType);

            return role;
        }

        /// <summary>
        /// Creates a ROLE container representing a seller.
        /// </summary>
        /// <param name="entityType">Indicates whether the seller is an individual or legal entity.</param>
        /// <param name="includedInDisclosure">Indicates whether the seller is included in the disclosure.</param>
        /// <param name="sequenceNumber">The sequence number of the container.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_Seller(EntityType entityType, bool includedInDisclosure, int sequenceNumber)
        {
            var role = new ROLE();

            var roleType = PartyRoleBase.PropertySeller;
            role.RoleDetail = this.CreateRoleDetail(roleType, string.Empty, includedInDisclosure);
            role.label = this.GeneratePartyLabel(roleType, entityType);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates ROLE Detail container with specified type and/or other description.
        /// </summary>
        /// <param name="partyRoleType">The specified party role type.</param>
        /// <param name="partyRoleTypeOtherDescription">The specified party role other type description. Use only if partyRoleType is other.</param>
        /// <param name="includedInDisclosure">Indicates whether this contact is included on the LE/CD. Defaults to false.</param>
        /// <returns>Returns ROLE Detail container with specified type and/or other description.</returns>
        private ROLE_DETAIL CreateRoleDetail(PartyRoleBase partyRoleType, string partyRoleTypeOtherDescription, bool includedInDisclosure = false)
        {
            ROLE_DETAIL roleDetail = new ROLE_DETAIL();
            roleDetail.PartyRoleType = ToPartyRoleEnum(partyRoleType);

            if (partyRoleType == PartyRoleBase.Other)
            {
                roleDetail.PartyRoleTypeOtherDescription = ToMismoString(partyRoleTypeOtherDescription);
            }

            roleDetail.Extension = this.CreateRoleDetailExtension(includedInDisclosure);

            return roleDetail;
        }

        /// <summary>
        /// Creates a container with extension data for ROLE_DETAIL.
        /// </summary>
        /// <param name="includedInDisclosure">Indicates whether this contact is included on the LE/CD. Defaults to false.</param>
        /// <returns>A ROLE_DETAIL_EXTENSION container.</returns>
        private ROLE_DETAIL_EXTENSION CreateRoleDetailExtension(bool includedInDisclosure)
        {
            var extension = new ROLE_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBRoleDetailExtension(includedInDisclosure);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension element for ROLE_DETAIL.
        /// </summary>
        /// <param name="includedInDisclosure">Indicates whether this contact is included on the LE/CD. Defaults to false.</param>
        /// <returns>An LQB_ROLE_DETAIL_EXTENSION container.</returns>
        private LQB_ROLE_DETAIL_EXTENSION CreateLQBRoleDetailExtension(bool includedInDisclosure)
        {
            var extension = new LQB_ROLE_DETAIL_EXTENSION();
            extension.IncludedInDisclosure = ToMismoIndicator(includedInDisclosure);

            return extension;
        }

        /// <summary>
        /// Creates a ROLES element holding a single ROLE representing a contact on the Loan Estimate or Closing Disclosure.
        /// </summary>
        /// <param name="roleType">The contact's role type.</param>
        /// <param name="entityType">Indicates whether the contact is an individual or legal entity.</param>
        /// <param name="licenseId">The contact's license number.</param>
        /// <param name="nmlsId">The contact's NMLS number.</param>
        /// <param name="realEstateAgentType">The Real Estate Agent Type. Can be omitted for non-Real Estate Agents.</param>
        /// <returns>A ROLES container.</returns>
        private ROLES CreateRoles_DisclosureContact(PartyRoleBase roleType, EntityType entityType, string licenseId, string nmlsId, RealEstateAgentBase realEstateAgentType = RealEstateAgentBase.Blank)
        {
            var roles = new ROLES();
            roles.Role.Add(this.CreateRole_DisclosureContact(roleType, entityType, licenseId, nmlsId, 1, realEstateAgentType));

            return roles;
        }

        /// <summary>
        /// Creates a container holding roles for a settlement service provider.
        /// </summary>
        /// <param name="provider">The service provider.</param>
        /// <param name="fees">Fees associated with the service provider.</param>
        /// <returns>A ROLES container.</returns>
        private ROLES CreateRoles_SSP(SettlementServiceProvider provider, List<Tuple<string, decimal>> fees)
        {
            var roles = new ROLES();

            string otherDescription = string.Empty;
            var partyRoleType = GetPartyRoleBaseValue(provider.ServiceT, provider.ServiceTDesc, EntityType.LegalEntity, this.loanData.sGfeIsTPOTransaction, this.loanData.sBranchChannelT, out otherDescription);

            roles.Role.Add(this.CreateRole(partyRoleType, otherDescription));
            roles.Role.Add(this.CreateRole_SSP(fees, sequenceNumber: 2));

            return roles;
        }

        /// <summary>
        /// Creates a ROLES element holding a single ROLE representing a seller.
        /// </summary>
        /// <param name="entityType"> Indicates whether the contact is an individual or legal entity.</param>
        /// <param name="includedInDisclosure">Indicates whether the seller is included in the disclosure.</param>
        /// <returns>A ROLE container.</returns>
        private ROLES CreateRoles_Seller(EntityType entityType, bool includedInDisclosure)
        {
            var roles = new ROLES();
            roles.Role.Add(this.CreateRole_Seller(entityType, includedInDisclosure, 1));

            return roles;
        }

        /// <summary>
        /// Creates ROLES container with one ROLE sub-container populated with specified agent role type information.
        /// </summary>
        /// <param name="agent">The agent object.</param>
        /// <param name="entityType">Indicates whether the agent is an individual or legal entity.</param>
        /// <returns>Returns ROLES container with one ROLE sub-container populated by specified agent role type information.</returns>
        private ROLES CreateRoles(CAgentFields agent, EntityType entityType)
        {
            var roles = new ROLES();
            roles.Role.Add(this.CreateRole(agent, entityType, sequenceNumber: 1));

            return roles;
        }

        /// <summary>
        /// Creates a basic ROLES container with a single role and type.
        /// </summary>
        /// <param name="roleType">The role type.</param>
        /// <param name="roleTypeOtherDescription">The description of the role type. Only populates if <paramref name="roleType"/> is <see cref="PartyRoleBase.Other"/></param>
        /// <returns>A ROLES container.</returns>
        private ROLES CreateRoles(PartyRoleBase roleType, string roleTypeOtherDescription = null)
        {
            var roles = new ROLES();
            roles.Role.Add(this.CreateRole(roleType, roleType == PartyRoleBase.Other ? roleTypeOtherDescription : null));

            return roles;
        }
        
        /// <summary>
        /// Creates a container to hold all instances of ROLE.
        /// </summary>
        /// <param name="appData">Application data associated with the loan.</param>
        /// <returns>A ROLES container.</returns>
        private ROLES CreateRoles(CAppData appData)
        {
            var roles = new ROLES();
            int borrowerRoleCounter = 0;
            roles.Role.Add(this.CreateRole_Borrower(appData, ++borrowerRoleCounter));

            if (appData.aTypeT == E_aTypeT.Individual || appData.aTypeT == E_aTypeT.TitleOnly)
            {
                roles.Role.Add(this.CreateRole_PropertyOwner(appData, ++borrowerRoleCounter));
            }

            foreach (var trustee in this.loanData.sTrustCollection.Trustees)
            {
                if (string.Equals(appData.aNm, trustee.Name, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(appData.aFirstNm + " " + appData.aLastNm, trustee.Name, StringComparison.OrdinalIgnoreCase))
                {
                    string label = (appData.BorrowerModeT == E_BorrowerModeT.Borrower ? appData.aBMismoId : appData.aCMismoId) + "_Trustee";
                    roles.Role.Add(this.CreateRole_Trustee(trustee, label, ++borrowerRoleCounter));
                }
            }

            foreach (var settlor in this.loanData.sTrustCollection.Trustors)
            {
                if (string.Equals(appData.aNm, settlor.Name, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(appData.aFirstNm + " " + appData.aLastNm, settlor.Name, StringComparison.OrdinalIgnoreCase))
                {
                    string label = (appData.BorrowerModeT == E_BorrowerModeT.Borrower ? appData.aBMismoId : appData.aCMismoId) + "_Settlor";
                    roles.Role.Add(this.CreateRole_Settlor(label, ++borrowerRoleCounter));
                }
            }
            
            return roles;
        }

        /// <summary>
        /// Creates a ROLES container with roles pertaining to LendingQB.
        /// </summary>
        /// <returns>A ROLES container with roles pertaining to LendingQB.</returns>
        private ROLES CreateRoles_LQB()
        {
            var roles = new ROLES();
            roles.Role.Add(this.CreateRole_SubmittingParty(1));
            roles.Role.Add(this.CreateRole_ReturnTo(2));

            return roles;
        }

        /// <summary>
        /// Creates a ROLES container with one ROLE sub-container populated with information pertaining to the lender as the requesting party.
        /// </summary>
        /// <returns>A ROLES container with one ROLE sub-container populated with information pertaining to the lender.</returns>
        private ROLES CreateRoles_RequestingParty()
        {
            var roles = new ROLES();
            roles.Role.Add(this.CreateRole_RequestingParty(1));

            return roles;
        }

        /// <summary>
        /// Creates a container with information pertaining to the entity submitting the order/message to the vendor, i.e. LendingQB.
        /// </summary>
        /// <returns>A SUBMITTING_PARTY container.</returns>
        private SUBMITTING_PARTY CreateSubmittingParty()
        {
            var submitter = new SUBMITTING_PARTY();
            submitter.SubmittingPartySequenceNumber = ToMismoSequenceNumber(1);

            if (!string.IsNullOrEmpty(this.transactionID))
            {
                submitter.SubmittingPartyTransactionIdentifier = ToMismoIdentifier(this.transactionID);
            }

            return submitter;
        }

        /// <summary>
        /// Creates a SERVICER container.
        /// </summary>
        /// <param name="servicerType">The type of servicer.</param>
        /// <returns>A SERVICER container.</returns>
        private SERVICER CreateServicer(ServicerBase servicerType)
        {
            var servicer = new SERVICER();
            servicer.ServicerType = ToMismoEnum(servicerType);
            return servicer;
        }

        /// <summary>
        /// Creates a container with data on one of a borrower's assets or liabilities.
        /// </summary>
        /// <param name="amount">The amount of the asset or liability.</param>
        /// <param name="type">The type of the asset or liability.</param>
        /// <param name="sequence">The sequence number of the summary among the list of summaries.</param>
        /// <returns>A SUMMARY container.</returns>
        private SUMMARY CreateSummary(string amount, SummaryAmountBase type, int sequence)
        {
            var summary = new SUMMARY();
            summary.SummaryAmount = ToMismoAmount(amount);
            summary.SummaryAmountType = ToSummaryAmountEnum(type);
            summary.SequenceNumber = sequence;

            return summary;
        }

        /// <summary>
        /// Creates a a container summarizing a borrower's assets and liabilities.
        /// </summary>
        /// <param name="appData">The borrower's application data.</param>
        /// <returns>A SUMMARIES container.</returns>
        private SUMMARIES CreateSummaries(CAppData appData)
        {
            var summaries = new SUMMARIES();

            summaries.Summary.Add(this.CreateSummary(appData.aAsstLiqTot_rep, SummaryAmountBase.SubtotalLiquidAssetsNotIncludingGift, 1));
            summaries.Summary.Add(this.CreateSummary(appData.aAsstNonReSolidTot_rep, SummaryAmountBase.SubtotalNonLiquidAssets, summaries.Summary.Count(s => s != null) + 1));
            summaries.Summary.Add(this.CreateSummary(appData.aLiaMonTot_rep, SummaryAmountBase.SubtotalLiabilitiesMonthlyPayment, summaries.Summary.Count(s => s != null) + 1));
            summaries.Summary.Add(this.CreateSummary(appData.aLiaBalTot_rep, SummaryAmountBase.TotalLiabilitiesBalance, summaries.Summary.Count(s => s != null) + 1));

            return summaries;
        }

        /// <summary>
        /// Creates a container holding details on the Truth In Lending document.
        /// </summary>
        /// <returns>A TIL_DISCLOSURE_DETAIL container.</returns>
        private TIL_DISCLOSURE_DETAIL CreateTilDisclosureDetail()
        {
            var tilDisclosureDetail = new TIL_DISCLOSURE_DETAIL();
            tilDisclosureDetail.TILDisclosureDate = ToMismoDate(this.loanData.sTilPrepareDate_rep);

            return tilDisclosureDetail;
        }

        /// <summary>
        /// Creates a container holding information on a trustee.
        /// </summary>
        /// <param name="trustee">The trustee to be mapped to MISMO.</param>
        /// <returns>A TRUSTEE container.</returns>
        private TRUSTEE CreateTrustee(TrustCollection.Trustee trustee)
        {
            var trusteeContainer = new TRUSTEE();
            trusteeContainer.DeedOfTrustTrusteeIndicator = ToMismoIndicator(false);

            return trusteeContainer;
        }

        /// <summary>
        /// Creates a container holding information on a trustee.
        /// </summary>
        /// <param name="entityType">Indicates whether the trustee is an individual or legal entity.</param>
        /// <returns>A TRUSTEE container.</returns>
        private TRUSTEE CreateTrustee(EntityType entityType)
        {
            var trusteeContainer = new TRUSTEE();
            trusteeContainer.DeedOfTrustTrusteeIndicator = ToMismoIndicator(entityType == EntityType.LegalEntity);

            return trusteeContainer;
        }

        /// <summary>
        /// Creates a container to hold underwriting information.
        /// </summary>
        /// <returns>An UNDERWRITING container.</returns>
        private UNDERWRITING CreateUnderwriting()
        {
            var underwriting = new UNDERWRITING();
            underwriting.AutomatedUnderwritings = this.CreateAutomatedUnderwritings();
            underwriting.UnderwritingDetail = this.CreateUnderwritingDetail();

            return underwriting;
        }

        /// <summary>
        /// Creates a container to hold underwriting detail information.
        /// </summary>
        /// <returns>An UNDERWRITING container.</returns>
        private UNDERWRITING_DETAIL CreateUnderwritingDetail()
        {
            var underwritingDetail = new UNDERWRITING_DETAIL();
            underwritingDetail.LoanManualUnderwritingIndicator = ToMismoIndicator(this.loanData.sIsManualUw);

            return underwritingDetail;
        }

        /// <summary>
        /// Creates a container holding an unparsed legal description.
        /// </summary>
        /// <param name="sequence">The sequence number of the unparsed legal description among the list of descriptions.</param>
        /// <returns>An UNPARSED_LEGAL_DESCRIPTION container.</returns>
        private UNPARSED_LEGAL_DESCRIPTION CreateUnparsedLegalDescription(int sequence)
        {
            var unparsedLegalDescription = new UNPARSED_LEGAL_DESCRIPTION();
            unparsedLegalDescription.UnparsedLegalDescription = ToMismoString(this.loanData.sSpLegalDesc);
            unparsedLegalDescription.SequenceNumber = sequence;

            return unparsedLegalDescription;
        }

        /// <summary>
        /// Creates a container holding unparsed legal descriptions.
        /// </summary>
        /// <returns>An UNPARSED_LEGAL_DESCRIPTIONS container.</returns>
        private UNPARSED_LEGAL_DESCRIPTIONS CreateUnparsedLegalDescriptions()
        {
            var unparsedLegalDescriptions = new UNPARSED_LEGAL_DESCRIPTIONS();
            unparsedLegalDescriptions.UnparsedLegalDescription.Add(this.CreateUnparsedLegalDescription(1));

            return unparsedLegalDescriptions;
        }

        /// <summary>
        /// Creates a container representing the URLA document.
        /// </summary>
        /// <returns>A URLA container.</returns>
        private URLA CreateUrla()
        {
            var urla = new URLA();
            urla.UrlaDetail = this.CreateUrlaDetail();
            urla.UrlaTotal = this.CreateUrlaTotal();
            urla.UrlaTotalHousingExpenses = this.CreateUrlaTotalHousingExpenses();

            return urla;
        }
        
        /// <summary>
        /// Creates a container with details on the URLA document.
        /// </summary>
        /// <returns>A URLA_DETAIL container populated with data.</returns>
        private URLA_DETAIL CreateUrlaDetail()
        {
            var urlaDetail = new URLA_DETAIL();
            urlaDetail.RequiredDepositIndicator = ToMismoIndicator(this.loanData.sAprIncludesReqDeposit);
            urlaDetail.AdditionalBorrowerAssetsConsideredIndicator = ToMismoIndicator(this.loanData.sMultiApps);
            urlaDetail.AdditionalBorrowerAssetsNotConsideredIndicator = ToMismoIndicator(this.primaryApp.aSpouseIExcl);
            urlaDetail.BorrowerRequestedInterestRatePercent = ToMismoPercent(this.loanData.sNoteIR_rep);
            urlaDetail.BorrowerRequestedLoanAmount = ToMismoAmount(this.loanData.sFinalLAmt_rep);
            urlaDetail.AlterationsImprovementsAndRepairsAmount = ToMismoAmount(this.loanData.sAltCost_rep);
            urlaDetail.BorrowerPaidDiscountPointsTotalAmount = ToMismoAmount(this.loanData.sLDiscnt1003_rep);
            urlaDetail.EstimatedClosingCostsAmount = ToMismoAmount(this.loanData.sTotEstCcNoDiscnt1003_rep);
            urlaDetail.PurchasePriceAmount = ToMismoAmount(this.loanData.sPurchasePrice1003_rep);
            urlaDetail.PrepaidItemsEstimatedAmount = ToMismoAmount(this.loanData.sTotEstPp1003_rep);
            urlaDetail.SellerPaidClosingCostsAmount = ToMismoAmount(this.loanData.sTotCcPbs_rep);
            urlaDetail.MIAndFundingFeeFinancedAmount = ToMismoAmount(this.loanData.sFfUfmipFinanced_rep);
            urlaDetail.MIAndFundingFeeTotalAmount = ToMismoAmount(this.loanData.sFfUfmip1003_rep);
            urlaDetail.ApplicationTakenMethodType = ToApplicationTakenMethodEnum(this.primaryApp.aIntrvwrMethodT);

            if (this.loanData.sFinMethT == E_sFinMethT.ARM)
            {
                urlaDetail.ARMTypeDescription = ToMismoString(this.loanData.sFinMethDesc);
            }

            if (this.loanData.sApp1003InterviewerPrepareDate.IsValid)
            {
                urlaDetail.ApplicationSignedByLoanOriginatorDate = ToMismoDate(this.loanData.sApp1003InterviewerPrepareDate_rep);
            }

            // Refinance loan data points
            if (this.loanData.sIsRefinancing)
            {
                urlaDetail.RefinanceImprovementCostsAmount = ToMismoAmount(this.loanData.sSpImprovC_rep);
                urlaDetail.RefinanceImprovementsType = ToRefinanceImprovementsEnum(this.loanData.sSpImprovTimeFrameT);
                urlaDetail.RefinanceProposedImprovementsDescription = ToMismoString(this.loanData.sSpImprovDesc);
            }

            if (!IsAmountStringBlankOrZero(this.loanData.sRefPdOffAmt1003_rep))
            {
                urlaDetail.RefinanceIncludingDebtsToBePaidOffAmount = ToMismoAmount(this.loanData.sRefPdOffAmt1003_rep);
            }

            urlaDetail.Extension = this.CreateUrlaDetailExtension();

            return urlaDetail;
        }

        /// <summary>
        /// Creates a container holding extension data for URLA_DETAIL.
        /// </summary>
        /// <returns>An URLA_DETAIL_EXTENSION container.</returns>
        private URLA_DETAIL_EXTENSION CreateUrlaDetailExtension()
        {
            var extension = new URLA_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBUrlaDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates a container holding proprietary extension data for URLA_DETAIL.
        /// </summary>
        /// <returns>An LQB_URLA_DETAIL_EXTENSION container.</returns>
        private LQB_URLA_DETAIL_EXTENSION CreateLQBUrlaDetailExtension()
        {
            var extension = new LQB_URLA_DETAIL_EXTENSION();
            extension.MannerInWhichTitleHeld = ToMismoString(this.primaryApp.aManner);
            extension.LandIfAcquiredSeparatelyAmount = ToMismoAmount(this.loanData.sLandIfAcquiredSeparately1003_rep);
            return extension;
        }

        /// <summary>
        /// Creates a container holding URLA Total information.
        /// </summary>
        /// <returns>A URLA_TOTAL container.</returns>
        private URLA_TOTAL CreateUrlaTotal()
        {
            var urlaTotal = new URLA_TOTAL();
            urlaTotal.URLATotalBaseIncomeAmount = ToMismoAmount(this.loanData.sLTotBaseI_rep);
            urlaTotal.URLATotalCashFromToBorrowerAmount = ToMismoAmount(this.loanData.sTransNetCash_rep);
            urlaTotal.URLATotalLiabilityMonthlyPaymentsAmount = ToMismoAmount(this.loanData.sLiaMonLTot_rep);
            urlaTotal.URLATotalMonthlyIncomeAmount = ToMismoAmount(this.loanData.sLTotI_rep);
            urlaTotal.URLATotalOtherTypesOfIncomeAmount = ToMismoAmount(this.loanData.sLTotOI_rep);
            urlaTotal.URLATotalTransactionCostAmount = ToMismoAmount(this.loanData.sTotTransC_rep);
            urlaTotal.URLATotalLotAndImprovementsAmount = ToMismoAmount(this.loanData.sLotWImprovTot_rep);

            return urlaTotal;
        }

        /// <summary>
        /// Creates a URLA_TOTAL_HOUSING_EXPENSES container with the proposed housing expenses from section V of the URLA.
        /// </summary>
        /// <returns>A URLA_TOTAL_HOUSING_EXPENSES container with the proposed housing expenses from section V of the URLA.</returns>
        private URLA_TOTAL_HOUSING_EXPENSES CreateUrlaTotalHousingExpenses()
        {
            var expenses = new URLA_TOTAL_HOUSING_EXPENSES();
            expenses.UrlaTotalHousingExpense.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.FirstMortgagePrincipalAndInterest, 1));
            expenses.UrlaTotalHousingExpense.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.OtherMortgageLoanPrincipalAndInterest, expenses.UrlaTotalHousingExpense.Count(e => e != null) + 1));
            expenses.UrlaTotalHousingExpense.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.HazardInsurance, expenses.UrlaTotalHousingExpense.Count(e => e != null) + 1));
            expenses.UrlaTotalHousingExpense.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.RealEstateTax, expenses.UrlaTotalHousingExpense.Count(e => e != null) + 1));
            expenses.UrlaTotalHousingExpense.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.MI, expenses.UrlaTotalHousingExpense.Count(e => e != null) + 1));
            expenses.UrlaTotalHousingExpense.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.HomeownersAssociationDuesAndCondominiumFees, expenses.UrlaTotalHousingExpense.Count(e => e != null) + 1));
            expenses.UrlaTotalHousingExpense.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.Other, expenses.UrlaTotalHousingExpense.Count(e => e != null) + 1));

            return expenses;
        }

        /// <summary>
        /// Creates a URLA_TOTAL_HOUSING_EXPENSE with a proposed housing expense listed on section V of the URLA.
        /// </summary>
        /// <param name="expenseType">The type of housing expense.</param>
        /// <param name="sequence">The sequence of the expense among the list of proposed housing expenses.</param>
        /// <returns>A URLA_TOTAL_HOUSING_EXPENSE with a proposed housing expense listed on section V of the URLA.</returns>
        private URLA_TOTAL_HOUSING_EXPENSE CreateUrlaTotalHousingExpense(HousingExpenseBase expenseType, int sequence)
        {
            var expense = new URLA_TOTAL_HOUSING_EXPENSE();
            expense.HousingExpenseType = ToHousingExpenseEnum(expenseType);
            expense.SequenceNumber = sequence;

            if (expenseType == HousingExpenseBase.FirstMortgagePrincipalAndInterest)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.loanData.sProFirstMPmt_rep);
            }
            else if (expenseType == HousingExpenseBase.OtherMortgageLoanPrincipalAndInterest)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.loanData.sProSecondMPmt_rep);
            }
            else if (expenseType == HousingExpenseBase.HazardInsurance)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.loanData.sProHazIns_rep);
            }
            else if (expenseType == HousingExpenseBase.RealEstateTax)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.loanData.sProRealETx_rep);
            }
            else if (expenseType == HousingExpenseBase.MI)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.loanData.sProMIns_rep);
            }
            else if (expenseType == HousingExpenseBase.HomeownersAssociationDuesAndCondominiumFees)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.loanData.sProHoAssocDues_rep);
            }
            else if (expenseType == HousingExpenseBase.Other)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.loanData.sProOHExp_rep);
                expense.HousingExpenseTypeOtherDescription = ToMismoString(this.loanData.sProOHExpDesc);
            }
            else
            {
                return null;
            }
            
            return expense;
        }

        /// <summary>
        /// Collects the record IDs of agents that should be excluded to prevent duplication with
        /// contacts on the Loan Estimate, Closing Disclosure, SSPL page, and elsewhere.
        /// </summary>
        /// <returns>A list of record IDs.</returns>
        private List<Guid> CompileExcludedAgents()
        {
            var agentRecords = new List<Guid>();

            if (!this.loanData.sIsInTRID2015Mode)
            {
                return agentRecords;
            }

            if (this.loanData.sTRIDLenderAgentId != Guid.Empty && this.vendor != E_DocumentVendor.DocsOnDemand)
            {
                agentRecords.Add(this.loanData.sTRIDLenderAgentId);
            }

            if (this.loanData.sTRIDMortgageBrokerAgentId != Guid.Empty)
            {
                agentRecords.Add(this.loanData.sTRIDMortgageBrokerAgentId);
            }

            foreach (var seller in this.loanData.sSellerCollection.ListOfSellers)
            {
                if (seller.AgentId != Guid.Empty)
                {
                    agentRecords.Add(seller.AgentId);
                }
            }

            if (this.isClosingPackage)
            {
                if (this.loanData.sTRIDSettlementAgentAgentId != Guid.Empty && this.vendor != E_DocumentVendor.DocsOnDemand)
                {
                    agentRecords.Add(this.loanData.sTRIDSettlementAgentAgentId);
                }

                if (this.loanData.sTRIDRealEstateBrokerBuyerAgentId != Guid.Empty)
                {
                    agentRecords.Add(this.loanData.sTRIDRealEstateBrokerBuyerAgentId);
                }

                if (this.loanData.sTRIDRealEstateBrokerSellerAgentId != Guid.Empty)
                {
                    agentRecords.Add(this.loanData.sTRIDRealEstateBrokerSellerAgentId);
                }
            }
            else
            {
                if (this.loanData.sTRIDLoanOfficerAgentId != Guid.Empty)
                {
                    agentRecords.Add(this.loanData.sTRIDLoanOfficerAgentId);
                }
            }

            foreach (var provider in this.loanData.sAvailableSettlementServiceProviders.GetAllProviders())
            {
                if (provider.AgentId != Guid.Empty)
                {
                    agentRecords.Add(provider.AgentId);
                }
            }

            return agentRecords;
        }

        /// <summary>
        /// Retrieves the closing cost fee associated with the mortgage insurance escrow.
        /// </summary>
        /// <returns>The closing cost fee associated with the mortgage insurance escrow.</returns>
        private BorrowerClosingCostFee GetMortgageInsuranceEscrowFee()
        {
            Func<BaseClosingCostFee, bool> setFilter =
                ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(
                    this.loanData.sClosingCostSet,
                    false,
                    this.loanData.sOriginatorCompensationPaymentSourceT,
                    this.loanData.sDisclosureRegulationT,
                    this.loanData.sGfeIsTPOTransaction);

            Func<BaseClosingCostFee, bool> trueFilter =
                fee => setFilter(fee)
                && fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId;

            return (BorrowerClosingCostFee)this.loanData.sClosingCostSet.GetFees(trueFilter).FirstOrDefault();
        }

        /// <summary>
        /// Gets prepaid fees from the closing cost set.
        /// </summary>
        /// <returns>A set of prepaid fees.</returns>
        private IEnumerable<BorrowerClosingCostFee> GetPrepaidFees()
        {
            BorrowerClosingCostFeeSection prepaids = (BorrowerClosingCostFeeSection)this.loanData.sClosingCostSet.GetSection(E_ClosingCostViewT.LoanClosingCost, E_IntegratedDisclosureSectionT.SectionF);

            if (prepaids == null || prepaids.FilteredClosingCostFeeList.Count(f => ((BorrowerClosingCostFee)f).TotalAmount != 0.00m) == 0)
            {
                return Enumerable.Empty<BorrowerClosingCostFee>();
            }

            return prepaids.FilteredClosingCostFeeList.Cast<BorrowerClosingCostFee>();
        }

        /// <summary>
        /// Populates the list of fees that should be excluded from the export. Some vendors need this
        /// because otherwise they will duplicate fees in the documents if they receive information on
        /// the same fee in multiple elements, such as the ESCROW and FEE sections.
        /// </summary>
        private void PopulateFeeIgnoreList()
        {
            this.feeIgnoreList = new List<Guid>();

            if (this.vendor != E_DocumentVendor.DocMagic && this.vendor != E_DocumentVendor.Simplifile)
            {
                return;
            }

            foreach (BaseHousingExpense expense in this.loanData.sHousingExpenses.ExpensesToUse.Where(e => (e.AnnualAmt != 0.00m || e.MonthlyAmtTotal != 0.00m || e.ReserveAmt != 0.00m) && e.IsEscrowedAtClosing == E_TriState.Yes))
            {
                BorrowerClosingCostFee escrowFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionG);
                if (escrowFee != null && escrowFee.ClosingCostFeeTypeId != Guid.Empty)
                {
                    this.feeIgnoreList.Add(escrowFee.ClosingCostFeeTypeId);
                }
            }

            foreach (BorrowerClosingCostFee prepaidFee in this.GetPrepaidFees())
            {
                if (prepaidFee != null && prepaidFee.ClosingCostFeeTypeId != Guid.Empty)
                {
                    this.feeIgnoreList.Add(prepaidFee.ClosingCostFeeTypeId);
                }
            }

            if (this.vendor == E_DocumentVendor.DocMagic)
            {
                this.feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId);
                this.feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId);
            }

            if (this.vendor == E_DocumentVendor.Simplifile)
            {
                var miEscrowFee = this.GetMortgageInsuranceEscrowFee();
                if (miEscrowFee != null && miEscrowFee.ClosingCostFeeTypeId != Guid.Empty)
                {
                    this.feeIgnoreList.Add(miEscrowFee.ClosingCostFeeTypeId);
                }

                this.feeIgnoreList.Add(DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId);
            }
        }

        /// <summary>
        /// Generates a role label for a <see cref="PARTY"/>. 
        /// </summary>
        /// <param name="roleType">The role type for the entity.</param>
        /// <param name="entityType">Indicates whether the <see cref="PARTY"/> is a legal entity or individual.</param>
        /// <returns>The label string.</returns>
        private string GeneratePartyLabel(E_AgentRoleT roleType, EntityType entityType)
        {
            return this.GeneratePartyLabel(GetXmlEnumName(roleType), entityType);
        }

        /// <summary>
        /// Generates a role label for a <see cref="PARTY"/>. 
        /// </summary>
        /// <param name="roleType">The party role type for the entity.</param>
        /// <param name="entityType">Indicates whether the <see cref="PARTY"/> is a legal entity or individual.</param>
        /// <returns>The label string.</returns>
        private string GeneratePartyLabel(PartyRoleBase roleType, EntityType entityType)
        {
            return this.GeneratePartyLabel(roleType.ToString(), entityType);
        }

        /// <summary>
        /// Generates a role label for a <see cref="PARTY"/>. 
        /// </summary>
        /// <param name="partyType">A string representing the entity's role.</param>
        /// <param name="entityType">Indicates whether the <see cref="PARTY"/> is a legal entity or individual.</param>
        /// <returns>The label string.</returns>
        private string GeneratePartyLabel(string partyType, EntityType entityType)
        {
            string label = partyType + this.counters.PartyLabels;

            this.partyLabels.StoreLabel(label, partyType, entityType);
            this.allRelationships.Add(this.CreateRelationship_ToLoan("ROLE", label, ArcroleVerbPhrase.IsAssociatedWith));

            return label;
        }
    }
}