﻿// <copyright file="Mismo33Counters.cs" company="MeridianLink">
//  Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//  Author: Justin Lara, Isaac Ribakoff
//  Date:   February 20, 2015
// </summary>
namespace LendersOffice.Conversions.Mismo3.Version3
{
    /// <summary>
    /// A set of self-incrementing counters to manage the sequence numbers in a MISMO 3.3 export.
    /// </summary>
    public class Mismo33Counters
    {
        /// <summary>
        /// Gets the number of LIABILITY containers.
        /// </summary>
        private int liabilities;

        /// <summary>
        /// Gets the number of EMPLOYER containers.
        /// </summary>
        private int employers;

        /// <summary>
        /// Gets the number of EXPENSE containers.
        /// </summary>
        private int expenses;

        /// <summary>
        /// Gets the number of ASSET containers.
        /// </summary>
        private int assets;

        /// <summary>
        /// Gets the number of ESCROW_ITEM containers.
        /// </summary>
        private int escrowItems;

        /// <summary>
        /// Gets the number of DOCUMENT_SPECIFIC_DATA_SET containers.
        /// </summary>
        private int documentSpecificDataSets;

        /// <summary>
        /// Gets the number of PROPERTY_TAX containers.
        /// </summary>
        private int propertyTaxes;

        /// <summary>
        /// Gets the number of RELATIONSHIP containers.
        /// </summary>
        private int relationships;

        /// <summary>
        /// Gets the number of PARTY containers using generated $$xlink$$ labels.
        /// </summary>
        private int partyLabels;

        /// <summary>
        /// Gets the number of PARTY containers.
        /// </summary>
        private int parties;

        /// <summary>
        /// Gets the number of TitleOnlyBorrowers on a loan file.
        /// </summary>
        private int titleOnlyBorrowers;

        /// <summary>
        /// Initializes a new instance of the <see cref="Mismo33Counters" /> class, which holds a set
        /// of counters for MISMO 3.3 containers.
        /// </summary>
        public Mismo33Counters()
        {
            this.liabilities = 0;
            this.employers = 0;
            this.expenses = 0;
            this.assets = 0;
            this.escrowItems = 0;
            this.documentSpecificDataSets = 0;
            this.propertyTaxes = 0;
            this.relationships = 0;
            this.partyLabels = 0;
            this.parties = 0;
            this.titleOnlyBorrowers = 0;
        }

        /// <summary>
        /// Gets the sequence number for the next LIABILITY container.
        /// </summary>
        /// <value>The number of LIABILITY containers.</value>
        public int Liabilities
        {
            get
            {
                return ++this.liabilities;
            }
        }

        /// <summary>
        /// Gets the sequence number for the next EMPLOYER container.
        /// </summary>
        /// <value>The number of EMPLOYER containers.</value>
        public int Employers
        {
            get
            {
                return ++this.employers;
            }
        }

        /// <summary>
        /// Gets the sequence number for the next EXPENSE container.
        /// </summary>
        /// <value>The number of EXPENSE containers.</value>
        public int Expenses
        {
            get
            {
                return ++this.expenses;
            }
        }

        /// <summary>
        /// Gets the sequence number for the next ASSET container.
        /// </summary>
        /// <value>The number of ASSET containers.</value>
        public int Assets
        {
            get
            {
                return ++this.assets;
            }
        }

        /// <summary>
        /// Gets the sequence number for the next ESCROW_ITEM container.
        /// </summary>
        /// <value>The number of ESCROW_ITEM containers.</value>
        public int EscrowItems
        {
            get
            {
                return ++this.escrowItems;
            }
        }

        /// <summary>
        /// Gets the sequence number for the next DOCUMENT_SPECIFIC_DATA_SET container.
        /// </summary>
        /// <value>The number of DOCUMENT_SPECIFIC_DATA_SET containers.</value>
        public int DocumentSpecificDataSets
        {
            get
            {
                return ++this.documentSpecificDataSets;
            }
        }

        /// <summary>
        /// Gets the sequence number for the next PROPERTY_TAX container.
        /// </summary>
        /// <value>The number of PROPERTY_TAX containers.</value>
        public int PropertyTaxes
        {
            get
            {
                return ++this.propertyTaxes;
            }
        }

        /// <summary>
        /// Gets the sequence number for the next RELATIONSHIP container.
        /// </summary>
        /// <value>The number of RELATIONSHIP containers.</value>
        public int Relationships
        {
            get
            {
                return ++this.relationships;
            }
        }

        /// <summary>
        /// Gets the $$xlink$$ label number suffix for the next party.
        /// </summary>
        /// <value>The $$xlink$$ label number suffix for the next party.</value>
        public int PartyLabels
        {
            get
            {
                return ++this.partyLabels;
            }
        }

        /// <summary>
        /// Gets the number of the next Party.
        /// </summary>
        /// <value>The number of Party containers.</value>
        public int Parties
        {
            get
            {
                return ++this.parties;
            }
        }

        /// <summary>
        /// Gets the number of the next TitleOnlyBorrower.
        /// </summary>
        /// <value>The number of TitleOnlyBorrowers.</value>
        public int TitleOnlyBorrowers
        {
            get
            {
                return ++this.titleOnlyBorrowers;
            }
        }
    }
}
