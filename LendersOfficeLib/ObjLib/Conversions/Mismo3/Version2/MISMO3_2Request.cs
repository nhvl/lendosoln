﻿// This was implemented to facilitate title orders via the title framework.
// Blocks/data can be added as needed for other MISMO 3.2 service types.
namespace LendersOffice.Conversions.Mismo3.Version2.Request
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using System.Xml.Serialization;
    using LendersOffice.Common;

    // This is not declared as an XML root because it's assumed that LQB requests/responses will be wrapped with proprietary XML
    public class MESSAGE
    {
        //3\.\d*((\.\d*)?(\[[B]\d*(\-\d*)?\])?)?
        [XmlAttribute()]
        public string MISMOReferenceModelIdentifier = "3.2.0[B292]";
        public bool ShouldSerializeMISMOReferenceModelIdentifier() { return !string.IsNullOrEmpty(MISMOReferenceModelIdentifier); }
        
        //3\.\d*((\.\d*)?(\.\d*)?(\[[B]\d*(\-\d*)?\])?)?
        [XmlAttribute()]
        public string MISMOLogicalDataDictionaryIdentifier = "3.2.0[B292]";
        public bool ShouldSerializeMISMOLogicalDataDictionaryIdentifier() { return !string.IsNullOrEmpty(MISMOLogicalDataDictionaryIdentifier); }

        [XmlElement(Order = 1)]
        public DEAL_SETS DEAL_SETS = new DEAL_SETS();
    }

    public class ADDRESS
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public MISMOString AddressAdditionalLineText;
        [XmlIgnore()]
        public bool AddressAdditionalLineTextSpecified { get { return AddressAdditionalLineText != null; } set { } }

        [XmlElement(Order = 2)]
        public MISMOString AddressLineText = new MISMOString();

        [XmlElement(Order = 3)]
        public AddressEnum AddressType = new AddressEnum();

        [XmlElement(Order = 4)]
        public MISMOString AddressTypeOtherDescription;
        [XmlIgnore()]
        public bool AddressTypeOtherDescriptionSpecified { get { return AddressTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 5)]
        public MISMOString CityName = new MISMOString();

        [XmlElement(Order = 6)]
        public MISMOCode CountryCode = new MISMOCode();

        [XmlElement(Order = 7)]
        public MISMOString CountryName = new MISMOString();

        [XmlElement(Order = 8)]
        public MISMOCode CountyCode;
        [XmlIgnore()]
        public bool CountyCodeSpecified { get { return CountyCode != null; } set { } }

        [XmlElement(Order = 9)]
        public MISMOString CountyName;
        [XmlIgnore()]
        public bool CountyNameSpecified { get { return CountyName != null; } set { } }

        [XmlElement(Order = 10)]
        public MISMOCode PostalCode = new MISMOCode();

        [XmlElement(Order = 11)]
        public MISMOCode StateCode;
        [XmlIgnore()]
        public bool StateCodeSpecified { get { return StateCode != null; } set { } }

        [XmlElement(Order = 12)]
        public MISMOString StateName = new MISMOString();
    }

    public class ADDRESSES
    {
        [XmlElement(Order = 1, ElementName = "ADDRESS")]
        public List<ADDRESS> ADDRESSList = new List<ADDRESS>();
    }

    public class BORROWER
    {
        [XmlElement(Order = 1)]
        public BORROWER_DETAIL BORROWER_DETAIL = new BORROWER_DETAIL();

        [XmlElement(Order = 2)]
        public RESIDENCES RESIDENCES = new RESIDENCES();
    }

    public class BORROWER_DETAIL
    {
        [XmlElement(Order = 1)]
        public BorrowerClassificationEnum BorrowerClassificationType = new BorrowerClassificationEnum();

        [XmlElement(Order = 2)]
        public MISMOIndicator BorrowerMailToAddressSameAsPropertyIndicator = new MISMOIndicator();

        [XmlElement(Order = 3)]
        public BorrowerRelationshipTitleEnum BorrowerRelationshipTitleType = new BorrowerRelationshipTitleEnum();

        [XmlElement(Order = 4)]
        public MISMOString BorrowerRelationshipTitleTypeOtherDescription;
        [XmlIgnore()]
        public bool BorrowerRelationshipTitleTypeOtherDescriptionSpecified { get { return BorrowerRelationshipTitleTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 5)]
        public MaritalStatusEnum MaritalStatusType = new MaritalStatusEnum();
    }

    public class COLLATERAL
    {
        [XmlElement(Order = 1, ElementName = "SUBJECT_PROPERTY")]
        public PROPERTY PROPERTY;
    }

    public class COLLATERALS
    {
        [XmlElement(Order = 1, ElementName = "COLLATERAL")]
        public List<COLLATERAL> COLLATERALList = new List<COLLATERAL>();
    }

    public class CONTACT
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public CONTACT_POINTS CONTACT_POINTS = new CONTACT_POINTS();

        [XmlElement(Order = 2)]
        public NAME NAME = new NAME();
    }

    public class CONTACT_POINT
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public CONTACT_POINT_EMAIL CONTACT_POINT_EMAIL;
        [XmlIgnore()]
        public bool CONTACT_POINT_EMAILSpecified { get { return CONTACT_POINT_EMAIL != null; } set { } }

        [XmlElement(Order = 2)]
        public CONTACT_POINT_TELEPHONE CONTACT_POINT_TELEPHONE;
        [XmlIgnore()]
        public bool CONTACT_POINT_TELEPHONESpecified { get { return CONTACT_POINT_TELEPHONE != null; } set { } }

        [XmlElement(Order = 3)]
        public CONTACT_POINT_DETAIL CONTACT_POINT_DETAIL = new CONTACT_POINT_DETAIL();
    }

    public class CONTACT_POINT_EMAIL
    {
        [XmlElement(Order = 1)]
        public MISMOValue ContactPointEmailValue = new MISMOValue();
    }

    public class CONTACT_POINT_TELEPHONE
    {
        [XmlElement(Order = 1)]
        public MISMONumericString ContactPointFaxExtensionValue;
        [XmlIgnore()]
        public bool ContactPointFaxExtensionValueSpecified { get { return ContactPointFaxExtensionValue != null; } set { } }

        [XmlElement(Order = 2)]
        public MISMONumericString ContactPointFaxValue;
        [XmlIgnore()]
        public bool ContactPointFaxValueSpecified { get { return ContactPointFaxValue != null; } set { } }

        [XmlElement(Order = 3)]
        public MISMONumericString ContactPointTelephoneExtensionValue;
        [XmlIgnore()]
        public bool ContactPointTelephoneExtensionValueSpecified { get { return ContactPointTelephoneExtensionValue != null; } set { } }

        [XmlElement(Order = 4)]
        public MISMONumericString ContactPointTelephoneValue;
        [XmlIgnore()]
        public bool ContactPointTelephoneValueSpecified { get { return ContactPointTelephoneValue != null; } set { } }
    }

    public class CONTACT_POINT_DETAIL
    {
        [XmlElement(Order = 1)]
        public MISMOIndicator ContactPointPreferenceIndicator = new MISMOIndicator();

        [XmlElement(Order = 2)]
        public ContactPointRoleEnum ContactPointRoleType = new ContactPointRoleEnum();

        [XmlElement(Order = 3)]
        public MISMOString ContactPointRoleTypeOtherDescription = new MISMOString();
    }

    public class CONTACT_POINTS
    {
        [XmlElement(Order = 1, ElementName = "CONTACT_POINT")]
        public List<CONTACT_POINT> CONTACT_POINTList = new List<CONTACT_POINT>();
    }

    public class CONTACTS
    {
        [XmlElement(Order = 1, ElementName = "CONTACT")]
        public List<CONTACT> CONTACTList = new List<CONTACT>();
    }

    public class DEAL
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public COLLATERALS COLLATERALS;
        [XmlIgnore()]
        public bool COLLATERALSSpecified { get { return COLLATERALS != null; } set { } }

        [XmlElement(Order = 2)]
        public LIABILITIES LIABILITIES;
        [XmlIgnore()]
        public bool LIABILITIESSpecified { get { return LIABILITIES != null; } set { } }

        [XmlElement(Order = 3)]
        public LOANS LOANS;
        [XmlIgnore()]
        public bool LOANSSpecified { get { return LOANS != null; } set { } }

        [XmlElement(Order = 4)]
        public PARTIES PARTIES;
        [XmlIgnore()]
        public bool PARTIESSpecified { get { return PARTIES != null; } set { } }

        [XmlElement(Order = 5)]
        public SERVICES SERVICES;
        [XmlIgnore()]
        public bool SERVICESSpecified { get { return SERVICES != null; } set { } }
    }

    public class DEAL_SET
    {
        [XmlElement(Order = 1)]
        public DEALS DEALS = new DEALS();
    }

    public class DEAL_SETS
    {
        [XmlElement(Order = 1, ElementName = "DEAL_SET")]
        public List<DEAL_SET> DEAL_SETList = new List<DEAL_SET>();
    }

    public class DEALS
    {
        [XmlElement(Order = 1, ElementName = "DEAL")]
        public List<DEAL> DEALList = new List<DEAL>();
    }

    public class INDIVIDUAL
    {
        [XmlElement(Order = 1)]
        public CONTACT_POINTS CONTACT_POINTS = new CONTACT_POINTS();

        [XmlElement(Order = 2)]
        public NAME NAME = new NAME();
    }

    public class LEGAL_DESCRIPTION
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public UNPARSED_LEGAL_DESCRIPTIONS UNPARSED_LEGAL_DESCRIPTIONS = new UNPARSED_LEGAL_DESCRIPTIONS();
    }

    public class LEGAL_DESCRIPTIONS
    {
        [XmlElement(Order = 1, ElementName = "LEGAL_DESCRIPTION")]
        public List<LEGAL_DESCRIPTION> LEGAL_DESCRIPTIONList = new List<LEGAL_DESCRIPTION>();
    }

    public class LEGAL_ENTITY
    {
        [XmlElement(Order = 1)]
        public CONTACTS CONTACTS = new CONTACTS();

        [XmlElement(Order = 2)]
        public LEGAL_ENTITY_DETAIL LEGAL_ENTITY_DETAIL = new LEGAL_ENTITY_DETAIL();
    }

    public class LEGAL_ENTITY_DETAIL
    {
        [XmlElement(Order = 1)]
        public MISMOString FullName = new MISMOString();
    }

    public class LIABILITIES
    {
        [XmlElement(Order = 1, ElementName = "LIABILITY")]
        public List<LIABILITY> LIABILITYList = new List<LIABILITY>();
    }

    public class LIABILITY
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public LIABILITY_DETAIL LIABILITY_DETAIL = new LIABILITY_DETAIL();

        [XmlElement(Order = 2)]
        public LIABILITY_HOLDER LIABILITY_HOLDER = new LIABILITY_HOLDER();

        [XmlElement(Order = 3)]
        public PAYOFF PAYOFF;
        [XmlIgnore()]
        public bool PAYOFFSpecified { get { return PAYOFF != null; } set { } }
    }

    public class LIABILITY_DETAIL
    {
        [XmlElement(Order = 1)]
        public MISMOIdentifier LiabilityAccountIdentifier = new MISMOIdentifier();

        [XmlElement(Order = 2)]
        public MISMOString LiabilityDescription = new MISMOString();

        [XmlElement(Order = 3)]
        public MISMOIndicator LiabilityExclusionIndicator = new MISMOIndicator();

        [XmlElement(Order = 4)]
        public MISMOAmount LiabilityMonthlyPaymentAmount = new MISMOAmount();

        [XmlElement(Order = 5)]
        public MISMOIndicator LiabilityPayoffStatusIndicator = new MISMOIndicator();

        [XmlElement(Order = 6)]
        public MISMOIndicator LiabilityPayoffWithCurrentAssetsIndicator;
        [XmlIgnore()]
        public bool LiabilityPayoffWithCurrentAssetsIndicatorSpecified { get { return LiabilityPayoffWithCurrentAssetsIndicator != null; } set { } }

        [XmlElement(Order = 7)]
        public MISMOCount LiabilityRemainingTermMonthsCount = new MISMOCount();

        [XmlElement(Order = 8)]
        public LiabilityEnum LiabilityType = new LiabilityEnum();

        [XmlElement(Order = 9)]
        public MISMOString LiabilityTypeOtherDescription = new MISMOString();

        [XmlElement(Order = 10)]
        public MISMOAmount LiabilityUnpaidBalanceAmount = new MISMOAmount();

        [XmlElement(Order = 11)]
        public MISMOIndicator SubjectLoanResubordinationIndicator = new MISMOIndicator();
    }

    public class LIABILITY_HOLDER
    {
        [XmlElement(Order = 1)]
        public NAME NAME = new NAME();
    }

    public class LOAN
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public LOAN_IDENTIFIERS LOAN_IDENTIFIERS = new LOAN_IDENTIFIERS();

        [XmlElement(Order = 2)]
        public TERMS_OF_LOAN TERMS_OF_LOAN = new TERMS_OF_LOAN();
    }

    public class LOAN_IDENTIFIER
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public MISMOIdentifier LoanIdentifier = new MISMOIdentifier();

        [XmlElement(Order = 2)]
        public LoanIdentifierEnum LoanIdentifierType = new LoanIdentifierEnum();

        [XmlElement(Order = 3)]
        public MISMOString LoanIdentifierTypeOtherDescription = new MISMOString();
    }

    public class LOAN_IDENTIFIERS
    {
        [XmlElement(Order = 1, ElementName = "LOAN_IDENTIFIER")]
        public List<LOAN_IDENTIFIER> LOAN_IDENTIFIERList = new List<LOAN_IDENTIFIER>();
    }

    public class LOANS
    {
        [XmlElement(Order = 1, ElementName = "LOAN")]
        public List<LOAN> LOANList = new List<LOAN>();
    }

    public class NAME
    {
        [XmlElement(Order = 1)]
        public MISMOString FirstName;
        [XmlIgnore()]
        public bool FirstNameSpecified { get { return FirstName != null; } set { } }

        [XmlElement(Order = 2)]
        public MISMOString FullName = new MISMOString();

        [XmlElement(Order = 3)]
        public MISMOString LastName;
        [XmlIgnore()]
        public bool LastNameSpecified { get { return LastName != null; } set { } }

        [XmlElement(Order = 4)]
        public MISMOString MiddleName;
        [XmlIgnore()]
        public bool MiddleNameSpecified { get { return MiddleName != null; } set { } }

        [XmlElement(Order = 5)]
        public MISMOString PrefixName;
        [XmlIgnore()]
        public bool PrefixNameSpecified { get { return PrefixName != null; } set { } }

        [XmlElement(Order = 6)]
        public MISMOString SuffixName;
        [XmlIgnore()]
        public bool SuffixNameSpecified { get { return SuffixName != null; } set { } }
    }

    public class PARTIES
    {
        [XmlElement(Order = 1, ElementName = "PARTY")]
        public List<PARTY> PARTYList = new List<PARTY>();
    }

    public class PARTY
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public INDIVIDUAL INDIVIDUAL;
        [XmlIgnore()]
        public bool INDIVIDUALSpecified { get { return INDIVIDUAL != null; } set { } }

        [XmlElement(Order = 2)]
        public LEGAL_ENTITY LEGAL_ENTITY;
        [XmlIgnore()]
        public bool LEGAL_ENTITYSpecified { get { return LEGAL_ENTITY != null; } set { } }

        [XmlElement(Order = 3)]
        public ADDRESSES ADDRESSES;
        [XmlIgnore()]
        public bool ADDRESSESSpecified { get { return ADDRESSES != null; } set { } }

        [XmlElement(Order = 4)]
        public ROLES ROLES;
        [XmlIgnore()]
        public bool ROLESSpecified { get { return ROLES != null; } set { } }

        [XmlElement(Order = 5)]
        public TAXPAYER_IDENTIFIERS TAXPAYER_IDENTIFIERS;
        [XmlIgnore()]
        public bool TAXPAYER_IDENTIFIERSSpecified { get { return TAXPAYER_IDENTIFIERS != null; } set { } }
    }

    public class PAYOFF
    {
        [XmlElement(Order = 1)]
        public MISMOIdentifier PayoffAccountNumberIdentifier = new MISMOIdentifier();

        [XmlElement(Order = 2)]
        public PayoffActionEnum PayoffActionType = new PayoffActionEnum();

        [XmlElement(Order = 3)]
        public MISMOString PayoffActionTypeOtherDescription;
        [XmlIgnore()]
        public bool PayoffActionTypeOtherDescriptionSpecified { get { return PayoffActionTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 4)]
        public MISMOAmount PayoffAmount = new MISMOAmount();
    }

    public class PREFERRED_RESPONSE
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public MISMOIdentifier MIMETypeIdentifier;
        [XmlIgnore()]
        public bool MIMETypeIdentifierSpecified { get { return MIMETypeIdentifier != null; } set { } }

        [XmlElement(Order = 2)]
        public MISMOString PreferredResponseDestinationDescription = new MISMOString();

        [XmlElement(Order = 3)]
        public PreferredResponseFormatEnum PreferredResponseFormatType = new PreferredResponseFormatEnum();

        [XmlElement(Order = 4)]
        public MISMOString PreferredResponseFormatTypeOtherDescription;
        [XmlIgnore()]
        public bool PreferredResponseFormatTypeOtherDescriptionSpecified { get { return PreferredResponseFormatTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 5)]
        public PreferredResponseMethodEnum PreferredResponseMethodType = new PreferredResponseMethodEnum();

        [XmlElement(Order = 6)]
        public MISMOString PreferredResponseMethodTypeOtherDescription;
        [XmlIgnore()]
        public bool PreferredResponseMethodTypeOtherDescriptionSpecified { get { return PreferredResponseMethodTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 7)]
        public MISMOIndicator PreferredResponseUseEmbeddedFileIndicator;
        [XmlIgnore()]
        public bool PreferredResponseUseEmbeddedFileIndicatorSpecified { get { return PreferredResponseUseEmbeddedFileIndicator != null; } set { } }

        [XmlElement(Order = 8)]
        public MISMOIdentifier PreferredResponseVersionIdentifier = new MISMOIdentifier();
    }

    public class PREFERRED_RESPONSES
    {
        [XmlElement(Order = 1, ElementName = "PREFERRED_RESPONSE")]
        public List<PREFERRED_RESPONSE> PREFERRED_RESPONSEList = new List<PREFERRED_RESPONSE>();
    }

    public class PROPERTY
    {
        [XmlIgnore()]
        public E_ValuationUseBase ValuationUseType;
        [XmlAttribute(AttributeName = "ValuationUseType")]
        public string ValuationUseTypeSerialized { get { return Utilities.ConvertEnumToString(ValuationUseType); } set { } }
        public bool ShouldSerializeValuationUseTypeSerialized() { return !string.IsNullOrEmpty(ValuationUseTypeSerialized); }
        
        [XmlElement(Order = 1)]
        public ADDRESS ADDRESS = new ADDRESS();

        [XmlElement(Order = 2)]
        public LEGAL_DESCRIPTIONS LEGAL_DESCRIPTIONS;
        [XmlIgnore()]
        public bool LEGAL_DESCRIPTIONSSpecified { get { return LEGAL_DESCRIPTIONS != null; } set { } }

        [XmlElement(Order = 3)]
        public PROPERTY_DETAIL PROPERTY_DETAIL;
        [XmlIgnore()]
        public bool PROPERTY_DETAILSpecified { get { return PROPERTY_DETAIL != null; } set { } }

        [XmlElement(Order = 4)]
        public PROPERTY_VALUATIONS PROPERTY_VALUATIONS;
        [XmlIgnore()]
        public bool PROPERTY_VALUATIONSSpecified { get { return PROPERTY_VALUATIONS != null; } set { } }
    }

    public class PROPERTY_DETAIL
    {
        [XmlElement(Order = 1)]
        public AttachmentEnum AttachmentType = new AttachmentEnum();

        [XmlElement(Order = 2)]
        public MISMOCount FinancedUnitCount = new MISMOCount();

        [XmlElement(Order = 3)]
        public MISMOYear PropertyStructureBuiltYear = new MISMOYear();

        [XmlElement(Order = 4)]
        public PropertyUsageEnum PropertyUsageType = new PropertyUsageEnum();

        [XmlElement(Order = 5)]
        public MISMOString PropertyUsageTypeOtherDescription = new MISMOString();

        [XmlElement(Order = 6)]
        public MISMOIndicator PUDIndicator = new MISMOIndicator();
    }

    public class PROPERTY_VALUATION
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public PROPERTY_VALUATION_DETAIL PROPERTY_VALUATION_DETAIL = new PROPERTY_VALUATION_DETAIL();
    }

    public class PROPERTY_VALUATION_DETAIL
    {
        [XmlElement(Order = 1)]
        public MISMOAmount PropertyValuationAmount = new MISMOAmount();
    }

    public class PROPERTY_VALUATIONS
    {
        [XmlElement(Order = 1, ElementName = "PROPERTY_VALUATION")]
        public List<PROPERTY_VALUATION> PROPERTY_VALUATIONList = new List<PROPERTY_VALUATION>();
    }

    public class REQUESTING_PARTY
    {
        [XmlElement(Order = 1)]
        public MISMOIdentifier InternalAccountIdentifier = new MISMOIdentifier();

        [XmlElement(Order = 2)]
        public MISMOString RequestedByName = new MISMOString();

        [XmlElement(Order = 3)]
        public MISMOIdentifier RequestingPartyBranchIdentifier;
        [XmlIgnore()]
        public bool RequestingPartyBranchIdentifierSpecified { get { return RequestingPartyBranchIdentifier != null; } set { } }
    }

    public class RESIDENCE
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public ADDRESS ADDRESS = new ADDRESS();
    }

    public class RESIDENCES
    {
        [XmlElement(Order = 1, ElementName = "RESIDENCE")]
        public List<RESIDENCE> RESIDENCEList = new List<RESIDENCE>();
    }

    public class RETURN_TO
    {
        [XmlElement(Order = 1)]
        public PREFERRED_RESPONSES PREFERRED_RESPONSES = new PREFERRED_RESPONSES();
    }

    public class ROLE
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public BORROWER BORROWER;
        [XmlIgnore()]
        public bool BORROWERSpecified { get { return BORROWER != null; } set { } }

        [XmlElement(Order = 2)]
        public REQUESTING_PARTY REQUESTING_PARTY;
        [XmlIgnore()]
        public bool REQUESTING_PARTYSpecified { get { return REQUESTING_PARTY != null; } set { } }

        [XmlElement(Order = 3)]
        public RETURN_TO RETURN_TO;
        [XmlIgnore()]
        public bool RETURN_TOSpecified { get { return RETURN_TO != null; } set { } }

        [XmlElement(Order = 4)]
        public SUBMITTING_PARTY SUBMITTING_PARTY;
        [XmlIgnore()]
        public bool SUBMITTING_PARTYSpecified { get { return SUBMITTING_PARTY != null; } set { } }

        [XmlElement(Order = 5)]
        public ROLE_DETAIL ROLE_DETAIL;
        [XmlIgnore()]
        public bool ROLE_DETAILSpecified { get { return ROLE_DETAIL != null; } set { } }
    }

    public class ROLE_DETAIL
    {
        [XmlElement(Order = 1)]
        public PartyRoleEnum PartyRoleType = new PartyRoleEnum();

        [XmlElement(Order = 2)]
        public MISMOString PartyRoleTypeOtherDescription;
        [XmlIgnore()]
        public bool PartyRoleTypeOtherDescriptionSpecified { get { return PartyRoleTypeOtherDescription != null; } set { } }
    }

    public class ROLES
    {
        [XmlElement(Order = 1, ElementName = "ROLE")]
        public List<ROLE> ROLEList = new List<ROLE>();
    }

    public class SERVICE
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public TITLE TITLE;
        [XmlIgnore()]
        public bool TITLESpecified { get { return TITLE != null; } set { } }

        [XmlElement(Order = 2)]
        public SERVICE_PRODUCT SERVICE_PRODUCT;
        [XmlIgnore()]
        public bool SERVICE_PRODUCTSpecified { get { return SERVICE_PRODUCT != null; } set { } }
    }

    public class SERVICE_PRODUCT
    {
        [XmlElement(Order = 1)]
        public SERVICE_PRODUCT_REQUEST SERVICE_PRODUCT_REQUEST = new SERVICE_PRODUCT_REQUEST();
    }

    public class SERVICE_PRODUCT_DETAIL
    {
        [XmlElement(Order = 1)]
        public MISMOString ServiceProductDescription = new MISMOString();

        [XmlElement(Order = 2)]
        public MISMOIdentifier ServiceProductIdentifier = new MISMOIdentifier();
    }

    public class SERVICE_PRODUCT_NAME
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public SERVICE_PRODUCT_NAME_ADD_ONS SERVICE_PRODUCT_NAME_ADD_ONS = new SERVICE_PRODUCT_NAME_ADD_ONS();

        [XmlElement(Order = 2)]
        public SERVICE_PRODUCT_NAME_DETAIL SERVICE_PRODUCT_NAME_DETAIL = new SERVICE_PRODUCT_NAME_DETAIL();
    }

    public class SERVICE_PRODUCT_NAME_ADD_ON
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public MISMOString ServiceProductNameAddOnDescription = new MISMOString();

        [XmlElement(Order = 2)]
        public MISMOIdentifier ServiceProductNameAddOnIdentifier = new MISMOIdentifier();
    }

    public class SERVICE_PRODUCT_NAME_ADD_ONS
    {
        [XmlElement(Order = 1, ElementName = "SERVICE_PRODUCT_NAME_ADD_ON")]
        public List<SERVICE_PRODUCT_NAME_ADD_ON> SERVICE_PRODUCT_NAME_ADD_ONList = new List<SERVICE_PRODUCT_NAME_ADD_ON>();
    }

    public class SERVICE_PRODUCT_NAME_DETAIL
    {
        [XmlElement(Order = 1)]
        public MISMOString ServiceProductNameDescription = new MISMOString();

        [XmlElement(Order = 2)]
        public MISMOIdentifier ServiceProductNameIdentifier = new MISMOIdentifier();
    }

    public class SERVICE_PRODUCT_NAMES
    {
        [XmlElement(Order = 1, ElementName = "SERVICE_PRODUCT_NAME")]
        public List<SERVICE_PRODUCT_NAME> SERVICE_PRODUCT_NAMEList = new List<SERVICE_PRODUCT_NAME>();
    }

    public class SERVICE_PRODUCT_REQUEST
    {
        [XmlElement(Order = 1)]
        public SERVICE_PRODUCT_DETAIL SERVICE_PRODUCT_DETAIL = new SERVICE_PRODUCT_DETAIL();

        [XmlElement(Order = 2)]
        public SERVICE_PRODUCT_NAMES SERVICE_PRODUCT_NAMES = new SERVICE_PRODUCT_NAMES();
    }

    public class SERVICES
    {
        [XmlElement(Order = 1, ElementName = "SERVICE")]
        public List<SERVICE> SERVICEList = new List<SERVICE>();
    }

    public class SUBMITTING_PARTY
    {
        [XmlElement(Order = 1)]
        public MISMOSequenceNumber SubmittingPartySequenceNumber = new MISMOSequenceNumber();

        [XmlElement(Order = 2)]
        public MISMOIdentifier SubmittingPartyTransactionIdentifier = new MISMOIdentifier();
    }

    public class TAXPAYER_IDENTIFIER
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public TaxpayerIdentifierEnum TaxpayerIdentifierType = new TaxpayerIdentifierEnum();

        [XmlElement(Order = 2)]
        public MISMONumericString TaxpayerIdentifierValue = new MISMONumericString();
    }

    public class TAXPAYER_IDENTIFIERS
    {
        [XmlElement(Order = 1, ElementName = "TAXPAYER_IDENTIFIER")]
        public List<TAXPAYER_IDENTIFIER> TAXPAYER_IDENTIFIERList = new List<TAXPAYER_IDENTIFIER>();
    }

    public class TERMS_OF_LOAN
    {
        [XmlElement(Order = 1)]
        public MISMOAmount BaseLoanAmount = new MISMOAmount();

        [XmlElement(Order = 2)]
        public LienPriorityEnum LienPriorityType = new LienPriorityEnum();

        [XmlElement(Order = 3)]
        public MISMOString LienPriorityTypeOtherDescription;
        [XmlIgnore()]
        public bool LienPriorityTypeOtherDescriptionSpecified { get { return LienPriorityTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 4)]
        public LoanPurposeEnum LoanPurposeType;
        [XmlIgnore()]
        public bool LoanPurposeTypeSpecified { get { return LoanPurposeType != null; } set { } }

        [XmlElement(Order = 5)]
        public MISMOString LoanPurposeTypeOtherDescription;
        [XmlIgnore()]
        public bool LoanPurposeTypeOtherDescriptionSpecified { get { return LoanPurposeTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 6)]
        public MortgageEnum MortgageType;
        [XmlIgnore()]
        public bool MortgageTypeSpecified { get { return MortgageType != null; } set { } }

        [XmlElement(Order = 7)]
        public MISMOString MortgageTypeOtherDescription;
        [XmlIgnore()]
        public bool MortgageTypeOtherDescriptionSpecified { get { return MortgageTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 8)]
        public MortgageEnum SupplementalMortgageType;
        [XmlIgnore()]
        public bool SupplementalMortgageTypeSpecified { get { return SupplementalMortgageType != null; } set { } }

        [XmlElement(Order = 9)]
        public MISMOString SupplementalMortgageTypeOtherDescription;
        [XmlIgnore()]
        public bool SupplementalMortgageTypeOtherDescriptionSpecified { get { return SupplementalMortgageTypeOtherDescription != null; } set { } }
    }

    public class TITLE
    {
        [XmlElement(Order = 1)]
        public TITLE_REQUEST TITLE_REQUEST = new TITLE_REQUEST();
    }

    public class TITLE_REQUEST
    {
        [XmlElement(Order = 1)]
        public TITLE_REQUEST_DETAIL TITLE_REQUEST_DETAIL = new TITLE_REQUEST_DETAIL();
    }

    public class TITLE_REQUEST_DETAIL
    {
        [XmlElement(Order = 1)]
        public MISMOString InsuredName = new MISMOString();

        [XmlElement(Order = 2)]
        public NAICTitlePolicyClassificationEnum NAICTitlePolicyClassificationType = new NAICTitlePolicyClassificationEnum();

        [XmlElement(Order = 3)]
        public NamedInsuredEnum NamedInsuredType = new NamedInsuredEnum();

        [XmlElement(Order = 4)]
        public MISMOIdentifier ProcessorIdentifier;
        [XmlIgnore()]
        public bool ProcessorIdentifierSpecified { get { return ProcessorIdentifier != null; } set { } }

        [XmlElement(Order = 5)]
        public MISMODate RequestedClosingDate;
        [XmlIgnore()]
        public bool RequestedClosingDateSpecified { get { return RequestedClosingDate != null; } set { } }

        [XmlElement(Order = 6)]
        public MISMOTime RequestedClosingTime;
        [XmlIgnore()]
        public bool RequestedClosingTimeSpecified { get { return RequestedClosingTime != null; } set { } }

        [XmlElement(Order = 7)]
        public TitleAssociationEnum TitleAssociationType = new TitleAssociationEnum();

        [XmlElement(Order = 8)]
        public MISMOString TitleAssociationTypeOtherDescription;
        [XmlIgnore()]
        public bool TitleAssociationTypeOtherDescriptionSpecified { get { return TitleAssociationTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 9)]
        public MISMOIdentifier TitleOfficeIdentifier;
        [XmlIgnore()]
        public bool TitleOfficeIdentifierSpecified { get { return TitleOfficeIdentifier != null; } set { } }

        [XmlElement(Order = 10)]
        public TitleOwnershipEnum TitleOwnershipType;
        [XmlIgnore()]
        public bool TitleOwnershipTypeSpecified { get { return TitleOwnershipType != null; } set { } }

        [XmlElement(Order = 11)]
        public MISMOString TitleOwnershipTypeOtherDescription;
        [XmlIgnore()]
        public bool TitleOwnershipTypeOtherDescriptionSpecified { get { return TitleOwnershipTypeOtherDescription != null; } set { } }

        [XmlElement(Order = 12)]
        public TitleRequestActionEnum TitleRequestActionType = new TitleRequestActionEnum();

        [XmlElement(Order = 13)]
        public MISMOString TitleRequestCommentDescription;
        [XmlIgnore()]
        public bool TitleRequestCommentDescriptionSpecified { get { return TitleRequestCommentDescription != null; } set { } }

        [XmlElement(Order = 14)]
        public MISMOAmount TitleRequestProposedTitleInsuranceCoverageAmount = new MISMOAmount();

        [XmlElement(Order = 15)]
        public MISMOIdentifier VendorOrderIdentifier;
        [XmlIgnore()]
        public bool VendorOrderIdentifierSpecified { get { return VendorOrderIdentifier != null; } set { } }

        [XmlElement(Order = 16)]
        public MISMOIdentifier VendorTransactionIdentifier;
        [XmlIgnore()]
        public bool VendorTransactionIdentifierSpecified { get { return VendorTransactionIdentifier != null; } set { } }
    }

    public class UNPARSED_LEGAL_DESCRIPTION
    {
        [XmlIgnore()]
        private int m_iSequenceNumber;
        [XmlIgnore()]
        public int SequenceNumber { get { return m_iSequenceNumber; } set { this.m_iSequenceNumber = value; this.SequenceNumberProvided = true; } }
        [XmlIgnore()]
        protected bool SequenceNumberProvided = false;
        [XmlAttribute(AttributeName = "SequenceNumber")]
        public string SequenceNumberSerialized { get { return SequenceNumber.ToString(); } set { } }
        public bool ShouldSerializeSequenceNumberSerialized() { return SequenceNumberProvided; }

        [XmlElement(Order = 1)]
        public MISMOString UnparsedLegalDescription = new MISMOString();
    }

    public class UNPARSED_LEGAL_DESCRIPTIONS
    {
        [XmlElement(Order = 1, ElementName = "UNPARSED_LEGAL_DESCRIPTION")]
        public List<UNPARSED_LEGAL_DESCRIPTION> UNPARSED_LEGAL_DESCRIPTIONList = new List<UNPARSED_LEGAL_DESCRIPTION>();
    }

    #region Enum Containers
    [XmlType(Namespace = "")]
    [XmlInclude(typeof(AddressEnum))]
    [XmlInclude(typeof(AttachmentEnum))]
    [XmlInclude(typeof(BorrowerClassificationEnum))]
    [XmlInclude(typeof(BorrowerRelationshipTitleEnum))]
    [XmlInclude(typeof(ContactPointRoleEnum))]
    [XmlInclude(typeof(LiabilityEnum))]
    [XmlInclude(typeof(LienPriorityEnum))]
    [XmlInclude(typeof(LoanIdentifierEnum))]
    [XmlInclude(typeof(LoanPurposeEnum))]
    [XmlInclude(typeof(MaritalStatusEnum))]
    [XmlInclude(typeof(MortgageEnum))]
    [XmlInclude(typeof(NAICTitlePolicyClassificationEnum))]
    [XmlInclude(typeof(NamedInsuredEnum))]
    [XmlInclude(typeof(PartyRoleEnum))]
    [XmlInclude(typeof(PayoffActionEnum))]
    [XmlInclude(typeof(PreferredResponseFormatEnum))]
    [XmlInclude(typeof(PreferredResponseMethodEnum))]
    [XmlInclude(typeof(PropertyUsageEnum))]
    [XmlInclude(typeof(TaxpayerIdentifierEnum))]
    [XmlInclude(typeof(TitleAssociationEnum))]
    [XmlInclude(typeof(TitleOwnershipEnum))]
    [XmlInclude(typeof(TitleRequestActionEnum))]
    public abstract class MISMOEnumContainer
    {
        [XmlIgnore()]
        private bool m_bIsSensitive;
        [XmlIgnore()]
        public bool IsSensitive { get { return m_bIsSensitive; } set { this.m_bIsSensitive = value; this.IsSensitiveProvided = true; } }
        [XmlIgnore()]
        protected bool IsSensitiveProvided = false;
        [XmlAttribute(AttributeName = "SensitiveIndicator")]
        public string SensitiveIndicatorSerialized { get { return MISMO3_2Convert.ToMISMO3_2Bool(IsSensitive); } set { } }
        public bool ShouldSerializeSensitiveIndicatorSerialized() { return IsSensitiveProvided; }
    }

    public class AddressEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_AddressBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class AttachmentEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_AttachmentBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class BorrowerClassificationEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_BorrowerClassificationBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class BorrowerRelationshipTitleEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_BorrowerRelationshipTitleBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class ContactPointRoleEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_ContactPointRoleBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class LiabilityEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_LiabilityBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class LienPriorityEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_LienPriorityBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class LoanIdentifierEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_LoanIdentifierBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class LoanPurposeEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_LoanPurposeBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class MaritalStatusEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_MaritalStatusBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class MortgageEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_MortgageBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class NAICTitlePolicyClassificationEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_NAICTitlePolicyClassificationBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class NamedInsuredEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_NamedInsuredBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class PartyRoleEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_PartyRoleBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class PayoffActionEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_PayoffActionBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class PreferredResponseFormatEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_PreferredResponseFormatBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class PreferredResponseMethodEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_PreferredResponseMethodBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class PropertyUsageEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_PropertyUsageBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class TaxpayerIdentifierEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_TaxpayerIdentifierBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class TitleAssociationEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_TitleAssociationBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class TitleOwnershipEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_TitleOwnershipBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }

    public class TitleRequestActionEnum : MISMOEnumContainer
    {
        [XmlIgnore()]
        public E_TitleRequestActionBase eValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return Utilities.ConvertEnumToString(eValue); } set { } }
    }
    #endregion

    #region Basic Types
    [XmlType(Namespace = "")]
    [XmlInclude(typeof(MISMOAmount))]
    [XmlInclude(typeof(MISMOCode))]
    [XmlInclude(typeof(MISMOCount))]
    [XmlInclude(typeof(MISMODate))]
    [XmlInclude(typeof(MISMOIdentifier))]
    [XmlInclude(typeof(MISMOIndicator))]
    [XmlInclude(typeof(MISMONumericString))]
    [XmlInclude(typeof(MISMOSequenceNumber))]
    [XmlInclude(typeof(MISMOString))]
    [XmlInclude(typeof(MISMOTime))]
    [XmlInclude(typeof(MISMOValue))]
    [XmlInclude(typeof(MISMOYear))]
    public abstract class MISMOBasicType
    {
        [XmlIgnore()]
        private bool m_bIsSensitive;
        [XmlIgnore()]
        public bool IsSensitive { get { return m_bIsSensitive; } set { this.m_bIsSensitive = value; this.IsSensitiveProvided = true; } }
        [XmlIgnore()]
        protected bool IsSensitiveProvided = false;
        [XmlAttribute(AttributeName = "SensitiveIndicator")]
        public string SensitiveIndicatorSerialized { get { return MISMO3_2Convert.ToMISMO3_2Bool(IsSensitive); } set { } }
        public bool ShouldSerializeSensitiveIndicatorSerialized() { return IsSensitiveProvided; }
    }

    /// <summary>
    /// MISMO annotation:
    /// A Class word of Amount SHOULD identify a data point that is an amount of money. It SHALL NOT contain any punctuation other than the decimal point or sign values.
    /// The decimal point is ALWAYS the US nationalization character (.) in the designated character encoding (UTF-8 assumed when not stated)
    /// EXAMPLE: an Unpaid Principle Balance of $100,000.12 would be expressed as 100000.12.
    /// </summary>
    public class MISMOAmount : MISMOBasicType
    {
        [XmlAttribute(DataType = "anyURI")]
        public string CurrencyURI;
        public bool ShouldSerializeCurrencyURI() { return !string.IsNullOrEmpty(CurrencyURI); }

        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class MISMOCode : MISMOBasicType
    {
        [XmlAttribute(DataType = "anyURI")]
        public string CodeOwnerURI;
        public bool ShouldSerializeCodeOwnerURI() { return !string.IsNullOrEmpty(CodeOwnerURI); }

        [XmlAttribute()]
        public string CodeEffectiveDate;
        public bool ShouldSerializeCodeEffectiveDate() { return !string.IsNullOrEmpty(CodeEffectiveDate); }

        [XmlText(Type = typeof(string))]
        public string Value;
    }

    // The MISMOCount base type is int
    public class MISMOCount : MISMOBasicType
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }

    /// <summary>
    /// MISMO annotation:
    /// Numeric value representing a specific day of the Gregorian Calendar made up of a combination of day, month, and year.
	/// Full dates MUST be represented in the extended format YYYY-MM-DD
    /// Padded zeros must be used [for MM, DD]
    /// - is the required separator between YYYY-MM-DD
    /// </summary>
    public class MISMODate : MISMOBasicType
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class MISMOIdentifier : MISMOBasicType
    {
        [XmlAttribute(DataType = "anyURI")]
        public string IdentifierOwnerURI;
        public bool ShouldSerializeIdentifierOwnerURI() { return !string.IsNullOrEmpty(IdentifierOwnerURI); }

        [XmlAttribute()]
        public string IdentifierEffectiveDate;
        public bool ShouldSerializeIdentifierEffectiveDate() { return !string.IsNullOrEmpty(IdentifierEffectiveDate); }

        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class MISMOIndicator : MISMOBasicType
    {
        [XmlIgnore()]
        public bool bValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return MISMO3_2Convert.ToMISMO3_2Bool(bValue); } set { } }
    }

    public class MISMONumericString : MISMOBasicType
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class MISMOSequenceNumber : MISMOBasicType
    {
        [XmlIgnore()]
        public int iValue;
        [XmlText(Type = typeof(string))]
        public string Value { get { return iValue.ToString(); } set { } }
    }

    public class MISMOString : MISMOBasicType
    {
        [XmlAttribute()]
        public string lang = "en";
        public bool ShouldSerializelang() { return !string.IsNullOrEmpty(lang); }

        [XmlText(Type = typeof(string))]
        public string Value;
    }

    // The MISMOTime base type is time (DateTime)
    public class MISMOTime : MISMOBasicType
    {
        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class MISMOValue : MISMOBasicType
    {
        [XmlAttribute()]
        public string lang = "en";
        public bool ShouldSerializelang() { return !string.IsNullOrEmpty(lang); }

        [XmlAttribute(DataType = "anyURI")]
        public string AlgorithmURI;
        public bool ShouldSerializeAlgorithmURI() { return !string.IsNullOrEmpty(AlgorithmURI); }

        [XmlText(Type = typeof(string))]
        public string Value;
    }

    public class MISMOYear : MISMOBasicType
    {
        [XmlText(Type = typeof(String), DataType = "gYear")]
        public string Value;
    }
    #endregion
}
