﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LendersOffice.Conversions.Mismo3.Version2.Request;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Conversions.Mismo3.Version2
{
    public static class MISMO3_2Convert
    {
        public static AddressEnum ToMISMO3_2(E_AddressBase eAddressType, bool bIsSensitive)
        {
            AddressEnum address = new AddressEnum();
            address.eValue = eAddressType;
            address.IsSensitive = bIsSensitive;

            return address;
        }

        public static AttachmentEnum ToMISMO3_2(E_sGseSpT sGseSpT, bool bIsSensitive)
        {
            AttachmentEnum attachment = new AttachmentEnum();
            attachment.IsSensitive = bIsSensitive;
            attachment.eValue = ToMISMO3_2AttachmentType(sGseSpT);

            return attachment;
        }

        public static BorrowerClassificationEnum ToMISMO3_2BorrowerClassification(bool bIsPrimaryWageEarner, bool bIsSensitive)
        {
            BorrowerClassificationEnum borrowerClass = new BorrowerClassificationEnum();
            borrowerClass.eValue = bIsPrimaryWageEarner ? E_BorrowerClassificationBase.Primary : E_BorrowerClassificationBase.Secondary;
            borrowerClass.IsSensitive = bIsSensitive;

            return borrowerClass;
        }

        public static BorrowerRelationshipTitleEnum ToMISMO3_2(E_aRelationshipTitleT eBorrowerRelationshipTitleType, bool bIsSensitive)
        {
            BorrowerRelationshipTitleEnum borrowerRelationship = new BorrowerRelationshipTitleEnum();
            borrowerRelationship.eValue = ToMISMO3_2BorrowerRelationshipTitleType(eBorrowerRelationshipTitleType);
            borrowerRelationship.IsSensitive = bIsSensitive;

            return borrowerRelationship;
        }

        public static ContactPointRoleEnum ToMISMO3_2(E_ContactPointRoleBase eContactPointType, bool bIsSensitive)
        {
            ContactPointRoleEnum contactPointType = new ContactPointRoleEnum();
            contactPointType.eValue = eContactPointType;
            contactPointType.IsSensitive = bIsSensitive;

            return contactPointType;
        }

        public static LiabilityEnum ToMISMO3_2(E_DebtT liabilityType, bool bIsSensitive)
        {
            LiabilityEnum liability = new LiabilityEnum();
            liability.IsSensitive = bIsSensitive;
            liability.eValue = ToMISMO3_2LiabilityType(liabilityType, false);

            return liability;
        }

        public static LiabilityEnum ToMISMO3_2(E_DebtT liabilityType, bool bIsSubjectProperty1stMortgage, string sDescription, bool bIsSensitive)
        {
            LiabilityEnum liability = new LiabilityEnum();
            liability.IsSensitive = bIsSensitive;

            liability.eValue = ToMISMO3_2LiabilityType(sDescription);

            if (liability.eValue == E_LiabilityBase.None)
            {
                liability.eValue = ToMISMO3_2LiabilityType(liabilityType, bIsSubjectProperty1stMortgage);
            }
            return liability;
        }

        public static LienPriorityEnum ToMISMO3_2(E_sLienPosT sLienPosT, bool bIsSensitive)
        {
            LienPriorityEnum lienPriority = new LienPriorityEnum();
            lienPriority.eValue = ToMISMO3_2LienPriorityType(sLienPosT);
            lienPriority.IsSensitive = bIsSensitive;

            return lienPriority;
        }

        public static LoanIdentifierEnum ToMISMO3_2(E_LoanIdentifierBase loanIdentifierType, bool bIsSensitive)
        {
            LoanIdentifierEnum loanIdentifier = new LoanIdentifierEnum();
            loanIdentifier.eValue = loanIdentifierType;
            loanIdentifier.IsSensitive = bIsSensitive;

            return loanIdentifier;
        }

        public static LoanPurposeEnum ToMISMO3_2(E_sLPurposeT sLPurposeT, bool bIsSensitive)
        {
            LoanPurposeEnum loanPurpose = new LoanPurposeEnum();
            loanPurpose.eValue = ToMISMO3_2LoanPurposeType(sLPurposeT);
            loanPurpose.IsSensitive = bIsSensitive;

            return loanPurpose;
        }

        public static MaritalStatusEnum ToMISMO3_2(E_aBMaritalStatT aBMaritalStatT, bool bIsSensitive)
        {
            MaritalStatusEnum maritalStatus = new MaritalStatusEnum();
            maritalStatus.eValue = ToMISMO3_2MaritalStatusType(aBMaritalStatT);
            maritalStatus.IsSensitive = bIsSensitive;

            return maritalStatus;
        }

        public static MortgageEnum ToMISMO3_2(E_sLT sLT, bool bIsSensitive)
        {
            MortgageEnum mortgageType = new MortgageEnum();
            mortgageType.eValue = ToMISMO3_2MortgageType(sLT);
            mortgageType.IsSensitive = bIsSensitive;

            return mortgageType;
        }

        public static NAICTitlePolicyClassificationEnum ToMISMO3_2(E_NAICTitlePolicyClassificationBase ePolicyClassType, bool bIsSensitive)
        {
            NAICTitlePolicyClassificationEnum policyClassificationType = new NAICTitlePolicyClassificationEnum();
            policyClassificationType.eValue = ePolicyClassType;
            policyClassificationType.IsSensitive = bIsSensitive;

            return policyClassificationType;
        }

        public static NamedInsuredEnum ToMISMO3_2(E_cTitleInsurancePolicyT ePolicyType, bool bIsSensitive)
        {
            NamedInsuredEnum namedInsuredType = new NamedInsuredEnum();
            namedInsuredType.eValue = ToMISMO3_2NamedInsuredType(ePolicyType);
            namedInsuredType.IsSensitive = bIsSensitive;

            return namedInsuredType;
        }

        public static PartyRoleEnum ToMISMO3_2(E_PartyRoleBase ePartyRoleType, bool bIsSensitive)
        {
            PartyRoleEnum partyRole = new PartyRoleEnum();
            partyRole.eValue = ePartyRoleType;
            partyRole.IsSensitive = bIsSensitive;

            return partyRole;
        }

        public static PayoffActionEnum ToMISMO3_2(bool bToBePaidOff, bool bResubordinate, bool bIsSensitive, E_DebtRegularT debtType, bool bEscrowServicesOrdered)
        {
            PayoffActionEnum payOffAction = new PayoffActionEnum();
            payOffAction.eValue = ToMISMO3_2PayOffActionType(bToBePaidOff, bResubordinate, debtType, bEscrowServicesOrdered);
            payOffAction.IsSensitive = bIsSensitive;

            return payOffAction;
        }

        public static PreferredResponseFormatEnum ToMISMO3_2(E_PreferredResponseFormatBase eResponseFormatType, bool bIsSensitive)
        {
            PreferredResponseFormatEnum preferredResponseFormat = new PreferredResponseFormatEnum();
            preferredResponseFormat.eValue = eResponseFormatType;
            preferredResponseFormat.IsSensitive = bIsSensitive;

            return preferredResponseFormat;
        }

        public static PreferredResponseMethodEnum ToMISMO3_2(E_PreferredResponseMethodBase eResponseMethodType, bool bIsSensitive)
        {
            PreferredResponseMethodEnum preferredResponseMethod = new PreferredResponseMethodEnum();
            preferredResponseMethod.eValue = eResponseMethodType;
            preferredResponseMethod.IsSensitive = bIsSensitive;

            return preferredResponseMethod;
        }

        public static PropertyUsageEnum ToMISMO3_2(E_sOccT sOccT, bool bIsSensitive)
        {
            PropertyUsageEnum propertyUsage = new PropertyUsageEnum();
            propertyUsage.IsSensitive = bIsSensitive;
            propertyUsage.eValue = ToMISMO3_2PropertyUsageType(sOccT);

            return propertyUsage;
        }

        public static TaxpayerIdentifierEnum ToMISMO3_2(E_TaxpayerIdentifierBase eTaxPayerIDType, bool bIsSensitive)
        {
            TaxpayerIdentifierEnum taxPayerIDType = new TaxpayerIdentifierEnum();
            taxPayerIDType.eValue = eTaxPayerIDType;
            taxPayerIDType.IsSensitive = bIsSensitive;

            return taxPayerIDType;
        }

        public static TitleAssociationEnum ToMISMO3_2TitleAssociation(string sPolicyType, bool bIsSensitive)
        {
            TitleAssociationEnum titleAssociationType = new TitleAssociationEnum();
            titleAssociationType.eValue = ToMISMO3_2TitleAssociationType(sPolicyType);
            titleAssociationType.IsSensitive = bIsSensitive;

            return titleAssociationType;
        }

        public static TitleOwnershipEnum ToMISMO3_2(E_TitleOwnershipBase eTitleOwnershipType, bool bIsSensitive)
        {
            TitleOwnershipEnum ownershipType = new TitleOwnershipEnum();
            ownershipType.eValue = eTitleOwnershipType;
            ownershipType.IsSensitive = bIsSensitive;

            return ownershipType;
        }

        public static TitleRequestActionEnum ToMISMO3_2(E_TitleRequestActionBase eTitleRequestType, bool bIsSensitive)
        {
            TitleRequestActionEnum titleRequestActionType = new TitleRequestActionEnum();
            titleRequestActionType.eValue = eTitleRequestType;
            titleRequestActionType.IsSensitive = bIsSensitive;

            return titleRequestActionType;
        }

        public static MISMOAmount ToMISMO3_2MISMOAmount(string sValue, bool bIsSensitive)
        {
            MISMOAmount amount = new MISMOAmount();
            // Todo: find a URI for US currency
            //amount.CurrencyURI;
            amount.IsSensitive = bIsSensitive;
            amount.Value = sValue;

            return amount;
        }

        public static MISMOCode ToMISMO3_2MISMOCode(string sValue, bool bIsSensitive)
        {
            MISMOCode mismoCode = new MISMOCode();
            // If the effective date and URI are known then use the overloaded method with those
            //mismoCode.CodeEffectiveDate
            //mismoCode.CodeOwnerURI
            mismoCode.IsSensitive = bIsSensitive;
            mismoCode.Value = sValue;

            return mismoCode;
        }

        public static MISMOCode ToMISMO3_2MISMOCode(string sValue, bool bIsSensitive, string sCodeEffectiveDate, string sCodeOwnerURI)
        {
            MISMOCode mismoCode = new MISMOCode();
            mismoCode.CodeEffectiveDate = sCodeEffectiveDate;
            mismoCode.CodeOwnerURI = sCodeOwnerURI;
            mismoCode.IsSensitive = bIsSensitive;
            mismoCode.Value = sValue;

            return mismoCode;
        }

        public static MISMOCount ToMISMO3_2MISMOCount(string sValue, bool bIsSensitive)
        {
            MISMOCount count = new MISMOCount();
            count.IsSensitive = bIsSensitive;
            count.Value = sValue;

            return count;
        }

        public static MISMOIdentifier ToMISMO3_2Identifier(string sValue, bool bIsSensitive)
        {
            MISMOIdentifier identifier = new MISMOIdentifier();
            // If the effective date and URI are known then use the overloaded method with those
            //identifier.IdentifierEffectiveDate;
            //identifier.IdentifierOwnerURI;
            identifier.IsSensitive = bIsSensitive;
            identifier.Value = sValue;

            return identifier;
        }

        public static MISMOIdentifier ToMISMO3_2Identifier(string sValue, bool bIsSensitive, string sIdentifierEffectiveDate, string sIdentifierOwnerURI)
        {
            MISMOIdentifier identifier = new MISMOIdentifier();
            identifier.IdentifierEffectiveDate = sIdentifierEffectiveDate;
            identifier.IdentifierOwnerURI = sIdentifierOwnerURI;
            identifier.IsSensitive = bIsSensitive;
            identifier.Value = sValue;

            return identifier;
        }

        public static MISMOIndicator ToMISMO3_2(bool bValue, bool bIsSensitive)
        {
            MISMOIndicator indicator = new MISMOIndicator();
            indicator.bValue = bValue;
            indicator.IsSensitive = bIsSensitive;

            return indicator;
        }

        public static MISMONumericString ToMISMO3_2MISMONumericString(string sValue, bool bIsSensitive)
        {
            MISMONumericString mismoString = new MISMONumericString();
            mismoString.IsSensitive = bIsSensitive;
            mismoString.Value = Regex.Replace(sValue, @"[\D]", ""); //^0-9

            return mismoString;
        }

        public static MISMOSequenceNumber ToMISMO3_2(int iValue, bool bIsSensitive)
        {
            MISMOSequenceNumber mismoSequence = new MISMOSequenceNumber();
            mismoSequence.IsSensitive = bIsSensitive;
            mismoSequence.iValue = iValue;

            return mismoSequence;
        }

        public static MISMOString ToMISMO3_2(string sValue, bool bIsSensitive)
        {
            MISMOString mismoString = new MISMOString();
            mismoString.IsSensitive = bIsSensitive;
            mismoString.Value = sValue;

            return mismoString;
        }

        public static MISMOValue ToMISMO3_2MISMOValue(string sValue, bool bIsSensitive)
        {
            MISMOValue mismoValue = new MISMOValue();
            // If the URI is known then use the overloaded method
            //mismoValue.AlgorithmURI;
            mismoValue.IsSensitive = bIsSensitive;
            mismoValue.Value = sValue;

            return mismoValue;
        }

        public static MISMOValue ToMISMO3_2MISMOValue(string sValue, bool bIsSensitive, string sAlgorithmURI)
        {
            MISMOValue mismoValue = new MISMOValue();
            mismoValue.AlgorithmURI = sAlgorithmURI;
            mismoValue.IsSensitive = bIsSensitive;
            mismoValue.Value = sValue;

            return mismoValue;
        }

        public static MISMOYear ToMISMO3_2MISMOYear(string sValue, bool bIsSensitive)
        {
            MISMOYear year = new MISMOYear();
            year.IsSensitive = bIsSensitive;
            year.Value = sValue;

            return year;
        }

        public static string ToMISMO3_2Bool(bool bValue)
        {
            return bValue.ToString().ToLower();
        }

        #region MISMO 3.2 Enumerated Types
        private static E_AttachmentBase ToMISMO3_2AttachmentType(E_sGseSpT sGseSpT)
        {
            switch (sGseSpT)
            {
                case E_sGseSpT.Attached:
                case E_sGseSpT.Condominium:
                    return E_AttachmentBase.Attached;
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.Detached:
                case E_sGseSpT.DetachedCondominium:
                    return E_AttachmentBase.Detached;
                case E_sGseSpT.HighRiseCondominium:
                    return E_AttachmentBase.Attached;
                case E_sGseSpT.LeaveBlank:
                    return E_AttachmentBase.None;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return E_AttachmentBase.Detached;
                default:
                    throw new UnhandledEnumException(sGseSpT);
            }
        }

        private static E_BorrowerRelationshipTitleBase ToMISMO3_2BorrowerRelationshipTitleType(E_aRelationshipTitleT eBorrowerRelationshipTitleType)
        {
            switch (eBorrowerRelationshipTitleType)
            {
                case E_aRelationshipTitleT.AHusbandAndWife:
                    return E_BorrowerRelationshipTitleBase.AHusbandAndWife;
                case E_aRelationshipTitleT.AMarriedMan:
                case E_aRelationshipTitleT.AMarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedManAsToAnUndividedHalfInterest:
                    return E_BorrowerRelationshipTitleBase.AMarriedMan;
                case E_aRelationshipTitleT.AMarriedPerson:
                    return E_BorrowerRelationshipTitleBase.AMarriedPerson;
                case E_aRelationshipTitleT.AMarriedWoman:
                case E_aRelationshipTitleT.AMarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedWomanAsToAnUndividedHalfInterest:
                    return E_BorrowerRelationshipTitleBase.AMarriedWoman;
                case E_aRelationshipTitleT.AnUnmarriedMan:
                case E_aRelationshipTitleT.AnUnmarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedManAsToAnUndividedHalfInterest:
                    return E_BorrowerRelationshipTitleBase.AnUnmarriedMan;
                case E_aRelationshipTitleT.AnUnmarriedPerson:
                    return E_BorrowerRelationshipTitleBase.AnUnmarriedPerson;
                case E_aRelationshipTitleT.AnUnmarriedWoman:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsToAnUndividedHalfInterest:
                    return E_BorrowerRelationshipTitleBase.AnUnmarriedWoman;
                case E_aRelationshipTitleT.ASingleMan:
                case E_aRelationshipTitleT.ASingleManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleManAsToAnUndividedHalfInterest:
                    return E_BorrowerRelationshipTitleBase.ASingleMan;
                case E_aRelationshipTitleT.ASinglePerson:
                    return E_BorrowerRelationshipTitleBase.ASinglePerson;
                case E_aRelationshipTitleT.ASingleWoman:
                case E_aRelationshipTitleT.ASingleWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleWomanAsToAnUndividedHalfInterest:
                    return E_BorrowerRelationshipTitleBase.ASingleWoman;
                case E_aRelationshipTitleT.AWidow:
                    return E_BorrowerRelationshipTitleBase.AWidow;
                case E_aRelationshipTitleT.AWidower:
                    return E_BorrowerRelationshipTitleBase.AWidower;
                case E_aRelationshipTitleT.AWifeAndHusband:
                    return E_BorrowerRelationshipTitleBase.AWifeAndHusband;
                case E_aRelationshipTitleT.ChiefExecutiveOfficer:
                case E_aRelationshipTitleT.DomesticPartners:
                case E_aRelationshipTitleT.GeneralPartner:
                    return E_BorrowerRelationshipTitleBase.Other;
                case E_aRelationshipTitleT.HerHusband:
                    return E_BorrowerRelationshipTitleBase.HerHusband;
                case E_aRelationshipTitleT.HisWife:
                    return E_BorrowerRelationshipTitleBase.HisWife;
                case E_aRelationshipTitleT.HusbandAndWife:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityProperty:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityPropertyWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenants:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenantsWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsByTheEntirety:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsInCommon:
                case E_aRelationshipTitleT.HusbandAndWifeAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.HusbandAndWifeTenancyByTheEntirety:
                    return E_BorrowerRelationshipTitleBase.AHusbandAndWife;
                case E_aRelationshipTitleT.LeaveBlank:
                    return E_BorrowerRelationshipTitleBase.None;
                case E_aRelationshipTitleT.NotApplicable:
                    return E_BorrowerRelationshipTitleBase.NotApplicable;
                case E_aRelationshipTitleT.Other:
                case E_aRelationshipTitleT.PersonalGuarantor:
                case E_aRelationshipTitleT.President:
                case E_aRelationshipTitleT.Secretary:
                case E_aRelationshipTitleT.TenancyByEntirety:
                case E_aRelationshipTitleT.TenantsByTheEntirety:
                case E_aRelationshipTitleT.Treasurer:
                case E_aRelationshipTitleT.Trustee:
                case E_aRelationshipTitleT.VicePresident:
                    return E_BorrowerRelationshipTitleBase.Other;
                case E_aRelationshipTitleT.WifeAndHusband:
                case E_aRelationshipTitleT.WifeAndHusbandAsCommunityProperty:
                case E_aRelationshipTitleT.WifeAndHusbandAsJointTenants:
                case E_aRelationshipTitleT.WifeAndHusbandAsTenantsInCommon:
                    return E_BorrowerRelationshipTitleBase.AWifeAndHusband;
                default:
                    throw new UnhandledEnumException(eBorrowerRelationshipTitleType);
            }
        }

        private static E_LiabilityBase ToMISMO3_2LiabilityType(string sLiabilityDescription)
        {
            switch (sLiabilityDescription.ToLower().TrimWhitespaceAndBOM())
            {
                case "heloc":
                case "home equity line of credit": return E_LiabilityBase.HELOC;
                case "lease payments": return E_LiabilityBase.LeasePayments;
                case "liens": return E_LiabilityBase.CollectionsJudgmentsAndLiens;
                case "taxes": return E_LiabilityBase.Taxes;
                default: return E_LiabilityBase.None; // fall back on the actual liability type
            }
        }

        private static E_LiabilityBase ToMISMO3_2LiabilityType(E_DebtT liabilityType, bool bIsSubjectProperty1stMortgage)
        {
            switch (liabilityType)
            {
                case E_DebtT.Alimony:
                case E_DebtT.JobRelatedExpense:
                case E_DebtT.ChildSupport:
                case E_DebtT.Other:
                    return E_LiabilityBase.Other;
                case E_DebtT.Installment: return E_LiabilityBase.Installment;
                case E_DebtT.Mortgage: return bIsSubjectProperty1stMortgage ? E_LiabilityBase.FirstMortgageBeingFinanced : E_LiabilityBase.MortgageLoan;
                case E_DebtT.Open: return E_LiabilityBase.Open30DayChargeAccount;
                case E_DebtT.Revolving: return E_LiabilityBase.Revolving;
                default:
                    throw new UnhandledEnumException(liabilityType);
            }
        }

        private static E_LienPriorityBase ToMISMO3_2LienPriorityType(E_sLienPosT sLienPosT)
        {
            switch(sLienPosT)
            {
                case E_sLienPosT.First: return E_LienPriorityBase.FirstLien;
                case E_sLienPosT.Second: return E_LienPriorityBase.SecondLien;
                default:
                    throw new UnhandledEnumException(sLienPosT);
            }
        }

        private static E_LoanPurposeBase ToMISMO3_2LoanPurposeType(E_sLPurposeT sLPurposeT)
        {
            switch(sLPurposeT)
            {
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    return E_LoanPurposeBase.Purchase;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                    return E_LoanPurposeBase.Refinance;
                case E_sLPurposeT.Other:
                    return E_LoanPurposeBase.Other;
                case E_sLPurposeT.Purchase:
                    return E_LoanPurposeBase.Purchase;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.HomeEquity:
                    return E_LoanPurposeBase.Refinance;
                default:
                    throw new UnhandledEnumException(sLPurposeT);
            }
        }

        private static E_MaritalStatusBase ToMISMO3_2MaritalStatusType(E_aBMaritalStatT aBMaritalStatT)
        {
            switch(aBMaritalStatT)
            {
                case E_aBMaritalStatT.LeaveBlank: return E_MaritalStatusBase.None;
                case E_aBMaritalStatT.Married: return E_MaritalStatusBase.Married;
                case E_aBMaritalStatT.NotMarried: return E_MaritalStatusBase.Unmarried;
                case E_aBMaritalStatT.Separated: return E_MaritalStatusBase.Separated;
                default:
                    throw new UnhandledEnumException(aBMaritalStatT);
            }
        }

        private static E_MortgageBase ToMISMO3_2MortgageType(E_sLT sLT)
        {
            switch(sLT)
            {
                case E_sLT.Conventional: return E_MortgageBase.Conventional;
                case E_sLT.FHA: return E_MortgageBase.FHA;
                case E_sLT.Other: return E_MortgageBase.Other;
                case E_sLT.UsdaRural: return E_MortgageBase.USDARuralDevelopment;
                case E_sLT.VA: return E_MortgageBase.VA;
                default:
                    throw new UnhandledEnumException(sLT);
            }
        }

        private static E_NamedInsuredBase ToMISMO3_2NamedInsuredType(E_cTitleInsurancePolicyT ePolicyType)
        {
            switch(ePolicyType)
            {
                case E_cTitleInsurancePolicyT.Lender: return E_NamedInsuredBase.Lender;
                case E_cTitleInsurancePolicyT.Owner: return E_NamedInsuredBase.Owner;
                case E_cTitleInsurancePolicyT.Simultaneous: return E_NamedInsuredBase.None;
                default:
                    throw new UnhandledEnumException(ePolicyType);
            }
        }

        public static E_PartyRoleBase ToMISMO3_2PartyRoleType(E_aTypeT eBorrowerTitleType)
        {
            switch(eBorrowerTitleType)
            {
                case E_aTypeT.CoSigner: return E_PartyRoleBase.Cosigner;
                case E_aTypeT.Individual: return E_PartyRoleBase.Borrower;
                case E_aTypeT.NonTitleSpouse: return E_PartyRoleBase.NonTitleSpouse;
                case E_aTypeT.TitleOnly: return E_PartyRoleBase.Borrower;
                default:
                    throw new UnhandledEnumException(eBorrowerTitleType);
            }
        }

        private static E_PayoffActionBase ToMISMO3_2PayOffActionType(bool bToBePaidOff, bool bResubordinate, E_DebtRegularT debtType, bool bEscrowServicesOrdered)
        {
            E_PayoffActionBase payoffType = E_PayoffActionBase.None;

            if(bEscrowServicesOrdered && (debtType == E_DebtRegularT.Mortgage))
            {
                // To-do: if a "lender holds note" indicator is added to the liability, use that to determine whether the lender will order payoff
                if (bToBePaidOff && bResubordinate)
                {
                    payoffType = E_PayoffActionBase.TitleCompanyOrderBothPayoffSubordinate;
                }
                else if (bResubordinate)
                {
                    payoffType = E_PayoffActionBase.TitleCompanyOrderSubordinate;
                }
                else if (bToBePaidOff)
                {
                    payoffType = E_PayoffActionBase.TitleCompanyOrderPayoff;
                }
            }
            else if(bEscrowServicesOrdered) // installment / open / revolving / other
            {
                if (bToBePaidOff && bResubordinate)
                {
                    payoffType = E_PayoffActionBase.TitleCompanyOrderBothPayoffSubordinate;
                }
                else if (bResubordinate)
                {
                    payoffType = E_PayoffActionBase.TitleCompanyOrderSubordinate;
                }
                else if (bToBePaidOff)
                {
                    payoffType = E_PayoffActionBase.TitleCompanyOrderPayoff;
                }
            }

            return payoffType;
        }

        private static E_PropertyUsageBase ToMISMO3_2PropertyUsageType(E_sOccT sOccT)
        {
            switch (sOccT)
            {
                case E_sOccT.PrimaryResidence: return E_PropertyUsageBase.PrimaryResidence;
                case E_sOccT.SecondaryResidence: return E_PropertyUsageBase.SecondHome;
                case E_sOccT.Investment: return E_PropertyUsageBase.Investment;
                default:
                    throw new UnhandledEnumException(sOccT);
            }
        }

        private static E_TitleAssociationBase ToMISMO3_2TitleAssociationType(string sPolicyType)
        {
            sPolicyType = sPolicyType.TrimWhitespaceAndBOM();
            if(sPolicyType.StartsWith("ALTA", StringComparison.OrdinalIgnoreCase))
            {
                return E_TitleAssociationBase.ALTA;
            }
            else if(sPolicyType.StartsWith("CLTA", StringComparison.OrdinalIgnoreCase))
            {
                return E_TitleAssociationBase.CLTA;
            }
            else if(sPolicyType.StartsWith("TLTA", StringComparison.OrdinalIgnoreCase))
            {
                return E_TitleAssociationBase.TLTA;
            }

            return E_TitleAssociationBase.Other;
        }
        #endregion
    }
}
