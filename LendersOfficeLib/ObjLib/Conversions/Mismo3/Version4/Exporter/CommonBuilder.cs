﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.Trust;
    using LendersOffice.ObjLib.Hmda;
    using LendersOffice.ObjLib.TRID2;
    using Mismo3Specification.Version4Schema;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Class that holds common builder and helper methods for the 3.4 exporter.
    /// </summary>
    public static class CommonBuilder
    {
        /// <summary>
        /// Retrieves the closing cost fee associated with the mortgage insurance escrow.
        /// </summary>
        /// <param name="loanData">The loan data.</param>
        /// <returns>The closing cost fee associated with the mortgage insurance escrow.</returns>
        public static BorrowerClosingCostFee GetMortgageInsuranceEscrowFee(CPageData loanData)
        {
            Func<BaseClosingCostFee, bool> setFilter =
                ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(
                    loanData.sClosingCostSet,
                    false,
                    loanData.sOriginatorCompensationPaymentSourceT,
                    loanData.sDisclosureRegulationT,
                    loanData.sGfeIsTPOTransaction);

            Func<BaseClosingCostFee, bool> trueFilter =
                fee => setFilter(fee)
                && fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId;

            return (BorrowerClosingCostFee)loanData.sClosingCostSet.GetFees(trueFilter).FirstOrDefault();
        }

        /// <summary>
        /// Gets prepaid fees from the closing cost set.
        /// </summary>
        /// <param name="dataLoan">The data loan to extract the prepaids from.</param>
        /// <returns>A set of prepaid fees.</returns>
        public static IEnumerable<BorrowerClosingCostFee> GetPrepaidFees(CPageData dataLoan)
        {
            BorrowerClosingCostFeeSection prepaids = (BorrowerClosingCostFeeSection)dataLoan.sClosingCostSet.GetSection(E_ClosingCostViewT.LoanClosingCost, E_IntegratedDisclosureSectionT.SectionF);

            if (prepaids == null || prepaids.FilteredClosingCostFeeList.Count(f => ((BorrowerClosingCostFee)f).TotalAmount != 0.00m) == 0)
            {
                return Enumerable.Empty<BorrowerClosingCostFee>();
            }

            return prepaids.FilteredClosingCostFeeList.Cast<BorrowerClosingCostFee>();
        }

        /// <summary>
        /// Indicates whether TRID 2017 subordinate financing data should be exported.
        /// </summary>
        /// <param name="lienPosition">The lien position.</param>
        /// <param name="isOtherFinancingNew">Indicates whether other financing is new.</param>
        /// <param name="concurrentSubordinateFinancing">The amount of concurrent subordinate financing.</param>
        /// <param name="regulationVersion">The loan regulation version.</param>
        /// <returns>A boolean indicating whether TRID 2017 subordinate financing data should be exported.</returns>
        public static bool ShouldSendTrid2017SubordinateFinancingData(E_sLienPosT lienPosition, bool isOtherFinancingNew, decimal concurrentSubordinateFinancing, TridTargetRegulationVersionT regulationVersion)
        {
            if (regulationVersion == TridTargetRegulationVersionT.TRID2017)
            {
                if (lienPosition == E_sLienPosT.First)
                {
                    if (isOtherFinancingNew && concurrentSubordinateFinancing > 0)
                    {
                        return true;
                    }
                }
                else if (lienPosition == E_sLienPosT.Second)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Creates a PAID_TO container with information regarding the company of the given agent, the recipient of a fee/payable. 
        /// </summary>
        /// <param name="paidToContact">The contact record associated with the entity to which the fee is paid.</param>
        /// <param name="companyName">The name of the company or legal entity to which the fee is paid.</param>
        /// <param name="beneficiaryAgentId">
        /// The ID of the <see cref="CAgentFields"/> 
        /// entry in the loan's contact list which is the beneficiary of the payment.
        /// </param>
        /// <returns>A PAID_TO container with information regarding the company of the given agent, the recipient of a fee/payable.</returns>
        public static PAID_TO CreatePaidTo(CAgentFields paidToContact, string companyName, Guid beneficiaryAgentId)
        {
            bool validContact = paidToContact != null && paidToContact.IsValid && !paidToContact.IsNewRecord;

            if (!validContact && string.IsNullOrEmpty(companyName))
            {
                return null;
            }

            var paidTo = new PAID_TO();

            if (validContact)
            {
                var addresses = CreateAddresses(paidToContact);

                if (addresses.AddressList != null && addresses.AddressList.Count(a => a != null) > 0)
                {
                    paidTo.Address = addresses.AddressList.Where(a => a != null).FirstOrDefault();
                }

                paidTo.LegalEntity = CreateLegalEntity(paidToContact, string.IsNullOrEmpty(companyName) ? paidToContact.CompanyName : companyName, ContactPointRoleBase.Work);
                paidTo.Extension = new PAID_TO_EXTENSION
                {
                    Other = new LQB_PAID_TO_EXTENSION
                    {
                        BeneficiaryAgentId = ToMismoIdentifier(beneficiaryAgentId.ToString())
                    }
                };
            }
            else
            {
                paidTo.LegalEntity = CreateLegalEntity(companyName);
            }

            return paidTo;
        }

        /// <summary>
        /// Creates ADDRESSES container holding one ADDRESS sub-container populated with information from Common Library Address object.
        /// </summary>
        /// <param name="address">Common Library Address object used to populate ADDRESS sub-container.</param>
        /// <param name="addressType">The type of address. E.G. current | mailing | etc.</param>
        /// <param name="countyName">The name of the county.</param>
        /// <param name="countyFIPSCode">The FIPS code assigned to the county.</param>
        /// <returns>Returns an ADDRESSES container holding one ADDRESS sub-container populated with information from Common Library Address object.</returns>
        public static ADDRESSES CreateAddresses(CommonLib.Address address, AddressBase addressType, string countyName, string countyFIPSCode)
        {
            return CreateAddresses(
                address.StreetAddress,
                address.City,
                address.State,
                address.Zipcode,
                addressType,
                countyName,
                countyFIPSCode);
        }

        /// <summary>
        /// Creates ADDRESSES container holding one ADDRESS sub-container populated with agent's address information. 
        /// </summary>
        /// <param name="agent">The agent object from which ADDRESS information is populated.</param>
        /// <returns>Returns ADDRESSES container holding one ADDRESS sub-container populated with agent's address information.</returns>
        public static ADDRESSES CreateAddresses(CAgentFields agent)
        {
            if (agent == null || !agent.IsValid || agent.IsNewRecord)
            {
                return null;
            }

            var addresses = new ADDRESSES();
            addresses.AddressList.Add(
                CreateAddress(
                agent.StreetAddr,
                agent.City,
                agent.State,
                agent.Zip,
                agent.County,
                string.Empty,
                string.Empty,
                AddressBase.Blank,
                GetNextSequenceNumber(addresses.AddressList)));

            return addresses;
        }

        /// <summary>
        /// Creates ADDRESSES container holding one ADDRESS sub-container populated with provider's address information. 
        /// </summary>
        /// <param name="provider">The settlement service provider object from which ADDRESS information is populated.</param>
        /// <returns>Returns ADDRESSES container holding one ADDRESS sub-container populated with provider's address information.</returns>
        public static ADDRESSES CreateAddresses(SettlementServiceProvider provider)
        {
            if (provider == null)
            {
                return null;
            }

            var addresses = new ADDRESSES();
            addresses.AddressList.Add(
                CreateAddress(
                provider.StreetAddress,
                provider.City,
                provider.State,
                provider.Zip,
                string.Empty,
                string.Empty,
                string.Empty,
                AddressBase.Blank,
                GetNextSequenceNumber(addresses.AddressList)));

            return addresses;
        }

        /// <summary>
        /// Creates ADDRESSES container holding one ADDRESS sub-container.
        /// </summary>
        /// <param name="streetAddress">The street address.</param>
        /// <param name="city">The city listed in the address.</param>
        /// <param name="state">The state listed in the address.</param>
        /// <param name="zip">The ZIP code listed in the address.</param>
        /// <param name="addressType">The type of address. E.G. current | mailing | etc.</param>
        /// <param name="countyName">The name of the county.</param>
        /// <param name="countyFIPSCode">The FIPS code assigned to the county.</param>
        /// <returns>An ADDRESSES container holding one ADDRESS sub-container.</returns>
        public static ADDRESSES CreateAddresses(string streetAddress, string city, string state, string zip, AddressBase addressType, string countyName, string countyFIPSCode)
        {
            var addresses = new ADDRESSES();
            addresses.AddressList.Add(CreateAddress(
                streetAddress,
                city,
                state,
                zip,
                countyName,
                countyFIPSCode,
                StateInfo.Instance.GetStateName(state),
                addressType,
                GetNextSequenceNumber(addresses.AddressList)));

            return addresses;
        }

        /// <summary>
        /// Creates ADDRESS container based on specified street address, city, state, zip code, and county.
        /// </summary>
        /// <param name="streetAddr">The street address.</param>
        /// <param name="city">The specified city.</param>
        /// <param name="state">The specified state abbreviation.</param>
        /// <param name="zip">The specified postal zip code.</param>
        /// <param name="county">The specified county.</param>
        /// <param name="countyFIPSCode">The $$FIPS$$ code for the county or blank/null if unknown.</param>
        /// <param name="stateFullName">The full name of the given U.S. state.</param>
        /// <param name="type">The address type.</param>
        /// <param name="sequenceNumber">The sequence number for this element.</param>
        /// <returns>Returns ADDRESS container with specified street address, city, state, and zip code.</returns>
        public static ADDRESS CreateAddress(string streetAddr, string city, string state, string zip, string county, string countyFIPSCode, string stateFullName, AddressBase type, int? sequenceNumber = null)
        {
            var address = new ADDRESS();
            address.AddressLineText = ToMismoString(streetAddr);
            address.AddressType = ToMismoEnum(type, isSensitiveData: false);
            address.CityName = ToMismoString(city);
            address.CountryCode = ToMismoCode(StateInfo.USACountryCode);
            address.CountryName = ToMismoString(StateInfo.USACountryName);
            address.CountyCode = ToMismoCode(countyFIPSCode);
            address.CountyName = ToMismoString(county);
            address.PostalCode = ToMismoCode(zip);
            address.StateCode = ToMismoCode(state);
            address.StateName = ToMismoString(stateFullName);

            if (sequenceNumber != null)
            {
                address.SequenceNumber = sequenceNumber.Value;
            }

            return address;
        }

        /// <summary>
        /// Creates a container holding data on an adjustment.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="isLenderExportMode">Whether this is in lender export mode.</param>
        /// <returns>An ADJUSTMENT populated with data.</returns>
        public static ADJUSTMENT CreateAdjustment(CPageData dataLoan, bool isLenderExportMode)
        {
            if (dataLoan.sFinMethT != E_sFinMethT.ARM)
            {
                return null;
            }

            var adjustment = new ADJUSTMENT();
            adjustment.InterestRateAdjustment = CreateInterestRateAdjustment(dataLoan);
            adjustment.RateOrPaymentChangeOccurrences = CreateRateOrPaymentChangeOccurrences(dataLoan);
            adjustment.PrincipalAndInterestPaymentAdjustment = CreatePrincipalAndInterestPaymentAdjustment(dataLoan, isLenderExportMode);

            return adjustment;
        }

        /// <summary>
        /// Creates a container holding data on an interest rate adjustment.
        /// </summary>
        /// <param name="dataLoan">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>An INTEREST_RATE_ADJUSTMENT populated with data.</returns>
        public static INTEREST_RATE_ADJUSTMENT CreateInterestRateAdjustment(CPageData dataLoan)
        {
            var interestRateAdjustment = new INTEREST_RATE_ADJUSTMENT();
            interestRateAdjustment.InterestRateLifetimeAdjustmentRule = CreateInterestRateLifetimeAdjustmentRule(dataLoan);
            interestRateAdjustment.IndexRules = CreateIndexRules(
                dataLoan.sArmIndexT,
                dataLoan.sFreddieArmIndexT,
                dataLoan.sRAdjCapMon,
                dataLoan.sArmIndexNameVstr,
                dataLoan.sArmIndexLeadDays_rep);
            interestRateAdjustment.InterestRatePerChangeAdjustmentRules = CreateInterestRatePerChangeAdjustmentRules(dataLoan);

            return interestRateAdjustment;
        }

        /// <summary>
        /// Creates a container that holds all instances of INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <returns>An INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES container.</returns>
        public static INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES CreateInterestRatePerChangeAdjustmentRules(CPageData dataLoan)
        {
            var adjustmentRules = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES();
            adjustmentRules.InterestRatePerChangeAdjustmentRuleList.Add(
                CreateInterestRatePerChangeAdjustmentRule(
                    dataLoan,
                    AdjustmentRuleBase.First,
                    GetNextSequenceNumber(adjustmentRules.InterestRatePerChangeAdjustmentRuleList)));

            adjustmentRules.InterestRatePerChangeAdjustmentRuleList.Add(
                CreateInterestRatePerChangeAdjustmentRule(
                    dataLoan,
                    AdjustmentRuleBase.Subsequent,
                    GetNextSequenceNumber(adjustmentRules.InterestRatePerChangeAdjustmentRuleList)));

            return adjustmentRules;
        }

        /// <summary>
        /// Creates a container holding rule data on either a first or subsequent rate change.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="ruleType">Indicates whether the rule pertains only to the first rate change or subsequent changes.</param>
        /// <param name="sequence">The sequence of the rule among the list of interest rate adjustment rules.</param>
        /// <returns>An INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE container populated with data.</returns>
        public static INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE CreateInterestRatePerChangeAdjustmentRule(CPageData dataLoan, AdjustmentRuleBase ruleType, int sequence)
        {
            var rule = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE();
            rule.SequenceNumber = sequence;

            if (sequence == 1)
            {
                rule.AdjustmentRuleType = ToMismoEnum(ruleType);
                rule.PerChangeCeilingRatePercent = ToMismoPercent(dataLoan.sR1stCapR_rep);
                rule.PerChangeRateAdjustmentEffectiveMonthsCount = ToMismoCount(dataLoan.sRAdj1stCapMon_rep, null);
                rule.PerChangeRateAdjustmentFrequencyMonthsCount = ToMismoCount(dataLoan.sRAdj1stCapMon_rep, null);
                rule.PerChangeMaximumIncreaseRatePercent = ToMismoPercent(dataLoan.sLimitsOnInterestRateFirstChange_rep);
            }
            else
            {
                rule.AdjustmentRuleType = ToMismoEnum(ruleType);
                rule.PerChangeCeilingRatePercent = ToMismoPercent(dataLoan.sRLifeCapR_rep);
                rule.PerChangeRateAdjustmentEffectiveMonthsCount = ToMismoCount(dataLoan.sRAdjCapMon_rep, null);
                rule.PerChangeRateAdjustmentFrequencyMonthsCount = ToMismoCount(dataLoan.sRAdjCapMon_rep, null);
                rule.PerChangeMaximumIncreaseRatePercent = ToMismoPercent(dataLoan.sLimitsOnInterestRateSubsequentChanges_rep);
            }

            rule.PerChangeFloorRatePercent = ToMismoPercent(dataLoan.m_convertLos.ToRateString(Math.Max(dataLoan.sNoteIR - dataLoan.sRAdj1stCapR, dataLoan.sRAdjFloorR)));

            return rule;
        }

        /// <summary>
        /// Creates a container holding data on a lifetime interest rate adjustment rule.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <returns>An INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE populated with data.</returns>
        public static INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE CreateInterestRateLifetimeAdjustmentRule(CPageData dataLoan)
        {
            var interestRateLifetimeAdjustmentRule = new INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE();
            interestRateLifetimeAdjustmentRule.CeilingRatePercent = ToMismoPercent(dataLoan.sRLifeCapR_rep);
            interestRateLifetimeAdjustmentRule.FloorRatePercent = ToMismoPercent(dataLoan.sRAdjFloorR_rep);
            interestRateLifetimeAdjustmentRule.InterestRateRoundingPercent = ToMismoPercent(dataLoan.sRAdjRoundToR_rep);
            interestRateLifetimeAdjustmentRule.InterestRateRoundingType = ToMismoEnum(GetInterestRateRoundingValue(dataLoan.sRAdjRoundT));
            interestRateLifetimeAdjustmentRule.MarginRatePercent = ToMismoPercent(dataLoan.sRAdjMarginR == 0m ? "0.00" : dataLoan.sRAdjMarginR_rep);
            interestRateLifetimeAdjustmentRule.MaximumIncreaseRatePercent = ToMismoPercent(dataLoan.sRAdjLifeCapR_rep);
            interestRateLifetimeAdjustmentRule.FirstRateChangeMonthsCount = ToMismoCount(dataLoan.sRAdj1stCapMon_rep, null);

            return interestRateLifetimeAdjustmentRule;
        }

        /// <summary>
        /// Retrieves the InterestRateRoundingBase value corresponding to an enumerated value from LQB.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding InterestRateRoundingBase value.</returns>
        public static InterestRateRoundingBase GetInterestRateRoundingValue(E_sRAdjRoundT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sRAdjRoundT.Down:
                    return InterestRateRoundingBase.Down;
                case E_sRAdjRoundT.Normal:
                    return InterestRateRoundingBase.Nearest;
                case E_sRAdjRoundT.Up:
                    return InterestRateRoundingBase.Up;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates a container to hold instances of INDEX_RULE.
        /// </summary>
        /// <param name="index">The adjustable rate mortgage index.</param>
        /// <param name="freddieIndex">The Freddie Mac index.</param>
        /// <param name="adjustmentCapMonths">The adjustment period (months) for the adjustable rate.</param>
        /// <param name="indexName">The name of the ARM index.</param>
        /// <param name="indexLeadDays_rep">The number of days prior to an interest rate effective date used to determine the date for the index value.</param>
        /// <returns>An INDEX_RULES container.</returns>
        public static INDEX_RULES CreateIndexRules(E_sArmIndexT index, E_sFreddieArmIndexT freddieIndex, int adjustmentCapMonths, string indexName, string indexLeadDays_rep)
        {
            var indexRules = new INDEX_RULES();
            indexRules.IndexRuleList.Add(CreateIndexRule(index, freddieIndex, adjustmentCapMonths, indexName, indexLeadDays_rep, GetNextSequenceNumber(indexRules.IndexRuleList)));

            return indexRules;
        }

        /// <summary>
        /// Creates a container holding data about an index rule.
        /// </summary>
        /// <param name="index">The adjustable rate mortgage index.</param>
        /// <param name="freddieIndex">The Freddie Mac index.</param>
        /// <param name="adjustmentCapMonths">The adjustment period (months) for the adjustable rate.</param>
        /// <param name="indexName">The name of the ARM index.</param>
        /// <param name="indexLeadDays_rep">The number of days prior to an interest rate effective date used to determine the date for the index value.</param>
        /// <param name="sequence">The sequence number of the index rule among the set of rules.</param>
        /// <returns>An INDEX_RULE container populated with data.</returns>
        public static INDEX_RULE CreateIndexRule(E_sArmIndexT index, E_sFreddieArmIndexT freddieIndex, int adjustmentCapMonths, string indexName, string indexLeadDays_rep, int sequence)
        {
            var indexRule = new INDEX_RULE();

            indexRule.IndexSourceType = ToMismoEnum(GetIndexSourceBase(index, freddieIndex, adjustmentCapMonths));

            if (indexRule.IndexSourceType?.IsSetToOther ?? false)
            {
                if (index != E_sArmIndexT.LeaveBlank)
                {
                    indexRule.IndexSourceTypeOtherDescription = ToMismoString(index);
                }
                else if (freddieIndex != E_sFreddieArmIndexT.LeaveBlank)
                {
                    indexRule.IndexSourceTypeOtherDescription = ToMismoString(freddieIndex);
                }
            }

            indexRule.IndexType = ToMismoEnum(IndexBase.Other);
            indexRule.IndexTypeOtherDescription = ToMismoString(indexName);
            indexRule.InterestAndPaymentAdjustmentIndexLeadDaysCount = ToMismoCount(indexLeadDays_rep, null);
            indexRule.SequenceNumber = sequence;

            return indexRule;
        }

        /// <summary>
        /// Converts the given intermediate rate adjustment period and the Fannie or Freddie index to the MISMO defined index type.
        /// </summary>
        /// <param name="fnmaIndexType">The Fannie Mae ARM index.</param>
        /// <param name="freddieIndexType">The Freddie Mac ARM index.</param>
        /// <param name="rateAdjustmentPeriod">The intermediate adjustment period for the adjustable interest rate.</param>
        /// <returns>An IndexSourceBase object with the MISMO defined index type corresponding to the given rate adjustment period and indices.</returns>
        public static IndexSourceBase GetIndexSourceBase(E_sArmIndexT fnmaIndexType, E_sFreddieArmIndexT freddieIndexType, int rateAdjustmentPeriod)
        {
            switch (fnmaIndexType)
            {
                case E_sArmIndexT.DailyCDRate:
                    break;
                case E_sArmIndexT.EleventhDistrictCOF:
                    return IndexSourceBase.FHLBEleventhDistrictMonthlyCostOfFundsIndex;
                case E_sArmIndexT.FannieMaeLIBOR:
                case E_sArmIndexT.LeaveBlank:
                case E_sArmIndexT.MonthlyAvgCMT:
                    break;
                case E_sArmIndexT.NationalMonthlyMedianCostOfFunds:
                    return IndexSourceBase.NationalMonthlyMedianCostOfFundsIndexOTS;
                case E_sArmIndexT.TBillDailyValue:
                    break;
                case E_sArmIndexT.WallStreetJournalLIBOR:
                    if (rateAdjustmentPeriod == 6)
                    {
                        return IndexSourceBase.SixMonthLIBOR_WSJDaily;
                    }
                    else if (rateAdjustmentPeriod == 12)
                    {
                        return IndexSourceBase.LIBOROneYearWSJDaily;
                    }

                    break;
                case E_sArmIndexT.WeeklyAvgCDRate:
                    break;
                case E_sArmIndexT.WeeklyAvgCMT:
                    if (rateAdjustmentPeriod == 12)
                    {
                        return IndexSourceBase.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
                    }
                    else if (rateAdjustmentPeriod == 36)
                    {
                        return IndexSourceBase.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
                    }
                    else if (rateAdjustmentPeriod == 60)
                    {
                        return IndexSourceBase.WeeklyFiveYearTreasurySecuritiesConstantMaturityFRBH15;
                    }
                    else if (rateAdjustmentPeriod == 120)
                    {
                        return IndexSourceBase.WeeklyTenYearTreasurySecuritiesConstantMaturityFRBH15;
                    }

                    break;
                case E_sArmIndexT.WeeklyAvgPrimeRate:
                case E_sArmIndexT.WeeklyAvgSMTI:
                case E_sArmIndexT.WeeklyAvgTAABD:
                case E_sArmIndexT.WeeklyAvgTAAI:
                    break;
                default:
                    throw new UnhandledEnumException(fnmaIndexType);
            }

            switch (freddieIndexType)
            {
                case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds:
                    return IndexSourceBase.FHLBEleventhDistrictMonthlyCostOfFundsIndex;
                case E_sFreddieArmIndexT.LIBOR:
                    if (rateAdjustmentPeriod == 6)
                    {
                        return IndexSourceBase.SixMonthLIBOR_WSJDaily;
                    }
                    else if (rateAdjustmentPeriod == 12)
                    {
                        return IndexSourceBase.LIBOROneYearWSJDaily;
                    }

                    break;
                case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds:
                    return IndexSourceBase.NationalMonthlyMedianCostOfFundsIndexOTS;
                case E_sFreddieArmIndexT.OneYearTreasury:
                    return IndexSourceBase.WeeklyOneYearTreasurySecuritiesConstantMaturityFRBH15;
                case E_sFreddieArmIndexT.Other:
                case E_sFreddieArmIndexT.SixMonthTreasury:
                    break;
                case E_sFreddieArmIndexT.ThreeYearTreasury:
                    return IndexSourceBase.WeeklyThreeYearTreasurySecuritiesConstantMaturityFRBH15;
                case E_sFreddieArmIndexT.LeaveBlank:
                    return IndexSourceBase.Blank;
                default:
                    throw new UnhandledEnumException(freddieIndexType);
            }

            return IndexSourceBase.Other;
        }

        /// <summary>
        /// Creates a container holding sub-elements with information on principal and interest payment adjustments.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="isLenderExportMode">Whether this is in lender export mode.</param>
        /// <returns>A PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT.</returns>
        public static PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT CreatePrincipalAndInterestPaymentAdjustment(CPageData dataLoan, bool isLenderExportMode)
        {
            var principalAndInterestPaymentAdjustment = new PRINCIPAL_AND_INTEREST_PAYMENT_ADJUSTMENT();
            principalAndInterestPaymentAdjustment.PrincipalAndInterestPaymentPerChangeAdjustmentRules = CreatePrincipalAndInterestPaymentPerChangeAdjustmentRules(dataLoan, isLenderExportMode);

            principalAndInterestPaymentAdjustment.PrincipalAndInterestPaymentLifetimeAdjustmentRule = CreatePrincipalAndInterestPaymentLifetimeAdjustmentRule(dataLoan);

            return principalAndInterestPaymentAdjustment;
        }

        /// <summary>
        /// Creates a container that holds all instances of PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="isLenderExportMode">Whether this is in lender export mode.</param>
        /// <returns>A PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES container.</returns>
        public static PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES CreatePrincipalAndInterestPaymentPerChangeAdjustmentRules(CPageData dataLoan, bool isLenderExportMode)
        {
            var adjustmentRules = new PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULES();
            adjustmentRules.PrincipalAndInterestPaymentPerChangeAdjustmentRuleList.Add(
                CreatePrincipalAndInterestPaymentPerChangeAdjustmentRule(
                    dataLoan,
                    isLenderExportMode,
                    GetNextSequenceNumber(adjustmentRules.PrincipalAndInterestPaymentPerChangeAdjustmentRuleList)));

            adjustmentRules.PrincipalAndInterestPaymentPerChangeAdjustmentRuleList.Add(
                CreatePrincipalAndInterestPaymentPerChangeAdjustmentRule(
                    dataLoan,
                    isLenderExportMode,
                    GetNextSequenceNumber(adjustmentRules.PrincipalAndInterestPaymentPerChangeAdjustmentRuleList)));

            return adjustmentRules;
        }

        /// <summary>
        /// Creates a container holding data on a particular principal and interest payment change rule.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="isLenderExportMode">Whether this is in lender export mode.</param>
        /// <param name="sequence">The sequence number of the adjustment rule among the set of rules.</param>
        /// <returns>A PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE container.</returns>
        public static PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE CreatePrincipalAndInterestPaymentPerChangeAdjustmentRule(CPageData dataLoan, bool isLenderExportMode, int sequence)
        {
            var rule = new PRINCIPAL_AND_INTEREST_PAYMENT_PER_CHANGE_ADJUSTMENT_RULE();
            rule.SequenceNumber = sequence;

            if (sequence == 1)
            {
                rule.AdjustmentRuleType = ToMismoEnum(AdjustmentRuleBase.First);
                rule.PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount = ToMismoCount(dataLoan.sPIPmtFirstChangeAfterMon_rep, null);
                rule.PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent = ToMismoPercent(dataLoan.sRAdj1stCapR_rep);

                if (dataLoan.sGfeCanPaymentIncrease && dataLoan.sPIPmtFirstChangeAfterMon.HasValue)
                {
                    try
                    {
                        int index = dataLoan.sPIPmtFirstChangeAfterMon.Value + 1;
                        AmortItem bestAmortItem = dataLoan.GetAmortTable(E_AmortizationScheduleT.BestCase, false).Items[index];
                        AmortItem worstAmortItem = dataLoan.GetAmortTable(E_AmortizationScheduleT.WorstCase, false).Items[index];

                        rule.PerChangeMinimumPrincipalAndInterestPaymentAmount = ToMismoAmount(dataLoan.m_convertLos.ToMoneyString(bestAmortItem.PaymentWOMI, FormatDirection.ToRep));

                        if (bestAmortItem.PaymentWOMI == worstAmortItem.PaymentWOMI)
                        {
                            rule.PerChangeMaximumPrincipalAndInterestPaymentAmount = rule.PerChangeMinimumPrincipalAndInterestPaymentAmount;
                        }
                        else
                        {
                            rule.PerChangeMaximumPrincipalAndInterestPaymentAmount = ToMismoAmount(dataLoan.m_convertLos.ToMoneyString(worstAmortItem.PaymentWOMI, FormatDirection.ToRep));
                        }
                    }
                    catch (AmortTableInvalidArgumentException) when (isLenderExportMode)
                    {
                        rule.PerChangeMinimumPrincipalAndInterestPaymentAmount = null;
                        rule.PerChangeMaximumPrincipalAndInterestPaymentAmount = null;
                    }
                }
            }
            else
            {
                rule.AdjustmentRuleType = ToMismoEnum(AdjustmentRuleBase.Subsequent);

                if (dataLoan.sGfeCanPaymentIncrease)
                {
                    rule.PerChangePrincipalAndInterestPaymentAdjustmentFrequencyMonthsCount = ToMismoCount(dataLoan.sTRIDPIPmtSubsequentAdjustFreqMon_rep, null);
                }

                rule.PerChangeMaximumPrincipalAndInterestPaymentIncreasePercent = ToMismoPercent(dataLoan.sRAdjCapR_rep);
            }

            return rule;
        }

        /// <summary>
        /// Creates a container holding information on a principal and interest payment lifetime adjustment rule.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <returns>A PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE.</returns>
        public static PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE CreatePrincipalAndInterestPaymentLifetimeAdjustmentRule(CPageData dataLoan)
        {
            var principalAndInterestPaymentLifetimeAdjustmentRule = new PRINCIPAL_AND_INTEREST_PAYMENT_LIFETIME_ADJUSTMENT_RULE();
            principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentMaximumAmount = ToMismoAmount(dataLoan.sTRIDPIPmtMaxEver_rep);
            principalAndInterestPaymentLifetimeAdjustmentRule.PrincipalAndInterestPaymentMaximumAmountEarliestEffectiveMonthsCount = ToMismoCount(dataLoan.sPIPmtMaxEverStartAfterMon_rep, null);
            principalAndInterestPaymentLifetimeAdjustmentRule.FirstPrincipalAndInterestPaymentChangeDate = ToMismoDate(dataLoan.sRAdj1stD_rep);

            if (dataLoan.sGfeCanPaymentIncrease)
            {
                principalAndInterestPaymentLifetimeAdjustmentRule.FirstPrincipalAndInterestPaymentChangeMonthsCount = ToMismoCount(dataLoan.sPIPmtFirstChangeAfterMon_rep, null);
            }

            return principalAndInterestPaymentLifetimeAdjustmentRule;
        }

        /// <summary>
        /// Creates a container holding data on a rate or payment change.
        /// </summary>
        /// <param name="indexRatePercent_rep">The index value used to calculate the interest rate percent for this adjustment.</param>
        /// <param name="sequence">The sequence number of the payment change occurrence among the list of occurrences.</param>
        /// <returns>A RATE_OR_PAYMENT_CHANGE_OCCURRENCE populated with data.</returns>
        public static RATE_OR_PAYMENT_CHANGE_OCCURRENCE CreateRateOrPaymentChangeOccurrence(string indexRatePercent_rep, int sequence)
        {
            var rateOrPaymentChangeOccurrence = new RATE_OR_PAYMENT_CHANGE_OCCURRENCE();
            rateOrPaymentChangeOccurrence.SequenceNumber = sequence;
            rateOrPaymentChangeOccurrence.AdjustmentChangeIndexRatePercent = ToMismoPercent(indexRatePercent_rep);

            return rateOrPaymentChangeOccurrence;
        }

        /// <summary>
        /// Creates a container that can hold multiple instances of RATE_OR_PAYMENT_CHANGE_OCCURRENCE.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <returns>A RATE_OR_PAYMENT_CHANGE_OCCURRENCES container.</returns>
        public static RATE_OR_PAYMENT_CHANGE_OCCURRENCES CreateRateOrPaymentChangeOccurrences(CPageData dataLoan)
        {
            var rateOrPaymentChangeOccurrences = new RATE_OR_PAYMENT_CHANGE_OCCURRENCES();
            rateOrPaymentChangeOccurrences.RateOrPaymentChangeOccurrenceList.Add(
                CreateRateOrPaymentChangeOccurrence(
                    dataLoan.sRAdjIndexR_rep,
                    GetNextSequenceNumber(rateOrPaymentChangeOccurrences.RateOrPaymentChangeOccurrenceList)));

            return rateOrPaymentChangeOccurrences;
        }

        /// <summary>
        /// Creates an container holding data on an interest-only rule.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <returns>An INTEREST_ONLY container populated with data.</returns>
        public static INTEREST_ONLY CreateInterestOnly(CPageData dataLoan)
        {
            if (dataLoan.sIOnlyMon == 0)
            {
                return null;
            }

            var interestOnly = new INTEREST_ONLY();
            interestOnly.InterestOnlyTermMonthsCount = ToMismoCount(dataLoan.sIOnlyMon_rep, null);
            interestOnly.InterestOnlyMonthlyPaymentAmount = ToMismoAmount(dataLoan.sProThisMPmt_rep);

            return interestOnly;
        }

        /// <summary>
        /// Creates a basic LEGAL_ENTITY container holding just a company name.
        /// </summary>
        /// <param name="companyName">The company name.</param>
        /// <returns>A LEGAL_ENTITY container.</returns>
        public static LEGAL_ENTITY CreateLegalEntity(string companyName)
        {
            var legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(companyName);

            return legalEntity;
        }

        /// <summary>
        /// Creates LEGAL_ENTITY container with contactName, companyName, email, fax, phone provided.
        /// </summary>
        /// <param name="agent">Agent object containing contact name, company name, phone, fax, and email.</param>
        /// <param name="companyName">The company name of the legal entity.</param>
        /// <param name="contactRoleType">Enumeration representing the type of contact (work/home/mobile/etc...) provided.</param>
        /// <returns>Returns a LEGAL_ENTITY container with contactName, companyName, email, fax, phone provided.</returns>
        public static LEGAL_ENTITY CreateLegalEntity(CAgentFields agent, string companyName, ContactPointRoleBase contactRoleType)
        {
            LEGAL_ENTITY legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(companyName);
            legalEntity.Contacts = CreateContacts(agent, contactRoleType, EntityType.LegalEntity);

            return legalEntity;
        }

        /// <summary>
        /// Creates a container holding a company's name and telephone number.
        /// </summary>
        /// <param name="companyName">The name of the company.</param>
        /// <param name="telephoneNumber">The company's telephone number.</param>
        /// <param name="faxNumber">The company's fax number.</param>
        /// <param name="email">An email address associated with the company or a representative.</param>
        /// <param name="contactName">A NAME object containing the name of a person associated with the legal entity.</param>
        /// <param name="contactPointType">The locale / type of the contact point, e.g. work | home | mobile.</param>
        /// <param name="mersOrganizationId">The MERSOrganizationIdentifier to be populated.</param>
        /// <returns>A LEGAL_ENTITY container holding a company's name and telephone number.</returns>
        public static LEGAL_ENTITY CreateLegalEntity(string companyName, string telephoneNumber, string faxNumber, string email, NAME contactName, ContactPointRoleBase contactPointType, string mersOrganizationId = null)
        {
            LEGAL_ENTITY legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(companyName, mersOrganizationId);

            if (!(string.IsNullOrEmpty(telephoneNumber) && string.IsNullOrEmpty(faxNumber) && string.IsNullOrEmpty(email)))
            {
                legalEntity.Contacts = CreateContacts(telephoneNumber, faxNumber, email, contactName, contactPointType);
            }

            return legalEntity;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY container for a property seller.
        /// </summary>
        /// <param name="companyName">The company name.</param>
        /// <param name="sellerType">The type of property seller.</param>
        /// <returns>A LEGAL_ENTITY container.</returns>
        public static LEGAL_ENTITY CreateLegalEntity(string companyName, E_SellerEntityType sellerType)
        {
            var legalEntity = new LEGAL_ENTITY();
            legalEntity.LegalEntityDetail = CreateLegalEntityDetail(companyName, sellerType);

            return legalEntity;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY_DETAIL container.
        /// </summary>
        /// <param name="companyName">Company name to be populated.</param>
        /// <param name="mersOrganizationId">The MERSOrganizationIdentifier to be populated.</param>
        /// <param name="legalEntityType">The type of Legal Entity, if applicable.</param>
        /// <returns>Returns LEGAL_ENTITY_DETAIL container.</returns>
        public static LEGAL_ENTITY_DETAIL CreateLegalEntityDetail(string companyName, string mersOrganizationId = null, LegalEntityBase legalEntityType = LegalEntityBase.Blank)
        {
            var legalEntityDetail = new LEGAL_ENTITY_DETAIL();
            legalEntityDetail.FullName = ToMismoString(companyName);
            legalEntityDetail.MERSOrganizationIdentifier = ToMismoIdentifier(mersOrganizationId);

            return legalEntityDetail;
        }

        /// <summary>
        /// Creates a LEGAL_ENTITY_DETAIL for a property seller.
        /// </summary>
        /// <param name="companyName">Company name to be populated.</param>
        /// <param name="sellerType">The type of property seller.</param>
        /// <returns>A LEGAL_ENTITY_DETAIL container.</returns>
        public static LEGAL_ENTITY_DETAIL CreateLegalEntityDetail(string companyName, E_SellerEntityType sellerType)
        {
            var legalEntityDetail = new LEGAL_ENTITY_DETAIL();
            legalEntityDetail.FullName = ToMismoString(companyName);

            if (sellerType != E_SellerEntityType.Undefined)
            {
                legalEntityDetail.LegalEntityType = ToMismoEnum(GetLegalEntityBaseValue(sellerType), isSensitiveData: false);

                if (legalEntityDetail.LegalEntityType?.IsSetToOther ?? false)
                {
                    legalEntityDetail.LegalEntityTypeOtherDescription = ToMismoString(sellerType);
                }
            }

            return legalEntityDetail;
        }

        /// <summary>
        /// Creates a CONTACTS container with one CONTACT sub-container holding contactName, email, fax, and phone.
        /// </summary>
        /// <param name="agent">Agent object holding contact name, phone, fax, and email.</param>
        /// <param name="contactRoleType">Enumeration representing the type of contact (work/home/mobile/etc...) provided.</param>
        /// <param name="entity">The type of entity associated with the contacts.</param>
        /// <returns>Returns a CONTACTS container with one CONTACT sub-container holding contactName, email, fax, and phone.</returns>
        public static CONTACTS CreateContacts(CAgentFields agent, ContactPointRoleBase contactRoleType, EntityType entity)
        {
            var contacts = new CONTACTS();
            contacts.ContactList.Add(CreateContact(agent, contactRoleType, entity, GetNextSequenceNumber(contacts.ContactList)));
            return contacts;
        }

        /// <summary>
        /// Creates a CONTACTS container with a phone number and fax number.
        /// </summary>
        /// <param name="telephoneNumber">A phone number.</param>
        /// <param name="faxNumber">A fax number.</param>
        /// <param name="email">An email address.</param>
        /// <param name="contactName">A NAME object containing the name of a person associated with the contacts.</param>
        /// <param name="contactPointType">The locale / type of the contact point, e.g. work | home | mobile.</param>
        /// <returns>A CONTACTS container.</returns>
        public static CONTACTS CreateContacts(string telephoneNumber, string faxNumber, string email, NAME contactName, ContactPointRoleBase contactPointType)
        {
            var contacts = new CONTACTS();
            contacts.ContactList.Add(CreateContact(telephoneNumber, faxNumber, email, contactName, contactPointType, GetNextSequenceNumber(contacts.ContactList)));

            return contacts;
        }

        /// <summary>
        /// Creates a CONTACT container holding contactName, email, fax, and phone.
        /// </summary>
        /// <param name="agent">Agent contact holding phone, fax, and email information.</param>
        /// <param name="contactRoleType">Enumeration representing the type of contact (work/home/mobile/etc...) provided.</param>
        /// <param name="entity">The type of entity associated with the contact.</param>
        /// <param name="sequence">The sequence number among the list of contacts.</param>
        /// <returns>Returns a CONTACT container holding contactName, email, fax, and phone.</returns>
        public static CONTACT CreateContact(CAgentFields agent, ContactPointRoleBase contactRoleType, EntityType entity, int sequence)
        {
            var contact = new CONTACT();
            contact.ContactPoints = CreateContactPoints(agent, contactRoleType, entity);
            contact.SequenceNumber = sequence;

            if (entity == EntityType.Individual)
            {
                contact.Name = CreateName(agent.AgentName, isLegalEntity: false);
            }

            return contact;
        }

        /// <summary>
        /// Creates CONTACT_POINTS container with single CONTACT_POINT holding agent information.
        /// </summary>
        /// <param name="agent">Agent object holding contact information.</param>
        /// <param name="contactRoleType">Enumeration representing the type of contact (work/home/mobile/etc...) provided.</param>
        /// <param name="entity">The type of entity associated with the contact points.</param>
        /// <returns>Returns a CONTACT_POINTS container with single CONTACT_POINT holding agent information.</returns>
        public static CONTACT_POINTS CreateContactPoints(CAgentFields agent, ContactPointRoleBase contactRoleType, EntityType entity)
        {
            if (agent == null || !agent.IsValid || agent.IsNewRecord)
            {
                return null;
            }

            var contactPoints = new CONTACT_POINTS();
            string phone = string.Empty;
            string fax = string.Empty;
            string email = string.Empty;

            if (entity == EntityType.Individual)
            {
                phone = agent.Phone;
                fax = agent.FaxNum;
                email = agent.EmailAddr;
            }
            else
            {
                phone = agent.PhoneOfCompany;
                fax = agent.FaxOfCompany;
            }

            contactPoints.ContactPointList.Add(CreateContactPoint_Phone(phone, contactRoleType, GetNextSequenceNumber(contactPoints.ContactPointList)));
            contactPoints.ContactPointList.Add(CreateContactPoint_Fax(fax, contactRoleType, GetNextSequenceNumber(contactPoints.ContactPointList)));
            contactPoints.ContactPointList.Add(CreateContactPoint_Email(email, contactRoleType, GetNextSequenceNumber(contactPoints.ContactPointList)));

            return contactPoints;
        }

        /// <summary>
        /// Creates a CONTACT container with a phone number and fax number.
        /// </summary>
        /// <param name="telephoneNumber">A phone number.</param>
        /// <param name="faxNumber">A fax number.</param>
        /// <param name="email">An email address.</param>
        /// <param name="contactName">A NAME object containing the name of a person associated with the contact.</param>
        /// <param name="contactPointType">The locale / type of the contact point, e.g. work | home | mobile.</param>
        /// <param name="sequence">The sequence number of the contact among the list of contacts.</param>
        /// <returns>A CONTACT container.</returns>
        public static CONTACT CreateContact(string telephoneNumber, string faxNumber, string email, NAME contactName, ContactPointRoleBase contactPointType, int sequence)
        {
            var contact = new CONTACT();
            contact.ContactPoints = CreateContactPoints(telephoneNumber, faxNumber, email, contactPointType);
            contact.Name = contactName;
            contact.SequenceNumber = sequence;

            return contact;
        }

        /// <summary>
        /// Creates CONTACT_POINTS element for given borrower.
        /// </summary>
        /// <param name="dataApp">Loan app from which contact information is populated.</param>
        /// <returns>CONTACT_POINTS object containing borrower data.</returns>
        public static CONTACT_POINTS CreateContactPoints(CAppData dataApp)
        {
            var contactPoints = new CONTACT_POINTS();
            contactPoints.ContactPointList.Add(CreateContactPoint_Phone(dataApp.aBusPhone, ContactPointRoleBase.Work, GetNextSequenceNumber(contactPoints.ContactPointList)));
            contactPoints.ContactPointList.Add(CreateContactPoint_Phone(dataApp.aCellPhone, ContactPointRoleBase.Mobile, GetNextSequenceNumber(contactPoints.ContactPointList)));
            contactPoints.ContactPointList.Add(CreateContactPoint_Phone(dataApp.aHPhone, ContactPointRoleBase.Home, GetNextSequenceNumber(contactPoints.ContactPointList)));
            contactPoints.ContactPointList.Add(CreateContactPoint_Fax(dataApp.aFax, ContactPointRoleBase.Home, GetNextSequenceNumber(contactPoints.ContactPointList)));
            contactPoints.ContactPointList.Add(CreateContactPoint_Email(dataApp.aEmail, ContactPointRoleBase.Blank, GetNextSequenceNumber(contactPoints.ContactPointList)));
            return contactPoints;
        }

        /// <summary>
        /// Creates a CONTACT_POINTS container with phone and fax contact points.
        /// </summary>
        /// <param name="phone">A phone number.</param>
        /// <param name="faxNumber">A fax number.</param>
        /// <param name="email">An email address.</param>
        /// <param name="contactPointType">The locale / type of the contact point, e.g. work | home | mobile.</param>
        /// <returns>A CONTACT_POINTS container.</returns>
        public static CONTACT_POINTS CreateContactPoints(string phone, string faxNumber, string email, ContactPointRoleBase contactPointType)
        {
            if (string.IsNullOrEmpty(phone) && string.IsNullOrEmpty(faxNumber) && string.IsNullOrEmpty(email))
            {
                return null;
            }

            var contactPoints = new CONTACT_POINTS();

            if (!string.IsNullOrEmpty(phone))
            {
                contactPoints.ContactPointList.Add(CreateContactPoint_Phone(phone, contactPointType, GetNextSequenceNumber(contactPoints.ContactPointList)));
            }

            if (!string.IsNullOrEmpty(faxNumber))
            {
                contactPoints.ContactPointList.Add(CreateContactPoint_Fax(faxNumber, contactPointType, GetNextSequenceNumber(contactPoints.ContactPointList)));
            }

            if (!string.IsNullOrEmpty(email))
            {
                contactPoints.ContactPointList.Add(CreateContactPoint_Email(email, contactPointType, GetNextSequenceNumber(contactPoints.ContactPointList)));
            }

            return contactPoints;
        }

        /// <summary>
        /// Creates a telephone contact point with the given contact information.
        /// </summary>
        /// <param name="phone">A phone number.</param>
        /// <param name="contactRoleType">The role of the contact.</param>
        /// <param name="sequence">The sequence number for the contact point.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        public static CONTACT_POINT CreateContactPoint_Phone(string phone, ContactPointRoleBase contactRoleType, int sequence)
        {
            if (string.IsNullOrEmpty(phone))
            {
                return null;
            }

            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequence;
            contactPoint.ContactPointTelephone = new CONTACT_POINT_TELEPHONE();
            contactPoint.ContactPointTelephone.ContactPointTelephoneValue = ToMismoNumericString(phone, isSensitiveData: true);

            if (contactRoleType != ContactPointRoleBase.Blank)
            {
                contactPoint.ContactPointDetail = new CONTACT_POINT_DETAIL();
                contactPoint.ContactPointDetail.ContactPointRoleType = ToMismoEnum(contactRoleType, isSensitiveData: true);
            }

            return contactPoint;
        }

        /// <summary>
        /// Creates a fax contact point with the given contact information.
        /// </summary>
        /// <param name="fax">A fax number.</param>
        /// <param name="contactRoleType">The role of the contact.</param>
        /// <param name="sequence">The sequence number for the contact point.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        public static CONTACT_POINT CreateContactPoint_Fax(string fax, ContactPointRoleBase contactRoleType, int sequence)
        {
            if (string.IsNullOrEmpty(fax))
            {
                return null;
            }

            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequence;
            contactPoint.ContactPointTelephone = new CONTACT_POINT_TELEPHONE();
            contactPoint.ContactPointTelephone.ContactPointFaxValue = ToMismoNumericString(fax, isSensitiveData: true);

            if (contactRoleType != ContactPointRoleBase.Blank)
            {
                contactPoint.ContactPointDetail = new CONTACT_POINT_DETAIL();
                contactPoint.ContactPointDetail.ContactPointRoleType = ToMismoEnum(contactRoleType, isSensitiveData: true);
            }

            return contactPoint;
        }

        /// <summary>
        /// Creates an email contact point with the given contact information.
        /// </summary>
        /// <param name="email">An email address.</param>
        /// <param name="contactRoleType">The role of the contact.</param>
        /// <param name="sequence">The sequence number for the contact point.</param>
        /// <returns>A CONTACT_POINT container.</returns>
        public static CONTACT_POINT CreateContactPoint_Email(string email, ContactPointRoleBase contactRoleType, int sequence)
        {
            if (string.IsNullOrEmpty(email))
            {
                return null;
            }

            var contactPoint = new CONTACT_POINT();
            contactPoint.SequenceNumber = sequence;
            contactPoint.ContactPointEmail = new CONTACT_POINT_EMAIL();
            contactPoint.ContactPointEmail.ContactPointEmailValue = ToMismoValue(email, isSensitiveData: true);

            if (contactRoleType != ContactPointRoleBase.Blank)
            {
                contactPoint.ContactPointDetail = new CONTACT_POINT_DETAIL();
                contactPoint.ContactPointDetail.ContactPointRoleType = ToMismoEnum(contactRoleType, isSensitiveData: true);
            }

            return contactPoint;
        }

        /// <summary>
        /// Creates NAME object with full name specified.
        /// </summary>
        /// <param name="fullName">The full name.</param>
        /// <param name="isLegalEntity">Indicates whether the name is for a legal entity.</param>
        /// <returns>NAME object with full name specified.</returns>
        public static NAME CreateName(string fullName, bool isLegalEntity)
        {
            if (string.IsNullOrEmpty(fullName))
            {
                return null;
            }

            if (!isLegalEntity)
            {
                try
                {
                    // If the name is not for a legal entity, we attempt to parse it.
                    var parser = new CommonLib.Name();
                    parser.ParseName(fullName);

                    return CreateName(parser.FirstName, parser.MiddleName, parser.LastName, parser.Suffix, fullName);
                }
                catch (NullReferenceException)
                {
                    // Unable to parse the name, let the default behavior occur instead.
                }
            }

            var name = new NAME();
            name.FullName = ToMismoString(fullName);
            return name;
        }

        /// <summary>
        /// Creates NAME object with individual names specified.
        /// </summary>
        /// <param name="first">The first name.</param>
        /// <param name="middle">The middle name or initial.</param>
        /// <param name="last">The last name.</param>
        /// <param name="suffix">The suffix for the name.</param>
        /// <param name="fullName">The unparsed full name.</param>
        /// <returns>A NAME object populated with the specified names.</returns>
        public static NAME CreateName(string first, string middle, string last, string suffix, string fullName)
        {
            NAME name = new NAME();
            name.FirstName = ToMismoString(first);
            name.FullName = ToMismoString(fullName);
            name.MiddleName = ToMismoString(middle);
            name.LastName = ToMismoString(last);
            name.SuffixName = ToMismoString(suffix);

            return name;
        }

        /// <summary>
        /// Creates a container holding information on the mortgage payment.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="isClosingPackage">Indicates whether a closing package is being generated.</param>
        /// <returns>A PAYMENT object.</returns>
        public static PAYMENT CreatePayment(CPageData loanData, bool isClosingPackage)
        {
            var payment = new PAYMENT();
            payment.PaymentComponentBreakouts = CreatePaymentComponentBreakouts(loanData);
            payment.PaymentRule = CreatePaymentRule(loanData.sSchedDueD1_rep, loanData.sProThisMPmt_rep, loanData.sTRIDPartialPaymentAcceptanceT, isClosingPackage);

            if (isClosingPackage)
            {
                payment.PartialPayments = CreatePartialPayments(loanData);
            }

            return payment;
        }

        /// <summary>
        /// Creates a container holding information on a payment component breakout.
        /// </summary>
        /// <param name="pipaymentAmount_rep">The principal and interest amount that is part of the total payment being reported.</param>
        /// <param name="sequence">The sequence number of the payment component breakout among the list of breakouts.</param>
        /// <returns>A PAYMENT_COMPONENT_BREAKOUT container.</returns>
        public static PAYMENT_COMPONENT_BREAKOUT CreatePaymentComponentBreakout(string pipaymentAmount_rep, int sequence)
        {
            var paymentComponentBreakout = new PAYMENT_COMPONENT_BREAKOUT();
            paymentComponentBreakout.SequenceNumber = sequence;
            paymentComponentBreakout.PaymentComponentBreakoutDetail = CreatePaymentComponentBreakoutDetail(pipaymentAmount_rep);

            return paymentComponentBreakout;
        }

        /// <summary>
        /// Creates a container to hold all instances of PAYMENT_COMPONENT_BREAKOUTS.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>A PAYMENT_COMPONENT_BREAKOUTS container.</returns>
        public static PAYMENT_COMPONENT_BREAKOUTS CreatePaymentComponentBreakouts(CPageData loanData)
        {
            var paymentComponentBreakouts = new PAYMENT_COMPONENT_BREAKOUTS();
            paymentComponentBreakouts.PaymentComponentBreakoutList.Add(CreatePaymentComponentBreakout(loanData.sProThisMPmt_rep, GetNextSequenceNumber(paymentComponentBreakouts.PaymentComponentBreakoutList)));

            return paymentComponentBreakouts;
        }

        /// <summary>
        /// Creates a container holding data on a payment component breakout.
        /// </summary>
        /// <param name="pipaymentAmount_rep">The principal and interest amount that is part of the total payment being reported.</param>
        /// <returns>A PAYMENT_COMPONENT_BREAKOUT_DETAIL populated with data.</returns>
        public static PAYMENT_COMPONENT_BREAKOUT_DETAIL CreatePaymentComponentBreakoutDetail(string pipaymentAmount_rep)
        {
            var paymentComponentBreakoutDetail = new PAYMENT_COMPONENT_BREAKOUT_DETAIL();
            paymentComponentBreakoutDetail.PrincipalAndInterestPaymentAmount = ToMismoAmount(pipaymentAmount_rep);

            return paymentComponentBreakoutDetail;
        }

        /// <summary>
        /// Creates a container holding data on a payment rule.
        /// </summary>
        /// <param name="scheduledFirstPaymentDate_rep">The date of the first scheduled mortgage payment to be made by the borrower under the terms of the mortgage.</param>
        /// <param name="initialPIPaymentAmount_rep">The dollar amount of the principal and interest payment as stated on the Note.</param>
        /// <param name="partialPaymentsAccepted">Indicates whether partial payments are accepted.</param>
        /// <param name="isClosingPackage">Indicates whether a closing package is currently being exported.</param>
        /// <returns>A PAYMENT_RULE object populated with data.</returns>
        public static PAYMENT_RULE CreatePaymentRule(string scheduledFirstPaymentDate_rep, string initialPIPaymentAmount_rep, E_sTRIDPartialPaymentAcceptanceT partialPaymentsAccepted, bool isClosingPackage)
        {
            var paymentRule = new PAYMENT_RULE();
            paymentRule.PaymentFrequencyType = ToMismoEnum(PaymentFrequencyBase.Monthly);
            paymentRule.ScheduledFirstPaymentDate = ToMismoDate(scheduledFirstPaymentDate_rep);
            paymentRule.InitialPrincipalAndInterestPaymentAmount = ToMismoAmount(initialPIPaymentAmount_rep);

            if (isClosingPackage)
            {
                paymentRule.PartialPaymentAllowedIndicator = ToMismoIndicator(partialPaymentsAccepted != E_sTRIDPartialPaymentAcceptanceT.NotAccepted);
            }

            return paymentRule;
        }

        /// <summary>
        /// Creates a container holding a set of partial payments.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <returns>A PARTIAL_PAYMENTS object.</returns>
        public static PARTIAL_PAYMENTS CreatePartialPayments(CPageData loanData)
        {
            var partialPayments = new PARTIAL_PAYMENTS();
            partialPayments.PartialPaymentList.Add(CreatePartialPayment(loanData.sTRIDPartialPaymentAcceptanceT, GetNextSequenceNumber(partialPayments.PartialPaymentList)));

            return partialPayments;
        }

        /// <summary>
        /// Creates a container holding data on a partial payment.
        /// </summary>
        /// <param name="partialPaymentsAccepted">Indicates whether partial payments are accepted.</param>
        /// <param name="sequenceNumber">The index of this element in the list of partial payments.</param>
        /// <returns>A PARTIAL_PAYMENT container.</returns>
        public static PARTIAL_PAYMENT CreatePartialPayment(E_sTRIDPartialPaymentAcceptanceT partialPaymentsAccepted, int sequenceNumber)
        {
            var partialPayment = new PARTIAL_PAYMENT();
            partialPayment.PartialPaymentApplicationMethodType = ToMismoEnum(GetPartialPaymentApplicationMethodBaseValue(partialPaymentsAccepted));
            partialPayment.SequenceNumber = sequenceNumber;

            return partialPayment;
        }

        /// <summary>
        /// Creates a PARTY extension.
        /// </summary>
        /// <param name="agent">An agent record.</param>
        /// <param name="entityType">Indicates whether the agent's individual or legal entity data is being exported.</param>
        /// <returns>A PARTY extension.</returns>
        public static PARTY_EXTENSION CreatePartyExtension(CAgentFields agent, EntityType entityType)
        {
            var extension = new PARTY_EXTENSION();
            extension.Other = CreateLqbPartyExtension(agent, entityType);

            return extension;
        }

        /// <summary>
        /// Creates a container holding data on the terms of a loan.
        /// </summary>
        /// <param name="loanData">A <see cref="CPageData"/> object with the loan information.</param>
        /// <param name="exportData">A collection of data used throughout the exporter.</param>
        /// <param name="helocEnabled">True if the HELOC feature is enabled for the lender. Otherwise false.</param>
        /// <returns>A TERMS_OF_LOAN object populated with data.</returns>
        public static TERMS_OF_LOAN CreateTermsOfLoan(CPageData loanData, Mismo34ExporterCache exportData, bool helocEnabled)
        {
            var termsOfLoan = new TERMS_OF_LOAN();
            termsOfLoan.LienPriorityType = ToMismoEnum(GetLienPriorityValue(loanData.sLienPosT), isSensitiveData: false);
            termsOfLoan.BaseLoanAmount = ToMismoAmount(loanData.sIsLineOfCredit ? loanData.sCreditLineAmt_rep : loanData.sLAmtCalc_rep);

            termsOfLoan.MortgageType = ToMismoEnum(GetMortgageBaseValue(loanData.sLT), isSensitiveData: false);

            if (termsOfLoan.MortgageType != null && termsOfLoan.MortgageType.EnumValue == MortgageBase.Other)
            {
                termsOfLoan.MortgageTypeOtherDescription = ToMismoString(loanData.sLTODesc);
            }

            // sg 10/20/2015 - OPM 229518: DocMagic specific expected values for HELOC
            if (exportData.Options.DocumentVendor == E_DocumentVendor.DocMagic && loanData.sIsLineOfCredit)
            {
                termsOfLoan.MortgageType = ToMismoEnum(GetMortgageBaseValue(E_sLT.Other), isSensitiveData: false);
                termsOfLoan.MortgageTypeOtherDescription = ToMismoString("HELOC");
            }

            termsOfLoan.NoteRatePercent = ToMismoPercent(string.IsNullOrEmpty(loanData.sNoteIR_rep) ? "0.00" : loanData.sNoteIR_rep);
            termsOfLoan.NoteAmount = ToMismoAmount(loanData.sFinalLAmt_rep);

            bool isHeloc = helocEnabled && loanData.sIsLineOfCredit;
            if (exportData.IsConstructionLoan)
            {
                termsOfLoan.LoanPurposeType = ToMismoEnum(GetLoanPurposeBaseValue(loanData.sNMLSLoanPurposeT), isSensitiveData: false);
            }
            else
            {
                termsOfLoan.LoanPurposeType = ToMismoEnum(GetLoanPurposeBaseValue(loanData.sLPurposeT, isHeloc), isSensitiveData: false);
            }

            termsOfLoan.LoanPurposeTypeOtherDescription = ToMismoString(isHeloc ? "HELOC" : loanData.sOLPurposeDesc);

            if (loanData.sFinMethT == E_sFinMethT.ARM)
            {
                termsOfLoan.DisclosedIndexRatePercent = ToMismoPercent(loanData.sRAdjIndexR_rep);
            }

            return termsOfLoan;
        }

        /// <summary>
        /// Creates a container holding an alias.
        /// </summary>
        /// <param name="aliasString">The alias represented by the container.</param>
        /// <param name="sequence">The sequence number of the alias among the list of aliases.</param>
        /// <returns>An ALIAS container.</returns>
        public static ALIAS CreateAlias(string aliasString, int sequence)
        {
            var alias = new ALIAS();
            alias.SequenceNumber = sequence;
            alias.Name = CreateName(aliasString, isLegalEntity: false);

            return alias;
        }

        /// <summary>
        /// Creates a container holding disbursements for an escrowed expense.
        /// </summary>
        /// <param name="payments">A list of projected or actual payments for the expense.</param>
        /// <param name="forceExportDisbursements">
        /// Normally disbursements will only be exported if they are to be paid by the escrow account.
        /// This can be overridden by setting this parameter to true.
        /// </param>
        /// <returns>An ESCROW_ITEM_DISBURSEMENTS container.</returns>
        public static ESCROW_ITEM_DISBURSEMENTS CreateEscrowItemDisbursements(ICollection<SingleDisbursement> payments, bool forceExportDisbursements = false)
        {
            var escrowItemDisbursements = new ESCROW_ITEM_DISBURSEMENTS();

            foreach (SingleDisbursement payment in payments.OrderBy(p => p.DueDate))
            {
                if (payment.PaidBy == E_DisbursementPaidByT.EscrowImpounds)
                {
                    escrowItemDisbursements.EscrowItemDisbursementList.Add(CreateEscrowItemDisbursement(payment, GetNextSequenceNumber(escrowItemDisbursements.EscrowItemDisbursementList)));
                }
            }

            return escrowItemDisbursements.EscrowItemDisbursementList.Count(e => e != null) > 0 ? escrowItemDisbursements : null;
        }

        /// <summary>
        /// Creates a container holding data on a single disbursement for an escrowed expense.
        /// </summary>
        /// <param name="payment">A projected or actual payment for the expense.</param>
        /// <param name="sequence">The sequence number of the disbursement among the list of disbursements.</param>
        /// <returns>An ESCROW_ITEM_DISBURSEMENT container.</returns>
        public static ESCROW_ITEM_DISBURSEMENT CreateEscrowItemDisbursement(SingleDisbursement payment, int sequence)
        {
            var escrowItemDisbursement = new ESCROW_ITEM_DISBURSEMENT();
            escrowItemDisbursement.EscrowItemDisbursementDate = ToMismoDate(payment.DueDate_rep);
            escrowItemDisbursement.EscrowItemDisbursementAmount = ToMismoAmount(payment.DisbursementAmt_rep);
            escrowItemDisbursement.EscrowItemDisbursementTermMonthsCount = ToMismoCount(payment.CoveredMonths_rep, null);
            escrowItemDisbursement.EscrowItemDisbursedDate = ToMismoDate(payment.PaidDate_rep);
            escrowItemDisbursement.SequenceNumber = sequence;

            return escrowItemDisbursement;
        }

        /// <summary>
        /// Creates a container holding information on a housing expense.
        /// </summary>
        /// <param name="amount">The amount of the housing expense.</param>
        /// <param name="housingExpenseTiming">Housing expense timing type.</param>
        /// <param name="expenseType">The type of housing expense.</param>
        /// <param name="sequence">The sequence number of the housing expense among the list of expenses.</param>
        /// <returns>A HOUSING_EXPENSE container populated with data.</returns>
        public static HOUSING_EXPENSE CreateHousingExpense(string amount, HousingExpenseTimingBase housingExpenseTiming, HousingExpenseBase expenseType, int sequence)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var housingExpense = new HOUSING_EXPENSE();
            housingExpense.HousingExpensePaymentAmount = ToMismoAmount(amount);
            housingExpense.HousingExpenseTimingType = ToMismoEnum(housingExpenseTiming, isSensitiveData: false);
            housingExpense.HousingExpenseType = ToMismoEnum(expenseType, isSensitiveData: false);
            housingExpense.SequenceNumber = sequence;

            return housingExpense;
        }

        /// <summary>
        /// Creates a container holding information on a trustee.
        /// </summary>
        /// <param name="exportData">A collection of data used throughout the exporter.</param>
        /// <param name="trustee">The trustee details.</param>
        /// <param name="label">The label for this role.</param>
        /// <param name="sequenceNumber">The sequence number for this role.</param>
        /// <returns>A ROLE container.</returns>
        public static ROLE CreateRole_Trustee(Mismo34ExporterCache exportData, TrustCollection.Trustee trustee, string label, int sequenceNumber)
        {
            var role = new ROLE();
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.Trustee, string.Empty);
            role.Trustee = CreateTrustee(trustee);

            role.XlinkLabel = exportData.GeneratePartyLabel(label, EntityType.Individual);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a container holding information on a trust settlor.
        /// </summary>
        /// <param name="exportData">A collection of data used throughout the exporter.</param>
        /// <param name="label">The label for this role.</param>
        /// <param name="sequenceNumber">The sequence number for this role.</param>
        /// <returns>A ROLE container.</returns>
        public static ROLE CreateRole_Settlor(Mismo34ExporterCache exportData, string label, int sequenceNumber)
        {
            var role = new ROLE();
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.Settlor, string.Empty);

            role.XlinkLabel = exportData.GeneratePartyLabel(label, EntityType.Individual);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates ROLE Detail container with specified type and/or other description.
        /// </summary>
        /// <param name="partyRoleType">The specified party role type.</param>
        /// <param name="partyRoleTypeOtherDescription">The specified party role other type description. Use only if partyRoleType is other.</param>
        /// <param name="includedInDisclosure">Indicates whether this contact is included on the LE/CD. Defaults to false.</param>
        /// <returns>Returns ROLE Detail container with specified type and/or other description.</returns>
        public static ROLE_DETAIL CreateRoleDetail(PartyRoleBase partyRoleType, string partyRoleTypeOtherDescription, bool includedInDisclosure = false)
        {
            ROLE_DETAIL roleDetail = new ROLE_DETAIL();
            roleDetail.PartyRoleType = ToMismoEnum(partyRoleType, isSensitiveData: false);

            if (partyRoleType == PartyRoleBase.Other)
            {
                roleDetail.PartyRoleTypeOtherDescription = ToMismoString(partyRoleTypeOtherDescription);
            }

            roleDetail.Extension = CreateRoleDetailExtension(includedInDisclosure);

            return roleDetail;
        }

        /// <summary>
        /// Creates a container holding a set of taxpayer identifiers.
        /// </summary>
        /// <param name="identifier">The taxpayer identifier number.</param>
        /// <param name="type">The taxpayer identifier type.</param>
        /// <returns>A TAXPAYER_IDENTIFIERS container.</returns>
        public static TAXPAYER_IDENTIFIERS CreateTaxpayerIdentifiers(string identifier, TaxpayerIdentifierBase type)
        {
            var taxpayerIdentifiers = new TAXPAYER_IDENTIFIERS();
            taxpayerIdentifiers.TaxpayerIdentifierList.Add(CreateTaxpayerIdentifier(identifier, type, GetNextSequenceNumber(taxpayerIdentifiers.TaxpayerIdentifierList)));

            return taxpayerIdentifiers;
        }

        /// <summary>
        /// Creates a basic INDIVIDUAL container with just a name.
        /// </summary>
        /// <param name="name">The individual's name.</param>
        /// <returns>An INDIVUDAL container with the individual's name.</returns>
        public static INDIVIDUAL CreateIndividual(string name)
        {
            return CreateIndividual(name, string.Empty, string.Empty, string.Empty, ContactPointRoleBase.Blank);
        }

        /// <summary>
        /// Creates a basic INDIVIDUAL container with a name and contact information.
        /// </summary>
        /// <param name="name">The name of the individual.</param>
        /// <param name="phone">The phone number of the individual.</param>
        /// <param name="fax">The fax number of the individual.</param>
        /// <param name="email">The email of the individual.</param>
        /// <param name="contactPointType">The type of contact point, home | work | etc.</param>
        /// <returns>An INDIVIDUAL container with a name and contact information.</returns>
        public static INDIVIDUAL CreateIndividual(string name, string phone, string fax, string email, ContactPointRoleBase contactPointType)
        {
            var individual = new INDIVIDUAL();
            individual.Name = CreateName(name, isLegalEntity: false);
            individual.ContactPoints = CreateContactPoints(phone, fax, email, contactPointType);

            return individual;
        }

        /// <summary>
        /// Creates a LICENSES container with the given state license and Nationwide Mortgage Licensing System license.
        /// </summary>
        /// <param name="licenseNumber">The state license.</param>
        /// <param name="nmlsLicense">The NMLS license.</param>
        /// <param name="state">The state of the license.</param>
        /// <param name="loanType">The type of the loan.</param>
        /// <returns>A LICENSES container with the given state license and Nationwide Mortgage Licensing System license.</returns>
        public static LICENSES CreateLicenses(string licenseNumber, string nmlsLicense, string state, E_sLT loanType)
        {
            LICENSES licenses = new LICENSES();
            licenses.LicenseList.Add(CreateLicense(nmlsLicense, nmls: true, fha_va: false, state: state, loanType: loanType, sequence: GetNextSequenceNumber(licenses.LicenseList)));
            licenses.LicenseList.Add(CreateLicense(licenseNumber, nmls: false, fha_va: false, state: state, loanType: loanType, sequence: GetNextSequenceNumber(licenses.LicenseList)));

            return licenses;
        }

        /// <summary>
        /// Creates a LICENSE container with the given license.
        /// </summary>
        /// <param name="licenseNumber">The license number.</param>
        /// <param name="nmls">True if the license is an NMLS license. Otherwise false.</param>
        /// <param name="fha_va">True if the license is an FHA or VA license. Otherwise false.</param>
        /// <param name="state">The state of the license.</param>
        /// <param name="loanType">The type of the loan.</param>
        /// <param name="sequence">The sequence number of the license among the list of licenses.</param>
        /// <returns>A LICENSE container with the given license.</returns>
        public static LICENSE CreateLicense(string licenseNumber, bool nmls, bool fha_va, string state, E_sLT loanType, int sequence)
        {
            if (string.IsNullOrEmpty(licenseNumber))
            {
                return null;
            }

            var license = new LICENSE();
            if (nmls)
            {
                license.LicenseDetail = CreateLicenseDetail_NMLS(licenseNumber);
            }
            else if (fha_va)
            {
                license.LicenseDetail = CreateLicenseDetail_FHA_VA(licenseNumber, loanType);
            }
            else
            {
                license.LicenseDetail = CreateLicenseDetail_State(licenseNumber, state);
            }

            license.SequenceNumber = sequence;

            return license;
        }

        /// <summary>
        /// Creates CLOSING_AGENT container based on specified type and/or other description.
        /// </summary>
        /// <param name="closingAgentType">The closing agent type.</param>
        /// <param name="closingAgentTypeOtherDescription">The closing agent other description, used for other type.</param>
        /// <returns>Returns CLOSING_AGENT container with specified type and/or other description.</returns>
        public static CLOSING_AGENT CreateClosingAgent(ClosingAgentBase closingAgentType, string closingAgentTypeOtherDescription)
        {
            var closingAgent = new CLOSING_AGENT();
            closingAgent.ClosingAgentType = ToMismoEnum(closingAgentType, isSensitiveData: false);

            if (closingAgentType == ClosingAgentBase.Other)
            {
                closingAgent.ClosingAgentTypeOtherDescription = ToMismoString(closingAgentTypeOtherDescription);
            }

            return closingAgent;
        }

        //////////// Mappings ////////////

        /// <summary>
        /// Retrieves the asset enumeration value corresponding to the given asset type.
        /// </summary>
        /// <param name="assetType">The type of asset.</param>
        /// <returns>The corresponding asset enumeration value.</returns>
        public static AssetBase GetAssetBaseValue(E_AssetRegularT assetType)
        {
            switch (assetType)
            {
                case E_AssetRegularT.Auto:
                    return AssetBase.Automobile;
                case E_AssetRegularT.Bonds:
                    return AssetBase.Bond;
                case E_AssetRegularT.BridgeLoanNotDeposited:
                    return AssetBase.BridgeLoanNotDeposited;
                case E_AssetRegularT.CertificateOfDeposit:
                    return AssetBase.CertificateOfDepositTimeDeposit;
                case E_AssetRegularT.Checking:
                    return AssetBase.CheckingAccount;
                case E_AssetRegularT.EmployerAssistance:
                    return AssetBase.EmployerAssistance;
                case E_AssetRegularT.GiftEquity:
                    return AssetBase.GiftOfPropertyEquity;
                case E_AssetRegularT.GiftFunds:
                    return AssetBase.GiftOfCash;
                case E_AssetRegularT.Grant:
                    return AssetBase.Grant;
                case E_AssetRegularT.IndividualDevelopmentAccount:
                    return AssetBase.IndividualDevelopmentAccount;
                case E_AssetRegularT.LeasePurchaseCredit:
                    return AssetBase.GiftOfPropertyEquity;
                case E_AssetRegularT.MoneyMarketFund:
                    return AssetBase.MoneyMarketFund;
                case E_AssetRegularT.MutualFunds:
                    return AssetBase.MutualFund;
                case E_AssetRegularT.PendingNetSaleProceedsFromRealEstateAssets:
                    return AssetBase.PendingNetSaleProceedsFromRealEstateAssets;
                case E_AssetRegularT.ProceedsFromSaleOfNonRealEstateAsset:
                    return AssetBase.ProceedsFromSaleOfNonRealEstateAsset;
                case E_AssetRegularT.ProceedsFromUnsecuredLoan:
                    return AssetBase.ProceedsFromUnsecuredLoan;
                case E_AssetRegularT.Savings:
                    return AssetBase.SavingsAccount;
                case E_AssetRegularT.SecuredBorrowedFundsNotDeposit:
                    return AssetBase.BridgeLoanNotDeposited;
                case E_AssetRegularT.StockOptions:
                    return AssetBase.StockOptions;
                case E_AssetRegularT.Stocks:
                    return AssetBase.Stock;
                case E_AssetRegularT.SweatEquity:
                    return AssetBase.GiftOfPropertyEquity;
                case E_AssetRegularT.TradeEquityFromPropertySwap:
                    return AssetBase.GiftOfPropertyEquity;
                case E_AssetRegularT.TrustFunds:
                    return AssetBase.TrustAccount;
                case E_AssetRegularT.OtherIlliquidAsset:
                case E_AssetRegularT.OtherLiquidAsset:
                case E_AssetRegularT.OtherPurchaseCredit:
                    return AssetBase.Other;
                default:
                    throw new UnhandledEnumException(assetType);
            }
        }

        /// <summary>
        /// Gets the loan purpose value.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static HMDAPurposeOfLoanBase GetHmdaPurposeOfLoanBaseValue(sHmdaLoanPurposeT loanPurpose)
        {
            switch (loanPurpose)
            {
                case sHmdaLoanPurposeT.HomeImprovement:
                    return HMDAPurposeOfLoanBase.HomeImprovement;
                case sHmdaLoanPurposeT.HomePurchase:
                    return HMDAPurposeOfLoanBase.HomePurchase;
                case sHmdaLoanPurposeT.CashOutRefinancing:
                case sHmdaLoanPurposeT.Refinancing:
                    return HMDAPurposeOfLoanBase.Refinancing;
                case sHmdaLoanPurposeT.OtherPurpose:
                    return HMDAPurposeOfLoanBase.Other;
                case sHmdaLoanPurposeT.LeaveBlank:
                    return HMDAPurposeOfLoanBase.Blank;
                default:
                    throw new UnhandledEnumException(loanPurpose);
            }
        }

        /// <summary>
        /// Gets the disposition value.
        /// </summary>
        /// <param name="action">The HMDA action taken.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static HMDADispositionBase GetHmdaDispositionBaseValue(HmdaActionTaken action)
        {
            switch (action)
            {
                case HmdaActionTaken.ApplicationApprovedButNotAccepted:
                    return HMDADispositionBase.ApplicationApprovedButNotAccepted;
                case HmdaActionTaken.ApplicationDenied:
                    return HMDADispositionBase.ApplicationDenied;
                case HmdaActionTaken.ApplicationWithdrawnByApplicant:
                    return HMDADispositionBase.ApplicationWithdrawn;
                case HmdaActionTaken.FileClosedForIncompleteness:
                    return HMDADispositionBase.FileClosedForIncompleteness;
                case HmdaActionTaken.LoanOriginated:
                    return HMDADispositionBase.LoanOriginated;
                case HmdaActionTaken.PreapprovalRequestApprovedButNotAccepted:
                    return HMDADispositionBase.PreapprovalRequestApprovedButNotAccepted;
                case HmdaActionTaken.PreapprovalRequestDenied:
                    return HMDADispositionBase.PreapprovalRequestDenied;
                case HmdaActionTaken.PurchasedLoan:
                    return HMDADispositionBase.LoanPurchasedByYourInstitution;
                case HmdaActionTaken.Blank:
                    return HMDADispositionBase.Blank;
                default:
                    throw new UnhandledEnumException(action);
            }
        }

        /// <summary>
        /// Gets the reason for denial value.
        /// </summary>
        /// <param name="reason">The reason an application was denied.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static HMDAReasonForDenialBase GetHmdaReasonForDenialBaseValue(HmdaDenialReason reason)
        {
            switch (reason)
            {
                case HmdaDenialReason.Collateral:
                    return HMDAReasonForDenialBase.InsufficientCollateralValue;
                case HmdaDenialReason.CreditApplicationIncomplete:
                    return HMDAReasonForDenialBase.CreditApplicationIncomplete;
                case HmdaDenialReason.CreditHistory:
                    return HMDAReasonForDenialBase.InsufficientCreditHistory;
                case HmdaDenialReason.DebtToIncomeRatio:
                    return HMDAReasonForDenialBase.InsufficientIncome;
                case HmdaDenialReason.EmploymentHistory:
                    return HMDAReasonForDenialBase.InsufficientEmploymentHistory;
                case HmdaDenialReason.InsufficientCash:
                    return HMDAReasonForDenialBase.InsufficientCash;
                case HmdaDenialReason.MortgageInsuranceDenied:
                    return HMDAReasonForDenialBase.MortgageInsuranceDenied;
                case HmdaDenialReason.NotApplicable:
                    return HMDAReasonForDenialBase.NotApplicable;
                case HmdaDenialReason.Other:
                    return HMDAReasonForDenialBase.Other;
                case HmdaDenialReason.UnverifiableInformation:
                    return HMDAReasonForDenialBase.UnverifiableInformation;
                case HmdaDenialReason.Blank:
                    return HMDAReasonForDenialBase.Blank;
                default:
                    throw new UnhandledEnumException(reason);
            }
        }

        /// <summary>
        /// Gets the application submission type value.
        /// </summary>
        /// <param name="submissionType">The application submission type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static HMDAApplicationSubmissionBase GetHmdaApplicationSubmissionBaseValue(sHmdaSubmissionApplicationT submissionType)
        {
            switch (submissionType)
            {
                case sHmdaSubmissionApplicationT.NA:
                    return HMDAApplicationSubmissionBase.NotApplicable;
                case sHmdaSubmissionApplicationT.NotSubmittedDirectlyToInstitution:
                    return HMDAApplicationSubmissionBase.NotSubmittedDirectly;
                case sHmdaSubmissionApplicationT.SubmittedDirectlyToInstitution:
                    return HMDAApplicationSubmissionBase.SubmittedDirectly;
                case sHmdaSubmissionApplicationT.LeaveBlank:
                    return HMDAApplicationSubmissionBase.Blank;
                default:
                    throw new UnhandledEnumException(submissionType);
            }
        }

        /// <summary>
        /// Gets the initially payable value.
        /// </summary>
        /// <param name="initiallyPayable">Indicates whether the loan was initially payable to the reporting institution.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusBase GetHmdaCoveredLoanInitiallyPayableToReportingInstitutionStatusBaseValue(sHmdaInitiallyPayableToInstitutionT initiallyPayable)
        {
            switch (initiallyPayable)
            {
                case sHmdaInitiallyPayableToInstitutionT.InitiallyPayableToInstitution:
                    return HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusBase.InitiallyPayable;
                case sHmdaInitiallyPayableToInstitutionT.NA:
                    return HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusBase.NotApplicable;
                case sHmdaInitiallyPayableToInstitutionT.NotInitiallyPayableToInstitution:
                    return HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusBase.NotInitiallyPayable;
                case sHmdaInitiallyPayableToInstitutionT.LeaveBlank:
                    return HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusBase.Blank;
                default:
                    throw new UnhandledEnumException(initiallyPayable);
            }
        }

        /// <summary>
        /// Creates a FeePaidToEnum object with the paid-to type based on the provided agent information.
        /// </summary>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <param name="affiliate">True if the entity imposing the fee is an affiliate of either the lender or the originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the entity imposing the fee is a third party. Otherwise false.</param>
        /// <param name="beneficiaryType">A string representation of the agent type of the fee beneficiary.</param>
        /// <param name="beneficiary">The agent type of the fee beneficiary.</param>
        /// <param name="beneficiaryDescription">A description of the fee beneficiary.</param>
        /// <param name="otherDescription">The target <see cref="MISMOString"/> to contain the paid-to type other description, if the paid-to type resolves to other. Otherwise it will be null.</param>
        /// <returns>A FeePaidToEnum object with the paid-to type based on the provided agent information.</returns>
        public static MISMOEnum<FeePaidToBase> GetFeePaidToType(CAgentFields beneficiaryContact, bool affiliate, bool thirdParty, string beneficiaryType, E_AgentRoleT beneficiary, string beneficiaryDescription, out MISMOString otherDescription)
        {
            MISMOEnum<FeePaidToBase> feePaidToType = null;
            otherDescription = null;

            if (beneficiaryContact != null)
            {
                feePaidToType = ToFeePaidToEnum(beneficiaryContact, affiliate, thirdParty);

                if (feePaidToType != null && feePaidToType.EnumValue == FeePaidToBase.Other)
                {
                    otherDescription = ToMismoString(beneficiaryType);
                }
            }

            if (feePaidToType == null)
            {
                feePaidToType = ToFeePaidToEnum(beneficiary, affiliate, thirdParty);

                if (feePaidToType?.IsSetToOther ?? false)
                {
                    string description = string.IsNullOrEmpty(beneficiaryDescription) ? GetXmlEnumName(beneficiary) : beneficiaryDescription;

                    otherDescription = ToMismoString(description);
                }
            }

            return feePaidToType;
        }

        /// <summary>
        /// Creates an object specifying the entity who will receive payment for a service.
        /// </summary>
        /// <param name="agent">The entity who will receive payment for a service.</param>
        /// <param name="affiliate">True if the entity is an affiliate of either the lender or the originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the entity is a third party. Otherwise false.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FeePaidToBase}" /> object.</returns>
        public static MISMOEnum<FeePaidToBase> ToFeePaidToEnum(CAgentFields agent, bool affiliate, bool thirdParty)
        {
            if (!agent.IsValid || agent.RecordId == Guid.Empty || agent.IsNewRecord)
            {
                return null;
            }

            return ToMismoEnum(GetFeePaidToBaseValue(agent.AgentRoleT, agent.IsLender, agent.IsOriginator, affiliate, agent.IsOriginatorAffiliate, agent.IsLenderAffiliate, thirdParty), isSensitiveData: true);
        }

        /// <summary>
        /// Retrieves the enumerated value corresponding to the entity who will receive payment for a service.
        /// </summary>
        /// <param name="agentType">The type of entity who will receive payment for a service.</param>
        /// <param name="lender">True if the entity is the lender. Otherwise false.</param>
        /// <param name="originator">True if the entity is the originator. Otherwise false.</param>
        /// <param name="affiliate">True if the entity is an affiliate of either the lender or originator. Otherwise false.</param>
        /// <param name="isOriginatorAffiliate">True if is an originator affiliate. Only applicable if is an affiliate.</param>
        /// <param name="isLenderAffiliate">True if is a lender affiliate. Only appliable if is an affiliate in the first place.</param>
        /// <param name="thirdParty">True if the agent is a third party. Otherwise false.</param>
        /// <returns>The corresponding value.</returns>
        public static FeePaidToBase GetFeePaidToBaseValue(E_AgentRoleT agentType, bool lender, bool originator, bool affiliate, bool isOriginatorAffiliate, bool isLenderAffiliate, bool thirdParty)
        {
            if (agentType == E_AgentRoleT.Broker)
            {
                return FeePaidToBase.Broker;
            }
            else if (agentType == E_AgentRoleT.Investor)
            {
                return FeePaidToBase.Investor;
            }
            else if (agentType == E_AgentRoleT.Lender)
            {
                return FeePaidToBase.Lender;
            }
            else if (lender)
            {
                return FeePaidToBase.Lender;
            }
            else if (originator)
            {
                return FeePaidToBase.Broker;
            }
            else if (affiliate)
            {
                if (isOriginatorAffiliate && !isLenderAffiliate)
                {
                    return FeePaidToBase.BrokerAffiliate;
                }
                else
                {
                    return FeePaidToBase.LenderAffiliate;
                }
            }
            else if (thirdParty)
            {
                return FeePaidToBase.ThirdPartyProvider;
            }
            else
            {
                return FeePaidToBase.Other;
            }
        }

        /// <summary>
        /// Creates an object specifying the entity who will receive payment for a service.
        /// For use when only the agent type is available, such as when a fee object has a beneficiary type but not an agent record.
        /// </summary>
        /// <param name="agentType">The type of entity who will receive payment for a service.</param>
        /// <param name="affiliate">True if the entity is an affiliate of either the lender or the originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the entity to which the fee is paid is a third party. Otherwise false.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FeePaidToBase}" /> object.</returns>
        public static MISMOEnum<FeePaidToBase> ToFeePaidToEnum(E_AgentRoleT agentType, bool affiliate, bool thirdParty)
        {
            //// Assumes false for parameters that would be gathered from the agent record, if there were an associated agent.
            return ToMismoEnum(GetFeePaidToBaseValue(agentType, false, false, affiliate, false, false, thirdParty), isSensitiveData: false);
        }

        /// <summary>
        /// Sets the disclosure section for Section K adjustments based on the current document package
        /// type and the calculation method for the package.
        /// </summary>
        /// <param name="isClosingPackage">Indicates whether a closing package is being requested.</param>
        /// <param name="loanEstimateCalculationType">The LE calculation type (sTRIDLoanEstimateCashToCloseCalcMethodT).</param>
        /// <param name="closingDisclosureCalculationType">The CD calculation type (sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodD).</param>
        /// <returns>An IntegratedDisclosureSectionBase enum.</returns>
        public static IntegratedDisclosureSectionBase GetDisclosureSectionForSectionKAdjustments(bool isClosingPackage, E_CashToCloseCalcMethodT loanEstimateCalculationType, E_CashToCloseCalcMethodT closingDisclosureCalculationType)
        {
            var disclosureSectionForSectionK = IntegratedDisclosureSectionBase.PayoffsAndPayments;

            if ((isClosingPackage && closingDisclosureCalculationType == E_CashToCloseCalcMethodT.Standard)
                || (!isClosingPackage && loanEstimateCalculationType == E_CashToCloseCalcMethodT.Standard))
            {
                disclosureSectionForSectionK = IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing;
            }

            return disclosureSectionForSectionK;
        }

        /// <summary>
        /// Gets the integrated disclosure section type based on the type of disclosure (loan estimate | closing disclosure) and the given closing cost details section of the disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure.</param>
        /// <param name="sectionType">The closing cost details section.</param>
        /// <param name="canShop">The "Can Shop" value for the fee, for determining "B or C" value. </param>
        /// <returns>The <see cref="IntegratedDisclosureSectionBase" /> value corresponding to the given closing cost details section of the given disclosure.</returns>
        public static IntegratedDisclosureSectionBase GetIntegratedDisclosureSectionBaseValue(IntegratedDisclosureDocumentBase documentType, E_IntegratedDisclosureSectionT sectionType, bool canShop = false)
        {
            if (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure)
            {
                return IntegratedDisclosureSectionBase.Blank;
            }

            bool estimate = documentType == IntegratedDisclosureDocumentBase.LoanEstimate;

            switch (sectionType)
            {
                case E_IntegratedDisclosureSectionT.SectionA:
                    return IntegratedDisclosureSectionBase.OriginationCharges;
                case E_IntegratedDisclosureSectionT.SectionB:
                    return estimate ? IntegratedDisclosureSectionBase.ServicesYouCannotShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidNotShopFor;
                case E_IntegratedDisclosureSectionT.SectionBorC:
                    // sg 10/08/2015 - Case 224832 - If canShop is checked, return Section C logic, if not checked, return Section B logic.
                    if (canShop)
                    {
                        return estimate ? IntegratedDisclosureSectionBase.ServicesYouCanShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidShopFor;
                    }
                    else
                    {
                        return estimate ? IntegratedDisclosureSectionBase.ServicesYouCannotShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidNotShopFor;
                    }

                case E_IntegratedDisclosureSectionT.SectionC:
                    return estimate ? IntegratedDisclosureSectionBase.ServicesYouCanShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidShopFor;
                case E_IntegratedDisclosureSectionT.SectionD:
                    return IntegratedDisclosureSectionBase.TotalLoanCosts;
                case E_IntegratedDisclosureSectionT.SectionE:
                    return IntegratedDisclosureSectionBase.TaxesAndOtherGovernmentFees;
                case E_IntegratedDisclosureSectionT.SectionF:
                    return IntegratedDisclosureSectionBase.Prepaids;
                case E_IntegratedDisclosureSectionT.SectionG:
                    return IntegratedDisclosureSectionBase.InitialEscrowPaymentAtClosing;
                case E_IntegratedDisclosureSectionT.SectionH:
                    return IntegratedDisclosureSectionBase.OtherCosts;
                case E_IntegratedDisclosureSectionT.LeaveBlank:
                    return IntegratedDisclosureSectionBase.Blank;
                default:
                    throw new UnhandledEnumException(sectionType);
            }
        }

        /// <summary>
        /// Retrieves the corresponding application taken method enumeration from the given method.
        /// </summary>
        /// <param name="method">The application taken method.</param>
        /// <returns>The corresponding value.</returns>
        public static ApplicationTakenMethodBase GetApplicationTakenMethodBaseValue(E_aIntrvwrMethodT method)
        {
            switch (method)
            {
                case E_aIntrvwrMethodT.ByMail:
                    return ApplicationTakenMethodBase.Mail;
                case E_aIntrvwrMethodT.ByTelephone:
                    return ApplicationTakenMethodBase.Telephone;
                case E_aIntrvwrMethodT.FaceToFace:
                    return ApplicationTakenMethodBase.FaceToFace;
                case E_aIntrvwrMethodT.Internet:
                    return ApplicationTakenMethodBase.Internet;
                case E_aIntrvwrMethodT.LeaveBlank:
                    return ApplicationTakenMethodBase.Blank;
                default:
                    throw new UnhandledEnumException(method);
            }
        }

        /// <summary>
        /// Retrieves the RefinanceImprovementsBase value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding RefinanceImprovementBase value.</returns>
        public static RefinanceImprovementsBase GetRefinanceImprovementsBaseValue(E_sSpImprovTimeFrameT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sSpImprovTimeFrameT.Made:
                    return RefinanceImprovementsBase.Made;
                case E_sSpImprovTimeFrameT.ToBeMade:
                    return RefinanceImprovementsBase.ToBeMade;
                case E_sSpImprovTimeFrameT.LeaveBlank:
                    return RefinanceImprovementsBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the BorrowerClassification value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable BorrowerClassification value.</returns>
        public static BorrowerClassificationBase GetBorrowerClassificationBaseValue(E_BorrowerModeT lqbValue)
        {
            switch (lqbValue)
            {
                case E_BorrowerModeT.Borrower:
                    return BorrowerClassificationBase.Primary;
                case E_BorrowerModeT.Coborrower:
                    return BorrowerClassificationBase.Secondary;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the BorrowerRelationshipTitle value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding BorrowerRelationshipTitle value.</returns>
        public static BorrowerRelationshipTitleBase GetBorrowerRelationshipTitleBaseValue(E_aRelationshipTitleT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aRelationshipTitleT.AHusbandAndWife:
                case E_aRelationshipTitleT.HusbandAndWife:
                    return BorrowerRelationshipTitleBase.AHusbandAndWife;
                case E_aRelationshipTitleT.AMarriedMan:
                    return BorrowerRelationshipTitleBase.AMarriedMan;
                case E_aRelationshipTitleT.AMarriedPerson:
                    return BorrowerRelationshipTitleBase.AMarriedPerson;
                case E_aRelationshipTitleT.AMarriedWoman:
                    return BorrowerRelationshipTitleBase.AMarriedWoman;
                case E_aRelationshipTitleT.AnUnmarriedMan:
                    return BorrowerRelationshipTitleBase.AnUnmarriedMan;
                case E_aRelationshipTitleT.AnUnmarriedPerson:
                    return BorrowerRelationshipTitleBase.AnUnmarriedPerson;
                case E_aRelationshipTitleT.AnUnmarriedWoman:
                    return BorrowerRelationshipTitleBase.AnUnmarriedWoman;
                case E_aRelationshipTitleT.DomesticPartners:
                    return BorrowerRelationshipTitleBase.AsDomesticPartners;
                case E_aRelationshipTitleT.ASingleMan:
                    return BorrowerRelationshipTitleBase.ASingleMan;
                case E_aRelationshipTitleT.ASinglePerson:
                    return BorrowerRelationshipTitleBase.ASinglePerson;
                case E_aRelationshipTitleT.ASingleWoman:
                    return BorrowerRelationshipTitleBase.ASingleWoman;
                case E_aRelationshipTitleT.AWidow:
                    return BorrowerRelationshipTitleBase.AWidow;
                case E_aRelationshipTitleT.AWidower:
                    return BorrowerRelationshipTitleBase.AWidower;
                case E_aRelationshipTitleT.AWifeAndHusband:
                case E_aRelationshipTitleT.WifeAndHusband:
                    return BorrowerRelationshipTitleBase.AWifeAndHusband;
                case E_aRelationshipTitleT.HerHusband:
                    return BorrowerRelationshipTitleBase.HerHusband;
                case E_aRelationshipTitleT.HisWife:
                    return BorrowerRelationshipTitleBase.HisWife;
                case E_aRelationshipTitleT.NotApplicable:
                    return BorrowerRelationshipTitleBase.NotApplicable;
                case E_aRelationshipTitleT.AMarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AMarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ChiefExecutiveOfficer:
                case E_aRelationshipTitleT.GeneralPartner:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityProperty:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityPropertyWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenants:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenantsWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsByTheEntirety:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsInCommon:
                case E_aRelationshipTitleT.HusbandAndWifeAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.HusbandAndWifeTenancyByTheEntirety:
                case E_aRelationshipTitleT.PersonalGuarantor:
                case E_aRelationshipTitleT.President:
                case E_aRelationshipTitleT.Secretary:
                case E_aRelationshipTitleT.TenancyByEntirety:
                case E_aRelationshipTitleT.TenantsByTheEntirety:
                case E_aRelationshipTitleT.Treasurer:
                case E_aRelationshipTitleT.Trustee:
                case E_aRelationshipTitleT.VicePresident:
                case E_aRelationshipTitleT.WifeAndHusbandAsCommunityProperty:
                case E_aRelationshipTitleT.WifeAndHusbandAsJointTenants:
                case E_aRelationshipTitleT.WifeAndHusbandAsTenantsInCommon:
                case E_aRelationshipTitleT.Other:
                    return BorrowerRelationshipTitleBase.Other;
                case E_aRelationshipTitleT.LeaveBlank:
                    return BorrowerRelationshipTitleBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the Income value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding Income value.</returns>
        public static IncomeBase GetIncomeBaseValue(E_aOIDescT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aOIDescT.AlimonyChildSupport:
                    return IncomeBase.ChildSupport;
                case E_aOIDescT.AutomobileExpenseAccount:
                    return IncomeBase.AutomobileAllowance;
                case E_aOIDescT.BoarderIncome:
                    return IncomeBase.BoarderIncome;
                case E_aOIDescT.SocialSecurityDisability:
                    return IncomeBase.Disability;
                case E_aOIDescT.FosterCare:
                    return IncomeBase.FosterCare;
                case E_aOIDescT.MilitaryBasePay:
                    return IncomeBase.MilitaryBasePay;
                case E_aOIDescT.MilitaryClothesAllowance:
                    return IncomeBase.MilitaryClothesAllowance;
                case E_aOIDescT.MilitaryCombatPay:
                    return IncomeBase.MilitaryCombatPay;
                case E_aOIDescT.MilitaryFlightPay:
                    return IncomeBase.MilitaryFlightPay;
                case E_aOIDescT.MilitaryHazardPay:
                    return IncomeBase.MilitaryHazardPay;
                case E_aOIDescT.MilitaryOverseasPay:
                    return IncomeBase.MilitaryOverseasPay;
                case E_aOIDescT.MilitaryPropPay:
                    return IncomeBase.MilitaryPropPay;
                case E_aOIDescT.MilitaryQuartersAllowance:
                    return IncomeBase.MilitaryQuartersAllowance;
                case E_aOIDescT.MilitaryRationsAllowance:
                    return IncomeBase.MilitaryRationsAllowance;
                case E_aOIDescT.MilitaryVariableHousingAllowance:
                    return IncomeBase.MilitaryVariableHousingAllowance;
                case E_aOIDescT.MortgageCreditCertificate:
                    return IncomeBase.MortgageCreditCertificate;
                case E_aOIDescT.RealEstateMortgageDifferential:
                    return IncomeBase.MortgageDifferential;
                case E_aOIDescT.NotesReceivableInstallment:
                    return IncomeBase.NotesReceivableInstallment;
                case E_aOIDescT.PensionRetirement:
                    return IncomeBase.Pension;
                case E_aOIDescT.SubjPropNetCashFlow:
                    return IncomeBase.SubjectPropertyNetCashFlow;
                case E_aOIDescT.TrailingCoBorrowerIncome:
                    return IncomeBase.TrailingCoBorrowerIncome;
                case E_aOIDescT.Trust:
                    return IncomeBase.Trust;
                case E_aOIDescT.UnemploymentWelfare:
                    return IncomeBase.Unemployment;
                case E_aOIDescT.VABenefitsNonEducation:
                    return IncomeBase.VABenefitsNonEducational;
                case E_aOIDescT.NonBorrowerHouseholdIncome:
                    return IncomeBase.NonBorrowerContribution;
                case E_aOIDescT.HousingChoiceVoucher:
                    return IncomeBase.PublicAssistance;
                case E_aOIDescT.SocialSecurity:
                    return IncomeBase.SocialSecurity;
                case E_aOIDescT.Disability:
                    return IncomeBase.Disability;
                case E_aOIDescT.Alimony:
                    return IncomeBase.Alimony;
                case E_aOIDescT.ChildSupport:
                    return IncomeBase.ChildSupport;
                case E_aOIDescT.ContractBasis:
                    return IncomeBase.ContractBasis;
                case E_aOIDescT.DefinedContributionPlan:
                    return IncomeBase.DefinedContributionPlan;
                case E_aOIDescT.HousingAllowance:
                    return IncomeBase.HousingAllowance;
                case E_aOIDescT.MiscellaneousIncome:
                    return IncomeBase.MiscellaneousIncome;
                case E_aOIDescT.PublicAssistance:
                    return IncomeBase.PublicAssistance;
                case E_aOIDescT.WorkersCompensation:
                    return IncomeBase.WorkersCompensation;
                case E_aOIDescT.AccessoryUnitIncome:
                case E_aOIDescT.CapitalGains:
                case E_aOIDescT.EmploymentRelatedAssets:
                case E_aOIDescT.ForeignIncome:
                case E_aOIDescT.RoyaltyPayment:
                case E_aOIDescT.SeasonalIncome:
                case E_aOIDescT.TemporaryLeave:
                case E_aOIDescT.TipIncome:
                case E_aOIDescT.Other:
                    return IncomeBase.Other;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the credit bureau.
        /// </summary>
        /// <param name="creditSource">The credit bureau.</param>
        /// <returns>The corresponding MISMO value.</returns>
        public static CreditRepositorySourceBase GetCreditRepositorySourceBaseValue(E_aDecisionCreditSourceT creditSource)
        {
            switch (creditSource)
            {
                case E_aDecisionCreditSourceT.Equifax:
                    return CreditRepositorySourceBase.Equifax;
                case E_aDecisionCreditSourceT.Experian:
                    return CreditRepositorySourceBase.Experian;
                case E_aDecisionCreditSourceT.TransUnion:
                    return CreditRepositorySourceBase.TransUnion;
                case E_aDecisionCreditSourceT.Other:
                    return CreditRepositorySourceBase.Other;
                default:
                    throw new UnhandledEnumException(creditSource);
            }
        }

        /// <summary>
        /// Retrieves the partial payment application method corresponding to the given method.
        /// </summary>
        /// <param name="partialPaymentsAccepted">The partial payment application method as represented in LendingQB.</param>
        /// <returns>THe corresponding MISMO partial payment application method.</returns>
        public static PartialPaymentApplicationMethodBase GetPartialPaymentApplicationMethodBaseValue(E_sTRIDPartialPaymentAcceptanceT partialPaymentsAccepted)
        {
            switch (partialPaymentsAccepted)
            {
                case E_sTRIDPartialPaymentAcceptanceT.NotAccepted:
                    return PartialPaymentApplicationMethodBase.Blank;
                case E_sTRIDPartialPaymentAcceptanceT.AcceptedAndApplied:
                    return PartialPaymentApplicationMethodBase.ApplyPartialPayment;
                case E_sTRIDPartialPaymentAcceptanceT.AcceptedButNotApplied:
                    return PartialPaymentApplicationMethodBase.HoldUntilCompleteAmount;
                default:
                    throw new UnhandledEnumException(partialPaymentsAccepted);
            }
        }

        /// <summary>
        /// Retrieves the RefinanceCashOutDetermination value corresponding to the given LQB value, or attempts to deduce from the loan purpose.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <param name="loanPurpose">The purpose of the loan.</param>
        /// <returns>The corresponding value.</returns>
        public static RefinanceCashOutDeterminationBase GetRefinanceCashOutDeterminationBaseValue(string lqbValue, E_sLPurposeT loanPurpose)
        {
            if (string.Equals("No Cash-Out Rate/Term", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinanceCashOutDeterminationBase.NoCashOut;
            }
            else if (string.Equals("Limited Cash-Out Rate/Term", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinanceCashOutDeterminationBase.LimitedCashOut;
            }
            else if (string.Equals("Cash-Out/Home Improvement", lqbValue, StringComparison.OrdinalIgnoreCase) ||
                     string.Equals("Cash-Out/Debt Consolidation", lqbValue, StringComparison.OrdinalIgnoreCase) ||
                     string.Equals("Cash-Out/Other", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinanceCashOutDeterminationBase.CashOut;
            }
            else if (loanPurpose == E_sLPurposeT.Refin ||
                     loanPurpose == E_sLPurposeT.FhaStreamlinedRefinance ||
                     loanPurpose == E_sLPurposeT.VaIrrrl)
            {
                return RefinanceCashOutDeterminationBase.NoCashOut;
            }
            else if (loanPurpose == E_sLPurposeT.RefinCashout || loanPurpose == E_sLPurposeT.HomeEquity)
            {
                return RefinanceCashOutDeterminationBase.CashOut;
            }
            else
            {
                return RefinanceCashOutDeterminationBase.Unknown;
            }
        }

        /// <summary>
        /// Retrieves the MISMO enumeration that corresponds to the LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding MISMO LienPriority enumeration.</returns>
        public static LienPriorityBase GetLienPriorityValue(E_sLienPosT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sLienPosT.First:
                    return LienPriorityBase.FirstLien;
                case E_sLienPosT.Second:
                    return LienPriorityBase.SecondLien;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the MortgageBase value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding MortgageBase value.</returns>
        public static MortgageBase GetMortgageBaseValue(E_sLT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sLT.Conventional:
                    return MortgageBase.Conventional;
                case E_sLT.FHA:
                    return MortgageBase.FHA;
                case E_sLT.Other:
                    return MortgageBase.Other;
                case E_sLT.UsdaRural:
                    return MortgageBase.USDARuralDevelopment;
                case E_sLT.VA:
                    return MortgageBase.VA;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the LoanPurposeBase vaue correspending to the E_NMLSLoanPurposeT value.
        /// </summary>
        /// <param name="lqbValue">The E_NMLSLoanPurposeT value.</param>
        /// <returns>The loan purpose base value.</returns>
        public static LoanPurposeBase GetLoanPurposeBaseValue(E_NMLSLoanPurposeT lqbValue)
        {
            switch (lqbValue)
            {
                case E_NMLSLoanPurposeT.Purchase:
                    return LoanPurposeBase.Purchase;
                case E_NMLSLoanPurposeT.RefinanceCashOutRefinance:
                case E_NMLSLoanPurposeT.RefinanceOtherUnknown:
                case E_NMLSLoanPurposeT.RefinanceRateTerm:
                case E_NMLSLoanPurposeT.RefinanceRestructure:
                    return LoanPurposeBase.Refinance;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the LoanPurpose enumeration corresponding to a given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <param name="isHeloc">Indicates whether the loan is a HELOC.</param>
        /// <returns>The corresponding LoanPurpose value.</returns>
        public static LoanPurposeBase GetLoanPurposeBaseValue(E_sLPurposeT lqbValue, bool isHeloc)
        {
            if (isHeloc)
            {
                return LoanPurposeBase.Other;
            }

            switch (lqbValue)
            {
                case E_sLPurposeT.Purchase:
                    return LoanPurposeBase.Purchase;
                case E_sLPurposeT.Other:
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    return LoanPurposeBase.Other;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.HomeEquity:
                case E_sLPurposeT.VaIrrrl:
                    return LoanPurposeBase.Refinance;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the MISMO fee type corresponding to the closing cost type.
        /// </summary>
        /// <param name="type">The type of the closing cost fee.</param>
        /// <param name="feeDescription">The actual fee description.</param>
        /// <param name="otherDescription">The description to go along with the FeeBase if FeeBase is Other.</param>
        /// <returns>The corresponding <see cref="FeeBase" /> type.</returns>
        public static FeeBase GetFeeBaseValue(E_ClosingCostFeeMismoFeeT type, string feeDescription, out string otherDescription)
        {
            otherDescription = null;
            switch (type)
            {
                case E_ClosingCostFeeMismoFeeT._203KConsultantFee:
                    return FeeBase.Item203KConsultantFee;
                case E_ClosingCostFeeMismoFeeT._203KDiscountOnRepairs:
                    return FeeBase.Item203KDiscountOnRepairs;
                case E_ClosingCostFeeMismoFeeT._203KInspectionFee:
                    return FeeBase.Item203KInspectionFee;
                case E_ClosingCostFeeMismoFeeT._203KPermits:
                    return FeeBase.Item203KPermits;
                case E_ClosingCostFeeMismoFeeT._203KSupplementalOriginationFee:
                    return FeeBase.Item203KSupplementalOriginationFee;
                case E_ClosingCostFeeMismoFeeT._203KTitleUpdate:
                    return FeeBase.Item203KTitleUpdate;
                case E_ClosingCostFeeMismoFeeT.ApplicationFee:
                    return FeeBase.ApplicationFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalDeskReviewFee:
                    return FeeBase.AppraisalDeskReviewFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFee:
                    return FeeBase.AppraisalFee;
                case E_ClosingCostFeeMismoFeeT.AppraisalFieldReviewFee:
                    return FeeBase.AppraisalFieldReviewFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentFee:
                    return FeeBase.AssignmentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.AssignmentRecordingFee:
                    return FeeBase.RecordingFeeForAssignment;
                case E_ClosingCostFeeMismoFeeT.AssumptionFee:
                    return FeeBase.AssumptionFee;
                case E_ClosingCostFeeMismoFeeT.AttorneyFee:
                    return FeeBase.AttorneyFee;
                case E_ClosingCostFeeMismoFeeT.BankruptcyMonitoringFee:
                    return FeeBase.BankruptcyMonitoringFee;
                case E_ClosingCostFeeMismoFeeT.BondFee:
                    return FeeBase.BondFee;
                case E_ClosingCostFeeMismoFeeT.BondReviewFee:
                    return FeeBase.BondReviewFee;
                case E_ClosingCostFeeMismoFeeT.CertificationFee:
                    return FeeBase.CertificationFee;
                case E_ClosingCostFeeMismoFeeT.CityDeedTaxStampFee:
                    return FeeBase.TaxStampForCityDeed;
                case E_ClosingCostFeeMismoFeeT.CityMortgageTaxStampFee:
                    return FeeBase.TaxStampForCityMortgage;
                case E_ClosingCostFeeMismoFeeT.CountyDeedTaxStampFee:
                    return FeeBase.TaxStampForCountyDeed;
                case E_ClosingCostFeeMismoFeeT.CountyMortgageTaxStampFee:
                    return FeeBase.TaxStampForCountyMortgage;
                case E_ClosingCostFeeMismoFeeT.ClosingProtectionLetterFee:
                    return FeeBase.TitleClosingProtectionLetterFee;
                case E_ClosingCostFeeMismoFeeT.CommitmentFee:
                    return FeeBase.CommitmentFee;
                case E_ClosingCostFeeMismoFeeT.CopyFaxFee:
                    return FeeBase.CopyOrFaxFee;
                case E_ClosingCostFeeMismoFeeT.CourierFee:
                    return FeeBase.CourierFee;
                case E_ClosingCostFeeMismoFeeT.CreditReportFee:
                    return FeeBase.CreditReportFee;
                case E_ClosingCostFeeMismoFeeT.DeedRecordingFee:
                    return FeeBase.RecordingFeeForDeed;
                case E_ClosingCostFeeMismoFeeT.DocumentaryStampFee:
                    return FeeBase.DocumentaryStampFee;
                case E_ClosingCostFeeMismoFeeT.DocumentPreparationFee:
                    return FeeBase.DocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.ElectronicDocumentDeliveryFee:
                    return FeeBase.ElectronicDocumentDeliveryFee;
                case E_ClosingCostFeeMismoFeeT.EscrowServiceFee:
                    return FeeBase.EscrowServiceFee;
                case E_ClosingCostFeeMismoFeeT.EscrowWaiverFee:
                    return FeeBase.EscrowWaiverFee;
                case E_ClosingCostFeeMismoFeeT.FloodCertification:
                    return FeeBase.FloodCertification;
                case E_ClosingCostFeeMismoFeeT.InspectionFee:
                    return FeeBase.HomeInspectionFee;
                case E_ClosingCostFeeMismoFeeT.LoanDiscountPoints:
                    return FeeBase.LoanDiscountPoints;
                case E_ClosingCostFeeMismoFeeT.LoanOriginationFee:
                    return FeeBase.LoanOriginationFee;
                case E_ClosingCostFeeMismoFeeT.MERSRegistrationFee:
                    return FeeBase.MERSRegistrationFee;
                case E_ClosingCostFeeMismoFeeT.ModificationFee:
                    return FeeBase.ModificationFee;
                case E_ClosingCostFeeMismoFeeT.MortgageBrokerFee:
                    return FeeBase.MortgageBrokerFee;
                case E_ClosingCostFeeMismoFeeT.MortgageRecordingFee:
                    return FeeBase.RecordingFeeForMortgage;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateFee:
                    return FeeBase.MunicipalLienCertificateFee;
                case E_ClosingCostFeeMismoFeeT.MunicipalLienCertificateRecordingFee:
                    return FeeBase.RecordingFeeForMunicipalLienCertificate;
                case E_ClosingCostFeeMismoFeeT.NotaryFee:
                    return FeeBase.NotaryFee;
                case E_ClosingCostFeeMismoFeeT.PayoffRequestFee:
                    return FeeBase.PayoffRequestFee;
                case E_ClosingCostFeeMismoFeeT.PestInspectionFee:
                    return FeeBase.PestInspectionFee;
                case E_ClosingCostFeeMismoFeeT.ProcessingFee:
                    return FeeBase.ProcessingFee;
                case E_ClosingCostFeeMismoFeeT.RealEstateCommission:
                    return FeeBase.RealEstateCommissionBuyersBroker;
                case E_ClosingCostFeeMismoFeeT.RedrawFee:
                    return FeeBase.RedrawFee;
                case E_ClosingCostFeeMismoFeeT.ReinspectionFee:
                    return FeeBase.ReinspectionFee;
                case E_ClosingCostFeeMismoFeeT.ReleaseRecordingFee:
                    return FeeBase.RecordingFeeForRelease;
                case E_ClosingCostFeeMismoFeeT.SettlementOrClosingFee:
                    return FeeBase.SettlementFee;
                case E_ClosingCostFeeMismoFeeT.SigningAgentFee:
                    return FeeBase.SigningAgentFee;
                case E_ClosingCostFeeMismoFeeT.StateDeedTaxStampFee:
                    return FeeBase.TaxStampForStateDeed;
                case E_ClosingCostFeeMismoFeeT.StateMortgageTaxStampFee:
                    return FeeBase.TaxStampForStateMortgage;
                case E_ClosingCostFeeMismoFeeT.SubordinationFee:
                    return FeeBase.SubordinationFee;
                case E_ClosingCostFeeMismoFeeT.SurveyFee:
                    return FeeBase.SurveyFee;
                case E_ClosingCostFeeMismoFeeT.TaxRelatedServiceFee:
                    return FeeBase.TaxServiceFee;
                case E_ClosingCostFeeMismoFeeT.TitleEndorsementFee:
                    return FeeBase.TitleEndorsementFee;
                case E_ClosingCostFeeMismoFeeT.TitleExaminationFee:
                    return FeeBase.TitleExaminationFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceBinderFee:
                    return FeeBase.TitleInsuranceBinderFee;
                case E_ClosingCostFeeMismoFeeT.TitleInsuranceFee:
                    return FeeBase.TitleInsuranceFee;
                case E_ClosingCostFeeMismoFeeT.TitleLendersCoveragePremium:
                    return FeeBase.TitleLendersCoveragePremium;
                case E_ClosingCostFeeMismoFeeT.TitleOwnersCoveragePremium:
                    return FeeBase.TitleOwnersCoveragePremium;
                case E_ClosingCostFeeMismoFeeT.UnderwritingFee:
                    return FeeBase.UnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.WireTransferFee:
                    return FeeBase.WireTransferFee;
                case E_ClosingCostFeeMismoFeeT.LoanOriginatorCompensation:
                    return FeeBase.LoanOriginatorCompensation;
                case E_ClosingCostFeeMismoFeeT.RuralHousingFee:
                    return FeeBase.USDARuralDevelopmentGuaranteeFee;
                //// OPM 227381 - MISMO 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.AppraisalManagementCompanyFee:
                    return FeeBase.AppraisalManagementCompanyFee;
                case E_ClosingCostFeeMismoFeeT.AsbestosInspectionFee:
                    return FeeBase.AsbestosInspectionFee;
                case E_ClosingCostFeeMismoFeeT.AutomatedUnderwritingFee:
                    return FeeBase.AutomatedUnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.AVMFee:
                    return FeeBase.AVMFee;
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationDues:
                    return FeeBase.CondominiumAssociationDues;
                case E_ClosingCostFeeMismoFeeT.CondominiumAssociationSpecialAssessment:
                    return FeeBase.CondominiumAssociationSpecialAssessment;
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationDues:
                    return FeeBase.CooperativeAssociationDues;
                case E_ClosingCostFeeMismoFeeT.CooperativeAssociationSpecialAssessment:
                    return FeeBase.CooperativeAssociationSpecialAssessment;
                case E_ClosingCostFeeMismoFeeT.CreditDisabilityInsurancePremium:
                    return FeeBase.CreditDisabilityInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.CreditLifeInsurancePremium:
                    return FeeBase.CreditLifeInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.CreditPropertyInsurancePremium:
                    return FeeBase.CreditPropertyInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.CreditUnemploymentInsurancePremium:
                    return FeeBase.CreditUnemploymentInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.DebtCancellationInsurancePremium:
                    return FeeBase.DebtCancellationInsurancePremium;
                case E_ClosingCostFeeMismoFeeT.DeedPreparationFee:
                    return FeeBase.DeedPreparationFee;
                case E_ClosingCostFeeMismoFeeT.DisasterInspectionFee:
                    return FeeBase.DisasterInspectionFee;
                case E_ClosingCostFeeMismoFeeT.DryWallInspectionFee:
                    return FeeBase.DryWallInspectionFee;
                case E_ClosingCostFeeMismoFeeT.ElectricalInspectionFee:
                    return FeeBase.ElectricalInspectionFee;
                case E_ClosingCostFeeMismoFeeT.EnvironmentalInspectionFee:
                    return FeeBase.EnvironmentalInspectionFee;
                case E_ClosingCostFeeMismoFeeT.FilingFee:
                    return FeeBase.FilingFee;
                case E_ClosingCostFeeMismoFeeT.FoundationInspectionFee:
                    return FeeBase.FoundationInspectionFee;
                case E_ClosingCostFeeMismoFeeT.HeatingCoolingInspectionFee:
                    return FeeBase.HeatingCoolingInspectionFee;
                case E_ClosingCostFeeMismoFeeT.HighCostMortgageCounselingFee:
                    return FeeBase.HighCostMortgageCounselingFee;
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationDues:
                    return FeeBase.HomeownersAssociationDues;
                case E_ClosingCostFeeMismoFeeT.HomeownersAssociationSpecialAssessment:
                    return FeeBase.HomeownersAssociationSpecialAssessment;
                case E_ClosingCostFeeMismoFeeT.HomeWarrantyFee:
                    return FeeBase.HomeWarrantyFee;
                case E_ClosingCostFeeMismoFeeT.LeadInspectionFee:
                    return FeeBase.LeadInspectionFee;
                case E_ClosingCostFeeMismoFeeT.LendersAttorneyFee:
                    return FeeBase.LendersAttorneyFee;
                case E_ClosingCostFeeMismoFeeT.LoanLevelPriceAdjustment:
                    return FeeBase.LoanLevelPriceAdjustment;
                case E_ClosingCostFeeMismoFeeT.ManualUnderwritingFee:
                    return FeeBase.ManualUnderwritingFee;
                case E_ClosingCostFeeMismoFeeT.ManufacturedHousingInspectionFee:
                    return FeeBase.ManufacturedHousingInspectionFee;
                case E_ClosingCostFeeMismoFeeT.MIUpfrontPremium:
                    return FeeBase.MIUpfrontPremium;
                case E_ClosingCostFeeMismoFeeT.MoldInspectionFee:
                    return FeeBase.MoldInspectionFee;
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeCountyOrParish:
                    return FeeBase.MortgageSurchargeCountyOrParish;
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeMunicipal:
                    return FeeBase.MortgageSurchargeMunicipal;
                case E_ClosingCostFeeMismoFeeT.MortgageSurchargeState:
                    return FeeBase.MortgageSurchargeState;
                case E_ClosingCostFeeMismoFeeT.PlumbingInspectionFee:
                    return FeeBase.PlumbingInspectionFee;
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyPreparationFee:
                    return FeeBase.PowerOfAttorneyPreparationFee;
                case E_ClosingCostFeeMismoFeeT.PowerOfAttorneyRecordingFee:
                    return FeeBase.PowerOfAttorneyRecordingFee;
                case E_ClosingCostFeeMismoFeeT.PreclosingVerificationControlFee:
                    return FeeBase.PreclosingVerificationControlFee;
                case E_ClosingCostFeeMismoFeeT.PropertyInspectionWaiverFee:
                    return FeeBase.PropertyInspectionWaiverFee;
                case E_ClosingCostFeeMismoFeeT.PropertyTaxStatusResearchFee:
                    return FeeBase.TaxStatusResearchFee;
                case E_ClosingCostFeeMismoFeeT.RadonInspectionFee:
                    return FeeBase.RadonInspectionFee;
                case E_ClosingCostFeeMismoFeeT.RateLockFee:
                    return FeeBase.RateLockFee;
                case E_ClosingCostFeeMismoFeeT.ReconveyanceFee:
                    return FeeBase.ReconveyanceFee;
                case E_ClosingCostFeeMismoFeeT.RecordingFeeForSubordination:
                    return FeeBase.RecordingFeeForSubordination;
                case E_ClosingCostFeeMismoFeeT.RecordingFeeTotal:
                    return FeeBase.RecordingFeeTotal;
                case E_ClosingCostFeeMismoFeeT.RepairsFee:
                    return FeeBase.RepairsFee;
                case E_ClosingCostFeeMismoFeeT.RoofInspectionFee:
                    return FeeBase.RoofInspectionFee;
                case E_ClosingCostFeeMismoFeeT.SepticInspectionFee:
                    return FeeBase.SepticInspectionFee;
                case E_ClosingCostFeeMismoFeeT.SmokeDetectorInspectionFee:
                    return FeeBase.SmokeDetectorInspectionFee;
                case E_ClosingCostFeeMismoFeeT.StateTitleInsuranceFee:
                    return FeeBase.StateTitleInsuranceFee;
                case E_ClosingCostFeeMismoFeeT.StructuralInspectionFee:
                    return FeeBase.StructuralInspectionFee;
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownAdministrationFee:
                    return FeeBase.TemporaryBuydownAdministrationFee;
                case E_ClosingCostFeeMismoFeeT.TemporaryBuydownPoints:
                    return FeeBase.TemporaryBuydownPoints;
                case E_ClosingCostFeeMismoFeeT.TitleCertificationFee:
                    return FeeBase.TitleCertificationFee;
                case E_ClosingCostFeeMismoFeeT.TitleClosingFee:
                    return FeeBase.TitleClosingFee;
                case E_ClosingCostFeeMismoFeeT.TitleDocumentPreparationFee:
                    return FeeBase.TitleDocumentPreparationFee;
                case E_ClosingCostFeeMismoFeeT.TitleFinalPolicyShortFormFee:
                    return FeeBase.TitleFinalPolicyShortFormFee;
                case E_ClosingCostFeeMismoFeeT.TitleNotaryFee:
                    return FeeBase.TitleNotaryFee;
                case E_ClosingCostFeeMismoFeeT.TitleServicesSalesTax:
                    return FeeBase.TitleServicesSalesTax;
                case E_ClosingCostFeeMismoFeeT.TitleUnderwritingIssueResolutionFee:
                    return FeeBase.TitleUnderwritingIssueResolutionFee;
                case E_ClosingCostFeeMismoFeeT.TransferTaxTotal:
                    return FeeBase.TransferTaxTotal;
                case E_ClosingCostFeeMismoFeeT.VerificationOfAssetsFee:
                    return FeeBase.VerificationOfAssetsFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfEmploymentFee:
                    return FeeBase.VerificationOfEmploymentFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfIncomeFee:
                    return FeeBase.VerificationOfIncomeFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfResidencyStatusFee:
                    return FeeBase.VerificationOfResidencyStatusFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxpayerIdentificationFee:
                    return FeeBase.VerificationOfTaxpayerIdentificationFee;
                case E_ClosingCostFeeMismoFeeT.VerificationOfTaxReturnFee:
                    return FeeBase.VerificationOfTaxReturnFee;
                case E_ClosingCostFeeMismoFeeT.WaterTestingFee:
                    return FeeBase.WaterTestingFee;
                case E_ClosingCostFeeMismoFeeT.WellInspectionFee:
                    return FeeBase.WellInspectionFee;
                //// OPM 227381 - Both MISMO 2.6 and 3.3 Fee Types
                case E_ClosingCostFeeMismoFeeT.VAFundingFee:
                    return FeeBase.VAFundingFee;
                case E_ClosingCostFeeMismoFeeT.MIInitialPremium:
                    return FeeBase.MIInitialPremium;
                case E_ClosingCostFeeMismoFeeT.ChosenInterestRateCreditOrChargeTotal:
                    return FeeBase.ChosenInterestRateCreditOrChargeTotal;
                case E_ClosingCostFeeMismoFeeT.OurOriginationChargeTotal:
                    return FeeBase.OurOriginationChargeTotal;
                case E_ClosingCostFeeMismoFeeT.TitleServicesFeeTotal:
                    return FeeBase.TitleServicesFeeTotal;
                case E_ClosingCostFeeMismoFeeT.RealEstateCommissionSellersBroker:
                    return FeeBase.RealEstateCommissionSellersBroker;
                case E_ClosingCostFeeMismoFeeT._203KArchitecturalAndEngineeringFee:
                case E_ClosingCostFeeMismoFeeT.AbstractOrTitleSearchFee:
                case E_ClosingCostFeeMismoFeeT.AmortizationFee:
                case E_ClosingCostFeeMismoFeeT.CLOAccessFee:
                case E_ClosingCostFeeMismoFeeT.GeneralCounselFee:
                case E_ClosingCostFeeMismoFeeT.NewLoanAdministrationFee:
                    otherDescription = GetXmlEnumName(type);
                    return FeeBase.Other;
                case E_ClosingCostFeeMismoFeeT.Other:
                case E_ClosingCostFeeMismoFeeT.Undefined:
                    otherDescription = feeDescription;
                    return FeeBase.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Retrieves the LegalEntityBase value corresponding to the given seller type.
        /// </summary>
        /// <param name="sellerType">The seller's legal entity type.</param>
        /// <returns>The corresponding LegalEntityBase value.</returns>
        public static LegalEntityBase GetLegalEntityBaseValue(E_SellerEntityType sellerType)
        {
            switch (sellerType)
            {
                case E_SellerEntityType.Corporation:
                    return LegalEntityBase.Corporation;
                case E_SellerEntityType.GeneralPartnership:
                    return LegalEntityBase.Partnership;
                case E_SellerEntityType.LimitedLiabilityCompany:
                    return LegalEntityBase.LimitedLiabilityCompany;
                case E_SellerEntityType.LimitedPartnership:
                    return LegalEntityBase.LimitedPartnership;
                case E_SellerEntityType.SoleProprietor:
                    return LegalEntityBase.SoleProprietorship;
                case E_SellerEntityType.Company:
                case E_SellerEntityType.LimitedLiabilityCorporation:
                    return LegalEntityBase.Other;
                default:
                    throw new UnhandledEnumException(sellerType);
            }
        }

        ////// mappings //////

        /// <summary>
        /// Creates a container with extension data for ROLE_DETAIL.
        /// </summary>
        /// <param name="includedInDisclosure">Indicates whether this contact is included on the LE/CD. Defaults to false.</param>
        /// <returns>A ROLE_DETAIL_EXTENSION container.</returns>
        private static ROLE_DETAIL_EXTENSION CreateRoleDetailExtension(bool includedInDisclosure)
        {
            var extension = new ROLE_DETAIL_EXTENSION();
            extension.Other = CreateLQBRoleDetailExtension(includedInDisclosure);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension element for ROLE_DETAIL.
        /// </summary>
        /// <param name="includedInDisclosure">Indicates whether this contact is included on the LE/CD. Defaults to false.</param>
        /// <returns>An LQB_ROLE_DETAIL_EXTENSION container.</returns>
        private static LQB_ROLE_DETAIL_EXTENSION CreateLQBRoleDetailExtension(bool includedInDisclosure)
        {
            var extension = new LQB_ROLE_DETAIL_EXTENSION();
            extension.IncludedInDisclosure = ToMismoIndicator(includedInDisclosure);

            return extension;
        }

        /// <summary>
        /// Creates a container holding information on a trustee.
        /// </summary>
        /// <param name="trustee">The trustee to be mapped to MISMO.</param>
        /// <returns>A TRUSTEE container.</returns>
        private static TRUSTEE CreateTrustee(TrustCollection.Trustee trustee)
        {
            var trusteeContainer = new TRUSTEE();
            trusteeContainer.DeedOfTrustTrusteeIndicator = ToMismoIndicator(false);

            return trusteeContainer;
        }

        /// <summary>
        /// Creates a container holding data on a borrower's social security number.
        /// </summary>
        /// <param name="identifier">The taxpayer identifier number.</param>
        /// <param name="type">The taxpayer identifier type.</param>
        /// <param name="sequenceNumber">The sequence number for this element.</param>
        /// <returns>A TAXPAYER_IDENTIFIER container.</returns>
        private static TAXPAYER_IDENTIFIER CreateTaxpayerIdentifier(string identifier, TaxpayerIdentifierBase type, int sequenceNumber)
        {
            var taxpayerIdentifier = new TAXPAYER_IDENTIFIER();
            taxpayerIdentifier.TaxpayerIdentifierValue = ToMismoNumericString(identifier, isSensitiveData: true);
            taxpayerIdentifier.TaxpayerIdentifierType = ToMismoEnum(type, isSensitiveData: true);
            taxpayerIdentifier.SequenceNumber = sequenceNumber;

            return taxpayerIdentifier;
        }

        /// <summary>
        /// Creates a LICENSE_DETAIL container with the given NMLS license.
        /// </summary>
        /// <param name="licenseNumber">The NMLS license.</param>
        /// <returns>A LICENSE_DETAIL container with the given NMLS license.</returns>
        private static LICENSE_DETAIL CreateLicenseDetail_NMLS(string licenseNumber)
        {
            if (string.IsNullOrEmpty(licenseNumber))
            {
                return null;
            }

            var detail = new LICENSE_DETAIL();
            detail.LicenseIdentifier = ToMismoIdentifier(licenseNumber, null, "mortgage.nationwidelicensingsystem.org", isSensitiveData: true);
            detail.LicenseIssuingAuthorityName = ToMismoString("NationwideMortgageLicensingSystemAndRegistry");

            return detail;
        }

        /// <summary>
        /// Creates a LICENSE_DETAIL container with the given VA/FHA license.
        /// </summary>
        /// <param name="licenseNumber">The VA/FHA license.</param>
        /// <param name="loanType">The loan type.</param>
        /// <returns>A LICENSE_DETAIL container with the given VA/FHA license.</returns>
        private static LICENSE_DETAIL CreateLicenseDetail_FHA_VA(string licenseNumber, E_sLT loanType)
        {
            if (string.IsNullOrEmpty(licenseNumber))
            {
                return null;
            }

            var detail = new LICENSE_DETAIL();
            detail.LicenseIdentifier = ToMismoIdentifier(licenseNumber, isSensitiveData: true);
            if (loanType == E_sLT.FHA)
            {
                detail.LicenseIssuingAuthorityName = ToMismoString("FHA");
            }
            else if (loanType == E_sLT.VA)
            {
                detail.LicenseIssuingAuthorityName = ToMismoString("VA");
            }

            return detail;
        }

        /// <summary>
        /// Creates a LICENSE_DETAIL container with the given state license.
        /// </summary>
        /// <param name="licenseNumber">The state license.</param>
        /// <param name="state">The U.S. state for which the license was issued.</param>
        /// <returns>A LICENSE_DETAIL container with the given state license.</returns>
        private static LICENSE_DETAIL CreateLicenseDetail_State(string licenseNumber, string state)
        {
            if (string.IsNullOrEmpty(licenseNumber))
            {
                return null;
            }

            var detail = new LICENSE_DETAIL();
            detail.LicenseIdentifier = ToMismoIdentifier(licenseNumber, isSensitiveData: true);
            detail.LicenseIssuingAuthorityStateCode = ToMismoCode(state);
            detail.LicenseIssuingAuthorityStateName = ToMismoString(StateInfo.Instance.GetStateName(state));

            return detail;
        }

        /// <summary>
        /// Creates a proprietary PARTY extension.
        /// </summary>
        /// <param name="agent">An agent record.</param>
        /// <param name="entityType">Indicates whether the agent's individual or legal entity data is being exported.</param>
        /// <returns>A proprietary PARTY extension.</returns>
        private static LQB_PARTY_EXTENSION CreateLqbPartyExtension(CAgentFields agent, EntityType entityType)
        {
            var extension = new LQB_PARTY_EXTENSION();

            if (agent.RecordId != Guid.Empty)
            {
                extension.AgentId = ToMismoIdentifier(agent.RecordId.ToString());
            }

            if (!string.IsNullOrEmpty(agent.CompanyId))
            {
                extension.CompanyIdentifier = ToMismoIdentifier(agent.CompanyId);
            }

            if (entityType == EntityType.Individual)
            {
                extension.EmployeeIdentifier = ToMismoIdentifier(agent.EmployeeIDInCompany);
            }

            return extension;
        }
    }
}