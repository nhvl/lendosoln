﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Migration;
    using LendingQB.Core.Data;
    using LqbGrammar.DataTypes;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Class for builder a party container for a (co)borrower.
    /// </summary>
    public class BorrowerPartyBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BorrowerPartyBuilder"/> class.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="exportData">A cache of data used throughout the exporter.</param>
        /// <param name="app">The application of the borrower to export.</param>
        /// <param name="borrowerType">The borrower type.</param>
        /// <param name="sequenceNumber">The sequence number for this PARTY container.</param>
        public BorrowerPartyBuilder(CPageData dataLoan, Mismo34ExporterCache exportData, CAppData app, E_BorrowerModeT borrowerType, int sequenceNumber)
        {
            this.DataLoan = dataLoan;
            this.ExportData = exportData;
            this.App = app;
            this.App.BorrowerModeT = borrowerType;
            this.SequenceNumber = sequenceNumber;
        }

        /// <summary>
        /// Gets the data loan.
        /// </summary>
        protected CPageData DataLoan { get; }

        /// <summary>
        /// Gets a collection of data used throughout the export.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Gets the app with the borrower to be exported.
        /// </summary>
        protected CAppData App { get; private set; }

        /// <summary>
        /// Gets the sequence number for this container.
        /// </summary>
        protected int SequenceNumber { get; }

        /// <summary>
        /// Creates the PARTY container.
        /// </summary>
        /// <returns>The PARTY container.</returns>
        public PARTY CreateContainer()
        {
            return this.CreateParty();
        }

        /// <summary>
        /// Retrieves the CounselingFormatBase value corresponding to the given counseling type.
        /// </summary>
        /// <param name="counselingType">The counseling format type.</param>
        /// <returns>The corresponding CounselingFormatBase value.</returns>
        private static CounselingBase GetCounselingTypeBaseValue(CounselingType? counselingType)
        {
            switch (counselingType)
            {
                case null:
                    return CounselingBase.Blank;
                case CounselingType.Counseling:
                    return CounselingBase.Counseling;
                case CounselingType.Education:
                    return CounselingBase.Education;
                default:
                    throw new UnhandledEnumException(counselingType);
            }
        }

        /// <summary>
        /// Retrieves the CounselingFormatBase value corresponding to the given counseling format type.
        /// </summary>
        /// <param name="counselingFormatType">The counseling format type.</param>
        /// <returns>The corresponding CounselingFormatBase value.</returns>
        private static CounselingFormatBase GetCounselingFormatTypeBaseValue(CounselingFormatType? counselingFormatType)
        {
            switch (counselingFormatType)
            {
                case null:
                    return CounselingFormatBase.Blank;
                case CounselingFormatType.FaceToFace:
                    return CounselingFormatBase.FaceToFace;
                case CounselingFormatType.Telephone:
                    return CounselingFormatBase.Telephone;
                case CounselingFormatType.Internet:
                    return CounselingFormatBase.Internet;
                default:
                    throw new UnhandledEnumException(counselingFormatType);
            }
        }

        /// <summary>
        /// Retrieves the IdentityDocument value corresponding to the given type.
        /// </summary>
        /// <param name="idType">The identification type.</param>
        /// <param name="idTypeOtherDesc">A description of the identification.</param>
        /// <returns>The corresponding IdentityDocument value.</returns>
        private static IdentityDocumentBase GetIdentityDocumentBaseValue(E_IdType idType, out string idTypeOtherDesc)
        {
            idTypeOtherDesc = string.Empty;

            switch (idType)
            {
                case E_IdType.DriversLicense:
                    return IdentityDocumentBase.DriversLicense;
                case E_IdType.GreenCard:
                    idTypeOtherDesc = "Green Card";
                    return IdentityDocumentBase.Other;
                case E_IdType.ImmigrationCard:
                    idTypeOtherDesc = "Immigration Card";
                    return IdentityDocumentBase.Other;
                case E_IdType.MilitaryId:
                    return IdentityDocumentBase.MilitaryIdentification;
                case E_IdType.OtherDocument:
                    return IdentityDocumentBase.Other;
                case E_IdType.Passport:
                    return IdentityDocumentBase.Passport;
                case E_IdType.StateId:
                    return IdentityDocumentBase.StateIdentification;
                case E_IdType.Visa:
                    return IdentityDocumentBase.Visa;
                case E_IdType.Blank:
                    return IdentityDocumentBase.Blank;
                default:
                    throw new UnhandledEnumException(idType);
            }
        }

        /// <summary>
        /// Retrieves the IdentityDocumentIssuedBy value corredponding to the identification type.
        /// </summary>
        /// <param name="idType">The type of identification.</param>
        /// <param name="issuedByOtherDesc">A description of the issuer.</param>
        /// <returns>The corresponding IdentityDocumentIssuedBy value.</returns>
        private static IdentityDocumentIssuedByBase GetIdentityDocumentIssuedByBaseValue(E_IdType idType, out string issuedByOtherDesc)
        {
            issuedByOtherDesc = string.Empty;

            switch (idType)
            {
                case E_IdType.DriversLicense:
                case E_IdType.StateId:
                    return IdentityDocumentIssuedByBase.StateProvince;
                case E_IdType.Passport:
                case E_IdType.MilitaryId:
                case E_IdType.ImmigrationCard:
                    return IdentityDocumentIssuedByBase.Country;
                case E_IdType.Visa:
                    issuedByOtherDesc = "Government Branch";
                    return IdentityDocumentIssuedByBase.Other;
                case E_IdType.GreenCard:
                case E_IdType.OtherDocument:
                case E_IdType.Blank:
                    return IdentityDocumentIssuedByBase.Blank;
                default:
                    throw new UnhandledEnumException(idType);
            }
        }

        /// <summary>
        /// Retrieves the party role type corresponding to the borrower type.
        /// </summary>
        /// <param name="borrowerType">The type of a borrower on an application.</param>
        /// <returns>The <see cref="PartyRoleBase" /> value corresponding to the borrower type.</returns>
        private static PartyRoleBase GetPartyRoleBaseValue(E_aTypeT borrowerType)
        {
            switch (borrowerType)
            {
                case E_aTypeT.CoSigner:
                    return PartyRoleBase.Cosigner;
                case E_aTypeT.Individual:
                    return PartyRoleBase.Borrower;
                case E_aTypeT.NonTitleSpouse:
                    return PartyRoleBase.NonTitleSpouse;
                case E_aTypeT.TitleOnly:
                    return PartyRoleBase.TitleHolder;
                default:
                    throw new UnhandledEnumException(borrowerType);
            }
        }

        /// <summary>
        /// Retrieves the JointAssetLiabilityReporting value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding JointAssetLiabilityReporting value.</returns>
        private static JointAssetLiabilityReportingBase GetJointAssetLiabilityReportingBaseValue(bool lqbValue)
        {
            return lqbValue ? JointAssetLiabilityReportingBase.NotJointly : JointAssetLiabilityReportingBase.Jointly;
        }

        /// <summary>
        /// Retrieves the MaritalStatus enumeration corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding MaritalStatus value.</returns>
        private static MaritalStatusBase GetMaritalStatusBaseValue(E_aBMaritalStatT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aBMaritalStatT.Married:
                    return MaritalStatusBase.Married;
                case E_aBMaritalStatT.NotMarried:
                    return MaritalStatusBase.Unmarried;
                case E_aBMaritalStatT.Separated:
                    return MaritalStatusBase.Separated;
                case E_aBMaritalStatT.LeaveBlank:
                    return MaritalStatusBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Gets the MISMO credit model value corresponding to the applicant's credit model.
        /// </summary>
        /// <param name="model">The applicant's credit model.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private static CreditScoreModelNameBase GetCreditScoreModelNameBaseValue(E_CreditScoreModelT model)
        {
            switch (model)
            {
                case E_CreditScoreModelT.Beacon09MortgageIndustryOption:
                    return CreditScoreModelNameBase.Beacon09MortgageIndustryOption;
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02781:
                    return CreditScoreModelNameBase.EquifaxBankruptcyNavigatorIndex02781;
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02782:
                    return CreditScoreModelNameBase.EquifaxBankruptcyNavigatorIndex02782;
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02783:
                    return CreditScoreModelNameBase.EquifaxBankruptcyNavigatorIndex02783;
                case E_CreditScoreModelT.EquifaxBankruptcyNavigatorIndex02784:
                    return CreditScoreModelNameBase.EquifaxBankruptcyNavigatorIndex02784;
                case E_CreditScoreModelT.EquifaxBeacon:
                    return CreditScoreModelNameBase.EquifaxBeacon;
                case E_CreditScoreModelT.EquifaxBeacon5:
                    return CreditScoreModelNameBase.EquifaxBeacon50;
                case E_CreditScoreModelT.EquifaxBeacon5Auto:
                    return CreditScoreModelNameBase.EquifaxBeacon50Auto;
                case E_CreditScoreModelT.EquifaxBeacon5BankCard:
                    return CreditScoreModelNameBase.EquifaxBeacon50BankCard;
                case E_CreditScoreModelT.EquifaxBeacon5Installment:
                    return CreditScoreModelNameBase.EquifaxBeacon50Installment;
                case E_CreditScoreModelT.EquifaxBeacon5PersonalFinance:
                    return CreditScoreModelNameBase.EquifaxBeacon50PersonalFinance;
                case E_CreditScoreModelT.EquifaxBeaconAuto:
                    return CreditScoreModelNameBase.EquifaxBeaconAuto;
                case E_CreditScoreModelT.EquifaxBeaconBankcard:
                    return CreditScoreModelNameBase.EquifaxBeaconBankcard;
                case E_CreditScoreModelT.EquifaxBeaconInstallment:
                    return CreditScoreModelNameBase.EquifaxBeaconInstallment;
                case E_CreditScoreModelT.EquifaxBeaconPersonalFinance:
                    return CreditScoreModelNameBase.EquifaxBeaconPersonalFinance;
                case E_CreditScoreModelT.EquifaxDAS:
                    return CreditScoreModelNameBase.EquifaxDAS;
                case E_CreditScoreModelT.EquifaxEnhancedBeacon:
                    return CreditScoreModelNameBase.EquifaxEnhancedBeacon;
                case E_CreditScoreModelT.EquifaxEnhancedDAS:
                    return CreditScoreModelNameBase.EquifaxEnhancedDAS;
                case E_CreditScoreModelT.EquifaxMarketMax:
                    return CreditScoreModelNameBase.EquifaxMarketMax;
                case E_CreditScoreModelT.EquifaxMortgageScore:
                    return CreditScoreModelNameBase.EquifaxMortgageScore;
                case E_CreditScoreModelT.EquifaxPinnacle:
                    return CreditScoreModelNameBase.EquifaxPinnacle;
                case E_CreditScoreModelT.EquifaxPinnacle2:
                    return CreditScoreModelNameBase.EquifaxPinnacle20;
                case E_CreditScoreModelT.EquifaxVantageScore:
                    return CreditScoreModelNameBase.EquifaxVantageScore;
                case E_CreditScoreModelT.EquifaxVantageScore3:
                    return CreditScoreModelNameBase.EquifaxVantageScore30;
                case E_CreditScoreModelT.ExperianFairIsaac:
                    return CreditScoreModelNameBase.ExperianFairIsaac;
                case E_CreditScoreModelT.ExperianFairIsaacAdvanced:
                    return CreditScoreModelNameBase.ExperianFairIsaacAdvanced;
                case E_CreditScoreModelT.ExperianFairIsaacAdvanced2:
                    return CreditScoreModelNameBase.ExperianFairIsaacAdvanced20;
                case E_CreditScoreModelT.ExperianFairIsaacAuto:
                    return CreditScoreModelNameBase.ExperianFairIsaacAuto;
                case E_CreditScoreModelT.ExperianFairIsaacBankcard:
                    return CreditScoreModelNameBase.ExperianFairIsaacBankcard;
                case E_CreditScoreModelT.ExperianFairIsaacInstallment:
                    return CreditScoreModelNameBase.ExperianFairIsaacInstallment;
                case E_CreditScoreModelT.ExperianFairIsaacPersonalFinance:
                    return CreditScoreModelNameBase.ExperianFairIsaacPersonalFinance;
                case E_CreditScoreModelT.ExperianFICOClassicV3:
                    return CreditScoreModelNameBase.ExperianFICOClassicV3;
                case E_CreditScoreModelT.ExperianMDSBankruptcyII:
                    return CreditScoreModelNameBase.ExperianMDSBankruptcyII;
                case E_CreditScoreModelT.ExperianNewNationalEquivalency:
                    return CreditScoreModelNameBase.ExperianNewNationalEquivalency;
                case E_CreditScoreModelT.ExperianNewNationalRisk:
                    return CreditScoreModelNameBase.ExperianNewNationalRisk;
                case E_CreditScoreModelT.ExperianOldNationalRisk:
                    return CreditScoreModelNameBase.ExperianOldNationalRisk;
                case E_CreditScoreModelT.ExperianScorexPLUS:
                    return CreditScoreModelNameBase.ExperianScorexPLUS;
                case E_CreditScoreModelT.ExperianVantageScore:
                    return CreditScoreModelNameBase.ExperianVantageScore;
                case E_CreditScoreModelT.ExperianVantageScore3:
                    return CreditScoreModelNameBase.ExperianVantageScore30;
                case E_CreditScoreModelT.FICOExpansionScore:
                    return CreditScoreModelNameBase.FICOExpansionScore;
                case E_CreditScoreModelT.FICORiskScoreClassic04:
                    return CreditScoreModelNameBase.FICORiskScoreClassic04;
                case E_CreditScoreModelT.FICORiskScoreClassic98:
                    return CreditScoreModelNameBase.FICORiskScoreClassic98;
                case E_CreditScoreModelT.FICORiskScoreClassicAuto98:
                    return CreditScoreModelNameBase.FICORiskScoreClassicAuto98;
                case E_CreditScoreModelT.FICORiskScoreClassicBankcard98:
                    return CreditScoreModelNameBase.FICORiskScoreClassicBankcard98;
                case E_CreditScoreModelT.FICORiskScoreClassicInstallmentLoan98:
                    return CreditScoreModelNameBase.FICORiskScoreClassicInstallmentLoan98;
                case E_CreditScoreModelT.FICORiskScoreClassicPersonalFinance98:
                    return CreditScoreModelNameBase.FICORiskScoreClassicPersonalFinance98;
                case E_CreditScoreModelT.FICORiskScoreNextGen00:
                    return CreditScoreModelNameBase.FICORiskScoreNextGen00;
                case E_CreditScoreModelT.FICORiskScoreNextGen03:
                    return CreditScoreModelNameBase.FICORiskScoreNextGen03;
                case E_CreditScoreModelT.TransUnionDelphi:
                    return CreditScoreModelNameBase.TransUnionDelphi;
                case E_CreditScoreModelT.TransUnionEmpirica:
                    return CreditScoreModelNameBase.TransUnionEmpirica;
                case E_CreditScoreModelT.TransUnionEmpiricaAuto:
                    return CreditScoreModelNameBase.TransUnionEmpiricaAuto;
                case E_CreditScoreModelT.TransUnionEmpiricaBankcard:
                    return CreditScoreModelNameBase.TransUnionEmpiricaBankcard;
                case E_CreditScoreModelT.TransUnionEmpiricaInstallment:
                    return CreditScoreModelNameBase.TransUnionEmpiricaInstallment;
                case E_CreditScoreModelT.TransUnionEmpiricaPersonalFinance:
                    return CreditScoreModelNameBase.TransUnionEmpiricaPersonalFinance;
                case E_CreditScoreModelT.TransUnionNewDelphi:
                    return CreditScoreModelNameBase.TransUnionNewDelphi;
                case E_CreditScoreModelT.TransUnionPrecision:
                    return CreditScoreModelNameBase.TransUnionPrecision;
                case E_CreditScoreModelT.TransUnionPrecision03:
                    return CreditScoreModelNameBase.TransUnionPrecision03;
                case E_CreditScoreModelT.TransUnionVantageScore:
                    return CreditScoreModelNameBase.TransUnionVantageScore;
                case E_CreditScoreModelT.TransUnionVantageScore30:
                    return CreditScoreModelNameBase.TransUnionVantageScore30;
                case E_CreditScoreModelT.VantageScore2:
                case E_CreditScoreModelT.VantageScore3:
                case E_CreditScoreModelT.MoreThanOneCreditScoringModel:
                case E_CreditScoreModelT.Other:
                    return CreditScoreModelNameBase.Other;
                case E_CreditScoreModelT.LeaveBlank:
                    return CreditScoreModelNameBase.Blank;
                default:
                    throw new UnhandledEnumException(model);
            }
        }

        /// <summary>
        /// Retrieves the CitizenshipResidency value corresponding to two LQB values.
        /// </summary>
        /// <param name="isResident">Indicates whether the borrower is a permanent resident alien.</param>
        /// <param name="isCitizen">Indicates whether the borrower is a citizen.</param>
        /// <returns>The corresponding CitizenshipResidency value.</returns>
        private static CitizenshipResidencyBase GetCitizenshipResidencyBaseValue(string isResident, string isCitizen)
        {
            if (string.IsNullOrWhiteSpace(isResident) && string.IsNullOrWhiteSpace(isCitizen))
            {
                return CitizenshipResidencyBase.Blank;
            }
            else if (string.Equals(isCitizen, "Y"))
            {
                return CitizenshipResidencyBase.USCitizen;
            }
            else if (string.Equals(isResident, "Y"))
            {
                return CitizenshipResidencyBase.PermanentResidentAlien;
            }
            else
            {
                return CitizenshipResidencyBase.NonResidentAlien;
            }
        }

        /// <summary>
        /// Retrieves the HomeownerPastThreeYears value corresponding to the given LQB string value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding HomeownerPastThreeYears value.</returns>
        private static HomeownerPastThreeYearsBase GetHomeownerPastThreeYearsBaseValue(string lqbValue)
        {
            if (string.Equals(lqbValue, "Y", StringComparison.OrdinalIgnoreCase))
            {
                return HomeownerPastThreeYearsBase.Yes;
            }
            else if (string.Equals(lqbValue, "N", StringComparison.OrdinalIgnoreCase))
            {
                return HomeownerPastThreeYearsBase.No;
            }
            else
            {
                return HomeownerPastThreeYearsBase.Unknown;
            }
        }

        /// <summary>
        /// Retrieves the HomeownerPastThreeYears value corresponding to the given LQB tri-state value.
        /// </summary>
        /// <param name="lqbValue">The LQB tri-state value to be converted.</param>
        /// <returns>The corresponding HomeownerPastThreeYears value.</returns>
        private static HomeownerPastThreeYearsBase GetHomeownerPastThreeYearsBaseValue(E_TriState lqbValue)
        {
            switch (lqbValue)
            {
                case E_TriState.Blank:
                    return HomeownerPastThreeYearsBase.Unknown;
                case E_TriState.Yes:
                    return HomeownerPastThreeYearsBase.Yes;
                case E_TriState.No:
                    return HomeownerPastThreeYearsBase.No;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the IntentToOccupy value corresponding to a given LQB string value.
        /// </summary>
        /// <param name="lqbValue">The LQB string value to be converted.</param>
        /// <returns>The corresponding IntentToOccupy value.</returns>
        private static IntentToOccupyBase GetIntentToOccupyBaseValue(string lqbValue)
        {
            if (string.Equals(lqbValue, "Y", StringComparison.OrdinalIgnoreCase))
            {
                return IntentToOccupyBase.Yes;
            }
            else if (string.Equals(lqbValue, "N", StringComparison.OrdinalIgnoreCase))
            {
                return IntentToOccupyBase.No;
            }
            else
            {
                return IntentToOccupyBase.Unknown;
            }
        }

        /// <summary>
        /// Retrieves the IntentToOccupy value corresponding to a given LQB tri-state value.
        /// </summary>
        /// <param name="lqbValue">The LQB tri-state value to be converted.</param>
        /// <returns>The corresponding IntentToOccupy value.</returns>
        private static IntentToOccupyBase GetIntentToOccupyBaseValue(E_TriState lqbValue)
        {
            switch (lqbValue)
            {
                case E_TriState.Blank:
                    return IntentToOccupyBase.Unknown;
                case E_TriState.Yes:
                    return IntentToOccupyBase.Yes;
                case E_TriState.No:
                    return IntentToOccupyBase.No;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the PriorPropertyTitle value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding PriorPropertyTitle value.</returns>
        private static PriorPropertyTitleBase GetPriorPropertyTitleBaseValue(E_aBDecPastOwnedPropTitleT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aBDecPastOwnedPropTitleT.O:
                    return PriorPropertyTitleBase.JointWithOtherThanSpouse;
                case E_aBDecPastOwnedPropTitleT.SP:
                    return PriorPropertyTitleBase.JointWithSpouse;
                case E_aBDecPastOwnedPropTitleT.S:
                    return PriorPropertyTitleBase.Sole;
                case E_aBDecPastOwnedPropTitleT.Empty:
                    return PriorPropertyTitleBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the PriorPropertyUsage value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding PriorPropertyUsage value.</returns>
        private static PriorPropertyUsageBase GetPriorPropertyUsageBaseValue(E_aBDecPastOwnedPropT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aBDecPastOwnedPropT.IP:
                    return PriorPropertyUsageBase.Investment;
                case E_aBDecPastOwnedPropT.PR:
                    return PriorPropertyUsageBase.PrimaryResidence;
                case E_aBDecPastOwnedPropT.SH:
                case E_aBDecPastOwnedPropT.SR:
                    return PriorPropertyUsageBase.SecondHome;
                case E_aBDecPastOwnedPropT.Empty:
                    return PriorPropertyUsageBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the EmploymentStatus value corresponding to a given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding EmploymentStatus value.</returns>
        private static EmploymentStatusBase GetEmploymentStatusBaseValue(bool lqbValue)
        {
            if (lqbValue)
            {
                return EmploymentStatusBase.Current;
            }
            else
            {
                return EmploymentStatusBase.Previous;
            }
        }

        /// <summary>
        /// Retrieves the EmploymentClassification value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding EmploymentClassification value.</returns>
        private static EmploymentClassificationBase GetEmploymentClassificationBaseValue(bool lqbValue)
        {
            if (lqbValue)
            {
                return EmploymentClassificationBase.Primary;
            }
            else
            {
                return EmploymentClassificationBase.Secondary;
            }
        }

        /// <summary>
        /// Maps LQB values to a base value for a VA "Borrower Certification Sales Price Exceeds Appraised Value" enumerated type.
        /// </summary>
        /// <param name="awareAtContractSigning">Indicates whether the borrower was aware of the property value at contract signing.</param>
        /// <param name="notAwareAtContractSigning">Indicates whether the borrower was not aware of the property value at contract signing.</param>
        /// <returns>An enumerated value based on the input.</returns>
        private static FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase GetFHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBaseValue(bool awareAtContractSigning, bool notAwareAtContractSigning)
        {
            if (awareAtContractSigning)
            {
                return FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.A;
            }
            else if (notAwareAtContractSigning)
            {
                return FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.B;
            }
            else
            {
                return FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.Blank;
            }
        }

        /// <summary>
        /// Retrieves the VABorrowerCertificationOccupancy value corresponding to the LQB values in the application.
        /// </summary>
        /// <param name="isHome">Indicates whether the property will be occupied as a home.</param>
        /// <param name="isHomeForSpouse">Indicates whether the property will be occupied by a spouse.</param>
        /// <param name="isPrevAsHome">Indicates whether the property was previously occupied as a home.</param>
        /// <param name="isPrevAsHomeForSpouse">Indicates whether the property was previously occupied by a spouse.</param>
        /// <returns>The corresponding VABorrowerCertificationOccupancy value.</returns>
        private static VABorrowerCertificationOccupancyBase GetVABorrowerCertificationOccupancyBaseValue(bool isHome, bool isHomeForSpouse, bool isPrevAsHome, bool isPrevAsHomeForSpouse)
        {
            if (isHome)
            {
                return VABorrowerCertificationOccupancyBase.A;
            }
            else if (isHomeForSpouse)
            {
                return VABorrowerCertificationOccupancyBase.B;
            }
            else if (isPrevAsHome)
            {
                return VABorrowerCertificationOccupancyBase.C;
            }
            else if (isPrevAsHomeForSpouse)
            {
                return VABorrowerCertificationOccupancyBase.D;
            }
            else
            {
                return VABorrowerCertificationOccupancyBase.Blank;
            }
        }

        /// <summary>
        /// Retrieves the borrower residency basis value corresponding to the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for their current residence.</param>
        /// <returns>The corresponding borrower residency basis value.</returns>
        private static BorrowerResidencyBasisBase GetBorrowerResidencyBasisBaseValue(E_aBAddrT residencyType)
        {
            switch (residencyType)
            {
                case E_aBAddrT.Own:
                    return BorrowerResidencyBasisBase.Own;
                case E_aBAddrT.Rent:
                    return BorrowerResidencyBasisBase.Rent;
                case E_aBAddrT.LivingRentFree:
                    return BorrowerResidencyBasisBase.LivingRentFree;
                case E_aBAddrT.LeaveBlank:
                    return BorrowerResidencyBasisBase.Unknown;
                default:
                    throw new UnhandledEnumException(residencyType);
            }
        }

        /// <summary>
        /// Retrieves the borrower residency basis value corresponding to the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for their past residence.</param>
        /// <returns>The corresponding borrower residency basis value.</returns>
        private static BorrowerResidencyBasisBase GetBorrowerResidencyBasisBaseValue(E_aBPrev1AddrT residencyType)
        {
            switch (residencyType)
            {
                case E_aBPrev1AddrT.Own:
                    return BorrowerResidencyBasisBase.Own;
                case E_aBPrev1AddrT.Rent:
                    return BorrowerResidencyBasisBase.Rent;
                case E_aBPrev1AddrT.LivingRentFree:
                    return BorrowerResidencyBasisBase.LivingRentFree;
                case E_aBPrev1AddrT.LeaveBlank:
                    return BorrowerResidencyBasisBase.Unknown;
                default:
                    throw new UnhandledEnumException(residencyType);
            }
        }

        /// <summary>
        /// Retrieves the borrower residency basis value corresponding to the given residency type.
        /// </summary>
        /// <param name="residencyType">The borrower's residency type for their past residence.</param>
        /// <returns>The corresponding borrower residency basis value.</returns>
        private static BorrowerResidencyBasisBase GetBorrowerResidencyBasisBaseValue(E_aBPrev2AddrT residencyType)
        {
            switch (residencyType)
            {
                case E_aBPrev2AddrT.Own:
                    return BorrowerResidencyBasisBase.Own;
                case E_aBPrev2AddrT.Rent:
                    return BorrowerResidencyBasisBase.Rent;
                case E_aBPrev2AddrT.LivingRentFree:
                    return BorrowerResidencyBasisBase.LivingRentFree;
                case E_aBPrev2AddrT.LeaveBlank:
                    return BorrowerResidencyBasisBase.Unknown;
                default:
                    throw new UnhandledEnumException(residencyType);
            }
        }

        /// <summary>
        /// Retrieves the <see cref="MilitaryBranchBase"/> value corresponding to the given identifier.
        /// </summary>
        /// <param name="militaryBranch">Identifies the military branch.</param>
        /// <returns>The corresponding <see cref="MilitaryBranchBase"/> value.</returns>
        private static MilitaryBranchBase GetMilitaryBranchBaseValue(string militaryBranch)
        {
            if (string.Equals(militaryBranch, "AirForce", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.AirForce;
            }
            else if (string.Equals(militaryBranch, "Army", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.Army;
            }
            else if (string.Equals(militaryBranch, "Marines", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.Marines;
            }
            else if (string.Equals(militaryBranch, "Navy", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.Navy;
            }
            else if (string.Equals(militaryBranch, "AirNationalGuard", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.AirNationalGuard;
            }
            else if (string.Equals(militaryBranch, "ArmyNationalGuard", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.ArmyNationalGuard;
            }
            else if (string.Equals(militaryBranch, "ArmyReserves", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.ArmyReserves;
            }
            else if (string.Equals(militaryBranch, "CoastGuard", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.CoastGuard;
            }
            else if (string.Equals(militaryBranch, "MarinesReserves", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.MarinesReserves;
            }
            else if (string.Equals(militaryBranch, "NavyReserves", StringComparison.OrdinalIgnoreCase))
            {
                return MilitaryBranchBase.NavyReserves;
            }
            else
            {
                return MilitaryBranchBase.Blank;
            }
        }

        /// <summary>
        /// Retrieves the <see cref="MilitaryStatusBase"/> corresponding to the given application data.
        /// </summary>
        /// <param name="onActiveDuty">Indicates whether the borrower is currently on active duty.</param>
        /// <param name="startDate">The start date of the borrower's military service.</param>
        /// <param name="endDate">The end date of the borrower's military service.</param>
        /// <param name="isActiveServiceLine">Indicates whether the data is from an active service or military reserve line.</param>
        /// <returns>The corresponding <see cref="MilitaryStatusBase"/> value.</returns>
        private static MilitaryStatusBase GetMilitaryStatusBaseValue(E_TriState onActiveDuty, string startDate, string endDate, bool isActiveServiceLine)
        {
            bool startDateIsValid = !string.IsNullOrWhiteSpace(startDate);
            bool endDateIsValid = !string.IsNullOrWhiteSpace(endDate);

            if (startDateIsValid && !endDateIsValid)
            {
                if (isActiveServiceLine)
                {
                    if (onActiveDuty == E_TriState.Yes)
                    {
                        return MilitaryStatusBase.ActiveDuty;
                    }
                }
                else
                {
                    return MilitaryStatusBase.ReserveNationalGuardNeverActivated;
                }
            }
            else if (startDateIsValid && endDateIsValid)
            {
                return MilitaryStatusBase.Separated;
            }

            return MilitaryStatusBase.Blank;
        }

        /// <summary>
        /// Maps the VA Service Branch type to a MilitaryBranchBase.
        /// </summary>
        /// <param name="serviceBranchType">The VA Service Branch type.</param>
        /// <returns>The matching MilitaryBranchBase.</returns>
        private static MilitaryBranchBase GetMilitaryBranchBase(E_aVaServiceBranchT serviceBranchType)
        {
            switch (serviceBranchType)
            {
                case E_aVaServiceBranchT.Army:
                    return MilitaryBranchBase.Army;
                case E_aVaServiceBranchT.Navy:
                    return MilitaryBranchBase.Navy;
                case E_aVaServiceBranchT.AirForce:
                    return MilitaryBranchBase.AirForce;
                case E_aVaServiceBranchT.Marine:
                    return MilitaryBranchBase.Marines;
                case E_aVaServiceBranchT.CoastGuard:
                    return MilitaryBranchBase.CoastGuard;
                case E_aVaServiceBranchT.Other:
                    return MilitaryBranchBase.Other;
                case E_aVaServiceBranchT.LeaveBlank:
                    return MilitaryBranchBase.Blank;
                default:
                    throw new UnhandledEnumException(serviceBranchType);
            }
        }

        /// <summary>
        /// Maps the VA Military Status type to MilitaryStatusBase.
        /// </summary>
        /// <param name="militaryStatusType">The VA Military Status type.</param>
        /// <returns>The MilitaryStatusBase.</returns>
        private static MilitaryStatusBase GetMilitaryStatusBase(E_aVaMilitaryStatT militaryStatusType)
        {
            switch (militaryStatusType)
            {
                case E_aVaMilitaryStatT.SeparatedFromService:
                    return MilitaryStatusBase.Separated;
                case E_aVaMilitaryStatT.InService:
                    return MilitaryStatusBase.ActiveDuty;
                case E_aVaMilitaryStatT.LeaveBlank:
                    return MilitaryStatusBase.Blank;
                default:
                    throw new UnhandledEnumException(militaryStatusType);
            }
        }

        /// <summary>
        /// Retrieves the relationship vesting value corresponding to the given title holding manner.
        /// </summary>
        /// <param name="manner">The manner in which the title will be held.</param>
        /// <returns>The corresponding relationship vesting value.</returns>
        private static RelationshipVestingBase GetRelationshipVestingBaseValue(string manner)
        {
            if (string.IsNullOrEmpty(manner))
            {
                return RelationshipVestingBase.Unassigned;
            }
            else if (string.Equals(manner, "As Community Property", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.CommunityProperty;
            }
            else if (string.Equals(manner, "All as Joint Tenants", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "As Joint Tenants", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Wife as Joint Tenants", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Husband", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Wife", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Spouse and Spouse", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Wife and Husband", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Wife and Wife", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.JointTenants;
            }
            else if (string.Equals(manner, "All as Tenants in Common", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "As Tenants in Common", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Wife as Tenants in Common", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.TenantsInCommon;
            }
            else if (string.Equals(manner, "As Tenants by the Entirety", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.TenantsByTheEntirety;
            }
            else if (string.Equals(manner, "As Joint Tenants with Right of Survivorship", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Husband and Wife as Joint Tenants with Right of Survivorship", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.JointTenantsWithRightOfSurvivorship;
            }
            else if (string.Equals(manner, "Single Man", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Single Woman", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Unmarried Man", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Unmarried Woman", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Married Man", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(manner, "Married Woman", StringComparison.OrdinalIgnoreCase))
            {
                return RelationshipVestingBase.Individual;
            }
            else
            {
                return RelationshipVestingBase.Other;
            }
        }

        /// <summary>
        /// Retrieves the Gender enumeration corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding Gender value.</returns>
        private static GenderBase GetGenderBaseValue(E_GenderT lqbValue)
        {
            switch (lqbValue)
            {
                case E_GenderT.Female:
                    return GenderBase.Female;
                case E_GenderT.Male:
                    return GenderBase.Male;
                case E_GenderT.Unfurnished:
                    return GenderBase.InformationNotProvidedUnknown;
                case E_GenderT.NA:
                    return GenderBase.NotApplicable;
                case E_GenderT.LeaveBlank:
                    return GenderBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the HMDAEthnicity enumeration corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding HMDAEthnicity value.</returns>
        private static HMDAEthnicityBase GetHmdaEthnicityBaseValue(E_aHispanicT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aHispanicT.Hispanic:
                    return HMDAEthnicityBase.HispanicOrLatino;
                case E_aHispanicT.NotHispanic:
                    return HMDAEthnicityBase.NotHispanicOrLatino;
                case E_aHispanicT.LeaveBlank:
                    return HMDAEthnicityBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Indicating whether the applicant refused to
        /// specify their gender.
        /// </summary>
        /// <param name="gender">The gender on the loan application.</param>
        /// <returns>Boolean value.</returns>
        private static bool GetHmdaGenderRefusalIndicator(E_GenderT gender)
        {
            return gender == E_GenderT.FemaleAndNotFurnished
                || gender == E_GenderT.MaleAndNotFurnished
                || gender == E_GenderT.MaleFemaleNotFurnished
                || gender == E_GenderT.Unfurnished;
        }

        /// <summary>
        /// Gets a list of ethnicity origins for the borrower.
        /// There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of ethnicity origins.</returns>
        private static List<HMDAEthnicityOriginBase> GetHmdaEthnicityOriginBaseValues(CAppData appData)
        {
            var origins = new List<HMDAEthnicityOriginBase>();

            if (appData.aIsCuban)
            {
                origins.Add(HMDAEthnicityOriginBase.Cuban);
            }

            if (appData.aIsMexican)
            {
                origins.Add(HMDAEthnicityOriginBase.Mexican);
            }

            if (appData.aIsOtherHispanicOrLatino)
            {
                origins.Add(HMDAEthnicityOriginBase.Other);
            }

            if (appData.aIsPuertoRican)
            {
                origins.Add(HMDAEthnicityOriginBase.PuertoRican);
            }

            return origins;
        }

        /// <summary>
        /// Gets a list of races associated with a borrower. There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower's app data.</param>
        /// <returns>A list of races.</returns>
        private static List<HMDARaceBase> GetHmdaRaceBaseValues(CAppData appData)
        {
            var races = new List<HMDARaceBase>();

            if (appData.aIsAmericanIndian)
            {
                races.Add(HMDARaceBase.AmericanIndianOrAlaskaNative);
            }

            if (appData.aIsAsian)
            {
                races.Add(HMDARaceBase.Asian);
            }

            if (appData.aIsBlack)
            {
                races.Add(HMDARaceBase.BlackOrAfricanAmerican);
            }

            if (appData.aIsPacificIslander)
            {
                races.Add(HMDARaceBase.NativeHawaiianOrOtherPacificIslander);
            }

            if (appData.aIsWhite)
            {
                races.Add(HMDARaceBase.White);
            }

            if (appData.aDoesNotWishToProvideRace)
            {
                races.Add(HMDARaceBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication);
            }

            return races;
        }

        /// <summary>
        /// Gets a list of ethnicities for the borrower. There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of ethnicities.</returns>
        private static List<HMDAEthnicityBase> GetHmdaEthnicityBaseValues(CAppData appData)
        {
            var ethnicities = new List<HMDAEthnicityBase>();

            if (appData.aHispanicT == E_aHispanicT.Hispanic || appData.aHispanicT == E_aHispanicT.BothHispanicAndNotHispanic)
            {
                ethnicities.Add(HMDAEthnicityBase.HispanicOrLatino);
            }

            if (appData.aDoesNotWishToProvideEthnicity
                && appData.aInterviewMethodT != E_aIntrvwrMethodT.LeaveBlank
                && appData.aInterviewMethodT != E_aIntrvwrMethodT.FaceToFace)
            {
                ethnicities.Add(HMDAEthnicityBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication);
            }

            if (appData.aHispanicT == E_aHispanicT.NotHispanic || appData.aHispanicT == E_aHispanicT.BothHispanicAndNotHispanic)
            {
                ethnicities.Add(HMDAEthnicityBase.NotHispanicOrLatino);
            }

            return ethnicities;
        }

        /// <summary>
        /// Gets a list of asian race designations associated with the borrower.
        /// There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of LQB asian race designations.</returns>
        private static List<LqbHMDARaceDesignationBase> GetHmdaRaceDesignationBaseValues_Asian(CAppData appData)
        {
            var designations = new List<LqbHMDARaceDesignationBase>();

            if (appData.aIsAsianIndian)
            {
                designations.Add(LqbHMDARaceDesignationBase.AsianIndian);
            }

            if (appData.aIsChinese)
            {
                designations.Add(LqbHMDARaceDesignationBase.Chinese);
            }

            if (appData.aIsFilipino)
            {
                designations.Add(LqbHMDARaceDesignationBase.Filipino);
            }

            if (appData.aIsJapanese)
            {
                designations.Add(LqbHMDARaceDesignationBase.Japanese);
            }

            if (appData.aIsKorean)
            {
                designations.Add(LqbHMDARaceDesignationBase.Korean);
            }

            if (appData.aIsVietnamese)
            {
                designations.Add(LqbHMDARaceDesignationBase.Vietnamese);
            }

            if (appData.aIsOtherAsian)
            {
                designations.Add(LqbHMDARaceDesignationBase.OtherAsian);
            }

            return designations;
        }

        /// <summary>
        /// Gets a list of pacific islander race designations for the borrower.
        /// There can be more than one.
        /// </summary>
        /// <param name="appData">The borrower data.</param>
        /// <returns>A list of pacific islander race designations.</returns>
        private static List<LqbHMDARaceDesignationBase> GetHmdaRaceDesignationBaseValues_PacificIslander(CAppData appData)
        {
            var designations = new List<LqbHMDARaceDesignationBase>();

            if (appData.aIsNativeHawaiian)
            {
                designations.Add(LqbHMDARaceDesignationBase.NativeHawaiian);
            }

            if (appData.aIsGuamanianOrChamorro)
            {
                designations.Add(LqbHMDARaceDesignationBase.GuamanianOrChamorro);
            }

            if (appData.aIsSamoan)
            {
                designations.Add(LqbHMDARaceDesignationBase.Samoan);
            }

            if (appData.aIsOtherPacificIslander)
            {
                designations.Add(LqbHMDARaceDesignationBase.OtherPacificIslander);
            }

            return designations;
        }

        /// <summary>
        /// Gets the vanilla HMDARaceDesignationBase from the expanded LQB version.
        /// </summary>
        /// <param name="lqbValue">The LQB version of that enum.</param>
        /// <returns>The vanilla version of that enum.</returns>
        private static HMDARaceDesignationBase GetHmdaRaceDesignationBase(LqbHMDARaceDesignationBase lqbValue)
        {
            switch (lqbValue)
            {
                case LqbHMDARaceDesignationBase.AsianIndian:
                    return HMDARaceDesignationBase.AsianIndian;
                case LqbHMDARaceDesignationBase.Chinese:
                    return HMDARaceDesignationBase.Chinese;
                case LqbHMDARaceDesignationBase.Filipino:
                    return HMDARaceDesignationBase.Filipino;
                case LqbHMDARaceDesignationBase.GuamanianOrChamorro:
                    return HMDARaceDesignationBase.GuamanianOrChamorro;
                case LqbHMDARaceDesignationBase.Japanese:
                    return HMDARaceDesignationBase.Japanese;
                case LqbHMDARaceDesignationBase.Korean:
                    return HMDARaceDesignationBase.Korean;
                case LqbHMDARaceDesignationBase.NativeHawaiian:
                    return HMDARaceDesignationBase.NativeHawaiian;
                case LqbHMDARaceDesignationBase.OtherAsian:
                case LqbHMDARaceDesignationBase.OtherPacificIslander:
                    return HMDARaceDesignationBase.Other;
                case LqbHMDARaceDesignationBase.Samoan:
                    return HMDARaceDesignationBase.Samoan;
                case LqbHMDARaceDesignationBase.Vietnamese:
                    return HMDARaceDesignationBase.Vietnamese;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Determines the HMDA gender value that corresponds to the gender value on the loan.
        /// </summary>
        /// <param name="gender">The gender on the loan application.</param>
        /// <param name="interviewMethod">The application interview method.</param>
        /// <returns>The corresponding HMDA gender value.</returns>
        private static HMDAGenderBase GetHmdaGenderBaseValue(E_GenderT gender, E_aIntrvwrMethodT interviewMethod)
        {
            if (gender == E_GenderT.MaleAndFemale || gender == E_GenderT.MaleFemaleNotFurnished)
            {
                return HMDAGenderBase.ApplicantHasSelectedBothMaleAndFemale;
            }
            else if (gender == E_GenderT.Female || gender == E_GenderT.FemaleAndNotFurnished)
            {
                return HMDAGenderBase.Female;
            }
            else if (gender == E_GenderT.Male || gender == E_GenderT.MaleAndNotFurnished)
            {
                return HMDAGenderBase.Male;
            }
            else if (gender == E_GenderT.NA)
            {
                return HMDAGenderBase.NotApplicable;
            }
            else if ((gender == E_GenderT.LeaveBlank || gender == E_GenderT.Unfurnished)
                && interviewMethod != E_aIntrvwrMethodT.LeaveBlank && interviewMethod != E_aIntrvwrMethodT.FaceToFace)
            {
                return HMDAGenderBase.InformationNotProvidedUnknown;
            }
            else
            {
                return HMDAGenderBase.LeaveBlank;
            }
        }

        /// <summary>
        /// Creates a PARTY container holding information on a borrower or co-borrower.
        /// </summary>
        /// <returns>A PARTY container populated with borrower data.</returns>
        private PARTY CreateParty()
        {
            var party = new PARTY();
            party.Individual = this.CreateIndividual();
            party.Languages = this.CreateLanguages();
            party.Roles = this.CreateRoles();
            party.TaxpayerIdentifiers = CreateTaxpayerIdentifiers(this.App.aSsn, TaxpayerIdentifierBase.SocialSecurityNumber);
            party.SequenceNumber = this.SequenceNumber;

            party.Extension = this.CreatePartyExtension();

            return party;
        }

        /// <summary>
        /// Creates a LANGUAGES container.
        /// </summary>
        /// <returns>A LANGUAGES container.</returns>
        private LANGUAGES CreateLanguages()
        {
            var languages = new LANGUAGES();
            languages.LanguageList.Add(this.CreateLanguage(GetNextSequenceNumber(languages.LanguageList)));

            return languages;
        }

        /// <summary>
        /// Creates a LANGUAGE container.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A LANGUAGE container.</returns>
        private LANGUAGE CreateLanguage(int sequenceNumber)
        {
            var language = new LANGUAGE();
            language.SequenceNumber = sequenceNumber;

            language.LanguageCode = ToMismoCode(this.GetLanguageCode(this.App.aLanguagePrefType));
            language.Extension = this.CreateLanguageExtension();

            return language;
        }

        /// <summary>
        /// Gets the language ISO 639-2 code corresponding to a language.
        /// </summary>
        /// <param name="language">A language.</param>
        /// <returns>The corresponding language code.</returns>
        private string GetLanguageCode(LanguagePrefType language)
        {
            switch (language)
            {
                case LanguagePrefType.Blank:
                    return string.Empty;
                case LanguagePrefType.Chinese:
                    return "CHI";
                case LanguagePrefType.English:
                    return "ENG";
                case LanguagePrefType.Korean:
                    return "KOR";
                case LanguagePrefType.Spanish:
                    return "SPA";
                case LanguagePrefType.Tagalog:
                    return "TGL";
                case LanguagePrefType.Vietnamese:
                    return "VIE";
                case LanguagePrefType.Other:
                    return "ZXX";
                default:
                    throw new UnhandledEnumException(language);
            }
        }

        /// <summary>
        /// Creates a LANGUAGE_EXTENSION container.
        /// </summary>
        /// <returns>A LANGUAGE_EXTENSION container.</returns>
        private LANGUAGE_EXTENSION CreateLanguageExtension()
        {
            var extension = new LANGUAGE_EXTENSION();
            extension.Other = this.CreateUladLanguageExtension();

            return extension;
        }

        /// <summary>
        /// Creates a ULAD_LANGUAGE_EXTENSION container.
        /// </summary>
        /// <returns>A ULAD_LANGUAGE_EXTENSION container.</returns>
        private ULAD_LANGUAGE_EXTENSION CreateUladLanguageExtension()
        {
            var extension = new ULAD_LANGUAGE_EXTENSION();

            if (this.App.aLanguagePrefType == LanguagePrefType.Other)
            {
                extension.LanguageCodeOtherDescription = ToMismoString(this.App.aLanguagePrefTypeOtherDesc);
            }
            
            extension.LanguageRefusalIndicator = ToMismoIndicator(this.App.aLanguageRefusal);

            return extension;
        }

        /// <summary>
        /// Creates a container representing an individual borrower.
        /// </summary>
        /// <returns>An INDIVIDUAL container representing a borrower.</returns>
        private INDIVIDUAL CreateIndividual()
        {
            var individual = new INDIVIDUAL();

            if (this.App.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                individual.Aliases = this.CreateAliases(this.App.aBAliases.ToArray());
            }
            else
            {
                individual.Aliases = this.CreateAliases(this.App.aCAliases.ToArray());
            }

            individual.Name = CreateName(this.App.aFirstNm, this.App.aMidNm, this.App.aLastNm, this.App.aSuffix, this.App.aNm);
            individual.ContactPoints = CreateContactPoints(this.App);
            individual.IdentificationVerification = this.CreateIdentificationVerification();

            return individual;
        }

        /// <summary>
        /// Creates a container with a set of aliases. ALIAS containers will be created to
        /// hold one alias each.
        /// </summary>
        /// <param name="aliasArray">The set of aliases.</param>
        /// <returns>An ALIASES container.</returns>
        private ALIASES CreateAliases(string[] aliasArray)
        {
            var aliases = new ALIASES();

            foreach (string alias in aliasArray)
            {
                aliases.AliasList.Add(CreateAlias(alias, GetNextSequenceNumber(aliases.AliasList)));
            }

            return aliases;
        }

        /// <summary>
        /// Creates an IDENTIFICATION_VERIFICATION container that holds a set of identification details.
        /// </summary>
        /// <returns>An IDENTIFICATION_VERIFICATION container.</returns>
        private IDENTIFICATION_VERIFICATION CreateIdentificationVerification()
        {
            var identificationVerification = new IDENTIFICATION_VERIFICATION();
            identificationVerification.IdentityDocumentations = this.CreateIdentityDocumentations();

            return identificationVerification;
        }

        /// <summary>
        /// Creates an IDENTITY_DOCUMENTATIONS container holding a set of details on identification documents.
        /// </summary>
        /// <returns>An IDENTITY_DOCUMENTATIONS container.</returns>
        private IDENTITY_DOCUMENTATIONS CreateIdentityDocumentations()
        {
            var identityDocumentations = new IDENTITY_DOCUMENTATIONS();

            if (this.App.aIdT != E_IdType.Blank)
            {
                identityDocumentations.IdentityDocumentationList.Add(
                    this.CreateIdentityDocumentation(
                        this.App.aIdT,
                        this.App.aIdNumber,
                        this.App.aIdIssueD_rep,
                        this.App.aIdExpireD_rep,
                        this.App.aIdState,
                        this.App.aIdCountry,
                        this.App.aIdGovBranch,
                        this.App.aIdOther));
            }

            if (this.App.a2ndIdT != E_IdType.Blank)
            {
                identityDocumentations.IdentityDocumentationList.Add(
                    this.CreateIdentityDocumentation(
                        this.App.a2ndIdT,
                        this.App.a2ndIdNumber,
                        this.App.a2ndIdIssueD_rep,
                        this.App.a2ndIdExpireD_rep,
                        this.App.a2ndIdState,
                        this.App.a2ndIdCountry,
                        this.App.a2ndIdGovBranch,
                        this.App.a2ndIdOther));
            }

            return identityDocumentations;
        }

        /// <summary>
        /// Creates an IDENTITY_DOCUMENTATION container holding information on a specific identification document.
        /// </summary>
        /// <param name="idType">The type of identification recorded.</param>
        /// <param name="idNumber">The identification number.</param>
        /// <param name="issueDate">The issue date.</param>
        /// <param name="expirationDate">The expiration date.</param>
        /// <param name="issuingState">The issuing state.</param>
        /// <param name="issuingCountry">The issuing country.</param>
        /// <param name="issuingGovernmentBranch">The issuing government branch.</param>
        /// <param name="otherInformation">Other information about the identification.</param>
        /// <returns>An IDENTITY_DOCUMENTATION container.</returns>
        private IDENTITY_DOCUMENTATION CreateIdentityDocumentation(
            E_IdType idType,
            string idNumber,
            string issueDate,
            string expirationDate,
            string issuingState,
            string issuingCountry,
            string issuingGovernmentBranch,
            string otherInformation)
        {
            var identityDocumentation = new IDENTITY_DOCUMENTATION();

            string idTypeOtherDesc = string.Empty;
            identityDocumentation.IdentityDocumentType = idType == E_IdType.Blank ? null : ToMismoEnum(GetIdentityDocumentBaseValue(idType, out idTypeOtherDesc), isSensitiveData: true);
            identityDocumentation.IdentityDocumentExpirationDate = ToMismoDate(expirationDate, true);

            if (identityDocumentation.IdentityDocumentType.EnumValue == IdentityDocumentBase.Other)
            {
                if (!string.IsNullOrWhiteSpace(idTypeOtherDesc))
                {
                    identityDocumentation.IdentityDocumentTypeOtherDescription = ToMismoString(idTypeOtherDesc, isSensitiveData: true);
                }
                else if (!string.IsNullOrWhiteSpace(otherInformation))
                {
                    identityDocumentation.IdentityDocumentTypeOtherDescription = ToMismoString(otherInformation, isSensitiveData: true);
                }
            }

            string issuedByOtherDesc = string.Empty;
            if (idType == E_IdType.Blank || idType == E_IdType.GreenCard || idType == E_IdType.OtherDocument)
            {
                identityDocumentation.IdentityDocumentIssuedByType = null;
            }
            else
            {
                identityDocumentation.IdentityDocumentIssuedByType = ToMismoEnum(GetIdentityDocumentIssuedByBaseValue(idType, out issuedByOtherDesc), isSensitiveData: true);
            }

            if (!string.IsNullOrWhiteSpace(issuedByOtherDesc))
            {
                identityDocumentation.IdentityDocumentIssuedByTypeOtherDescription = ToMismoString(issuedByOtherDesc, isSensitiveData: true);
            }

            if (idType == E_IdType.DriversLicense || idType == E_IdType.Passport || idType == E_IdType.StateId || idType == E_IdType.OtherDocument)
            {
                identityDocumentation.IdentityDocumentIssuedDate = ToMismoDate(issueDate, isSensitiveData: true);
            }

            if (idType != E_IdType.OtherDocument)
            {
                identityDocumentation.IdentityDocumentIdentifier = ToMismoIdentifier(idNumber, isSensitiveData: true);
            }

            if (idType == E_IdType.DriversLicense || idType == E_IdType.StateId)
            {
                identityDocumentation.IdentityDocumentIssuedByStateCode = ToMismoCode(issuingState, isSensitiveData: true);
            }

            if (idType == E_IdType.Passport || idType == E_IdType.MilitaryId || idType == E_IdType.ImmigrationCard)
            {
                identityDocumentation.IdentityDocumentIssuedByCountryName = ToMismoString(issuingCountry, isSensitiveData: true);
            }

            if (idType == E_IdType.Visa)
            {
                identityDocumentation.IdentityDocumentIssuedByName = ToMismoString(issuingGovernmentBranch, isSensitiveData: true);
            }

            return identityDocumentation;
        }

        /// <summary>
        /// Creates a ROLES container for a borrower.
        /// </summary>
        /// <returns>A ROLES container.</returns>
        private ROLES CreateRoles()
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole_Borrower(GetNextSequenceNumber(roles.RoleList)));

            if (this.App.aTypeT == E_aTypeT.Individual || this.App.aTypeT == E_aTypeT.TitleOnly)
            {
                roles.RoleList.Add(this.CreateRole_PropertyOwner(GetNextSequenceNumber(roles.RoleList)));
            }

            foreach (var trustee in this.DataLoan.sTrustCollection.Trustees)
            {
                if (string.Equals(this.App.aNm, trustee.Name, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(this.App.aFirstNm + " " + this.App.aLastNm, trustee.Name, StringComparison.OrdinalIgnoreCase))
                {
                    string label = (this.App.BorrowerModeT == E_BorrowerModeT.Borrower ? this.App.aBMismoId : this.App.aCMismoId) + "_Trustee";
                    roles.RoleList.Add(CreateRole_Trustee(this.ExportData, trustee, label, GetNextSequenceNumber(roles.RoleList)));
                }
            }

            foreach (var settlor in this.DataLoan.sTrustCollection.Trustors)
            {
                if (string.Equals(this.App.aNm, settlor.Name, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(this.App.aFirstNm + " " + this.App.aLastNm, settlor.Name, StringComparison.OrdinalIgnoreCase))
                {
                    string label = (this.App.BorrowerModeT == E_BorrowerModeT.Borrower ? this.App.aBMismoId : this.App.aCMismoId) + "_Settlor";
                    roles.RoleList.Add(CreateRole_Settlor(this.ExportData, label, GetNextSequenceNumber(roles.RoleList)));
                }
            }

            return roles;
        }

        /// <summary>
        /// Creates a property owner role.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number for the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_PropertyOwner(int sequenceNumber)
        {
            var role = new ROLE();
            PartyRoleBase roleBase = PartyRoleBase.PropertyOwner;

            role.RoleDetail = CreateRoleDetail(roleBase, string.Empty);
            role.PropertyOwner = this.CreatePropertyOwner();
            role.SequenceNumber = sequenceNumber;

            role.XlinkLabel = this.ExportData.GeneratePartyLabel(roleBase, EntityType.Individual);
            return role;
        }

        /// <summary>
        /// Creates a container representing a property owner.
        /// </summary>
        /// <returns>A PROPERTY_OWNER container.</returns>
        private PROPERTY_OWNER CreatePropertyOwner()
        {
            var propertyOwner = new PROPERTY_OWNER();
            propertyOwner.RelationshipVestingType = ToMismoEnum(GetRelationshipVestingBaseValue(this.App.aManner), isSensitiveData: false);

            if (propertyOwner.RelationshipVestingType != null && propertyOwner.RelationshipVestingType.EnumValue == RelationshipVestingBase.Other)
            {
                propertyOwner.RelationshipVestingTypeOtherDescription = ToMismoString(this.App.aManner);
            }

            return propertyOwner;
        }

        /// <summary>
        /// Creates a borrower role.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number for the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_Borrower(int sequenceNumber)
        {
            var role = new ROLE();

            role.RoleDetail = CreateRoleDetail(GetPartyRoleBaseValue(this.App.aTypeT), string.Empty);
            role.Borrower = this.CreateBorrower();
            role.XlinkLabel = this.App.aMismoId;
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a container representing a borrower.
        /// </summary>
        /// <returns>A BORROWER container.</returns>
        private BORROWER CreateBorrower()
        {
            var borrower = new BORROWER();

            if (this.DataLoan.sIsTargeting2019Ulad)
            {
                borrower.Bankruptcies = this.CreateBankruptcies();
            }

            borrower.BorrowerDetail = this.CreateBorrowerDetail();
            borrower.Counseling = this.CreateCounseling();
            borrower.CurrentIncome = this.CreateCurrentIncome();
            borrower.CreditScores = this.CreateCreditScores();
            borrower.Dependents = this.CreateDependents();
            borrower.Declaration = this.CreateDeclaration();
            borrower.Employers = this.CreateEmployers();
            borrower.GovernmentBorrower = this.CreateGovernmentBorrower();
            borrower.GovernmentMonitoring = this.CreateGovernmentMonitoring();
            borrower.Residences = this.CreateResidences();
            borrower.HousingExpenses = this.CreateHousingExpenses();

            if (this.App.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                borrower.Summaries = this.CreateSummaries();

                if (this.DataLoan.sLT == E_sLT.VA)
                {
                    borrower.MilitaryServices = this.CreateMilitaryServices();
                }
            }

            if (this.App.aMismoId == "B0" || this.App.aMismoId == "C0")
            {
                borrower.NearestLivingRelative = this.CreateNearestLivingRelative(this.App.aMismoId == "B0");
            }

            return borrower;
        }

        /// <summary>
        /// Creates a container holding data about bankruptcies.
        /// </summary>
        /// <returns>A <see cref="BANKRUPTCIES"/> container.</returns>
        private BANKRUPTCIES CreateBankruptcies()
        {
            var bankruptcies = new BANKRUPTCIES();

            Func<bool, bool, bool> indicatorGetter = (borrowerField, coborrowerField) => 
                this.App.BorrowerModeT == E_BorrowerModeT.Borrower ? borrowerField : coborrowerField;

            if (indicatorGetter(this.App.aBDecHasBankruptcyChapter7Ulad, this.App.aCDecHasBankruptcyChapter7Ulad))
            {
                bankruptcies.BankruptcyList.Add(this.CreateBankruptcy(BankruptcyChapterBase.ChapterSeven));
            }

            if (indicatorGetter(this.App.aBDecHasBankruptcyChapter11Ulad, this.App.aCDecHasBankruptcyChapter11Ulad))
            {
                bankruptcies.BankruptcyList.Add(this.CreateBankruptcy(BankruptcyChapterBase.ChapterEleven));
            }

            if (indicatorGetter(this.App.aBDecHasBankruptcyChapter12Ulad, this.App.aCDecHasBankruptcyChapter12Ulad))
            {
                bankruptcies.BankruptcyList.Add(this.CreateBankruptcy(BankruptcyChapterBase.ChapterTwelve));
            }

            if (indicatorGetter(this.App.aBDecHasBankruptcyChapter13Ulad, this.App.aCDecHasBankruptcyChapter13Ulad))
            {
                bankruptcies.BankruptcyList.Add(this.CreateBankruptcy(BankruptcyChapterBase.ChapterThirteen));
            }

            return bankruptcies;
        }

        /// <summary>
        /// Creates a container for a bankruptcy item.
        /// </summary>
        /// <param name="bankruptcyType">
        /// The type of bankruptcy.
        /// </param>
        /// <returns>A <see cref="BANKRUPTCY"/> container.</returns>
        private BANKRUPTCY CreateBankruptcy(BankruptcyChapterBase bankruptcyType)
        {
            var bankruptcy = new BANKRUPTCY();
            bankruptcy.BankruptcyDetail = this.CreateBankruptcyDetail(bankruptcyType);

            return bankruptcy;
        }

        /// <summary>
        /// Creates a container for a bankruptcy detail item.
        /// </summary>
        /// <param name="bankruptcyType">
        /// The type of bankruptcy.
        /// </param>
        /// <returns>A <see cref="BANKRUPTCY_DETAIL"/> container.</returns>
        private BANKRUPTCY_DETAIL CreateBankruptcyDetail(BankruptcyChapterBase bankruptcyType)
        {
            var bankruptcyDetail = new BANKRUPTCY_DETAIL();
            bankruptcyDetail.BankruptcyChapterType = ToMismoEnum(bankruptcyType);

            return bankruptcyDetail;
        }

        /// <summary>
        /// Creates a container holding data about a borrower.
        /// </summary>
        /// <returns>A BORROWER_DETAIL container.</returns>
        private BORROWER_DETAIL CreateBorrowerDetail()
        {
            var borrowerDetail = new BORROWER_DETAIL();
            borrowerDetail.BorrowerAgeAtApplicationYearsCount = ToMismoCount(this.App.aAge_rep, null);
            borrowerDetail.BorrowerApplicationSignedDate = ToMismoDate(this.DataLoan.sAppSubmittedD_rep);
            borrowerDetail.BorrowerBirthDate = ToMismoDate(this.App.aDob_rep);
            borrowerDetail.BorrowerClassificationType = ToMismoEnum(GetBorrowerClassificationBaseValue(this.App.BorrowerModeT), isSensitiveData: false);
            borrowerDetail.CreditReportIdentifier = ToMismoIdentifier(this.App.aCreditReportId, isSensitiveData: true);
            borrowerDetail.BorrowerMailToAddressSameAsPropertyIndicator = ToMismoIndicator(
                                                                                this.App.aAddrMailSourceT == E_aAddrMailSourceT.SubjectPropertyAddress ||
                                                                                (this.App.aAddrMailSourceT == E_aAddrMailSourceT.PresentAddress && this.App.aBorrowerAddressSameAsSubjectProperty));
            borrowerDetail.BorrowerRelationshipTitleType = this.App.aRelationshipTitleT == E_aRelationshipTitleT.LeaveBlank ? null : ToMismoEnum(GetBorrowerRelationshipTitleBaseValue(this.App.aRelationshipTitleT), isSensitiveData: false);

            if (borrowerDetail.BorrowerRelationshipTitleType != null && borrowerDetail.BorrowerRelationshipTitleType.EnumValue == BorrowerRelationshipTitleBase.Other)
            {
                // If the relationship title is set to "Other", use the description field, otherwise
                // use the mapped description of the relationship title setting.
                borrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = ToMismoString(this.App.aRelationshipTitleT == E_aRelationshipTitleT.Other ? this.App.aRelationshipTitleOtherDesc : CAppBase.aRelationshipTitleT_Map_rep(this.App.aRelationshipTitleT));
            }

            borrowerDetail.DependentCount = ToMismoCount(string.IsNullOrEmpty(this.App.aDependNum_rep) ? "0" : this.App.aDependNum_rep, null);
            borrowerDetail.JointAssetLiabilityReportingType = ToMismoEnum(GetJointAssetLiabilityReportingBaseValue(this.App.aAsstLiaCompletedNotJointly), isSensitiveData: false);
            borrowerDetail.MaritalStatusType = this.App.aMaritalStatT == E_aBMaritalStatT.LeaveBlank ? null : ToMismoEnum(GetMaritalStatusBaseValue(this.App.aMaritalStatT), isSensitiveData: false);
            borrowerDetail.SchoolingYearsCount = ToMismoCount(this.App.aSchoolYrs_rep, null);
            borrowerDetail.DomesticRelationshipIndicator = ToMismoIndicator(this.App.aDomesticRelationshipTri);
            borrowerDetail.DomesticRelationshipStateCode = ToMismoCode(this.App.aDomesticRelationshipStateCode);
            borrowerDetail.DomesticRelationshipType = ToMismoEnum(this.GetDomesticRelationshipBaseValue(this.App.aDomesticRelationshipType));
            
            if (borrowerDetail.DomesticRelationshipType?.IsSetToOther ?? false)
            {
                borrowerDetail.DomesticRelationshipTypeOtherDescription = ToMismoString(this.App.aDomesticRelationshipTypeOtherDescription);
            }

            borrowerDetail.Extension = this.CreateBorrowerDetailExtension();
            return borrowerDetail;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to a domestic relationship type.
        /// </summary>
        /// <param name="type">A domestic relationship type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private DomesticRelationshipBase GetDomesticRelationshipBaseValue(DomesticRelationshipType type)
        {
            switch (type)
            {
                case DomesticRelationshipType.Blank:
                    return DomesticRelationshipBase.Blank;
                case DomesticRelationshipType.CivilUnion:
                    return DomesticRelationshipBase.CivilUnion;
                case DomesticRelationshipType.DomesticPartnership:
                    return DomesticRelationshipBase.DomesticPartnership;
                case DomesticRelationshipType.RegisteredReciprocalBeneficiaryRelationship:
                    return DomesticRelationshipBase.RegisteredReciprocalBeneficiaryRelationship;
                case DomesticRelationshipType.Other:
                    return DomesticRelationshipBase.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Creates a container holding extended details about a borrower.
        /// </summary>
        /// <returns>A BORROWER_DETAIL_EXTENSION container.</returns>
        private BORROWER_DETAIL_EXTENSION CreateBorrowerDetailExtension()
        {
            var borrowerDetailExtension = new BORROWER_DETAIL_EXTENSION();

            var extension = new LQB_BORROWER_DETAIL_EXTENSION();
            extension.FinancialInstitutionMemberIdentifier = ToMismoIdentifier(this.App.BorrowerModeT == E_BorrowerModeT.Borrower ? this.App.aBCoreSystemId : this.App.aCCoreSystemId, isSensitiveData: true);
            borrowerDetailExtension.Other = extension;

            return borrowerDetailExtension;
        }

        /// <summary>
        /// Creates a container representing borrower counseling.
        /// </summary>
        /// <returns>A <see cref="COUNSELING"/> container.</returns>
        private COUNSELING CreateCounseling()
        {
            var counseling = new COUNSELING();
            counseling.CounselingDetail = this.CreateCounselingDetail();
            counseling.CounselingEvents = this.CreateCounselingEvents();

            return counseling;
        }

        /// <summary>
        /// Creates a container with details on borrower counseling.
        /// </summary>
        /// <returns>A <see cref="COUNSELING_DETAIL"/> container.</returns>
        private COUNSELING_DETAIL CreateCounselingDetail()
        {
            var counselingDetail = new COUNSELING_DETAIL();
            counselingDetail.HousingCounselingAgenciesListProvidedDate = ToMismoDate(this.DataLoan.sHomeownerCounselingOrganizationDisclosureD_rep);

            return counselingDetail;
        }

        /// <summary>
        /// Creates a container with borrower counseling events.
        /// </summary>
        /// <returns>A <see cref="COUNSELING_EVENTS"/> container.</returns>
        private COUNSELING_EVENTS CreateCounselingEvents()
        {
            if (this.DataLoan.sBorrowerApplicationCollectionT == E_sLqbCollectionT.Legacy)
            {
                // Counseling events are a new collection utilizing
                // the LQB collection datalayer. Loans on the legacy
                // datalayer do not support this collection.
                return null;
            }

            var counselingEvents = new COUNSELING_EVENTS();
            counselingEvents.CounselingEventList.AddRange(this.CreateCounselingEventList());

            return counselingEvents;
        }

        /// <summary>
        /// Creates a list of counseling event containers.
        /// </summary>
        /// <returns>A list of counseling event containers.</returns>
        private IEnumerable<COUNSELING_EVENT> CreateCounselingEventList()
        {
            var consumerId = this.App.BorrowerModeT == E_BorrowerModeT.Borrower 
                ? this.App.aBConsumerId.ToIdentifier<DataObjectKind.Consumer>()
                : this.App.aCConsumerId.ToIdentifier<DataObjectKind.Consumer>();

            var counselingEventsForConsumer = new HashSet<DataObjectIdentifier<DataObjectKind.CounselingEvent, Guid>>(this.DataLoan.CounselingEventAttendances.Values
                .Where(attendanceConsumerIdEventIdPair => attendanceConsumerIdEventIdPair.ConsumerId == consumerId)
                .Select(attendanceConsumerIdEventIdPair => attendanceConsumerIdEventIdPair.CounselingEventId));

            return this.DataLoan.CounselingEvents
                .Where(counselingEvent => counselingEventsForConsumer.Contains(counselingEvent.Key))
                .Select(relevantCounselingEventIdValuePair => this.CreateCounselingEvent(relevantCounselingEventIdValuePair.Value));
        }

        /// <summary>
        /// Creates a container for a counseling event.
        /// </summary>
        /// <param name="lqbCounselingEvent">
        /// The counseling event on file.
        /// </param>
        /// <returns>A <see cref="COUNSELING_EVENT"/> container.</returns>
        private COUNSELING_EVENT CreateCounselingEvent(CounselingEvent lqbCounselingEvent)
        {
            var counselingEvent = new COUNSELING_EVENT();
            counselingEvent.CounselingEventDetail = this.CreateCounselingEventDetail(lqbCounselingEvent);

            return counselingEvent;
        }

        /// <summary>
        /// Creates a container for a counseling event detail.
        /// </summary>
        /// <param name="counselingEvent">
        /// The counseling event on file.
        /// </param>
        /// <returns>A <see cref="COUNSELING_EVENT_DETAIL"/> container.</returns>
        private COUNSELING_EVENT_DETAIL CreateCounselingEventDetail(CounselingEvent counselingEvent)
        {
            var stringFormatter = counselingEvent.GetStringFormatter(this.DataLoan.RepStringFormatter, null);
            return new COUNSELING_EVENT_DETAIL()
            {
                CounselingCompletedDate = ToMismoDate(stringFormatter.CompletedDate),
                CounselingType = ToMismoEnum(GetCounselingTypeBaseValue(counselingEvent.CounselingType)),
                CounselingFormatType = ToMismoEnum(GetCounselingFormatTypeBaseValue(counselingEvent.CounselingFormat))
            };
        }

        /// <summary>
        /// Creates a container to hold current income items.
        /// </summary>
        /// <returns>A CURRENT_INCOME container.</returns>
        private CURRENT_INCOME CreateCurrentIncome()
        {
            var currentIncome = new CURRENT_INCOME();
            currentIncome.CurrentIncomeItems = this.CreateCurrentIncomeItems();

            return currentIncome;
        }

        /// <summary>
        /// Creates a container to hold all instances of CURRENT_INCOME_ITEM, and populates it
        /// using types of income listed in the application data.
        /// </summary>
        /// <returns>A CURRENT_INCOME_ITEMS container.</returns>
        private CURRENT_INCOME_ITEMS CreateCurrentIncomeItems()
        {
            if (this.App.BorrowerModeT == E_BorrowerModeT.Borrower ? IsAmountStringBlankOrZero(this.App.aBTotI_rep) : IsAmountStringBlankOrZero(this.App.aCTotI_rep))
            {
                return null;
            }

            var currentIncomeItems = new CURRENT_INCOME_ITEMS();
            currentIncomeItems.CurrentIncomeItemList.Add(this.CreateCurrentIncomeItem(this.App.aBaseI_rep, IncomeBase.Base, GetNextSequenceNumber(currentIncomeItems.CurrentIncomeItemList)));
            currentIncomeItems.CurrentIncomeItemList.Add(this.CreateCurrentIncomeItem(this.App.aOvertimeI_rep, IncomeBase.Overtime, GetNextSequenceNumber(currentIncomeItems.CurrentIncomeItemList)));
            currentIncomeItems.CurrentIncomeItemList.Add(this.CreateCurrentIncomeItem(this.App.aBonusesI_rep, IncomeBase.Bonus, GetNextSequenceNumber(currentIncomeItems.CurrentIncomeItemList)));
            currentIncomeItems.CurrentIncomeItemList.Add(this.CreateCurrentIncomeItem(this.App.aCommisionI_rep, IncomeBase.Commissions, GetNextSequenceNumber(currentIncomeItems.CurrentIncomeItemList)));
            currentIncomeItems.CurrentIncomeItemList.Add(this.CreateCurrentIncomeItem(this.App.aDividendI_rep, IncomeBase.DividendsInterest, GetNextSequenceNumber(currentIncomeItems.CurrentIncomeItemList)));
            currentIncomeItems.CurrentIncomeItemList.Add(this.CreateCurrentIncomeItem(this.App.aNetRentI1003_rep, IncomeBase.NetRentalIncome, GetNextSequenceNumber(currentIncomeItems.CurrentIncomeItemList)));
            currentIncomeItems.CurrentIncomeItemList.Add(this.CreateCurrentIncomeItem(this.App.aSpPosCf_rep, IncomeBase.SubjectPropertyNetCashFlow, GetNextSequenceNumber(currentIncomeItems.CurrentIncomeItemList)));

            foreach (OtherIncome otherIncome in this.App.aOtherIncomeList)
            {
                if ((otherIncome.IsForCoBorrower && this.App.BorrowerModeT == E_BorrowerModeT.Coborrower) || (!otherIncome.IsForCoBorrower && this.App.BorrowerModeT == E_BorrowerModeT.Borrower))
                {
                    currentIncomeItems.CurrentIncomeItemList.Add(this.CreateCurrentIncomeItem(this.App.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep), OtherIncome.Get_aOIDescT(otherIncome.Desc), GetNextSequenceNumber(currentIncomeItems.CurrentIncomeItemList), otherIncome.Desc));
                }
            }

            return currentIncomeItems;
        }

        /// <summary>
        /// Creates a container holding data on a common type of current income.
        /// </summary>
        /// <param name="amount">The amount of the income.</param>
        /// <param name="type">The type of the income.</param>
        /// <param name="sequence">The sequence number of the income item among the list of items.</param>
        /// <returns>A CURRENT_INCOME_ITEM container.</returns>
        private CURRENT_INCOME_ITEM CreateCurrentIncomeItem(string amount, IncomeBase type, int sequence)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var currentIncomeItem = new CURRENT_INCOME_ITEM();
            currentIncomeItem.CurrentIncomeItemDetail = this.CreateCurrentIncomeItemDetail(amount, type);
            currentIncomeItem.SequenceNumber = sequence;

            return currentIncomeItem;
        }

        /// <summary>
        /// Creates a container with details on a common income item.
        /// </summary>
        /// <param name="amount">The amount of the income.</param>
        /// <param name="type">The type of the income.</param>
        /// <returns>A CURRENT_INCOME_ITEM_DETAIL container.</returns>
        private CURRENT_INCOME_ITEM_DETAIL CreateCurrentIncomeItemDetail(string amount, IncomeBase type)
        {
            var currentIncomeItemDetail = new CURRENT_INCOME_ITEM_DETAIL();
            currentIncomeItemDetail.CurrentIncomeMonthlyTotalAmount = ToMismoAmount(amount);
            currentIncomeItemDetail.IncomeType = ToMismoEnum(type, isSensitiveData: false);

            return currentIncomeItemDetail;
        }

        /// <summary>
        /// Creates a container holding data on an uncommon type of current income, which must be
        /// mapped to MISMO from the income description.
        /// </summary>
        /// <param name="amount">The amount of the income.</param>
        /// <param name="type">The type of the income.</param>
        /// <param name="sequence">The sequence number of the income item among the list of items.</param>
        /// <param name="otherDescription">The description for income type "Other".</param>
        /// <returns>A CURRENT_INCOME_ITEM container.</returns>
        private CURRENT_INCOME_ITEM CreateCurrentIncomeItem(string amount, E_aOIDescT type, int sequence, string otherDescription)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var currentIncomeItem = new CURRENT_INCOME_ITEM();
            currentIncomeItem.CurrentIncomeItemDetail = this.CreateCurrentIncomeItemDetail(amount, type, otherDescription);
            currentIncomeItem.SequenceNumber = sequence;

            return currentIncomeItem;
        }

        /// <summary>
        /// Creates a container with details on an uncommon income item, which must be mapped from an LQB value.
        /// </summary>
        /// <param name="amount">The amount of the income.</param>
        /// <param name="type">The type of the income.</param>
        /// <param name="otherDescription">The description of the income if the type is "Other".</param>
        /// <returns>A CURRENT_INCOME_ITEM_DETAIL container.</returns>
        private CURRENT_INCOME_ITEM_DETAIL CreateCurrentIncomeItemDetail(string amount, E_aOIDescT type, string otherDescription)
        {
            var currentIncomeItemDetail = new CURRENT_INCOME_ITEM_DETAIL();
            currentIncomeItemDetail.CurrentIncomeMonthlyTotalAmount = ToMismoAmount(amount);
            currentIncomeItemDetail.IncomeType = ToMismoEnum(GetIncomeBaseValue(type), isSensitiveData: false);
            if (currentIncomeItemDetail.IncomeType != null && currentIncomeItemDetail.IncomeType.EnumValue == IncomeBase.Other)
            {
                currentIncomeItemDetail.IncomeTypeOtherDescription = ToMismoString(otherDescription);
            }

            return currentIncomeItemDetail;
        }

        /// <summary>
        /// Creates a container to hold up to three of a borrower's credit scores.
        /// </summary>
        /// <returns>A CREDIT_SCORES container.</returns>
        private CREDIT_SCORES CreateCreditScores()
        {
            var creditScores = new CREDIT_SCORES();
            var isOnOrBeyondVersion26 = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.DataLoan.sLoanVersionT, LoanVersionT.V26_ReplaceFreeformCreditModelsWithEnums);

            creditScores.CreditScoreList.Add(this.CreateCreditScore(
                scoreCreatedDateRep: this.App.aEquifaxCreatedD_rep,
                scoreValueRep: this.App.aEquifaxScore_rep,
                scorePercentile: this.App.aEquifaxPercentile_rep,
                agencyFactors: this.App.aEquifaxFactors,
                agency: CreditRepositorySourceBase.Equifax,
                model: isOnOrBeyondVersion26 ? this.App.aEquifaxModelT : default(E_CreditScoreModelT?),
                modelOtherDescription: isOnOrBeyondVersion26 ? this.App.aEquifaxModelTOtherDescription : null,
                sequence: GetNextSequenceNumber(creditScores.CreditScoreList)));

            creditScores.CreditScoreList.Add(this.CreateCreditScore(
                scoreCreatedDateRep: this.App.aExperianCreatedD_rep,
                scoreValueRep: this.App.aExperianScore_rep,
                scorePercentile: this.App.aExperianPercentile_rep,
                agencyFactors: this.App.aExperianFactors,
                agency: CreditRepositorySourceBase.Experian,
                model: isOnOrBeyondVersion26 ? this.App.aExperianModelT : default(E_CreditScoreModelT?),
                modelOtherDescription: isOnOrBeyondVersion26 ? this.App.aExperianModelTOtherDescription : null,
                sequence: GetNextSequenceNumber(creditScores.CreditScoreList)));

            creditScores.CreditScoreList.Add(this.CreateCreditScore(
                scoreCreatedDateRep: this.App.aTransUnionCreatedD_rep,
                scoreValueRep: this.App.aTransUnionScore_rep,
                scorePercentile: this.App.aTransUnionPercentile_rep,
                agencyFactors: this.App.aTransUnionFactors,
                agency: CreditRepositorySourceBase.TransUnion,
                model: isOnOrBeyondVersion26 ? this.App.aTransUnionModelT : default(E_CreditScoreModelT?),
                modelOtherDescription: isOnOrBeyondVersion26 ? this.App.aTransUnionModelTOtherDescription : null,
                sequence: GetNextSequenceNumber(creditScores.CreditScoreList)));

            return creditScores;
        }

        /// <summary>
        /// Creates a container representing a credit score from a specific reporting agency.
        /// </summary>
        /// <param name="scoreValueRep">The numeric value of the credit score, as a string.</param>
        /// <param name="scorePercentile">The credit score percentile.</param>
        /// <param name="sequence">The sequence number of the credit score among the list of scores.</param>
        /// <param name="scoreCreatedDateRep">The creation date of the credit score, as a string.</param>
        /// <param name="agencyFactors">The credit reporting agency's factors behind the score.</param>
        /// <param name="agency">The credit reporting agency.</param>
        /// <param name="model">The credit scoring model type.</param>
        /// <param name="modelOtherDescription">The credit scoring model other description.</param>
        /// <returns>A CREDIT_SCORE container.</returns>
        private CREDIT_SCORE CreateCreditScore(string scoreValueRep, string scorePercentile, int sequence, string scoreCreatedDateRep = null, string agencyFactors = null, CreditRepositorySourceBase agency = CreditRepositorySourceBase.Blank, E_CreditScoreModelT? model = null, string modelOtherDescription = null)
        {
            var creditScore = new CREDIT_SCORE();
            creditScore.SequenceNumber = sequence;

            if (!string.IsNullOrEmpty(scoreValueRep))
            {
                creditScore.CreditScoreDetail = this.CreateCreditScoreDetail(agency, scoreCreatedDateRep, scoreValueRep, scorePercentile, model, modelOtherDescription);
                creditScore.CreditScoreFactors = this.CreateCreditScoreFactors(agencyFactors);
            }

            return creditScore;
        }

        /// <summary>
        /// Creates a container holding data on a specific credit score.
        /// </summary>
        /// <param name="agency">The credit reporting agency.</param>
        /// <param name="date">The issue date of the credit report.</param>
        /// <param name="score">The credit score.</param>
        /// <param name="percentile">The credit score percentile.</param>
        /// <param name="model">The credit scoring model type.</param>
        /// <param name="modelOtherDescription">The credit scoring model other description.</param>
        /// <returns>A CREDIT_SCORE_DETAIL container.</returns>
        private CREDIT_SCORE_DETAIL CreateCreditScoreDetail(CreditRepositorySourceBase agency, string date, string score, string percentile, E_CreditScoreModelT? model, string modelOtherDescription)
        {
            var creditScoreDetail = new CREDIT_SCORE_DETAIL();
            creditScoreDetail.CreditRepositorySourceType = ToMismoEnum(agency, isSensitiveData: true);
            creditScoreDetail.CreditScoreDate = ToMismoDate(date, isSensitiveData: true);
            creditScoreDetail.CreditScoreValue = ToMismoValue(score, isSensitiveData: true);
            creditScoreDetail.CreditScoreRankPercentileValue = ToMismoValue(percentile, isSensitiveData: true);

            if (model.HasValue)
            {
                creditScoreDetail.CreditScoreModelNameType = model.Value == E_CreditScoreModelT.LeaveBlank ? null : ToMismoEnum(GetCreditScoreModelNameBaseValue(model.Value), isSensitiveData: true);

                if (creditScoreDetail.CreditScoreModelNameType?.EnumValue == CreditScoreModelNameBase.Other)
                {
                    string otherDescription = model.Value == E_CreditScoreModelT.Other ? modelOtherDescription : EnumUtilities.GetDescription(model.Value);
                    creditScoreDetail.CreditScoreModelNameTypeOtherDescription = ToMismoString(otherDescription);
                }
            }

            return creditScoreDetail;
        }

        /// <summary>
        /// Creates a container holding factors of a borrower's credit score.
        /// </summary>
        /// <param name="factors">A list of factors delimited by newlines.</param>
        /// <returns>A CREDIT_SCORE_FACTORS container.</returns>
        private CREDIT_SCORE_FACTORS CreateCreditScoreFactors(string factors)
        {
            if (string.IsNullOrEmpty(factors))
            {
                return null;
            }

            var creditScoreFactors = new CREDIT_SCORE_FACTORS();

            foreach (string factor in factors.Split(Environment.NewLine.ToCharArray()))
            {
                if (string.IsNullOrEmpty(factor))
                {
                    continue;
                }

                creditScoreFactors.CreditScoreFactorList.Add(this.CreateCreditScoreFactor(factor, GetNextSequenceNumber(creditScoreFactors.CreditScoreFactorList)));
            }

            return creditScoreFactors;
        }

        /// <summary>
        /// Creates a container holding a single credit score factor.
        /// </summary>
        /// <param name="factor">The credit score factor.</param>
        /// <param name="sequence">The sequence number of the credit score factor among the list of factors.</param>
        /// <returns>A CREDIT_SCORE_FACTOR container.</returns>
        private CREDIT_SCORE_FACTOR CreateCreditScoreFactor(string factor, int sequence)
        {
            var creditScoreFactor = new CREDIT_SCORE_FACTOR();
            creditScoreFactor.CreditScoreFactorText = ToMismoString(factor, isSensitiveData: true);
            creditScoreFactor.SequenceNumber = sequence;

            return creditScoreFactor;
        }

        /// <summary>
        /// Creates a container to hold all instances of DEPENDENT and populates it with
        /// a specific borrower's dependents.
        /// </summary>
        /// <returns>A DEPENDENTS container.</returns>
        private DEPENDENTS CreateDependents()
        {
            var dependents = new DEPENDENTS();
            string[] dependentAges = this.App.aDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':');

            foreach (string age in dependentAges)
            {
                if (string.IsNullOrEmpty(age))
                {
                    continue;
                }

                dependents.DependentList.Add(this.CreateDependent(age, GetNextSequenceNumber(dependents.DependentList)));
            }

            return dependents;
        }

        /// <summary>
        /// Creates a container holding data on a dependent of the borrower.
        /// </summary>
        /// <param name="age">The age of the dependent.</param>
        /// <param name="sequence">The sequence number of the dependent among the list of dependents.</param>
        /// <returns>A DEPENDENT container.</returns>
        private DEPENDENT CreateDependent(string age, int sequence)
        {
            var dependent = new DEPENDENT();
            dependent.DependentAgeYearsCount = ToMismoCount(age, this.DataLoan.m_convertLos);
            dependent.SequenceNumber = sequence;

            return dependent;
        }

        /// <summary>
        /// Creates a container representing a declaration.
        /// </summary>
        /// <returns>A DECLARATION container.</returns>
        private DECLARATION CreateDeclaration()
        {
            var declaration = new DECLARATION();

            if (this.DataLoan.sIsTargeting2019Ulad)
            {
                declaration.DeclarationDetail = this.CreateDeclarationDetail_Ulad2019();
            }
            else
            {
                declaration.DeclarationDetail = this.CreateDeclarationDetail_Legacy();
            }

            return declaration;
        }

        /// <summary>
        /// Creates a container holding data on a declaration for declarations
        /// on loans targeting ULAD 2019.
        /// </summary>
        /// <returns>A DECLARATION_DETAIL container.</returns>
        private DECLARATION_DETAIL CreateDeclarationDetail_Ulad2019()
        {
            var declarationDetail = new DECLARATION_DETAIL();

            // Section A
            declarationDetail.IntentToOccupyType = ToMismoEnum(GetIntentToOccupyBaseValue(this.App.aDecIsPrimaryResidenceUlad));
            declarationDetail.HomeownerPastThreeYearsType = ToMismoEnum(GetHomeownerPastThreeYearsBaseValue(this.App.aDecHasOwnershipIntOtherPropLastThreeYearsUlad));
            declarationDetail.PriorPropertyUsageType = ToMismoEnum(GetPriorPropertyUsageBaseValue(this.App.aDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad));
            declarationDetail.FHASecondaryResidenceIndicator = ToMismoIndicator(this.App.aDecOwnershipIntOtherPropLastThreeYearsPropTypeUlad == E_aBDecPastOwnedPropT.SR);
            declarationDetail.PriorPropertyTitleType = ToMismoEnum(GetPriorPropertyTitleBaseValue(this.App.aDecOwnershipIntOtherPropLastThreeYearsTitleHoldTypeUlad));

            // Section B -- Create extension for family or business affiliate datapoint
            declarationDetail.Extension = this.CreateDeclarationDetailExtension_Ulad2019();

            // Section C
            declarationDetail.UndisclosedBorrowedFundsIndicator = ToMismoIndicator(this.App.aDecIsBorrowingFromOthersForTransUlad);
            declarationDetail.UndisclosedBorrowedFundsAmount = ToMismoAmount(this.DataLoan.m_convertLos.ToMoneyString(this.App.aDecAmtBorrowedFromOthersForTransUlad, FormatDirection.ToRep));

            // Section D
            declarationDetail.UndisclosedMortgageApplicationIndicator = ToMismoIndicator(this.App.aDecHasOrWillApplyForMtgOnOtherPropBeforeClosingNotAlreadyDiscUlad);
            declarationDetail.UndisclosedCreditApplicationIndicator = ToMismoIndicator(this.App.aDecHasOrWillApplyForNewCreditBeforeClosingNotAlreadyDiscUlad);

            // Section E
            declarationDetail.PropertyProposedCleanEnergyLienIndicator = ToMismoIndicator(this.App.aDecPropSubjectToHigherPriorityLienThanFirstMtgUlad);

            // Section F
            declarationDetail.UndisclosedComakerOfNoteIndicator = ToMismoIndicator(this.App.aDecIsCosignerGuarantorOnOtherDebtNotAlreadyDisclosedUlad);

            // Section G
            declarationDetail.OutstandingJudgmentsIndicator = ToMismoIndicator(this.App.aDecHasOutstandingJudgmentsUlad);

            // Section H
            declarationDetail.PresentlyDelinquentIndicator = ToMismoIndicator(this.App.aDecIsDelinquentOrDefaultFedDebtUlad);

            // Section I
            declarationDetail.PartyToLawsuitIndicator = ToMismoIndicator(this.App.aDecIsPartyLawsuitWithPotentialPersonalFinancialLiabilityUlad);

            // Section J
            declarationDetail.PriorPropertyDeedInLieuConveyedIndicator = ToMismoIndicator(this.App.aDecHasConveyedTitleInLieuOfFcLast7YearsUlad);

            // Section K
            declarationDetail.PriorPropertyShortSaleCompletedIndicator = ToMismoIndicator(this.App.aDecCompletedPreFcSaleShortSaleLast7YearsLessOutstandingMtgBalDueUlad);

            // Section L
            declarationDetail.PriorPropertyForeclosureCompletedIndicator = ToMismoIndicator(this.App.aDecHasForeclosedLast7YearsUlad);

            // Section M
            declarationDetail.BankruptcyIndicator = ToMismoIndicator(this.App.aDecHasBankruptcyLast7YearsUlad);

            return declarationDetail;
        }

        /// <summary>
        /// Creates a container holding data on a declaration for declarations
        /// on loans not targeting ULAD 2019.
        /// </summary>
        /// <returns>A DECLARATION_DETAIL container.</returns>
        private DECLARATION_DETAIL CreateDeclarationDetail_Legacy()
        {
            var declarationDetail = new DECLARATION_DETAIL();
            declarationDetail.AlimonyChildSupportObligationIndicator = ToMismoIndicator(this.App.aDecAlimony);
            declarationDetail.BankruptcyIndicator = ToMismoIndicator(this.App.aDecBankrupt);
            declarationDetail.UndisclosedBorrowedFundsIndicator = ToMismoIndicator(this.App.aDecBorrowing);

            if (this.DataLoan.sLT == E_sLT.FHA || this.DataLoan.sLT == E_sLT.VA)
            {
                declarationDetail.BorrowerFirstTimeHomebuyerIndicator = ToMismoIndicator(this.App.aHas1stTimeBuyerTri == E_TriState.Yes);
            }
            else
            {
                declarationDetail.BorrowerFirstTimeHomebuyerIndicator = ToMismoIndicator(this.DataLoan.sHas1stTimeBuyer);
            }

            declarationDetail.CitizenshipResidencyType = ToMismoEnum(GetCitizenshipResidencyBaseValue(this.App.aDecResidency, this.App.aDecCitizen), isSensitiveData: false);
            declarationDetail.CoMakerEndorserOfNoteIndicator = ToMismoIndicator(this.App.aDecEndorser);
            declarationDetail.HomeownerPastThreeYearsType = ToMismoEnum(GetHomeownerPastThreeYearsBaseValue(this.App.aDecPastOwnership), isSensitiveData: false);
            declarationDetail.IntentToOccupyType = ToMismoEnum(GetIntentToOccupyBaseValue(this.App.aDecOcc), isSensitiveData: false);
            declarationDetail.LoanForeclosureOrJudgmentIndicator = ToMismoIndicator(this.App.aDecObligated);
            declarationDetail.OutstandingJudgmentsIndicator = ToMismoIndicator(this.App.aDecJudgment);
            declarationDetail.PartyToLawsuitIndicator = ToMismoIndicator(this.App.aDecLawsuit);
            declarationDetail.PresentlyDelinquentIndicator = ToMismoIndicator(this.App.aDecDelinquent);
            declarationDetail.PriorPropertyTitleType = ToMismoEnum(GetPriorPropertyTitleBaseValue(this.App.aDecPastOwnedPropTitleT), isSensitiveData: false);
            declarationDetail.PriorPropertyUsageType = ToMismoEnum(GetPriorPropertyUsageBaseValue(this.App.aDecPastOwnedPropT), isSensitiveData: false);
            declarationDetail.PriorPropertyForeclosureCompletedIndicator = ToMismoIndicator(this.App.aDecForeclosure);

            return declarationDetail;
        }

        /// <summary>
        /// Creates an extension container for a declaration detail.
        /// </summary>
        /// <returns>A <see cref="DECLARATION_DETAIL_EXTENSION"/> container.</returns>
        private DECLARATION_DETAIL_EXTENSION CreateDeclarationDetailExtension_Ulad2019()
        {
            var extension = new DECLARATION_DETAIL_EXTENSION();
            extension.Other = this.CreateLqbDeclarationDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates an LQB declaration detail extension.
        /// </summary>
        /// <returns>A <see cref="LQB_DECLARATION_DETAIL_EXTENSION"/> container.</returns>
        private LQB_DECLARATION_DETAIL_EXTENSION CreateLqbDeclarationDetailExtension()
        {
            var extension = new LQB_DECLARATION_DETAIL_EXTENSION();
            extension.SpecialBorrowerSellerRelationshipIndicator = ToMismoIndicator(this.App.BorrowerModeT == E_BorrowerModeT.Borrower 
                ? this.App.aBDecIsFamilyOrBusAffiliateOfSellerUlad
                : this.App.aCDecIsFamilyOrBusAffiliateOfSellerUlad);

            return extension;
        }

        /// <summary>
        /// Creates a container to hold all instances of EMPLOYER, and populates it with employer
        /// data from the borrower's application.
        /// </summary>
        /// <returns>An EMPLOYERS container.</returns>
        private EMPLOYERS CreateEmployers()
        {
            var employers = new EMPLOYERS();
            employers.EmployerList.Add(this.CreateEmployer(this.App.aEmpCollection.GetPrimaryEmp(false), GetNextSequenceNumber(employers.EmployerList)));

            foreach (IRegularEmploymentRecord record in this.App.aEmpCollection.GetSubcollection(true, E_EmpGroupT.Previous))
            {
                employers.EmployerList.Add(this.CreateEmployer(record, GetNextSequenceNumber(employers.EmployerList)));
            }

            return employers;
        }

        /// <summary>
        /// Creates a container representing a borrower's primary employer.
        /// </summary>
        /// <param name="record">The primary employer record.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>An EMPLOYER container.</returns>
        private EMPLOYER CreateEmployer(IPrimaryEmploymentRecord record, int sequenceNumber)
        {
            if (record == null)
            {
                return null;
            }

            var employer = new EMPLOYER();
            employer.SequenceNumber = sequenceNumber;

            employer.LegalEntity = CreateLegalEntity(record.EmplrNm, record.EmplrBusPhone, record.EmplrFax, string.Empty, null, ContactPointRoleBase.Work);
            employer.Address = CreateAddress(
                record.EmplrAddr,
                record.EmplrCity,
                record.EmplrState,
                record.EmplrZip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(record.EmplrState),
                AddressBase.Blank,
                1);

            employer.Employment = this.CreateEmployment(record);

            return employer;
        }

        /// <summary>
        /// Creates a container holding data on a borrower's employment with a primary employer.
        /// </summary>
        /// <param name="record">The primary employer record.</param>
        /// <returns>An EMPLOYMENT container.</returns>
        private EMPLOYMENT CreateEmployment(IPrimaryEmploymentRecord record)
        {
            var employment = new EMPLOYMENT();
            employment.EmploymentMonthsOnJobCount = ToMismoCount(record.EmplmtLenInMonths_rep, null);
            employment.EmploymentYearsOnJobCount = ToMismoCount(record.EmplmtLenInYrs_rep, null);
            employment.EmploymentTimeInLineOfWorkYearsCount = ToMismoCount(record.ProfLenYears, null);
            employment.EmploymentTimeInLineOfWorkMonthsCount = ToMismoCount(record.ProfLenRemainderMonths, null);
            employment.EmploymentBorrowerSelfEmployedIndicator = ToMismoIndicator(record.IsSelfEmplmt);
            employment.EmploymentStatusType = ToMismoEnum(GetEmploymentStatusBaseValue(record.IsCurrent), isSensitiveData: false);
            employment.EmploymentPositionDescription = ToMismoString(record.JobTitle);
            employment.EmploymentClassificationType = ToMismoEnum(GetEmploymentClassificationBaseValue(record.IsPrimaryEmp), isSensitiveData: false);
            employment.SpecialBorrowerEmployerRelationshipIndicator = ToMismoIndicator(record.IsSpecialBorrowerEmployerRelationship);
            employment.OwnershipInterestType = ToMismoEnum(this.GetOwnershipInterestBaseValue(record.SelfOwnershipShareT));
            
            return employment;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to a self-ownership share in a business.
        /// </summary>
        /// <param name="share">The self-ownership share.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private OwnershipInterestBase GetOwnershipInterestBaseValue(SelfOwnershipShare share)
        {
            switch (share)
            {
                case SelfOwnershipShare.Blank:
                    return OwnershipInterestBase.Blank;
                case SelfOwnershipShare.GreaterOrEqualTo25:
                    return OwnershipInterestBase.GreaterThanOrEqualTo25Percent;
                case SelfOwnershipShare.LessThan25:
                    return OwnershipInterestBase.LessThan25Percent;
                default:
                    throw new UnhandledEnumException(share);
            }
        }

        /// <summary>
        /// Creates a container representing one of a borrower's secondary employers.
        /// </summary>
        /// <param name="record">A secondary employer record.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>An EMPLOYER container.</returns>
        private EMPLOYER CreateEmployer(IRegularEmploymentRecord record, int sequenceNumber)
        {
            if (record == null)
            {
                return null;
            }

            var employer = new EMPLOYER();
            employer.SequenceNumber = sequenceNumber;

            employer.LegalEntity = CreateLegalEntity(record.EmplrNm, record.EmplrBusPhone, record.EmplrFax, string.Empty, null, ContactPointRoleBase.Work);
            employer.Address = CreateAddress(
                record.EmplrAddr,
                record.EmplrCity,
                record.EmplrState,
                record.EmplrZip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(record.EmplrState),
                AddressBase.Blank,
                1);

            employer.Employment = this.CreateEmployment(record);

            return employer;
        }

        /// <summary>
        /// Creates a container holding data on a borrower's employment with a secondary employer.
        /// </summary>
        /// <param name="record">A secondary employer record.</param>
        /// <returns>An EMPLOYMENT container.</returns>
        private EMPLOYMENT CreateEmployment(IRegularEmploymentRecord record)
        {
            var employment = new EMPLOYMENT();
            employment.EmploymentBorrowerSelfEmployedIndicator = ToMismoIndicator(record.IsSelfEmplmt);
            employment.EmploymentClassificationType = ToMismoEnum(GetEmploymentClassificationBaseValue(record.IsPrimaryEmp), isSensitiveData: false);
            employment.EmploymentEndDate = ToMismoDate(record.EmplmtEndD_rep);
            employment.EmploymentMonthlyIncomeAmount = ToMismoAmount(record.MonI_rep);
            employment.EmploymentPositionDescription = ToMismoString(record.JobTitle);
            employment.EmploymentStartDate = ToMismoDate(record.EmplmtStartD_rep);
            employment.EmploymentStatusType = ToMismoEnum(GetEmploymentStatusBaseValue(record.IsCurrent), isSensitiveData: false);

            return employment;
        }

        /// <summary>
        /// Creates a container holding data about a borrower who is taking an FHA or VA loan.
        /// </summary>
        /// <returns>A GOVERNMENT_BORROWER container.</returns>
        private GOVERNMENT_BORROWER CreateGovernmentBorrower()
        {
            if (this.DataLoan.sLT != E_sLT.FHA && this.DataLoan.sLT != E_sLT.VA)
            {
                return null;
            }

            var governmentBorrower = new GOVERNMENT_BORROWER();
            governmentBorrower.CAIVRSIdentifier = ToMismoIdentifier(this.App.BorrowerModeT == E_BorrowerModeT.Borrower ? this.App.aFHABCaivrsNum : this.App.aFHACCaivrsNum);

            // FHABorrowerCertification... used for both FHA and VA
            if (this.App.BorrowerModeT == E_BorrowerModeT.Borrower && (this.DataLoan.sLT == E_sLT.FHA || this.DataLoan.sLT == E_sLT.VA))
            {
                governmentBorrower.FHABorrowerCertificationOriginalMortgageAmount = ToMismoAmount(this.App.aFHABorrCertOtherPropOrigMAmt_rep);
                governmentBorrower.FHABorrowerCertificationOwnFourOrMoreDwellingsIndicator = ToMismoIndicator(this.App.aFHABorrCertOwnMoreThan4DwellingsTri);
                governmentBorrower.FHABorrowerCertificationOwnOtherPropertyIndicator = ToMismoIndicator(this.App.aFHABorrCertOwnOrSoldOtherFHAPropTri);
                governmentBorrower.FHABorrowerCertificationPropertySoldCityName = ToMismoString(this.App.aFHABorrCertOtherPropCity);
                governmentBorrower.FHABorrowerCertificationPropertySoldPostalCode = ToMismoCode(this.App.aFHABorrCertOtherPropZip);
                governmentBorrower.FHABorrowerCertificationPropertySoldStateName = ToMismoString(this.App.aFHABorrCertOtherPropState);
                governmentBorrower.FHABorrowerCertificationPropertySoldStreetAddressLineText = ToMismoString(this.App.aFHABorrCertOtherPropStAddr);
                governmentBorrower.FHABorrowerCertificationPropertyToBeSoldIndicator = ToMismoIndicator(this.App.aFHABorrCertOtherPropToBeSoldTri);
                governmentBorrower.FHABorrowerCertificationSalesPriceAmount = ToMismoAmount(this.App.aFHABorrCertOtherPropSalesPrice_rep);
                governmentBorrower.FHABorrowerCertificationLeadPaintIndicator = ToMismoIndicator(this.App.aFHABorrCertReceivedLeadPaintPoisonInfoTri);

                var appraisedValueEnum = GetFHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBaseValue(this.App.aFHABorrCertInformedPropValAwareAtContractSigning, this.App.aFHABorrCertInformedPropValNotAwareAtContractSigning);
                governmentBorrower.FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueType = appraisedValueEnum == FHA_VABorrowerCertificationSalesPriceExceedsAppraisedValueBase.Blank ? null : ToMismoEnum(appraisedValueEnum, isSensitiveData: false);
            }

            // VA only
            if (this.App.BorrowerModeT == E_BorrowerModeT.Borrower && this.DataLoan.sLT == E_sLT.VA)
            {
                var occupancyEnumBase = GetVABorrowerCertificationOccupancyBaseValue(this.App.aFHABorrCertOccIsAsHome, this.App.aFHABorrCertOccIsAsHomeForActiveSpouse, this.App.aFHABorrCertOccIsAsHomePrev, this.App.aFHABorrCertOccIsAsHomePrevForActiveSpouse);
                governmentBorrower.VABorrowerCertificationOccupancyType = occupancyEnumBase == VABorrowerCertificationOccupancyBase.Blank ? null : ToMismoEnum(occupancyEnumBase, isSensitiveData: false);
                governmentBorrower.VACoBorrowerNonTaxableIncomeAmount = ToMismoAmount(this.App.aVaCONetI_rep);
                governmentBorrower.VAFederalTaxAmount = ToMismoAmount(this.DataLoan.m_convertLos.ToMoneyString(this.App.aVaBFedITax + this.App.aVaCFedITax, FormatDirection.ToRep));
                governmentBorrower.VALocalTaxAmount = ToMismoAmount(this.DataLoan.m_convertLos.ToMoneyString(this.App.aVaBOITax + this.App.aVaCOITax, FormatDirection.ToRep));
                governmentBorrower.VAPrimaryBorrowerNonTaxableIncomeAmount = ToMismoAmount(this.App.aVaBONetI_rep);
                governmentBorrower.VASocialSecurityTaxAmount = ToMismoAmount(this.DataLoan.m_convertLos.ToMoneyString(this.App.aVaBSsnTax + this.App.aVaCSsnTax, FormatDirection.ToRep));
                governmentBorrower.VAStateTaxAmount = ToMismoAmount(this.DataLoan.m_convertLos.ToMoneyString(this.App.aVaBStateITax + this.App.aVaCStateITax, FormatDirection.ToRep));
            }

            return governmentBorrower;
        }

        /// <summary>
        /// Creates a container holding the borrower's current and previous two residences.
        /// </summary>
        /// <returns>A RESIDENCES container.</returns>
        private RESIDENCES CreateResidences()
        {
            var residences = new RESIDENCES();

            residences.ResidenceList.Add(this.CreateResidence(
                                                          this.App.aAddr,
                                                          this.App.aCity,
                                                          this.App.aState,
                                                          this.App.aZip,
                                                          this.App.aAddrWholeYears_rep,
                                                          this.App.aAddrRemainderMonths_rep,
                                                          this.App.aAddrT,
                                                          BorrowerResidencyBase.Current,
                                                          GetNextSequenceNumber(residences.ResidenceList)));

            residences.ResidenceList.Add(this.CreateResidence(
                                                          this.App.aPrev1Addr,
                                                          this.App.aPrev1City,
                                                          this.App.aPrev1State,
                                                          this.App.aPrev1Zip,
                                                          this.App.aPrev1AddrWholeYears_rep,
                                                          this.App.aPrev1AddrRemainderMonths_rep,
                                                          this.App.aPrev1AddrT,
                                                          BorrowerResidencyBase.Prior,
                                                          GetNextSequenceNumber(residences.ResidenceList)));

            residences.ResidenceList.Add(this.CreateResidence(
                                                          this.App.aPrev2Addr,
                                                          this.App.aPrev2City,
                                                          this.App.aPrev2State,
                                                          this.App.aPrev2Zip,
                                                          this.App.aPrev2AddrWholeYears_rep,
                                                          this.App.aPrev2AddrRemainderMonths_rep,
                                                          this.App.aPrev2AddrT,
                                                          BorrowerResidencyBase.Prior,
                                                          GetNextSequenceNumber(residences.ResidenceList)));

            residences.ResidenceList.Add(this.CreateResidence_Mailing(
                                                            this.App.aAddrMail,
                                                            this.App.aCityMail,
                                                            this.App.aStateMail,
                                                            this.App.aZipMail,
                                                            GetNextSequenceNumber(residences.ResidenceList)));

            return residences;
        }

        /// <summary>
        /// Creates a container representing a borrower's current residence.
        /// </summary>
        /// <param name="address">The street address of the residence.</param>
        /// <param name="city">The city in which the residence is located.</param>
        /// <param name="state">The state in which the residence is located.</param>
        /// <param name="zip">The ZIP code for the residence.</param>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <param name="sequence">The sequence number of the residence among the list of residences.</param>
        /// <returns>A RESIDENCE container.</returns>
        private RESIDENCE CreateResidence(string address, string city, string state, string zip, string years, string months, E_aBAddrT residencyType, BorrowerResidencyBase currentResidence, int sequence)
        {
            var residence = new RESIDENCE();
            residence.SequenceNumber = sequence;
            residence.ResidenceDetail = this.CreateResidenceDetail(years, months, residencyType, currentResidence);

            var addressType = currentResidence.Equals(BorrowerResidencyBase.Current) ? AddressBase.Current : AddressBase.Prior;
            residence.Address = CreateAddress(
                address,
                city,
                state,
                zip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(state),
                addressType,
                1);

            return residence;
        }

        /// <summary>
        /// Creates a container with detail about a borrower's current residence.
        /// </summary>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <returns>A RESIDENCE_DETAIL container.</returns>
        private RESIDENCE_DETAIL CreateResidenceDetail(string years, string months, E_aBAddrT residencyType, BorrowerResidencyBase currentResidence)
        {
            var residenceDetail = new RESIDENCE_DETAIL();
            residenceDetail.BorrowerResidencyDurationYearsCount = ToMismoCount(years, this.DataLoan.m_convertLos);
            residenceDetail.BorrowerResidencyDurationMonthsCount = ToMismoCount(months, this.DataLoan.m_convertLos);

            residenceDetail.BorrowerResidencyBasisType = ToMismoEnum(GetBorrowerResidencyBasisBaseValue(residencyType), isSensitiveData: false);
            residenceDetail.BorrowerResidencyType = ToMismoEnum(currentResidence, isSensitiveData: false);

            return residenceDetail;
        }

        /// <summary>
        /// Creates a container representing a borrower's current residence.
        /// </summary>
        /// <param name="address">The street address of the residence.</param>
        /// <param name="city">The city in which the residence is located.</param>
        /// <param name="state">The state in which the residence is located.</param>
        /// <param name="zip">The ZIP code for the residence.</param>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <param name="sequence">The sequence number of the residence among the list of residences.</param>
        /// <returns>A RESIDENCE container.</returns>
        private RESIDENCE CreateResidence(string address, string city, string state, string zip, string years, string months, E_aBPrev1AddrT residencyType, BorrowerResidencyBase currentResidence, int sequence)
        {
            var residence = new RESIDENCE();
            residence.SequenceNumber = sequence;
            residence.ResidenceDetail = this.CreateResidenceDetail(years, months, residencyType, currentResidence);

            var addressType = currentResidence.Equals(BorrowerResidencyBase.Current) ? AddressBase.Current : AddressBase.Prior;
            residence.Address = CreateAddress(
                address,
                city,
                state,
                zip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(state),
                addressType,
                1);

            return residence;
        }

        /// <summary>
        /// Creates a container with detail about a borrower's past residence.
        /// </summary>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <returns>A RESIDENCE_DETAIL container.</returns>
        private RESIDENCE_DETAIL CreateResidenceDetail(string years, string months, E_aBPrev1AddrT residencyType, BorrowerResidencyBase currentResidence)
        {
            var residenceDetail = new RESIDENCE_DETAIL();
            residenceDetail.BorrowerResidencyDurationYearsCount = ToMismoCount(years, this.DataLoan.m_convertLos);
            residenceDetail.BorrowerResidencyDurationMonthsCount = ToMismoCount(months, this.DataLoan.m_convertLos);

            residenceDetail.BorrowerResidencyBasisType = ToMismoEnum(GetBorrowerResidencyBasisBaseValue(residencyType), isSensitiveData: false);
            residenceDetail.BorrowerResidencyType = ToMismoEnum(currentResidence, isSensitiveData: false);

            return residenceDetail;
        }

        /// <summary>
        /// Creates a container representing a borrower's current residence.
        /// </summary>
        /// <param name="address">The street address of the residence.</param>
        /// <param name="city">The city in which the residence is located.</param>
        /// <param name="state">The state in which the residence is located.</param>
        /// <param name="zip">The ZIP code for the residence.</param>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <param name="sequence">The sequence number of the residence among the list of residences.</param>
        /// <returns>A RESIDENCE container.</returns>
        private RESIDENCE CreateResidence(string address, string city, string state, string zip, string years, string months, E_aBPrev2AddrT residencyType, BorrowerResidencyBase currentResidence, int sequence)
        {
            var residence = new RESIDENCE();
            residence.SequenceNumber = sequence;
            residence.ResidenceDetail = this.CreateResidenceDetail(years, months, residencyType, currentResidence);

            var addressType = currentResidence.Equals(BorrowerResidencyBase.Current) ? AddressBase.Current : AddressBase.Prior;
            residence.Address = CreateAddress(
                address,
                city,
                state,
                zip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(state),
                addressType,
                1);

            return residence;
        }

        /// <summary>
        /// Creates a container with detail about a borrower's past residence.
        /// </summary>
        /// <param name="years">The number of years the borrower has lived in this residence.</param>
        /// <param name="months">The number of months the borrower has lived in this residence.</param>
        /// <param name="residencyType">The borrower's residency type for a tertiary residence.</param>
        /// <param name="currentResidence">Indicates whether this is the borrower's current or prior residence.</param>
        /// <returns>A RESIDENCE_DETAIL container.</returns>
        private RESIDENCE_DETAIL CreateResidenceDetail(string years, string months, E_aBPrev2AddrT residencyType, BorrowerResidencyBase currentResidence)
        {
            var residenceDetail = new RESIDENCE_DETAIL();
            residenceDetail.BorrowerResidencyDurationYearsCount = ToMismoCount(years, this.DataLoan.m_convertLos);
            residenceDetail.BorrowerResidencyDurationMonthsCount = ToMismoCount(months, this.DataLoan.m_convertLos);

            residenceDetail.BorrowerResidencyBasisType = ToMismoEnum(GetBorrowerResidencyBasisBaseValue(residencyType), isSensitiveData: false);
            residenceDetail.BorrowerResidencyType = ToMismoEnum(currentResidence, isSensitiveData: false);

            return residenceDetail;
        }

        /// <summary>
        /// Creates a container representing a borrower's mailing residence. Serves as a container for the mailing address in this case.
        /// </summary>
        /// <param name="address">The street address of the residence.</param>
        /// <param name="city">The city in which the residence is located.</param>
        /// <param name="state">The state in which the residence is located.</param>
        /// <param name="zip">The ZIP code for the residence.</param>
        /// <param name="sequence">The sequence number of the residence among the list of residences.</param>
        /// <returns>A RESIDENCE container.</returns>
        private RESIDENCE CreateResidence_Mailing(string address, string city, string state, string zip, int sequence)
        {
            var residence = new RESIDENCE();
            residence.SequenceNumber = sequence;

            residence.Address = CreateAddress(
                address,
                city,
                state,
                zip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(state),
                AddressBase.Mailing,
                1);

            return residence;
        }

        /// <summary>
        /// Creates the container for the present housing expenses - xpath //DEAL/PARTIES/PARTY/ROLES/ROLE[1]/BORROWER/HOUSING_EXPENSES .  
        /// </summary>
        /// <returns>A HOUSING_EXPENSES container containing only the present housing expenses.</returns>
        private HOUSING_EXPENSES CreateHousingExpenses()
        {
            if (this.App.BorrowerModeT == E_BorrowerModeT.Coborrower)
            {
                return null;
            }

            var housingExpenses = new HOUSING_EXPENSES();
            housingExpenses.HousingExpenseList.Add(
                CreateHousingExpense(
                    this.App.aPres1stM_rep,
                    HousingExpenseTimingBase.Present,
                    HousingExpenseBase.FirstMortgagePrincipalAndInterest,
                    GetNextSequenceNumber(housingExpenses.HousingExpenseList)));

            housingExpenses.HousingExpenseList.Add(
                CreateHousingExpense(
                    this.App.aPresHazIns_rep,
                    HousingExpenseTimingBase.Present,
                    HousingExpenseBase.HomeownersInsurance,
                    GetNextSequenceNumber(housingExpenses.HousingExpenseList)));

            housingExpenses.HousingExpenseList.Add(
                CreateHousingExpense(
                    this.App.aPresHoAssocDues_rep,
                    HousingExpenseTimingBase.Present,
                    HousingExpenseBase.HomeownersAssociationDuesAndCondominiumFees,
                    GetNextSequenceNumber(housingExpenses.HousingExpenseList)));

            housingExpenses.HousingExpenseList.Add(
                CreateHousingExpense(
                    this.App.aPresMIns_rep,
                    HousingExpenseTimingBase.Present,
                    HousingExpenseBase.MIPremium,
                    GetNextSequenceNumber(housingExpenses.HousingExpenseList)));

            housingExpenses.HousingExpenseList.Add(
                CreateHousingExpense(
                    this.App.aPresOHExp_rep,
                    HousingExpenseTimingBase.Present,
                    HousingExpenseBase.Other,
                    GetNextSequenceNumber(housingExpenses.HousingExpenseList)));

            housingExpenses.HousingExpenseList.Add(
                CreateHousingExpense(
                    this.App.aPresOFin_rep,
                    HousingExpenseTimingBase.Present,
                    HousingExpenseBase.OtherMortgageLoanPrincipalAndInterest,
                    GetNextSequenceNumber(housingExpenses.HousingExpenseList)));

            housingExpenses.HousingExpenseList.Add(
                CreateHousingExpense(
                    this.App.aPresRealETx_rep,
                    HousingExpenseTimingBase.Present,
                    HousingExpenseBase.RealEstateTax,
                    GetNextSequenceNumber(housingExpenses.HousingExpenseList)));

            housingExpenses.HousingExpenseList.Add(
                CreateHousingExpense(
                    this.App.aPresRent_rep,
                    HousingExpenseTimingBase.Present,
                    HousingExpenseBase.Rent,
                    GetNextSequenceNumber(housingExpenses.HousingExpenseList)));

            return housingExpenses;
        }

        /// <summary>
        /// Creates a a container summarizing a borrower's assets and liabilities.
        /// </summary>
        /// <returns>A SUMMARIES container.</returns>
        private SUMMARIES CreateSummaries()
        {
            var summaries = new SUMMARIES();

            summaries.SummaryList.Add(this.CreateSummary(this.App.aAsstLiqTot_rep, SummaryAmountBase.SubtotalLiquidAssetsNotIncludingGift, GetNextSequenceNumber(summaries.SummaryList)));
            summaries.SummaryList.Add(this.CreateSummary(this.App.aAsstNonReSolidTot_rep, SummaryAmountBase.SubtotalNonLiquidAssets, GetNextSequenceNumber(summaries.SummaryList)));
            summaries.SummaryList.Add(this.CreateSummary(this.App.aLiaMonTot_rep, SummaryAmountBase.SubtotalLiabilitiesMonthlyPayment, GetNextSequenceNumber(summaries.SummaryList)));
            summaries.SummaryList.Add(this.CreateSummary(this.App.aLiaBalTot_rep, SummaryAmountBase.TotalLiabilitiesBalance, GetNextSequenceNumber(summaries.SummaryList)));

            return summaries;
        }

        /// <summary>
        /// Creates a container with data on one of a borrower's assets or liabilities.
        /// </summary>
        /// <param name="amount">The amount of the asset or liability.</param>
        /// <param name="type">The type of the asset or liability.</param>
        /// <param name="sequence">The sequence number of the summary among the list of summaries.</param>
        /// <returns>A SUMMARY container.</returns>
        private SUMMARY CreateSummary(string amount, SummaryAmountBase type, int sequence)
        {
            var summary = new SUMMARY();
            summary.SummaryAmount = ToMismoAmount(amount);
            summary.SummaryAmountType = ToMismoEnum(type, false);
            summary.SequenceNumber = sequence;

            return summary;
        }

        /// <summary>
        /// Creates a MILITARY_SERVICES container holding data on a borrower's periods of military service.
        /// </summary>
        /// <returns>A MILITARY_SERVICES container.</returns>
        private MILITARY_SERVICES CreateMilitaryServices()
        {
            var militaryServices = new MILITARY_SERVICES();

            militaryServices.MilitaryServiceList.Add(
                this.CreateMilitaryService(
                    this.App.aVaServ1BranchNum,
                    this.App.aVaServ1Num,
                    this.App.aVaServ1StartD_rep,
                    this.App.aVaServ1EndD_rep,
                    string.IsNullOrWhiteSpace(this.App.aVaServedAltName) ? this.App.aNm : this.App.aVaServedAltName,
                    this.App.aActiveMilitaryStatTri,
                    isActiveServiceLine: true));

            militaryServices.MilitaryServiceList.Add(
                this.CreateMilitaryService(
                    this.App.aVaServ2BranchNum,
                    this.App.aVaServ2Num,
                    this.App.aVaServ2StartD_rep,
                    this.App.aVaServ2EndD_rep,
                    string.IsNullOrWhiteSpace(this.App.aVaServedAltName) ? this.App.aNm : this.App.aVaServedAltName,
                    this.App.aActiveMilitaryStatTri,
                    isActiveServiceLine: true));

            militaryServices.MilitaryServiceList.Add(
                this.CreateMilitaryService(
                    this.App.aVaServ3BranchNum,
                    this.App.aVaServ3Num,
                    this.App.aVaServ3StartD_rep,
                    this.App.aVaServ3EndD_rep,
                    string.IsNullOrWhiteSpace(this.App.aVaServedAltName) ? this.App.aNm : this.App.aVaServedAltName,
                    this.App.aActiveMilitaryStatTri,
                    isActiveServiceLine: true));

            militaryServices.MilitaryServiceList.Add(
                this.CreateMilitaryService(
                    this.App.aVaServ4BranchNum,
                    this.App.aVaServ4Num,
                    this.App.aVaServ4StartD_rep,
                    this.App.aVaServ4EndD_rep,
                    string.IsNullOrWhiteSpace(this.App.aVaServedAltName) ? this.App.aNm : this.App.aVaServedAltName,
                    this.App.aActiveMilitaryStatTri,
                    isActiveServiceLine: false));

            militaryServices.MilitaryServiceList.Add(
                this.CreateMilitaryService(
                    this.App.aVaServ5BranchNum,
                    this.App.aVaServ5Num,
                    this.App.aVaServ5StartD_rep,
                    this.App.aVaServ5EndD_rep,
                    string.IsNullOrWhiteSpace(this.App.aVaServedAltName) ? this.App.aNm : this.App.aVaServedAltName,
                    this.App.aActiveMilitaryStatTri,
                    isActiveServiceLine: false));

            militaryServices.MilitaryServiceList.Add(
                this.CreateMilitaryService(
                    this.App.aVaServ6BranchNum,
                    this.App.aVaServ6Num,
                    this.App.aVaServ6StartD_rep,
                    this.App.aVaServ6EndD_rep,
                    string.IsNullOrWhiteSpace(this.App.aVaServedAltName) ? this.App.aNm : this.App.aVaServedAltName,
                    this.App.aActiveMilitaryStatTri,
                    isActiveServiceLine: false));

            militaryServices.MilitaryServiceList.Add(
                this.CreateMilitaryService(
                    this.App.aVaServ7BranchNum,
                    this.App.aVaServ7Num,
                    this.App.aVaServ7StartD_rep,
                    this.App.aVaServ7EndD_rep,
                    string.IsNullOrWhiteSpace(this.App.aVaServedAltName) ? this.App.aNm : this.App.aVaServedAltName,
                    this.App.aActiveMilitaryStatTri,
                    isActiveServiceLine: false));

            militaryServices.MilitaryServiceList.Add(this.CreateMilitaryService(this.App.aVaServiceBranchT, this.App.aVaMilitaryStatT));

            return militaryServices;
        }

        /// <summary>
        /// Creates a MILITARY_SERVICE container holding data on one period of a borrower's service.
        /// </summary>
        /// <param name="branchNumber">The branch the borrower served in.</param>
        /// <param name="identifier">The borrower's unique identifier in the service.</param>
        /// <param name="startDate">The borrower's service start date.</param>
        /// <param name="endDate">The borrower's service end date.</param>
        /// <param name="name">The name the borrower served under.</param>
        /// <param name="onActiveDuty">Indicates whether the borrower is currently on active duty.</param>
        /// <param name="isActiveServiceLine">Indicates whether the data is from an active service or military reserve line.</param>
        /// <returns>A MILITARY_SERVICE container.</returns>
        private MILITARY_SERVICE CreateMilitaryService(
            string branchNumber,
            string identifier,
            string startDate,
            string endDate,
            string name,
            E_TriState onActiveDuty,
            bool isActiveServiceLine)
        {
            if (string.IsNullOrWhiteSpace(branchNumber) && string.IsNullOrWhiteSpace(identifier) &&
                string.IsNullOrWhiteSpace(startDate) && string.IsNullOrWhiteSpace(endDate))
            {
                return null;
            }

            var militaryService = new MILITARY_SERVICE();
            militaryService.MilitaryBranchType = ToMismoEnum(GetMilitaryBranchBaseValue(branchNumber), isSensitiveData: false);
            militaryService.MilitaryServiceNumberIdentifier = ToMismoIdentifier(identifier);
            militaryService.MilitaryServiceFromDate = ToMismoDate(startDate);
            militaryService.MilitaryServiceToDate = ToMismoDate(endDate);
            militaryService.MilitaryServiceServedAsName = ToMismoString(name);
            militaryService.MilitaryStatusType = ToMismoEnum(GetMilitaryStatusBaseValue(onActiveDuty, startDate, endDate, isActiveServiceLine), isSensitiveData: false);

            return militaryService;
        }

        /// <summary>
        /// Creates a MILITARY_SERVICE container using service branch type and military status type.
        /// </summary>
        /// <param name="serviceBranchType">The service branch type.</param>
        /// <param name="militaryStatusType">The military status type.</param>
        /// <returns>The MILITARY_SERVICE container.</returns>
        private MILITARY_SERVICE CreateMilitaryService(E_aVaServiceBranchT serviceBranchType, E_aVaMilitaryStatT militaryStatusType)
        {
            var militaryService = new MILITARY_SERVICE();
            militaryService.MilitaryBranchType = ToMismoEnum(GetMilitaryBranchBase(serviceBranchType), isSensitiveData: false);
            militaryService.MilitaryStatusType = ToMismoEnum(GetMilitaryStatusBase(militaryStatusType), isSensitiveData: false);

            return militaryService;
        }

        /// <summary>
        /// Creates a NEAREST_LIVING_RELATIVE element.
        /// </summary>
        /// <param name="usePrimaryBorrowerInformation">True if borrower information should be used.
        /// False if coborrower information should be used.</param>
        /// <returns>A NEAREST_LIVING_RELATIVE element.</returns>
        private NEAREST_LIVING_RELATIVE CreateNearestLivingRelative(bool usePrimaryBorrowerInformation)
        {
            var nearestLivingRelative = new NEAREST_LIVING_RELATIVE();

            if (usePrimaryBorrowerInformation)
            {
                nearestLivingRelative.Name = CreateName(this.DataLoan.sFHAPropImprovBorrRelativeNm, isLegalEntity: false);
                nearestLivingRelative.NearestLivingRelativeDetail = this.CreateNearestLivingRelativeDetail(usePrimaryBorrowerInformation);

                nearestLivingRelative.ContactPoints = CreateContactPoints(
                    this.DataLoan.sFHAPropImprovBorrRelativePhone,
                    string.Empty,
                    string.Empty,
                    ContactPointRoleBase.Home);

                if (!string.IsNullOrEmpty(this.DataLoan.sFHAPropImprovBorrRelativeStreetAddress))
                {
                    nearestLivingRelative.Address = CreateAddress(
                        this.DataLoan.sFHAPropImprovBorrRelativeStreetAddress,
                        this.DataLoan.sFHAPropImprovBorrRelativeCity,
                        this.DataLoan.sFHAPropImprovBorrRelativeState,
                        this.DataLoan.sFHAPropImprovBorrRelativeZip,
                        string.Empty,
                        string.Empty,
                        this.ExportData.StatesAndTerritories.GetStateName(this.DataLoan.sFHAPropImprovBorrRelativeState),
                        AddressBase.Mailing,
                        sequenceNumber: 1);
                }
            }
            else
            {
                nearestLivingRelative.Name = CreateName(this.DataLoan.sFHAPropImprovCoborRelativeNm, isLegalEntity: false);
                nearestLivingRelative.NearestLivingRelativeDetail = this.CreateNearestLivingRelativeDetail(usePrimaryBorrowerInformation);

                nearestLivingRelative.ContactPoints = CreateContactPoints(
                    this.DataLoan.sFHAPropImprovCoborRelativePhone,
                    string.Empty,
                    string.Empty,
                    ContactPointRoleBase.Home);

                if (!string.IsNullOrEmpty(this.DataLoan.sFHAPropImprovCoborRelativeAddr))
                {
                    nearestLivingRelative.Address = CreateAddress(
                        this.DataLoan.sFHAPropImprovCoborRelativeStreetAddress,
                        this.DataLoan.sFHAPropImprovCoborRelativeCity,
                        this.DataLoan.sFHAPropImprovCoborRelativeState,
                        this.DataLoan.sFHAPropImprovCoborRelativeZip,
                        string.Empty,
                        string.Empty,
                        this.ExportData.StatesAndTerritories.GetStateName(this.DataLoan.sFHAPropImprovCoborRelativeState),
                        AddressBase.Mailing,
                        sequenceNumber: 1);
                }
            }

            return nearestLivingRelative;
        }

        /// <summary>
        /// Creates a NEAREST_LIVING_RELATIVE_DETAIL element.
        /// </summary>
        /// <param name="usePrimaryBorrowerInformation">True if borrower information should be used.
        /// False if coborrower information should be used.</param>
        /// <returns>A NEAREST_LIVING_RELATIVE_DETAIL element.</returns>
        private NEAREST_LIVING_RELATIVE_DETAIL CreateNearestLivingRelativeDetail(bool usePrimaryBorrowerInformation)
        {
            var nearestLivingRelativeDetail = new NEAREST_LIVING_RELATIVE_DETAIL();

            if (usePrimaryBorrowerInformation)
            {
                nearestLivingRelativeDetail.BorrowerNearestLivingRelativeRelationshipDescription = ToMismoString(this.DataLoan.sFHAPropImprovBorrRelativeRelationship);
            }
            else
            {
                nearestLivingRelativeDetail.BorrowerNearestLivingRelativeRelationshipDescription = ToMismoString(this.DataLoan.sFHAPropImprovCoborRelativeRelationship);
            }

            return nearestLivingRelativeDetail;
        }

        /// <summary>
        /// Creates an extension to the PARTY element.
        /// </summary>
        /// <returns>An extension container.</returns>
        private PARTY_EXTENSION CreatePartyExtension()
        {
            var extension = new PARTY_EXTENSION();
            extension.Other = this.CreateLqbPartyExtension();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container for the PARTY element.
        /// </summary>
        /// <returns>A proprietary extension container.</returns>
        private LQB_PARTY_EXTENSION CreateLqbPartyExtension()
        {
            var extension = new LQB_PARTY_EXTENSION();
            extension.ConsumerId = ToMismoIdentifier(this.App.aConsumerId_rep);

            return extension;
        }

        /// <summary>
        /// Creates a container to hold all demographic data on a borrower.
        /// </summary>
        /// <returns>A GOVERNMENT_MONITORING container.</returns>
        private GOVERNMENT_MONITORING CreateGovernmentMonitoring()
        {
            var governmentMonitoring = new GOVERNMENT_MONITORING();
            governmentMonitoring.GovernmentMonitoringDetail = this.CreateGovernmentMonitoringDetail();
            governmentMonitoring.HmdaRaces = this.CreateHmdaRaces();
            governmentMonitoring.HmdaEthnicityOrigins = this.CreateHmdaEthnicityOrigins();
            governmentMonitoring.Extension = this.CreateGovernmentMonitoringExtension();

            return governmentMonitoring;
        }

        /// <summary>
        /// Creates a container holding non-race demographic data on a borrower.
        /// </summary>
        /// <returns>A GOVERNMENT_MONITORING_DETAIL container.</returns>
        private GOVERNMENT_MONITORING_DETAIL CreateGovernmentMonitoringDetail()
        {
            var governmentMonitoringDetail = new GOVERNMENT_MONITORING_DETAIL();
            governmentMonitoringDetail.HMDAEthnicityType = this.App.aBHispanicTFallback == E_aHispanicT.LeaveBlank ? null : ToMismoEnum(GetHmdaEthnicityBaseValue(this.App.aBHispanicTFallback));
            governmentMonitoringDetail.RaceNationalOriginRefusalIndicator = ToMismoIndicator(this.App.aNoFurnish);
            governmentMonitoringDetail.GenderType = this.App.aGenderFallback == E_GenderT.LeaveBlank ? null : ToMismoEnum(GetGenderBaseValue(this.App.aGenderFallback), isSensitiveData: false);
            governmentMonitoringDetail.HMDAGenderRefusalIndicator = ToMismoIndicator(GetHmdaGenderRefusalIndicator(this.App.aGender), isSensitiveData: false);
            governmentMonitoringDetail.HMDAGenderCollectedBasedOnVisualObservationOrNameIndicator = ToMismoIndicator(this.App.aSexCollectedByObservationOrSurname);
            governmentMonitoringDetail.HMDAEthnicityRefusalIndicator = ToMismoIndicator(this.App.aDoesNotWishToProvideEthnicity);
            governmentMonitoringDetail.HMDAEthnicityCollectedBasedOnVisualObservationOrSurnameIndicator = ToMismoIndicator(this.App.aEthnicityCollectedByObservationOrSurname);
            governmentMonitoringDetail.HMDARaceRefusalIndicator = ToMismoIndicator(this.App.aDoesNotWishToProvideRace);
            governmentMonitoringDetail.HMDARaceCollectedBasedOnVisualObservationOrSurnameIndicator = ToMismoIndicator(this.App.aRaceCollectedByObservationOrSurname);

            return governmentMonitoringDetail;
        }

        /// <summary>
        /// Creates an extension container holding data on a borrower's races.
        /// </summary>
        /// <returns>An HMDA_RACES container.</returns>
        private HMDA_RACES CreateHmdaRaces()
        {
            var hmdaRaces = new HMDA_RACES();
            List<HMDARaceBase> races = GetHmdaRaceBaseValues(this.App);

            foreach (var race in races)
            {
                if (race == HMDARaceBase.Asian
                    || race == HMDARaceBase.NativeHawaiianOrOtherPacificIslander
                    || race == HMDARaceBase.AmericanIndianOrAlaskaNative)
                {
                    // These are handled separately.
                    continue;
                }

                hmdaRaces.HmdaRaceList.Add(this.CreateHmdaRace(race, includeRaceType: true, sequenceNumber: GetNextSequenceNumber(hmdaRaces.HmdaRaceList)));
            }

            bool hasAsianRace = races.Contains(HMDARaceBase.Asian);
            var asianDesignations = GetHmdaRaceDesignationBaseValues_Asian(this.App);
            if (hasAsianRace || asianDesignations.Any() || !string.IsNullOrEmpty(this.App.aOtherAsianDescription))
            {
                hmdaRaces.HmdaRaceList.Add(this.CreateHmdaRace(HMDARaceBase.Asian, hasAsianRace, GetNextSequenceNumber(hmdaRaces.HmdaRaceList), asianDesignations, this.App.aOtherAsianDescription));
            }

            bool hasPacificIslanderRace = races.Contains(HMDARaceBase.NativeHawaiianOrOtherPacificIslander);
            var pacificIslanderDesignations = GetHmdaRaceDesignationBaseValues_PacificIslander(this.App);
            if (hasPacificIslanderRace || pacificIslanderDesignations.Any() || !string.IsNullOrEmpty(this.App.aOtherPacificIslanderDescription))
            {
                hmdaRaces.HmdaRaceList.Add(this.CreateHmdaRace(HMDARaceBase.NativeHawaiianOrOtherPacificIslander, hasPacificIslanderRace, GetNextSequenceNumber(hmdaRaces.HmdaRaceList), pacificIslanderDesignations, this.App.aOtherPacificIslanderDescription));
            }

            bool hasNativeAmericanRace = races.Contains(HMDARaceBase.AmericanIndianOrAlaskaNative);
            bool hasNativeAmericanDescription = !string.IsNullOrEmpty(this.App.aOtherAmericanIndianDescription);
            if (hasNativeAmericanRace || hasNativeAmericanDescription)
            {
                hmdaRaces.HmdaRaceList.Add(this.CreateHmdaRace(HMDARaceBase.AmericanIndianOrAlaskaNative, hasNativeAmericanRace, GetNextSequenceNumber(hmdaRaces.HmdaRaceList)));
            }

            return hmdaRaces;
        }

        /// <summary>
        /// Creates a container holding information on a borrower race.
        /// </summary>
        /// <param name="race">The race to populate.</param>
        /// <param name="includeRaceType">Indicates whether to include the HMDA_RACE_DETAIL element.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="designations">Designations for the race, if applicable.</param>
        /// <param name="designationOtherDescription">The catchall description for any supplementary race designations.</param>
        /// <returns>An HMDA_RACE container.</returns>
        private HMDA_RACE CreateHmdaRace(HMDARaceBase race, bool includeRaceType, int sequenceNumber, List<LqbHMDARaceDesignationBase> designations = null, string designationOtherDescription = null)
        {
            var hmdaRace = new HMDA_RACE();
            hmdaRace.SequenceNumber = sequenceNumber;
            if (includeRaceType || race == HMDARaceBase.AmericanIndianOrAlaskaNative)
            {
                hmdaRace.HmdaRaceDetail = this.CreateHmdaRaceDetail(race, includeRaceType);
            }

            if ((designations != null && designations.Any()) || !string.IsNullOrEmpty(designationOtherDescription))
            {
                hmdaRace.HmdaRaceDesignations = this.CreateHmdaRaceDesignations(race, designations, designationOtherDescription);
            }

            return hmdaRace;
        }

        /// <summary>
        /// Creates a container with details on a borrower's race.
        /// </summary>
        /// <param name="race">The borrower's race.</param>
        /// <param name="includeRaceType">Indicates whether to include the race type datapoint.</param>
        /// <returns>An HMDA_RACE_DETAIL container.</returns>
        private HMDA_RACE_DETAIL CreateHmdaRaceDetail(HMDARaceBase race, bool includeRaceType)
        {
            var hmdaRaceDetail = new HMDA_RACE_DETAIL();

            if (includeRaceType)
            {
                hmdaRaceDetail.HMDARaceType = ToMismoEnum(race);
            }

            if (race == HMDARaceBase.AmericanIndianOrAlaskaNative && !string.IsNullOrEmpty(this.App.aOtherAmericanIndianDescription))
            {
                hmdaRaceDetail.HMDARaceTypeAdditionalDescription = ToMismoString(this.App.aOtherAmericanIndianDescription);
            }

            return hmdaRaceDetail;
        }

        /// <summary>
        /// Creates a container containing a borrower's race designations.
        /// </summary>
        /// <param name="race">The race to populate.</param>
        /// <param name="designationCollection">Designations for the race.</param>
        /// <param name="designationOtherDescription">The catchall description for any supplementary race designations.</param>
        /// <returns>An HMDA_RACE_DESIGNATIONS container.</returns>
        private HMDA_RACE_DESIGNATIONS CreateHmdaRaceDesignations(HMDARaceBase race, List<LqbHMDARaceDesignationBase> designationCollection, string designationOtherDescription)
        {
            var hmdaRaceDesignations = new HMDA_RACE_DESIGNATIONS();

            // Special handling for the race designation other descriptions, we should send them
            // regardless of whether there's an Other value to pair with.
            if (!string.IsNullOrEmpty(designationOtherDescription))
            {
                LqbHMDARaceDesignationBase? designation = null;

                if (designationCollection.Contains(LqbHMDARaceDesignationBase.OtherAsian))
                {
                    designation = LqbHMDARaceDesignationBase.OtherAsian;
                    designationCollection.Remove(designation.Value);
                }

                if (designationCollection.Contains(LqbHMDARaceDesignationBase.OtherPacificIslander))
                {
                    designation = LqbHMDARaceDesignationBase.OtherPacificIslander;
                    designationCollection.Remove(designation.Value);
                }

                hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(race, designation, GetNextSequenceNumber(hmdaRaceDesignations.HmdaRaceDesignationList), designationOtherDescription));
            }

            foreach (var designation in designationCollection)
            {
                hmdaRaceDesignations.HmdaRaceDesignationList.Add(this.CreateHmdaRaceDesignation(race, designation, GetNextSequenceNumber(hmdaRaceDesignations.HmdaRaceDesignationList)));
            }

            return hmdaRaceDesignations;
        }

        /// <summary>
        /// Creates a container with information on a race designation.
        /// </summary>
        /// <param name="race">The borrower's race.</param>
        /// <param name="designation">The race designation.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="otherDescription">An other description, if applicable.</param>
        /// <returns>An HMDA_RACE_DESIGNATION container.</returns>
        private HMDA_RACE_DESIGNATION CreateHmdaRaceDesignation(HMDARaceBase race, LqbHMDARaceDesignationBase? designation, int sequenceNumber, string otherDescription = null)
        {
            var hmdaRaceDesignation = new HMDA_RACE_DESIGNATION();
            hmdaRaceDesignation.SequenceNumber = sequenceNumber;
            if (designation.HasValue)
            {
                hmdaRaceDesignation.HMDARaceDesignationType = ToMismoEnum(GetHmdaRaceDesignationBase(designation.Value));
            }

            if (!string.IsNullOrEmpty(otherDescription) && (race == HMDARaceBase.Asian || race == HMDARaceBase.NativeHawaiianOrOtherPacificIslander))
            {
                hmdaRaceDesignation.HMDARaceDesignationTypeOtherDescription = ToMismoString(otherDescription);
            }

            hmdaRaceDesignation.Extension = this.CreateHmdaRaceDesignationExtension(race, designation, otherDescription);

            return hmdaRaceDesignation;
        }

        /// <summary>
        /// Creates a HMDA_RACE_DESIGNATION_EXTENSION container.
        /// </summary>
        /// <param name="race">The borrower's race.</param>
        /// <param name="designation">The race designation.</param>
        /// <param name="otherDescription">An other description, if applicable.</param>
        /// <returns>The HMDA_RACE_DESIGNATION_EXTENSION container.</returns>
        private HMDA_RACE_DESIGNATION_EXTENSION CreateHmdaRaceDesignationExtension(HMDARaceBase race, LqbHMDARaceDesignationBase? designation, string otherDescription = null)
        {
            HMDA_RACE_DESIGNATION_EXTENSION extension = new HMDA_RACE_DESIGNATION_EXTENSION();
            extension.Other = this.CreateLqbHmdaRaceDesignationExtension(race, designation, otherDescription);

            return extension;
        }

        /// <summary>
        /// The LQB extension for HMDA_RACE_DESIGNATION.
        /// </summary>
        /// <param name="race">The borrower's race.</param>
        /// <param name="designation">The race designation.</param>
        /// <param name="otherDescription">An other description, if applicable.</param>
        /// <returns>The LQB_HMDA_RACE_DESIGNATION_EXTENSION.</returns>
        private LQB_HMDA_RACE_DESIGNATION_EXTENSION CreateLqbHmdaRaceDesignationExtension(HMDARaceBase race, LqbHMDARaceDesignationBase? designation, string otherDescription = null)
        {
            var extension = new LQB_HMDA_RACE_DESIGNATION_EXTENSION();
            if (designation.HasValue && (designation.Value == LqbHMDARaceDesignationBase.OtherAsian || designation.Value == LqbHMDARaceDesignationBase.OtherPacificIslander))
            {
                extension.HMDARaceDesignationType = ToMismoEnum(designation.Value);
            }

            if (!string.IsNullOrEmpty(otherDescription))
            {
                if (race == HMDARaceBase.Asian)
                {
                    extension.HMDARaceDesignationTypeOtherAsianDescription = ToMismoString(otherDescription);
                }
                else if (race == HMDARaceBase.NativeHawaiianOrOtherPacificIslander)
                {
                    extension.HMDARaceDesignationTypeOtherPacificIslanderDescription = ToMismoString(otherDescription);
                }
            }

            return extension;
        }

        /// <summary>
        /// Creates a container holding a borrower's ethnicity origins.
        /// </summary>
        /// <returns>An HMDA_ETHNICITY_ORIGINS container.</returns>
        private HMDA_ETHNICITY_ORIGINS CreateHmdaEthnicityOrigins()
        {
            var hmdaEthnicityOrigins = new HMDA_ETHNICITY_ORIGINS();
            List<HMDAEthnicityOriginBase> originCollection = GetHmdaEthnicityOriginBaseValues(this.App);

            if (!originCollection.Any() && string.IsNullOrEmpty(this.App.aOtherHispanicOrLatinoDescription))
            {
                return null;
            }

            // Special handling for the other description, we should send it regardless of
            // whether the set of ethnicity origins contains Other.
            HMDAEthnicityOriginBase? otherOrigin = null;

            if (originCollection.Contains(HMDAEthnicityOriginBase.Other))
            {
                otherOrigin = HMDAEthnicityOriginBase.Other;
                originCollection.Remove(otherOrigin.Value);
            }

            hmdaEthnicityOrigins.HmdaEthnicityOriginList.Add(
                this.CreateLqbHmdaEthnicityOrigin(otherOrigin, GetNextSequenceNumber(hmdaEthnicityOrigins.HmdaEthnicityOriginList), this.App.aOtherHispanicOrLatinoDescription));

            foreach (var origin in originCollection)
            {
                hmdaEthnicityOrigins.HmdaEthnicityOriginList.Add(this.CreateLqbHmdaEthnicityOrigin(origin, GetNextSequenceNumber(hmdaEthnicityOrigins.HmdaEthnicityOriginList)));
            }

            return hmdaEthnicityOrigins;
        }

        /// <summary>
        /// Creates a container holding a borrower's ethnicity origin.
        /// </summary>
        /// <param name="origin">The ethnicity origin.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <param name="otherDescription">An other description, if applicable.</param>
        /// <returns>An LQB_HMDA_ETHNICITY_ORIGIN container.</returns>
        private HMDA_ETHNICITY_ORIGIN CreateLqbHmdaEthnicityOrigin(HMDAEthnicityOriginBase? origin, int sequenceNumber, string otherDescription = null)
        {
            var hmdaEthnicityOrigin = new HMDA_ETHNICITY_ORIGIN();
            hmdaEthnicityOrigin.SequenceNumber = sequenceNumber;

            if (origin.HasValue)
            {
                hmdaEthnicityOrigin.HMDAEthnicityOriginType = ToMismoEnum(origin.Value);
            }

            if (!string.IsNullOrEmpty(otherDescription))
            {
                hmdaEthnicityOrigin.HMDAEthnicityOriginTypeOtherDescription = ToMismoString(otherDescription);
            }

            return hmdaEthnicityOrigin;
        }

        /// <summary>
        /// Creates a container holding extended government monitoring information.
        /// </summary>
        /// <returns>A GOVERNMENT_MONITORING_EXTENSION container.</returns>
        private GOVERNMENT_MONITORING_EXTENSION CreateGovernmentMonitoringExtension()
        {
            var governmentMonitoringExtension = new GOVERNMENT_MONITORING_EXTENSION();
            governmentMonitoringExtension.Other = this.CreateLqbGovernmentMonitoringExtension();

            return governmentMonitoringExtension;
        }

        /// <summary>
        /// Creates an extension container holding HMDA information.
        /// </summary>
        /// <returns>An LQB_GOVERNMENT_MONITORING_EXTENSION container.</returns>
        private LQB_GOVERNMENT_MONITORING_EXTENSION CreateLqbGovernmentMonitoringExtension()
        {
            var lqbExtension = new LQB_GOVERNMENT_MONITORING_EXTENSION();

            var genderType = GetHmdaGenderBaseValue(this.App.aGender, this.App.aInterviewMethodT);
            lqbExtension.GenderType = genderType == HMDAGenderBase.LeaveBlank ? null : ToMismoEnum(genderType, isSensitiveData: false);
            lqbExtension.HMDAEthnicities = this.CreateLqbHmdaEthnicities();
            lqbExtension.ApplicationTakenMethodType = this.App.aInterviewMethodT == E_aIntrvwrMethodT.LeaveBlank ? null : ToMismoEnum(GetApplicationTakenMethodBaseValue(this.App.aInterviewMethodT), isSensitiveData: false);
            lqbExtension.DecisionCreditScore = this.CreateLqbDecisionCreditScore(this.App.aDecisionCreditScore_rep, this.App.aDecisionCreditSourceT, this.App.aDecisionCreditModelName);

            return lqbExtension;
        }

        /// <summary>
        /// Creates a container holding a borrower's ethnicities.
        /// </summary>
        /// <returns>An LQB_HMDA_ETHNICITIES container.</returns>
        private LQB_HMDA_ETHNICITIES CreateLqbHmdaEthnicities()
        {
            var hmdaEthnicities = new LQB_HMDA_ETHNICITIES();
            List<HMDAEthnicityBase> ethnicities = GetHmdaEthnicityBaseValues(this.App);

            if (!ethnicities.Any())
            {
                return null;
            }

            foreach (var ethnicity in ethnicities)
            {
                hmdaEthnicities.HmdaEthnicity.Add(this.CreateLqbHmdaEthnicity(ethnicity));
            }

            return hmdaEthnicities;
        }

        /// <summary>
        /// Creates a container holding a borrower's ethnicity.
        /// </summary>
        /// <param name="ethnicity">The ethnicity to populate.</param>
        /// <returns>An LQB_HMDA_ETHNICITY container.</returns>
        private LQB_HMDA_ETHNICITY CreateLqbHmdaEthnicity(HMDAEthnicityBase ethnicity)
        {
            var hmdaEthnicity = new LQB_HMDA_ETHNICITY();
            hmdaEthnicity.HMDAEthnicityType = ToMismoEnum(ethnicity);

            return hmdaEthnicity;
        }

        /// <summary>
        /// Creates a container holding a decision credit score.
        /// </summary>
        /// <param name="score">The credit score.</param>
        /// <param name="creditSource">The credit bureau.</param>
        /// <param name="modelName">The credit model name.</param>
        /// <returns>An LQB_DECISION_CREDIT_SCORE container.</returns>
        private LQB_DECISION_CREDIT_SCORE CreateLqbDecisionCreditScore(string score, E_aDecisionCreditSourceT creditSource, string modelName)
        {
            if (string.IsNullOrEmpty(score))
            {
                return null;
            }

            var decisionScore = new LQB_DECISION_CREDIT_SCORE();
            decisionScore.DecisionCreditScoreValue = ToMismoValue(score);
            decisionScore.DecisionCreditRepositorySourceType = ToMismoEnum(GetCreditRepositorySourceBaseValue(creditSource), isSensitiveData: true);
            decisionScore.DecisionCreditModelName = ToMismoString(modelName);

            return decisionScore;
        }

        ///////// mapper ////////
    }
}
