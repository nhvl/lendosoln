﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Linq;
    using DataAccess;
    using DataAccess.Core.Construction;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Exporter for MISMO 3.4 DEALS data.
    /// </summary>
    public class DealBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DealBuilder"/> class.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="exportData">A collection of cached data to populate the export.</param>
        public DealBuilder(CPageData dataLoan, Mismo34ExporterCache exportData)
        {
            this.DataLoan = dataLoan;
            this.PrimaryApp = Mismo34ExporterCache.GetPrimaryApp(dataLoan);
            this.ExportData = exportData;
        }

        /// <summary>
        /// Gets the data loan.
        /// </summary>
        protected CPageData DataLoan { get; private set; }

        /// <summary>
        /// Gets the primary application on the loan.
        /// </summary>
        protected CAppData PrimaryApp { get; private set; }

        /// <summary>
        /// Gets a collection of cached data to populate the export.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Creates a <see cref="DEAL"/> container.
        /// </summary>
        /// <param name="sequenceNumber">A number uniquely identifying this element from others of its type.</param>
        /// <returns>A <see cref="DEAL"/> container.</returns>
        public DEAL CreateContainer(int sequenceNumber)
        {
            var deal = new DEAL();
            deal.SequenceNumber = sequenceNumber;

            // Do this first to ensure that other containers can create relationships to the subject loan.
            deal.Loans = this.CreateLoans();

            deal.Assets = this.CreateAssets();
            deal.Collaterals = this.CreateCollaterals();
            deal.Expenses = this.CreateExpenses();
            deal.Liabilities = this.CreateLiabilities();

            var partyExporter = new PartiesBuilder(this.DataLoan, this.ExportData);
            deal.Parties = partyExporter.CreateContainer();

            // This must be the final call to ensure all relationships are populated before the set is serialized.
            deal.Relationships = this.CreateRelationships();

            return deal;
        }

        /// <summary>
        /// Creates a container to hold all instances of ASSET.
        /// </summary>
        /// <returns>An ASSETS container.</returns>
        private ASSETS CreateAssets()
        {
            var assets = new ASSETS();

            int appsCount = this.DataLoan.nApps;
            for (int appIndex = 0; appIndex < appsCount; appIndex++)
            {
                CAppData appData = this.DataLoan.GetAppData(appIndex);

                IAssetCollection assetCollection = appData.aAssetCollection;

                var retirement = assetCollection.GetRetirement(false);
                if (retirement != null && retirement.Val > 0)
                {
                    assets.AssetList.Add(this.CreateAsset(retirement.Val_rep, AssetBase.RetirementFund, null, null, appData.aBMismoId, GetNextSequenceNumber(assets.AssetList)));
                }

                var business = assetCollection.GetBusinessWorth(false);
                if (business != null && business.Val > 0)
                {
                    assets.AssetList.Add(this.CreateAsset(business.Val_rep, AssetBase.NetWorthOfBusinessOwned, null, null, appData.aBMismoId, GetNextSequenceNumber(assets.AssetList)));
                }

                var lifeInsurance = assetCollection.GetLifeInsurance(false);
                if (lifeInsurance != null && lifeInsurance.Val > 0)
                {
                    assets.AssetList.Add(this.CreateAsset(lifeInsurance.Val_rep, AssetBase.LifeInsurance, lifeInsurance.FaceVal_rep, null, appData.aBMismoId, GetNextSequenceNumber(assets.AssetList)));
                }

                var deposit1 = assetCollection.GetCashDeposit1(false);
                if (deposit1 != null && deposit1.Val > 0)
                {
                    assets.AssetList.Add(this.CreateAsset(deposit1.Val_rep, AssetBase.EarnestMoneyCashDepositTowardPurchase, null, deposit1.Desc, appData.aBMismoId, GetNextSequenceNumber(assets.AssetList)));
                }

                var deposit2 = assetCollection.GetCashDeposit2(false);
                if (deposit2 != null && deposit2.Val > 0)
                {
                    assets.AssetList.Add(this.CreateAsset(deposit2.Val_rep, AssetBase.EarnestMoneyCashDepositTowardPurchase, null, deposit2.Desc, appData.aBMismoId, GetNextSequenceNumber(assets.AssetList)));
                }

                for (int i = 0; i < assetCollection.CountRegular; i++)
                {
                    assets.AssetList.Add(this.CreateAsset(assetCollection.GetRegularRecordAt(i), appData.aBMismoId, appData.aCMismoId, appData.aCIsValidNameSsn, GetNextSequenceNumber(assets.AssetList)));
                }

                var ownedPropertyCollection = appData.aReCollection;
                for (int i = 0; i < ownedPropertyCollection.CountRegular; i++)
                {
                    assets.AssetList.Add(this.CreateAsset(ownedPropertyCollection.GetRegularRecordAt(i), appData, GetNextSequenceNumber(assets.AssetList)));
                }
            }

            return assets;
        }

        /// <summary>
        /// Creates a container representing a specific asset.
        /// </summary>
        /// <param name="marketValue">The market or cash value of the asset.</param>
        /// <param name="type">The type of asset.</param>
        /// <param name="faceValue">The face value of the asset.</param>
        /// <param name="holder">The name of the asset holder.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>An ASSET container.</returns>
        private ASSET CreateAsset(string marketValue, AssetBase type, string faceValue, string holder, string borrowerLabel, int sequenceNumber)
        {
            var asset = new ASSET();
            asset.AssetDetail = this.CreateAssetDetail(marketValue, type, faceValue);
            asset.AssetHolder = this.CreateAssetHolder(holder, null, null, null, null, isLegalEntity: true);
            asset.SequenceNumber = sequenceNumber;
            asset.XlinkLabel = "Asset" + asset.SequenceNumberStringified;

            this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.XlinkLabel, borrowerLabel);

            return asset;
        }

        /// <summary>
        /// Creates a container representing a regular asset.
        /// </summary>
        /// <param name="regularAsset">An object holding information on the asset.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <param name="coborrowerLabel">The $$xlink$$ label for the co-borrower (role).</param>
        /// <param name="coborrowerIsValidSSN">Co borrower valid SSN indicator from app data. Used for determining whether to add RELATIONSHIP.</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>An ASSET container.</returns>
        private ASSET CreateAsset(IAssetRegular regularAsset, string borrowerLabel, string coborrowerLabel, bool coborrowerIsValidSSN, int sequenceNumber)
        {
            var asset = new ASSET();
            asset.AssetHolder = this.CreateAssetHolder(regularAsset.ComNm, regularAsset.StAddr, regularAsset.City, regularAsset.State, regularAsset.Zip, isLegalEntity: true);
            asset.AssetDetail = this.CreateAssetDetail(regularAsset);
            asset.SequenceNumber = sequenceNumber;
            asset.XlinkLabel = "Asset" + asset.SequenceNumberStringified;

            if (regularAsset.OwnerT == E_AssetOwnerT.Borrower || regularAsset.OwnerT == E_AssetOwnerT.Joint)
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.XlinkLabel, borrowerLabel);
            }

            if (coborrowerIsValidSSN && (regularAsset.OwnerT == E_AssetOwnerT.CoBorrower || regularAsset.OwnerT == E_AssetOwnerT.Joint))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.XlinkLabel, coborrowerLabel);
            }

            return asset;
        }

        /// <summary>
        /// Creates a container representing a real estate owned asset.
        /// </summary>
        /// <param name="reo">The real estate owned object.</param>
        /// <param name="appData">The borrower application.</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>An ASSET container.</returns>
        private ASSET CreateAsset(IRealEstateOwned reo, CAppData appData, int sequenceNumber)
        {
            var asset = new ASSET();
            asset.AssetDetail = this.CreateAssetDetail(reo);
            asset.OwnedProperty = this.CreateOwnedProperty(reo);

            if (reo.ReOwnerT == E_ReOwnerT.Borrower || reo.ReOwnerT == E_ReOwnerT.Joint)
            {
                asset.AssetHolder = this.CreateAssetHolder(appData.aBNm, appData.aBAddr, appData.aBCity, appData.aBState, appData.aBZip, isLegalEntity: false);
            }
            else
            {
                asset.AssetHolder = this.CreateAssetHolder(appData.aCNm, appData.aCAddr, appData.aCCity, appData.aCState, appData.aCZip, isLegalEntity: false);
            }

            asset.SequenceNumber = sequenceNumber;
            asset.XlinkLabel = "Asset" + asset.SequenceNumberStringified;
            if (reo.ReOwnerT == E_ReOwnerT.Borrower || reo.ReOwnerT == E_ReOwnerT.Joint)
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.XlinkLabel, appData.aBMismoId);
            }

            if (appData.aCIsValidNameSsn && (reo.ReOwnerT == E_ReOwnerT.CoBorrower || reo.ReOwnerT == E_ReOwnerT.Joint))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "ROLE", asset.XlinkLabel, appData.aCMismoId);
            }

            this.ExportData.LabelManager.ReoAssetLabels.Add(reo.RecordId.ToString("D"), asset.XlinkLabel);

            return asset;
        }

        /// <summary>
        /// Creates a container with details on an asset.
        /// </summary>
        /// <param name="regularAsset">An object holding information on the asset.</param>
        /// <returns>An ASSET_DETAIL container.</returns>
        private ASSET_DETAIL CreateAssetDetail(IAssetRegular regularAsset)
        {
            var assetDetail = new ASSET_DETAIL();
            assetDetail.AssetAccountIdentifier = ToMismoIdentifier(regularAsset.AccNum.Value);
            assetDetail.AssetAccountInNameOfDescription = ToMismoString(regularAsset.AccNm);
            assetDetail.AssetCashOrMarketValueAmount = ToMismoAmount(regularAsset.Val_rep);
            assetDetail.AssetType = ToMismoEnum(GetAssetBaseValue(regularAsset.AssetT));
            assetDetail.FundsSourceType = ToMismoEnum(this.GetFundsSourceBaseValue(regularAsset.GiftSource));

            if (assetDetail.AssetType?.IsSetToOther ?? false)
            {
                assetDetail.AssetTypeOtherDescription = ToMismoString(regularAsset.OtherTypeDesc);
            }

            if (assetDetail.AssetType?.EnumValue == AssetBase.Automobile)
            {
                assetDetail.AutomobileMakeDescription = ToMismoString(regularAsset.Desc);
            }

            return assetDetail;
        }

        /// <summary>
        /// Gets the MISMO value corresponding to a gift fund source.
        /// </summary>
        /// <param name="source">A gift fund source.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private FundsSourceBase GetFundsSourceBaseValue(E_GiftFundSourceT source)
        {
            switch (source)
            {
                case E_GiftFundSourceT.Blank:
                    return FundsSourceBase.Blank;
                case E_GiftFundSourceT.CommunityNonProfit:
                    return FundsSourceBase.CommunityNonProfit;
                case E_GiftFundSourceT.Employer:
                    return FundsSourceBase.Employer;
                case E_GiftFundSourceT.FederalAgency:
                case E_GiftFundSourceT.Government:
                    return FundsSourceBase.FederalAgency;
                case E_GiftFundSourceT.LocalAgency:
                    return FundsSourceBase.LocalAgency;
                case E_GiftFundSourceT.Nonprofit:
                    return FundsSourceBase.CommunityNonProfit;
                case E_GiftFundSourceT.Relative:
                    return FundsSourceBase.Relative;
                case E_GiftFundSourceT.ReligiousNonProfit:
                    return FundsSourceBase.ReligiousNonProfit;
                case E_GiftFundSourceT.Seller:
                    return FundsSourceBase.PropertySeller;
                case E_GiftFundSourceT.StateAgency:
                    return FundsSourceBase.StateAgency;
                case E_GiftFundSourceT.UnmarriedPartner:
                    return FundsSourceBase.UnmarriedPartner;
                case E_GiftFundSourceT.Other:
                    return FundsSourceBase.Other;
                default:
                    throw new UnhandledEnumException(source);
            }
        }

        /// <summary>
        /// Creates a container with details on an asset.
        /// </summary>
        /// <param name="reo">An object holding information on the real-estate owned asset.</param>
        /// <returns>An ASSET_DETAIL container.</returns>
        private ASSET_DETAIL CreateAssetDetail(IRealEstateOwned reo)
        {
            var assetDetail = new ASSET_DETAIL();
            assetDetail.AssetCashOrMarketValueAmount = ToMismoAmount(reo.Val_rep);
            assetDetail.AssetNetValueAmount = ToMismoAmount(reo.NetValue_rep);
            assetDetail.AssetType = ToMismoEnum(AssetBase.RealEstateOwned);

            return assetDetail;
        }

        /// <summary>
        /// Creates a container holding details on an asset.
        /// </summary>
        /// <param name="marketValue">The market value of the asset.</param>
        /// <param name="type">The type of asset.</param>
        /// <param name="faceValue">The face value of the asset.</param>
        /// <returns>An ASSET_DETAIL container.</returns>
        private ASSET_DETAIL CreateAssetDetail(string marketValue, AssetBase type, string faceValue)
        {
            var assetDetail = new ASSET_DETAIL();
            assetDetail.AssetCashOrMarketValueAmount = ToMismoAmount(marketValue);
            assetDetail.AssetType = ToMismoEnum(type);

            if (assetDetail.AssetType?.EnumValue == AssetBase.LifeInsurance)
            {
                assetDetail.LifeInsuranceFaceValueAmount = ToMismoAmount(faceValue);
            }

            return assetDetail;
        }

        /// <summary>
        /// Creates a container holding information on the asset's owner.
        /// </summary>
        /// <param name="name">The name of the asset holder.</param>
        /// <param name="streetAddress">The street address of the asset holder.</param>
        /// <param name="city">The city of the asset holder.</param>
        /// <param name="state">The state of the asset holder.</param>
        /// <param name="zip">The ZIP code of the asset holder.</param>
        /// <param name="isLegalEntity">Indicates whether the asset holder is a legal entity.</param>
        /// <returns>An ASSET_HOLDER container.</returns>
        private ASSET_HOLDER CreateAssetHolder(string name, string streetAddress, string city, string state, string zip, bool isLegalEntity)
        {
            var assetHolder = new ASSET_HOLDER();
            assetHolder.Name = CreateName(name, isLegalEntity);

            assetHolder.Address = CreateAddress(
                streetAddress,
                city,
                state,
                zip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(state),
                AddressBase.Blank,
                1);

            return assetHolder;
        }

        /// <summary>
        /// Creates a CENSUS_INFORMATION object with the census tract of the subject property.
        /// </summary>
        /// <returns>A CENSUS_INFORMATION object with the census tract of the subject property.</returns>
        private CENSUS_INFORMATION CreateCensusInformation()
        {
            var info = new CENSUS_INFORMATION();
            info.CensusTractIdentifier = ToMismoIdentifier(this.DataLoan.sHmdaCensusTract);

            return info;
        }

        /// <summary>
        /// Creates a container that holds elements with information about the subject property.
        /// </summary>
        /// <param name="loanId">Loan identifier associated with the collateral.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A COLLATERAL container.</returns>
        private COLLATERAL CreateCollateral(Guid loanId, int sequenceNumber)
        {
            var collateral = new COLLATERAL();
            collateral.SubjectProperty = this.CreateSubjectProperty(1);
            collateral.SequenceNumber = sequenceNumber;
            collateral.XlinkLabel = "Collateral" + sequenceNumber;

            this.ExportData.CreateRelationship_ToSubjectLoan("COLLATERAL", collateral.XlinkLabel, ArcroleVerbPhrase.IsAssociatedWith);

            return collateral;
        }

        /// <summary>
        /// Creates a container that holds all instances of COLLATERAL.
        /// </summary>
        /// <returns>A COLLATERALS container.</returns>
        private COLLATERALS CreateCollaterals()
        {
            var collaterals = new COLLATERALS();
            collaterals.CollateralList.Add(this.CreateCollateral(this.DataLoan.sLId, 1));
            return collaterals;
        }

        /// <summary>
        /// Creates EXPENSE container holding alimony information.
        /// </summary>
        /// <param name="alimony">Loan file's alimony object.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <returns>Returns an EXPENSE container holding alimony information.</returns>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        private EXPENSE CreateExpense(ILiabilityAlimony alimony, string borrowerLabel, int sequenceNumber)
        {
            var expense = new EXPENSE();

            expense.AlimonyOwedToName = ToMismoString(alimony.OwedTo);
            expense.ExpenseMonthlyPaymentAmount = ToMismoAmount(alimony.Pmt_rep);
            expense.ExpenseRemainingTermMonthsCount = ToMismoCount(alimony.RemainMons_rep, null);
            expense.ExpenseType = ToMismoEnum(ExpenseBase.Alimony);
            expense.SequenceNumber = sequenceNumber;
            expense.XlinkLabel = "Expense" + expense.SequenceNumberStringified;

            this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "EXPENSE", "ROLE", expense.XlinkLabel, borrowerLabel);

            return expense;
        }

        /// <summary>
        /// Creates EXPENSE container holding child support information.
        /// </summary>
        /// <param name="childSupport">Loan file's child support object.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>Returns an EXPENSE container holding child support information.</returns>
        private EXPENSE CreateExpense(ILiabilityChildSupport childSupport, string borrowerLabel, int sequenceNumber)
        {
            var expense = new EXPENSE();

            expense.ExpenseDescription = ToMismoString(childSupport.OwedTo);
            expense.ExpenseMonthlyPaymentAmount = ToMismoAmount(childSupport.Pmt_rep);
            expense.ExpenseRemainingTermMonthsCount = ToMismoCount(childSupport.RemainMons_rep, null);
            expense.ExpenseType = ToMismoEnum(ExpenseBase.ChildSupport);
            expense.SequenceNumber = sequenceNumber;
            expense.XlinkLabel = "Expense" + expense.SequenceNumberStringified;

            this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "EXPENSE", "ROLE", expense.XlinkLabel, borrowerLabel);

            return expense;
        }

        /// <summary>
        /// Creates EXPENSE container holding job expense information.
        /// </summary>
        /// <param name="jobExpense">The job expense object.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>Returns an EXPENSE container holding job expense information.</returns>
        private EXPENSE CreateExpense(ILiabilityJobExpense jobExpense, string borrowerLabel, int sequenceNumber)
        {
            var expense = new EXPENSE();

            expense.ExpenseMonthlyPaymentAmount = ToMismoAmount(jobExpense.Pmt_rep);
            expense.ExpenseType = ToMismoEnum(ExpenseBase.JobRelatedExpenses);
            expense.ExpenseDescription = ToMismoString(jobExpense.ExpenseDesc);
            expense.SequenceNumber = sequenceNumber;
            expense.XlinkLabel = "Expense" + expense.SequenceNumberStringified;

            this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "EXPENSE", "ROLE", expense.XlinkLabel, borrowerLabel);

            return expense;
        }

        /// <summary>
        /// Creates EXPENSES container holding alimony and job-related expenses for all applicants.
        /// </summary>
        /// <returns>Returns EXPENSES container holding alimony and job-related expenses for all applicants.</returns>
        private EXPENSES CreateExpenses()
        {
            EXPENSES expenses = new EXPENSES();
            var validApps = this.DataLoan.Apps.Where(app => app.aBIsValidNameSsn || app.aCIsValidNameSsn);
            foreach (CAppData appData in validApps)
            {
                ILiaCollection liabilityCollection = appData.aLiaCollection;
                ////Create Alimony expense
                var alimony = liabilityCollection.GetAlimony(false);
                if (alimony != null)
                {
                    expenses.ExpenseList.Add(this.CreateExpense(alimony, appData.aBMismoId, GetNextSequenceNumber(expenses.ExpenseList)));
                }

                var childSupport = liabilityCollection.GetChildSupport(false);
                if (childSupport != null)
                {
                    expenses.ExpenseList.Add(this.CreateExpense(childSupport, appData.aBMismoId, GetNextSequenceNumber(expenses.ExpenseList)));
                }

                ////Create job-related expenses
                ILiabilityJobExpense[] jobExpenses = { liabilityCollection.GetJobRelated1(false), liabilityCollection.GetJobRelated2(false) };
                expenses.ExpenseList.AddRange(jobExpenses.Where(jobExpense => jobExpense != null).Select(jobExpense => this.CreateExpense(jobExpense, appData.aBMismoId, GetNextSequenceNumber(expenses.ExpenseList))));
            }

            return expenses;
        }

        /// <summary>
        /// Creates a FIPS_INFORMATION container with Federal Information Processing Standards codes associated with the subject property.
        /// </summary>
        /// <returns>A FIPS_INFORMATION container with Federal Information Processing Standards codes associated with the subject property.</returns>
        private FIPS_INFORMATION CreateFipsInformation()
        {
            var info = new FIPS_INFORMATION();
            info.FIPSCountryCode = ToMismoCode(StateInfo.USACountryCode);
            info.FIPSCountyCode = ToMismoCode(this.DataLoan.sHmdaCountyCode);
            info.FIPSStateAlphaCode = ToMismoCode(this.DataLoan.sSpState);
            info.FIPSStateNumericCode = ToMismoCode(this.DataLoan.sHmdaStateCode);

            return info;
        }

        /// <summary>
        /// Creates a container holding Flood Determination information for subject property.
        /// </summary>
        /// <returns>Returns a container holding Flood Determination information for subject property.</returns>
        private FLOOD_DETERMINATION CreateFloodDetermination()
        {
            FLOOD_DETERMINATION floodDetermination = new FLOOD_DETERMINATION();
            floodDetermination.FloodDeterminationDetail = this.CreateFloodDeterminationDetail();
            return floodDetermination;
        }

        /// <summary>
        /// Creates a container holding details on a flood determination.
        /// </summary>
        /// <returns>A FLOOD_DETERMINATION_DETAIL container.</returns>
        private FLOOD_DETERMINATION_DETAIL CreateFloodDeterminationDetail()
        {
            var floodDeterminationDetail = new FLOOD_DETERMINATION_DETAIL();
            floodDeterminationDetail.NFIPCommunityIdentifier = ToMismoIdentifier(this.DataLoan.sFloodCertificationCommunityNum);
            floodDeterminationDetail.NFIPFloodZoneIdentifier = ToMismoIdentifier(this.DataLoan.sNfipFloodZoneId);
            floodDeterminationDetail.FloodCertificationIdentifier = ToMismoIdentifier(this.DataLoan.sFloodCertId);
            floodDeterminationDetail.FloodDeterminationLifeOfLoanIndicator = ToMismoIndicator(this.DataLoan.sFloodCertificationIsLOLUpgraded);
            floodDeterminationDetail.FloodPartialIndicator = ToMismoIndicator(this.DataLoan.sNfipIsPartialZone);
            floodDeterminationDetail.FloodProductCertifyDate = ToMismoDate(this.DataLoan.sFloodCertificationDeterminationD_rep);
            floodDeterminationDetail.NFIPCommunityName = ToMismoString(this.DataLoan.sFloodHazardCommunityDesc);
            floodDeterminationDetail.NFIPCommunityParticipationStatusType = ToMismoEnum(this.GetNFIPCommunityParticipationStatusBaseValue(this.DataLoan.sFloodCertificationParticipationStatus));
            floodDeterminationDetail.NFIPMapIdentifier = ToMismoIdentifier(this.DataLoan.sFloodCertificationMapNum + this.DataLoan.sFloodCertificationPanelNums + this.DataLoan.sFloodCertificationPanelSuffix);
            floodDeterminationDetail.NFIPMapPanelDate = ToMismoDate(this.DataLoan.sFloodCertificationMapD_rep);
            floodDeterminationDetail.NFIPMapPanelIdentifier = ToMismoIdentifier(this.DataLoan.sFloodCertificationPanelNums);
            floodDeterminationDetail.SpecialFloodHazardAreaIndicator = ToMismoIndicator(this.DataLoan.sFloodCertificationIsInSpecialArea);

            var floodInsuranceExpense = this.DataLoan.sHousingExpenses.GetExpense(E_HousingExpenseTypeT.FloodInsurance);
            if (floodInsuranceExpense != null)
            {
                floodDeterminationDetail.NFIPCommunityParticipationStartDate = ToMismoDate(floodInsuranceExpense.PolicyActivationD_rep);
            }

            return floodDeterminationDetail;
        }

        /// <summary>
        /// Retrieves the NFIPCommunityParticipationStatus value corresponding to a given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding NFIPCommunityParticipationStatus value.</returns>
        private NFIPCommunityParticipationStatusBase GetNFIPCommunityParticipationStatusBaseValue(string lqbValue)
        {
            if (string.Equals("E", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.Emergency;
            }
            else if (string.Equals("N", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.NonParticipating;
            }
            else if (string.Equals("P", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.Probation;
            }
            else if (string.Equals("R", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.Regular;
            }
            else if (string.Equals("S", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return NFIPCommunityParticipationStatusBase.Suspended;
            }
            else
            {
                return NFIPCommunityParticipationStatusBase.Unknown;
            }
        }

        /// <summary>
        /// Creates a container holding a general identifier.
        /// </summary>
        /// <returns>A GENERAL_IDENTIFIER container.</returns>
        private GENERAL_IDENTIFIER CreateGeneralIdentifier()
        {
            var generalIdentifier = new GENERAL_IDENTIFIER();
            generalIdentifier.MSAIdentifier = ToMismoIdentifier(this.DataLoan.sHmdaMsaNum);

            return generalIdentifier;
        }

        /// <summary>
        /// Creates a container to hold a set of hazard insurance policies.
        /// </summary>
        /// <returns>A HAZARD_INSURANCE container.</returns>
        private HAZARD_INSURANCES CreateHazardInsurances()
        {
            var hazardInsurances = new HAZARD_INSURANCES();

            foreach (BaseHousingExpense insuranceExpense in this.DataLoan.sHousingExpenses.ExpensesToUse.Where(e => e.IsInsurance))
            {
                hazardInsurances.HazardInsuranceList.Add(this.CreateHazardInsurance(insuranceExpense, GetNextSequenceNumber(hazardInsurances.HazardInsuranceList)));
            }

            return hazardInsurances;
        }

        /// <summary>
        /// Creates a container holding information on a hazard insurance policy.
        /// </summary>
        /// <param name="insuranceExpense">A <see cref="BaseHousingExpense"/> representing an insurance policy.</param>
        /// <param name="sequence">The sequence number of the hazard insurance among the list of hazard insurances.</param>
        /// <returns>A HAZARD_INSURANCE container.</returns>
        private HAZARD_INSURANCE CreateHazardInsurance(BaseHousingExpense insuranceExpense, int sequence)
        {
            if (!insuranceExpense.IsInsurance)
            {
                return null;
            }

            var hazardInsurance = new HAZARD_INSURANCE();
            hazardInsurance.SequenceNumber = sequence;
            hazardInsurance.HazardInsuranceCoverageType = ToMismoEnum(this.GetHazardInsuranceCoverageBase(insuranceExpense.HousingExpenseType));
            hazardInsurance.HazardInsuranceEffectiveDate = ToMismoDate(insuranceExpense.PolicyActivationD_rep);
            hazardInsurance.HazardInsuranceEscrowedIndicator = ToMismoIndicator(insuranceExpense.IsEscrowedAtClosing);

            var firstDisbursement = insuranceExpense.FirstDisbursement;

            if (firstDisbursement != null)
            {
                hazardInsurance.HazardInsuranceNextPremiumDueDate = ToMismoDate(firstDisbursement.DueDate_rep);
                hazardInsurance.HazardInsurancePremiumAmount = ToMismoAmount(firstDisbursement.DisbursementAmt_rep);
                hazardInsurance.HazardInsurancePremiumMonthsCount = ToMismoCount(firstDisbursement.CoveredMonths_rep, null);
            }

            decimal coverageAmount = 0.00m;

            if (insuranceExpense.HousingExpenseType == E_HousingExpenseTypeT.CondoHO6Insurance)
            {
                hazardInsurance.HazardInsuranceCoverageAmount = ToMismoAmount(this.DataLoan.sCondoHO6InsCoverageAmt_rep);
                hazardInsurance.HazardInsuranceExpirationDate = ToMismoDate(this.DataLoan.sCondoHO6InsPolicyExpirationD_rep);
                hazardInsurance.HazardInsurancePolicyIdentifier = ToMismoIdentifier(this.DataLoan.sCondoHO6InsPolicyNum);
                coverageAmount = this.DataLoan.sCondoHO6InsCoverageAmt;
            }
            else if (insuranceExpense.HousingExpenseType == E_HousingExpenseTypeT.FloodInsurance)
            {
                hazardInsurance.HazardInsuranceCoverageAmount = ToMismoAmount(this.DataLoan.sFloodInsCoverageAmt_rep);
                hazardInsurance.HazardInsuranceExpirationDate = ToMismoDate(this.DataLoan.sFloodInsPolicyExpirationD_rep);
                hazardInsurance.HazardInsurancePolicyIdentifier = ToMismoIdentifier(this.DataLoan.sFloodInsPolicyNum);
                coverageAmount = this.DataLoan.sFloodInsCoverageAmt;
            }
            else if (insuranceExpense.HousingExpenseType == E_HousingExpenseTypeT.HazardInsurance)
            {
                hazardInsurance.HazardInsuranceCoverageAmount = ToMismoAmount(this.DataLoan.sHazInsCoverageAmt_rep);
                hazardInsurance.HazardInsuranceExpirationDate = ToMismoDate(this.DataLoan.sHazInsPolicyExpirationD_rep);
                hazardInsurance.HazardInsurancePolicyIdentifier = ToMismoIdentifier(this.DataLoan.sHazInsPolicyNum);
                coverageAmount = this.DataLoan.sHazInsCoverageAmt;
            }
            else if (insuranceExpense.HousingExpenseType == E_HousingExpenseTypeT.WindstormInsurance)
            {
                hazardInsurance.HazardInsuranceCoverageAmount = ToMismoAmount(this.DataLoan.sWindInsCoverageAmt_rep);
                hazardInsurance.HazardInsuranceExpirationDate = ToMismoDate(this.DataLoan.sWindInsPolicyExpirationD_rep);
                hazardInsurance.HazardInsurancePolicyIdentifier = ToMismoIdentifier(this.DataLoan.sWindInsPolicyNum);
                coverageAmount = this.DataLoan.sWindInsCoverageAmt;
            }

            return coverageAmount > 0.00m || (firstDisbursement != null && firstDisbursement.DisbursementAmt > 0.00m) ? hazardInsurance : null;
        }

        /// <summary>
        /// Gets the Hazard Insurance Coverage type corresponding to the given housing expense type.
        /// </summary>
        /// <param name="expenseType">The housing expense type associated with the insurance policy.</param>
        /// <returns>The Hazard Insurance Coverage type corresponding to the given housing expense type.</returns>
        private HazardInsuranceCoverageBase GetHazardInsuranceCoverageBase(E_HousingExpenseTypeT expenseType)
        {
            switch (expenseType)
            {
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return HazardInsuranceCoverageBase.Homeowners;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return HazardInsuranceCoverageBase.Flood;
                case E_HousingExpenseTypeT.GroundRent:
                    return HazardInsuranceCoverageBase.Blank;
                case E_HousingExpenseTypeT.HazardInsurance:
                    return HazardInsuranceCoverageBase.Hazard;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.RealEstateTaxes:
                case E_HousingExpenseTypeT.SchoolTaxes:
                case E_HousingExpenseTypeT.Unassigned:
                    return HazardInsuranceCoverageBase.Blank;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return HazardInsuranceCoverageBase.Wind;
                default:
                    throw new UnhandledEnumException(expenseType);
            }
        }

        /// <summary>
        /// Creates a container that holds information on a property improvement.
        /// </summary>
        /// <returns>An IMPROVEMENT container.</returns>
        private IMPROVEMENT CreateImprovement()
        {
            var improvement = new IMPROVEMENT();
            improvement.Structure = this.CreateStructure();

            return improvement;
        }

        /// <summary>
        /// Creates a container holding a legal description.
        /// </summary>
        /// <param name="sequence">The sequence number of the legal description among the list of descriptions.</param>
        /// <returns>A LEGAL_DESCRIPTION container.</returns>
        private LEGAL_DESCRIPTION CreateLegalDescription(int sequence)
        {
            var legalDescription = new LEGAL_DESCRIPTION();
            legalDescription.UnparsedLegalDescriptions = this.CreateUnparsedLegalDescriptions();
            legalDescription.ParcelIdentifications = this.CreateParcelIdentifications();
            legalDescription.SequenceNumber = sequence;

            return legalDescription;
        }

        /// <summary>
        /// Creates a container to hold legal descriptions.
        /// </summary>
        /// <returns>A LEGAL_DESCRIPTIONS container.</returns>
        private LEGAL_DESCRIPTIONS CreateLegalDescriptions()
        {
            var legalDescriptions = new LEGAL_DESCRIPTIONS();
            legalDescriptions.LegalDescriptionList.Add(this.CreateLegalDescription(GetNextSequenceNumber(legalDescriptions.LegalDescriptionList)));

            return legalDescriptions;
        }

        /// <summary>
        /// Creates LIABILITIES container populated with liabilities from loan file.
        /// </summary>
        /// <returns>Returns a LIABILITIES container populated with liabilities from loan file.</returns>
        private LIABILITIES CreateLiabilities()
        {
            LIABILITIES liabilities = new LIABILITIES();

            bool isPayoffManuallySpecified = this.DataLoan.sRefPdOffAmt1003Lckd && !IsAmountStringBlankOrZero(this.DataLoan.sRefPdOffAmt1003_rep);
            bool includeManualPayoff = isPayoffManuallySpecified && this.ExportData.Options.DocumentVendor != E_DocumentVendor.Simplifile;

            if (includeManualPayoff)
            {
                liabilities.LiabilityList.Add(
                    this.CreateLiabilityManualPayoff(
                        this.DataLoan.sRefPdOffAmt1003_rep,
                        GetNextSequenceNumber(liabilities.LiabilityList)));
            }

            var validApps = this.DataLoan.Apps.Where(app => app.aBIsValidNameSsn || app.aCIsValidNameSsn);
            foreach (CAppData appData in validApps)
            {
                ILiaCollection liabilityCollection = appData.aLiaCollection;
                for (int recordIndex = 0; recordIndex < liabilityCollection.CountRegular; recordIndex++)
                {
                    ILiabilityRegular liability = liabilityCollection.GetRegularRecordAt(recordIndex);

                    IRealEstateOwned associatedReo = null;
                    if (liability.MatchedReRecordId != Guid.Empty)
                    {
                        associatedReo = appData.aReCollection.GetRegRecordOf(liability.MatchedReRecordId);
                    }

                    liabilities.LiabilityList.Add(
                        this.CreateLiabilityBorrower(
                            liability,
                            associatedReo,
                            appData.aBMismoId,
                            appData.aCMismoId,
                            appData.aCIsValidNameSsn,
                            includeManualPayoff,
                            GetNextSequenceNumber(liabilities.LiabilityList)));
                }
            }

            if ((!this.ExportData.IsClosingPackage && this.DataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative)
                || (this.ExportData.IsClosingPackage && this.DataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative))
            {
                foreach (var adjustment in this.DataLoan.sAdjustmentList.Where(adj => adj.AmountToBorrower != 0 && (adj.IsPaidToBorrower || adj.IsPaidFromBorrower)))
                {
                    liabilities.LiabilityList.Add(
                        this.CreateLiabilityAdjustmentsAndOtherCredits(
                            adjustment.Description,
                            -adjustment.AmountToBorrower,
                             adjustment.AdjustmentType,
                            !adjustment.IncludeAdjustmentInLeCdForThisLien,
                            this.DataLoan.GetAppData(0).aBMismoId,
                            GetNextSequenceNumber(liabilities.LiabilityList)));
                }

                if (this.DataLoan.sONewFinNetProceeds != 0)
                {
                    liabilities.LiabilityList.Add(
                        this.CreateLiabilityAdjustmentsAndOtherCredits(
                            "Subordinate financing",
                            -this.DataLoan.sONewFinNetProceeds,
                            null,
                            null,
                            this.DataLoan.GetAppData(0).aBMismoId,
                            GetNextSequenceNumber(liabilities.LiabilityList)));
                }

                if (this.DataLoan.sTRIDClosingDisclosureAltCost != 0)
                {
                    liabilities.LiabilityList.Add(this.CreateLiabilityAdjustmentsAndOtherCredits(
                        this.DataLoan.sTRIDClosingDisclosureAltCostDescription,
                        this.DataLoan.sTRIDClosingDisclosureAltCost,
                        null,
                        null,
                        this.PrimaryApp.aBMismoId,
                        GetNextSequenceNumber(liabilities.LiabilityList)));
                }

                if (this.DataLoan.sLandCost != 0)
                {
                    liabilities.LiabilityList.Add(this.CreateLiabilityAdjustmentsAndOtherCredits(
                        "Land Cost",
                        this.DataLoan.sLandCost,
                        null,
                        null,
                        this.PrimaryApp.aBMismoId,
                        GetNextSequenceNumber(liabilities.LiabilityList)));
                }
            }

            ////https://www.fanniemae.com/content/technology_requirements/ucd-closing-disclosure-reference-numbers-appendix-c-purchase.pdf
            //// \\megatron\lendersoffice\Integration\UCD\UCDEditedDraft.yml, 15.4.1, 15.5.1, 15.8.1

            liabilities.LiabilityList.Add(
                this.CreateLiabilitySeller(
                    LiabilityBase.FirstPositionMortgageLien,
                    string.Empty,
                    this.DataLoan.sReductionsDueToSellerPayoffOf1stMtgLoan_rep,
                    GetNextSequenceNumber(liabilities.LiabilityList)));

            liabilities.LiabilityList.Add(
                this.CreateLiabilitySeller(
                    LiabilityBase.SecondPositionMortgageLien,
                    string.Empty,
                    this.DataLoan.sReductionsDueToSellerPayoffOf2ndMtgLoan_rep,
                    GetNextSequenceNumber(liabilities.LiabilityList)));

            return liabilities;
        }

        /// <summary>
        /// Creates LIABILITY container from borrower liability entry.
        /// </summary>
        /// <param name="liabilityData">The borrower's liability entry.</param>
        /// <param name="associatedReo">The real estate record associated with the liability, if one exists.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <param name="coborrowerLabel">The $$xlink$$ label for the co-borrower (role).</param>
        /// <param name="coborrowerIsValidSSN">Whether the co borrower's SSN is valid, from the app data (for determining whether to make a RELATIONSHIP with co borrower).</param>
        /// <param name="isPayoffManuallySpecifiedIn1003DOTLineD">Boolean indicating whether payoff is manually specified in Details of Transaction line d. If true, a separate liability element to handle the manual payoff will be created, so there should be no payoffs added to this liability element.</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>Returns a LIABILITY container populated with borrower liability entry information.</returns>
        private LIABILITY CreateLiabilityBorrower(ILiabilityRegular liabilityData, IRealEstateOwned associatedReo, string borrowerLabel, string coborrowerLabel, bool coborrowerIsValidSSN, bool isPayoffManuallySpecifiedIn1003DOTLineD, int sequenceNumber)
        {
            if (this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic
                && liabilityData.PayoffTiming == E_Timing.Before_Closing
                && !this.DataLoan.BrokerDB.IncludeLiabilitiesPaidBeforeClosingInDocMagicMismo33Payload)
            {
                return null;
            }

            bool includePayoff = !isPayoffManuallySpecifiedIn1003DOTLineD && liabilityData.PayoffTiming != E_Timing.Before_Closing &&
                liabilityData.WillBePdOff && !IsAmountStringBlankOrZero(liabilityData.PayoffAmt_rep);
            var liability = new LIABILITY();

            liability.LiabilityDetail = this.CreateLiabilityDetail(liabilityData, associatedReo);
            liability.LiabilityHolder = this.CreateLiabilityHolder(liabilityData);
            liability.Payoff = includePayoff ? this.CreatePayoff_Borrower(liabilityData.PayoffAmt_rep) : null;
            liability.SequenceNumber = sequenceNumber;
            liability.XlinkLabel = "Liability" + liability.SequenceNumberStringified;

            if (liabilityData.OwnerT == E_LiaOwnerT.Borrower || liabilityData.OwnerT == E_LiaOwnerT.Joint)
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.XlinkLabel, borrowerLabel);
            }

            if (coborrowerIsValidSSN && (liabilityData.OwnerT == E_LiaOwnerT.CoBorrower || liabilityData.OwnerT == E_LiaOwnerT.Joint))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.XlinkLabel, coborrowerLabel);
            }

            string reoAssetXlinkLabel = string.Empty;
            this.ExportData.LabelManager.ReoAssetLabels.TryGetValue(liabilityData.MatchedReRecordId.ToString("D"), out reoAssetXlinkLabel);
            if (!string.IsNullOrEmpty(reoAssetXlinkLabel))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "ASSET", "LIABILITY", reoAssetXlinkLabel, liability.XlinkLabel);
            }

            return liability;
        }

        /// <summary>
        /// Creates a liability from an adjustment on the "Adjustments and other credits" list on the "Adjustments and Other Credits" page, for use with the alternate LE/CD form.
        /// </summary>
        /// <param name="description">The description for the LIABILITY instance.</param>
        /// <param name="amountFromBorrower">The amount for the liability.</param>
        /// <param name="adjustmentType">The adjustment type from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="excludeFromLECDForThisLien">The adjustment exclude from CE LE setting from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="borrowerLabel">The label of the borrower to whom the liability will belong.</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>A LIABILITY container with the amount from the borrower (negative if TO the borrower).</returns>
        private LIABILITY CreateLiabilityAdjustmentsAndOtherCredits(string description, decimal amountFromBorrower, E_AdjustmentT? adjustmentType, bool? excludeFromLECDForThisLien, string borrowerLabel, int sequenceNumber)
        {
            var liability = new LIABILITY();

            liability.LiabilityDetail = new LIABILITY_DETAIL
            {
                LiabilityDescription = ToMismoString(description),
                LiabilityExclusionIndicator = ToMismoIndicator(true),
                LiabilityPayoffStatusIndicator = ToMismoIndicator(true),
            };

            liability.Payoff = this.CreatePayoff(this.DataLoan.m_convertLos.ToMoneyString(amountFromBorrower, FormatDirection.ToRep), adjustmentType, excludeFromLECDForThisLien, IntegratedDisclosureSectionBase.PayoffsAndPayments);

            liability.SequenceNumber = sequenceNumber;
            liability.XlinkLabel = "Liability" + liability.SequenceNumberStringified;

            this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.XlinkLabel, borrowerLabel);

            return liability;
        }

        /// <summary>
        /// Creates LIABILITY container holding overriding refinance payoff amount on 1003 Detail of Transaction, section VII d.
        /// </summary>
        /// <param name="payoffAmount">Amount of payoff.</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>Returns a LIABILITY container holding overriding refinance payoff amount on 1003 Detail of Transaction, section VII d.</returns>
        private LIABILITY CreateLiabilityManualPayoff(string payoffAmount, int sequenceNumber)
        {
            var liability = new LIABILITY();

            liability.Payoff = this.CreatePayoff_Borrower(payoffAmount);
            liability.SequenceNumber = sequenceNumber;
            liability.XlinkLabel = "Liability" + liability.SequenceNumberStringified;

            this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.XlinkLabel, this.PrimaryApp.aBMismoId);

            return liability;
        }

        /// <summary>
        /// Creates LIABILITY container intended for SELLER party role.
        /// </summary>
        /// <param name="liabilityType">Specified type of liability.</param>
        /// <param name="liabilityTypeOtherDescription">Free-form description of other liability type.</param>
        /// <param name="payoffAmount">Amount of seller payoff.</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>Returns a LIABILITY container intended for SELLER party role.</returns>
        private LIABILITY CreateLiabilitySeller(LiabilityBase liabilityType, string liabilityTypeOtherDescription, string payoffAmount, int sequenceNumber)
        {
            if (IsAmountStringBlankOrZero(payoffAmount) || (liabilityType == LiabilityBase.Other && string.IsNullOrEmpty(liabilityTypeOtherDescription)))
            {
                return null;
            }

            var liability = new LIABILITY();
            liability.LiabilityDetail = this.CreateLiabilityDetail(liabilityType, liabilityTypeOtherDescription);
            liability.Payoff = this.CreatePayoff_Seller(payoffAmount);
            liability.SequenceNumber = sequenceNumber;
            liability.XlinkLabel = "Liability" + liability.SequenceNumberStringified;

            foreach (var sellerLabel in this.ExportData.LabelManager.PartyLabels.RetrieveLabels(PartyRoleBase.PropertySeller, EntityType.Individual))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAssociatedWith, "LIABILITY", "ROLE", liability.XlinkLabel, sellerLabel);
            }

            return liability;
        }

        /// <summary>
        /// Creates LIABILITY_DETAIL container from borrower liability entry.
        /// </summary>
        /// <param name="liability">The borrower's liability entry.</param>
        /// <param name="associatedReo">The real estate record associated with the liability, if one exists.</param>
        /// <returns>Returns a LIABILITY_DETAIL container populated with borrower liability entry information.</returns>
        private LIABILITY_DETAIL CreateLiabilityDetail(ILiabilityRegular liability, IRealEstateOwned associatedReo)
        {
            var liabilityDetail = new LIABILITY_DETAIL();
            liabilityDetail.LiabilityAccountIdentifier = ToMismoIdentifier(liability.AccNum.Value, isSensitiveData: true);
            liabilityDetail.LiabilityMonthlyPaymentAmount = ToMismoAmount(liability.Pmt_rep, isSensitiveData: true);
            liabilityDetail.LiabilityPayoffStatusIndicator = ToMismoIndicator(liability.WillBePdOff, isSensitiveData: true);
            liabilityDetail.LiabilityRemainingTermMonthsCount = ToMismoCount(liability.RemainMons_rep, null, isSensitiveData: true);

            var liabilityType = this.GetLiabilityBaseValue(liability.DebtT);
            liabilityDetail.LiabilityType = ToMismoEnum(liabilityType, displayLabelText: string.IsNullOrEmpty(liability.Desc) ? liabilityType.ToString() : liability.Desc);
            if (liabilityDetail.LiabilityType?.IsSetToOther ?? false)
            {
                liabilityDetail.LiabilityTypeOtherDescription = ToMismoString(string.IsNullOrEmpty(liability.Desc) ? liability.DebtT_rep : liability.Desc);
            }

            liabilityDetail.LiabilityUnpaidBalanceAmount = ToMismoAmount(liability.Bal_rep, isSensitiveData: true);
            liabilityDetail.LiabilityPayoffWithCurrentAssetsIndicator = this.ToLiabilityPayoffWithCurrentAssetsIndicator(liability.PayoffTiming);

            if (this.ExportData.Options.DocumentVendor == E_DocumentVendor.UnknownOtherNone)
            {
                liabilityDetail.LiabilityExclusionIndicator = this.ToLiabilityExclusionIndicator(liability.DebtT, liability.WillBePdOff, liability.NotUsedInRatio);
            }
            else
            {
                bool excludeLiabilityFromUnderwriting = false;
                if (liability.DebtT == E_DebtRegularT.Mortgage)
                {
                    excludeLiabilityFromUnderwriting = liability.ExcFromUnderwriting || (associatedReo != null && associatedReo.StatT == E_ReoStatusT.Sale);
                }
                else
                {
                    excludeLiabilityFromUnderwriting = liability.ExcFromUnderwriting;
                }

                liabilityDetail.LiabilityExclusionIndicator = ToMismoIndicator(excludeLiabilityFromUnderwriting, isSensitiveData: true);
            }

            return liabilityDetail;
        }

        /// <summary>
        /// Converts Liability type to MISMO compliant value.
        /// </summary>
        /// <param name="debtType">Liability type, as represented in liability editor.</param>
        /// <returns>Returns MISMO compliant liability type.</returns>
        private LiabilityBase GetLiabilityBaseValue(E_DebtRegularT debtType)
        {
            switch (debtType)
            {
                case E_DebtRegularT.Heloc:
                    return LiabilityBase.HELOC;
                case E_DebtRegularT.Installment:
                    return LiabilityBase.Installment;
                case E_DebtRegularT.Lease:
                    return LiabilityBase.LeasePayment;
                case E_DebtRegularT.Mortgage:
                    return LiabilityBase.MortgageLoan;
                case E_DebtRegularT.Open:
                    return LiabilityBase.Open30DayChargeAccount;
                case E_DebtRegularT.Revolving:
                    return LiabilityBase.Revolving;
                case E_DebtRegularT.SeparateMaintenance:
                case E_DebtRegularT.OtherExpenseLiability:
                case E_DebtRegularT.Other:
                    return LiabilityBase.Other;
                default:
                    throw new UnhandledEnumException(debtType);
            }
        }

        /// <summary>
        /// Creates an indicator specifying whether the buyer will pay off a liability before closing with current assets.
        /// </summary>
        /// <param name="timing">Indicates when the buyer will pay off the liability before closing with current assets.</param>
        /// <returns>A serializable MISMOIndicator object.</returns>
        private MISMOIndicator ToLiabilityPayoffWithCurrentAssetsIndicator(E_Timing timing)
        {
            if (timing == E_Timing.Blank)
            {
                return null;
            }

            return ToMismoIndicator(timing == E_Timing.Before_Closing, isSensitiveData: true);
        }

        /// <summary>
        /// Creates a liability exclusion indicator object from the given debt type.
        /// </summary>
        /// <param name="debtType">The type of debt represented by the liability.</param>
        /// <param name="willBePaidOff">Indicates whether the liability will be paid off.</param>
        /// <param name="notUsedInRatio">Indicates whether the liability is used to calculate the debt ratios.</param>
        /// <returns>A serializable MISMOIndicator object.</returns>
        private MISMOIndicator ToLiabilityExclusionIndicator(E_DebtRegularT debtType, bool willBePaidOff, bool notUsedInRatio)
        {
            if (debtType == E_DebtRegularT.Mortgage)
            {
                return ToMismoIndicator(willBePaidOff);
            }
            else
            {
                return ToMismoIndicator(notUsedInRatio);
            }
        }

        /// <summary>
        /// Creates LIABILITY_DETAIL container with type and other-type description specified.
        /// </summary>
        /// <param name="liabilityType">The type of liability.</param>
        /// <param name="liabilityTypeOtherDescription">Free-form description of other liability type.</param>
        /// <returns>Returns a LIABILITY_DETAIL container with type and other-type description specified.</returns>
        private LIABILITY_DETAIL CreateLiabilityDetail(LiabilityBase liabilityType, string liabilityTypeOtherDescription)
        {
            string liabilityDisplayLabelText = liabilityTypeOtherDescription;
            if (liabilityType == LiabilityBase.FirstPositionMortgageLien)
            {
                liabilityDisplayLabelText = "Payoff of First Mortgage Loan";
            }
            else if (liabilityType == LiabilityBase.SecondPositionMortgageLien)
            {
                liabilityDisplayLabelText = "Payoff of Second Mortgage Loan";
            }

            var liabilityDetail = new LIABILITY_DETAIL()
            {
                LiabilityType = ToMismoEnum(liabilityType, displayLabelText: liabilityDisplayLabelText),
                LiabilityTypeOtherDescription = liabilityType == LiabilityBase.Other ? ToMismoString(liabilityTypeOtherDescription) : null
            };
            return liabilityDetail;
        }

        /// <summary>
        /// Creates LIABILITY_HOLDER container from borrower liability entry.
        /// </summary>
        /// <param name="liability">The borrower's liability entry.</param>
        /// <returns>Returns a LIABILITY_HOLDER container populated with borrower liability entry information.</returns>
        private LIABILITY_HOLDER CreateLiabilityHolder(ILiabilityRegular liability)
        {
            var holder = new LIABILITY_HOLDER();
            holder.Address =
                CreateAddress(
                liability.ComAddr,
                liability.ComCity,
                liability.ComState,
                liability.ComZip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(liability.ComState),
                AddressBase.Blank,
                1);

            holder.Name = CreateName(liability.ComNm, isLegalEntity: true);

            return holder;
        }

        /// <summary>
        /// Creates a <see cref="LOANS"/> container.
        /// </summary>
        /// <returns>A <see cref="LOANS"/> container.</returns>
        private LOANS CreateLoans()
        {
            var loans = new LOANS();
            var subjectLoanExporter = new SubjectLoanBuilder(this.DataLoan, this.ExportData, GetNextSequenceNumber(loans.LoanList));
            loans.LoanList.Add(subjectLoanExporter.CreateContainer());

            if (this.DataLoan.sLT == E_sLT.VA && this.DataLoan.sIsRefinancing)
            {
                var relatedLoanBuilder = new RelatedLoanBuilder(this.DataLoan, this.ExportData, GetNextSequenceNumber(loans.LoanList));
                loans.LoanList.Add(relatedLoanBuilder.CreateContainer());
            }

            return loans;
        }

        /// <summary>
        /// Creates a container holding a location identifier.
        /// </summary>
        /// <returns>A LOCATION_IDENTIFIER container.</returns>
        private LOCATION_IDENTIFIER CreateLocationIdentifier()
        {
            var locationIdentifier = new LOCATION_IDENTIFIER();
            locationIdentifier.CensusInformation = this.CreateCensusInformation();
            locationIdentifier.FipsInformation = this.CreateFipsInformation();
            locationIdentifier.GeneralIdentifier = this.CreateGeneralIdentifier();

            return locationIdentifier;
        }

        /// <summary>
        /// Creates a container representing a manufactured home.
        /// </summary>
        /// <returns>A MANUFACTURED_HOME container.</returns>
        private MANUFACTURED_HOME CreateManufacturedHome()
        {
            if (!(this.DataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium
                || this.DataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeMultiwide
                || this.DataLoan.sGseSpT == E_sGseSpT.ManufacturedHousing
                || this.DataLoan.sGseSpT == E_sGseSpT.ManufacturedHousingSingleWide))
            {
                return null;
            }

            var manufacturedHome = new MANUFACTURED_HOME();
            manufacturedHome.ManufacturedHomeDetail = this.CreateManufacturedHomeDetail();

            return manufacturedHome;
        }

        /// <summary>
        /// Creates a container holding details on a manufactured home.
        /// </summary>
        /// <returns>A MANUFACTURED_HOME_DETAIL container.</returns>
        private MANUFACTURED_HOME_DETAIL CreateManufacturedHomeDetail()
        {
            var detail = new MANUFACTURED_HOME_DETAIL();
            detail.LengthFeetNumber = ToMismoNumeric(this.DataLoan.sManufacturedHomeLengthFeet_rep);
            detail.ManufacturedHomeAttachedToFoundationIndicator = ToMismoIndicator(this.DataLoan.sManufacturedHomeAttachedToFoundation);
            detail.ManufacturedHomeConditionDescriptionType = ToMismoEnum(this.GetManufacturedHomeConditionDescriptionBase(this.DataLoan.sManufacturedHomeConditionT));
            detail.ManufacturedHomeManufacturerName = ToMismoString(this.DataLoan.sManufacturedHomeManufacturer);
            detail.ManufacturedHomeManufactureYear = ToMismoYear(this.DataLoan.sManufacturedHomeYear, "sManufacturedHomeYear", false, this.ExportData.Options.IsLenderExportMode);
            detail.ManufacturedHomeModelIdentifier = ToMismoIdentifier(this.DataLoan.sManufacturedHomeModel);
            detail.ManufacturedHomeSerialNumberIdentifier = ToMismoIdentifier(this.DataLoan.sManufacturedHomeSerialNumber);
            detail.ManufacturedHomeWidthType = ToMismoEnum(this.GetManufacturedHomeWidthBaseValue(this.DataLoan.sGseSpT));
            detail.WidthFeetNumber = ToMismoNumeric(this.DataLoan.sManufacturedHomeWidthFeet_rep);
            detail.ManufacturedHomeHUDDataPlateIdentifier = ToMismoIdentifier(this.DataLoan.sManufacturedHomeHUDLabelNumber);

            detail.Extension = this.CreateManufacturedHomeDetailExtension();

            return detail;
        }

        /// <summary>
        /// Converts the given manufactured home condition type to the corresponding ManufacturedHomeConditionDescriptionBase.
        /// </summary>
        /// <param name="condition">New or Used.</param>
        /// <returns>The ManufacturedHomeConditionDescriptionBase corresponding to the given condition.</returns>
        private ManufacturedHomeConditionDescriptionBase GetManufacturedHomeConditionDescriptionBase(E_ManufacturedHomeConditionT condition)
        {
            switch (condition)
            {
                case E_ManufacturedHomeConditionT.New:
                    return ManufacturedHomeConditionDescriptionBase.New;
                case E_ManufacturedHomeConditionT.Used:
                    return ManufacturedHomeConditionDescriptionBase.Used;
                case E_ManufacturedHomeConditionT.LeaveBlank:
                    return ManufacturedHomeConditionDescriptionBase.Blank;
                default:
                    throw new UnhandledEnumException(condition);
            }
        }

        /// <summary>
        /// Retrieves the ManufacturedHomeWidth value that corresponds to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding ManufacturedHomeWidth value.</returns>
        private ManufacturedHomeWidthBase GetManufacturedHomeWidthBaseValue(E_sGseSpT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sGseSpT.ManufacturedHomeMultiwide:
                    return ManufacturedHomeWidthBase.MultiWide;
                case E_sGseSpT.ManufacturedHousingSingleWide:
                    return ManufacturedHomeWidthBase.SingleWide;
                case E_sGseSpT.LeaveBlank:
                    return ManufacturedHomeWidthBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates a container holding extension data related to a manufactured home.
        /// </summary>
        /// <returns>A MANUFACTURED_HOME_DETAIL_EXTENSION container.</returns>
        private MANUFACTURED_HOME_DETAIL_EXTENSION CreateManufacturedHomeDetailExtension()
        {
            var extension = new MANUFACTURED_HOME_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBManufacturedHomeDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates a container holding proprietary datapoints related to a manufactured home.
        /// </summary>
        /// <returns>An LQB_MANUFACTURED_HOME_DETAIL_EXTENSION container.</returns>
        private LQB_MANUFACTURED_HOME_DETAIL_EXTENSION CreateLQBManufacturedHomeDetailExtension()
        {
            var extension = new LQB_MANUFACTURED_HOME_DETAIL_EXTENSION();
            extension.CertificateOfTitleType = ToMismoEnum(this.GetLqbCertificateOfTitleBaseValue(this.DataLoan.sManufacturedHomeCertificateofTitleT));

            return extension;
        }

        /// <summary>
        /// Retrieves the certificate of title base value corresponding to the given type.
        /// </summary>
        /// <param name="type">The given certificate of title type.</param>
        /// <returns>The corresponding value.</returns>
        private LqbCertificateOfTitleBase GetLqbCertificateOfTitleBaseValue(E_ManufacturedHomeCertificateofTitleT type)
        {
            switch (type)
            {
                case E_ManufacturedHomeCertificateofTitleT.HomeIsNotCoveredOrCertIsAttached:
                    return LqbCertificateOfTitleBase.HomeIsNotCoveredOrCertIsAttached;
                case E_ManufacturedHomeCertificateofTitleT.HomeIsNotCoveredOrNotAbleToProduceCert:
                    return LqbCertificateOfTitleBase.HomeIsNotCoveredOrNotAbleToProduceCert;
                case E_ManufacturedHomeCertificateofTitleT.HomeShallBeCoveredByCertificateOfTitle:
                    return LqbCertificateOfTitleBase.HomeShallBeCoveredByCertificateOfTitle;
                case E_ManufacturedHomeCertificateofTitleT.ManufacturersCertificateHasBeenEliminated:
                    return LqbCertificateOfTitleBase.ManufacturersCertificateHasBeenEliminated;
                case E_ManufacturedHomeCertificateofTitleT.ManufacturersCertificateShallBeEliminated:
                    return LqbCertificateOfTitleBase.ManufacturersCertificateShallBeEliminated;
                case E_ManufacturedHomeCertificateofTitleT.TitleCertificateHasBeenEliminated:
                    return LqbCertificateOfTitleBase.TitleCertificateHasBeenEliminated;
                case E_ManufacturedHomeCertificateofTitleT.TitleCertificateShallBeEliminated:
                    return LqbCertificateOfTitleBase.TitleCertificateShallBeEliminated;
                case E_ManufacturedHomeCertificateofTitleT.LeaveBlank:
                    return LqbCertificateOfTitleBase.Blank;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Creates a container representing an owned property.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <returns>An OWNED_PROPERTY container.</returns>
        private OWNED_PROPERTY CreateOwnedProperty(IRealEstateOwned reo)
        {
            var ownedProperty = new OWNED_PROPERTY();
            ownedProperty.Property = this.CreateProperty(reo, 1);
            ownedProperty.OwnedPropertyDetail = this.CreateOwnedPropertyDetail(reo);

            return ownedProperty;
        }

        /// <summary>
        /// Creates a container holding details on an owned property.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <returns>An OWNED_PROPERTY_DETAIL container.</returns>
        private OWNED_PROPERTY_DETAIL CreateOwnedPropertyDetail(IRealEstateOwned reo)
        {
            var ownedPropertyDetail = new OWNED_PROPERTY_DETAIL();
            ownedPropertyDetail.OwnedPropertyDispositionStatusType = ToMismoEnum(this.GetOwnedPropertyDispositionStatusBaseValue(reo.StatT));
            ownedPropertyDetail.OwnedPropertyLienInstallmentAmount = ToMismoAmount(reo.MPmt_rep);
            ownedPropertyDetail.OwnedPropertyLienUPBAmount = ToMismoAmount(reo.MAmt_rep);
            ownedPropertyDetail.OwnedPropertyMaintenanceExpenseAmount = ToMismoAmount(reo.HExp_rep);
            ownedPropertyDetail.OwnedPropertyRentalIncomeGrossAmount = ToMismoAmount(reo.GrossRentI_rep);
            ownedPropertyDetail.OwnedPropertyRentalIncomeNetAmount = ToMismoAmount(reo.NetRentI_rep);
            if (reo.TypeT == E_ReoTypeT._2_4Plx)
            {
                ownedPropertyDetail.OwnedPropertyOwnedUnitCount = ToMismoCount(2, new LosConvert());
            }
            else if (reo.TypeT == E_ReoTypeT.Multi)
            {
                ownedPropertyDetail.OwnedPropertyOwnedUnitCount = ToMismoCount(5, new LosConvert());
            }

            ownedPropertyDetail.Extension = this.CreateOwnedPropertyDetailExtension(reo.TypeT);
            return ownedPropertyDetail;
        }

        /// <summary>
        /// Converts a LendingQB REO status to the corresponding MISMO value.
        /// </summary>
        /// <param name="status">The owned property status.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private OwnedPropertyDispositionStatusBase GetOwnedPropertyDispositionStatusBaseValue(E_ReoStatusT status)
        {
            switch (status)
            {
                case E_ReoStatusT.PendingSale:
                    return OwnedPropertyDispositionStatusBase.PendingSale;
                case E_ReoStatusT.Rental:
                case E_ReoStatusT.Residence:
                    return OwnedPropertyDispositionStatusBase.Retain;
                case E_ReoStatusT.Sale:
                    return OwnedPropertyDispositionStatusBase.Sold;
                default:
                    throw new UnhandledEnumException(status);
            }
        }

        /// <summary>
        /// Creates a container to add custom data points to the OWNED_PROPERTY_DETAIL container.
        /// </summary>
        /// <param name="propertyType">The property type.</param>
        /// <returns>An OWNED_PROPERTY_DETAIL_EXTENSION container.</returns>
        private OWNED_PROPERTY_DETAIL_EXTENSION CreateOwnedPropertyDetailExtension(E_ReoTypeT propertyType)
        {
            var extension = new OWNED_PROPERTY_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBOwnedPropertyDetailExtension(propertyType);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container to hold extra data on owned properties.
        /// </summary>
        /// <param name="propertyType">The property type.</param>
        /// <returns>An LQB_OWNED_PROPERTY_DETAIL_EXTENSION container.</returns>
        private LQB_OWNED_PROPERTY_DETAIL_EXTENSION CreateLQBOwnedPropertyDetailExtension(E_ReoTypeT propertyType)
        {
            var extension = new LQB_OWNED_PROPERTY_DETAIL_EXTENSION();
            extension.GsePropertyType = ToMismoEnum(this.GetGsePropertyBaseValue(propertyType));

            return extension;
        }

        /// <summary>
        /// Retrieves the MISMO value corresponding to the GSE property type.
        /// </summary>
        /// <param name="propertyType">The GSE property type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbGsePropertyBase GetGsePropertyBaseValue(E_ReoTypeT propertyType)
        {
            switch (propertyType)
            {
                case E_ReoTypeT._2_4Plx:
                    return LqbGsePropertyBase.TwoToFourUnitProperty;
                case E_ReoTypeT.ComNR:
                    return LqbGsePropertyBase.CommercialNonResidential;
                case E_ReoTypeT.ComR:
                    return LqbGsePropertyBase.HomeAndBusinessCombined;
                case E_ReoTypeT.Condo:
                    return LqbGsePropertyBase.Condominium;
                case E_ReoTypeT.Coop:
                    return LqbGsePropertyBase.Cooperative;
                case E_ReoTypeT.Farm:
                    return LqbGsePropertyBase.Farm;
                case E_ReoTypeT.Land:
                    return LqbGsePropertyBase.Land;
                case E_ReoTypeT.Mixed:
                    return LqbGsePropertyBase.MixedUseResidential;
                case E_ReoTypeT.Mobil:
                    return LqbGsePropertyBase.ManufacturedMobileHome;
                case E_ReoTypeT.Multi:
                    return LqbGsePropertyBase.MultifamilyMoreThanFourUnits;
                case E_ReoTypeT.SFR:
                    return LqbGsePropertyBase.SingleFamily;
                case E_ReoTypeT.Town:
                    return LqbGsePropertyBase.Townhouse;
                case E_ReoTypeT.Other:
                case E_ReoTypeT.LeaveBlank:
                    return LqbGsePropertyBase.Blank;
                default:
                    throw new UnhandledEnumException(propertyType);
            }
        }

        /// <summary>
        /// Creates a container holding a parcel identification.
        /// </summary>
        /// <param name="sequence">The sequence number of the parcel identification among the list of identifications.</param>
        /// <returns>A PARCEL_IDENTIFICATION container.</returns>
        private PARCEL_IDENTIFICATION CreateParcelIdentification(int sequence)
        {
            var parcelIdentification = new PARCEL_IDENTIFICATION();
            parcelIdentification.ParcelIdentifier = ToMismoIdentifier(this.DataLoan.sAssessorsParcelId);
            parcelIdentification.ParcelIdentificationType = ToMismoEnum(ParcelIdentificationBase.AssessorUnformattedIdentifier);
            parcelIdentification.SequenceNumber = sequence;

            return parcelIdentification;
        }

        /// <summary>
        /// Creates a container holding parcel identifications.
        /// </summary>
        /// <returns>A PARCEL_IDENTIFICATIONS container.</returns>
        private PARCEL_IDENTIFICATIONS CreateParcelIdentifications()
        {
            var parcelIdentifications = new PARCEL_IDENTIFICATIONS();
            parcelIdentifications.ParcelIdentificationList.Add(this.CreateParcelIdentification(GetNextSequenceNumber(parcelIdentifications.ParcelIdentificationList)));

            return parcelIdentifications;
        }

        /// <summary>
        /// Creates a borrower payoff.
        /// </summary>
        /// <param name="payoffAmount">The payoff amount.</param>
        /// <returns>A PAYOFF element.</returns>
        private PAYOFF CreatePayoff_Borrower(string payoffAmount)
        {
            var disclosureSection = GetDisclosureSectionForSectionKAdjustments(
                this.ExportData.IsClosingPackage,
                this.DataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT,
                this.DataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT);

            return this.CreatePayoff(
                payoffAmount,
                null,
                null,
                disclosureSection);
        }

        /// <summary>
        /// Creates a seller payoff.
        /// </summary>
        /// <param name="payoffAmount">The payoff amount.</param>
        /// <returns>A PAYOFF element.</returns>
        private PAYOFF CreatePayoff_Seller(string payoffAmount)
        {
            return this.CreatePayoff(
            payoffAmount,
            null,
            null,
            IntegratedDisclosureSectionBase.DueFromSellerAtClosing);
        }

        /// <summary>
        /// Creates PAYOFF container with specified payoff amount.
        /// </summary>
        /// <param name="payoffAmount">The specified payoff amount.</param>
        /// <param name="adjustmentType">The adjustment type from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="excludeFromLECDForThisLien">The adjustment exclude from CE LE setting from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="section">The section identifier enum for the payoff.</param>
        /// <returns>Returns a PAYOFF container with specified payoff amount.</returns>
        private PAYOFF CreatePayoff(string payoffAmount, E_AdjustmentT? adjustmentType, bool? excludeFromLECDForThisLien, IntegratedDisclosureSectionBase section)
        {
            var payoff = new PAYOFF();
            payoff.PayoffAmount = ToMismoAmount(payoffAmount);
            payoff.IntegratedDisclosureSectionType = ToMismoEnum(section);

            payoff.Extension = this.CreatePayoffExtension(adjustmentType, excludeFromLECDForThisLien);

            return payoff;
        }

        /// <summary>
        /// Creates a container extending the PAYOFF element.
        /// </summary>
        /// <param name="adjustmentType">The adjustment type from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="excludeFromLECDForThisLien">The adjustment exclude from CE LE setting from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <returns>A PAYOFF_EXTENSION container.</returns>
        private PAYOFF_EXTENSION CreatePayoffExtension(E_AdjustmentT? adjustmentType, bool? excludeFromLECDForThisLien)
        {
            var extension = new PAYOFF_EXTENSION();
            extension.Other = this.CreateLqbPayoffExtension(adjustmentType, excludeFromLECDForThisLien);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LendingQB extension to the PAYOFF element.
        /// </summary>
        /// <param name="adjustmentType">The adjustment type from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <param name="excludeFromLECDForThisLien">The adjustment exclude from CE LE setting from which the liability was derived, or null if the liability was not from an adjustment.</param>
        /// <returns>An LQB_PAYOFF_EXTENSION container.</returns>
        private LQB_PAYOFF_EXTENSION CreateLqbPayoffExtension(E_AdjustmentT? adjustmentType, bool? excludeFromLECDForThisLien)
        {
            var extension = new LQB_PAYOFF_EXTENSION();

            if (adjustmentType.HasValue && excludeFromLECDForThisLien.HasValue)
            {
                if (ShouldSendTrid2017SubordinateFinancingData(this.DataLoan.sLienPosT, this.DataLoan.sIsOFinNew, this.DataLoan.sConcurSubFin, this.DataLoan.sTridTargetRegulationVersionT))
                {
                    extension.ExcludeFromLECDForThisLien = ToMismoIndicator(excludeFromLECDForThisLien.Value);
                }

                if (adjustmentType.Value == E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)
                {
                    extension.AdjustmentIsPrincipalReduction = ToMismoIndicator(true);
                    extension.PrincipalReductionForToleranceCure = ToMismoIndicator(false);
                }
                else if (adjustmentType.Value == E_AdjustmentT.PrincipalReductionToCureToleranceViolation)
                {
                    extension.AdjustmentIsPrincipalReduction = ToMismoIndicator(true);
                    extension.PrincipalReductionForToleranceCure = ToMismoIndicator(true);
                }
            }
            else
            {
                if (ShouldSendTrid2017SubordinateFinancingData(this.DataLoan.sLienPosT, this.DataLoan.sIsOFinNew, this.DataLoan.sConcurSubFin, this.DataLoan.sTridTargetRegulationVersionT))
                {
                    extension.ExcludeFromLECDForThisLien = ToMismoIndicator(this.DataLoan.sLienToPayoffTotDebt == ResponsibleLien.OtherLienTransaction);
                }
            }

            return extension;
        }

        /// <summary>
        /// Creates a container that represents a property.
        /// </summary>
        /// <param name="sequence">The sequence number of the property among the list of subject properties.</param>
        /// <returns>A PROPERTY container.</returns>
        private PROPERTY CreateSubjectProperty(int sequence)
        {
            var property = new PROPERTY();
            property.Address = CreateAddress(
                this.DataLoan.sSpAddr,
                this.DataLoan.sSpCity,
                this.DataLoan.sSpState,
                this.DataLoan.sSpZip,
                this.DataLoan.sSpCounty,
                this.DataLoan.sHmdaCountyCode,
                this.ExportData.StatesAndTerritories.GetStateName(this.DataLoan.sSpState),
                AddressBase.Blank,
                1);

            property.FloodDetermination = this.CreateFloodDetermination();
            property.HazardInsurances = this.CreateHazardInsurances();
            property.Improvement = this.CreateImprovement();
            property.LegalDescriptions = this.CreateLegalDescriptions();
            property.LocationIdentifier = this.CreateLocationIdentifier();
            property.ManufacturedHome = this.CreateManufacturedHome();
            property.Project = this.CreateProject();
            property.PropertyDetail = this.CreatePropertyDetail();
            property.PropertyTaxes = this.CreatePropertyTaxes();
            property.PropertyTitle = this.CreatePropertyTitle();
            property.PropertyValuations = this.CreatePropertyValuations();

            property.PropertyUnits = this.CreatePropertyUnits();
            if (!this.DataLoan.sIsRefinancing || this.DataLoan.sGrossDueFromBorrPersonalProperty != 0.00m)
            {
                property.SalesContracts = this.CreateSalesContracts();
            }

            property.ValuationUseType = ValuationUseBase.SubjectProperty;
            property.SequenceNumber = sequence;

            return property;
        }

        /// <summary>
        /// Creates a container that represents a property.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <param name="sequence">The sequence number of the property among the list of real estate owned properties.</param>
        /// <returns>A PROPERTY container.</returns>
        private PROPERTY CreateProperty(IRealEstateOwned reo, int sequence)
        {
            var property = new PROPERTY();
            property.Address = CreateAddress(
                reo.Addr,
                reo.City,
                reo.State,
                reo.Zip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(reo.State),
                AddressBase.Blank,
                1);

            property.PropertyDetail = this.CreatePropertyDetail(reo);
            property.SequenceNumber = sequence;

            if (reo.TypeT == E_ReoTypeT.Condo || reo.TypeT == E_ReoTypeT.Coop || reo.TypeT == E_ReoTypeT.Town)
            {
                property.Project = this.CreateProject(reo);
            }

            if (reo.IsSubjectProp)
            {
                property.ValuationUseType = ValuationUseBase.SubjectProperty;
            }

            return property;
        }

        /// <summary>
        /// Creates a container holding data on a project.
        /// </summary>
        /// <returns>A PROJECT container.</returns>
        private PROJECT CreateProject()
        {
            var project = new PROJECT();
            project.ProjectDetail = this.CreateProjectDetail();

            return project;
        }

        /// <summary>
        /// Creates a container holding data on a project.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <returns>A PROJECT container.</returns>
        private PROJECT CreateProject(IRealEstateOwned reo)
        {
            var project = new PROJECT();
            project.ProjectDetail = this.CreateProjectDetail(reo);

            return project;
        }

        /// <summary>
        /// Creates a container holding details on a project.
        /// </summary>
        /// <returns>A PROJECT_DETAIL container.</returns>
        private PROJECT_DETAIL CreateProjectDetail()
        {
            var projectDetail = new PROJECT_DETAIL();
            projectDetail.ProjectName = ToMismoString(this.DataLoan.sProjNm);
            projectDetail.ProjectAttachmentType = ToMismoEnum(this.GetProjectAttachmentBaseValue(this.DataLoan.sGseSpT));
            projectDetail.ProjectDesignType = ToMismoEnum(this.GetProjectDesignBaseValue(this.DataLoan.sGseSpT));
            projectDetail.ProjectLegalStructureType = ToMismoEnum(this.GetProjectLegalStructureBaseValue(this.DataLoan.sGseSpT));

            return projectDetail;
        }

        /// <summary>
        /// Gets the project attachment type value corresponding to the given property property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding project attachment base value.</returns>
        private ProjectAttachmentBase GetProjectAttachmentBaseValue(E_sGseSpT propertyType)
        {
            switch (propertyType)
            {
                case E_sGseSpT.Attached:
                case E_sGseSpT.Condominium:
                    return ProjectAttachmentBase.Attached;
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.Detached:
                case E_sGseSpT.DetachedCondominium:
                    return ProjectAttachmentBase.Detached;
                case E_sGseSpT.HighRiseCondominium:
                    return ProjectAttachmentBase.Attached;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return ProjectAttachmentBase.Detached;
                case E_sGseSpT.LeaveBlank:
                    return ProjectAttachmentBase.Blank;
                default:
                    throw new UnhandledEnumException(propertyType);
            }
        }

        /// <summary>
        /// Retrieves the project design value corresponding to the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding project design value.</returns>
        private ProjectDesignBase GetProjectDesignBaseValue(E_sGseSpT propertyType)
        {
            if (propertyType == E_sGseSpT.HighRiseCondominium)
            {
                return ProjectDesignBase.HighriseProject;
            }
            else
            {
                return ProjectDesignBase.Other;
            }
        }

        /// <summary>
        /// Gets the project legal structure value corresponding to the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding ProjectLegalStructureBase value.</returns>
        private ProjectLegalStructureBase GetProjectLegalStructureBaseValue(E_sGseSpT propertyType)
        {
            if (propertyType == E_sGseSpT.Condominium ||
                propertyType == E_sGseSpT.DetachedCondominium ||
                propertyType == E_sGseSpT.HighRiseCondominium ||
                propertyType == E_sGseSpT.ManufacturedHomeCondominium)
            {
                return ProjectLegalStructureBase.Condominium;
            }
            else if (propertyType == E_sGseSpT.Cooperative)
            {
                return ProjectLegalStructureBase.Cooperative;
            }
            else
            {
                return ProjectLegalStructureBase.Unknown;
            }
        }

        /// <summary>
        /// Creates a container holding details on a project.
        /// </summary>
        /// <param name="reo">The real estate owned object.</param>
        /// <returns>A PROJECT_DETAIL container.</returns>
        private PROJECT_DETAIL CreateProjectDetail(IRealEstateOwned reo)
        {
            var projectDetail = new PROJECT_DETAIL();
            projectDetail.ProjectLegalStructureType = ToMismoEnum(this.GetProjectLegalStructureBaseValue(reo.TypeT));
            projectDetail.ProjectDesignType = ToMismoEnum(this.GetProjectDesignBaseValue(reo.TypeT));

            return projectDetail;
        }

        /// <summary>
        /// Maps REO type to project design enumeration.
        /// </summary>
        /// <param name="reoType">The REO type.</param>
        /// <returns>The mapped <see cref="ProjectDesignBase"/> enumeration, or Blank if unmapped.</returns>
        private ProjectDesignBase GetProjectDesignBaseValue(E_ReoTypeT reoType)
        {
            if (reoType == E_ReoTypeT.Town)
            {
                return ProjectDesignBase.TownhouseRowhouse;
            }
            else
            {
                return ProjectDesignBase.Blank;
            }
        }

        /// <summary>
        /// Gets the project legal structure value corresponding to the given real estate owned type.
        /// </summary>
        /// <param name="reoType">The real estate owned type.</param>
        /// <returns>The corresponding ProjectLegalStructureBase value.</returns>
        private ProjectLegalStructureBase GetProjectLegalStructureBaseValue(E_ReoTypeT reoType)
        {
            if (reoType == E_ReoTypeT.Condo)
            {
                return ProjectLegalStructureBase.Condominium;
            }
            else if (reoType == E_ReoTypeT.Coop)
            {
                return ProjectLegalStructureBase.Cooperative;
            }
            else
            {
                return ProjectLegalStructureBase.Blank;
            }
        }

        /// <summary>
        /// Creates a container holding data about a property.
        /// </summary>
        /// <returns>A PROPERTY_DETAIL container.</returns>
        private PROPERTY_DETAIL CreatePropertyDetail()
        {
            var propertyDetail = new PROPERTY_DETAIL();
            propertyDetail.AttachmentType = ToMismoEnum(this.GetAttachmentBaseValue(this.DataLoan.sGseSpT));
            propertyDetail.ConstructionMethodType = ToMismoEnum(this.GetConstructionMethodBaseValue(this.DataLoan.sConstructionMethodT));
            propertyDetail.FinancedUnitCount = ToMismoCount(this.DataLoan.sUnitsNum_rep, null);
            propertyDetail.PropertyEstateType = ToMismoEnum(this.GetPropertyEstateBaseValue(this.DataLoan.sEstateHeldT));
            propertyDetail.PropertyEstimatedValueAmount = ToMismoAmount(this.DataLoan.sApprVal_rep);
            propertyDetail.PropertyGroundLeaseExpirationDate = ToMismoDate(this.DataLoan.sLeaseHoldExpireD_rep);
            propertyDetail.PropertyStructureBuiltYear = ToMismoYear(this.DataLoan.sYrBuilt, "sYrBuilt", false, this.ExportData.Options.IsLenderExportMode);
            propertyDetail.PropertyUsageType = ToMismoEnum(this.GetPropertyUsageBaseValue(this.PrimaryApp.aOccT));
            propertyDetail.PUDIndicator = ToMismoIndicator(this.DataLoan.sGseSpT == E_sGseSpT.PUD);
            propertyDetail.PropertyMixedUsageIndicator = ToMismoIndicator(this.DataLoan.sSpIsMixedUse);

            // Construction loan data points
            if (this.ExportData.IsConstructionLoan)
            {
                propertyDetail.PropertyAcquiredYear = ToMismoYear(this.DataLoan.sLotAcqYr, "sLotAcqYr", isLenderExportMode: this.ExportData.Options.IsLenderExportMode);
                propertyDetail.PropertyAcquiredDate = ToMismoDate(this.DataLoan.sLotAcquiredD_rep);
                propertyDetail.PropertyExistingLienAmount = ToMismoAmount(this.DataLoan.sLotLien_rep);
                propertyDetail.PropertyOriginalCostAmount = ToMismoAmount(this.DataLoan.sLotOrigC_rep);
                propertyDetail.ConstructionStatusType = ToMismoEnum(this.GetConstructionStatusBaseValue(this.DataLoan.sBuildingStatusT));
                    
                if (propertyDetail.ConstructionStatusType?.IsSetToOther ?? false)
                {
                    propertyDetail.ConstructionStatusTypeOtherDescription = ToMismoString(this.DataLoan.sFHAConstructionT.ToString());
                }
            }

            // Refinance loan data points
            if (this.DataLoan.sIsRefinancing)
            {
                propertyDetail.PropertyAcquiredYear = ToMismoYear(this.DataLoan.sSpAcqYr, "sSpAcqYr", false, this.ExportData.Options.IsLenderExportMode);
                propertyDetail.PropertyExistingLienAmount = ToMismoAmount(this.DataLoan.sSpLien_rep);
                propertyDetail.PropertyOriginalCostAmount = ToMismoAmount(this.DataLoan.sSpOrigC_rep);
            }

            return propertyDetail;
        }

        /// <summary>
        /// Gets the construction method value corresponding to the given property type.
        /// </summary>
        /// <param name="constructionMethod">The construction method type.</param>
        /// <returns>The corresponding construction method value.</returns>
        private ConstructionMethodBase GetConstructionMethodBaseValue(ConstructionMethod constructionMethod)
        {
            switch (constructionMethod)
            {
                case ConstructionMethod.Blank:
                    return ConstructionMethodBase.Blank;
                case ConstructionMethod.Manufactured:
                    return ConstructionMethodBase.Manufactured;
                case ConstructionMethod.MobileHome:
                    return ConstructionMethodBase.MobileHome;
                case ConstructionMethod.Modular:
                    return ConstructionMethodBase.Modular;
                case ConstructionMethod.OnFrameModular:
                    return ConstructionMethodBase.OnFrameModular;
                case ConstructionMethod.SiteBuilt:
                    return ConstructionMethodBase.SiteBuilt;
                case ConstructionMethod.Other:
                    return ConstructionMethodBase.Other;
                default:
                    throw new UnhandledEnumException(constructionMethod);
            }
        }

        /// <summary>
        /// Creates a container holding data about a property.
        /// </summary>
        /// <param name="reo">An object representing the property.</param>
        /// <returns>A PROPERTY_DETAIL container.</returns>
        private PROPERTY_DETAIL CreatePropertyDetail(IRealEstateOwned reo)
        {
            var propertyDetail = new PROPERTY_DETAIL();
            propertyDetail.PropertyEstimatedValueAmount = ToMismoAmount(reo.Val_rep);
            propertyDetail.LandUseType = ToMismoEnum(this.GetLandUseBaseValue(reo.TypeT));

            if (reo.TypeT == E_ReoTypeT.Mobil)
            {
                propertyDetail.ConstructionMethodType = ToMismoEnum(ConstructionMethodBase.Manufactured);
            }

            if (reo.TypeT == E_ReoTypeT.SFR)
            {
                propertyDetail.AttachmentType = ToMismoEnum(this.GetAttachmentBaseValue(E_sGseSpT.Detached));
            }

            return propertyDetail;
        }

        /// <summary>
        /// Maps LQB REO type to MISMO Land Use.
        /// </summary>
        /// <param name="reoType">The REO type to be mapped.</param>
        /// <returns>Corresponding land use enum, or blank if not mapped.</returns>
        private LandUseBase GetLandUseBaseValue(E_ReoTypeT reoType)
        {
            if (reoType == E_ReoTypeT.ComNR)
            {
                return LandUseBase.Commercial;
            }
            else if (reoType == E_ReoTypeT.ComR)
            {
                return LandUseBase.Income;
            }
            else if (reoType == E_ReoTypeT.Farm)
            {
                return LandUseBase.Agricultural;
            }
            else if (reoType == E_ReoTypeT.Land)
            {
                return LandUseBase.Vacant;
            }
            else if (reoType == E_ReoTypeT.Mixed)
            {
                return LandUseBase.Residential;
            }
            else
            {
                return LandUseBase.Blank;
            }
        }

        /// <summary>
        /// Gets the attachment type value corresponding to the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding attachment base value.</returns>
        private AttachmentBase GetAttachmentBaseValue(E_sGseSpT propertyType)
        {
            switch (propertyType)
            {
                case E_sGseSpT.Attached:
                case E_sGseSpT.Condominium:
                    return AttachmentBase.Attached;
                case E_sGseSpT.Cooperative:
                case E_sGseSpT.Detached:
                case E_sGseSpT.DetachedCondominium:
                    return AttachmentBase.Detached;
                case E_sGseSpT.HighRiseCondominium:
                    return AttachmentBase.Attached;
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return AttachmentBase.Detached;
                case E_sGseSpT.LeaveBlank:
                    return AttachmentBase.Blank;
                default:
                    throw new UnhandledEnumException(propertyType);
            }
        }

        /// <summary>
        /// Gets the construction method value corresponding to the given property type.
        /// </summary>
        /// <param name="propertyType">The subject property type.</param>
        /// <returns>The corresponding construction method value.</returns>
        private ConstructionMethodBase GetConstructionMethodBaseValue(E_sGseSpT propertyType)
        {
            if (propertyType == E_sGseSpT.ManufacturedHousing
                || propertyType == E_sGseSpT.ManufacturedHomeCondominium
                || propertyType == E_sGseSpT.ManufacturedHomeMultiwide
                || propertyType == E_sGseSpT.ManufacturedHousingSingleWide)
            {
                return ConstructionMethodBase.Manufactured;
            }
            else if (propertyType == E_sGseSpT.Modular)
            {
                return ConstructionMethodBase.Modular;
            }
            else
            {
                return ConstructionMethodBase.Other;
            }
        }

        /// <summary>
        /// Retrieves the PropertyEstate value that corresponds to the LQB enumeration.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding PropertyEstateBase value.</returns>
        private PropertyEstateBase GetPropertyEstateBaseValue(E_sEstateHeldT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sEstateHeldT.FeeSimple:
                    return PropertyEstateBase.FeeSimple;
                case E_sEstateHeldT.LeaseHold:
                    return PropertyEstateBase.Leasehold;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the property usage value corresponding to the occupancy type.
        /// </summary>
        /// <param name="occupancyType">The occupancy type.</param>
        /// <returns>The corresponding property usage value.</returns>
        private PropertyUsageBase GetPropertyUsageBaseValue(E_aOccT occupancyType)
        {
            switch (occupancyType)
            {
                case E_aOccT.Investment:
                    return PropertyUsageBase.Investment;
                case E_aOccT.PrimaryResidence:
                    return PropertyUsageBase.PrimaryResidence;
                case E_aOccT.SecondaryResidence:
                    return PropertyUsageBase.SecondHome;
                default:
                    throw new UnhandledEnumException(occupancyType);
            }
        }

        /// <summary>
        /// Retrieves the ConstructionStatus value corresponding to the given LQB value.
        /// </summary>
        /// <param name="buildingStatus">The building status type.</param>
        /// <returns>The corresponding ConstructionStatusBase value.</returns>
        private ConstructionStatusBase GetConstructionStatusBaseValue(E_sBuildingStatusT buildingStatus)
        {
            switch (buildingStatus)
            {
                case E_sBuildingStatusT.LeaveBlank:
                    return ConstructionStatusBase.Blank;
                case E_sBuildingStatusT.Existing:
                    return ConstructionStatusBase.Existing;
                case E_sBuildingStatusT.Proposed:
                    return ConstructionStatusBase.Proposed;
                case E_sBuildingStatusT.SubjectToAlterImproveRepairAndRehab:
                    return ConstructionStatusBase.SubjectToAlterationImprovementRepairAndRehabilitation;
                case E_sBuildingStatusT.SubstantiallyRehabilitated:
                    return ConstructionStatusBase.SubstantiallyRehabilitated;
                case E_sBuildingStatusT.UnderConstruction:
                    return ConstructionStatusBase.UnderConstruction;
                default:
                    throw new UnhandledEnumException(buildingStatus);
            }
        }

        /// <summary>
        /// Creates a container holding a set of property taxes.
        /// </summary>
        /// <returns>A PROPERTY_TAXES container.</returns>
        private PROPERTY_TAXES CreatePropertyTaxes()
        {
            var propertyTaxes = new PROPERTY_TAXES();

            foreach (BaseHousingExpense expense in this.DataLoan.sHousingExpenses.ExpensesToUse.Where(e => e.IsTax && e.MonthlyAmtTotal != 0.00m))
            {
                propertyTaxes.PropertyTaxList.Add(this.CreatePropertyTax(expense, GetNextSequenceNumber(propertyTaxes.PropertyTaxList)));
            }

            return propertyTaxes;
        }

        /// <summary>
        /// Creates a container representing a property tax.
        /// </summary>
        /// <param name="tax">A <see cref="BaseHousingExpense"/> representing the tax.</param>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>A PROPERTY_TAX container.</returns>
        private PROPERTY_TAX CreatePropertyTax(BaseHousingExpense tax, int sequenceNumber)
        {
            var propertyTax = new PROPERTY_TAX();
            propertyTax.PropertyTaxDetail = this.CreatePropertyTaxDetail(tax);
            propertyTax.Extension = this.CreatePropertyTaxExtension(tax);
            propertyTax.SequenceNumber = sequenceNumber;

            return propertyTax;
        }

        /// <summary>
        /// Creates a container holding details on a property tax.
        /// </summary>
        /// <param name="tax">A <see cref="BaseHousingExpense"/> representing the tax.</param>
        /// <returns>A PROPERTY_TAX_DETAIL container.</returns>
        private PROPERTY_TAX_DETAIL CreatePropertyTaxDetail(BaseHousingExpense tax)
        {
            //// 6/18/2015 BB - This container is meant to capture data on the appraisal, e.g. 1004, most of which we don't have. The actual tax monthlies/escrows are captured elsewhere. This container could probably be omitted, but it's fine to include what data we have.
            if (!tax.IsTax || tax.MonthlyAmtTotal == 0.00m)
            {
                return null;
            }

            var propertyTaxDetail = new PROPERTY_TAX_DETAIL();
            propertyTaxDetail.TaxAuthorityType = ToMismoEnum(this.GetTaxAuthorityBaseValue(tax.TaxType));

            if (propertyTaxDetail.TaxAuthorityType?.IsSetToOther ?? false)
            {
                propertyTaxDetail.TaxAuthorityTypeOtherDescription = ToMismoString(tax.TaxType);
            }

            propertyTaxDetail.TaxEscrowedIndicator = ToMismoIndicator(tax.IsEscrowedAtClosing);

            return propertyTaxDetail;
        }

        /// <summary>
        /// Retrieves the tax authority value corresponding to the given tax type.
        /// </summary>
        /// <param name="taxType">The tax type.</param>
        /// <returns>The corresponding tax authority value.</returns>
        private TaxAuthorityBase GetTaxAuthorityBaseValue(E_TaxTableTaxT taxType)
        {
            switch (taxType)
            {
                case E_TaxTableTaxT.Borough:
                    return TaxAuthorityBase.BoroughTax;
                case E_TaxTableTaxT.City:
                    return TaxAuthorityBase.CityTax;
                case E_TaxTableTaxT.County:
                    return TaxAuthorityBase.CountyTax;
                case E_TaxTableTaxT.FireDist:
                    return TaxAuthorityBase.FireOrPoliceTax;
                case E_TaxTableTaxT.LocalImprovementDist:
                    return TaxAuthorityBase.ImprovementTax;
                case E_TaxTableTaxT.School:
                    return TaxAuthorityBase.SchoolDistrictTax;
                case E_TaxTableTaxT.SpecialAssessmentDist:
                    return TaxAuthorityBase.SpecialAssessment;
                case E_TaxTableTaxT.Town:
                    return TaxAuthorityBase.TownTax;
                case E_TaxTableTaxT.Township:
                    return TaxAuthorityBase.TownshipTax;
                case E_TaxTableTaxT.Utility:
                case E_TaxTableTaxT.MunicipalUtilDist:
                    return TaxAuthorityBase.UtilityDistrictTax;
                case E_TaxTableTaxT.Village:
                    return TaxAuthorityBase.VillageTax;
                case E_TaxTableTaxT.Water_Irrigation:
                    return TaxAuthorityBase.WaterControlTax;
                case E_TaxTableTaxT.WasteFeeDist:
                    return TaxAuthorityBase.SanitationTax;
                case E_TaxTableTaxT.Water_Sewer:
                    return TaxAuthorityBase.WaterOrSewerDistrictTax;
                case E_TaxTableTaxT.LeaveBlank:
                case E_TaxTableTaxT.Miscellaneous:
                    return TaxAuthorityBase.Other;
                default:
                    throw new UnhandledEnumException(taxType);
            }
        }

        /// <summary>
        /// Creates a PROPERTY_TAX_EXTENSION container.
        /// </summary>
        /// <param name="tax">The tax to export.</param>
        /// <returns>A PROPERTY_TAX_EXTENSION container.</returns>
        private PROPERTY_TAX_EXTENSION CreatePropertyTaxExtension(BaseHousingExpense tax)
        {
            var extension = new PROPERTY_TAX_EXTENSION();
            extension.Mismo = this.CreateMismoPropertyTaxExtension(tax);

            return extension;
        }

        /// <summary>
        /// Creates a MISMO_PROPERTY_TAX_EXTENSION container.
        /// </summary>
        /// <param name="tax">The tax to export.</param>
        /// <returns>A MISMO_PROPERTY_TAX_EXTENSION container.</returns>
        private MISMO_PROPERTY_TAX_EXTENSION CreateMismoPropertyTaxExtension(BaseHousingExpense tax)
        {
            var extension = new MISMO_PROPERTY_TAX_EXTENSION();

            var disbursementSet = tax.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements
                ? tax.ActualDisbursementsList
                : tax.ProjectedDisbursements;
            extension.EscrowItemDisbursements = CreateEscrowItemDisbursements(disbursementSet, forceExportDisbursements: true);

            return extension;
        }

        /// <summary>
        /// Creates Property Title container with title preliminary report date for subject property. 
        /// </summary>
        /// <returns>Returns Property Title container with title preliminary report date for subject property. </returns>
        private PROPERTY_TITLE CreatePropertyTitle()
        {
            PROPERTY_TITLE propertyTitle = new PROPERTY_TITLE();
            propertyTitle.TitlePreliminaryReportDate = ToMismoDate(this.DataLoan.sPrelimRprtDocumentD_rep);
            propertyTitle.TitleReportItemsDescription = ToMismoString(this.DataLoan.sTitleReportItemsDescription);
            propertyTitle.TitleReportRequiredEndorsementsDescription = ToMismoString(this.DataLoan.sRequiredEndorsements);
            return propertyTitle;
        }

        /// <summary>
        /// Creates a PROPERTY_UNITS container.
        /// </summary>
        /// <returns>The PROPERTY_UNITS container.</returns>
        private PROPERTY_UNITS CreatePropertyUnits()
        {
            PROPERTY_UNITS propertyUnits = new PROPERTY_UNITS();
            propertyUnits.PropertyUnitList.Add(this.CreatePropertyUnit(GetNextSequenceNumber(propertyUnits.PropertyUnitList)));

            return propertyUnits;
        }

        /// <summary>
        /// Creates a PROPERTY_UNIT container.
        /// </summary>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>The PROPERTY_UNIT container.</returns>
        private PROPERTY_UNIT CreatePropertyUnit(int sequenceNumber)
        {
            PROPERTY_UNIT propertyUnit = new PROPERTY_UNIT();
            propertyUnit.PropertyUnitDetail = this.CreatePropertyUnitDetail();
            propertyUnit.SequenceNumber = sequenceNumber;

            return propertyUnit;
        }

        /// <summary>
        /// Creates a PROPERTY_UNIT_DETAIL container.
        /// </summary>
        /// <returns>The PROPERTY_UNIT_DETAIL container.</returns>
        private PROPERTY_UNIT_DETAIL CreatePropertyUnitDetail()
        {
            PROPERTY_UNIT_DETAIL propertyUnitDetail = new PROPERTY_UNIT_DETAIL();
            propertyUnitDetail.BedroomCount = ToMismoCount(this.DataLoan.sSpBedroomCountAsRoundedNonNegativeInt, this.DataLoan.m_convertLos);
            propertyUnitDetail.TotalRoomCount = ToMismoCount(this.DataLoan.sSpRoomCountAsRoundedNonNegativeInt, this.DataLoan.m_convertLos);

            return propertyUnitDetail;
        }

        /// <summary>
        /// Creates a container holding information on a property valuation.
        /// </summary>
        /// <param name="sequenceNumber">The index of this container within a collection of similar containers.</param>
        /// <returns>A PROPERTY_VALUATION container.</returns>
        private PROPERTY_VALUATION CreatePropertyValuation(int sequenceNumber)
        {
            var propertyValuation = new PROPERTY_VALUATION();
            propertyValuation.SequenceNumber = sequenceNumber;
            propertyValuation.PropertyValuationDetail = this.CreatePropertyValuationDetail();
            propertyValuation.Extension = this.CreatePropertyValuationExtension();

            return propertyValuation;
        }

        /// <summary>
        /// Creates a container holding extended information on a property valuation.
        /// </summary>
        /// <returns>A PROPERTY_VALUATION_EXTENSION container.</returns>
        private PROPERTY_VALUATION_EXTENSION CreatePropertyValuationExtension()
        {
            var propertyValuationExtension = new PROPERTY_VALUATION_EXTENSION();
            propertyValuationExtension.Other = this.CreateLqbPropertyValuationExtension();

            return propertyValuationExtension;
        }

        /// <summary>
        /// Creates a container holding LQB-branded information on a property valuation.
        /// </summary>
        /// <returns>An LQB_PROPERTY_VALUATION_EXTENSION container.</returns>
        private LQB_PROPERTY_VALUATION_EXTENSION CreateLqbPropertyValuationExtension()
        {
            var extension = new LQB_PROPERTY_VALUATION_EXTENSION();
            extension.CuRiskScore = ToMismoString(this.DataLoan.sSpCuScore_rep);

            return extension;
        }

        /// <summary>
        /// Creates a container to hold all instances of PROPERTY_VALUATION.
        /// </summary>
        /// <returns>A PROPERTY_VALUATIONS container.</returns>
        private PROPERTY_VALUATIONS CreatePropertyValuations()
        {
            var propertyValuations = new PROPERTY_VALUATIONS();
            propertyValuations.PropertyValuationList.Add(this.CreatePropertyValuation(GetNextSequenceNumber(propertyValuations.PropertyValuationList)));

            return propertyValuations;
        }

        /// <summary>
        /// Creates a container holding details on a property valuation.
        /// </summary>
        /// <returns>A PROPERTY_VALUATION_DETAIL container.</returns>
        private PROPERTY_VALUATION_DETAIL CreatePropertyValuationDetail()
        {
            var propertyValuationDetail = new PROPERTY_VALUATION_DETAIL();
            propertyValuationDetail.PropertyValuationAmount = ToMismoAmount(this.DataLoan.sApprVal_rep);
            propertyValuationDetail.PropertyInspectionType = ToMismoEnum(this.GetPropertyInspectionBaseValue(this.DataLoan.sPropertyInspectionT));
            propertyValuationDetail.PropertyValuationFormType = ToMismoEnum(this.GetPropertyValuationFormBaseValue(this.DataLoan.sSpAppraisalFormT));
            propertyValuationDetail.PropertyValuationMethodType = ToMismoEnum(this.GetPropertyValuationMethodBaseValue(this.DataLoan.sSpValuationMethodT));

            // OPM 247161 - UCD support.
            if (!string.IsNullOrEmpty(this.DataLoan.sSpAppraisalId))
            {
                propertyValuationDetail.AppraisalIdentifier = ToMismoIdentifier(this.DataLoan.sSpAppraisalId, ownerUri: ConstStage.MismoAppraisalIdentifierOwnerURI);
            }

            if (this.DataLoan.sSpValuationMethodT == E_sSpValuationMethodT.FieldReview)
            {
                propertyValuationDetail.PropertyValuationMethodTypeOtherDescription = ToMismoString("FieldReview-FNM2000");
            }

            return propertyValuationDetail;
        }

        /// <summary>
        /// Maps the property inspection type to MISMO.
        /// </summary>
        /// <param name="inspectionType">The property inspection type as represented in LendingQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private PropertyInspectionBase GetPropertyInspectionBaseValue(E_sPropertyInspectionT inspectionType)
        {
            switch (inspectionType)
            {
                case E_sPropertyInspectionT.ExteriorAndInterior:
                    return PropertyInspectionBase.ExteriorAndInterior;
                case E_sPropertyInspectionT.ExteriorOnly:
                    return PropertyInspectionBase.ExteriorOnly;
                case E_sPropertyInspectionT.None:
                    return PropertyInspectionBase.None;
                case E_sPropertyInspectionT.Other:
                    return PropertyInspectionBase.Other;
                default:
                    throw new UnhandledEnumException(inspectionType);
            }
        }

        /// <summary>
        /// Maps the LendingQB appraisal valuation form type to MISMO.
        /// </summary>
        /// <param name="form">The valuation form as represented in LendingQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private PropertyValuationFormBase GetPropertyValuationFormBaseValue(E_sSpAppraisalFormT form)
        {
            switch (form)
            {
                case E_sSpAppraisalFormT.AppraisalUpdateAndOrCompletionReport:
                    return PropertyValuationFormBase.AppraisalUpdateAndOrCompletionReport;
                case E_sSpAppraisalFormT.DesktopUnderwriterPropertyInspectionReport:
                    return PropertyValuationFormBase.DesktopUnderwriterPropertyInspectionReport;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport:
                    return PropertyValuationFormBase.ExteriorOnlyInspectionIndividualCondominiumUnitAppraisalReport;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport:
                    return PropertyValuationFormBase.ExteriorOnlyInspectionIndividualCooperativeInterestAppraisalReport;
                case E_sSpAppraisalFormT.ExteriorOnlyInspectionResidentialAppraisalReport:
                    return PropertyValuationFormBase.ExteriorOnlyInspectionResidentialAppraisalReport;
                case E_sSpAppraisalFormT.IndividualCondominiumUnitAppraisalReport:
                    return PropertyValuationFormBase.IndividualCondominiumUnitAppraisalReport;
                case E_sSpAppraisalFormT.IndividualCooperativeInterestAppraisalReport:
                    return PropertyValuationFormBase.IndividualCooperativeInterestAppraisalReport;
                case E_sSpAppraisalFormT.LoanProspectorConditionAndMarketability:
                    return PropertyValuationFormBase.LoanProspectorConditionAndMarketability;
                case E_sSpAppraisalFormT.ManufacturedHomeAppraisalReport:
                    return PropertyValuationFormBase.ManufacturedHomeAppraisalReport;
                case E_sSpAppraisalFormT.OneUnitResidentialAppraisalFieldReviewReport:
                    return PropertyValuationFormBase.OneUnitResidentialAppraisalFieldReviewReport;
                case E_sSpAppraisalFormT.SmallResidentialIncomePropertyAppraisalReport:
                    return PropertyValuationFormBase.SmallResidentialIncomePropertyAppraisalReport;
                case E_sSpAppraisalFormT.TwoToFourUnitResidentialAppraisal:
                    return PropertyValuationFormBase.TwoToFourUnitResidentialAppraisalFieldReviewReport;
                case E_sSpAppraisalFormT.UniformResidentialAppraisalReport:
                    return PropertyValuationFormBase.UniformResidentialAppraisalReport;
                case E_sSpAppraisalFormT.Blank:
                case E_sSpAppraisalFormT.Other:
                    return PropertyValuationFormBase.Other;
                default:
                    throw new UnhandledEnumException(form);
            }
        }

        /// <summary>
        /// Maps the LendingQB valuation method to MISMO.
        /// </summary>
        /// <param name="valuationMethod">The valuation method as represented in LendingQB.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private PropertyValuationMethodBase GetPropertyValuationMethodBaseValue(E_sSpValuationMethodT valuationMethod)
        {
            switch (valuationMethod)
            {
                case E_sSpValuationMethodT.AutomatedValuationModel:
                    return PropertyValuationMethodBase.AutomatedValuationModel;
                case E_sSpValuationMethodT.DesktopAppraisal:
                    return PropertyValuationMethodBase.DesktopAppraisal;
                case E_sSpValuationMethodT.DriveBy:
                    return PropertyValuationMethodBase.DriveBy;
                case E_sSpValuationMethodT.FullAppraisal:
                    return PropertyValuationMethodBase.FullAppraisal;
                case E_sSpValuationMethodT.PriorAppraisalUsed:
                    return PropertyValuationMethodBase.PriorAppraisalUsed;
                case E_sSpValuationMethodT.None:
                    return PropertyValuationMethodBase.None;
                case E_sSpValuationMethodT.FieldReview:
                case E_sSpValuationMethodT.DeskReview:
                case E_sSpValuationMethodT.LeaveBlank:
                case E_sSpValuationMethodT.Other:
                    return PropertyValuationMethodBase.Other;
                default:
                    throw new UnhandledEnumException(valuationMethod);
            }
        }

        /// <summary>
        /// Creates a container to hold all <see cref="RELATIONSHIP"/> elements in the payload.
        /// </summary>
        /// <returns>A <see cref="RELATIONSHIPS"/> container.</returns>
        private RELATIONSHIPS CreateRelationships()
        {
            RELATIONSHIPS relationships = new RELATIONSHIPS();

            foreach (RELATIONSHIP relationship in this.ExportData.RelationshipList)
            {
                if (!string.IsNullOrWhiteSpace(relationship.From) && !string.IsNullOrWhiteSpace(relationship.To))
                {
                    relationships.RelationshipList.Add(relationship);
                }
            }

            return relationships;
        }

        /// <summary>
        /// Creates a container representing a sales concession.
        /// </summary>
        /// <param name="sequence">The sequence number of the sales concession among the list of concessions.</param>
        /// <returns>A SALES_CONCESSION container.</returns>
        private SALES_CONCESSION CreateSalesConcession(int sequence)
        {
            var salesConcession = new SALES_CONCESSION();
            salesConcession.SequenceNumber = sequence;

            if (this.DataLoan.sLT == E_sLT.Conventional)
            {
                salesConcession.SalesConcessionAmount = ToMismoAmount(this.DataLoan.sFHASalesConcessions_rep);
            }

            return salesConcession;
        }

        /// <summary>
        /// Creates a container to hold all sales concessions.
        /// </summary>
        /// <returns>A SALES_CONCESSIONS container.</returns>
        private SALES_CONCESSIONS CreateSalesConcessions()
        {
            var salesConcessions = new SALES_CONCESSIONS();
            salesConcessions.SalesConcessionList.Add(this.CreateSalesConcession(GetNextSequenceNumber(salesConcessions.SalesConcessionList)));

            return salesConcessions;
        }

        /// <summary>
        /// Creates a container representing a sales contract.
        /// </summary>
        /// <param name="sequence">The sequence number of the sales contract among the list of contracts.</param>
        /// <returns>A SALES_CONTRACT container.</returns>
        private SALES_CONTRACT CreateSalesContract(int sequence)
        {
            var salesContract = new SALES_CONTRACT();
            salesContract.SequenceNumber = sequence;
            salesContract.SalesConcessions = this.CreateSalesConcessions();
            salesContract.SalesContractDetail = this.CreateSalesContractDetail();

            return salesContract;
        }

        /// <summary>
        /// Creates a container with details on a sales contract.
        /// </summary>
        /// <returns>A SALES_CONTRACT_DETAIL container.</returns>
        private SALES_CONTRACT_DETAIL CreateSalesContractDetail()
        {
            var salesContractDetail = new SALES_CONTRACT_DETAIL();

            bool personalPropertyIncluded = this.DataLoan.sGrossDueFromBorrPersonalProperty != 0.00m;
            salesContractDetail.PersonalPropertyIncludedIndicator = ToMismoIndicator(personalPropertyIncluded);

            if (personalPropertyIncluded)
            {
                salesContractDetail.RealPropertyAmount = ToMismoAmount(this.DataLoan.sPurchPrice_rep);
                salesContractDetail.PersonalPropertyAmount = ToMismoAmount(this.DataLoan.sGrossDueFromBorrPersonalProperty_rep);
            }
            else
            {
                salesContractDetail.SalesContractAmount = ToMismoAmount(this.DataLoan.sPurchPrice_rep);
            }

            return salesContractDetail;
        }

        /// <summary>
        /// Creates a container to hold all sales contracts.
        /// </summary>
        /// <returns>A SALES_CONTRACTS container.</returns>
        private SALES_CONTRACTS CreateSalesContracts()
        {
            var salesContracts = new SALES_CONTRACTS();
            salesContracts.SalesContractList.Add(this.CreateSalesContract(GetNextSequenceNumber(salesContracts.SalesContractList)));

            return salesContracts;
        }

        /// <summary>
        /// Creates a container holding information on a structure.
        /// </summary>
        /// <returns>A STRUCTURE container.</returns>
        private STRUCTURE CreateStructure()
        {
            var structure = new STRUCTURE();
            structure.StructureDetail = this.CreateStructureDetail();

            return structure;
        }

        /// <summary>
        /// Creates a container holding details on a structure.
        /// </summary>
        /// <returns>A STRUCTURE_DETAIL container.</returns>
        private STRUCTURE_DETAIL CreateStructureDetail()
        {
            var structureDetail = new STRUCTURE_DETAIL();

            if (this.DataLoan.sGseSpT == E_sGseSpT.Condominium ||
                this.DataLoan.sGseSpT == E_sGseSpT.DetachedCondominium ||
                this.DataLoan.sGseSpT == E_sGseSpT.HighRiseCondominium ||
                this.DataLoan.sGseSpT == E_sGseSpT.ManufacturedHomeCondominium)
            {
                structureDetail.StoriesCount = ToMismoCount(this.DataLoan.sProdCondoStories_rep);
            }

            return structureDetail;
        }

        /// <summary>
        /// Creates a container holding an unparsed legal description.
        /// </summary>
        /// <param name="sequence">The sequence number of the unparsed legal description among the list of descriptions.</param>
        /// <returns>An UNPARSED_LEGAL_DESCRIPTION container.</returns>
        private UNPARSED_LEGAL_DESCRIPTION CreateUnparsedLegalDescription(int sequence)
        {
            var unparsedLegalDescription = new UNPARSED_LEGAL_DESCRIPTION();
            unparsedLegalDescription.UnparsedLegalDescription = ToMismoString(this.DataLoan.sSpLegalDesc);
            unparsedLegalDescription.SequenceNumber = sequence;

            return unparsedLegalDescription;
        }

        /// <summary>
        /// Creates a container holding unparsed legal descriptions.
        /// </summary>
        /// <returns>An UNPARSED_LEGAL_DESCRIPTIONS container.</returns>
        private UNPARSED_LEGAL_DESCRIPTIONS CreateUnparsedLegalDescriptions()
        {
            var unparsedLegalDescriptions = new UNPARSED_LEGAL_DESCRIPTIONS();
            unparsedLegalDescriptions.UnparsedLegalDescriptionList.Add(this.CreateUnparsedLegalDescription(GetNextSequenceNumber(unparsedLegalDescriptions.UnparsedLegalDescriptionList)));

            return unparsedLegalDescriptions;
        }
    }
}
