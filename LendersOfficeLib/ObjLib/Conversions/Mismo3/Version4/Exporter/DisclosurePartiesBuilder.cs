﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Class to construct the Disclosure parties.
    /// </summary>
    public class DisclosurePartiesBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DisclosurePartiesBuilder"/> class.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="exportData">A collection of cached data used throughout the exporter.</param>
        /// <param name="partyList">The party list to populate.</param>
        public DisclosurePartiesBuilder(CPageData dataLoan, Mismo34ExporterCache exportData, List<PARTY> partyList)
        {
            this.DataLoan = dataLoan;
            this.ExportData = exportData;
            this.PartyList = partyList;
            this.ExportData = exportData;
        }

        /// <summary>
        /// Gets the data loan.
        /// </summary>
        protected CPageData DataLoan { get; }

        /// <summary>
        /// Gets the running party list.
        /// </summary>
        protected List<PARTY> PartyList { get; }

        /// <summary>
        /// Gets a collection of data used throughout the exporter.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Populates the parties list with disclosure parties.
        /// </summary>
        public void PopulatePartiesList()
        {
            if (this.ExportData.IsClosingPackage)
            {
                this.AddLenderParties();
                this.AddMortgageBrokerParties();
                this.AddRealEstateBrokerBuyerParties();
                this.AddRealEstateBrokerSellerParties();
                this.AddSettlementAgentParties();
            }
            else
            {
                var lenderParty = this.AddLoanEstimateLenderParty();
                var loanOfficerParty = this.AddLoanEstimateLoanOfficerParty();
                var brokerParty = this.CreateParty_LoanEstimate_MortgageBroker();

                if (loanOfficerParty != null)
                {
                    string employeeLabel = loanOfficerParty.Roles.RoleList[0].XlinkLabel;
                    string employerLabel = string.Empty;

                    if (this.DataLoan.sGfeIsTPOTransaction && brokerParty != null)
                    {
                        employerLabel = brokerParty.Roles.RoleList[0].XlinkLabel;
                    }
                    else if (lenderParty != null)
                    {
                        employerLabel = lenderParty.Roles.RoleList[0].XlinkLabel;
                    }

                    if (!string.IsNullOrEmpty(employeeLabel) && !string.IsNullOrEmpty(employerLabel))
                    {
                        this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", employeeLabel, employerLabel);
                    }
                }
            }

            // Seller info should always be added, but they will only be listed on the disclosure for CD exports.
            this.AddClosingDisclosureSellers();
        }

        /// <summary>
        /// Creates a list of parties representing the Lender on the Closing Disclosure.
        /// </summary>
        private void AddLenderParties()
        {
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDLenderName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);
                legalEntityParty.LegalEntity = CreateLegalEntity(this.DataLoan.sTRIDLenderName);
                legalEntityParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDLenderAddr,
                    this.DataLoan.sTRIDLenderCity,
                    this.DataLoan.sTRIDLenderState,
                    this.DataLoan.sTRIDLenderZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.Lender,
                    EntityType.LegalEntity,
                    this.DataLoan.sTRIDLenderLicenseIDNum,
                    this.DataLoan.sTRIDLenderNMLSNum);
                legalEntityLabel = legalEntityParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDLenderContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                individualParty.Individual = CreateIndividual(
                    this.DataLoan.sTRIDLenderContact,
                    this.DataLoan.sTRIDLenderContactPhoneNum,
                    string.Empty,
                    this.DataLoan.sTRIDLenderContactEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDLenderAddr,
                    this.DataLoan.sTRIDLenderCity,
                    this.DataLoan.sTRIDLenderState,
                    this.DataLoan.sTRIDLenderZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.Lender,
                    EntityType.Individual,
                    this.DataLoan.sTRIDLenderContactLicenseIDNum,
                    this.DataLoan.sTRIDLenderContactNMLSNum);
                individualLabel = individualParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel);
            }
        }

        /// <summary>
        /// Creates a list of parties representing the Mortgage Broker on the Closing Disclosure.
        /// </summary>
        private void AddMortgageBrokerParties()
        {
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDMortgageBrokerName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                legalEntityParty.LegalEntity = CreateLegalEntity(this.DataLoan.sTRIDMortgageBrokerName);

                legalEntityParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDMortgageBrokerAddr,
                    this.DataLoan.sTRIDMortgageBrokerCity,
                    this.DataLoan.sTRIDMortgageBrokerState,
                    this.DataLoan.sTRIDMortgageBrokerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.MortgageBroker,
                    EntityType.LegalEntity,
                    this.DataLoan.sTRIDMortgageBrokerLicenseIDNum,
                    this.DataLoan.sTRIDMortgageBrokerNMLSNum);
                legalEntityLabel = legalEntityParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDMortgageBrokerContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                individualParty.Individual = CreateIndividual(
                    this.DataLoan.sTRIDMortgageBrokerContact,
                    this.DataLoan.sTRIDMortgageBrokerPhoneNum,
                    string.Empty,
                    this.DataLoan.sTRIDMortgageBrokerEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDMortgageBrokerAddr,
                    this.DataLoan.sTRIDMortgageBrokerCity,
                    this.DataLoan.sTRIDMortgageBrokerState,
                    this.DataLoan.sTRIDMortgageBrokerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.MortgageBroker,
                    EntityType.Individual,
                    this.DataLoan.sTRIDMortgageBrokerContactLicenseIDNum,
                    this.DataLoan.sTRIDMortgageBrokerContactNMLSNum);
                individualLabel = individualParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel);
            }
        }

        /// <summary>
        /// Creates a list of parties representing the Real Estate Selling Agents on the Closing Disclosure.
        /// </summary>
        private void AddRealEstateBrokerBuyerParties()
        {
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDRealEstateBrokerBuyerName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                legalEntityParty.LegalEntity = CreateLegalEntity(this.DataLoan.sTRIDRealEstateBrokerBuyerName);

                legalEntityParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDRealEstateBrokerBuyerAddr,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerCity,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerState,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.RealEstateAgent,
                    EntityType.LegalEntity,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerLicenseIDNum,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerNMLSNum,
                    RealEstateAgentBase.Selling);
                legalEntityLabel = legalEntityParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDRealEstateBrokerBuyerContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                individualParty.Individual = CreateIndividual(
                    this.DataLoan.sTRIDRealEstateBrokerBuyerContact,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerPhoneNum,
                    string.Empty,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDRealEstateBrokerBuyerAddr,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerCity,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerState,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.RealEstateAgent,
                    EntityType.Individual,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerContactLicenseIDNum,
                    this.DataLoan.sTRIDRealEstateBrokerBuyerContactNMLSNum,
                    RealEstateAgentBase.Selling);
                individualLabel = individualParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel);
            }
        }

        /// <summary>
        /// Creates a list of parties representing the Real Estate Listing Agents on the Closing Disclosure.
        /// </summary>
        private void AddRealEstateBrokerSellerParties()
        {
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDRealEstateBrokerSellerName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                legalEntityParty.LegalEntity = CreateLegalEntity(this.DataLoan.sTRIDRealEstateBrokerSellerName);

                legalEntityParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDRealEstateBrokerSellerAddr,
                    this.DataLoan.sTRIDRealEstateBrokerSellerCity,
                    this.DataLoan.sTRIDRealEstateBrokerSellerState,
                    this.DataLoan.sTRIDRealEstateBrokerSellerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.RealEstateAgent,
                    EntityType.LegalEntity,
                    this.DataLoan.sTRIDRealEstateBrokerSellerLicenseIDNum,
                    this.DataLoan.sTRIDRealEstateBrokerSellerNMLSNum,
                    RealEstateAgentBase.Listing);
                legalEntityLabel = legalEntityParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDRealEstateBrokerSellerContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                individualParty.Individual = CreateIndividual(
                    this.DataLoan.sTRIDRealEstateBrokerSellerContact,
                    this.DataLoan.sTRIDRealEstateBrokerSellerPhoneNum,
                    string.Empty,
                    this.DataLoan.sTRIDRealEstateBrokerSellerEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDRealEstateBrokerSellerAddr,
                    this.DataLoan.sTRIDRealEstateBrokerSellerCity,
                    this.DataLoan.sTRIDRealEstateBrokerSellerState,
                    this.DataLoan.sTRIDRealEstateBrokerSellerZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.RealEstateAgent,
                    EntityType.Individual,
                    this.DataLoan.sTRIDRealEstateBrokerSellerContactLicenseIDNum,
                    this.DataLoan.sTRIDRealEstateBrokerSellerContactNMLSNum,
                    RealEstateAgentBase.Listing);
                individualLabel = individualParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel);
            }
        }

        /// <summary>
        /// Creates a list of parties representing the Settlement Agents on the Closing Disclosure.
        /// </summary>
        private void AddSettlementAgentParties()
        {
            string legalEntityLabel = string.Empty;
            string individualLabel = string.Empty;

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDSettlementAgentName))
            {
                var legalEntityParty = new PARTY();
                legalEntityParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                legalEntityParty.LegalEntity = CreateLegalEntity(this.DataLoan.sTRIDSettlementAgentName);

                legalEntityParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDSettlementAgentAddr,
                    this.DataLoan.sTRIDSettlementAgentCity,
                    this.DataLoan.sTRIDSettlementAgentState,
                    this.DataLoan.sTRIDSettlementAgentZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                legalEntityParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.ClosingAgent,
                    EntityType.LegalEntity,
                    this.DataLoan.sTRIDSettlementAgentLicenseIDNum,
                    this.DataLoan.sTRIDSettlementAgentNMLSNum);
                legalEntityLabel = legalEntityParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(legalEntityParty);
            }

            if (!string.IsNullOrEmpty(this.DataLoan.sTRIDSettlementAgentContact))
            {
                var individualParty = new PARTY();
                individualParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                individualParty.Individual = CreateIndividual(
                    this.DataLoan.sTRIDSettlementAgentContact,
                    this.DataLoan.sTRIDSettlementAgentPhoneNum,
                    string.Empty,
                    this.DataLoan.sTRIDSettlementAgentEmail,
                    ContactPointRoleBase.Work);

                individualParty.Addresses = CreateAddresses(
                    this.DataLoan.sTRIDSettlementAgentAddr,
                    this.DataLoan.sTRIDSettlementAgentCity,
                    this.DataLoan.sTRIDSettlementAgentState,
                    this.DataLoan.sTRIDSettlementAgentZip,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                individualParty.Roles = this.CreateRoles_DisclosureContact(
                    PartyRoleBase.ClosingAgent,
                    EntityType.Individual,
                    this.DataLoan.sTRIDSettlementAgentContactLicenseIDNum,
                    this.DataLoan.sTRIDSettlementAgentContactNMLSNum);
                individualLabel = individualParty.Roles.RoleList[0].XlinkLabel;

                this.PartyList.Add(individualParty);
            }

            if (!string.IsNullOrEmpty(individualLabel) && !string.IsNullOrEmpty(legalEntityLabel))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualLabel, legalEntityLabel);
            }
        }

        /// <summary>
        /// Creates a party representing the Lender on the Loan Estimate.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY AddLoanEstimateLenderParty()
        {
            if (string.IsNullOrEmpty(this.DataLoan.sTRIDLenderName))
            {
                return null;
            }

            var party = new PARTY();
            party.SequenceNumber = GetNextSequenceNumber(this.PartyList);
            party.LegalEntity = CreateLegalEntity(
                this.DataLoan.sTRIDLenderName,
                this.DataLoan.sTRIDLenderPhoneNum,
                string.Empty,
                this.DataLoan.sTRIDLenderContactEmail,
                null,
                ContactPointRoleBase.Work);

            party.Addresses = CreateAddresses(
                this.DataLoan.sTRIDLenderAddr,
                this.DataLoan.sTRIDLenderCity,
                this.DataLoan.sTRIDLenderState,
                this.DataLoan.sTRIDLenderZip,
                AddressBase.Mailing,
                string.Empty,
                string.Empty);

            party.Roles = this.CreateRoles_DisclosureContact(
                PartyRoleBase.Lender,
                EntityType.LegalEntity,
                this.DataLoan.sTRIDLenderLicenseIDNum,
                this.DataLoan.sTRIDLenderNMLSNum);

            this.PartyList.Add(party);

            return party;
        }

        /// <summary>
        /// Creates a party representing the Loan Officer on the Loan Estimate.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY AddLoanEstimateLoanOfficerParty()
        {
            if (string.IsNullOrEmpty(this.DataLoan.sTRIDLoanOfficerName))
            {
                return null;
            }

            var party = new PARTY();
            party.SequenceNumber = GetNextSequenceNumber(this.PartyList);

            party.Individual = CreateIndividual(
                this.DataLoan.sTRIDLoanOfficerName,
                this.DataLoan.sTRIDLoanOfficerPhoneNum,
                string.Empty,
                this.DataLoan.sTRIDLoanOfficerEmail,
                ContactPointRoleBase.Work);

            party.Roles = this.CreateRoles_DisclosureContact(
                PartyRoleBase.LoanOfficer,
                EntityType.Individual,
                this.DataLoan.sTRIDLoanOfficerLicenseIDNum,
                this.DataLoan.sTRIDLoanOfficerNMLSNum);

            var agentId = this.DataLoan.sTRIDLoanOfficerAgentId;
            if (agentId != Guid.Empty)
            {
                var loanOfficerAgent = this.DataLoan.GetAgentFields(agentId);
                if (loanOfficerAgent != null)
                {
                    party.Extension = CreatePartyExtension(loanOfficerAgent, EntityType.Individual);
                }
            }

            this.PartyList.Add(party);
            return party;
        }

        /// <summary>
        /// Creates a party representing the Mortgage Broker on the Loan Estimate.
        /// </summary>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_LoanEstimate_MortgageBroker()
        {
            if (string.IsNullOrEmpty(this.DataLoan.sTRIDMortgageBrokerName))
            {
                return null;
            }

            var party = new PARTY();
            party.SequenceNumber = GetNextSequenceNumber(this.PartyList);

            party.LegalEntity = CreateLegalEntity(this.DataLoan.sTRIDMortgageBrokerName);

            party.Roles = this.CreateRoles_DisclosureContact(
                PartyRoleBase.MortgageBroker,
                EntityType.LegalEntity,
                this.DataLoan.sTRIDMortgageBrokerLicenseIDNum,
                this.DataLoan.sTRIDMortgageBrokerNMLSNum);

            this.PartyList.Add(party);
            return party;
        }

        /// <summary>
        /// Creates a list of parties representing the Sellers on the Closing Disclosure.
        /// </summary>
        private void AddClosingDisclosureSellers()
        {
            foreach (var seller in this.DataLoan.sSellerCollection.ListOfSellers)
            {
                var sellerParty = new PARTY();
                sellerParty.SequenceNumber = GetNextSequenceNumber(this.PartyList);

                if (seller.Type == E_SellerType.Individual)
                {
                    sellerParty.Individual = CreateIndividual(seller.Name);
                }
                else
                {
                    sellerParty.LegalEntity = CreateLegalEntity(seller.Name, seller.EntityType);
                }

                sellerParty.Addresses = CreateAddresses(
                    seller.Address.StreetAddress,
                    seller.Address.City,
                    seller.Address.State,
                    seller.Address.PostalCode,
                    AddressBase.Mailing,
                    string.Empty,
                    string.Empty);

                sellerParty.Roles = this.CreateRoles_Seller(
                    seller.Type == E_SellerType.Individual ? EntityType.Individual : EntityType.LegalEntity,
                    this.ExportData.IsClosingPackage);

                this.PartyList.Add(sellerParty);
            }
        }

        /// <summary>
        /// Creates a ROLES element holding a single ROLE representing a seller.
        /// </summary>
        /// <param name="entityType"> Indicates whether the contact is an individual or legal entity.</param>
        /// <param name="includedInDisclosure">Indicates whether the seller is included in the disclosure.</param>
        /// <returns>A ROLE container.</returns>
        private ROLES CreateRoles_Seller(EntityType entityType, bool includedInDisclosure)
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole_Seller(entityType, includedInDisclosure, GetNextSequenceNumber(roles.RoleList)));

            return roles;
        }

        /// <summary>
        /// Creates a ROLE container representing a seller.
        /// </summary>
        /// <param name="entityType">Indicates whether the seller is an individual or legal entity.</param>
        /// <param name="includedInDisclosure">Indicates whether the seller is included in the disclosure.</param>
        /// <param name="sequenceNumber">The sequence number of the container.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_Seller(EntityType entityType, bool includedInDisclosure, int sequenceNumber)
        {
            var role = new ROLE();

            var roleType = PartyRoleBase.PropertySeller;
            role.RoleDetail = CreateRoleDetail(roleType, string.Empty, includedInDisclosure);
            role.XlinkLabel = this.ExportData.GeneratePartyLabel(roleType, entityType);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        ////// shared //////

        /// <summary>
        /// Creates a ROLES element holding a single ROLE representing a contact on the Loan Estimate or Closing Disclosure.
        /// </summary>
        /// <param name="roleType">The contact's role type.</param>
        /// <param name="entityType">Indicates whether the contact is an individual or legal entity.</param>
        /// <param name="licenseId">The contact's license number.</param>
        /// <param name="nmlsId">The contact's NMLS number.</param>
        /// <param name="realEstateAgentType">The Real Estate Agent Type. Can be omitted for non-Real Estate Agents.</param>
        /// <returns>A ROLES container.</returns>
        private ROLES CreateRoles_DisclosureContact(PartyRoleBase roleType, EntityType entityType, string licenseId, string nmlsId, RealEstateAgentBase realEstateAgentType = RealEstateAgentBase.Blank)
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole_DisclosureContact(roleType, entityType, licenseId, nmlsId, GetNextSequenceNumber(roles.RoleList), realEstateAgentType));

            return roles;
        }

        /// <summary>
        /// Creates a role for a contact listed on the Loan Estimate or Closing Disclosure.
        /// </summary>
        /// <param name="roleType">The contact's role type.</param>
        /// <param name="entityType">Indicates whether the contact is an individual or legal entity.</param>
        /// <param name="licenseId">The contact's license number.</param>
        /// <param name="nmlsId">The contact's NMLS number.</param>
        /// <param name="sequenceNumber">The sequence number for the ROLE element.</param>
        /// <param name="realEstateAgentType">The Real Estate Agent Type. Can be omitted for non-Real Estate Agents.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_DisclosureContact(PartyRoleBase roleType, EntityType entityType, string licenseId, string nmlsId, int sequenceNumber, RealEstateAgentBase realEstateAgentType = RealEstateAgentBase.Blank)
        {
            var role = new ROLE();
            role.RoleDetail = CreateRoleDetail(roleType, string.Empty, includedInDisclosure: true);
            role.SequenceNumber = sequenceNumber;

            if (!string.IsNullOrEmpty(licenseId) || !string.IsNullOrEmpty(nmlsId))
            {
                role.Licenses = CreateLicenses(licenseId, nmlsId, this.DataLoan.sSpState, this.DataLoan.sLT);
            }

            if (realEstateAgentType != RealEstateAgentBase.Blank)
            {
                role.RealEstateAgent = this.CreateRealEstateAgent(realEstateAgentType);
            }

            if (roleType == PartyRoleBase.ClosingAgent)
            {
                role.ClosingAgent = CreateClosingAgent(ClosingAgentBase.Other, "SettlementAgent");
            }

            if (this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic && roleType == PartyRoleBase.Lender && this.DataLoan.sDocMagicAlternateLenderId != Guid.Empty)
            {
                role.PartyRoleIdentifiers = this.CreatePartyRoleIdentifiers_Lender();
            }

            role.XlinkLabel = this.ExportData.GeneratePartyLabel(roleType, entityType);

            return role;
        }

        /// <summary>
        /// Creates a REAL_ESTATE_AGENT container based on the given agent type.
        /// </summary>
        /// <param name="realEstateAgentType">The real estate agent type.</param>
        /// <returns>A REAL_ESTATE_AGENT container.</returns>
        private REAL_ESTATE_AGENT CreateRealEstateAgent(RealEstateAgentBase realEstateAgentType)
        {
            var agent = new REAL_ESTATE_AGENT();
            agent.RealEstateAgentType = ToMismoEnum(realEstateAgentType, isSensitiveData: false);

            return agent;
        }

        /// <summary>
        /// Creates a container holding party role identifiers for the lender.
        /// </summary>
        /// <returns>A set of party role identifiers.</returns>
        private PARTY_ROLE_IDENTIFIERS CreatePartyRoleIdentifiers_Lender()
        {
            var partyRoleIdentifiers = new PARTY_ROLE_IDENTIFIERS();
            partyRoleIdentifiers.PartyRoleIdentifierList.Add(this.CreatePartyRoleIdentifier_Lender());

            return partyRoleIdentifiers;
        }

        /// <summary>
        /// Creates a party role identifier for the lender.
        /// </summary>
        /// <returns>A party role identifier.</returns>
        private PARTY_ROLE_IDENTIFIER CreatePartyRoleIdentifier_Lender()
        {
            var altLender = new ObjLib.DocMagicLib.DocMagicAlternateLender(this.DataLoan.BrokerDB.BrokerID, this.DataLoan.sDocMagicAlternateLenderId);

            var partyRoleIdentifier = new PARTY_ROLE_IDENTIFIER();
            partyRoleIdentifier.PartyRoleIdentifier = ToMismoIdentifier(altLender.DocMagicAlternateLenderInternalId, string.Empty, @"www.docmagic.com/altlenderidentifier");

            return partyRoleIdentifier;
        }
    }
}
