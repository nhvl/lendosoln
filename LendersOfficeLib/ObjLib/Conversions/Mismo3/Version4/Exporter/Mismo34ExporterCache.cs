﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using Mismo3Specification;
    using Mismo3Specification.Version4Schema;
    using static Mismo3Specification.Mismo3Utilities;

    /// <summary>
    /// A cache for data used throughout the various MISMO 3.4 export classes.
    /// </summary>
    public class Mismo34ExporterCache
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Mismo34ExporterCache"/> class.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <param name="options">A collection of export options defined by the caller.</param>
        public Mismo34ExporterCache(CPageData dataLoan, Mismo34ExporterOptions options)
        {
            this.Options = options;
            this.RelationshipList = new List<RELATIONSHIP>();
            this.StatesAndTerritories = StateInfo.Instance;
            this.LabelManager = new Mismo3XLinkLabelManager();

            this.PopulateBaselineFeeIgnoreList(dataLoan);

            if (string.IsNullOrEmpty(this.Options.VendorAccountId))
            {
                this.Options.VendorAccountId = dataLoan.BrokerDB.CustomerCode;
            }

            this.IsClosingPackage = this.Options.DocumentPackage.IsOfType(Integration.DocumentVendor.VendorConfig.PackageType.Closing)
                || (this.Options.DocumentVendor == E_DocumentVendor.UnknownOtherNone && dataLoan.sHasClosingDisclosureArchive);

            this.IsConstructionLoan = dataLoan.sLPurposeT == E_sLPurposeT.Construct
                || dataLoan.sLPurposeT == E_sLPurposeT.ConstructPerm;

            if (this.Options.Principal != null && !(this.Options.Principal is Security.SystemUserPrincipal))
            {
                this.User = new EmployeeDB(this.Options.Principal.EmployeeId, this.Options.Principal.BrokerId);
                this.User.Retrieve();
            }
        }

        /// <summary>
        /// Gets a collection of MISMO 3.4 export options selected by the caller.
        /// </summary>
        public Mismo34ExporterOptions Options { get; private set; }

        /// <summary>
        /// Gets a list of relationships to be serialized toward the end of the export process.
        /// </summary>
        public List<RELATIONSHIP> RelationshipList { get; private set; }

        /// <summary>
        /// Gets the list of ignored fees.
        /// </summary>
        public IEnumerable<Guid> FeeIgnoreList { get; private set; }

        /// <summary>
        /// Gets a collection of state and territory information.
        /// </summary>
        public StateInfo StatesAndTerritories { get; private set; }

        /// <summary>
        /// Gets an XLink label manager.
        /// </summary>
        public Mismo3XLinkLabelManager LabelManager { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the package to be exported is a closing package.
        /// </summary>
        public bool IsClosingPackage { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the loan is a construction loan.
        /// </summary>
        public bool IsConstructionLoan { get; private set; }

        /// <summary>
        /// Gets the user running the export.
        /// </summary>
        public EmployeeDB User { get; private set; }

        /// <summary>
        /// Retrieves the primary application from a loan object.
        /// </summary>
        /// <param name="dataLoan">A loan object.</param>
        /// <returns>The primary application on the loan.</returns>
        public static CAppData GetPrimaryApp(CPageData dataLoan)
        {
            var primaryApp = dataLoan.GetAppData(0);
            primaryApp.BorrowerModeT = E_BorrowerModeT.Borrower;
            return primaryApp;
        }

        /// <summary>
        /// Creates a <see cref="RELATIONSHIP"/> container that describes the relationship between two objects.
        /// </summary>
        /// <param name="arcroleVerbPhrase">Describes the nature of the relationship between two objects.</param>
        /// <param name="fromName">The element name of the source object.</param>
        /// <param name="toname">The element name of the target object.</param>
        /// <param name="fromLabel">The unique label identifying the source object for the relationship.</param>
        /// <param name="tolabel">The unique label identifying the target object for the relationship.</param>
        public void CreateRelationship(ArcroleVerbPhrase arcroleVerbPhrase, string fromName, string toname, string fromLabel, string tolabel)
        {
            var relationship = new RELATIONSHIP();

            relationship.Arcrole = $"{Mismo3Constants.XlinkArcroleURN}{fromName}_{GetXmlEnumName(arcroleVerbPhrase)}_{toname}";
            relationship.From = fromLabel;
            relationship.To = tolabel;
            relationship.SequenceNumber = GetNextSequenceNumber(this.RelationshipList);

            this.RelationshipList.Add(relationship);
        }

        /// <summary>
        /// Adds a relation between the given element and the parent loan.
        /// </summary>
        /// <param name="name">The name of the element.</param>
        /// <param name="label">The unique label for the element.</param>
        /// <param name="verbPhrase">The verb phrase to link the object and loan.</param>
        /// <param name="isDataPoint">True if the element is a datapoint, false if it is a container.</param>
        public void CreateRelationship_ToSubjectLoan(string name, string label, ArcroleVerbPhrase verbPhrase, bool isDataPoint = false)
        {
            string loanContainerName = nameof(LOAN);
            string loanXlinkLabel = string.Empty;
            this.LabelManager.LoanLabels.TryGetValue(this.Options.LoanId, out loanXlinkLabel);

            if (string.IsNullOrEmpty(loanXlinkLabel))
            {
                return;
            }

            // The container that has alphabetical precedence is listed first.
            // If the given element is a data point, however, it is always listed last.
            if (isDataPoint || loanContainerName.CompareTo(name) < 0)
            {
                this.CreateRelationship(verbPhrase, "LOAN", name, loanXlinkLabel, label);
            }
            else
            {
                this.CreateRelationship(verbPhrase, name, "LOAN", label, loanXlinkLabel);
            }
        }

        /// <summary>
        /// Generates an XLink label for a loan.
        /// </summary>
        /// <param name="loanId">The loan's ID.</param>
        /// <param name="sequenceNumber">The loan's sequence number.</param>
        /// <returns>An XLink label.</returns>
        public string GenerateLoanLabel(Guid loanId, string sequenceNumber)
        {
            var label = $"LOAN{sequenceNumber}";
            this.LabelManager.LoanLabels.Add(loanId, label);
            return label;
        }

        /// <summary>
        /// Generates a role label for a <see cref="PARTY"/>. 
        /// </summary>
        /// <param name="roleType">The party role type for the entity.</param>
        /// <param name="entityType">Indicates whether the <see cref="PARTY"/> is a legal entity or individual.</param>
        /// <returns>The label string.</returns>
        public string GeneratePartyLabel(PartyRoleBase roleType, EntityType entityType)
        {
            return this.GeneratePartyLabel(roleType.ToString(), entityType);
        }

        /// <summary>
        /// Generates a role label for a <see cref="PARTY"/>. 
        /// </summary>
        /// <param name="partyType">A string representing the entity's role.</param>
        /// <param name="entityType">Indicates whether the <see cref="PARTY"/> is a legal entity or individual.</param>
        /// <returns>The label string.</returns>
        public string GeneratePartyLabel(string partyType, EntityType entityType)
        {
            string label = partyType + GetNextSequenceNumber(this.LabelManager.PartyLabels.Labels);

            this.LabelManager.PartyLabels.StoreLabel(label, partyType, entityType);
            this.CreateRelationship_ToSubjectLoan("ROLE", label, ArcroleVerbPhrase.IsAssociatedWith);

            return label;
        }

        /// <summary>
        /// Populates the list of fees that should be excluded from the export. Some vendors need this
        /// because otherwise they will duplicate fees in the documents if they receive information on
        /// the same fee in multiple elements, such as the ESCROW and FEE sections.
        /// </summary>
        /// <param name="dataLoan">The loan being exported.</param>
        private void PopulateBaselineFeeIgnoreList(CPageData dataLoan)
        {
            var ignoreList = new List<Guid>();

            if (this.Options.DocumentVendor != E_DocumentVendor.DocMagic && this.Options.DocumentVendor != E_DocumentVendor.Simplifile)
            {
                return;
            }

            foreach (BaseHousingExpense expense in dataLoan.sHousingExpenses.ExpensesToUse.Where(e => (e.AnnualAmt != 0.00m || e.MonthlyAmtTotal != 0.00m || e.ReserveAmt != 0.00m) && e.IsEscrowedAtClosing == E_TriState.Yes))
            {
                BorrowerClosingCostFee escrowFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionG);
                if (escrowFee != null && escrowFee.ClosingCostFeeTypeId != Guid.Empty)
                {
                    ignoreList.Add(escrowFee.ClosingCostFeeTypeId);
                }
            }

            foreach (BorrowerClosingCostFee prepaidFee in CommonBuilder.GetPrepaidFees(dataLoan))
            {
                if (prepaidFee != null && prepaidFee.ClosingCostFeeTypeId != Guid.Empty)
                {
                    ignoreList.Add(prepaidFee.ClosingCostFeeTypeId);
                }
            }

            if (this.Options.DocumentVendor == E_DocumentVendor.DocMagic)
            {
                ignoreList.Add(DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId);
                ignoreList.Add(DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId);
            }

            if (this.Options.DocumentVendor == E_DocumentVendor.Simplifile)
            {
                var miEscrowFee = CommonBuilder.GetMortgageInsuranceEscrowFee(dataLoan);
                if (miEscrowFee != null && miEscrowFee.ClosingCostFeeTypeId != Guid.Empty)
                {
                    ignoreList.Add(miEscrowFee.ClosingCostFeeTypeId);
                }

                ignoreList.Add(DefaultSystemClosingCostFee.Hud1000AggregateAdjustmentFeeTypeId);
            }

            this.FeeIgnoreList = ignoreList;
        }
    }
}
