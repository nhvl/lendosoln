﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.Core.Construction;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib;
    using LendersOffice.ObjLib.AdjustmentsAndOtherCredits;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.ObjLib.TRID2;
    using LendersOffice.Reminders;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Exporter for MISMO 3.4 LOAN data for the subject loan.
    /// </summary>
    public class SubjectLoanBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SubjectLoanBuilder"/> class.
        /// </summary>
        /// <param name="dataLoan">The loan data representing the subject loan.</param>
        /// <param name="exportData">A collection of data used throughout the exporter.</param>
        /// <param name="sequenceNumber">The sequence number of the loan to be created.</param>
        public SubjectLoanBuilder(
            CPageData dataLoan,
            Mismo34ExporterCache exportData,
            int sequenceNumber)
        {
            this.DataLoan = dataLoan;
            this.PrimaryApp = Mismo34ExporterCache.GetPrimaryApp(dataLoan);
            this.ExportData = exportData;
            this.SequenceNumber = sequenceNumber;
        }

        /// <summary>
        /// Gets the loan object to be exported.
        /// </summary>
        protected CPageData DataLoan { get; private set; }

        /// <summary>
        /// Gets the primary application on the loan.
        /// </summary>
        protected CAppData PrimaryApp { get; private set; }

        /// <summary>
        /// Gets a collection of data used throughout the exporter.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Gets the sequence number of the item to create.
        /// </summary>
        protected int SequenceNumber
        {
            get; private set;
        }

        /// <summary>
        /// Creates a new LOAN MISMO container representing the subject loan.
        /// </summary>
        /// <returns>A new LOAN container.</returns>
        public LOAN CreateContainer()
        {
            var loan = new LOAN();
            loan.SequenceNumber = this.SequenceNumber;
            loan.XlinkLabel = this.ExportData.GenerateLoanLabel(this.DataLoan.sLId, loan.SequenceNumberStringified);
            loan.LoanRoleType = LoanRoleBase.SubjectLoan;

            loan.Adjustment = CreateAdjustment(this.DataLoan, this.ExportData.Options.IsLenderExportMode);
            loan.Amortization = this.CreateAmortization();
            loan.Assumability = this.CreateAssumability();
            loan.BillingAddress = this.CreateBillingAddress();
            loan.Buydown = this.CreateBuydown();
            loan.ClosingInformation = this.CreateClosingInformation();
            loan.Construction = this.CreateConstruction();
            loan.Documentations = this.CreateDocumentations();
            
            var documentSpecificDataSetBuilder = new DocumentSpecificDataSetsBuilder(this.DataLoan, this.ExportData);
            loan.DocumentSpecificDataSets = documentSpecificDataSetBuilder.CreateContainer();
            
            loan.Draw = this.CreateDraw();

            var escrowBuilder = new EscrowBuilder(this.DataLoan, this.ExportData);
            loan.Escrow = escrowBuilder.CreateContainer();

            loan.FeeInformation = this.CreateFeeInformation();
            loan.Foreclosures = this.CreateForeclosures();
            loan.GovernmentLoan = this.CreateGovernmentLoan();
            loan.Heloc = this.CreateHeloc();
            loan.HighCostMortgages = this.CreateHighCostMortgages();
            loan.HmdaLoan = this.CreateHmdaLoan();
            loan.HousingExpenses = this.CreateHousingExpenses();
            loan.InterestCalculation = this.CreateInterestCalculation();
            loan.InterestOnly = CreateInterestOnly(this.DataLoan);
            loan.InvestorLoanInformation = this.CreateInvestorLoanInformation();
            loan.LateCharge = this.CreateLateCharge();
            loan.LoanDetail = this.CreateLoanDetail();
            loan.LoanIdentifiers = this.CreateLoanIdentifiers();
            loan.LoanLevelCredit = this.CreateLoanLevelCredit();
            loan.LoanProduct = this.CreateLoanProduct();
            loan.Ltv = this.CreateLtv();
            loan.Maturity = this.CreateMaturity();
            loan.MersRegistrations = this.CreateMersRegistrations();
            loan.MiData = this.CreateMiData();
            loan.NegativeAmortization = this.CreateNegativeAmortization();
            loan.Payment = CreatePayment(this.DataLoan, this.ExportData.IsClosingPackage);
            loan.PrepaymentPenalty = this.CreatePrepaymentPenalty();
            loan.PurchaseCredits = this.CreatePurchaseCredits();
            loan.Qualification = this.CreateQualification();
            loan.QualifiedMortgage = this.CreateQualifiedMortgage();
            loan.Refinance = this.CreateRefinance();
            loan.Rehabilitation = this.CreateRehabilitation();
            loan.TermsOfLoan = CreateTermsOfLoan(this.DataLoan, this.ExportData, this.DataLoan.BrokerDB.IsEnableHELOC);
            loan.Underwriting = this.CreateUnderwriting();

            if (!this.DataLoan.sIsRefinancing)
            {
                loan.DownPayments = this.CreateDownPayments();
            }

            return loan;
        }

        /// <summary>
        /// Converts the given agency type to the corresponding QualifiedMortgageTemporaryGSEBase, e.g. Fannie Mae or Freddie Mac.
        /// </summary>
        /// <param name="agency">The agency by which the loan is eligible for purchase.</param>
        /// <returns>The QualifiedMortgageTemporaryGSEBase that corresponds to the given agency type.</returns>
        private static QualifiedMortgageTemporaryGSEBase GetQualifiedMortgageTemporaryGSEBase(E_sQMLoanPurchaseAgency agency)
        {
            switch (agency)
            {
                case E_sQMLoanPurchaseAgency.Blank:
                    return QualifiedMortgageTemporaryGSEBase.Blank;
                case E_sQMLoanPurchaseAgency.FannieMae:
                    return QualifiedMortgageTemporaryGSEBase.FannieMae;
                case E_sQMLoanPurchaseAgency.FHA:
                    return QualifiedMortgageTemporaryGSEBase.Blank;
                case E_sQMLoanPurchaseAgency.FreddieMac:
                    return QualifiedMortgageTemporaryGSEBase.FreddieMac;
                case E_sQMLoanPurchaseAgency.USDA:
                case E_sQMLoanPurchaseAgency.VA:
                    return QualifiedMortgageTemporaryGSEBase.Blank;
                default:
                    throw new UnhandledEnumException(agency);
            }
        }

        /// <summary>
        /// Retrieves the RefinancePrimaryPurpose value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding RefinancePrimaryPurpose value.</returns>
        private static RefinancePrimaryPurposeBase GetRefinancePrimaryPurposeBaseValue(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return RefinancePrimaryPurposeBase.Unspecified;
            }
            else if (string.Equals("Cash-Out/Home Improvement", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinancePrimaryPurposeBase.HomeImprovement;
            }
            else if (string.Equals("Cash-Out/Debt Consolidation", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return RefinancePrimaryPurposeBase.DebtConsolidation;
            }
            else
            {
                return RefinancePrimaryPurposeBase.Other;
            }
        }

        /// <summary>
        /// Retrieves the underwriting decision date for the given loan.
        /// </summary>
        /// <param name="approvalDate">The approval date for the loan.</param>
        /// <param name="approvalDateString">A string representation of the approval date.</param>
        /// <param name="rejectionDate">The rejection date for the loan.</param>
        /// <param name="rejectionDateString">A string representation of the rejection date.</param>
        /// <returns>The underwriting decision date.</returns>
        private static string GetUnderwritingDecisionDate(CDateTime approvalDate, string approvalDateString, CDateTime rejectionDate, string rejectionDateString)
        {
            if (approvalDate.IsValid)
            {
                return (approvalDate.CompareTo(rejectionDate, true) >= 0) ? approvalDateString : rejectionDateString;
        }
            else if (rejectionDate.IsValid)
            {
                return rejectionDateString;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Retrieves the automated underwriting system result value based on the result fields for the loan.
        /// </summary>
        /// <param name="isAccepted">Indicates whether the loan is rated Accepted.</param>
        /// <param name="isRefer">Indicates whether the loan is rated Refer.</param>
        /// <returns>The corresponding result value.</returns>
        private static string GetAutomatedUnderwritingSystemResultValue(bool isAccepted, bool isRefer)
        {
            if (isAccepted)
            {
                return "AA";
            }
            else if (isRefer)
            {
                return "Refer";
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Retrieves the system type corresponding to the indicators in the loan.
        /// </summary>
        /// <param name="isDesktopUnderwriter">Indicates whether the automated system is a desktop underwriter.</param>
        /// <param name="isLoanProspector">Indicates whether the automated system is a loan prospector.</param>
        /// <param name="isOther">Indicates whether the automated system is something else.</param>
        /// <returns>The corresponding system type.</returns>
        private static AutomatedUnderwritingSystemBase GetAutomatedUnderwritingSystemBaseValue(bool isDesktopUnderwriter, bool isLoanProspector, bool isOther)
            {
            if (isDesktopUnderwriter)
            {
                return AutomatedUnderwritingSystemBase.DesktopUnderwriter;
            }
            else if (isLoanProspector)
            {
                return AutomatedUnderwritingSystemBase.LoanProspector;
            }
            else if (isOther)
            {
                return AutomatedUnderwritingSystemBase.Other;
            }
            else
            {
                return AutomatedUnderwritingSystemBase.Blank;
            }
        }

        /// <summary>
        /// Retrieves the recommendation description based on the given VA risk.
        /// </summary>
        /// <param name="result">The VA risk.</param>
        /// <returns>The string description of the result.</returns>
        private static string GetAutomatedUnderwritingRecommendationDescription(E_sVaRiskT result)
        {
            switch (result)
            {
                case E_sVaRiskT.LeaveBlank:
                    return string.Empty;
                case E_sVaRiskT.Approve:
                    return "Approve";
                case E_sVaRiskT.Refer:
                    return "Refer";
                default:
                    throw new UnhandledEnumException(result);
            }
        }

        /// <summary>
        /// Retrieves the string result corresponding to the given automated underwriting result.
        /// </summary>
        /// <param name="result">The automated underwriting result.</param>
        /// <returns>The string description of the result.</returns>
        private static string GetAutomatedUnderwritingRecommendationDescription(E_sProd3rdPartyUwResultT result)
        {
            switch (result)
            {
                case E_sProd3rdPartyUwResultT.DU_ApproveEligible:
                    return "ApproveEligible";
                case E_sProd3rdPartyUwResultT.DU_ApproveIneligible:
                    return "ApproveIneligible";
                case E_sProd3rdPartyUwResultT.DU_EAIEligible:
                    return "EAIEligible";
                case E_sProd3rdPartyUwResultT.DU_EAIIEligible:
                    return "EAIIEligible";
                case E_sProd3rdPartyUwResultT.DU_EAIIIEligible:
                    return "EAIIIEligible";
                case E_sProd3rdPartyUwResultT.DU_ReferEligible:
                    return "ReferEligible";
                case E_sProd3rdPartyUwResultT.DU_ReferIneligible:
                    return "ReferIneligible";
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.DU_ReferWCautionIneligible:
                    return "ReferWithCautionIV";
                case E_sProd3rdPartyUwResultT.GUS_AcceptEligible:
                case E_sProd3rdPartyUwResultT.GUS_AcceptIneligible:
                    return "Accept";
                case E_sProd3rdPartyUwResultT.GUS_ReferEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferIneligible:
                    return "Refer";
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionEligible:
                case E_sProd3rdPartyUwResultT.GUS_ReferWCautionIneligible:
                    return "ReferWCaution";
                case E_sProd3rdPartyUwResultT.LP_AcceptEligible:
                case E_sProd3rdPartyUwResultT.LP_AcceptIneligible:
                    return "Accept";
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level1:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level2:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level3:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level4:
                case E_sProd3rdPartyUwResultT.Lp_AMinus_Level5:
                    return "CautionEligibleForAMinus";
                case E_sProd3rdPartyUwResultT.LP_CautionEligible:
                case E_sProd3rdPartyUwResultT.LP_CautionIneligible:
                    return "C1Caution";
                case E_sProd3rdPartyUwResultT.Lp_Refer:
                    return "Unknown";
                case E_sProd3rdPartyUwResultT.NA:
                    return string.Empty;
                case E_sProd3rdPartyUwResultT.OutOfScope:
                    return "OutofScope";
                case E_sProd3rdPartyUwResultT.Total_ApproveEligible:
                    return "ApproveEligible";
                case E_sProd3rdPartyUwResultT.Total_ApproveIneligible:
                    return "ApproveIneligible";
                case E_sProd3rdPartyUwResultT.Total_ReferEligible:
                    return "ReferEligible";
                case E_sProd3rdPartyUwResultT.Total_ReferIneligible:
                    return "ReferIneligible";
                default:
                    throw new UnhandledEnumException(result);
            }
        }

        /// <summary>
        /// Converts an LQB string into the appropriate FundsBase enumerated value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to a FundsBase enumeration.</param>
        /// <returns>A FundsBase enumerated value.</returns>
        private static FundsBase GetFundsBaseValue(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return FundsBase.Blank;
            }
            else if (string.Equals(lqbValue, "Checking/Savings", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.CheckingSavings;
            }
            else if (string.Equals(lqbValue, "Deposit on Sales Contract", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.DepositOnSalesContract;
            }
            else if (string.Equals(lqbValue, "Equity on Sold Property", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.EquityOnSoldProperty;
            }
            else if (string.Equals(lqbValue, "Equity from Pending Sale", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.EquityOnPendingSale;
            }
            else if (string.Equals(lqbValue, "Equity from Subject Property", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.EquityOnSubjectProperty;
            }
            else if (string.Equals(lqbValue, "Stocks & Bonds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.StocksAndBonds;
            }
            else if (string.Equals(lqbValue, "Lot Equity", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.LotEquity;
            }
            else if (string.Equals(lqbValue, "Bridge Loan", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.BridgeLoan;
            }
            else if (string.Equals(lqbValue, "Unsecured Borrowed Funds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.UnsecuredBorrowedFunds;
            }
            else if (string.Equals(lqbValue, "Trust Funds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.TrustFunds;
            }
            else if (string.Equals(lqbValue, "Retirement Funds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.RetirementFunds;
            }
            else if (string.Equals(lqbValue, "Rent with Option to Purchase", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.RentWithOptionToPurchase;
            }
            else if (string.Equals(lqbValue, "Life Insurance Cash Value", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.LifeInsuranceCashValue;
            }
            else if (string.Equals(lqbValue, "Sale of Chattel", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.SaleOfChattel;
            }
            else if (string.Equals(lqbValue, "Trade Equity", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.TradeEquity;
            }
            else if (string.Equals(lqbValue, "Sweat Equity", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.SweatEquity;
            }
            else if (string.Equals(lqbValue, "Cash on Hand", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.CashOnHand;
            }
            else if (string.Equals(lqbValue, "Secured Borrowed Funds", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.SecuredLoan;
            }
            else if (string.Equals(lqbValue, "Gift Funds", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source N/A", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Relative", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Government Assistance", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Employer", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Nonprofit/Religious/Community - Seller Funded", StringComparison.OrdinalIgnoreCase) ||
                     string.Equals(lqbValue, "FHA - Gift - Source Nonprofit/Religious/Community - Non-Seller Funded", StringComparison.OrdinalIgnoreCase))
            {
                return FundsBase.GiftFunds;
            }
            else
            {
                // For any custom option or "Other Type of Down Payment"
                return FundsBase.Other;
            }
        }

        /// <summary>
        /// Creates a container holding data on an adjustment for a construction ARM.
        /// </summary>
        /// <returns>An ADJUSTMENT container.</returns>
        private ADJUSTMENT CreateAdjustment_Construction()
        {
            var adjustment = new ADJUSTMENT();
            adjustment.InterestRateAdjustment = this.CreateInterestRateAdjustment_Construction();
            adjustment.RateOrPaymentChangeOccurrences = this.CreateRateOrPaymentChangeOccurrences_Construction();

            return adjustment;
        }

        /// <summary>
        /// Creates a container holding data about an HMDA loan.
        /// </summary>
        /// <returns>An HMDA_LOAN container.</returns>
        private HMDA_LOAN CreateHmdaLoan()
        {
            var hmdaLoan = new HMDA_LOAN();
            hmdaLoan.HmdaLoanDetail = new HMDA_LOAN_DETAIL();
            hmdaLoan.HmdaLoanDetail.HMDA_HOEPALoanStatusIndicator = ToMismoIndicator(this.DataLoan.sHmdaReportAsHoepaLoan);
            hmdaLoan.HmdaLoanDetail.HMDAPreapprovalType = ToMismoEnum(this.GetHmdaPreapprovalBaseValue(this.DataLoan.sHmdaPreapprovalT));
            hmdaLoan.HmdaLoanDetail.HMDAPurposeOfLoanType = ToMismoEnum(this.GetHmdaPurposeOfLoanBaseValue(this.DataLoan.sLPurposeT, this.DataLoan.sHmdaReportAsHomeImprov));
            hmdaLoan.HmdaLoanDetail.HMDARateSpreadPercent = ToMismoPercent(this.DataLoan.sHmdaAprRateSpread, this.DataLoan.m_convertLos);

            hmdaLoan.Extension = this.CreateHmdaLoanExtension();

            return hmdaLoan;
        }

        /// <summary>
        /// Creates an extension to the HMDA_LOAN container.
        /// </summary>
        /// <returns>An extension to the HMDA_LOAN container.</returns>
        private HMDA_LOAN_EXTENSION CreateHmdaLoanExtension()
        {
            var extension = new HMDA_LOAN_EXTENSION();
            extension.Other = this.CreateLqbHmdaLoanExtension();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LQB extension to the HMDA_LOAN container.
        /// </summary>
        /// <returns>A proprietary LQB extension to the HMDA_LOAN container.</returns>
        private LQB_HMDA_LOAN_EXTENSION CreateLqbHmdaLoanExtension()
        {
            var extension = new LQB_HMDA_LOAN_EXTENSION();
            extension.HmdaLarData = this.CreateLqbHmdaLarData();
            extension.HmdaReliedOn = this.CreateLqbHmdaReliedOn();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension element containing HMDA LAR data.
        /// </summary>
        /// <returns>A container holding HMDA LAR data.</returns>
        /// <remarks>Some datapoints use the MISMO 3.4 types as those contain HMDA data that is not captured in 3.3.</remarks>
        private LQB_HMDA_LAR_DATA CreateLqbHmdaLarData()
        {
            var hmdaLar = new LQB_HMDA_LAR_DATA();

            hmdaLar.GlobalLegalEntityIdentifier = ToMismoString(this.DataLoan.sLegalEntityIdentifier);
            hmdaLar.LoanIdentifier = ToMismoString(this.DataLoan.sUniversalLoanIdentifier);
            hmdaLar.ApplicationReceivedDate = ToMismoString(this.DataLoan.sHmdaApplicationDate_rep);
            hmdaLar.MortgageType = ToMismoEnum(this.GetMortgageBaseValue(this.DataLoan.sHmdaLoanTypeT));
            hmdaLar.HMDAPurposeOfLoanType = ToMismoEnum(
                CommonBuilder.GetHmdaPurposeOfLoanBaseValue(this.DataLoan.sHmdaLoanPurposeT));
            hmdaLar.HMDAPreapprovalType = ToMismoEnum(this.GetHmdaPreapprovalBaseValue(this.DataLoan.sHmdaPreapprovalT));
            hmdaLar.ConstructionMethodType = ToMismoEnum(this.GetConstructionMethodBaseValue(this.DataLoan.sHmdaConstructionMethT));
            hmdaLar.PropertyUsageType = ToMismoEnum(this.GetPropertyUsageBaseValue(this.DataLoan.sOccT));
            hmdaLar.BorrowerRequestedLoanAmount = ToMismoString(this.DataLoan.sHmdaLoanAmount_rep);
            hmdaLar.HMDADispositionType = ToMismoEnum(
                CommonBuilder.GetHmdaDispositionBaseValue(this.DataLoan.sHmdaActionTakenT));
            hmdaLar.HMDADispositionDate = ToMismoString(this.DataLoan.sHmdaActionD_rep);

            hmdaLar.AddressLineText = ToMismoString(this.DataLoan.sHmdaSpAddr);
            hmdaLar.CityName = ToMismoString(this.DataLoan.sHmdaSpCity);
            hmdaLar.StateCode = ToMismoString(this.DataLoan.sHmdaSpState);
            hmdaLar.PostalCode = ToMismoString(this.DataLoan.sHmdaSpZip);
            hmdaLar.FIPSCountyCode = ToMismoString(this.DataLoan.sHmda2018CountyCode);
            hmdaLar.CensusTractIdentifier = ToMismoString(this.DataLoan.sHmda2018CensusTract);

            hmdaLar.BorrowerHMDAEthnicityType1 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaBEthnicity1T));
            hmdaLar.BorrowerHMDAEthnicityType2 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaBEthnicity2T));
            hmdaLar.BorrowerHMDAEthnicityType3 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaBEthnicity3T));
            hmdaLar.BorrowerHMDAEthnicityType4 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaBEthnicity4T));
            hmdaLar.BorrowerHMDAEthnicityType5 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaBEthnicity5T));
            hmdaLar.BorrowerHMDAEthnicityTypeOtherDescription = ToMismoString(this.DataLoan.sHmdaBEthnicityOtherDescription);

            hmdaLar.CoborrowerHMDAEthnicityType1 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaCEthnicity1T));
            hmdaLar.CoborrowerHMDAEthnicityType2 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaCEthnicity2T));
            hmdaLar.CoborrowerHMDAEthnicityType3 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaCEthnicity3T));
            hmdaLar.CoborrowerHMDAEthnicityType4 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaCEthnicity4T));
            hmdaLar.CoborrowerHMDAEthnicityType5 = ToMismoEnum(this.GetLqbHmdaEthnicityAndOriginBaseValue(this.DataLoan.sHmdaCEthnicity5T));
            hmdaLar.CoborrowerHMDAEthnicityTypeOtherDescription = ToMismoString(this.DataLoan.sHmdaCEthnicityOtherDescription);

            hmdaLar.BorrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType = ToMismoEnum(this.GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(this.DataLoan.sHmdaBEthnicityCollectedByObservationOrSurname));
            hmdaLar.CoborrowerHMDAEthnicityCollectedBasedOnVisualObservationOrSurnameType = ToMismoEnum(this.GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(this.DataLoan.sHmdaCEthnicityCollectedByObservationOrSurname));

            hmdaLar.BorrowerHMDARaceType1 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaBRace1T));
            hmdaLar.BorrowerHMDARaceType2 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaBRace2T));
            hmdaLar.BorrowerHMDARaceType3 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaBRace3T));
            hmdaLar.BorrowerHMDARaceType4 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaBRace4T));
            hmdaLar.BorrowerHMDARaceType5 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaBRace5T));
            hmdaLar.BorrowerHMDAEnrolledOrPrincipalTribe = ToMismoString(this.DataLoan.sHmdaBEnrolledOrPrincipalTribe);
            hmdaLar.BorrowerHMDARaceDesignationTypeOtherAsianDescription = ToMismoString(this.DataLoan.sHmdaBOtherAsianDescription);
            hmdaLar.BorrowerHMDARaceDesignationTypeOtherPacificIslanderDescription = ToMismoString(this.DataLoan.sHmdaBOtherPacificIslanderDescription);

            hmdaLar.CoborrowerHMDARaceType1 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaCRace1T));
            hmdaLar.CoborrowerHMDARaceType2 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaCRace2T));
            hmdaLar.CoborrowerHMDARaceType3 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaCRace3T));
            hmdaLar.CoborrowerHMDARaceType4 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaCRace4T));
            hmdaLar.CoborrowerHMDARaceType5 = ToMismoEnum(this.GetLqbHmdaRaceAndDesignationBaseValue(this.DataLoan.sHmdaCRace5T));
            hmdaLar.CoborrowerHMDAEnrolledOrPrincipalTribe = ToMismoString(this.DataLoan.sHmdaCEnrolledOrPrincipalTribe);
            hmdaLar.CoborrowerHMDARaceDesignationTypeOtherAsianDescription = ToMismoString(this.DataLoan.sHmdaCOtherAsianDescription);
            hmdaLar.CoborrowerHMDARaceDesignationTypeOtherPacificIslanderDescription = ToMismoString(this.DataLoan.sHmdaCOtherPacificIslanderDescription);

            hmdaLar.BorrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType = ToMismoEnum(this.GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(this.DataLoan.sHmdaBRaceCollectedByObservationOrSurname));
            hmdaLar.CoborrowerHMDARaceCollectedBasedOnVisualObservationOrSurnameType = ToMismoEnum(this.GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(this.DataLoan.sHmdaCRaceCollectedByObservationOrSurname));

            hmdaLar.BorrowerHMDAGenderType = ToMismoEnum(this.GetHmdaGenderBaseValue(this.DataLoan.sHmdaBSexT));
            hmdaLar.CoborrowerHMDAGenderType = ToMismoEnum(this.GetHmdaGenderBaseValue(this.DataLoan.sHmdaCSexT));

            hmdaLar.BorrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType = ToMismoEnum(this.GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(this.DataLoan.sHmdaBSexCollectedByObservationOrSurname));
            hmdaLar.CoborrowerHMDAGenderCollectedBasedOnVisualObservationOrSurnameType = ToMismoEnum(this.GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(this.DataLoan.sHmdaCSexCollectedByObservationOrSurname));

            hmdaLar.BorrowerAgeAtApplicationYearsCount = ToMismoString(this.DataLoan.sHmdaBAge_rep);
            hmdaLar.CoborrowerAgeAtApplicationYearsCount = ToMismoString(this.DataLoan.sHmdaCAge_rep);

            hmdaLar.TotalMonthlyIncomeAmount = ToMismoString(this.DataLoan.sHmdaIncome_rep);
            hmdaLar.HMDAPurchaserType = ToMismoEnum(this.GetHmdaPurchaserBaseValue(this.DataLoan.sHmdaPurchaser2015T));
            hmdaLar.HMDARateSpreadPercent = ToMismoString(this.DataLoan.sHmda2018AprRateSpread);
            hmdaLar.HMDAHOEPALoanStatusType = ToMismoEnum(this.GetLqbHmdaHoepaLoanStatusBaseValue(this.DataLoan.sHmdaHoepaStatusT));
            hmdaLar.LienPriorityType = ToMismoEnum(this.GetLqbLienPriorityBaseValue(this.DataLoan.sHmdaLienT));

            hmdaLar.BorrowerCreditScoreValue = ToMismoString(this.DataLoan.sHmdaBCreditScore_rep);
            hmdaLar.CoborrowerCreditScoreValue = ToMismoString(this.DataLoan.sHmdaCCreditScore_rep);
            hmdaLar.BorrowerCreditScoreModelNameType = ToMismoEnum(this.GetLqbCreditScoreModelNameBaseValue(this.DataLoan.sHmdaBCreditScoreModelT));
            hmdaLar.BorrowerCreditScoreModelNameTypeOtherDescription = ToMismoString(this.DataLoan.sHmdaBCreditScoreModelOtherDescription);
            hmdaLar.CoborrowerCreditScoreModelNameType = ToMismoEnum(this.GetLqbCreditScoreModelNameBaseValue(this.DataLoan.sHmdaCCreditScoreModelT));
            hmdaLar.CoborrowerCreditScoreModelNameTypeOtherDescription = ToMismoString(this.DataLoan.sHmdaCCreditScoreModelOtherDescription);

            hmdaLar.HMDAReasonForDenialType1 = ToMismoEnum(GetHmdaReasonForDenialBaseValue(this.DataLoan.sHmdaDenialReasonT1));
            hmdaLar.HMDAReasonForDenialType2 = ToMismoEnum(GetHmdaReasonForDenialBaseValue(this.DataLoan.sHmdaDenialReasonT2));
            hmdaLar.HMDAReasonForDenialType3 = ToMismoEnum(GetHmdaReasonForDenialBaseValue(this.DataLoan.sHmdaDenialReasonT3));
            hmdaLar.HMDAReasonForDenialType4 = ToMismoEnum(GetHmdaReasonForDenialBaseValue(this.DataLoan.sHmdaDenialReasonT4));
            hmdaLar.HMDAReasonForDenialTypeOtherDescription = ToMismoString(this.DataLoan.sHmdaDenialReasonOtherDescription);

            if (this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.GFE)
            {
                hmdaLar.TotalPointsAndFeesAmount = ToMismoString(this.DataLoan.sHmdaTotalLoanCosts_rep);
            }
            else
            {
                hmdaLar.TotalLoanCostsAmount = ToMismoString(this.DataLoan.sHmdaTotalLoanCosts_rep);
            }

            hmdaLar.OriginationChargesAmount = ToMismoString(this.DataLoan.sHmdaOriginationCharge_rep);
            hmdaLar.DiscountPointsAmount = ToMismoString(this.DataLoan.sHmdaDiscountPoints_rep);
            hmdaLar.LenderCreditsAmount = ToMismoString(this.DataLoan.sHmdaLenderCredits_rep);
            hmdaLar.NoteRatePercent = ToMismoString(this.DataLoan.sHmdaInterestRate_rep);
            hmdaLar.PrepaymentPenaltyExpirationMonthsCount = ToMismoString(this.DataLoan.sHmdaPrepaymentTerm_rep);
            hmdaLar.TotalDebtExpenseRatioPercent = ToMismoString(this.DataLoan.sHmdaDebtRatio_rep);
            hmdaLar.CombinedLTVRatioPercent = ToMismoString(this.DataLoan.sHmdaCombinedRatio_rep);
            hmdaLar.LoanMaturityPeriodCount = ToMismoString(this.DataLoan.sHmdaTerm_rep);
            hmdaLar.FirstRateChangeMonthsCount = ToMismoString(this.DataLoan.sHmdaIntroductoryPeriod_rep);

            var hasBalloonPayment = this.DataLoan.sHmdaBalloonPaymentT;
            hmdaLar.BalloonPaymentIndicator = hasBalloonPayment == sHmdaBalloonPaymentT.LeaveBlank ?
                null : ToMismoIndicator(hasBalloonPayment == sHmdaBalloonPaymentT.BalloonPayment);

            var hasInterestOnlyPayments = this.DataLoan.sHmdaInterestOnlyPaymentT;
            hmdaLar.InterestOnlyIndicator = hasInterestOnlyPayments == sHmdaInterestOnlyPaymentT.LeaveBlank ?
                null : ToMismoIndicator(hasInterestOnlyPayments == sHmdaInterestOnlyPaymentT.InterestOnlyPayments);

            var hasNegativeAmortization = this.DataLoan.sHmdaNegativeAmortizationT;
            hmdaLar.NegativeAmortizationIndicator = hasNegativeAmortization == sHmdaNegativeAmortizationT.LeaveBlank ?
                null : ToMismoIndicator(hasNegativeAmortization == sHmdaNegativeAmortizationT.NegativeAmortization);

            var hasOtherNonAmortizingFeatures = this.DataLoan.sHmdaOtherNonAmortFeatureT;
            hmdaLar.HMDAOtherNonAmortizingFeaturesIndicator = hasOtherNonAmortizingFeatures == sHmdaOtherNonAmortFeatureT.LeaveBlank ?
                null : ToMismoIndicator(hasOtherNonAmortizingFeatures == sHmdaOtherNonAmortFeatureT.OtherNonFullyAmortizingFeatures);

            hmdaLar.PropertyValuationAmount = ToMismoString(this.DataLoan.sHmdaPropertyValue_rep);
            hmdaLar.ManufacturedHomeSecuredPropertyType = ToMismoEnum(this.GetLqbManufacturedHomeSecuredPropertyBaseValue(this.DataLoan.sHmdaManufacturedTypeT));
            hmdaLar.ManufacturedHomeLandPropertyInterestType = ToMismoEnum(this.GetLqbManufacturedHomeLandPropertyInterestBaseValue(this.DataLoan.sHmdaManufacturedInterestT));
            hmdaLar.FinancedUnitCount = ToMismoString(this.DataLoan.sHmdaTotalUnits_rep);
            hmdaLar.AffordableUnitsCount = ToMismoString(this.DataLoan.sHmdaMultifamilyUnits_rep);

            hmdaLar.HMDAApplicationSubmissionType = ToMismoEnum(
                GetHmdaApplicationSubmissionBaseValue(this.DataLoan.sHmdaSubmissionApplicationT));
            hmdaLar.HMDACoveredLoanInitiallyPayableToReportingInstitutionStatusType = ToMismoEnum(
                GetHmdaCoveredLoanInitiallyPayableToReportingInstitutionStatusBaseValue(this.DataLoan.sHmdaInitiallyPayableToInstitutionT));
            hmdaLar.LoanOriginatorNMLSIdentifier = ToMismoString(this.DataLoan.sApp1003InterviewerLoanOriginatorIdentifier);

            hmdaLar.AutomatedUnderwritingSystemType1 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystem1T));
            hmdaLar.AutomatedUnderwritingSystemType2 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystem2T));
            hmdaLar.AutomatedUnderwritingSystemType3 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystem3T));
            hmdaLar.AutomatedUnderwritingSystemType4 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystem4T));
            hmdaLar.AutomatedUnderwritingSystemType5 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystem5T));
            hmdaLar.AutomatedUnderwritingSystemTypeOtherDescription = ToMismoString(this.DataLoan.sHmdaAutomatedUnderwritingSystemOtherDescription);

            hmdaLar.AutomatedUnderwritingSystemResultType1 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemResultBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystemResult1T));
            hmdaLar.AutomatedUnderwritingSystemResultType2 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemResultBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystemResult2T));
            hmdaLar.AutomatedUnderwritingSystemResultType3 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemResultBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystemResult3T));
            hmdaLar.AutomatedUnderwritingSystemResultType4 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemResultBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystemResult4T));
            hmdaLar.AutomatedUnderwritingSystemResultType5 = ToMismoEnum(this.GetLqbHmdaAutomatedUnderwritingSystemResultBaseValue(this.DataLoan.sHmdaAutomatedUnderwritingSystemResult5T));
            hmdaLar.AutomatedUnderwritingSystemResultTypeOtherDescription = ToMismoString(this.DataLoan.sHmdaAutomatedUnderwritingSystemResultOtherDescription);

            var isReverseMortgage = this.DataLoan.sHmdaReverseMortgageT;
            hmdaLar.ReverseMortgageIndicator = isReverseMortgage == sHmdaReverseMortgageT.LeaveBlank ?
                null : ToMismoIndicator(isReverseMortgage == sHmdaReverseMortgageT.ReverseMortgage);
            hmdaLar.OpenEndCreditIndicator = ToMismoIndicator(this.DataLoan.sIsLineOfCredit);

            var hasBusinessPurpose = this.DataLoan.sHmdaBusinessPurposeT;
            hmdaLar.HMDABusinessPurposeIndicator = hasBusinessPurpose == sHmdaBusinessPurposeT.LeaveBlank ?
                 null : ToMismoIndicator(hasBusinessPurpose == sHmdaBusinessPurposeT.PrimarilyForBusinessOrCommercial);

            return hmdaLar;
        }

        /// <summary>
        /// Creates a container holding HMDA relied-on data.
        /// </summary>
        /// <returns>An LQB_HMDA_RELIED_ON container.</returns>
        private LQB_HMDA_RELIED_ON CreateLqbHmdaReliedOn()
        {
            var reliedOn = new LQB_HMDA_RELIED_ON();

            reliedOn.TotalAnnualIncomeReliedOnIndicator = ToMismoIndicator(this.DataLoan.sHmdaIsIncomeReliedOn);
            reliedOn.ReliedOnTotalAnnualIncomeAmount = ToMismoAmount(this.DataLoan.sReliedOnTotalIncome_rep);

            reliedOn.DebtToIncomeRatioReliedOnIndicator = ToMismoIndicator(this.DataLoan.sHmdaIsDebtRatioReliedOn);
            reliedOn.ReliedOnDebtToIncomeRatioPercent = ToMismoPercent(this.DataLoan.sReliedOnDebtRatio_rep);

            reliedOn.CombinedLoanToValueRatioReliedOnIndicator = ToMismoIndicator(this.DataLoan.sHmdaIsCombinedRatioReliedOn);
            reliedOn.ReliedOnCombinedLoanToValueRatioPercent = ToMismoPercent(this.DataLoan.sReliedOnCombinedLTVRatio_rep);

            reliedOn.PropertyValueReliedOnIndicator = ToMismoIndicator(this.DataLoan.sHmdaIsPropertyValueReliedOn);
            reliedOn.ReliedOnPropertyValueAmount = ToMismoAmount(this.DataLoan.sReliedOnPropertyValue_rep);

            reliedOn.CreditScoreModeReliedOnIndicator = ToMismoIndicator(this.DataLoan.sHmdaIsCreditScoreReliedOn);
            reliedOn.ReliedOnCreditScoreModeType = ToMismoEnum(this.GetLqbReliedOnCreditScoreModeBaseValue(this.DataLoan.sReliedOnCreditScoreModeT));

            return reliedOn;
        }

        /// <summary>
        /// Creates a container holding data on the amortization of a loan.
        /// </summary>
        /// <returns>An AMORTIZATION container.</returns>
        private AMORTIZATION CreateAmortization()
        {
            var amortization = new AMORTIZATION();
            amortization.AmortizationRule = this.CreateAmortizationRule();

            return amortization;
        }

        /// <summary>
        /// Creates a container holding data on the rules governing amortization.
        /// </summary>
        /// <returns>An AMORTIZATION_RULE populated with data.</returns>
        private AMORTIZATION_RULE CreateAmortizationRule()
        {
            var amortizationRule = new AMORTIZATION_RULE();
            amortizationRule.AmortizationType = ToMismoEnum(this.GetAmortizationBaseValue(this.DataLoan.sFinMethT));
            amortizationRule.AmortizationTypeOtherDescription = ToMismoString(this.DataLoan.sFinMethDesc);
            amortizationRule.LoanAmortizationPeriodCount = ToMismoCount(this.DataLoan.sTerm_rep, null);
            amortizationRule.LoanAmortizationPeriodType = ToMismoEnum(LoanAmortizationPeriodBase.Month);

            return amortizationRule;
        }

        /// <summary>
        /// Creates a container holding information about the assumption option of the loan.
        /// </summary>
        /// <returns>An ASSUMABILITY container.</returns>
        private ASSUMABILITY CreateAssumability()
        {
            if (this.DataLoan.sAssumeLT == E_sAssumeLT.LeaveBlank || this.DataLoan.sAssumeLT == E_sAssumeLT.MayNot)
            {
                return null;
            }

            var assumability = new ASSUMABILITY();
            assumability.AssumabilityRule = this.CreateAssumabilityRule();

            return assumability;
        }

        /// <summary>
        /// Creates a container holding data on the rules of the assumption option.
        /// </summary>
        /// <returns>An ASSUMABILITY_RULE container populated with data.</returns>
        private ASSUMABILITY_RULE CreateAssumabilityRule()
        {
            if (this.DataLoan.sAssumeLT == E_sAssumeLT.LeaveBlank || this.DataLoan.sAssumeLT == E_sAssumeLT.MayNot)
            {
                return null;
            }

            var assumabilityRule = new ASSUMABILITY_RULE();

            // MaySubjectToCondition maps to true, May maps to false.
            bool conditions = this.DataLoan.sAssumeLT == E_sAssumeLT.MaySubjectToCondition;
            assumabilityRule.ConditionsToAssumabilityIndicator = ToMismoIndicator(conditions);

            return assumabilityRule;
        }

        /// <summary>
        /// Converts an LQB enum into the appropriate AmortizationBase enumerated value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to an Amortization enumeration.</param>
        /// <returns>An AmortizationBase enumerated value.</returns>
        private AmortizationBase GetAmortizationBaseValue(E_sFinMethT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sFinMethT.ARM:
                    return AmortizationBase.AdjustableRate;
                case E_sFinMethT.Fixed:
                    return AmortizationBase.Fixed;
                case E_sFinMethT.Graduated:
                    return AmortizationBase.GraduatedPaymentARM;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the HMDA preapproval value corresponding to the given preapproval request status.
        /// </summary>
        /// <param name="preapproval">The preapproval request status.</param>
        /// <returns>The corresponding HMDA preapproval value.</returns>
        private HMDAPreapprovalBase GetHmdaPreapprovalBaseValue(E_sHmdaPreapprovalT preapproval)
        {
            switch (preapproval)
            {
                case E_sHmdaPreapprovalT.NotApplicable:
                    return HMDAPreapprovalBase.NotApplicable;
                case E_sHmdaPreapprovalT.PreapprovalNotRequested:
                    return HMDAPreapprovalBase.PreapprovalWasNotRequested;
                case E_sHmdaPreapprovalT.PreapprovalRequested:
                    return HMDAPreapprovalBase.PreapprovalWasRequested;
                case E_sHmdaPreapprovalT.LeaveBlank:
                    return HMDAPreapprovalBase.Blank;
                default:
                    throw new UnhandledEnumException(preapproval);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the collection status.
        /// </summary>
        /// <param name="collectionStatus">The data collection status.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase GetLqbHmdaCollectedBasedOnVisualObservationOrSurnameBaseValue(VisualObservationSurnameCollection collectionStatus)
        {
            switch (collectionStatus)
            {
                case VisualObservationSurnameCollection.CollectedOnBasisOfVisualObservationOrSurname:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.CollectedBasedOnVisualObservationOrSurname;
                case VisualObservationSurnameCollection.NotCollectedOnBasisOfVisualObservationOrSurname:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.NotCollectedBasedOnVisualObservationOrSurname;
                case VisualObservationSurnameCollection.NotApplicable:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.NotApplicable;
                case VisualObservationSurnameCollection.NoCoApplicant:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.NoCoApplicant;
                case VisualObservationSurnameCollection.Blank:
                    return LqbHmdaCollectedBasedOnVisualObservationOrSurnameBase.Blank;
                default:
                    throw new UnhandledEnumException(collectionStatus);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to an applicant's sex.
        /// </summary>
        /// <param name="sex">The applicant's sex.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaGenderBase GetHmdaGenderBaseValue(Hmda_Sex sex)
        {
            switch (sex)
            {
                case Hmda_Sex.BothMaleFemale:
                    return LqbHmdaGenderBase.ApplicantHasSelectedBothMaleAndFemale;
                case Hmda_Sex.Female:
                    return LqbHmdaGenderBase.Female;
                case Hmda_Sex.Male:
                    return LqbHmdaGenderBase.Male;
                case Hmda_Sex.NotApplicable:
                    return LqbHmdaGenderBase.NotApplicable;
                case Hmda_Sex.NotProvided:
                    return LqbHmdaGenderBase.InformationNotProvidedUnknown;
                case Hmda_Sex.NoCoApplicant:
                    return LqbHmdaGenderBase.NoCoApplicant;
                case Hmda_Sex.Blank:
                    return LqbHmdaGenderBase.LeaveBlank;
                default:
                    throw new UnhandledEnumException(sex);
            }
        }

        /// <summary>
        /// Creates a container holding a set of high cost mortgage information.
        /// </summary>
        /// <returns>A HIGH_COST_MORTGAGES container.</returns>
        private HIGH_COST_MORTGAGES CreateHighCostMortgages()
        {
            var highCostMortgages = new HIGH_COST_MORTGAGES();
            highCostMortgages.HighCostMortgageList.Add(this.CreateHighCostMortgage());

            return highCostMortgages;
        }

        /// <summary>
        /// Creates a container holding information on a high cost mortgage.
        /// </summary>
        /// <returns>A HIGH_COST_MORTGAGE container.</returns>
        private HIGH_COST_MORTGAGE CreateHighCostMortgage()
        {
            var highCostMortgage = new HIGH_COST_MORTGAGE();
            highCostMortgage.SequenceNumber = 1;

            highCostMortgage.AveragePrimeOfferRatePercent = ToMismoPercent(this.DataLoan.sQMAveragePrimeOfferR_rep);
            highCostMortgage.RegulationZExcludedBonaFideDiscountPointsIndicator = ToMismoIndicator(this.DataLoan.sGfeDiscountPointFProps_BF);
            highCostMortgage.RegulationZExcludedBonaFideDiscountPointsPercent = ToMismoPercent(this.DataLoan.sExcludedBonaFideDiscountPointsPc_rep);
            highCostMortgage.RegulationZTotalLoanAmount = ToMismoAmount(this.DataLoan.sQMLAmt_rep);
            highCostMortgage.RegulationZTotalPointsAndFeesAmount = ToMismoAmount(this.DataLoan.sQMTotFeeAmt_rep);
            highCostMortgage.RegulationZTotalPointsAndFeesPercent = ToMismoPercent(this.DataLoan.sQMTotFeePc_rep);

            return highCostMortgage;
        }

        /// <summary>
        /// Creates a container holding HELOC information.
        /// </summary>
        /// <returns>A HELOC container, or null if the loan is not a HELOC.</returns>
        private HELOC CreateHeloc()
        {
            if (!this.DataLoan.BrokerDB.IsEnableHELOC || !this.DataLoan.sIsLineOfCredit)
            {
                return null;
            }

            var heloc = new HELOC();
            heloc.HelocRule = this.CreateHelocRule();
            heloc.HelocOccurrences = this.CreateHelocOccurrences();

            return heloc;
        }

        /// <summary>
        /// Creates a container holding data on a HELOC occurrence.
        /// </summary>
        /// <param name="sequence">The sequence number of the occurrence among the list of occurrences.</param>
        /// <returns>A HELOC_OCCURRENCE container populated with data.</returns>
        private HELOC_OCCURRENCE CreateHelocOccurrence(int sequence)
        {
            var helocOccurrence = new HELOC_OCCURRENCE();
            helocOccurrence.HELOCDailyPeriodicInterestRatePercent = ToMismoPercent(this.DataLoan.sDailyPeriodicR_rep);
            helocOccurrence.SequenceNumber = sequence;

            return helocOccurrence;
        }

        /// <summary>
        /// Creates a container holding information on a subordinate HELOC.
        /// </summary>
        /// <param name="currentMax">The maximum balance of the current HELOC.</param>
        /// <param name="subordinateAmount">The balance of the HELOC.</param>
        /// <param name="subordinateRate">The daily interest rate of the HELOC.</param>
        /// <param name="sequence">The sequence number of the occurrence among the list of occurrences.</param>
        /// <returns>A HELOC_OCCURRENCE container.</returns>
        private HELOC_OCCURRENCE CreateHelocOccurrence(string currentMax, string subordinateAmount, string subordinateRate, int sequence)
        {
            var helocOccurrence = new HELOC_OCCURRENCE();
            helocOccurrence.CurrentHELOCMaximumBalanceAmount = ToMismoAmount(currentMax);
            helocOccurrence.HELOCBalanceAmount = ToMismoAmount(subordinateAmount);
            helocOccurrence.HELOCDailyPeriodicInterestRatePercent = ToMismoPercent(subordinateRate);
            helocOccurrence.SequenceNumber = sequence;

            return helocOccurrence;
        }

        /// <summary>
        /// Creates a container holding all instances of HELOC_OCCURRENCE.
        /// </summary>
        /// <returns>A HELOC_OCCURRENCES container.</returns>
        private HELOC_OCCURRENCES CreateHelocOccurrences()
        {
            var helocOccurrences = new HELOC_OCCURRENCES();

            if (this.DataLoan.sIsOFinCreditLineInDrawPeriod)
            {
                // Add fields related to a subordinate HELOC.
                helocOccurrences.HelocOccurrenceList.Add(this.CreateHelocOccurrence(this.DataLoan.sSubFin_rep, this.DataLoan.sConcurSubFin_rep, this.DataLoan.sSubFinIR_rep, 1));
            }
            else
            {
                helocOccurrences.HelocOccurrenceList.Add(this.CreateHelocOccurrence(1));
            }

            return helocOccurrences;
        }

        /// <summary>
        /// Creates a container holding data on a HELOC.
        /// </summary>
        /// <returns>A HELOC_RULE populated with data.</returns>
        private HELOC_RULE CreateHelocRule()
        {
            var helocRule = new HELOC_RULE();
            helocRule.HELOCAnnualFeeAmount = ToMismoAmount(this.DataLoan.sHelocAnnualFee_rep);
            helocRule.HELOCCreditLineDrawAccessFeeAmount = ToMismoAmount(this.DataLoan.sHelocDrawFee_rep);
            helocRule.HELOCDailyPeriodicInterestRateCalculationType = ToMismoEnum(this.GetHelocDailyPeriodicInterestRateCalculationBaseValue(this.DataLoan.sDaysInYr_rep));
            helocRule.HELOCInitialAdvanceAmount = ToMismoAmount(this.DataLoan.sLAmtCalc_rep);
            helocRule.HELOCMaximumAPRPercent = ToMismoPercent(this.DataLoan.sRLifeCapR_rep);
            helocRule.HELOCMaximumBalanceAmount = ToMismoAmount(this.DataLoan.sCreditLineAmt_rep);
            helocRule.HELOCMinimumAdvanceAmount = ToMismoAmount(this.DataLoan.sHelocMinimumAdvanceAmt_rep);
            helocRule.HELOCMinimumPaymentAmount = ToMismoAmount(this.DataLoan.sHelocMinimumPayment_rep);
            helocRule.HELOCMinimumPaymentPercent = ToMismoPercent(this.DataLoan.sHelocMinimumPaymentPc_rep);
            helocRule.HELOCRepayPeriodMonthsCount = ToMismoCount(this.DataLoan.sHelocRepay_rep, null);
            helocRule.HELOCReturnedCheckChargeAmount = ToMismoAmount(this.DataLoan.sHelocReturnedCheckFee_rep);
            helocRule.HELOCStopPaymentChargeAmount = ToMismoAmount(this.DataLoan.sHelocStopPaymentFee_rep);
            helocRule.HELOCTeaserMarginRatePercent = ToMismoPercent(this.DataLoan.sNoteIR_rep);
            helocRule.HELOCTeaserRateType = ToMismoEnum(this.GetHelocTeaserRateBase(this.DataLoan.sInitialRateT));
            helocRule.HELOCTeaserTermMonthsCount = ToMismoCount(this.DataLoan.sRAdj1stCapMon_rep, null);
            helocRule.HELOCTerminationFeeAmount = ToMismoAmount(this.DataLoan.sHelocTerminationFee_rep);
            helocRule.HELOCTerminationPeriodMonthsCount = ToMismoCount(this.DataLoan.sHelocTerminationFeePeriod_rep, null);

            return helocRule;
        }

        /// <summary>
        /// Retrieves the HELOCDailyPeriodicInterestRateCalculation value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding HELOCDailyPeriodicInterestRateCalculation value.</returns>
        private HELOCDailyPeriodicInterestRateCalculationBase GetHelocDailyPeriodicInterestRateCalculationBaseValue(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return HELOCDailyPeriodicInterestRateCalculationBase.Blank;
            }

            switch (lqbValue)
            {
                case "360":
                    return HELOCDailyPeriodicInterestRateCalculationBase.Item360;
                case "365":
                    return HELOCDailyPeriodicInterestRateCalculationBase.Item365;
                default:
                    throw new ArgumentException("Unhandled value='" + lqbValue + "' for sDaysInYr");
            }
        }

        /// <summary>
        /// Gets the MISMO mortgage type value corresponding to the loan type.
        /// </summary>
        /// <param name="loanType">The loan type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private MortgageBase GetMortgageBaseValue(sHmdaLoanTypeT loanType)
        {
            switch (loanType)
            {
                case sHmdaLoanTypeT.Conventional:
                    return MortgageBase.Conventional;
                case sHmdaLoanTypeT.FHA:
                    return MortgageBase.FHA;
                case sHmdaLoanTypeT.USDA:
                    return MortgageBase.USDARuralDevelopment;
                case sHmdaLoanTypeT.VA:
                    return MortgageBase.VA;
                case sHmdaLoanTypeT.LeaveBlank:
                    return MortgageBase.Blank;
                default:
                    throw new UnhandledEnumException(loanType);
            }
        }

        /// <summary>
        /// Gets the construction method type.
        /// </summary>
        /// <param name="method">The construction method.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private ConstructionMethodBase GetConstructionMethodBaseValue(sHmdaConstructionMethT method)
        {
            switch (method)
            {
                case sHmdaConstructionMethT.ManufacturedHome:
                    return ConstructionMethodBase.Manufactured;
                case sHmdaConstructionMethT.SiteBuilt:
                    return ConstructionMethodBase.SiteBuilt;
                case sHmdaConstructionMethT.LeaveBlank:
                    return ConstructionMethodBase.Blank;
                default:
                    throw new UnhandledEnumException(method);
            }
        }

        /// <summary>
        /// Retrieves the property usage value corresponding to the occupancy type.
        /// </summary>
        /// <param name="occupancyType">The occupancy type.</param>
        /// <returns>The corresponding property usage value.</returns>
        private PropertyUsageBase GetPropertyUsageBaseValue(E_sOccT occupancyType)
        {
            switch (occupancyType)
            {
                case E_sOccT.Investment:
                    return PropertyUsageBase.Investment;
                case E_sOccT.PrimaryResidence:
                    return PropertyUsageBase.PrimaryResidence;
                case E_sOccT.SecondaryResidence:
                    return PropertyUsageBase.SecondHome;
                default:
                    throw new UnhandledEnumException(occupancyType);
            }
        }

        /// <summary>
        /// Retrieves the HMDA loan purpose value corresponding to the LQB loan purpose.
        /// </summary>
        /// <param name="loanPurpose">The purpose of the loan.</param>
        /// <param name="reportedAsHomeImprovement">Indicates whether the purpose of the loan is to finance a home improvement.</param>
        /// <returns>The corresponding HMDA loan purpose value.</returns>
        private HMDAPurposeOfLoanBase GetHmdaPurposeOfLoanBaseValue(E_sLPurposeT loanPurpose, bool reportedAsHomeImprovement)
        {
            switch (loanPurpose)
            {
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    return HMDAPurposeOfLoanBase.HomePurchase;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.HomeEquity:
                    return reportedAsHomeImprovement ? HMDAPurposeOfLoanBase.HomeImprovement : HMDAPurposeOfLoanBase.Refinancing;
                case E_sLPurposeT.Other:
                    return HMDAPurposeOfLoanBase.Blank;
                case E_sLPurposeT.Purchase:
                    return HMDAPurposeOfLoanBase.HomePurchase;
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.VaIrrrl:
                    return reportedAsHomeImprovement ? HMDAPurposeOfLoanBase.HomeImprovement : HMDAPurposeOfLoanBase.Refinancing;
                default:
                    throw new UnhandledEnumException(loanPurpose);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to an applicant's ethnicity or ethnicity origin.
        /// </summary>
        /// <param name="ethnicity">The applicant's ethnicity or ethnicity origin.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaEthnicityAndOriginBase GetLqbHmdaEthnicityAndOriginBaseValue(Hmda_Ethnicity ethnicity)
        {
            switch (ethnicity)
            {
                case Hmda_Ethnicity.Cuban:
                    return LqbHmdaEthnicityAndOriginBase.Cuban;
                case Hmda_Ethnicity.HispanicOrLatino:
                    return LqbHmdaEthnicityAndOriginBase.HispanicOrLatino;
                case Hmda_Ethnicity.Mexican:
                    return LqbHmdaEthnicityAndOriginBase.Mexican;
                case Hmda_Ethnicity.NotApplicable:
                    return LqbHmdaEthnicityAndOriginBase.NotApplicable;
                case Hmda_Ethnicity.NotHispanicOrLatino:
                    return LqbHmdaEthnicityAndOriginBase.NotHispanicOrLatino;
                case Hmda_Ethnicity.NotProvided:
                    return LqbHmdaEthnicityAndOriginBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
                case Hmda_Ethnicity.OtherHispanicOrLatino:
                    return LqbHmdaEthnicityAndOriginBase.Other;
                case Hmda_Ethnicity.PuertoRican:
                    return LqbHmdaEthnicityAndOriginBase.PuertoRican;
                case Hmda_Ethnicity.NoCoApplicant:
                    return LqbHmdaEthnicityAndOriginBase.NoCoApplicant;
                case Hmda_Ethnicity.Blank:
                    return LqbHmdaEthnicityAndOriginBase.Blank;
                default:
                    throw new UnhandledEnumException(ethnicity);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the HOEPA status.
        /// </summary>
        /// <param name="hoepaStatus">The loan's HOEPA status.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaHoepaLoanStatusBase GetLqbHmdaHoepaLoanStatusBaseValue(sHmdaHoepaStatusT hoepaStatus)
        {
            switch (hoepaStatus)
            {
                case sHmdaHoepaStatusT.HighCost:
                    return LqbHmdaHoepaLoanStatusBase.HighCost;
                case sHmdaHoepaStatusT.NotHighCost:
                    return LqbHmdaHoepaLoanStatusBase.NotHighCost;
                case sHmdaHoepaStatusT.NA:
                    return LqbHmdaHoepaLoanStatusBase.NotApplicable;
                case sHmdaHoepaStatusT.LeaveBlank:
                    return LqbHmdaHoepaLoanStatusBase.Blank;
                default:
                    throw new UnhandledEnumException(hoepaStatus);
            }
        }

        /// <summary>
        /// Gets the property interest value.
        /// </summary>
        /// <param name="interestType">The property interest type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbManufacturedHomeLandPropertyInterestBase GetLqbManufacturedHomeLandPropertyInterestBaseValue(sHmdaManufacturedInterestT interestType)
        {
            switch (interestType)
            {
                case sHmdaManufacturedInterestT.DirectOwnership:
                    return LqbManufacturedHomeLandPropertyInterestBase.DirectOwnership;
                case sHmdaManufacturedInterestT.IndirectOwnership:
                    return LqbManufacturedHomeLandPropertyInterestBase.IndirectOwnership;
                case sHmdaManufacturedInterestT.NA:
                    return LqbManufacturedHomeLandPropertyInterestBase.NotApplicable;
                case sHmdaManufacturedInterestT.PaidLeasehold:
                    return LqbManufacturedHomeLandPropertyInterestBase.PaidLeasehold;
                case sHmdaManufacturedInterestT.UnpaidLeasehold:
                    return LqbManufacturedHomeLandPropertyInterestBase.UnpaidLeasehold;
                case sHmdaManufacturedInterestT.LeaveBlank:
                    return LqbManufacturedHomeLandPropertyInterestBase.LeaveBlank;
                default:
                    throw new UnhandledEnumException(interestType);
            }
        }

        /// <summary>
        /// Gets the secured property value.
        /// </summary>
        /// <param name="securedPropertyType">The manufactured home property type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbManufacturedHomeSecuredPropertyBase GetLqbManufacturedHomeSecuredPropertyBaseValue(sHmdaManufacturedTypeT securedPropertyType)
        {
            switch (securedPropertyType)
            {
                case sHmdaManufacturedTypeT.ManufacturedHomeAndLand:
                    return LqbManufacturedHomeSecuredPropertyBase.ManufacturedHomeAndLand;
                case sHmdaManufacturedTypeT.ManufacturedHomeAndNotLand:
                    return LqbManufacturedHomeSecuredPropertyBase.ManufacturedHomeAndNotLand;
                case sHmdaManufacturedTypeT.NA:
                    return LqbManufacturedHomeSecuredPropertyBase.NotApplicable;
                case sHmdaManufacturedTypeT.LeaveBlank:
                    return LqbManufacturedHomeSecuredPropertyBase.LeaveBlank;
                default:
                    throw new UnhandledEnumException(securedPropertyType);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the AUS type.
        /// </summary>
        /// <param name="aus">The AUS used to underwrite the loan.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaAutomatedUnderwritingSystemBase GetLqbHmdaAutomatedUnderwritingSystemBaseValue(AUSType aus)
        {
            switch (aus)
            {
                case AUSType.DesktopUnderwriter:
                    return LqbHmdaAutomatedUnderwritingSystemBase.DesktopUnderwriter;
                case AUSType.GUS:
                    return LqbHmdaAutomatedUnderwritingSystemBase.GuaranteedUnderwritingSystem;
                case AUSType.LoanProspector:
                    return LqbHmdaAutomatedUnderwritingSystemBase.LoanProspector;
                case AUSType.TotalScorecard:
                    return LqbHmdaAutomatedUnderwritingSystemBase.TotalScorecard;
                case AUSType.NotApplicable:
                    return LqbHmdaAutomatedUnderwritingSystemBase.NotApplicable;
                case AUSType.Other:
                    return LqbHmdaAutomatedUnderwritingSystemBase.Other;
                case AUSType.Blank:
                    return LqbHmdaAutomatedUnderwritingSystemBase.Blank;
                default:
                    throw new UnhandledEnumException(aus);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to an AUS result.
        /// </summary>
        /// <param name="result">The AUS result.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaAutomatedUnderwritingSystemResult GetLqbHmdaAutomatedUnderwritingSystemResultBaseValue(AUSResult result)
        {
            switch (result)
            {
                case AUSResult.Accept:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Accept;
                case AUSResult.ApproveEligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ApproveEligible;
                case AUSResult.ApproveIneligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ApproveIneligible;
                case AUSResult.Caution:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Caution;
                case AUSResult.Eligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Eligible;
                case AUSResult.Error:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Error;
                case AUSResult.Incomplete:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Incomplete;
                case AUSResult.Ineligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Ineligible;
                case AUSResult.Invalid:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Invalid;
                case AUSResult.OutOfScope:
                    return LqbHmdaAutomatedUnderwritingSystemResult.OutOfScope;
                case AUSResult.Refer:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Refer;
                case AUSResult.ReferCaution:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ReferCaution;
                case AUSResult.ReferEligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ReferEligible;
                case AUSResult.ReferIneligible:
                    return LqbHmdaAutomatedUnderwritingSystemResult.ReferIneligible;
                case AUSResult.UnableToDetermine:
                    return LqbHmdaAutomatedUnderwritingSystemResult.UnableToDetermine;
                case AUSResult.Other:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Other;
                case AUSResult.NotApplicable:
                    return LqbHmdaAutomatedUnderwritingSystemResult.NotApplicable;
                case AUSResult.Blank:
                    return LqbHmdaAutomatedUnderwritingSystemResult.Blank;
                default:
                    throw new UnhandledEnumException(result);
            }
        }

        /// <summary>
        /// Gets the relied-on credit score mode.
        /// </summary>
        /// <param name="mode">The relied-on credit score mode.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbReliedOnCreditScoreModeBase GetLqbReliedOnCreditScoreModeBaseValue(ObjLib.Hmda.ReliedOnCreditScoreMode mode)
        {
            switch (mode)
            {
                case ObjLib.Hmda.ReliedOnCreditScoreMode.ApplicantIndividualScore:
                    return LqbReliedOnCreditScoreModeBase.ApplicantsIndividualScore;
                case ObjLib.Hmda.ReliedOnCreditScoreMode.SingleScore:
                    return LqbReliedOnCreditScoreModeBase.SingleScore;
                case ObjLib.Hmda.ReliedOnCreditScoreMode.Blank:
                    return LqbReliedOnCreditScoreModeBase.Blank;
                default:
                    throw new UnhandledEnumException(mode);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the credit score model type.
        /// </summary>
        /// <param name="modelType">The credit score model type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaCreditScoreModelNameBase GetLqbCreditScoreModelNameBaseValue(sHmdaCreditScoreModelT modelType)
        {
            switch (modelType)
            {
                case sHmdaCreditScoreModelT.EquifaxBeacon5:
                    return LqbHmdaCreditScoreModelNameBase.EquifaxBeacon5;
                case sHmdaCreditScoreModelT.ExperianFairIsaac:
                    return LqbHmdaCreditScoreModelNameBase.ExperianFairIsaac;
                case sHmdaCreditScoreModelT.FicoRiskScoreClassic04:
                    return LqbHmdaCreditScoreModelNameBase.FicoRiskScoreClassic04;
                case sHmdaCreditScoreModelT.FicoRiskScoreClassic98:
                    return LqbHmdaCreditScoreModelNameBase.FicoRiskScoreClassic98;
                case sHmdaCreditScoreModelT.MoreThanOneScoringModel:
                    return LqbHmdaCreditScoreModelNameBase.MoreThanOneScoringModel;
                case sHmdaCreditScoreModelT.NotApplicable:
                    return LqbHmdaCreditScoreModelNameBase.NotApplicable;
                case sHmdaCreditScoreModelT.Other:
                    return LqbHmdaCreditScoreModelNameBase.Other;
                case sHmdaCreditScoreModelT.VantageScore2:
                    return LqbHmdaCreditScoreModelNameBase.VantageScore2;
                case sHmdaCreditScoreModelT.VantageScore3:
                    return LqbHmdaCreditScoreModelNameBase.VantageScore3;
                case sHmdaCreditScoreModelT.NoCoApplicant:
                    return LqbHmdaCreditScoreModelNameBase.NoCoApplicant;
                case sHmdaCreditScoreModelT.LeaveBlank:
                    return LqbHmdaCreditScoreModelNameBase.LeaveBlank;
                default:
                    throw new UnhandledEnumException(modelType);
            }
        }

        /// <summary>
        /// Gets the MISMO lien priority.
        /// </summary>
        /// <param name="lienPriority">The lien type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbLienPriorityBase GetLqbLienPriorityBaseValue(E_sHmdaLienT lienPriority)
        {
            switch (lienPriority)
            {
                case E_sHmdaLienT.FirstLien:
                    return LqbLienPriorityBase.FirstLien;
                case E_sHmdaLienT.SubordinateLien:
                    return LqbLienPriorityBase.SubordinateLien;
                case E_sHmdaLienT.NotSecuredByLien:
                    return LqbLienPriorityBase.NotSecuredByLien;
                case E_sHmdaLienT.NotApplicablePurchaseLoan:
                    return LqbLienPriorityBase.NotApplicable;
                case E_sHmdaLienT.LeaveBlank:
                    return LqbLienPriorityBase.Blank;
                default:
                    throw new UnhandledEnumException(lienPriority);
            }
        }

        /// <summary>
        /// Gets the purchaser type value.
        /// </summary>
        /// <param name="purchaserType">The loan purchaser type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaPurchaserBase GetHmdaPurchaserBaseValue(HmdaPurchaser2015T purchaserType)
        {
            switch (purchaserType)
            {
                case HmdaPurchaser2015T.AffiliateInstitution:
                    return LqbHmdaPurchaserBase.AffiliateInstitution;
                case HmdaPurchaser2015T.CommercialBankSavingsBankSavingsAssociation:
                    return LqbHmdaPurchaserBase.CommercialOrSavingsBank;
                case HmdaPurchaser2015T.CreditUnionMortgageCompanyFinanceCompany:
                    return LqbHmdaPurchaserBase.CreditUnionMortgageCompanyFinanceCompany;
                case HmdaPurchaser2015T.LifeInsCreditUnion:
                    return LqbHmdaPurchaserBase.LifeInsuranceCreditUnion;
                case HmdaPurchaser2015T.LifeInsuranceCompany:
                    return LqbHmdaPurchaserBase.LifeInsuranceCompany;
                case HmdaPurchaser2015T.FannieMae:
                    return LqbHmdaPurchaserBase.FannieMae;
                case HmdaPurchaser2015T.FarmerMac:
                    return LqbHmdaPurchaserBase.FarmerMac;
                case HmdaPurchaser2015T.FreddieMac:
                    return LqbHmdaPurchaserBase.FreddieMac;
                case HmdaPurchaser2015T.GinnieMae:
                    return LqbHmdaPurchaserBase.GinnieMae;
                case HmdaPurchaser2015T.NA:
                    return LqbHmdaPurchaserBase.NotApplicable;
                case HmdaPurchaser2015T.PrivateSecuritizer:
                    return LqbHmdaPurchaserBase.PrivateSecuritization;
                case HmdaPurchaser2015T.Other:
                    return LqbHmdaPurchaserBase.Other;
                case HmdaPurchaser2015T.LeaveBlank:
                    return LqbHmdaPurchaserBase.Blank;
                default:
                    throw new UnhandledEnumException(purchaserType);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to an applicant's race or race designation.
        /// </summary>
        /// <param name="race">An applicant's race or race designation.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbHmdaRaceAndDesignationBase GetLqbHmdaRaceAndDesignationBaseValue(Hmda_Race race)
        {
            switch (race)
            {
                case Hmda_Race.AmericanIndianOrAlaskanNative:
                    return LqbHmdaRaceAndDesignationBase.AmericanIndianOrAlaskaNative;
                case Hmda_Race.Asian:
                    return LqbHmdaRaceAndDesignationBase.Asian;
                case Hmda_Race.AsianIndian:
                    return LqbHmdaRaceAndDesignationBase.AsianIndian;
                case Hmda_Race.BlackOrAfricanAmerican:
                    return LqbHmdaRaceAndDesignationBase.BlackOrAfricanAmerican;
                case Hmda_Race.Chinese:
                    return LqbHmdaRaceAndDesignationBase.Chinese;
                case Hmda_Race.Filipino:
                    return LqbHmdaRaceAndDesignationBase.Filipino;
                case Hmda_Race.GuamanianOrChamorro:
                    return LqbHmdaRaceAndDesignationBase.GuamanianOrChamorro;
                case Hmda_Race.Japanese:
                    return LqbHmdaRaceAndDesignationBase.Japanese;
                case Hmda_Race.Korean:
                    return LqbHmdaRaceAndDesignationBase.Korean;
                case Hmda_Race.NativeHawaiian:
                    return LqbHmdaRaceAndDesignationBase.NativeHawaiian;
                case Hmda_Race.NativeHawaiianOrOtherPacificIslander:
                    return LqbHmdaRaceAndDesignationBase.NativeHawaiianOrOtherPacificIslander;
                case Hmda_Race.NotApplicable:
                    return LqbHmdaRaceAndDesignationBase.NotApplicable;
                case Hmda_Race.NotProvided:
                    return LqbHmdaRaceAndDesignationBase.InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication;
                case Hmda_Race.OtherAsian:
                    return LqbHmdaRaceAndDesignationBase.OtherAsian;
                case Hmda_Race.OtherPacificIslander:
                    return LqbHmdaRaceAndDesignationBase.OtherPacificIslander;
                case Hmda_Race.Samoan:
                    return LqbHmdaRaceAndDesignationBase.Samoan;
                case Hmda_Race.Vietnamese:
                    return LqbHmdaRaceAndDesignationBase.Vietnamese;
                case Hmda_Race.White:
                    return LqbHmdaRaceAndDesignationBase.White;
                case Hmda_Race.NoCoApplicant:
                    return LqbHmdaRaceAndDesignationBase.NoCoApplicant;
                case Hmda_Race.Blank:
                    return LqbHmdaRaceAndDesignationBase.Blank;
                default:
                    throw new UnhandledEnumException(race);
            }
        }

        /// <summary>
        /// Converts the given initial rate type to a HELOC teaser rate type.
        /// </summary>
        /// <param name="rateType">The initial rate type on the HELOC.</param>
        /// <returns>The HELOC teaser rate type corresponding to the given initial rate type.</returns>
        private HELOCTeaserRateBase GetHelocTeaserRateBase(E_sInitialRateT rateType)
        {
            switch (rateType)
            {
                case E_sInitialRateT.DiscountToMargin:
                    return HELOCTeaserRateBase.Adjustable;
                case E_sInitialRateT.FixedRate:
                    return HELOCTeaserRateBase.Fixed;
                case E_sInitialRateT.NoDiscount:
                    return HELOCTeaserRateBase.Fixed;
                case E_sInitialRateT.LeaveBlank:
                    return HELOCTeaserRateBase.Blank;
                default:
                    throw new UnhandledEnumException(rateType);
            }
        }

        /// <summary>
        /// Creates a container holding information on a VA or FHA loan.
        /// </summary>
        /// <returns>A GOVERNMENT_LOAN container.</returns>
        private GOVERNMENT_LOAN CreateGovernmentLoan()
        {
            if (this.DataLoan.sLT != E_sLT.FHA && this.DataLoan.sLT != E_sLT.VA)
            {
                return null;
            }

            var governmentLoan = new GOVERNMENT_LOAN();

            if (this.DataLoan.sLT == E_sLT.FHA)
            {
                governmentLoan.FHA_MIPremiumRefundAmount = ToMismoAmount(this.DataLoan.sFHA203kFHAMipRefund_rep);
                governmentLoan.FHAAssignmentDate = ToMismoDate(this.DataLoan.sCaseAssignmentD_rep);
                governmentLoan.FHACoverageRenewalRatePercent = ToMismoPercent(this.DataLoan.sProMInsR_rep);
                governmentLoan.FHAEnergyRelatedRepairsOrImprovementsAmount = ToMismoAmount(this.DataLoan.sFHAEnergyEffImprov_rep);
                governmentLoan.FHAUpfrontPremiumAmount = ToMismoAmount(this.DataLoan.sFfUfmip1003_rep);
                governmentLoan.FHAUpfrontPremiumPercent = ToMismoPercent(this.DataLoan.sFfUfmipR_rep);
                governmentLoan.FHALoanSponsorIdentifier = ToMismoIdentifier(this.DataLoan.sFHASponsorAgentIdCode);
                governmentLoan.FHALoanLenderIdentifier = ToMismoIdentifier(this.DataLoan.sFHALenderIdCode);
                governmentLoan.SectionOfActType = ToMismoEnum(this.GetSectionOfActBaseValue(this.DataLoan.sFHAHousingActSection));
                governmentLoan.FHAEndorsementDate = ToMismoDate(this.DataLoan.sFhaEndorsementD_rep);

                if (governmentLoan.SectionOfActType?.IsSetToOther ?? false)
                {
                    governmentLoan.SectionOfActTypeOtherDescription = ToMismoString(this.DataLoan.sFHAHousingActSection);
                }
            }

            if (this.DataLoan.sLT == E_sLT.VA)
            {
                governmentLoan.BorrowerFundingFeePercent = ToMismoPercent(this.DataLoan.sFfUfmipR_rep);
                governmentLoan.VAEntitlementAmount = ToMismoAmount(this.PrimaryApp.aVaEntitleAmt_rep);
                governmentLoan.VAEntitlementIdentifier = ToMismoIdentifier(this.PrimaryApp.aVaEntitleCode);
                governmentLoan.VAMaintenanceExpenseMonthlyAmount = ToMismoAmount(this.DataLoan.sVaProMaintenancePmt_rep);
                governmentLoan.VAResidualIncomeAmount = ToMismoAmount(this.PrimaryApp.aVaFamilySupportBal_rep);
                governmentLoan.VAUtilityExpenseMonthlyAmount = ToMismoAmount(this.DataLoan.sVaProUtilityPmt_rep);
                governmentLoan.VAResidualIncomeGuidelineAmount = ToMismoAmount(this.PrimaryApp.aVaFamilySuportGuidelineAmt_rep);
                governmentLoan.FHALoanSponsorIdentifier = ToMismoIdentifier(this.DataLoan.sVASponsorAgentIdCode);
                governmentLoan.FHALoanLenderIdentifier = ToMismoIdentifier(this.DataLoan.sVALenderIdCode);

                var vestingTypes = new List<E_aVaVestTitleT>();
                foreach (var app in this.DataLoan.Apps)
                {
                    vestingTypes.Add(app.aVaVestTitleT);
                }

                governmentLoan.VATitleVestingType = ToMismoEnum(this.GetVATitleVestingEnum(vestingTypes));

                if (this.DataLoan.sVaIsAutoProc)
                {
                    governmentLoan.VALoanProcedureType = ToMismoEnum(VALoanProcedureBase.Automatic);
                }
                else if (this.DataLoan.sVaIsAutoIrrrlProc)
                {
                    governmentLoan.VALoanProcedureType = ToMismoEnum(VALoanProcedureBase.AutomaticInterestRateReductionRefinanceLoan);
                }
                else if (this.DataLoan.sVaIsPriorApprovalProc)
                {
                    governmentLoan.VALoanProcedureType = ToMismoEnum(VALoanProcedureBase.VAPriorApproval);
                }

                bool anyMarriedVeteransOnApplication = false;
                foreach (var app in this.DataLoan.Apps)
                {
                    if (app.aBMaritalStatT == E_aBMaritalStatT.Married && app.aCMaritalStatT == E_aCMaritalStatT.Married
                        && (app.aBIsVeteran || app.aCIsVeteran))
                    {
                        anyMarriedVeteransOnApplication = true;
                        break;
                    }
                }

                governmentLoan.VABorrowerCoBorrowerMarriedIndicator = ToMismoIndicator(anyMarriedVeteransOnApplication);

                if (governmentLoan.VATitleVestingType.EnumValue == VATitleVestingBase.Other)
                {
                    governmentLoan.VATitleVestingTypeOtherDescription = ToMismoString(this.PrimaryApp.aVaVestTitleODesc);
                }
            }

            bool previousVAHomeLoanIndicatorVal = false;
            foreach (CAppData appdata in this.DataLoan.Apps)
            {
                if (appdata.aVaBorrCertHadVaLoanTri == E_TriState.Yes)
                {
                    previousVAHomeLoanIndicatorVal = true;
                }
            }

            governmentLoan.PreviousVAHomeLoanIndicator = ToMismoIndicator(previousVAHomeLoanIndicatorVal);
            governmentLoan.FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier = ToMismoIdentifier(this.DataLoan.sFHAAddendumUnderwriterChumsId);
            governmentLoan.GovernmentRefinanceType = ToMismoEnum(this.ToGovernmentRefinanceBaseValue(this.DataLoan.sLPurposeT, this.DataLoan.sHasAppraisal));

            governmentLoan.Extension = this.CreateGovernmentLoanExtension();
            governmentLoan.VAAppraisalType = ToMismoEnum(this.GetVaAppraisalBaseEnum(this.DataLoan.sVaApprT));

            return governmentLoan;
        }

        /// <summary>
        /// Creates a container holding extra information about a government loan.
        /// </summary>
        /// <returns>A <see cref="GOVERNMENT_LOAN_EXTENSION"/> container.</returns>
        private GOVERNMENT_LOAN_EXTENSION CreateGovernmentLoanExtension()
        {
            var extension = new GOVERNMENT_LOAN_EXTENSION();

            extension.Other = this.CreateLQBGovernmentLoanExtension();

            return extension;
        }

        /// <summary>
        /// Creates an LQB proprietary container with extra information about a government loan.
        /// </summary>
        /// <returns>A proprietary <see cref="LQB_GOVERNMENT_LOAN_EXTENSION"/>.</returns>
        private LQB_GOVERNMENT_LOAN_EXTENSION CreateLQBGovernmentLoanExtension()
        {
            var lqbExtension = new LQB_GOVERNMENT_LOAN_EXTENSION();

            lqbExtension.FHA_VALoanPurposeType = ToMismoEnum(this.GetFHA_VALoanPurposeType(this.DataLoan));

            return lqbExtension;
        }

        /// <summary>
        /// Converts LQB values from the loan data to an FHA/VA loan purpose type.
        /// </summary>
        /// <remarks>
        /// Converts a set of booleans into a single enum, without checking them for mutual exclusivity. This may lead to unexpected behavior.
        /// </remarks>
        /// <param name="loanData">The loan data from which to pull the LQB values.</param>
        /// <returns>A serializable <see cref="MISMOEnum{FHA_VALoanPurposeBase}"/>.</returns>
        private FHA_VALoanPurposeBase GetFHA_VALoanPurposeType(CPageData loanData)
        {
            if (loanData.sFHAPurposeIsPurchaseExistHome)
            {
                return FHA_VALoanPurposeBase.PurchaseExistingHomePreviouslyOccupied;
            }
            else if (loanData.sFHAPurposeIsFinanceImprovement)
            {
                return FHA_VALoanPurposeBase.FinanceImprovementsToExistingProperty;
            }
            else if (loanData.sFHAPurposeIsRefinance)
            {
                return FHA_VALoanPurposeBase.Refinance;
            }
            else if (loanData.sFHAPurposeIsPurchaseNewCondo)
            {
                return FHA_VALoanPurposeBase.PurchaseNewCondominiumUnit;
            }
            else if (loanData.sFHAPurposeIsPurchaseExistCondo)
            {
                return FHA_VALoanPurposeBase.PurchaseExistingCondominiumUnit;
            }
            else if (loanData.sFHAPurposeIsPurchaseNewHome)
            {
                return FHA_VALoanPurposeBase.PurchaseExistingHomeNotPreviouslyOccupied;
            }
            else if (loanData.sFHAPurposeIsConstructHome)
            {
                return FHA_VALoanPurposeBase.ConstructHome;
            }
            else if (loanData.sFHAPurposeIsFinanceCoopPurchase)
            {
                return FHA_VALoanPurposeBase.FinanceCooperativePurchase;
            }
            else if (loanData.sFHAPurposeIsPurchaseManufacturedHome)
            {
                return FHA_VALoanPurposeBase.PurchasePermanentlySitedManufacturedHome;
            }
            else if (loanData.sFHAPurposeIsManufacturedHomeAndLot)
            {
                return FHA_VALoanPurposeBase.PurchasePermanentlySitedManufacturedHomeAndLot;
            }
            else if (loanData.sFHAPurposeIsRefiManufacturedHomeToBuyLot)
            {
                return FHA_VALoanPurposeBase.RefinancePermanentlySitedManufacturedHomeToBuyLot;
            }
            else if (loanData.sFHAPurposeIsRefiManufacturedHomeOrLotLoan)
            {
                return FHA_VALoanPurposeBase.RefinancePermanentlySitedManufacturedHomeLotLoan;
            }
            else
            {
                return FHA_VALoanPurposeBase.Blank;
            }
        }

        /// <summary>
        /// Maps VA appraisal type to mismoe VA appraisal base.
        /// </summary>
        /// <param name="appraisalType">The appraisal type.</param>
        /// <returns>The equivalent appraisal base.</returns>
        private VAAppraisalBase GetVaAppraisalBaseEnum(E_sVaApprT appraisalType)
        {
            VAAppraisalBase appraisalBase;
            switch (appraisalType)
            {
                case E_sVaApprT.SingleProp:
                    appraisalBase = VAAppraisalBase.SingleProperty;
                    break;
                case E_sVaApprT.MasterCrvCase:
                    appraisalBase = VAAppraisalBase.MasterCertificateOfReasonableValueCase;
                    break;
                case E_sVaApprT.LappLenderAppr:
                    appraisalBase = VAAppraisalBase.LenderAppraisal;
                    break;
                case E_sVaApprT.ManufacturedHome:
                    appraisalBase = VAAppraisalBase.ManufacturedHome;
                    break;
                case E_sVaApprT.HudVaConversion:
                    appraisalBase = VAAppraisalBase.HUDConversion;
                    break;
                case E_sVaApprT.PropMgmtCase:
                    appraisalBase = VAAppraisalBase.PropertyManagementCase;
                    break;
                case E_sVaApprT.LeaveBlank:
                    appraisalBase = VAAppraisalBase.Blank;
                    break;
                default:
                    throw new UnhandledEnumException(appraisalType);
            }

            return appraisalBase;
        }

        /// <summary>
        /// Determines the Government Refinance enum value corresponding to a loan's purpose and whether it has an appraisal.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose.</param>
        /// <param name="hasAppraisal">Whether the loan has an appraisal on file.</param>
        /// <returns>A GovernmentRefinanceBase enum value.</returns>
        private GovernmentRefinanceBase ToGovernmentRefinanceBaseValue(E_sLPurposeT loanPurpose, bool hasAppraisal)
        {
            if (loanPurpose == E_sLPurposeT.VaIrrrl)
            {
                return GovernmentRefinanceBase.InterestRateReductionRefinanceLoan;
            }
            else if (loanPurpose == E_sLPurposeT.FhaStreamlinedRefinance)
            {
                if (hasAppraisal)
                {
                    return GovernmentRefinanceBase.StreamlineWithAppraisal;
                }
                else
                {
                    return GovernmentRefinanceBase.StreamlineWithoutAppraisal;
                }
            }
            else
            {
                return GovernmentRefinanceBase.FullDocumentation;
            }
        }

        /// <summary>
        /// Retrieves the VA title and vesting value based on the names listed on the title.
        /// </summary>
        /// <param name="vestingTypes">A list of vestingTypes on the title for each application.</param>
        /// <returns>The corresponding <see cref="MISMOEnum{VATitleVestingBase}"/> value.</returns>
        private VATitleVestingBase GetVATitleVestingEnum(List<E_aVaVestTitleT> vestingTypes)
        {
            VATitleVestingBase titleVestingBaseValue;
            if (vestingTypes.Any(type => type == E_aVaVestTitleT.VeteranAndSpouse))
            {
                // If any apps include a spouse, we override with this value.
                titleVestingBaseValue = VATitleVestingBase.VeteranAndSpouse;
            }
            else if (vestingTypes.Count == 1 && vestingTypes.First() == E_aVaVestTitleT.Veteran)
            {
                titleVestingBaseValue = VATitleVestingBase.Veteran;
            }
            else if (vestingTypes.Count(type => type == E_aVaVestTitleT.Veteran) > 1)
            {
                titleVestingBaseValue = VATitleVestingBase.JointTwoOrMoreVeterans;
            }
            else
            {
                titleVestingBaseValue = VATitleVestingBase.Other;
            }

            return titleVestingBaseValue;
        }

        /// <summary>
        /// Retrieves the SectionOfAct value that corresponds to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding SectionOfAct value.</returns>
        private SectionOfActBase GetSectionOfActBaseValue(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return SectionOfActBase.Blank;
            }
            else if (string.Equals("184", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item184;
            }
            else if (string.Equals("203(b)", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203B;
            }
            else if (string.Equals("203(b)2", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203B2;
            }
            else if (string.Equals("203(b)/251", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203B251;
            }
            else if (string.Equals("203(h)", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203H;
            }
            else if (string.Equals("203(k)", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203K;
            }
            else if (string.Equals("203(k)/251", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item203K251;
            }
            else if (string.Equals("234(c)", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item234C;
            }
            else if (string.Equals("234(c)/251", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item234C251;
            }
            else if (string.Equals("248", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item248;
            }
            else if (string.Equals("251", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item251;
            }
            else if (string.Equals("255", lqbValue, StringComparison.OrdinalIgnoreCase))
            {
                return SectionOfActBase.Item255;
            }
            else
            {
                return SectionOfActBase.Other;
            }
        }

        /// <summary>
        /// Creates a container holding information on a housing expense.
        /// </summary>
        /// <param name="amount">The amount of the housing expense.</param>
        /// <param name="paymentTimingType">The Timing type of the housing expense (such as "proposed").</param>
        /// <param name="expenseType">The type of housing expense.</param>
        /// <param name="sequence">The sequence number of the housing expense among the list of expenses.</param>
        /// <returns>A HOUSING_EXPENSE container populated with data.</returns>
        private HOUSING_EXPENSE CreateHousingExpense(string amount, HousingExpenseTimingBase paymentTimingType, HousingExpenseBase expenseType, int sequence)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var housingExpense = new HOUSING_EXPENSE();
            housingExpense.HousingExpensePaymentAmount = ToMismoAmount(amount);
            housingExpense.HousingExpenseTimingType = ToMismoEnum(paymentTimingType);
            housingExpense.HousingExpenseType = ToMismoEnum(expenseType);
            housingExpense.SequenceNumber = sequence;

            return housingExpense;
        }

        /// <summary>
        /// Creates the container for the proposed housing expenses - xpath //DEAL/LOANS/LOAN[1]/HOUSING_EXPENSES .  
        /// </summary>
        /// <returns>A HOUSING_EXPENSES container containing only the Proposed housing expenses.</returns>
        private HOUSING_EXPENSES CreateHousingExpenses()
        {
            var housingExpenses = new HOUSING_EXPENSES();

            housingExpenses.HousingExpenseList.Add(
                this.CreateHousingExpense(
                    this.DataLoan.sProMIns_rep,
                    HousingExpenseTimingBase.Proposed,
                    HousingExpenseBase.MIPremium,
                    1));

            housingExpenses.HousingExpenseList.Add(
                this.CreateHousingExpense(
                    this.DataLoan.sProHazIns_rep,
                    HousingExpenseTimingBase.Proposed,
                    HousingExpenseBase.HomeownersInsurance,
                    housingExpenses.HousingExpenseList.Count(h => h != null) + 1));

            housingExpenses.HousingExpenseList.Add(
                this.CreateHousingExpense(
                    this.DataLoan.sProSecondMPmt_rep,
                    HousingExpenseTimingBase.Proposed,
                    HousingExpenseBase.OtherMortgageLoanPrincipalAndInterest,
                    housingExpenses.HousingExpenseList.Count(h => h != null) + 1));

            housingExpenses.HousingExpenseList.Add(
                this.CreateHousingExpense(
                    this.DataLoan.sProOHExp_rep,
                    HousingExpenseTimingBase.Proposed,
                    HousingExpenseBase.Other,
                    housingExpenses.HousingExpenseList.Count(h => h != null) + 1));

            housingExpenses.HousingExpenseList.Add(
                this.CreateHousingExpense(
                    this.DataLoan.sProRealETx_rep,
                    HousingExpenseTimingBase.Proposed,
                    HousingExpenseBase.RealEstateTax,
                    housingExpenses.HousingExpenseList.Count(h => h != null) + 1));

            housingExpenses.HousingExpenseList.Add(
                this.CreateHousingExpense(
                    this.DataLoan.sProHoAssocDues_rep,
                    HousingExpenseTimingBase.Proposed,
                    HousingExpenseBase.HomeownersAssociationDuesAndCondominiumFees,
                    housingExpenses.HousingExpenseList.Count(h => h != null) + 1));

            housingExpenses.HousingExpenseList.Add(
                this.CreateHousingExpense(
                    this.DataLoan.sProFirstMPmt_rep,
                    HousingExpenseTimingBase.Proposed,
                    HousingExpenseBase.FirstMortgagePrincipalAndInterest,
                    housingExpenses.HousingExpenseList.Count(h => h != null) + 1));

            return housingExpenses;
        }

        /// <summary>
        /// Creates a container holding a primary billing address for the loan.
        /// </summary>
        /// <returns>A BILLING_ADDRESS container.</returns>
        private BILLING_ADDRESS CreateBillingAddress()
        {
            var billingAddress = new BILLING_ADDRESS();
            billingAddress.Address = CreateAddress(
                this.PrimaryApp.aBAddrPost,
                this.PrimaryApp.aBCityPost,
                this.PrimaryApp.aBStatePost,
                this.PrimaryApp.aBZipPost,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(this.PrimaryApp.aBStatePost),
                AddressBase.Mailing,
                1);

            return billingAddress;
        }

        /// <summary>
        /// Creates a container with information on a buydown.
        /// Logic adapted from the DocMagicMismoExporter class; OPM 61315.
        /// </summary>
        /// <returns>A BUYDOWN container.</returns>
        private BUYDOWN CreateBuydown()
        {
            var buydown = new BUYDOWN();
            var buydownRows = new[]
            {
                //// R = Rate Reduction, Mon = Term (mths)
                new
                {
                    R = this.DataLoan.sBuydwnR1,
                    R_rep = this.DataLoan.sBuydwnR1_rep,
                    Mon = this.DataLoan.sBuydwnMon1,
                    Mon_rep = this.DataLoan.sBuydwnMon1_rep,
                    Num = "1"
                },

                new
                {
                    R = this.DataLoan.sBuydwnR2,
                    R_rep = this.DataLoan.sBuydwnR2_rep,
                    Mon = this.DataLoan.sBuydwnMon2,
                    Mon_rep = this.DataLoan.sBuydwnMon2_rep,
                    Num = "2"
                },

                new
                {
                    R = this.DataLoan.sBuydwnR3,
                    R_rep = this.DataLoan.sBuydwnR3_rep,
                    Mon = this.DataLoan.sBuydwnMon3,
                    Mon_rep = this.DataLoan.sBuydwnMon3_rep,
                    Num = "3"
                },

                new
                {
                    R = this.DataLoan.sBuydwnR4,
                    R_rep = this.DataLoan.sBuydwnR4_rep,
                    Mon = this.DataLoan.sBuydwnMon4,
                    Mon_rep = this.DataLoan.sBuydwnMon4_rep,
                    Num = "4"
                },

                new
                {
                    R = this.DataLoan.sBuydwnR5,
                    R_rep = this.DataLoan.sBuydwnR5_rep,
                    Mon = this.DataLoan.sBuydwnMon5,
                    Mon_rep = this.DataLoan.sBuydwnMon5_rep,
                    Num = "5"
            }
            };

            //// Get nonempty rows. A row is nonzero if both fields have a nonzero value.
            var buydownRowsFiltered = buydownRows.Where((row) => row.R != 0 && row.Mon != 0);

            if (buydownRowsFiltered.Count() == 0)
            {
                return null;
            }

            bool isIncreaseRatePercentConstant = true;
            bool isChangeFrequencyMonthsConstant = true;
            decimal increaseRatePercent = buydownRowsFiltered.First().R;
            int changeFrequencyMonths = buydownRowsFiltered.First().Mon;
            int durationMonths = 0;

            buydown.BuydownSchedules = new BUYDOWN_SCHEDULES();
            foreach (var row in buydownRowsFiltered)
            {
                buydown.BuydownSchedules.BuydownScheduleList.Add(this.CreateBuydownSchedule(row.R_rep, row.Num, row.Mon_rep, GetNextSequenceNumber(buydown.BuydownSchedules.BuydownScheduleList)));

                durationMonths += row.Mon;

                //// Detect increase rate percent: iff the value in sBuydwnR1 through sBuydwnR5 drops at the same rate
                if (row.R != increaseRatePercent)
                {
                    isIncreaseRatePercentConstant = false;
            }

                //// Detect change frequency months: iff the value in sBuydwnMon1 through sBuydwnMon5 remains constant
                if (row.Mon != changeFrequencyMonths)
            {
                    isChangeFrequencyMonthsConstant = false;
            }
            }

            buydown.BuydownRule = new BUYDOWN_RULE();
            buydown.BuydownRule.BuydownBaseDateType = ToMismoEnum(BuydownBaseDateBase.FirstPaymentDate);
            buydown.BuydownRule.BuydownDurationMonthsCount = ToMismoCount(durationMonths, this.DataLoan.m_convertLos);

            if (isChangeFrequencyMonthsConstant)
            {
                buydown.BuydownRule.BuydownChangeFrequencyMonthsCount = ToMismoCount(changeFrequencyMonths, this.DataLoan.m_convertLos);
            }

            if (isIncreaseRatePercentConstant)
            {
                buydown.BuydownRule.BuydownIncreaseRatePercent = ToMismoPercent(this.DataLoan.m_convertLos.ToRateString(increaseRatePercent));
            }

            return buydown;
        }

        /// <summary>
        /// Creates a container holding information on a period of the buydown.
        /// </summary>
        /// <param name="adjustmentPercent">The rate adjustment for this period.</param>
        /// <param name="periodIdentifier">A sequence identifier for all periods of the buydown.</param>
        /// <param name="periodicTerm">The months/number of payments in this period.</param>
        /// <param name="sequence">The sequence number of the schedule among the list of schedules.</param>
        /// <returns>A BUYDOWN_SCHEDULE container.</returns>
        private BUYDOWN_SCHEDULE CreateBuydownSchedule(string adjustmentPercent, string periodIdentifier, string periodicTerm, int sequence)
        {
            var buydownSchedule = new BUYDOWN_SCHEDULE();
            buydownSchedule.SequenceNumber = sequence;
            buydownSchedule.BuydownScheduleAdjustmentPercent = ToMismoPercent(adjustmentPercent);
            buydownSchedule.BuydownSchedulePeriodIdentifier = ToMismoIdentifier(periodIdentifier);
            buydownSchedule.BuydownSchedulePeriodicPaymentsCount = ToMismoCount(periodicTerm, this.DataLoan.m_convertLos);

            return buydownSchedule;
        }

        /// <summary>
        /// Creates CLOSING_ADJUSTMENT_ITEMS container with CLOSING_ADJUSTMENT_ITEM sub-container populated with loan file information.
        /// </summary>
        /// <returns>Returns a CLOSING_ADJUSTMENT_ITEMS container with CLOSING_ADJUSTMENT_ITEM sub-container populated with loan file information.</returns>
        private CLOSING_ADJUSTMENT_ITEMS CreateClosingAdjustmentItems()
        {
            var adjItems = new CLOSING_ADJUSTMENT_ITEMS();

            //// Section K. Due from Borrower at Closing
            adjItems.ClosingAdjustmentItemList.Add(
                this.CreateClosingAdjustmentItem(
                GetDisclosureSectionForSectionKAdjustments(this.ExportData.IsClosingPackage, this.DataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT, this.DataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.Other,
                "Sale Price of Any Personal Property Included in Sale",
                this.DataLoan.sGrossDueFromBorrPersonalProperty_rep,
                E_PartyT.Borrower,
                paidOutsideOfClosing: false,
                sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList)));

            adjItems.ClosingAdjustmentItemList.Add(
                this.CreateClosingAdjustmentItem(
                GetDisclosureSectionForSectionKAdjustments(this.ExportData.IsClosingPackage, this.DataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT, this.DataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                IntegratedDisclosureSubsectionBase.Adjustments,
                ClosingAdjustmentItemBase.Repairs,
                this.DataLoan.sTRIDClosingDisclosureAltCostDescription,
                this.DataLoan.sTRIDClosingDisclosureAltCost_rep,
                E_PartyT.Borrower,
                paidOutsideOfClosing: false,
                sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList)));

            var adjList = this.DataLoan.sAdjustmentList.GetEnumeratorWithoutSellerCredit() ?? new List<Adjustment>();

            foreach (Adjustment adj in adjList.Where(a => a.IsPaidFromBorrower && a.Amount > 0.00m))
            {
                adjItems.ClosingAdjustmentItemList.Add(
                    this.CreateClosingAdjustmentItem(
                    GetDisclosureSectionForSectionKAdjustments(this.ExportData.IsClosingPackage, this.DataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT, this.DataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                    IntegratedDisclosureSubsectionBase.Adjustments,
                    ClosingAdjustmentItemBase.Other,
                    adj.Description,
                    adj.Amount_rep,
                    adj.PaidFromParty,
                    adj.POC,
                    lqbAdjustmentType: adj.AdjustmentType,
                    excludeFromLeCd: !adj.IncludeAdjustmentInLeCdForThisLien,
                    sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList)));
            }

            //// K - AdjustmentsForItemsPaidBySellerInAdvance -> see the prorations

            //// Section M. Due to Seller at Closing
            adjItems.ClosingAdjustmentItemList.Add(
                this.CreateClosingAdjustmentItem(
                IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.Other,
                "Sale Price of Any Personal Property Included in Sale",
                this.DataLoan.sGrossDueFromBorrPersonalProperty_rep,
                E_PartyT.Borrower,
                paidOutsideOfClosing: false,
                sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList)));

            foreach (Adjustment adj in adjList.Where(a => a.IsPaidToSeller && a.Amount > 0.00m))
            {
                adjItems.ClosingAdjustmentItemList.Add(
                    this.CreateClosingAdjustmentItem(
                    IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                    IntegratedDisclosureSubsectionBase.Blank,
                    ClosingAdjustmentItemBase.Other,
                    adj.Description,
                    adj.Amount_rep,
                    adj.PaidFromParty,
                    adj.POC,
                    excludeFromLeCd: !adj.IncludeAdjustmentInLeCdForThisLien,
                    lqbAdjustmentType: adj.AdjustmentType,
                    sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList)));
            }

            //// M - AdjustmentsForItemsPaidBySellerInAdvance -> see the prorations

            //// Section L. Paid Already by or on Behalf of the Borrower at Closing
            //// Deposit -> see the closing cost funds

            bool populateExcludeFromLeCdForProceedsOfSubordinateLiensOverride = this.DataLoan.sLienPosT == E_sLienPosT.Second &&
                                                                                this.DataLoan.sIsOFinNew &&
                                                                                this.DataLoan.sRemain1stMBal > 0 &&
                                                                                this.DataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017;
            adjItems.ClosingAdjustmentItemList.Add(
                this.CreateClosingAdjustmentItem(
                IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.ProceedsOfSubordinateLiens,
                "Other New Financing",
                this.DataLoan.sONewFinNetProceeds_rep,
                E_PartyT.Borrower,
                paidOutsideOfClosing: false,
                sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList),
                excludeFromLeCd: true,
                populateExcludeFromLeCdOverride: populateExcludeFromLeCdForProceedsOfSubordinateLiensOverride));

            Adjustment sellerCredit = this.DataLoan.sAdjustmentList.SellerCredit ?? new Adjustment();

            adjItems.ClosingAdjustmentItemList.Add(
                this.CreateClosingAdjustmentItem(
                IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.SellerCredit,
                "Seller Credit",
                sellerCredit.Amount_rep,
                sellerCredit.PaidFromParty,
                sellerCredit.POC,
                excludeFromLeCd: !sellerCredit.IncludeAdjustmentInLeCdForThisLien,
                sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList)));

            bool enablePredefinedAdjustmentMapping = LoanDataMigrationUtils.IsOnOrBeyondLoanVersion(this.DataLoan.GetLiveLoanVersion(), LoanVersionT.V24_PredefinedDescriptionsForLoanAdjustments);

            ////Other Credits & Adjustments
            foreach (Adjustment adj in adjList.Where(a => a.IsPaidToBorrower && a.Amount > 0.00m))
            {
                var subsection = this.GetIntegratedDisclosureSubsectionBaseValue_AdjustmentPaidToBorrower(adj.AdjustmentType);
                var adjItemType = ClosingAdjustmentItemBase.Other;

                if (enablePredefinedAdjustmentMapping)
                {
                    adjItemType = this.GetClosingAdjustmentItemBaseValue(adj.AdjustmentType);
                }

                adjItems.ClosingAdjustmentItemList.Add(
                    this.CreateClosingAdjustmentItem(
                    IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                    subsection,
                    adjItemType,
                    adj.Description,
                    adj.Amount_rep,
                    adj.PaidFromParty,
                    adj.POC,
                    excludeFromLeCd: !adj.IncludeAdjustmentInLeCdForThisLien,
                    sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList),
                    lqbAdjustmentType: adj.AdjustmentType));
            }

            //// L - AdjustmentsForItemsUnpaidBySellerInAdvance -> see the prorations

            //// Section N. Due from Seller at Closing
            //// Excess Deposit -> see the closing cost funds

            adjItems.ClosingAdjustmentItemList.Add(
                this.CreateClosingAdjustmentItem(
                IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                IntegratedDisclosureSubsectionBase.Blank,
                ClosingAdjustmentItemBase.SellerCredit,
                "Seller Credit",
                sellerCredit.Amount_rep,
                sellerCredit.PaidFromParty,
                sellerCredit.POC,
                excludeFromLeCd: !sellerCredit.IncludeAdjustmentInLeCdForThisLien,
                sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList)));

            foreach (Adjustment adj in adjList.Where(a => a.IsPaidFromSeller && a.Amount > 0.00m))
            {
                adjItems.ClosingAdjustmentItemList.Add(
                    this.CreateClosingAdjustmentItem(
                    IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                    IntegratedDisclosureSubsectionBase.Blank,
                    ClosingAdjustmentItemBase.Other,
                    adj.Description,
                    adj.Amount_rep,
                    adj.PaidFromParty,
                    adj.POC,
                    excludeFromLeCd: !adj.IncludeAdjustmentInLeCdForThisLien,
                    sequence: GetNextSequenceNumber(adjItems.ClosingAdjustmentItemList),
                    lqbAdjustmentType: adj.AdjustmentType));
            }

            //// N - AdjustmentsForItemsUnpaidBySellerInAdvance -> see the prorations

            return adjItems.ClosingAdjustmentItemList.Count(i => i != null) > 0 ? adjItems : null;
        }

        /// <summary>
        /// Gets the closing adjustment item base value corresponding to the adjustment type.
        /// </summary>
        /// <param name="adjustmentType">The adjustment type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private ClosingAdjustmentItemBase GetClosingAdjustmentItemBaseValue(E_AdjustmentT adjustmentType)
        {
            switch (adjustmentType)
            {
                case E_AdjustmentT.FuelCosts:
                    return ClosingAdjustmentItemBase.FuelCosts;
                case E_AdjustmentT.Gift:
                    return ClosingAdjustmentItemBase.Gift;
                case E_AdjustmentT.Grant:
                    return ClosingAdjustmentItemBase.Grant;
                case E_AdjustmentT.ProceedsOfSubordinateLiens:
                    return ClosingAdjustmentItemBase.ProceedsOfSubordinateLiens;
                case E_AdjustmentT.RebateCredit:
                    return ClosingAdjustmentItemBase.RebateCredit;
                case E_AdjustmentT.RelocationFunds:
                    return ClosingAdjustmentItemBase.RelocationFunds;
                case E_AdjustmentT.RentFromSubjectProperty:
                    return ClosingAdjustmentItemBase.RentFromSubjectProperty;
                case E_AdjustmentT.RepairCompletionEscrowHoldback:
                    return ClosingAdjustmentItemBase.RepairCompletionEscrowHoldback;
                case E_AdjustmentT.Repairs:
                    return ClosingAdjustmentItemBase.Repairs;
                case E_AdjustmentT.SatisfactionOfSubordinateLien:
                    return ClosingAdjustmentItemBase.SatisfactionOfSubordinateLien;
                case E_AdjustmentT.SellerCredit:
                    return ClosingAdjustmentItemBase.SellerCredit;
                case E_AdjustmentT.SellersEscrowAssumption:
                    return ClosingAdjustmentItemBase.SellersEscrowAssumption;
                case E_AdjustmentT.SellersMortgageInsuranceAssumption:
                    return ClosingAdjustmentItemBase.SellersMortgageInsuranceAssumption;
                case E_AdjustmentT.Services:
                    return ClosingAdjustmentItemBase.Services;
                case E_AdjustmentT.SubordinateFinancingProceeds:
                    return ClosingAdjustmentItemBase.SubordinateFinancingProceeds;
                case E_AdjustmentT.SweatEquity:
                    return ClosingAdjustmentItemBase.SweatEquity;
                case E_AdjustmentT.TenantSecurityDeposit:
                    return ClosingAdjustmentItemBase.TenantSecurityDeposit;
                case E_AdjustmentT.TradeEquity:
                    return ClosingAdjustmentItemBase.TradeEquity;
                case E_AdjustmentT.UnpaidUtilityEscrowHoldback:
                    return ClosingAdjustmentItemBase.UnpaidUtilityEscrowHoldback;
                case E_AdjustmentT.Blank:
                case E_AdjustmentT.BorrowerPaidFees:
                case E_AdjustmentT.BuydownFund:
                case E_AdjustmentT.CommitmentOriginationFee:
                case E_AdjustmentT.EmployerAssistedHousing:
                case E_AdjustmentT.FederalAgencyFundingFeeRefund:
                case E_AdjustmentT.LeasePurchaseFund:
                case E_AdjustmentT.MIPremiumRefund:
                case E_AdjustmentT.Other:
                case E_AdjustmentT.SellersReserveAccountAssumption:
                case E_AdjustmentT.TitlePremiumAdjustment:
                case E_AdjustmentT.TradeEquityCredit:
                case E_AdjustmentT.TradeEquityFromPropertySwap:
                case E_AdjustmentT.GiftOfEquity:
                case E_AdjustmentT.PrincipalReductionToReduceCashToBorrower:
                case E_AdjustmentT.PrincipalReductionToCureToleranceViolation:
                    return ClosingAdjustmentItemBase.Other;
                default:
                    throw UnhandledEnumException.GenericException("Unhandled enum exception in E_AdjustmentT");
            }
        }

        /// <summary>
        /// Gets the TRID subsection for a closing adjustment paid to a borrower.
        /// </summary>
        /// <param name="adjustmentType">The adjustment type.</param>
        /// <returns>The corresponding subsection value.</returns>
        private IntegratedDisclosureSubsectionBase GetIntegratedDisclosureSubsectionBaseValue_AdjustmentPaidToBorrower(E_AdjustmentT adjustmentType)
        {
            if (adjustmentType == E_AdjustmentT.Gift
                || adjustmentType == E_AdjustmentT.Grant
                || adjustmentType == E_AdjustmentT.RebateCredit
                || adjustmentType == E_AdjustmentT.Other)
            {
                return IntegratedDisclosureSubsectionBase.OtherCredits;
            }
            else
            {
                return IntegratedDisclosureSubsectionBase.Adjustments;
            }
        }

        /// <summary>
        /// Creates a CLOSING_ADJUSTMENT_ITEM container with description, value, integrated disclosure type and line number specified.
        /// </summary>
        /// <param name="section">The loan estimate or closing disclosure section that includes the adjustment.</param>
        /// <param name="subsection">The loan estimate or closing disclosure subsection that the adjustment belongs to, if any. Pass blank if not applicable.</param>
        /// <param name="adjItemType">The adjustment type.</param>
        /// <param name="adjItemTypeOtherDescription">A verbal description of the adjustment.</param>
        /// <param name="amount">The adjustment amount. Requires a <code>rep or LOSConverted</code> value.</param>
        /// <param name="paidBy">The type of entity that pays/paid the adjustment amount.</param>
        /// <param name="paidOutsideOfClosing">Indicates whether the adjustment is paid outside of closing.</param>
        /// <param name="sequence">The sequence number of the adjustment among the list of adjustments.</param>
        /// <param name="excludeFromLeCd">Indicates whether the adjustment item should be excluded from the LE/CD.</param>
        /// <param name="lqbAdjustmentType">The adjustment type in LendingQB.</param>
        /// <param name="populateExcludeFromLeCdOverride">Whether the ExcludeFromLECDForThisLien field will be populated. Will default to <see cref="ShouldSendTrid2017SubordinateFinancingData"/> if not provided.</param>
        /// <returns>Returns a CLOSING_ADJUSTMENT_ITEM container with description, value, integrated disclosure type and line number specified.</returns>
        private CLOSING_ADJUSTMENT_ITEM CreateClosingAdjustmentItem(
                                            IntegratedDisclosureSectionBase section,
                                            IntegratedDisclosureSubsectionBase subsection,
                                            ClosingAdjustmentItemBase adjItemType,
                                            string adjItemTypeOtherDescription,
                                            string amount,
                                            E_PartyT paidBy,
                                            bool paidOutsideOfClosing,
                                            int sequence,
                                            bool? excludeFromLeCd = null,
                                            E_AdjustmentT lqbAdjustmentType = E_AdjustmentT.Blank,
                                            bool? populateExcludeFromLeCdOverride = null)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var closingAdjustmentItem = new CLOSING_ADJUSTMENT_ITEM();
            closingAdjustmentItem.ClosingAdjustmentItemDetail = this.CreateClosingAdjustmentItemDetail(
                section,
                subsection,
                adjItemType,
                adjItemTypeOtherDescription,
                amount,
                paidBy,
                paidOutsideOfClosing,
                excludeFromLeCd,
                lqbAdjustmentType,
                populateExcludeFromLeCdOverride);
            closingAdjustmentItem.SequenceNumber = sequence;

            return closingAdjustmentItem;
        }

        /// <summary>
        /// Creates a CLOSING_ADJUSTMENT_ITEM_DETAIL with information pertaining to a closing adjustment.
        /// </summary>
        /// <param name="section">The loan estimate or closing disclosure section that includes the adjustment.</param>
        /// <param name="subsection">The loan estimate or closing disclosure subsection that the adjustment belongs to, if any. Pass blank if not applicable.</param>
        /// <param name="adjItemType">The adjustment type.</param>
        /// <param name="adjItemTypeOtherDescription">A verbal description of the adjustment.</param>
        /// <param name="amount">The adjustment amount. Requires a <code>rep or LOSConverted</code> value.</param>
        /// <param name="paidBy">The type of entity that pays/paid the adjustment amount.</param>
        /// <param name="paidOutsideOfClosing">Indicates whether the closing adjustment is paid outside of closing.</param>
        /// <param name="excludeFromLeCd">Indicates whether the adjustment item should be excluded from the LE/CD.</param>
        /// <param name="lqbAdjustmentType">The adjustment type in LendingQB.</param>
        /// <param name="populateExcludeFromLeCdOverride">Whether the ExcludeFromLECDForThisLien field will be populated. Will default to <see cref="ShouldSendTrid2017SubordinateFinancingData"/> if not provided.</param>
        /// <returns>A CLOSING_ADJUSTMENT_ITEM_DETAIL with information pertaining to a closing adjustment.</returns>
        private CLOSING_ADJUSTMENT_ITEM_DETAIL CreateClosingAdjustmentItemDetail(
                                                    IntegratedDisclosureSectionBase section,
                                                    IntegratedDisclosureSubsectionBase subsection,
                                                    ClosingAdjustmentItemBase adjItemType,
                                                    string adjItemTypeOtherDescription,
                                                    string amount,
                                                    E_PartyT paidBy,
                                                    bool paidOutsideOfClosing,
                                                    bool? excludeFromLeCd,
                                                    E_AdjustmentT lqbAdjustmentType,
                                                    bool? populateExcludeFromLeCdOverride)
        {
            var detail = new CLOSING_ADJUSTMENT_ITEM_DETAIL();
            detail.ClosingAdjustmentItemAmount = ToMismoAmount(amount);
            detail.ClosingAdjustmentItemPaidByType = ToMismoEnum(this.GetClosingAdjustmentItemPaidByBase(paidBy));
            detail.ClosingAdjustmentItemType = ToMismoEnum(adjItemType, displayLabelText: adjItemTypeOtherDescription);
            detail.ClosingAdjustmentItemPaidOutsideOfClosingIndicator = ToMismoIndicator(paidOutsideOfClosing);

            if (detail.ClosingAdjustmentItemType?.IsSetToOther ?? false)
            {
                detail.ClosingAdjustmentItemTypeOtherDescription = ToMismoString(adjItemTypeOtherDescription);
            }

            detail.IntegratedDisclosureSectionType = ToMismoEnum(section);
            detail.IntegratedDisclosureSubsectionType = ToMismoEnum(subsection);

            detail.Extension = this.ToClosingAdjustmentItemDetailExtension(lqbAdjustmentType, excludeFromLeCd, populateExcludeFromLeCdOverride);

            return detail;
        }

        /// <summary>
        /// Converts the given party type to a ClosingAdjustmentItemPaidByBase.
        /// </summary>
        /// <param name="partyType">The type of party that will/has paid the adjustment amount.</param>
        /// <returns>The ClosingAdjustmentItemPaidByBase corresponding to the given party type.</returns>
        private ClosingAdjustmentItemPaidByBase GetClosingAdjustmentItemPaidByBase(E_PartyT partyType)
        {
            switch (partyType)
            {
                case E_PartyT.Borrower:
                    return ClosingAdjustmentItemPaidByBase.Buyer;
                case E_PartyT.BorrowersEmployer:
                case E_PartyT.BorrowersFriend:
                case E_PartyT.BorrowersParent:
                case E_PartyT.BorrowersRelative:
                case E_PartyT.BuilderOrDeveloper:
                case E_PartyT.FederalAgency:
                    return ClosingAdjustmentItemPaidByBase.ThirdParty;
                case E_PartyT.Lender:
                    return ClosingAdjustmentItemPaidByBase.Lender;
                case E_PartyT.Other:
                case E_PartyT.RealEstateAgent:
                    return ClosingAdjustmentItemPaidByBase.ThirdParty;
                case E_PartyT.Seller:
                    return ClosingAdjustmentItemPaidByBase.Seller;
                case E_PartyT.Blank:
                    return ClosingAdjustmentItemPaidByBase.Blank;
                default:
                    throw new UnhandledEnumException(partyType);
            }
        }

        /// <summary>
        /// Creates a container extending the closing adjustment item details.
        /// </summary>
        /// <param name="adjustmentType">The type of adjustment.</param>
        /// <param name="excludeFromLeCd">Indicates whether the adjustment item should be excluded from the LE/CD.</param>
        /// <param name="populateExcludeFromLeCdOverride">Whether the ExcludeFromLECDForThisLien field will be populated. Will default to <see cref="ShouldSendTrid2017SubordinateFinancingData"/> if not provided.</param>
        /// <returns>A serializable container.</returns>
        private CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION ToClosingAdjustmentItemDetailExtension(E_AdjustmentT adjustmentType, bool? excludeFromLeCd, bool? populateExcludeFromLeCdOverride)
        {
            var extension = new CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION();
            extension.Other = this.ToLqbClosingAdjustmentItemDetailExtension(adjustmentType, excludeFromLeCd, populateExcludeFromLeCdOverride);

            return extension;
        }

        /// <summary>
        /// Creates a container with LQB-specific data for the closing adjustment item details.
        /// </summary>
        /// <param name="adjustmentType">The type of adjustment.</param>
        /// <param name="excludeFromLeCd">Indicates whether the adjustment item should be excluded from the LE/CD.</param>
        /// <param name="populateExcludeFromLeCdOverride">Whether the ExcludeFromLECDForThisLien field will be populated. Will default to <see cref="ShouldSendTrid2017SubordinateFinancingData"/> if not provided.</param>
        /// <returns>A serializeable container.</returns>
        private LQB_CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION ToLqbClosingAdjustmentItemDetailExtension(E_AdjustmentT adjustmentType, bool? excludeFromLeCd, bool? populateExcludeFromLeCdOverride)
        {
            var extension = new LQB_CLOSING_ADJUSTMENT_ITEM_DETAIL_EXTENSION();

            if (adjustmentType == E_AdjustmentT.PrincipalReductionToReduceCashToBorrower)
            {
                extension.AdjustmentIsPrincipalReduction = ToMismoIndicator(true);
                extension.PrincipalReductionForToleranceCure = ToMismoIndicator(false);
            }
            else if (adjustmentType == E_AdjustmentT.PrincipalReductionToCureToleranceViolation)
            {
                extension.AdjustmentIsPrincipalReduction = ToMismoIndicator(true);
                extension.PrincipalReductionForToleranceCure = ToMismoIndicator(true);
            }

            if (excludeFromLeCd.HasValue)
            {
                bool shouldInclude = populateExcludeFromLeCdOverride ?? ShouldSendTrid2017SubordinateFinancingData(this.DataLoan.sLienPosT, this.DataLoan.sIsOFinNew, this.DataLoan.sConcurSubFin, this.DataLoan.sTridTargetRegulationVersionT);
                if (shouldInclude)
                {
                    extension.ExcludeFromLECDForThisLien = ToMismoIndicator(excludeFromLeCd.Value);
                }
            }

            return extension;
        }

        /// <summary>
        /// Creates Closing Costs for loan.
        /// </summary>
        /// <returns>Returns a CLOSING_COSTS container populated according to the loan file.</returns>
        private CLOSING_COST_FUNDS CreateClosingCostFunds()
        {
            var closingCostFunds = new CLOSING_COST_FUNDS();

            closingCostFunds.ClosingCostFundList.Add(
                this.CreateClosingCostFund(
                    IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                    FundsBase.DepositOnSalesContract,
                    this.DataLoan.sTotCashDeposit_rep,
                    sequence: 1,
                    excludeFromLeCd: this.DataLoan.sLienToIncludeCashDepositInTridDisclosures == ResponsibleLien.OtherLienTransaction));

            closingCostFunds.ClosingCostFundList.Add(
                this.CreateClosingCostFund(
                    IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                    FundsBase.ExcessDeposit,
                    this.DataLoan.sReductionsDueToSellerExcessDeposit_rep,
                    GetNextSequenceNumber(closingCostFunds.ClosingCostFundList)));

            return closingCostFunds;
        }

        /// <summary>
        /// Creates a CLOSING_COST_FUND element with specified integrated disclosure section type, funds type, and dollar amount.
        /// </summary>
        /// <param name="section">The section of the loan estimate or closing disclosure on which the fund amount is displayed.</param>
        /// <param name="fundsType">Type of funds used.</param>
        /// <param name="amount">The dollar amount. Requires a <code>rep or LosConverted</code> value.</param>
        /// <param name="sequence">The sequence number of the fund among the list of funds.</param>
        /// <param name="excludeFromLeCd">The value to populate the ExcludeFromLECDForThisLien property with. Null if no need to populate.</param>
        /// <returns>Returns a CLOSING_COST_FUND element with specified integrated disclosure section type, funds type, and dollar amount.</returns>
        private CLOSING_COST_FUND CreateClosingCostFund(IntegratedDisclosureSectionBase section, FundsBase fundsType, string amount, int sequence, bool? excludeFromLeCd = null)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var closingCostFund = new CLOSING_COST_FUND();
            closingCostFund.ClosingCostFundAmount = ToMismoAmount(amount);
            closingCostFund.FundsType = ToMismoEnum(fundsType);
            closingCostFund.IntegratedDisclosureSectionType = ToMismoEnum(section);
            closingCostFund.SequenceNumber = sequence;

            return closingCostFund;
        }

        /// <summary>
        /// Creates a container holding information about closing.
        /// </summary>
        /// <returns>A CLOSING_INFORMATION container.</returns>
        private CLOSING_INFORMATION CreateClosingInformation()
        {
            var closingInformation = new CLOSING_INFORMATION();
            closingInformation.ClosingAdjustmentItems = this.CreateClosingAdjustmentItems();
            closingInformation.ClosingCostFunds = this.CreateClosingCostFunds();
            closingInformation.ClosingInformationDetail = this.CreateClosingInformationDetail();
            closingInformation.ClosingInstruction = this.CreateClosingInstruction();
            closingInformation.PrepaidItems = this.CreatePrepaidItems();
            closingInformation.ProrationItems = this.CreateProrationItems();

            return closingInformation;
        }

        /// <summary>
        /// Creates a container holding details about closing.
        /// </summary>
        /// <returns>A CLOSING_INFORMATION_DETAIL container populated with data.</returns>
        private CLOSING_INFORMATION_DETAIL CreateClosingInformationDetail()
        {
            var closingInformationDetail = new CLOSING_INFORMATION_DETAIL();

            closingInformationDetail.ClosingAgentOrderNumberIdentifier = ToMismoIdentifier(this.DataLoan.sSettlementAgentFileNum);
            closingInformationDetail.ClosingDate = ToMismoDate(this.DataLoan.sDocMagicClosingD_rep);
            closingInformationDetail.CurrentRateSetDate = ToMismoDate(this.DataLoan.sRLckdD_rep);
            closingInformationDetail.DisbursementDate = ToMismoDate(this.DataLoan.sDocMagicDisbursementD_rep);
            closingInformationDetail.DocumentPreparationDate = ToMismoDate(this.DataLoan.sDocMagicDocumentD_rep);
            closingInformationDetail.EstimatedPrepaidDaysCount = ToMismoCount(this.DataLoan.sIPiaDy_rep, null);
            closingInformationDetail.EstimatedPrepaidDaysPaidByType = ToMismoEnum(this.GetEstimatedPrepaidDaysPaidByBaseValue(this.DataLoan.sIPiaProps));

            // LQB only sets EstimatedPrepaidDaysPaidByType to Other when the prepayment was made by the Broker.
            if (closingInformationDetail.EstimatedPrepaidDaysPaidByType?.IsSetToOther ?? false)
            {
                closingInformationDetail.EstimatedPrepaidDaysPaidByTypeOtherDescription = ToMismoString("Broker");
            }

            closingInformationDetail.LoanEstimatedClosingDate = ToMismoDate(this.DataLoan.sDocMagicClosingD_rep);
            closingInformationDetail.LoanScheduledClosingDate = ToMismoDate(this.DataLoan.sDocMagicClosingD_rep);
            closingInformationDetail.RescissionDate = ToMismoDate(this.DataLoan.sDocMagicCancelD_rep);
            closingInformationDetail.ClosingDocumentsExpirationDate = ToMismoDate(this.DataLoan.sDocExpirationD_rep);

            closingInformationDetail.Extension = this.CreateClosingInformationDetailExtension();

            return closingInformationDetail;
        }

        /// <summary>
        /// Creates a container holding extension data on the closing process.
        /// </summary>
        /// <returns>A CLOSING_INFORMATION_DETAIL_EXTENSION container.</returns>
        private CLOSING_INFORMATION_DETAIL_EXTENSION CreateClosingInformationDetailExtension()
        {
            var extension = new CLOSING_INFORMATION_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBClosingDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates a container holding LendingQB extension values for CLOSING_INFORMATION_DETAIL.
        /// </summary>
        /// <returns>An LQB_CLOSING_INFORMATION_DETAIL_EXTENSION container.</returns>
        private LQB_CLOSING_INFORMATION_DETAIL_EXTENSION CreateLQBClosingDetailExtension()
        {
            var extension = new LQB_CLOSING_INFORMATION_DETAIL_EXTENSION();
            extension.EstimatedPrepaidDaysCount = ToMismoString(this.DataLoan.sIPiaDy_rep);
            extension.AdditionalInterestDaysRequiringConsentCount = ToMismoCount(
                string.IsNullOrEmpty(this.DataLoan.sAdditionalInterestDaysRequiringConsent_rep)
                    ? "0"
                    : this.DataLoan.sAdditionalInterestDaysRequiringConsent_rep,
                this.DataLoan.m_convertLos);

            return extension;
        }

        /// <summary>
        /// Retrieves the EstimatedPrepaidDaysPaidBy enumerated value corresponding to an LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding EstimatedPrepaidDaysPaidBy enumerated value.</returns>
        private EstimatedPrepaidDaysPaidByBase GetEstimatedPrepaidDaysPaidByBaseValue(int lqbValue)
        {
            switch (LosConvert.GfeItemProps_Payer(lqbValue))
            {
                case LosConvert.BORR_PAID_OUTOFPOCKET:
                case LosConvert.BORR_PAID_FINANCED:
                    return EstimatedPrepaidDaysPaidByBase.Buyer;
                case LosConvert.SELLER_PAID:
                    return EstimatedPrepaidDaysPaidByBase.Seller;
                case LosConvert.LENDER_PAID:
                    return EstimatedPrepaidDaysPaidByBase.Lender;
                case LosConvert.BROKER_PAID:
                    return EstimatedPrepaidDaysPaidByBase.Other;
                default:
                    throw new ArgumentException("Unhandled value='" + LosConvert.GfeItemProps_Payer(lqbValue).ToString() + "' for sIPiaProps");
            }
        }

        /// <summary>
        /// Creates a container holding Closing Instructions.
        /// </summary>
        /// <returns>Returns a container holding Closing Instructions.</returns>
        private CLOSING_INSTRUCTION CreateClosingInstruction()
        {
            CLOSING_INSTRUCTION closingInstruction = new CLOSING_INSTRUCTION();

            // Holds a list of conditions, including their description and a boolean indicating whether the
            // condition is complete.
            var conditionList = new List<Tuple<string, bool>>();

            if (this.DataLoan.BrokerDB.IsUseNewTaskSystem)
            {
                List<Task> tasks = Task.GetActiveConditionsByLoanId(this.DataLoan.sBrokerId, this.DataLoan.sLId, false);
                foreach (Task task in tasks)
                {
                    if (task.TaskStatus == E_TaskStatus.Closed)
                    {
                        continue; // Skip complete condition
                    }

                    conditionList.Add(new Tuple<string, bool>(task.TaskSubject, task.TaskStatus != E_TaskStatus.Active));
                }
            }
            else
            {
                LoanConditionSetObsolete conditionSet = new LoanConditionSetObsolete();

                if (this.DataLoan.BrokerDB.HasLenderDefaultFeatures)
                {
                    conditionSet.Retrieve(this.DataLoan.sLId, false, false, false);

                    foreach (CLoanConditionObsolete condition in conditionSet)
                    {
                        if (condition.CondStatus == E_CondStatus.Done)
                        {
                            continue; // Skip complete condition.
                        }

                        conditionList.Add(new Tuple<string, bool>(condition.CondDesc, condition.CondStatus != E_CondStatus.Active));
                    }
                }
                else
                {
                    int count = this.DataLoan.GetCondRecordCount();
                    for (int i = 0; i < count; i++)
                    {
                        CCondFieldsObsolete condition = this.DataLoan.GetCondFieldsObsolete(i);
                        if (condition.IsDone)
                        {
                            continue;
                        }

                        conditionList.Add(new Tuple<string, bool>(condition.CondDesc, condition.IsDone));
                    }
                }
            }

            closingInstruction.Conditions = this.CreateConditions(conditionList);
            closingInstruction.ClosingInstructionDetail = this.CreateClosingInstructionDetail();
            return closingInstruction;
        }

        /// <summary>
        /// Creates a container holding closing instruction detail.
        /// </summary>
        /// <returns>Returns a container holding closing instruction detail.</returns>
        private CLOSING_INSTRUCTION_DETAIL CreateClosingInstructionDetail()
        {
            CLOSING_INSTRUCTION_DETAIL closingInstructionDetail = new CLOSING_INSTRUCTION_DETAIL();
            if (this.PrimaryApp != null)
            {
                closingInstructionDetail.LeadBasedPaintCertificationRequiredIndicator = ToMismoIndicator(this.PrimaryApp.aFHABorrCertReceivedLeadPaintPoisonInfoTri);
            }

            closingInstructionDetail.ClosingInstructionsPropertyTaxMessageDescription = ToMismoString(this.DataLoan.sPropertyTaxMessageDescription);
            return closingInstructionDetail;
        }

        /// <summary>
        /// Creates a container to hold all instances of CONDITIONS.
        /// </summary>
        /// <param name="conditionList">A list of conditions holding descriptions and a boolean
        /// indicating whether they are complete.</param>
        /// <returns>A CONDITIONS container.</returns>
        private CONDITIONS CreateConditions(IEnumerable<Tuple<string, bool>> conditionList)
        {
            CONDITIONS conditions = new CONDITIONS();
            foreach (var condition in conditionList)
            {
                conditions.ConditionList.Add(this.CreateCondition(condition, GetNextSequenceNumber(conditions.ConditionList)));
            }

            return conditions;
        }

        /// <summary>
        /// Creates a container holding information on a condition.
        /// </summary>
        /// <param name="condition">Indicates the description of a condition and whether it is complete.</param>
        /// <param name="sequence">The sequence number of the condition among the list of conditions.</param>
        /// <returns>A CONDITION container.</returns>
        private CONDITION CreateCondition(Tuple<string, bool> condition, int sequence)
        {
            CONDITION conditionElement = new CONDITION();
            conditionElement.SequenceNumber = sequence;
            conditionElement.ConditionDescription = ToMismoString(condition.Item1);
            conditionElement.ConditionMetIndicator = ToMismoIndicator(condition.Item2);
            return conditionElement;
        }

        /// <summary>
        /// Creates a container holding data on a construction loan.
        /// </summary>
        /// <returns>A CONSTRUCTION container, or null if the loan is not a construction loan.</returns>
        private CONSTRUCTION CreateConstruction()
        {
            var construction = new CONSTRUCTION();
            if (this.ExportData.IsConstructionLoan)
            {
                construction.ConstructionLoanType = ToMismoEnum(this.GetConstructionLoanBaseValue(this.DataLoan.sLPurposeT));
            }

            construction.ConstructionImprovementCostsAmount = ToMismoAmount(this.DataLoan.sLotImprovC_rep);
            construction.ConstructionPeriodInterestRatePercent = ToMismoPercent(this.DataLoan.sConstructionPeriodIR_rep);
            construction.ConstructionPeriodNumberOfMonthsCount = ToMismoCount(this.DataLoan.sConstructionPeriodMon_rep, null);
            construction.ConstructionLoanEstimatedInterestCalculationMethodType = ToMismoEnum(this.GetConstructionLoanEstimatedInterestCalculationMethodBaseValue(this.DataLoan.sConstructionIntCalcT));
            construction.LandOriginalCostAmount = ToMismoAmount(this.DataLoan.sLotOrigC_rep);
            construction.LandEstimatedValueAmount = ToMismoAmount(this.DataLoan.sPresentValOfLot_rep);
            construction.ConstructionLoanInterestReserveAmount = ToMismoAmount(this.DataLoan.sIntReserveAmt_rep);

            // The construction purpose will be blank if the SMF migrations have not run to populate the new
            // construction data. If this is the case, don't serialize any of the new datapoints.
            if (this.DataLoan.sConstructionPurposeT != ConstructionPurpose.Blank)
            {
                construction.ConstructionPhaseInterestPaymentType = ToMismoEnum(this.GetConstructionPhaseInterestPaymentBaseValue(this.DataLoan.sConstructionPhaseIntPaymentTimingT));
                construction.ConstructionPhaseInterestPaymentFrequencyType = ToMismoEnum(this.GetConstructionPhaseInterestPaymentFrequencyBaseValue(this.DataLoan.sConstructionPhaseIntPaymentFrequencyT));
                construction.ConstructionToPermanentClosingType = ToMismoEnum(this.GetConstructionToPermanentClosingBaseValue(this.DataLoan.sConstructionToPermanentClosingT));

                construction.Extension = this.CreateConstructionExtension();
            }

            return construction;
        }

        /// <summary>
        /// Maps LendingQB data to a construction loan calculation method.
        /// </summary>
        /// <param name="calcType">The LendingQB calculation method.</param>
        /// <returns>A MISMO construction loan calculation method.</returns>
        private ConstructionLoanEstimatedInterestCalculationMethodBase GetConstructionLoanEstimatedInterestCalculationMethodBaseValue(ConstructionIntCalcType calcType)
        {
            switch (calcType)
            {
                case ConstructionIntCalcType.Blank:
                    return ConstructionLoanEstimatedInterestCalculationMethodBase.Blank;
                case ConstructionIntCalcType.FullCommitment:
                    return ConstructionLoanEstimatedInterestCalculationMethodBase.FullLoanCommitment;
                case ConstructionIntCalcType.HalfCommitment:
                    return ConstructionLoanEstimatedInterestCalculationMethodBase.HalfLoanCommitment;
                default:
                    throw new UnhandledEnumException(calcType);
            }
        }

        /// <summary>
        /// Maps a construction phase interest payment type to a MISMO value.
        /// </summary>
        /// <param name="paymentTiming">The interest payment timing during the construction phase.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private ConstructionPhaseInterestPaymentBase GetConstructionPhaseInterestPaymentBaseValue(ConstructionPhaseIntPaymentTiming paymentTiming)
        {
            switch (paymentTiming)
            {
                case ConstructionPhaseIntPaymentTiming.Blank:
                    return ConstructionPhaseInterestPaymentBase.Blank;
                case ConstructionPhaseIntPaymentTiming.InterestPaidAtEndOfConstruction:
                    return ConstructionPhaseInterestPaymentBase.InterestPaidAtEndOfConstruction;
                case ConstructionPhaseIntPaymentTiming.InterestPaidPeriodically:
                    return ConstructionPhaseInterestPaymentBase.InterestPaidPeriodically;
                case ConstructionPhaseIntPaymentTiming.Other:
                    return ConstructionPhaseInterestPaymentBase.Other;
                default:
                    throw new UnhandledEnumException(paymentTiming);
            }
        }

        /// <summary>
        /// Maps a construction phase interest payment frequency enum to a MISMO value.
        /// </summary>
        /// <param name="paymentFrequency">The interest payment frequency during the construction phase.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private ConstructionPhaseInterestPaymentFrequencyBase GetConstructionPhaseInterestPaymentFrequencyBaseValue(ConstructionPhaseIntPaymentFrequency paymentFrequency)
        {
            switch (paymentFrequency)
            {
                case ConstructionPhaseIntPaymentFrequency.Blank:
                    return ConstructionPhaseInterestPaymentFrequencyBase.Blank;
                case ConstructionPhaseIntPaymentFrequency.Biweekly:
                    return ConstructionPhaseInterestPaymentFrequencyBase.Biweekly;
                case ConstructionPhaseIntPaymentFrequency.Monthly:
                    return ConstructionPhaseInterestPaymentFrequencyBase.Monthly;
                case ConstructionPhaseIntPaymentFrequency.Other:
                    return ConstructionPhaseInterestPaymentFrequencyBase.Other;
                default:
                    throw new UnhandledEnumException(paymentFrequency);
            }
        }

        /// <summary>
        /// Maps a construction-to-permanent closing type to a MISMO value.
        /// </summary>
        /// <param name="closingType">The closing type for a construction-to-permanent loan.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private ConstructionToPermanentClosingBase GetConstructionToPermanentClosingBaseValue(ConstructionToPermanentClosingType closingType)
        {
            switch (closingType)
            {
                case ConstructionToPermanentClosingType.Blank:
                    return ConstructionToPermanentClosingBase.Blank;
                case ConstructionToPermanentClosingType.OneClosing:
                    return ConstructionToPermanentClosingBase.OneClosing;
                case ConstructionToPermanentClosingType.TwoClosing:
                    return ConstructionToPermanentClosingBase.TwoClosing;
                case ConstructionToPermanentClosingType.Other:
                    return ConstructionToPermanentClosingBase.Other;
                default:
                    throw new UnhandledEnumException(closingType);
            }
        }

        /// <summary>
        /// Creates an extension container with data on a construction loan.
        /// </summary>
        /// <returns>A CONSTRUCTION_EXTENSION container.</returns>
        private CONSTRUCTION_EXTENSION CreateConstructionExtension()
        {
            var extension = new CONSTRUCTION_EXTENSION();

            if (this.DataLoan.sConstructionAmortT == E_sFinMethT.ARM)
            {
                extension.Mismo = this.CreateMismoConstructionExtension();
            }

            extension.Other = this.CreateLqbConstructionExtension();

            return extension;
        }

        /// <summary>
        /// Creates a MISMO extension with data on a construction loan.
        /// </summary>
        /// <returns>A MISMO_CONSTRUCTION_EXTENSION container.</returns>
        private MISMO_CONSTRUCTION_EXTENSION CreateMismoConstructionExtension()
        {
            var extension = new MISMO_CONSTRUCTION_EXTENSION();
            extension.Adjustment = this.CreateAdjustment_Construction();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LQB extension with data on a construction loan.
        /// </summary>
        /// <returns>An LQB_CONSTRUCTION_EXTENSION container.</returns>
        private LQB_CONSTRUCTION_EXTENSION CreateLqbConstructionExtension()
        {
            var extension = new LQB_CONSTRUCTION_EXTENSION();
            extension.ConstructionPurposeType = ToMismoEnum(this.GetLqbConstructionPurposeBaseValue(this.DataLoan.sConstructionPurposeT));
            extension.ConstructionLoanAmortizationType = ToMismoEnum(this.GetLqbConstructionLoanAmortizationBaseValue(this.DataLoan.sConstructionAmortT));
            extension.ConstructionInitialAdvanceAmount = ToMismoAmount(this.DataLoan.sConstructionInitialAdvanceAmt_rep);
            extension.ConstructionRequiredInterestReserveIndicator = ToMismoIndicator(this.DataLoan.sIsIntReserveRequired);
            extension.ConstructionPhaseInterestAccrualType = ToMismoEnum(this.GetLqbConstructionPhaseInterestAccrualBaseValue(this.DataLoan.sConstructionPhaseIntAccrualT));
            extension.ConstructionLoanDate = ToMismoDate(this.DataLoan.sConstructionLoanD_rep);
            extension.ConstructionPeriodEndDate = ToMismoDate(this.DataLoan.sConstructionPeriodEndD_rep);
            extension.ConstructionFirstPaymentDueDate = ToMismoDate(this.DataLoan.sConstructionFirstPaymentD_rep);
            extension.LandCostAmount = ToMismoAmount(this.DataLoan.sLandCost_rep);
            extension.LotValueAmount = ToMismoAmount(this.DataLoan.sLotVal_rep);
            extension.LotAcquiredDate = ToMismoDate(this.DataLoan.sLotAcquiredD_rep);
            extension.LotOwnerType = ToMismoEnum(this.GetLqbLotOwnerBaseValue(this.DataLoan.sLotOwnerT));
            extension.SubsequentlyPaidFinanceChargeAmount = ToMismoAmount(this.DataLoan.sSubsequentlyPaidFinanceChargeAmt_rep);
            extension.ConstructionInterestAmount = ToMismoAmount(this.DataLoan.sConstructionIntAmount_rep);
            extension.ConstructionInterestAccrualDate = ToMismoDate(this.DataLoan.sConstructionIntAccrualD_rep);
            extension.ConstructionDisclosureType = ToMismoEnum(this.GetLqbConstructionDisclosureBaseValue(this.DataLoan.sConstructionDiscT));

            return extension;
        }

        /// <summary>
        /// Maps LendingQB data to a MISMO construction purpose.
        /// </summary>
        /// <param name="purpose">The LendingQB construction purpose.</param>
        /// <returns>A MISMO construction purpose.</returns>
        private LqbConstructionPurposeBase GetLqbConstructionPurposeBaseValue(ConstructionPurpose purpose)
        {
            switch (purpose)
            {
                case ConstructionPurpose.Blank:
                    return LqbConstructionPurposeBase.Blank;
                case ConstructionPurpose.ConstructionAndLotPurchase:
                    return LqbConstructionPurposeBase.ConstructionAndLotPurchase;
                case ConstructionPurpose.ConstructionAndLotRefinance:
                    return LqbConstructionPurposeBase.ConstructionAndLotRefinance;
                case ConstructionPurpose.ConstructionOnOwnedLot:
                    return LqbConstructionPurposeBase.Construction;
                default:
                    throw new UnhandledEnumException(purpose);
            }
        }

        /// <summary>
        /// Maps LendingQB data to a MISMO construction loan amortization type.
        /// </summary>
        /// <param name="amortType">The amortization type.</param>
        /// <returns>A MISMO construction loan amortization type.</returns>
        private LqbConstructionLoanAmortizationBase GetLqbConstructionLoanAmortizationBaseValue(E_sFinMethT amortType)
        {
            switch (amortType)
            {
                case E_sFinMethT.ARM:
                    return LqbConstructionLoanAmortizationBase.AdjustableRate;
                case E_sFinMethT.Fixed:
                    return LqbConstructionLoanAmortizationBase.Fixed;
                default:
                    throw new UnhandledEnumException(amortType);
            }
        }

        /// <summary>
        /// Maps LendingQB data to a MISMO construction phase interest accrual type.
        /// </summary>
        /// <param name="accrualType">The interest accrual type.</param>
        /// <returns>A MISMO construction phase interest accrual type.</returns>
        private LqbConstructionPhaseInterestAccrualBase GetLqbConstructionPhaseInterestAccrualBaseValue(ConstructionPhaseIntAccrual accrualType)
        {
            switch (accrualType)
            {
                case ConstructionPhaseIntAccrual.Blank:
                    return LqbConstructionPhaseInterestAccrualBase.Blank;
                case ConstructionPhaseIntAccrual.ActualDays_365_360:
                    return LqbConstructionPhaseInterestAccrualBase.Daily365_360;
                case ConstructionPhaseIntAccrual.ActualDays_365_365:
                    return LqbConstructionPhaseInterestAccrualBase.Daily365_365;
                case ConstructionPhaseIntAccrual.Monthly_360_360:
                    return LqbConstructionPhaseInterestAccrualBase.Monthly360_360;
                default:
                    throw new UnhandledEnumException(accrualType);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the lot owner.
        /// </summary>
        /// <param name="lotOwner">The lot owner.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbLotOwnerBase GetLqbLotOwnerBaseValue(LotOwnerType lotOwner)
        {
            switch (lotOwner)
            {
                case LotOwnerType.Blank:
                    return LqbLotOwnerBase.Blank;
                case LotOwnerType.Borrower:
                    return LqbLotOwnerBase.Borrower;
                case LotOwnerType.Builder:
                    return LqbLotOwnerBase.Builder;
                case LotOwnerType.Other:
                    return LqbLotOwnerBase.Other;
                default:
                    throw new UnhandledEnumException(lotOwner);
            }
        }

        /// <summary>
        /// Gets the MISMO value corresponding to the construction disclosure type.
        /// </summary>
        /// <param name="disclosureType">The construction disclosure type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbConstructionDisclosureBase GetLqbConstructionDisclosureBaseValue(ConstructionDisclosureType disclosureType)
        {
            switch (disclosureType)
            {
                case ConstructionDisclosureType.Blank:
                    return LqbConstructionDisclosureBase.Blank;
                case ConstructionDisclosureType.Combined:
                    return LqbConstructionDisclosureBase.Combined;
                case ConstructionDisclosureType.Separate:
                    return LqbConstructionDisclosureBase.Separate;
                default:
                    throw new UnhandledEnumException(disclosureType);
            }
        }

        /// <summary>
        /// Retrieves the ConstructionLoan enumeration corresponding to an LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>The corresponding ConstructionLoan value.</returns>
        private ConstructionLoanBase GetConstructionLoanBaseValue(E_sLPurposeT lqbValue)
        {
            switch (lqbValue)
            {
                case E_sLPurposeT.Construct:
                    return ConstructionLoanBase.ConstructionOnly;
                case E_sLPurposeT.ConstructPerm:
                    return ConstructionLoanBase.ConstructionToPermanent;
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.HomeEquity:
                case E_sLPurposeT.Purchase:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.Other:
                    return ConstructionLoanBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Creates the DOCUMENTATIONS container.
        /// </summary>
        /// <returns>The container.</returns>
        private DOCUMENTATIONS CreateDocumentations()
        {
            DOCUMENTATIONS documentations = new DOCUMENTATIONS();
            documentations.DocumentationList.Add(this.CreateDocumentation(GetNextSequenceNumber(documentations.DocumentationList)));

            return documentations;
        }

        /// <summary>
        /// Creates a DOCUMENTATION container.
        /// </summary>
        /// <param name="sequenceNumber">A sequence number.</param>
        /// <returns>The container.</returns>
        private DOCUMENTATION CreateDocumentation(int sequenceNumber)
        {
            var doc = new DOCUMENTATION();
            doc.SequenceNumber = sequenceNumber;

            doc.Extension = this.CreateDocumentationExtension();

            return doc;
        }

        /// <summary>
        /// Creates the DOCUMENTATION container extension.
        /// </summary>
        /// <returns>The container.</returns>
        private DOCUMENTATION_EXTENSION CreateDocumentationExtension()
        {
            DOCUMENTATION_EXTENSION extension = new DOCUMENTATION_EXTENSION();
            extension.Other = this.CreateLqbDocumentationExtension();

            return extension;
        }

        /// <summary>
        /// Creates LQB Documentation container extension.
        /// </summary>
        /// <returns>The extension container.</returns>
        private LQB_DOCUMENTATION_EXTENSION CreateLqbDocumentationExtension()
        {
            LQB_DOCUMENTATION_EXTENSION extension = new LQB_DOCUMENTATION_EXTENSION();
            extension.DocumentTypeName = ToMismoEnum(this.GetLqbDocumentTypeNameBaseValue(this.DataLoan.sProdDocT));

            return extension;
        }

        /// <summary>
        /// Maps the document type to MISMO.
        /// </summary>
        /// <param name="docType">The doc type.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LqbDocumentTypeNameBase GetLqbDocumentTypeNameBaseValue(E_sProdDocT docType)
        {
            switch (docType)
            {
                case E_sProdDocT.Alt:
                    return LqbDocumentTypeNameBase.Alt;
                case E_sProdDocT.AssetUtilization:
                    return LqbDocumentTypeNameBase.AssetUtilization;
                case E_sProdDocT.DebtServiceCoverage:
                    return LqbDocumentTypeNameBase.DebtServiceCoverage;
                case E_sProdDocT.Full:
                    return LqbDocumentTypeNameBase.Full;
                case E_sProdDocT.Light:
                    return LqbDocumentTypeNameBase.Light;
                case E_sProdDocT.NINA:
                    return LqbDocumentTypeNameBase.NINA;
                case E_sProdDocT.NINANE:
                    return LqbDocumentTypeNameBase.NINANE;
                case E_sProdDocT.NISA:
                    return LqbDocumentTypeNameBase.NISA;
                case E_sProdDocT.NIVA:
                    return LqbDocumentTypeNameBase.NIVA;
                case E_sProdDocT.NIVANE:
                    return LqbDocumentTypeNameBase.NIVANE;
                case E_sProdDocT.NoIncome:
                    return LqbDocumentTypeNameBase.NoIncome;
                case E_sProdDocT.OtherBankStatements:
                    return LqbDocumentTypeNameBase.OtherBankStatements;
                case E_sProdDocT.SISA:
                    return LqbDocumentTypeNameBase.SISA;
                case E_sProdDocT.SIVA:
                    return LqbDocumentTypeNameBase.SIVA;
                case E_sProdDocT.Streamline:
                    return LqbDocumentTypeNameBase.Streamline;
                case E_sProdDocT.VINA:
                    return LqbDocumentTypeNameBase.VINA;
                case E_sProdDocT.VISA:
                    return LqbDocumentTypeNameBase.VISA;
                case E_sProdDocT.Voe:
                    return LqbDocumentTypeNameBase.Voe;
                case E_sProdDocT._12MoBusinessBankStatements:
                    return LqbDocumentTypeNameBase._12MoBusinessBankStatements;
                case E_sProdDocT._12MoPersonalBankStatements:
                    return LqbDocumentTypeNameBase._12MoPersonalBankStatements;
                case E_sProdDocT._1YrTaxReturns:
                    return LqbDocumentTypeNameBase._1YrTaxReturns;
                case E_sProdDocT._24MoBusinessBankStatements:
                    return LqbDocumentTypeNameBase._24MoBusinessBankStatements;
                case E_sProdDocT._24MoPersonalBankStatements:
                    return LqbDocumentTypeNameBase._24MoPersonalBankStatements;
                default:
                    throw new UnhandledEnumException(docType);
            }
        }

        /// <summary>
        /// Creates a container holding data on index rules for a construction loan.
        /// </summary>
        /// <returns>An INDEX_RULES container.</returns>
        private INDEX_RULES CreateIndexRules_Construction()
        {
            var indexRules = new INDEX_RULES();
            indexRules.IndexRuleList.Add(this.CreateIndexRule_Construction());

            return indexRules;
        }

        /// <summary>
        /// Creates a container holding data on an index rule for a construction loan.
        /// </summary>
        /// <returns>An INDEX_RULE container.</returns>
        private INDEX_RULE CreateIndexRule_Construction()
        {
            var indexRule = new INDEX_RULE();
            indexRule.IndexType = ToMismoEnum(IndexBase.Other);
            indexRule.IndexTypeOtherDescription = ToMismoString(this.DataLoan.sConstructionArmIndexNameVstr);

            indexRule.SequenceNumber = 1;
            return indexRule;
        }

        /// <summary>
        /// Creates a container holding data on an interest rate adjustment for a construction ARM.
        /// </summary>
        /// <returns>An INTEREST_RATE_ADJUSTMENT container.</returns>
        private INTEREST_RATE_ADJUSTMENT CreateInterestRateAdjustment_Construction()
        {
            var interestRateAdjustment = new INTEREST_RATE_ADJUSTMENT();
            interestRateAdjustment.InterestRatePerChangeAdjustmentRules = this.CreateInterestRatePerChangeAdjustmentRules_Construction();
            interestRateAdjustment.InterestRateLifetimeAdjustmentRule = this.CreateInterestRateLifetimeAdjustmentRule_Construction();
            interestRateAdjustment.IndexRules = this.CreateIndexRules_Construction();

            return interestRateAdjustment;
        }

        /// <summary>
        /// Creates a container holding data on interest rate adjustment rules for a construction ARM.
        /// </summary>
        /// <returns>An INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES container.</returns>
        private INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES CreateInterestRatePerChangeAdjustmentRules_Construction()
        {
            var adjustmentRules = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULES();
            adjustmentRules.InterestRatePerChangeAdjustmentRuleList.Add(this.CreateInterestRatePerChangeAdjustmentRule_Construction(sequenceNumber: 1));
            adjustmentRules.InterestRatePerChangeAdjustmentRuleList.Add(this.CreateInterestRatePerChangeAdjustmentRule_Construction(sequenceNumber: 2));

            return adjustmentRules;
        }

        /// <summary>
        /// Creates a container holding interest rate adjustment rules for a construction ARM.
        /// </summary>
        /// <param name="sequenceNumber">The container's sequence number.</param>
        /// <returns>An INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE container.</returns>
        private INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE CreateInterestRatePerChangeAdjustmentRule_Construction(int sequenceNumber)
        {
            var adjustmentRule = new INTEREST_RATE_PER_CHANGE_ADJUSTMENT_RULE();

            if (sequenceNumber == 1)
            {
                adjustmentRule.AdjustmentRuleType = ToMismoEnum(AdjustmentRuleBase.First);
                adjustmentRule.PerChangeMaximumIncreaseRatePercent = ToMismoPercent(this.DataLoan.sConstructionRAdj1stCapR_rep);
                adjustmentRule.PerChangeRateAdjustmentFrequencyMonthsCount = ToMismoCount(this.DataLoan.sConstructionRAdj1stCapMon_rep, this.DataLoan.m_convertLos);
            }
            else
            {
                adjustmentRule.AdjustmentRuleType = ToMismoEnum(AdjustmentRuleBase.Subsequent);
                adjustmentRule.PerChangeMaximumIncreaseRatePercent = ToMismoPercent(this.DataLoan.sConstructionRAdjCapR_rep);
                adjustmentRule.PerChangeRateAdjustmentFrequencyMonthsCount = ToMismoCount(this.DataLoan.sConstructionRAdjCapMon_rep, this.DataLoan.m_convertLos);
            }

            return adjustmentRule;
        }

        /// <summary>
        /// Creates a container holding data on a lifetime adjustment rule for a construction ARM.
        /// </summary>
        /// <returns>An INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE container.</returns>
        private INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE CreateInterestRateLifetimeAdjustmentRule_Construction()
        {
            var adjustmentRule = new INTEREST_RATE_LIFETIME_ADJUSTMENT_RULE();
            adjustmentRule.MarginRatePercent = ToMismoPercent(this.DataLoan.sConstructionRAdjMarginR_rep);
            adjustmentRule.MaximumIncreaseRatePercent = ToMismoPercent(this.DataLoan.sConstructionRAdjLifeCapR_rep);
            adjustmentRule.FloorRatePercent = ToMismoPercent(this.DataLoan.sConstructionRAdjFloorR_rep);
            adjustmentRule.InterestRateRoundingType = ToMismoEnum(GetInterestRateRoundingValue(this.DataLoan.sConstructionRAdjRoundT));
            adjustmentRule.InterestRateRoundingPercent = ToMismoPercent(this.DataLoan.sConstructionRAdjRoundToR_rep);

            return adjustmentRule;
        }

        /// <summary>
        /// Creates a container holding data on an interest calculation.
        /// </summary>
        /// <returns>An INTEREST_CALCULATION container.</returns>
        private INTEREST_CALCULATION CreateInterestCalculation()
        {
            var interestCalculation = new INTEREST_CALCULATION();
            interestCalculation.InterestCalculationRules = this.CreateInterestCalculationRules();

            return interestCalculation;
        }

        /// <summary>
        /// Creates a container holding data on a set of interest calculation rules.
        /// </summary>
        /// <returns>An INTEREST_CALCULATION_RULE container.</returns>
        private INTEREST_CALCULATION_RULES CreateInterestCalculationRules()
        {
            var interestCalculationRules = new INTEREST_CALCULATION_RULES();
            interestCalculationRules.InterestCalculationRuleList.Add(
                this.CreateInterestCalculationRule(
                    GetNextSequenceNumber(interestCalculationRules.InterestCalculationRuleList)));

            return interestCalculationRules;
        }

        /// <summary>
        /// Creates a container holding data on an interest calculation rule.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number of the element.</param>
        /// <returns>An INTEREST_CALCULATION_RULE container.</returns>
        private INTEREST_CALCULATION_RULE CreateInterestCalculationRule(int sequenceNumber)
        {
            var interestCalculationRule = new INTEREST_CALCULATION_RULE();
            interestCalculationRule.SequenceNumber = sequenceNumber;
            interestCalculationRule.LoanInterestAccrualStartDate = ToMismoDate(this.DataLoan.sConsummationD_rep);

            return interestCalculationRule;
        }

        /// <summary>
        /// Creates an INVESTOR_LOAN_INFORMATION container.
        /// </summary>
        /// <returns>An INVESTOR_LOAN_INFORMATION container.</returns>
        private INVESTOR_LOAN_INFORMATION CreateInvestorLoanInformation()
        {
            var investorLoanInformation = new INVESTOR_LOAN_INFORMATION();
            investorLoanInformation.Extension = this.CreateInvestorLoanInformationExtension();

            return investorLoanInformation;
        }

        /// <summary>
        /// Creates an extension holding investor loan information.
        /// </summary>
        /// <returns>An INVESTOR_LOAN_INFORMATION_EXTENSION container.</returns>
        private INVESTOR_LOAN_INFORMATION_EXTENSION CreateInvestorLoanInformationExtension()
        {
            var extension = new INVESTOR_LOAN_INFORMATION_EXTENSION();
            extension.Other = this.CreateLqbInvestorLoanInformationExtension();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container holding investor loan information.
        /// </summary>
        /// <returns>An LQB_INVESTOR_LOAN_INFORMATION_EXTENSION container.</returns>
        private LQB_INVESTOR_LOAN_INFORMATION_EXTENSION CreateLqbInvestorLoanInformationExtension()
        {
            var extension = new LQB_INVESTOR_LOAN_INFORMATION_EXTENSION();
            extension.InvestorProgramIdentifier = ToMismoIdentifier(this.DataLoan.sInvestorLockProgramId);

            return extension;
        }

        /// <summary>
        /// Creates a container holding nodes with information on late charges.
        /// </summary>
        /// <returns>A LATE_CHARGE container.</returns>
        private LATE_CHARGE CreateLateCharge()
        {
            var lateCharge = new LATE_CHARGE();
            lateCharge.LateChargeRule = this.CreateLateChargeRule();

            return lateCharge;
        }

        /// <summary>
        /// Creates a container holding data on the late charge rules.
        /// </summary>
        /// <returns>A LATE_CHARGE_RULE container.</returns>
        private LATE_CHARGE_RULE CreateLateChargeRule()
        {
            var lateChargeRule = new LATE_CHARGE_RULE();
            lateChargeRule.LateChargeGracePeriodDaysCount = ToMismoCount(this.DataLoan.sLateDays, this.DataLoan.m_convertLos);
            lateChargeRule.LateChargeRatePercent = ToMismoPercent(this.DataLoan.sLateChargePc.Replace("%", string.Empty));

            return lateChargeRule;
        }

        /// <summary>
        /// Creates a container holding data on the details of a loan.
        /// </summary>
        /// <returns>A LOAN_DETAIL container populated with data.</returns>
        private LOAN_DETAIL CreateLoanDetail()
        {
            var loanDetail = new LOAN_DETAIL();
            loanDetail.ApplicationReceivedDate = ToMismoDate(this.DataLoan.sAppReceivedByLenderD_rep);
            loanDetail.BalloonIndicator = ToMismoIndicator(this.DataLoan.sBalloonPmt);
            loanDetail.DemandFeatureIndicator = ToMismoIndicator(this.DataLoan.sHasDemandFeature);
            loanDetail.EscrowIndicator = ToMismoIndicator(this.DataLoan.sTridEscrowAccountExists);
            loanDetail.EscrowAccountLenderRequirementType = ToMismoEnum(this.GetEscrowAccountLenderRequirementBaseValue(this.DataLoan.sNonMIHousingExpensesEscrowedReasonT, this.DataLoan.sTridEscrowAccountExists));
            loanDetail.EscrowAbsenceReasonType = ToMismoEnum(this.GetEscrowAbsenceReasonBase(this.DataLoan.sNonMIHousingExpensesNotEscrowedReasonT));

            if (loanDetail.EscrowAbsenceReasonType?.IsSetToOther ?? false)
            {
                loanDetail.EscrowAbsenceReasonTypeOtherDescription = ToMismoString(this.DataLoan.sNonMIHousingExpensesNotEscrowedReasonT);
            }

            loanDetail.PrepaymentPenaltyIndicator = ToMismoIndicator(this.DataLoan.sPrepmtPenaltyT == E_sPrepmtPenaltyT.May);
            loanDetail.HELOCIndicator = ToMismoIndicator(this.DataLoan.BrokerDB.IsEnableHELOC && this.DataLoan.sIsLineOfCredit);
            loanDetail.BuydownTemporarySubsidyFundingIndicator = ToMismoIndicator(this.DataLoan.sHasTempBuydown);
            loanDetail.ServicingTransferStatusType = ToMismoEnum(this.DataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan ? ServicingTransferStatusBase.Retained : ServicingTransferStatusBase.Released);
            loanDetail.HigherPricedMortgageLoanIndicator = this.ToHigherPricedMortgageLoanIndicator(this.DataLoan.sHighPricedMortgageT);
            loanDetail.CreditorServicingOfLoanStatementType = this.ToCreditorServicingOfLoanStatementEnum(this.DataLoan.sMayAssignSellTransfer, this.DataLoan.sTRIDLoanEstimateLenderIntendsToServiceLoan);
            loanDetail.ConstructionLoanIndicator = ToMismoIndicator(this.ExportData.IsConstructionLoan);
            loanDetail.TotalSubordinateFinancingAmount = ToMismoAmount(this.DataLoan.sONewFinBal_rep);
            loanDetail.LoanApprovalExpirationDate = ToMismoDate(this.DataLoan.sAppExpD_rep);
            loanDetail.WarehouseLenderIndicator = ToMismoIndicator(this.DataLoan.sWarehouseLenderRolodexId >= 0);
            loanDetail.QualifiedMortgageIndicator = ToMismoIndicator(this.DataLoan.sQMStatusT != E_sQMStatusT.Ineligible);
            loanDetail.TotalMortgagedPropertiesCount = ToMismoCount(this.DataLoan.sNumFinancedProperties_rep, null);
            loanDetail.ConvertibleIndicator = ToMismoIndicator(this.DataLoan.sIsConvertibleMortgage);
            loanDetail.PropertyInspectionWaiverIndicator = ToMismoIndicator(this.DataLoan.sSpValuationMethodT == E_sSpValuationMethodT.None && this.DataLoan.sSpGseCollateralProgramT == E_sSpGseCollateralProgramT.PropertyInspectionWaiver);

            if (this.DataLoan.sAssumeLT != E_sAssumeLT.LeaveBlank)
            {
                // MayNot maps to false, May or MaySubjectToCondition map to true.
                bool assumable = this.DataLoan.sAssumeLT != E_sAssumeLT.MayNot;
                loanDetail.AssumabilityIndicator = ToMismoIndicator(assumable);
            }

            loanDetail.Extension = this.CreateLoanDetailExtension();

            // OPM 247161 - UCD Support.
            loanDetail.LoanAmountIncreaseIndicator = ToMismoIndicator(this.DataLoan.sGfeCanLoanBalanceIncrease);
            loanDetail.InterestRateIncreaseIndicator = ToMismoIndicator(this.DataLoan.sGfeCanRateIncrease);
            loanDetail.PaymentIncreaseIndicator = ToMismoIndicator(this.DataLoan.sGfeCanPaymentIncrease);
            loanDetail.InterestOnlyIndicator = ToMismoIndicator(this.DataLoan.sIsIOnly);
            loanDetail.BalloonPaymentAmount = ToMismoAmount(this.DataLoan.sGfeBalloonPmt_rep);

            loanDetail.LoanFundingDate = ToMismoDate(this.DataLoan.sFundD_rep);

            return loanDetail;
        }

        /// <summary>
        /// Creates an EXTENSION container to put in the LOAN_DETAIL element.
        /// </summary>
        /// <returns>A LOAN_DETAIL_EXTENSION element to serialize to LOAN_DETAIL/EXTENSION.</returns>
        private LOAN_DETAIL_EXTENSION CreateLoanDetailExtension()
        {
            var extension = new LOAN_DETAIL_EXTENSION();

            extension.Other = this.CreateLQBLoanDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates the proprietary LQB OTHER container for holding information pertaining to the LOAN_DETAIL.
        /// </summary>
        /// <returns>An LQB_LOAN_DETAIL_EXTENSION object suitable to serialize to LOAN_DETAIL/EXTENSION/OTHER.</returns>
        private LQB_LOAN_DETAIL_EXTENSION CreateLQBLoanDetailExtension()
        {
            var lqbExtension = new LQB_LOAN_DETAIL_EXTENSION();

            lqbExtension.LoanOriginationChannelType = ToMismoEnum(this.GetLqbLoanOriginationChannelBaseValue(this.DataLoan.sBranchChannelT));
            lqbExtension.TridTargetRegulationVersionType = ToMismoEnum(this.GetLqbTridTargetRegulationVersionBaseValue(this.DataLoan.sTridTargetRegulationVersionT));
            lqbExtension.IntentToProceedDate = ToMismoDate(this.DataLoan.sIntentToProceedD_rep);
            lqbExtension.NewConcurrentOtherFinancingIndicator = ToMismoIndicator(this.DataLoan.sIsNewConcurrentOtherFinancing);

            return lqbExtension;
        }

        /// <summary>
        /// Converts the given reason for having an escrow account to the corresponding MISMO type.
        /// </summary>
        /// <param name="reason">The reason for having an escrow account.</param>
        /// <param name="hasEscrow">Indicates whether the loan has an escrow account.</param>
        /// <returns>The MISMO value corresponding to the reason for having an escrow account.</returns>
        private EscrowAccountLenderRequirementBase GetEscrowAccountLenderRequirementBaseValue(E_sNonMIHousingExpensesEscrowedReasonT reason, bool hasEscrow)
        {
            if (reason == E_sNonMIHousingExpensesEscrowedReasonT.LenderRequired)
            {
                return EscrowAccountLenderRequirementBase.EscrowsRequiredByLender;
            }
            else if (reason == E_sNonMIHousingExpensesEscrowedReasonT.Unknown && hasEscrow)
            {
                return EscrowAccountLenderRequirementBase.Blank;
            }
            else
            {
                return EscrowAccountLenderRequirementBase.EscrowsWaivedByLender;
            }
        }

        /// <summary>
        /// Converts the given reason for lack of an escrow account to the corresponding MISMO type.
        /// </summary>
        /// <param name="reason">The reason for lack of an escrow account.</param>
        /// <returns>The EscrowAbsenceReasonBase corresponding to the reason for lack of an escrow account.</returns>
        private EscrowAbsenceReasonBase GetEscrowAbsenceReasonBase(E_sNonMIHousingExpensesNotEscrowedReasonT reason)
        {
            switch (reason)
            {
                case E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined:
                    return EscrowAbsenceReasonBase.BorrowerDeclined;
                case E_sNonMIHousingExpensesNotEscrowedReasonT.LenderDidNotOffer:
                    return EscrowAbsenceReasonBase.LenderDoesNotOffer;
                case E_sNonMIHousingExpensesNotEscrowedReasonT.Unknown:
                    return EscrowAbsenceReasonBase.Blank;
                default:
                    throw new UnhandledEnumException(reason);
            }
        }

        /// <summary>
        /// Creates an indicator determining whether the loan is higher priced.
        /// </summary>
        /// <param name="highPricedType">The high priced mortgage type.</param>
        /// <returns>A <see cref="MISMOIndicator"/> object.</returns>
        private MISMOIndicator ToHigherPricedMortgageLoanIndicator(E_HighPricedMortgageT highPricedType)
        {
            if (highPricedType == E_HighPricedMortgageT.None || highPricedType == E_HighPricedMortgageT.Unknown)
            {
                return null;
            }

            bool isHighPriced = highPricedType == E_HighPricedMortgageT.HigherPricedQm || highPricedType == E_HighPricedMortgageT.HPML;
            return ToMismoIndicator(isHighPriced);
        }

        /// <summary>
        /// Creates a creditor servicing of loan enumeration object based on the given indicator.
        /// </summary>
        /// <param name="lenderMayTransferLoan">Indicates whether it is possible that the lender will transfer some ownership of the loan.</param>
        /// <param name="lenderIntendsToServiceLoan">Indicates whether the creditor intends to service the loan.</param>
        /// <returns>A serializable <see cref="MISMOEnum{CreditorServicingOfLoanStatementBase}" /> object.</returns>
        private MISMOEnum<CreditorServicingOfLoanStatementBase> ToCreditorServicingOfLoanStatementEnum(bool lenderMayTransferLoan, bool lenderIntendsToServiceLoan)
        {
            var creditorServicingOfLoanStatementEnum = new MISMOEnum<CreditorServicingOfLoanStatementBase>();

            CreditorServicingOfLoanStatementBase servicingType;
            if (lenderMayTransferLoan)
            {
                servicingType = CreditorServicingOfLoanStatementBase.CreditorMayAssignSellOrTransferServicingOfLoan;
            }
            else if (lenderIntendsToServiceLoan)
            {
                servicingType = CreditorServicingOfLoanStatementBase.CreditorIntendsToServiceLoan;
            }
            else
            {
                servicingType = CreditorServicingOfLoanStatementBase.CreditorIntendsToTransferServicingOfLoan;
            }

            return ToMismoEnum(servicingType);
        }

        /// <summary>
        /// Converts an LQB branch channel into a Loan Origination Channel value.
        /// </summary>
        /// <param name="branchChannel">The LQB value to convert.</param>
        /// <returns>The corresponding MISMO extension value.</returns>
        private LqbLoanOriginationChannelBase GetLqbLoanOriginationChannelBaseValue(E_BranchChannelT branchChannel)
        {
            switch (branchChannel)
            {
                case E_BranchChannelT.Retail:
                    return LqbLoanOriginationChannelBase.Retail;
                case E_BranchChannelT.Wholesale:
                    return LqbLoanOriginationChannelBase.Wholesale;
                case E_BranchChannelT.Correspondent:
                    return LqbLoanOriginationChannelBase.Correspondent;
                case E_BranchChannelT.Broker:
                    return LqbLoanOriginationChannelBase.BrokeredOut;
                case E_BranchChannelT.Blank:
                    return LqbLoanOriginationChannelBase.Blank;
                default:
                    throw new UnhandledEnumException(branchChannel);
            }
        }

        /// <summary>
        /// Gets the TRID regulation serialization enum value.
        /// </summary>
        /// <param name="version">The TRID version.</param>
        /// <returns>The corresponding serialization value.</returns>
        private LqbTridTargetRegulationVersionBase GetLqbTridTargetRegulationVersionBaseValue(TridTargetRegulationVersionT version)
        {
            switch (version)
            {
                case TridTargetRegulationVersionT.TRID2015:
                    return LqbTridTargetRegulationVersionBase.TRID2015;
                case TridTargetRegulationVersionT.TRID2017:
                    return LqbTridTargetRegulationVersionBase.TRID2017;
                case TridTargetRegulationVersionT.Blank:
                    return LqbTridTargetRegulationVersionBase.Blank;
                default:
                    throw new UnhandledEnumException(version);
            }
        }

        /// <summary>
        /// Creates a container that holds all instances of LOAN_IDENTIFIER.
        /// </summary>
        /// <returns>A LOAN_IDENTIFIERS container.</returns>
        private LOAN_IDENTIFIERS CreateLoanIdentifiers()
        {
            var loanIdentifiers = new LOAN_IDENTIFIERS();
            loanIdentifiers.LoanIdentifierList.Add(
                this.CreateLoanIdentifier(
                    this.DataLoan.sMersMin,
                    LoanIdentifierBase.MERS_MIN,
                    GetNextSequenceNumber(loanIdentifiers.LoanIdentifierList)));

            loanIdentifiers.LoanIdentifierList.Add(
                this.CreateLoanIdentifier(
                    this.DataLoan.sAgencyCaseNum,
                    LoanIdentifierBase.AgencyCase,
                    GetNextSequenceNumber(loanIdentifiers.LoanIdentifierList)));

            loanIdentifiers.LoanIdentifierList.Add(
                this.CreateLoanIdentifier(
                    this.DataLoan.sLenderCaseNum,
                    LoanIdentifierBase.LenderCase,
                    GetNextSequenceNumber(loanIdentifiers.LoanIdentifierList)));

            loanIdentifiers.LoanIdentifierList.Add(
                this.CreateLoanIdentifier(
                    this.DataLoan.sLNm,
                    LoanIdentifierBase.LenderLoan,
                    GetNextSequenceNumber(loanIdentifiers.LoanIdentifierList)));

            loanIdentifiers.LoanIdentifierList.Add(
                this.CreateLoanIdentifier(
                    this.DataLoan.sInvestorLockLoanNum,
                    LoanIdentifierBase.InvestorLoan,
                    GetNextSequenceNumber(loanIdentifiers.LoanIdentifierList)));

            loanIdentifiers.LoanIdentifierList.Add(
                this.CreateLoanIdentifier(
                    this.DataLoan.sCoreLoanId,
                    LoanIdentifierBase.Other,
                    GetNextSequenceNumber(loanIdentifiers.LoanIdentifierList),
                    "CoreSystemAccountNumber"));

            return loanIdentifiers;
        }

        /// <summary>
        /// Creates a container holding information on a loan identifier.
        /// </summary>
        /// <param name="identifier">The identifier string.</param>
        /// <param name="identifierType">The type of identifier.</param>
        /// <param name="sequenceNumber">The sequence number of the LOAN_IDENTIFIER element.</param>
        /// <param name="identifierTypeOtherDescription">The description for when <paramref name="identifierType"/> is <see cref="LoanIdentifierBase.Other"/>.</param>
        /// <returns>A LOAN_IDENTIFIER container populated with data.</returns>
        private LOAN_IDENTIFIER CreateLoanIdentifier(string identifier, LoanIdentifierBase identifierType, int sequenceNumber, string identifierTypeOtherDescription = null)
        {
            if (string.IsNullOrEmpty(identifier))
            {
                return null;
            }

            var loanIdentifier = new LOAN_IDENTIFIER();
            loanIdentifier.SequenceNumber = sequenceNumber;
            loanIdentifier.LoanIdentifier = ToMismoIdentifier(identifier);
            loanIdentifier.LoanIdentifierType = ToMismoEnum(identifierType);
            if (loanIdentifier.LoanIdentifierType?.IsSetToOther ?? false)
            {
                loanIdentifier.LoanIdentifierTypeOtherDescription = ToMismoString(identifierTypeOtherDescription);
            }

            return loanIdentifier;
        }

        /// <summary>
        /// Creates a LOAN_LEVEL_CREDIT container with information regarding the credit score used to qualify the loan.
        /// </summary>
        /// <returns>A LOAN_LEVEL_CREDIT container with information regarding the credit score used to qualify the loan.</returns>
        private LOAN_LEVEL_CREDIT CreateLoanLevelCredit()
        {
            var credit = new LOAN_LEVEL_CREDIT();
            credit.LoanLevelCreditDetail = this.CreateLoanLevelCreditDetail();

            return credit;
        }

        /// <summary>
        /// Creates a LOAN_LEVEL_CREDIT_DETAIL container with information regarding the credit score used to qualify the loan.
        /// </summary>
        /// <returns>A LOAN_LEVEL_CREDIT_DETAIL container with information regarding the credit score used to qualify the loan.</returns>
        private LOAN_LEVEL_CREDIT_DETAIL CreateLoanLevelCreditDetail()
        {
            var detail = new LOAN_LEVEL_CREDIT_DETAIL();
            detail.LoanLevelCreditScoreValue = ToMismoValue(this.DataLoan.sCreditScoreLpeQual_rep);

            if (detail.LoanLevelCreditScoreValue == null)
            {
                return null;
            }

            detail.LoanLevelCreditScoreSelectionMethodType = ToMismoEnum(LoanLevelCreditScoreSelectionMethodBase.MiddleOrLowerThenLowest);

            return detail;
        }

        /// <summary>
        /// Creates a container holding information on a loan product.
        /// </summary>
        /// <returns>A LOAN_PRODUCT container.</returns>
        private LOAN_PRODUCT CreateLoanProduct()
        {
            var loanProduct = new LOAN_PRODUCT();
            loanProduct.LoanProductDetail = this.CreateLoanProductDetail();
            loanProduct.Locks = this.CreateLocks();

            return loanProduct;
        }

        /// <summary>
        /// Creates a container holding details on a loan product.
        /// </summary>
        /// <returns>A LOAN_PRODUCT_DETAIL populated with data.</returns>
        private LOAN_PRODUCT_DETAIL CreateLoanProductDetail()
        {
            var loanProductDetail = new LOAN_PRODUCT_DETAIL();

            loanProductDetail.ProductName = ToMismoString(this.DataLoan.sLpTemplateNm);
            loanProductDetail.ProductIdentifier = ToMismoIdentifier(this.DataLoan.sLoanProductIdentifier);
            loanProductDetail.ProductDescription = ToMismoString(this.DataLoan.sOtherLoanFeaturesDescription);

            return loanProductDetail;
        }

        /// <summary>
        /// Creates a container holding data about a rate lock.
        /// </summary>
        /// <param name="sequence">The sequence number of the lock among the list of locks.</param>
        /// <returns>A LOCK container.</returns>
        private LOCK CreateLock(int sequence)
        {
            var rateLock = new LOCK();
            rateLock.LockExpirationDatetime = ToMismoDatetime(this.DataLoan.sTRIDLoanEstimateNoteIRAvailTillD_rep_WithTime, true);
            if (!(this.DataLoan.sTRIDLoanEstimateSetLockStatusMethodT == E_sTRIDLoanEstimateSetLockStatusMethodT.SetManually || this.DataLoan.sTRIDIsLoanEstimateRateLocked == false))
            {
                rateLock.LockDurationDaysCount = ToMismoCount(this.DataLoan.sRLckdDays_rep, null);
            }

            rateLock.LockDatetime = ToMismoDatetime(this.DataLoan.sRLckdD_rep_WithTime, false);
            rateLock.SequenceNumber = sequence;

            return rateLock;
        }

        /// <summary>
        /// Creates a container holding all instances of LOCK.
        /// </summary>
        /// <returns>A LOCKS container.</returns>
        private LOCKS CreateLocks()
        {
            var locks = new LOCKS();
            locks.LockList.Add(this.CreateLock(GetNextSequenceNumber(locks.LockList)));

            return locks;
        }

        /// <summary>
        /// Creates a container holding data on the loan-to-value ratio.
        /// </summary>
        /// <returns>An LTV container.</returns>
        private LTV CreateLtv()
        {
            var ltv = new LTV();
            ltv.LTVRatioPercent = ToMismoPercent(this.DataLoan.sLtvR_rep);

            return ltv;
        }

        /// <summary>
        /// Creates a DOWN_PAYMENTS container which holds a set of down payments.
        /// </summary>
        /// <returns>A DOWN_PAYMENTS container.</returns>
        private DOWN_PAYMENTS CreateDownPayments()
        {
            var downPayments = new DOWN_PAYMENTS();
            downPayments.DownPaymentList.Add(this.CreateDownPayment(sequence: GetNextSequenceNumber(downPayments.DownPaymentList)));

            return downPayments;
        }

        /// <summary>
        /// Creates a DOWN_PAYMENT holding information on the down payment for a loan.
        /// </summary>
        /// <param name="sequence">The sequence number of the down payment among the set of down payments.</param>
        /// <returns>A DOWN_PAYMENT populated with data.</returns>
        private DOWN_PAYMENT CreateDownPayment(int sequence)
        {
            var downPayment = new DOWN_PAYMENT();
            downPayment.DownPaymentAmount = ToMismoAmount(this.DataLoan.sEquityCalc_rep);

            MISMOEnum<FundsSourceBase> fundsSourceType = ToMismoEnum(this.GetFundsSourceBaseValue(this.DataLoan.sDwnPmtSrc));

            downPayment.FundsType = ToMismoEnum(GetFundsBaseValue(this.DataLoan.sDwnPmtSrc));

            if (downPayment.FundsType?.IsSetToOther ?? false)
            {
                downPayment.FundsTypeOtherDescription = ToMismoString(this.DataLoan.sDwnPmtSrc);
            }

            if (fundsSourceType != null)
            {
                downPayment.PropertySellerFundingIndicator = ToMismoIndicator(fundsSourceType.EnumValue == FundsSourceBase.PropertySeller);
            }

            downPayment.SequenceNumber = sequence;

            return downPayment;
        }

        /// <summary>
        /// Creates a container holding elements with information on a draw.
        /// </summary>
        /// <returns>A DRAW container.</returns>
        private DRAW CreateDraw()
        {
            if (!this.DataLoan.sIsLineOfCredit)
            {
                return null;
            }

            var draw = new DRAW();
            draw.DrawRule = this.CreateDrawRule();

            return draw;
        }

        /// <summary>
        /// Creates a container holding data on a draw rule.
        /// </summary>
        /// <returns>A DRAW_RULE container populated with data.</returns>
        private DRAW_RULE CreateDrawRule()
        {
            if (!this.DataLoan.sIsLineOfCredit)
            {
                return null;
            }

            var drawRule = new DRAW_RULE();
            drawRule.LoanDrawMaximumAmount = ToMismoAmount(this.DataLoan.sCreditLineAmt_rep);
            drawRule.LoanDrawMinimumAmount = ToMismoAmount(this.DataLoan.sHelocMinimumAdvanceAmt_rep);
            drawRule.LoanDrawTermMonthsCount = ToMismoCount(this.DataLoan.sHelocDraw_rep, null);

            return drawRule;
        }

        /// <summary>
        /// Creates a container holding information on fees. This method is used at the loan level, for live data, and at the loan estimate | closing disclosure level (given that extensions are requested).
        /// This method is static to guarantee that neither it nor any sub-methods mix live data with archived data.
        /// </summary>
        /// <returns>A FEE_INFORMATION container.</returns>
        private FEE_INFORMATION CreateFeeInformation()
        {
            var builder = new FeeInformationBuilder(this.DataLoan, this.ExportData);
            return builder.CreateContainer();
        }

        /// <summary>
        /// Creates a container holding a set of foreclosures.
        /// </summary>
        /// <returns>A FORECLOSURES container.</returns>
        private FORECLOSURES CreateForeclosures()
        {
            var foreclosures = new FORECLOSURES();
            foreclosures.ForeclosureList.Add(this.CreateForeclosure(GetNextSequenceNumber(foreclosures.ForeclosureList)));

            return foreclosures;
        }

        /// <summary>
        /// Creates a container representing a foreclosure.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number of this container.</param>
        /// <returns>A FORECLOSURE container.</returns>
        private FORECLOSURE CreateForeclosure(int sequenceNumber)
        {
            var foreclosure = new FORECLOSURE();
            foreclosure.SequenceNumber = sequenceNumber;
            foreclosure.ForeclosureDetail = this.CreateForeclosureDetail();

            return foreclosure;
        }

        /// <summary>
        /// Creates a container holding details on a foreclosure.
        /// </summary>
        /// <returns>A FORECLOSURE_DETAIL container.</returns>
        private FORECLOSURE_DETAIL CreateForeclosureDetail()
        {
            var foreclosureDetail = new FORECLOSURE_DETAIL();

            foreclosureDetail.DeficiencyRightsPreservedIndicator = ToMismoIndicator(!this.DataLoan.sTRIDPropertyIsInNonRecourseLoanState);

            return foreclosureDetail;
        }

        /// <summary>
        /// Converts an LQB string into the appropriate FundsSourceBase enumerated value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted to a FundsSourceBase enumeration.</param>
        /// <returns>A FundsSourceBase enumerated value.</returns>
        private FundsSourceBase GetFundsSourceBaseValue(string lqbValue)
        {
            if (string.IsNullOrEmpty(lqbValue))
            {
                return FundsSourceBase.Blank;
    }
            else if (string.Equals(lqbValue, "FHA - Gift - Source N/A", StringComparison.OrdinalIgnoreCase))
            {
                return FundsSourceBase.Unknown;
}
            else if (string.Equals(lqbValue, "FHA - Gift - Source Relative", StringComparison.OrdinalIgnoreCase))
            {
                return FundsSourceBase.Relative;
            }
            else if (string.Equals(lqbValue, "FHA - Gift - Source Employer", StringComparison.OrdinalIgnoreCase))
            {
                return FundsSourceBase.Employer;
            }
            else if (string.Equals(lqbValue, "FHA - Gift - Source Nonprofit/Religious/Community - Seller Funded", StringComparison.OrdinalIgnoreCase))
            {
                return FundsSourceBase.PropertySeller;
            }
            else
            {
                return FundsSourceBase.Other;
            }
        }

        /// <summary>
        /// Creates a container holding information on the loan maturity.
        /// </summary>
        /// <returns>A MATURITY container.</returns>
        private MATURITY CreateMaturity()
        {
            var maturity = new MATURITY();
            maturity.MaturityRule = this.CreateMaturityRule();

            return maturity;
        }

        /// <summary>
        /// Creates a container holding the rules of the loan maturity.
        /// </summary>
        /// <returns>A MATURITY_RULE object populated with data.</returns>
        private MATURITY_RULE CreateMaturityRule()
        {
            var maturityRule = new MATURITY_RULE();
            maturityRule.LoanMaturityPeriodCount = ToMismoCount(this.DataLoan.sDue_rep, null);
            maturityRule.LoanMaturityPeriodType = ToMismoEnum(LoanMaturityPeriodBase.Month);
            maturityRule.LoanMaturityDate = ToMismoDate(this.DataLoan.sLoanMaturityD_rep);

            return maturityRule;
        }

        /// <summary>
        /// Creates a container holding data about a MERS registration.
        /// </summary>
        /// <param name="sequence">The sequence number of the MERS registration among the list of registrations.</param>
        /// <returns>A MERS_REGISTRATION container populated with data.</returns>
        private MERS_REGISTRATION CreateMersRegistration(int sequence)
        {
            var mersRegistration = new MERS_REGISTRATION();
            mersRegistration.SequenceNumber = sequence;
            mersRegistration.MERSOriginalMortgageeOfRecordIndicator = ToMismoIndicator(this.DataLoan.sMersIsOriginalMortgagee);

            return mersRegistration;
        }

        /// <summary>
        /// Creates a container to hold all instances of MERS_REGISTRATION.
        /// </summary>
        /// <returns>A MERS_REGISTRATIONS container.</returns>
        private MERS_REGISTRATIONS CreateMersRegistrations()
        {
            var mersRegistrations = new MERS_REGISTRATIONS();
            mersRegistrations.MersRegistrationList.Add(this.CreateMersRegistration(GetNextSequenceNumber(mersRegistrations.MersRegistrationList)));

            return mersRegistrations;
        }

        /// <summary>
        /// Creates a set of mortgage insurance data.
        /// </summary>
        /// <returns>An MI_DATA container.</returns>
        private MI_DATA CreateMiData()
        {
            if (this.DataLoan.sLT == E_sLT.VA)
            {
                return null;
            }

            var mortgageInsuranceData = new MI_DATA();
            mortgageInsuranceData.MiDataDetail = this.CreateMiDataDetail();
            mortgageInsuranceData.MiPremiums = this.CreateMiPremiums();

            return mortgageInsuranceData;
        }

        /// <summary>
        /// Details about mortgage insurance.
        /// </summary>
        /// <returns>An MI_DATA_DETAIL container.</returns>
        private MI_DATA_DETAIL CreateMiDataDetail()
        {
            Func<BaseClosingCostFee, bool> initialFilter = ClosingCostSetUtils.GetOrigCompDiscountPrepsSecGMIFilter(this.DataLoan.sClosingCostSet, false, this.DataLoan.sOriginatorCompensationPaymentSourceT, this.DataLoan.sDisclosureRegulationT, this.DataLoan.sGfeIsTPOTransaction);

            Func<BaseClosingCostFee, bool> setFilter = fee => initialFilter(fee) && fee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud1000MortgageInsuranceReserveFeeTypeId;

            BorrowerClosingCostFee mifee = (BorrowerClosingCostFee)this.DataLoan.sClosingCostSet.GetFees(setFilter).FirstOrDefault();
            var mortgageInsuranceDataDetail = new MI_DATA_DETAIL();

            if (mifee != null)
            {
                MISMOString paidToTypeOtherDescription = null;
                CAgentFields beneficiaryContact = null;
                if (mifee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.DataLoan.GetAgentFields(mifee.BeneficiaryAgentId);
                }

                mortgageInsuranceDataDetail.FeePaidToType = this.GetFeePaidToType(
                    beneficiaryContact,
                    mifee.IsAffiliate,
                    mifee.IsThirdParty,
                    mifee.BeneficiaryType,
                    mifee.Beneficiary,
                    mifee.BeneficiaryDescription,
                    out paidToTypeOtherDescription);

                if (paidToTypeOtherDescription != null)
                {
                    mortgageInsuranceDataDetail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
                }
            }

            mortgageInsuranceDataDetail.LenderPaidMIInterestRateAdjustmentPercent = ToMismoPercent(this.DataLoan.sMiLenderPaidAdj_rep);
            mortgageInsuranceDataDetail.MI_LTVCutoffPercent = ToMismoPercent(string.IsNullOrEmpty(this.DataLoan.sProMInsCancelLtv_rep) ? "0.000" : this.DataLoan.sProMInsCancelLtv_rep);
            mortgageInsuranceDataDetail.MICertificateIdentifier = ToMismoIdentifier(this.DataLoan.sMiCertId);
            mortgageInsuranceDataDetail.MICollectedNumberOfMonthsCount = ToMismoCount(this.DataLoan.sMInsRsrvMon == 0 ? "0" : this.DataLoan.sMInsRsrvMon_rep, null);
            mortgageInsuranceDataDetail.MICushionNumberOfMonthsCount = ToMismoCount(this.DataLoan.GetCushionMonths(E_EscrowItemT.MortgageInsurance), this.DataLoan.m_convertLos);
            mortgageInsuranceDataDetail.MICompanyNameType = ToMismoEnum(this.GetMICompanyNameBaseValue(this.DataLoan.sMiCompanyNmT));

            if (mortgageInsuranceDataDetail.MICompanyNameType?.IsSetToOther ?? false)
            {
                mortgageInsuranceDataDetail.MICompanyNameTypeOtherDescription = ToMismoString(Tools.Get_sMiCompanyNmTFriendlyDisplay(this.DataLoan.sMiCompanyNmT));
            }

            mortgageInsuranceDataDetail.MICoveragePercent = ToMismoPercent(this.DataLoan.sMiLenderPaidCoverage_rep);
            mortgageInsuranceDataDetail.MIDurationType = ToMismoEnum(this.GetMiDurationBaseValue(this.DataLoan.sProdConvMIOptionT));
            mortgageInsuranceDataDetail.MIEscrowedIndicator = ToMismoIndicator(this.DataLoan.sMInsRsrvEscrowedTri);

            string initialPremium = this.DataLoan.sFfUfmip1003_rep;
            string initialPremiumPercent = this.DataLoan.sFfUfmipR_rep;

            if (this.DataLoan.sMiInsuranceT == E_sMiInsuranceT.LenderPaid)
            {
                initialPremium = this.DataLoan.sLenderUfmip_rep;
                initialPremiumPercent = this.DataLoan.sLenderUfmipR_rep;
            }

            mortgageInsuranceDataDetail.MIInitialPremiumAmount = ToMismoAmount(initialPremium);
            mortgageInsuranceDataDetail.MIInitialPremiumRatePercent = ToMismoPercent(initialPremiumPercent);
            mortgageInsuranceDataDetail.MIPremiumFinancedAmount = ToMismoAmount(this.DataLoan.sFfUfmipFinanced_rep);
            mortgageInsuranceDataDetail.MIPremiumFinancedIndicator = ToMismoIndicator(this.DataLoan.sFfUfmipFinanced > 0);
            mortgageInsuranceDataDetail.MIPremiumFromClosingAmount = ToMismoAmount(this.DataLoan.sUfCashPd_rep);
            mortgageInsuranceDataDetail.MIPremiumPaymentType = ToMismoEnum(this.GetMIPremiumPaymentBaseValue(this.DataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes));
            mortgageInsuranceDataDetail.MIPremiumRefundableType = ToMismoEnum(this.GetMIPremiumRefundableBaseValue(this.DataLoan.sUfmipIsRefundableOnProRataBasis));
            mortgageInsuranceDataDetail.MIPremiumSourceType = ToMismoEnum(this.GetMIPremiumSourceBaseValue(this.DataLoan.sMiInsuranceT));

            if (mortgageInsuranceDataDetail.MIPremiumSourceType?.IsSetToOther ?? false)
            {
                mortgageInsuranceDataDetail.MIPremiumSourceTypeOtherDescription = ToMismoString(this.DataLoan.sMiInsuranceT);
            }

            mortgageInsuranceDataDetail.MISourceType = ToMismoEnum(this.GetMISourceBaseValue(this.DataLoan.sLT));

            return mortgageInsuranceDataDetail;
        }

        /// <summary>
        /// Determines the MI company name for the given company.
        /// </summary>
        /// <param name="company">The mortgage insurance provider.</param>
        /// <returns>The MI company name for the given company.</returns>
        private MICompanyNameBase GetMICompanyNameBaseValue(E_sMiCompanyNmT company)
        {
            switch (company)
            {
                case E_sMiCompanyNmT.Amerin:
                case E_sMiCompanyNmT.Arch:
                case E_sMiCompanyNmT.CAHLIF:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.CMG:
                case E_sMiCompanyNmT.CMGPre94:
                    return MICompanyNameBase.CMG;
                case E_sMiCompanyNmT.Commonwealth:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.Essent:
                    return MICompanyNameBase.Essent;
                case E_sMiCompanyNmT.FHA:
                    return MICompanyNameBase.Blank;
                case E_sMiCompanyNmT.Genworth:
                    return MICompanyNameBase.Genworth;
                case E_sMiCompanyNmT.MDHousing:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.MGIC:
                    return MICompanyNameBase.MGIC;
                case E_sMiCompanyNmT.MIF:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.NationalMI:
                    return MICompanyNameBase.NationalMI;
                case E_sMiCompanyNmT.PMI:
                    return MICompanyNameBase.PMI;
                case E_sMiCompanyNmT.Radian:
                    return MICompanyNameBase.Radian;
                case E_sMiCompanyNmT.RMIC:
                case E_sMiCompanyNmT.RMICNC:
                    return MICompanyNameBase.RMIC;
                case E_sMiCompanyNmT.SONYMA:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.Triad:
                    return MICompanyNameBase.Triad;
                case E_sMiCompanyNmT.UnitedGuaranty:
                    return MICompanyNameBase.UGI;
                case E_sMiCompanyNmT.USDA:
                case E_sMiCompanyNmT.VA:
                    return MICompanyNameBase.Blank;
                case E_sMiCompanyNmT.Verex:
                case E_sMiCompanyNmT.WiscMtgAssr:
                case E_sMiCompanyNmT.MassHousing:
                    return MICompanyNameBase.Other;
                case E_sMiCompanyNmT.LeaveBlank:
                    return MICompanyNameBase.Blank;
                default:
                    throw new UnhandledEnumException(company);
            }
        }

        /// <summary>
        /// Retrieves the MI duration value corresponding to the given duration.
        /// </summary>
        /// <param name="duration">The MI duration.</param>
        /// <returns>The corresponding MI duration enumeration.</returns>
        private MIDurationBase GetMiDurationBaseValue(E_sProdConvMIOptionT duration)
        {
            switch (duration)
            {
                case E_sProdConvMIOptionT.NoMI:
                case E_sProdConvMIOptionT.Blank:
                    return MIDurationBase.NotApplicable;
                case E_sProdConvMIOptionT.BorrPaidMonPrem:
                    return MIDurationBase.PeriodicMonthly;
                case E_sProdConvMIOptionT.BorrPaidSplitPrem:
                    return MIDurationBase.SplitPremium;
                case E_sProdConvMIOptionT.BorrPaidSinglePrem:
                case E_sProdConvMIOptionT.LendPaidSinglePrem:
                    return MIDurationBase.SingleLifeOfLoan;
                default:
                    throw new UnhandledEnumException(duration);
            }
        }

        /// <summary>
        /// Determines how the mortgage insurance premium payment type based on whether the premiums are escrowed.
        /// </summary>
        /// <param name="escrowed">True if the mortgage insurance premiums are escrowed. Otherwise false.</param>
        /// <returns>A serializable <see cref="MIPremiumPaymentBase" /> object.</returns>
        private MIPremiumPaymentBase GetMIPremiumPaymentBaseValue(bool escrowed)
        {
            return escrowed ? MIPremiumPaymentBase.Escrowed : MIPremiumPaymentBase.Blank;
        }

        /// <summary>
        /// Determines the MI premium refundable base value from the given indicator.
        /// </summary>
        /// <param name="refundable">True if the premium is refundable. Otherwise false.</param>
        /// <returns>The corresponding MI premium refundable base value.</returns>
        private MIPremiumRefundableBase GetMIPremiumRefundableBaseValue(bool refundable)
        {
            return refundable ? MIPremiumRefundableBase.Refundable : MIPremiumRefundableBase.NotRefundable;
        }

        /// <summary>
        /// Determines the mortgage insurance premium source from the given mortgage insurance type.
        /// </summary>
        /// <param name="mitype">The type of mortgage insurance, e.g. lender-paid or borrower-paid.</param>
        /// <returns>The corresponding MIPremiumSourceBase value.</returns>
        private MIPremiumSourceBase GetMIPremiumSourceBaseValue(E_sMiInsuranceT mitype)
        {
            switch (mitype)
            {
                case E_sMiInsuranceT.BorrowerPaid:
                    return MIPremiumSourceBase.Borrower;
                case E_sMiInsuranceT.InvestorPaid:
                    return MIPremiumSourceBase.Other;
                case E_sMiInsuranceT.LenderPaid:
                    return MIPremiumSourceBase.Lender;
                case E_sMiInsuranceT.None:
                    return MIPremiumSourceBase.Blank;
                default:
                    throw new UnhandledEnumException(mitype);
            }
        }

        /// <summary>
        /// Determines the MISourceBase value from the given loan type.
        /// </summary>
        /// <param name="loanType">The loan type, e.g. FHA | Conventional.</param>
        /// <returns>The corresponding MISourceBase value.</returns>
        private MISourceBase GetMISourceBaseValue(E_sLT loanType)
        {
            switch (loanType)
            {
                case E_sLT.Conventional:
                    return MISourceBase.PMI;
                case E_sLT.FHA:
                    return MISourceBase.FHA;
                case E_sLT.Other:
                    return MISourceBase.PMI;
                case E_sLT.UsdaRural:
                    return MISourceBase.USDA;
                case E_sLT.VA:
                    return MISourceBase.Blank;
                default:
                    throw new UnhandledEnumException(loanType);
            }
        }

        /// <summary>
        /// Creates a container to hold a set of mortgage insurance premiums.
        /// </summary>
        /// <returns>An MI_PREMIUMS container.</returns>
        private MI_PREMIUMS CreateMiPremiums()
        {
            var mortgageInsurancePremiums = new MI_PREMIUMS();

            if (this.DataLoan.sMiInsuranceT == E_sMiInsuranceT.LenderPaid)
            {
                mortgageInsurancePremiums.MiPremiumList.Add(this.CreateMiPremium_LPMI(GetNextSequenceNumber(mortgageInsurancePremiums.MiPremiumList)));
            }
            else
            {
                if (this.DataLoan.sProMInsMon > 0)
                {
                    mortgageInsurancePremiums.MiPremiumList.Add(this.CreateMiPremium(GetNextSequenceNumber(mortgageInsurancePremiums.MiPremiumList), true));
                }

                if (this.DataLoan.sProMIns2Mon > 0)
                {
                    mortgageInsurancePremiums.MiPremiumList.Add(this.CreateMiPremium(GetNextSequenceNumber(mortgageInsurancePremiums.MiPremiumList), false));
                }
            }

            return mortgageInsurancePremiums;
        }

        /// <summary>
        /// Creates a container to hold information on a mortgage insurance premium.
        /// </summary>
        /// <param name="sequence">The sequence number of the MI premium among the list of premiums.</param>
        /// <param name="monthly">True if the premium is the monthly/initial. False if it is the renewal.</param>
        /// <returns>An MI_PREMIUM container.</returns>
        private MI_PREMIUM CreateMiPremium(int sequence, bool monthly)
        {
            var mortgageInsurancePremium = new MI_PREMIUM();
            mortgageInsurancePremium.SequenceNumber = sequence;
            mortgageInsurancePremium.MiPremiumDetail = this.CreateMiPremiumDetail(monthly ? MIPremiumSequenceBase.First : MIPremiumSequenceBase.Second);

            return mortgageInsurancePremium;
        }

        /// <summary>
        /// Creates an MI_PREMIUM container with information regarding lender-paid mortgage insurance.
        /// </summary>
        /// <param name="sequence">The sequence number of the MI premium among the list of premiums.</param>
        /// <returns>An MI_PREMIUM container.</returns>
        private MI_PREMIUM CreateMiPremium_LPMI(int sequence)
        {
            var mortgageInsurancePremium = new MI_PREMIUM();
            mortgageInsurancePremium.SequenceNumber = sequence;
            mortgageInsurancePremium.MiPremiumDetail = this.CreateMiPremiumDetail_LPMI();
            mortgageInsurancePremium.MiPremiumPayments = this.CreateMiPremiumPayments_LPMI();

            return mortgageInsurancePremium;
        }

        /// <summary>
        /// Creates a container to hold details on a mortgage insurance premium.
        /// </summary>
        /// <param name="premiumSequence">The mortgage insurance premium sequence among the list of premiums.</param>
        /// <returns>An MI_PREMIUM_DETAIL container.</returns>
        private MI_PREMIUM_DETAIL CreateMiPremiumDetail(MIPremiumSequenceBase premiumSequence)
        {
            var mortgageInsurancePremiumDetail = new MI_PREMIUM_DETAIL();
            mortgageInsurancePremiumDetail.MIPremiumCalculationType = ToMismoEnum(this.GetMIPremiumCalculationBaseValue(this.DataLoan.sProMInsT));

            if (mortgageInsurancePremiumDetail.MIPremiumCalculationType?.IsSetToOther ?? false)
            {
                mortgageInsurancePremiumDetail.MIPremiumCalculationTypeOtherDescription = ToMismoString(this.DataLoan.sProMInsT);
            }

            mortgageInsurancePremiumDetail.MIPremiumMonthlyPaymentAmount = ToMismoAmount(
                premiumSequence == MIPremiumSequenceBase.First ? this.DataLoan.sProMIns_rep : this.DataLoan.sProMIns2_rep);

            mortgageInsurancePremiumDetail.MIPremiumPeriodType = ToMismoEnum(MIPremiumPeriodBase.Renewal);
            mortgageInsurancePremiumDetail.MIPremiumRateDurationMonthsCount = ToMismoCount(
                premiumSequence == MIPremiumSequenceBase.First ? this.DataLoan.sProMInsMon_rep : this.DataLoan.sProMIns2Mon_rep,
                null);

            mortgageInsurancePremiumDetail.MIPremiumRatePercent = ToMismoPercent(
                premiumSequence == MIPremiumSequenceBase.First ? this.DataLoan.sProMInsR_rep : this.DataLoan.sProMInsR2_rep);

            mortgageInsurancePremiumDetail.MIPremiumSequenceType = ToMismoEnum(premiumSequence);

            return mortgageInsurancePremiumDetail;
        }

        /// <summary>
        /// Retrieves the MI premium calculation type corresponding to the given percent base.
        /// </summary>
        /// <param name="type">The percent base type.</param>
        /// <returns>The corresponding <see cref="MIPremiumCalculationBase" /> value.</returns>
        private MIPremiumCalculationBase GetMIPremiumCalculationBaseValue(E_PercentBaseT type)
        {
            switch (type)
            {
                case E_PercentBaseT.AppraisalValue:
                case E_PercentBaseT.LoanAmount:
                case E_PercentBaseT.SalesPrice:
                case E_PercentBaseT.TotalLoanAmount:
                    return MIPremiumCalculationBase.Constant;
                case E_PercentBaseT.AverageOutstandingBalance:
                case E_PercentBaseT.DecliningRenewalsAnnually:
                case E_PercentBaseT.DecliningRenewalsMonthly:
                    return MIPremiumCalculationBase.Declining;
                case E_PercentBaseT.AllYSP:
                case E_PercentBaseT.OriginalCost:
                    return MIPremiumCalculationBase.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Creates an MI_PREMIUM_DETAIL container with information regarding the lender-paid mortgage insurance premium.
        /// </summary>
        /// <returns>An MI_PREMIUM_DETAIL container.</returns>
        private MI_PREMIUM_DETAIL CreateMiPremiumDetail_LPMI()
        {
            var mortgageInsurancePremiumDetail = new MI_PREMIUM_DETAIL();

            mortgageInsurancePremiumDetail.MIPremiumCalculationType = ToMismoEnum(MIPremiumCalculationBase.BaseLoanAmount);
            mortgageInsurancePremiumDetail.MIPremiumPeriodType = ToMismoEnum(MIPremiumPeriodBase.Upfront);
            mortgageInsurancePremiumDetail.MIPremiumRatePercent = ToMismoPercent(this.DataLoan.sLenderUfmipR_rep);
            mortgageInsurancePremiumDetail.MIPremiumSequenceType = ToMismoEnum(MIPremiumSequenceBase.First);

            return mortgageInsurancePremiumDetail;
        }

        /// <summary>
        /// Creates an MI_PREMIUM_PAYMENT container with information regarding the lender-paid mortgage insurance premium payment.
        /// </summary>
        /// <param name="sequence">The sequence of the payment among the list of payments.</param>
        /// <returns>An MI_PREMIUM_PAYMENT container.</returns>
        private MI_PREMIUM_PAYMENT CreateMiPremiumPayment_LPMI(int sequence)
        {
            var payment = new MI_PREMIUM_PAYMENT();
            payment.MIPremiumActualPaymentAmount = ToMismoAmount(this.DataLoan.sLenderUfmip_rep);
            payment.MIPremiumPaymentPaidByType = ToMismoEnum(this.GetMIPremiumPaymentPaidByBaseValue(E_sMiInsuranceT.LenderPaid));
            payment.PaymentFinancedIndicator = ToMismoIndicator(false);
            payment.SequenceNumber = sequence;

            return payment;
        }

        /// <summary>
        /// Retrieves the entity who paid the MI premium.
        /// </summary>
        /// <param name="paidBy">The entity who paid the MI premium.</param>
        /// <returns>The value representing the entity who paid the MI premium.</returns>
        private MIPremiumPaymentPaidByBase GetMIPremiumPaymentPaidByBaseValue(E_sMiInsuranceT paidBy)
        {
            switch (paidBy)
            {
                case E_sMiInsuranceT.BorrowerPaid:
                    return MIPremiumPaymentPaidByBase.Buyer;
                case E_sMiInsuranceT.LenderPaid:
                    return MIPremiumPaymentPaidByBase.Lender;
                case E_sMiInsuranceT.InvestorPaid:
                case E_sMiInsuranceT.None:
                    return MIPremiumPaymentPaidByBase.Other;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Creates an MI_PREMIUM_PAYMENTS container with information regarding the lender-paid mortgage insurance premium payment.
        /// </summary>
        /// <returns>An MI_PREMIUM_PAYMENTS container.</returns>
        private MI_PREMIUM_PAYMENTS CreateMiPremiumPayments_LPMI()
        {
            var payments = new MI_PREMIUM_PAYMENTS();
            payments.MiPremiumPaymentList.Add(this.CreateMiPremiumPayment_LPMI(1));

            return payments;
        }

        /// <summary>
        /// Creates a container holding information about negative amortization.
        /// </summary>
        /// <returns>A NEGATIVE_AMORTIZATION object.</returns>
        private NEGATIVE_AMORTIZATION CreateNegativeAmortization()
        {
            var negativeAmortization = new NEGATIVE_AMORTIZATION();
            negativeAmortization.NegativeAmortizationRule = this.CreateNegativeAmortizationRule();

            return negativeAmortization;
        }

        /// <summary>
        /// Creates a container holding data on the negative amortization rules.
        /// </summary>
        /// <returns>A NEGATIVE_AMORTIZATION_RULE object populated with data.</returns>
        private NEGATIVE_AMORTIZATION_RULE CreateNegativeAmortizationRule()
        {
            var negativeAmortizationRule = new NEGATIVE_AMORTIZATION_RULE();
            negativeAmortizationRule.NegativeAmortizationLimitPercent = ToMismoPercent(this.DataLoan.sPmtAdjMaxBalPc_rep);
            negativeAmortizationRule.NegativeAmortizationLimitMonthsCount = ToMismoCount(this.DataLoan.sPmtAdjRecastPeriodMon_rep);

            return negativeAmortizationRule;
        }

        /// <summary>
        /// Creates a PAID_TO container with information regarding the company of the given agent, the recipient of a fee/payable. 
        /// </summary>
        /// <param name="paidToContact">The contact record associated with the entity to which the fee is paid.</param>
        /// <param name="companyName">The name of the company or legal entity to which the fee is paid.</param>
        /// <param name="beneficiaryAgentId">
        /// The ID of the <see cref="CAgentFields"/> 
        /// entry in the loan's contact list which is the beneficiary of the payment.
        /// </param>
        /// <returns>A PAID_TO container with information regarding the company of the given agent, the recipient of a fee/payable.</returns>
        private PAID_TO CreatePaidTo(CAgentFields paidToContact, string companyName, Guid beneficiaryAgentId)
        {
            bool validContact = paidToContact != null && paidToContact.IsValid && !paidToContact.IsNewRecord;

            if (!validContact && string.IsNullOrEmpty(companyName))
            {
                return null;
            }

            var paidTo = new PAID_TO();

            if (validContact)
            {
                var addresses = CreateAddresses(paidToContact);

                if (addresses.AddressList != null && addresses.AddressList.Count(a => a != null) > 0)
                {
                    paidTo.Address = addresses.AddressList.Where(a => a != null).FirstOrDefault();
                }

                paidTo.LegalEntity = CreateLegalEntity(paidToContact, string.IsNullOrEmpty(companyName) ? paidToContact.CompanyName : companyName, ContactPointRoleBase.Work);
                paidTo.Extension = this.CreatePaidToExtension(beneficiaryAgentId);
            }
            else
            {
                paidTo.LegalEntity = CreateLegalEntity(companyName);
            }

            return paidTo;
        }

        /// <summary>
        /// Creates an extension to the PAID_TO element.
        /// </summary>
        /// <param name="beneficiaryAgentId">The agent receiving the payment.</param>
        /// <returns>An extension element.</returns>
        private PAID_TO_EXTENSION CreatePaidToExtension(Guid beneficiaryAgentId)
        {
            var extension = new PAID_TO_EXTENSION();
            extension.Other = this.CreateLqbPaidToExtension(beneficiaryAgentId);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension to the PAID_TO element.
        /// </summary>
        /// <param name="beneficiaryAgentId">The agent receiving the payment.</param>
        /// <returns>A proprietary extension element.</returns>
        private LQB_PAID_TO_EXTENSION CreateLqbPaidToExtension(Guid beneficiaryAgentId)
        {
            var extension = new LQB_PAID_TO_EXTENSION();
            extension.BeneficiaryAgentId = ToMismoIdentifier(beneficiaryAgentId.ToString());

            return extension;
        }

        /// <summary>
        /// Creates a container holding a set of prepaid items.
        /// </summary>
        /// <returns>A PREPAID_ITEMS container.</returns>
        private PREPAID_ITEMS CreatePrepaidItems()
        {
            var prepaidItems = new PREPAID_ITEMS();
            var prepaidFees = GetPrepaidFees(this.DataLoan);

            if (!prepaidFees.Any())
            {
                return null;
            }

            BorrowerClosingCostSet estimatedFeeSet = null;
            if (this.DataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017
                && this.DataLoan.sAppliedCCArchiveLinkedArchiveId != Guid.Empty)
            {
                estimatedFeeSet = this.DataLoan.GetClosingCostArchiveById(this.DataLoan.sAppliedCCArchiveLinkedArchiveId)?.GetClosingCostSet(this.DataLoan.m_convertLos);
            }
            else
            {
                estimatedFeeSet = this.DataLoan.LastDisclosedLoanEstimateArchive?.GetClosingCostSet(this.DataLoan.m_convertLos);
            }

            foreach (BorrowerClosingCostFee fee in prepaidFees)
            {
                var estimatedFee = (BorrowerClosingCostFee)estimatedFeeSet?.FindFeeByTypeId(fee.ClosingCostFeeTypeId);
                prepaidItems.PrepaidItemList.Add(this.CreatePrepaidItem(fee, estimatedFee, GetNextSequenceNumber(prepaidItems.PrepaidItemList)));
            }

            return prepaidItems;
        }

        /// <summary>
        /// Creates a PREPAID_ITEM container with information regarding the given prepaid fee.
        /// </summary>
        /// <param name="prepaidFee">The prepaid fee.</param>
        /// <param name="estimatedFee">The last disclosed estimate of the prepaid fee.</param>
        /// <param name="sequence">The sequence number of the prepaid item among the list of items.</param>
        /// <returns>A PREPAID_ITEM container with information regarding the given prepaid fee.</returns>
        private PREPAID_ITEM CreatePrepaidItem(BorrowerClosingCostFee prepaidFee, BorrowerClosingCostFee estimatedFee, int sequence)
        {
            CAgentFields beneficiaryContact = null;
            if (prepaidFee.BeneficiaryAgentId != Guid.Empty)
            {
                beneficiaryContact = this.DataLoan.GetAgentFields(prepaidFee.BeneficiaryAgentId);
            }

            var prepaid = new PREPAID_ITEM();

            prepaid.PrepaidItemDetail = this.CreatePrepaidItemDetail(prepaidFee, estimatedFee, beneficiaryContact);
            prepaid.PrepaidItemPaidTo = this.CreatePaidTo(beneficiaryContact, prepaidFee.BeneficiaryDescription, prepaidFee.BeneficiaryAgentId);
            prepaid.PrepaidItemPayments = this.CreatePrepaidItemPayments(prepaidFee.Payments, prepaidFee.IsApr, prepaidFee.QmAmount > 0.00m);

            if (beneficiaryContact != null)
            {
                prepaid.SelectedServiceProvider = this.CreateSelectedServiceProvider(beneficiaryContact);
            }

            prepaid.SequenceNumber = sequence;

            return prepaid;
        }

        /// <summary>
        /// Creates a container representing a purchase credit.
        /// </summary>
        /// <param name="amount">The amount of the purchase credit.</param>
        /// <param name="description">A description of the purchase credit.</param>
        /// <param name="sequence">The sequence number of the purchase credit among the list of credits.</param>
        /// <returns>A PURCHASE_CREDIT container.</returns>
        private PURCHASE_CREDIT CreatePurchaseCredit(string amount, string description, int sequence)
        {
            if (IsAmountStringBlankOrZero(amount) ||
                string.IsNullOrEmpty(description))
            {
                return null;
            }

            var purchaseCredit = new PURCHASE_CREDIT();

            purchaseCredit.PurchaseCreditAmount = ToMismoAmount(amount);
            purchaseCredit.PurchaseCreditSourceType = ToMismoEnum(this.GetPurchaseCreditSourceBaseValue(description));
            purchaseCredit.PurchaseCreditType = ToMismoEnum(this.GetPurchaseCreditBaseValue(description));
            purchaseCredit.SequenceNumber = sequence;

            if (purchaseCredit.PurchaseCreditType?.IsSetToOther ?? false)
            {
                purchaseCredit.PurchaseCreditTypeOtherDescription = ToMismoString(description);
            }

            return purchaseCredit;
        }

        /// <summary>
        /// Retrieves the purchase credit source value corresponding to the given description.
        /// </summary>
        /// <param name="description">A description of the purchase credit.</param>
        /// <returns>The corresponding purchase credit source value.</returns>
        private PurchaseCreditSourceBase GetPurchaseCreditSourceBaseValue(string description)
        {
            if (string.Equals("lender credit", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditSourceBase.Lender;
            }
            else if (string.Equals("seller credit", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditSourceBase.PropertySeller;
            }
            else if (string.Equals("earnest deposit", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditSourceBase.BorrowerPaidOutsideClosing;
            }
            else
            {
                return PurchaseCreditSourceBase.Other;
            }
        }

        /// <summary>
        /// Retrieves the purchase credit value corresponding to the given description.
        /// </summary>
        /// <param name="description">A description of the purchase credit.</param>
        /// <returns>The corresponding value.</returns>
        private PurchaseCreditBase GetPurchaseCreditBaseValue(string description)
        {
            if (string.Equals("cash deposit on sales contract", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.EarnestMoney;
            }
            else if (string.Equals("employer assisted housing", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.EmployerAssistedHousing;
            }
            else if (string.Equals("lease purchase fund", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.LeasePurchaseFund;
            }
            else if (string.Equals("relocation funds", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.RelocationFunds;
            }
            else if (string.Equals("mi premium refund", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.MIPremiumRefund;
            }
            else if (string.Equals("buydown fund", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.BuydownFund;
            }
            else if (string.Equals("commitment origination fee", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.CommitmentOriginationFee;
            }
            else if (string.Equals("federal agency funding fee refund", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.FederalAgencyFundingFeeRefund;
            }
            else if (string.Equals("gift of equity", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.GiftOfEquity;
            }
            else if (string.Equals("sweat equity", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.SweatEquity;
            }
            else if (string.Equals("trade equity", description, StringComparison.OrdinalIgnoreCase))
            {
                return PurchaseCreditBase.TradeEquity;
            }
            else
            {
                return PurchaseCreditBase.Other;
            }
        }

        /// <summary>
        /// Creates a container to hold all purchase credits.
        /// </summary>
        /// <returns>A PURCHASE_CREDITS container.</returns>
        private PURCHASE_CREDITS CreatePurchaseCredits()
        {
            var purchaseCredits = new PURCHASE_CREDITS();
            purchaseCredits.PurchaseCreditList.Add(this.CreatePurchaseCredit(this.DataLoan.sOCredit1Amt_rep, this.DataLoan.sOCredit1Desc, GetNextSequenceNumber(purchaseCredits.PurchaseCreditList)));
            purchaseCredits.PurchaseCreditList.Add(this.CreatePurchaseCredit(this.DataLoan.sOCredit2Amt_rep, this.DataLoan.sOCredit2Desc, GetNextSequenceNumber(purchaseCredits.PurchaseCreditList)));
            purchaseCredits.PurchaseCreditList.Add(this.CreatePurchaseCredit(this.DataLoan.sOCredit3Amt_rep, this.DataLoan.sOCredit3Desc, GetNextSequenceNumber(purchaseCredits.PurchaseCreditList)));
            purchaseCredits.PurchaseCreditList.Add(this.CreatePurchaseCredit(this.DataLoan.sOCredit4Amt_rep, this.DataLoan.sOCredit4Desc, GetNextSequenceNumber(purchaseCredits.PurchaseCreditList)));
            if (!this.ExportData.IsClosingPackage && this.DataLoan.sDisclosureRegulationT == E_sDisclosureRegulationT.TRID)
            {
                var creditAmount = this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic ? this.DataLoan.sTRIDLoanEstimateGeneralLenderCredits_rep : this.DataLoan.sTRIDLoanEstimateLenderCredits_rep;
                purchaseCredits.PurchaseCreditList.Add(this.CreatePurchaseCredit(creditAmount, this.DataLoan.sOCredit5Desc, GetNextSequenceNumber(purchaseCredits.PurchaseCreditList)));
            }
            else
            {
                var creditAmount = this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic ? this.DataLoan.sTRIDClosingDisclosureGeneralLenderCredits_rep : this.DataLoan.sOCredit5Amt_rep;
                purchaseCredits.PurchaseCreditList.Add(this.CreatePurchaseCredit(creditAmount, this.DataLoan.sOCredit5Desc, GetNextSequenceNumber(purchaseCredits.PurchaseCreditList)));
            }

            return purchaseCredits;
        }

        /// <summary>
        /// Creates a SELECTED_SERVICE_PROVIDER container with information regarding the company of the given agent, a service provider required by the lender. 
        /// </summary>
        /// <param name="agent">The contact record associated with the service provider.</param>
        /// <returns>A SELECTED_SERVICE_PROVIDER container with information regarding the company of the given agent, a service provider required by the lender.</returns>
        private SELECTED_SERVICE_PROVIDER CreateSelectedServiceProvider(CAgentFields agent)
        {
            if (!agent.IsValid || agent.IsNewRecord)
            {
                return null;
            }

            var provider = new SELECTED_SERVICE_PROVIDER();
            var addresses = CreateAddresses(agent);

            if (addresses.AddressList != null && addresses.AddressList.Count(a => a != null) > 0)
            {
                provider.Address = addresses.AddressList.FirstOrDefault(a => a != null);
            }

            provider.LegalEntity = CreateLegalEntity(agent, agent.CompanyName, ContactPointRoleBase.Work);
            provider.SelectedServiceProviderDetail = this.CreateSelectedServiceProviderDetail(agent);

            return provider;
        }

        /// <summary>
        /// Creates a SELECTED_SERVICE_PROVIDER_DETAIL container with information regarding the service provider's relationship with the lender.
        /// </summary>
        /// <param name="agent">The agent record of the service provider.</param>
        /// <returns>A SELECTED_SERVICE_PROVIDER_DETAIL container with information regarding the service provider's relationship with the lender.</returns>
        private SELECTED_SERVICE_PROVIDER_DETAIL CreateSelectedServiceProviderDetail(CAgentFields agent)
        {
            var providerDetail = new SELECTED_SERVICE_PROVIDER_DETAIL();
            string relationship = string.Empty;

            if (agent.IsLender)
            {
                relationship = "This company is the lender";
            }
            else if (agent.IsLenderAssociation)
            {
                relationship = "Associate of the lender";
            }
            else if (agent.IsLenderRelative)
            {
                relationship = "Relative of the lender";
            }
            else if (agent.HasLenderRelationship)
            {
                relationship = "Has an employment, franchise or other business relationship with the lender";
            }
            else if (agent.HasLenderAccountLast12Months)
            {
                relationship = "Within the last 12 months, the provider has maintained an account with the lender or had an outstanding loan or credit arrangement with the lender";
            }
            else if (agent.IsUsedRepeatlyByLenderLast12Months)
            {
                relationship = "Within the last 12 months, the lender has repeatedly used or required borrowers to use the services of this provider";
            }
            else if (agent.IsLenderAffiliate)
            {
                relationship = "Affiliate of the lender";
            }

            if (string.IsNullOrEmpty(relationship))
            {
                return null;
            }

            providerDetail.SelectedServiceProviderNatureOfRelationshipDescription = ToMismoString(relationship);
            return providerDetail;
        }

        /// <summary>
        /// Creates a PREPAID_ITEM_DETAIL with information regarding the given prepaid fee.
        /// </summary>
        /// <param name="prepaidFee">The prepaid fee.</param>
        /// <param name="estimatedPrepaidFee">The last disclosed estimate of the prepaid fee.</param>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <returns>A PREPAID_ITEM_DETAIL container with information regarding the given prepaid fee.</returns>
        private PREPAID_ITEM_DETAIL CreatePrepaidItemDetail(BorrowerClosingCostFee prepaidFee, BorrowerClosingCostFee estimatedPrepaidFee, CAgentFields beneficiaryContact)
        {
            var detail = new PREPAID_ITEM_DETAIL();

            detail.BorrowerChosenProviderIndicator = ToMismoIndicator(prepaidFee.DidShop);

            MISMOString paidToTypeOtherDescription = null;
            detail.FeePaidToType = this.GetFeePaidToType(
                beneficiaryContact,
                prepaidFee.IsAffiliate,
                prepaidFee.IsThirdParty,
                prepaidFee.BeneficiaryType,
                prepaidFee.Beneficiary,
                prepaidFee.BeneficiaryDescription,
                out paidToTypeOtherDescription);

            if (paidToTypeOtherDescription != null)
            {
                detail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
            }

            var documentType = this.ExportData.IsClosingPackage ? IntegratedDisclosureDocumentBase.ClosingDisclosure : IntegratedDisclosureDocumentBase.LoanEstimate;
            detail.IntegratedDisclosureSectionType = ToMismoEnum(this.GetIntegratedDisclosureSectionBaseValue(documentType, prepaidFee.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage), prepaidFee.CanShop));

            if (detail.IntegratedDisclosureSectionType?.IsSetToOther ?? false)
            {
                detail.IntegratedDisclosureSectionTypeOtherDescription = ToMismoString(prepaidFee.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage));
            }

            detail.PrepaidItemActualTotalAmount = ToMismoAmount(prepaidFee.TotalAmount_rep);

            if (estimatedPrepaidFee != null)
            {
                detail.PrepaidItemEstimatedTotalAmount = ToMismoAmount(estimatedPrepaidFee.TotalAmount_rep);
            }

            E_TaxTableTaxT taxType = E_TaxTableTaxT.LeaveBlank;

            if (prepaidFee.IsPrepaidFromExpensesFee)
            {
                var housingExpense = this.DataLoan.sHousingExpenses.GetExpenseFrom900Or1000FeeTypeId(prepaidFee.ClosingCostFeeTypeId);

                if (housingExpense != null)
                {
                    detail.PrepaidItemMonthlyAmount = ToMismoAmount(housingExpense.MonthlyAmtTotal_rep);
                    detail.PrepaidItemMonthsPaidCount = ToMismoCount(housingExpense.PrepaidMonths_rep, null);
                }

                taxType = housingExpense.TaxType;
            }
            else if ((prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId
                        || prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId
                        || prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
                    && this.DataLoan.sMipPiaMon > 0)
            {
                detail.PrepaidItemMonthsPaidCount = ToMismoCount(this.DataLoan.sMipPiaMon_rep, null);
            }
            else if (prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
            {
                if (prepaidFee.NumberOfPeriods < 0 && this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic)
                {
                    // Since MISMOCount requires a positive value, for a negative interim interest term we take the absolute value.
                    detail.PrepaidItemNumberOfDaysCount = ToMismoCount(Math.Abs(prepaidFee.NumberOfPeriods), this.DataLoan.m_convertLos);
                    detail.FeePaidToType = ToMismoEnum(FeePaidToBase.Other);
                    detail.FeePaidToTypeOtherDescription = ToMismoString("Borrower");
                }
                else
                {
                    detail.PrepaidItemNumberOfDaysCount = ToMismoCount(prepaidFee.NumberOfPeriods_rep, null);
                }

                detail.PrepaidItemPaidFromDate = ToMismoDate(this.DataLoan.sConsummationD_rep);

                if (this.DataLoan.sSchedDueD1.IsValid)
                {
                    detail.PrepaidItemPaidThroughDate = ToMismoDate(this.DataLoan.m_convertLos.ToDateTimeString(this.DataLoan.sSchedDueD1.DateTimeForComputation.AddMonths(-1)));
                }

                detail.PrepaidItemPerDiemAmount = ToMismoAmount(prepaidFee.BaseAmount_rep);
                detail.PrepaidItemPerDiemCalculationMethodType = ToMismoEnum(this.GetPrepaidItemPerDiemCalculationMethodBaseValue(this.DataLoan.sDaysInYr));

                if (detail.PrepaidItemPerDiemCalculationMethodType?.IsSetToOther ?? false)
                {
                    detail.PrepaidItemPerDiemCalculationMethodTypeOtherDescription = ToMismoString(this.DataLoan.sDaysInYr_rep);
                }
            }

            detail.PrepaidItemType = ToMismoEnum(this.GetPrepaidItemBaseValue(prepaidFee.ClosingCostFeeTypeId, taxType), displayLabelText: prepaidFee.Description);

            if (detail.PrepaidItemType?.IsSetToOther ?? false)
            {
                detail.PrepaidItemTypeOtherDescription = ToMismoString(prepaidFee.Description);
            }

            detail.RegulationZPointsAndFeesIndicator = ToMismoIndicator(prepaidFee.IsIncludedInQm);
            detail.RequiredProviderOfServiceIndicator = ToMismoIndicator(!prepaidFee.CanShop);

            detail.Extension = this.CreatePrepaidItemDetailExtension(prepaidFee);

            return detail;
        }

        /// <summary>
        /// Creates an extension container with extra data about this prepaid item.
        /// </summary>
        /// <param name="prepaidFee">The fee representing the prepaid item.</param>
        /// <returns>A <see cref="PREPAID_ITEM_DETAIL_EXTENSION"/> container.</returns>
        private PREPAID_ITEM_DETAIL_EXTENSION CreatePrepaidItemDetailExtension(BorrowerClosingCostFee prepaidFee)
        {
            var extension = new PREPAID_ITEM_DETAIL_EXTENSION();

            extension.Other = this.CreateLQBPrepaidItemDetailExtension(prepaidFee);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LQB extension container with extra non-MISMO data.
        /// </summary>
        /// <param name="prepaidFee">The fee representing the prepaid item.</param>
        /// <returns>A <see cref="LQB_PREPAID_ITEM_DETAIL_EXTENSION"/> container.</returns>
        private LQB_PREPAID_ITEM_DETAIL_EXTENSION CreateLQBPrepaidItemDetailExtension(BorrowerClosingCostFee prepaidFee)
        {
            var extension = new LQB_PREPAID_ITEM_DETAIL_EXTENSION();
            extension.PrepaidItemSpecifiedHud1LineNumberValue = ToMismoValue(prepaidFee.HudLine_rep);
            extension.ClosingCostFeeTypeId = ToMismoString(prepaidFee.ClosingCostFeeTypeId.ToString());

            if (prepaidFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
            {
                extension.PrepaidItemPerDiemUnroundedAmount = ToMismoNumeric(this.DataLoan.sIPerDay_rep);
            }

            return extension;
        }

        /// <summary>
        /// Creates a FeePaidToEnum object with the paid-to type based on the provided agent information.
        /// </summary>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <param name="affiliate">True if the entity imposing the fee is an affiliate of either the lender or the originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the entity imposing the fee is a third party. Otherwise false.</param>
        /// <param name="beneficiaryType">A string representation of the agent type of the fee beneficiary.</param>
        /// <param name="beneficiary">The agent type of the fee beneficiary.</param>
        /// <param name="beneficiaryDescription">A description of the fee beneficiary.</param>
        /// <param name="otherDescription">The target <see cref="MISMOString"/> to contain the paid-to type other description, if the paid-to type resolves to other. Otherwise it will be null.</param>
        /// <returns>A FeePaidToEnum object with the paid-to type based on the provided agent information.</returns>
        private MISMOEnum<FeePaidToBase> GetFeePaidToType(CAgentFields beneficiaryContact, bool affiliate, bool thirdParty, string beneficiaryType, E_AgentRoleT beneficiary, string beneficiaryDescription, out MISMOString otherDescription)
        {
            MISMOEnum<FeePaidToBase> feePaidToType = null;
            otherDescription = null;

            if (beneficiaryContact != null)
            {
                feePaidToType = ToMismoEnum(GetFeePaidToBaseValue(
                    beneficiaryContact.AgentRoleT,
                    beneficiaryContact.IsLender,
                    beneficiaryContact.IsOriginator,
                    affiliate,
                    beneficiaryContact.IsOriginatorAffiliate,
                    beneficiaryContact.IsLenderAffiliate,
                    thirdParty));

                if (feePaidToType?.IsSetToOther ?? false)
                {
                    otherDescription = ToMismoString(beneficiaryType);
                }
            }

            //// If the fee PaidTo is set w/o having an associated agent, such as set from the fee setup, then map it.
            if (feePaidToType == null)
            {
                feePaidToType = ToMismoEnum(GetFeePaidToBaseValue(
                    beneficiary,
                    lender: false,
                    originator: false,
                    affiliate: affiliate,
                    isOriginatorAffiliate: false,
                    isLenderAffiliate: false,
                    thirdParty: thirdParty));

                if (feePaidToType?.IsSetToOther ?? false)
                {
                    string description = string.IsNullOrEmpty(beneficiaryDescription) ? GetXmlEnumName(beneficiary) : beneficiaryDescription;

                    otherDescription = ToMismoString(description);
                }
            }

            return feePaidToType;
        }

        /// <summary>
        /// Gets the integrated disclosure section type based on the type of disclosure (loan estimate | closing disclosure) and the given closing cost details section of the disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure.</param>
        /// <param name="sectionType">The closing cost details section.</param>
        /// <param name="canShop">The "Can Shop" value for the fee, for determining "B or C" value. </param>
        /// <returns>The <see cref="IntegratedDisclosureSectionBase" /> value corresponding to the given closing cost details section of the given disclosure.</returns>
        private IntegratedDisclosureSectionBase GetIntegratedDisclosureSectionBaseValue(IntegratedDisclosureDocumentBase documentType, E_IntegratedDisclosureSectionT sectionType, bool canShop = false)
        {
            if (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure)
            {
                return IntegratedDisclosureSectionBase.Blank;
            }

            bool estimate = documentType == IntegratedDisclosureDocumentBase.LoanEstimate;

            switch (sectionType)
            {
                case E_IntegratedDisclosureSectionT.SectionA:
                    return IntegratedDisclosureSectionBase.OriginationCharges;
                case E_IntegratedDisclosureSectionT.SectionB:
                    return estimate ? IntegratedDisclosureSectionBase.ServicesYouCannotShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidNotShopFor;
                case E_IntegratedDisclosureSectionT.SectionBorC:
                    // sg 10/08/2015 - Case 224832 - If canShop is checked, return Section C logic, if not checked, return Section B logic.
                    if (canShop)
                    {
                        return estimate ? IntegratedDisclosureSectionBase.ServicesYouCanShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidShopFor;
                    }
                    else
                    {
                        return estimate ? IntegratedDisclosureSectionBase.ServicesYouCannotShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidNotShopFor;
                    }

                case E_IntegratedDisclosureSectionT.SectionC:
                    return estimate ? IntegratedDisclosureSectionBase.ServicesYouCanShopFor : IntegratedDisclosureSectionBase.ServicesBorrowerDidShopFor;
                case E_IntegratedDisclosureSectionT.SectionD:
                    return IntegratedDisclosureSectionBase.TotalLoanCosts;
                case E_IntegratedDisclosureSectionT.SectionE:
                    return IntegratedDisclosureSectionBase.TaxesAndOtherGovernmentFees;
                case E_IntegratedDisclosureSectionT.SectionF:
                    return IntegratedDisclosureSectionBase.Prepaids;
                case E_IntegratedDisclosureSectionT.SectionG:
                    return IntegratedDisclosureSectionBase.InitialEscrowPaymentAtClosing;
                case E_IntegratedDisclosureSectionT.SectionH:
                    return IntegratedDisclosureSectionBase.OtherCosts;
                case E_IntegratedDisclosureSectionT.LeaveBlank:
                    return IntegratedDisclosureSectionBase.Blank;
                default:
                    throw new UnhandledEnumException(sectionType);
            }
        }

        /// <summary>
        /// Retrieves the per diem calculation method object corresponding to the given days in the calculation year.
        /// </summary>
        /// <param name="daysInYear">The days in the calculation year: should be 360 or 365.</param>
        /// <returns>The corresponding value.</returns>
        private PrepaidItemPerDiemCalculationMethodBase GetPrepaidItemPerDiemCalculationMethodBaseValue(int daysInYear)
        {
            if (daysInYear == 360)
            {
                return PrepaidItemPerDiemCalculationMethodBase.Item360;
            }
            else if (daysInYear == 365)
            {
                return PrepaidItemPerDiemCalculationMethodBase.Item365;
            }

            return PrepaidItemPerDiemCalculationMethodBase.Other;
        }

        /// <summary>
        /// Retrieves the MISMO prepaid item type corresponding to the closing cost type.
        /// </summary>
        /// <param name="defaultSystemClosingCostFeeType">The <see cref="DefaultSystemClosingCostFee" /> type of the prepaid fee.</param>
        /// <param name="taxType">The tax type associated with the prepaid. Pass the "leave blank" type if the prepaid is not a tax.</param>
        /// <returns>The corresponding <see cref="PrepaidItemBase" /> type.</returns>
        private PrepaidItemBase GetPrepaidItemBaseValue(Guid defaultSystemClosingCostFeeType, E_TaxTableTaxT taxType)
        {
            if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900CondoInsuranceFeeTypeId)
            {
                return PrepaidItemBase.HomeownersInsurancePremium;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900DailyInterestFeeTypeId)
            {
                return PrepaidItemBase.PrepaidInterest;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900FloodInsuranceFeeTypeId)
            {
                return PrepaidItemBase.FloodInsurancePremium;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900GroundRentFeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900Custom1FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900Custom2FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherExp1FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherExp2FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherExp3FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherExp4FeeTypeId)
            {
                return PrepaidItemBase.Other;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900HazardInsuranceFeeTypeId)
            {
                return PrepaidItemBase.HazardInsurancePremium;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900HOADuesFeeTypeId)
            {
                return PrepaidItemBase.HomeownersAssociationDues;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumRecurringFeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900VAFundingFeeTypeId)
            {
                return PrepaidItemBase.MortgageInsurancePremium;
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900PropertyTaxFeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherTax1FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherTax2FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherTax3FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900OtherTax4FeeTypeId
                || defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900SchoolTaxFeeTypeId)
            {
                switch (taxType)
                {
                    case E_TaxTableTaxT.Borough:
                        return PrepaidItemBase.BoroughPropertyTax;
                    case E_TaxTableTaxT.City:
                        return PrepaidItemBase.CityPropertyTax;
                    case E_TaxTableTaxT.County:
                        return PrepaidItemBase.CountyPropertyTax;
                    case E_TaxTableTaxT.FireDist:
                        return PrepaidItemBase.DistrictPropertyTax;
                    case E_TaxTableTaxT.LocalImprovementDist:
                        return PrepaidItemBase.DistrictPropertyTax;
                    case E_TaxTableTaxT.Miscellaneous:
                        return PrepaidItemBase.Other;
                    case E_TaxTableTaxT.MunicipalUtilDist:
                    case E_TaxTableTaxT.School:
                    case E_TaxTableTaxT.SpecialAssessmentDist:
                        return PrepaidItemBase.DistrictPropertyTax;
                    case E_TaxTableTaxT.Town:
                        return PrepaidItemBase.TownPropertyTax;
                    case E_TaxTableTaxT.Township:
                    case E_TaxTableTaxT.Utility:
                    case E_TaxTableTaxT.Village:
                        return PrepaidItemBase.Other;
                    case E_TaxTableTaxT.WasteFeeDist:
                        return PrepaidItemBase.DistrictPropertyTax;
                    case E_TaxTableTaxT.Water_Irrigation:
                    case E_TaxTableTaxT.Water_Sewer:
                        return PrepaidItemBase.Other;
                    case E_TaxTableTaxT.LeaveBlank:
                        return PrepaidItemBase.Other;
                    default:
                        throw new UnhandledEnumException(taxType);
                }
            }
            else if (defaultSystemClosingCostFeeType == DefaultSystemClosingCostFee.Hud900WindstormInsuranceFeeTypeId)
            {
                return PrepaidItemBase.WindAndStormInsurancePremium;
            }

            return PrepaidItemBase.Other;
        }

        /// <summary>
        /// Creates a PREPAID_ITEM_PAYMENTS container with the payments associated with a prepaid fee.
        /// </summary>
        /// <param name="payments">The set of payments for a prepaid fee.</param>
        /// <param name="apr">True if the prepaid fee is to be included in APR calculations. Otherwise false.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <returns>A PREPAID_ITEM_PAYMENTS container with the payments associated with a prepaid fee.</returns>
        private PREPAID_ITEM_PAYMENTS CreatePrepaidItemPayments(IEnumerable<LoanClosingCostFeePayment> payments, bool apr, bool qm)
        {
            var prepaidItemPayments = new PREPAID_ITEM_PAYMENTS();

            foreach (BorrowerClosingCostFeePayment payment in payments.Where(p => p.Amount > 0.00m))
            {
                prepaidItemPayments.PrepaidItemPaymentList.Add(this.CreatePrepaidItemPayment(payment, apr, qm, GetNextSequenceNumber(prepaidItemPayments.PrepaidItemPaymentList)));
            }

            return prepaidItemPayments;
        }

        /// <summary>
        /// Creates a PREPAID_ITEM_PAYMENT container with information pertaining to a prepaid fee payment.
        /// </summary>
        /// <param name="payment">An individual payment for a prepaid fee.</param>
        /// <param name="apr">True if the prepaid fee is to be included in APR calculations. Otherwise false.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <param name="sequence">The sequence number of the payment among the list of payments.</param>
        /// <returns>A PREPAID_ITEM_PAYMENT container with information pertaining to a prepaid fee payment.</returns>
        private PREPAID_ITEM_PAYMENT CreatePrepaidItemPayment(BorrowerClosingCostFeePayment payment, bool apr, bool qm, int sequence)
        {
            var prepaidItemPayment = new PREPAID_ITEM_PAYMENT();

            prepaidItemPayment.PaymentFinancedIndicator = ToMismoIndicator(payment.IsFinanced);
            prepaidItemPayment.PaymentIncludedInAPRIndicator = ToMismoIndicator(apr);
            prepaidItemPayment.PrepaidItemActualPaymentAmount = ToMismoAmount(payment.Amount_rep);

            var paidByType = payment.GetPaidByType(this.DataLoan.sLenderPaidFeeDiscloseLocationT, this.ExportData.IsClosingPackage);
            prepaidItemPayment.PrepaidItemPaymentPaidByType = ToMismoEnum(this.GetPrepaidItemPaymentPaidByBaseValue(paidByType));

            if (prepaidItemPayment.PrepaidItemPaymentPaidByType?.IsSetToOther ?? false)
            {
                prepaidItemPayment.PrepaidItemPaymentPaidByTypeOtherDescription = ToMismoString(payment.GetPaidByType(this.DataLoan.sLenderPaidFeeDiscloseLocationT, this.ExportData.IsClosingPackage));
            }

            prepaidItemPayment.PrepaidItemPaymentTimingType = ToMismoEnum(this.GetPrepaidItemPaymentTimingBaseValue(payment.GfeClosingCostFeePaymentTimingT));

            if (prepaidItemPayment.PrepaidItemPaymentTimingType?.IsSetToOther ?? false)
            {
                prepaidItemPayment.PrepaidItemPaymentTimingTypeOtherDescription = ToMismoString(payment.GfeClosingCostFeePaymentTimingT);
            }

            prepaidItemPayment.RegulationZPointsAndFeesIndicator = ToMismoIndicator(qm);
            prepaidItemPayment.Extension = this.CreatePrepaidItemPaymentExtension(payment.Id.ToString());
            prepaidItemPayment.SequenceNumber = sequence;

            return prepaidItemPayment;
        }

        /// <summary>
        /// Creates an extension container holding data on a prepaid item payment.
        /// </summary>
        /// <param name="paymentId">The prepaid item payment ID.</param>
        /// <returns>A serializable <see cref="PREPAID_ITEM_PAYMENT_EXTENSION"/> container.</returns>
        private PREPAID_ITEM_PAYMENT_EXTENSION CreatePrepaidItemPaymentExtension(string paymentId)
        {
            var extension = new PREPAID_ITEM_PAYMENT_EXTENSION();
            extension.Other = this.CreateLqbPrepaidItemPaymentExtension(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container holding data on a prepaid item payment.
        /// </summary>
        /// <param name="paymentId">The prepaid item payment ID.</param>
        /// <returns>A serializable <see cref="LQB_PREPAID_ITEM_PAYMENT_EXTENSION"/> container.</returns>
        private LQB_PREPAID_ITEM_PAYMENT_EXTENSION CreateLqbPrepaidItemPaymentExtension(string paymentId)
        {
            var extension = new LQB_PREPAID_ITEM_PAYMENT_EXTENSION();
            extension.Id = ToMismoString(paymentId);

            return extension;
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid the prepaid fee payment.
        /// </summary>
        /// <param name="paidBy">The entity who paid the prepaid fee payment.</param>
        /// <returns>The corresponding <see cref="PrepaidItemPaymentPaidByBase" /> value.</returns>
        private PrepaidItemPaymentPaidByBase GetPrepaidItemPaymentPaidByBaseValue(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return PrepaidItemPaymentPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return PrepaidItemPaymentPaidByBase.Broker;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return PrepaidItemPaymentPaidByBase.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return PrepaidItemPaymentPaidByBase.Seller;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return PrepaidItemPaymentPaidByBase.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return PrepaidItemPaymentPaidByBase.Blank;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Determines the prepaid item payment timing based on the given closing cost fee payment timing.
        /// </summary>
        /// <param name="paymentTime">The closing cost fee payment timing, e.g. at closing | outside of closing.</param>
        /// <returns>The prepaid item payment timing based on the given closing cost fee payment timing.</returns>
        private PrepaidItemPaymentTimingBase GetPrepaidItemPaymentTimingBaseValue(E_GfeClosingCostFeePaymentTimingT paymentTime)
        {
            switch (paymentTime)
            {
                case E_GfeClosingCostFeePaymentTimingT.AtClosing:
                    return PrepaidItemPaymentTimingBase.AtClosing;
                case E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing:
                    return PrepaidItemPaymentTimingBase.BeforeClosing;
                case E_GfeClosingCostFeePaymentTimingT.LeaveBlank:
                    return PrepaidItemPaymentTimingBase.Blank;
                default:
                    throw new UnhandledEnumException(paymentTime);
            }
        }

        /// <summary>
        /// Creates a container holding data about a prepayment penalty.
        /// </summary>
        /// <returns>A PREPAYMENT_PENALTY container populated with data.</returns>
        private PREPAYMENT_PENALTY CreatePrepaymentPenalty()
        {
            var prepaymentPenalty = new PREPAYMENT_PENALTY();
            prepaymentPenalty.PrepaymentPenaltyPerChangeRules = this.CreatePrepaymentPenaltyPerChangeRules();
            prepaymentPenalty.PrepaymentPenaltyLifetimeRule = this.CreatePrepaymentPenaltyLifetimeRule();

            return prepaymentPenalty;
        }

        /// <summary>
        /// Creates a container holding data about a lifetime prepayment penalty.
        /// </summary>
        /// <returns>A PREPAYMENT_PENALTY_LIFETIME_RULE object populated with data.</returns>
        private PREPAYMENT_PENALTY_LIFETIME_RULE CreatePrepaymentPenaltyLifetimeRule()
        {
            var prepaymentPenaltyLifetimeRule = new PREPAYMENT_PENALTY_LIFETIME_RULE();
            prepaymentPenaltyLifetimeRule.PrepaymentFinanceChargeRefundableIndicator = this.ToPrepaymentFinanceChargeRefundableIndicator(this.DataLoan.sPrepmtRefundT);
            prepaymentPenaltyLifetimeRule.PrepaymentPenaltyExpirationMonthsCount = ToMismoCount(this.DataLoan.sHardPlusSoftPrepmtPeriodMonths_rep, null);
            prepaymentPenaltyLifetimeRule.PrepaymentPenaltyMaximumLifeOfLoanAmount = ToMismoAmount(this.DataLoan.sGfeMaxPpmtPenaltyAmt_rep);

            return prepaymentPenaltyLifetimeRule;
        }

        /// <summary>
        /// Converts an LQB enumerated value representing a prepayment finance charge indicator into a MISMO indicator.
        /// </summary>
        /// <param name="lqbValue">The LQB value to be converted.</param>
        /// <returns>A serializable MISMOIndicator object, or null if the LQB field was left blank.</returns>
        private MISMOIndicator ToPrepaymentFinanceChargeRefundableIndicator(E_sPrepmtRefundT lqbValue)
        {
            if (lqbValue == E_sPrepmtRefundT.LeaveBlank)
            {
                return null;
            }

            // May maps to true, Will Not maps to false.
            return ToMismoIndicator(lqbValue == E_sPrepmtRefundT.May);
        }

        /// <summary>
        /// Creates a container holding data about a prepayment penalty per change.
        /// </summary>
        /// <param name="sequence">The sequence number of the prepayment penalty per change rule among the list of rules.</param>
        /// <returns>A PREPAYMENT_PENALTY_PER_CHANGE_RULE object populated with data.</returns>
        private PREPAYMENT_PENALTY_PER_CHANGE_RULE CreatePrepaymentPenaltyPerChangeRule(int sequence)
        {
            var prepaymentPenaltyPerChangeRule = new PREPAYMENT_PENALTY_PER_CHANGE_RULE();
            prepaymentPenaltyPerChangeRule.SequenceNumber = sequence;
            prepaymentPenaltyPerChangeRule.PrepaymentPenaltyPeriodCount = ToMismoCount(this.DataLoan.sPrepmtPeriodMonths_rep, null);
            prepaymentPenaltyPerChangeRule.PrepaymentPenaltyPeriodType = ToMismoEnum(PrepaymentPenaltyPeriodBase.Month);
            prepaymentPenaltyPerChangeRule.PrepaymentPenaltyMaximumAmount = ToMismoAmount(this.DataLoan.sGfeMaxPpmtPenaltyAmt_rep);

            return prepaymentPenaltyPerChangeRule;
        }

        /// <summary>
        /// Creates a container holding all instances of PREPAYMENT_PENALTY_PER_CHANGE_RULE.
        /// </summary>
        /// <returns>A PREPAYMENT_PENALTY_PER_CHANGE_RULES container.</returns>
        private PREPAYMENT_PENALTY_PER_CHANGE_RULES CreatePrepaymentPenaltyPerChangeRules()
        {
            var prepaymentPenaltyPerChangeRules = new PREPAYMENT_PENALTY_PER_CHANGE_RULES();
            prepaymentPenaltyPerChangeRules.PrepaymentPenaltyPerChangeRuleList.Add(this.CreatePrepaymentPenaltyPerChangeRule(1));

            return prepaymentPenaltyPerChangeRules;
        }

        /// <summary>
        /// Creates a PRORATION_ITEMS container populated with the prorations as entered on the Adjustments and Other Credits page.
        /// </summary>
        /// <returns>Returns a PRORATION_ITEMS container.</returns>
        private PRORATION_ITEMS CreateProrationItems()
        {
            if (this.DataLoan.sProrationList == null)
            {
                return null;
            }

            var prorationItems = new PRORATION_ITEMS();
            var excludeProrationsFromLeCd = !this.DataLoan.sProrationList.IncludeProrationsInLeCdForThisLien;

            //// K. Due from Borrower at Closing - Adjustments for Items Paid by Seller in Advance
            //// And M. Due to Seller at Closing - Adjustments for Items Paid by Seller in Advance
            if (this.DataLoan.sProrationList.CityTownTaxes != null && this.DataLoan.sProrationList.CityTownTaxes.IsPaidFromBorrowerToSeller)
            {
                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.CityPropertyTax,
                        this.DataLoan.sProrationList.CityTownTaxes,
                        this.DataLoan.sProrationList.CityTownTaxes.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        1));

                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.CityPropertyTax,
                        this.DataLoan.sProrationList.CityTownTaxes,
                        this.DataLoan.sProrationList.CityTownTaxes.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));
            }

            if (this.DataLoan.sProrationList.CountyTaxes != null && this.DataLoan.sProrationList.CountyTaxes.IsPaidFromBorrowerToSeller)
            {
                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.CountyPropertyTax,
                        this.DataLoan.sProrationList.CountyTaxes,
                        this.DataLoan.sProrationList.CountyTaxes.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));

                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.CountyPropertyTax,
                        this.DataLoan.sProrationList.CountyTaxes,
                        this.DataLoan.sProrationList.CountyTaxes.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));
            }

            ProrationItemBase assessmentType = this.GetProrationItemBase_Assessment(this.DataLoan.sGseSpT);

            if (this.DataLoan.sProrationList.Assessments != null && this.DataLoan.sProrationList.Assessments.IsPaidFromBorrowerToSeller)
            {
                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        assessmentType,
                        this.DataLoan.sProrationList.Assessments,
                        this.DataLoan.sProrationList.Assessments.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));

                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        assessmentType,
                        this.DataLoan.sProrationList.Assessments,
                        this.DataLoan.sProrationList.Assessments.AmountPaidFromBorrowerToSeller_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));
            }

            var prorationList = this.DataLoan.sProrationList.GetCustomItemsEnumerable() ?? new List<Proration>();

            foreach (Proration proration in prorationList.Where(a => a.IsPaidFromBorrowerToSeller && a.Amount > 0.00m))
            {
                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.Other,
                        proration,
                        proration.Amount_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));

                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsPaidBySellerInAdvance,
                        ProrationItemBase.Other,
                        proration,
                        proration.Amount_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));
            }

            //// L. Paid Already by or on Behalf of the Borrower at Closing - Adjustments for Items Unpaid by Seller in Advance
            //// And N. Due from Seller at Closing - Adjustments for Items Unpaid by Seller in Advance
            if (this.DataLoan.sProrationList.CityTownTaxes != null && this.DataLoan.sProrationList.CityTownTaxes.IsPaidFromSellerToBorrower)
            {
                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.CityPropertyTax,
                        this.DataLoan.sProrationList.CityTownTaxes,
                        this.DataLoan.sProrationList.CityTownTaxes.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));

                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.CityPropertyTax,
                        this.DataLoan.sProrationList.CityTownTaxes,
                        this.DataLoan.sProrationList.CityTownTaxes.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));
            }

            if (this.DataLoan.sProrationList.CountyTaxes != null && this.DataLoan.sProrationList.CountyTaxes.IsPaidFromSellerToBorrower)
            {
                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.CountyPropertyTax,
                        this.DataLoan.sProrationList.CountyTaxes,
                        this.DataLoan.sProrationList.CountyTaxes.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));

                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.CountyPropertyTax,
                        this.DataLoan.sProrationList.CountyTaxes,
                        this.DataLoan.sProrationList.CountyTaxes.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));
            }

            if (this.DataLoan.sProrationList.Assessments != null && this.DataLoan.sProrationList.Assessments.IsPaidFromSellerToBorrower)
            {
                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        assessmentType,
                        this.DataLoan.sProrationList.Assessments,
                        this.DataLoan.sProrationList.Assessments.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));

                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        assessmentType,
                        this.DataLoan.sProrationList.Assessments,
                        this.DataLoan.sProrationList.Assessments.AmountPaidFromSellerToBorrower_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));
            }

            foreach (Proration proration in prorationList.Where(a => a.IsPaidFromSellerToBorrower && a.Amount > 0.00m))
            {
                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.Other,
                        proration,
                        proration.Amount_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));

                prorationItems.ProrationItemList.Add(
                    this.CreateProrationItem(
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        IntegratedDisclosureSubsectionBase.AdjustmentsForItemsUnpaidBySeller,
                        ProrationItemBase.Other,
                        proration,
                        proration.Amount_rep,
                        excludeProrationsFromLeCd,
                        GetNextSequenceNumber(prorationItems.ProrationItemList)));
            }

            return prorationItems.ProrationItemList.Count(i => i != null) > 0 ? prorationItems : null;
        }

        /// <summary>
        /// Returns the proration/assessment type based on the given property type.
        /// </summary>
        /// <param name="propertyType">The type of subject property.</param>
        /// <returns>A ProrationItemBase with the assessment type corresponding to the given property type.</returns>
        private ProrationItemBase GetProrationItemBase_Assessment(E_sGseSpT propertyType)
        {
            switch (propertyType)
            {
                case E_sGseSpT.Attached:
                    return ProrationItemBase.HomeownersAssociationSpecialAssessment;
                case E_sGseSpT.Condominium:
                    return ProrationItemBase.CondominiumAssociationSpecialAssessment;
                case E_sGseSpT.Cooperative:
                    return ProrationItemBase.CooperativeAssociationSpecialAssessment;
                case E_sGseSpT.Detached:
                    return ProrationItemBase.HomeownersAssociationSpecialAssessment;
                case E_sGseSpT.DetachedCondominium:
                case E_sGseSpT.HighRiseCondominium:
                    return ProrationItemBase.CondominiumAssociationSpecialAssessment;
                case E_sGseSpT.LeaveBlank:
                case E_sGseSpT.ManufacturedHomeCondominium:
                case E_sGseSpT.ManufacturedHomeMultiwide:
                case E_sGseSpT.ManufacturedHousing:
                case E_sGseSpT.ManufacturedHousingSingleWide:
                case E_sGseSpT.Modular:
                case E_sGseSpT.PUD:
                    return ProrationItemBase.HomeownersAssociationSpecialAssessment;
                default:
                    throw new UnhandledEnumException(propertyType);
            }
        }

        /// <summary>
        /// Creates a PRORATION_ITEM container with the given amount, payment period, etc.
        /// </summary>
        /// <param name="section">The section of the loan estimate or closing disclosure in which the proration is displayed.</param>
        /// <param name="subsection">The subsection of the loan estimate or closing disclosure that corresponds to the proration.</param>
        /// <param name="prorationItemType">The type of proration.</param>
        /// <param name="proration">The proration item to create the container for. Not all values are used, only PaidFromDate, PaidToDate, and Description.</param>
        /// <param name="amount">The proration amount. Requires a <code>rep or LosConverted</code> value.</param>
        /// <param name="excludeFromLeCd">Indicates whether the proration item should be excluded from the LE/CD.</param>
        /// <param name="sequence">The sequence of the proration among the list of prorations.</param>
        /// <returns>A PRORATION_ITEM container with the given amount, payment period, etc.</returns>
        private PRORATION_ITEM CreateProrationItem(
                                                    IntegratedDisclosureSectionBase section,
                                                    IntegratedDisclosureSubsectionBase subsection,
                                                    ProrationItemBase prorationItemType,
                                                    Proration proration,
                                                    string amount,
                                                    bool excludeFromLeCd,
                                                    int sequence)
        {
            if (IsAmountStringBlankOrZero(amount))
            {
                return null;
            }

            var prorationItem = new PRORATION_ITEM();
            prorationItem.IntegratedDisclosureSectionType = ToMismoEnum(section);
            prorationItem.IntegratedDisclosureSubsectionType = ToMismoEnum(subsection);
            prorationItem.ProrationItemAmount = ToMismoAmount(amount);
            prorationItem.ProrationItemPaidFromDate = ToMismoDate(proration.PaidFromDate_rep);
            prorationItem.ProrationItemPaidThroughDate = ToMismoDate(proration.PaidToDate_rep);
            prorationItem.ProrationItemType = ToMismoEnum(prorationItemType, displayLabelText: proration.Description);
            prorationItem.ProrationItemTypeOtherDescription = ToMismoString(prorationItemType == ProrationItemBase.Other ? proration.Description : string.Empty);

            if (ShouldSendTrid2017SubordinateFinancingData(this.DataLoan.sLienPosT, this.DataLoan.sIsOFinNew, this.DataLoan.sConcurSubFin, this.DataLoan.sTridTargetRegulationVersionT))
            {
                prorationItem.Extension = this.CreateProrationItemExtension(excludeFromLeCd);
            }

            prorationItem.SequenceNumber = sequence;
            return prorationItem;
        }

        /// <summary>
        /// Creates a container extending the PRORATION_ITEM element.
        /// </summary>
        /// <param name="excludeFromLeCd">Indicates whether the proration item should be excluded from the LE/CD.</param>
        /// <returns>A PRORATION_ITEM_EXTENSION container.</returns>
        private PRORATION_ITEM_EXTENSION CreateProrationItemExtension(bool excludeFromLeCd)
        {
            var extension = new PRORATION_ITEM_EXTENSION();
            extension.Other = this.CreateLqbProrationItemExtension(excludeFromLeCd);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary container extending the PRORATION_ITEM element.
        /// </summary>
        /// <param name="excludeFromLeCd">Indicates whether the proration item should be excluded from the LE/CD.</param>
        /// <returns>An LQB_PRORATION_ITEM_EXTENSION container.</returns>
        private LQB_PRORATION_ITEM_EXTENSION CreateLqbProrationItemExtension(bool excludeFromLeCd)
        {
            var extension = new LQB_PRORATION_ITEM_EXTENSION();
            extension.ExcludeFromLECDForThisLien = ToMismoIndicator(excludeFromLeCd);

            return extension;
        }

        /// <summary>
        /// Creates a container holding data on a qualification.
        /// </summary>
        /// <returns>A QUALIFICATION populated with data.</returns>
        private QUALIFICATION CreateQualification()
        {
            var qualification = new QUALIFICATION();
            qualification.QualifyingRatePercent = ToMismoPercent(this.DataLoan.sQualIR_rep);
            qualification.TotalVerifiedReservesAmount = ToMismoAmount(this.DataLoan.sFredieReservesAmt_rep);
            qualification.HousingExpenseRatioPercent = ToMismoPercent(this.DataLoan.sQualTopR_rep);

            if (this.DataLoan.sLT == E_sLT.VA)
            {
                qualification.TotalDebtExpenseRatioPercent = ToMismoPercent(this.PrimaryApp.aVaRatio_rep);
            }
            else
            {
                qualification.TotalDebtExpenseRatioPercent = ToMismoPercent(this.DataLoan.sQualBottomR_rep);
            }

            return qualification;
        }

        /// <summary>
        /// Creates a QUALIFIED_MORTGAGE container with information regarding the loan file's Qualified Mortgage eligibility.
        /// </summary>
        /// <returns>A QUALIFIED_MORTGAGE container with information regarding the loan file's Qualified Mortgage eligibility.</returns>
        private QUALIFIED_MORTGAGE CreateQualifiedMortgage()
        {
            var qualifiedMortgage = new QUALIFIED_MORTGAGE();
            qualifiedMortgage.QualifiedMortgageDetail = this.CreateQualifiedMortgageDetail();
            qualifiedMortgage.Exemptions = this.CreateExemptions();

            return qualifiedMortgage;
        }

        /// <summary>
        /// Creates a QUALIFIED_MORTGAGE_DETAIL container with information regarding the loan file's Qualified Mortgage eligibility.
        /// </summary>
        /// <returns>A QUALIFIED_MORTGAGE_DETAIL container with information regarding the loan file's Qualified Mortgage eligibility.</returns>
        private QUALIFIED_MORTGAGE_DETAIL CreateQualifiedMortgageDetail()
        {
            var detail = new QUALIFIED_MORTGAGE_DETAIL();

            decimal dti;
            if (decimal.TryParse(this.DataLoan.sQMQualBottom_rep, out dti))
            {
                detail.QualifiedMortgageHighestRateDebtExpenseRatioPercent = ToMismoPercent(this.DataLoan.sQMQualBottom_rep);
            }

            detail.QualifiedMortgageTotalPointsAndFeesThresholdAmount = ToMismoAmount(this.DataLoan.sQMMaxPointAndFeesAllowedAmt_rep);
            detail.QualifiedMortgageTotalPointsAndFeesThresholdPercent = ToMismoPercent(this.DataLoan.sQMMaxPointAndFeesAllowedPc_rep);
            detail.AbilityToRepayMethodType = ToMismoEnum(this.GetAbilityToRepayMethodBaseValue(this.DataLoan.sIsExemptFromAtr, this.DataLoan.sQMStatusT));
            detail.QualifiedMortgageType = ToMismoEnum(this.DataLoan.sQMIsEligibleByLoanPurchaseAgency ? QualifiedMortgageBase.TemporaryAgencyGSE : QualifiedMortgageBase.Standard, isSensitiveData: false);

            if (detail.QualifiedMortgageType != null && detail.QualifiedMortgageType.EnumValue == QualifiedMortgageBase.TemporaryAgencyGSE)
            {
                var qualifiedMortgageTemporaryGse = GetQualifiedMortgageTemporaryGSEBase(this.DataLoan.sQMLoanPurchaseAgency);
                detail.QualifiedMortgageTemporaryGSEType = qualifiedMortgageTemporaryGse == QualifiedMortgageTemporaryGSEBase.Blank ? null : ToMismoEnum(qualifiedMortgageTemporaryGse, isSensitiveData: false);
            }

            return detail;
        }

        /// <summary>
        /// Gets the MISMO value indicating the ATR type.
        /// </summary>
        /// <param name="isExemptFromAtr">Indicates whether the loan is exempt from ATR.</param>
        /// <param name="qmStatus">The QM status of the loan.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private AbilityToRepayMethodBase GetAbilityToRepayMethodBaseValue(bool isExemptFromAtr, E_sQMStatusT qmStatus)
        {
            if (isExemptFromAtr)
            {
                return AbilityToRepayMethodBase.Exempt;
            }

            switch (qmStatus)
            {
                case E_sQMStatusT.Blank:
                    return AbilityToRepayMethodBase.Blank;
                case E_sQMStatusT.Eligible:
                    return AbilityToRepayMethodBase.QualifiedMortgage;
                case E_sQMStatusT.Ineligible:
                    return AbilityToRepayMethodBase.General;
                case E_sQMStatusT.ProvisionallyEligible:
                    return AbilityToRepayMethodBase.QualifiedMortgage;
                default:
                    throw new UnhandledEnumException(qmStatus);
            }
        }

        /// <summary>
        /// Creates a container holding a list of exemptions.
        /// </summary>
        /// <returns>An EXEMPTIONS container.</returns>
        private EXEMPTIONS CreateExemptions()
        {
            var exemptions = new EXEMPTIONS();
            exemptions.ExemptionList.Add(this.CreateExemption(GetNextSequenceNumber(exemptions.ExemptionList)));

            return exemptions;
        }

        /// <summary>
        /// Creates a container holding data on an exemption.
        /// </summary>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>An EXEMPTION container.</returns>
        private EXEMPTION CreateExemption(int sequence)
        {
            var exemption = new EXEMPTION();
            exemption.AbilityToRepayExemptionCreditExtendedPurposeIndicator = ToMismoIndicator(this.DataLoan.sIsExemptFromAtr);
            exemption.SequenceNumber = sequence;

            return exemption;
        }

        /// <summary>
        /// Creates a container holding rate or payment change occurrences for a construction ARM.
        /// </summary>
        /// <returns>A RATE_OR_PAYMENT_CHANGE_OCCURRENCES container.</returns>
        private RATE_OR_PAYMENT_CHANGE_OCCURRENCES CreateRateOrPaymentChangeOccurrences_Construction()
        {
            var occurrences = new RATE_OR_PAYMENT_CHANGE_OCCURRENCES();
            occurrences.RateOrPaymentChangeOccurrenceList.Add(this.CreateRateOrPaymentChangeOccurrence_Construction());

            return occurrences;
        }

        /// <summary>
        /// Creates a container holding a rate or payment change occurrence for a construction ARM.
        /// </summary>
        /// <returns>A RATE_OR_PAYMENT_CHANGE_OCCURRENCE container.</returns>
        private RATE_OR_PAYMENT_CHANGE_OCCURRENCE CreateRateOrPaymentChangeOccurrence_Construction()
        {
            var occurrence = new RATE_OR_PAYMENT_CHANGE_OCCURRENCE();
            occurrence.AdjustmentChangeIndexRatePercent = ToMismoPercent(this.DataLoan.sConstructionRAdjIndexR_rep);
            occurrence.SequenceNumber = 1;

            return occurrence;
        }

        /// <summary>
        /// Creates a container holding refinance information.
        /// </summary>
        /// <returns>A REFINANCE container, or null if the loan is not a refinance.</returns>
        private REFINANCE CreateRefinance()
        {
            if (!this.DataLoan.sIsRefinancing)
            {
                return null;
            }

            var refinance = new REFINANCE();
            refinance.RefinanceCashOutAmount = ToMismoAmount(this.DataLoan.sProdCashoutAmt_rep);
            refinance.SecondaryFinancingRefinanceIndicator = ToMismoIndicator(this.DataLoan.sPayingOffSubordinate);
            refinance.RefinanceCashOutDeterminationType = ToMismoEnum(GetRefinanceCashOutDeterminationBaseValue(this.DataLoan.sRefPurpose, this.DataLoan.sLPurposeT), isSensitiveData: false);
            refinance.RefinancePrimaryPurposeType = ToMismoEnum(GetRefinancePrimaryPurposeBaseValue(this.DataLoan.sRefPurpose), isSensitiveData: false);

            if (refinance.RefinancePrimaryPurposeType != null && refinance.RefinancePrimaryPurposeType.EnumValue == RefinancePrimaryPurposeBase.Other)
            {
                refinance.RefinancePrimaryPurposeTypeOtherDescription = ToMismoString(this.DataLoan.sRefPurpose);
            }

            return refinance;
        }

        /// <summary>
        /// Creates a REHABILITATION container with information pertaining to FHA 203k loans.
        /// </summary>
        /// <returns>A REHABILITATION container with information pertaining to FHA 203k loans.</returns>
        private REHABILITATION CreateRehabilitation()
        {
            var rehabilitation = new REHABILITATION();

            if (!this.DataLoan.sIsRenovationLoan && (this.DataLoan.sLT != E_sLT.FHA || (this.DataLoan.sFHASpAfterImprovedVal == 0.00m && this.DataLoan.sFHA203kRepairCostReserveR == 0.00m)))
            {
                return null;
            }
            else if (this.DataLoan.sIsRenovationLoan)
            {
                rehabilitation.RehabilitationContingencyPercent = ToMismoPercent(this.DataLoan.sRehabContingencyPcFinalLAmt_rep);
                rehabilitation.RehabilitationEstimatedPropertyValueAmount = ToMismoAmount(this.DataLoan.sApprVal_rep);
                rehabilitation.RehabilitationLoanFundsAmount = ToMismoAmount(this.DataLoan.sTotalRenovationCosts_rep);
            }
            else
            {
                rehabilitation.RehabilitationContingencyPercent = ToMismoPercent(this.DataLoan.sFHA203kRepairCostReserveR_rep);
                rehabilitation.RehabilitationEstimatedPropertyValueAmount = ToMismoAmount(this.DataLoan.sFHASpAfterImprovedVal_rep);
            }

            return rehabilitation;
        }

        /// <summary>
        /// Creates a container to hold underwriting information.
        /// </summary>
        /// <returns>An UNDERWRITING container.</returns>
        private UNDERWRITING CreateUnderwriting()
        {
            var underwriting = new UNDERWRITING();
            underwriting.AutomatedUnderwritings = this.CreateAutomatedUnderwritings();
            underwriting.UnderwritingDetail = this.CreateUnderwritingDetail();

            return underwriting;
        }

        /// <summary>
        /// Creates a container to hold all instances of automated underwriting processes.
        /// </summary>
        /// <returns>An AUTOMATED_UNDERWRITINGS container.</returns>
        private AUTOMATED_UNDERWRITINGS CreateAutomatedUnderwritings()
        {
            var automatedUnderwritings = new AUTOMATED_UNDERWRITINGS();
            automatedUnderwritings.AutomatedUnderwritingList.Add(this.CreateAutomatedUnderwriting(GetNextSequenceNumber(automatedUnderwritings.AutomatedUnderwritingList)));

            var latestUcdDelivery = this.DataLoan.sUcdDeliveryCollection.OrderByDescending(ucd => ucd.DateOfDelivery.DateTimeForComputationWithTime).FirstOrDefault();
            if (latestUcdDelivery != null)
            {
                automatedUnderwritings.AutomatedUnderwritingList.Add(this.CreateAutomatedUnderwriting_UcdDelivery(latestUcdDelivery.CaseFileId, GetNextSequenceNumber(automatedUnderwritings.AutomatedUnderwritingList)));
            }

            return automatedUnderwritings;
        }

        /// <summary>
        /// Creates a container with information about the automated underwriting process.
        /// </summary>
        /// <param name="sequence">The sequence number of the automated underwriting object.</param>
        /// <returns>An AUTOMATED_UNDERWRITING container.</returns>
        private AUTOMATED_UNDERWRITING CreateAutomatedUnderwriting(int sequence)
        {
            var automatedUnderwriting = new AUTOMATED_UNDERWRITING();
            automatedUnderwriting.SequenceNumber = sequence;
            automatedUnderwriting.AutomatedUnderwritingCaseIdentifier = ToMismoIdentifier(string.IsNullOrEmpty(this.DataLoan.sDuCaseId) ? this.DataLoan.sLpAusKey : this.DataLoan.sDuCaseId);
            automatedUnderwriting.AutomatedUnderwritingDecisionDatetime = ToMismoDatetime(GetUnderwritingDecisionDate(this.DataLoan.sApprovD, this.DataLoan.sApprovD_rep_WithTime, this.DataLoan.sRejectD, this.DataLoan.sRejectD_rep_WithTime), false);
            automatedUnderwriting.AutomatedUnderwritingSystemResultValue = ToMismoValue(GetAutomatedUnderwritingSystemResultValue(this.DataLoan.sFHARatedAcceptedByTotalScorecard, this.DataLoan.sFHARatedReferByTotalScorecard));
            automatedUnderwriting.AutomatedUnderwritingSystemType = ToMismoEnum(GetAutomatedUnderwritingSystemBaseValue(this.DataLoan.sIsDuUw, this.DataLoan.sIsLpUw, this.DataLoan.sIsOtherUw));

            if (automatedUnderwriting.AutomatedUnderwritingSystemType != null && automatedUnderwriting.AutomatedUnderwritingSystemType.EnumValue == AutomatedUnderwritingSystemBase.Other)
            {
                automatedUnderwriting.AutomatedUnderwritingSystemTypeOtherDescription = ToMismoString(this.DataLoan.sOtherUwDesc);
            }

            if (this.DataLoan.sLT == E_sLT.VA)
            {
                automatedUnderwriting.AutomatedUnderwritingRecommendationDescription = ToMismoString(GetAutomatedUnderwritingRecommendationDescription(this.DataLoan.sVaRiskT));
            }
            else
            {
                automatedUnderwriting.AutomatedUnderwritingRecommendationDescription = ToMismoString(GetAutomatedUnderwritingRecommendationDescription(this.DataLoan.sProd3rdPartyUwResultT));
            }

            if (!automatedUnderwriting.ShouldSerialize)
            {
                return null;
            }

            return automatedUnderwriting;
        }

        /// <summary>
        /// Creates a container to hold a UCD delivery casefile.
        /// </summary>
        /// <param name="ucdCaseFileId">The UCD casefile ID.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>An AUTOMATED_UNDERWRITING container.</returns>
        private AUTOMATED_UNDERWRITING CreateAutomatedUnderwriting_UcdDelivery(string ucdCaseFileId, int sequenceNumber)
        {
            var automatedUnderwriting = new AUTOMATED_UNDERWRITING();
            automatedUnderwriting.AutomatedUnderwritingSystemType = ToMismoEnum(AutomatedUnderwritingSystemBase.Other);
            automatedUnderwriting.AutomatedUnderwritingSystemTypeOtherDescription = ToMismoString("UCD");
            automatedUnderwriting.AutomatedUnderwritingCaseIdentifier = ToMismoIdentifier(ucdCaseFileId);
            automatedUnderwriting.SequenceNumber = sequenceNumber;

            return automatedUnderwriting;
        }

        /// <summary>
        /// Creates a container to hold underwriting detail information.
        /// </summary>
        /// <returns>An UNDERWRITING container.</returns>
        private UNDERWRITING_DETAIL CreateUnderwritingDetail()
        {
            var underwritingDetail = new UNDERWRITING_DETAIL();
            underwritingDetail.LoanManualUnderwritingIndicator = ToMismoIndicator(this.DataLoan.sIsManualUw);

            return underwritingDetail;
        }
    }
}