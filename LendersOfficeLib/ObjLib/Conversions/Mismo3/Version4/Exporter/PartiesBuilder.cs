﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using DataAccess.Trust;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Rolodex;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Exporter for MISMO 3.4 PARTIES data.
    /// </summary>
    public class PartiesBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartiesBuilder" /> class.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="exportData">The exporter cache.</param>
        public PartiesBuilder(CPageData dataLoan, Mismo34ExporterCache exportData)
        {
            this.DataLoan = dataLoan;
            this.ExportData = exportData;
        }

        /// <summary>
        /// Gets the loan object to be exported.
        /// </summary>
        protected CPageData DataLoan { get; private set; }

        /// <summary>
        /// Gets a cache for data used throughout the export.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Creates the container output of this exporter.
        /// </summary>
        /// <returns>A PARTIES exporter.</returns>
        public PARTIES CreateContainer()
        {
            return this.CreateParties();
        }

        /// <summary>
        /// Retrieves the BorrowerRelationshipTitle value corresponding to the given LQB value.
        /// </summary>
        /// <param name="lqbValue">The LQB value to convert.</param>
        /// <returns>The corresponding BorrowerRelationshipTitle value.</returns>
        private static BorrowerRelationshipTitleBase GetBorrowerRelationshipTitleBaseValue(E_aRelationshipTitleT lqbValue)
        {
            switch (lqbValue)
            {
                case E_aRelationshipTitleT.AHusbandAndWife:
                case E_aRelationshipTitleT.HusbandAndWife:
                    return BorrowerRelationshipTitleBase.AHusbandAndWife;
                case E_aRelationshipTitleT.AMarriedMan:
                    return BorrowerRelationshipTitleBase.AMarriedMan;
                case E_aRelationshipTitleT.AMarriedPerson:
                    return BorrowerRelationshipTitleBase.AMarriedPerson;
                case E_aRelationshipTitleT.AMarriedWoman:
                    return BorrowerRelationshipTitleBase.AMarriedWoman;
                case E_aRelationshipTitleT.AnUnmarriedMan:
                    return BorrowerRelationshipTitleBase.AnUnmarriedMan;
                case E_aRelationshipTitleT.AnUnmarriedPerson:
                    return BorrowerRelationshipTitleBase.AnUnmarriedPerson;
                case E_aRelationshipTitleT.AnUnmarriedWoman:
                    return BorrowerRelationshipTitleBase.AnUnmarriedWoman;
                case E_aRelationshipTitleT.DomesticPartners:
                    return BorrowerRelationshipTitleBase.AsDomesticPartners;
                case E_aRelationshipTitleT.ASingleMan:
                    return BorrowerRelationshipTitleBase.ASingleMan;
                case E_aRelationshipTitleT.ASinglePerson:
                    return BorrowerRelationshipTitleBase.ASinglePerson;
                case E_aRelationshipTitleT.ASingleWoman:
                    return BorrowerRelationshipTitleBase.ASingleWoman;
                case E_aRelationshipTitleT.AWidow:
                    return BorrowerRelationshipTitleBase.AWidow;
                case E_aRelationshipTitleT.AWidower:
                    return BorrowerRelationshipTitleBase.AWidower;
                case E_aRelationshipTitleT.AWifeAndHusband:
                case E_aRelationshipTitleT.WifeAndHusband:
                    return BorrowerRelationshipTitleBase.AWifeAndHusband;
                case E_aRelationshipTitleT.HerHusband:
                    return BorrowerRelationshipTitleBase.HerHusband;
                case E_aRelationshipTitleT.HisWife:
                    return BorrowerRelationshipTitleBase.HisWife;
                case E_aRelationshipTitleT.NotApplicable:
                    return BorrowerRelationshipTitleBase.NotApplicable;
                case E_aRelationshipTitleT.AMarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AMarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AMarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.AnUnmarriedWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleManAsHisSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleManAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ASingleWomanAsHerSoleAndSeparateProperty:
                case E_aRelationshipTitleT.ASingleWomanAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.ChiefExecutiveOfficer:
                case E_aRelationshipTitleT.GeneralPartner:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityProperty:
                case E_aRelationshipTitleT.HusbandAndWifeAsCommunityPropertyWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenants:
                case E_aRelationshipTitleT.HusbandAndWifeAsJointTenantsWithRightOfSurvivorship:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsByTheEntirety:
                case E_aRelationshipTitleT.HusbandAndWifeAsTenantsInCommon:
                case E_aRelationshipTitleT.HusbandAndWifeAsToAnUndividedHalfInterest:
                case E_aRelationshipTitleT.HusbandAndWifeTenancyByTheEntirety:
                case E_aRelationshipTitleT.PersonalGuarantor:
                case E_aRelationshipTitleT.President:
                case E_aRelationshipTitleT.Secretary:
                case E_aRelationshipTitleT.TenancyByEntirety:
                case E_aRelationshipTitleT.TenantsByTheEntirety:
                case E_aRelationshipTitleT.Treasurer:
                case E_aRelationshipTitleT.Trustee:
                case E_aRelationshipTitleT.VicePresident:
                case E_aRelationshipTitleT.WifeAndHusbandAsCommunityProperty:
                case E_aRelationshipTitleT.WifeAndHusbandAsJointTenants:
                case E_aRelationshipTitleT.WifeAndHusbandAsTenantsInCommon:
                case E_aRelationshipTitleT.Other:
                    return BorrowerRelationshipTitleBase.Other;
                case E_aRelationshipTitleT.LeaveBlank:
                    return BorrowerRelationshipTitleBase.Blank;
                default:
                    throw new UnhandledEnumException(lqbValue);
            }
        }

        /// <summary>
        /// Retrieves the party role type corresponding to the given agent role.
        /// </summary>
        /// <param name="agentRole">The agent role to be mapped to MISMO.</param>
        /// <param name="agentRoleDescription">The description of the agent's role if agentRole is set to Other.</param>
        /// <param name="entityType">Indicates whether the agent is an individual or legal entity.</param>
        /// <param name="tpoTransaction">Indicates whether the loan is originated by a third-party.</param>
        /// <param name="branchChannel">The branch channel of the loan.</param>
        /// <param name="otherDescription">Will be populated with the party role if the agent does not map directly.</param>
        /// <returns>The <see cref="PartyRoleBase" /> value corresponding to the given agent role.</returns>
        private static PartyRoleBase GetPartyRoleBaseValue(E_AgentRoleT agentRole, string agentRoleDescription, EntityType entityType, bool tpoTransaction, E_BranchChannelT branchChannel, out string otherDescription)
        {
            otherDescription = string.Empty;

            switch (agentRole)
            {
                case E_AgentRoleT.Appraiser:
                    return PartyRoleBase.Appraiser;
                case E_AgentRoleT.Broker:
                case E_AgentRoleT.BrokerRep:
                    return PartyRoleBase.MortgageBroker;
                case E_AgentRoleT.BrokerProcessor:
                    if (branchChannel == E_BranchChannelT.Wholesale)
                    {
                        otherDescription = "BrokerProcessor";
                        return PartyRoleBase.Other;
                    }

                    return PartyRoleBase.LoanProcessor;

                case E_AgentRoleT.Builder:
                    return PartyRoleBase.Builder;
                case E_AgentRoleT.BuyerAgent:
                    return PartyRoleBase.RealEstateAgent;
                case E_AgentRoleT.BuyerAttorney:
                    return PartyRoleBase.Attorney;
                case E_AgentRoleT.ClosingAgent:
                case E_AgentRoleT.Escrow:
                    return PartyRoleBase.ClosingAgent;
                case E_AgentRoleT.FloodProvider:
                    return PartyRoleBase.FloodCertificateProvider;
                case E_AgentRoleT.HazardInsurance:
                    return entityType == EntityType.Individual ? PartyRoleBase.HazardInsuranceAgent : PartyRoleBase.HazardInsuranceCompany;
                case E_AgentRoleT.HomeOwnerAssociation:
                    return PartyRoleBase.HomeownersAssociation;
                case E_AgentRoleT.HomeOwnerInsurance:
                    return entityType == EntityType.Individual ? PartyRoleBase.HazardInsuranceAgent : PartyRoleBase.HazardInsuranceCompany;
                case E_AgentRoleT.Investor:
                    return PartyRoleBase.Investor;
                case E_AgentRoleT.Lender:
                    return PartyRoleBase.Lender;
                case E_AgentRoleT.ListingAgent:
                    return PartyRoleBase.RealEstateAgent;
                case E_AgentRoleT.LoanOfficer:
                    return tpoTransaction ? PartyRoleBase.MortgageBroker : PartyRoleBase.LoanOfficer;
                case E_AgentRoleT.LoanPurchasePayee:
                    return PartyRoleBase.Payee;
                case E_AgentRoleT.MortgageInsurance:
                    return PartyRoleBase.MICompany;
                case E_AgentRoleT.Processor:
                    return PartyRoleBase.LoanProcessor;
                case E_AgentRoleT.Realtor:
                    return PartyRoleBase.RealEstateAgent;
                case E_AgentRoleT.Seller:
                    return PartyRoleBase.PropertySeller;
                case E_AgentRoleT.SellerAttorney:
                    return PartyRoleBase.Attorney;
                case E_AgentRoleT.SellingAgent:
                    return PartyRoleBase.RealEstateAgent;
                case E_AgentRoleT.Servicing:
                case E_AgentRoleT.Subservicer:
                    return PartyRoleBase.Servicer;
                case E_AgentRoleT.Title:
                    return PartyRoleBase.TitleCompany;
                case E_AgentRoleT.TitleUnderwriter:
                    return PartyRoleBase.TitleUnderwriter;
                case E_AgentRoleT.Trustee:
                    return PartyRoleBase.Trustee;
                case E_AgentRoleT.Underwriter:
                    return PartyRoleBase.LoanUnderwriter;
                case E_AgentRoleT.Bank:
                case E_AgentRoleT.CallCenterAgent:
                case E_AgentRoleT.CollateralAgent:
                case E_AgentRoleT.CreditAuditor:
                case E_AgentRoleT.CreditReport:
                case E_AgentRoleT.CreditReportAgency2:
                case E_AgentRoleT.CreditReportAgency3:
                case E_AgentRoleT.DisclosureDesk:
                case E_AgentRoleT.DocDrawer:
                case E_AgentRoleT.ECOA:
                case E_AgentRoleT.ExternalPostCloser:
                case E_AgentRoleT.ExternalSecondary:
                case E_AgentRoleT.FairHousingLending:
                case E_AgentRoleT.Funder:
                case E_AgentRoleT.HomeInspection:
                case E_AgentRoleT.Insuring:
                case E_AgentRoleT.JuniorProcessor:
                case E_AgentRoleT.JuniorUnderwriter:
                case E_AgentRoleT.LegalAuditor:
                case E_AgentRoleT.LoanOfficerAssistant:
                case E_AgentRoleT.LoanOpener:
                case E_AgentRoleT.Manager:
                case E_AgentRoleT.MarketingLead:
                case E_AgentRoleT.Mortgagee:
                case E_AgentRoleT.PestInspection:
                case E_AgentRoleT.PostCloser:
                case E_AgentRoleT.PropertyManagement:
                case E_AgentRoleT.Purchaser:
                case E_AgentRoleT.QCCompliance:
                case E_AgentRoleT.Secondary:
                case E_AgentRoleT.Shipper:
                case E_AgentRoleT.Surveyor:
                case E_AgentRoleT.AppraisalManagementCompany:
                case E_AgentRoleT.Referral:
                    otherDescription = agentRole.ToString();
                    return PartyRoleBase.Other;
                case E_AgentRoleT.Other:
                    // OPM 236992: Enum Parse will succeed if input string is an int, even if it does not correspond to a valid enumeration.
                    PartyRoleBase partyRoleType;
                    if (Enum.TryParse<PartyRoleBase>(agentRoleDescription, out partyRoleType) && Enum.IsDefined(typeof(PartyRoleBase), partyRoleType))
                    {
                        return partyRoleType;
                    }

                    otherDescription = agentRoleDescription;
                    return PartyRoleBase.Other;
                default:
                    throw new UnhandledEnumException(agentRole);
            }
        }

        /// <summary>
        /// Converts the agent type to real estate agent type.
        /// </summary>
        /// <param name="agentType">The agent type.</param>
        /// <returns>The RealEstateAgentBase corresponding to the agent type.</returns>
        private static RealEstateAgentBase GetRealEstateAgentBase(E_AgentRoleT agentType)
        {
            switch (agentType)
            {
                case E_AgentRoleT.AppraisalManagementCompany:
                case E_AgentRoleT.Appraiser:
                case E_AgentRoleT.Bank:
                case E_AgentRoleT.Broker:
                case E_AgentRoleT.BrokerProcessor:
                case E_AgentRoleT.BrokerRep:
                case E_AgentRoleT.Builder:
                    return RealEstateAgentBase.Blank;
                case E_AgentRoleT.BuyerAgent:
                    return RealEstateAgentBase.Selling;
                case E_AgentRoleT.BuyerAttorney:
                case E_AgentRoleT.CallCenterAgent:
                case E_AgentRoleT.ClosingAgent:
                case E_AgentRoleT.CollateralAgent:
                case E_AgentRoleT.CreditAuditor:
                case E_AgentRoleT.CreditReport:
                case E_AgentRoleT.CreditReportAgency2:
                case E_AgentRoleT.CreditReportAgency3:
                case E_AgentRoleT.DisclosureDesk:
                case E_AgentRoleT.DocDrawer:
                case E_AgentRoleT.ECOA:
                case E_AgentRoleT.Escrow:
                case E_AgentRoleT.ExternalPostCloser:
                case E_AgentRoleT.ExternalSecondary:
                case E_AgentRoleT.FairHousingLending:
                case E_AgentRoleT.FloodProvider:
                case E_AgentRoleT.Funder:
                case E_AgentRoleT.HazardInsurance:
                case E_AgentRoleT.HomeInspection:
                case E_AgentRoleT.HomeOwnerAssociation:
                case E_AgentRoleT.HomeOwnerInsurance:
                case E_AgentRoleT.Insuring:
                case E_AgentRoleT.Investor:
                case E_AgentRoleT.JuniorProcessor:
                case E_AgentRoleT.JuniorUnderwriter:
                case E_AgentRoleT.LegalAuditor:
                case E_AgentRoleT.Lender:
                    return RealEstateAgentBase.Blank;
                case E_AgentRoleT.ListingAgent:
                    return RealEstateAgentBase.Listing;
                case E_AgentRoleT.LoanOfficer:
                case E_AgentRoleT.LoanOfficerAssistant:
                case E_AgentRoleT.LoanOpener:
                case E_AgentRoleT.LoanPurchasePayee:
                case E_AgentRoleT.Manager:
                case E_AgentRoleT.MarketingLead:
                case E_AgentRoleT.Mortgagee:
                case E_AgentRoleT.MortgageInsurance:
                case E_AgentRoleT.Other:
                case E_AgentRoleT.PestInspection:
                case E_AgentRoleT.PostCloser:
                case E_AgentRoleT.Processor:
                case E_AgentRoleT.PropertyManagement:
                case E_AgentRoleT.Purchaser:
                case E_AgentRoleT.QCCompliance:
                    return RealEstateAgentBase.Blank;
                case E_AgentRoleT.Realtor:
                    return RealEstateAgentBase.Other;
                case E_AgentRoleT.Secondary:
                case E_AgentRoleT.Seller:
                case E_AgentRoleT.SellerAttorney:
                    return RealEstateAgentBase.Blank;
                case E_AgentRoleT.SellingAgent:
                    return RealEstateAgentBase.Selling;
                case E_AgentRoleT.Servicing:
                case E_AgentRoleT.Shipper:
                case E_AgentRoleT.Subservicer:
                case E_AgentRoleT.Surveyor:
                case E_AgentRoleT.Title:
                case E_AgentRoleT.TitleUnderwriter:
                case E_AgentRoleT.Trustee:
                case E_AgentRoleT.Underwriter:
                    return RealEstateAgentBase.Blank;
                default:
                    throw new UnhandledEnumException(agentType);
            }
        }

        /// <summary>
        /// Retrieves the loan originator value corresponding to the given originator type.
        /// </summary>
        /// <param name="originatorType">The loan originator type.</param>
        /// <returns>The corresponding loan originator value.</returns>
        private static LoanOriginatorBase GetLoanOriginatorBaseValue(E_BranchChannelT originatorType)
        {
            switch (originatorType)
            {
                case E_BranchChannelT.Broker:
                case E_BranchChannelT.Wholesale:
                    return LoanOriginatorBase.Broker;
                case E_BranchChannelT.Correspondent:
                    return LoanOriginatorBase.Correspondent;
                case E_BranchChannelT.Blank:
                case E_BranchChannelT.Retail:
                    return LoanOriginatorBase.Lender;
                default:
                    throw new UnhandledEnumException(originatorType);
            }
        }

        /// <summary>
        /// Collects the record IDs of agents that should be excluded to prevent duplication with
        /// contacts on the Loan Estimate, Closing Disclosure, SSPL page, and elsewhere.
        /// </summary>
        /// <returns>A list of record IDs.</returns>
        private List<Guid> CompileExcludedAgents()
        {
            var agentRecords = new List<Guid>();

            if (!this.DataLoan.sIsInTRID2015Mode)
            {
                return agentRecords;
            }

            if (this.DataLoan.sTRIDLenderAgentId != Guid.Empty && this.ExportData.Options.DocumentVendor != E_DocumentVendor.DocsOnDemand)
            {
                agentRecords.Add(this.DataLoan.sTRIDLenderAgentId);
            }

            if (this.DataLoan.sTRIDMortgageBrokerAgentId != Guid.Empty)
            {
                agentRecords.Add(this.DataLoan.sTRIDMortgageBrokerAgentId);
            }

            foreach (var seller in this.DataLoan.sSellerCollection.ListOfSellers)
            {
                if (seller.AgentId != Guid.Empty)
                {
                    agentRecords.Add(seller.AgentId);
                }
            }

            if (this.ExportData.IsClosingPackage)
            {
                if (this.DataLoan.sTRIDSettlementAgentAgentId != Guid.Empty && this.ExportData.Options.DocumentVendor != E_DocumentVendor.DocsOnDemand)
                {
                    agentRecords.Add(this.DataLoan.sTRIDSettlementAgentAgentId);
                }

                if (this.DataLoan.sTRIDRealEstateBrokerBuyerAgentId != Guid.Empty)
                {
                    agentRecords.Add(this.DataLoan.sTRIDRealEstateBrokerBuyerAgentId);
                }

                if (this.DataLoan.sTRIDRealEstateBrokerSellerAgentId != Guid.Empty)
                {
                    agentRecords.Add(this.DataLoan.sTRIDRealEstateBrokerSellerAgentId);
                }
            }
            else
            {
                if (this.DataLoan.sTRIDLoanOfficerAgentId != Guid.Empty)
                {
                    agentRecords.Add(this.DataLoan.sTRIDLoanOfficerAgentId);
                }
            }

            foreach (var provider in this.DataLoan.sAvailableSettlementServiceProviders.GetAllProviders())
            {
                if (provider.AgentId != Guid.Empty)
                {
                    agentRecords.Add(provider.AgentId);
                }
            }

            return agentRecords;
        }

        /// <summary>
        /// Creates a PARTIES container holding all party information for the loan file, and
        /// populates RELATIONSHIPS related to parties as necessary.
        /// </summary>
        /// <returns>A PARTIES container populated with all party information for the loan file.</returns>
        private PARTIES CreateParties()
        {
            var parties = new PARTIES();
            var borrowerNames = new List<string>();
            var borrowersByApplicationId = new Dictionary<Guid, string>();

            this.CreateBorrowerParties(parties.PartyList, borrowerNames, borrowersByApplicationId);
            this.CreateTitleBorrowerParties(parties.PartyList, borrowerNames, borrowersByApplicationId);
            this.CreateAgentParties(parties.PartyList);

            DisclosurePartiesBuilder disclosurePartiesBuilder = new DisclosurePartiesBuilder(this.DataLoan, this.ExportData, parties.PartyList);
            disclosurePartiesBuilder.PopulatePartiesList();

            this.CreateSspParties(parties.PartyList);
            this.CreateTrusteeParties(parties.PartyList, borrowerNames);

            // Other Parties
            parties.PartyList.Add(this.CreateParty_Lender(GetNextSequenceNumber(parties.PartyList)));
            parties.PartyList.Add(this.CreateParty_LQB(GetNextSequenceNumber(parties.PartyList)));
            this.CreateBranchParty(parties.PartyList);
            this.CreatePreparerParties(parties.PartyList);

            parties.PartyList.Add(this.CreateParty_WindInsurance(GetNextSequenceNumber(parties.PartyList)));
            parties.PartyList.Add(this.CreateParty_WarehouseLender(GetNextSequenceNumber(parties.PartyList)));
            parties.PartyList.Add(this.CreateParty(this.DataLoan.sCalcInvestorNm, GetNextSequenceNumber(parties.PartyList)));

            this.CreateInvestorParties(parties.PartyList);
            return parties;
        }

        /// <summary>
        /// Creates the parties for a borrower.
        /// </summary>
        /// <param name="partyList">The party list to populate.</param>
        /// <param name="borrowerNames">Running list of borrower names.</param>
        /// <param name="borrowersByApplicationId">Running list of borrowers mapped to application id.</param>
        private void CreateBorrowerParties(List<PARTY> partyList, List<string> borrowerNames, Dictionary<Guid, string> borrowersByApplicationId)
        {
            // Borrower Parties
            for (int borrowerIndex = 0; borrowerIndex < this.DataLoan.nApps; borrowerIndex++)
            {
                CAppData appData = this.DataLoan.GetAppData(borrowerIndex);

                // Skip if there is no borrower or coborrower
                if (!appData.aBIsValidNameSsn && !appData.aCIsValidNameSsn)
                {
                    continue;
                }

                if (appData.aBIsValidNameSsn)
                {
                    appData.BorrowerModeT = E_BorrowerModeT.Borrower;
                    var borrowerPartyBuilder = new BorrowerPartyBuilder(this.DataLoan, this.ExportData, appData, E_BorrowerModeT.Borrower, GetNextSequenceNumber(partyList));
                    partyList.Add(borrowerPartyBuilder.CreateContainer());

                    partyList.Add(this.CreateParty_AttorneyInFact(appData.aBPowerOfAttorneyNm, appData.aBMismoId, GetNextSequenceNumber(partyList)));

                    this.ExportData.CreateRelationship_ToSubjectLoan("ROLE", appData.aBMismoId, ArcroleVerbPhrase.IsAssociatedWith);
                    borrowerNames.Add(appData.aBNm);
                    borrowersByApplicationId.Add(appData.aAppId, appData.aBMismoId);
                }

                if (appData.aCIsValidNameSsn)
                {
                    appData.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    var coborrowerPartyBuilder = new BorrowerPartyBuilder(this.DataLoan, this.ExportData, appData, E_BorrowerModeT.Coborrower, GetNextSequenceNumber(partyList));
                    partyList.Add(coborrowerPartyBuilder.CreateContainer());

                    partyList.Add(this.CreateParty_AttorneyInFact(appData.aCPowerOfAttorneyNm, appData.aCMismoId, GetNextSequenceNumber(partyList)));
                    if (this.ExportData.Options.DocumentVendor != E_DocumentVendor.DocMagic || (appData.aCTypeT != E_aTypeT.TitleOnly && appData.aCTypeT != E_aTypeT.NonTitleSpouse))
                    {
                        this.ExportData.CreateRelationship(ArcroleVerbPhrase.SharesSufficientAssetsAndLiabilitiesWith, "ROLE", "ROLE", appData.aBMismoId, appData.aCMismoId);
                    }

                    this.ExportData.CreateRelationship_ToSubjectLoan("ROLE", appData.aCMismoId, ArcroleVerbPhrase.IsAssociatedWith);
                    borrowerNames.Add(appData.aCNm);
                }
            }
        }

        /// <summary>
        /// Creates the parties for title borrowers.
        /// </summary>
        /// <param name="partyList">The party list to populate.</param>
        /// <param name="borrowerNames">Running list of borrower names.</param>
        /// <param name="borrowersByApplicationId">Running list of borrowers mapped to application id.</param>
        private void CreateTitleBorrowerParties(List<PARTY> partyList, List<string> borrowerNames, Dictionary<Guid, string> borrowersByApplicationId)
        {
            var titleOnlyBorrowerPartyCount = 1;
            foreach (var borrower in this.DataLoan.sTitleBorrowers)
            {
                if (borrower.FullName.TrimWhitespaceAndBOM().Length > 0 || borrower.SSN.Length > 0)
                {
                    PARTY titleOnlyBorrowerParty = this.CreateParty_TitleOnly(borrower, titleOnlyBorrowerPartyCount++, GetNextSequenceNumber(partyList));
                    partyList.Add(titleOnlyBorrowerParty);

                    partyList.Add(this.CreateParty_AttorneyInFact(borrower.POA, titleOnlyBorrowerParty.Roles.RoleList.Count > 0 ? titleOnlyBorrowerParty.Roles.RoleList[0].XlinkLabel : string.Empty, GetNextSequenceNumber(partyList)));
                    borrowerNames.Add(borrower.FullName);

                    if (borrower.AssociatedApplicationId != Guid.Empty && borrowersByApplicationId.ContainsKey(borrower.AssociatedApplicationId))
                    {
                        this.ExportData.CreateRelationship(
                            ArcroleVerbPhrase.SharesSufficientAssetsAndLiabilitiesWith,
                            "ROLE",
                            "ROLE",
                            borrowersByApplicationId[borrower.AssociatedApplicationId],
                            titleOnlyBorrowerParty.Roles.RoleList[0].XlinkLabel);
                    }
                }
            }
        }

        /// <summary>
        /// Creates a PARTY container representing a title-only borrower.
        /// </summary>
        /// <param name="borrower">The borrower.</param>
        /// <param name="labelCounter">The counter for this party. For use for the label.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_TitleOnly(TitleBorrower borrower, int labelCounter, int sequenceNumber)
        {
            var party = new PARTY();
            string baseLabel = "TitleOnlyBorrower" + labelCounter;

            party.Roles = new ROLES();
            party.Roles.RoleList.Add(this.CreateRole_TitleOnly(borrower, baseLabel, GetNextSequenceNumber(party.Roles.RoleList)));

            foreach (var trustee in this.DataLoan.sTrustCollection.Trustees)
            {
                if (string.Equals(borrower.FullName, trustee.Name, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(borrower.FirstNm + " " + borrower.LastNm, trustee.Name, StringComparison.OrdinalIgnoreCase))
                {
                    string label = baseLabel + "_Trustee";
                    party.Roles.RoleList.Add(CreateRole_Trustee(this.ExportData, trustee, label, GetNextSequenceNumber(party.Roles.RoleList)));
                }
            }

            foreach (var settlor in this.DataLoan.sTrustCollection.Trustors)
            {
                if (string.Equals(borrower.FullName, settlor.Name, StringComparison.OrdinalIgnoreCase) ||
                    string.Equals(borrower.FirstNm + " " + borrower.LastNm, settlor.Name, StringComparison.OrdinalIgnoreCase))
                {
                    string label = baseLabel + "_Settlor";
                    party.Roles.RoleList.Add(CreateRole_Settlor(this.ExportData, label, GetNextSequenceNumber(party.Roles.RoleList)));
                }
            }

            party.Addresses = new ADDRESSES();
            party.Addresses.AddressList.Add(
                CreateAddress(
                borrower.Address,
                borrower.City,
                borrower.State,
                borrower.Zip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(borrower.State),
                AddressBase.Mailing,
                GetNextSequenceNumber(party.Addresses.AddressList)));

            party.Individual = new INDIVIDUAL();
            party.Individual.Name = CreateName(borrower.FirstNm, borrower.MidNm, borrower.LastNm, string.Empty, borrower.FullName);
            party.Individual.ContactPoints = CreateContactPoints(borrower.HPhone, string.Empty, borrower.Email, ContactPointRoleBase.Blank);
            party.Individual.Aliases = new ALIASES();

            party.TaxpayerIdentifiers = CreateTaxpayerIdentifiers(borrower.SSNForExport, TaxpayerIdentifierBase.SocialSecurityNumber);
            party.SequenceNumber = sequenceNumber;

            foreach (string alias in borrower.Aliases)
            {
                if (string.IsNullOrEmpty(alias))
                {
                    continue;
                }
                else
                {
                    party.Individual.Aliases.AliasList.Add(CreateAlias(alias, GetNextSequenceNumber(party.Individual.Aliases.AliasList)));
                }
            }

            return party;
        }

        /// <summary>
        /// Creates a borrower role for a Title-Only Borrower.
        /// </summary>
        /// <param name="borrower">The Title-Only Borrower.</param>
        /// <param name="label">The role label to use.</param>
        /// <param name="sequenceNumber">The sequence number of the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_TitleOnly(TitleBorrower borrower, string label, int sequenceNumber)
        {
            var role = new ROLE();
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.TitleHolder, string.Empty);
            role.Borrower = new BORROWER();
            role.Borrower.BorrowerDetail = new BORROWER_DETAIL();
            role.Borrower.BorrowerDetail.BorrowerRelationshipTitleType = borrower.RelationshipTitleT == E_aRelationshipTitleT.LeaveBlank ? null : ToMismoEnum(GetBorrowerRelationshipTitleBaseValue(borrower.RelationshipTitleT), isSensitiveData: false);
            role.Borrower.BorrowerDetail.BorrowerBirthDate = ToMismoDate(this.DataLoan.m_convertLos.ToDateTimeString(borrower.Dob), isSensitiveData: true);

            var partyRoleIdentifier = new PARTY_ROLE_IDENTIFIER();
            partyRoleIdentifier.SequenceNumber = 1;
            partyRoleIdentifier.PartyRoleIdentifier = ToMismoIdentifier(borrower.SSNForExport, isSensitiveData: true);
            role.PartyRoleIdentifiers = new PARTY_ROLE_IDENTIFIERS();
            role.PartyRoleIdentifiers.PartyRoleIdentifierList.Add(partyRoleIdentifier);

            if (role.Borrower.BorrowerDetail.BorrowerRelationshipTitleType != null && role.Borrower.BorrowerDetail.BorrowerRelationshipTitleType.EnumValue == BorrowerRelationshipTitleBase.Other)
            {
                role.Borrower.BorrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = ToMismoString(CAppBase.aRelationshipTitleT_Map_rep(borrower.RelationshipTitleT));
            }
            else
            {
                role.Borrower.BorrowerDetail.BorrowerRelationshipTitleTypeOtherDescription = ToMismoString(borrower.RelationshipTitleTOtherDesc);
            }

            role.SequenceNumber = sequenceNumber;
            role.XlinkLabel = this.ExportData.GeneratePartyLabel(label, EntityType.Individual);

            return role;
        }

        /// <summary>
        /// Creates the parties for agents.
        /// </summary>
        /// <param name="partyList">The party list to populate.</param>
        private void CreateAgentParties(List<PARTY> partyList)
        {
            List<Guid> agentsToExclude = this.CompileExcludedAgents();
            for (int i = 0; i < this.DataLoan.GetAgentRecordCount(); i++)
            {
                CAgentFields agent = this.DataLoan.GetAgentFields(i);

                if (agentsToExclude.Contains(agent.RecordId))
                {
                    continue;
                }

                var individualAgent = this.CreateParty(agent, EntityType.Individual, GetNextSequenceNumber(partyList));
                partyList.Add(individualAgent);

                var legalEntityAgent = this.CreateParty(agent, EntityType.LegalEntity, GetNextSequenceNumber(partyList));
                partyList.Add(legalEntityAgent);

                if (individualAgent != null && legalEntityAgent != null)
                {
                    this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualAgent.Roles.RoleList[0].XlinkLabel, legalEntityAgent.Roles.RoleList[0].XlinkLabel);
                }
            }

            foreach (var supervisor in this.ExportData.LabelManager.PartyLabels.RetrieveLabels("AppraiserSupervisor", EntityType.Individual))
            {
                foreach (var appraiser in this.ExportData.LabelManager.PartyLabels.RetrieveLabels("Appraiser", EntityType.Individual))
                {
                    this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsSupervisedBy, "ROLE", "ROLE", appraiser, supervisor);
                }
            }
        }

        /// <summary>
        /// Creates a PARTY container holding agent information if such an agent exists on the loan file.
        /// </summary>
        /// <param name="agent">Agent role from which party information is populated.</param>
        /// <param name="entityType">Indicates whether the agent is an individual or legal entity.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>PARTY container with agent information if specified agent type exists on loan file, otherwise null.</returns>
        private PARTY CreateParty(CAgentFields agent, EntityType entityType, int sequenceNumber)
        {
            // We allow AgentName to be blank if we're creating an Individual of role Lender because we can use the field sEmployeeLoanRepName.
            if (!agent.IsValid ||
                (string.IsNullOrEmpty(agent.AgentName) && entityType == EntityType.Individual && agent.AgentRoleT != E_AgentRoleT.Lender) ||
                (string.IsNullOrEmpty(agent.CompanyName) && entityType == EntityType.LegalEntity))
            {
                return null;
            }

            var party = new PARTY();
            party.Addresses = CreateAddresses(agent);
            party.Roles = this.CreateRoles(agent, entityType);
            party.SequenceNumber = sequenceNumber;

            if (entityType == EntityType.Individual)
            {
                party.Individual = this.CreateIndividual(agent, ContactPointRoleBase.Work);
            }
            else
            {
                party.LegalEntity = CreateLegalEntity(agent, agent.CompanyName, ContactPointRoleBase.Work);
                party.TaxpayerIdentifiers = CreateTaxpayerIdentifiers(agent.TaxId, TaxpayerIdentifierBase.EmployerIdentificationNumber);
            }

            party.Extension = CommonBuilder.CreatePartyExtension(agent, entityType);

            return party;
        }

        /// <summary>
        /// Creates ROLES container with one ROLE sub-container populated with specified agent role type information.
        /// </summary>
        /// <param name="agent">The agent object.</param>
        /// <param name="entityType">Indicates whether the agent is an individual or legal entity.</param>
        /// <returns>Returns ROLES container with one ROLE sub-container populated by specified agent role type information.</returns>
        private ROLES CreateRoles(CAgentFields agent, EntityType entityType)
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole(agent, entityType, GetNextSequenceNumber(roles.RoleList)));

            return roles;
        }

        /// <summary>
        /// Creates ROLE container with agent information populated by agent role type.
        /// </summary>
        /// <param name="agent">The agent object.</param>
        /// <param name="entityType">Indicates whether the agent is an individual or legal entity.</param>
        /// <param name="sequenceNumber">The sequence number for the ROLE container.</param>
        /// <returns>Returns ROLE container with agent information populated by agent role type.</returns>
        private ROLE CreateRole(CAgentFields agent, EntityType entityType, int sequenceNumber)
        {
            var role = new ROLE();
            role.SequenceNumber = sequenceNumber;
            E_AgentRoleT agentRole = agent.AgentRoleT;

            if (entityType == EntityType.Individual)
            {
                role.Licenses = CommonBuilder.CreateLicenses(agent.LicenseNumOfAgent, agent.LoanOriginatorIdentifier, this.DataLoan.sSpState, this.DataLoan.sLT);
            }
            else if (entityType == EntityType.LegalEntity && agentRole == E_AgentRoleT.Lender)
            {
                role.Licenses = this.CreateLicenses(agent.LicenseNumOfCompany, agent.CompanyLoanOriginatorIdentifier, this.DataLoan.sVALenderIdCode);
            }
            else
            {
                role.Licenses = CommonBuilder.CreateLicenses(agent.LicenseNumOfCompany, agent.CompanyLoanOriginatorIdentifier, this.DataLoan.sSpState, this.DataLoan.sLT);
            }

            string otherDescription;
            var partyRoleType = GetPartyRoleBaseValue(agent.AgentRoleT, agent.OtherAgentRoleTDesc, entityType, this.DataLoan.sGfeIsTPOTransaction, this.DataLoan.sBranchChannelT, out otherDescription);
            role.RoleDetail = CreateRoleDetail(partyRoleType, otherDescription);

            if (partyRoleType == PartyRoleBase.ClosingAgent)
            {
                role.ClosingAgent = CreateClosingAgent(ClosingAgentBase.EscrowCompany, string.Empty);
            }
            else if (partyRoleType == PartyRoleBase.TitleCompany)
            {
                role.ClosingAgent = CreateClosingAgent(ClosingAgentBase.TitleCompany, string.Empty);
            }
            else if (partyRoleType == PartyRoleBase.RealEstateAgent)
            {
                role.RealEstateAgent = this.CreateRealEstateAgent(agent.AgentRoleT);
            }
            else if (partyRoleType == PartyRoleBase.Trustee)
            {
                role.Trustee = this.CreateTrustee(entityType);
            }

            string roleString = agent.AgentRoleT != E_AgentRoleT.Other ? agent.AgentRoleT.ToString() : agent.OtherAgentRoleTDesc;
            role.XlinkLabel = this.ExportData.GeneratePartyLabel(roleString, entityType);

            return role;
        }

        /// <summary>
        /// Creates a LICENSES container with the given state license, Nationwide Mortgage Licensing System license, and VA license.
        /// </summary>
        /// <param name="licenseNumber">The state license.</param>
        /// <param name="nmlsLicense">The NMLS license.</param>
        /// <param name="licenseVAFHA">The VA license.</param>
        /// <returns>A LICENSES container with the given state license, Nationwide Mortgage Licensing System license, and VA license.</returns>
        private LICENSES CreateLicenses(string licenseNumber, string nmlsLicense, string licenseVAFHA)
        {
            LICENSES licenses = CommonBuilder.CreateLicenses(licenseNumber, nmlsLicense, this.DataLoan.sSpState, this.DataLoan.sLT);
            if (this.DataLoan.sLT == E_sLT.VA || this.DataLoan.sLT == E_sLT.FHA)
            {
                licenses.LicenseList.Add(CreateLicense(licenseVAFHA, nmls: false, fha_va: true, state: this.DataLoan.sSpState, loanType: this.DataLoan.sLT, sequence: GetNextSequenceNumber(licenses.LicenseList)));
            }

            return licenses;
        }

        /// <summary>
        /// Creates a REAL_ESTATE_AGENT container based on the given agent type.
        /// </summary>
        /// <param name="agentType">The agent type.</param>
        /// <returns>A REAL_ESTATE_AGENT container.</returns>
        private REAL_ESTATE_AGENT CreateRealEstateAgent(E_AgentRoleT agentType)
        {
            var agent = new REAL_ESTATE_AGENT();
            var realEstateAgentType = GetRealEstateAgentBase(agentType);
            agent.RealEstateAgentType = realEstateAgentType == RealEstateAgentBase.Blank ? null : ToMismoEnum(realEstateAgentType, isSensitiveData: false);

            if (agent.RealEstateAgentType == null)
            {
                return null;
            }

            if (agent.RealEstateAgentType?.IsSetToOther ?? false)
            {
                agent.RealEstateAgentTypeOtherDescription = ToMismoString(agentType);
            }

            return agent;
        }

        /// <summary>
        /// Creates a container holding information on a trustee.
        /// </summary>
        /// <param name="entityType">Indicates whether the trustee is an individual or legal entity.</param>
        /// <returns>A TRUSTEE container.</returns>
        private TRUSTEE CreateTrustee(EntityType entityType)
        {
            var trusteeContainer = new TRUSTEE();
            trusteeContainer.DeedOfTrustTrusteeIndicator = ToMismoIndicator(entityType == EntityType.LegalEntity);

            return trusteeContainer;
        }

        /// <summary>
        /// Creates a container with information on an individual agent.
        /// </summary>
        /// <param name="agent">The agent to be converted to MISMO.</param>
        /// <param name="contactType">The type of the agent's contact info (Work, Home, etc.).</param>
        /// <returns>An INDIVIDUAL container.</returns>
        private INDIVIDUAL CreateIndividual(CAgentFields agent, ContactPointRoleBase contactType)
        {
            var individual = new INDIVIDUAL();

            var name = agent.AgentName;
            if (agent.AgentRoleT == E_AgentRoleT.Lender && string.IsNullOrEmpty(agent.AgentName))
            {
                name = this.DataLoan.sEmployeeLoanRepName;
            }

            individual.Name = CreateName(name, isLegalEntity: false);

            // Special case for Trustee
            if (agent.AgentRoleT == E_AgentRoleT.Trustee)
            {
                contactType = ContactPointRoleBase.Blank;
            }

            individual.ContactPoints = CreateContactPoints(agent, contactType, EntityType.Individual);

            return individual;
        }

        /// <summary>
        /// Creates the parties for SSPs.
        /// </summary>
        /// <param name="partyList">The party list to populate.</param>
        private void CreateSspParties(List<PARTY> partyList)
        {
            var feeTypes = this.DataLoan.sAvailableSettlementServiceProviders.GetFeeTypeIds();
            var feesByProvider = new Dictionary<Guid, Tuple<SettlementServiceProvider, List<Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>>>>();
            foreach (var feeType in feeTypes)
            {
                var providersForFee = this.DataLoan.sAvailableSettlementServiceProviders.GetProvidersForFeeTypeId(feeType);
                foreach (var providerForFee in providersForFee)
                {
                    // For any providers that are not loan contacts and don't have their own agent ID, we will assign a
                    // new GUID to prevent all the empty GUIDs from deduping against each other.
                    if (providerForFee.AgentId == Guid.Empty)
                    {
                        providerForFee.AgentId = Guid.NewGuid();
                    }

                    if (!feesByProvider.ContainsKey(providerForFee.AgentId))
                    {
                        feesByProvider[providerForFee.AgentId] = new Tuple<SettlementServiceProvider, List<Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>>>(providerForFee, new List<Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>>());
                    }

                    var fee = this.DataLoan.sClosingCostSet.FindFeeByTypeId(feeType);
                    feesByProvider[providerForFee.AgentId].Item2.Add(new Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>(fee.Description, providerForFee.EstimatedCostAmount, fee.MismoFeeT));
                }
            }

            foreach (var kvp in feesByProvider)
            {
                partyList.Add(this.CreateParty_SSP(kvp.Value.Item1, kvp.Value.Item2, GetNextSequenceNumber(partyList)));
            }
        }

        /// <summary>
        /// Creates a container representing a settlement service provider.
        /// </summary>
        /// <param name="provider">The service provider.</param>
        /// <param name="fees">Fees associated with the service provider.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_SSP(SettlementServiceProvider provider, List<Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>> fees, int sequenceNumber)
        {
            var party = new PARTY();
            party.SequenceNumber = sequenceNumber;
            party.Roles = this.CreateRoles_SSP(provider, fees);

            var agent = this.DataLoan.GetAgentFields(provider.AgentId);

            // If the agent comes back as a new record, that indicates that the AgentId does not
            // correspond to an agent for this loan, and we must default to using the SSP data.
            if (agent.IsNewRecord)
            {
                party.Addresses = CreateAddresses(provider);
                party.LegalEntity = CreateLegalEntity(
                    provider.CompanyName,
                    provider.Phone,
                    string.Empty,
                    string.Empty,
                    CreateName(provider.ContactName, isLegalEntity: false),
                    ContactPointRoleBase.Work);
            }
            else
            {
                party.Addresses = CreateAddresses(agent);
                party.LegalEntity = CreateLegalEntity(
                    agent.CompanyName,
                    agent.Phone,
                    string.Empty,
                    string.Empty,
                    CreateName(agent.AgentName, isLegalEntity: false),
                    ContactPointRoleBase.Work);
            }

            return party;
        }

        /// <summary>
        /// Creates a container holding roles for a settlement service provider.
        /// </summary>
        /// <param name="provider">The service provider.</param>
        /// <param name="fees">Fees associated with the service provider.</param>
        /// <returns>A ROLES container.</returns>
        private ROLES CreateRoles_SSP(SettlementServiceProvider provider, List<Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>> fees)
        {
            var roles = new ROLES();

            string otherDescription = string.Empty;
            var partyRoleType = GetPartyRoleBaseValue(provider.ServiceT, provider.ServiceTDesc, EntityType.LegalEntity, this.DataLoan.sGfeIsTPOTransaction, this.DataLoan.sBranchChannelT, out otherDescription);

            roles.RoleList.Add(this.CreateRole(partyRoleType, GetNextSequenceNumber(roles.RoleList), otherDescription));
            roles.RoleList.Add(this.CreateRole_SSP(fees, sequenceNumber: GetNextSequenceNumber(roles.RoleList)));

            return roles;
        }

        /// <summary>
        /// Creates a role for a settlement service provider.
        /// </summary>
        /// <param name="fees">Fees associated with the service provider.</param>
        /// <param name="sequenceNumber">The sequence number for the ROLE element.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_SSP(List<Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>> fees, int sequenceNumber)
        {
            var role = new ROLE();
            role.SequenceNumber = sequenceNumber;

            var roleType = PartyRoleBase.ServiceProvider;
            role.RoleDetail = CreateRoleDetail(roleType, string.Empty);
            role.XlinkLabel = this.ExportData.GeneratePartyLabel(roleType, EntityType.LegalEntity);

            if (fees != null && fees.Count() > 0)
            {
                role.ServiceProvider = this.CreateServiceProvider(fees);
            }

            return role;
        }

        /// <summary>
        /// Creates a SERVICE_PROVIDER container.
        /// </summary>
        /// <param name="fees">The fees to use.</param>
        /// <returns>A SERVICE_PROVIDER container.</returns>
        private SERVICE_PROVIDER CreateServiceProvider(List<Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>> fees)
        {
            SERVICE_PROVIDER serviceProvider = new SERVICE_PROVIDER();
            serviceProvider.ProvidedServices = this.CreateProvidedServices(fees);
            return serviceProvider;
        }

        /// <summary>
        /// CREATES A PROVIDED_SERVICES container.
        /// </summary>
        /// <param name="fees">The fees to use.</param>
        /// <returns>The PROVIDED_SERVICES container.</returns>
        private PROVIDED_SERVICES CreateProvidedServices(List<Tuple<string, decimal, E_ClosingCostFeeMismoFeeT>> fees)
        {
            PROVIDED_SERVICES providedServices = new PROVIDED_SERVICES();

            foreach (var fee in fees)
            {
                providedServices.ProvidedServiceList.Add(
                    this.CreateProvidedService(
                        canShop: true,
                        description: fee.Item1,
                        amount: this.DataLoan.m_convertLos.ToDecimalString(fee.Item2, FormatDirection.ToRep),
                        feeType: fee.Item3,
                        sequence: GetNextSequenceNumber(providedServices.ProvidedServiceList)));
            }

            return providedServices;
        }

        /// <summary>
        /// CREATES A PROVIDED_SERVICE container.
        /// </summary>
        /// <param name="canShop">Indicates whether the service can be shopped for.</param>
        /// <param name="description">A description of the service.</param>
        /// <param name="amount">The dollar amount of the service.</param>
        /// <param name="feeType">The associated fee's mismo type.</param>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>The PROVIDED_SERVICE container.</returns>
        private PROVIDED_SERVICE CreateProvidedService(bool canShop, string description, string amount, E_ClosingCostFeeMismoFeeT feeType, int sequence)
        {
            PROVIDED_SERVICE providedService = new PROVIDED_SERVICE();
            providedService.SequenceNumber = sequence;
            providedService.ProvidedServiceBorrowerCanShopForIndicator = ToMismoIndicator(canShop);
            providedService.ProvidedServiceDescription = ToMismoString(description);
            providedService.ProvidedServiceEstimatedCostAmount = ToMismoAmount(amount);

            string otherFeeDescription;
            var feeBase = GetFeeBaseValue(feeType, description, out otherFeeDescription);
            providedService.FeeType = ToMismoEnum(feeBase, isSensitiveData: false);
            if (feeBase == FeeBase.Other)
            {
                providedService.FeeTypeOtherDescription = ToMismoString(otherFeeDescription);
            }

            return providedService;
        }

        /// <summary>
        /// Creates the trustee parties.
        /// </summary>
        /// <param name="partyList">The running party list.</param>
        /// <param name="borrowerNames">The list of borrower names.</param>
        private void CreateTrusteeParties(List<PARTY> partyList, List<string> borrowerNames)
        {
            // Trust Parties
            partyList.Add(this.CreateParty_Trust(GetNextSequenceNumber(partyList)));
            foreach (var settlor in this.DataLoan.sTrustCollection.Trustors)
            {
                if (!borrowerNames.Contains(settlor.Name) && !string.IsNullOrWhiteSpace(settlor.Name))
                {
                    partyList.Add(this.CreateParty_Settlor(settlor, GetNextSequenceNumber(partyList)));
                }
            }

            foreach (var trustee in this.DataLoan.sTrustCollection.Trustees)
            {
                if (!borrowerNames.Contains(trustee.Name) && !string.IsNullOrWhiteSpace(trustee.Name))
                {
                    partyList.Add(this.CreateParty_Trustee(trustee, GetNextSequenceNumber(partyList)));
                }
            }

            foreach (var trust in this.ExportData.LabelManager.PartyLabels.RetrieveLabels("Trust", EntityType.LegalEntity))
            {
                foreach (var settlor in this.ExportData.LabelManager.PartyLabels.RetrieveLabels("Settlor", EntityType.Individual))
                {
                    this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsSettlorFor, "ROLE", "ROLE", settlor, trust);
                }

                foreach (var trustee in this.ExportData.LabelManager.PartyLabels.RetrieveLabels("Trustee", EntityType.Individual))
                {
                    this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsTrusteeFor, "ROLE", "ROLE", trustee, trust);
                }
            }
        }

        /// <summary>
        /// Creates a container representing a trust.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_Trust(int sequenceNumber)
        {
            if (string.IsNullOrEmpty(this.DataLoan.sTrustName))
            {
                return null;
            }

            var party = new PARTY();
            party.LegalEntity = new LEGAL_ENTITY();
            party.LegalEntity.LegalEntityDetail = new LEGAL_ENTITY_DETAIL();
            party.LegalEntity.LegalEntityDetail.FullName = ToMismoString(this.DataLoan.sTrustName);
            party.SequenceNumber = sequenceNumber;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.Trust = new TRUST();
            role.Trust.TrustEstablishedDate = ToMismoDate(this.DataLoan.sTrustAgreementD_rep);
            role.Trust.TrustEstablishedStateName = ToMismoString(this.DataLoan.sTrustState);
            role.Trust.TrustClassificationType = ToMismoEnum(TrustClassificationBase.LivingTrust, isSensitiveData: false);

            var roleDetail = new ROLE_DETAIL();
            roleDetail.PartyRoleType = ToMismoEnum(PartyRoleBase.Trust, isSensitiveData: false);
            role.RoleDetail = roleDetail;

            role.XlinkLabel = this.ExportData.GeneratePartyLabel("Trust", EntityType.LegalEntity);

            party.Roles = new ROLES();
            party.Roles.RoleList.Add(role);

            return party;
        }

        /// <summary>
        /// Creates a container representing a trust settlor.
        /// </summary>
        /// <param name="settlor">The settlor information.</param>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_Settlor(TrustCollection.Trustor settlor, int sequence)
        {
            var party = new PARTY();
            party.Individual = new INDIVIDUAL();
            party.Individual.Name = CreateName(settlor.Name, isLegalEntity: false);
            party.SequenceNumber = sequence;

            party.Roles = new ROLES();
            string label = "Settlor" + sequence;
            party.Roles.RoleList.Add(CreateRole_Settlor(this.ExportData, label, GetNextSequenceNumber(party.Roles.RoleList)));

            return party;
        }

        /// <summary>
        /// Creates a container representing a trustee.
        /// </summary>
        /// <param name="trustee">The trustee information.</param>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_Trustee(TrustCollection.Trustee trustee, int sequence)
        {
            var party = new PARTY();
            party.Individual = this.CreateIndividual(trustee);

            party.Addresses = new ADDRESSES();
            party.Addresses.AddressList.Add(
                CreateAddress(
                trustee.Address.StreetAddress,
                trustee.Address.City,
                trustee.Address.State,
                trustee.Address.PostalCode,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(trustee.Address.State),
                AddressBase.Blank,
                GetNextSequenceNumber(party.Addresses.AddressList)));

            party.SequenceNumber = sequence;

            party.Roles = new ROLES();
            string label = "Trustee" + sequence;
            party.Roles.RoleList.Add(CreateRole_Trustee(this.ExportData, trustee, label, GetNextSequenceNumber(party.Roles.RoleList)));

            return party;
        }

        /// <summary>
        /// Creates a container representing an individual trustee.
        /// </summary>
        /// <param name="trustee">The data for the trustee.</param>
        /// <returns>An INDIVIDUAL container.</returns>
        private INDIVIDUAL CreateIndividual(TrustCollection.Trustee trustee)
        {
            var individual = new INDIVIDUAL();
            individual.Name = CreateName(trustee.Name, isLegalEntity: false);
            individual.ContactPoints = CreateContactPoints(trustee.Phone, string.Empty, string.Empty, ContactPointRoleBase.Blank);

            return individual;
        }

        /// <summary>
        /// Creates a party container with information regarding the lender.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A PARTY container with lender information.</returns>
        private PARTY CreateParty_Lender(int sequenceNumber)
        {
            PARTY lenderParty = new PARTY();
            lenderParty.SequenceNumber = sequenceNumber;
            lenderParty.Addresses = CreateAddresses(this.DataLoan.BrokerDB.Address, AddressBase.Current, string.Empty, string.Empty);

            NAME contactName = null;
            string contactEmail = string.Empty;
            if (this.ExportData.User != null)
            {
                contactName = CreateName(this.ExportData.User.FirstName, this.ExportData.User.MiddleName, this.ExportData.User.LastName, this.ExportData.User.Suffix, this.ExportData.User.FullName);
                contactEmail = this.ExportData.User.Email;
            }

            lenderParty.Extension = new PARTY_EXTENSION
            {
                Other = new LQB_PARTY_EXTENSION
                {
                    AgentId = ToMismoIdentifier(this.DataLoan.sBrokerId.ToString())
                }
            };

            lenderParty.LegalEntity = CreateLegalEntity(this.DataLoan.BrokerDB.Name, this.DataLoan.BrokerDB.Phone, this.DataLoan.BrokerDB.Fax, contactEmail, contactName, ContactPointRoleBase.Work);
            lenderParty.Roles = this.CreateRoles_RequestingParty();

            return lenderParty;
        }

        /// <summary>
        /// Creates a ROLES container with one ROLE sub-container populated with information pertaining to the lender as the requesting party.
        /// </summary>
        /// <returns>A ROLES container with one ROLE sub-container populated with information pertaining to the lender.</returns>
        private ROLES CreateRoles_RequestingParty()
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole_RequestingParty(GetNextSequenceNumber(roles.RoleList)));

            return roles;
        }

        /// <summary>
        /// Creates a role container representing the requesting party.
        /// </summary>
        /// <param name="sequence">The sequence number of the role among the list of roles.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_RequestingParty(int sequence)
        {
            var role = new ROLE();

            role.RequestingParty = this.CreateRequestingParty();
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.RequestingParty, string.Empty);
            role.XlinkLabel = this.ExportData.GeneratePartyLabel(PartyRoleBase.RequestingParty, EntityType.LegalEntity);
            role.SequenceNumber = sequence;

            return role;
        }

        /// <summary>
        /// Creates a container with information pertaining to the person placing the order, or making the request of the vendor to receive the MISMO message.
        /// </summary>
        /// <returns>A REQUESTING_PARTY container.</returns>
        private REQUESTING_PARTY CreateRequestingParty()
        {
            var requestor = new REQUESTING_PARTY();
            requestor.InternalAccountIdentifier = ToMismoIdentifier(this.ExportData.Options.VendorAccountId, isSensitiveData: true);

            if (this.ExportData.User != null)
            {
                requestor.RequestedByName = ToMismoString(this.ExportData.User.FullName);
            }

            return requestor;
        }

        /// <summary>
        /// Creates a party container with information regarding LendingQB.
        /// </summary>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>A PARTY container with LQB information.</returns>
        private PARTY CreateParty_LQB(int sequence)
        {
            PARTY lqbParty = new PARTY();
            lqbParty.SequenceNumber = sequence;

            CommonLib.Address lqbAddress = Constants.ConstApp.GetLendingQBAddress();
            lqbParty.Addresses = CreateAddresses(lqbAddress, AddressBase.CorporateHeadquarters, Constants.ConstApp.lqbCounty, this.ExportData.StatesAndTerritories.GetFIPSCodeForCounty(lqbAddress.State, Constants.ConstApp.lqbCounty));

            NAME lqbName = new NAME();
            lqbName.FullName = ToMismoString("LendingQB Support");
            lqbName.FunctionalTitleDescription = ToMismoString("Technical Support");

            lqbParty.LegalEntity = CreateLegalEntity("LendingQB", Constants.ConstApp.lqbPhoneNumber, string.Empty, Constants.ConstStage.LQBIntegrationEmail, lqbName, ContactPointRoleBase.Work);
            lqbParty.Roles = this.CreateRoles_LQB();

            return lqbParty;
        }

        /// <summary>
        /// Creates a ROLES container with roles pertaining to LendingQB.
        /// </summary>
        /// <returns>A ROLES container with roles pertaining to LendingQB.</returns>
        private ROLES CreateRoles_LQB()
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole_SubmittingParty(GetNextSequenceNumber(roles.RoleList)));
            roles.RoleList.Add(this.CreateRole_ReturnTo(GetNextSequenceNumber(roles.RoleList)));

            return roles;
        }

        /// <summary>
        /// Creates a role container representing the submitting party role.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number for the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_SubmittingParty(int sequenceNumber)
        {
            var role = new ROLE();

            role.SubmittingParty = this.CreateSubmittingParty();
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.SubmittingParty, string.Empty);
            role.XlinkLabel = this.ExportData.GeneratePartyLabel(PartyRoleBase.SubmittingParty, EntityType.LegalEntity);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a container with information pertaining to the entity submitting the order/message to the vendor, i.e. LendingQB.
        /// </summary>
        /// <returns>A SUBMITTING_PARTY container.</returns>
        private SUBMITTING_PARTY CreateSubmittingParty()
        {
            var submitter = new SUBMITTING_PARTY();
            submitter.SubmittingPartySequenceNumber = ToMismoSequenceNumber(1);

            if (!string.IsNullOrEmpty(this.ExportData.Options.TransactionId))
            {
                submitter.SubmittingPartyTransactionIdentifier = ToMismoIdentifier(this.ExportData.Options.TransactionId);
            }

            return submitter;
        }

        /// <summary>
        /// Creates a role container representing the return-to role.
        /// </summary>
        /// <param name="sequenceNumber">The sequence number for the role.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_ReturnTo(int sequenceNumber)
        {
            var role = new ROLE();

            role.ReturnTo = this.CreateReturnTo();
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.ReturnTo, string.Empty);
            role.XlinkLabel = this.ExportData.GeneratePartyLabel(PartyRoleBase.ReturnTo, EntityType.LegalEntity);
            role.SequenceNumber = sequenceNumber;

            return role;
        }

        /// <summary>
        /// Creates a RETURN_TO with the preferred responses to the MISMO message.
        /// </summary>
        /// <returns>A RETURN_TO container.</returns>
        private RETURN_TO CreateReturnTo()
        {
            var returnTo = new RETURN_TO();
            returnTo.PreferredResponses = this.CreatePreferredResponses();

            return returnTo;
        }

        /// <summary>
        /// Creates a PREFERRED_RESPONSES container with a list of preferred responses.
        /// </summary>
        /// <returns>A PREFERRED_RESPONSES container.</returns>
        private PREFERRED_RESPONSES CreatePreferredResponses()
        {
            var responses = new PREFERRED_RESPONSES();
            responses.PreferredResponseList.Add(this.CreatePreferredResponse(GetNextSequenceNumber(responses.PreferredResponseList)));

            return responses;
        }

        /// <summary>
        /// Creates a PREFERRED_RESPONSE container with information regarding the intended response, e.g. XML format via HTTPS.
        /// </summary>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>A PREFERRED_RESPONSE container.</returns>
        private PREFERRED_RESPONSE CreatePreferredResponse(int sequence)
        {
            var response = new PREFERRED_RESPONSE();
            response.PreferredResponseDestinationDescription = ToMismoString("Direct the response to LendingQB.");
            response.PreferredResponseFormatType = ToMismoEnum(PreferredResponseFormatBase.XML, false);
            response.PreferredResponseMethodType = ToMismoEnum(PreferredResponseMethodBase.HTTPS, false);
            response.SequenceNumber = sequence;

            return response;
        }

        /// <summary>
        /// Creates the party for the branch.
        /// </summary>
        /// <param name="partyList">The running party list.</param>
        private void CreateBranchParty(List<PARTY> partyList)
        {
            Admin.BranchDB lenderBranch = new BranchDB(this.DataLoan.sBranchId, this.DataLoan.sBrokerId);
            lenderBranch.Retrieve();
            partyList.Add(this.CreateParty(lenderBranch, GetNextSequenceNumber(partyList)));
        }

        /// <summary>
        /// Creates PARTY container with lender branch information.
        /// </summary>
        /// <param name="lenderBranch">The lender's branch used to populate PARTY container.</param>
        /// <param name="sequence">The sequence.</param>
        /// <returns>Returns PARTY container with lender branch information.</returns>
        private PARTY CreateParty(BranchDB lenderBranch, int sequence)
        {
            PARTY party = new PARTY();
            party.Addresses = CreateAddresses(lenderBranch.Address, AddressBase.Current, string.Empty, string.Empty);
            party.LegalEntity = CreateLegalEntity(lenderBranch.DisplayNm, lenderBranch.Phone, lenderBranch.Fax, string.Empty, null, ContactPointRoleBase.Work);
            party.SequenceNumber = sequence;

            party.Roles = new ROLES();
            var role = new ROLE();
            role.SequenceNumber = 1;
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.LenderBranch, string.Empty);
            role.XlinkLabel = this.ExportData.GeneratePartyLabel("LenderBranch", EntityType.LegalEntity);

            party.Roles.RoleList.Add(role);

            party.Extension = this.CreatePartyExtension(lenderBranch);

            return party;
        }

        /// <summary>
        /// Creates a PARTY extension.
        /// </summary>
        /// <param name="lenderBranch">The lender branch data.</param>
        /// <returns>A PARTY extension.</returns>
        private PARTY_EXTENSION CreatePartyExtension(BranchDB lenderBranch)
        {
            var extension = new PARTY_EXTENSION();
            extension.Other = this.CreateLqbPartyExtension(lenderBranch);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary PARTY extension.
        /// </summary>
        /// <param name="lenderBranch">The lender branch data.</param>
        /// <returns>A proprietary PARTY extension.</returns>
        private LQB_PARTY_EXTENSION CreateLqbPartyExtension(BranchDB lenderBranch)
        {
            var extension = new LQB_PARTY_EXTENSION();
            extension.BranchCode = ToMismoCode(lenderBranch.BranchCode);

            return extension;
        }

        /// <summary>
        /// Creates the parties from preparers.
        /// </summary>
        /// <param name="partyList">The running party list.</param>
        private void CreatePreparerParties(List<PARTY> partyList)
        {
            var individualFHAAddendumSponsor = this.CreateParty_Preparer(this.DataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject), PartyRoleBase.FHASponsor, EntityType.Individual, GetNextSequenceNumber(partyList));
            partyList.Add(individualFHAAddendumSponsor);
            var legalEntityFHAAddendumSponsor = this.CreateParty_Preparer(this.DataLoan.GetPreparerOfForm(E_PreparerFormT.FHAAddendumSponsor, E_ReturnOptionIfNotExist.ReturnEmptyObject), PartyRoleBase.FHASponsor, EntityType.LegalEntity, GetNextSequenceNumber(partyList));
            partyList.Add(legalEntityFHAAddendumSponsor);

            if (individualFHAAddendumSponsor != null && legalEntityFHAAddendumSponsor != null)
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualFHAAddendumSponsor.Roles.RoleList[0].XlinkLabel, legalEntityFHAAddendumSponsor.Roles.RoleList[0].XlinkLabel);
            }

            var individualInterviewer = this.CreateParty_Preparer(this.DataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject), PartyRoleBase.Interviewer, EntityType.Individual, GetNextSequenceNumber(partyList));
            partyList.Add(individualInterviewer);
            var legalEntityInterviewer = this.CreateParty_Preparer(this.DataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject), PartyRoleBase.InterviewerEmployer, EntityType.LegalEntity, GetNextSequenceNumber(partyList));
            partyList.Add(legalEntityInterviewer);

            if (individualInterviewer != null && legalEntityInterviewer != null)
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualInterviewer.Roles.RoleList[0].XlinkLabel, legalEntityInterviewer.Roles.RoleList[0].XlinkLabel);

                if (individualInterviewer.Roles.RoleList.Count > 1 && legalEntityInterviewer.Roles.RoleList.Count > 1)
                {
                    this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", individualInterviewer.Roles.RoleList[1].XlinkLabel, legalEntityInterviewer.Roles.RoleList[1].XlinkLabel);
                }
            }
        }

        /// <summary>
        /// Creates a container representing a preparer.
        /// </summary>
        /// <param name="preparer">An object with data on the preparer.</param>
        /// <param name="roleType">The role type for the party object.</param>
        /// <param name="entityType">Indicates whether this party represents a person or legal entity.</param>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_Preparer(IPreparerFields preparer, PartyRoleBase roleType, EntityType entityType, int sequence)
        {
            if (preparer.IsEmpty)
            {
                return null;
            }

            var party = new PARTY();

            if (entityType == EntityType.Individual)
            {
                party.Individual = new INDIVIDUAL();
                party.Individual.Name = CreateName(preparer.PreparerName, isLegalEntity: false);

                party.Individual.ContactPoints = new CONTACT_POINTS();
                party.Individual.ContactPoints.ContactPointList.Add(CreateContactPoint_Phone(preparer.Phone, ContactPointRoleBase.Work, GetNextSequenceNumber(party.Individual.ContactPoints.ContactPointList)));
                party.Individual.ContactPoints.ContactPointList.Add(CreateContactPoint_Fax(preparer.FaxNum, ContactPointRoleBase.Work, GetNextSequenceNumber(party.Individual.ContactPoints.ContactPointList)));
                party.Individual.ContactPoints.ContactPointList.Add(CreateContactPoint_Email(preparer.EmailAddr, ContactPointRoleBase.Work, GetNextSequenceNumber(party.Individual.ContactPoints.ContactPointList)));
            }
            else
            {
                party.LegalEntity = new LEGAL_ENTITY();
                party.LegalEntity.LegalEntityDetail = new LEGAL_ENTITY_DETAIL();
                party.LegalEntity.LegalEntityDetail.FullName = ToMismoString(preparer.CompanyName);
            }

            party.Addresses = new ADDRESSES();
            party.Addresses.AddressList.Add(
                CreateAddress(
                preparer.StreetAddr,
                preparer.City,
                preparer.State,
                preparer.Zip,
                string.Empty,
                string.Empty,
                this.ExportData.StatesAndTerritories.GetStateName(preparer.State),
                AddressBase.Blank,
                GetNextSequenceNumber(party.Addresses.AddressList)));

            party.SequenceNumber = sequence;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.RoleDetail = new ROLE_DETAIL();
            role.RoleDetail.PartyRoleType = ToMismoEnum(roleType, isSensitiveData: false);

            if (entityType == EntityType.Individual)
            {
                role.Licenses = CommonBuilder.CreateLicenses(preparer.LicenseNumOfAgent, preparer.LoanOriginatorIdentifier, this.DataLoan.sSpState, this.DataLoan.sLT);
            }
            else
            {
                role.Licenses = CommonBuilder.CreateLicenses(preparer.LicenseNumOfCompany, preparer.CompanyLoanOriginatorIdentifier, this.DataLoan.sSpState, this.DataLoan.sLT);
            }

            party.Roles = new ROLES();
            party.Roles.RoleList.Add(role);

            if (roleType == PartyRoleBase.Interviewer || roleType == PartyRoleBase.InterviewerEmployer)
            {
                party.Roles.RoleList.Add(this.CreateRole_LoanOriginator(preparer, entityType, GetNextSequenceNumber(party.Roles.RoleList)));
            }

            role.XlinkLabel = this.ExportData.GeneratePartyLabel(GetXmlEnumName(preparer.AgentRoleT), entityType);

            return party;
        }

        /// <summary>
        /// Creates a role container representing the loan originator role.
        /// </summary>
        /// <param name="preparer">The 1003 preparer information.</param>
        /// <param name="entityType">The entity type for the associated party, i.e. individual or legal entity.</param>
        /// <param name="sequence">The sequence number of the role among the list of roles.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole_LoanOriginator(IPreparerFields preparer, EntityType entityType, int sequence)
        {
            var role = new ROLE();
            role.LoanOriginator = new LOAN_ORIGINATOR();
            role.LoanOriginator.LoanOriginatorType = ToMismoEnum(GetLoanOriginatorBaseValue(this.DataLoan.sBranchChannelT), isSensitiveData: false);
            role.SequenceNumber = sequence;

            string licenseNumber = entityType == EntityType.Individual ? preparer.LicenseNumOfAgent : preparer.LicenseNumOfCompany;
            string nmlsLicense = entityType == EntityType.Individual ? preparer.LoanOriginatorIdentifier : preparer.CompanyLoanOriginatorIdentifier;
            role.Licenses = CommonBuilder.CreateLicenses(licenseNumber, nmlsLicense, this.DataLoan.sSpState, this.DataLoan.sLT);

            PartyRoleBase roleBase = entityType == EntityType.Individual ? PartyRoleBase.LoanOriginator : PartyRoleBase.LoanOriginationCompany;
            role.RoleDetail = new ROLE_DETAIL();
            role.RoleDetail.PartyRoleType = ToMismoEnum(roleBase, isSensitiveData: false);

            role.XlinkLabel = this.ExportData.GeneratePartyLabel(roleBase, entityType);

            return role;
        }

        /// <summary>
        /// Creates a container representing a wind insurance company.
        /// </summary>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_WindInsurance(int sequence)
        {
            if (string.IsNullOrEmpty(this.DataLoan.sWindInsCompanyNm))
            {
                return null;
            }

            PARTY party = new PARTY();
            party.LegalEntity = CreateLegalEntity(this.DataLoan.sWindInsCompanyNm, string.Empty, string.Empty, string.Empty, null, ContactPointRoleBase.Blank);
            party.SequenceNumber = sequence;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.ClosingAgent, string.Empty);
            role.ClosingAgent = CreateClosingAgent(ClosingAgentBase.Other, "WindstormInsuranceAgent");
            role.XlinkLabel = this.ExportData.GeneratePartyLabel("WindInsuranceCompany", EntityType.LegalEntity);

            party.Roles = new ROLES();
            party.Roles.RoleList.Add(role);
            return party;
        }

        /// <summary>
        /// Creates a container representing a warehouse lender.
        /// </summary>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_WarehouseLender(int sequence)
        {
            if (this.DataLoan.sWarehouseLenderRolodexId < 0)
            {
                return null;
            }

            var warehouseLender = WarehouseLenderRolodexEntry.Get(this.DataLoan.sBrokerId, this.DataLoan.sWarehouseLenderRolodexId);
            var party = new PARTY();
            party.LegalEntity = CreateLegalEntity(warehouseLender.WarehouseLenderName, string.Empty, string.Empty, string.Empty, null, ContactPointRoleBase.Work);
            party.Roles = this.CreateRoles(PartyRoleBase.WarehouseLender);
            party.SequenceNumber = sequence;

            return party;
        }

        /// <summary>
        /// Creates PARTY container with loan investor.
        /// </summary>
        /// <param name="investorName">Name of loan investor.</param>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>Returns PARTY container with loan investor.</returns>
        private PARTY CreateParty(string investorName, int sequence)
        {
            PARTY party = new PARTY();
            party.LegalEntity = CreateLegalEntity(investorName, string.Empty, string.Empty, string.Empty, null, ContactPointRoleBase.Blank);
            party.SequenceNumber = sequence;

            var role = new ROLE();
            role.SequenceNumber = 1;
            role.RoleDetail = CreateRoleDetail(PartyRoleBase.Investor, string.Empty);
            role.XlinkLabel = this.ExportData.GeneratePartyLabel("Investor", EntityType.LegalEntity);

            party.Roles = new ROLES();
            party.Roles.RoleList.Add(role);

            return party;
        }

        /// <summary>
        /// Creates the investor parties.
        /// </summary>
        /// <param name="partyList">The running party list.</param>
        private void CreateInvestorParties(List<PARTY> partyList)
        {
            // Add investor parties from Investor Info page.
            InvestorRolodexEntry loanInvestorInfo = this.DataLoan.sInvestorInfo;

            partyList.Add(this.CreatePartyFromInvestorInfo(
                PartyRoleBase.LossPayee,
                loanInvestorInfo.LossPayeeContactName,
                loanInvestorInfo.LossPayeePhone,
                loanInvestorInfo.LossPayeeAddress,
                loanInvestorInfo.LossPayeeCity,
                loanInvestorInfo.LossPayeeState,
                loanInvestorInfo.LossPayeeZip,
                GetNextSequenceNumber(partyList)));

            // Create a variable because the ROLE xlink label is used in the next PARTY to create a relationship.
            PARTY mainInvestorParty = this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Investor,
                loanInvestorInfo.CompanyName,
                loanInvestorInfo.MainPhone,
                loanInvestorInfo.MainAddress,
                loanInvestorInfo.MainCity,
                loanInvestorInfo.MainState,
                loanInvestorInfo.MainZip,
                GetNextSequenceNumber(partyList));
            partyList.Add(mainInvestorParty);

            partyList.Add(this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Investor,
                loanInvestorInfo.MainContactName,
                loanInvestorInfo.MainPhone,
                loanInvestorInfo.MainAddress,
                loanInvestorInfo.MainCity,
                loanInvestorInfo.MainState,
                loanInvestorInfo.MainZip,
                GetNextSequenceNumber(partyList),
                isIndividual: true,
                companyLabel: mainInvestorParty == null ? null : mainInvestorParty.Roles.RoleList[0].XlinkLabel));

            partyList.Add(this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Other,
                loanInvestorInfo.NoteShipToContactName,
                loanInvestorInfo.NoteShipToPhone,
                loanInvestorInfo.NoteShipToAddress,
                loanInvestorInfo.NoteShipToCity,
                loanInvestorInfo.NoteShipToState,
                loanInvestorInfo.NoteShipToZip,
                GetNextSequenceNumber(partyList),
                roleTypeOtherDescription: "NoteShipTo"));

            partyList.Add(this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Other,
                loanInvestorInfo.PaymentToContactName,
                loanInvestorInfo.PaymentToPhone,
                loanInvestorInfo.PaymentToAddress,
                loanInvestorInfo.PaymentToCity,
                loanInvestorInfo.PaymentToState,
                loanInvestorInfo.PaymentToZip,
                GetNextSequenceNumber(partyList),
                roleTypeOtherDescription: "MailPmt"));

            InvestorRolodexEntry loanSubservicerInfo = this.DataLoan.sSubservicerInfo;
            PARTY mainSubservicerParty = this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Servicer,
                loanSubservicerInfo.CompanyName,
                loanSubservicerInfo.MainPhone,
                loanSubservicerInfo.MainAddress,
                loanSubservicerInfo.MainCity,
                loanSubservicerInfo.MainState,
                loanSubservicerInfo.MainZip,
                GetNextSequenceNumber(partyList),
                mersOrganizationId: loanSubservicerInfo.MERSOrganizationId);
            if (mainSubservicerParty != null)
            {
                mainSubservicerParty.Roles.RoleList[0].Servicer = this.CreateServicer(ServicerBase.Subservicer);
                partyList.Add(mainSubservicerParty);
            }

            PARTY individualSubservicerParty = this.CreatePartyFromInvestorInfo(
                PartyRoleBase.Servicer,
                loanSubservicerInfo.MainContactName,
                loanSubservicerInfo.MainPhone,
                loanSubservicerInfo.MainAddress,
                loanSubservicerInfo.MainCity,
                loanSubservicerInfo.MainState,
                loanSubservicerInfo.MainZip,
                GetNextSequenceNumber(partyList),
                isIndividual: true,
                companyLabel: mainSubservicerParty == null ? null : mainSubservicerParty.Roles.RoleList[0].XlinkLabel);
            if (individualSubservicerParty != null)
            {
                individualSubservicerParty.Roles.RoleList[0].Servicer = this.CreateServicer(ServicerBase.Subservicer);
                partyList.Add(individualSubservicerParty);
            }
        }

        /// <summary>
        /// Creates a party representing a party from the Investor Info page.
        /// Creates a <see cref="ROLE"/>, <see cref="LEGAL_ENTITY"/>, and <see cref="ADDRESS"/> for the party.
        /// </summary>
        /// <param name="roleType">The role type of the party to create.</param>
        /// <param name="contactName">The name of the company, entered as "Contact Name" on the investor info page.</param>
        /// <param name="phoneNumber">The phone number for the company.</param>
        /// <param name="address">The street address for the company.</param>
        /// <param name="city">The city of the company's address.</param>
        /// <param name="state">The state code of the company's address.</param>
        /// <param name="zip">The zip code of the company's address.</param>
        /// <param name="sequence">The sequence number.</param>
        /// <param name="roleTypeOtherDescription">The description for the role type if the role type is "Other".</param>
        /// <param name="isIndividual">Whether this party should contain an INDIVIDUAL container. Alternative is LEGAL_ENTITY container.</param>
        /// <param name="companyLabel">The label for the employing company for referencing with a relationship.</param>
        /// <param name="mersOrganizationId">The MERSOrganizationIdentifier to be populated.</param>
        /// <remarks>Added specifically for better IDS integration (case 242202).</remarks>
        /// <returns>A <see cref="PARTY"/> container with information about the investor entity for the loan file.</returns>
        private PARTY CreatePartyFromInvestorInfo(
            PartyRoleBase roleType,
            string contactName,
            string phoneNumber,
            string address,
            string city,
            string state,
            string zip,
            int sequence,
            string roleTypeOtherDescription = null,
            bool isIndividual = false,
            string companyLabel = null,
            string mersOrganizationId = null)
        {
            if (string.IsNullOrEmpty(contactName))
            {
                return null;
            }

            ROLES roles = this.CreateRoles(roleType, roleTypeOtherDescription);
            if (isIndividual)
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsEmployedBy, "ROLE", "ROLE", roles.RoleList[0].XlinkLabel, companyLabel);
            }

            return new PARTY
            {
                Roles = roles,
                Individual = isIndividual ? CommonBuilder.CreateIndividual(contactName) : null,
                LegalEntity = isIndividual ? null : CreateLegalEntity(contactName, phoneNumber, null, null, null, ContactPointRoleBase.Blank, mersOrganizationId),
                Addresses = CreateAddresses(
                    address,
                    city,
                    state,
                    zip,
                    AddressBase.Blank,
                    countyName: null,
                    countyFIPSCode: null),
                SequenceNumber = sequence,
            };
        }

        /// <summary>
        /// Creates a SERVICER container.
        /// </summary>
        /// <param name="servicerType">The type of servicer.</param>
        /// <returns>A SERVICER container.</returns>
        private SERVICER CreateServicer(ServicerBase servicerType)
        {
            var servicer = new SERVICER();
            servicer.ServicerType = ToMismoEnum(servicerType);
            return servicer;
        }

        //////////// shared ///////////////////

        /// <summary>
        /// Creates a party representing a power of attorney.
        /// </summary>
        /// <param name="name">The attorney name.</param>
        /// <param name="borrowerLabel">The $$xlink$$ label for the borrower (role).</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        /// <returns>A PARTY container.</returns>
        private PARTY CreateParty_AttorneyInFact(string name, string borrowerLabel, int sequenceNumber)
        {
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }

            var party = new PARTY();
            party.Individual = new INDIVIDUAL();
            party.Individual.Name = CreateName(name, isLegalEntity: false);
            party.SequenceNumber = sequenceNumber;

            party.Roles = new ROLES();

            var role = new ROLE();
            role.RoleDetail = new ROLE_DETAIL();
            role.RoleDetail.PartyRoleType = ToMismoEnum(PartyRoleBase.AttorneyInFact, isSensitiveData: false);
            role.XlinkLabel = "Attorney" + borrowerLabel; //// Each borrower can have only one power of attorney, so the borrower MISMO label is used to create a unique id per attorney.
            role.SequenceNumber = GetNextSequenceNumber(party.Roles.RoleList);

            party.Roles.RoleList.Add(role);

            if (!string.IsNullOrEmpty(borrowerLabel))
            {
                this.ExportData.CreateRelationship(ArcroleVerbPhrase.IsAttorneyInFactFor, "ROLE", "ROLE", role.XlinkLabel, borrowerLabel);
                this.ExportData.CreateRelationship_ToSubjectLoan("ROLE", role.XlinkLabel, ArcroleVerbPhrase.IsAssociatedWith);
            }

            return party;
        }

        /// <summary>
        /// Creates a basic ROLES container with a single role and type.
        /// </summary>
        /// <param name="roleType">The role type.</param>
        /// <param name="roleTypeOtherDescription">The description of the role type. Only populates if <paramref name="roleType"/> is <see cref="PartyRoleBase.Other"/></param>
        /// <returns>A ROLES container.</returns>
        private ROLES CreateRoles(PartyRoleBase roleType, string roleTypeOtherDescription = null)
        {
            var roles = new ROLES();
            roles.RoleList.Add(this.CreateRole(roleType, GetNextSequenceNumber(roles.RoleList), roleType == PartyRoleBase.Other ? roleTypeOtherDescription : null));

            return roles;
        }

        /// <summary>
        /// Creates a basic role with a role type.
        /// </summary>
        /// <param name="roleType">The role type.</param>
        /// <param name="sequenceNumber">The sequence number for the ROLE.</param>
        /// <param name="otherDescription">The description if the role type is Other.</param>
        /// <returns>A ROLE container.</returns>
        private ROLE CreateRole(PartyRoleBase roleType, int sequenceNumber, string otherDescription = "")
        {
            var role = new ROLE();
            role.RoleDetail = CreateRoleDetail(roleType, otherDescription);
            role.SequenceNumber = sequenceNumber;
            role.XlinkLabel = this.ExportData.GeneratePartyLabel(roleType, EntityType.LegalEntity);

            return role;
        }
    }
}
