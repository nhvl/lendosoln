﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using DataAccess;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static TypeBuilder;

    /// <summary>
    /// Class that builds a RelatedLoan LOAN container.
    /// </summary>
    public class RelatedLoanBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RelatedLoanBuilder"/> class.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="exportData">A collection of data used throughout the exporter.</param>
        /// <param name="sequenceNumber">The sequence number.</param>
        public RelatedLoanBuilder(CPageData dataLoan, Mismo34ExporterCache exportData, int sequenceNumber)
        {
            this.DataLoan = dataLoan;
            this.ExportData = exportData;
            this.SequenceNumber = sequenceNumber;
        }

        /// <summary>
        /// Gets the data loan.
        /// </summary>
        protected CPageData DataLoan { get; }

        /// <summary>
        /// Gets a collection of data used throughout the exporter.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; }

        /// <summary>
        /// Gets the sequence number.
        /// </summary>
        protected int SequenceNumber { get; }

        /// <summary>
        /// Creates the LOAN container.
        /// </summary>
        /// <returns>The LOAN container.</returns>
        public LOAN CreateContainer()
        {
            var loan = new LOAN();
            loan.SequenceNumber = this.SequenceNumber;
            loan.LoanRoleType = LoanRoleBase.RelatedLoan;

            loan.TermsOfLoan = this.CreateTermsOfLoanForRelatedLoan();
            loan.Amortization = this.CreateAmortization();
            loan.Maturity = this.CreateMaturity();
            loan.FeeInformation = this.CreateFeeInformation();
            loan.Ltv = this.CreateLtv();
            loan.LoanDetail = this.CreateLoanDetail();
            loan.Payment = this.CreatePayment();

            return loan;
        }

        /// <summary>
        /// Converts from E_sVaPriorLoanT to MortgageBase.
        /// </summary>
        /// <param name="priorLoanType">The prior loan type.</param>
        /// <returns>The MortgageBase enum.</returns>
        private static MortgageBase ToMortgageEnumBase(E_sVaPriorLoanT priorLoanType)
        {
            switch (priorLoanType)
            {
                case E_sVaPriorLoanT.FhaFixed:
                case E_sVaPriorLoanT.FhaArm:
                    return MortgageBase.FHA;
                case E_sVaPriorLoanT.ConventionalFixed:
                case E_sVaPriorLoanT.ConventionalArm:
                case E_sVaPriorLoanT.ConventionalInterestOnly:
                    return MortgageBase.Conventional;
                case E_sVaPriorLoanT.VaFixed:
                case E_sVaPriorLoanT.VaArm:
                    return MortgageBase.VA;
                case E_sVaPriorLoanT.Other:
                    return MortgageBase.Other;
                case E_sVaPriorLoanT.LeaveBlank:
                    return MortgageBase.Blank;
                default:
                    throw new UnhandledEnumException(priorLoanType);
            }
        }

        /// <summary>
        /// Converts from E_sVaPriorLoanT to AmortizationBase.
        /// </summary>
        /// <param name="amortType">The amortization type.</param>
        /// <returns>The AmortizationBase.</returns>
        private static AmortizationBase ToAmortizationBase(E_sLoanBeingRefinancedAmortizationT amortType)
        {
            switch (amortType)
            {
                case E_sLoanBeingRefinancedAmortizationT.Fixed:
                    return AmortizationBase.Fixed;
                case E_sLoanBeingRefinancedAmortizationT.Adjustable:
                    return AmortizationBase.AdjustableRate;
                case E_sLoanBeingRefinancedAmortizationT.LeaveBlank:
                    return AmortizationBase.Blank;
                default:
                    throw new UnhandledEnumException(amortType);
            }
        }

        /// <summary>
        /// Create a TERMS_OF_LOAN container for the related loan.
        /// </summary>
        /// <returns>The TERMS_OF_LOAN container.</returns>
        private TERMS_OF_LOAN CreateTermsOfLoanForRelatedLoan()
        {
            var termsOfLoan = new TERMS_OF_LOAN();
            termsOfLoan.MortgageType = ToMismoEnum(ToMortgageEnumBase(this.DataLoan.sVaPriorLoanT), isSensitiveData: false);
            termsOfLoan.NoteRatePercent = ToMismoPercent(this.DataLoan.sLoanBeingRefinancedInterestRate_rep);
            termsOfLoan.LienPriorityType = ToMismoEnum(this.GetLienPriorityValue(this.DataLoan.sLoanBeingRefinancedLienPosT));
            return termsOfLoan;
        }

        /// <summary>
        /// Retrieves the MISMO value corresponding to a lien position.
        /// </summary>
        /// <param name="position">A lien position.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private LienPriorityBase GetLienPriorityValue(E_sLoanBeingRefinancedLienPosT position)
        {
            switch (position)
            {
                case E_sLoanBeingRefinancedLienPosT.LeaveBlank:
                    return LienPriorityBase.Blank;
                case E_sLoanBeingRefinancedLienPosT.First:
                    return LienPriorityBase.FirstLien;
                case E_sLoanBeingRefinancedLienPosT.Second:
                    return LienPriorityBase.SecondLien;
                default:
                    throw new UnhandledEnumException(position);
            }
        }

        /// <summary>
        /// Creates a container holding data on the amortization of a loan.
        /// </summary>
        /// <returns>An AMORTIZATION container.</returns>
        private AMORTIZATION CreateAmortization()
        {
            var amortization = new AMORTIZATION();
            amortization.AmortizationRule = this.CreateAmortizationRule();

            return amortization;
        }

        /// <summary>
        /// Creates an AMORTIZATION_RULE container for the related loan.
        /// </summary>
        /// <returns>The AMORTIZATION_RULE container.</returns>
        private AMORTIZATION_RULE CreateAmortizationRule()
        {
            var amortizationRule = new AMORTIZATION_RULE();
            amortizationRule.AmortizationType = ToMismoEnum(ToAmortizationBase(this.DataLoan.sLoanBeingRefinancedAmortizationT), isSensitiveData: false);

            return amortizationRule;
        }

        /// <summary>
        /// Creates a MATURITY container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private MATURITY CreateMaturity()
        {
            MATURITY maturity = new MATURITY();
            maturity.MaturityOccurrences = this.CreateMaturityOccurences();

            return maturity;
        }

        /// <summary>
        /// Creates a MATURITY_OCCURENCES container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private MATURITY_OCCURRENCES CreateMaturityOccurences()
        {
            MATURITY_OCCURRENCES maturityOccurences = new MATURITY_OCCURRENCES();
            maturityOccurences.MaturityOccurrenceList.Add(this.CreateMaturityOccurence());

            return maturityOccurences;
        }

        /// <summary>
        /// Creates a MATURITY_OCCURENCE container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private MATURITY_OCCURRENCE CreateMaturityOccurence()
        {
            MATURITY_OCCURRENCE maturityOccurence = new MATURITY_OCCURRENCE();
            maturityOccurence.LoanRemainingMaturityTermMonthsCount = ToMismoCount(this.DataLoan.sLoanBeingRefinancedRemainingTerm_rep);

            return maturityOccurence;
        }

        /// <summary>
        /// Creates a FEE_INFORMATION container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private FEE_INFORMATION CreateFeeInformation()
        {
            FEE_INFORMATION feeInformation = new FEE_INFORMATION();
            feeInformation.FeesSummary = this.CreateFeeSummary();

            return feeInformation;
        }

        /// <summary>
        /// Creates a FEES_SUMMARY container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private FEES_SUMMARY CreateFeeSummary()
        {
            FEES_SUMMARY feeSummary = new FEES_SUMMARY();
            feeSummary.FeeSummaryDetail = this.CreateFeeSummaryDetail();

            return feeSummary;
        }

        /// <summary>
        /// Creates a FEE_SUMMARY_DETAIL container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private FEE_SUMMARY_DETAIL CreateFeeSummaryDetail()
        {
            FEE_SUMMARY_DETAIL feeSummaryDetail = new FEE_SUMMARY_DETAIL();
            feeSummaryDetail.FeeSummaryTotalOfAllPaymentsAmount = ToMismoAmount(this.DataLoan.sLoanBeingRefinancedTotalOfPayments_rep);

            return feeSummaryDetail;
        }

        /// <summary>
        /// Creates a LTV container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private LTV CreateLtv()
        {
            LTV ltv = new LTV();
            ltv.LTVRatioPercent = ToMismoPercent(this.DataLoan.sLoanBeingRefinancedLtvR_rep);

            return ltv;
        }

        /// <summary>
        /// Creates a LOAN_DETAIL container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private LOAN_DETAIL CreateLoanDetail()
        {
            LOAN_DETAIL loanDetail = new LOAN_DETAIL();
            loanDetail.Extension = this.CreateLoanDetailExtension();

            return loanDetail;
        }

        /// <summary>
        /// Creates a LOAN_DETAIL_EXTENSION container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private LOAN_DETAIL_EXTENSION CreateLoanDetailExtension()
        {
            LOAN_DETAIL_EXTENSION loanDetailExtension = new LOAN_DETAIL_EXTENSION();
            loanDetailExtension.Other = this.CreateLqbLoanDetailExtension();

            return loanDetailExtension;
        }

        /// <summary>
        /// Creates a LQB_LOAN_DETAIL_EXTENSION container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private LQB_LOAN_DETAIL_EXTENSION CreateLqbLoanDetailExtension()
        {
            LQB_LOAN_DETAIL_EXTENSION extension = new LQB_LOAN_DETAIL_EXTENSION();
            extension.LoanBeingRefinancedUsedToConstructAlterRepairIndicator = ToMismoIndicator(this.DataLoan.sLoanBeingRefinancedUsedToConstructAlterRepair);

            return extension;
        }

        /// <summary>
        /// Creates a PAYMENT container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private PAYMENT CreatePayment()
        {
            PAYMENT payment = new PAYMENT();
            payment.PaymentSummary = this.CreatePaymentSummary();

            return payment;
        }

        /// <summary>
        /// Creates a PAYMENT_SUMMARY container for the related loan.
        /// </summary>
        /// <returns>The container.</returns>
        private PAYMENT_SUMMARY CreatePaymentSummary()
        {
            PAYMENT_SUMMARY paymentSummary = new PAYMENT_SUMMARY();
            paymentSummary.ScheduledUPBAmount = ToMismoAmount(this.DataLoan.sSpLien_rep);

            return paymentSummary;
        }
    }
}
