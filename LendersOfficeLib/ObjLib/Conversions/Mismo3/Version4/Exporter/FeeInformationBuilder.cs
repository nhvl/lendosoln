﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Migration;
    using LendersOffice.ObjLib.TRID2;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Builds MISMO 3.4 FEE_INFORMATION data.
    /// </summary>
    public class FeeInformationBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeeInformationBuilder"/> class.
        /// </summary>
        /// <param name="dataLoan">The loan data representing the subject loan.</param>
        /// <param name="exportData">A collection of data used throughout the exporter.</param>
        /// <param name="documentTypeOverride">Overrides the document type if this is an archive instead of live data.</param>
        public FeeInformationBuilder(CPageData dataLoan, Mismo34ExporterCache exportData, IntegratedDisclosureDocumentBase? documentTypeOverride = null)
        {
            this.DataLoan = dataLoan;
            this.ExportData = exportData;

            if (documentTypeOverride.HasValue)
            {
                this.DocumentType = documentTypeOverride.Value;
            }
            else
            {
                this.DocumentType = this.ExportData.IsClosingPackage
                    ? IntegratedDisclosureDocumentBase.ClosingDisclosure
                    : IntegratedDisclosureDocumentBase.LoanEstimate;
            }

            this.EstimatedFeeSet = null;
            if (this.DataLoan.sTridTargetRegulationVersionT == TridTargetRegulationVersionT.TRID2017
                && this.DataLoan.sAppliedCCArchiveLinkedArchiveId != Guid.Empty)
            {
                // If there is a linked pair of archives, the archive applied to the loan contains the
                // actual fee amounts while the linked archive contains the tolerance data.
                var linkedArchive = this.DataLoan.GetClosingCostArchiveById(this.DataLoan.sAppliedCCArchiveLinkedArchiveId);
                this.EstimatedFeeSet = linkedArchive?.GetClosingCostSet(this.DataLoan.m_convertLos);
            }

            if (this.EstimatedFeeSet == null)
            {
                this.EstimatedFeeSet = this.DataLoan.LastDisclosedLoanEstimateArchive?.GetClosingCostSet(this.DataLoan.m_convertLos);
            }
        }

        /// <summary>
        /// Gets the loan object to be exported.
        /// </summary>
        protected CPageData DataLoan { get; private set; }

        /// <summary>
        /// Gets a collection of data used  throughout the exporter.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this request is for a closing package.
        /// </summary>
        protected IntegratedDisclosureDocumentBase DocumentType { get; private set; }

        /// <summary>
        /// Gets the estimated fee set for the fee list.
        /// </summary>
        protected BorrowerClosingCostSet EstimatedFeeSet { get; private set; }

        /// <summary>
        /// Creates a new <see cref="FEE_INFORMATION"/> MISMO container representing the subject loan.
        /// </summary>
        /// <returns>A new <see cref="FEE_INFORMATION"/> container.</returns>
        public FEE_INFORMATION CreateContainer()
        {
            var feeInformation = new FEE_INFORMATION();

            feeInformation.Fees = this.CreateFees();
            feeInformation.FeesSummary = this.CreateFeesSummary();

            return feeInformation;
        }

        /// <summary>
        /// Creates a container holding details on a closing cost fee.
        /// </summary>
        /// <param name="closingFee">An individual closing cost fee.</param>
        /// <param name="estimatedAmount">The estimated amount of the fee.</param>
        /// <param name="documentType">Indicates the TRID document type the fees are associated with.</param>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <param name="loanType">The loan type.</param>
        /// <param name="vendor">The vendor associated with the request.</param>
        /// <returns>A FEE_DETAIL container.</returns>
        private static FEE_DETAIL CreateFeeDetail(BorrowerClosingCostFee closingFee, string estimatedAmount, IntegratedDisclosureDocumentBase documentType, CAgentFields beneficiaryContact, E_sLT loanType, E_DocumentVendor vendor)
        {
            var feeDetail = new FEE_DETAIL();
            feeDetail.BorrowerChosenProviderIndicator = ToMismoIndicator(closingFee.DidShop);

            feeDetail.FeeEstimatedTotalAmount = ToMismoAmount(estimatedAmount);
            feeDetail.FeeActualTotalAmount = ToMismoAmount(closingFee.TotalAmount_rep);

            MISMOString paidToTypeOtherDescription = null;

            feeDetail.FeePaidToType = GetFeePaidToType(
                beneficiaryContact,
                closingFee.IsAffiliate,
                closingFee.IsThirdParty,
                closingFee.BeneficiaryType,
                closingFee.Beneficiary,
                closingFee.BeneficiaryDescription,
                out paidToTypeOtherDescription);

            if (paidToTypeOtherDescription != null)
            {
                feeDetail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
            }

            string feeDescription = closingFee.Description;
            if (closingFee.IsTitleFee)
            {
                feeDescription = "TITLE - " + feeDescription;
            }

            feeDetail.FeeDescription = ToMismoString(feeDescription);
            feeDetail.FeePercentBasisType = ToMismoEnum(GetFeePercentBasisBaseValue(closingFee.PercentBaseT));

            if (feeDetail.FeePercentBasisType?.IsSetToOther ?? false)
            {
                feeDetail.FeePercentBasisTypeOtherDescription = ToMismoString(closingFee.PercentBaseT);
            }

            feeDetail.FeeSpecifiedFixedAmount = ToMismoAmount(closingFee.BaseAmount_rep);
            feeDetail.FeeSpecifiedHUD1LineNumberValue = ToMismoValue(closingFee.HudLine_rep);
            feeDetail.FeeTotalPercent = ToMismoPercent(closingFee.Percent_rep);

            string feeTypeOtherDescription;
            var feeTypeBase = GetFeeBaseValue(closingFee.MismoFeeT, closingFee.Description, out feeTypeOtherDescription);
            feeDetail.FeeType = ToMismoEnum(feeTypeBase, displayLabelText: feeDescription);
            if (feeTypeBase == FeeBase.Other)
            {
                feeDetail.FeeTypeOtherDescription = ToMismoString(feeTypeOtherDescription);
            }

            feeDetail.IntegratedDisclosureSectionType = ToMismoEnum(GetIntegratedDisclosureSectionBaseValue(documentType, closingFee.GetTRIDSectionBasedOnDisclosure(documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure), closingFee.CanShop));

            if (feeDetail.IntegratedDisclosureSectionType?.IsSetToOther ?? false)
            {
                feeDetail.IntegratedDisclosureSectionTypeOtherDescription = ToMismoString(closingFee.GetTRIDSectionBasedOnDisclosure(documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure));
            }

            feeDetail.OptionalCostIndicator = ToMismoIndicator(closingFee.IsOptional);
            feeDetail.RegulationZPointsAndFeesIndicator = ToMismoIndicator(closingFee.IsIncludedInQm);
            feeDetail.RequiredProviderOfServiceIndicator = ToMismoIndicator(!closingFee.CanShop);

            if (closingFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud900MortgageInsurancePremiumUpfrontFeeTypeId && loanType == E_sLT.UsdaRural)
            {
                string notUseDescription;
                feeDetail.FeeType = ToMismoEnum(GetFeeBaseValue(E_ClosingCostFeeMismoFeeT.RuralHousingFee, null, out notUseDescription), displayLabelText: "Rural Housing Fee");
            }
            else if (closingFee.ClosingCostFeeTypeId == DefaultSystemClosingCostFee.Hud800OriginatorCompensationFeeTypeId && vendor == E_DocumentVendor.DocuTech)
            {
                feeDetail.FeeSpecifiedHUD1LineNumberValue = ToMismoValue("895");
            }

            feeDetail.Extension = CreateFeeDetailExtension(closingFee.Dflp, closingFee.OriginalDescription, closingFee.ClosingCostFeeTypeId, closingFee.CanShop, closingFee.DidShop);

            return feeDetail;
        }

        /// <summary>
        /// Creates an "EXTENSION" container with extra information about a fee detail.
        /// </summary>
        /// <param name="dflp">Whether this fee has the DFLP checkbox checked.</param>
        /// <param name="originalDescription">The unmodified description of the fee.</param>
        /// <param name="feeTypeId">The unique closing cost type ID.</param>
        /// <param name="canShop">A boolean value indicating whether the borrower can shop for this fee.</param>
        /// <param name="didShop">A boolean value indicating whether the borrower did shop for this fee.</param>
        /// <returns>A <see cref="FEE_DETAIL_EXTENSION"/> container.</returns>
        private static FEE_DETAIL_EXTENSION CreateFeeDetailExtension(bool dflp, string originalDescription, Guid feeTypeId, bool canShop, bool didShop)
        {
            var extension = new FEE_DETAIL_EXTENSION();
            extension.Other = CreateLQBFeeDetailExtension(dflp, originalDescription, feeTypeId, canShop, didShop);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary LQB "OTHER" container with extra information about a fee detail.
        /// </summary>
        /// <param name="dflp">Whether this fee has the DFLP checkbox checked.</param>
        /// <param name="originalDescription">The unmodified description of the fee.</param>
        /// <param name="feeTypeId">The unique closing cost type ID.</param>
        /// <param name="canShop">A boolean value indicating whether the borrower can shop for this fee.</param>
        /// <param name="didShop">A boolean value indicating whether the borrower did shop for this fee.</param>
        /// <returns>A <see cref="LQB_FEE_DETAIL_EXTENSION"/> container.</returns>
        private static LQB_FEE_DETAIL_EXTENSION CreateLQBFeeDetailExtension(bool dflp, string originalDescription, Guid feeTypeId, bool canShop, bool didShop)
        {
            var extension = new LQB_FEE_DETAIL_EXTENSION();
            extension.DFLPIndicator = ToMismoIndicator(dflp);
            extension.OriginalDescription = ToMismoString(originalDescription);
            extension.ClosingCostFeeTypeID = ToMismoString(feeTypeId.ToString());
            extension.CanShop = ToMismoIndicator(canShop);
            extension.DidShop = ToMismoIndicator(didShop);

            return extension;
        }

        /// <summary>
        /// Creates a container to hold all instances of FEE_PAYMENT.
        /// </summary>
        /// <param name="payments">The set of payments for a fee.</param>
        /// <param name="percent">The payment percent.</param>
        /// <param name="allowableFHA">True if the fee is an allowable FHA closing cost. Otherwise false.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <param name="documentType">The type of document the fees will appear on.</param>
        /// <param name="creditDiscloseLocation">The document that will disclose the lender credit.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="liveLoanVersion">The live (not archived) version of the loan.</param>
        /// <returns>A FEE_PAYMENTS container.</returns>
        private static FEE_PAYMENTS CreateFeePayments(IEnumerable<LoanClosingCostFeePayment> payments, string percent, bool allowableFHA, bool qm, IntegratedDisclosureDocumentBase documentType, E_LenderCreditDiscloseLocationT creditDiscloseLocation, E_DocumentVendor vendor, LoanVersionT liveLoanVersion)
        {
            var feePayments = new FEE_PAYMENTS();

            foreach (BorrowerClosingCostFeePayment payment in payments.Where(p => p.Amount != 0.00m))
            {
                feePayments.FeePaymentList.Add(CreateFeePayment(payment, percent, allowableFHA, qm, GetNextSequenceNumber(feePayments.FeePaymentList), documentType, creditDiscloseLocation, vendor, liveLoanVersion));
            }

            return feePayments;
        }

        /// <summary>
        /// Creates a container holding data on a fee payment.
        /// </summary>
        /// <param name="payment">An individual payment for a fee.</param>
        /// <param name="percent">The payment percent.</param>
        /// <param name="allowableFHA">True if the fee is an allowable FHA closing cost. Otherwise false.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <param name="sequence">The sequence number of the payment among the list of payments.</param>
        /// <param name="documentType">The type of document the fees will appear on.</param>
        /// <param name="creditDiscloseLocation">Indicates which form the lender credit is disclosed on.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="liveLoanVersion">The live (not archived) version of the loan.</param>
        /// <returns>A FEE_PAYMENT container.</returns>
        private static FEE_PAYMENT CreateFeePayment(BorrowerClosingCostFeePayment payment, string percent, bool allowableFHA, bool qm, int sequence, IntegratedDisclosureDocumentBase documentType, E_LenderCreditDiscloseLocationT creditDiscloseLocation, E_DocumentVendor vendor, LoanVersionT liveLoanVersion)
        {
            var feePayment = new FEE_PAYMENT();
            feePayment.FeeActualPaymentAmount = ToMismoAmount(payment.Amount_rep);
            feePayment.FeePaymentAllowableFHAClosingCostIndicator = ToMismoIndicator(allowableFHA);
            feePayment.FeePaymentPaidByType = ToMismoEnum(GetFeePaymentPaidByBaseValue(payment.GetPaidByType(creditDiscloseLocation, documentType != IntegratedDisclosureDocumentBase.LoanEstimate)));
            feePayment.FeePaymentPaidOutsideOfClosingIndicator = ToMismoIndicator(payment.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing);
            feePayment.FeePaymentPercent = ToMismoPercent(percent);
            feePayment.FeePaymentResponsiblePartyType = ToMismoEnum(GetFeePaymentResponsiblePartyBaseValue(payment.ResponsiblePartyT));
            feePayment.PaymentFinancedIndicator = ToMismoIndicator(payment.IsFinanced);
            feePayment.PaymentIncludedInAPRIndicator = vendor.Equals(E_DocumentVendor.DocsOnDemand) ?
                ToMismoIndicator(payment.IsAPRRaw) :
                ToMismoIndicator(payment.IsIncludedInApr(liveLoanVersion));
            feePayment.RegulationZPointsAndFeesIndicator = ToMismoIndicator(qm);
            feePayment.Extension = CreateFeePaymentExtension(payment.Id.ToString());
            feePayment.SequenceNumber = sequence;

            return feePayment;
        }

        /// <summary>
        /// Creates an extension container holding data on a fee payment.
        /// </summary>
        /// <param name="paymentId">The fee payment ID.</param>
        /// <returns>A serializable <see cref="FEE_PAYMENT_EXTENSION"/> container.</returns>
        private static FEE_PAYMENT_EXTENSION CreateFeePaymentExtension(string paymentId)
        {
            var extension = new FEE_PAYMENT_EXTENSION();
            extension.Other = CreateLqbFeePaymentExtension(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container holding data on a fee payment.
        /// </summary>
        /// <param name="paymentId">The fee payment ID.</param>
        /// <returns>A serializable <see cref="LQB_FEE_PAYMENT_EXTENSION"/> container.</returns>
        private static LQB_FEE_PAYMENT_EXTENSION CreateLqbFeePaymentExtension(string paymentId)
        {
            var extension = new LQB_FEE_PAYMENT_EXTENSION();
            extension.Id = ToMismoString(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a REQUIRED_SERVICE_PROVIDER container with information regarding the company of the given agent, a service provider required by the lender. 
        /// </summary>
        /// <param name="agent">The contact record associated with the service provider.</param>
        /// <returns>A REQUIRED_SERVICE_PROVIDER container with information regarding the company of the given agent, a service provider required by the lender.</returns>
        private static SELECTED_SERVICE_PROVIDER CreateRequiredServiceProvider(CAgentFields agent)
        {
            if (!agent.IsValid || agent.IsNewRecord)
            {
                return null;
            }

            var provider = new SELECTED_SERVICE_PROVIDER();
            var addresses = CommonBuilder.CreateAddresses(agent);

            if (addresses.AddressList != null && addresses.AddressList.Count(a => a != null) > 0)
            {
                provider.Address = addresses.AddressList.Where(a => a != null).FirstOrDefault();
            }

            provider.LegalEntity = CommonBuilder.CreateLegalEntity(agent, agent.CompanyName, ContactPointRoleBase.Work);
            provider.SelectedServiceProviderDetail = CreateSelectedServiceProviderDetail(agent);

            return provider;
        }

        /// <summary>
        /// Creates a SELECTED_SERVICE_PROVIDER_DETAIL container with information regarding the service provider's relationship with the lender.
        /// </summary>
        /// <param name="provider">The agent record of the service provider.</param>
        /// <returns>A SELECTED_SERVICE_PROVIDER_DETAIL container with information regarding the service provider's relationship with the lender.</returns>
        private static SELECTED_SERVICE_PROVIDER_DETAIL CreateSelectedServiceProviderDetail(CAgentFields provider)
        {
            var detail = new SELECTED_SERVICE_PROVIDER_DETAIL();
            string relationship = string.Empty;

            if (provider.IsLender)
            {
                relationship = "This company is the lender";
            }
            else if (provider.IsLenderAssociation)
            {
                relationship = "Associate of the lender";
            }
            else if (provider.IsLenderRelative)
            {
                relationship = "Relative of the lender";
            }
            else if (provider.HasLenderRelationship)
            {
                relationship = "Has an employment, franchise or other business relationship with the lender";
            }
            else if (provider.HasLenderAccountLast12Months)
            {
                relationship = "Within the last 12 months, the provider has maintained an account with the lender or had an outstanding loan or credit arrangement with the lender";
            }
            else if (provider.IsUsedRepeatlyByLenderLast12Months)
            {
                relationship = "Within the last 12 months, the lender has repeatedly used or required borrowers to use the services of this provider";
            }
            else if (provider.IsLenderAffiliate)
            {
                relationship = "Affiliate of the lender";
            }

            if (string.IsNullOrEmpty(relationship))
            {
                return null;
            }

            detail.SelectedServiceProviderNatureOfRelationshipDescription = ToMismoString(relationship);

            return detail;
        }

        //// Enum Mappings

        /// <summary>
        /// Creates a FeePaidToEnum object with the paid-to type based on the provided agent information.
        /// </summary>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <param name="affiliate">True if the entity imposing the fee is an affiliate of either the lender or the originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the entity imposing the fee is a third party. Otherwise false.</param>
        /// <param name="beneficiaryType">A string representation of the agent type of the fee beneficiary.</param>
        /// <param name="beneficiary">The agent type of the fee beneficiary.</param>
        /// <param name="beneficiaryDescription">A description of the fee beneficiary.</param>
        /// <param name="otherDescription">The target <see cref="MISMOString"/> to contain the paid-to type other description, if the paid-to type resolves to other. Otherwise it will be null.</param>
        /// <returns>A FeePaidToEnum object with the paid-to type based on the provided agent information.</returns>
        private static MISMOEnum<FeePaidToBase> GetFeePaidToType(CAgentFields beneficiaryContact, bool affiliate, bool thirdParty, string beneficiaryType, E_AgentRoleT beneficiary, string beneficiaryDescription, out MISMOString otherDescription)
        {
            MISMOEnum<FeePaidToBase> feePaidToType = null;
            otherDescription = null;

            if (beneficiaryContact != null)
            {
                feePaidToType = beneficiaryContact.RecordId == Guid.Empty || beneficiaryContact.IsNewRecord ? null
                    : ToMismoEnum(
                        GetFeePaidToBaseValue(
                            beneficiaryContact.AgentRoleT,
                            beneficiaryContact.IsLender,
                            beneficiaryContact.IsOriginator,
                            affiliate,
                            thirdParty,
                            beneficiaryContact.IsLenderAffiliate,
                            beneficiaryContact.IsOriginatorAffiliate));

                if (feePaidToType != null && feePaidToType.EnumValue == FeePaidToBase.Other)
                {
                    otherDescription = ToMismoString(beneficiaryType);
                }
            }

            //// If the fee PaidTo is set w/o having an associated agent, such as set from the fee setup, then map it.
            if (feePaidToType == null)
            {
                feePaidToType = ToMismoEnum(
                    GetFeePaidToBaseValue(
                        beneficiary,
                        lender: false,
                        originator: false,
                        affiliate: affiliate,
                        thirdParty: thirdParty,
                        isLenderAffiliate: false,
                        isOriginatorAffiliate: affiliate));

                if (feePaidToType?.IsSetToOther ?? false)
                {
                    string description = string.IsNullOrEmpty(beneficiaryDescription) ? GetXmlEnumName(beneficiary) : beneficiaryDescription;

                    otherDescription = ToMismoString(description);
                }
            }

            return feePaidToType;
        }

        /// <summary>
        /// Retrieves the enumerated value corresponding to the entity who will receive payment for a service.
        /// </summary>
        /// <param name="agentType">The type of entity who will receive payment for a service.</param>
        /// <param name="lender">True if the entity is the lender. Otherwise false.</param>
        /// <param name="originator">True if the entity is the originator. Otherwise false.</param>
        /// <param name="affiliate">True if the entity is an affiliate of either the lender or originator. Otherwise false.</param>
        /// <param name="thirdParty">True if the agent is a third party. Otherwise false.</param>
        /// <param name="isLenderAffiliate">True if the entity is an affiliate of the lender. Otherwise false.</param>
        /// <param name="isOriginatorAffiliate">True if the entity is an affiliate of the originator. Otherwise false.</param>
        /// <returns>The corresponding value.</returns>
        private static FeePaidToBase GetFeePaidToBaseValue(E_AgentRoleT agentType, bool lender, bool originator, bool affiliate, bool thirdParty, bool isLenderAffiliate, bool isOriginatorAffiliate)
        {
            if (agentType == E_AgentRoleT.Broker)
            {
                return FeePaidToBase.Broker;
            }
            else if (agentType == E_AgentRoleT.Investor)
            {
                return FeePaidToBase.Investor;
            }
            else if (agentType == E_AgentRoleT.Lender)
            {
                return FeePaidToBase.Lender;
            }
            else if (lender)
            {
                return FeePaidToBase.Lender;
            }
            else if (originator)
            {
                return FeePaidToBase.Broker;
            }
            else if (affiliate && isLenderAffiliate && !isOriginatorAffiliate)
            {
                return FeePaidToBase.LenderAffiliate;
            }
            else if (affiliate)
            {
                // If it's ambiguous which is the affiliate, default to the broker
                return FeePaidToBase.BrokerAffiliate;
            }
            else if (thirdParty)
            {
                return FeePaidToBase.ThirdPartyProvider;
            }
            else
            {
                return FeePaidToBase.Other;
            }
        }

        /// <summary>
        /// Retrieves the value corresponding to the entity who paid the fee payment.
        /// </summary>
        /// <param name="paidBy">The entity who paid the fee payment.</param>
        /// <returns>The corresponding <see cref="FeePaymentPaidByBase" /> value.</returns>
        private static FeePaymentPaidByBase GetFeePaymentPaidByBaseValue(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return FeePaymentPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return FeePaymentPaidByBase.Broker;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return FeePaymentPaidByBase.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return FeePaymentPaidByBase.Seller;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return FeePaymentPaidByBase.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return FeePaymentPaidByBase.Blank;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Converts the given responsible party type to the corresponding FeePaymentResponsiblePartyBase.
        /// </summary>
        /// <param name="responsible">The type of party responsible for paying the fee.</param>
        /// <returns>The FeePaymentResponsiblePartyBase corresponding to the given responsible party type.</returns>
        private static FeePaymentResponsiblePartyBase GetFeePaymentResponsiblePartyBaseValue(E_GfeResponsiblePartyT responsible)
        {
            switch (responsible)
            {
                case E_GfeResponsiblePartyT.Buyer:
                    return FeePaymentResponsiblePartyBase.Buyer;
                case E_GfeResponsiblePartyT.Seller:
                    return FeePaymentResponsiblePartyBase.Seller;
                case E_GfeResponsiblePartyT.Lender:
                    return FeePaymentResponsiblePartyBase.Lender;
                case E_GfeResponsiblePartyT.Broker:
                    return FeePaymentResponsiblePartyBase.Broker;
                case E_GfeResponsiblePartyT.LeaveBlank:
                    return FeePaymentResponsiblePartyBase.Blank;
                default:
                    throw new UnhandledEnumException(responsible);
            }
        }

        /// <summary>
        /// Retrieves the fee percent basis type corresponding to the given percent base.
        /// </summary>
        /// <param name="percentBasis">The percent base type.</param>
        /// <returns>The corresponding <see cref="FeePercentBasisBase" /> value.</returns>
        private static FeePercentBasisBase GetFeePercentBasisBaseValue(E_PercentBaseT percentBasis)
        {
            switch (percentBasis)
            {
                case E_PercentBaseT.AppraisalValue:
                    return FeePercentBasisBase.PropertyAppraisedValueAmount;
                case E_PercentBaseT.LoanAmount:
                    return FeePercentBasisBase.BaseLoanAmount;
                case E_PercentBaseT.TotalLoanAmount:
                    return FeePercentBasisBase.OriginalLoanAmount;
                case E_PercentBaseT.SalesPrice:
                    return FeePercentBasisBase.PurchasePriceAmount;
                case E_PercentBaseT.OriginalCost:
                case E_PercentBaseT.AverageOutstandingBalance:
                case E_PercentBaseT.DecliningRenewalsAnnually:
                case E_PercentBaseT.DecliningRenewalsMonthly:
                case E_PercentBaseT.AllYSP:
                    return FeePercentBasisBase.Other;
                default:
                    throw new UnhandledEnumException(percentBasis);
            }
        }

        /// <summary>
        /// Creates a container to hold a summary of the loan fees.
        /// </summary>
        /// <returns>A FEES_SUMMARY container.</returns>
        private FEES_SUMMARY CreateFeesSummary()
        {
            var feesSummary = new FEES_SUMMARY();
            feesSummary.FeeSummaryDetail = this.CreateFeeSummaryDetail();

            return feesSummary;
        }

        /// <summary>
        /// Creates a container holding details on the summarized loan fees.
        /// </summary>
        /// <returns>A FEE_SUMMARY_DETAIL container.</returns>
        private FEE_SUMMARY_DETAIL CreateFeeSummaryDetail()
        {
            var feeSummaryDetail = new FEE_SUMMARY_DETAIL();

            feeSummaryDetail.APRPercent = ToMismoPercent(this.DataLoan.sApr_rep);
            feeSummaryDetail.FeeSummaryDisclosedTotalSalesPriceAmount = ToMismoAmount(this.DataLoan.sPurchPrice_rep);
            feeSummaryDetail.FeeSummaryTotalAmountFinancedAmount = ToMismoAmount(this.DataLoan.sFinancedAmt_rep);
            feeSummaryDetail.FeeSummaryTotalAPRFeesAmount = ToMismoAmount(this.DataLoan.sAprIncludedCc_rep);
            feeSummaryDetail.FeeSummaryTotalDepositedReservesAmount = ToMismoAmount(this.DataLoan.sGfeInitialImpoundDeposit_rep);
            feeSummaryDetail.FeeSummaryTotalFinanceChargeAmount = ToMismoAmount(this.DataLoan.sFinCharge_rep);
            feeSummaryDetail.FeeSummaryTotalInterestPercent = ToMismoPercent(
                this.DocumentType == IntegratedDisclosureDocumentBase.LoanEstimate
                ? this.DataLoan.sCFPBTotalInterestPercentLE_rep
                : this.DataLoan.sCFPBTotalInterestPercent_rep);

            // OPM 247161 - UCD Support.
            feeSummaryDetail.FeeSummaryTotalOfAllPaymentsAmount = ToMismoAmount(this.DataLoan.sTRIDTotalOfPayments_rep);
            feeSummaryDetail.Extension = this.CreateFeeSummaryDetailExtension();

            return feeSummaryDetail;
        }

        /// <summary>
        /// Creates an extension container for the fee summary details.
        /// </summary>
        /// <returns>An extension container.</returns>
        private FEE_SUMMARY_DETAIL_EXTENSION CreateFeeSummaryDetailExtension()
        {
            var extension = new FEE_SUMMARY_DETAIL_EXTENSION();
            extension.Other = this.CreateLqbFeeSummaryDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container for the fee summary details.
        /// </summary>
        /// <returns>An extension container.</returns>
        private LQB_FEE_SUMMARY_DETAIL_EXTENSION CreateLqbFeeSummaryDetailExtension()
        {
            var extension = new LQB_FEE_SUMMARY_DETAIL_EXTENSION();
            extension.LenderCreditCanOffsetAprIndicator = ToMismoIndicator(this.DataLoan.sGfeLenderCreditFProps_Apr);

            return extension;
        }

        /// <summary>
        /// Creates a container to hold all instances of FEE.
        /// </summary>
        /// <returns>A FEES container.</returns>
        private FEES CreateFees()
        {
            var fees = new FEES();

            var sellerFeeIds = this.DataLoan.sSellerResponsibleClosingCostSet.GetFees(null).Select(f => f.UniqueId);
            bool shouldAlwaysIncludeOCompFee = this.DocumentType == IntegratedDisclosureDocumentBase.LoanEstimate && this.DataLoan.BrokerDB.IncludeLenderPaidOriginatorCompensationInMismo33Payload;

            CombinedClosingCostSet actualFeeSet = this.DataLoan.GetCombinedClosingCostSet(shouldAlwaysIncludeOCompFee);
            foreach (BorrowerClosingCostFee closingFee in actualFeeSet.GetFees(null))
            {
                bool isIgnoredFee = this.ExportData.FeeIgnoreList != null
                    && this.ExportData.FeeIgnoreList.Any(id => id == (closingFee.SourceFeeTypeId ?? closingFee.ClosingCostFeeTypeId));

                bool isZeroFee = closingFee.TotalAmount == 0M
                    && closingFee.Payments.All(payment => payment.Amount == 0M);

                bool isExcludedForIDS = this.ExportData.Options.DocumentVendor == E_DocumentVendor.IDS
                    && this.DocumentType == IntegratedDisclosureDocumentBase.LoanEstimate
                    && sellerFeeIds.Contains(closingFee.UniqueId);

                if (isIgnoredFee || isZeroFee || isExcludedForIDS)
                {
                    continue;
                }

                CAgentFields beneficiaryContact = null;
                if (closingFee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.DataLoan.GetAgentFields(closingFee.BeneficiaryAgentId);
                }

                var estimatedFee = (BorrowerClosingCostFee)this.EstimatedFeeSet?.FindFeeByTypeId(closingFee.ClosingCostFeeTypeId);
                var estimatedAmount = estimatedFee?.TotalAmount_rep ?? string.Empty;

                FEE fee = this.CreateFee(closingFee, estimatedAmount, this.DocumentType, beneficiaryContact, this.DataLoan.sAvailableSettlementServiceProviders, this.DataLoan.sLenderPaidFeeDiscloseLocationT, GetNextSequenceNumber(fees.FeeList), this.ExportData.Options.DocumentVendor, this.DataLoan.GetLiveLoanVersion(), this.DataLoan.sLT);
                fees.FeeList.Add(fee);
            }

            return fees;
        }

        /// <summary>
        /// Creates a container holding data on a fee.
        /// </summary>
        /// <param name="closingFee">An instance of a closing cost fee.</param>
        /// <param name="estimatedAmount">The estimated amount of the fee.</param>
        /// <param name="documentType">Indicates the TRID document type the fees are associated with.</param>
        /// <param name="beneficiaryContact">The contact record associated with the beneficiary of the fee.</param>
        /// <param name="availableProviders">The available settlement service providers for the loan.</param>
        /// <param name="creditDiscloseLocation">The document that will disclose the lender credit.</param>
        /// <param name="sequence">The sequence number of the fee among the list of fees.</param>
        /// <param name="vendor">The vendor target, used for vendor-specific configuration.</param>
        /// <param name="liveLoanVersion">The live (not archived) version of the loan.</param>
        /// <param name="loanType">The loan type.</param>
        /// <returns>A FEE container.</returns>
        private FEE CreateFee(BorrowerClosingCostFee closingFee, string estimatedAmount, IntegratedDisclosureDocumentBase documentType, CAgentFields beneficiaryContact, AvailableSettlementServiceProviders availableProviders, E_LenderCreditDiscloseLocationT creditDiscloseLocation, int sequence, E_DocumentVendor vendor, LendersOffice.Migration.LoanVersionT liveLoanVersion, E_sLT loanType)
        {
            var fee = new FEE();
            fee.SequenceNumber = sequence;
            fee.FeeDetail = CreateFeeDetail(closingFee, estimatedAmount, documentType, beneficiaryContact, loanType, vendor);
            fee.FeePaidTo = CommonBuilder.CreatePaidTo(beneficiaryContact, closingFee.BeneficiaryDescription, closingFee.BeneficiaryAgentId);
            fee.FeePayments = CreateFeePayments(closingFee.Payments, closingFee.Percent_rep, closingFee.IsFhaAllowable, closingFee.QmAmount > 0.00m, documentType, creditDiscloseLocation, vendor, liveLoanVersion);

            if (beneficiaryContact != null && !closingFee.CanShop)
            {
                fee.SelectedServiceProvider = CreateRequiredServiceProvider(beneficiaryContact);
            }

            return fee;
        }
    }
}
