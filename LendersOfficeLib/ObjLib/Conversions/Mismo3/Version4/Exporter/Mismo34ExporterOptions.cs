﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using DataAccess;
    using LendersOffice.Integration.DocumentVendor;

    /// <summary>
    /// A collection of user-defined options for exporting MISMO 3.4.
    /// </summary>
    public class Mismo34ExporterOptions
    {
        /// <summary>
        /// Gets or sets the ID of the loan to be exported.
        /// </summary>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the lender's account ID with the vendor to receive the MISMO message. Null/empty if there is no target vendor or if they do not require a lender-level account ID.
        /// </summary>
        public string VendorAccountId { get; set; }

        /// <summary>
        /// Gets or sets the transaction ID associated with the message. Null/empty if there is no such ID.
        /// </summary>
        public string TransactionId { get; set; }

        /// <summary>
        /// Gets or sets the document package, if this is a document framework request. Null otherwise.
        /// </summary>
        public VendorConfig.DocumentPackage DocumentPackage { get; set; } = VendorConfig.DocumentPackage.InvalidPackage;

        /// <summary>
        /// Gets or sets a value indicating whether the export should include an extension to the <see cref="INTEGRATED_DISCLOSURE"/> container with fee information. Otherwise false.
        /// </summary>
        public bool IncludeIntegratedDisclosureExtension { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether the temporary archive should be used in place of the last disclosed archive. Only expected to be true for seamless.
        /// </summary>
        public bool UseTemporaryArchive { get; set; } = false;

        /// <summary>
        /// Gets or sets the user running the export.
        /// </summary>
        public Security.AbstractUserPrincipal Principal { get; set; }

        /// <summary>
        /// Gets or sets the vendor target, used for vendor-specific configuration.
        /// </summary>
        public E_DocumentVendor DocumentVendor { get; set; } = E_DocumentVendor.UnknownOtherNone;

        /// <summary>
        /// Gets or sets a value indicating whether lender export mode is enabled.
        /// </summary>
        public bool IsLenderExportMode { get; set; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether should use the MISMO 3 default namespace for the MESSAGE element.
        /// </summary>
        public bool UseMismo3DefaultNamespace { get; set; } = false;
    }
}
