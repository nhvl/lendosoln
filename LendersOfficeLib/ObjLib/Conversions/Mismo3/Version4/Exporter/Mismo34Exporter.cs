﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using DataAccess;
    using LendersOffice.Common;
    using Mismo3Specification;
    using Mismo3Specification.Version4Schema;
    using static Mismo3Specification.Mismo3Utilities;

    /// <summary>
    /// Serializes loan data to the MISMO 3.4 data standard.
    /// </summary>
    public class Mismo34Exporter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Mismo34Exporter"/> class.
        /// </summary>
        /// <param name="exporterOptions">A collection of export options.</param>
        public Mismo34Exporter(Mismo34ExporterOptions exporterOptions)
            : this(SetupLoan(exporterOptions.LoanId), exporterOptions)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Mismo34Exporter"/> class.
        /// </summary>
        /// <param name="dataLoan">A loan data object initialized with the dependencies of this exporter, with InitLoad() already called.</param>
        /// <param name="options">A collection of export options defined by the caller.</param>
        public Mismo34Exporter(CPageData dataLoan, Mismo34ExporterOptions options = null)
        {
            if (options == null)
            {
                options = new Mismo34ExporterOptions();
            }

            options.LoanId = dataLoan.sLId;
            this.DataLoan = dataLoan;

            if (this.DataLoan.BrokerDB.IsGFEandCoCVersioningEnabled)
            {
                if (options.UseTemporaryArchive)
                {
                    this.DataLoan.ApplyTemporaryArchive(options.Principal);
                }
                else if (!options.DocumentPackage.IsOfType(Integration.DocumentVendor.VendorConfig.PackageType.Closing)
                    && !options.DocumentPackage.IsUndefinedType && this.DataLoan.sHasLastDisclosedLoanEstimateArchive)
                {
                    this.DataLoan.ApplyClosingCostArchive(this.DataLoan.sLastDisclosedClosingCostArchive);
                }
            }

            this.ExportData = new Mismo34ExporterCache(dataLoan, options);
        }

        /// <summary>
        /// Gets the data loan.
        /// </summary>
        protected CPageData DataLoan { get; }

        /// <summary>
        /// Gets a collection of cached data to populate the export.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Serializes the loan associated with this exporter.
        /// </summary>
        /// <returns>A serialized MISMO 3.4 string.</returns>
        public string SerializePayload()
        {
            var message = this.CreateMessage();
            return SerializationHelper.XmlSerialize(
                message,
                includeXMLDeclaration: false,
                defaultNamespace: Mismo3Constants.MismoNamespace);
        }

        /// <summary>
        /// Creates and sets up a loan for export.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <returns>The set up loan.</returns>
        private static CPageData SetupLoan(Guid loanId)
        {
            var loan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(Mismo34Exporter));
            loan.SetFormatTarget(FormatTarget.MismoClosing);
            loan.ByPassFieldSecurityCheck = true;
            loan.InitLoad();

            return loan;
        }

        /// <summary>
        /// Creates a <see cref="MESSAGE"/> container.
        /// </summary>
        /// <returns>A <see cref="MESSAGE"/> container.</returns>
        private MESSAGE CreateMessage()
        {
            var message = new MESSAGE();
            message.DealSets = this.CreateDealSets();

            return message;
        }

        /// <summary>
        /// Creates a <see cref="DEAL_SETS"/> container.
        /// </summary>
        /// <returns>A <see cref="DEAL_SETS"/> container.</returns>
        private DEAL_SETS CreateDealSets()
        {
            var dealSets = new DEAL_SETS();
            dealSets.DealSetList.Add(this.CreateDealSet(GetNextSequenceNumber(dealSets.DealSetList)));

            return dealSets;
        }

        /// <summary>
        /// Creates a <see cref="DEAL_SET"/> container.
        /// </summary>
        /// <param name="sequenceNumber">A number uniquely identifying this element from others of its type.</param>
        /// <returns>A <see cref="DEAL_SET"/> container.</returns>
        private DEAL_SET CreateDealSet(int sequenceNumber)
        {
            var dealSet = new DEAL_SET();
            dealSet.SequenceNumber = sequenceNumber;
            dealSet.Deals = this.CreateDeals();

            return dealSet;
        }

        /// <summary>
        /// Creates the container output of this exporter.
        /// </summary>
        /// <returns>A DEALS container.</returns>
        private DEALS CreateDeals()
        {
            var deals = new DEALS();

            var dealExporter = new DealBuilder(this.DataLoan, this.ExportData);
            deals.DealList.Add(dealExporter.CreateContainer(GetNextSequenceNumber(deals.DealList)));

            return deals;
        }
    }
}