﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Class for building an ESCROW container.
    /// </summary>
    public class EscrowBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EscrowBuilder"/> class.
        /// </summary>
        /// <param name="dataLoan">The data loan.</param>
        /// <param name="exportData">A collection of data used throughout the exporter.</param>
        public EscrowBuilder(CPageData dataLoan, Mismo34ExporterCache exportData)
        {
            this.DataLoan = dataLoan;
            this.ExportData = exportData;
        }

        /// <summary>
        /// Gets the loan to pull data from.
        /// </summary>
        protected CPageData DataLoan { get; private set; }

        /// <summary>
        /// Gets a collection of data used throughout the exporter.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Creates an ESCROW container.
        /// </summary>
        /// <returns>The escrow container.</returns>
        public ESCROW CreateContainer()
        {
            return this.CreateEscrow();
        }

        /// <summary>
        /// Retrieves the escrow payment frequency value corresponding to the disbursement interval.
        /// </summary>
        /// <param name="frequency">The payment interval.</param>
        /// <returns>The corresponding escrow payment frequency value.</returns>
        private static EscrowPaymentFrequencyBase GetEscrowPaymentFrequencyBaseValue(E_DisbursementRepIntervalT frequency)
        {
            switch (frequency)
            {
                case E_DisbursementRepIntervalT.Annual:
                case E_DisbursementRepIntervalT.AnnuallyInClosingMonth:
                    return EscrowPaymentFrequencyBase.Annual;
                case E_DisbursementRepIntervalT.Monthly:
                    return EscrowPaymentFrequencyBase.Monthly;
                default:
                    throw new UnhandledEnumException(frequency);
            }
        }

        /// <summary>
        /// Retrieves the escrow premium rate percent basis value corresponding to the given percent basis.
        /// </summary>
        /// <param name="percentBasis">The percent basis.</param>
        /// <returns>The corresponding escrow premium rate percent basis value.</returns>
        private static EscrowPremiumRatePercentBasisBase GetEscrowPremiumRatePercentBasisBaseValue(E_PercentBaseT percentBasis)
        {
            switch (percentBasis)
            {
                case E_PercentBaseT.AppraisalValue:
                    return EscrowPremiumRatePercentBasisBase.PropertyValuationAmount;
                case E_PercentBaseT.LoanAmount:
                    return EscrowPremiumRatePercentBasisBase.NoteAmount;
                case E_PercentBaseT.SalesPrice:
                    return EscrowPremiumRatePercentBasisBase.PurchasePriceAmount;
                case E_PercentBaseT.TotalLoanAmount:
                    return EscrowPremiumRatePercentBasisBase.TotalLoanAmount;
                case E_PercentBaseT.OriginalCost:
                case E_PercentBaseT.AverageOutstandingBalance:
                case E_PercentBaseT.DecliningRenewalsAnnually:
                case E_PercentBaseT.DecliningRenewalsMonthly:
                case E_PercentBaseT.AllYSP:
                    return EscrowPremiumRatePercentBasisBase.Other;
                default:
                    throw new UnhandledEnumException(percentBasis);
            }
        }

        /// <summary>
        /// Converts the given paid by indicator to the corresponding EscrowPaidByBase.
        /// </summary>
        /// <param name="paidBy">The type of entity that pays the escrow amount.</param>
        /// <returns>The EscrowPaidByBase corresponding to the given paid by type.</returns>
        private static EscrowPaidByBase GetEscrowPaidByBase(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return EscrowPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return EscrowPaidByBase.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return EscrowPaidByBase.LenderPremium;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return EscrowPaidByBase.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return EscrowPaidByBase.Seller;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return EscrowPaidByBase.Blank;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Converts the given paid by indicator to the corresponding EscrowPremiumPaidByBase.
        /// </summary>
        /// <param name="paidBy">The type of entity that pays the escrow amount.</param>
        /// <returns>The EscrowPremiumPaidByBase corresponding to the given paid by type.</returns>
        private static EscrowPremiumPaidByBase GetEscrowPremiumPaidByBase(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return EscrowPremiumPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return EscrowPremiumPaidByBase.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return EscrowPremiumPaidByBase.Seller;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                case E_ClosingCostFeePaymentPaidByT.Other:
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return EscrowPremiumPaidByBase.Blank;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Converts the given escrow payment timing to the corresponding EscrowPremiumPaymentBase.
        /// </summary>
        /// <param name="timing">The timing of the escrow payment.</param>
        /// <returns>The EscrowPremiumPaymentBase that corresponds to the given escrow payment timing.</returns>
        private static EscrowPremiumPaymentBase GetEscrowPremiumPaymentBase(E_GfeClosingCostFeePaymentTimingT timing)
        {
            switch (timing)
            {
                case E_GfeClosingCostFeePaymentTimingT.AtClosing:
                    return EscrowPremiumPaymentBase.CollectAtClosing;
                case E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing:
                    return EscrowPremiumPaymentBase.PaidOutsideOfClosing;
                case E_GfeClosingCostFeePaymentTimingT.LeaveBlank:
                    return EscrowPremiumPaymentBase.Blank;
                default:
                    throw new UnhandledEnumException(timing);
            }
        }

        /// <summary>
        /// Retrieves the escrow item payment paid by value.
        /// </summary>
        /// <param name="paidBy">The entity who made the payment.</param>
        /// <returns>The corresponding paid by value.</returns>
        private static EscrowItemPaymentPaidByBase GetEscrowItemPaymentPaidByBaseValue(E_ClosingCostFeePaymentPaidByT paidBy)
        {
            switch (paidBy)
            {
                case E_ClosingCostFeePaymentPaidByT.Borrower:
                case E_ClosingCostFeePaymentPaidByT.BorrowerFinance:
                    return EscrowItemPaymentPaidByBase.Buyer;
                case E_ClosingCostFeePaymentPaidByT.Broker:
                    return EscrowItemPaymentPaidByBase.Broker;
                case E_ClosingCostFeePaymentPaidByT.Lender:
                    return EscrowItemPaymentPaidByBase.Lender;
                case E_ClosingCostFeePaymentPaidByT.Seller:
                    return EscrowItemPaymentPaidByBase.Seller;
                case E_ClosingCostFeePaymentPaidByT.Other:
                    return EscrowItemPaymentPaidByBase.ThirdParty;
                case E_ClosingCostFeePaymentPaidByT.LeaveBlank:
                    return EscrowItemPaymentPaidByBase.Blank;
                default:
                    throw new UnhandledEnumException(paidBy);
            }
        }

        /// <summary>
        /// Retrieves the escrow item payment timing value corresponding to the given disbursement payment type.
        /// </summary>
        /// <param name="timing">The timing of the payment.</param>
        /// <returns>The corresponding escrow item payment timing value.</returns>
        private static EscrowItemPaymentTimingBase GetEscrowItemPaymentTimingBaseValue(E_GfeClosingCostFeePaymentTimingT timing)
        {
            switch (timing)
            {
                case E_GfeClosingCostFeePaymentTimingT.AtClosing:
                    return EscrowItemPaymentTimingBase.AtClosing;
                case E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing:
                    return EscrowItemPaymentTimingBase.BeforeClosing;
                case E_GfeClosingCostFeePaymentTimingT.LeaveBlank:
                    return EscrowItemPaymentTimingBase.Blank;
                default:
                    throw new UnhandledEnumException(timing);
            }
        }

        /// <summary>
        /// Retrieves the escrow item type corresponding to a tax type.
        /// </summary>
        /// <param name="taxType">A tax type.</param>
        /// <returns>The corresponding escrow item type.</returns>
        private static EscrowItemBase GetEscrowItemBaseValue_Tax(E_TaxTableTaxT taxType)
        {
            switch (taxType)
            {
                case E_TaxTableTaxT.Borough:
                case E_TaxTableTaxT.City:
                    return EscrowItemBase.CityPropertyTax;
                case E_TaxTableTaxT.County:
                    return EscrowItemBase.CountyPropertyTax;
                case E_TaxTableTaxT.FireDist:
                case E_TaxTableTaxT.LocalImprovementDist:
                case E_TaxTableTaxT.MunicipalUtilDist:
                case E_TaxTableTaxT.SpecialAssessmentDist:
                case E_TaxTableTaxT.WasteFeeDist:
                    return EscrowItemBase.DistrictPropertyTax;
                case E_TaxTableTaxT.School:
                    return EscrowItemBase.SchoolPropertyTax;
                case E_TaxTableTaxT.Town:
                    return EscrowItemBase.TownPropertyTax;
                case E_TaxTableTaxT.Township:
                    return EscrowItemBase.TownshipPropertyTax;
                case E_TaxTableTaxT.Village:
                    return EscrowItemBase.VillagePropertyTax;
                case E_TaxTableTaxT.LeaveBlank:
                case E_TaxTableTaxT.Miscellaneous:
                case E_TaxTableTaxT.Utility:
                case E_TaxTableTaxT.Water_Irrigation:
                case E_TaxTableTaxT.Water_Sewer:
                    return EscrowItemBase.Other;
                default:
                    throw new UnhandledEnumException(taxType);
            }
        }

        /// <summary>
        /// Retrieves the Escrow Item type corresponding to the expense type.
        /// </summary>
        /// <param name="type">The housing expense type.</param>
        /// <returns>The corresponding <see cref="EscrowItemBase" /> enumeration.</returns>
        private static EscrowItemBase GetEscrowItemBaseValue(E_HousingExpenseTypeT type)
        {
            switch (type)
            {
                case E_HousingExpenseTypeT.HazardInsurance:
                    return EscrowItemBase.HazardInsurance;
                case E_HousingExpenseTypeT.FloodInsurance:
                    return EscrowItemBase.FloodInsurance;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return EscrowItemBase.WindstormInsurance;
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                    return EscrowItemBase.HomeownersInsurance;
                case E_HousingExpenseTypeT.RealEstateTaxes:
                    return EscrowItemBase.PropertyTax;
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return EscrowItemBase.SchoolPropertyTax;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return EscrowItemBase.HomeownersAssociationDues;
                case E_HousingExpenseTypeT.GroundRent:
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.Unassigned:
                    return EscrowItemBase.Other;
                default:
                    throw new UnhandledEnumException(type);
            }
        }

        /// <summary>
        /// Creates an escrow item enumeration object from the given tax or expense type.
        /// </summary>
        /// <param name="taxType">The tax type.</param>
        /// <param name="escrowType">The expense type.</param>
        /// <param name="escrowDescription">The text to populate to a display label attribute on the MISMO enum container. Requested by DocuTech for describing certain enums (case 463886).</param>
        /// <returns>A serializable <see cref="MISMOEnum{EscrowItemBase}" /> object.</returns>
        private static MISMOEnum<EscrowItemBase> ToEscrowItemEnum(E_TaxTableTaxT taxType, E_HousingExpenseTypeT escrowType, string escrowDescription)
        {
            var typeBasedOnTax = GetEscrowItemBaseValue_Tax(taxType);

            if (typeBasedOnTax != EscrowItemBase.Other)
            {
                return ToMismoEnum(typeBasedOnTax, displayLabelText: escrowDescription);
            }
            else
            {
                return ToMismoEnum(GetEscrowItemBaseValue(escrowType), displayLabelText: escrowDescription);
            }
        }

        /// <summary>
        /// Creates ESCROW container with loan escrow detail provided.
        /// </summary>
        /// <returns>Returns ESCROW container with loan escrow detail provided.</returns>
        private ESCROW CreateEscrow()
        {
            if (this.DataLoan == null)
            {
                return null;
            }

            var escrow = new ESCROW();
            escrow.EscrowDetail = this.CreateEscrowDetail();
            escrow.EscrowItems = this.CreateEscrowItems();
            return escrow;
        }

        /// <summary>
        /// Creates ESCROW_DETAIL container with loan escrow detail provided.
        /// </summary>
        /// <returns>Returns ESCROW_DETAIL container with loan escrow detail provided.</returns>
        private ESCROW_DETAIL CreateEscrowDetail()
        {
            var escrowDetail = new ESCROW_DETAIL();
            escrowDetail.InitialEscrowDepositIncludesOtherDescription = ToMismoString(this.DataLoan.sGfeHasImpoundDepositDescription);
            escrowDetail.EscrowAggregateAccountingAdjustmentAmount = ToMismoAmount(this.DataLoan.sAggregateAdjRsrv_rep);
            escrowDetail.EscrowCushionNumberOfMonthsCount = ToMismoCount(this.DataLoan.sEscrowCushionMonths_rep, null) ?? ToMismoCount("0", null);
            escrowDetail.EscrowAccountMinimumBalanceAmount = ToMismoAmount(this.DataLoan.sAggrEscrowCushionRequired_rep);

            return escrowDetail;
        }

        /// <summary>
        /// Creates a container to hold all escrow expenses.
        /// </summary>
        /// <returns>An ESCROW_ITEMS container.</returns>
        private ESCROW_ITEMS CreateEscrowItems()
        {
            var escrowItems = new ESCROW_ITEMS();

            if (this.DataLoan.sMInsRsrvEscrowedTri == E_TriState.Yes && (this.DataLoan.sProMIns > 0.00m || this.DataLoan.sMInsRsrv > 0.00m))
            {
                escrowItems.EscrowItemList.Add(this.CreateEscrowItem_MI(GetNextSequenceNumber(escrowItems.EscrowItemList)));
            }

            if (this.DataLoan.sHousingExpenses != null)
            {
                foreach (BaseHousingExpense expense in this.DataLoan.sHousingExpenses.ExpensesToUse.Where(e => (e.AnnualAmt != 0.00m || e.MonthlyAmtTotal != 0.00m || e.ReserveAmt != 0.00m) && e.IsEscrowedAtClosing == E_TriState.Yes))
                {
                    escrowItems.EscrowItemList.Add(this.CreateEscrowItem(expense, GetNextSequenceNumber(escrowItems.EscrowItemList)));
                }
            }

            return escrowItems;
        }

        /// <summary>
        /// Creates a container holding data on a specific escrow item.
        /// </summary>
        /// <param name="expense">The escrowed expense.</param>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>An ESCROW_ITEM container.</returns>
        private ESCROW_ITEM CreateEscrowItem(BaseHousingExpense expense, int sequence)
        {
            BorrowerClosingCostFee escrowFee = expense.GetClosingCostFee(E_IntegratedDisclosureSectionT.SectionG);
            BorrowerClosingCostFee archivedFee = null;
            if (this.DataLoan.sTridTargetRegulationVersionT == ObjLib.TRID2.TridTargetRegulationVersionT.TRID2017 && this.DataLoan.sAppliedCCArchiveLinkedArchiveId != Guid.Empty)
            {
                // If there is a linked pair of archives, the archive applied to the loan contains the
                // actual fee amounts while the linked archive contains the tolerance data.
                var linkedArchive = this.DataLoan.GetClosingCostArchiveById(this.DataLoan.sAppliedCCArchiveLinkedArchiveId);
                archivedFee = linkedArchive?.GetClosingCostSet(this.DataLoan.m_convertLos)?.GetFees(f => f.ClosingCostFeeTypeId == expense.GetClosingCostFee900Or1000Type(E_IntegratedDisclosureSectionT.SectionG))?.FirstOrDefault() as BorrowerClosingCostFee;
            }

            if (archivedFee == null)
            {
                archivedFee = this.DataLoan.LastDisclosedLoanEstimateArchive?.GetClosingCostSet(this.DataLoan.m_convertLos)?.GetFees(f => f.ClosingCostFeeTypeId == expense.GetClosingCostFee900Or1000Type(E_IntegratedDisclosureSectionT.SectionG))?.FirstOrDefault() as BorrowerClosingCostFee;
            }

            var escrowItem = new ESCROW_ITEM();
            escrowItem.EscrowItemDetail = this.CreateEscrowItemDetail(expense, escrowFee, archivedFee);
            escrowItem.SequenceNumber = sequence;

            if (expense.AnnualAmtCalcType == E_AnnualAmtCalcTypeT.Disbursements)
            {
                escrowItem.EscrowItemDisbursements = CreateEscrowItemDisbursements(expense.ActualDisbursementsList);
            }
            else
            {
                escrowItem.EscrowItemDisbursements = CreateEscrowItemDisbursements(expense.ProjectedDisbursements);
            }

            if (escrowFee != null)
            {
                CAgentFields beneficiaryContact = null;
                if (escrowFee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.DataLoan.GetAgentFields(escrowFee.BeneficiaryAgentId);
                }

                escrowItem.EscrowPaidTo = CreatePaidTo(beneficiaryContact, escrowFee.BeneficiaryDescription, escrowFee.BeneficiaryAgentId);
                escrowItem.EscrowItemPayments = this.CreateEscrowItemPayments(escrowFee.Payments, escrowFee.QmAmount > 0.00m);
            }

            return escrowItem;
        }

        /// <summary>
        /// Creates a container with details on an escrowed expense.
        /// </summary>
        /// <param name="expense">The escrowed expense.</param>
        /// <param name="escrowFee">The fee associated with the escrow.</param>
        /// <param name="archivedFee">The archived fee associated with the escrow.</param>
        /// <returns>An ESCROW_ITEM_DETAIL container.</returns>
        private ESCROW_ITEM_DETAIL CreateEscrowItemDetail(BaseHousingExpense expense, BorrowerClosingCostFee escrowFee, BorrowerClosingCostFee archivedFee)
        {
            var escrowItemDetail = new ESCROW_ITEM_DETAIL();
            escrowItemDetail.EscrowAnnualPaymentAmount = ToMismoAmount(expense.AnnualAmt_rep);
            escrowItemDetail.EscrowCollectedNumberOfMonthsCount = ToMismoCount(expense.ReserveMonths_rep, null);
            escrowItemDetail.EscrowItemTotalReserveCollectedAtClosingAmount = ToMismoAmount(expense.ReserveAmt_rep);

            if (expense.IsTax)
            {
                escrowItemDetail.EscrowItemType = ToEscrowItemEnum(expense.TaxType, expense.HousingExpenseType, expense.DescriptionOrDefault);
            }
            else
            {
                escrowItemDetail.EscrowItemType = ToMismoEnum(GetEscrowItemBaseValue(expense.HousingExpenseType), displayLabelText: expense.DescriptionOrDefault);
            }

            if (escrowItemDetail.EscrowItemType != null && escrowItemDetail.EscrowItemType.EnumValue == EscrowItemBase.Other)
            {
                escrowItemDetail.EscrowItemTypeOtherDescription = ToMismoString(expense.DescriptionOrDefault);
            }

            escrowItemDetail.EscrowMonthlyPaymentAmount = ToMismoAmount(expense.MonthlyAmtServicing_rep);
            escrowItemDetail.EscrowPaymentFrequencyType = ToMismoEnum(GetEscrowPaymentFrequencyBaseValue(expense.DisbursementRepInterval), isSensitiveData: false);

            if (escrowItemDetail.EscrowPaymentFrequencyType?.IsSetToOther ?? false)
            {
                escrowItemDetail.EscrowPaymentFrequencyTypeOtherDescription = ToMismoString(expense.DisbursementRepInterval);
            }

            escrowItemDetail.EscrowPremiumDurationMonthsCount = ToMismoCount(expense.PrepaidMonths_rep, null);
            escrowItemDetail.EscrowPremiumRatePercent = ToMismoPercent(expense.AnnualAmtCalcBasePerc_rep);
            escrowItemDetail.EscrowPremiumRatePercentBasisType = ToMismoEnum(GetEscrowPremiumRatePercentBasisBaseValue(expense.AnnualAmtCalcBaseType), isSensitiveData: false);

            if (escrowItemDetail.EscrowPremiumRatePercentBasisType != null && escrowItemDetail.EscrowPremiumRatePercentBasisType.EnumValue == EscrowPremiumRatePercentBasisBase.Other)
            {
                escrowItemDetail.EscrowPremiumRatePercentBasisTypeOtherDescription = ToMismoString(GetXmlEnumName(expense.AnnualAmtCalcBaseType));
            }

            escrowItemDetail.EscrowItemEstimatedTotalAmount = ToMismoAmount(archivedFee?.TotalAmount_rep);
            if (escrowFee != null)
            {
                if (escrowFee.Payments != null && escrowFee.Payments.Count(p => p != null && p.Amount > 0.00m) > 0)
                {
                    var payment = escrowFee.Payments.First(p => p != null && p.Amount > 0.00m);

                    var escrowPaidByBase = GetEscrowPaidByBase(payment.GetPaidByType(this.DataLoan.sLenderPaidFeeDiscloseLocationT, this.ExportData.IsClosingPackage));
                    escrowItemDetail.EscrowPaidByType = escrowPaidByBase == EscrowPaidByBase.Blank ? null : ToMismoEnum(escrowPaidByBase, isSensitiveData: false);

                    var escrowPremiumPaidByTypeBase = GetEscrowPremiumPaidByBase(payment.GetPaidByType(this.DataLoan.sLenderPaidFeeDiscloseLocationT, this.ExportData.IsClosingPackage));
                    escrowItemDetail.EscrowPremiumPaidByType = escrowPremiumPaidByTypeBase == EscrowPremiumPaidByBase.Blank ? null : ToMismoEnum(escrowPremiumPaidByTypeBase, isSensitiveData: false);

                    var escrowPremiumPaymentBase = GetEscrowPremiumPaymentBase(payment.GfeClosingCostFeePaymentTimingT);
                    escrowItemDetail.EscrowPremiumPaymentType = escrowPremiumPaymentBase == EscrowPremiumPaymentBase.Blank ? null : ToMismoEnum(escrowPremiumPaymentBase, isSensitiveData: false);
                }

                escrowItemDetail.EscrowSpecifiedHUD1LineNumberValue = ToMismoValue(escrowFee.HudLine_rep);

                MISMOString paidToTypeOtherDescription = null;
                CAgentFields beneficiaryContact = null;
                if (escrowFee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.DataLoan.GetAgentFields(escrowFee.BeneficiaryAgentId);
                }

                escrowItemDetail.FeePaidToType = GetFeePaidToType(
                    beneficiaryContact,
                    escrowFee.IsAffiliate,
                    escrowFee.IsThirdParty,
                    escrowFee.BeneficiaryType,
                    escrowFee.Beneficiary,
                    escrowFee.BeneficiaryDescription,
                    out paidToTypeOtherDescription);

                if (paidToTypeOtherDescription != null)
                {
                    escrowItemDetail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
                }
            }

            var feeTypeId = escrowFee == null ? Guid.Empty : escrowFee.ClosingCostFeeTypeId;
            escrowItemDetail.Extension = this.CreateEscrowItemDetailExtension(expense, feeTypeId);

            return escrowItemDetail;
        }

        /// <summary>
        /// Creates an ESCROW_ITEM container with information on the mortgage insurance premium escrows.
        /// </summary>
        /// <param name="sequence">The sequence number.</param>
        /// <returns>An ESCROW_ITEM container.</returns>
        private ESCROW_ITEM CreateEscrowItem_MI(int sequence)
        {
            var escrowItem = new ESCROW_ITEM();
            var miEscrowFee = GetMortgageInsuranceEscrowFee(this.DataLoan);

            escrowItem.SequenceNumber = sequence;
            escrowItem.EscrowItemDetail = this.CreateEscrowItemDetail_MI(miEscrowFee);

            if (miEscrowFee != null)
            {
                escrowItem.EscrowItemPayments = this.CreateEscrowItemPayments(miEscrowFee.Payments, miEscrowFee.QmAmount > 0.00m);
            }

            return escrowItem;
        }

        /// <summary>
        /// Creates a container with details on escrowed mortgage insurance premiums.
        /// </summary>
        /// <param name="miEscrowFee">The closing cost fee associated with the MI escrow row.</param>
        /// <returns>An ESCROW_ITEM_DETAIL container.</returns>
        private ESCROW_ITEM_DETAIL CreateEscrowItemDetail_MI(BorrowerClosingCostFee miEscrowFee)
        {
            var escrowItemDetail = new ESCROW_ITEM_DETAIL();
            escrowItemDetail.EscrowCollectedNumberOfMonthsCount = ToMismoCount(this.DataLoan.sMInsRsrvMon_rep, null);
            escrowItemDetail.EscrowItemTotalReserveCollectedAtClosingAmount = ToMismoAmount(this.DataLoan.sMInsRsrv_rep);
            escrowItemDetail.EscrowItemType = ToMismoEnum(EscrowItemBase.MortgageInsurance, displayLabelText: "Mortgage Insurance");
            escrowItemDetail.EscrowMonthlyPaymentAmount = ToMismoAmount(this.DataLoan.sProMIns_rep);
            escrowItemDetail.EscrowPaymentFrequencyType = ToMismoEnum(GetEscrowPaymentFrequencyBaseValue(E_DisbursementRepIntervalT.Monthly), isSensitiveData: false);
            escrowItemDetail.EscrowPremiumAmount = ToMismoAmount(this.DataLoan.sProMIns_rep);
            escrowItemDetail.EscrowPremiumRatePercent = ToMismoPercent(this.DataLoan.sProMInsR_rep);
            escrowItemDetail.EscrowPremiumRatePercentBasisType = ToMismoEnum(GetEscrowPremiumRatePercentBasisBaseValue(this.DataLoan.sProMInsT), isSensitiveData: false);

            if (escrowItemDetail.EscrowPremiumRatePercentBasisType?.IsSetToOther ?? false)
            {
                escrowItemDetail.EscrowPremiumRatePercentBasisTypeOtherDescription = ToMismoString(this.DataLoan.sProMInsT);
            }

            if (miEscrowFee != null)
            {
                if (miEscrowFee.Payments != null && miEscrowFee.Payments.Count(p => p != null && p.Amount > 0.00m) > 0)
                {
                    var payment = miEscrowFee.Payments.First(p => p != null && p.Amount > 0.00m);

                    var paidByEnum = GetEscrowPaidByBase(payment.GetPaidByType(this.DataLoan.sLenderPaidFeeDiscloseLocationT, this.ExportData.IsClosingPackage));
                    escrowItemDetail.EscrowPaidByType = paidByEnum == EscrowPaidByBase.Blank ? null : ToMismoEnum(paidByEnum, isSensitiveData: false);

                    var premiumPaidby = GetEscrowPremiumPaidByBase(payment.GetPaidByType(this.DataLoan.sLenderPaidFeeDiscloseLocationT, this.ExportData.IsClosingPackage));
                    escrowItemDetail.EscrowPremiumPaidByType = premiumPaidby == EscrowPremiumPaidByBase.Blank ? null : ToMismoEnum(premiumPaidby, isSensitiveData: false);

                    var premiumPayment = GetEscrowPremiumPaymentBase(payment.GfeClosingCostFeePaymentTimingT);
                    escrowItemDetail.EscrowPremiumPaymentType = premiumPayment == EscrowPremiumPaymentBase.Blank ? null : ToMismoEnum(premiumPayment, isSensitiveData: false);
                }

                escrowItemDetail.EscrowSpecifiedHUD1LineNumberValue = ToMismoValue(miEscrowFee.HudLine_rep);

                MISMOString paidToTypeOtherDescription = null;
                CAgentFields beneficiaryContact = null;
                if (miEscrowFee.BeneficiaryAgentId != Guid.Empty)
                {
                    beneficiaryContact = this.DataLoan.GetAgentFields(miEscrowFee.BeneficiaryAgentId);
                }

                escrowItemDetail.FeePaidToType = GetFeePaidToType(
                    beneficiaryContact,
                    miEscrowFee.IsAffiliate,
                    miEscrowFee.IsThirdParty,
                    miEscrowFee.BeneficiaryType,
                    miEscrowFee.Beneficiary,
                    miEscrowFee.BeneficiaryDescription,
                    out paidToTypeOtherDescription);

                if (paidToTypeOtherDescription != null)
                {
                    escrowItemDetail.FeePaidToTypeOtherDescription = paidToTypeOtherDescription;
                }
            }

            var feeTypeId = miEscrowFee == null ? Guid.Empty : miEscrowFee.ClosingCostFeeTypeId;
            escrowItemDetail.Extension = this.CreateEscrowItemDetailExtension(null, feeTypeId);

            return escrowItemDetail;
        }

        /// <summary>
        /// Creates a container holding a list of payments for an escrowed expense.
        /// </summary>
        /// <param name="payments">The set of payments for an escrow.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <returns>An ESCROW_ITEM_PAYMENTS container.</returns>
        private ESCROW_ITEM_PAYMENTS CreateEscrowItemPayments(IEnumerable<LoanClosingCostFeePayment> payments, bool qm)
        {
            var escrowItemPayments = new ESCROW_ITEM_PAYMENTS();

            foreach (BorrowerClosingCostFeePayment payment in payments.Where(p => p.Amount > 0.00m))
            {
                escrowItemPayments.EscrowItemPaymentList.Add(this.CreateEscrowItemPayment(payment, qm, GetNextSequenceNumber(escrowItemPayments.EscrowItemPaymentList)));
            }

            return escrowItemPayments;
        }

        /// <summary>
        /// Creates a container holding data on a specific payment for an escrowed expense.
        /// </summary>
        /// <param name="payment">An escrow payment.</param>
        /// <param name="qm">True if the payment is included in the regulation Z points and fees calculation.</param>
        /// <param name="sequence">The sequence number of the payment among the list of payments.</param>
        /// <returns>An ESCROW_ITEM_PAYMENT container.</returns>
        private ESCROW_ITEM_PAYMENT CreateEscrowItemPayment(BorrowerClosingCostFeePayment payment, bool qm, int sequence)
        {
            var escrowItemPayment = new ESCROW_ITEM_PAYMENT();

            escrowItemPayment.EscrowItemActualPaymentAmount = ToMismoAmount(payment.Amount_rep);
            escrowItemPayment.EscrowItemPaymentPaidByType = ToMismoEnum(GetEscrowItemPaymentPaidByBaseValue(payment.GetPaidByType(this.DataLoan.sLenderPaidFeeDiscloseLocationT, this.ExportData.IsClosingPackage)), isSensitiveData: false);

            if (escrowItemPayment.EscrowItemPaymentPaidByType?.IsSetToOther ?? false)
            {
                escrowItemPayment.EscrowItemPaymentPaidByTypeOtherDescription = ToMismoString(GetXmlEnumName(payment.GetPaidByType(this.DataLoan.sLenderPaidFeeDiscloseLocationT, this.ExportData.IsClosingPackage)));
            }

            var itemPaymentTiming = GetEscrowItemPaymentTimingBaseValue(payment.GfeClosingCostFeePaymentTimingT);
            escrowItemPayment.EscrowItemPaymentTimingType = itemPaymentTiming == EscrowItemPaymentTimingBase.Blank ? null : ToMismoEnum(itemPaymentTiming, isSensitiveData: false);

            if (escrowItemPayment.EscrowItemPaymentTimingType?.IsSetToOther ?? false)
            {
                escrowItemPayment.EscrowItemPaymentTimingTypeOtherDescription = ToMismoString(payment.GfeClosingCostFeePaymentTimingT);
            }

            escrowItemPayment.PaymentFinancedIndicator = ToMismoIndicator(payment.IsFinanced);
            escrowItemPayment.PaymentIncludedInAPRIndicator = ToMismoIndicator(payment.IsIncludedInApr(this.DataLoan.GetLiveLoanVersion()));
            escrowItemPayment.RegulationZPointsAndFeesIndicator = ToMismoIndicator(qm);
            escrowItemPayment.Extension = this.CreateEscrowItemPaymentExtension(payment.Id.ToString());
            escrowItemPayment.SequenceNumber = sequence;

            return escrowItemPayment;
        }

        /// <summary>
        /// Creates an extension container with further details on an escrow item.
        /// </summary>
        /// <param name="expense">The expense object.</param>
        /// <param name="feeTypeId">The unique closing cost fee type associated with the escrow.</param>
        /// <returns>An ESCROW_ITEM_DETAIL container.</returns>
        private ESCROW_ITEM_DETAIL_EXTENSION CreateEscrowItemDetailExtension(BaseHousingExpense expense, Guid feeTypeId)
        {
            var escrowItemDetailExtension = new ESCROW_ITEM_DETAIL_EXTENSION();

            if (expense != null)
            {
                escrowItemDetailExtension.Mismo = this.CreateMismoEscrowItemDetailExtension(expense);
            }

            if (feeTypeId != Guid.Empty)
            {
                escrowItemDetailExtension.Other = this.CreateLQBEscrowItemDetailExtension(feeTypeId);
            }

            return escrowItemDetailExtension;
        }

        /// <summary>
        /// Creates an extension to the ESCROW_ITEM_DETAIL container with proprietary datapoints.
        /// </summary>
        /// <param name="feeTypeId">The unique closing cost fee type associated with the escrow.</param>
        /// <returns>An LQB_ESCROW_ITEM_DETAIL_EXTENSION container.</returns>
        private LQB_ESCROW_ITEM_DETAIL_EXTENSION CreateLQBEscrowItemDetailExtension(Guid feeTypeId)
        {
            var extension = new LQB_ESCROW_ITEM_DETAIL_EXTENSION();

            if (feeTypeId != Guid.Empty)
            {
                extension.ClosingCostFeeTypeID = ToMismoString(feeTypeId.ToString());
            }

            return extension;
        }

        /// <summary>
        /// Creates an extension to the ESCROW_ITEM_DETAIL container with MISMO datapoints.
        /// </summary>
        /// <param name="expense">The housing expense.</param>
        /// <returns>A MISMO_ESCROW_ITEM_DETAIL_EXTENSION container.</returns>
        private MISMO_ESCROW_ITEM_DETAIL_EXTENSION CreateMismoEscrowItemDetailExtension(BaseHousingExpense expense)
        {
            var extension = new MISMO_ESCROW_ITEM_DETAIL_EXTENSION();
            extension.EscrowDetailCushionNumberOfMonths = ToMismoCount(expense.CushionMonths_rep, null);

            return extension;
        }

        /// <summary>
        /// Creates an extension container holding data on an escrow item payment.
        /// </summary>
        /// <param name="paymentId">The escrow item payment ID.</param>
        /// <returns>A serializable <see cref="ESCROW_ITEM_PAYMENT_EXTENSION"/> container.</returns>
        private ESCROW_ITEM_PAYMENT_EXTENSION CreateEscrowItemPaymentExtension(string paymentId)
        {
            var extension = new ESCROW_ITEM_PAYMENT_EXTENSION();
            extension.Other = this.CreateLqbEscrowItemPaymentExtension(paymentId);

            return extension;
        }

        /// <summary>
        /// Creates a proprietary extension container holding data on an escrow item payment.
        /// </summary>
        /// <param name="paymentId">The escrow item payment ID.</param>
        /// <returns>A serializable <see cref="LQB_ESCROW_ITEM_PAYMENT_EXTENSION"/> container.</returns>
        private LQB_ESCROW_ITEM_PAYMENT_EXTENSION CreateLqbEscrowItemPaymentExtension(string paymentId)
        {
            var extension = new LQB_ESCROW_ITEM_PAYMENT_EXTENSION();
            extension.Id = ToMismoString(paymentId);

            return extension;
        }
    }
}
