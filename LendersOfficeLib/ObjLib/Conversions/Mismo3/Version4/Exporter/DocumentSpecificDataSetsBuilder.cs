﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.TRID2;
    using Mismo3Specification.Version4Schema;
    using static CommonBuilder;
    using static Mismo3Specification.Mismo3Utilities;
    using static TypeBuilder;

    /// <summary>
    /// Class for creating a DOCUMENT_SPECIFIC_DATA_SETS container.
    /// </summary>
    public class DocumentSpecificDataSetsBuilder
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentSpecificDataSetsBuilder"/> class.
        /// </summary>
        /// <param name="dataLoan">The loan data.</param>
        /// <param name="exportData">A collection of data used throughout the exporter.</param>
        public DocumentSpecificDataSetsBuilder(CPageData dataLoan, Mismo34ExporterCache exportData)
        {
            this.DataLoan = dataLoan;
            this.PrimaryApp = Mismo34ExporterCache.GetPrimaryApp(dataLoan);
            this.ExportData = exportData;

            // As far as I can tell, these two data loans are only used in this container. So we can skip passing them in from all the way up top and just make them in here.
            // Of course, we'll want to have another constructor that takes these two in for testing purposes.
            this.LoanEstimateArchiveData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.DataLoan.sLId, typeof(Mismo34Exporter));
            this.LoanEstimateArchiveData.SetFormatTarget(FormatTarget.MismoClosing);
            this.LoanEstimateArchiveData.ByPassFieldSecurityCheck = true;
            this.LoanEstimateArchiveData.InitLoad();

            this.ClosingDisclosureArchiveData = CPageData.CreateUsingSmartDependencyWithSecurityBypass(this.DataLoan.sLId, typeof(Mismo34Exporter));
            this.ClosingDisclosureArchiveData.SetFormatTarget(FormatTarget.MismoClosing);
            this.ClosingDisclosureArchiveData.ByPassFieldSecurityCheck = true;
            this.ClosingDisclosureArchiveData.InitLoad();
        }

        /// <summary>
        /// Gets the loan data.
        /// </summary>
        protected CPageData DataLoan { get; private set; }

        /// <summary>
        /// Gets the primary application on the loan.
        /// </summary>
        protected CAppData PrimaryApp { get; private set; }

        /// <summary>
        /// Gets a collection of data used throughout the exporter.
        /// </summary>
        protected Mismo34ExporterCache ExportData { get; private set; }

        /// <summary>
        /// Gets a value indicating whether archives should be omitted.
        /// </summary>
        protected bool ShouldOmitArchive { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the LE and CD loan data objects have an archive applied to them.
        /// </summary>
        protected bool HasArchive { get; private set; } = false;

        /// <summary>
        /// Gets the loan estimate archive data loan.
        /// </summary>
        protected CPageData LoanEstimateArchiveData { get; private set; }

        /// <summary>
        /// Gets the closing disclosure archive data loan.
        /// </summary>
        protected CPageData ClosingDisclosureArchiveData { get; private set; }

        /// <summary>
        /// Creates the DOCUMENT_SPECIFIC_DATA_SETS container.
        /// </summary>
        /// <returns>The populated container.</returns>
        public DOCUMENT_SPECIFIC_DATA_SETS CreateContainer()
        {
            return this.CreateDocumentSpecificDataSets();
        }

        /// <summary>
        /// Determines whether the loan is a home equity loan.
        /// </summary>
        /// <param name="loanPurpose">The loan purpose.</param>
        /// <param name="isLineOfCredit">Indicates whether the loan is a line of credit.</param>
        /// <returns>A boolean indicating whether the loan is a home equity loan.</returns>
        private static bool GetIntegratedDisclosureHomeEquityLoanIndicatorValue(E_sLPurposeT loanPurpose, bool isLineOfCredit)
        {
            if (loanPurpose == E_sLPurposeT.HomeEquity)
            {
                return true;
            }
            else
            {
                if (isLineOfCredit && loanPurpose != E_sLPurposeT.Purchase
                    && loanPurpose != E_sLPurposeT.Construct && loanPurpose != E_sLPurposeT.ConstructPerm)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Gets the ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase value corresponding to the type of the associated cost/expense.
        /// </summary>
        /// <param name="costType">The cost type, e.g. property taxes, hazard insurance, etc.</param>
        /// <returns>The ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase value based on the cost type.</returns>
        private static ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase GetProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase(E_HousingExpenseTypeT costType)
        {
            switch (costType)
            {
                case E_HousingExpenseTypeT.CondoHO6Insurance:
                case E_HousingExpenseTypeT.FloodInsurance:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.HomeownersInsurance;
                case E_HousingExpenseTypeT.GroundRent:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.GroundRent;
                case E_HousingExpenseTypeT.HazardInsurance:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.HomeownersInsurance;
                case E_HousingExpenseTypeT.HomeownersAsscDues:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.HomeownersAssociationDues;
                case E_HousingExpenseTypeT.OtherTaxes1:
                case E_HousingExpenseTypeT.OtherTaxes2:
                case E_HousingExpenseTypeT.OtherTaxes3:
                case E_HousingExpenseTypeT.OtherTaxes4:
                case E_HousingExpenseTypeT.RealEstateTaxes:
                case E_HousingExpenseTypeT.SchoolTaxes:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.PropertyTaxes;
                case E_HousingExpenseTypeT.Unassigned:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.Other;
                case E_HousingExpenseTypeT.WindstormInsurance:
                    return ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.HomeownersInsurance;
                default:
                    throw new UnhandledEnumException(costType);
            }
        }

        /// <summary>
        /// Retrieves the payment credit enumeration corresponding to the given payment credit value.
        /// </summary>
        /// <param name="paymentCredit">Describes the payment credit.</param>
        /// <returns>The corresponding value.</returns>
        private static GFELoanOriginatorFeePaymentCreditBase GetGfeLoanOriginatorFeePaymentCreditBaseValue(string paymentCredit)
        {
            if (string.Equals("AmountsPaidByOrInBehalfOfBorrower", paymentCredit, StringComparison.OrdinalIgnoreCase))
            {
                return GFELoanOriginatorFeePaymentCreditBase.AmountsPaidByOrInBehalfOfBorrower;
            }
            else if (string.Equals("ChosenInterestRateCreditOrCharge", paymentCredit, StringComparison.OrdinalIgnoreCase))
            {
                return GFELoanOriginatorFeePaymentCreditBase.ChosenInterestRateCreditOrCharge;
            }
            else
            {
                return GFELoanOriginatorFeePaymentCreditBase.Other;
            }
        }

        /// <summary>
        /// Retrieves the cash to close calculation method enum.
        /// </summary>
        /// <param name="calculationMethod">The LQB cash to close method enum.</param>
        /// <returns>The enum for MISMO.</returns>
        private static CashToCloseCalculationMethodBase ToCashToCloseCalculationMethodBase(E_CashToCloseCalcMethodT calculationMethod)
        {
            switch (calculationMethod)
            {
                case E_CashToCloseCalcMethodT.Standard:
                    return CashToCloseCalculationMethodBase.Standard;
                case E_CashToCloseCalcMethodT.Alternative:
                    return CashToCloseCalculationMethodBase.Alternative;
                default:
                    throw new UnhandledEnumException(calculationMethod);
            }
        }

        /// <summary>
        /// Retrieves the serializable value corresponding to the start period for first-year analysis.
        /// </summary>
        /// <param name="startFrom">The point from which analysis will begin.</param>
        /// <returns>The corresponding MISMO value.</returns>
        private static LqbCdEscrowFirstYearAnalysisStartFromBase GetLqbCdEscrowFirstYearAnalysisStartFromBaseValue(TridClosingDisclosureCalculateEscrowAccountFromT startFrom)
        {
            switch (startFrom)
            {
                case TridClosingDisclosureCalculateEscrowAccountFromT.Consummation:
                    return LqbCdEscrowFirstYearAnalysisStartFromBase.Consummation;
                case TridClosingDisclosureCalculateEscrowAccountFromT.FirstPaymentDate:
                    return LqbCdEscrowFirstYearAnalysisStartFromBase.FirstPayment;
                case TridClosingDisclosureCalculateEscrowAccountFromT.NA:
                    return LqbCdEscrowFirstYearAnalysisStartFromBase.Blank;
                default:
                    throw new UnhandledEnumException(startFrom);
            }
        }
        //// mapper ////

        /// <summary>
        /// Applies a loan estimate or closing disclosure archive to the associated loan data object.
        /// Note: This method will be replaced when OPM 214209 is complete. That one will allow an archive to be associated with the relevant meta-data from the general status page. The current method here will grab the wrong archive under certain circumstances, but it's the best we can do for now for testing purposes.
        /// </summary>
        /// <param name="docType">The type of integrated disclosure, e.g. loan estimate | closing disclosure.</param>
        /// <param name="archiveID">The GUID associated with the archive.</param>
        /// <remarks>For Loan Estimates, we check specifically for the Disclosed status instead of using the IsDisclosed property because the LE dates should NOT include LEs that were disclosed as part of a Closing Disclosure.</remarks>
        private void ApplyIntegratedDisclosureArchive(DocumentBase docType, Guid archiveID)
        {
            this.HasArchive = false;
            this.ShouldOmitArchive = false;

            if (archiveID == null || archiveID == Guid.Empty || this.LoanEstimateArchiveData == null || this.ClosingDisclosureArchiveData == null)
            {
                return;
            }

            if (docType == DocumentBase.LoanEstimate && this.LoanEstimateArchiveData.sClosingCostArchive.Count != 0)
            {
                try
                {
                    var archive = this.LoanEstimateArchiveData.sClosingCostArchive.FirstOrDefault(a => a.Id == archiveID);
                    if (archive == null)
                    {
                        return;
                    }
                    else if (archive.Status != Common.SerializationTypes.ClosingCostArchive.E_ClosingCostArchiveStatus.Disclosed)
                    {
                        this.ShouldOmitArchive = true;
                        return;
                    }

                    this.LoanEstimateArchiveData.ApplyClosingCostArchive(archiveID);
                    this.HasArchive = true;
                }
                catch (ArgumentNullException)
                {
                    return;
                }
            }
            else if (docType == DocumentBase.ClosingDisclosure && this.ClosingDisclosureArchiveData.sClosingCostArchive.Count != 0)
            {
                var archive = this.ClosingDisclosureArchiveData.sClosingCostArchive.FirstOrDefault(a => a.Id == archiveID);
                if (archive == null)
                {
                    return;
                }
                else if (!archive.IsDisclosed)
                {
                    this.ShouldOmitArchive = true;
                    return;
                }

                try
                {
                    this.ClosingDisclosureArchiveData.ApplyClosingCostArchive(archiveID);
                    this.HasArchive = true;
                }
                catch (ArgumentNullException)
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Create the DOCUMENT_SPECIFIC_DATA_SETS container.
        /// </summary>
        /// <returns>The populated container.</returns>
        private DOCUMENT_SPECIFIC_DATA_SETS CreateDocumentSpecificDataSets()
        {
            var documentSpecificDataSets = new DOCUMENT_SPECIFIC_DATA_SETS();

            if (this.ExportData.Options.DocumentVendor != E_DocumentVendor.UnknownOtherNone)
            {
                documentSpecificDataSets.DocumentSpecificDataSetList.Add(this.CreateToBeDisclosed(GetNextSequenceNumber(documentSpecificDataSets.DocumentSpecificDataSetList)));
            }

            if (this.ExportData.Options.DocumentVendor != E_DocumentVendor.DocMagic)
            {
                foreach (LoanEstimateDates loanEstimate in this.DataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList)
                {
                    this.ApplyIntegratedDisclosureArchive(DocumentBase.LoanEstimate, loanEstimate.ArchiveId);

                    if (!this.ShouldOmitArchive)
                    {
                        documentSpecificDataSets.DocumentSpecificDataSetList.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.LoanEstimate, document: loanEstimate, currentLoanTRID: false, sequence: GetNextSequenceNumber(documentSpecificDataSets.DocumentSpecificDataSetList)));
                    }
                }

                foreach (ClosingDisclosureDates closingDisclosure in this.DataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList)
                {
                    this.ApplyIntegratedDisclosureArchive(DocumentBase.ClosingDisclosure, closingDisclosure.ArchiveId);

                    if (!this.ShouldOmitArchive)
                    {
                        documentSpecificDataSets.DocumentSpecificDataSetList.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.ClosingDisclosure, document: closingDisclosure, currentLoanTRID: false, sequence: GetNextSequenceNumber(documentSpecificDataSets.DocumentSpecificDataSetList)));
                    }
                }
            }

            documentSpecificDataSets.DocumentSpecificDataSetList.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.LoanApplicationURLA, document: null, currentLoanTRID: false, sequence: GetNextSequenceNumber(documentSpecificDataSets.DocumentSpecificDataSetList)));
            documentSpecificDataSets.DocumentSpecificDataSetList.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.SecurityInstrument, document: null, currentLoanTRID: false, sequence: GetNextSequenceNumber(documentSpecificDataSets.DocumentSpecificDataSetList)));
            documentSpecificDataSets.DocumentSpecificDataSetList.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.TILDisclosure, document: null, currentLoanTRID: false, sequence: GetNextSequenceNumber(documentSpecificDataSets.DocumentSpecificDataSetList)));
            documentSpecificDataSets.DocumentSpecificDataSetList.Add(this.CreateDocumentSpecificDataSet(docType: DocumentBase.GFE, document: null, currentLoanTRID: false, sequence: GetNextSequenceNumber(documentSpecificDataSets.DocumentSpecificDataSetList)));

            if (this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic)
            {
                documentSpecificDataSets.DocumentSpecificDataSetList.Add(this.CreateDocumentSpecificDataSet(DocumentBase.Note, document: null, currentLoanTRID: false, sequence: GetNextSequenceNumber(documentSpecificDataSets.DocumentSpecificDataSetList)));
            }

            return documentSpecificDataSets;
        }

        /// <summary>
        /// Creates a container holding information on specific documents.
        /// </summary>
        /// <param name="docType">Indicates the type of document that should be created.</param>
        /// <param name="document">An instance of either <see cref="LoanEstimateDates" /> or <see cref="ClosingDisclosureDates" />, depending on the <see cref="DocumentBase" /> parameter.</param>
        /// <param name="currentLoanTRID">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued. Should also be false if the disclosure is not a TRID disclosure.</param>
        /// <param name="sequence">The sequence number of the data-set within the list of data-sets.</param>
        /// <returns>A DOCUMENT_SPECIFIC_DATA_SET container.</returns>
        private DOCUMENT_SPECIFIC_DATA_SET CreateDocumentSpecificDataSet(DocumentBase docType, object document, bool currentLoanTRID, int sequence)
        {
            var documentSpecificDataSet = new DOCUMENT_SPECIFIC_DATA_SET();

            documentSpecificDataSet.DocumentClasses = this.CreateDocumentClasses(docType);
            documentSpecificDataSet.SequenceNumber = sequence;

            if (docType == DocumentBase.ClosingDisclosure || docType == DocumentBase.LoanEstimate)
            {
                documentSpecificDataSet.IntegratedDisclosure = this.CreateIntegratedDisclosure(docType, document, currentLoanTRID);

                if (!documentSpecificDataSet.IntegratedDisclosureSpecified ||
                    !documentSpecificDataSet.IntegratedDisclosure.IntegratedDisclosureDetailSpecified ||
                    !documentSpecificDataSet.IntegratedDisclosure.IntegratedDisclosureDetail.IntegratedDisclosureIssuedDateSpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.LoanApplicationURLA)
            {
                documentSpecificDataSet.Urla = this.CreateUrla();

                if (!documentSpecificDataSet.UrlaSpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.SecurityInstrument)
            {
                // 4/23/2015 BB - These are both applicable to the deed of trust.
                documentSpecificDataSet.SecurityInstrument = this.CreateSecurityInstrument();
                documentSpecificDataSet.RecordingEndorsements = this.CreateRecordingEndorsements();
                documentSpecificDataSet.Execution = this.CreateExecution();

                if (!documentSpecificDataSet.SecurityInstrumentSpecified && !documentSpecificDataSet.RecordingEndorsementsSpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.TILDisclosure)
            {
                documentSpecificDataSet.TilDisclosure = this.CreateTilDisclosure();

                if (!documentSpecificDataSet.TilDisclosureSpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.GFE)
            {
                documentSpecificDataSet.Gfe = this.CreateGfe();

                if (!documentSpecificDataSet.GfeSpecified)
                {
                    return null;
                }
            }
            else if (docType == DocumentBase.Note)
            {
                documentSpecificDataSet.Note = this.CreateNote();
                documentSpecificDataSet.Execution = this.CreateExecution();
            }

            return documentSpecificDataSet;
        }

        /// <summary>
        /// Creates a container with the classifications of a document.
        /// </summary>
        /// <param name="docType">The document type.</param>
        /// <returns>A DOCUMENT_CLASSES container.</returns>
        private DOCUMENT_CLASSES CreateDocumentClasses(DocumentBase docType)
        {
            var documentClasses = new DOCUMENT_CLASSES();
            documentClasses.DocumentClassList.Add(this.CreateDocumentClass(docType, GetNextSequenceNumber(documentClasses.DocumentClassList)));

            return documentClasses;
        }

        /// <summary>
        /// Creates a container with a document class for the document given by the document type.
        /// </summary>
        /// <param name="docType">The document type.</param>
        /// <param name="sequence">The sequence number of the document class among the list of classes.</param>
        /// <returns>A DOCUMENT_CLASS container.</returns>
        private DOCUMENT_CLASS CreateDocumentClass(DocumentBase docType, int sequence)
        {
            var documentClass = new DOCUMENT_CLASS();
            if (this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic && (docType == DocumentBase.ClosingDisclosure || docType == DocumentBase.LoanEstimate))
            {
                documentClass.DocumentType = ToMismoEnum(DocumentBase.Other, isSensitiveData: false);

                string prefix = string.Empty;
                if (docType == DocumentBase.LoanEstimate)
                {
                    prefix = "LoanEstimate:";
                }
                else
                {
                    // if (docType == DocumentType.ClosingDisclosure)
                    prefix = "ClosingDisclosure:";
                }

                if ((docType == DocumentBase.LoanEstimate && this.LoanEstimateArchiveData.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative) ||
                    (docType == DocumentBase.ClosingDisclosure && this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative))
                {
                    documentClass.DocumentTypeOtherDescription = ToMismoString(prefix + "AlternateForm");
                }
                else
                {
                    // E_CashToCloseCalcMethodT.Standard
                    documentClass.DocumentTypeOtherDescription = ToMismoString(prefix + "ModelForm");
                }
            }
            else
            {
                documentClass.DocumentType = ToMismoEnum(docType, isSensitiveData: false);
            }

            documentClass.SequenceNumber = sequence;

            return documentClass;
        }

        /// <summary>
        /// Creates a container holding information on an integrated disclosure.
        /// </summary>
        /// <param name="docType">Distinguishes between the Loan Estimate and Closing Disclosure.</param>
        /// <param name="document">An instance of either <see cref="LoanEstimateDates" /> or <see cref="ClosingDisclosureDates" />, depending on the <see cref="DocumentBase" /> parameter.</param>
        /// <param name="currentLoan">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued.</param>
        /// <returns>An INTEGRATED_DISCLOSURE container.</returns>
        private INTEGRATED_DISCLOSURE CreateIntegratedDisclosure(DocumentBase docType, object document, bool currentLoan)
        {
            var integratedDisclosure = new INTEGRATED_DISCLOSURE();
            IntegratedDisclosureDocumentBase documentType = IntegratedDisclosureDocumentBase.Blank;

            if (docType == DocumentBase.LoanEstimate)
            {
                documentType = IntegratedDisclosureDocumentBase.LoanEstimate;
                LoanEstimateDates loanEstimateDates = (LoanEstimateDates)document;

                integratedDisclosure.IntegratedDisclosureDetail = this.CreateIntegratedDisclosureDetail(documentType, loanEstimateDates.IssuedDate, loanEstimateDates.ReceivedDate, loanEstimateDates.UniqueId, loanEstimateDates.DeliveryMethod, loanEstimateDates.LastDisclosedTRIDLoanProductDescription, currentLoan);
            }
            else if (docType == DocumentBase.ClosingDisclosure)
            {
                documentType = IntegratedDisclosureDocumentBase.ClosingDisclosure;
                ClosingDisclosureDates closingDisclosureDates = (ClosingDisclosureDates)document;

                integratedDisclosure.IntegratedDisclosureDetail = this.CreateIntegratedDisclosureDetail(documentType, closingDisclosureDates.IssuedDate, closingDisclosureDates.ReceivedDate, closingDisclosureDates.UniqueId, closingDisclosureDates.DeliveryMethod, closingDisclosureDates.LastDisclosedTRIDLoanProductDescription, currentLoan);
            }
            else
            {
                return null;
            }

            integratedDisclosure.CashToCloseItems = this.CreateCashToCloseItems(documentType);
            integratedDisclosure.EstimatedPropertyCost = this.CreateEstimatedPropertyCost(documentType);
            integratedDisclosure.IntegratedDisclosureSectionSummaries = this.CreateIntegratedDisclosureSectionSummaries(documentType);
            integratedDisclosure.OtherLoanConsiderationsAndDisclosuresItems = this.CreateOtherLoanConsiderationsAndDisclosuresItems(documentType);
            integratedDisclosure.ProjectedPayments = this.CreateProjectedPayments(documentType);

            if (this.ExportData.Options.IncludeIntegratedDisclosureExtension)
            {
                integratedDisclosure.Extension = this.CreateIntegratedDisclosureExtension(documentType);
            }

            return integratedDisclosure;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_EXTENSION container with information that is otherwise captured at the loan level, but not per loan estimate | closing disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_EXTENSION container.</returns>
        private INTEGRATED_DISCLOSURE_EXTENSION CreateIntegratedDisclosureExtension(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var extension = new INTEGRATED_DISCLOSURE_EXTENSION();
            var mismoExtension = new MISMO_INTEGRATED_DISCLOSURE_EXTENSION();
            extension.Mismo = mismoExtension;

            var loanArchive = documentType == IntegratedDisclosureDocumentBase.LoanEstimate ? this.LoanEstimateArchiveData : this.ClosingDisclosureArchiveData;
            var feeInformationBuilder = new FeeInformationBuilder(loanArchive, this.ExportData, documentTypeOverride: documentType);
            mismoExtension.FeeInformation = feeInformationBuilder.CreateContainer();

            if (this.ExportData.Options.DocumentVendor == E_DocumentVendor.UnknownOtherNone)
            {
                mismoExtension.Adjustment = CreateAdjustment(loanArchive, this.ExportData.Options.IsLenderExportMode);
                mismoExtension.InterestOnly = CreateInterestOnly(loanArchive);
                mismoExtension.Payment = CreatePayment(loanArchive, this.ExportData.IsClosingPackage);
                mismoExtension.TermsOfLoan = CreateTermsOfLoan(loanArchive, this.ExportData, this.DataLoan.BrokerDB.IsEnableHELOC);
            }

            return extension;
        }

        /// <summary>
        /// Creates a container holding details about an integrated disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <param name="issuedDate">The date the document was issued.</param>
        /// <param name="receivedDate">The date the document was received by the borrower.</param>
        /// <param name="disclosureId">The GUID of the Integrated Disclosure object.</param>
        /// <param name="deliveryMethod">The method used to deliver the disclosure to the borrower.</param>
        /// <param name="loanProduct">The loan product name.</param>
        /// <param name="currentLoan">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_DETAIL container.</returns>
        private INTEGRATED_DISCLOSURE_DETAIL CreateIntegratedDisclosureDetail(IntegratedDisclosureDocumentBase documentType, DateTime issuedDate, DateTime receivedDate, Guid disclosureId, E_DeliveryMethodT deliveryMethod, string loanProduct, bool currentLoan)
        {
            var integratedDisclosureDetail = new INTEGRATED_DISCLOSURE_DETAIL();
            integratedDisclosureDetail.IntegratedDisclosureDocumentType = ToMismoEnum(documentType, isSensitiveData: false);
            integratedDisclosureDetail.IntegratedDisclosureIssuedDate = ToMismoDate(this.DataLoan.m_convertLos.ToDateTimeString(issuedDate));

            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate && this.HasArchive)
            {
                try
                {
                    integratedDisclosureDetail.FiveYearPrincipalReductionComparisonAmount = ToMismoAmount(this.LoanEstimateArchiveData.sTRIDLoanEstimatePrincipalIn5Yrs_rep);
                    integratedDisclosureDetail.FiveYearTotalOfPaymentsComparisonAmount = ToMismoAmount(this.LoanEstimateArchiveData.sTRIDLoanEstimateTotalPIMIPLoanCostsIn5Yrs_rep);
                }
                catch (CBaseException) when (this.ExportData.Options.IsLenderExportMode)
                {
                    integratedDisclosureDetail.FiveYearPrincipalReductionComparisonAmount = null;
                    integratedDisclosureDetail.FiveYearTotalOfPaymentsComparisonAmount = null;
                }

                integratedDisclosureDetail.IntegratedDisclosureEstimatedClosingCostsExpirationDatetime = ToMismoDatetime(this.LoanEstimateArchiveData.sGfeEstScAvailTillD_rep_WithTime, false);

                integratedDisclosureDetail.IntegratedDisclosureHomeEquityLoanIndicator = ToMismoIndicator(GetIntegratedDisclosureHomeEquityLoanIndicatorValue(this.LoanEstimateArchiveData.sLPurposeT, this.LoanEstimateArchiveData.sIsLineOfCredit), isSensitiveData: false);
                integratedDisclosureDetail.IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount = ToMismoAmount(this.LoanEstimateArchiveData.sProThisMPmt_rep);
                integratedDisclosureDetail.IntegratedDisclosureLoanProductDescription = ToMismoString(loanProduct);
            }
            else if (documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure && this.HasArchive)
            {
                integratedDisclosureDetail.FirstYearTotalEscrowPaymentAmount = ToMismoAmount(this.ClosingDisclosureArchiveData.sClosingDisclosureEscrowedPropertyCostsFirstYear_rep);

                if (this.ClosingDisclosureArchiveData.sTridEscrowAccountExists)
                {
                    integratedDisclosureDetail.FirstYearTotalNonEscrowPaymentAmount = ToMismoAmount(this.ClosingDisclosureArchiveData.sClosingDisclosureNonEscrowedPropertyCostsFirstYear_rep);
                }
                else
                {
                    integratedDisclosureDetail.FirstYearTotalNonEscrowPaymentAmount = ToMismoAmount(this.ClosingDisclosureArchiveData.sClosingDisclosurePropertyCostsFirstYear_rep);
                }

                integratedDisclosureDetail.IntegratedDisclosureHomeEquityLoanIndicator = ToMismoIndicator(GetIntegratedDisclosureHomeEquityLoanIndicatorValue(this.ClosingDisclosureArchiveData.sLPurposeT, this.ClosingDisclosureArchiveData.sIsLineOfCredit), isSensitiveData: false);
                integratedDisclosureDetail.IntegratedDisclosureInitialPrincipalAndInterestPaymentAmount = ToMismoAmount(this.ClosingDisclosureArchiveData.sProThisMPmt_rep);
                integratedDisclosureDetail.IntegratedDisclosureLoanProductDescription = ToMismoString(loanProduct);
            }

            integratedDisclosureDetail.Extension = this.CreateIntegratedDisclosureDetailExtension(documentType, disclosureId, receivedDate, deliveryMethod, currentLoan);

            return integratedDisclosureDetail;
        }

        /// <summary>
        /// Creates a container with extension data for INTEGRATED_DISCLOSURE_DETAIL.
        /// </summary>
        /// <param name="documentType">The Integrated Disclosure document type.</param>
        /// <param name="disclosureId">The GUID of the Integrated Disclosure object.</param>
        /// <param name="receivedDate">The date the document was received by the borrower.</param>
        /// <param name="deliveryMethod">The method used to deliver the disclosure to the borrower.</param>
        /// <param name="currentLoan">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_DETAIL_EXTENSION container.</returns>
        private INTEGRATED_DISCLOSURE_DETAIL_EXTENSION CreateIntegratedDisclosureDetailExtension(IntegratedDisclosureDocumentBase documentType, Guid disclosureId, DateTime receivedDate, E_DeliveryMethodT deliveryMethod, bool currentLoan)
        {
            var extension = new INTEGRATED_DISCLOSURE_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBIntegratedDisclosureDetailExtension(documentType, disclosureId, receivedDate, deliveryMethod, currentLoan);

            return extension;
        }

        /// <summary>
        /// Creates a container with extension data for INTEGRATED_DISCLOSURE_DETAIL.
        /// </summary>
        /// <param name="documentType">The Integrated Disclosure document type.</param>
        /// <param name="disclosureId">The GUID of the Integrated Disclosure object.</param>
        /// <param name="receivedDate">The date the document was received by the borrower.</param>
        /// <param name="deliveryMethod">The method used to deliver the disclosure to the borrower.</param>
        /// <param name="currentLoan">Indicates that the document data-set will reflect the current loan file, as opposed to a disclosure that was previously issued.</param>
        /// <returns>An LQB_INTEGRATED_DISCLOSURE_DETAIL_EXTENSION container.</returns>
        private LQB_INTEGRATED_DISCLOSURE_DETAIL_EXTENSION CreateLQBIntegratedDisclosureDetailExtension(IntegratedDisclosureDocumentBase documentType, Guid disclosureId, DateTime receivedDate, E_DeliveryMethodT deliveryMethod, bool currentLoan)
        {
            var extension = new LQB_INTEGRATED_DISCLOSURE_DETAIL_EXTENSION();
            extension.CurrentLoan = ToMismoIndicator(currentLoan);
            extension.LastDisclosed = ToMismoIndicator(false);
            extension.ReceivedDate = ToMismoDate(this.DataLoan.m_convertLos.ToDateTimeString(receivedDate));

            if (deliveryMethod != E_DeliveryMethodT.LeaveEmpty)
            {
                extension.DeliveryMethodType = ToMismoString(GetXmlEnumName(deliveryMethod));
            }

            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
            {
                extension.RefinanceIncludingDebtsToBePaidOffAmount = ToMismoAmount(this.LoanEstimateArchiveData.sRefPdOffAmt1003_rep);
                extension.BorrowerRequestedLoanAmount = ToMismoAmount(this.LoanEstimateArchiveData.sFinalLAmt_rep);
                extension.AlterationsImprovementsAndRepairsAmount = ToMismoAmount(this.LoanEstimateArchiveData.sTRIDClosingDisclosureAltCost_rep);
                extension.LandOriginalCostAmount = ToMismoAmount(this.LoanEstimateArchiveData.sLandCost_rep);

                if (disclosureId != Guid.Empty && !currentLoan
                    && this.DataLoan.GetLastDisclosedLoanEstimate() != null
                    && (disclosureId == this.DataLoan.GetLastDisclosedLoanEstimate().UniqueId
                    || this.DataLoan.sLoanEstimateDatesInfo.LoanEstimateDatesList.Count() == 1))
                {
                    extension.LastDisclosed = ToMismoIndicator(true);
                }
            }
            else if (documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure)
            {
                extension.RefinanceIncludingDebtsToBePaidOffAmount = ToMismoAmount(this.ClosingDisclosureArchiveData.sRefPdOffAmt1003_rep);
                extension.BorrowerRequestedLoanAmount = ToMismoAmount(this.ClosingDisclosureArchiveData.sFinalLAmt_rep);
                extension.AlterationsImprovementsAndRepairsAmount = ToMismoAmount(this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureAltCost_rep);
                extension.LandOriginalCostAmount = ToMismoAmount(this.ClosingDisclosureArchiveData.sLandCost_rep);
                extension.ClosingDate = ToMismoDate(this.ClosingDisclosureArchiveData.sDocMagicClosingD_rep);
                extension.CdEscrowFirstYearAnalysisStartFromType = ToMismoEnum(GetLqbCdEscrowFirstYearAnalysisStartFromBaseValue(this.ClosingDisclosureArchiveData.sTridClosingDisclosureCalculateEscrowAccountFromTCalculated), isSensitiveData: false);

                if (disclosureId != Guid.Empty && !currentLoan
                    && this.DataLoan.GetLastDisclosedClosingDisclosure() != null
                    && (disclosureId == this.DataLoan.GetLastDisclosedClosingDisclosure().UniqueId
                    || this.DataLoan.sClosingDisclosureDatesInfo.ClosingDisclosureDatesList.Count() == 1))
                {
                    extension.LastDisclosed = ToMismoIndicator(true);
                }
            }

            return extension;
        }

        /// <summary>
        /// Creates a CASH_TO_CLOSE_ITEMS container with a list of cash to close items from the archived disclosure given by the document type.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>A CASH_TO_CLOSE_ITEMS container with a list of cash to close items.</returns>
        private CASH_TO_CLOSE_ITEMS CreateCashToCloseItems(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var cashToCloseItems = new CASH_TO_CLOSE_ITEMS();

            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
            {
                cashToCloseItems.Extension = this.CreateCashToCloseItemsExtension(this.LoanEstimateArchiveData.sTRIDLoanEstimateCashToCloseCalcMethodT);
                cashToCloseItems.CashToCloseItemList.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.LoanEstimate,
                        IntegratedDisclosureCashToCloseItemBase.LoanCostsAndOtherCostsTotal,
                        estimatedAmount: this.LoanEstimateArchiveData.sTRIDLoanEstimateTotalAllCosts_rep,
                        finalAmount: null,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                cashToCloseItems.CashToCloseItemList.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.LoanEstimate,
                        IntegratedDisclosureCashToCloseItemBase.LenderCredits,
                        estimatedAmount: this.LoanEstimateArchiveData.sTRIDLoanEstimateLenderCredits_rep,
                        finalAmount: null,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                        sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                cashToCloseItems.CashToCloseItemList.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.LoanEstimate,
                        IntegratedDisclosureCashToCloseItemBase.TotalClosingCosts,
                        estimatedAmount: this.LoanEstimateArchiveData.sTRIDLoanEstimateTotalClosingCosts_rep,
                        finalAmount: null,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                if (this.LoanEstimateArchiveData.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard)
                {
                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.ClosingCostsFinanced,
                            estimatedAmount: this.LoanEstimateArchiveData.sTRIDLoanEstimateBorrowerFinancedClosingCosts_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.FundsFromBorrower,
                            estimatedAmount: this.LoanEstimateArchiveData.sTRIDLoanEstimateDownPaymentOrFundsFromBorrower_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.Deposit,
                            estimatedAmount: this.LoanEstimateArchiveData.sTRIDCashDeposit_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.FundsForBorrower,
                            estimatedAmount: this.LoanEstimateArchiveData.sTRIDLoanEstimateFundsForBorrower_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.SellerCredits,
                            estimatedAmount: this.LoanEstimateArchiveData.sTRIDLoanEstimateSellerCredits_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.AdjustmentsAndOtherCredits,
                            estimatedAmount: this.LoanEstimateArchiveData.sTRIDAdjustmentsAndOtherCredits_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));
                }
                else if (this.LoanEstimateArchiveData.sTRIDLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative)
                {
                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.LoanAmount,
                            estimatedAmount: this.LoanEstimateArchiveData.sFinalLAmt_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.TotalPayoffsAndPayments,
                            estimatedAmount: this.LoanEstimateArchiveData.sTRIDTotalPayoffsAndPayments_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));
                }

                cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.LoanEstimate,
                            IntegratedDisclosureCashToCloseItemBase.CashToCloseTotal,
                            estimatedAmount: this.LoanEstimateArchiveData.sTRIDLoanEstimateCashToClose_rep,
                            finalAmount: null,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));
            }
            else
            {
                cashToCloseItems.Extension = this.CreateCashToCloseItemsExtension(this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT);
                cashToCloseItems.CashToCloseItemList.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.LoanCostsAndOtherCostsTotal,
                        estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateTotalClosingCostsExcludingLenderCredit_rep,
                        finalAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureTotalClosingCostsExcludingLenderCredit_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                cashToCloseItems.CashToCloseItemList.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.LenderCredits,
                        estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateLenderCredit_rep,
                        finalAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureGeneralLenderCredits_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                        sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                cashToCloseItems.CashToCloseItemList.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.TotalClosingCosts,
                        estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateTotalClosingCosts_rep,
                        finalAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureTotalClosingCosts_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                //// 5/11/2015 BB - Estimated amount is hard-coded to $0.00 on the closing disclosure page. It would be better to migrate this value to the data layer at some point.
                cashToCloseItems.CashToCloseItemList.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.ClosingCostsPaidBeforeClosing,
                        estimatedAmount: "0.00",
                        finalAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureClosingCostsPaidBeforeClosing_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                if (this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Standard)
                {
                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.ClosingCostsFinanced,
                            estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateBorrowerFinancedClosingCosts_rep,
                            finalAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureBorrowerFinancedClosingCosts_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.FundsFromBorrower,
                            estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateDownPaymentOrFundsFromBorrower_rep,
                            finalAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureDownPaymentOrFundsFromBorrower_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.Deposit,
                            estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashDeposit_rep,
                            finalAmount: this.ClosingDisclosureArchiveData.sTRIDCashDeposit_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.FundsForBorrower,
                            estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateFundsForBorrower_rep,
                            finalAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureFundsForBorrower_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.SellerCredits,
                            estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateSellerCredits_rep,
                            finalAmount: this.ClosingDisclosureArchiveData.sTRIDSellerCredits_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.AdjustmentsAndOtherCredits,
                            estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateAdjustmentsAndOtherCredits_rep,
                            finalAmount: this.ClosingDisclosureArchiveData.sTRIDAdjustmentsAndOtherCredits_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));
                }
                else if (this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT == E_CashToCloseCalcMethodT.Alternative)
                {
                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.LoanAmount,
                            estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateFinalLAmt_rep,
                            finalAmount: this.ClosingDisclosureArchiveData.sFinalLAmt_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));

                    cashToCloseItems.CashToCloseItemList.Add(
                        this.CreateCashToCloseItem(
                            DocumentBase.ClosingDisclosure,
                            IntegratedDisclosureCashToCloseItemBase.TotalPayoffsAndPayments,
                            estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateTotalPayoffsAndPayments_rep,
                            finalAmount: this.ClosingDisclosureArchiveData.sTRIDTotalPayoffsAndPayments_rep,
                            paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                            sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));
                }

                cashToCloseItems.CashToCloseItemList.Add(
                    this.CreateCashToCloseItem(
                        DocumentBase.ClosingDisclosure,
                        IntegratedDisclosureCashToCloseItemBase.CashToCloseTotal,
                        estimatedAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureLastLoanEstimateCashToClose_rep,
                        finalAmount: this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureCashToClose_rep,
                        paymentType: IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower,
                        sequenceNumber: GetNextSequenceNumber(cashToCloseItems.CashToCloseItemList)));
            }

            return cashToCloseItems;
        }

        /// <summary>
        /// Creates an extension to the CASH_TO_CLOSE_ITEMS container with additional LQB and/or MISMO information. 
        /// </summary>
        /// <param name="calculationMethod">The LQB calculation method for cash to close items. Appears in the "OTHER" (LendingQB-proprietary) subcontainer.</param>
        /// <returns>A serializable <see cref="CASH_TO_CLOSE_ITEMS_EXTENSION"/> container.</returns>
        private CASH_TO_CLOSE_ITEMS_EXTENSION CreateCashToCloseItemsExtension(E_CashToCloseCalcMethodT calculationMethod)
        {
            // Build CASH_TO_CLOSE_ITEMS_EXTENSION/OTHER/CASH_TO_CLOSE_DETAIL/CalculationMethodType from inside out
            var cashToCloseDetail = new LQB_CASH_TO_CLOSE_DETAIL()
            {
                CalculationMethodType = ToMismoEnum(ToCashToCloseCalculationMethodBase(calculationMethod), isSensitiveData: false)
            };

            var other = new LQB_CASH_TO_CLOSE_ITEMS_EXTENSION()
            {
                LqbCashToCloseDetail = cashToCloseDetail
            };

            return new CASH_TO_CLOSE_ITEMS_EXTENSION()
            {
                Other = other
            };
        }

        /// <summary>
        /// Creates a CASH_TO_CLOSE_ITEM container with a cash to close item from the disclosure given by the document type.
        /// </summary>
        /// <param name="docType">Distinguishes between the Loan Estimate and Closing Disclosure.</param>
        /// <param name="itemType">The type of cash to close item.</param>
        /// <param name="estimatedAmount">The estimated amount of cash.</param>
        /// <param name="finalAmount">If the document is a closing disclosure then this is the final cash amount. Otherwise null/empty string.</param>
        /// <param name="paymentType">The type of payment (from or to borrower) for this item when the amount is positive.</param>
        /// <param name="sequenceNumber">The item sequence among the list of items.</param>
        /// <returns>A CASH_TO_CLOSE_ITEM container with a cash to close item.</returns>
        private CASH_TO_CLOSE_ITEM CreateCashToCloseItem(DocumentBase docType, IntegratedDisclosureCashToCloseItemBase itemType, string estimatedAmount, string finalAmount, IntegratedDisclosureCashToCloseItemPaymentBase paymentType, int sequenceNumber)
        {
            var cashToCloseItem = new CASH_TO_CLOSE_ITEM();

            decimal? parsedEstimatedAmount = estimatedAmount.ToNullable<decimal>(decimal.TryParse);
            decimal? parsedFinalAmount = finalAmount.ToNullable<decimal>(decimal.TryParse);
            string estimatedAmountToSend = estimatedAmount;
            string finalAmountToSend = finalAmount;
            IntegratedDisclosureCashToCloseItemPaymentBase paymentTypeToSend = paymentType;

            if (docType == DocumentBase.ClosingDisclosure && parsedEstimatedAmount.HasValue && parsedFinalAmount.HasValue)
            {
                if (parsedFinalAmount.Value < 0)
                {
                    finalAmountToSend = this.DataLoan.m_convertLos.ToMoneyString(-1 * parsedFinalAmount.Value, FormatDirection.ToRep);
                    estimatedAmountToSend = this.DataLoan.m_convertLos.ToMoneyString(-1 * parsedEstimatedAmount.Value, FormatDirection.ToRep);
                    paymentTypeToSend = paymentType != IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower ? IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower : IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower;
                }

                cashToCloseItem.IntegratedDisclosureCashToCloseItemFinalAmount = ToMismoAmount(finalAmountToSend);
            }
            else if (docType == DocumentBase.LoanEstimate && parsedEstimatedAmount.HasValue)
            {
                if (parsedEstimatedAmount.Value < 0)
                {
                    estimatedAmountToSend = this.DataLoan.m_convertLos.ToMoneyString(-1 * parsedEstimatedAmount.Value, FormatDirection.ToRep);
                    paymentTypeToSend = paymentType != IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower ? IntegratedDisclosureCashToCloseItemPaymentBase.FromBorrower : IntegratedDisclosureCashToCloseItemPaymentBase.ToBorrower;
                }
            }
            else
            {
                return null;
            }

            cashToCloseItem.IntegratedDisclosureCashToCloseItemEstimatedAmount = ToMismoAmount(estimatedAmountToSend);
            cashToCloseItem.IntegratedDisclosureCashToCloseItemPaymentType = ToMismoEnum(paymentTypeToSend, isSensitiveData: false);

            cashToCloseItem.IntegratedDisclosureCashToCloseItemType = ToMismoEnum(itemType, isSensitiveData: false);
            cashToCloseItem.SequenceNumber = sequenceNumber;

            return cashToCloseItem;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST container with a set of property costs from the archived disclosure given by the document type.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST container with a list of property costs.</returns>
        private ESTIMATED_PROPERTY_COST CreateEstimatedPropertyCost(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var propertyCost = new ESTIMATED_PROPERTY_COST();
            propertyCost.EstimatedPropertyCostComponents = this.CreateEstimatedPropertyCostComponents(documentType);
            propertyCost.EstimatedPropertyCostDetail = this.CreateEstimatedPropertyCostDetail(documentType);

            return propertyCost;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST_COMPONENTS container with a set of cost components from the archived disclosure given by the document type.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST_COMPONENTS container with a set of cost components.</returns>
        private ESTIMATED_PROPERTY_COST_COMPONENTS CreateEstimatedPropertyCostComponents(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var propertyCostComponents = new ESTIMATED_PROPERTY_COST_COMPONENTS();

            int sequence = 0;
            foreach (BaseHousingExpense expense in (documentType == IntegratedDisclosureDocumentBase.LoanEstimate ? this.LoanEstimateArchiveData : this.ClosingDisclosureArchiveData).sHousingExpenses.ExpensesToUse)
            {
                if (expense.MonthlyAmtTotal != 0)
                {
                    propertyCostComponents.EstimatedPropertyCostComponentList.Add(
                        this.CreateEstimatedPropertyCostComponent(
                        expense.HousingExpenseType,
                        expense.IsEscrowedAtClosing == E_TriState.Yes,
                        expense.DefaultExpenseDescription,
                        expense.AnnualAmt_rep,
                        ++sequence));
                }
            }

            return propertyCostComponents;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST_COMPONENT container with information pertaining to the given housing expense/cost.
        /// </summary>
        /// <param name="costType">The type of expense/cost.</param>
        /// <param name="escrowed">True if the cost is escrowed. Otherwise false.</param>
        /// <param name="description">A description of the cost.</param>
        /// <param name="annualAmount_rep">The annual amount of the cost (as a string).</param>
        /// <param name="sequenceNumber">The sequence number for the cost.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST_COMPONENT container with information pertaining to the given housing expense/cost.</returns>
        private ESTIMATED_PROPERTY_COST_COMPONENT CreateEstimatedPropertyCostComponent(E_HousingExpenseTypeT costType, bool escrowed, string description, string annualAmount_rep, int sequenceNumber)
        {
            var component = new ESTIMATED_PROPERTY_COST_COMPONENT();
            component.ProjectedPaymentEscrowedType = ToMismoEnum(escrowed ? ProjectedPaymentEscrowedBase.Escrowed : ProjectedPaymentEscrowedBase.NotEscrowed, isSensitiveData: false);
            component.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType = ToMismoEnum(GetProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase(costType), isSensitiveData: false);

            if (component.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentType.EnumValue == ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentBase.Other)
            {
                component.ProjectedPaymentEstimatedTaxesInsuranceAssessmentComponentTypeOtherDescription = ToMismoString(description);
            }

            component.SequenceNumber = sequenceNumber;

            component.Extension = this.CreateEstimatedPropertyCostComponentExtension(annualAmount_rep, description);

            return component;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container.
        /// </summary>
        /// <param name="annualAmount_rep">The annual amount of the cost (as a string).</param>
        /// <param name="description">The description of the cost.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container.</returns>
        private ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION CreateEstimatedPropertyCostComponentExtension(string annualAmount_rep, string description)
        {
            var extension = new ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION();

            extension.Other = this.CreateLQBEstimatedPropertyCostComponentExtension(annualAmount_rep, description);

            return extension;
        }

        /// <summary>
        /// Creates an LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container to contain extra information.
        /// </summary>
        /// <param name="annualAmount_rep">The annual amount of the cost.</param>
        /// <param name="description">The description of the cost.</param>
        /// <returns>An LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION container.</returns>
        private LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION CreateLQBEstimatedPropertyCostComponentExtension(string annualAmount_rep, string description)
        {
            var lqbExtension = new LQB_ESTIMATED_PROPERTY_COST_COMPONENT_EXTENSION();

            lqbExtension.ExpenseDescription = ToMismoString(description);

            lqbExtension.AnnualAmount = ToMismoAmount(annualAmount_rep);

            return lqbExtension;
        }

        /// <summary>
        /// Creates an ESTIMATED_PROPERTY_COST_DETAIL container with information regarding the property costs from the archived disclosure given by the document type. 
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An ESTIMATED_PROPERTY_COST_DETAIL container with information regarding the archived property costs.</returns>
        private ESTIMATED_PROPERTY_COST_DETAIL CreateEstimatedPropertyCostDetail(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var propertyCostDetail = new ESTIMATED_PROPERTY_COST_DETAIL();
            propertyCostDetail.ProjectedPaymentEstimatedTaxesInsuranceAssessmentTotalAmount = ToMismoAmount(this.DataLoan.sMonthlyNonPINonMIExpenseTotalPITI_rep);

            return propertyCostDetail;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SECTION_SUMMARIES container with items A-N and other summary information from the Loan Estimate/Closing Disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SECTION_SUMMARIES container with summary information from the given integrated disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SECTION_SUMMARIES CreateIntegratedDisclosureSectionSummaries(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var summaries = new INTEGRATED_DISCLOSURE_SECTION_SUMMARIES();

            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
            {
                foreach (BorrowerClosingCostFeeSection feeSection in this.LoanEstimateArchiveData.sClosingCostSet.GetViewBase(E_ClosingCostViewT.LoanClosingCost))
                {
                    //// A, B, C, E, F, G, H
                    summaries.IntegratedDisclosureSectionSummaryList.Add(
                        this.CreateIntegratedDisclosureSectionSummary(
                        documentType,
                        feeSection,
                        GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));
                }

                //// D, I, J
                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.LoanEstimateArchiveData.sTRIDLoanEstimateTotalLoanCosts_rep,
                    IntegratedDisclosureSectionBase.TotalLoanCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.Blank,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.LoanEstimateArchiveData.sTRIDLoanEstimateTotalOtherCosts_rep,
                    IntegratedDisclosureSectionBase.TotalOtherCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.Blank,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.LoanEstimateArchiveData.sTRIDLoanEstimateTotalClosingCosts_rep,
                    IntegratedDisclosureSectionBase.TotalClosingCosts,
                    string.Empty,
                    this.LoanEstimateArchiveData.sTRIDLoanEstimateTotalAllCosts_rep,
                    IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                var lenderCreditAmount = this.LoanEstimateArchiveData.sTRIDLoanEstimateLenderCredits_Neg_rep;
                if (this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic && documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
                {
                    lenderCreditAmount = this.LoanEstimateArchiveData.sTRIDLoanEstimateGeneralLenderCredits_Neg_rep;
                }

                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    string.Empty,
                    IntegratedDisclosureSectionBase.TotalClosingCosts,
                    string.Empty,
                    lenderCreditAmount,
                    IntegratedDisclosureSubsectionBase.LenderCredits,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));
            }
            else
            {
                //// Closing Disclosure
                //// A, B, C, E, F, G, H
                foreach (var feeSection in this.ClosingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.GetCompressedView(E_ClosingCostViewT.CombinedLenderTypeEstimate))
                {
                    summaries.IntegratedDisclosureSectionSummaryList.Add(
                        this.CreateIntegratedDisclosureSectionSummary(
                        documentType,
                        feeSection,
                        GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));
                }

                //// D, I, J
                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureTotalLoanCosts_rep,
                    IntegratedDisclosureSectionBase.TotalLoanCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.LoanCostsSubtotal,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureTotalOtherCosts_rep,
                    IntegratedDisclosureSectionBase.TotalOtherCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.OtherCostsSubtotal,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureTotalClosingCosts_rep,
                    IntegratedDisclosureSectionBase.TotalClosingCosts,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    string.Empty,
                    IntegratedDisclosureSectionBase.TotalClosingCosts,
                    string.Empty,
                    this.ClosingDisclosureArchiveData.sTRIDNegativeClosingDisclosureGeneralLenderCredits_rep,
                    IntegratedDisclosureSubsectionBase.LenderCredits,
                    this.ClosingDisclosureArchiveData.sToleranceCure_rep,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                //// K, L
                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.ClosingDisclosureArchiveData.sTRIDTotalDueFromBorrowerAtClosing_rep,
                    GetDisclosureSectionForSectionKAdjustments(this.ExportData.IsClosingPackage, this.DataLoan.sTRIDLoanEstimateCashToCloseCalcMethodT, this.DataLoan.sTRIDClosingDisclosureLastLoanEstimateCashToCloseCalcMethodT),
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                summaries.IntegratedDisclosureSectionSummaryList.Add(
                    this.CreateIntegratedDisclosureSectionSummary(
                    documentType,
                    this.ClosingDisclosureArchiveData.sTRIDTotalPaidAlreadyByOrOnBehalfOfBorrowerAtClosing_rep,
                    IntegratedDisclosureSectionBase.PaidAlreadyByOrOnBehalfOfBorrowerAtClosing,
                    string.Empty,
                    string.Empty,
                    IntegratedDisclosureSubsectionBase.Blank,
                    string.Empty,
                    GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                //// M, N
                if (!this.ClosingDisclosureArchiveData.sIsRefinancing)
                {
                    summaries.IntegratedDisclosureSectionSummaryList.Add(
                        this.CreateIntegratedDisclosureSectionSummary(
                        documentType,
                        this.ClosingDisclosureArchiveData.sTRIDTotalDueToSellerAtClosing_rep,
                        IntegratedDisclosureSectionBase.DueToSellerAtClosing,
                        string.Empty,
                        string.Empty,
                        IntegratedDisclosureSubsectionBase.Blank,
                        string.Empty,
                        GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));

                    summaries.IntegratedDisclosureSectionSummaryList.Add(
                        this.CreateIntegratedDisclosureSectionSummary(
                        documentType,
                        this.ClosingDisclosureArchiveData.sTRIDTotalDueFromSellerAtClosing_rep,
                        IntegratedDisclosureSectionBase.DueFromSellerAtClosing,
                        string.Empty,
                        string.Empty,
                        IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal,
                        string.Empty,
                        GetNextSequenceNumber(summaries.IntegratedDisclosureSectionSummaryList)));
                }
            }

            return summaries;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SECTION_SUMMARY container with information pertaining to a section of the Loan Estimate/Closing Disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <param name="sectionAmount">The section total amount.</param>
        /// <param name="section">The section type.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="subsectionAmount">The subsection total amount.</param>
        /// <param name="subsection">The subsection type.</param>
        /// <param name="toleranceCureAmount">The amount of the section total allocated to tolerance cures, if any.</param>
        /// <param name="sequence">The sequence number of the summary within the list of summaries.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SECTION_SUMMARY container with information pertaining to a section of the Loan Estimate/Closing Disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SECTION_SUMMARY CreateIntegratedDisclosureSectionSummary(IntegratedDisclosureDocumentBase documentType, string sectionAmount, IntegratedDisclosureSectionBase section, string sectionName, string subsectionAmount, IntegratedDisclosureSubsectionBase subsection, string toleranceCureAmount, int sequence)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var summary = new INTEGRATED_DISCLOSURE_SECTION_SUMMARY();
            summary.SequenceNumber = sequence;
            summary.IntegratedDisclosureSectionSummaryDetail = this.CreateIntegratedDisclosureSectionSummaryDetail(sectionAmount, section, sectionName, subsectionAmount, subsection, toleranceCureAmount);

            if (documentType == IntegratedDisclosureDocumentBase.ClosingDisclosure)
            {
                summary.IntegratedDisclosureSubsectionPayments = this.CreateIntegratedDisclosureSubsectionPayments_CD(section, subsection);
            }
            else if (this.ExportData.Options.DocumentVendor == E_DocumentVendor.DocMagic)
            {
                //// 10/26/2015 BB case 229689
                summary.IntegratedDisclosureSubsectionPayments = this.CreateIntegratedDisclosureSubsectionPayments_LE(section, subsection, subsectionAmount);
            }

            return summary;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL container with section and subsection totals from the closing cost details area of the integrated disclosure.
        /// </summary>
        /// <param name="sectionAmount">The section total amount.</param>
        /// <param name="section">The section type.</param>
        /// <param name="sectionName">The section name.</param>
        /// <param name="subsectionAmount">The subsection total amount.</param>
        /// <param name="subsection">The subsection type.</param>
        /// <param name="toleranceCureAmount">The amount of the section total allocated to tolerance cures, if any.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL container with section and subsection totals.</returns>
        private INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL CreateIntegratedDisclosureSectionSummaryDetail(string sectionAmount, IntegratedDisclosureSectionBase section, string sectionName, string subsectionAmount, IntegratedDisclosureSubsectionBase subsection, string toleranceCureAmount)
        {
            if (section == IntegratedDisclosureSectionBase.Blank || (string.IsNullOrEmpty(sectionAmount) && string.IsNullOrEmpty(subsectionAmount)))
            {
                return null;
            }

            var summaryDetail = new INTEGRATED_DISCLOSURE_SECTION_SUMMARY_DETAIL();

            if (!string.IsNullOrEmpty(sectionAmount))
            {
                summaryDetail.IntegratedDisclosureSectionTotalAmount = ToMismoAmount(sectionAmount);
            }

            summaryDetail.IntegratedDisclosureSectionType = section == IntegratedDisclosureSectionBase.Blank ? null : ToMismoEnum(section, isSensitiveData: false);

            if (section == IntegratedDisclosureSectionBase.Other)
            {
                summaryDetail.IntegratedDisclosureSectionTypeOtherDescription = ToMismoString(sectionName);
            }

            if (!string.IsNullOrEmpty(subsectionAmount))
            {
                summaryDetail.IntegratedDisclosureSubsectionTotalAmount = ToMismoAmount(subsectionAmount);
            }

            if (subsection != IntegratedDisclosureSubsectionBase.Blank)
            {
                summaryDetail.IntegratedDisclosureSubsectionType = ToMismoEnum(subsection, isSensitiveData: false);
            }

            if (!string.IsNullOrEmpty(toleranceCureAmount))
            {
                summaryDetail.LenderCreditToleranceCureAmount = ToMismoAmount(toleranceCureAmount);
            }

            return summaryDetail;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container with a list of payments pertaining to a subsection of a closing disclosure.
        /// </summary>
        /// <param name="section">The section type.</param>
        /// <param name="subsection">The subsection type.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container with a list of payments pertaining to a subsection of a closing disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS CreateIntegratedDisclosureSubsectionPayments_CD(IntegratedDisclosureSectionBase section, IntegratedDisclosureSubsectionBase subsection)
        {
            var payments = new INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS();
            payments.IntegratedDisclosureSubsectionPaymentList = new List<INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT>();

            Func<LoanClosingCostFee, bool> sectionPredicate;
            string amountAtClosing = string.Empty;
            string amountBeforeClosing = string.Empty;
            string sellerAmountAtClosing = string.Empty;
            string sellerAmountBeforeClosing = string.Empty;
            string amountPaidByOther = string.Empty;

            //// To-do: move these to the data-layer. Both the page and this/any export should pull these values from the data-layer rather than calculating them on the fly.
            if (section == IntegratedDisclosureSectionBase.TotalLoanCosts && subsection == IntegratedDisclosureSubsectionBase.LoanCostsSubtotal)
            {
                sectionPredicate = p =>
                p.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage) == E_IntegratedDisclosureSectionT.SectionA ||
                p.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage) == E_IntegratedDisclosureSectionT.SectionB ||
                p.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage) == E_IntegratedDisclosureSectionT.SectionC;

                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalLoanCostsOfBorrowerAtClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalLoanCostsOfBorrowerBeforeClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);

                amountAtClosing = this.ClosingDisclosureArchiveData.m_convertLos.ToMoneyString(this.ClosingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalLoanCostsOfBorrowerAtClosingFeesPred), FormatDirection.ToRep);
                amountBeforeClosing = this.ClosingDisclosureArchiveData.m_convertLos.ToMoneyString(this.ClosingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalLoanCostsOfBorrowerBeforeClosingFeesPred), FormatDirection.ToRep);
            }
            else if (section == IntegratedDisclosureSectionBase.TotalOtherCosts && subsection == IntegratedDisclosureSubsectionBase.OtherCostsSubtotal)
            {
                sectionPredicate = p =>
                p.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage) == E_IntegratedDisclosureSectionT.SectionE ||
                p.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage) == E_IntegratedDisclosureSectionT.SectionF ||
                p.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage) == E_IntegratedDisclosureSectionT.SectionG ||
                p.GetTRIDSectionBasedOnDisclosure(this.ExportData.IsClosingPackage) == E_IntegratedDisclosureSectionT.SectionH;

                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalOtherBorrowerCostsAtClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalOtherBorrowerCostsBeforeClosingFeesPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);

                amountAtClosing = this.ClosingDisclosureArchiveData.m_convertLos.ToMoneyString(this.ClosingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalOtherBorrowerCostsAtClosingFeesPred), FormatDirection.ToRep);
                amountBeforeClosing = this.ClosingDisclosureArchiveData.m_convertLos.ToMoneyString(this.ClosingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalOtherBorrowerCostsBeforeClosingFeesPred), FormatDirection.ToRep);
            }
            else if (section == IntegratedDisclosureSectionBase.TotalClosingCosts && subsection == IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal)
            {
                sectionPredicate = p => p != null;

                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalAllCostsBorrowerAtClosingPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.AtClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Borrower || p.PaidByT == E_ClosingCostFeePaymentPaidByT.BorrowerFinance);
                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalAllCostsSellerBeforeClosingPred = p => p.GfeClosingCostFeePaymentTimingT == E_GfeClosingCostFeePaymentTimingT.OutsideOfClosing && (p.PaidByT == E_ClosingCostFeePaymentPaidByT.Seller);
                Func<LoanClosingCostFeePayment, bool> tridClosingDisclosureTotalAllCostsOtherPred = p => (p.PaidByT != E_ClosingCostFeePaymentPaidByT.BorrowerFinance && p.PaidByT != E_ClosingCostFeePaymentPaidByT.Borrower && p.PaidByT != E_ClosingCostFeePaymentPaidByT.Seller);

                amountAtClosing = this.ClosingDisclosureArchiveData.m_convertLos.ToMoneyString(this.ClosingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalAllCostsBorrowerAtClosingPred), FormatDirection.ToRep);
                amountBeforeClosing = this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureClosingCostsPaidBeforeClosing_rep;

                if (!this.ClosingDisclosureArchiveData.sIsRefinancing)
                {
                    sellerAmountAtClosing = this.ClosingDisclosureArchiveData.sTotalSellerPaidClosingCostsPaidAtClosing_rep;
                    sellerAmountBeforeClosing = this.ClosingDisclosureArchiveData.m_convertLos.ToMoneyString(this.ClosingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalAllCostsSellerBeforeClosingPred), FormatDirection.ToRep);
                }

                amountPaidByOther = this.ClosingDisclosureArchiveData.m_convertLos.ToMoneyString(this.ClosingDisclosureArchiveData.sBorrowerAndSellerClosingCostSet.Sum(sectionPredicate, tridClosingDisclosureTotalAllCostsOtherPred), FormatDirection.ToRep);
            }
            else if (section == IntegratedDisclosureSectionBase.TotalClosingCosts && subsection == IntegratedDisclosureSubsectionBase.LenderCredits)
            {
                amountAtClosing = this.ClosingDisclosureArchiveData.sTRIDNegativeClosingDisclosureGeneralLenderCredits_rep;
            }
            else if (section == IntegratedDisclosureSectionBase.DueFromBorrowerAtClosing && subsection == IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal)
            {
                amountAtClosing = this.ClosingDisclosureArchiveData.sTRIDClosingDisclosureBorrowerCostsPaidAtClosing_rep;
            }
            else if (section == IntegratedDisclosureSectionBase.DueFromSellerAtClosing && subsection == IntegratedDisclosureSubsectionBase.ClosingCostsSubtotal)
            {
                amountAtClosing = this.ClosingDisclosureArchiveData.sTotalSellerPaidClosingCostsPaidAtClosing_rep;
            }
            else
            {
                return null;
            }
            //// End-of needs to be moved.

            payments.IntegratedDisclosureSubsectionPaymentList.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                IntegratedDisclosureSubsectionPaidByBase.Buyer,
                amountAtClosing,
                IntegratedDisclosureSubsectionPaymentTimingBase.AtClosing,
                GetNextSequenceNumber(payments.IntegratedDisclosureSubsectionPaymentList)));

            if (!string.IsNullOrEmpty(amountBeforeClosing))
            {
                payments.IntegratedDisclosureSubsectionPaymentList.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                    IntegratedDisclosureSubsectionPaidByBase.Buyer,
                    amountBeforeClosing,
                    IntegratedDisclosureSubsectionPaymentTimingBase.BeforeClosing,
                    GetNextSequenceNumber(payments.IntegratedDisclosureSubsectionPaymentList)));
            }

            if (!string.IsNullOrEmpty(sellerAmountAtClosing))
            {
                payments.IntegratedDisclosureSubsectionPaymentList.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                    IntegratedDisclosureSubsectionPaidByBase.Seller,
                    sellerAmountAtClosing,
                    IntegratedDisclosureSubsectionPaymentTimingBase.AtClosing,
                    GetNextSequenceNumber(payments.IntegratedDisclosureSubsectionPaymentList)));
            }

            if (!string.IsNullOrEmpty(sellerAmountBeforeClosing))
            {
                payments.IntegratedDisclosureSubsectionPaymentList.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                    IntegratedDisclosureSubsectionPaidByBase.Seller,
                    sellerAmountBeforeClosing,
                    IntegratedDisclosureSubsectionPaymentTimingBase.BeforeClosing,
                    GetNextSequenceNumber(payments.IntegratedDisclosureSubsectionPaymentList)));
            }

            if (!string.IsNullOrEmpty(amountPaidByOther))
            {
                payments.IntegratedDisclosureSubsectionPaymentList.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                    IntegratedDisclosureSubsectionPaidByBase.ThirdParty,
                    amountPaidByOther,
                    IntegratedDisclosureSubsectionPaymentTimingBase.Blank,
                    GetNextSequenceNumber(payments.IntegratedDisclosureSubsectionPaymentList)));
            }

            return payments;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT container with information pertaining to a subsection payment amount on the integrated disclosure.
        /// </summary>
        /// <param name="paidBy">The type of party that has / will make the payment (e.g. buyer | seller | other).</param>
        /// <param name="amount">The payment amount.</param>
        /// <param name="timing">Whether the payment will be made at closing or prior to.</param>
        /// <param name="sequence">The sequence number of the payment among the list of payments.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT container with information pertaining to a subsection payment amount on the integrated disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT CreateIntegratedDisclosureSubsectionPayment(IntegratedDisclosureSubsectionPaidByBase paidBy, string amount, IntegratedDisclosureSubsectionPaymentTimingBase timing, int sequence)
        {
            var payment = new INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENT();
            payment.IntegratedDisclosureSubsectionPaidByType = paidBy == IntegratedDisclosureSubsectionPaidByBase.Blank ? null : ToMismoEnum(paidBy, isSensitiveData: false);
            payment.IntegratedDisclosureSubsectionPaymentAmount = ToMismoAmount(amount);
            payment.IntegratedDisclosureSubsectionPaymentTimingType = timing == IntegratedDisclosureSubsectionPaymentTimingBase.Blank ? null : ToMismoEnum(timing, isSensitiveData: false);
            payment.SequenceNumber = sequence;

            return payment;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container with a list of payments pertaining to a subsection of a loan estimate.
        /// </summary>
        /// <param name="section">The section type.</param>
        /// <param name="subsection">The subsection type.</param>
        /// <param name="subsectionAmount">The payment amount.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS container with a list of payments pertaining to a subsection of a loan estimate.</returns>
        private INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS CreateIntegratedDisclosureSubsectionPayments_LE(IntegratedDisclosureSectionBase section, IntegratedDisclosureSubsectionBase subsection, string subsectionAmount)
        {
            if (section == IntegratedDisclosureSectionBase.Blank || subsection != IntegratedDisclosureSubsectionBase.LenderCredits)
            {
                return null;
            }

            var payments = new INTEGRATED_DISCLOSURE_SUBSECTION_PAYMENTS();

            payments.IntegratedDisclosureSubsectionPaymentList.Add(this.CreateIntegratedDisclosureSubsectionPayment(
                IntegratedDisclosureSubsectionPaidByBase.Buyer,
                subsectionAmount,
                IntegratedDisclosureSubsectionPaymentTimingBase.Blank,
                GetNextSequenceNumber(payments.IntegratedDisclosureSubsectionPaymentList)));

            return payments;
        }

        /// <summary>
        /// Creates an INTEGRATED_DISCLOSURE_SECTION_SUMMARY container with information pertaining to a section of the Loan Estimate/Closing Disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <param name="feeSection">A <see cref="BorrowerClosingCostFeeSection" /> object with fee data from a section of the given integrated disclosure.</param>
        /// <param name="sequence">The sequence number of the summary within the list of summaries.</param>
        /// <returns>An INTEGRATED_DISCLOSURE_SECTION_SUMMARY container with information pertaining to a section of the Loan Estimate/Closing Disclosure.</returns>
        private INTEGRATED_DISCLOSURE_SECTION_SUMMARY CreateIntegratedDisclosureSectionSummary(IntegratedDisclosureDocumentBase documentType, BorrowerClosingCostFeeSection feeSection, int sequence)
        {
            if (!this.HasArchive || feeSection.SectionType == E_IntegratedDisclosureSectionT.LeaveBlank || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var summary = new INTEGRATED_DISCLOSURE_SECTION_SUMMARY();
            summary.SequenceNumber = sequence;

            IntegratedDisclosureSectionBase section = GetIntegratedDisclosureSectionBaseValue(documentType, feeSection.SectionType);

            summary.IntegratedDisclosureSectionSummaryDetail = this.CreateIntegratedDisclosureSectionSummaryDetail(
                documentType == IntegratedDisclosureDocumentBase.LoanEstimate ? feeSection.TotalAmount_rep : feeSection.TotalAmountBorrPaid_rep,
                section,
                feeSection.SectionName,
                string.Empty,
                IntegratedDisclosureSubsectionBase.Blank,
                string.Empty);

            return summary;
        }

        /// <summary>
        /// Creates a container with a list of Loan Disclosure and Consideration statements from the related integrated disclosure. 
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>An OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS object containing a list of consideration and disclosure statements from the related integrated disclosure.</returns>
        private OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS CreateOtherLoanConsiderationsAndDisclosuresItems(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            var items = new OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEMS();
            string description = string.Empty;

            //// To-do: move these to the data-layer. Both the page and this/any export should pull these values from the data-layer rather than calculating them on the fly.
            if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
            {
                description = "We may order an appraisal to determine the property's value and charge you for this appraisal. We will promptly give you a copy of any appraisal, even if your loan does not close. You can pay for an additional appraisal for your own use at your own cost.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Appraisal, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "If you sell or transfer this property to another person, we ";
                description += (this.LoanEstimateArchiveData.sAssumeLT == E_sAssumeLT.May || this.LoanEstimateArchiveData.sAssumeLT == E_sAssumeLT.MaySubjectToCondition) ?
                    "will allow, under certain conditions, this person to assume this loan on the original terms." : "will not allow assumption of this loan on the original terms.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Assumption, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                if (this.LoanEstimateArchiveData.sReqPropIns)
                {
                    description = "This loan requires homeowner's insurance on the property, which you may obtain from a company of your choice that we find acceptable.";
                    items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.HomeownersInsurance, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));
                }

                description = string.Format("If your payment is more than {0} days late, we will charge a late fee of {1} of {2}.", this.LoanEstimateArchiveData.sLateDays, this.LoanEstimateArchiveData.sLateChargePc, this.LoanEstimateArchiveData.sLateChargeBaseDesc);
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.LatePayment, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "Refinancing this loan will depend on your future financial situation, the property value, and market conditions. You may not be able to refinance this loan.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Refinance, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "We intend ";
                description += this.LoanEstimateArchiveData.sTRIDLoanEstimateLenderIntendsToServiceLoan ? "to service your loan. If so, you will make payments to us." : "to transfer servicing of your loan.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Servicing, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "Certain State law protections against liability for any deficiency after foreclosure may be lost upon refinancing. The potential consequences of the loss of such protections include the consumer's liability for the unpaid balance. The consumer should consult an attorney for additional information.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.LiabilityAfterForeclosure, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));
            }
            else
            {
                //// Closing Disclosure
                description = "If you sell or transfer this property to another person, we ";
                description += (this.ClosingDisclosureArchiveData.sAssumeLT == E_sAssumeLT.May || this.ClosingDisclosureArchiveData.sAssumeLT == E_sAssumeLT.MaySubjectToCondition) ? "will allow, under certain conditions, this person to assume this loan on the original terms." : "will not allow assumption of this loan on the original terms.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Assumption, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "Your loan ";
                description += this.ClosingDisclosureArchiveData.sHasDemandFeature ? "has a demand feature which permits your lender to require early repayment of the loan." : "does not have a demand feature.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.DemandFeature, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = string.Format("If your payment is more than {0} days late, we will charge a late fee of {1} of {2}.", this.ClosingDisclosureArchiveData.sLateDays, this.ClosingDisclosureArchiveData.sLateChargePc, this.ClosingDisclosureArchiveData.sLateChargeBaseDesc);
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.LatePayment, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "Under your loan terms you ";
                if (this.ClosingDisclosureArchiveData.sTRIDNegativeAmortizationT == E_sTRIDNegativeAmortizationT.Scheduled)
                {
                    description += "are scheduled to make monthly payments that do not pay all of the interest due that month.";
                }
                else if (this.ClosingDisclosureArchiveData.sTRIDNegativeAmortizationT == E_sTRIDNegativeAmortizationT.Potential)
                {
                    description += "may have monthly payments that do not pay all of the interest due that month.";
                }
                else
                {
                    description += "do not have a negative amortization feature.";
                }

                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.NegativeAmortization, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "Your lender ";
                if (this.ClosingDisclosureArchiveData.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.AcceptedAndApplied)
                {
                    description += "may accept partial payments that are less than the full amount due (partial payments) and apply them to your loan.";
                }
                else if (this.ClosingDisclosureArchiveData.sTRIDPartialPaymentAcceptanceT == E_sTRIDPartialPaymentAcceptanceT.AcceptedButNotApplied)
                {
                    description += "may hold them in a separate account until you pay the rest of the payment, and then apply the full payment to your loan.";
                }
                else
                {
                    description += "does not accept partial payments.";
                }

                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.PartialPayment, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "You are granting a security interest in " + this.ClosingDisclosureArchiveData.sSpFullAddr;
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.SecurityInterest, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "For now, your loan ";
                if (this.ClosingDisclosureArchiveData.sTridEscrowAccountExists)
                {
                    description += "will have an escrow account to pay the property costs listed below.";
                }
                else
                {
                    description += "will not have an escrow account because ";
                    description += this.ClosingDisclosureArchiveData.sNonMIHousingExpensesNotEscrowedReasonT == E_sNonMIHousingExpensesNotEscrowedReasonT.BorrowerDeclined ? "you declined it." : "your lender does not offer one.";
                }

                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.EscrowAccountCurrent, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "We may order an appraisal to determine the property's value and charge you for this appraisal. We will promptly give you a copy of any appraisal, even if your loan does not close. You can pay for an additional appraisal for your own use at your own cost.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Appraisal, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "See your note and security instrument for information about what happens if you fail to make your payments, what is a default on the loan, situations in which your lender can require early repayments of the loan, and the rules for making payments before they are due.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.ContractDetails, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "If your lender forecloses on this property and the foreclosure does not cover the amount of unpaid balance on this loan, ";
                description += this.ClosingDisclosureArchiveData.sTRIDPropertyIsInNonRecourseLoanState ? "state law may protect you from liability for the unpaid balance. If you refinance or take on any additional debt on this property, you may lose this protection and have to pay any debt remaining even after foreclosure. You may want to consult a lawyer for more information." : "state law does not protect you from liability for the unpaid balance.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.LiabilityAfterForeclosure, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "Refinancing this loan will depend on your future financial situation, the property value, and market conditions. You may not be able to refinance this loan.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.Refinance, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));

                description = "If you borrow more than this property is worth, the interest on the loan amount above this property's fair market value is not deductible from your federal income taxes. You should consult a tax advisor for more information.";
                items.OtherLoanConsiderationsAndDisclosuresItemList.Add(this.CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase.TaxDeductions, description, GetNextSequenceNumber(items.OtherLoanConsiderationsAndDisclosuresItemList)));
            }
            //// End-of needs to be moved.

            return items;
        }

        /// <summary>
        /// Creates a container with a Loan Disclosure or Consideration statement from the related integrated disclosure.
        /// </summary>
        /// <param name="statementType">The type of disclosure or consideration statement.</param>
        /// <param name="description">The text of the statement.</param>
        /// <param name="sequence">The sequence number of the statement among the list of statements.</param>
        /// <returns>An OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM object containing a consideration or disclosure statement from the related integrated disclosure.</returns>
        private OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM CreateOtherLoanConsiderationsAndDisclosuresItem(LoanConsiderationDisclosureStatementBase statementType, string description, int sequence)
        {
            var item = new OTHER_LOAN_CONSIDERATIONS_AND_DISCLOSURES_ITEM();
            item.LoanConsiderationDisclosureStatementDescription = ToMismoString(description);
            item.LoanConsiderationDisclosureStatementType = ToMismoEnum(statementType, isSensitiveData: false);
            item.SequenceNumber = sequence;

            return item;
        }

        /// <summary>
        /// Creates a PROJECTED_PAYMENTS object with a list of projected payments as displayed on a Loan Estimate or Closing Disclosure.
        /// </summary>
        /// <param name="documentType">The type of integrated disclosure document, e.g. LoanEstimate | ClosingDisclosure.</param>
        /// <returns>A PROJECTED_PAYMENTS object with a list of projected payments as displayed on a Loan Estimate or Closing Disclosure.</returns>
        private PROJECTED_PAYMENTS CreateProjectedPayments(IntegratedDisclosureDocumentBase documentType)
        {
            if (!this.HasArchive || (documentType != IntegratedDisclosureDocumentBase.LoanEstimate && documentType != IntegratedDisclosureDocumentBase.ClosingDisclosure))
            {
                return null;
            }

            List<TridProjectedPayment> payments;

            try
            {
                if (documentType == IntegratedDisclosureDocumentBase.LoanEstimate)
                {
                    payments = this.LoanEstimateArchiveData.sTridProjectedPayments;
                }
                else
                {
                    payments = this.ClosingDisclosureArchiveData.sTridProjectedPayments;
                }
            }
            catch (CBaseException) when (this.ExportData.Options.IsLenderExportMode)
            {
                payments = null;
            }

            if (payments == null || payments.Count == 0)
            {
                return null;
            }

            var projectedPayments = new PROJECTED_PAYMENTS();

            foreach (TridProjectedPayment payment in payments)
            {
                projectedPayments.ProjectedPaymentList.Add(this.CreateProjectedPayment(payment, GetNextSequenceNumber(projectedPayments.ProjectedPaymentList)));
            }

            return projectedPayments;
        }

        /// <summary>
        /// Creates a container with amounts and other information pertaining to a projected payment from the related integrated disclosure.
        /// </summary>
        /// <param name="projectedPayment">A <see cref="TridProjectedPayment"/>.</param>
        /// <param name="sequence">The sequence number of the projected payment among the list of payments.</param>
        /// <returns>A PROJECTED_PAYMENT container with amounts and other information pertaining to a projected payment from the related integrated disclosure.</returns>
        private PROJECTED_PAYMENT CreateProjectedPayment(TridProjectedPayment projectedPayment, int sequence)
        {
            var payment = new PROJECTED_PAYMENT();
            payment.PaymentFrequencyType = ToMismoEnum(PaymentFrequencyBase.Monthly, isSensitiveData: false);
            payment.ProjectedPaymentCalculationPeriodEndNumber = ToMismoNumeric(projectedPayment.EndYear_rep);
            payment.ProjectedPaymentCalculationPeriodStartNumber = ToMismoNumeric(projectedPayment.StartYear_rep);
            payment.ProjectedPaymentCalculationPeriodTermType = ToMismoEnum(ProjectedPaymentCalculationPeriodTermBase.Yearly, isSensitiveData: false);
            payment.ProjectedPaymentEstimatedEscrowPaymentAmount = ToMismoAmount(projectedPayment.Escrow_rep);
            payment.ProjectedPaymentEstimatedTotalMaximumPaymentAmount = ToMismoAmount(projectedPayment.PaymentMax_rep);
            payment.ProjectedPaymentEstimatedTotalMinimumPaymentAmount = ToMismoAmount(projectedPayment.PaymentMin_rep);
            payment.ProjectedPaymentMIPaymentAmount = ToMismoAmount(projectedPayment.MI_rep);
            payment.ProjectedPaymentPrincipalAndInterestMaximumPaymentAmount = ToMismoAmount(projectedPayment.PIMax_rep);
            payment.ProjectedPaymentPrincipalAndInterestMinimumPaymentAmount = ToMismoAmount(projectedPayment.PIMin_rep);
            payment.SequenceNumber = sequence;

            payment.Extension = this.CreateProjectedPaymentExtension(projectedPayment.HasInterestOnly);

            return payment;
        }

        /// <summary>
        /// Creates a PROJECTED_PAYMENT_EXTENSION container with information regarding the projected payment that is not captured by the MISMO standard.
        /// </summary>
        /// <param name="interestOnly">Indicates whether the projected payment includes an interest-only payment.</param>
        /// <returns>A PROJECTED_PAYMENT_EXTENSION container.</returns>
        private PROJECTED_PAYMENT_EXTENSION CreateProjectedPaymentExtension(bool interestOnly)
        {
            var extension = new PROJECTED_PAYMENT_EXTENSION();
            extension.Other = this.CreateLQBProjectedPaymentExtension(interestOnly);

            return extension;
        }

        /// <summary>
        /// Creates an LQB_PROJECTED_PAYMENT_EXTENSION container with proprietary data-points.
        /// </summary>
        /// <param name="interestOnly">Indicates whether the projected payment includes an interest-only payment.</param>
        /// <returns>An LQB_PROJECTED_PAYMENT_EXTENSION container.</returns>
        private LQB_PROJECTED_PAYMENT_EXTENSION CreateLQBProjectedPaymentExtension(bool interestOnly)
        {
            var extension = new LQB_PROJECTED_PAYMENT_EXTENSION();
            extension.InterestOnlyIndicator = ToMismoIndicator(interestOnly);

            return extension;
        }

        /// <summary>
        /// Creates a container representing the URLA document.
        /// </summary>
        /// <returns>A URLA container.</returns>
        private URLA CreateUrla()
        {
            var urla = new URLA();
            urla.UrlaDetail = this.CreateUrlaDetail();
            urla.UrlaTotal = this.CreateUrlaTotal();
            urla.UrlaTotalHousingExpenses = this.CreateUrlaTotalHousingExpenses();

            return urla;
        }

        /// <summary>
        /// Creates a container with details on the URLA document.
        /// </summary>
        /// <returns>A URLA_DETAIL container populated with data.</returns>
        private URLA_DETAIL CreateUrlaDetail()
        {
            var urlaDetail = new URLA_DETAIL();
            urlaDetail.RequiredDepositIndicator = ToMismoIndicator(this.DataLoan.sAprIncludesReqDeposit);
            urlaDetail.AdditionalBorrowerAssetsConsideredIndicator = ToMismoIndicator(this.DataLoan.sMultiApps);
            urlaDetail.AdditionalBorrowerAssetsNotConsideredIndicator = ToMismoIndicator(this.PrimaryApp.aSpouseIExcl);
            urlaDetail.BorrowerRequestedInterestRatePercent = ToMismoPercent(this.DataLoan.sNoteIR_rep);
            urlaDetail.BorrowerRequestedLoanAmount = ToMismoAmount(this.DataLoan.sFinalLAmt_rep);
            urlaDetail.AlterationsImprovementsAndRepairsAmount = ToMismoAmount(this.DataLoan.sAltCost_rep);
            urlaDetail.BorrowerPaidDiscountPointsTotalAmount = ToMismoAmount(this.DataLoan.sLDiscnt1003_rep);
            urlaDetail.EstimatedClosingCostsAmount = ToMismoAmount(this.DataLoan.sTotEstCcNoDiscnt1003_rep);
            urlaDetail.PurchasePriceAmount = ToMismoAmount(this.DataLoan.sPurchasePrice1003_rep);
            urlaDetail.PrepaidItemsEstimatedAmount = ToMismoAmount(this.DataLoan.sTotEstPp1003_rep);
            urlaDetail.SellerPaidClosingCostsAmount = ToMismoAmount(this.DataLoan.sTotCcPbs_rep);
            urlaDetail.MIAndFundingFeeFinancedAmount = ToMismoAmount(this.DataLoan.sFfUfmipFinanced_rep);
            urlaDetail.MIAndFundingFeeTotalAmount = ToMismoAmount(this.DataLoan.sFfUfmip1003_rep);

            var appTakenMethod = GetApplicationTakenMethodBaseValue(this.PrimaryApp.aIntrvwrMethodT);
            urlaDetail.ApplicationTakenMethodType = appTakenMethod == ApplicationTakenMethodBase.Blank ? null : ToMismoEnum(appTakenMethod, isSensitiveData: false);

            if (this.DataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                urlaDetail.ARMTypeDescription = ToMismoString(this.DataLoan.sFinMethDesc);
            }

            if (this.DataLoan.sApp1003InterviewerPrepareDate.IsValid)
            {
                urlaDetail.ApplicationSignedByLoanOriginatorDate = ToMismoDate(this.DataLoan.sApp1003InterviewerPrepareDate_rep);
            }

            // Refinance loan data points
            if (this.DataLoan.sIsRefinancing)
            {
                urlaDetail.RefinanceImprovementCostsAmount = ToMismoAmount(this.DataLoan.sSpImprovC_rep);

                var refinanceImprovements = GetRefinanceImprovementsBaseValue(this.DataLoan.sSpImprovTimeFrameT);
                urlaDetail.RefinanceImprovementsType = refinanceImprovements == RefinanceImprovementsBase.Blank ? null : ToMismoEnum(refinanceImprovements, isSensitiveData: false);
                urlaDetail.RefinanceProposedImprovementsDescription = ToMismoString(this.DataLoan.sSpImprovDesc);
            }

            if (!IsAmountStringBlankOrZero(this.DataLoan.sRefPdOffAmt1003_rep))
            {
                urlaDetail.RefinanceIncludingDebtsToBePaidOffAmount = ToMismoAmount(this.DataLoan.sRefPdOffAmt1003_rep);
            }

            urlaDetail.Extension = this.CreateUrlaDetailExtension();

            return urlaDetail;
        }

        /// <summary>
        /// Creates a container holding extension data for URLA_DETAIL.
        /// </summary>
        /// <returns>An URLA_DETAIL_EXTENSION container.</returns>
        private URLA_DETAIL_EXTENSION CreateUrlaDetailExtension()
        {
            var extension = new URLA_DETAIL_EXTENSION();
            extension.Other = this.CreateLQBUrlaDetailExtension();

            return extension;
        }

        /// <summary>
        /// Creates a container holding proprietary extension data for URLA_DETAIL.
        /// </summary>
        /// <returns>An LQB_URLA_DETAIL_EXTENSION container.</returns>
        private LQB_URLA_DETAIL_EXTENSION CreateLQBUrlaDetailExtension()
        {
            var extension = new LQB_URLA_DETAIL_EXTENSION();
            extension.MannerInWhichTitleHeld = ToMismoString(this.PrimaryApp.aManner);
            extension.LandIfAcquiredSeparatelyAmount = ToMismoAmount(this.DataLoan.sLandIfAcquiredSeparately1003_rep);

            return extension;
        }

        /// <summary>
        /// Creates a container holding URLA Total information.
        /// </summary>
        /// <returns>A URLA_TOTAL container.</returns>
        private URLA_TOTAL CreateUrlaTotal()
        {
            var urlaTotal = new URLA_TOTAL();
            urlaTotal.URLATotalBaseIncomeAmount = ToMismoAmount(this.DataLoan.sLTotBaseI_rep);
            urlaTotal.URLATotalCashFromToBorrowerAmount = ToMismoAmount(this.DataLoan.sTransNetCash_rep);
            urlaTotal.URLATotalLiabilityMonthlyPaymentsAmount = ToMismoAmount(this.DataLoan.sLiaMonLTot_rep);
            urlaTotal.URLATotalMonthlyIncomeAmount = ToMismoAmount(this.DataLoan.sLTotI_rep);
            urlaTotal.URLATotalOtherTypesOfIncomeAmount = ToMismoAmount(this.DataLoan.sLTotOI_rep);
            urlaTotal.URLATotalTransactionCostAmount = ToMismoAmount(this.DataLoan.sTotTransC_rep);
            urlaTotal.URLATotalLotAndImprovementsAmount = ToMismoAmount(this.DataLoan.sLotWImprovTot_rep);

            return urlaTotal;
        }

        /// <summary>
        /// Creates a URLA_TOTAL_HOUSING_EXPENSES container with the proposed housing expenses from section V of the URLA.
        /// </summary>
        /// <returns>A URLA_TOTAL_HOUSING_EXPENSES container with the proposed housing expenses from section V of the URLA.</returns>
        private URLA_TOTAL_HOUSING_EXPENSES CreateUrlaTotalHousingExpenses()
        {
            var expenses = new URLA_TOTAL_HOUSING_EXPENSES();
            expenses.UrlaTotalHousingExpenseList.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.FirstMortgagePrincipalAndInterest, GetNextSequenceNumber(expenses.UrlaTotalHousingExpenseList)));
            expenses.UrlaTotalHousingExpenseList.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.OtherMortgageLoanPrincipalAndInterest, GetNextSequenceNumber(expenses.UrlaTotalHousingExpenseList)));
            expenses.UrlaTotalHousingExpenseList.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.HomeownersInsurance, GetNextSequenceNumber(expenses.UrlaTotalHousingExpenseList)));
            expenses.UrlaTotalHousingExpenseList.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.RealEstateTax, GetNextSequenceNumber(expenses.UrlaTotalHousingExpenseList)));
            expenses.UrlaTotalHousingExpenseList.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.MIPremium, GetNextSequenceNumber(expenses.UrlaTotalHousingExpenseList)));
            expenses.UrlaTotalHousingExpenseList.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.HomeownersAssociationDuesAndCondominiumFees, GetNextSequenceNumber(expenses.UrlaTotalHousingExpenseList)));
            expenses.UrlaTotalHousingExpenseList.Add(this.CreateUrlaTotalHousingExpense(HousingExpenseBase.Other, GetNextSequenceNumber(expenses.UrlaTotalHousingExpenseList)));

            return expenses;
        }

        /// <summary>
        /// Creates a URLA_TOTAL_HOUSING_EXPENSE with a proposed housing expense listed on section V of the URLA.
        /// </summary>
        /// <param name="expenseType">The type of housing expense.</param>
        /// <param name="sequence">The sequence of the expense among the list of proposed housing expenses.</param>
        /// <returns>A URLA_TOTAL_HOUSING_EXPENSE with a proposed housing expense listed on section V of the URLA.</returns>
        private URLA_TOTAL_HOUSING_EXPENSE CreateUrlaTotalHousingExpense(HousingExpenseBase expenseType, int sequence)
        {
            var expense = new URLA_TOTAL_HOUSING_EXPENSE();
            expense.HousingExpenseType = ToMismoEnum(expenseType, isSensitiveData: false);
            expense.SequenceNumber = sequence;

            if (expenseType == HousingExpenseBase.FirstMortgagePrincipalAndInterest)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.DataLoan.sProFirstMPmt_rep);
            }
            else if (expenseType == HousingExpenseBase.OtherMortgageLoanPrincipalAndInterest)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.DataLoan.sProSecondMPmt_rep);
            }
            else if (expenseType == HousingExpenseBase.RealEstateTax)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.DataLoan.sProRealETx_rep);
            }
            else if (expenseType == HousingExpenseBase.HomeownersAssociationDuesAndCondominiumFees)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.DataLoan.sProHoAssocDues_rep);
            }
            else if (expenseType == HousingExpenseBase.Other)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.DataLoan.sProOHExp_rep);
                expense.HousingExpenseTypeOtherDescription = ToMismoString(this.DataLoan.sProOHExpDesc);
            }
            else if (expenseType == HousingExpenseBase.HomeownersInsurance)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.DataLoan.sProHazIns_rep);
            }
            else if (expenseType == HousingExpenseBase.MIPremium)
            {
                expense.URLATotalHousingExpensePaymentAmount = ToMismoAmount(this.DataLoan.sProMIns_rep);
            }
            else
            {
                return null;
            }

            return expense;
        }

        /// <summary>
        /// Creates a container representing the security instrument document.
        /// </summary>
        /// <returns>A SECURITY_INSTRUMENT container.</returns>
        private SECURITY_INSTRUMENT CreateSecurityInstrument()
        {
            var securityInstrument = new SECURITY_INSTRUMENT();
            securityInstrument.SecurityInstrumentDetail = this.CreateSecurityInstrumentDetail();

            return securityInstrument;
        }

        /// <summary>
        /// Creates a container holding details on the security instrument document.
        /// </summary>
        /// <returns>A SECURITY_INSTRUMENT_DETAIL container.</returns>
        private SECURITY_INSTRUMENT_DETAIL CreateSecurityInstrumentDetail()
        {
            var securityInstrumentDetail = new SECURITY_INSTRUMENT_DETAIL();
            securityInstrumentDetail.DefaultAcknowledgmentOfCashAdvanceAgainstNonHomesteadPropertyIndicator = ToMismoIndicator(this.DataLoan.sAcknowledgeCashAdvanceForNonHomestead);
            securityInstrumentDetail.SecurityInstrumentAttorneyFeePercent = ToMismoPercent(this.DataLoan.sSecInstrAttorneyFeesPc_rep);
            securityInstrumentDetail.SecurityInstrumentMultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator = ToMismoIndicator(this.DataLoan.sNyPropertyStatementT == E_sNyPropertyStatementT.MultipleUnitsRealPropertyIsImprovedOrToBeImprovedIndicator);
            securityInstrumentDetail.SecurityInstrumentOweltyOfPartitionIndicator = ToMismoIndicator(this.DataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.OweltyOfPartition);
            securityInstrumentDetail.SecurityInstrumentPurchaseMoneyIndicator = ToMismoIndicator(this.DataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.PurchaseMoney);
            securityInstrumentDetail.SecurityInstrumentRealPropertyImprovedOrToBeImprovedIndicator = ToMismoIndicator(this.DataLoan.sNyPropertyStatementT == E_sNyPropertyStatementT.RealPropertyImprovedOrToBeImprovedIndicator);
            securityInstrumentDetail.SecurityInstrumentRealPropertyImprovementsNotCoveredIndicator = ToMismoIndicator(this.DataLoan.sNyPropertyStatementT == E_sNyPropertyStatementT.RealPropertyImprovementsNotCoveredIndicator);
            securityInstrumentDetail.SecurityInstrumentRenewalAndExtensionOfLiensAgainstHomesteadPropertyIndicator = ToMismoIndicator(this.DataLoan.sTxSecurityInstrumentPar27T == E_sTxSecurityInstrumentPar27T.RenewalAndExtensionOfLiens);
            securityInstrumentDetail.SecurityInstrumentTrusteeFeePercent = ToMismoPercent(this.DataLoan.sTrusteeFeePc_rep);
            securityInstrumentDetail.SecurityInstrumentVestingDescription = ToMismoString(this.DataLoan.sVestingToRead);

            return securityInstrumentDetail;
        }

        /// <summary>
        /// Creates a container that holds all instances of RECORDING_ENDORSEMENT.
        /// </summary>
        /// <returns>A RECORDING_ENDORSEMENTS container.</returns>
        private RECORDING_ENDORSEMENTS CreateRecordingEndorsements()
        {
            var recordingEndorsements = new RECORDING_ENDORSEMENTS();
            recordingEndorsements.RecordingEndorsementList.Add(this.CreateRecordingEndorsement(GetNextSequenceNumber(recordingEndorsements.RecordingEndorsementList)));

            return recordingEndorsements;
        }

        /// <summary>
        /// Creates a container that holds information on a recording endorsement.
        /// </summary>
        /// <param name="sequence">The sequence number of the recording endorsement among the list of endorsements.</param>
        /// <returns>A RECORDING_ENDORSEMENT container.</returns>
        private RECORDING_ENDORSEMENT CreateRecordingEndorsement(int sequence)
        {
            var recordingEndorsement = new RECORDING_ENDORSEMENT();
            recordingEndorsement.RecordingEndorsementInformation = this.CreateRecordingEndorsementInformation();
            recordingEndorsement.SequenceNumber = sequence;

            return recordingEndorsement;
        }

        /// <summary>
        /// Creates a container holding data on a recording endorsement.
        /// </summary>
        /// <returns>A RECORDING_ENDORSEMENT_INFORMATION object.</returns>
        private RECORDING_ENDORSEMENT_INFORMATION CreateRecordingEndorsementInformation()
        {
            var recordingEndorsementInformation = new RECORDING_ENDORSEMENT_INFORMATION();
            recordingEndorsementInformation.CountyOfRecordationName = ToMismoString(this.DataLoan.sSpCounty);
            recordingEndorsementInformation.FirstPageNumberValue = ToMismoValue(this.DataLoan.sRecordingPageNum);
            recordingEndorsementInformation.InstrumentNumberIdentifier = ToMismoIdentifier(this.DataLoan.sRecordingInstrumentNum);
            recordingEndorsementInformation.RecordedDatetime = ToMismoDatetime(this.DataLoan.sRecordedD_rep_WithTime, false);
            recordingEndorsementInformation.RecordingEndorsementIdentifier = ToMismoIdentifier(this.DataLoan.sRecordingBookNum);
            recordingEndorsementInformation.StateOfRecordationName = ToMismoString(this.ExportData.StatesAndTerritories.GetStateName(this.DataLoan.sSpState));
            recordingEndorsementInformation.VolumeIdentifier = ToMismoIdentifier(this.DataLoan.sRecordingVolumeNum);

            return recordingEndorsementInformation;
        }

        /// <summary>
        /// Creates a container holding information on the loan execution.
        /// </summary>
        /// <returns>An EXECUTION container.</returns>
        private EXECUTION CreateExecution()
        {
            var execution = new EXECUTION();

            execution.Address = CreateAddress(
                this.DataLoan.sExecutionLocationAddress,
                this.DataLoan.sExecutionLocationCity,
                this.DataLoan.sExecutionLocationState,
                this.DataLoan.sExecutionLocationZip,
                this.DataLoan.sExecutionLocationCounty,
                string.Empty,
                string.Empty,
                AddressBase.Blank);
            execution.ExecutionDetail = this.CreateExecutionDetail();

            return execution;
        }

        /// <summary>
        /// Creates a container holding information on the details of the loan execution.
        /// </summary>
        /// <returns>An EXECUTION_DETAIL container.</returns>
        private EXECUTION_DETAIL CreateExecutionDetail()
        {
            var executionDetail = new EXECUTION_DETAIL();
            executionDetail.ExecutionDate = ToMismoDate(this.DataLoan.sDocMagicClosingD_rep);
            return executionDetail;
        }

        /// <summary>
        /// Creates a container representing the Truth In Lending document.
        /// </summary>
        /// <returns>A TIL_DISCLOSURE container.</returns>
        private TIL_DISCLOSURE CreateTilDisclosure()
        {
            var tilDisclosure = new TIL_DISCLOSURE();
            tilDisclosure.TilDisclosureDetail = this.CreateTilDisclosureDetail();

            return tilDisclosure;
        }

        /// <summary>
        /// Creates a container holding details on the Truth In Lending document.
        /// </summary>
        /// <returns>A TIL_DISCLOSURE_DETAIL container.</returns>
        private TIL_DISCLOSURE_DETAIL CreateTilDisclosureDetail()
        {
            var tilDisclosureDetail = new TIL_DISCLOSURE_DETAIL();
            tilDisclosureDetail.TILDisclosureDate = ToMismoDate(this.DataLoan.sTilPrepareDate_rep);

            return tilDisclosureDetail;
        }

        /// <summary>
        /// Creates a container representing the Good Faith Estimate document.
        /// </summary>
        /// <returns>A GFE container.</returns>
        private GFE CreateGfe()
        {
            var gfe = new GFE();
            gfe.GfeDetail = this.CreateGfeDetail();
            gfe.GfeSectionSummaries = this.CreateGfeSectionSummaries();

            return gfe;
        }

        /// <summary>
        /// Creates a container holding details on the Good Faith Estimate.
        /// </summary>
        /// <returns>A GFE_DETAIL container.</returns>
        private GFE_DETAIL CreateGfeDetail()
        {
            var gfeDetail = new GFE_DETAIL();
            gfeDetail.GFEDisclosureDate = ToMismoDate(this.DataLoan.sGfeTilPrepareDate_rep);
            gfeDetail.GFEInterestRateAvailableThroughDate = ToMismoDate(this.DataLoan.sIsRateLocked ? this.DataLoan.sRLckdExpiredD_rep : this.DataLoan.sGfeNoteIRAvailTillD_Date);
            gfeDetail.GFELoanOriginatorFeePaymentCreditType = ToMismoEnum(GetGfeLoanOriginatorFeePaymentCreditBaseValue(this.DataLoan.sGFELoanOriginatorFeePaymentCreditType), isSensitiveData: false);
            gfeDetail.GFERateLockMinimumDaysPriorToSettlementCount = ToMismoCount(this.DataLoan.sGfeLockPeriodBeforeSettlement_rep, null);
            gfeDetail.GFERateLockPeriodDaysCount = ToMismoCount(this.DataLoan.sGfeRateLockPeriod_rep, null);
            gfeDetail.GFESettlementChargesAvailableThroughDate = ToMismoDate(this.DataLoan.sGfeEstScAvailTillD_Date);

            return gfeDetail;
        }

        /// <summary>
        /// Creates a container holding summaries of various GFE sections.
        /// </summary>
        /// <returns>A GFE_SECTION_SUMMARIES container.</returns>
        private GFE_SECTION_SUMMARIES CreateGfeSectionSummaries()
        {
            var gfeSectionSummaries = new GFE_SECTION_SUMMARIES();
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeAdjOriginationCharge_rep, GFESectionIdentifierBase.A, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeTotalEstimateSettlementCharge_rep, GFESectionIdentifierBase.AB, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeTotalOtherSettlementServiceFee_rep, GFESectionIdentifierBase.B, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeOriginationF_rep, GFESectionIdentifierBase.One, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sLDiscnt_rep, GFESectionIdentifierBase.Two, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeRequiredServicesTotalFee_rep, GFESectionIdentifierBase.Three, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeLenderTitleTotalFee_rep, GFESectionIdentifierBase.Four, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeOwnerTitleTotalFee_rep, GFESectionIdentifierBase.Five, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeServicesYouShopTotalFee_rep, GFESectionIdentifierBase.Six, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeGovtRecTotalFee_rep, GFESectionIdentifierBase.Seven, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeTransferTaxTotalFee_rep, GFESectionIdentifierBase.Eight, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeInitialImpoundDeposit_rep, GFESectionIdentifierBase.Nine, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeDailyInterestTotalFee_rep, GFESectionIdentifierBase.Ten, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeHomeOwnerInsuranceTotalFee_rep, GFESectionIdentifierBase.Eleven, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));
            gfeSectionSummaries.GfeSectionSummaryList.Add(this.CreateGfeSectionSummary(this.DataLoan.sGfeNotApplicableF_rep, GFESectionIdentifierBase.Other, GetNextSequenceNumber(gfeSectionSummaries.GfeSectionSummaryList)));

            return gfeSectionSummaries;
        }

        /// <summary>
        /// Creates a container summarizing the amount of a particular GFE section.
        /// </summary>
        /// <param name="amount">The amount.</param>
        /// <param name="section">The GFE section.</param>
        /// <param name="sequence">The sequence number of the section summary among the list of summaries.</param>
        /// <returns>A GFE_SECTION_SUMMARY container.</returns>
        private GFE_SECTION_SUMMARY CreateGfeSectionSummary(string amount, GFESectionIdentifierBase section, int sequence)
        {
            var gfeSectionSummary = new GFE_SECTION_SUMMARY();
            gfeSectionSummary.GFESectionDisclosedTotalAmount = ToMismoAmount(amount);
            gfeSectionSummary.GFESectionIdentifierType = ToMismoEnum(section, isSensitiveData: false);
            gfeSectionSummary.SequenceNumber = sequence;

            return gfeSectionSummary;
        }

        /// <summary>
        /// Creates a container holding data on a note.
        /// </summary>
        /// <returns>A NOTE container.</returns>
        private NOTE CreateNote()
        {
            var note = new NOTE();
            note.AllongeToNote = this.CreateAllongeToNote();

            return note;
        }

        /// <summary>
        /// Creates a container holding data about an allonge to a note.
        /// </summary>
        /// <returns>An ALLONGE_TO_NOTE container.</returns>
        private ALLONGE_TO_NOTE CreateAllongeToNote()
        {
            var allongeToNote = new ALLONGE_TO_NOTE();
            allongeToNote.AllongeToNotePayToTheOrderOfDescription = ToMismoString(this.DataLoan.sDocMagicTransferToInvestorCode);

            return allongeToNote;
        }

        /// <summary>
        /// Creates a DOCUMENT_SPECIFIC_DATA_SET with an integrated disclosure data packet representing the current loan information to be disclosed.
        /// </summary>
        /// <param name="sequence">The sequence of the data-set among the list of sets.</param>
        /// <returns>A DOCUMENT_SPECIFIC_DATA_SET with an integrated disclosure data packet representing the current loan information to be disclosed.</returns>
        private DOCUMENT_SPECIFIC_DATA_SET CreateToBeDisclosed(int sequence)
        {
            this.HasArchive = true; //// Tricks the exporter into generating all of the integrated disclosure sub-containers.
            bool resetExtendIntegratedDisclosure = this.ExportData.Options.IncludeIntegratedDisclosureExtension;
            this.ExportData.Options.IncludeIntegratedDisclosureExtension = false; //// Omits the fee information extension that is meant only for disclosures that have already been issued.

            if (this.ExportData.IsClosingPackage)
            {
                CPageData resetCDArchive = this.ClosingDisclosureArchiveData;
                this.ClosingDisclosureArchiveData = this.DataLoan; //// Tricks the exporter into using the live loan data when generating the integrated disclosure sub-containers.

                var metadata = new ClosingDisclosureDatesMetadata(
                    this.ClosingDisclosureArchiveData.GetClosingCostArchiveById,
                    this.ClosingDisclosureArchiveData.GetConsumerDisclosureMetadataByConsumerId(),
                    this.ClosingDisclosureArchiveData.sLoanRescindableT,
                    this.ClosingDisclosureArchiveData.sAlwaysRequireAllBorrowersToReceiveClosingDisclosure);

                ClosingDisclosureDates currentCDDates = ClosingDisclosureDates.Create(metadata);
                currentCDDates.IssuedDateLckd = true;
                currentCDDates.IssuedDate = System.DateTime.Now; //// Hardcodes the integrated disclosure issue date to the current date.
                if (this.ClosingDisclosureArchiveData.sDocMagicClosingD.IsValid)
                {
                    currentCDDates.LoanClosingDate = this.ClosingDisclosureArchiveData.sDocMagicClosingD.DateTimeForComputation;
                }

                var closingDisclosure = this.CreateDocumentSpecificDataSet(docType: DocumentBase.ClosingDisclosure, document: currentCDDates, currentLoanTRID: true, sequence: sequence);

                this.ClosingDisclosureArchiveData = resetCDArchive;
                this.HasArchive = false;
                this.ExportData.Options.IncludeIntegratedDisclosureExtension = resetExtendIntegratedDisclosure;
                return closingDisclosure;
            }
            else
            {
                CPageData resetLEArchive = this.LoanEstimateArchiveData;
                this.LoanEstimateArchiveData = this.DataLoan;

                var metadata = new LoanEstimateDatesMetadata(
                    this.LoanEstimateArchiveData.GetClosingCostArchiveById,
                    this.LoanEstimateArchiveData.GetConsumerDisclosureMetadataByConsumerId(),
                    this.LoanEstimateArchiveData.sLoanRescindableT);

                LoanEstimateDates currentLEDates = LoanEstimateDates.Create(metadata);
                currentLEDates.IssuedDateLckd = true;
                currentLEDates.IssuedDate = System.DateTime.Now;

                var loanEstimate = this.CreateDocumentSpecificDataSet(docType: DocumentBase.LoanEstimate, document: currentLEDates, currentLoanTRID: true, sequence: sequence);

                this.LoanEstimateArchiveData = resetLEArchive;
                this.HasArchive = false;
                this.ExportData.Options.IncludeIntegratedDisclosureExtension = resetExtendIntegratedDisclosure;
                return loanEstimate;
            }
        }
    }
}
