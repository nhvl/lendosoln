﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using Mismo3Specification.Version4Schema;

    /// <summary>
    /// Imports MISMO 3.4 payloads into LendingQB.
    /// </summary>
    public class Mismo34Importer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Mismo34Importer"/> class.
        /// </summary>
        /// <param name="importerOptions">A collection of importer options.</param>
        public Mismo34Importer(Mismo34ImporterOptions importerOptions)
        {
            this.DataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(importerOptions.LoanId, typeof(Mismo34Importer));
            this.DataLoan.InitSave();
        }

        /// <summary>
        /// Gets or sets the loan file to populate.
        /// </summary>
        protected CPageData DataLoan
        {
            get; set;
        }

        /// <summary>
        /// Imports a MISMO 3.4 payload to LendingQB.
        /// </summary>
        /// <param name="payload">The MISMO 3.4 payload.</param>
        private void ImportLoan(string payload)
        {
            var message = (MESSAGE)SerializationHelper.XmlDeserialize(payload, typeof(MESSAGE));
            this.ParseMessage(message);

            //// MISMO 3.4 TODO: What do we want to return? The GUID of the newly created loan?
            //// How will this change when we add an option to import to an existing file?
        }

        /// <summary>
        /// Parses data from a root <see cref="MESSAGE"/> container.
        /// </summary>
        /// <param name="message">A <see cref="MESSAGE"/> container.</param>
        private void ParseMessage(MESSAGE message)
        {
            this.ParseDealSets(message.DealSets);
        }

        /// <summary>
        /// Parses data from a <see cref="DEAL_SETS"/> container.
        /// </summary>
        /// <param name="dealSets">A <see cref="DEAL_SETS"/> container.</param>
        private void ParseDealSets(DEAL_SETS dealSets)
        {
            //// MISMO 3.4 TODO: A deal set contains at least one loan, if there is more than one deal set
            //// in the payload we may want to return some sort of error. To be clarified with the PDE.
            var dealSet = dealSets.DealSetList.FirstOrDefault();
            if (dealSet != null)
            {
                this.ParseDealSet(dealSet);
            }
        }

        /// <summary>
        /// Parses data from a <see cref="DEAL_SET"/> container.
        /// </summary>
        /// <param name="dealSet">A <see cref="DEAL_SET"/> container.</param>
        private void ParseDealSet(DEAL_SET dealSet)
        {
            this.ParseDeals(dealSet.Deals);
        }

        /// <summary>
        /// Parses data from a <see cref="DEALS"/> container.
        /// </summary>
        /// <param name="deals">A <see cref="DEALS"/> container.</param>
        private void ParseDeals(DEALS deals)
        {
            //// MISMO 3.4 TODO: A deal contains at least one loan, if there is more than one deal
            //// in the payload we may want to return some sort of error. To be clarified with the PDE.
            var deal = deals.DealList.FirstOrDefault();
            if (deal != null)
            {
                this.ParseDeal(deal);
            }
        }

        /// <summary>
        /// Parses data from a <see cref="DEAL"/> container.
        /// </summary>
        /// <param name="deal">A <see cref="DEAL"/> container.</param>
        private void ParseDeal(DEAL deal)
        {
            //// MISMO 3.4 TODO: We'll want to parse out the relationships first (either as raw RELATIONSHIP containers
            //// or as some sort of shim class) to ensure we have access to them as we continue parsing the payload.
            this.ParseRelationships(deal.Relationships);

            ////this.ParseAssets(deal.Assets);
            ////this.ParseCollaterals(deal.Collaterals);
            ////this.ParseExpenses(deal.Expenses);
            ////this.ParseLiabilities(deal.Liabilities);
            this.ParseLoans(deal.Loans);
            ////this.ParseParties(deal.Parties);
        }

        /// <summary>
        /// Parses data from a <see cref="LOANS"/> container.
        /// </summary>
        /// <param name="loans">A <see cref="LOANS"/> container.</param>
        private void ParseLoans(LOANS loans)
        {
            var loan = loans.LoanList.FirstOrDefault(l => l.LoanRoleType == LoanRoleBase.SubjectLoan);
            if (loan != null)
            {
                this.ParseLoan(loan);
            }
        }

        /// <summary>
        /// Parses data from a <see cref="LOAN"/> container.
        /// </summary>
        /// <param name="loan">A <see cref="LOAN"/> container.</param>
        private void ParseLoan(LOAN loan)
        {
            ////this.ParseAdjustment(loan.Adjustment);
            ////this.ParseAmortization(loan.Amortization);
            ////this.ParseAssumability(loan.Assumability);
            ////this.ParseBillingAddress(loan.BillingAddress);
            ////this.ParseBuydown(loan.Buydown);
            ////this.ParseClosingInformation(loan.ClosingInformation);
            ////this.ParseConstruction(loan.Construction);
            ////this.ParseDocumentSpecificDataSets(loan.DocumentSpecificDataSets);
            ////this.ParseDownPayments(loan.DownPayments);
            ////this.ParseDraw(loan.Draw);
            ////this.ParseEscrow(loan.Escrow);
            ////this.ParseFeeInformation(loan.FeeInformation);
            ////this.ParseForeclosures(loan.Foreclosures);
            ////this.ParseGovernmentLoan(loan.GovernmentLoan);
            ////this.ParseHeloc(loan.Heloc);
            ////this.ParseHighCostMortgages(loan.HighCostMortgages);
            ////this.ParseHmdaLoan(loan.HmdaLoan);
            ////this.ParseHousingExpenses(loan.HousingExpenses);
            ////this.ParseInterestCalculation(loan.InterestCalculation);
            ////this.ParseInterestOnly(loan.InterestOnly);
            ////this.ParseLateCharge(loan.LateCharge);
            ////this.ParseLoanDetail(loan.LoanDetail);
            ////this.ParseLoanIdentifiers(loan.LoanIdentifiers);
            ////this.ParseLoanLevelCredit(loan.LoanLevelCredit);
            ////this.ParseLoanProduct(loan.LoanProduct);
            ////this.ParseLtv(loan.Ltv);
            ////this.ParseMaturity(loan.Maturity);
            ////this.ParseMersRegistration(loan.MersRegistrations);
            ////this.ParseMiData(loan.MiData);
            ////this.ParseNegativeAmortization(loan.NegativeAmortization);
            ////this.ParsePayment(loan.Payment);
            ////this.ParsePrepaymentPenalty(loan.PrepaymentPenalty);
            ////this.ParsePurchaseCredits(loan.PurchaseCredits);
            ////this.ParseQualification(loan.Qualification);
            ////this.ParseQualifiedMortgage(loan.QualifiedMortgage);
            ////this.ParseRefinance(loan.Refinance);
            ////this.ParseRehabilitation(loan.Rehabilitation);
            ////this.ParseTermsOfLoan(loan.TermsOfLoan);
            ////this.ParseUnderwriting(loan.Underwriting);
        }

        /// <summary>
        /// Parses data from a <see cref="RELATIONSHIPS"/> container.
        /// </summary>
        /// <param name="relationships">A <see cref="RELATIONSHIPS"/> container.</param>
        private void ParseRelationships(RELATIONSHIPS relationships)
        {
            //// MISMO 3.4 TODO: Determine how we want to parse and store relationships during the import
        }
    }
}
