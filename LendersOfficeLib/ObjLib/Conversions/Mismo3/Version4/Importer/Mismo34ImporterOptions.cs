﻿namespace LendersOffice.Conversions.Mismo3.Version4
{
    using System;

    /// <summary>
    /// A collection of options for importing MISMO 3.4.
    /// </summary>
    public class Mismo34ImporterOptions
    {
        /// <summary>
        /// Gets or sets the ID of the loan that will receive imported data.
        /// </summary>
        public Guid LoanId { get; set; }
    }
}