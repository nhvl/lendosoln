namespace LendersOffice.Conversions.Mismo23
{
    using System;
    using System.Linq;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Common;
    using System.Collections.Generic;
    using ObjLib.AdjustmentsAndOtherCredits;

    public class LoanMismo23Exporter
    {
        private CPageData m_dataLoan;
        private XmlWriter m_xmlWriter;

        /// <summary>
        /// True if the requestor will plug the loan MISMO into a mortgage insurance request.
        /// </summary>
        private bool mirequest = false;

        public LoanMismo23Exporter(Guid sLId)
        {
            m_dataLoan = new CAllData(sLId);
            m_dataLoan.InitLoad();

            m_dataLoan.SetFormatTarget(FormatTarget.LON_Mismo);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanMismo23Exporter" /> class.
        /// </summary>
        /// <param name="loanID">The sLId of the loan file.</param>
        /// <param name="mismoClosingFormat">True for FormatTarget.MismoClosing. False for FormatTarget.LON_Mismo.</param>
        /// <param name="mortgageInsuranceRequest">True if the loan MISMO will be plugged into a mortgage insurance request. Otherwise false.</param>
        public LoanMismo23Exporter(Guid loanID, bool mismoClosingFormat, bool mortgageInsuranceRequest)
        {
            this.m_dataLoan = new CFullAccessPageData(loanID, CPageData.GetCPageBaseAndCAppDataDependencyList(typeof(LoanMismo23Exporter)));
            this.m_dataLoan.ByPassFieldSecurityCheck = true;
            this.m_dataLoan.InitLoad();
            this.m_dataLoan.SetFormatTarget(mismoClosingFormat ? FormatTarget.MismoClosing : FormatTarget.LON_Mismo);
            this.mirequest = mortgageInsuranceRequest;
        }

        /// <summary>
        /// XmlReader parameter provides a hint as to what data is export. It may return more data than asked but it
        /// will never return less data than asked unless data isn't available
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="writer"></param>
        public void Export(XmlReader reader, XmlWriter writer)
        {
            m_xmlWriter = writer;

            m_xmlWriter.WriteStartElement("LOAN_APPLICATION");
            m_xmlWriter.WriteAttributeString("MISMOVersionID", "2.3.1");

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "_DATA_INFORMATION": WriteDataInformation(); break; // <xs:element ref="_DATA_INFORMATION" minOccurs="0" />
                        case "ADDITIONAL_CASE_DATA": WriteAdditionalCaseData(); break; // <xs:element ref="ADDITIONAL_CASE_DATA" minOccurs="0" /> 
                        case "AFFORDABLE_LENDING": WriteAffordableLending(); break; // <xs:element ref="AFFORDABLE_LENDING" minOccurs="0" /> 
                        case "ASSET": WriteAssets(); break; // <xs:element ref="ASSET" minOccurs="0" maxOccurs="unbounded" /> 
                        case "DOWN_PAYMENT": WriteDownpayment(); break; // <xs:element ref="DOWN_PAYMENT" minOccurs="0" maxOccurs="unbounded" /> 
                        case "GOVERNMENT_LOAN": WriteGovernmentLoan(); break; // <xs:element ref="GOVERNMENT_LOAN" minOccurs="0" /> 
                        case "GOVERNMENT_REPORTING": WriteGovernmentReporting(); break; // <xs:element ref="GOVERNMENT_REPORTING" minOccurs="0" /> 
                        case "INTERVIEWER_INFORMATION": WriteInterviewerInformation(); break; // <xs:element ref="INTERVIEWER_INFORMATION" minOccurs="0" /> 
                        case "LIABILITY": WriteLiabilities(); break; // <xs:element ref="LIABILITY" minOccurs="0" maxOccurs="unbounded" /> 
                        case "LOAN_PRODUCT_DATA": WriteLoanProductData(); break; // <xs:element ref="LOAN_PRODUCT_DATA" minOccurs="0" /> 
                        case "LOAN_PURPOSE": WriteLoanPurpose(); break;// <xs:element ref="LOAN_PURPOSE" minOccurs="0" /> 
                        case "LOAN_QUALIFICATION": WriteLoanQualification(); break; // <xs:element ref="LOAN_QUALIFICATION" minOccurs="0" /> 
                        case "MORTGAGE_TERMS": WriteMortgageTerms(); break; // <xs:element ref="MORTGAGE_TERMS" minOccurs="0" /> 
                        case "PROPERTY": WriteProperty(); break; // <xs:element ref="PROPERTY" minOccurs="0" /> 
                        case "PROPOSED_HOUSING_EXPENSE": WriteProposedHousingExpense(); break; // <xs:element ref="PROPOSED_HOUSING_EXPENSE" minOccurs="0" maxOccurs="unbounded" /> 
                        case "REO_PROPERTY": WriteREOProperties(); break;// <xs:element ref="REO_PROPERTY" minOccurs="0" maxOccurs="unbounded" /> 
                        case "TITLE_HOLDER": WriteTitleHolder(); break; // <xs:element ref="TITLE_HOLDER" minOccurs="0" maxOccurs="unbounded" /> 
                        case "TRANSACTION_DETAIL": WriteTransactionDetail(); break; // <xs:element ref="TRANSACTION_DETAIL" minOccurs="0" /> 
                        case "BORROWER": WriteBorrowers(); break; // <xs:element ref="BORROWER" maxOccurs="unbounded" /> 
                        case "ESCROW": WriteEscrow(); break; // <xs:element ref="ESCROW" minOccurs="0" maxOccurs="unbounded" /> 
                        case "RESPA_FEE": WriteRESPAFee(); break; // <xs:element ref="RESPA_FEE" minOccurs="0" maxOccurs="unbounded" /> 
                        case "MI_DATA": WriteMIData(); break; // <xs:element ref="MI_DATA" minOccurs="0" /> 
                        case "DISCLOSURE_DATA": WriteDisclosureData(); break; // <xs:element ref="DISCLOSURE_DATA" minOccurs="0" /> 
                    } // switch (reader.Name) 
                } // if (reader.NodeType == XmlNodeType.Element) 
            }
            m_xmlWriter.WriteEndElement();

        }

        /// <summary>
        /// Export complete MISMO 2.3 document. 
        /// </summary>
        /// <param name="writer"></param>
        public void Export(XmlWriter writer)
        {
            m_xmlWriter = writer;

            m_xmlWriter.WriteStartElement("LOAN_APPLICATION");
            m_xmlWriter.WriteAttributeString("MISMOVersionID", "2.3.1");

            WriteDataInformation();        // <xs:element ref="_DATA_INFORMATION" minOccurs="0" />
            WriteAdditionalCaseData();     // <xs:element ref="ADDITIONAL_CASE_DATA" minOccurs="0" /> 
            WriteAffordableLending();      // <xs:element ref="AFFORDABLE_LENDING" minOccurs="0" /> 
            WriteAssets();                 // <xs:element ref="ASSET" minOccurs="0" maxOccurs="unbounded" /> 
            WriteDownpayment();            // <xs:element ref="DOWN_PAYMENT" minOccurs="0" maxOccurs="unbounded" /> 
            WriteGovernmentLoan();         // <xs:element ref="GOVERNMENT_LOAN" minOccurs="0" /> 
            WriteGovernmentReporting();    // <xs:element ref="GOVERNMENT_REPORTING" minOccurs="0" /> 
            WriteInterviewerInformation(); // <xs:element ref="INTERVIEWER_INFORMATION" minOccurs="0" /> 
            WriteLiabilities();            // <xs:element ref="LIABILITY" minOccurs="0" maxOccurs="unbounded" /> 
            WriteLoanProductData();        // <xs:element ref="LOAN_PRODUCT_DATA" minOccurs="0" /> 
            WriteLoanPurpose();            // <xs:element ref="LOAN_PURPOSE" minOccurs="0" /> 
            WriteLoanQualification();      // <xs:element ref="LOAN_QUALIFICATION" minOccurs="0" /> 
            WriteMortgageTerms();          // <xs:element ref="MORTGAGE_TERMS" minOccurs="0" /> 
            WriteProperty();               // <xs:element ref="PROPERTY" minOccurs="0" /> 
            WriteProposedHousingExpense(); // <xs:element ref="PROPOSED_HOUSING_EXPENSE" minOccurs="0" maxOccurs="unbounded" /> 
            WriteREOProperties();          // <xs:element ref="REO_PROPERTY" minOccurs="0" maxOccurs="unbounded" /> 
            WriteTitleHolder();            // <xs:element ref="TITLE_HOLDER" minOccurs="0" maxOccurs="unbounded" /> 
            WriteTransactionDetail();      // <xs:element ref="TRANSACTION_DETAIL" minOccurs="0" /> 
            WriteBorrowers();              // <xs:element ref="BORROWER" maxOccurs="unbounded" /> 
            WriteEscrow();                 // <xs:element ref="ESCROW" minOccurs="0" maxOccurs="unbounded" /> 
            WriteRESPAFee();               // <xs:element ref="RESPA_FEE" minOccurs="0" maxOccurs="unbounded" /> 
            WriteMIData();                 // <xs:element ref="MI_DATA" minOccurs="0" /> 
            WriteDisclosureData();         // <xs:element ref="DISCLOSURE_DATA" minOccurs="0" /> 

            m_xmlWriter.WriteEndElement(); // </LOAN_APPLICATION>
        }

        private void WriteDataInformation()
        {
            // TODO
            //  <xs:element name="_DATA_INFORMATION">
            //      <xs:complexType>
            //          <xs:sequence>
            //              <xs:element ref="DATA_VERSION" maxOccurs="unbounded"/>
            //          </xs:sequence>
            //      </xs:complexType>
            //  </xs:element>

            //  <xs:element name="DATA_VERSION">
            //	    <xs:complexType>
            //          <xs:attribute name="_Name" type="xs:string" use="required"/>
            //          <xs:attribute name="_Number" type="xs:string" use="required"/>
            //      </xs:complexType>
            //  </xs:element>
        }
        private void WriteAdditionalCaseData()
        {
            //  <xs:element name="ADDITIONAL_CASE_DATA">
            //      <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="MORTGAGE_SCORE" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="TRANSMITTAL_DATA" minOccurs="0"/>
            //          </xs:sequence>
            //      </xs:complexType>
            //  </xs:element>

            m_xmlWriter.WriteStartElement("ADDITIONAL_CASE_DATA");

            WriteMortgageScore();
            WriteTransmittalData();

            m_xmlWriter.WriteEndElement(); // </ADDITIONAL_CASE_DATA>

        }
        private void WriteMortgageScore()
        {
            //  <xs:element name="MORTGAGE_SCORE">
            //	    <xs:complexType>
            //          <xs:attribute name="_Date" type="xs:string"/>
            //	        <xs:attribute name="_Type">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="FraudFilterScore"/>
            //                      <xs:enumeration value="GE_IQScore"/>
            //                      <xs:enumeration value="Other"/>
            //                      <xs:enumeration value="PMIAuraAQIScore"/>
            //                      <xs:enumeration value="UGIAccuscore"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_Value" type="xs:string"/>
            //          <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

            // TODO
        }
        private void WriteTransmittalData()
        {
            //  <xs:element name="TRANSMITTAL_DATA">
            //	    <xs:complexType>
            //	        <xs:attribute name="ArmsLengthIndicator" /> // NMTOKEN: {Y, N}
            //	        <xs:attribute name="BelowMarketSubordinateFinancingIndicator"/> // NMTOKEN: {Y, N}
            //          <xs:attribute name="BuydownRatePercent" type="xs:string"/>
            //	        <xs:attribute name="CaseStateType" /> // NMTOKEN: {Application, FinalDisposition, PostClosingQualityControl, Prequalification, Underwriting}
            //	        <xs:attribute name="CreditReportAuthorizationIndicator" /> // NMTOKEN: {Y, N} 
            //	        <xs:attribute name="CurrentFirstMortgageHolderType" /> // NMTOKEN: {FNM, FRE, Other, Unknown}
            //          <xs:attribute name="LenderBranchIdentifier" type="xs:string"/>
            //          <xs:attribute name="LenderRegistrationIdentifier" type="xs:string"/>
            //          <xs:attribute name="PropertyAppraisedValueAmount" type="xs:string"/>
            //          <xs:attribute name="PropertyEstimatedValueAmount" type="xs:string"/>
            //          <xs:attribute name="InvestorLoanIdentifier" type="xs:string"/>
            //          <xs:attribute name="InvestorInstitutionIdentifier" type="xs:string"/>
            //          <xs:attribute name="CommitmentReferenceIdentifier" type="xs:string"/>
            //	        <xs:attribute name="ConcurrentOriginationIndicator" /> // NMTOKEN: {Y, N}
            //	        <xs:attribute name="ConcurrentOriginationLenderIndicator" /> // NMTOKEN: {Y, N}
            //          <xs:attribute name="RateLockPeriodDays" type="xs:string"/>
            //	        <xs:attribute name="RateLockType" /> // NMTOKEN: {BestEfforts, Mandatory}
            //          <xs:attribute name="RateLockRequestedExtensionDays" type="xs:string"/>
            //          <xs:attribute name="LoanOriginationSystemLoanIdentifier" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
            this.m_xmlWriter.WriteStartElement("TRANSMITTAL_DATA");

            this.WriteAttributeString("BuydownRatePercent", this.m_dataLoan.sBuydownResultIR_rep);
            this.WriteAttributeString("CaseStateType", CaseStateTypeMap.ToMismo(m_dataLoan.sFredProcPointT));
            this.WriteAttributeString("LenderBranchIdentifier", this.m_dataLoan.BranchCode);
            this.WriteAttributeString("PropertyAppraisedValueAmount", this.m_dataLoan.sApprVal_rep);

            if (this.m_dataLoan.sSpMarketVal != 0.00M)
            {
                this.WriteAttributeString("PropertyEstimatedValueAmount", this.m_dataLoan.sSpMarketVal_rep);
            }

            this.WriteAttributeString("LoanOriginationSystemLoanIdentifier", this.m_dataLoan.sLNm);

            this.m_xmlWriter.WriteEndElement(); // </TRANSMITTAL_DATA>
        }

        private void WriteAffordableLending()
        {
            //  <xs:element name="AFFORDABLE_LENDING">
            //	    <xs:complexType>
            //          <xs:attribute name="FNMCommunityLendingProductName" type="xs:string"/>
            //	        <xs:attribute name="FNMCommunityLendingProductType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="CommunityHomeBuyerProgram"/>
            //                      <xs:enumeration value="Fannie97"/>
            //                      <xs:enumeration value="Fannie32"/>
            //                      <xs:enumeration value="MyCommunityMortgage"/>
            //                      <xs:enumeration value="Other"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="FNMCommunityLendingProductTypeOtherDescription" type="xs:string"/>
            //	        <xs:attribute name="FNMCommunitySecondsIndicator" /> // NMTOKEN: {Y, N}
            //	        <xs:attribute name="FNMNeighborsMortgageEligibilityIndicator" /> // NMTOKEN: {Y,N}
            //          <xs:attribute name="FREAffordableProgramIdentifier" type="xs:string"/>
            //          <xs:attribute name="HUDIncomeLimitAdjustmentFactor" type="xs:string"/>
            //          <xs:attribute name="HUDLendingIncomeLimitAmount" type="xs:string"/>
            //          <xs:attribute name="HUDMedianIncomeAmount" type="xs:string"/>
            //          <xs:attribute name="MSAIdentifier" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

            if (this.m_dataLoan.sIsCommunityLending)
            {
                this.m_xmlWriter.WriteStartElement("AFFORDABLE_LENDING");

                string fnmaCLType = FNMACommunityLendingTypeMap.ToMismo(this.m_dataLoan.sFannieCommunityLendingT);
                this.WriteAttributeString("FNMCommunityLendingProductType", fnmaCLType);

                if (fnmaCLType.Equals("Other", StringComparison.OrdinalIgnoreCase))
                {
                    this.WriteAttributeString("FNMCommunityLendingProductTypeOtherDescription", this.m_dataLoan.sFannieCommunityLendingT.ToString("G"));
                }

                this.m_xmlWriter.WriteEndElement(); // </AFFORDABLE_LENDING>
            }
        }

        private void WriteAssets()
        {
            //  <xs:element name="ASSET">
            //	    <xs:complexType>
            //          <xs:attribute name="BorrowerID" type="xs:IDREFS"/>
            //          <xs:attribute name="_AccountIdentifier" type="xs:string"/>
            //          <xs:attribute name="_CashOrMarketValueAmount" type="xs:string"/>
            //	        <xs:attribute name="_Type">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Automobile"/>
            //                      <xs:enumeration value="Bond"/>
            //                      <xs:enumeration value="BridgeLoanNotDeposited"/>
            //                      <xs:enumeration value="CashOnHand"/>
            //                      <xs:enumeration value="CertificateOfDepositTimeDeposit"/>
            //                      <xs:enumeration value="CheckingAccount"/>
            //                      <xs:enumeration value="EarnestMoneyCashDepositTowardPurchase"/>
            //                      <xs:enumeration value="GiftsTotal"/>
            //                      <xs:enumeration value="GiftsNotDeposited"/>
            //                      <xs:enumeration value="LifeInsurance"/>
            //                      <xs:enumeration value="MoneyMarketFund"/>
            //                      <xs:enumeration value="MutualFund"/>
            //                      <xs:enumeration value="NetWorthOfBusinessOwned"/>
            //                      <xs:enumeration value="OtherLiquidAssets"/>
            //                      <xs:enumeration value="OtherNonLiquidAssets"/>
            //                      <xs:enumeration value="PendingNetSaleProceedsFromRealEstateAssets"/>
            //                      <xs:enumeration value="RelocationMoney"/>
            //                      <xs:enumeration value="RetirementFund"/>
            //                      <xs:enumeration value="SaleOtherAssets"/>
            //                      <xs:enumeration value="SavingsAccount"/>
            //                      <xs:enumeration value="SecuredBorrowedFundsNotDeposited"/>
            //                      <xs:enumeration value="Stock"/>
            //                      <xs:enumeration value="TrustAccount"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="_VerifiedIndicator" /> // NMTOKEN: {Y,N}
            //          <xs:attribute name="_HolderName" type="xs:string"/>
            //          <xs:attribute name="_HolderStreetAddress" type="xs:string"/>
            //          <xs:attribute name="_HolderCity" type="xs:string"/>
            //          <xs:attribute name="_HolderState" type="xs:string"/>
            //          <xs:attribute name="_HolderPostalCode" type="xs:string"/>
            //          <xs:attribute name="AutomobileMakeDescription" type="xs:string"/>
            //          <xs:attribute name="AutomobileModelYear" type="xs:string"/>
            //          <xs:attribute name="LifeInsuranceFaceValueAmount" type="xs:string"/>
            //          <xs:attribute name="OtherAssetTypeDescription" type="xs:string"/>
            //          <xs:attribute name="StockBondMutualFundShareCount" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
            int nApps = m_dataLoan.nApps;

            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                var subColl = dataApp.aAssetCollection.GetSubcollection(true, E_AssetGroupT.All);
                foreach (var item in subColl)
                {
                    var fields = (IAsset)item;
                    m_xmlWriter.WriteStartElement("ASSET");
                    string borrowerId = dataApp.aBMismoId;

                    if (fields.OwnerT == E_AssetOwnerT.CoBorrower)
                    {
                        borrowerId = dataApp.aCMismoId;
                    }
                    else if (fields.OwnerT == E_AssetOwnerT.Joint && dataApp.aCIsValidNameSsn)
                    {
                        borrowerId += " " + dataApp.aCMismoId;
                    }

                    WriteAttributeString("BorrowerID", borrowerId);

                    if (fields.IsRegularType)
                    {
                        var reg = fields as IAssetRegular;
                        WriteAttributeString("_AccountIdentifier", reg.AccNum.Value);
                        WriteAttributeString("_HolderName", reg.AccNm);
                        WriteAttributeString("_HolderStreetAddress", reg.StAddr);
                        WriteAttributeString("_HolderCity", reg.City);
                        WriteAttributeString("_HolderState", reg.State);
                        WriteAttributeString("_HolderPostalCode", reg.Zip);
                        switch (reg.AssetT)
                        {
                            case E_AssetRegularT.Auto:
                                m_xmlWriter.WriteAttributeString("AutomobileMakeDescription", fields.Desc);
                                //WE DON'T HAVE THIS FIELD EXPLICITLY
                                //m_xmlWriter.WriteAttributeString("AutomobileModelYear", xxxxx) ;
                                break;
                        }
                        //WE DON'T HAVE THIS FIELD.
                        //m_xmlWriter.WriteAttributeString("StockBondMutualFundShareCount", fields.) ;
                    }
                    else
                    {
                        switch (fields.AssetT)
                        {
                            case E_AssetT.LifeInsurance:
                                var lifeIns = fields as IAssetLifeInsurance;
                                WriteAttributeString("LifeInsuranceFaceValueAmount", lifeIns.FaceVal_rep);
                                break;
                        }
                    }

                    WriteAttributeString("_CashOrMarketValueAmount", fields.Val_rep);
                    WriteAttributeString("_Type", AssetTypeMap.ToMismo(fields.AssetT));
                    m_xmlWriter.WriteEndElement(); // </ASSET>
                }
            }
        }
        private void WriteDownpayment()
        {
            //  <xs:element name="DOWN_PAYMENT">
            //	    <xs:complexType>
            //          <xs:attribute name="_Amount" type="xs:string"/>
            //          <xs:attribute name="_SourceDescription" type="xs:string"/>
            //	        <xs:attribute name="_Type">
            //	        <xs:simpleType>
            //	            <xs:restriction base="xs:NMTOKEN">
            //                  <xs:enumeration value="BridgeLoan"/>
            //                  <xs:enumeration value="CashOnHand"/>
            //                  <xs:enumeration value="CheckingSavings"/>
            //                  <xs:enumeration value="DepositOnSalesContract"/>
            //                  <xs:enumeration value="EquityOnPendingSale"/>
            //                  <xs:enumeration value="EquityOnSoldProperty"/>
            //                  <xs:enumeration value="EquityOnSubjectProperty"/>
            //                  <xs:enumeration value="GiftFunds"/>
            //                  <xs:enumeration value="LifeInsuranceCashValue"/>
            //                  <xs:enumeration value="LotEquity"/>
            //                  <xs:enumeration value="OtherTypeOfDownPayment"/>
            //                  <xs:enumeration value="RentWithOptionToPurchase"/>
            //                  <xs:enumeration value="RetirementFunds"/>
            //                  <xs:enumeration value="SaleOfChattel"/>
            //                  <xs:enumeration value="SecuredBorrowedFunds"/>
            //                  <xs:enumeration value="StocksAndBonds"/>
            //                  <xs:enumeration value="SweatEquity"/>
            //                  <xs:enumeration value="TradeEquity"/>
            //                  <xs:enumeration value="TrustFunds"/>
            //                  <xs:enumeration value="UnsecuredBorrowedFunds"/>
            //              </xs:restriction>
            //          </xs:simpleType>
            //          </xs:attribute>
            //      </xs:complexType>
            //  </xs:element>
            m_xmlWriter.WriteStartElement("DOWN_PAYMENT");
            WriteAttributeString("_Type", DownPaymentMap.ToMismo(m_dataLoan.sDwnPmtSrc));
            WriteAttributeString("_SourceDescription", m_dataLoan.sDwnPmtSrcExplain);
            WriteAttributeString("_Amount", m_dataLoan.sEquityCalc_rep);
            m_xmlWriter.WriteEndElement();
        }
        private void WriteGovernmentLoan()
        {
            //  <xs:element name="GOVERNMENT_LOAN">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="FHA_LOAN" minOccurs="0"/>
            //              <xs:element ref="FHA_VA_LOAN" minOccurs="0"/>
            //              <xs:element ref="VA_LOAN" minOccurs="0"/>
            //          </xs:sequence>
            //      </xs:complexType>
            //  </xs:element>

            //            m_xmlWriter.WriteStartElement("GOVERNMENT_LOAN");
            //            WriteFhaLoan();
            //            WriteFhaVaLoan();
            //            WriteVaLoan();
            //            m_xmlWriter.WriteEndElement(); // </GOVERNMENT_LOAN>
        }
        private void WriteFhaLoan()
        {
            //  <xs:element name="FHA_LOAN">
            //	    <xs:complexType>
            //          <xs:attribute name="BorrowerFinancedFHADiscountPointsAmount" type="xs:string"/>
            //	        <xs:attribute name="FHAAlimonyLiabilityTreatmentType" /> // NMTOKEN {AdditionToDebt, ReductionToIncome}
            //          <xs:attribute name="FHACoverageRenewalRatePercent" type="xs:string"/>
            //          <xs:attribute name="FHA_MIPremiumRefundAmount" type="xs:string"/>
            //          <xs:attribute name="FHAUpfrontMIPremiumPercent" type="xs:string"/>
            //          <xs:attribute name="_LenderIdentifier" type="xs:string"/>
            //          <xs:attribute name="_SponsorIdentifier" type="xs:string"/>
            //	        <xs:attribute name="SectionOfActType" /> // NMTOKEN {203B, 203B251, 203B2, 203K, 203K251, 221D2, 221D2251, 234C, 234C251}
            //          <xs:attribute name="FHAEnergyRelatedRepairsOrImprovementsAmount" type="xs:string"/>
            //          <xs:attribute name="FHAGeneralServicesAdminstrationCodeIdentifier" type="xs:string"/>
            //          <xs:attribute name="FHALimitedDenialParticipationIdentifier" type="xs:string"/>
            //          <xs:attribute name="FHARefinanceInterestOnExistingLienAmount" type="xs:string"/>
            //          <xs:attribute name="FHARefinanceOriginalExistingFHACaseIdentifier" type="xs:string"/>
            //          <xs:attribute name="FHARefinanceOriginalExistingUpFrontMIPAmount" type="xs:string"/>
            //          <xs:attribute name="FHAUnderwriterComputerizedHomesUnderwritingSystemIdentifier" type="xs:string"/>
            //	        <xs:attribute name="HUDAdequateAvailableAssetsIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="HUDAdequateEffectiveIncomeIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="HUDCreditCharacteristicsIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="HUDStableEffectiveIncomeIndicator" /> // NMTOKEN {Y, N}
            //      </xs:complexType>
            //  </xs:element>
        }


        private void WriteFhaVaLoan()
        {
            //  <xs:element name="FHA_VA_LOAN">
            //	    <xs:complexType>
            //          <xs:attribute name="BorrowerPaidFHA_VAClosingCostsAmount" type="xs:string"/>
            //          <xs:attribute name="BorrowerPaidFHA_VAClosingCostsPercent" type="xs:string"/>
            //          <xs:attribute name="GovernmentMortgageCreditCertificateAmount" type="xs:string"/>
            //	        <xs:attribute name="GovernmentRefinanceType" /> // NMTOKEN {FullDocumentation, InterestRateReductionRefinanceLoan, StreamlineWithAppraisal, StreamlineWithoutAppraisal}
            //          <xs:attribute name="OtherPartyPaidFHA_VAClosingCostsAmount" type="xs:string"/>
            //          <xs:attribute name="OtherPartyPaidFHA_VAClosingCostsPercent" type="xs:string"/>
            //	        <xs:attribute name="PropertyEnergyEfficientHomeIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="SellerPaidFHA_VAClosingCostsPercent" type="xs:string"/>
            //          <xs:attribute name="_OriginatorIdentifier" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
        }

        private void WriteVaLoan()
        {
            //  <xs:element name="VA_LOAN">
            //	    <xs:complexType>
            //	        <xs:attribute name="VABorrowerCoBorrowerMarriedIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="BorrowerFundingFeePercent" type="xs:string"/>
            //          <xs:attribute name="VAEntitlementAmount" type="xs:string"/>
            //          <xs:attribute name="VAMaintenanceExpenseMonthlyAmount" type="xs:string"/>
            //          <xs:attribute name="VAUtilityExpenseMonthlyAmount" type="xs:string"/>
            //          <xs:attribute name="VAEntitlementCodeIdentifier" type="xs:string"/>
            //          <xs:attribute name="VAHouseholdSizeCount" type="xs:string"/>
            //          <xs:attribute name="VAResidualIncomeAmount" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
        }
        private void WriteGovernmentReporting()
        {
            //  <xs:element name="GOVERNMENT_REPORTING">
            //	    <xs:complexType>
            //	        <xs:attribute name="HMDAPurposeOfLoanType" /> // NMTOKEN {HomePurchase, HomeImprovement, Refinancing}
            //	        <xs:attribute name="HMDAPreapprovalType" /> // NMTOKEN {PreapprovalWasRequested, PreapprovalWasNotRequested, NotApplicable}
            //	        <xs:attribute name="HMDA_HOEPALoanStatusIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="HMDARateSpreadPercent" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
        }
        private void WriteInterviewerInformation()
        {
            //  <xs:element name="INTERVIEWER_INFORMATION">
            //	    <xs:complexType>
            //          <xs:attribute name="InterviewersEmployerStreetAddress" type="xs:string"/>
            //          <xs:attribute name="InterviewersEmployerCity" type="xs:string"/>
            //          <xs:attribute name="InterviewersEmployerState" type="xs:string"/>
            //          <xs:attribute name="InterviewersEmployerPostalCode" type="xs:string"/>
            //          <xs:attribute name="InterviewersTelephoneNumber" type="xs:string"/>
            //	        <xs:attribute name="ApplicationTakenMethodType" /> // NMTOKEN {FaceToFace, Mail, Telephone, Internet}
            //          <xs:attribute name="InterviewerApplicationSignedDate" type="xs:string"/>
            //          <xs:attribute name="InterviewersEmployerName" type="xs:string"/>
            //          <xs:attribute name="InterviewersName" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
            IPreparerFields interviewer = m_dataLoan.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject);

            m_xmlWriter.WriteStartElement("INTERVIEWER_INFORMATION");
            WriteAttributeString("InterviewersEmployerStreetAddress", interviewer.StreetAddr);
            WriteAttributeString("InterviewersEmployerCity", interviewer.City);
            WriteAttributeString("InterviewersEmployerState", interviewer.State);
            WriteAttributeString("InterviewersEmployerPostalCode", interviewer.Zip);
            WriteAttributeString("InterviewersTelephoneNumber", interviewer.Phone);
            WriteAttributeString("ApplicationTakenMethodType", InterviewerMethodMap.ToMismo(m_dataLoan.GetAppData(0).aIntrvwrMethodT));
            //            WriteAttributeString("InterviewerApplicationSignedDate", xxx);
            WriteAttributeString("InterviewersEmployerName", interviewer.CompanyName);
            WriteAttributeString("InterviewersName", interviewer.PreparerName);
            m_xmlWriter.WriteEndElement(); // </INTERVIEWER_INFORMATION>
        }


        private void WriteLiabilities()
        {
            //  <xs:element name="LIABILITY">
            //	    <xs:complexType>
            //          <xs:attribute name="_ID" type="xs:ID"/>
            //          <xs:attribute name="BorrowerID" type="xs:IDREFS"/>
            //          <xs:attribute name="REO_ID" type="xs:IDREF"/>
            //          <xs:attribute name="_HolderStreetAddress" type="xs:string"/>
            //          <xs:attribute name="_HolderCity" type="xs:string"/>
            //          <xs:attribute name="_HolderState" type="xs:string"/>
            //          <xs:attribute name="_HolderPostalCode" type="xs:string"/>
            //          <xs:attribute name="AlimonyOwedToName" type="xs:string"/>
            //          <xs:attribute name="_AccountIdentifier" type="xs:string"/>
            //	        <xs:attribute name="_ExclusionIndicator"/> // NMTOKEN {Y, N}
            //          <xs:attribute name="_HolderName" type="xs:string"/>
            //          <xs:attribute name="_MonthlyPaymentAmount" type="xs:string"/>
            //	        <xs:attribute name="_PayoffStatusIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="_PayoffWithCurrentAssetsIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="_RemainingTermMonths" type="xs:string"/>
            //	        <xs:attribute name="_Type">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Alimony"/>
            //                      <xs:enumeration value="ChildCare"/>
            //                      <xs:enumeration value="ChildSupport"/>
            //                      <xs:enumeration value="CollectionsJudgementsAndLiens"/>
            //                      <xs:enumeration value="HELOC"/>
            //                      <xs:enumeration value="Installment"/>
            //                      <xs:enumeration value="JobRelatedExpenses"/>
            //                      <xs:enumeration value="LeasePayments"/>
            //                      <xs:enumeration value="MortgageLoan"/>
            //                      <xs:enumeration value="Open30DayChargeAccount"/>
            //                      <xs:enumeration value="OtherLiability"/>
            //                      <xs:enumeration value="Revolving"/>
            //                      <xs:enumeration value="SeparateMaintenanceExpense"/>
            //                      <xs:enumeration value="OtherExpense"/>
            //                      <xs:enumeration value="Taxes"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_UnpaidBalanceAmount" type="xs:string"/>
            //	        <xs:attribute name="SubjectLoanResubordinationIndicator" /> // NMTOKEN {Y, N}
            //      </xs:complexType>
            //  </xs:element>
            int nApps = m_dataLoan.nApps;
            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                var subColl = dataApp.aLiaCollection.GetSubcollection(true, E_DebtGroupT.ALL);

                foreach (var item in subColl)
                {
                    var fields = (ILiability)item;
                    m_xmlWriter.WriteStartElement("LIABILITY");
                    WriteAttributeString("_ID", MakeLiabilityID(fields.RecordId));
                    WriteAttributeString("BorrowerID", fields.OwnerT == E_LiaOwnerT.CoBorrower ? dataApp.aCMismoId : dataApp.aBMismoId);

                    if (CLiaFields.IsTypeOfSpecialRecord(fields.DebtT))
                    {
                        var spec = fields as ILiabilitySpecial;
                        switch (spec.DebtT)
                        {
                            case E_DebtSpecialT.Alimony:
                                var alimony = spec as ILiabilityAlimony;
                                WriteAttributeString("AlimonyOwedToName", alimony.OwedTo);
                                break;
                            case E_DebtSpecialT.JobRelatedExpense:
                                ILiabilityJobExpense jobExpense = spec as ILiabilityJobExpense;
                                WriteAttributeString("_HolderName", jobExpense.ExpenseDesc);
                                break;
                            case E_DebtSpecialT.ChildSupport:
                                // Child support has no special attributes.
                                break;
                            default:
                                Tools.LogWarning("LoanMismo23Writer: Unsupport " + spec.DebtT);
                                continue;
                        }
                    }
                    else
                    {
                        ILiabilityRegular reg = fields as ILiabilityRegular;
                        if (reg.DebtT == E_DebtRegularT.Mortgage)
                        {
                            if (reg.MatchedReRecordId != Guid.Empty)
                            {
                                WriteAttributeString("REO_ID", MakeReoID(reg.MatchedReRecordId));
                            }
                        }

                        WriteAttributeString("_HolderName", reg.ComNm);
                        WriteAttributeString("_HolderStreetAddress", reg.ComAddr);
                        WriteAttributeString("_HolderCity", reg.ComCity);
                        WriteAttributeString("_HolderState", reg.ComState);
                        WriteAttributeString("_HolderPostalCode", reg.ComZip);
                        WriteAttributeString("_AccountIdentifier", reg.AccNum.Value);
                        WriteAttributeString("_UnpaidBalanceAmount", reg.Bal_rep);
                        WriteAttributeYN("SubjectLoanResubordinationIndicator", reg.IsPiggyBack);
                    }

                    WriteAttributeYN("_ExclusionIndicator", this.IsLiabilityExcludedFromRatio(fields, dataApp));
                    WriteAttributeString("_MonthlyPaymentAmount", fields.Pmt_rep);
                    WriteAttributeYN("_PayoffStatusIndicator", fields.WillBePdOff);
                    WriteAttributeString("_RemainingTermMonths", fields.RemainMons_rep);
                    WriteAttributeString("_Type", LiabilityTypeMap.ToMismo(fields.DebtT));
                    m_xmlWriter.WriteEndElement(); // </LIABILITY>
                }

            }
        }

        /// <summary>
        /// Determines whether a liability should be excluded from the ratio.
        /// </summary>
        /// <param name="liability">A liability.</param>
        /// <param name="dataApp">The parent application.</param>
        /// <returns>A boolean indicating whether the liability should be excluded from the ratio.</returns>
        private bool IsLiabilityExcludedFromRatio(ILiability liability, CAppData dataApp)
        {
            if (this.mirequest && liability is ILiabilityRegular)
            {
                var regularLiability = liability as ILiabilityRegular;
                if (regularLiability.MatchedReRecordId != Guid.Empty)
                {
                    var linkedReoRecord = dataApp.aReCollection.GetRegRecordOf(regularLiability.MatchedReRecordId);
                    if (linkedReoRecord.StatT == E_ReoStatusT.Rental || linkedReoRecord.StatT == E_ReoStatusT.Residence)
                    {
                        return false;
                    }
                }
            }

            return liability.NotUsedInRatio;
        }

        private void WriteProperty()
        {
            //  <xs:element name="PROPERTY">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="_LEGAL_DESCRIPTION" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="PARSED_STREET_ADDRESS" minOccurs="0"/>
            //              <xs:element ref="_VALUATION" minOccurs="0" maxOccurs="unbounded"/>
            //          </xs:sequence>
            //          <xs:attribute name="_StreetAddress" type="xs:string"/>
            //          <xs:attribute name="_StreetAddress2" type="xs:string"/>
            //          <xs:attribute name="_City" type="xs:string"/>
            //          <xs:attribute name="_State" type="xs:string"/>
            //          <xs:attribute name="_County" type="xs:string"/>
            //          <xs:attribute name="_PostalCode" type="xs:string"/>
            //	        <xs:attribute name="BuildingStatusType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Existing"/>
            //                      <xs:enumeration value="Proposed"/>
            //                      <xs:enumeration value="SubjectToAlterationImprovementRepairAndRehabilitation"/>
            //                      <xs:enumeration value="SubstantiallyRehabilitated"/>
            //                      <xs:enumeration value="UnderConstruction"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_FinancedNumberOfUnits" type="xs:string"/>
            //          <xs:attribute name="_StructureBuiltYear" type="xs:string"/>
            //          <xs:attribute name="_AcquiredDate" type="xs:string"/>
            //	        <xs:attribute name="PlannedUnitDevelopmentIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="_AcreageNumber" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

            m_xmlWriter.WriteStartElement("PROPERTY");
            WriteAttributeString("_StreetAddress", m_dataLoan.sSpAddr);
            WriteAttributeString("_City", m_dataLoan.sSpCity);
            WriteAttributeString("_State", m_dataLoan.sSpState);
            WriteAttributeString("_PostalCode", m_dataLoan.sSpZip);
            WriteAttributeString("_County", m_dataLoan.sSpCounty);

            WriteAttributeString("_FinancedNumberOfUnits", m_dataLoan.sUnitsNum_rep);
            WriteAttributeString("_StructureBuiltYear", m_dataLoan.sYrBuilt);

            m_xmlWriter.WriteStartElement("_LEGAL_DESCRIPTION");
            WriteAttributeString("_TextDescription", m_dataLoan.sSpLegalDesc);
            m_xmlWriter.WriteEndElement(); // </_LEGAL_DESCRIPTION>

            this.WriteValuation();

            m_xmlWriter.WriteEndElement(); // </PROPERTY>
        }

        /// <summary>
        /// Writes a Valuation block. 
        /// </summary>
        private void WriteValuation()
        {
            this.m_xmlWriter.WriteStartElement("_VALUATION");

            string method = ValuationMethodTypeMap.ToMismo(this.m_dataLoan.sSpAppraisalFormT, this.m_dataLoan.sSpValuationMethodT);
            this.WriteAttributeString("_MethodType", method);

            if (method.Equals("Other", StringComparison.OrdinalIgnoreCase))
            {
                this.WriteAttributeString("_MethodTypeOtherDescription", this.m_dataLoan.sSpAppraisalFormT.ToString("G"));
            }

            this.WriteAppraiser();
            this.WriteAVM();

            this.m_xmlWriter.WriteEndElement(); // </_VALUATION>
        }

        /// <summary>
        /// Writes an Appraiser block.
        /// </summary>
        private void WriteAppraiser()
        {
            CAgentFields agent = m_dataLoan.GetAgentOfRole(E_AgentRoleT.Appraiser, E_ReturnOptionIfNotExist.ReturnEmptyObject);
            if (!agent.IsValid)
            {
                return;
            }

            this.m_xmlWriter.WriteStartElement("APPRAISER");

            this.WriteAttributeString("_Name", agent.AgentName);
            this.WriteAttributeString("_CompanyName", agent.CompanyName);
            this.WriteAttributeString("_LicenseIdentifier", agent.LicenseNumOfAgent);
            this.WriteAttributeString("_LicenseState", agent.State);

            this.m_xmlWriter.WriteEndElement(); // </APPRAISER>
        }

        /// <summary>
        /// Writes an AVM block. This was added to MISMO 2.3.1 by the mortgage insurance work group and is not applicable to other service types.
        /// </summary>
        private void WriteAVM()
        {
            if (this.mirequest == false || this.m_dataLoan.sSpValuationMethodT != E_sSpValuationMethodT.AutomatedValuationModel)
            {
                return;
            }

            this.m_xmlWriter.WriteStartElement("AVM");

            this.WriteAttributeString("_Date", this.m_dataLoan.sSpValuationEffectiveD_rep);

            Mismo231.MI.MI_AVMModelNameTypeEnumerated avmModel = LendersOffice.Integration.MortgageInsurance.MIMismoConvertor.ToMismo(this.m_dataLoan.sSpAvmModelT);
            string avmModelString = Utilities.ConvertEnumToString(avmModel);

            this.WriteAttributeString("_ModelNameType", avmModelString);
            this.WriteAttributeString("_ModelNameTypeOtherDescription", avmModel == Mismo231.MI.MI_AVMModelNameTypeEnumerated.Other ? this.m_dataLoan.sSpAvmModelT.ToString("G") : string.Empty);
            this.WriteAttributeString("_IndicatedValueAmount", this.m_dataLoan.sApprVal_rep);

            ////_HighValueRangeAmount
            ////_LowValueRangeAmount
            ////_ConfidenceScoreIdentifier
            ////_ConfidenceScoreIndicator

            this.m_xmlWriter.WriteEndElement(); // </AVM>
        }

        private void WriteProposedHousingExpense()
        {
            //  <xs:element name="PROPOSED_HOUSING_EXPENSE">
            //	    <xs:complexType>
            //	        <xs:attribute name="HousingExpenseType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="FirstMortgagePrincipalAndInterest"/>
            //                      <xs:enumeration value="GroundRent"/>
            //                      <xs:enumeration value="HazardInsurance"/>
            //                      <xs:enumeration value="HomeownersAssociationDuesAndCondominiumFees"/>
            //                      <xs:enumeration value="MI"/>
            //                      <xs:enumeration value="OtherHousingExpense"/>
            //                      <xs:enumeration value="OtherMortgageLoanPrincipalAndInterest"/>
            //                      <xs:enumeration value="RealEstateTax"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_PaymentAmount" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
            string[,] table = {
                                    { "FirstMortgagePrincipalAndInterest",           this.m_dataLoan.sProFirstMPmt_rep },
                                    { "HazardInsurance",                             this.m_dataLoan.sProHazIns_rep },
                                    { "HomeownersAssociationDuesAndCondominiumFees", this.m_dataLoan.sProHoAssocDues_rep },
                                    { "MI",                                          this.m_dataLoan.sProMIns_rep },
                                    { "OtherHousingExpense",                         this.m_dataLoan.sProOHExp_rep },
                                    { "OtherMortgageLoanPrincipalAndInterest",       this.m_dataLoan.sProSecondMPmt_rep },
                                    { "RealEstateTax",                               this.m_dataLoan.sProRealETx_rep } 
                               };

            for (int i = 0; i < table.GetLength(0); i++)
            {
                this.m_xmlWriter.WriteStartElement("PROPOSED_HOUSING_EXPENSE");

                this.WriteAttributeString("HousingExpenseType", table[i, 0]);
                this.WriteAttributeString("_PaymentAmount", table[i, 1]);

                this.m_xmlWriter.WriteEndElement();
            }
        }

        private void WriteLoanProductData()
        {
            //  <xs:element name="LOAN_PRODUCT_DATA">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="ARM" minOccurs="0"/>
            //              <xs:element ref="BUYDOWN" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="LOAN_FEATURES" minOccurs="0"/>
            //              <xs:element ref="PAYMENT_ADJUSTMENT" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="RATE_ADJUSTMENT" minOccurs="0" maxOccurs="unbounded"/>
            //          </xs:sequence>
            //      </xs:complexType>
            //  </xs:element>
            m_xmlWriter.WriteStartElement("LOAN_PRODUCT_DATA");

            if (m_dataLoan.sFinMethT == E_sFinMethT.ARM)
            {
                WriteARM();
            }

            WriteBuyDown();
            WriteLoanFeatures();
            WritePaymentAdjustment();
            WriteRateAdjustment();

            m_xmlWriter.WriteEndElement(); // </LOAN_PRODUCT_DATA>
        }
        private void WriteARM()
        {
            //  <xs:element name="ARM">
            //	    <xs:complexType>
            //          <xs:attribute name="_IndexCurrentValuePercent" type="xs:string"/>
            //          <xs:attribute name="_IndexMarginPercent" type="xs:string"/>
            //	        <xs:attribute name="_IndexType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="EleventhDistrictCostOfFunds"/>
            //                      <xs:enumeration value="OneYearTreasury"/>
            //                      <xs:enumeration value="ThreeYearTreasury"/>
            //                      <xs:enumeration value="SixMonthTreasury"/>
            //                      <xs:enumeration value="DailyCertificateOfDepositRate"/>
            //                      <xs:enumeration value="FNM60DayRequiredNetYield"/>
            //                      <xs:enumeration value="FNM_LIBOR"/>
            //                      <xs:enumeration value="FederalCostOfFunds"/>
            //                      <xs:enumeration value="FRE60DayRequiredNetYield"/>
            //                      <xs:enumeration value="FRE_LIBOR"/>
            //                      <xs:enumeration value="LIBOR"/>
            //                      <xs:enumeration value="MonthlyAverageConstantMaturingTreasury"/>
            //                      <xs:enumeration value="NationalAverageContractRateFHLBB"/>
            //                      <xs:enumeration value="NationalMonthlyMedianCostOfFunds"/>
            //                      <xs:enumeration value="Other"/>
            //                      <xs:enumeration value="TreasuryBillDailyValue"/>
            //                      <xs:enumeration value="WallStreetJournalLIBOR"/>
            //                      <xs:enumeration value="WeeklyAverageCertificateOfDepositRate"/>
            //                      <xs:enumeration value="WeeklyAverageConstantMaturingTreasury"/>
            //                      <xs:enumeration value="WeeklyAveragePrimeRate"/>
            //                      <xs:enumeration value="WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield"/>
            //                      <xs:enumeration value="WeeklyAverageTreasuryAuctionAverageBondDiscountYield"/>
            //                      <xs:enumeration value="WeeklyAverageTreasuryAuctionAverageInvestmentYield"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_QualifyingRatePercent" type="xs:string"/>
            //          <xs:attribute name="PaymentAdjustmentLifetimeCapAmount" type="xs:string"/>
            //          <xs:attribute name="PaymentAdjustmentLifetimeCapPercent" type="xs:string"/>
            //          <xs:attribute name="RateAdjustmentLifetimeCapPercent" type="xs:string"/>
            //	        <xs:attribute name="_ConversionOptionIndicator" /> // NMTOKEN {Y, N}
            //      </xs:complexType>
            //  </xs:element>
            this.m_xmlWriter.WriteStartElement("ARM");

            this.WriteAttributeString("_IndexCurrentValuePercent", m_dataLoan.sRAdjIndexR_rep);
            this.WriteAttributeString("_IndexMarginPercent", m_dataLoan.sRAdjMarginR_rep);
            this.WriteAttributeString("_IndexType", this.RetrieveIndexType());
            this.WriteAttributeString("_QualifyingRatePercent", m_dataLoan.sQualIR_rep);
            ////this.WriteAttributeString("PaymentAdjustmentLifetimeCapAmount", xxxx);
            this.WriteAttributeString("PaymentAdjustmentLifetimeCapPercent", m_dataLoan.sPmtAdjMaxBalPc_rep);
            this.WriteAttributeString("RateAdjustmentLifetimeCapPercent", m_dataLoan.sRAdjLifeCapR_rep);
            ////this.m_xmlWriter.WriteAttributeString("_ConversionOptionIndicator", xxxx);

            m_xmlWriter.WriteEndElement(); // </ARM>
        }

        /// <summary>
        /// Retrieves the index type for an ARM loan. Uses the Fannie index if it exists, 
        /// otherwise uses the Freddie index.
        /// </summary>
        /// <returns>The index type for an ARM loan.</returns>
        private string RetrieveIndexType()
        {
            var indexType = string.Empty;

            switch (m_dataLoan.sArmIndexT)
            {
                case E_sArmIndexT.LeaveBlank:
                    indexType = string.Empty; break;
                case E_sArmIndexT.WeeklyAvgCMT:
                    indexType = "WeeklyAverageConstantMaturingTreasury"; break;
                case E_sArmIndexT.MonthlyAvgCMT:
                    indexType = "MonthlyAverageConstantMaturingTreasury"; break;
                case E_sArmIndexT.WeeklyAvgTAAI:
                    indexType = "WeeklyAverageTreasuryAuctionAverageInvestmentYield"; break;
                case E_sArmIndexT.WeeklyAvgTAABD:
                    indexType = "WeeklyAverageTreasuryAuctionAverageBondDiscountYield"; break;
                case E_sArmIndexT.WeeklyAvgSMTI:
                    indexType = "WeeklyAverageSecondaryMarketTreasuryBillInvestmentYield"; break;
                case E_sArmIndexT.DailyCDRate:
                    indexType = "DailyCertificateOfDepositRate"; break;
                case E_sArmIndexT.WeeklyAvgCDRate:
                    indexType = "WeeklyAverageCertificateOfDepositRate"; break;
                case E_sArmIndexT.WeeklyAvgPrimeRate:
                    indexType = "WeeklyAveragePrimeRate"; break;
                case E_sArmIndexT.TBillDailyValue:
                    indexType = "TreasuryBillDailyValue"; break;
                case E_sArmIndexT.EleventhDistrictCOF:
                    indexType = "EleventhDistrictCostOfFunds"; break;
                case E_sArmIndexT.NationalMonthlyMedianCostOfFunds:
                    indexType = "NationalMonthlyMedianCostOfFunds"; break;
                case E_sArmIndexT.WallStreetJournalLIBOR:
                    indexType = "WallStreetJournalLIBOR"; break;
                case E_sArmIndexT.FannieMaeLIBOR:
                    indexType = "FNM_LIBOR"; break;
                default:
                    throw new UnhandledEnumException(m_dataLoan.sArmIndexT);
            }

            if (string.IsNullOrEmpty(indexType))
            {
                switch (m_dataLoan.sFreddieArmIndexT)
                {
                    case E_sFreddieArmIndexT.LeaveBlank:
                        indexType = string.Empty; break;
                    case E_sFreddieArmIndexT.OneYearTreasury:
                        indexType = "OneYearTreasury"; break;
                    case E_sFreddieArmIndexT.ThreeYearTreasury:
                        indexType = "ThreeYearTreasury"; break;
                    case E_sFreddieArmIndexT.SixMonthTreasury:
                        indexType = "SixMonthTreasury"; break;
                    case E_sFreddieArmIndexT.EleventhDistrictCostOfFunds:
                        indexType = "EleventhDistrictCostOfFunds"; break;
                    case E_sFreddieArmIndexT.NationalMonthlyMedianCostOfFunds:
                        indexType = "NationalMonthlyMedianCostOfFunds"; break;
                    case E_sFreddieArmIndexT.LIBOR:
                        indexType = "LIBOR"; break;
                    case E_sFreddieArmIndexT.Other:
                        indexType = "Other"; break;
                    default:
                        throw new UnhandledEnumException(m_dataLoan.sFreddieArmIndexT);
                }
            }

            return indexType;
        }

        private void WriteBuyDown() 
        {
            //  <xs:element name="BUYDOWN">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="_SUBSIDY_SCHEDULE" minOccurs="0" maxOccurs="unbounded"/>
            //          </xs:sequence>
            //	        <xs:attribute name="_BaseDateType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="NoteDate"/>
            //                      <xs:enumeration value="FirstPaymentDate"/>
            //                      <xs:enumeration value="LastPaymentDate"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_ChangeFrequencyMonths" type="xs:string"/>
            //	        <xs:attribute name="_ContributorType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Borrower"/>
            //                      <xs:enumeration value="Other"/>
            //                      <xs:enumeration value="LenderPremiumFinanced"/>
            //                      <xs:enumeration value="Seller"/>
            //                      <xs:enumeration value="Builder"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_ContributorTypeOtherDescription" type="xs:string"/>
            //          <xs:attribute name="_DurationMonths" type="xs:string"/>
            //          <xs:attribute name="_IncreaseRatePercent" type="xs:string"/>
            //	        <xs:attribute name="_LenderFundingIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="_OriginalBalanceAmount" type="xs:string"/>
            //	        <xs:attribute name="_SubsidyCalculationType" /> // NMTOKEN {DecliningLoanBalance, OriginalLoanAmount}
            //	        <xs:attribute name="_PermanentIndicator" /> // NMTOKEN {Y, N}
            //      </xs:complexType>
            //  </xs:element>

            if (m_dataLoan.sHasTempBuydown)
            {
                List<Buydown> condensedBuydownList = GenerateCondensedBuydown(m_dataLoan);

                foreach (var condensedBuydown in condensedBuydownList)
                {
                    this.m_xmlWriter.WriteStartElement("BUYDOWN");

                    this.WriteAttributeString("_ChangeFrequencyMonths", (m_dataLoan.m_convertLos.ToCountString(condensedBuydown.ChangeFrequencyMonths).Equals("")) ? "0" : m_dataLoan.m_convertLos.ToCountString(condensedBuydown.ChangeFrequencyMonths));
                    this.WriteAttributeString("_DurationMonths", (m_dataLoan.m_convertLos.ToCountString(condensedBuydown.DurationMonths).Equals("")) ? "0" : m_dataLoan.m_convertLos.ToCountString(condensedBuydown.DurationMonths));
                    this.WriteAttributeString("_IncreaseRatePercent", (m_dataLoan.m_convertLos.ToRateStringNoPercent(condensedBuydown.IncreaseRatePercent).Equals("")) ? "0.000" : m_dataLoan.m_convertLos.ToRateStringNoPercent(condensedBuydown.IncreaseRatePercent));

                    m_xmlWriter.WriteEndElement(); // </ARM>
                }
            }
        }
        private void WriteLoanFeatures()
        {
            //  <xs:element name="LOAN_FEATURES">
            //	    <xs:complexType>
            //	        <xs:attribute name="NameDocumentsDrawnInType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Broker"/>
            //                      <xs:enumeration value="Lender"/>
            //                      <xs:enumeration value="Investor"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="AssumabilityIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="BalloonIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="BalloonLoanMaturityTermMonths" type="xs:string"/>
            //	        <xs:attribute name="BuydownTemporarySubsidyIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="CounselingConfirmationIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="CounselingConfirmationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="AmericanHomeownerEducationInstituteApprovedCounseling"/>
            //                      <xs:enumeration value="LenderTrainedCounseling"/>
            //                      <xs:enumeration value="NoBorrowerCounseling"/>
            //                      <xs:enumeration value="ThirdPartyCounseling"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="DownPaymentOptionType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="ThreeTwoOption"/>
            //                      <xs:enumeration value="FivePercentOption"/>
            //                      <xs:enumeration value="FNM97Option"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="EscrowAggregateAccountingAdjustmentAmount" type="xs:string"/>
            //	        <xs:attribute name="EscrowWaiverIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="EscrowCushionNumberOfMonthsCount" type="xs:string"/>
            //          <xs:attribute name="FREOfferingIdentifier" type="xs:string"/>
            //          <xs:attribute name="FNMProductPlanIdentifier" type="xs:string"/>
            //          <xs:attribute name="FNMProductPlanIndentifier" type="xs:string"/>
            //	        <xs:attribute name="GSEProjectClassificationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="A_IIICondominium"/>
            //                      <xs:enumeration value="ApprovedFHA_VACondominiumProjectOrSpotLoan"/>
            //                      <xs:enumeration value="B_IICondominium"/>
            //                      <xs:enumeration value="C_ICondominium"/>
            //                      <xs:enumeration value="OneCooperative"/>
            //                      <xs:enumeration value="TwoCooperative"/>
            //                      <xs:enumeration value="E_PUD"/>
            //                      <xs:enumeration value="F_PUD"/>
            //                      <xs:enumeration value="III_PUD"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="GSEPropertyType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Attached"/>
            //                      <xs:enumeration value="Condominium"/>
            //                      <xs:enumeration value="Cooperative"/>
            //                      <xs:enumeration value="Detached"/>
            //                      <xs:enumeration value="HighRiseCondominium"/>
            //                      <xs:enumeration value="ManufacturedHousing"/>
            //                      <xs:enumeration value="Modular"/>
            //                      <xs:enumeration value="PUD"/>
            //                      <xs:enumeration value="ManufacturedHousingSingleWide"/>
            //                      <xs:enumeration value="ManufacturedHousingDoubleWide"/>
            //                      <xs:enumeration value="DetachedCondominium"/>
            //                      <xs:enumeration value="ManufacturedHomeCondominium"/>
            //                      <xs:enumeration value="ManufacturedHousingMultiWide"/>
            //                      <xs:enumeration value="ManufacturedHomeCondominiumOrPUDOrCooperative"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="HELOCMaximumBalanceAmount" type="xs:string"/>
            //          <xs:attribute name="HELOCInitialAdvanceAmount" type="xs:string"/>
            //          <xs:attribute name="InterestOnlyTerm" type="xs:string"/>
            //	        <xs:attribute name="LenderSelfInsuredIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="LienPriorityType" /> // NMTOKEN {FirstLien, Other, SecondLien}
            //	        <xs:attribute name="LoanDocumentationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Alternative"/>
            //                      <xs:enumeration value="FullDocumentation"/>
            //                      <xs:enumeration value="NoDepositVerification"/>
            //                      <xs:enumeration value="NoDepositVerificationEmploymentVerificationOrIncomeVerification"/>
            //                      <xs:enumeration value="NoDocumentation"/>
            //                      <xs:enumeration value="NoEmploymentVerificationOrIncomeVerification"/>
            //                      <xs:enumeration value="Reduced"/>
            //                      <xs:enumeration value="StreamlineRefinance"/>
            //                      <xs:enumeration value="NoRatio"/>
            //                      <xs:enumeration value="NoIncomeNoEmploymentNoAssetsOn1003"/>
            //                      <xs:enumeration value="NoIncomeOn1003"/>
            //                      <xs:enumeration value="NoVerificationOfStatedIncomeEmploymentOrAssets"/>
            //                      <xs:enumeration value="NoVerificationOfStatedIncomeOrAssests"/>
            //                      <xs:enumeration value="NoVerificationOfStatedAssets"/>
            //                      <xs:enumeration value="NoVerificationOfStatedIncomeOrEmployment"/>
            //                      <xs:enumeration value="NoVerificationOfStatedIncome"/>
            //                      <xs:enumeration value="VerbalVerificationOfEmployment"/>
            //                      <xs:enumeration value="OnePaystub"/>
            //                      <xs:enumeration value="OnePaystubAndVerbalVerificationOfEmployment"/>
            //                      <xs:enumeration value="OnePaystubAndOneW2AndVerbalVerificationOfEmploymentOrOneYear1040"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="LoanRepaymentType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="InterestOnly"/>
            //                      <xs:enumeration value="NoNegativeAmortization"/>
            //                      <xs:enumeration value="PotentialNegativeAmortization"/>
            //                      <xs:enumeration value="ScheduledAmortization"/>
            //                      <xs:enumeration value="ScheduledNegativeAmortization"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="LoanScheduledClosingDate" type="xs:string"/>
            //	        <xs:attribute name="MICertificationStatusType" /> // NMTOKEN {LenderToObtain, SellerOfLoanToObtain}
            //          <xs:attribute name="MICoveragePercent" type="xs:string"/>
            //	        <xs:attribute name="MICompanyNameType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="AmerinGuaranteeCorporation"/>
            //                      <xs:enumeration value="CMG_MICompany"/>
            //                      <xs:enumeration value="CommonwealthMortgageAssuranceCompany"/>
            //                      <xs:enumeration value="GECapitalMICorporation"/>
            //                      <xs:enumeration value="MortgageGuarantyInsuranceCorporation"/>
            //                      <xs:enumeration value="PMI_MICorporation"/>
            //                      <xs:enumeration value="RadianGuarantyIncorporated"/>
            //                      <xs:enumeration value="RepublicMICompany"/>
            //                      <xs:enumeration value="TriadGuarantyInsuranceCorporation"/>
            //                      <xs:enumeration value="UnitedGuarantyCorporation"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="NegativeAmortizationLimitPercent" type="xs:string"/>
            //	        <xs:attribute name="PaymentFrequencyType" /> // NMTOKEN {Biweekly, Monthly}
            //	        <xs:attribute name="PrepaymentPenaltyIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="FullPrepaymentPenaltyOptionType" /> // NMTOKEN {Hard, Soft}
            //          <xs:attribute name="PrepaymentPenaltyTermMonths" type="xs:string"/>
            //	        <xs:attribute name="PrepaymentRestrictionIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="ProductDescription" type="xs:string"/>
            //          <xs:attribute name="ProductName" type="xs:string"/>
            //          <xs:attribute name="ScheduledFirstPaymentDate" type="xs:string"/>
            //	        <xs:attribute name="LoanClosingStatusType" /> // NMTOKEN {Closed, TableFunded}
            //	        <xs:attribute name="ServicingTransferStatusType" /> // NMTOKEN {Retained, Released}
            //	        <xs:attribute name="DemandFeatureIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="EstimatedPrepaidDays" type="xs:string"/>
            //	        <xs:attribute name="EstimatedPrepaidDaysPaidByType" /> // NMTOKEN {Buyer, Lender, Other, Seller}
            //          <xs:attribute name="EstimatedPrepaidDaysPaidByOtherTypeDescription" type="xs:string"/>
            //          <xs:attribute name="InitialPaymentRatePercent" type="xs:string"/>
            //	        <xs:attribute name="PrepaymentFinanceChargeRefundableIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="RefundableApplicationFeeIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="RequiredDepositIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="OriginalBalloonTermMonths" type="xs:string"/>
            //	        <xs:attribute name="ConditionsToAssumabilityIndicator" /> // NMTOKEN {Y, N}
            //      </xs:complexType>
            //  </xs:element>

            this.m_xmlWriter.WriteStartElement("LOAN_FEATURES");

            this.WriteAttributeString("AssumabilityIndicator", AssumabilityMap.ToMismo(this.m_dataLoan.sAssumeLT)); // NMTOKEN (Y, N)
            this.WriteAttributeYN("BalloonIndicator", this.m_dataLoan.sBalloonPmt); // NMTOKEN (Y, N)
            this.WriteAttributeString("BalloonLoanMaturityTermMonths", this.m_dataLoan.sDue_rep);
            this.WriteAttributeString("EscrowAggregateAccountingAdjustmentAmount", this.m_dataLoan.sAggregateAdjRsrv_rep);

            string mappedProgramId = Tools.sFredAffordProdIdToLoanProspectorId(this.m_dataLoan.sFredAffordProgId);
            if (!string.IsNullOrEmpty(mappedProgramId) && mappedProgramId != this.m_dataLoan.sFredAffordProgId)
            {
                this.WriteAttributeString("FREOfferingIdentifier", mappedProgramId);
            }
            else if (this.m_dataLoan.sLpProductT == E_sLpProductT.HomePossible)
            {
                this.WriteAttributeString("FREOfferingIdentifier", "241");
            }

            this.WriteAttributeString("ProjectName", this.m_dataLoan.sProjNm);
            
            if (this.m_dataLoan.sHomeIsMhAdvantageTri == E_TriState.Yes)
            {
                this.WriteAttributeString("GSEPropertyType", GSEPropertyTypeMap.ToMismo(E_sGseSpT.ManufacturedHomeMultiwide));
            }
            else
            {
                this.WriteAttributeString("GSEPropertyType", GSEPropertyTypeMap.ToMismo(this.m_dataLoan.sGseSpT));
            }

            if (this.m_dataLoan.BrokerDB.IsEnableHELOC && this.m_dataLoan.sIsLineOfCredit)
            {
                this.WriteAttributeString("HELOCInitialAdvanceAmount", this.m_dataLoan.sLAmtCalc_rep);
                this.WriteAttributeString("HELOCMaximumBalanceAmount", this.m_dataLoan.sCreditLineAmt_rep);
            }

            this.WriteAttributeString("InterestOnlyTerm", this.m_dataLoan.sIOnlyMon_rep);
            this.WriteAttributeString("LienPriorityType", LienPositionMap.ToMismo(this.m_dataLoan.sLienPosT));
            this.WriteAttributeString("LoanDocumentationType", LoanDocumentationTypeMap.ToMismo(this.m_dataLoan.sProdDocT, this.m_dataLoan.sLPurposeT));
            this.WriteAttributeString("LoanScheduledClosingDate", this.m_dataLoan.sEstCloseD_rep);

            //// 01/06/2015 BB - The MI vendor only accepts a whole percentage without decimals.
            string coverage = string.Empty;

            if (this.mirequest)
            {
                int roundedCoverage = (int)Math.Round(this.m_dataLoan.sMiLenderPaidCoverage);
                coverage = this.m_dataLoan.m_convertLos.ToCountString(roundedCoverage);
                coverage = string.IsNullOrEmpty(coverage) ? "0" : coverage;
            }
            else
            {
                coverage = this.m_dataLoan.sMiLenderPaidCoverage_rep;
            }

            this.WriteAttributeString("MICoveragePercent", coverage);

            this.WriteAttributeString("ProductDescription", this.m_dataLoan.sLpTemplateNm);
            
            if (this.m_dataLoan.sHomeIsMhAdvantageTri == E_TriState.Yes)
            {
                this.WriteAttributeString("ProductName", "MH Advantage");
            }
            else
            {
                this.WriteAttributeString("ProductName", this.m_dataLoan.sLpTemplateNm);
            }

            this.WriteAttributeString("ScheduledFirstPaymentDate", this.m_dataLoan.sSchedDueD1_rep);
            this.WriteAttributeString("NegativeAmortizationLimitPercent", this.m_dataLoan.sPmtAdjMaxBalPc_rep);
            this.WriteAttributeString("PaymentFrequencyType", "Monthly");
            this.WriteAttributeString("PrepaymentPenaltyIndicator", PrepayPenaltyMap.ToMismo(this.m_dataLoan.sPrepmtPenaltyT)); // NMTOKEN (Y, N)
            this.WriteAttributeString("PrepaymentPenaltyTermMonths", this.m_dataLoan.sPrepmtPeriodMonths_rep);
            this.WriteAttributeYN("DemandFeatureIndicator", this.m_dataLoan.sHasDemandFeature); // NMTOKEN (Y, N)
            this.WriteAttributeString("EstimatedPrepaidDays", this.m_dataLoan.sIPiaDy_rep);
            this.WriteAttributeString("PrepaymentFinanceChargeRefundableIndicator", PrepayRefundChargeMap.ToMismo(this.m_dataLoan.sPrepmtRefundT)); // NMTOKEN (Y, N)
            this.WriteAttributeString("OriginalBalloonTermMonths", this.m_dataLoan.sTerm_rep);

            this.m_xmlWriter.WriteEndElement(); // </LOAN_FEATURES>
        }

        private void WritePaymentAdjustment()
        {
            //  <xs:element name="PAYMENT_ADJUSTMENT">
            //	    <xs:complexType>
            //          <xs:attribute name="FirstPaymentAdjustmentMonths" type="xs:string"/>
            //          <xs:attribute name="_Amount" type="xs:string"/>
            //	        <xs:attribute name="_CalculationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="AddFixedDollarAmountToTheCurrentPayment"/>
            //                      <xs:enumeration value="AddPercentToCurrentPaymentAmount"/>
            //                      <xs:enumeration value="AddPercentToEffectivePaymentRate"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_DurationMonths" type="xs:string"/>
            //          <xs:attribute name="_Percent" type="xs:string"/>
            //          <xs:attribute name="_PeriodicCapAmount" type="xs:string"/>
            //          <xs:attribute name="_PeriodicCapPercent" type="xs:string"/>
            //          <xs:attribute name="_PeriodNumber" type="xs:string"/>
            //          <xs:attribute name="SubsequentPaymentAdjustmentMonths" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

            this.m_xmlWriter.WriteStartElement("PAYMENT_ADJUSTMENT");

            this.WriteAttributeString("FirstPaymentAdjustmentMonths", this.m_dataLoan.sPmtAdjCapMon_rep);
            this.WriteAttributeString("_PeriodicCapPercent", this.m_dataLoan.sPmtAdjCapR_rep);
            this.WriteAttributeString("_PeriodNumber", this.m_dataLoan.sPmtAdjCapMon_rep);
            this.WriteAttributeString("SubsequentPaymentAdjustmentMonths", this.m_dataLoan.sPmtAdjCapMon_rep);

            this.m_xmlWriter.WriteEndElement(); // </PAYMENT_ADJUSTMENT>
        }

        private void WriteRateAdjustment()
        {
            //  <xs:element name="RATE_ADJUSTMENT">
            //	    <xs:complexType>
            //          <xs:attribute name="FirstRateAdjustmentMonths" type="xs:string"/>
            //	        <xs:attribute name="_CalculationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="AddPercentToCurrentRate"/>
            //                      <xs:enumeration value="AddPercentToOriginalRate"/>
            //                      <xs:enumeration value="IndexPlusMargin"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_DurationMonths" type="xs:string"/>
            //          <xs:attribute name="_Percent" type="xs:string"/>
            //          <xs:attribute name="_PeriodNumber" type="xs:string"/>
            //          <xs:attribute name="_SubsequentCapPercent" type="xs:string"/>
            //          <xs:attribute name="SubsequentRateAdjustmentMonths" type="xs:string"/>
            //          <xs:attribute name="_InitialCapPercent" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

            this.m_xmlWriter.WriteStartElement("RATE_ADJUSTMENT");

            this.WriteAttributeString("FirstRateAdjustmentMonths", this.m_dataLoan.sRAdj1stCapMon_rep);
            this.WriteAttributeString("SubsequentRateAdjustmentMonths", this.m_dataLoan.sRAdjCapMon_rep);
            this.WriteAttributeString("_SubsequentCapPercent", this.m_dataLoan.sRAdjCapR_rep);
            this.WriteAttributeString("_InitialCapPercent", this.m_dataLoan.sRAdj1stCapR_rep);

            this.m_xmlWriter.WriteEndElement(); // </RATE_ADJUSTMENT>
        }
        private void WriteLoanPurpose()
        {
            //  <xs:element name="LOAN_PURPOSE">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="CONSTRUCTION_REFINANCE_DATA" minOccurs="0"/>
            //          </xs:sequence>
            //          <xs:attribute name="GSETitleMannerHeldDescription" type="xs:string"/>
            //	        <xs:attribute name="_Type">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="ConstructionOnly"/>
            //                      <xs:enumeration value="ConstructionToPermanent"/>
            //                      <xs:enumeration value="Other"/>
            //                      <xs:enumeration value="Purchase"/>
            //                      <xs:enumeration value="Refinance"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="OtherLoanPurposeDescription" type="xs:string"/>
            //          <xs:attribute name="PropertyLeaseholdExpirationDate" type="xs:string"/>
            //	        <xs:attribute name="PropertyRightsType" /> // NMTOKEN {FeeSimple, Leasehold}
            //	        <xs:attribute name="PropertyUsageType" /> // NMTOKEN {Investor, PrimaryResidence, SecondHome}
            //      </xs:complexType>
            //  </xs:element>
            m_xmlWriter.WriteStartElement("LOAN_PURPOSE");

            WriteAttributeString("GSETitleMannerHeldDescription", m_dataLoan.GetAppData(0).aManner);
            WriteAttributeString("_Type", LoanPurposeTypeMap.ToMismo(m_dataLoan.sLPurposeT));
            WriteAttributeString("OtherLoanPurposeDescription", m_dataLoan.sOLPurposeDesc);
            WriteAttributeString("PropertyLeaseholdExpirationDate", m_dataLoan.sLeaseHoldExpireD_rep);
            WriteAttributeString("PropertyRightsType", EstateHeldTypeMap.ToMismo(m_dataLoan.sEstateHeldT));
            WriteAttributeString("PropertyUsageType", OccupancyTypeMap.ToMismo(m_dataLoan.GetAppData(0).aOccT));

            WriteConstructionRefinanceData();

            m_xmlWriter.WriteEndElement(); //</LOAN_PURPOSE>
        }
        private void WriteConstructionRefinanceData()
        {
            //  <xs:element name="CONSTRUCTION_REFINANCE_DATA">
            //	    <xs:complexType>
            //          <xs:attribute name="ConstructionImprovementCostsAmount" type="xs:string"/>
            //	        <xs:attribute name="ConstructionPurposeType" /> // NMTOKEN {ConstructionOnly, ConstructionToPermanent}
            //          <xs:attribute name="FRECashOutAmount" type="xs:string"/>
            //	        <xs:attribute name="GSERefinancePurposeType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="CashOutDebtConsolidation"/>
            //                      <xs:enumeration value="CashOutHomeImprovement"/>
            //                      <xs:enumeration value="CashOutLimited"/>
            //                      <xs:enumeration value="CashOutOther"/>
            //                      <xs:enumeration value="NoCashOutFHAStreamlinedRefinance"/>
            //                      <xs:enumeration value="NoCashOutFREOwnedRefinance"/>
            //                      <xs:enumeration value="NoCashOutOther"/>
            //                      <xs:enumeration value="NoCashOutStreamlinedRefinance"/>
            //                      <xs:enumeration value="ChangeInRateTerm"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="LandEstimatedValueAmount" type="xs:string"/>
            //          <xs:attribute name="LandOriginalCostAmount" type="xs:string"/>
            //          <xs:attribute name="PropertyAcquiredYear" type="xs:string"/>
            //          <xs:attribute name="PropertyExistingLienAmount" type="xs:string"/>
            //          <xs:attribute name="PropertyOriginalCostAmount" type="xs:string"/>
            //          <xs:attribute name="RefinanceImprovementCostsAmount" type="xs:string"/>
            //	        <xs:attribute name="RefinanceImprovementsType" /> // NMTOKEN {Made, ToBeMade, Unknown}
            //          <xs:attribute name="RefinanceProposedImprovementsDescription" type="xs:string"/>
            //	        <xs:attribute name="SecondaryFinancingRefinanceIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="ConstructionPeriodInterestRatePercent" type="xs:string"/>
            //          <xs:attribute name="ConstructionPeriodNumberOfMonthsCount" type="xs:string"/>
            //	        <xs:attribute name="FNMSecondMortgageFinancingOriginalPropertyIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="StructuralAlterationsConventionalAmount" type="xs:string"/>
            //          <xs:attribute name="NonStructuralAlterationsConventionalAmount" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

            this.m_xmlWriter.WriteStartElement("CONSTRUCTION_REFINANCE_DATA");

            switch (m_dataLoan.sLPurposeT)
            {
                case E_sLPurposeT.Construct:
                case E_sLPurposeT.ConstructPerm:
                    this.WriteAttributeString("ConstructionPurposeType", this.m_dataLoan.sLPurposeT == E_sLPurposeT.Construct ? "ConstructionOnly" : "ConstructionToPermanent");
                    this.WriteAttributeString("PropertyAcquiredYear", this.m_dataLoan.sLotAcqYr);
                    this.WriteAttributeString("PropertyExistingLienAmount", this.m_dataLoan.sLotLien_rep);
                    this.WriteAttributeString("LandEstimatedValueAmount", this.m_dataLoan.sLotVal_rep);
                    this.WriteAttributeString("LandOriginalCostAmount", this.m_dataLoan.sLotOrigC_rep);
                    this.WriteAttributeString("ConstructionImprovementCostsAmount", this.m_dataLoan.sLotImprovC_rep);
                    this.WriteAttributeString("PropertyOriginalCostAmount", this.m_dataLoan.sLotOrigC_rep);
                    this.WriteAttributeString("ConstructionPeriodInterestRatePercent", this.m_dataLoan.sConstructionPeriodIR_rep);
                    this.WriteAttributeString("ConstructionPeriodNumberOfMonthsCount", this.m_dataLoan.sConstructionPeriodMon_rep);
                    break;
                case E_sLPurposeT.VaIrrrl:
                case E_sLPurposeT.FhaStreamlinedRefinance:
                case E_sLPurposeT.Refin:
                case E_sLPurposeT.RefinCashout:
                case E_sLPurposeT.HomeEquity:
                    this.WriteAttributeString("PropertyAcquiredYear", this.m_dataLoan.sSpAcqYr);
                    this.WriteAttributeString("PropertyExistingLienAmount", this.m_dataLoan.sSpLien_rep);
                    this.WriteAttributeString("FRECashOutAmount", this.m_dataLoan.sProdCashoutAmt_rep);
                    this.WriteAttributeString("GSERefinancePurposeType", GSERefiPurposeMap.ToMismo(this.m_dataLoan.sRefPurpose));
                    this.WriteAttributeString("PropertyOriginalCostAmount", this.m_dataLoan.sSpOrigC_rep);
                    this.WriteAttributeString("RefinanceImprovementCostsAmount", this.m_dataLoan.sSpImprovC_rep);
                    this.WriteAttributeString("RefinanceImprovementsType", RefinanceImprovementTypeMap.ToMismo(this.m_dataLoan.sSpImprovTimeFrameT));
                    this.WriteAttributeString("RefinanceProposedImprovementsDescription", this.m_dataLoan.sSpImprovDesc);
                    this.WriteAttributeYN("SecondaryFinancingRefinanceIndicator", this.m_dataLoan.sPayingOffSubordinate);
                    break;
                case E_sLPurposeT.Other:
                case E_sLPurposeT.Purchase:
                    break;
                default:
                    throw new UnhandledEnumException(this.m_dataLoan.sLPurposeT);
            }

            this.m_xmlWriter.WriteEndElement(); // </CONSTRUCTION_REFINANCE_DATA>
        }

        private void WriteLoanQualification()
        {
            //  <xs:element name="LOAN_QUALIFICATION">
            //	    <xs:complexType>
            //	        <xs:attribute name="AdditionalBorrowerAssetsNotConsideredIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="AdditionalBorrowerAssetsConsideredIndicator" /> // NMTOKEN {Y, N}
            //      </xs:complexType>
            //  </xs:element>

            m_xmlWriter.WriteStartElement("LOAN_QUALIFICATION");
            WriteAttributeYN("AdditionalBorrowerAssetsNotConsideredIndicator", m_dataLoan.GetAppData(0).aSpouseIExcl);
            WriteAttributeYN("AdditionalBorrowerAssetsConsideredIndicator", m_dataLoan.sMultiApps);
            m_xmlWriter.WriteEndElement(); // </LOAN_QUALIFICATION>
        }
        private void WriteMortgageTerms()
        {
            //  <xs:element name="MORTGAGE_TERMS">
            //	    <xs:complexType>
            //          <xs:attribute name="AgencyCaseIdentifier" type="xs:string"/>
            //          <xs:attribute name="ARMTypeDescription" type="xs:string"/>
            //          <xs:attribute name="BaseLoanAmount" type="xs:string"/>
            //          <xs:attribute name="BorrowerRequestedLoanAmount" type="xs:string"/>
            //          <xs:attribute name="LenderCaseIdentifier" type="xs:string"/>
            //          <xs:attribute name="LoanAmortizationTermMonths" type="xs:string"/>
            //	        <xs:attribute name="LoanAmortizationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="AdjustableRate"/>
            //                      <xs:enumeration value="Fixed"/>
            //                      <xs:enumeration value="GraduatedPaymentMortgage"/>
            //                      <xs:enumeration value="GrowingEquityMortgage"/>
            //                      <xs:enumeration value="OtherAmortizationType"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="MortgageType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Conventional"/>
            //                      <xs:enumeration value="FarmersHomeAdministration"/>
            //                      <xs:enumeration value="FHA"/>
            //                      <xs:enumeration value="Other"/>
            //                      <xs:enumeration value="VA"/>
            //                      <xs:enumeration value="HELOC"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="OtherAmortizationTypeDescription" type="xs:string"/>
            //          <xs:attribute name="OtherMortgageTypeDescription" type="xs:string"/>
            //          <xs:attribute name="RequestedInterestRatePercent" type="xs:string"/>
            //          <xs:attribute name="LendersContactPrimaryTelephoneNumber" type="xs:string"/>
            //          <xs:attribute name="LoanEstimatedClosingDate" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

            this.m_xmlWriter.WriteStartElement("MORTGAGE_TERMS");

            this.WriteAttributeString("AgencyCaseIdentifier", this.m_dataLoan.sAgencyCaseNum);
            this.WriteAttributeString("ARMTypeDescription", this.m_dataLoan.sFinMethDesc);
            this.WriteAttributeString("BaseLoanAmount", this.m_dataLoan.sLAmtCalc_rep);
            this.WriteAttributeString("BorrowerRequestedLoanAmount", this.m_dataLoan.sFinalLAmt_rep);
            this.WriteAttributeString("LenderCaseIdentifier", this.m_dataLoan.sLenderCaseNum);
            this.WriteAttributeString("LoanAmortizationTermMonths", this.m_dataLoan.sTerm_rep);
            this.WriteAttributeString("LoanAmortizationType", FinanceMethodMap.ToMismo(this.m_dataLoan.sFinMethT));
            this.WriteAttributeString("MortgageType", LoanTypeMap.ToMismo(this.m_dataLoan.sLT));
            this.WriteAttributeString("OtherAmortizationTypeDescription", this.m_dataLoan.sFinMethDesc);
            this.WriteAttributeString("OtherMortgageTypeDescription", this.m_dataLoan.sLTODesc);
            this.WriteAttributeString("RequestedInterestRatePercent", this.m_dataLoan.sNoteIR_rep);
            this.WriteAttributeString("LendersContactPrimaryTelephoneNumber", this.m_dataLoan.GetAgentOfRole(E_AgentRoleT.Lender, E_ReturnOptionIfNotExist.ReturnEmptyObject).Phone);
            this.WriteAttributeString("LoanEstimatedClosingDate", this.m_dataLoan.sEstCloseD_rep);

            this.m_xmlWriter.WriteEndElement(); // </MORTGAGE_TERMS>
        }

        private void WriteTitleHolder()
        {
            //  <xs:element name="TITLE_HOLDER">
            //	    <xs:complexType>
            //          <xs:attribute name="_Name" type="xs:string"/>
            //	        <xs:attribute name="LandTrustType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="IllinoisLandTrust"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //      </xs:complexType>
            //  </xs:element>
            int nApps = m_dataLoan.nApps;
            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                string nm1 = dataApp.aTitleNm1;
                string nm2 = dataApp.aTitleNm2;

                if ("" != nm1)
                {
                    m_xmlWriter.WriteStartElement("TITLE_HOLDER");
                    WriteAttributeString("_Name", nm1);
                    m_xmlWriter.WriteEndElement();
                }
                if ("" != nm2)
                {
                    m_xmlWriter.WriteStartElement("TITLE_HOLDER");
                    WriteAttributeString("_Name", nm2);
                    m_xmlWriter.WriteEndElement();
                }
            }
        }
        private void WriteTransactionDetail()
        {
            //  <xs:element name="TRANSACTION_DETAIL">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="PURCHASE_CREDIT" minOccurs="0" maxOccurs="unbounded"/>
            //          </xs:sequence>
            //          <xs:attribute name="AlterationsImprovementsAndRepairsAmount" type="xs:string"/>
            //          <xs:attribute name="BorrowerPaidDiscountPointsTotalAmount" type="xs:string"/>
            //          <xs:attribute name="EstimatedClosingCostsAmount" type="xs:string"/>
            //          <xs:attribute name="MIAndFundingFeeFinancedAmount" type="xs:string"/>
            //          <xs:attribute name="MIAndFundingFeeTotalAmount" type="xs:string"/>
            //          <xs:attribute name="PrepaidItemsEstimatedAmount" type="xs:string"/>
            //          <xs:attribute name="PurchasePriceAmount" type="xs:string"/>
            //          <xs:attribute name="RefinanceIncludingDebtsToBePaidOffAmount" type="xs:string"/>
            //          <xs:attribute name="SalesConcessionAmount" type="xs:string"/>
            //          <xs:attribute name="SellerPaidClosingCostsAmount" type="xs:string"/>
            //          <xs:attribute name="SubordinateLienAmount" type="xs:string"/>
            //          <xs:attribute name="SubordinateLienHELOCAmount" type="xs:string"/>
            //          <xs:attribute name="FREReserveAmount" type="xs:string"/>
            //          <xs:attribute name="FREReservesAmount" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
            this.m_xmlWriter.WriteStartElement("TRANSACTION_DETAIL");

            this.WriteAttributeString("AlterationsImprovementsAndRepairsAmount", this.m_dataLoan.sAltCost_rep);
            this.WriteAttributeString("BorrowerPaidDiscountPointsTotalAmount", this.m_dataLoan.sLDiscnt1003_rep);
            this.WriteAttributeString("EstimatedClosingCostsAmount", this.m_dataLoan.sTotEstCcNoDiscnt1003_rep);
            this.WriteAttributeString("MIAndFundingFeeFinancedAmount", this.m_dataLoan.sFfUfmipFinanced_rep);
            this.WriteAttributeString("MIAndFundingFeeTotalAmount", this.m_dataLoan.sFfUfmip1003_rep);
            this.WriteAttributeString("PrepaidItemsEstimatedAmount", this.m_dataLoan.sTotEstPp1003_rep);

            if (this.m_dataLoan.sLPurposeT == E_sLPurposeT.Purchase)
            {
                this.WriteAttributeString("PurchasePriceAmount", this.m_dataLoan.sPurchPrice_rep);
                this.WriteAttributeString("SalesConcessionAmount", this.m_dataLoan.sFHASalesConcessions_rep);
                this.WriteAttributeString("SellerPaidClosingCostsAmount", this.m_dataLoan.sTotCcPbs_rep);
            }
            else if ((this.m_dataLoan.sLPurposeT == E_sLPurposeT.FhaStreamlinedRefinance) ||
                (this.m_dataLoan.sLPurposeT == E_sLPurposeT.Refin) ||
                (this.m_dataLoan.sLPurposeT == E_sLPurposeT.RefinCashout) ||
                (this.m_dataLoan.sLPurposeT == E_sLPurposeT.HomeEquity) ||
                (this.m_dataLoan.sLPurposeT == E_sLPurposeT.VaIrrrl))
            {
                this.WriteAttributeString("RefinanceIncludingDebtsToBePaidOffAmount", this.m_dataLoan.sRefPdOffAmt1003_rep);
            }

            if (this.m_dataLoan.sLienPosT == E_sLienPosT.First)
            {
                if (this.m_dataLoan.sIsOFinCreditLineInDrawPeriod)
                {
                    this.WriteAttributeString("SubordinateLienAmount", this.m_dataLoan.sSubFin_rep);
                    this.WriteAttributeString("SubordinateLienHELOCAmount", this.m_dataLoan.sConcurSubFin_rep);
                }
                else
                {
                    this.WriteAttributeString("SubordinateLienAmount", this.m_dataLoan.sConcurSubFin_rep);
                }
            }

            this.WriteAttributeString("FREReservesAmount", this.m_dataLoan.sFredieReservesAmt_rep);

            this.WritePurchaseCredits();

            m_xmlWriter.WriteEndElement(); //</TRANSACTION_DETAIL>
        }

        /// <summary>
        /// Writes elements for the purchase credits. If adjustments and 1003 details are synced, we write all adjustments and any
        /// cash deposits or lender credits from the 1003 details. If they are not synced, we write all adjustments and any 1003
        /// details that are not duplicates of adjustments.
        /// </summary>
        private void WritePurchaseCredits()
        {
            var adjustments = this.m_dataLoan.sAdjustmentList.Where(adjustment => adjustment.IsPopulateToLineLOn1003);
            foreach (var adjustment in adjustments)
            {
                this.WritePurchaseCredit(adjustment.Amount_rep, adjustment.AdjustmentType, adjustment.PaidFromParty);
            }

            var customCredits = new[]
            {
                new { Description = this.m_dataLoan.sOCredit1Desc, Amount = this.m_dataLoan.sOCredit1Amt, Amount_rep = this.m_dataLoan.sOCredit1Amt_rep },
                new { Description = this.m_dataLoan.sOCredit2Desc, Amount = this.m_dataLoan.sOCredit2Amt, Amount_rep = this.m_dataLoan.sOCredit2Amt_rep },
                new { Description = this.m_dataLoan.sOCredit3Desc, Amount = this.m_dataLoan.sOCredit3Amt, Amount_rep = this.m_dataLoan.sOCredit3Amt_rep },
                new { Description = this.m_dataLoan.sOCredit4Desc, Amount = this.m_dataLoan.sOCredit4Amt, Amount_rep = this.m_dataLoan.sOCredit4Amt_rep },
                new { Description = this.m_dataLoan.sOCredit5Desc, Amount = this.m_dataLoan.sOCredit5Amt, Amount_rep = this.m_dataLoan.sOCredit5Amt_rep }
            };

            if (!this.m_dataLoan.sLoads1003LineLFromAdjustments)
            {
                // Dedupe against adjustments by both description and amount
                customCredits = (from credit in customCredits
                                 where !adjustments.Any(
                                     adjustment => string.Equals(adjustment.Description, credit.Description, StringComparison.OrdinalIgnoreCase)
                                         && adjustment.Amount == credit.Amount)
                                 select credit)
                                 .ToArray();
            }

            foreach (var credit in customCredits)
            {
                // If 1003 details are synced with adjustments, we only send Cash Deposits and Lender Credits.
                // Otherwise, no need to filter.
                if (!this.m_dataLoan.sLoads1003LineLFromAdjustments
                    || string.Equals(credit.Description, "Cash Deposit on sales contract", StringComparison.OrdinalIgnoreCase)
                    || string.Equals(credit.Description, "Lender Credit", StringComparison.OrdinalIgnoreCase))
                {
                    this.WritePurchaseCredit(credit.Amount_rep, credit.Description);
                }
            }
        }

        /// <summary>
        /// Writes a purchase credit element with the given information.
        /// </summary>
        /// <param name="amount">The credit amount.</param>
        /// <param name="type">The credit type.</param>
        /// <param name="source">The credit source.</param>
        private void WritePurchaseCredit(string amount, string type, string source)
        {
            //  <xs:element name="PURCHASE_CREDIT">
            //	    <xs:complexType>
            //          <xs:attribute name="_Amount" type="xs:string"/>
            //	        <xs:attribute name="_SourceType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="BorrowerPaidOutsideClosing"/>
            //                      <xs:enumeration value="PropertySeller"/>
            //                      <xs:enumeration value="Lender"/>
            //                      <xs:enumeration value="NonParentRelative"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="_Type">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="EarnestMoney"/>
            //                      <xs:enumeration value="RelocationFunds"/>
            //                      <xs:enumeration value="EmployerAssistedHousing"/>
            //                      <xs:enumeration value="LeasePurchaseFund"/>
            //                      <xs:enumeration value="Other"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //      </xs:complexType>
            //  </xs:element>
            if (string.IsNullOrEmpty(amount) || amount.Equals("$0.00") || amount.Equals("0.00"))
            {
                return;
            }

            m_xmlWriter.WriteStartElement("PURCHASE_CREDIT");
            WriteAttributeString("_Amount", amount);
            WriteAttributeString("_Type", type);

            if (!string.IsNullOrEmpty(source))
            {
                WriteAttributeString("_SourceType", source);
            }

            m_xmlWriter.WriteEndElement(); // </PURCHASE_CREDIT>
        }

        /// <summary>
        /// Writes a purchase credit element from an adjustment.
        /// </summary>
        /// <param name="amount">The credit amount.</param>
        /// <param name="typeEnum">The type of credit.</param>
        /// <param name="sourceEnum">The source of the credit.</param>
        private void WritePurchaseCredit(string amount, E_AdjustmentT typeEnum, E_PartyT sourceEnum)
        {
            string type;
            switch (typeEnum)
            {
                case E_AdjustmentT.EmployerAssistedHousing:
                    type = "EmployerAssistedHousing";
                    break;
                case E_AdjustmentT.LeasePurchaseFund:
                    type = "LeasePurchaseFund";
                    break;
                case E_AdjustmentT.RelocationFunds:
                    type = "RelocationFunds";
                    break;
                case E_AdjustmentT.SellerCredit:
                case E_AdjustmentT.Other:
                case E_AdjustmentT.Blank:
                case E_AdjustmentT.FuelCosts:
                case E_AdjustmentT.Grant:
                case E_AdjustmentT.ProceedsOfSubordinateLiens:
                case E_AdjustmentT.RebateCredit:
                case E_AdjustmentT.RentFromSubjectProperty:
                case E_AdjustmentT.RepairCompletionEscrowHoldback:
                case E_AdjustmentT.Repairs:
                case E_AdjustmentT.SatisfactionOfSubordinateLien:
                case E_AdjustmentT.SellersEscrowAssumption:
                case E_AdjustmentT.SellersMortgageInsuranceAssumption:
                case E_AdjustmentT.SellersReserveAccountAssumption:
                case E_AdjustmentT.Services:
                case E_AdjustmentT.SubordinateFinancingProceeds:
                case E_AdjustmentT.TenantSecurityDeposit:
                case E_AdjustmentT.TitlePremiumAdjustment:
                case E_AdjustmentT.TradeEquity:
                case E_AdjustmentT.UnpaidUtilityEscrowHoldback:
                case E_AdjustmentT.BuydownFund:
                case E_AdjustmentT.CommitmentOriginationFee:
                case E_AdjustmentT.FederalAgencyFundingFeeRefund:
                case E_AdjustmentT.GiftOfEquity:
                case E_AdjustmentT.MIPremiumRefund:
                case E_AdjustmentT.SweatEquity:
                case E_AdjustmentT.TradeEquityFromPropertySwap:
                case E_AdjustmentT.BorrowerPaidFees:
                case E_AdjustmentT.TradeEquityCredit:
                case E_AdjustmentT.Gift:
                case E_AdjustmentT.PrincipalReductionToReduceCashToBorrower:
                case E_AdjustmentT.PrincipalReductionToCureToleranceViolation:
                    type = "Other";
                    break;
                default:
                    throw new UnhandledEnumException(typeEnum);
            }

            string source;
            switch (sourceEnum)
            {
                case E_PartyT.Seller:
                    source = "PropertySeller";
                    break;
                case E_PartyT.Lender:
                    source = "Lender";
                    break;
                case E_PartyT.BorrowersRelative:
                    source = "NonParentRelative";
                    break;
                case E_PartyT.Blank:
                case E_PartyT.Borrower:
                case E_PartyT.BorrowersEmployer:
                case E_PartyT.BorrowersFriend:
                case E_PartyT.BorrowersParent:
                case E_PartyT.BuilderOrDeveloper:
                case E_PartyT.FederalAgency:
                case E_PartyT.RealEstateAgent:
                case E_PartyT.Other:
                    source = string.Empty;
                    break;
                default:
                    throw new UnhandledEnumException(sourceEnum);
            }

            this.WritePurchaseCredit(amount, type, source);
        }

        /// <summary>
        /// Writes a PurchaseCredit element from a 1003 detail line.
        /// </summary>
        /// <param name="amount">The credit amount.</param>
        /// <param name="description">The description of the credit.</param>
        private void WritePurchaseCredit(string amount, string description)
        {
            string type;
            if (string.Equals(description, "Cash Deposit on sales contract", StringComparison.OrdinalIgnoreCase))
            {
                type = "EarnestMoney";
            }
            else if (string.Equals(description, "Relocation Funds", StringComparison.OrdinalIgnoreCase))
            {
                type = "RelocationFunds";
            }
            else if (string.Equals(description, "Employer Assisted Housing", StringComparison.OrdinalIgnoreCase))
            {
                type = "EmployerAssistedHousing";
            }
            else if (string.Equals(description, "Lease Purchase Fund", StringComparison.OrdinalIgnoreCase))
            {
                type = "LeasePurchaseFund";
            }
            else
            {
                type = "Other";
            }

            string source;
            if (string.Equals(description, "Seller Credit", StringComparison.OrdinalIgnoreCase))
            {
                source =  "PropertySeller";
            }
            else if (string.Equals(description, "Lender Credit", StringComparison.OrdinalIgnoreCase))
            {
                source = "Lender";
            }
            else
            {
                source = string.Empty;
            }

            this.WritePurchaseCredit(amount, type, source);
        }


        private void WriteREOProperties()
        {
            //  <xs:element name="REO_PROPERTY">
            //	    <xs:complexType>
            //          <xs:attribute name="REO_ID" type="xs:ID"/>
            //          <xs:attribute name="BorrowerID" type="xs:IDREFS"/>
            //          <xs:attribute name="LiabilityID" type="xs:IDREFS"/>
            //          <xs:attribute name="_StreetAddress" type="xs:string"/>
            //          <xs:attribute name="_StreetAddress2" type="xs:string"/>
            //          <xs:attribute name="_City" type="xs:string"/>
            //          <xs:attribute name="_State" type="xs:string"/>
            //          <xs:attribute name="_PostalCode" type="xs:string"/>
            //	        <xs:attribute name="_CurrentResidenceIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="_DispositionStatusType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="PendingSale"/>
            //                      <xs:enumeration value="RetainForRental"/>
            //                      <xs:enumeration value="RetainForPrimaryOrSecondaryResidence"/>
            //                      <xs:enumeration value="Sold"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="_GSEPropertyType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="SingleFamily"/>
            //                      <xs:enumeration value="Condominium"/>
            //                      <xs:enumeration value="Townhouse"/>
            //                      <xs:enumeration value="Cooperative"/>
            //                      <xs:enumeration value="TwoToFourUnitProperty"/>
            //                      <xs:enumeration value="MultifamilyMoreThanFourUnits"/>
            //                      <xs:enumeration value="ManufacturedMobileHome"/>
            //                      <xs:enumeration value="CommercialNonResidential"/>
            //                      <xs:enumeration value="MixedUseResidential"/>
            //                      <xs:enumeration value="Farm"/>
            //                      <xs:enumeration value="HomeAndBusinessCombined"/>
            //                      <xs:enumeration value="Land"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_LienInstallmentAmount" type="xs:string"/>
            //          <xs:attribute name="_LienUPBAmount" type="xs:string"/>
            //          <xs:attribute name="_MaintenanceExpenseAmount" type="xs:string"/>
            //          <xs:attribute name="_MarketValueAmount" type="xs:string"/>
            //          <xs:attribute name="_RentalIncomeGrossAmount" type="xs:string"/>
            //          <xs:attribute name="_RentalIncomeNetAmount" type="xs:string"/>
            //	        <xs:attribute name="_SubjectIndicator" /> // NMTOKEN {Y, N}
            //      </xs:complexType>
            //  </xs:element>
            int nApps = m_dataLoan.nApps;
            for (int i = 0; i < nApps; i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                var subColl = dataApp.aReCollection.GetSubcollection(true, E_ReoGroupT.All);
                foreach (var item in subColl)
                {
                    var fields = (IRealEstateOwned)item;
                    m_xmlWriter.WriteStartElement("REO_PROPERTY");
                    WriteAttributeString("REO_ID", MakeReoID(fields.RecordId));
                    WriteAttributeString("BorrowerID", E_ReOwnerT.CoBorrower == fields.ReOwnerT ? dataApp.aCMismoId : dataApp.aBMismoId);

                    Guid[] matchedLiabilities = dataApp.FindMatchedLiabilities(fields.RecordId);
                    string str = "";

                    foreach (Guid id in matchedLiabilities)
                    {
                        str += MakeLiabilityID(id) + " ";
                    }
                    if (matchedLiabilities.Length > 0)
                        WriteAttributeString("LiabilityID", str.TrimWhitespaceAndBOM());

                    WriteAttributeString("_StreetAddress", fields.Addr);
                    WriteAttributeString("_City", fields.City);
                    WriteAttributeString("_State", fields.State);
                    WriteAttributeString("_PostalCode", fields.Zip);
                    WriteAttributeYN("_CurrentResidenceIndicator", fields.StatT == E_ReoStatusT.Residence);
                    WriteAttributeString("_DispositionStatusType", ReoDispositionMap.ToMismo(fields.StatT));
                    WriteAttributeString("_GSEPropertyType", REOGsePropertyTypeMap.ToMismo(fields.TypeT));
                    WriteAttributeString("_LienInstallmentAmount", fields.MPmt_rep);
                    WriteAttributeString("_LienUPBAmount", fields.MAmt_rep);
                    WriteAttributeString("_MaintenanceExpenseAmount", fields.HExp_rep);
                    WriteAttributeString("_MarketValueAmount", fields.Val_rep);
                    WriteAttributeString("_RentalIncomeGrossAmount", fields.GrossRentI_rep);
                    WriteAttributeString("_RentalIncomeNetAmount", fields.NetRentI_rep);
                    string sp = m_dataLoan.sSpAddr.ToUpper();
                    bool bIsSubjectProp = (sp.Length > 0) && (0 == sp.CompareTo(fields.Addr.ToUpper()));
                    WriteAttributeYN("_SubjectIndicator", bIsSubjectProp);

                    m_xmlWriter.WriteEndElement();
                }
            }
        }
        private void WriteBorrowers()
        {
            //  <xs:element name="BORROWER">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="_ALIAS" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="_MAIL_TO" minOccurs="0"/>
            //              <xs:element ref="_RESIDENCE" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="CURRENT_INCOME" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="DECLARATION" minOccurs="0"/>
            //              <xs:element ref="DEPENDENT" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="EMPLOYER" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="FHA_VA_BORROWER" minOccurs="0"/>
            //              <xs:element ref="GOVERNMENT_MONITORING" minOccurs="0"/>
            //              <xs:element ref="PRESENT_HOUSING_EXPENSE" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="SUMMARY" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="VA_BORROWER" minOccurs="0"/>
            //              <xs:element ref="FHA_BORROWER" minOccurs="0"/>
            //              <xs:element ref="_NEAREST_LIVING_RELATIVE" minOccurs="0"/>
            //              <xs:element ref="CONTACT_POINT" minOccurs="0" maxOccurs="unbounded"/>
            //          </xs:sequence>
            //          <xs:attribute name="BorrowerID" type="xs:ID"/>
            //          <xs:attribute name="JointAssetBorrowerID" type="xs:IDREF"/>
            //          <xs:attribute name="_FirstName" type="xs:string" use="required"/>
            //          <xs:attribute name="_MiddleName" type="xs:string"/>
            //          <xs:attribute name="_LastName" type="xs:string" use="required"/>
            //          <xs:attribute name="_NameSuffix" type="xs:string"/>
            //          <xs:attribute name="_AgeAtApplicationYears" type="xs:string"/>
            //          <xs:attribute name="_ApplicationSignedDate" type="xs:string"/>
            //          <xs:attribute name="_HomeTelephoneNumber" type="xs:string"/>
            //	        <xs:attribute name="_PrintPositionType" /> // NMTOKEN {Borrower, CoBorrower}
            //          <xs:attribute name="_SSN" type="xs:string" use="required"/>
            //          <xs:attribute name="_BirthDate" type="xs:string"/>
            //          <xs:attribute name="_UnparsedName" type="xs:string"/>
            //          <xs:attribute name="DependentCount" type="xs:string"/>
            //	        <xs:attribute name="JointAssetLiabilityReportingType" /> // NMTOKEN {Jointly, NotJointly}
            //	        <xs:attribute name="MaritalStatusType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Married"/>
            //                      <xs:enumeration value="NotProvided"/>
            //                      <xs:enumeration value="Separated"/>
            //                      <xs:enumeration value="Unknown"/>
            //                      <xs:enumeration value="Unmarried"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="SchoolingYears" type="xs:string"/>
            //          <xs:attribute name="CreditReportIdentifier" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

            int nApps = this.m_dataLoan.nApps;
            for (int appIndex = 0; appIndex < nApps; appIndex++)
            {
                CAppData dataApp = this.m_dataLoan.GetAppData(appIndex);

                if (!dataApp.aBIsValidNameSsn && !dataApp.aCIsValidNameSsn)
                {
                    continue;
                }

                if (dataApp.aBIsValidNameSsn)
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;
                    this.WriteBorrower(dataApp);
                }

                if (dataApp.aCIsValidNameSsn)
                {
                    dataApp.BorrowerModeT = E_BorrowerModeT.Coborrower;
                    this.WriteBorrower(dataApp);
                }
            }
        }

        /// <summary>
        /// Write a BORROWER block.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteBorrower(CAppData dataApp)
        {
            this.m_xmlWriter.WriteStartElement("BORROWER");

            this.WriteAttributeString("BorrowerID", dataApp.aMismoId);
            this.WriteAttributeString("JointAssetBorrowerID", dataApp.aSpouseMismoId);
            this.WriteAttributeString("_FirstName", dataApp.aFirstNm);
            this.WriteAttributeString("_MiddleName", dataApp.aMidNm);
            this.WriteAttributeString("_LastName", dataApp.aLastNm);
            this.WriteAttributeString("_NameSuffix", dataApp.aSuffix);
            this.WriteAttributeString("_AgeAtApplicationYears", dataApp.aAge_rep);
            this.WriteAttributeString("_ApplicationSignedDate", this.m_dataLoan.sAppSubmittedD_rep);
            this.WriteAttributeString("_HomeTelephoneNumber", dataApp.aHPhone);
            this.WriteAttributeString("_PrintPositionType", dataApp.BorrowerModeT == E_BorrowerModeT.Borrower ? "Borrower" : "CoBorrower");
            this.WriteAttributeString("_SSN", dataApp.aSsn);
            this.WriteAttributeString("_BirthDate", dataApp.aDob_rep);
            this.WriteAttributeString("_UnparsedName", dataApp.aNm);
            this.WriteAttributeString("DependentCount", dataApp.aDependNum_rep);
            this.WriteAttributeString("JointAssetLiabilityReportingType", dataApp.aAsstLiaCompletedNotJointly ? "NotJointly" : "Jointly");
            this.WriteAttributeString("MaritalStatusType", MaritalStatusMap.ToMismo(dataApp.aMaritalStatT));
            this.WriteAttributeString("SchoolingYears", dataApp.aSchoolYrs_rep);
            //WriteAttributeString( "CreditReportIdentifier", xxx);

            //<xs:element ref="_ALIAS" minOccurs="0" maxOccurs="unbounded" />

            this.WriteMailTo(dataApp);
            this.WriteResidences(dataApp);
            this.WriteCurrentIncomes(dataApp);
            this.WriteDeclaration(dataApp);

            string[] dependentAges = dataApp.aDependAges.Split(',', ';', ' ', '-', '&', '+', '~', ':');
            dependentAges.Where(age => !string.IsNullOrEmpty(age)).ToList().ForEach(age => this.WriteDependent(age));

            this.WriteEmployers(dataApp);

            ////<xs:element ref="FHA_VA_BORROWER" minOccurs="0" />

            this.WriteGovernmentMonitoring(dataApp);
            this.WritePresentHousingExpenses(dataApp);
            this.WriteSummaries(dataApp);

            ////<xs:element ref="VA_BORROWER" minOccurs="0" />
            ////<xs:element ref="FHA_BORROWER" minOccurs="0" />
            ////<xs:element ref="_NEAREST_LIVING_RELATIVE" minOccurs="0" />

            this.WriteContactPoints(dataApp);

            this.m_xmlWriter.WriteEndElement(); // </BORROWER>
        }

        /// <summary>
        /// Writes a _MAIL_TO block.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteMailTo(CAppData dataApp)
        {
            this.m_xmlWriter.WriteStartElement("_MAIL_TO");

            this.WriteAttributeString("_StreetAddress", dataApp.aAddrMail);
            //WriteAttributeString( "_StreetAddress2", xxx);
            this.WriteAttributeString("_City", dataApp.aCityMail);
            this.WriteAttributeString("_State", dataApp.aStateMail);
            this.WriteAttributeString("_PostalCode", dataApp.aZipMail);
            ////WriteAttributeString("_Country", "US");

            this.m_xmlWriter.WriteEndElement(); // </_MAIL_TO>
        }

        /// <summary>
        /// Writes the _RESIDENCE blocks for the current borrower.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteResidences(CAppData dataApp)
        {
            this.WriteResidence(dataApp.aAddr, dataApp.aCity, dataApp.aState, dataApp.aZip, dataApp.aAddrT, dataApp.aAddrYrs, true);
            this.WriteResidence(dataApp.aPrev1Addr, dataApp.aPrev1City, dataApp.aPrev1State, dataApp.aPrev1Zip, (E_aBAddrT)dataApp.aPrev1AddrT, dataApp.aPrev1AddrYrs, false);
            this.WriteResidence(dataApp.aPrev2Addr, dataApp.aPrev2City, dataApp.aPrev2State, dataApp.aPrev2Zip, (E_aBAddrT)dataApp.aPrev2AddrT, dataApp.aPrev2AddrYrs, false);
        }

        /// <summary>
        /// Writes a _RESIDENCE block with the given address data.
        /// </summary>
        /// <param name="streetAddress">The street address.</param>
        /// <param name="city">The address city.</param>
        /// <param name="state">The address state.</param>
        /// <param name="zip">The address zipcode.</param>
        /// <param name="addressType">The ownership type for the residence (own | rent | etc).</param>
        /// <param name="residencyYears">The number of years at the residence.</param>
        /// <param name="current">True if the residence is the current one given the context. False for a prior residence.</param>
        private void WriteResidence(string streetAddress, string city, string state, string zip, E_aBAddrT addressType, string residencyYears, bool current)
        {
            if (string.IsNullOrEmpty(streetAddress) && string.IsNullOrEmpty(city) && string.IsNullOrEmpty(state) && string.IsNullOrEmpty(zip))
            {
                return;
            }

            YearMonthParser parser = new YearMonthParser();
            parser.Parse(residencyYears);

            this.m_xmlWriter.WriteStartElement("_RESIDENCE");

            this.WriteAttributeString("_StreetAddress", streetAddress);
            this.WriteAttributeString("_City", city);
            this.WriteAttributeString("_State", state);
            this.WriteAttributeString("_PostalCode", zip);
            ////WriteAttributeString("_Country", "US");
            this.WriteAttributeString("BorrowerResidencyBasisType", AddressTypeMap.ToMismo(addressType));
            this.WriteAttributeString("BorrowerResidencyDurationMonths", parser.NumberOfMonths.ToString());
            this.WriteAttributeString("BorrowerResidencyDurationYears", parser.NumberOfYears.ToString());
            this.WriteAttributeString("BorrowerResidencyType", current ? "Current" : "Prior");

            this.m_xmlWriter.WriteEndElement(); // </_RESIDENCE>
        }

        /// <summary>
        /// Writes the CURRENT_INCOME blocks for the current borrower.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteCurrentIncomes(CAppData dataApp)
        {
            this.WriteCurrentIncome("Base", dataApp.aBaseI_rep);
            this.WriteCurrentIncome("Overtime", dataApp.aOvertimeI_rep);
            this.WriteCurrentIncome("Bonus", dataApp.aBonusesI_rep);
            this.WriteCurrentIncome("Commissions", dataApp.aCommisionI_rep);
            this.WriteCurrentIncome("DividendsInterest", dataApp.aDividendI_rep);
            this.WriteCurrentIncome("NetRentalIncome", dataApp.aNetRentI1003_rep);
            this.WriteCurrentIncome("SubjectPropertyNetCashFlow", dataApp.aSpPosCf_rep);

            foreach (var otherIncome in dataApp.aOtherIncomeList)
            {
                if ((otherIncome.IsForCoBorrower && dataApp.BorrowerModeT == E_BorrowerModeT.Coborrower) || (!otherIncome.IsForCoBorrower && dataApp.BorrowerModeT == E_BorrowerModeT.Borrower))
                {
                    this.WriteCurrentIncome("OtherTypesOfIncome", dataApp.m_convertLos.ToMoneyString(otherIncome.Amount, FormatDirection.ToRep));
                }
            }
        }

        /// <summary>
        /// Writes a CURRENT_INCOME block with the given income type and amount.
        /// </summary>
        /// <param name="type">The type of income.</param>
        /// <param name="monthlyAmount">The monthly amount of income.</param>
        private void WriteCurrentIncome(string type, string monthlyAmount)
        {
            if (this.ZeroAmount(monthlyAmount))
            {
                return;
            }

            this.m_xmlWriter.WriteStartElement("CURRENT_INCOME");

            this.WriteAttributeString("IncomeType", type);
            this.WriteAttributeString("_MonthlyTotalAmount", monthlyAmount);

            this.m_xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Writes the DECLARATION block for the current borrower.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteDeclaration(CAppData dataApp)
        {
            this.m_xmlWriter.WriteStartElement("DECLARATION");

            this.WriteAttributeString("AlimonyChildSupportObligationIndicator", dataApp.aDecAlimony);
            this.WriteAttributeString("BankruptcyIndicator", dataApp.aDecBankrupt);
            this.WriteAttributeString("BorrowedDownPaymentIndicator", dataApp.aDecBorrowing);

            if (dataApp.aDecCitizen.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                this.WriteAttributeString("CitizenshipResidencyType", "USCitizen");
            }
            else if (dataApp.aDecResidency.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                this.WriteAttributeString("CitizenshipResidencyType", "PermanentResidentAlien");
            }
            else
            {
                this.WriteAttributeString("CitizenshipResidencyType", "NonPermanentResidentAlien");
            }

            this.WriteAttributeString("CoMakerEndorserOfNoteIndicator", dataApp.aDecEndorser);

            if (dataApp.aDecPastOwnership.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                this.WriteAttributeYesNo("HomeownerPastThreeYearsType", true);
            }
            else if (dataApp.aDecPastOwnership.Equals("N", StringComparison.OrdinalIgnoreCase))
            {
                this.WriteAttributeYesNo("HomeownerPastThreeYearsType", false);
            }

            if (dataApp.aDecOcc.Equals("Y", StringComparison.OrdinalIgnoreCase))
            {
                this.WriteAttributeYesNo("IntentToOccupyType", true);
            }
            else if (dataApp.aDecOcc.Equals("N", StringComparison.OrdinalIgnoreCase))
            {
                this.WriteAttributeYesNo("IntentToOccupyType", false);
            }

            this.WriteAttributeString("LoanForeclosureOrJudgementIndicator", dataApp.aDecObligated);
            this.WriteAttributeString("OutstandingJudgementsIndicator", dataApp.aDecJudgment);
            this.WriteAttributeString("PartyToLawsuitIndicator", dataApp.aDecLawsuit);
            this.WriteAttributeString("PresentlyDelinquentIndicator", dataApp.aDecDelinquent);
            this.WriteAttributeString("PriorPropertyTitleType", PriorPropTitleTypeMap.ToMismo(dataApp.aDecPastOwnedPropTitleT));
            this.WriteAttributeString("PriorPropertyUsageType", PriorPropUsageTypeMap.ToMismo(dataApp.aDecPastOwnedPropT));
            this.WriteAttributeString("PropertyForeclosedPastSevenYearsIndicator", dataApp.aDecForeclosure);
            this.WriteAttributeYN("BorrowerFirstTimeHomebuyerIndicator", m_dataLoan.sHas1stTimeBuyer);

            this.m_xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Writes the DEPENDENT block for the current borrower.
        /// </summary>
        /// <param name="age">The age of the dependent in years.</param>
        private void WriteDependent(string age)
        {
            this.m_xmlWriter.WriteStartElement("DEPENDENT");

            this.WriteAttributeString("_AgeYears", age);

            this.m_xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Writes the EMPLOYER blocks for the current borrower.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteEmployers(CAppData dataApp)
        {
            IEmpCollection empCollection = dataApp.aEmpCollection;
            this.WriteEmployer(empCollection.GetPrimaryEmp(false));

            foreach (IRegularEmploymentRecord record in empCollection.GetSubcollection(true, E_EmpGroupT.Previous))
            {
                this.WriteEmployer(record);
            }
        }

        /// <summary>
        /// Writes an EMPLOYER block for the current borrower's primary employer.
        /// </summary>
        /// <param name="record">The primary employer record for the current borrower.</param>
        private void WriteEmployer(IPrimaryEmploymentRecord record)
        {
            if (record == null)
            {
                return;
            }

            this.m_xmlWriter.WriteStartElement("EMPLOYER");

            this.WriteAttributeString("_Name", record.EmplrNm);
            this.WriteAttributeString("_StreetAddress", record.EmplrAddr);
            this.WriteAttributeString("_City", record.EmplrCity);
            this.WriteAttributeString("_State", record.EmplrState);
            this.WriteAttributeString("_PostalCode", record.EmplrZip);
            this.WriteAttributeString("_TelephoneNumber", record.EmplrBusPhone);
            this.WriteAttributeString("CurrentEmploymentTimeInLineOfWorkYears", record.ProfLen_rep);
            this.WriteAttributeString("CurrentEmploymentMonthsOnJob", record.EmplmtLenInMonths_rep);
            this.WriteAttributeString("CurrentEmploymentYearsOnJob", record.EmplmtLenInYrs_rep);
            this.WriteAttributeYN("EmploymentBorrowerSelfEmployedIndicator", record.IsSelfEmplmt);
            this.WriteAttributeYN("EmploymentCurrentIndicator", true);
            this.WriteAttributeString("EmploymentPositionDescription", record.JobTitle);
            this.WriteAttributeYN("EmploymentPrimaryIndicator", true);
            ////WriteAttributeString("IncomeEmploymentMonthlyAmount", "");
            ////WriteAttributeString("PreviousEmploymentStartDate", "");
            ////WriteAttributeString("PreviousEmploymentEndDate", "");

            this.m_xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Writes an EMPLOYER block for the given prior employer of the current borrower.
        /// </summary>
        /// <param name="record">The employer record for a prior employer of the current borrower.</param>
        private void WriteEmployer(IRegularEmploymentRecord record)
        {
            if (record == null)
            {
                return;
            }

            this.m_xmlWriter.WriteStartElement("EMPLOYER");

            this.WriteAttributeString("_Name", record.EmplrNm);
            this.WriteAttributeString("_StreetAddress", record.EmplrAddr);
            this.WriteAttributeString("_City", record.EmplrCity);
            this.WriteAttributeString("_State", record.EmplrState);
            this.WriteAttributeString("_PostalCode", record.EmplrZip);
            this.WriteAttributeString("_TelephoneNumber", record.EmplrBusPhone);
            ////WriteAttributeString("CurrentEmploymentTimeInLineOfWorkYears", "");
            ////WriteAttributeString("CurrentEmploymentMonthsOnJob", "");
            ////WriteAttributeString("CurrentEmploymentYearsOnJob", "");
            this.WriteAttributeYN("EmploymentBorrowerSelfEmployedIndicator", record.IsSelfEmplmt);
            this.WriteAttributeYN("EmploymentCurrentIndicator", record.IsCurrent);
            this.WriteAttributeString("EmploymentPositionDescription", record.JobTitle);
            this.WriteAttributeYN("EmploymentPrimaryIndicator", record.IsPrimaryEmp);
            this.WriteAttributeString("IncomeEmploymentMonthlyAmount", record.MonI_rep);
            this.WriteAttributeString("PreviousEmploymentStartDate", record.EmplmtStartD_rep);
            this.WriteAttributeString("PreviousEmploymentEndDate", record.EmplmtEndD_rep);

            this.m_xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Writes the GOVERNMENT_MONITORING block for the current borrower.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteGovernmentMonitoring(CAppData dataApp)
        {
            this.m_xmlWriter.WriteStartElement("GOVERNMENT_MONITORING");

            this.WriteAttributeString("GenderType", GenderTypeMap.ToMismo(dataApp.aGenderFallback));
            this.WriteAttributeYN("RaceNationalOriginRefusalIndicator", dataApp.aNoFurnish);
            this.WriteAttributeString("RaceNationalOriginType", RaceTypeMap.ToMismo(dataApp.aRaceT));

            if (dataApp.aRaceT == E_aBRaceT.Other)
            {
                this.WriteAttributeString("OtherRaceNationalOriginDescription", dataApp.aORaceDesc);
            }

            this.WriteAttributeString("HMDAEthnicityType", HispanicTypeMap.ToMismo(dataApp.aHispanicTFallback));

            bool raceProvided = false;
            if ((dataApp.aIsAmericanIndian))
            {
                this.WriteHMDARace("AmericanIndianOrAlaskaNative");
                raceProvided = true;
            }

            if ((dataApp.aIsAsian))
            {
                this.WriteHMDARace("Asian");
                raceProvided = true;
            }

            if ((dataApp.aIsBlack))
            {
                this.WriteHMDARace("BlackOrAfricanAmerican");
                raceProvided = true;
            }

            if ((dataApp.aIsPacificIslander))
            {
                this.WriteHMDARace("NativeHawaiianOrOtherPacificIslander");
                raceProvided = true;
            }

            if ((dataApp.aIsWhite))
            {
                this.WriteHMDARace("White");
                raceProvided = true;
            }

            if (!raceProvided)
            {
                this.WriteHMDARace("InformationNotProvidedByApplicantInMailInternetOrTelephoneApplication");
            }

            this.m_xmlWriter.WriteEndElement(); // </GOVERNMENT_MONITORING>
        }

        /// <summary>
        /// Writes a HMDA_RACE with the given race.
        /// </summary>
        /// <param name="race">The race to write.</param>
        private void WriteHMDARace(string race)
        {
            this.m_xmlWriter.WriteStartElement("HMDA_RACE");

            this.WriteAttributeString("_Type", race);

            this.m_xmlWriter.WriteEndElement(); // </HMDA_RACE>
        }

        /// <summary>
        /// Writes the PRESENT_HOUSING_EXPENSE blocks for the current borrower.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WritePresentHousingExpenses(CAppData dataApp)
        {
            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                // Only add present housing for Borrower.
                this.WritePresentHousingExpense("Rent", dataApp.aPresRent_rep);
                this.WritePresentHousingExpense("FirstMortgagePrincipalAndInterest", dataApp.aPres1stM_rep);
                this.WritePresentHousingExpense("OtherMortgageLoanPrincipalAndInterest", dataApp.aPresOFin_rep);
                this.WritePresentHousingExpense("HazardInsurance", dataApp.aPresHazIns_rep);
                this.WritePresentHousingExpense("RealEstateTax", dataApp.aPresRealETx_rep);
                this.WritePresentHousingExpense("MI", dataApp.aPresMIns_rep);
                this.WritePresentHousingExpense("HomeownersAssociationDuesAndCondominiumFees", dataApp.aPresHoAssocDues_rep);
                this.WritePresentHousingExpense("OtherHousingExpense", dataApp.aPresOHExp_rep);
            }
        }

        /// <summary>
        /// Write a PRESENT_HOUSING_EXPENSE block for the expense of the given type and monthly amount.
        /// </summary>
        /// <param name="type">The type of housing expense.</param>
        /// <param name="amount">The monthly amount of the expense.</param>
        private void WritePresentHousingExpense(string type, string amount)
        {
            if (this.ZeroAmount(amount))
            {
                return;
            }

            this.m_xmlWriter.WriteStartElement("PRESENT_HOUSING_EXPENSE");

            this.WriteAttributeString("HousingExpenseType", type);
            this.WriteAttributeString("_PaymentAmount", amount);

            this.m_xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Writes the SUMMARY blocks for the current borrower.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteSummaries(CAppData dataApp)
        {
            this.WriteSummary("TotalMonthlyIncomeNotIncludingNetRentalIncome", dataApp.aMonthlyINotIncludingNetRentalI_rep);
            //this.WriteSummary("SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensBalance", "");

            if (dataApp.BorrowerModeT == E_BorrowerModeT.Borrower)
            {
                this.WriteSummary("TotalPresentHousingExpense", dataApp.aPresTotHExp_rep);
            }

            this.WriteSummary("TotalLiabilitesBalance", dataApp.aLiaBal_rep);
            this.WriteSummary("SubtotalLiabilitesMonthlyPayment", dataApp.aLiaMonTot_Borr_rep);
            this.WriteSummary("SubtotalOmittedLiabilitesBalance", dataApp.aOmittedLiaBal_rep);
            this.WriteSummary("SubtotalOmittedLiabilitiesMonthlyPayment", dataApp.aOmittedLiaMonPmt_rep);
            ////this.WriteSummary("SubtotalLiabilitiesPaidByClosingNotIncludingSubjectPropertyLiensMonthlyPayment",	"");
            ////this.WriteSummary("SubtotalResubordinatedLiabilitesMonthlyPaymentForSubjectProperty", "");
            ////this.WriteSummary("SubtotalSubjectPropertyLiensPaidByClosingBalance", "");
            ////this.WriteSummary("SubtotalSubjectPropertyLiensPaidByClosingMonthlyPayment", "");
            ////this.WriteSummary("SubtotalLiabilitiesForRentalPropertyBalance", "");
            ////this.WriteSummary("SubtotalLiabilitiesForRentalPropertyMonthlyPayment", "");
            ////this.WriteSummary("SubtotalResubordinatedLiabilitiesBalanceForSubjectProperty", "");
            this.WriteSummary("SubtotalLiquidAssetsNotIncludingGift", dataApp.aLiquidAssetsNotIncludingGift_rep);
            this.WriteSummary("SubtotalNonLiquidAssets", dataApp.aIlliquidAssets_rep);
        }

        /// <summary>
        /// Write a SUMMARY block with the given type and monthly amount.
        /// </summary>
        /// <param name="type">The type of the dollar amount.</param>
        /// <param name="amount">The monthly dollar amount.</param>
        private void WriteSummary(string type, string amount)
        {
            if (this.ZeroAmount(amount))
            {
                return;
            }

            this.m_xmlWriter.WriteStartElement("SUMMARY");

            this.m_xmlWriter.WriteAttributeString("_AmountType", type);
            this.m_xmlWriter.WriteAttributeString("_Amount", amount);

            this.m_xmlWriter.WriteEndElement();
        }

        /// <summary>
        /// Writes the CONTACT_POINT blocks for the current borrower.
        /// </summary>
        /// <param name="dataApp">The CAppData object containing the borrower data.</param>
        private void WriteContactPoints(CAppData dataApp)
        {
            this.WriteContactPoint("Work", "Phone", dataApp.aBusPhone);
            this.WriteContactPoint("Mobile", "Phone", dataApp.aCellPhone);
            this.WriteContactPoint("Home", "Fax", dataApp.aFax);
            this.WriteContactPoint("Home", "Email", dataApp.aEmail);
            this.WriteContactPoint("Home", "Phone", dataApp.aHPhone);
        }

        /// <summary>
        /// Writes a CONTACT_POINT block with the given contact information.
        /// </summary>
        /// <param name="environment">The type of environment corresponding to the contact (Work | Home | Mobile).</param>
        /// <param name="type">The contact point type (Phone | Fax | Email).</param>
        /// <param name="value">The phone/fax/mobile number or email address.</param>
        private void WriteContactPoint(string environment, string type, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return;
            }

            this.m_xmlWriter.WriteStartElement("CONTACT_POINT");

            this.WriteAttributeString("_RoleType", environment);
            this.WriteAttributeString("_Type", type);
            this.WriteAttributeString("_Value", value);

            this.m_xmlWriter.WriteEndElement();
        }

        private void WriteEscrow()
        {
            //  <xs:element name="ESCROW">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="_UNEQUAL_PAYMENTS" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="_PAYMENTS" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="_PAID_TO" minOccurs="0"/>
            //          </xs:sequence>
            //          <xs:attribute name="_AnnualPaymentAmount" type="xs:string"/>
            //          <xs:attribute name="_CollectedNumberOfMonthsCount" type="xs:string"/>
            //          <xs:attribute name="_DueDate" type="xs:string"/>
            //	        <xs:attribute name="_ItemType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Assessment"/>
            //                      <xs:enumeration value="CityPropertyTax"/>
            //                      <xs:enumeration value="CountyPropertyTax"/>
            //                      <xs:enumeration value="EarthquakeInsurance"/>
            //                      <xs:enumeration value="FloodInsurance"/>
            //                      <xs:enumeration value="HazardInsurance"/>
            //                      <xs:enumeration value="Other"/>
            //                      <xs:enumeration value="SchoolPropertyTax"/>
            //                      <xs:enumeration value="TownPropertyTax"/>
            //                      <xs:enumeration value="VillagePropertyTax"/>
            //                      <xs:enumeration value="WindstormInsurance"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="_PaidByType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Buyer"/>
            //                      <xs:enumeration value="LenderPremium"/>
            //                      <xs:enumeration value="Seller"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_PaidToName" type="xs:string"/>
            //	        <xs:attribute name="_PaymentFrequencyType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Annual"/>
            //                      <xs:enumeration value="Monthly"/>
            //                      <xs:enumeration value="Quarterly"/>
            //                      <xs:enumeration value="SemiAnnual"/>
            //                      <xs:enumeration value="Unequal"/>
            //                      <xs:enumeration value="Other"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_PaymentFrequencyTypeOtherDescription" type="xs:string"/>
            //          <xs:attribute name="_PremiumAmount" type="xs:string"/>
            //	        <xs:attribute name="_PremiumPaidByType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Buyer"/>
            //                      <xs:enumeration value="Lender"/>
            //                      <xs:enumeration value="Seller"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="_PremiumPaymentType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="CollectAtClosing"/>
            //                      <xs:enumeration value="PaidOutsideOfClosing"/>
            //                      <xs:enumeration value="Waived"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_ItemTypeOtherDescription" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>
        }
        private void WriteRESPAFee()
        {
            //  <xs:element name="RESPA_FEE">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="_PAYMENT" maxOccurs="unbounded"/>
            //              <xs:element ref="_REQUIRED_SERVICE_PROVIDER" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="_PAID_TO" minOccurs="0"/>
            //          </xs:sequence>
            //	        <xs:attribute name="RESPASectionClassificationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="100:GrossAmountDueFromBorrower"/>
            //                      <xs:enumeration value="1000:ReservesDepositedWithLender"/>
            //                      <xs:enumeration value="1100:TitleCharges"/>
            //                      <xs:enumeration value="1200:RecordingAndTransferCharges"/>
            //                      <xs:enumeration value="1300:AdditionalSettlementFees"/>
            //                      <xs:enumeration value="200:AmountsPaidByOrInBehalfOfBorrower"/>
            //                      <xs:enumeration value="300:CashAtSettlementFromToBorrower"/>
            //                      <xs:enumeration value="400:GrossAmountDueToSeller"/>
            //                      <xs:enumeration value="500:ReductionsInAmountDueToSeller"/>
            //                      <xs:enumeration value="600:CashAtSettlementToFromSeller"/>
            //                      <xs:enumeration value="700:DivisionOfCommission"/>
            //                      <xs:enumeration value="800:LoanFees"/>
            //                      <xs:enumeration value="900:RequiredLenderPaidInAdvance"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_PaidToName" type="xs:string"/>
            //	        <xs:attribute name="_RequiredProviderOfServiceIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="_ResponsiblePartyType" /> // NMTOKEN {Buyer, Seller}
            //          <xs:attribute name="_SpecifiedHUDLineNumber" type="xs:string"/>
            //	        <xs:attribute name="_Type">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="203KDiscountOnRepairs"/>
            //                      <xs:enumeration value="203KPermits"/>
            //                      <xs:enumeration value="203KArchitecturalAndEngineeringFee"/>
            //                      <xs:enumeration value="203KInspectionFee"/>
            //                      <xs:enumeration value="203KSupplementalOriginationFee"/>
            //                      <xs:enumeration value="203KConsultantFee"/>
            //                      <xs:enumeration value="203KTitleUpdate"/>
            //                      <xs:enumeration value="AbstractOrTitleSearchFee"/>
            //                      <xs:enumeration value="AmortizationFee"/>
            //                      <xs:enumeration value="ApplicationFee"/>
            //                      <xs:enumeration value="AppraisalFee"/>
            //                      <xs:enumeration value="AssignmentFee"/>
            //                      <xs:enumeration value="AssignmentRecordingFee"/>
            //                      <xs:enumeration value="AssumptionFee"/>
            //                      <xs:enumeration value="AttorneyFee"/>
            //                      <xs:enumeration value="BondReviewFee"/>
            //                      <xs:enumeration value="CityCountyDeedTaxStampFee"/>
            //                      <xs:enumeration value="CityCountyMortgageTaxStampFee"/>
            //                      <xs:enumeration value="CLOAccessFee"/>
            //                      <xs:enumeration value="CommitmentFee"/>
            //                      <xs:enumeration value="CopyFaxFee"/>
            //                      <xs:enumeration value="CourierFee"/>
            //                      <xs:enumeration value="CreditReportFee"/>
            //                      <xs:enumeration value="DeedRecordingFee"/>
            //                      <xs:enumeration value="DocumentPreparationFee"/>
            //                      <xs:enumeration value="DocumentaryStampFee"/>
            //                      <xs:enumeration value="EscrowWaiverFee"/>
            //                      <xs:enumeration value="FloodCertification"/>
            //                      <xs:enumeration value="GeneralCounselFee"/>
            //                      <xs:enumeration value="InspectionFee"/>
            //                      <xs:enumeration value="LoanDiscountPoints"/>
            //                      <xs:enumeration value="LoanOriginationFee"/>
            //                      <xs:enumeration value="ModificationFee"/>
            //                      <xs:enumeration value="MortgageBrokerFee"/>
            //                      <xs:enumeration value="MortgageRecordingFee"/>
            //                      <xs:enumeration value="MunicipalLienCertificateFee"/>
            //                      <xs:enumeration value="MunicipalLienCertificateRecordingFee"/>
            //                      <xs:enumeration value="NewLoanAdministrationFee"/>
            //                      <xs:enumeration value="NotaryFee"/>
            //                      <xs:enumeration value="Other"/>
            //                      <xs:enumeration value="PestInspectionFee"/>
            //                      <xs:enumeration value="ProcessingFee"/>
            //                      <xs:enumeration value="RedrawFee"/>
            //                      <xs:enumeration value="ReinspectionFee"/>
            //                      <xs:enumeration value="ReleaseRecordingFee"/>
            //                      <xs:enumeration value="RuralHousingFee"/>
            //                      <xs:enumeration value="SettlementOrClosingFee"/>
            //                      <xs:enumeration value="StateDeedTaxStampFee"/>
            //                      <xs:enumeration value="StateMortgageTaxStampFee"/>
            //                      <xs:enumeration value="SurveyFee"/>
            //                      <xs:enumeration value="TaxRelatedServiceFee"/>
            //                      <xs:enumeration value="TitleExaminationFee"/>
            //                      <xs:enumeration value="TitleInsuranceBinderFee"/>
            //                      <xs:enumeration value="TitleInsuranceFee"/>
            //                      <xs:enumeration value="UnderwritingFee"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="_TypeOtherDescription" type="xs:string"/>
            //	        <xs:attribute name="_PaidToType" /> // NMTOKEN {Broker, Lender, Investor, Other}
            //          <xs:attribute name="_PaidToTypeOtherDescription" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

        }
        private void WriteMIData()
        {
            //  <xs:element name="MI_DATA">
            //	    <xs:complexType>
            //	        <xs:sequence>
            //              <xs:element ref="MI_RENEWAL_PREMIUM" minOccurs="0" maxOccurs="unbounded"/>
            //              <xs:element ref="MI_PREMIUM_TAX" minOccurs="0" maxOccurs="unbounded"/>
            //          </xs:sequence>
            //	        <xs:attribute name="MIPremiumFinancedIndicator" /> // NMTOKEN {Y, N}
            //	        <xs:attribute name="MIPremiumPaymentType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="BorrowerPaid"/>
            //                      <xs:enumeration value="BothBorrowerAndLenderPaid"/>
            //                      <xs:enumeration value="LenderPaid"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="MIPremiumRefundableType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="NotRefundable"/>
            //                      <xs:enumeration value="Refundable"/>
            //                      <xs:enumeration value="RefundableWithLimits"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="MIPremiumTermMonths" type="xs:string"/>
            //	        <xs:attribute name="MIRenewalCalculationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Constant"/>
            //                      <xs:enumeration value="Declining"/>
            //                      <xs:enumeration value="NoRenewals"/>
            //                      <xs:enumeration value="NotApplicable"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //	        <xs:attribute name="MIDurationType">
            //	            <xs:simpleType>
            //	                <xs:restriction base="xs:NMTOKEN">
            //                      <xs:enumeration value="Annual"/>
            //                      <xs:enumeration value="NotApplicable"/>
            //                      <xs:enumeration value="PeriodicMonthly"/>
            //                      <xs:enumeration value="SingleLifeOfLoan"/>
            //                      <xs:enumeration value="SingleSpecific"/>
            //                  </xs:restriction>
            //              </xs:simpleType>
            //          </xs:attribute>
            //          <xs:attribute name="MIInitialPremiumRateDurationMonths" type="xs:string"/>
            //          <xs:attribute name="MIInitialPremiumRatePercent" type="xs:string"/>
            //	        <xs:attribute name="MIEscrowIncludedInAggregateIndicator" /> // NMTOKEN {Y, N}
            //          <xs:attribute name="MI_LTVCutoffPercent" type="xs:string"/>
            //	        <xs:attribute name="MI_LTVCutoffType" /> // NMTOKEN {AppraisedValue, SalesPrice}
            //          <xs:attribute name="MICollectedNumberOfMonthsCount" type="xs:string"/>
            //          <xs:attribute name="MICushionNumberOfMonthsCount" type="xs:string"/>
            //      </xs:complexType>
            //  </xs:element>

        }
        private void WriteDisclosureData()
        {
            // 5/4/2004 dd - TODO

        }

        /// <summary>
        /// Write an attribute with the given name and value, iff the value is nonempty.
        /// </summary>
        /// <param name="localName">The attribute name.</param>
        /// <param name="value">The attribute value.</param>
        private void WriteAttributeString(string localName, string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                this.m_xmlWriter.WriteAttributeString(localName, value);
            }
        }

        /// <summary>
        /// Writes an attribute with Y | N value based on the given boolean.
        /// </summary>
        /// <param name="localName">The attribute name.</param>
        /// <param name="value">The attribute value as a boolean.</param>
        private void WriteAttributeYN(string localName, bool value)
        {
            this.m_xmlWriter.WriteAttributeString(localName, value ? "Y" : "N");
        }

        /// <summary>
        /// Writes an attribute with Yes | No value based on the given boolean.
        /// </summary>
        /// <param name="localName">The attribute name.</param>
        /// <param name="value">The attribute value as a boolean.</param>
        private void WriteAttributeYesNo(string localName, bool value)
        {
            this.m_xmlWriter.WriteAttributeString(localName, value ? "Yes" : "No");
        }

        /// <summary>
        /// Indicates whether the given dollar amount is 0 or empty.
        /// </summary>
        /// <param name="amount">The dollar amount as a string.</param>
        /// <returns>True if the amount is null, empty, or equal to 0. False otherwise.</returns>
        private bool ZeroAmount(string amount)
        {
            return (string.IsNullOrEmpty(amount) || amount == "$0.00" || amount == "0.00");
        }

        private string MakeLiabilityID(Guid id)
        {
            if (Guid.Empty == id)
                return "";

            return "LIA" + id.ToString("N");
        }
        private string MakeReoID(Guid id)
        {
            if (Guid.Empty == id)
                return "";

            return "REO" + id.ToString("N");

        }

        private class Buydown
        {
            #region Private Member Variables
            private int durationMonths = 0;
            private decimal increaseRatePercent = 0.0M;
            private int changeFrequencyMonths = 0;
            #endregion

            #region Public Properties
            public int ChangeFrequencyMonths
            {
                get { return changeFrequencyMonths; }
                set { changeFrequencyMonths = value; }
            }
            public int DurationMonths
            {
                get { return durationMonths; }
                set { durationMonths = value; }
            }
            public decimal IncreaseRatePercent
            {
                get { return increaseRatePercent; }
                set { increaseRatePercent = value; }
            }
            #endregion
        }

        /// <summary>
        /// Generates new Condensed Buydown List. Case 238101 
        /// </summary>
        /// <param name="dataLoan">loan data</param>
        /// <returns>A list of Buydown object populated with calculated increasedRate, changeFrequencyMonths, and durationMonths</returns>
        private List<Buydown> GenerateCondensedBuydown(CPageData dataLoan)
        {
            var buydownRows = new[]
                    {
                        //// R = Rate Reduction, Mon = Term (mths)
                        new { R = dataLoan.sBuydwnR1, R_rep = dataLoan.sBuydwnR1_rep, Mon = dataLoan.sBuydwnMon1, Mon_rep = dataLoan.sBuydwnMon1_rep, Num = "1"},
                        new { R = dataLoan.sBuydwnR2, R_rep = dataLoan.sBuydwnR2_rep, Mon = dataLoan.sBuydwnMon2, Mon_rep = dataLoan.sBuydwnMon2_rep, Num = "2"},
                        new { R = dataLoan.sBuydwnR3, R_rep = dataLoan.sBuydwnR3_rep, Mon = dataLoan.sBuydwnMon3, Mon_rep = dataLoan.sBuydwnMon3_rep, Num = "3"},
                        new { R = dataLoan.sBuydwnR4, R_rep = dataLoan.sBuydwnR4_rep, Mon = dataLoan.sBuydwnMon4, Mon_rep = dataLoan.sBuydwnMon4_rep, Num = "4"},
                        new { R = dataLoan.sBuydwnR5, R_rep = dataLoan.sBuydwnR5_rep, Mon = dataLoan.sBuydwnMon5, Mon_rep = dataLoan.sBuydwnMon5_rep, Num = "5"},
                    };

            //// Get nonempty rows. A row is nonzero if both fields have a nonzero value.
            var buydownRowsFiltered = buydownRows.Where((row) => row.R != 0 && row.Mon != 0);

            Buydown buydownResult = new Buydown();
            List<Buydown> buydownResultList = new List<Buydown>();

            if (buydownRowsFiltered.Count() > 1)
            {
                buydownResult.IncreaseRatePercent = buydownRowsFiltered.First().R - buydownRowsFiltered.ElementAt(1).R;
                buydownResult.ChangeFrequencyMonths = buydownRowsFiltered.First().Mon;
                buydownResult.DurationMonths = buydownRowsFiltered.First().Mon;

                for (int i = 1; i < buydownRowsFiltered.Count(); i++)
                {
                    if (i != buydownRowsFiltered.Count() - 1)
                    {
                        if ((buydownResult.IncreaseRatePercent != (buydownRowsFiltered.ElementAt(i).R - buydownRowsFiltered.ElementAt(i + 1).R))
                            || (buydownResult.ChangeFrequencyMonths != buydownRowsFiltered.ElementAt(i).Mon))
                        {
                            buydownResultList.Add(buydownResult);

                            buydownResult = new Buydown();
                            buydownResult.IncreaseRatePercent = buydownRowsFiltered.ElementAt(i).R - buydownRowsFiltered.ElementAt(i + 1).R;
                            buydownResult.ChangeFrequencyMonths = buydownRowsFiltered.ElementAt(i).Mon;
                            buydownResult.DurationMonths = buydownRowsFiltered.ElementAt(i).Mon;
                        }
                        else
                        {
                            buydownResult.DurationMonths += buydownRowsFiltered.ElementAt(i).Mon;
                        }
                    }
                    else
                    {
                        if (buydownResult.IncreaseRatePercent != buydownRowsFiltered.ElementAt(i).R
                            || buydownResult.ChangeFrequencyMonths != buydownRowsFiltered.ElementAt(i).Mon)
                        {
                            buydownResultList.Add(buydownResult);

                            buydownResult = new Buydown();
                            buydownResult.IncreaseRatePercent = buydownRowsFiltered.ElementAt(i).R;
                            buydownResult.ChangeFrequencyMonths = buydownRowsFiltered.ElementAt(i).Mon;
                            buydownResult.DurationMonths = buydownRowsFiltered.ElementAt(i).Mon;
                        }
                        else
                        {
                            buydownResult.DurationMonths += buydownRowsFiltered.ElementAt(i).Mon;
                        }
                        buydownResultList.Add(buydownResult);
                    }
                }
            }
            else if (buydownRowsFiltered.Count() == 1)
            {
                buydownResult.IncreaseRatePercent = buydownRowsFiltered.First().R;
                buydownResult.ChangeFrequencyMonths = buydownRowsFiltered.First().Mon;
                buydownResult.DurationMonths = buydownRowsFiltered.First().Mon;

                buydownResultList.Add(buydownResult);
            }

            return buydownResultList;
        }
    }
}
