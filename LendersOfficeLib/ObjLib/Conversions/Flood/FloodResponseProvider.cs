﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Conversions.FloodOrder.Response;
using System.Xml;
using System.Xml.Serialization;

namespace LendersOffice.Conversions.FloodOrder
{
    /// <summary>
    /// Creates and populates a Flood response.
    /// </summary>
    public class FloodResponseProvider
    {
        private FloodResponse m_response;

        public FloodResponse CreateResponse(XmlReader reader)
        {
            XmlSerializer deserializer = new XmlSerializer(typeof(FloodResponse));
            
            m_response = (FloodResponse) deserializer.Deserialize(reader);

            return m_response;
        }

    }
}
