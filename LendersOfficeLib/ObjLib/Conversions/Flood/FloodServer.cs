﻿namespace LendersOffice.Conversions.FloodOrder
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Serialization;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Conversions.FloodOrder.Request;
    using LendersOffice.Conversions.FloodOrder.Response;

    /// <summary>
    /// Sends a Flood request and obtains a response.
    /// </summary>
    public static class FloodServer
    {
        /// <summary>
        /// The Flood integration log context.
        /// </summary>
        private const string LOG_CONTEXT = "Flood";

        /// <summary>
        /// The ID for MCL's beta flood URL.
        /// </summary>
        private static Guid FLOOD_BETA_SERVER_ID = new Guid("FD2D106B-50D6-40AA-BBA6-EB8B7A3A712D"); // https://demo.mortgagecreditlink.com/inetapi/get_mismo_flood.aspx

        /// <summary>
        /// The default ID for Certified Credit's flood URL.
        /// </summary>
        private static Guid FLOOD_PRODUCTION_CERTIFIED_CREDIT_ID = new Guid("3129093C-2D20-44EA-BF6E-3CF0BED46CB0"); //https://certifiedcredit.meridianlink.com/inetapi/get_mismo_flood.aspx

        /// <summary>
        /// The default ID for MCL's production flood URL.
        /// </summary>
        private static Guid FLOOD_PRODUCTION_SERVER_ID = new Guid("114BD009-2D3E-4C99-8411-66872869D728"); //https://credit.meridianlink.com/inetapi/get_mismo_flood.aspx

        /// <summary>
        /// Generates a flood request.
        /// </summary>
        /// <param name="sLogin">The flood login.</param>
        /// <param name="sPassword">The flood password.</param>
        /// <param name="sLId">The loan ID.</param>
        /// <returns>A flood request.</returns>
        public static FloodRequest CreateRequest(string sLogin, string sPassword, Guid sLId)
        {
            FloodRequestProvider provider = new FloodRequestProvider(sLogin, sPassword, sLId);
            return provider.CreateRequest();
        }

        /// <summary>
        /// Generates a flood request for a pending report.
        /// </summary>
        /// <param name="sLogin">The flood login.</param>
        /// <param name="sPassword">The flood password.</param>
        /// <param name="sLId">The loan ID.</param>
        /// <returns>A flood request.</returns>
        public static FloodRequest CreateRequestForPendingReport(string sLogin, string sPassword, Guid sLId)
        {
            FloodRequestProvider provider = new FloodRequestProvider(sLogin, sPassword, sLId);
            return provider.CreateRequestForPendingReport();
        }

        /// <summary>
        /// Submits a flood request and obtains a response.
        /// </summary>
        /// <param name="request">The request to transmit.</param>
        /// <param name="floodProvider">The flood provider.</param>
        /// <param name="sResponseXml">The response from the vendor.</param>
        /// <param name="BrokerID">The broker ID.</param>
        /// <returns>a flood response.</returns>
        public static FloodResponse Submit(FloodRequest request, E_FloodCertificationPreparerT floodProvider, out string sResponseXml, Guid BrokerID)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(FloodRequest));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            byte[] bytes = null;
            using (MemoryStream stream = new MemoryStream(50000))
            {
                using (XmlTextWriter xmlTextWriter = new XmlTextWriter(stream, Encoding.ASCII))
                {
                    serializer.Serialize(xmlTextWriter, request, ns);
                }
                bytes = stream.ToArray();
            }

            if (Tools.IsLogLargeRequest(bytes.Length))
            {
                string sRequestString = Encoding.ASCII.GetString(bytes);
                sRequestString = Regex.Replace(sRequestString, "Password=\"[^\"]*?\"", "Password=\"*****\""); // mask the password
                Tools.LogInfo("FloodServerRequest", sRequestString);
            }
            else
            {
                Tools.LogInfo("FloodServerRequest", "Request size is " + bytes.Length + " bytes and it exceeding threshold for logging.");
            }

            string url = FloodServiceURL(floodProvider, BrokerID);
            Tools.LogInfo("FloodServer", "Url=" + url);
            // Submit to MCL
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Method = "POST";
            webRequest.ContentType = "text/xml";
            webRequest.ContentLength = bytes.Length;
            webRequest.Timeout = 60000;

            using (Stream stream = webRequest.GetRequestStream())
            {
                stream.Write(bytes, 0, bytes.Length);
            }

            // Get the response
            StringBuilder sb = new StringBuilder();
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    byte[] buffer = new byte[60000];
                    int iSize = stream.Read(buffer, 0, buffer.Length);
                    while (iSize > 0)
                    {
                        sb.Append(System.Text.Encoding.ASCII.GetString(buffer, 0, iSize));
                        iSize = stream.Read(buffer, 0, buffer.Length);
                    }
                }
            }
            sResponseXml = sb.ToString();

            if (Tools.IsLogLargeRequest(sResponseXml.Length))
            {
                Tools.LogInfo("FloodServerResponse", sResponseXml);
            }
            else
            {
                Tools.LogInfo("FloodServerResponse", "Response size is " + bytes.Length + " bytes and it exceeding threshold for logging.");
            }

            // Convert to object
            FloodResponse floodResponse = null;

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings();
            xmlReaderSettings.XmlResolver = null;
            xmlReaderSettings.DtdProcessing = DtdProcessing.Parse;

            try
            {
                using (XmlReader xmlReader = XmlReader.Create(new StringReader(sResponseXml), xmlReaderSettings))
                {
                    // Deserialize to object
                    FloodResponseProvider responseProvider = new FloodResponseProvider();
                    floodResponse = responseProvider.CreateResponse(xmlReader);
                }
            }
            catch (InvalidOperationException) // Could not read the XML
            {
                Tools.LogErrorWithCriticalTracking(ErrorMessages.Flood.UnrecognizedResponse + Environment.NewLine + "=== FLOOD RESPONSE ===" + Environment.NewLine + sResponseXml);
                floodResponse = null;
            }

            return floodResponse;
        }

        /// <summary>
        /// Determines the URL to transmit to.
        /// </summary>
        /// <param name="floodProvider">The flood provider.</param>
        /// <param name="BrokerID">The broker ID, to determine whether the client has a reseller configured.</param>
        /// <returns>The appropriate URL to transmit to.</returns>
        private static string FloodServiceURL(E_FloodCertificationPreparerT floodProvider, Guid BrokerID)
        {
            Guid resellerId = FLOOD_BETA_SERVER_ID;
            if (ConstAppDavid.CurrentServerLocation == ServerLocation.Production)
            {
                var brokerDB = Admin.BrokerDB.RetrieveById(BrokerID);
                if (brokerDB.FloodResellerId != Guid.Empty)
                {
                    resellerId = brokerDB.FloodResellerId;
                }
                else
                {
                    switch (floodProvider)
                    {
                    case E_FloodCertificationPreparerT.CertifiedCredit:
                        resellerId = FLOOD_PRODUCTION_CERTIFIED_CREDIT_ID;
                        break;

                    case E_FloodCertificationPreparerT.ServiceLink:
                    case E_FloodCertificationPreparerT.CBCInnovis_FZDS: // 11/20/2015 BS - Case 228701
                    case E_FloodCertificationPreparerT.ChicagoTitle:
                    case E_FloodCertificationPreparerT.Fidelity:
                    case E_FloodCertificationPreparerT.KrollFactualData:
                    case E_FloodCertificationPreparerT.LandSafe:
                    case E_FloodCertificationPreparerT.Leretta:
                    case E_FloodCertificationPreparerT.MDA:
                    case E_FloodCertificationPreparerT.Other:
                    case E_FloodCertificationPreparerT.USFlood:
                    default:
                        resellerId = FLOOD_PRODUCTION_SERVER_ID;
                        break;
                    }
                }
            }

            return ObjLib.Conversions.Flood.FloodReseller.ListAll()[resellerId].Url;
        }
    }
}
