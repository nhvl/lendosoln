﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EDocs;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Security;
using System.IO;

namespace LendersOffice.Conversions.FloodOrder
{
    public static class FloodCertImporter
    {
        public static Guid UploadReportToEDocs(string sBase64EncodedPDF, int? iDocTypeID, Guid aAppId, Guid sLId, Guid brokerId)
        {
            string sFileName = Utilities.Base64StringToFile(sBase64EncodedPDF, "pdf");

            EDocumentRepository EDocRepo = EDocumentRepository.GetUserRepository();
            EDocument EDoc = EDocRepo.CreateDocument(E_EDocumentSource.FloodService);
            EDoc.InternalDescription = "Flood Cert " + DateTime.Today.ToShortDateString();
            EDoc.DocumentTypeId = (iDocTypeID != null) ? (int)iDocTypeID : EDocumentDocType.GetOrCreatePartnerUpload(brokerId);
            EDoc.LoanId = sLId;
            EDoc.AppId = aAppId;
            EDoc.IsUploadedByPmlUser = false; // feature won't be exposed to PML; the flood cert is typically ordered by the processor/loan opener
            EDoc.EDocOrigin = E_EDocOrigin.LO;
            EDoc.PublicDescription = "Flood Cert";
            EDoc.UpdatePDFContentOnSave(sFileName);
            EDocRepo.Save(EDoc);
            return EDoc.DocumentId;
        }
    }
}
