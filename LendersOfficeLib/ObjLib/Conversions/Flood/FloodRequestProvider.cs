﻿namespace LendersOffice.Conversions.FloodOrder
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using DataAccess;
    using LendersOffice.Conversions.FloodOrder.Request;
    using LendersOffice.Security;

    /// <summary>
    /// Creates and populates a Flood request.
    /// </summary>
    public class FloodRequestProvider
    {
        #region Variables
        private FloodRequest m_request; // The internal representation of our data
        private E_FloodActionType m_eRequestType;

        private CPageData m_dataLoan;
        private CAppData m_dataApp;

        private string m_sLogin;
        private string m_sPassword;
        private string m_sMISMOVersion = "2.1";
        #endregion

        public FloodRequestProvider(string sLogin, string sPassword, Guid sLId)
        {
            m_dataLoan = CPageData.CreateUsingSmartDependency(sLId, typeof(FloodRequestProvider));
            m_dataLoan.InitLoad();
            m_dataLoan.SetFormatTarget(FormatTarget.MismoClosing);
            m_dataLoan.ByPassFieldSecurityCheck = true;

            if (m_dataLoan.nApps > 0)
            {
                m_dataApp = m_dataLoan.GetAppData(0);
            }
            m_dataApp.BorrowerModeT = E_BorrowerModeT.Borrower;

            m_sLogin = sLogin;
            m_sPassword = sPassword;
        }

        public FloodRequest CreateRequest()
        {
            m_eRequestType = m_dataLoan.sFloodOrderType;
            // if the flood order type is blank because the user has not saved the order page then assume Original, i.e. new report
            m_eRequestType = (m_eRequestType == E_FloodActionType.None) ? E_FloodActionType.Original : m_eRequestType;
            m_request = CreateFloodRequest();
            return m_request;
        }
        public FloodRequest CreateRequestForPendingReport()
        {
            m_eRequestType = E_FloodActionType.StatusQuery;
            m_request = CreateFloodRequest();
            return m_request;
        }

        private FloodRequest CreateFloodRequest()
        {
            FloodRequest floodRequest = new FloodRequest();
            floodRequest.MISMOVersionID = m_sMISMOVersion;
            floodRequest.REQUEST = CreateREQ();
            return floodRequest;
        }

        private REQUEST CreateREQ()
        {
            REQUEST req = new REQUEST();
            req.LoginAccountIdentifier = m_sLogin;
            req.LoginAccountPassword = m_sPassword;
            if (!string.IsNullOrEmpty(m_dataLoan.BrokerDB.FloodAccountId))
            {
                req.InternalAccountIdentifier = m_dataLoan.BrokerDB.FloodAccountId;
            }

            req.REQUEST_DATA = CreateRequestData();

            string userEmail = GetEmployeeEmailFromId();
            if (!string.IsNullOrEmpty(userEmail))
            {
                req.KeyList.Add(CreateKey("Email", userEmail));
            }

            return req;
        }

        private REQUEST_DATA CreateRequestData()
        {
            REQUEST_DATA reqData = new REQUEST_DATA();
            switch (m_eRequestType)
            {
                case E_FloodActionType.Original:
                    reqData.FloodRequestData = CreateNewReportRequestData();
                    break;
                case E_FloodActionType.StatusQuery:
                    reqData.FloodRequestData = CreateStatusQueryRequestData();
                    break;
                case E_FloodActionType.Upgrade:
                    reqData.FloodRequestData = CreateUpgradeRequestData();
                    break;
                case E_FloodActionType.Reissue:
                    reqData.FloodRequestData = CreateReissueRequestData();
                    break;
                case E_FloodActionType.None:
                default:
                    throw new UnhandledEnumException(m_eRequestType);
            }

            reqData.FloodRequestData.ActionType = m_eRequestType;
            return reqData;
        }

        #region Request Types
        private FloodRequestData CreateNewReportRequestData()
        {
            FloodRequestData newOrder = new FloodRequestData();
            newOrder.MISMOVersionID = m_sMISMOVersion;
            newOrder.RushIndicator = FloodOrderConvert.ToYN(m_dataLoan.sIsFloodOrderRushOrder);
            newOrder._PRODUCT = CreateProduct();

            int iBorrowerCount = 0;
            BORROWER borrower = CreateBorrower(m_dataApp, E_BorrowerModeT.Borrower);
            if (borrower != null)
            {
                newOrder.BorrowerList.Add(borrower);
                iBorrowerCount++;
            }

            borrower = CreateBorrower(m_dataApp, E_BorrowerModeT.Coborrower);
            if (borrower != null)
            {
                newOrder.BorrowerList.Add(borrower);
                iBorrowerCount++;
            }
            // a max of two borrowers will be passed in the request
            for (int i = 1; (i < m_dataLoan.nApps) && (iBorrowerCount < 2); i++)
            {
                CAppData dataApp = m_dataLoan.GetAppData(i);
                borrower = CreateBorrower(dataApp, E_BorrowerModeT.Borrower);
                if (borrower != null)
                {
                    newOrder.BorrowerList.Add(borrower);
                    iBorrowerCount++;
                }
            }

            m_dataApp.BorrowerModeT = E_BorrowerModeT.Borrower; // reset the borrower mode
            newOrder.MORTGAGE_TERMS = CreateMortgageTerms();
            newOrder.PROPERTY = CreateProperty();
            return newOrder;
        }

        private FloodRequestData CreateStatusQueryRequestData()
        {
            FloodRequestData statusQuery = new FloodRequestData();
            statusQuery.MISMOVersionID = m_sMISMOVersion;
            statusQuery.FloodCertificationIdentifier = m_dataLoan.sFloodOrderQueryID;
            return statusQuery;
        }

        private FloodRequestData CreateUpgradeRequestData()
        {
            FloodRequestData upgradeReq = new FloodRequestData();
            upgradeReq.MISMOVersionID = m_sMISMOVersion;
            upgradeReq.FloodCertificationIdentifier = m_dataLoan.sFloodCertId;
            upgradeReq._PRODUCT = CreateProduct();
            return upgradeReq;
        }

        private FloodRequestData CreateReissueRequestData()
        {
            FloodRequestData reissueReq = new FloodRequestData();
            reissueReq.MISMOVersionID = m_sMISMOVersion;
            reissueReq.FloodCertificationIdentifier = m_dataLoan.sFloodCertId;
            return reissueReq;
        }
        #endregion

        /// <summary>
        /// Creates a key associated with a request. A key holds a name/value pair.
        /// </summary>
        /// <param name="name">The name of the key.</param>
        /// <param name="value">The value of the key.</param>
        /// <returns>A populated KEY container.</returns>
        private KEY CreateKey(string name, string value)
        {
            KEY KEY = new KEY();
            KEY.Name = name;
            KEY.Value = value;
            return KEY;
        }

        private _PRODUCT CreateProduct()
        {
            _PRODUCT prod = new _PRODUCT();
            switch (m_eRequestType)
            {
                case E_FloodActionType.Original:
                    if (m_dataLoan.sIsFloodOrderHMDA)
                        prod.ProductList.Add(CreateProductItem(E_FloodProductType.HMDA));
                    if (m_dataLoan.sIsFloodOrderLOL)
                        prod.ProductList.Add(CreateProductItem(E_FloodProductType.LifeOfLoan));
                    break;
                case E_FloodActionType.Upgrade:
                    prod.ProductList.Add(CreateProductItem(E_FloodProductType.LifeOfLoan)); // Upgrade == LifeOfLoan, 100% of the time
                    break;
                case E_FloodActionType.Reissue:
                case E_FloodActionType.StatusQuery:
                    break;
                case E_FloodActionType.None:
                default:
                    throw new UnhandledEnumException(m_eRequestType);
            }
            return prod;
        }
        private Product CreateProductItem(E_FloodProductType eFloodProductT)
        {
            if (eFloodProductT == E_FloodProductType.None)
                throw new UnhandledEnumException(eFloodProductT);

            Product prod = new Product();
            prod.ProductType = eFloodProductT;
            return prod;
        }

        private BORROWER CreateBorrower(CAppData dataApp, E_BorrowerModeT eBorrowerModeT)
        {
            dataApp.BorrowerModeT = eBorrowerModeT;
            if ((string.IsNullOrEmpty(m_dataApp.aSsn)) && (eBorrowerModeT == E_BorrowerModeT.Coborrower))
                return null;

            BORROWER borrower = new BORROWER();
            borrower.BorrowerType = (eBorrowerModeT == E_BorrowerModeT.Borrower) ? E_FloodBorrowerType.Borrower : E_FloodBorrowerType.CoBorrower;
            borrower._FirstName = dataApp.aFirstNm;
            borrower._LastName = dataApp.aLastNm;
            return borrower;
        }

        private MORTGAGE_TERMS CreateMortgageTerms()
        {
            MORTGAGE_TERMS mtgTerms = new MORTGAGE_TERMS();
            mtgTerms.LenderCaseIdentifier = m_dataLoan.sLNm;
            return mtgTerms;
        }

        private PROPERTY CreateProperty()
        {
            PROPERTY subjProperty = new PROPERTY();
            subjProperty._StreetAddress = m_dataLoan.sSpAddr;
            subjProperty._City = m_dataLoan.sSpCity;
            subjProperty._State = m_dataLoan.sSpState;
            subjProperty._PostalCode = m_dataLoan.sSpZip;
            subjProperty.AssessorsParcelIdentifier = m_dataLoan.sAssessorsParcelId;
            subjProperty._LEGAL_DESCRIPTION = CreateLegalDescription();
            return subjProperty;
        }

        private _LEGAL_DESCRIPTION CreateLegalDescription()
        {
            _LEGAL_DESCRIPTION legalDescr = new _LEGAL_DESCRIPTION();
            legalDescr._TextDescription = m_dataLoan.sSpLegalDesc;
            return legalDescr;
        }

        /// <summary>
        /// Retrieves the email address of the current user.
        /// </summary>
        /// <returns>The email address of the current user.</returns>
        private string GetEmployeeEmailFromId()
		{
            var principal = PrincipalFactory.CurrentPrincipal;
            SqlParameter[] parameters = 
            {
                new SqlParameter("@EmployeeID", principal.EmployeeId)
            };

			using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "GetEmployeeDetailsByEmployeeId", parameters )) 
			{
				return reader.Read() ? (string)reader["Email"] : null;
			}
		}
    }
}
