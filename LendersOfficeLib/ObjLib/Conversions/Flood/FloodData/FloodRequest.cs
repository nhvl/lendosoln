﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;
using LendersOffice.Common;

namespace LendersOffice.Conversions.FloodOrder.Request
{
    public enum E_YNIndicator
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Y")]
        Y,
        [XmlEnum("N")]
        N,
    }

    public enum E_FloodActionType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Original")]
        Original,
        [XmlEnum("StatusQuery")]
        StatusQuery,
        [XmlEnum("Upgrade")]
        Upgrade,
        [XmlEnum("Reissue")]
        Reissue,
    }

    public enum E_FloodProductType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("HMDA")]
        HMDA,
        [XmlEnum("LifeOfLoan")]
        LifeOfLoan,
    }

    public enum E_FloodBorrowerType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Borrower")]
        Borrower,
        [XmlEnum("CoBorrower")]
        CoBorrower,
    }
}
namespace LendersOffice.Conversions.FloodOrder.Request
{
    [XmlRoot("REQUEST_GROUP")]
    public class FloodRequest
    {
        [XmlAttribute()]
        public string MISMOVersionID;
        public bool ShouldSerializeMISMOVersionID() { return !string.IsNullOrEmpty(MISMOVersionID); }
        [XmlElement(Order = 1)]
        public REQUEST REQUEST = new REQUEST();
    }

    public class REQUEST
    {
        [XmlAttribute()]
        public string LoginAccountIdentifier;
        public bool ShouldSerializeLoginAccountIdentifier() { return !string.IsNullOrEmpty(LoginAccountIdentifier); }
        [XmlAttribute()]
        public string LoginAccountPassword;
        public bool ShouldSerializeLoginAccountPassword() { return !string.IsNullOrEmpty(LoginAccountPassword); }
        [XmlAttribute()]
        public string InternalAccountIdentifier;
        public bool ShouldSerializeInternalAccountIdentifier() { return !string.IsNullOrEmpty(InternalAccountIdentifier); }
        [XmlElement(Order = 1)]
        public REQUEST_DATA REQUEST_DATA = new REQUEST_DATA();
        [XmlElement(Order = 2, ElementName = "KEY")]
        public List<KEY> KeyList = new List<KEY>();
    }

    public class REQUEST_DATA
    {
        [XmlElement(Order = 1, ElementName = "FLOOD_REQUEST")]
        public FloodRequestData FloodRequestData;
    }

    /// <summary>
    /// A class to hold name/value pairs associated with the request
    /// </summary>
    public class KEY
    {
        /// <summary>
        /// The name of the key.
        /// </summary>
        [XmlAttribute(AttributeName = "_Name")]
        public string Name;

        /// <summary>
        /// Indicates whether the name of the key is set and able to be serialized.
        /// </summary>
        /// <returns>A bool indicating whether the name of the key is able to be serialized.</returns>
        public bool ShouldSerializeName()
        { 
            return !string.IsNullOrEmpty(Name);
        }

        /// <summary>
        /// The value of the key.
        /// </summary>
        [XmlAttribute(AttributeName = "_Value")]
        public string Value;

        /// <summary>
        /// Indicates whether the value of the key is set and able to be serialized.
        /// </summary>
        /// <returns>A bool indicating whether the value of the key is able to be serialized.</returns>
        public bool ShouldSerializeValue()
        { 
            return !string.IsNullOrEmpty(Value);
        }
    }

    public class FloodRequestData
    {
        [XmlAttribute()]
        public string MISMOVersionID;
        public bool ShouldSerializeMISMOVersionID() { return !string.IsNullOrEmpty(MISMOVersionID); }
        [XmlIgnore()]
        public E_FloodActionType ActionType;
        [XmlAttribute(AttributeName = "_ActionType")]
        public string _ActionTypeSerialized { get { return Utilities.ConvertEnumToString(ActionType); } set { } }
        public bool ShouldSerialize_ActionTypeSerialized() { return !string.IsNullOrEmpty(_ActionTypeSerialized); }

        [XmlIgnore()]
        public E_YNIndicator RushIndicator;
        [XmlAttribute(AttributeName = "_RushIndicator")]
        public string _RushIndicatorSerialized { get { return Utilities.ConvertEnumToString(RushIndicator); } set { } }
        public bool ShouldSerialize_RushIndicatorSerialized() { return !string.IsNullOrEmpty(_RushIndicatorSerialized); }

        [XmlAttribute()]
        public string FloodCertificationIdentifier;
        public bool ShouldSerializeFloodCertificationIdentifier() { return !string.IsNullOrEmpty(FloodCertificationIdentifier); }

        [XmlElement(Order = 1)]
        public _PRODUCT _PRODUCT;
        [XmlIgnore()]
        public bool _PRODUCTSpecified { get { return _PRODUCT != null; } set { } }
        [XmlElement(Order = 2, ElementName = "BORROWER")]
        public List<BORROWER> BorrowerList = new List<BORROWER>();
        [XmlElement(Order = 3)]
        public MORTGAGE_TERMS MORTGAGE_TERMS;
        [XmlIgnore()]
        public bool MORTGAGE_TERMSSpecified { get { return MORTGAGE_TERMS != null; } set { } }
        [XmlElement(Order = 4)]
        public PROPERTY PROPERTY;
        [XmlIgnore()]
        public bool PROPERTYSpecified { get { return PROPERTY != null; } set { } }
    }

    public class _PRODUCT 
    {
        [XmlElement(Order = 1, ElementName = "_NAME")]
        public List<Product> ProductList = new List<Product>();
    }
    public class Product
    {
        [XmlIgnore()]
        public E_FloodProductType ProductType;
        [XmlAttribute(AttributeName = "_Identifier")]
        public string _IdentifierSerialized { get { return Utilities.ConvertEnumToString(ProductType); } set { } }
        public bool ShouldSerialize_IdentifierSerialized() { return !string.IsNullOrEmpty(_IdentifierSerialized); }
    }

    public class BORROWER
    {
        [XmlIgnore()]
        public E_FloodBorrowerType BorrowerType;
        [XmlAttribute(AttributeName = "BorrowerID")]
        public string BorrowerIDSerialized { get { return Utilities.ConvertEnumToString(BorrowerType); } set { } }
        public bool ShouldSerializeBorrowerIDSerialized() { return !string.IsNullOrEmpty(BorrowerIDSerialized); }
        [XmlAttribute()]
        public string _FirstName;
        public bool ShouldSerialize_FirstName() { return !string.IsNullOrEmpty(_FirstName); }
        [XmlAttribute()]
        public string _LastName;
        public bool ShouldSerialize_LastName() { return !string.IsNullOrEmpty(_LastName); }
    }

    public class MORTGAGE_TERMS
    {
        [XmlAttribute()]
        public string LenderCaseIdentifier;
        public bool ShouldSerializeLenderCaseIdentifier() { return !string.IsNullOrEmpty(LenderCaseIdentifier); }
    }

    public class PROPERTY
    {
        [XmlAttribute()]
        public string _StreetAddress;
        public bool ShouldSerialize_StreetAddress() { return !string.IsNullOrEmpty(_StreetAddress); }
        [XmlAttribute()]
        public string _City;
        public bool ShouldSerialize_City() { return !string.IsNullOrEmpty(_City); }
        [XmlAttribute()]
        public string _State;
        public bool ShouldSerialize_State() { return !string.IsNullOrEmpty(_State); }
        [XmlAttribute()]
        public string _PostalCode;
        public bool ShouldSerialize_PostalCode() { return !string.IsNullOrEmpty(_PostalCode); }
        [XmlAttribute()]
        public string AssessorsParcelIdentifier;
        public bool ShouldSerializeAssessorsParcelIdentifier() { return !string.IsNullOrEmpty(AssessorsParcelIdentifier); }
        [XmlElement(Order = 1)]
        public _LEGAL_DESCRIPTION _LEGAL_DESCRIPTION = new _LEGAL_DESCRIPTION();
    }
    public class _LEGAL_DESCRIPTION
    {
        [XmlAttribute()]
        public string _TextDescription;
        public bool ShouldSerialize_TextDescription() { return !string.IsNullOrEmpty(_TextDescription); }
    }
}