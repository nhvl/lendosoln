﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
//using System.Xml.Linq;

namespace LendersOffice.Conversions.FloodOrder.Response 
{
    public enum E_YNIndicator
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Y")]
        Y,
        [XmlEnum("N")]
        N,
    }

    public enum E_FloodStatus
    {
        [XmlEnum("")]
        None,
        [XmlEnum("COM")]
        Completed,
        [XmlEnum("DUP")]
        Duplicate,
        [XmlEnum("MAN")]
        Manual,
        [XmlEnum("PND")]
        Pending,
        [XmlEnum("ERR")]
        ERROR,
    }

    public enum E_NFIPCommunityParticipationStatusType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("Emergency")]
        Emergency,
        [XmlEnum("Non-Participating")]
        NonParticipating,
        [XmlEnum("NonParticipating")]
        NonPart,
        [XmlEnum("Probation")]
        Probation,
        [XmlEnum("Regular")]
        Regular,
        [XmlEnum("Suspended")]
        Suspended,
    }

    public enum E_DocumentFileType
    {
        [XmlEnum("")]
        None,
        [XmlEnum("pdf")]
        PDF,
    }
}
namespace LendersOffice.Conversions.FloodOrder.Response
{
    [XmlRoot("RESPONSE_GROUP")]
    public class FloodResponse
    {
        [XmlAttribute()]
        public String MISMOVersionID;
        [XmlElement()]
        public RESPONSE RESPONSE = new RESPONSE();
    }

    public class RESPONSE
    {
        [XmlAttribute()]
        public String ResponseDateTime;
        [XmlElement("KEY")]
        public List<KEY> KeyList = new List<KEY>();
        [XmlElement()]
        public RESPONSE_DATA RESPONSE_DATA = new RESPONSE_DATA();
        [XmlElement("STATUS")]
        public List<STATUS> StatusList = new List<STATUS>();
    }

    public class KEY
    {
        [XmlAttribute()]
        public String _Name;
        [XmlAttribute()]
        public String _Value;
    }

    public class RESPONSE_DATA
    {
        [XmlElement("FLOOD_RESPONSE")]
        public Flood_Response_Data Flood_Response_Data = new Flood_Response_Data();
    }

    #region Flood_Response
    public class Flood_Response_Data
    {
        [XmlAttribute()]
        public String MISMOVersionID;
        [XmlElement("PROPERTY")]
        public FLOOD_PROPERTY PROPERTY = new FLOOD_PROPERTY();
        [XmlElement()]
        public EMBEDDED_FILE EMBEDDED_FILE = new EMBEDDED_FILE();
        [XmlElement()]
        public FLOOD_DETERMINATION FLOOD_DETERMINATION = new FLOOD_DETERMINATION();
    }

    #region PROPERTY
    public class FLOOD_PROPERTY
    {
        [XmlElement()]
        public _IDENTIFICATION _IDENTIFICATION = new _IDENTIFICATION();
    }

    public class _IDENTIFICATION
    {
        [XmlAttribute()]
        public String CountyFIPSCode;
        [XmlAttribute()]
        public String StateFIPSCode;
        [XmlAttribute()]
        public String CensusTractIdentifier;
        [XmlAttribute()]
        public String MSAIdentifier;
    }
    #endregion

    public class EMBEDDED_FILE
    {
        [XmlAttribute()]
        public String _EncodingType;
        [XmlAttribute()]
        public E_DocumentFileType _Extension;
        [XmlElement()]
        public DOCUMENT DOCUMENT;
    }

    public class DOCUMENT
    {
        [XmlText(Type = typeof(String))]
        public String PDF;
    }

    #region Flood Determination
    public class FLOOD_DETERMINATION
    {
        [XmlAttribute()]
        public String FloodCertificationIdentifier;
        [XmlAttribute()]
        public E_YNIndicator SpecialFloodHazardAreaIndicator;
        [XmlAttribute()]
        public E_YNIndicator FloodPartialIndicator;
        [XmlAttribute()]
        public E_YNIndicator _LifeOfLoanIndicator;
        [XmlAttribute()]
        public String FloodProductCertifyDate;
        [XmlElement()]
        public _COMMUNITY_INFORMATION _COMMUNITY_INFORMATION = new _COMMUNITY_INFORMATION();
        [XmlElement()]
        public _BUILDING_INFORMATION _BUILDING_INFORMATION = new _BUILDING_INFORMATION();
        [XmlElement()]
        public _INSURANCE_INFORMATION _INSURANCE_INFORMATION = new _INSURANCE_INFORMATION();
    }

    public class _COMMUNITY_INFORMATION
    {
        [XmlAttribute()]
        public String NFIPCommunityIdentifier;
        [XmlAttribute()]
        public String NFIPCommunityName;
        [XmlAttribute()]
        public String NFIPCommunityParticipationStartDate;
    }

    public class _BUILDING_INFORMATION
    {
        [XmlAttribute()]
        public String NFIPFloodZoneIdentifier;
        [XmlAttribute()]
        public E_YNIndicator NFIPMapIndicator;
        [XmlAttribute()]
        public String NFIPMapPanelDate;
        [XmlAttribute()]
        public String NFIPMapIdentifier;
        [XmlAttribute()]
        public String NFIPMapPanelIdentifier;
        [XmlAttribute()]
        public String NFIPMapPanelSuffixIdentifier;
        [XmlAttribute()]
        public String NFIPLetterOfMapDate;
    }

    public class _INSURANCE_INFORMATION
    {
        [XmlAttribute()]
        public E_NFIPCommunityParticipationStatusType NFIPCommunityParticipationStatusType;
        [XmlAttribute()]
        public E_YNIndicator ProtectedAreaIndicator;
        [XmlAttribute()]
        public String ProtectedAreaDesignationDate;
    }
    #endregion
    #endregion

    public class STATUS
    {
        [XmlAttribute()]
        public E_FloodStatus _Code;
        [XmlAttribute()]
        public String _Description;
    }
}