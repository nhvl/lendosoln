﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Conversions.FloodOrder.Request;

namespace LendersOffice.Conversions.FloodOrder
{
    public static class FloodOrderConvert
    {
        public static E_YNIndicator ToYN(bool val)
        {
            return val ? E_YNIndicator.Y : E_YNIndicator.N;
        }
    }
}
