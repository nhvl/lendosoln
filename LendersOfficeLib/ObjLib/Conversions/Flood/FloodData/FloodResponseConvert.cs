﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Conversions.FloodOrder.Response;
using System.Text.RegularExpressions;

namespace LendersOffice.Conversions.FloodOrder
{
    public class FloodMapNumPanelSuffix
    {
        private string m_sMapNumber;
        private string m_sPannelNumber;
        private string m_sSuffix;

        public string MapNumber
        {
            get { return m_sMapNumber; }
            set { m_sMapNumber = value; }
        }
        public string PannelNumber
        {
            get { return m_sPannelNumber; }
            set { m_sPannelNumber = value; }
        }
        public string Suffix
        {
            get { return m_sSuffix; }
            set { m_sSuffix = value; }
        }
    }

    public static class FloodResponseConvert
    {
        public static bool IsFloodInsuranceAvailable(E_NFIPCommunityParticipationStatusType eParticipation)
        {
            bool bIsAvailable = false;
            switch (eParticipation)
            {
                case E_NFIPCommunityParticipationStatusType.Emergency:
                case E_NFIPCommunityParticipationStatusType.Probation:
                case E_NFIPCommunityParticipationStatusType.Regular:
                    bIsAvailable = true;
                    break;
                case E_NFIPCommunityParticipationStatusType.Suspended:
                case E_NFIPCommunityParticipationStatusType.None:
                case E_NFIPCommunityParticipationStatusType.NonParticipating:
                case E_NFIPCommunityParticipationStatusType.NonPart:
                default:
                    break;
            }
            return bIsAvailable;
        }

        public static string FloodParticipationStatus(E_NFIPCommunityParticipationStatusType eParticipation)
        {
            switch(eParticipation)
            {
                case E_NFIPCommunityParticipationStatusType.Emergency: return "E";
                case E_NFIPCommunityParticipationStatusType.None:
                case E_NFIPCommunityParticipationStatusType.NonParticipating:
                case E_NFIPCommunityParticipationStatusType.NonPart:
                    return "N";
                case E_NFIPCommunityParticipationStatusType.Probation: return "P";
                case E_NFIPCommunityParticipationStatusType.Regular: return "R";
                case E_NFIPCommunityParticipationStatusType.Suspended: return "S";
                default:
                    return "N";
            }
        }

        /// <summary>
        /// Gets a regex used to split out the map, panel, and suffix from a single string.
        /// </summary>
        public static Regex MapNumPanelSuffixRegex { get; } = new Regex(@"(?<map>.{1,6})(?<panel>.{0,5})(?<suffix>.{1,2})", RegexOptions.IgnoreCase);

        // Map, Panel, and Suffix may either be provided as a single string or separately
        public static FloodMapNumPanelSuffix MapNumberPanelSuffix(_BUILDING_INFORMATION building)
        {
            FloodMapNumPanelSuffix MNPS = new FloodMapNumPanelSuffix();

            MNPS.MapNumber = building.NFIPMapIdentifier;
            MNPS.PannelNumber = building.NFIPMapPanelIdentifier;
            MNPS.Suffix = building.NFIPMapPanelSuffixIdentifier;
            
            if(!String.IsNullOrEmpty(building.NFIPMapIdentifier))
            {
                Match match = MapNumPanelSuffixRegex.Match(building.NFIPMapIdentifier);
                if ((building.NFIPMapIdentifier.Length > 6) && (match.Success))
                {
                    MNPS.MapNumber = match.Groups["map"].Value;
                    MNPS.PannelNumber = (String.IsNullOrEmpty(MNPS.PannelNumber)) ? match.Groups["panel"].Value : MNPS.PannelNumber;
                    MNPS.Suffix = (String.IsNullOrEmpty(MNPS.Suffix)) ? match.Groups["suffix"].Value : MNPS.Suffix;
                }
            }

            return MNPS;
        }
    }
}
