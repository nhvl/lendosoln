﻿namespace LendersOffice.ObjLib.Conversions.Flood
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Common;
    using DataAccess;

    /// <summary>
    /// Flood reseller entry.
    /// </summary>
    public class FloodReseller
    {
        /// <summary>
        /// Flood Resellers Cache.
        /// </summary>
        private static TimeBasedCacheObject<IDictionary<Guid, FloodReseller>> resellerDict;

        /// <summary>
        /// Initializes static members of the<see cref="FloodReseller" /> class.
        /// </summary>
        static FloodReseller()
        {
            ReloadAll();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FloodReseller"/> class.
        /// </summary>
        /// <param name="id">Flood Reseller Id.</param>
        /// <param name="name">Company name.</param>
        /// <param name="url">Service Url.</param>
        public FloodReseller(Guid id, string name, string url)
        {
            this.Id = id;
            this.Name = name.Trim();
            this.Url = url.Trim();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FloodReseller"/> class.
        /// </summary>
        public FloodReseller()
        {
        }

        /// <summary>
        /// Gets/sets the value for the flood reseller id.
        /// </summary>
        public Guid Id { get; private set; } = Guid.Empty;

        /// <summary>
        /// Gets/sets the value for the flood reseller name.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Gets/sets the value for the service url.
        /// </summary>
        public string Url { get; private set; }

        /// <summary>
        /// Create a new flood reseller.
        /// </summary>
        /// <param name="floodResellerId">Flood Reseller Id.</param>
        /// <param name="name">Company Name.</param>
        /// <param name="url">Service Url.</param>
        /// <returns>Return Flood Resseler Id.</returns>
        public static Guid Create(Guid floodResellerId, string name, string url)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@FloodResellerId", floodResellerId),
                new SqlParameter("@Name", name),
                new SqlParameter("@Url", url)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "FLOOD_RESELLER_Insert", 3, parameters);
            ReloadAll();

            return floodResellerId;
        }

        /// <summary>
        /// Update Flood Reseller to database.
        /// </summary>
        /// <param name="id">The Flood Reseller Id.</param>
        /// <param name="name">Company Name.</param>
        /// <param name="url">Service url.</param>
        public static void Update(Guid id, string name, string url)
        {
            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@FloodResellerId", id),
                new SqlParameter("@Name", name),
                new SqlParameter("@Url", url)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "FLOOD_RESELLER_Update", 3, parameters);
            ReloadAll();
        }

        /// <summary>
        /// Get all Flood Resellers from database.
        /// </summary>
        /// <returns>Return a dictionary of flood reselllers.</returns>
        public static IDictionary<Guid, FloodReseller> ListAll()
        {
            return resellerDict.Value;
        }

        /// <summary>
        /// Load all Flood Resellers from database and cache them.
        /// </summary>
        private static void ReloadAll()
        {
            // Cache the dictionary for 5 minutes.
            resellerDict = new TimeBasedCacheObject<IDictionary<Guid, FloodReseller>>(new TimeSpan(0, 5, 0), ListAllImpl);
        }

        /// <summary>
        /// Load all Flood Resellers from database.
        /// </summary>
        /// <returns>Return a dictionary of flood reselllers.</returns>
        private static IDictionary<Guid, FloodReseller> ListAllImpl()
        {
            var dict = new Dictionary<Guid, FloodReseller>();

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "FLOOD_RESELLER_ListAll"))
            {
                while (reader.Read())
                {
                    Guid id = (Guid)reader["FloodResellerId"];
                    string name = (string)reader["Name"];
                    string url = (string)reader["Url"];

                    dict.Add(id, new FloodReseller(id, name, url));
                }
            }

            return dict;
        }
    }
}
