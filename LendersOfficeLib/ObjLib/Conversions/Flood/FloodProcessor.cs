﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Conversions.FloodOrder.Request;
using LendersOffice.Conversions.FloodOrder.Response;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOffice.Conversions.FloodOrder
{
    public class FloodProcessor
    {
        #region Variables
        private E_FloodStatus m_floodStatus;
        private FloodResponse m_response;
        private const int TIMEOUT = 300000; // 5 mins
        private int m_iTimeRemaining;
        private string m_sMessage;
        private string m_sLogin;
        private string m_sPassword;
        private Guid m_sLId;
        private string m_sResponseXML;
        private CPageData m_dataLoan;
        private string m_sOldQueryID;
        private FLOOD_DETERMINATION m_FloodData;
        private FLOOD_PROPERTY m_SubjProperty;
        private Guid m_EDocID;
        private E_FloodCertificationPreparerT m_floodProvider;
        private bool m_bIsEdocsEnabled;
        private int? m_sDocTypeId;
        private Guid m_sBrokerId;

        public E_FloodStatus Status
        {
            get { return m_floodStatus; }
        }

        public string Message
        {
            get { return m_sMessage; }
        }

        public string ResponseXML
        {
            get { return m_sResponseXML; }
        }

        public Guid EDocID
        {
            get { return m_EDocID; }
        }
        #endregion

        public FloodProcessor(FloodResponse response, Guid sLId, E_FloodCertificationPreparerT floodProvider, bool bIsEdocsEnabled, int? sDocTypeId, Guid sBrokerId)
        {
            m_response = response;
            m_floodStatus = E_FloodStatus.None;
            m_iTimeRemaining = TIMEOUT;
            m_sMessage = "";
            m_sResponseXML = "";
            m_sLId = sLId;
            m_floodProvider = floodProvider;
            m_bIsEdocsEnabled = bIsEdocsEnabled;
            m_sDocTypeId = sDocTypeId;
            m_sBrokerId = sBrokerId;
            m_EDocID = Guid.Empty;

            m_dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_sLId, typeof(FloodProcessor));
            m_dataLoan.InitLoad();
            m_sOldQueryID = m_dataLoan.sFloodOrderQueryID;
        }

        public void ProcessFloodResponse(string sLogin, string sPassword)
        {
            m_sLogin = sLogin;
            m_sPassword = sPassword;
            ProcessResponse();
        }
        private void ProcessResponse()
        {
            if (!ValidateWrapper())
            {
                return;
            }

            if (m_response.RESPONSE.StatusList.Count > 0)
            {
                m_floodStatus = FloodStatus();
                if ((m_floodStatus == E_FloodStatus.Completed) || (m_floodStatus == E_FloodStatus.Manual) || (m_floodStatus == E_FloodStatus.Pending))
                    ValidateFloodResponseTag();

                switch (m_floodStatus)
                {
                    case E_FloodStatus.Completed:
                        SaveFloodData();
                        UploadEDoc();
                        break;
                    case E_FloodStatus.Manual:
                        SaveQueryID();
                        m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                        if (this.TrySetOrderDate())
                        {
                            m_dataLoan.Save();
                        }
                        break;
                    case E_FloodStatus.Pending:
                        if ((m_iTimeRemaining > 0) && (SaveQueryID()))
                        {
                            int interval = 20000;

                            LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(interval);
                            m_iTimeRemaining -= interval;
                            RequestPendingReport();
                        }
                        else if (m_iTimeRemaining <= 0)
                        {
                            m_floodStatus = E_FloodStatus.ERROR;
                            m_sMessage = ErrorMessages.Flood.Timeout;
                        }
                        break;
                    case E_FloodStatus.ERROR:
                        // Do nothing; MCL error messages are already set when the flood status is retrieved above
                        break;
                    case E_FloodStatus.Duplicate:
                    case E_FloodStatus.None:
                    default:
                        SetUnexpectedResponse();
                        break;
                }
            }
            else
            {
                SetUnexpectedResponse();
            }
        }

        private E_FloodStatus FloodStatus()
        {
            foreach (STATUS status in m_response.RESPONSE.StatusList)
            {
                switch (status._Code)
                {
                    case E_FloodStatus.Completed:
                    case E_FloodStatus.Pending:
                        return status._Code;
                    case E_FloodStatus.Manual:
                        // Case 216274: Display Message from MCL for MAN Status. Note: "Manual Completion" seeems to be the default string for MCL to send with MAN status
                        m_sMessage = (String.IsNullOrEmpty(status._Description) || status._Description.Equals("Manual Completion", StringComparison.OrdinalIgnoreCase)) ? ErrorMessages.Flood.Manual : status._Description;
                        return status._Code;
                    case E_FloodStatus.ERROR:
                        m_sMessage = (String.IsNullOrEmpty(status._Description)) ? ErrorMessages.Flood.UnknownError : status._Description;
                        return status._Code;
                    case E_FloodStatus.Duplicate: // dup is ignored because MCL will return the prior copy w/o charge, with Completed status
                    case E_FloodStatus.None:
                        break;
                    default:
                        SetUnexpectedResponse();
                        break;
                }
            }
            return E_FloodStatus.None;
        }

        private void RequestPendingReport()
        {
            FloodRequest request = FloodServer.CreateRequestForPendingReport(m_sLogin, m_sPassword, m_sLId);

            m_response = FloodServer.Submit(request, m_floodProvider, out m_sResponseXML, m_sBrokerId); // let any webexception bubble up
            ProcessResponse();
        }

        private void SaveFloodData()
        {
            bool bValid = ValidateFloodDetermination();

            bValid = bValid ? ValidateCommunityInfo() : false;
            bValid = bValid ? ValidateBuildingInfo() : false;
            bValid = bValid ? ValidateInsuranceInfo() : false;

            _COMMUNITY_INFORMATION community = m_FloodData._COMMUNITY_INFORMATION;
            _BUILDING_INFORMATION building = m_FloodData._BUILDING_INFORMATION;
            _INSURANCE_INFORMATION insurance = m_FloodData._INSURANCE_INFORMATION;

            m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (bValid)
            {
                // National Flood Insurance Program Community Jurisdiction
                m_dataLoan.sFloodHazardCommunityDesc = community.NFIPCommunityName;
                m_dataLoan.sFloodCertificationCommunityNum = community.NFIPCommunityIdentifier;
                //National Flood Insurance Program Data Affecting Building/Mobile Home
                FloodMapNumPanelSuffix MapNumPanelSuffix = FloodResponseConvert.MapNumberPanelSuffix(building);
                m_dataLoan.sFloodCertificationMapNum = MapNumPanelSuffix.MapNumber;
                m_dataLoan.sFloodCertificationPanelNums = MapNumPanelSuffix.PannelNumber;
                m_dataLoan.sFloodCertificationPanelSuffix = MapNumPanelSuffix.Suffix;
                m_dataLoan.sFloodCertificationMapD_rep = building.NFIPMapPanelDate;
                // Todo: CoreLogic Loma/Lomr type
                m_dataLoan.sFloodCertificationLOMChangedD_rep = building.NFIPLetterOfMapDate;
                m_dataLoan.sNfipFloodZoneId = building.NFIPFloodZoneIdentifier;
                m_dataLoan.sFloodCertificationIsNoMap = (building.NFIPMapIndicator != Response.E_YNIndicator.Y);
                m_dataLoan.sNfipIsPartialZone = (m_FloodData.FloodPartialIndicator == Response.E_YNIndicator.Y);
                // Federal Flood Insurance Availability
                bool bIsInsuranceAvailable = FloodResponseConvert.IsFloodInsuranceAvailable(insurance.NFIPCommunityParticipationStatusType);
                m_dataLoan.sFloodHazardFedInsAvail = bIsInsuranceAvailable;
                m_dataLoan.sFloodHazardFedInsNotAvail = !bIsInsuranceAvailable;
                m_dataLoan.sFloodCertificationParticipationStatus = FloodResponseConvert.FloodParticipationStatus(insurance.NFIPCommunityParticipationStatusType);
                m_dataLoan.sFfiaDateCommunityEnteredProgramD_rep = community.NFIPCommunityParticipationStartDate;
                m_dataLoan.sFloodCertificationIsCBRAorOPA = (insurance.ProtectedAreaIndicator == Response.E_YNIndicator.Y);
                m_dataLoan.sFloodCertificationDesignationD_rep = insurance.ProtectedAreaDesignationDate;
                // Determination
                m_dataLoan.sFloodCertificationIsInSpecialArea = (m_FloodData.SpecialFloodHazardAreaIndicator == Response.E_YNIndicator.Y);
                // Preparer's Information
                m_dataLoan.sFloodCertificationPreparerT = m_floodProvider;
                m_dataLoan.sFloodCertificationDeterminationD_rep = m_FloodData.FloodProductCertifyDate;
                // The certify date TZ is not provided
                m_dataLoan.sFloodCertId = m_FloodData.FloodCertificationIdentifier;
                m_dataLoan.sFloodCertificationIsLOLUpgraded = (m_FloodData._LifeOfLoanIndicator == Response.E_YNIndicator.Y);
                m_dataLoan.sFloodCertRd_rep = DateTime.Now.ToShortDateString();

                if (ValidatePropertyIdent())
                {
                    if (!string.IsNullOrEmpty(m_SubjProperty._IDENTIFICATION.MSAIdentifier))
                    {
                        m_dataLoan.sHmdaMsaNum = m_SubjProperty._IDENTIFICATION.MSAIdentifier;
                    }
                    if (!string.IsNullOrEmpty(m_SubjProperty._IDENTIFICATION.CountyFIPSCode))
                    {
                        m_dataLoan.sHmdaCountyCode = m_SubjProperty._IDENTIFICATION.CountyFIPSCode;
                    }
                    if (!string.IsNullOrEmpty(m_SubjProperty._IDENTIFICATION.StateFIPSCode))
                    {
                        m_dataLoan.sHmdaStateCode = m_SubjProperty._IDENTIFICATION.StateFIPSCode;
                    }
                    if (!string.IsNullOrEmpty(m_SubjProperty._IDENTIFICATION.CensusTractIdentifier))
                    {
                        m_dataLoan.sHmdaCensusTract = m_SubjProperty._IDENTIFICATION.CensusTractIdentifier;
                    }
                }
            }

            if (this.TrySetOrderDate() || bValid)
            {
                m_dataLoan.Save();
            }
        }

        private bool SaveQueryID()
        {
            bool bSaved = false;
            if (ValidateFloodDetermination())
            {
                string sQueryID;
                sQueryID = m_FloodData.FloodCertificationIdentifier;
                if (!String.IsNullOrEmpty(sQueryID))
                {
                    sQueryID = sQueryID.TrimWhitespaceAndBOM();
                    if (!String.Equals(sQueryID, m_sOldQueryID))
                    {
                        m_dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                        m_dataLoan.sFloodOrderQueryID = m_sOldQueryID = sQueryID;
                        m_dataLoan.Save();
                    }
                    bSaved = true;
                }
                else
                {
                    SetBadResponse("FloodCertificationIdentifier (Query ID)");
                }
            }
            return bSaved;
        }

        private void SetBadResponse(string sMissing)
        {
            m_floodStatus = E_FloodStatus.ERROR;
            m_sMessage = string.Format("{0} {1}Missing portion: {2}", ErrorMessages.Flood.BadFormat, Environment.NewLine, sMissing);
        }

        private void SetUnexpectedResponse()
        {
            m_floodStatus = E_FloodStatus.ERROR;
            m_sMessage = ErrorMessages.Flood.UnexpectedResponse;
        }

        /// <summary>
        /// Attempts to set the initial flood order date. Will not overwrite the field on a future query.
        /// </summary>
        /// <returns>True if the date was set, false if it already has a value.</returns>
        private bool TrySetOrderDate()
        {
            if (string.IsNullOrEmpty(m_dataLoan.sFloodCertOd_rep))
            {
                m_dataLoan.sFloodCertOd_rep = DateTime.Now.ToShortDateString();
                return true;
            }

            return false;
        }

        private void UploadEDoc()
        {
            bool bValidFormat = true;
            EMBEDDED_FILE cert = m_response.RESPONSE.RESPONSE_DATA.Flood_Response_Data.EMBEDDED_FILE;
            if (cert == null)
            {
                SetBadResponse("EMBEDDED_FILE");
                bValidFormat = false;
            }

            if (bValidFormat)
            {
                if (String.IsNullOrEmpty(cert._EncodingType) || String.IsNullOrEmpty(cert.DOCUMENT.PDF))
                {
                    SetBadResponse("certificate PDF encoding or contents");
                    bValidFormat = false;
                }
            }

            if (bValidFormat)
            {
                if ((!String.Equals(cert._EncodingType.ToUpper().TrimWhitespaceAndBOM(), "BASE64")) || cert._Extension != E_DocumentFileType.PDF)
                {
                    m_sMessage = string.Format("{0} {1}The certificate has an unexpected encoding or extension.", ErrorMessages.Flood.BadFormat, Environment.NewLine);
                    bValidFormat = false;
                }
            }

            if (bValidFormat)
            {
                if (!m_bIsEdocsEnabled)
                {
                    m_sMessage = ErrorMessages.Flood.EdocSetupIncomplete;
                    bValidFormat = false;
                }
            }

            if (bValidFormat)
            {
                try
                {
                    m_EDocID = FloodCertImporter.UploadReportToEDocs(cert.DOCUMENT.PDF, m_sDocTypeId, m_dataLoan.GetAppData(0).aAppId, m_sLId, m_sBrokerId);
                }
                catch (AccessDenied)
                {
                    m_sMessage = ErrorMessages.Flood.InsufficientEdocPermission;
                    bValidFormat = false;
                }
            }

            if (!bValidFormat)
            {
                m_floodStatus = E_FloodStatus.ERROR;
            }
        }

        #region Sanity Checks
        private bool ValidateBuildingInfo()
        {
            return ValidateDeserializedXMLElement(m_FloodData._BUILDING_INFORMATION, "_BUILDING_INFORMATION");
        }

        private bool ValidateCommunityInfo()
        {
            return ValidateDeserializedXMLElement(m_FloodData._COMMUNITY_INFORMATION, "_COMMUNITY_INFORMATION");
        }

        private bool ValidateFloodDetermination()
        {
            m_FloodData = m_response.RESPONSE.RESPONSE_DATA.Flood_Response_Data.FLOOD_DETERMINATION;
            return ValidateDeserializedXMLElement(m_FloodData, "FLOOD_DETERMINATION");
        }

        private void ValidateFloodResponseTag()
        {
            if (ValidateDeserializedXMLElement(m_response.RESPONSE.RESPONSE_DATA, "RESPONSE_DATA"))
                ValidateDeserializedXMLElement(m_response.RESPONSE.RESPONSE_DATA.Flood_Response_Data, "FLOOD_RESPONSE");
        }

        private bool ValidateInsuranceInfo()
        {
            return ValidateDeserializedXMLElement(m_FloodData._INSURANCE_INFORMATION, "_INSURANCE_INFORMATION");
        }

        private bool ValidatePropertyIdent()
        {
            bool bIsValid = false;
            m_SubjProperty = m_response.RESPONSE.RESPONSE_DATA.Flood_Response_Data.PROPERTY;
            if (ValidateOptionalDeserializedXMLElement(m_SubjProperty))
            {
                bIsValid = ValidateOptionalDeserializedXMLElement(m_SubjProperty._IDENTIFICATION);
            }
            return bIsValid;
        }

        private bool ValidateWrapper()
        {
            return (ValidateDeserializedXMLElement(m_response, "RESPONSE_GROUP")) ? ValidateDeserializedXMLElement(m_response.RESPONSE, "RESPONSE") : false;
        }

        private bool ValidateDeserializedXMLElement(Object element, string sName)
        {
            bool bIsValid = true;
            bIsValid = (element != null);

            if (!bIsValid)
            {
                SetBadResponse(sName);
            }
            return bIsValid;
        }
        private bool ValidateOptionalDeserializedXMLElement(Object element)
        {
            return (element != null);
        }
        #endregion
    }
}
