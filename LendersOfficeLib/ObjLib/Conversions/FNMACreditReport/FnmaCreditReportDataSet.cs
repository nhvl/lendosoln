﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Conversions.MORNETPlus;

namespace LendersOffice.Conversions.FNMACreditReport
{
    public class FnmaCreditReportDataSet : MORNETPlusDataset
    {
        private static SegmentDefinition[] s_creditReportDefinitions = {
                                                                           new SegmentDefinition("H001", 1, 5, 9, 24, 34, 36, 37, 40, 74),
                                                                           new SegmentDefinition("S001", 1, 5, 9, 17, 42, 62, 82, 86, 95, 120,140,160,164,172),
                                                                           new SegmentDefinition("A001", 1, 5, 9, 10, 11, 17, 77, 97, 99, 108, 109, 113, 115),
                                                                           new SegmentDefinition("P001", 1, 5, 9, 11, 15, 21, 27, 31, 32, 41, 50, 59, 68, 74, 80, 82),
                                                                           new SegmentDefinition("I001", 1, 5, 9, 34, 42, 44),
                                                                           new SegmentDefinition("C001", 1, 5, 9, 10, 25, 55, 80, 81, 82, 83, 87, 91, 97, 103, 112, 113, 122, 126, 135, 136, 142, 148, 184, 185, 191, 200, 203, 206, 209, 212, 215, 218, 221, 224, 227, 230, 233, 236, 242, 248, 254, 260, 262),
                                                                           new SegmentDefinition("R001", 1, 5, 9, 10, 12, 14, 15, 21, 26, 31, 36, 41, 45, 46),
                                                                           new SegmentDefinition("E001", 1, 5, 9, 13, 15),
                                                                           new SegmentDefinition("T001", 1, 5, 8)
                                                                       };
        public FnmaCreditReportDataSet() : base(s_creditReportDefinitions) { }
    }
}
