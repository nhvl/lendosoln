﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.ObjLib.ServiceCredential
{
    using System;

    /// <summary>
    /// A user context for a service credential edit or load context.
    /// </summary>
    public partial class UserContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserContext"/> class.
        /// </summary>
        /// <param name="isPUser">A value indicating whether this user has <see cref="LendersOffice.Security.AbstractUserPrincipal.Type"/> equal to "P".</param>
        /// <param name="employeeId">The identifier of the employee record associated with this user.</param>
        public UserContext(bool isPUser, Guid employeeId)
        {
            this.IsPUser = isPUser;
            this.EmployeeId = employeeId;
        }

        /// <summary>
        /// Gets a value indicating whether this user has <see cref="LendersOffice.Security.AbstractUserPrincipal.Type"/> equal to "P".
        /// </summary>
        public bool IsPUser { get; }

        /// <summary>
        /// Gets the identifier of the employee record associated with this user.
        /// </summary>
        public Guid EmployeeId { get; }
    }
}
