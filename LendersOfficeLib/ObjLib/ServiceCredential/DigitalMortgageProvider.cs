﻿namespace LendersOffice.ObjLib.ServiceCredential
{
    using System.ComponentModel;

    /// <summary>
    /// Digital Mortgage Provider. More will be added to this enum later.
    /// </summary>
    public enum DigitalMortgageProvider
    {
        [Description("eOriginal")]
        EOriginal = 0
    }
}
