﻿namespace LendersOffice.ObjLib.ServiceCredential
{
    using System.ComponentModel;

    /// <summary>
    /// Defines the service credential options for UCD Delivery.
    /// </summary>
    public enum UcdDeliveryTargetOption
    {
        /// <summary>
        /// Credentials are for Fannie Mae's UCD Collection Solution.
        /// </summary>
        [Description("FNMA UCD Collection Solution")]
        FannieMaeUcdCollectionSolution = 0,

        /// <summary>
        /// Credentials are for LCA user login.
        /// </summary>
        [Description("Loan Closing Advisor User Login")]
        LcaUserLogin = 1,

        /// <summary>
        /// Credentials are for LPA seller login.  This is sometimes called "lender", since they're set at the lender-level for our users.
        /// </summary>
        [Description("Loan Closing Advisor Lender Login")]
        LcaSellerCredential = 2,
    }
}
