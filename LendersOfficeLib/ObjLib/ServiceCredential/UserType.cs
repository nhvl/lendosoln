﻿namespace LendersOffice.ObjLib.ServiceCredential
{
    /// <summary>
    /// The user type.
    /// </summary>
    public enum UserType
    {
        /// <summary>
        /// Any type of user.
        /// </summary>
        Any = 0,

        /// <summary>
        /// LQB User type.
        /// </summary>
        LQB = 1,
        
        /// <summary>
        /// Third party originator user.
        /// </summary>
        TPO = 2
    }
}
