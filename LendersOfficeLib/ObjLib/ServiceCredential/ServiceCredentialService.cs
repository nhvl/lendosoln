﻿namespace LendersOffice.ObjLib.ServiceCredential
{
    using System;

    /// <summary>
    /// Enum for determining what services a service credential is good for.
    /// </summary>
    /// <remarks>
    /// Flags because a service credential can be enabled for multiple services at once.
    /// Note that this is not stored in the DB. This is purely for easier access in the code.
    /// </remarks>
    [Flags]
    public enum ServiceCredentialService
    {
        /// <summary>
        /// Enabled for Credit Reports.
        /// </summary>
        CreditReports = 1 << 0,

        /// <summary>
        /// Enabled for Verifications.
        /// </summary>
        Verifications = 1 << 1,

        /// <summary>
        /// Enabled for AUS Submission.
        /// </summary>
        AusSubmission = 1 << 2,

        /// <summary>
        /// Enabled for Title Quotes.
        /// </summary>
        TitleQuotes = 1 << 3,

        /// <summary>
        /// Enabled for UCD Delivery.
        /// </summary>
        UcdDelivery = 1 << 4,

        /// <summary>
        /// Enabled for Document Capture.
        /// </summary>
        DocumentCapture = 1 << 5,

        /// <summary>
        /// Enabled for Digital Mortgage.
        /// </summary>
        DigitalMortgage = 1 << 6,

        /// <summary>
        /// Gets all service credentials, regardless of the enabled services.
        /// </summary>
        All = CreditReports | Verifications | AusSubmission | TitleQuotes | UcdDelivery | DocumentCapture | DigitalMortgage
    }
}
