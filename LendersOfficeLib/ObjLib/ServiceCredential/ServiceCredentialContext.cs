﻿namespace LendersOffice.ObjLib.ServiceCredential
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Provides static creation methods and a caching field for the core data record class.
    /// </summary>
    public partial class ServiceCredentialContext
    {
        /// <summary>
        /// A backing field for <see cref="CachedOriginatingCompanyGroupIds"/>. 
        /// </summary>
        private Lazy<IEnumerable<Guid>> cachedOriginatingCompanyGroupIds;

        /// <summary>
        /// Gets a cache of the group identifiers (<see cref="Group.Id"/>) with ties to the originating company specified by this instance.
        /// </summary>
        /// <remarks>
        /// This is used for VOX (and possibly other services) to indicate if a user or OC has OC group access.  We're caching it in a public property
        /// because the immutability of <see cref="OriginatingCompanyId"/> makes this safe and it's a quick solution to get us some form of caching.
        /// </remarks>
        public Lazy<IEnumerable<Guid>> CachedOriginatingCompanyGroupIds
        {
            get
            {
                if (this.cachedOriginatingCompanyGroupIds == null && this.OriginatingCompanyId.HasValue)
                {
                    this.cachedOriginatingCompanyGroupIds = new Lazy<IEnumerable<Guid>>(
                        () => GroupDB.ListInclusiveGroupForPmlBroker(this.BrokerId, this.OriginatingCompanyId.Value).Select(g => g.Id).ToList());
                }

                return this.cachedOriginatingCompanyGroupIds;
            }
        }

        /// <summary>
        /// From the current principal (passed in as <paramref name="principal"/>), creates an editing context for the current user's service credentials on their profile page.
        /// </summary>
        /// <param name="principal">The user to create an editing context for.</param>
        /// <param name="isLqbEditor">A value indicating whether this context originated in the LQB editor.</param>
        /// <returns>An instance representing the data of the user editing the service credentials.</returns>
        public static ServiceCredentialContext CreateEditFromProfileContext(AbstractUserPrincipal principal, bool isLqbEditor = false)
        {
            return new ServiceCredentialContext(
                brokerId: principal.BrokerId,
                branchId: default(Guid?),
                originatingCompanyId: principal.IsPUser ? principal.PmlBrokerId : default(Guid?),
                user: new UserContext(
                    isPUser: principal.IsPUser,
                    employeeId: principal.EmployeeId),
                isLqbEditor: isLqbEditor);
        }

        /// <summary>
        /// Creates an editing context for an employee, specifically intended for the case of administrators editing users.
        /// </summary>
        /// <param name="employee">The employee (B or P user) to create an editing context for.</param>
        /// <param name="isLqbEditor">A value indicating whether this context originated in the LQB editor.</param>
        /// <returns>An instance representing the data of the user specified by employee.</returns>
        public static ServiceCredentialContext CreateEditEmployee(EmployeeDB employee, bool isLqbEditor = false)
        {
            bool isPUser = employee.UserType == 'P';
            return new ServiceCredentialContext(
                brokerId: employee.BrokerID,
                branchId: default(Guid?),
                originatingCompanyId: isPUser ? employee.PmlBrokerId : default(Guid?),
                user: new UserContext(
                    isPUser: isPUser,
                    employeeId: employee.ID),
                isLqbEditor: isLqbEditor);
        }

        /// <summary>
        /// Creates a context for editing the originating company of the current user.
        /// </summary>
        /// <param name="principal">The user who is a member of the OC.  Note that if the user is not a P-user, this will throw.</param>
        /// <param name="isLqbEditor">A value indicating whether this context originated in the LQB editor.</param>
        /// <returns>An instance representing the data needed to edit an OC.</returns>
        /// <exception cref="DataAccess.CBaseException"><paramref name="principal"/> is not a P-user, and thus does not make sense to link to an OC.</exception>
        public static ServiceCredentialContext CreateEditOriginatingCompanyContextForUser(AbstractUserPrincipal principal, bool isLqbEditor = false)
        {
            if (!principal.IsPUser)
            {
                throw DataAccess.CBaseException.GenericException("Invalid user to edit originating company");
            }

            return new ServiceCredentialContext(
                brokerId: principal.BrokerId,
                branchId: default(Guid?),
                originatingCompanyId: principal.PmlBrokerId,
                user: null,
                isLqbEditor: isLqbEditor);
        }
    }
}
