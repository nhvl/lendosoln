﻿namespace LendersOffice.ObjLib.ServiceCredential
{
    using System.ComponentModel;
    using DataAccess;

    /// <summary>
    /// The AUS options for service credentials.
    /// </summary>
    public enum AusOption
    {
        /// <summary>
        /// Indicates that the user can use the credentials for Seamless DU and (non-seamless under some circumstances).
        /// </summary>
        [Description("Desktop Underwriter")]
        DesktopUnderwriter = 0,

        /// <summary>
        /// Credentials are for LPA user login.
        /// </summary>
        [Description("Seamless LPA User Login")]
        LpaUserLogin = 1,

        /// <summary>
        /// Credentials are for LPA seller login.
        /// </summary>
        [Description("Seamless LPA Lender Login")]
        LpaSellerCredential = 2
    }
}
