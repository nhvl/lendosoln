﻿namespace LendersOffice.ObjLib.ServiceCredential
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Runtime.Serialization;
    using CreditReport;
    using DataAccess;
    using Integration.VOXFramework;
    using LendersOffice.Common;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A service credential.
    /// </summary>
    [DataContract]
    public class ServiceCredential
    {
        /// <summary>
        /// Represents the display name for document capture service credentials.
        /// </summary>
        private const string DocumentCaptureDisplayName = "Capture";

        /// <summary>
        /// Whether the service credential is enabled for non-seamless DU.
        /// </summary>
        private bool? isEnabledForNonSeamlessDu;

        /// <summary>
        /// The broker Id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The branch Id.
        /// </summary>
        private Guid? branchId;

        /// <summary>
        /// The identifier of the originating company associated with this credential, or null if no association exists.<para/>
        /// This is as implicitly the value of <see cref="LendersOffice.Admin.PmlBroker.PmlBrokerId"/>.
        /// </summary>
        private Guid? originatingCompanyId;

        /// <summary>
        /// The employee id.
        /// </summary>
        private Guid? employeeId;

        /// <summary>
        /// Whether the credential is new.
        /// </summary>
        private bool isNew = false;

        /// <summary>
        /// The lazy decryption of the password.
        /// </summary>
        private Lazy<string> lazyPassword;

        /// <summary>
        /// The identifier for the encryption key used to encrypt <see cref="UserPassword"/>. 
        /// </summary>
        private EncryptionKeyIdentifier encryptionKeyId;

        /// <summary>
        /// Prevents a default instance of the <see cref="ServiceCredential"/> class from being created.
        /// </summary>
        private ServiceCredential()
        {
        }

        /// <summary>
        /// Gets identifier value.
        /// </summary>
        /// <value>The identifier value.</value>
        [DataMember]
        public int Id { get; private set; }

        /// <summary>
        /// Gets or sets a value indicating whether this service credential is configured for credit reports.
        /// </summary>
        /// <value>Whether this service credential is for credit.</value>
        [DataMember]
        public bool IsForCreditReports { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this service credential is configured for verifications.
        /// </summary>
        /// <value>Whether this service credential is configured for verifications.</value>
        [DataMember]
        public bool IsForVerifications { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this service credential is configured for AUS submission.
        /// </summary>
        /// <value>Whether this service credential is configured for AUS submission.</value>
        [DataMember]
        public bool IsForAusSubmission { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this service credential is configured for document capture.
        /// </summary>
        /// <value>Whether this service credential is configured for document capture.</value>
        [DataMember]
        public bool IsForDocumentCapture { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is for title quotes.
        /// </summary>
        /// <value>Whether this service credential is for title quotes.</value>
        [DataMember]
        public bool IsForTitleQuotes { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this service credential is configured for UCD delivery.
        /// </summary>
        /// <value>Whether this service credential is configured for UCD delivery.</value>
        [DataMember]
        public bool IsForUcdDelivery { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is for digital mortgage.
        /// </summary>
        /// <value>Whether this service credential is configured for digital mortgage.</value>
        [DataMember]
        public bool IsForDigitalMortgage { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this service credential is configured for non-seamless DU.
        /// </summary>
        /// <value>Whether this service credential is configured for non-seamless DU.</value>
        public bool IsEnabledForNonSeamlessDu
        {
            get
            {
                if (!this.ChosenAusOption.HasValue || this.ChosenAusOption.Value != ObjLib.ServiceCredential.AusOption.DesktopUnderwriter || !this.isEnabledForNonSeamlessDu.HasValue)
                {
                    return false;
                }

                return this.isEnabledForNonSeamlessDu.Value;
            }

            set
            {
                this.isEnabledForNonSeamlessDu = value;
            }
        }

        /// <summary>
        /// Gets or sets the AUS option set for this credential.
        /// </summary>
        /// <value>The chosen aus option.</value>
        public AusOption? ChosenAusOption { get; set; }

        /// <summary>
        /// Gets the AUS option as a friendly string.
        /// </summary>
        /// <value>The AUS option as a friendly string.</value>
        public string ChosenAusOptionAsString => this.ChosenAusOption?.GetDescription() ?? string.Empty;

        /// <summary>
        /// Gets or sets the associated VOX vendor id.
        /// </summary>
        /// <value>The associated VOX vendor id.</value>
        [DataMember]
        public int? VoxVendorId { get; set; }

        /// <summary>
        /// Gets or sets the associated VOX vendor name.
        /// </summary>
        /// <value>The associated VOX vendor name.</value>
        [DataMember]
        public string VoxVendorName { get; set; }

        /// <summary>
        /// Gets or sets the UCD delivery target entity for a UCD credential.
        /// </summary>
        /// <value>UCD delivery target.</value>
        [DataMember]
        public UcdDeliveryTargetOption? UcdDeliveryTarget { get; set; }

        /// <summary>
        /// Gets or sets the Digital mortgage provider target.
        /// </summary>
        [DataMember]
        public DigitalMortgageProvider? DigitalMortgageProviderTarget { get; set; }

        /// <summary>
        /// Gets the <see cref="UcdDeliveryTarget"/> value as a string for display.
        /// </summary>
        /// <value><see cref="UcdDeliveryTarget"/> as a string.</value>
        public string UcdDeliveryTargetAsString
        {
            get
            {
                if (this.UcdDeliveryTarget == UcdDeliveryTargetOption.FannieMaeUcdCollectionSolution)
                {
                    return "Fannie Mae";
                }
                else if (this.UcdDeliveryTarget == UcdDeliveryTargetOption.LcaSellerCredential)
                {
                    return "Freddie Mac, Lender";
                }
                else if (this.UcdDeliveryTarget == UcdDeliveryTargetOption.LcaUserLogin)
                {
                    return "Freddie Mac, User";
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Digital mortgage provider as string.
        /// </summary>       
        public string DigitalMortgageProviderAsString
        {
            get
            {
                return EnumUtilities.GetDescription(this.DigitalMortgageProviderTarget);
            }
        }

        /// <summary>
        /// Gets a list of enabled service type strings to display to the user.
        /// </summary>
        /// <value>A list of enabled service type strings.</value>
        [DataMember]
        public List<string> EnabledServiceTypesToDisplay
        {
            get
            {
                var services = new List<string>();
                if (this.IsForCreditReports)
                {
                    services.Add("Credit Reports");
                }

                if (this.IsForVerifications)
                {
                    services.Add("Verifications");
                }

                if (this.IsForUcdDelivery)
                {
                    services.Add("UCD Delivery");
                }

                if (this.IsForAusSubmission)
                {
                    services.Add("Automated Underwriting");
                }

                if (this.IsForTitleQuotes)
                {
                    services.Add("Title Quote Ordering");
                }
                
                if (this.IsForDocumentCapture)
                {
                    services.Add("Document Capture");
                }

                if (this.IsForDigitalMortgage)
                {
                    services.Add("Digital Mortgage");
                }

                return services;
            }
        }

        /// <summary>
        /// Gets or sets ServiceProviderId value.
        /// </summary>
        /// <value>The ServiceProviderId value.</value>
        [DataMember]
        public Guid? ServiceProviderId { get; set; }

        /// <summary>
        /// Gets or sets ServiceProviderName value.
        /// </summary>
        /// <value>The ServiceProviderName value.</value>
        [DataMember]
        public string ServiceProviderName { get; set; }

        /// <summary>
        /// Gets or sets the title quote lender service id.
        /// </summary>
        /// <value>The title quote lender service id.</value>
        [DataMember]
        public int? TitleQuoteLenderServiceId { get; set; }

        /// <summary>
        /// Gets or sets the title quote lender service name.
        /// </summary>
        /// <value>The title lender service name.</value>
        [DataMember]
        public string TitleQuoteLenderServiceName { get; set; }

        /// <summary>
        /// Gets or sets UserType value.
        /// </summary>
        /// <value>The UserType value.</value>
        public UserType UserType { get; set; }

        /// <summary>
        /// Gets or sets representative UserType value.
        /// </summary>
        /// <value>The UserType value.</value>
        [DataMember]
        public string UserTypeRep
        {
            get { return UserTypeToRep(UserType); }
            set { UserType = UserTypeRepToEnum(value); }
        }

        /// <summary>
        /// Gets or sets AccountId value.
        /// </summary>
        /// <value>The AccountId value.</value>
        [DataMember]
        public string AccountId { get; set; }

        /// <summary>
        /// Gets or sets UserName value.
        /// </summary>
        /// <value>The UserName value.</value>
        [DataMember]
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets user password value.
        /// </summary>
        /// <value>The user password.</value>
        public Sensitive<string> UserPassword
        {
            get { return this.lazyPassword?.Value; }
            set { this.lazyPassword = new Lazy<string>(() => value.Value); }
        }

        /// <summary>
        /// Gets or sets the Service Provider display name. Used for UI only.
        /// </summary>
        /// <value>Service Provider display name.</value>
        [DataMember]
        public string ServiceProviderDisplayName
        {
            get
            {
                if (this.IsForCreditReports)
                {
                    return this.ServiceProviderName;
                }
                else if (this.IsForVerifications)
                {
                    return this.VoxVendorName;
                }
                else if (this.IsForUcdDelivery)
                {
                    return this.UcdDeliveryTargetAsString;
                }
                else if (this.IsForAusSubmission)
                {
                    return this.ChosenAusOptionAsString;
                }
                else if (this.IsForTitleQuotes)
                {
                    return this.TitleQuoteLenderServiceName;
                }
                else if (this.IsForDocumentCapture)
                {
                    return DocumentCaptureDisplayName;
                }
                else if (this.IsForDigitalMortgage)
                {
                    return this.DigitalMortgageProviderAsString;
                }
                else
                {
                    return string.Empty;
                }
            }

            set
            {
            }
        }

        /// <summary>
        /// Gets a value indicating whether this is a user-level credential.
        /// </summary>
        /// <value>Whether this is a user-level credential.</value>
        public bool IsUserCredential
        {
            get
            {
                return this.employeeId.HasValue;
            }
        }

        /// <summary>
        /// Gets the associated employee id if this is a user credential, returning null otherwise.
        /// </summary>
        public Guid? AssociatedEmployeeId => this.IsUserCredential ? this.employeeId.Value : default(Guid?);

        /// <summary>
        /// Gets a value indicating whether this is an originating company-level credential.
        /// </summary>
        public bool IsOriginatingCompanyCredential => this.originatingCompanyId.HasValue;

        /// <summary>
        /// Gets a value indicating whether this is a branch-level credential.
        /// </summary>
        /// <value>Whether this is a branch-level credential.</value>
        public bool IsBranchCredential
        {
            get
            {
                return this.branchId.HasValue;
            }
        }

        /// <summary>
        /// Gets the associated branch id if this is a branch credential. Null otherwise.
        /// </summary>
        /// <value>The associated branch id if this is a branch credential.</value>
        public Guid? AssociatedBranchId
        {
            get
            {
                return this.IsBranchCredential ? this.branchId.Value : (Guid?)null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this is a lender-level credential.
        /// </summary>
        /// <value>Whether this is a lender-level credential.</value>
        public bool IsLenderCredential
        {
            get
            {
                return !this.IsUserCredential && !this.IsOriginatingCompanyCredential && !this.IsBranchCredential;
            }
        }

        /// <summary>
        /// Gets the associated lender id.
        /// </summary>
        /// <value>The associated lender id.</value>
        public Guid AssociatedBrokerId
        {
            get
            {
                return this.brokerId;
            }
        }

        /// <summary>
        /// Create a new credential to be associated with a broker.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>The service credential.</returns>
        public static ServiceCredential CreateNewBrokerCredental(Guid brokerId)
        {
            var result = new ServiceCredential();
            result.brokerId = brokerId;
            result.branchId = null;
            result.isNew = true;
            return result;
        }

        /// <summary>
        /// Create a new credential to be associated with a branch.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="branchId">The branch identifier.</param>
        /// <returns>The service credential.</returns>
        public static ServiceCredential CreateNewBranchCredental(Guid brokerId, Guid branchId)
        {
            var result = new ServiceCredential();
            result.brokerId = brokerId;
            result.branchId = branchId;
            result.isNew = true;
            return result;
        }

        /// <summary>
        /// Create a new credential to be associated with an originating company.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="originatingCompanyId">The originating company identifier, also known as <see cref="LendersOffice.Admin.PmlBroker.PmlBrokerId"/>.</param>
        /// <returns>The new service credential.</returns>
        public static ServiceCredential CreateNewOriginatingCompanyCredential(Guid brokerId, Guid originatingCompanyId)
        {
            var result = new ServiceCredential();
            result.brokerId = brokerId;
            result.originatingCompanyId = originatingCompanyId;
            result.isNew = true;
            return result;
        }

        /// <summary>
        /// Creates a new credential for an employee.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="employeeId">The employee id.</param>
        /// <returns>The new service credential.</returns>
        public static ServiceCredential CreateNewEmployeeCredential(Guid brokerId, Guid employeeId)
        {
            var result = new ServiceCredential();
            result.brokerId = brokerId;
            result.employeeId = employeeId;
            result.isNew = true;
            return result;
        }

        /// <summary>
        /// Load an existing service credential.
        /// </summary>
        /// <param name="id">The service credential Identifier.</param>
        /// <param name="brokerId">The broker identifier.</param>
        /// <returns>The service credential.</returns>
        public static ServiceCredential Load(int id, Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@Id", id),
                new SqlParameter("@BrokerId", brokerId),
            };

            var result = new ServiceCredential();

            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "SERVICE_CREDENTIAL_GetById", parameters))
            {
                if (reader.Read())
                {
                    result = LoadFromReader(reader, brokerId);
                }
                else
                {
                    throw new NotFoundException("Credential Not Found.", $"Credential Not Found: id:{id}, brokerid:{brokerId}");
                }
            }

            result.Id = id;
            result.brokerId = brokerId;

            return result;
        }

        /// <summary>
        /// Get all broker level service credentials.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="enabledServices">The services that the credentials should be enabled for.</param>
        /// <returns>The service credential enumerable.</returns>
        public static IEnumerable<ServiceCredential> GetAllBrokerServiceCredentials(Guid brokerId, ServiceCredentialService enabledServices)
        {
            var parameters = ConstructEnabledServicesSqlParameters(enabledServices);
            parameters.Add(new SqlParameter("@BrokerId", brokerId));

            var result = new List<ServiceCredential>();
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "SERVICE_CREDENTIAL_ListByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    result.Add(LoadFromReader(reader, brokerId));
                }
            }

            return result;
        }

        /// <summary>
        /// Get all branch service credentials.
        /// </summary>
        /// <param name="branchId">The branch identifier.</param>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="enabledServices">The services this credential should have enabled.</param>
        /// <returns>The service credential enumerable.</returns>
        public static IEnumerable<ServiceCredential> GetAllBranchServiceCredentials(Guid branchId, Guid brokerId, ServiceCredentialService enabledServices)
        {
            var parameters = ConstructEnabledServicesSqlParameters(enabledServices);
            parameters.Add(new SqlParameter("@BranchId", branchId));
            parameters.Add(new SqlParameter("@BrokerId", brokerId));

            var result = new List<ServiceCredential>();
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "SERVICE_CREDENTIAL_ListByBranchId", parameters))
            {
                while (reader.Read())
                {
                    result.Add(LoadFromReader(reader, brokerId));
                }
            }

            return result;
        }

        /// <summary>
        /// Get all originating company service credentials.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="originatingCompanyId">The originating company identifier.</param>
        /// <param name="enabledServices">The services this credential should have enabled.</param>
        /// <returns>The service credential enumerable.</returns>
        public static IEnumerable<ServiceCredential> GetAllOriginatingCompanyServiceCredentials(Guid brokerId, Guid originatingCompanyId, ServiceCredentialService enabledServices)
        {
            var parameters = ConstructEnabledServicesSqlParameters(enabledServices);
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@PmlBrokerId", originatingCompanyId));

            var result = new List<ServiceCredential>();
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "SERVICE_CREDENTIAL_ListByPmlBrokerId", parameters))
            {
                while (reader.Read())
                {
                    result.Add(LoadFromReader(reader, brokerId));
                }
            }

            return result;
        }

        /// <summary>
        /// Get all employee level service credentials.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="employeeId">The employee id.</param>
        /// <param name="enabledServices">The services this service credential should have enabled.</param>
        /// <returns>The service credential enumerable.</returns>
        public static IEnumerable<ServiceCredential> GetAllEmployeeServiceCredentials(Guid brokerId, Guid employeeId, ServiceCredentialService enabledServices)
        {
            var parameters = ConstructEnabledServicesSqlParameters(enabledServices);
            parameters.Add(new SqlParameter("@BrokerId", brokerId));
            parameters.Add(new SqlParameter("@EmployeeId", employeeId));

            var result = new List<ServiceCredential>();
            using (IDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "SERVICE_CREDENTIAL_ListByEmployeeId", parameters))
            {
                while (reader.Read())
                {
                    result.Add(LoadFromReader(reader, brokerId));
                }
            }

            return result;
        }

        /// <summary>
        /// Gets the service credentials editable for a given originating company in the TPO portal.
        /// </summary>
        /// <param name="context">The context for editing the credentials.</param>
        /// <returns>A set of service credentials.</returns>
        public static IEnumerable<ServiceCredential> GetTpoEditableOriginatingCompanyServiceCredentials(ServiceCredentialContext context)
        {
            if (!context.OriginatingCompanyId.HasValue)
            {
                return Enumerable.Empty<ServiceCredential>();
            }

            var allCredentials = GetAllOriginatingCompanyServiceCredentials(context.BrokerId, context.OriginatingCompanyId.Value, ServiceCredentialService.Verifications);
            var voxVendors = GetAvailableVoxVendorsForServiceCredentials(context);
            return allCredentials.Where(cred => !cred.IsForCreditReports && cred.IsForVerifications && cred.VoxVendorId.HasValue && voxVendors.ContainsKey(cred.VoxVendorId.Value));
        }

        /// <summary>
        /// Gets the service credentials editable for a given employee in the TPO portal.
        /// </summary>
        /// <param name="context">The context for editing the credentials.</param>
        /// <returns>A set of service credentials.</returns>
        public static IEnumerable<ServiceCredential> GetTpoEditableEmployeeServiceCredentials(ServiceCredentialContext context)
        {
            if (context.User == null)
            {
                return Enumerable.Empty<ServiceCredential>();
            }

            if (!context.User.IsPUser)
            {
                return GetAllEmployeeServiceCredentials(context.BrokerId, context.User.EmployeeId, ServiceCredentialService.All).Where(cred => cred.IsForCreditReports || cred.IsForVerifications);
            }

            var allCredentials = GetAllEmployeeServiceCredentials(context.BrokerId, context.User.EmployeeId, ServiceCredentialService.Verifications);
            var voxVendors = GetAvailableVoxVendorsForServiceCredentials(context);
            return allCredentials.Where(cred => !cred.IsForCreditReports && cred.IsForVerifications && cred.VoxVendorId.HasValue && voxVendors.ContainsKey(cred.VoxVendorId.Value));
        }

        /// <summary>
        /// List all available credentials matching the criteria for the given user's principal.
        /// </summary>
        /// <param name="principal">The principal of the user.</param>
        /// <param name="branchId">The branch identifier.</param>
        /// <param name="enabledServices">The services this service credential should have enabled.</param>
        /// <returns>The service credential enumerable.</returns>
        public static IEnumerable<ServiceCredential> ListAvailableServiceCredentials(AbstractUserPrincipal principal, Guid branchId, ServiceCredentialService enabledServices)
        {
            var userType = principal.Type.Equals("P", StringComparison.OrdinalIgnoreCase) ? UserType.TPO : UserType.LQB;
            return ListAvailableServiceCredentials(branchId, principal.EmployeeId, principal.PmlBrokerId, principal.BrokerId, userType, enabledServices);
        }

        /// <summary>
        /// List all available credentials matching the criteria.
        /// </summary>
        /// <param name="branchId">The branch identifier.</param>
        /// <param name="employeeId">The employee id.</param>
        /// <param name="originatingCompanyId">The identifier of the originating company.</param>
        /// <param name="brokerId">The broker identifier.</param>
        /// <param name="userType">The user type.</param>
        /// <param name="enabledServices">The services this service credential should have enabled.</param>
        /// <returns>The service credential enumerable.</returns>
        public static IEnumerable<ServiceCredential> ListAvailableServiceCredentials(Guid branchId, Guid employeeId, Guid originatingCompanyId, Guid brokerId, UserType userType, ServiceCredentialService enabledServices)
        {
            var employeeResults = GetAllEmployeeServiceCredentials(brokerId, employeeId, enabledServices);
            foreach (var credential in employeeResults)
            {
                yield return credential;
            }

            var originatingCompanyResults = userType == UserType.LQB ? Enumerable.Empty<ServiceCredential>() : GetAllOriginatingCompanyServiceCredentials(brokerId, originatingCompanyId, enabledServices);
            foreach (var credential in originatingCompanyResults)
            {
                yield return credential;
            }

            var branchResults = GetAllBranchServiceCredentials(branchId, brokerId, enabledServices).Where(credential => credential.UserType == userType || credential.UserType == UserType.Any);
            foreach (var credential in branchResults)
            {
                yield return credential;
            }

            var brokerResults = GetAllBrokerServiceCredentials(brokerId, enabledServices).Where(brokerCredential => (brokerCredential.UserType == userType || brokerCredential.UserType == UserType.Any));
            foreach (var credential in brokerResults)
            {
                yield return credential;
            }
        }

        /// <summary>
        /// Delete an existing service credential.
        /// </summary>
        /// <param name="id">The service credential Id.</param>
        /// <param name="brokerId">The broker Id.</param>
        /// <returns>True if a row was deleted.</returns>
        public static bool DeleteServiceCredential(int id, Guid brokerId)
        {
            var parameters = new SqlParameter[]
                {
                    new SqlParameter("@Id", id),
                    new SqlParameter("@BrokerId", brokerId)
                };

            return StoredProcedureHelper.ExecuteNonQuery(brokerId, "SERVICE_CREDENTIAL_DeleteCredential", 2, parameters) > 0;
        }

        /// <summary>
        /// Gets whether the current user has service credentials they can configure.
        /// </summary>
        /// <param name="context">The user context to check for configuration options.</param>
        /// <returns><c>true</c> if the user can configure services; <c>false</c> otherwise.</returns>
        public static bool HasAvailableServiceCredentialConfiguration(ServiceCredentialContext context)
        {
            if (!context.IsLqbEditor && (context.User?.IsPUser ?? false || context.OriginatingCompanyId.HasValue))
            {
                return GetAvailableVoxVendorsForServiceCredentials(context).Count > 0;
            }

            return true;
        }

        /// <summary>
        /// Gets a list of the available Credit vendors for a service credential.
        /// </summary>
        /// <param name="context">The context for editing the credentials.</param>
        /// <returns>The list of available vendors.</returns>
        public static IEnumerable<CRA> GetAvailableCreditVendorsForServiceCredentials(ServiceCredentialContext context)
        {
            if (!context.IsLqbEditor && (context.User?.IsPUser ?? false || context.OriginatingCompanyId.HasValue))
            {
                return Enumerable.Empty<CRA>();
            }

            return MasterCRAList.RetrieveAvailableCras(context.BrokerId).Where(cra => !cra.IsInternalOnly);
        }

        /// <summary>
        /// Gets the available vox vendors for a service credential.
        /// </summary>
        /// <param name="context">The context for editing the credentials.</param>
        /// <returns>The list of available vendors.</returns>
        public static Dictionary<int, LightVOXVendor> GetAvailableVoxVendorsForServiceCredentials(ServiceCredentialContext context)
        {
            var vendorsWithServices = VOXVendor.LoadLightVendorsWithServices().Where(vendor => vendor.Value.IsEnabled);
            var lenderServices = VOXLenderService.GetLightLenderServices(context.BrokerId, null);
            if (!context.IsLqbEditor && context.OriginatingCompanyId.HasValue)
            {
                lenderServices = lenderServices.Where(
                    lenderService => lenderService.CanBeUsedForCredentialsInTpo && VOXLenderService.ShouldAllowTpoAccess(lenderService, context.CachedOriginatingCompanyGroupIds));
            }

            HashSet<int> vendorsInLenderService = new HashSet<int>();
            foreach (var lenderService in lenderServices)
            {
                vendorsInLenderService.Add(lenderService.VendorId);
                vendorsInLenderService.Add(lenderService.ResellerId);
            }

            Dictionary<int, LightVOXVendor> vendors = new Dictionary<int, LightVOXVendor>();
            foreach (var lightVendor in vendorsWithServices)
            {
                if (!vendors.ContainsKey(lightVendor.Key) &&
                    vendorsInLenderService.Contains(lightVendor.Key) &&
                    lightVendor.Value.Services.Any(service => service.Value.IsADirectVendor || service.Value.IsAReseller))
                {
                    vendors.Add(lightVendor.Key, lightVendor.Value);
                }
            }

            return vendors;
        }

        /// <summary>
        /// Gets a value indicating whether the specified user can edit this in the TPO portal.
        /// </summary>
        /// <param name="context">The context for editing the credentials.</param>
        /// <returns><c>true</c> if the credential can be edited in the TPO portal; false otherwise.</returns>
        public bool CanEditInTpoPortal(ServiceCredentialContext context)
        {
            if (!context.IsLqbEditor && (context.User?.IsPUser ?? false || context.OriginatingCompanyId.HasValue))
            {
                if ((this.IsOriginatingCompanyCredential && this.originatingCompanyId != context.OriginatingCompanyId)
                    || (this.IsUserCredential && this.employeeId != context.User?.EmployeeId))
                {
                    return false;
                }

                return this.IsForVerifications
                    && !this.IsForCreditReports
                    && this.VoxVendorId.HasValue
                    && GetAvailableVoxVendorsForServiceCredentials(context).ContainsKey(this.VoxVendorId.Value);
            }

            return true;
        }

        /// <summary>
        /// Save the service credential.
        /// </summary>
        /// <returns>The id of the credential.</returns>
        public int Save()
        {
            this.ValidateData();

            if (this.encryptionKeyId == default(EncryptionKeyIdentifier))
            {
                this.encryptionKeyId = Drivers.Encryption.EncryptionHelper.GenerateNewKey();
            }

            byte[] passwordBytes = Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId, this.UserPassword.Value);

            var parameters = new List<SqlParameter>(
                        new SqlParameter[]
                        {
                                        new SqlParameter("@BrokerId", this.brokerId),
                                        new SqlParameter("@ServiceProviderId", this.ServiceProviderId),
                                        new SqlParameter("@ServiceProviderName", this.ServiceProviderName),
                                        new SqlParameter("@UserType", this.UserType),
                                        new SqlParameter("@AccountId", this.AccountId),
                                        new SqlParameter("@UserName", this.UserName),
                                        new SqlParameter("@UserPassword", passwordBytes ?? new byte[0]),
                                        new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value),
                                        new SqlParameter("@IsForCreditReports", this.IsForCreditReports),
                                        new SqlParameter("@IsForVerifications", this.IsForVerifications),
                                        new SqlParameter("@VoxVendorId", this.VoxVendorId),
                                        new SqlParameter("@VoxVendorName", this.VoxVendorName),
                                        new SqlParameter("@IsEnabledForNonSeamlessDu", this.IsEnabledForNonSeamlessDu),
                                        new SqlParameter("@IsForAusSubmission", this.IsForAusSubmission),
                                        new SqlParameter("@AusOption", this.ChosenAusOption),
                                        new SqlParameter("@IsForTitleQuotes", this.IsForTitleQuotes),
                                        new SqlParameter("@TitleQuoteLenderServiceId", this.TitleQuoteLenderServiceId),
                                        new SqlParameter("@TitleQuoteLenderServiceName", this.TitleQuoteLenderServiceName),
                                        new SqlParameter("@IsForUcdDelivery", this.IsForUcdDelivery),
                                        new SqlParameter("@UcdDeliveryTarget", this.UcdDeliveryTarget),
                                        new SqlParameter("@IsForDocumentCapture", this.IsForDocumentCapture),
                                        new SqlParameter("@IsForDigitalMortgage", this.IsForDigitalMortgage),
                                        new SqlParameter("@DigitalMortgageProviderTarget", this.DigitalMortgageProviderTarget)
                        });

            if (this.isNew)
            {
                string storedProcedure;
                if (this.employeeId.HasValue)
                {
                    storedProcedure = "SERVICE_CREDENTIAL_CreateEmployeeCredential";
                    parameters.Add(new SqlParameter("@EmployeeId", this.employeeId.Value));
                }
                else if (this.originatingCompanyId.HasValue)
                {
                    storedProcedure = "SERVICE_CREDENTIAL_CreatePmlBrokerCredential";
                    parameters.Add(new SqlParameter("@PmlBrokerId", this.originatingCompanyId.Value));
                }
                else if (this.branchId.HasValue)
                {
                    storedProcedure = "SERVICE_CREDENTIAL_CreateBranchCredential";
                    parameters.Add(new SqlParameter("@BranchId", this.branchId.Value));
                }
                else
                {
                    storedProcedure = "SERVICE_CREDENTIAL_CreateBrokerCredential";
                }

                var outputIdParam = new SqlParameter("@Id", System.Data.SqlDbType.Int);
                outputIdParam.Direction = System.Data.ParameterDirection.Output;
                parameters.Add(outputIdParam);

                StoredProcedureHelper.ExecuteNonQuery(this.brokerId, storedProcedure, 2, parameters);
                this.isNew = false;
                this.Id = (int)outputIdParam.Value;
                return this.Id;
            }
            else
            {
                parameters.Add(new SqlParameter("@Id", this.Id));
                StoredProcedureHelper.ExecuteNonQuery(this.brokerId, "SERVICE_CREDENTIAL_UpdateCredential", 2, parameters);
                return this.Id;
            }
        }

        /// <summary>
        /// Constructs the parameters for getting credentials based on the services they have enabled.
        /// </summary>
        /// <param name="enabledServices">The services that should be enabled for the service credential.</param>
        /// <returns>The parameters for the SPs.</returns>
        /// <remarks>
        /// <see cref="ServiceCredentialService"/> is a flags enum.  Specifying more than one flag means the stored procedure
        /// will select only service credentials where both flags are true.
        /// </remarks>
        private static List<SqlParameter> ConstructEnabledServicesSqlParameters(ServiceCredentialService enabledServices)
        {
            bool? isForCredit = null;
            bool? isForVerification = null;
            bool? isForAus = null;
            bool? isForTitleQuote = null;
            bool? isForUcdDelivery = null;
            bool? isForDocumentCapture = null;
            bool? isForDigitalMortgage = null;

            if (enabledServices != ServiceCredentialService.All)
            {
                isForCredit = enabledServices.HasFlag(ServiceCredentialService.CreditReports) ? true : (bool?)null;
                isForVerification = enabledServices.HasFlag(ServiceCredentialService.Verifications) ? true : (bool?)null;
                isForAus = enabledServices.HasFlag(ServiceCredentialService.AusSubmission) ? true : (bool?)null;
                isForTitleQuote = enabledServices.HasFlag(ServiceCredentialService.TitleQuotes) ? true : (bool?)null;
                isForUcdDelivery = enabledServices.HasFlag(ServiceCredentialService.UcdDelivery) ? true : (bool?)null;
                isForDocumentCapture = enabledServices.HasFlag(ServiceCredentialService.DocumentCapture) ? true : (bool?)null;
                isForDigitalMortgage = enabledServices.HasFlag(ServiceCredentialService.DigitalMortgage) ? true : (bool?)null;
            }

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@IsForCreditReports", isForCredit),
                new SqlParameter("@IsForVerifications", isForVerification),
                new SqlParameter("@IsForAusSubmission", isForAus),
                new SqlParameter("@IsForTitleQuotes", isForTitleQuote),
                new SqlParameter("@IsForUcdDelivery", isForUcdDelivery),
                new SqlParameter("@IsForDocumentCapture", isForDocumentCapture),
                new SqlParameter("@IsForDigitalMortgage", isForDigitalMortgage)
            };

            return parameters;
        }

        /// <summary>
        /// Load the service credential from a data reader.
        /// </summary>
        /// <param name="reader">A sql data reader.</param>
        /// <param name="brokerId">The broker id used.</param>
        /// <returns>The service credential.</returns>
        private static ServiceCredential LoadFromReader(IDataRecord reader, Guid brokerId)
        {
            var result = new ServiceCredential();
            result.Id = (int)reader["Id"];
            result.UserType = (UserType)(int)reader["UserType"];
            result.AccountId = reader["AccountId"].ToString();
            result.UserName = reader["UserName"].ToString();
            byte[] passwordBytes = (byte[])reader["UserPassword"];
            EncryptionKeyIdentifier encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)reader["EncryptionKeyId"]).ForceValue();
            result.lazyPassword = new Lazy<string>(() => Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyId, passwordBytes));
            result.encryptionKeyId = encryptionKeyId;
            result.IsForCreditReports = (bool)reader["IsForCreditReports"];
            result.IsForVerifications = (bool)reader["IsForVerifications"];
            result.IsForAusSubmission = (bool)reader["IsForAusSubmission"];
            result.IsForTitleQuotes = (bool)reader["IsForTitleQuotes"];
            result.IsForUcdDelivery = (bool)reader["IsForUcdDelivery"];
            result.IsForDocumentCapture = (bool)reader["IsForDocumentCapture"];
            result.IsForDigitalMortgage = (bool)reader["IsForDigitalMortgage"];
            result.brokerId = brokerId;
            result.employeeId = reader.AsNullableGuid("EmployeeId");
            result.originatingCompanyId = reader.AsNullableGuid("PmlBrokerId");
            result.branchId = reader.AsNullableGuid("BranchId");
            result.UcdDeliveryTarget = reader.GetNullableIntEnum<UcdDeliveryTargetOption>("UcdDeliveryTarget");
            result.DigitalMortgageProviderTarget = reader.GetNullableIntEnum<DigitalMortgageProvider>("DigitalMortgageProviderTarget");

            result.ServiceProviderId = reader.AsNullableGuid("ServiceProviderId");
            result.ServiceProviderName = reader.AsNullableString("ServiceProviderName");

            result.VoxVendorId = reader.AsNullableInt("VoxVendorId");
            result.VoxVendorName = reader.AsNullableString("VoxVendorName");

            result.ChosenAusOption = reader.GetNullableIntEnum<AusOption>("AusOption");
            result.isEnabledForNonSeamlessDu = reader.AsNullableBool("IsEnabledForNonSeamlessDu");

            result.TitleQuoteLenderServiceId = reader.AsNullableInt("TitleQuoteLenderServiceId");
            result.TitleQuoteLenderServiceName = reader.AsNullableString("TitleQuoteLenderServiceName");

            return result;
        }

        /// <summary>
        /// Map a user type to string for display.
        /// </summary>
        /// <param name="userType">The user type.</param>
        /// <returns>The string for display.</returns>
        private static string UserTypeToRep(UserType userType)
        {
            switch (userType)
            {
                case UserType.Any:
                    return "Any";
                case UserType.LQB:
                    return "LQB";
                case UserType.TPO:
                    return "TPO";
                default:
                    throw new UnhandledEnumException(userType);
            }
        }

        /// <summary>
        /// Map a user type string to enumeration.
        /// </summary>
        /// <param name="userType">The user type string description.</param>
        /// <returns>The enum mapped.</returns>
        private static UserType UserTypeRepToEnum(string userType)
        {
            switch (userType.ToLower())
            {
                case "any":
                    return UserType.Any;
                case "lqb":
                    return UserType.LQB;
                case "tpo":
                    return UserType.TPO;
                default:
                    throw new GenericUserErrorMessageException("Unknown user type: " + userType);
            }
        }

        /// <summary>
        /// Checks if more than one exclusive groups are selected.
        /// </summary>
        /// <returns>True if only one exclusive group is selected. False otherwise.</returns>
        private bool HasOnlyOneExclusiveGroup()
        {
            List<List<bool>> exclusiveGroups = new List<List<bool>>()
            {
                new List<bool>() { this.IsForCreditReports, this.IsForVerifications },
                new List<bool>() { this.IsForAusSubmission, this.IsForUcdDelivery },
                new List<bool>() { this.IsForTitleQuotes },
                new List<bool>() { this.IsForDocumentCapture }
            };

            return exclusiveGroups.Where(group => group.Any(selected => selected)).Count() <= 1;
        }

        /// <summary>
        /// Verify the data for save.
        /// </summary>
        private void ValidateData()
        {
            string errorMessage = string.Empty;
            var basicValidityChecks = new[]
            {
                new { IsValid = !string.IsNullOrEmpty(this.UserName), ErrorMsg = "Missing Username." },
                new { IsValid = !string.IsNullOrEmpty(this.UserPassword.Value) || (!this.IsForAusSubmission && this.IsForUcdDelivery && this.UcdDeliveryTarget == UcdDeliveryTargetOption.LcaSellerCredential), ErrorMsg = "Missing User password." },
                new { IsValid = this.IsForVerifications || this.IsForCreditReports || this.IsForAusSubmission || this.IsForTitleQuotes || this.IsForUcdDelivery || this.IsForDocumentCapture || this.IsForDigitalMortgage, ErrorMsg = "Please select at least one service." },
                new { IsValid = !this.IsForCreditReports || this.ServiceProviderId.HasValue, ErrorMsg = "Please select a CRA." },
                new { IsValid = !this.IsForVerifications || this.VoxVendorId.HasValue, ErrorMsg = "Please select a Vendor Provider." },
                new { IsValid = !this.IsForAusSubmission || this.ChosenAusOption.HasValue, ErrorMsg = "Please select an AUS option." },
                new { IsValid = !this.IsForTitleQuotes || this.TitleQuoteLenderServiceId.HasValue, ErrorMsg = "Please select a Title Quote Service Provider." },
                new { IsValid = !this.IsForUcdDelivery || this.UcdDeliveryTarget.HasValue, ErrorMsg = "Please select a UCD delivery option." },
                new { IsValid = !this.IsForDigitalMortgage || this.DigitalMortgageProviderTarget.HasValue, ErrorMsg = "Please select a Digital Mortgage Provider." },
                new { IsValid = !this.IsOriginatingCompanyCredential || this.UserType == UserType.TPO, ErrorMsg = "Originating Company credentials may only be applied to TPO users." },
            };

            foreach (var checks in basicValidityChecks)
            {
                if (!checks.IsValid)
                {
                    throw new CBaseException(checks.ErrorMsg, checks.ErrorMsg);
                }
            }

            if (!this.HasOnlyOneExclusiveGroup())
            {
                errorMessage = "The combination of services selected is invalid";
                throw new CBaseException(errorMessage, errorMessage);
            }

            IEnumerable<ServiceCredential> potentialDuplicates;
            if (this.employeeId.HasValue)
            {
                potentialDuplicates = ServiceCredential.GetAllEmployeeServiceCredentials(this.brokerId, this.employeeId.Value, ServiceCredentialService.All);
            }
            else if (this.originatingCompanyId.HasValue)
            {
                potentialDuplicates = ServiceCredential.GetAllOriginatingCompanyServiceCredentials(this.brokerId, this.originatingCompanyId.Value, ServiceCredentialService.All);
            }
            else if (this.branchId.HasValue)
            {
                potentialDuplicates = ServiceCredential.GetAllBranchServiceCredentials(this.branchId.Value, this.brokerId, ServiceCredentialService.All);
            }
            else
            {
                potentialDuplicates = ServiceCredential.GetAllBrokerServiceCredentials(this.brokerId, ServiceCredentialService.All);
            }

            if (this.IsForCreditReports && this.ServiceProviderId.HasValue && potentialDuplicates.Any(cred => cred.Id != this.Id && cred.IsForCreditReports && cred.UserType == this.UserType && cred.ServiceProviderId.HasValue && cred.ServiceProviderId.Value == this.ServiceProviderId.Value))
            {
                var msg = "There is an existing service credential for this CRA. Please edit that one instead of adding a duplicate.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForVerifications && this.VoxVendorId.HasValue && potentialDuplicates.Any(cred => cred.Id != this.Id && cred.IsForVerifications && cred.UserType == this.UserType && cred.VoxVendorId.HasValue && cred.VoxVendorId.Value == this.VoxVendorId.Value))
            {
                var msg = "There is an existing service credential for this Verification Provider. Please edit that one instead of adding a duplicate.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForAusSubmission && this.ChosenAusOption.HasValue && potentialDuplicates.Any(cred => cred.Id != this.Id && cred.IsForAusSubmission && cred.UserType == this.UserType && cred.ChosenAusOption.HasValue && cred.ChosenAusOption.Value == this.ChosenAusOption.Value))
            {
                var msg = "There is an existing service credential for this AUS option. Please edit that one instead of adding a duplicate.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForTitleQuotes && this.TitleQuoteLenderServiceId.HasValue && potentialDuplicates.Any(cred => cred.Id != this.Id && cred.IsForTitleQuotes && cred.UserType == this.UserType && cred.TitleQuoteLenderServiceId.HasValue && cred.TitleQuoteLenderServiceId.Value == this.TitleQuoteLenderServiceId.Value))
            {
                var msg = "There is an existing service credential for this Title Quote Service Provider. Please edit that one instead of adding a duplicate.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForUcdDelivery && this.UcdDeliveryTarget.HasValue && potentialDuplicates.Any(cred => cred.Id != this.Id && cred.IsForUcdDelivery && cred.UserType == this.UserType && cred.UcdDeliveryTarget.HasValue && cred.UcdDeliveryTarget.Value == this.UcdDeliveryTarget.Value))
            {
                var msg = "There is an existing service credential for this UCD Delivery option. Please edit that one instead of adding a duplicate.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForDocumentCapture && potentialDuplicates.Any(this.IsDuplicateDocumentCaptureCredential))
            {
                var msg = "There is an existing service credential for this capture option. Please edit that one instead of adding a duplicate.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForDigitalMortgage && this.DigitalMortgageProviderTarget.HasValue && potentialDuplicates.Any(cred => cred.Id != this.Id && cred.IsForDigitalMortgage && cred.UserType == this.UserType && cred.DigitalMortgageProviderTarget.Value == this.DigitalMortgageProviderTarget.Value))
            {
                var msg = "There is an existing service credential for this Digital Mortgage option. Please edit that one instead of adding a duplicate.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForCreditReports)
            {
                var masterCra = MasterCRAList.FindById(this.ServiceProviderId.Value, false);
                if (masterCra == null)
                {
                    var msg = "Invalid CRA chosen.";
                    throw new CBaseException(msg, msg);
                }

                if (this.IsForVerifications)
                {
                    if (!masterCra.VoxVendorId.HasValue || masterCra.VoxVendorId.Value != this.VoxVendorId.Value)
                    {
                        var msg = "The selected CRA and Verification Provider do not appear to be the same organization. If they are then please contact LendingQB support.";
                        throw new CBaseException(msg, msg);
                    }
                }
                else
                {
                    if (masterCra.VoxVendorId.HasValue && this.VoxVendorId.HasValue && masterCra.VoxVendorId.Value != this.VoxVendorId.Value)
                    {
                        var msg = "The selected CRA and Verification Provider do not appear to be the same organization. If they are then please contact LendingQB support.";
                        throw new CBaseException(msg, msg);
                    }
                }
            }

            if (this.IsForAusSubmission && this.ChosenAusOption == AusOption.LpaSellerCredential && this.IsUserCredential)
            {
                var msg = "LPA Lender Credentials are not available at the user level.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForUcdDelivery && this.UcdDeliveryTarget == UcdDeliveryTargetOption.LcaSellerCredential && this.IsUserCredential)
            {
                var msg = "Loan Closing Advisor Lender Credentials are not available at the user level.";
                throw new CBaseException(msg, msg);
            }

            if (this.IsForAusSubmission && this.IsForUcdDelivery
                && ((this.ChosenAusOption == AusOption.DesktopUnderwriter && this.UcdDeliveryTarget != UcdDeliveryTargetOption.FannieMaeUcdCollectionSolution)
                    || (this.ChosenAusOption == AusOption.LpaUserLogin && this.UcdDeliveryTarget != UcdDeliveryTargetOption.LcaUserLogin)
                    || (this.ChosenAusOption == AusOption.LpaSellerCredential && this.UcdDeliveryTarget != UcdDeliveryTargetOption.LcaSellerCredential)))
            {
                var msg = "Invalid combination of AUS/UCD Delivery options.";
                throw new CBaseException(msg, msg);
            }
        }
        
        /// <summary>
        /// Determines whether this service credential instance for document capture
        /// equals another document capture credential for document capture.
        /// </summary>
        /// <param name="other">
        /// The other document capture credential.
        /// </param>
        /// <returns>
        /// True if the instance is equal, false otherwise.
        /// </returns>
        private bool IsDuplicateDocumentCaptureCredential(ServiceCredential other)
        {
            return this.Id != other.Id &&
                other.IsForDocumentCapture &&
                (this.UserType == UserType.Any || other.UserType == UserType.Any || this.UserType == other.UserType) &&
                this.brokerId == other.brokerId &&
                ((!this.branchId.HasValue && !other.branchId.HasValue && !this.employeeId.HasValue && !other.employeeId.HasValue) ||
                (this.branchId.HasValue && other.branchId.HasValue && this.branchId.Value == other.branchId.Value) ||
                (this.employeeId.HasValue && other.employeeId.HasValue && this.employeeId.Value == other.employeeId.Value));
        }
    }
}
