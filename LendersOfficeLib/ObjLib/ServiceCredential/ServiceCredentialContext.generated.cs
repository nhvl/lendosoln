﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.ObjLib.ServiceCredential
{
    using System;

    /// <summary>
    /// A context for a service credential edit or load operation.
    /// </summary>
    public partial class ServiceCredentialContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceCredentialContext"/> class.
        /// </summary>
        /// <param name="brokerId">The identifier of the lender who owns the service credential.</param>
        /// <param name="branchId">The identifier of the branch for the current context, reading from <see cref="DataAccess.CPageData.sBranchId"/>. If this value is null, it means the context was generated without a branch.</param>
        /// <param name="originatingCompanyId">The identifier of the originating company for the current context, usually read from <see cref="LendersOffice.Security.AbstractUserPrincipal.PmlBrokerId"/>. If this value is null, it means the context was generated without an originating company, or that no suitable OC exists, as would be the case for a B-user.</param>
        /// <param name="user">The user context for this instance. If this value is null, it means the context was generated without a user.</param>
        /// <param name="isLqbEditor">A value indicating whether this context originated in the LQB editor.  This allows us to override various checks on what is available for a user.</param>
        public ServiceCredentialContext(Guid brokerId, Guid? branchId, Guid? originatingCompanyId, UserContext user, bool isLqbEditor)
        {
            this.BrokerId = brokerId;
            this.BranchId = branchId;
            this.OriginatingCompanyId = originatingCompanyId;
            this.User = user;
            this.IsLqbEditor = isLqbEditor;
        }

        /// <summary>
        /// Gets the identifier of the lender who owns the service credential.
        /// </summary>
        public Guid BrokerId { get; }

        /// <summary>
        /// Gets the identifier of the branch for the current context, reading from <see cref="DataAccess.CPageData.sBranchId"/>. If this value is null, it means the context was generated without a branch.
        /// </summary>
        public Guid? BranchId { get; }

        /// <summary>
        /// Gets the identifier of the originating company for the current context, usually read from <see cref="LendersOffice.Security.AbstractUserPrincipal.PmlBrokerId"/>. If this value is null, it means the context was generated without an originating company, or that no suitable OC exists, as would be the case for a B-user.
        /// </summary>
        public Guid? OriginatingCompanyId { get; }

        /// <summary>
        /// Gets the user context for this instance. If this value is null, it means the context was generated without a user.
        /// </summary>
        public UserContext User { get; }

        /// <summary>
        /// Gets a value indicating whether this context originated in the LQB editor.  This allows us to override various checks on what is available for a user.
        /// </summary>
        public bool IsLqbEditor { get; }
    }
}