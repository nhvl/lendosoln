﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Data;

namespace LendersOffice.CustomFormField
{
    public class FormFieldCategory
    {
        public int Id { get; private set; }
        public string Name { get; set; }

        private FormFieldCategory()
        {
        }
        public void Save()
        {
            if (Id == -1)
            {
                SqlParameter parm = new SqlParameter("@CategoryId", SqlDbType.Int);
                parm.Direction = ParameterDirection.Output;

                SqlParameter[] parameters = {
                                                parm,
                                                new SqlParameter("@CategoryName", Name)
                                            };

                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "CUSTOM_FORM_CreateFieldCategory", 3, parameters);
                this.Id = (int) parm.Value;
            }
            else
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@CategoryId", Id),
                                                new SqlParameter("@CategoryName", Name)
                                            };
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "CUSTOM_FORM_UpdateFieldCategory", 3, parameters);
            }
        }

        public static FormFieldCategory Create(string category)
        {
            if (string.IsNullOrEmpty(category))
            {
                return null;
            }
            FormFieldCategory field = new FormFieldCategory();
            field.Name = category;
            field.Id = -1;
            return field;
        }
        public static IEnumerable<FormFieldCategory> ListAll()
        {
            List<FormFieldCategory> list = new List<FormFieldCategory>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "CUSTOM_FORM_ListFieldCategory"))
            {
                while (reader.Read())
                {
                    FormFieldCategory category = new FormFieldCategory();
                    category.Id = (int)reader["CategoryId"];
                    category.Name = (string)reader["CategoryName"];
                    list.Add(category);
                }
            }
            return list;
        }

        public static FormFieldCategory DefaultCategory
        {
            get
            {
                // 5/27/2010 dd - TODO: Return a default category and cache it.
                foreach (var o in ListAll())
                {
                    if (o.Id == 0)
                    {
                        return o;
                    }
                }
                return null;
            }
        }
    }
}
