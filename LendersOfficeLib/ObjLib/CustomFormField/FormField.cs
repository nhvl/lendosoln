﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;


namespace LendersOffice.CustomFormField
{
    public class FormFieldDuplicateException : CBaseException
    {
        public FormFieldDuplicateException(string id)
            : base(id + " already existed.", id + " already existed.")
        {
        }
    }
    public class FormField
    {
        private static Dictionary<string, FormField> x_dictionary = null;
        private static DateTime x_lastUpdatedTime = new DateTime(2000, 1, 1);
        public string Id { get; private set; }
        public string FriendlyName { get; set; }
        public FormFieldCategory Category { get; set; }
        public bool IsReviewNeeded { get; set; }
        public bool IsBoolean { get; set; }
        public string Description { get; set; }
        public bool IsHidden { get; set; }

        private bool m_isNew = false;
        public void Save()
        {
            if (null == Category)
            {
                throw new CBaseException("Category is required.", "Category is required.");
            }
            SqlParameter[] parameters = {
                                            new SqlParameter("@FieldId", Id)
                                            , new SqlParameter("@FieldFriendlyName", FriendlyName)
                                            , new SqlParameter("@IsReviewNeeded", IsReviewNeeded)
                                            , new SqlParameter("@IsBoolean", IsBoolean)
                                            , new SqlParameter("@Description", Description)
                                            , new SqlParameter("@CategoryId", Category.Id)
                                            , new SqlParameter("@IsHidden", IsHidden)
                                        };
            string storedProcedureName = m_isNew ? "CUSTOM_FORM_AddFormField" : "CUSTOM_FORM_UpdateFormField";

            try
            {
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, storedProcedureName, 3, parameters);
            }
            catch (SqlException exc)
            {
                if (exc.Message.Contains("pk_CUSTOM_FORM_FIELD_LIST_FieldId"))
                {
                    throw new FormFieldDuplicateException(Id);
                }
                throw;
            }

            
        }

        public static void Delete(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                throw new CBaseException("FieldID cannot be blank.", "FieldID cannot be blank.");
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@FieldId", fieldId)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "CUSTOM_FORM_DeleteFormField", 3, parameters);

            // Clear out dictionary so it can be reloaded
            x_dictionary = null;
        }

        private static Dictionary<string, FormField> GetFormFieldDictionary()
        {
            if (null == x_dictionary || x_lastUpdatedTime.AddMinutes(5) < DateTime.Now)
            {
                // 5/21/2010 dd - Load all from database and cache for 5 minutes.
                Dictionary<string, FormField> newDictionary = new Dictionary<string, FormField>(StringComparer.OrdinalIgnoreCase);

                Dictionary<int, FormFieldCategory> dictionary = new Dictionary<int, FormFieldCategory>();
                foreach (FormFieldCategory category in FormFieldCategory.ListAll())
                {
                    dictionary.Add(category.Id, category);
                }

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "CUSTOM_FORM_ListFormFields"))
                {
                    while (reader.Read())
                    {
                        FormField field = new FormField();
                        field.Id = (string)reader["FieldId"];
                        field.FriendlyName = (string)reader["FieldFriendlyName"];
                        field.Category = dictionary[(int)reader["CategoryId"]];
                        field.IsReviewNeeded = (bool)reader["IsReviewNeeded"];
                        field.IsBoolean = (bool)reader["IsBoolean"];
                        field.Description = (string)reader["Description"];
                        field.IsHidden = (bool)reader["IsHidden"];
                        field.m_isNew = false;

                        newDictionary.Add(field.Id, field);
                    }
                }
                x_dictionary = newDictionary;
                x_lastUpdatedTime = DateTime.Now;                
            }
            return x_dictionary;
        }

        /// <summary>
        /// Attempts to get the friendly name for an enum value. Throws an exception if none exists.
        /// </summary>
        /// <typeparam name="T">Must be an enum, or else a CBaseException will be thrown.</typeparam>
        /// <param name="input">The enum value to convert</param>
        /// <returns>The friendly name of this value in the database.</returns>
        public static string GetFriendlyName<T>(T input) where T : struct, IConvertible
        {
            var name = typeof(T).Name;
            if (!typeof(T).IsEnum)
            {
                throw new CBaseException("Type " + name + " is not an enum!", "Type " + name + " is not an enum!");
            }

            //The name looks like E_<something>, we want to get rid of the E_.
            return GetFormFieldDictionary()[name.Substring(2) + ":" + Convert.ToInt32(input)].FriendlyName;
        }

        public static FormField RetrieveById(string id)
        {
            FormField ret = null;
            if (!GetFormFieldDictionary().TryGetValue(id, out ret))
            {
                ret = null;
            }
            return ret;
        }
        public static FormField Create(string fieldId)
        {
            if (string.IsNullOrEmpty(fieldId))
            {
                return null;
            }
            if (!PageDataUtilities.ContainsField(fieldId))
            {
                // 5/26/2010 dd - Only allow valid field id to be add.
                return null;
            }
            FormField field = new FormField();
            field.Id = fieldId;
            field.IsHidden = false;
            field.m_isNew = true;
            field.Description = fieldId;
            return field;
        }
        public static IEnumerable<FormField> ListAll()
        {

            List<FormField> list = new List<FormField>();

            foreach (var o in GetFormFieldDictionary())
            {
                list.Add(o.Value);
            }

            return list.OrderBy(o=> o.FriendlyName);
        }

        /// <summary>
        /// This will add a new field id to database. If field id does not existed in datalayer then it will be ignore.
        /// If field id is a Enumeration type then it automatically expand to all possible values.
        /// </summary>
        /// <param name="id"></param>
        public static bool AddNewFieldToDataBase(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return false;
            }
            E_PageDataFieldType fieldType;

            if (!PageDataUtilities.GetFieldType(id, out fieldType))
            {
                return false;
            }
            FormField formField = null;

            FormFieldCategory defaultCategory = FormFieldCategory.DefaultCategory;
            switch (fieldType)
            {
                case E_PageDataFieldType.String:
                case E_PageDataFieldType.Money:
                case E_PageDataFieldType.Percent:
                case E_PageDataFieldType.Ssn:
                case E_PageDataFieldType.Phone:
                case E_PageDataFieldType.Integer:
                case E_PageDataFieldType.DateTime:
                case E_PageDataFieldType.Decimal:
                    formField = Create(id);
                    break;
                case E_PageDataFieldType.Bool:
                    formField = Create(id);
                    formField.IsBoolean = true;
                    break;
                case E_PageDataFieldType.Enum:
                    formField = Create(id);
                    break;
                case E_PageDataFieldType.Unknown:
                    return false;
                default:
                    throw new UnhandledEnumException(fieldType);
            }

            formField.Category = defaultCategory;
            formField.IsReviewNeeded = true;
            formField.FriendlyName = id;
            if (fieldType == E_PageDataFieldType.Enum)
            {
                Type enumType;
                if (PageDataUtilities.GetFieldEnumType(id, out enumType))
                {
                    foreach (int o in Enum.GetValues(enumType))
                    {
                        string fieldId = id + ":" + o.ToString();
                        string description = id + " (" + Enum.GetName(enumType, o) + ")";
                        FormField f = new FormField();
                        f.m_isNew = true;
                        f.Id = fieldId;
                        f.FriendlyName = description;
                        f.Description = description;
                        f.Category = defaultCategory;
                        f.IsBoolean = true;
                        f.IsReviewNeeded = true;
                        f.IsHidden = false;
                        f.Save();
                    }
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Invalid Program State. FormField.AddNewFieldToDatabase. Id=" + id);
                }
            }
            else if (fieldType == E_PageDataFieldType.Bool)
            {
                FormField f = new FormField();
                f.m_isNew = true;
                f.Id = id + ":0";
                f.FriendlyName = id + " (No)";
                f.Description = f.FriendlyName;
                f.Category = defaultCategory;
                f.IsBoolean = true;
                f.IsReviewNeeded = true;
                f.IsHidden = false;
                f.Save();

                f = new FormField();
                f.m_isNew = true;
                f.Id = id + ":1";
                f.FriendlyName = id + " (Yes)";
                f.Description = f.FriendlyName;
                f.Category = defaultCategory;
                f.IsBoolean = true;
                f.IsReviewNeeded = true;
                f.IsHidden = false;
                f.Save();
            }
            else
            {
                formField.Save();
            }
            x_dictionary = null;
            return true;
        }
    }
}
