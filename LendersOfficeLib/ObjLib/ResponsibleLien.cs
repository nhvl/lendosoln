﻿namespace LendersOffice.ObjLib
{
    /// <summary>
    /// Enum that controls with which lien the debt will be paid off.
    /// </summary>
    public enum ResponsibleLien
    {
        /// <summary>
        /// Loan will be paid in this lien.
        /// </summary>
        ThisLienTransaction = 0,

        /// <summary>
        /// Loan will be paid in another lien.
        /// </summary>
        OtherLienTransaction = 1
    }
}
