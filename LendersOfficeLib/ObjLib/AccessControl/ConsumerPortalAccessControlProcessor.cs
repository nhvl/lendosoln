﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.ObjLib.Security;
using LendersOffice.Admin;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.Security
{
    public class ConsumerPortalAccessControlProcessor
    {

        public static AccessBinding Resolve(ConsumerUserPrincipal principal, LoanAccessInfo loanInfo)
        {
            bool canRead = false;
            bool canWrite = false;
            if (principal.BrokerId == loanInfo.BrokerId && CanConsumerAccessLoanFile(principal, loanInfo.LoanId)) 
            {
                canRead = true;
            }

            AccessBinding accessBinding = new AccessBinding(Guid.Empty, loanInfo.LoanId, canRead, canWrite);

            accessBinding.AddComment("ConsumerId=" + principal.ConsumerId);
            accessBinding.AddComment("LoanId=" + loanInfo.LoanId);

            return accessBinding; 
        }

        private static bool CanConsumerAccessLoanFile(ConsumerUserPrincipal consumer, Guid loanId)
        {
            Dictionary<Int64, Dictionary<Guid, bool>> consumerAccess = new Dictionary<long, Dictionary<Guid, bool>>();
            RetrieveConsumerAccessibleLoans(consumer, consumerAccess);

            return consumerAccess[consumer.ConsumerId].ContainsKey(loanId);
        }

        private static void RetrieveConsumerAccessibleLoans(ConsumerUserPrincipal consumer, Dictionary<long, Dictionary<Guid, bool>> dict)
        {
            Int64 consumerId = consumer.ConsumerId;

            if (!dict.ContainsKey(consumerId))
            {
                Dictionary<Guid, bool> entry = new Dictionary<Guid, bool>();
                SqlParameter[] parameters = { new SqlParameter("@ConsumerId", consumerId) };

                using (var reader = StoredProcedureHelper.ExecuteReader(consumer.BrokerId, "CP_RetrieveAccessibleLoansByConsumerId", parameters))
                {
                    while (reader.Read())
                    {
                        Guid loanId = (Guid)reader["sLId"];
                        if (!entry.ContainsKey(loanId))
                        {
                            entry.Add(loanId, true);
                        }
                    }
                }
                dict.Add(consumerId, entry);
            }
        }
    }
}
