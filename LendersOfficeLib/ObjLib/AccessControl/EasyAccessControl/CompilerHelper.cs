using System;
using System.Text;
using System.Reflection;

namespace LendersOffice.Security.EasyAccessControl
{
    public class LoadAssemblyHelper
    {
        private StringBuilder m_sb = new StringBuilder();

        public Assembly ResolveEventHandler(object sender,ResolveEventArgs args)
        {
            string assemblyName = args.Name.Substring(0, args.Name.IndexOf(","));
            foreach( Assembly assembly in  AppDomain.CurrentDomain.GetAssemblies() )
            {
                try
                {
                    string name = assembly.GetName().Name;
                    if( assembly.GetName().Name == assemblyName )
                        return assembly;
                }
                catch
                {
                }
            }
            m_sb.Append( "Can not find the assembly " + args.Name + "\n" );
            return null;
        }

        public string ErrorMessage
        {
            get { return m_sb.ToString(); }
        }

    }

}
