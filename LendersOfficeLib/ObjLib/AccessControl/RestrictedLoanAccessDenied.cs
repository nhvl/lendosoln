﻿namespace LendersOffice.Security
{
    /// <summary>
    /// An exception thrown when access is restricted to a particular loan file or set of loan files (for Generic Framework vendors, for example).
    /// </summary>
    public class RestrictedLoanAccessDenied : AccessDenied
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RestrictedLoanAccessDenied"/> class.
        /// </summary>
        /// <param name="binding">The access binding.</param>
        /// <param name="message">Exception message.</param>
        public RestrictedLoanAccessDenied(AccessBinding binding, string message) : base(binding, message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestrictedLoanAccessDenied"/> class.
        /// </summary>
        /// <param name="message">Exception message.</param>
        public RestrictedLoanAccessDenied(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestrictedLoanAccessDenied"/> class.
        /// </summary>
        /// <param name="userMessage">Exception message to display to the user.</param>
        /// <param name="developerMessage">Internal exception message for developers.</param>
        public RestrictedLoanAccessDenied(string userMessage, string developerMessage) : base(userMessage, developerMessage)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RestrictedLoanAccessDenied"/> class with default error messages.
        /// </summary>
        public RestrictedLoanAccessDenied() : base()
        {
        }
    }
}
