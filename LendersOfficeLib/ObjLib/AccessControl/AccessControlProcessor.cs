using System;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.Security
{

    /// <summary>
    /// Wrap access denial with specific class so we can
    /// filter crashes from denials.
    /// </summary>

    public class AccessDenied : CBaseException
    {

        #region ( Constructors )

        /// <summary>
        /// Construct default.
        /// </summary>

        public AccessDenied(AccessBinding aBinding, String message)
            : base(ErrorMessages.GenericAccessDenied, String.Format("Access denied ({0}).\r\n{1}", message, aBinding))
        {
            this.IsEmailDeveloper = false;
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public AccessDenied(String message)
            : base(ErrorMessages.GenericAccessDenied, String.Format("Access denied ({0}).", message))
        {
            this.IsEmailDeveloper = false;
        }

        public AccessDenied(string userMessage, string developerMessage)
            : base(userMessage, developerMessage)
        {
            this.IsEmailDeveloper = false;
        }

        /// <summary>
        /// Construct default.
        /// </summary>

        public AccessDenied()
            : base(ErrorMessages.GenericAccessDenied, ErrorMessages.GenericAccessDenied)
        {
            this.IsEmailDeveloper = false;
        }

        #endregion

    }

}
