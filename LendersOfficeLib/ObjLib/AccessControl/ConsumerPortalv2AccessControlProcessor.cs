﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.ObjLib.ConsumerPortal;

namespace LendersOffice.Security
{
    static class ConsumerPortalv2AccessControlProcessor
    {
        public static AccessBinding Resolve(ConsumerPortalUserPrincipal principal, Guid sLId)
        {
            bool canRead = false;
            bool canWrite = false;

            // 1. Check to see if the consumer is assign to a loan
            // 2. Loan is in open status.
            foreach (var linkedLoanApp in ConsumerPortalUser.GetLinkedLoanApps(principal.BrokerId, principal.Id))
            {
                if (linkedLoanApp.sLId == sLId)
                {
                    canRead = true;
                    if (linkedLoanApp.sStatusT == DataAccess.E_sStatusT.Lead_New)
                    {
                        // 9/29/2013 dd - Consumer can only edit loan in Lead New status.
                        canWrite = true;
                    }
                    else
                    {
                        // 10/28/2013 dd - If loan is not in Lead New status then check to make sure user provide
                        // a valid partial SSN to loan app before allow read.
                        if (principal.AllowViewSubmittedLoanId == sLId)
                        {
                            canRead = true;
                        }
                        else
                        {
                            canRead = false;
                        }
                    }
                    break;
                }
            }

            return new AccessBinding(Guid.Empty, sLId, canRead, canWrite);

        }
    }
}
