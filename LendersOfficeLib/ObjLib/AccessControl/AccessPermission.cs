using System;
using System.Collections.Generic;

namespace LendersOffice.Security
{
	/// <summary>
	/// Keep track of individual access rights that a user object
	/// may have on a given data/user object.
	/// </summary>

	public class AccessRights
	{


		#region ( Rights properties )

        public bool CanRead { get; set;}

        public bool CanWrite { get; set;}


        #endregion

		#region ( Constructors )

		/// <summary>
		/// Construct default.
		/// </summary>

        public AccessRights(bool bCanRead, bool bCanWrite)
		{
			// Initialize members.

			CanRead         = bCanRead;
			CanWrite        = bCanWrite;
		}

		#endregion

	}

	/// <summary>
	/// Bind rights to the endpoints of the access granted to
	/// an accessor/user for a given subject/data object.
	/// </summary>

	public class AccessBinding : AccessRights
	{
		/// <summary>
		/// Bind rights to the endpoints of the access
		/// granted to an accessor/user for a given
		/// subject/data object.
		/// </summary>
        private List<string> m_ticketList = new List<string>();


        private List<string> m_Notes = new List<string>();

		#region ( Control properties )

		public Guid AccessorObjId { get; private set;}

		public Guid SubjObjId { get; private set;}


		#endregion

		/// <summary>
		/// Append new note to our internal list for debugging.
		/// </summary>

		internal void AddComment( String sMessage )
		{
			// Append new discovery comment so we can better
			// debug why access control results occur.

			m_Notes.Add( sMessage );
		}

		/// <summary>
		/// Write out our binding as a string for debugging.
		/// </summary>

		public override string ToString()
		{
			// Build a description of this access binding.

			String s = String.Format( "\r\n"
				+ "Uid: {0}\r\n"
				+ "Oid: {1}\r\n"
				+ "(W): {2}\r\n"
				+ "(R): {3}\r\n"
				, AccessorObjId
				, SubjObjId
				, CanWrite
				, CanRead
				);

			if( m_Notes.Count > 0 )
			{
				for( int i = 0 ; i < m_Notes.Count ; ++i )
				{
					if( i > 0 )
					{
						s += "\r\n";
					}

					s += String.Format( "({0}): {1}" , i , m_Notes[ i ] );
				}
			}

			return s;
		}

		#region ( Constructors )

		public AccessBinding( Guid accessorObjId, Guid subjObjId, bool bCanRead, bool bCanWrite) : base( bCanRead , bCanWrite)
		{

			AccessorObjId = accessorObjId;
			SubjObjId     = subjObjId;
		}


		#endregion
    
    }
}