/// Author: David Dao

using System.IO.Compression;
using System.Web;
using DataAccess;

namespace LendersOffice.HttpModule
{
	public class HttpCompressionModule : AbstractHttpModule
	{
        protected override void Application_BeginRequest(HttpContext context) 
        {
            string acceptedTypes = context.Request.Headers.Get("Accept-Encoding");

            if (null == acceptedTypes)
                return;

            if (context.Response.ContentType == "text/html" && acceptedTypes.IndexOf("gzip") >= 0) 
            {

                if (IsRequireCompression(context.Request)) 
                {
                    context.Items["COMPRESSED"] = true;

                    context.Response.Filter = new GZipStream(context.Response.Filter, CompressionMode.Compress, true);

                    context.Response.AppendHeader("Content-Encoding", "gzip");
                }
            }

        }

        private bool IsRequireCompression(HttpRequest request) 
        {
            // 4/9/2008 dd - This is in beta mode, I do not want to compress all the page yet. Therefore I will only
            // compress few pages that currently generate large output.
            string path = request.Url.AbsolutePath.ToLower();

            if (path.IndexOf("/main/main.aspx") >= 0) 
            {
                if (request.Url.Query.ToLower().IndexOf("tabkey=result") >= 0) 
                {
                    // 4/9/2008 dd - We are only interest in the result page of PML.
                    return true;
                }
            }
            string[] compressPage = {
                                     //   "/los/admin/editbranch.aspx"
                                         "/los/brokeradmin/editpmluser.aspx"
                                        , "/los/brokeradmin/pmluserlist.aspx"
                                      //  , "/los/brokeradmin/pmluserlistresults.aspx" the flush in this page when writing csv breaks iis 7s
                                        , "/los/loanfind.aspx"
                                        , "/los/pipeline.aspx"
                                        , "/los/pricinggroup/pricinggroupedit.aspx"
                                        , "/los/reminders/taskeditor.aspx"
                                        , "/los/reports/loanreports.aspx"
                                        , "/los/reports/reportresult.aspx"
                                        , "/los/rolodex/rolodexlist.aspx"
                                        , "/los/rolodexlist.aspx"
                                        , "/los/template/loanproductfolder.aspx"
                                        , "/rateprice/pricepolicylist.aspx"
                                        , "/rateprice/pricepolicylistbyproduct.aspx"
                                        , "/reports/loanreporteditor.aspx"
                                        , "/template/createderivationjob.aspx"
                                        , "/underwriting/taskconditions.aspx"
                                        , "/loadmin/broker/userlist.aspx"
                                        , "/los/rateprice/helpers/policybuilder.aspx"
                                        , "/los/admin/editemployee.aspx"
                                        , "/newlos/forms/amortizationtable.aspx"
                                        , "/loadmin/sae/ratesheetversion.aspx"
                                        , "/los/brokeradmin/pmlcompanylist.aspx"
                                        , "/newlos/underwriting/conditions/taskconditions.aspx"
                                        , "/los/webservice/pricing.asmx"
                                        , "/loadmin/broker/workflow/editworkflowconfigitem.aspx"
                                        , "/loadmin/broker/workflow/editworkflowconfigfieldprotectionrule.aspx"
                                        , "/loadmin/broker/workflow/editworkflowconfigcustomvariable.aspx"
                                        , "/newlos/electronicdocs/doclist.aspx"
                                        , "/newlos/lefttreeframe.aspx"
                                        , "/newlos/forms/goodfaithestimate2010.aspx"
                                        , "/newlos/printlist.aspx"
                                        , "/newlos/borrowerinfo.aspx"
                                        , "/newlos/loaninfo.aspx"
                                        , "/newlos/lockdesk/brokerratelock.aspx"
                                        , "/newlos/forms/loan1003.aspx"
                                        , "/newlos/leftsummaryframe.aspx"
                                        , "/los/loanfind.aspx"
                                        , "/newlos/status/general.aspx"
                                        , "/main/mainnewui.aspx"
                                        , "/newlos/closer/settlementcharges.aspx"
                                        , "/newlos/bigloaninfo.aspx"
                                        , "/pml/anonymousstyleprovider.aspx"
                                        , "/pml/styleprovider.aspx"
                                    };

            if (path.IndexOf("/los/rolodex/rolodexlist.aspx") >= 0)
            {
                return request.Form["m_exportButton"] == null && request.Form["m_exportBottomBtn"] == null;
            }

            foreach (string page in compressPage) 
            {
                if (path.IndexOf(page) >= 0)
                    return true;
            }
            return false;
        }

	}
}
