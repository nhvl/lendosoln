/// Author: David Dao

using System;
using System.Diagnostics;
using System.Web;
using LendersOffice.Common;

namespace LendersOffice.HttpModule
{
	public abstract class AbstractHttpModule : IHttpModule
	{
        private delegate void ApplicationEventDelegate(HttpContext context);
        public virtual void Init(HttpApplication application) 
        {
            application.BeginRequest += new EventHandler(this.OnBeginRequest);
            application.EndRequest += new EventHandler(this.OnEndRequest);
            application.PostMapRequestHandler +=new EventHandler(this.PostMapRequest);
        }



        private void MapEvent(ApplicationEventDelegate function, object source) 
        {
            if (WebsiteUtilities.IsDatabaseOffline)
            {
                return;
            }
            if (null == source)
                return;

            string key = this.GetType().FullName + "_" + function.Method.Name;
            HttpContext context = ((HttpApplication) source).Context;

            if (!context.Items.Contains(key)) 
            {
                function(context);
                context.Items.Add(key, "TRUE");
            }
        }
        public virtual void Dispose() 
        {
        }

        private void OnBeginRequest(object source, EventArgs e) 
        {
            MapEvent(new ApplicationEventDelegate(this.Application_BeginRequest), source);
        }


        private void OnEndRequest(object source, EventArgs e) 
        {
            MapEvent(new ApplicationEventDelegate(this.Application_EndRequest), source);
        }

        private void PostMapRequest(object source, EventArgs e)
        {
            MapEvent(new ApplicationEventDelegate(this.Application_PostMapRequest), source);
        }
        protected virtual void Application_BeginRequest(HttpContext context) {}
        protected virtual void Application_EndRequest(HttpContext context) {}

        protected virtual void Application_PostMapRequest(HttpContext context) { }


	}
}
