﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using LendersOffice.AntiXss;
using System.Web.UI.WebControls;
using LendersOffice.Common;
using DataAccess;
using System.Text;

namespace LendersOffice.HttpModule
{
    public class AntiXssHttpModule : AbstractHttpModule
    {
        protected override void Application_PostMapRequest(HttpContext context)
        {
            if (context == null)
                return;

            Page page = context.Handler as Page;

            if (null == page)
                return;

            page.SaveStateComplete += Page_SaveStateComplete;
            base.Application_PostMapRequest(context);
        }

        private void Page_SaveStateComplete(object sender, EventArgs e)
        {
            PreRender(sender, e);
        }

        //private bool m_isSanitizeOutput = false;

        private void PreRender(object sender, EventArgs e)
        {
            // Scan all user controls and protected here.
            Page page = sender as Page;
            if (null == page)
                return;

            StringBuilder sb = new StringBuilder();

            bool isSanitizeOutput = false;

            if ((string)HttpContext.Current.Items["SanitizeOutput"] == "Yes")
            {
                isSanitizeOutput = true;
            }

            AntiXssProtection(page, sb, isSanitizeOutput);

            if (sb.Length > 0)
            {
                EmailUtilities.SendDavidCrossScriptingWarning(page.Request.RawUrl, sb.ToString());
            }
        }

        private void AppendError(StringBuilder sb, Control control, string propertyName, string value)
        {
            //sb.AppendLine(string.Format("[{0}] - ID={1}, Property={2}, Value={3}", control.GetType().FullName, control.ID, propertyName, value));
        }
        private void AntiXssProtection(Control control, StringBuilder sb, bool isSanitizeOutput)
        {
            if (null == control)
                return;

            string fullType = control.GetType().FullName;

            switch (fullType)
            {
                case "System.Web.UI.DesignerDataBoundLiteralControl":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.DesignerDataBoundLiteralControl) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlAnchor":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlAnchor) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlButton":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlButton) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlForm":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlForm) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlGenericControl":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlGenericControl) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlHead":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlHead) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlImage":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlImage) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputButton":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputButton) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputCheckBox":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputCheckBox) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputFile":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputFile) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputHidden":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputHidden) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputImage":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputImage) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputPassword":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputPassword) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputRadioButton":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputRadioButton) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputReset":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputReset) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputSubmit":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputSubmit) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlInputText":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlInputText) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlLink":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlLink) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlMeta":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlMeta) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlSelect":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlSelect) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlTable":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlTable) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlTableCell":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlTableCell) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlTableRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlTableRow) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlTextArea":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlTextArea) control);
                    break;
                case "System.Web.UI.HtmlControls.HtmlTitle":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.HtmlControls.HtmlTitle) control);
                    break;
                case "System.Web.UI.MasterPage":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.MasterPage) control);
                    break;
                case "System.Web.UI.Page":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.Page) control);
                    break;
                case "System.Web.UI.UserControl":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.UserControl) control);
                    break;
                case "System.Web.UI.WebControls.AccessDataSource":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.AccessDataSource) control);
                    break;
                case "System.Web.UI.WebControls.AdRotator":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.AdRotator) control);
                    break;
                case "System.Web.UI.WebControls.BulletedList":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.BulletedList) control);
                    break;
                case "System.Web.UI.WebControls.Button":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Button) control);
                    break;
                case "System.Web.UI.WebControls.Calendar":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Calendar) control);
                    break;
                case "System.Web.UI.WebControls.ChangePassword":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.ChangePassword) control);
                    break;
                case "System.Web.UI.WebControls.CheckBox":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.CheckBox) control);
                    break;
                case "System.Web.UI.WebControls.CheckBoxList":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.CheckBoxList) control);
                    break;
                case "System.Web.UI.WebControls.CompareValidator":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.CompareValidator) control);
                    break;
                case "System.Web.UI.WebControls.CompleteWizardStep":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.CompleteWizardStep) control);
                    break;
                case "System.Web.UI.WebControls.Content":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Content) control);
                    break;
                case "System.Web.UI.WebControls.CreateUserWizard":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.CreateUserWizard) control);
                    break;
                case "System.Web.UI.WebControls.CreateUserWizardStep":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.CreateUserWizardStep) control);
                    break;
                case "System.Web.UI.WebControls.CustomValidator":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.CustomValidator) control);
                    break;
                case "System.Web.UI.WebControls.DataControlFieldCell":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DataControlFieldCell) control);
                    break;
                case "System.Web.UI.WebControls.DataControlFieldHeaderCell":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DataControlFieldHeaderCell) control);
                    break;
                case "System.Web.UI.WebControls.DataControlLinkButton":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.LinkButton)control);
                    break;
                case "System.Web.UI.WebControls.DataGrid":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DataGrid) control);
                    break;
                case "System.Web.UI.WebControls.DataGridItem":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DataGridItem) control);
                    break;
                case "System.Web.UI.WebControls.DataList":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DataList) control);
                    break;
                case "System.Web.UI.WebControls.DataListItem":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DataListItem) control);
                    break;
                case "System.Web.UI.WebControls.DetailsView":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DetailsView) control);
                    break;
                case "System.Web.UI.WebControls.DetailsViewPagerRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DetailsViewPagerRow) control);
                    break;
                case "System.Web.UI.WebControls.DetailsViewRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DetailsViewRow) control);
                    break;
                case "System.Web.UI.WebControls.DropDownList":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.DropDownList) control);
                    break;
                case "System.Web.UI.WebControls.FileUpload":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.FileUpload) control);
                    break;
                case "System.Web.UI.WebControls.FormView":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.FormView) control);
                    break;
                case "System.Web.UI.WebControls.FormViewPagerRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.FormViewPagerRow) control);
                    break;
                case "System.Web.UI.WebControls.FormViewRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.FormViewRow) control);
                    break;
                case "System.Web.UI.WebControls.GridView":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.GridView) control);
                    break;
                case "System.Web.UI.WebControls.GridViewRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.GridViewRow) control);
                    break;
                case "System.Web.UI.WebControls.HiddenField":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.HiddenField) control);
                    break;
                case "System.Web.UI.WebControls.HyperLink":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.HyperLink) control);
                    break;
                case "System.Web.UI.WebControls.Image":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Image) control);
                    break;
                case "System.Web.UI.WebControls.ImageButton":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.ImageButton) control);
                    break;
                case "System.Web.UI.WebControls.ImageMap":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.ImageMap) control);
                    break;
                case "System.Web.UI.WebControls.Label":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Label) control);
                    break;
                case "System.Web.UI.WebControls.LinkButton":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.LinkButton) control);
                    break;
                case "System.Web.UI.WebControls.ListBox":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.ListBox) control);
                    break;
                case "System.Web.UI.WebControls.Literal":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Literal) control);
                    break;
                case "System.Web.UI.WebControls.Localize":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Localize) control);
                    break;
                case "System.Web.UI.WebControls.Login":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Login) control);
                    break;
                case "System.Web.UI.WebControls.LoginName":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.LoginName) control);
                    break;
                case "System.Web.UI.WebControls.LoginStatus":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.LoginStatus) control);
                    break;
                case "System.Web.UI.WebControls.Menu":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Menu) control);
                    break;
                case "System.Web.UI.WebControls.ObjectDataSource":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.ObjectDataSource) control);
                    break;
                case "System.Web.UI.WebControls.Panel":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Panel) control);
                    break;
                case "System.Web.UI.WebControls.PasswordRecovery":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.PasswordRecovery) control);
                    break;
                case "System.Web.UI.WebControls.RadioButton":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.RadioButton) control);
                    break;
                case "System.Web.UI.WebControls.RadioButtonList":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.RadioButtonList) control);
                    break;
                case "System.Web.UI.WebControls.RangeValidator":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.RangeValidator) control);
                    break;
                case "System.Web.UI.WebControls.RegularExpressionValidator":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.RegularExpressionValidator) control);
                    break;
                case "System.Web.UI.WebControls.Repeater":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Repeater) control);
                    break;
                case "System.Web.UI.WebControls.RepeaterItem":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.RepeaterItem)control);
                    break;
                case "System.Web.UI.WebControls.RequiredFieldValidator":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.RequiredFieldValidator) control);
                    break;
                case "System.Web.UI.WebControls.SiteMapDataSource":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.SiteMapDataSource) control);
                    break;
                case "System.Web.UI.WebControls.SiteMapNodeItem":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.SiteMapNodeItem) control);
                    break;
                case "System.Web.UI.WebControls.SiteMapPath":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.SiteMapPath) control);
                    break;
                case "System.Web.UI.WebControls.SqlDataSource":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.SqlDataSource) control);
                    break;
                case "System.Web.UI.WebControls.Substitution":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Substitution) control);
                    break;
                case "System.Web.UI.WebControls.Table":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Table) control);
                    break;
                case "System.Web.UI.WebControls.TableCell":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.TableCell) control);
                    break;
                case "System.Web.UI.WebControls.TableFooterRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.TableFooterRow) control);
                    break;
                case "System.Web.UI.WebControls.TableHeaderCell":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.TableHeaderCell) control);
                    break;
                case "System.Web.UI.WebControls.TableHeaderRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.TableHeaderRow) control);
                    break;
                case "System.Web.UI.WebControls.TableRow":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.TableRow) control);
                    break;
                case "System.Web.UI.WebControls.TemplatedWizardStep":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.TemplatedWizardStep) control);
                    break;
                case "System.Web.UI.WebControls.TextBox":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.TextBox) control);
                    break;
                case "System.Web.UI.WebControls.TreeView":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.TreeView) control);
                    break;
                case "System.Web.UI.WebControls.ValidationSummary":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.ValidationSummary) control);
                    break;
                case "System.Web.UI.WebControls.WebControl":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebControl) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.AppearanceEditorPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.AppearanceEditorPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.BehaviorEditorPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.BehaviorEditorPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.CatalogZone":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.CatalogZone) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.ConnectionsZone":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.ConnectionsZone) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.DeclarativeCatalogPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.DeclarativeCatalogPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.EditorZone":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.EditorZone) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.ErrorWebPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.ErrorWebPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.GenericWebPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.GenericWebPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.ImportCatalogPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.ImportCatalogPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.LayoutEditorPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.LayoutEditorPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.PageCatalogPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.PageCatalogPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.PropertyGridEditorPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.PropertyGridEditorPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.UnauthorizedWebPart":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.UnauthorizedWebPart) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.WebPartManager":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.WebPartManager) control);
                    break;
                case "System.Web.UI.WebControls.WebParts.WebPartZone":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WebParts.WebPartZone) control);
                    break;
                case "System.Web.UI.WebControls.Wizard":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Wizard) control);
                    break;
                case "System.Web.UI.WebControls.WizardStep":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.WizardStep) control);
                    break;
                case "System.Web.UI.WebControls.Xml":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.Xml) control);
                    break;
                case "System.Web.UI.WebControls.XmlDataSource":
                    Sanitize(sb, isSanitizeOutput, (System.Web.UI.WebControls.XmlDataSource)control);
                    break;






                case "MeridianLink.CommonControls.CommonDataGrid": 
                    Sanitize(sb, isSanitizeOutput, (MeridianLink.CommonControls.CommonDataGrid)control);
                    break;
                case "System.Web.UI.StaticPartialCachingControl":
                case "System.Web.UI.LiteralControl":
                case "Microsoft.Web.UI.WebControls.TabStrip":
                case "System.Web.UI.ResourceBasedLiteralControl":
                case "MeridianLink.CommonControls.StateDropDownList":
                case "MeridianLink.CommonControls.ZipcodeTextBox":
                case "MeridianLink.CommonControls.PhoneTextBox":
                case "MeridianLink.CommonControls.PercentTextBox":
                case "MeridianLink.CommonControls.SSNTextBox":
                case "MeridianLink.CommonControls.DateTextBox":
                case "MeridianLink.CommonControls.EncodedLiteral":
                case "MeridianLink.CommonControls.PassthroughLiteral":
                case "MeridianLink.CommonControls.EncodedLabel":
                case "MeridianLink.CommonControls.PassthroughLabel":
                case "MeridianLink.CommonControls.TimeTextBox":
                case "MeridianLink.CommonControls.ComboBox":
                case "MeridianLink.CommonControls.NoDoubleClickButton":
                case "MeridianLink.CommonControls.MoneyTextBox":
                case "System.Web.UI.WebControls.PlaceHolder":
                case "System.Web.UI.DataBoundLiteralControl":
                case "PriceMyLoan.UI.LoanProductResultControl":
                case "PriceMyLoan.UI.LoanProductResultControlNewUI":
                case "System.Web.UI.WebControls.DataGridLinkButton":
                case "LendersOfficeApp.common.MenuItem":
                case "Microsoft.Web.UI.WebControls.TreeView":
                case "System.Web.UI.HtmlControls.HtmlIframe":
                case "System.Web.UI.WebControls.ContentPlaceHolder":
                case "System.Web.UI.ScriptManager":
                    break;
                default:
                    if (control is System.Web.UI.UserControl)
                    {
                        // No-OP
                    }
                    else if (control is Page)
                    {
                        //Sanitize(sb, isSanitizeOutput, (Page)control);
                    }
                    else
                    {
                        if (control.Controls.Count == 0)
                        {
                            Tools.LogBug("Unhandle Control in XSS. Control=" + fullType);
                        }

                    }
                    break;
            }

            foreach (Control o in control.Controls)
            {
                AntiXssProtection(o, sb, isSanitizeOutput);
            }
        }

        private bool HasDangerousScript(string text)
        {
            // 2/24/2009 dd - Only detect for existing of both '<' and '>' in the same string. This method is need
            // in the intermediate phase. Once we always encoding this method is not need.
            if (string.IsNullOrEmpty(text))
                return false;

            bool hasOpenCharacter = false; // If string contains '<'
            bool hasCloseCharacter = false; // If string contains '>'
            
            foreach (char ch in text.ToCharArray())
            {
                if (ch == '<') hasOpenCharacter = true;
                else if (ch == '>') hasCloseCharacter = true;
                if (hasOpenCharacter && hasCloseCharacter)
                    return true;
            }
            return false;
        }


        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Literal control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (isSanitizeOutput)
            {
                if (control.Mode != System.Web.UI.WebControls.LiteralMode.PassThrough)
                {
                    // 2/17/2009 dd - Only encode literal text that does not contains HTML fragment. To have literal not encode, developer
                    // must explicit set Mode=Passthrough. (The use should be limited)
                    control.Text = AspxTools.HtmlString(control.Text);
                }
            }

        }

        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DataGrid control)
        {
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.Caption))
            {
                AppendError(sb, control, "Caption", control.Caption);
            }
            if (HasDangerousScript(control.DataKeyField))
            {
                AppendError(sb, control, "DataKeyField", control.DataKeyField);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }

            if (isSanitizeOutput)
            {
                foreach (System.Web.UI.WebControls.DataGridItem item in control.Items)
                {
                    if (item.ItemType == System.Web.UI.WebControls.ListItemType.AlternatingItem ||
                        item.ItemType == System.Web.UI.WebControls.ListItemType.Item)
                    {
                        foreach (TableCell cell in item.Cells)
                        {
                            if (cell.HasControls())
                            {
                                continue; // This cell contains other user control. Do not set Text property if it has child control.
                                // Setting text will wipe out all childs controls.
                            }
                            if (!string.IsNullOrEmpty(cell.Text))
                            {
                                if (HasDangerousScript(cell.Text))
                                {
                                    AppendError(sb, control, "Cell", cell.Text);
                                }
                                // 2/20/2009 dd - For unknown reason if the asp:BoundColumn is empty string or null, then .NET will set .Text="&nbsp;"
                                // Therefore if we see this text then skip encoding.
                                if (cell.Text != "&nbsp;")
                                {

                                    cell.Text = AspxTools.HtmlString(cell.Text);
                                }
                            }
                        }
                    }
                }
            }
        }





        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.DesignerDataBoundLiteralControl control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.Page control)
        {
            if (HasDangerousScript(control.ClientTarget))
            {
                AppendError(sb, control, "ClientTarget", control.ClientTarget);
            }
            if (HasDangerousScript(control.ErrorPage))
            {
                AppendError(sb, control, "ErrorPage", control.ErrorPage);
            }
            if (HasDangerousScript(control.MasterPageFile))
            {
                AppendError(sb, control, "MasterPageFile", control.MasterPageFile);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.Theme))
            {
                AppendError(sb, control, "Theme", control.Theme);
            }
            if (HasDangerousScript(control.StyleSheetTheme))
            {
                AppendError(sb, control, "StyleSheetTheme", control.StyleSheetTheme);
            }
            if (HasDangerousScript(control.ViewStateUserKey))
            {
                AppendError(sb, control, "ViewStateUserKey", control.ViewStateUserKey);
            }
            if (HasDangerousScript(control.ContentType))
            {
                AppendError(sb, control, "ContentType", control.ContentType);
            }
            if (HasDangerousScript(control.ResponseEncoding))
            {
                AppendError(sb, control, "ResponseEncoding", control.ResponseEncoding);
            }
            if (HasDangerousScript(control.Culture))
            {
                AppendError(sb, control, "Culture", control.Culture);
            }
            if (HasDangerousScript(control.UICulture))
            {
                AppendError(sb, control, "UICulture", control.UICulture);
            }
            if (HasDangerousScript(control.AppRelativeVirtualPath))
            {
                AppendError(sb, control, "AppRelativeVirtualPath", control.AppRelativeVirtualPath);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlForm control)
        {
            if (HasDangerousScript(control.Action))
            {
                AppendError(sb, control, "Action", control.Action);
            }
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.DefaultFocus))
            {
                AppendError(sb, control, "DefaultFocus", control.DefaultFocus);
            }
            if (HasDangerousScript(control.Enctype))
            {
                AppendError(sb, control, "Enctype", control.Enctype);
            }
            if (HasDangerousScript(control.Method))
            {
                AppendError(sb, control, "Method", control.Method);
            }
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
            if (HasDangerousScript(control.Target))
            {
                AppendError(sb, control, "Target", control.Target);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.LiteralControl control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.UserControl control)
        {
            if (HasDangerousScript(control.AppRelativeVirtualPath))
            {
                AppendError(sb, control, "AppRelativeVirtualPath", control.AppRelativeVirtualPath);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.MasterPage control)
        {
            if (HasDangerousScript(control.MasterPageFile))
            {
                AppendError(sb, control, "MasterPageFile", control.MasterPageFile);
            }
            if (HasDangerousScript(control.AppRelativeVirtualPath))
            {
                AppendError(sb, control, "AppRelativeVirtualPath", control.AppRelativeVirtualPath);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlAnchor control)
        {
            if (HasDangerousScript(control.HRef))
            {
                AppendError(sb, control, "HRef", control.HRef);
            }
            if (control.Controls.Count == 0 && HasDangerousScript(control.InnerHtml))
            {
                AppendError(sb, control, "InnerHtml", control.InnerHtml);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlButton control)
        {
            if (control.Controls.Count == 0 && HasDangerousScript(control.InnerHtml))
            {
                AppendError(sb, control, "InnerHtml", control.InnerHtml);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlGenericControl control)
        {
            if (HasDangerousScript(control.TagName))
            {
                AppendError(sb, control, "TagName", control.TagName);
            }
            if (control.Controls.Count == 0 && HasDangerousScript(control.InnerHtml))
            {
                AppendError(sb, control, "InnerHtml", control.InnerHtml);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlHead control)
        {
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
                if(isSanitizeOutput)
                {
                    control.Title = AspxTools.HtmlString(control.Title);
                }
            }
            if (HasDangerousScript(control.TagName))
            {
                AppendError(sb, control, "TagName", control.TagName);
            }

        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlImage control)
        {
            if (HasDangerousScript(control.Src))
            {
                AppendError(sb, control, "Src", control.Src);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputButton control)
        {
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputCheckBox control)
        {
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputFile control)
        {
            if (HasDangerousScript(control.Accept))
            {
                AppendError(sb, control, "Accept", control.Accept);
            }
            if (HasDangerousScript(control.Value))
            {
                AppendError(sb, control, "Value", control.Value);
            }
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputHidden control)
        {
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputImage control)
        {
            if (HasDangerousScript(control.Src))
            {
                AppendError(sb, control, "Src", control.Src);
            }
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputText control)
        {
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputPassword control)
        {
            if (HasDangerousScript(control.Value))
            {
                AppendError(sb, control, "Value", control.Value);
            }
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputRadioButton control)
        {
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
            if (HasDangerousScript(control.Value))
            {
                AppendError(sb, control, "Value", control.Value);
                if(isSanitizeOutput)
                {
                    control.Value = AspxTools.HtmlString(control.Value);
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputReset control)
        {
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlInputSubmit control)
        {
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlLink control)
        {
            if (HasDangerousScript(control.Href))
            {
                AppendError(sb, control, "Href", control.Href);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlMeta control)
        {
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlSelect control)
        {
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.DataTextField))
            {
                AppendError(sb, control, "DataTextField", control.DataTextField);
            }
            if (HasDangerousScript(control.DataValueField))
            {
                AppendError(sb, control, "DataValueField", control.DataValueField);
            }
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
            if (HasDangerousScript(control.Value))
            {
                AppendError(sb, control, "Value", control.Value);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlTable control)
        {
            if (HasDangerousScript(control.Align))
            {
                AppendError(sb, control, "Align", control.Align);
            }
            if (HasDangerousScript(control.BgColor))
            {
                AppendError(sb, control, "BgColor", control.BgColor);
            }
            if (HasDangerousScript(control.BorderColor))
            {
                AppendError(sb, control, "BorderColor", control.BorderColor);
            }
            if (HasDangerousScript(control.Height))
            {
                AppendError(sb, control, "Height", control.Height);
            }
            if (HasDangerousScript(control.Width))
            {
                AppendError(sb, control, "Width", control.Width);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlTableCell control)
        {
            
            if (control.Controls.Count == 0 && HasDangerousScript(control.InnerHtml))
            {
                AppendError(sb, control, "InnerHtml", control.InnerHtml);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlTableRow control)
        {
            if (HasDangerousScript(control.Align))
            {
                AppendError(sb, control, "Align", control.Align);
            }
            if (HasDangerousScript(control.BgColor))
            {
                AppendError(sb, control, "BgColor", control.BgColor);
            }
            if (HasDangerousScript(control.BorderColor))
            {
                AppendError(sb, control, "BorderColor", control.BorderColor);
            }
            if (HasDangerousScript(control.Height))
            {
                AppendError(sb, control, "Height", control.Height);
            }
            if (HasDangerousScript(control.VAlign))
            {
                AppendError(sb, control, "VAlign", control.VAlign);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlTextArea control)
        {
            if (HasDangerousScript(control.Name))
            {
                AppendError(sb, control, "Name", control.Name);
            }
            if (HasDangerousScript(control.Value))
            {
                AppendError(sb, control, "Value", control.Value);
            }
            if (control.Controls.Count == 0 && HasDangerousScript(control.InnerHtml))
            {
                AppendError(sb, control, "InnerHtml", control.InnerHtml);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.HtmlControls.HtmlTitle control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.SqlDataSource control)
        {
            if (HasDangerousScript(control.CacheKeyDependency))
            {
                AppendError(sb, control, "CacheKeyDependency", control.CacheKeyDependency);
            }
            if (HasDangerousScript(control.ConnectionString))
            {
                AppendError(sb, control, "ConnectionString", control.ConnectionString);
            }
            if (HasDangerousScript(control.DeleteCommand))
            {
                AppendError(sb, control, "DeleteCommand", control.DeleteCommand);
            }
            if (HasDangerousScript(control.FilterExpression))
            {
                AppendError(sb, control, "FilterExpression", control.FilterExpression);
            }
            if (HasDangerousScript(control.InsertCommand))
            {
                AppendError(sb, control, "InsertCommand", control.InsertCommand);
            }
            if (HasDangerousScript(control.OldValuesParameterFormatString))
            {
                AppendError(sb, control, "OldValuesParameterFormatString", control.OldValuesParameterFormatString);
            }
            if (HasDangerousScript(control.ProviderName))
            {
                AppendError(sb, control, "ProviderName", control.ProviderName);
            }
            if (HasDangerousScript(control.SelectCommand))
            {
                AppendError(sb, control, "SelectCommand", control.SelectCommand);
            }
            if (HasDangerousScript(control.SortParameterName))
            {
                AppendError(sb, control, "SortParameterName", control.SortParameterName);
            }
            if (HasDangerousScript(control.SqlCacheDependency))
            {
                AppendError(sb, control, "SqlCacheDependency", control.SqlCacheDependency);
            }
            if (HasDangerousScript(control.UpdateCommand))
            {
                AppendError(sb, control, "UpdateCommand", control.UpdateCommand);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.AccessDataSource control)
        {
            if (HasDangerousScript(control.ConnectionString))
            {
                AppendError(sb, control, "ConnectionString", control.ConnectionString);
            }
            if (HasDangerousScript(control.DataFile))
            {
                AppendError(sb, control, "DataFile", control.DataFile);
            }
            if (HasDangerousScript(control.ProviderName))
            {
                AppendError(sb, control, "ProviderName", control.ProviderName);
            }
            if (HasDangerousScript(control.SqlCacheDependency))
            {
                AppendError(sb, control, "SqlCacheDependency", control.SqlCacheDependency);
            }
            if (HasDangerousScript(control.CacheKeyDependency))
            {
                AppendError(sb, control, "CacheKeyDependency", control.CacheKeyDependency);
            }
            if (HasDangerousScript(control.DeleteCommand))
            {
                AppendError(sb, control, "DeleteCommand", control.DeleteCommand);
            }
            if (HasDangerousScript(control.FilterExpression))
            {
                AppendError(sb, control, "FilterExpression", control.FilterExpression);
            }
            if (HasDangerousScript(control.InsertCommand))
            {
                AppendError(sb, control, "InsertCommand", control.InsertCommand);
            }
            if (HasDangerousScript(control.OldValuesParameterFormatString))
            {
                AppendError(sb, control, "OldValuesParameterFormatString", control.OldValuesParameterFormatString);
            }
            if (HasDangerousScript(control.SelectCommand))
            {
                AppendError(sb, control, "SelectCommand", control.SelectCommand);
            }
            if (HasDangerousScript(control.SortParameterName))
            {
                AppendError(sb, control, "SortParameterName", control.SortParameterName);
            }
            if (HasDangerousScript(control.UpdateCommand))
            {
                AppendError(sb, control, "UpdateCommand", control.UpdateCommand);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebControl control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);

                if (isSanitizeOutput)
                {
                    // OPM 464695, 1/31/2018, ML
                    // CSS classes should only contain letters, numbers, underscores,
                    // and dashes. An inspection of all references to CssClass indicate
                    // we follow this convention, but just in case, we'll only
                    // encode the CssClass when we detect angle brackets.
                    control.CssClass = AspxTools.HtmlString(control.CssClass);
                }
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);

                if (isSanitizeOutput)
                {
                    // OPM 464695, 1/31/2018, ML
                    // Finding all references of the tooltip property indicates
                    // that all tooltips are controlled by us and use safe, static
                    // text. As such, we'll perform this sanitization only if we detect
                    // angle brackets, as other characters like parentheses are expected 
                    // in tooltips.
                    control.ToolTip = AspxTools.HtmlString(control.ToolTip);
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.AdRotator control)
        {
            if (HasDangerousScript(control.AdvertisementFile))
            {
                AppendError(sb, control, "AdvertisementFile", control.AdvertisementFile);
            }
            if (HasDangerousScript(control.AlternateTextField))
            {
                AppendError(sb, control, "AlternateTextField", control.AlternateTextField);
            }
            if (HasDangerousScript(control.ImageUrlField))
            {
                AppendError(sb, control, "ImageUrlField", control.ImageUrlField);
            }
            if (HasDangerousScript(control.KeywordFilter))
            {
                AppendError(sb, control, "KeywordFilter", control.KeywordFilter);
            }
            if (HasDangerousScript(control.NavigateUrlField))
            {
                AppendError(sb, control, "NavigateUrlField", control.NavigateUrlField);
            }
            if (HasDangerousScript(control.Target))
            {
                AppendError(sb, control, "Target", control.Target);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Label control)
        {
            if (HasDangerousScript(control.AssociatedControlID))
            {
                AppendError(sb, control, "AssociatedControlID", control.AssociatedControlID);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.BulletedList control)
        {
            if (HasDangerousScript(control.BulletImageUrl))
            {
                AppendError(sb, control, "BulletImageUrl", control.BulletImageUrl);
            }
            if (HasDangerousScript(control.SelectedValue))
            {
                AppendError(sb, control, "SelectedValue", control.SelectedValue);
            }
            if (HasDangerousScript(control.Target))
            {
                AppendError(sb, control, "Target", control.Target);
            }
            if (isSanitizeOutput)
            {
                control.Target = AspxTools.HtmlString(control.Target);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.DataTextField))
            {
                AppendError(sb, control, "DataTextField", control.DataTextField);
            }
            if (HasDangerousScript(control.DataTextFormatString))
            {
                AppendError(sb, control, "DataTextFormatString", control.DataTextFormatString);
            }
            if (HasDangerousScript(control.DataValueField))
            {
                AppendError(sb, control, "DataValueField", control.DataValueField);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }

            // Items are not controls in their own right; rather, they are used at render time to generate new controls.
            // Thus, we want to handle this as particular for Items of a BulletedList, rather than for ListItems of a general ListControl.
            foreach (ListItem item in control.Items)
            {
                if (HasDangerousScript(item.Text))
                {
                    AppendError(sb, control, "Text", item.Text); // This value is encoded in either an <li> or <a> tag, depending on DisplayMode
                }
                if (HasDangerousScript(item.Value))
                {
                    AppendError(sb, control, "Value", item.Value); // This is ignored for DisplayMode == Text; encoded in the <a> tag's href attribute for HyperLink; not sure for LinkButton.
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Button control)
        {
            if (HasDangerousScript(control.CommandName))
            {
                AppendError(sb, control, "CommandName", control.CommandName);
            }
            if (HasDangerousScript(control.CommandArgument))
            {
                AppendError(sb, control, "CommandArgument", control.CommandArgument);
            }
            if (HasDangerousScript(control.PostBackUrl))
            {
                AppendError(sb, control, "PostBackUrl", control.PostBackUrl);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.OnClientClick))
            {
                AppendError(sb, control, "OnClientClick", control.OnClientClick);

                if (isSanitizeOutput)
                {
                    // OnClientClick does not encode values. We manually encode
                    // any potentially dangerous values when used, however, so 
                    // it seems likely for this block to be hit.
                    control.OnClientClick = AspxTools.HtmlString(control.OnClientClick);
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Calendar control)
        {
            if (HasDangerousScript(control.Caption))
            {
                AppendError(sb, control, "Caption", control.Caption);
            }
            if (HasDangerousScript(control.NextMonthText))
            {
                AppendError(sb, control, "NextMonthText", control.NextMonthText);
            }
            if (HasDangerousScript(control.PrevMonthText))
            {
                AppendError(sb, control, "PrevMonthText", control.PrevMonthText);
            }
            if (HasDangerousScript(control.SelectMonthText))
            {
                AppendError(sb, control, "SelectMonthText", control.SelectMonthText);
            }
            if (HasDangerousScript(control.SelectWeekText))
            {
                AppendError(sb, control, "SelectWeekText", control.SelectWeekText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.ChangePassword control)
        {
            if (HasDangerousScript(control.CancelButtonImageUrl))
            {
                AppendError(sb, control, "CancelButtonImageUrl", control.CancelButtonImageUrl);
            }
            if (HasDangerousScript(control.CancelDestinationPageUrl))
            {
                AppendError(sb, control, "CancelDestinationPageUrl", control.CancelDestinationPageUrl);
            }
            if (HasDangerousScript(control.ChangePasswordButtonImageUrl))
            {
                AppendError(sb, control, "ChangePasswordButtonImageUrl", control.ChangePasswordButtonImageUrl);
            }
            if (HasDangerousScript(control.ChangePasswordFailureText))
            {
                AppendError(sb, control, "ChangePasswordFailureText", control.ChangePasswordFailureText);
            }
            if (HasDangerousScript(control.ChangePasswordTitleText))
            {
                AppendError(sb, control, "ChangePasswordTitleText", control.ChangePasswordTitleText);
            }
            if (HasDangerousScript(control.ConfirmNewPasswordLabelText))
            {
                AppendError(sb, control, "ConfirmNewPasswordLabelText", control.ConfirmNewPasswordLabelText);
            }
            if (HasDangerousScript(control.ConfirmPasswordCompareErrorMessage))
            {
                AppendError(sb, control, "ConfirmPasswordCompareErrorMessage", control.ConfirmPasswordCompareErrorMessage);
            }
            if (HasDangerousScript(control.ContinueButtonImageUrl))
            {
                AppendError(sb, control, "ContinueButtonImageUrl", control.ContinueButtonImageUrl);
            }
            if (HasDangerousScript(control.ContinueButtonText))
            {
                AppendError(sb, control, "ContinueButtonText", control.ContinueButtonText);
            }
            if (HasDangerousScript(control.ContinueDestinationPageUrl))
            {
                AppendError(sb, control, "ContinueDestinationPageUrl", control.ContinueDestinationPageUrl);
            }
            if (HasDangerousScript(control.CreateUserText))
            {
                AppendError(sb, control, "CreateUserText", control.CreateUserText);
            }
            if (HasDangerousScript(control.EditProfileText))
            {
                AppendError(sb, control, "EditProfileText", control.EditProfileText);
            }
            if (HasDangerousScript(control.HelpPageText))
            {
                AppendError(sb, control, "HelpPageText", control.HelpPageText);
            }
            if (HasDangerousScript(control.InstructionText))
            {
                AppendError(sb, control, "InstructionText", control.InstructionText);
            }
            if (HasDangerousScript(control.MembershipProvider))
            {
                AppendError(sb, control, "MembershipProvider", control.MembershipProvider);
            }
            if (HasDangerousScript(control.NewPasswordRegularExpressionErrorMessage))
            {
                AppendError(sb, control, "NewPasswordRegularExpressionErrorMessage", control.NewPasswordRegularExpressionErrorMessage);
            }
            if (HasDangerousScript(control.NewPasswordLabelText))
            {
                AppendError(sb, control, "NewPasswordLabelText", control.NewPasswordLabelText);
            }
            if (HasDangerousScript(control.NewPasswordRegularExpression))
            {
                AppendError(sb, control, "NewPasswordRegularExpression", control.NewPasswordRegularExpression);
            }
            if (HasDangerousScript(control.PasswordHintText))
            {
                AppendError(sb, control, "PasswordHintText", control.PasswordHintText);
            }
            if (HasDangerousScript(control.PasswordLabelText))
            {
                AppendError(sb, control, "PasswordLabelText", control.PasswordLabelText);
            }
            if (HasDangerousScript(control.PasswordRecoveryText))
            {
                AppendError(sb, control, "PasswordRecoveryText", control.PasswordRecoveryText);
            }
            if (HasDangerousScript(control.SuccessPageUrl))
            {
                AppendError(sb, control, "SuccessPageUrl", control.SuccessPageUrl);
            }
            if (HasDangerousScript(control.SuccessText))
            {
                AppendError(sb, control, "SuccessText", control.SuccessText);
            }
            if (HasDangerousScript(control.SuccessTitleText))
            {
                AppendError(sb, control, "SuccessTitleText", control.SuccessTitleText);
            }
            if (HasDangerousScript(control.UserName))
            {
                AppendError(sb, control, "UserName", control.UserName);
            }
            if (HasDangerousScript(control.UserNameLabelText))
            {
                AppendError(sb, control, "UserNameLabelText", control.UserNameLabelText);
            }
            if (HasDangerousScript(control.UserNameRequiredErrorMessage))
            {
                AppendError(sb, control, "UserNameRequiredErrorMessage", control.UserNameRequiredErrorMessage);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.TableRow control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.CheckBox control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);

                if (isSanitizeOutput)
                {
                    control.Text = AspxTools.HtmlString(control.Text);
                }
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.CheckBoxList control)
        {
            if (HasDangerousScript(control.DataTextField))
            {
                AppendError(sb, control, "DataTextField", control.DataTextField);
            }
            if (HasDangerousScript(control.DataTextFormatString))
            {
                AppendError(sb, control, "DataTextFormatString", control.DataTextFormatString);
            }
            if (HasDangerousScript(control.DataValueField))
            {
                AppendError(sb, control, "DataValueField", control.DataValueField);
            }
            if (HasDangerousScript(control.SelectedValue))
            {
                AppendError(sb, control, "SelectedValue", control.SelectedValue);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }

            // Items are not controls in their own right; rather, they are used at render time to generate new controls.
            // Thus, we want to handle this as particular for Items of a CheckBoxList, rather than for ListItems of a general ListControl.
            foreach (ListItem item in control.Items)
            {
                if (HasDangerousScript(item.Text))
                {
                    AppendError(sb, control, "Text", item.Text); // This value is not encoded, and is placed directly in a <label> tag

                    if (isSanitizeOutput)
                    {
                        item.Text = AspxTools.HtmlString(item.Text);
                    }
                }
                if (HasDangerousScript(item.Value))
                {
                    AppendError(sb, control, "Value", item.Value); // This is encoded in the <input> value attribute
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Table control)
        {
            if (HasDangerousScript(control.Caption))
            {
                AppendError(sb, control, "Caption", control.Caption);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.CompareValidator control)
        {
            if (isSanitizeOutput)
            {
                /* ejm opm 464967
                 * Same as RequiredFieldValidator.InitialValue
                 * Need to JS encode since these value are rendered in a script tag.
                 */
                control.ValueToCompare = AspxTools.JsStringUnquoted(control.ValueToCompare);
                control.ValidationGroup = AspxTools.JsStringUnquoted(control.ValidationGroup);

                // This one is a bit special. The ControlToCompare property needs to match the ID of another control on the page.
                // However, that ID is bound to be HTML encoded while we need to JS encode this property.
                // But we don't really need to worry about a mismatch for valid input since the IDs we use should result in the same string regardless of the encoding.
                control.ControlToCompare = AspxTools.JsStringUnquoted(control.ControlToCompare);
            }

            if (HasDangerousScript(control.ControlToCompare))
            {
                AppendError(sb, control, "ControlToCompare", control.ControlToCompare);
            }
            if (HasDangerousScript(control.ValueToCompare))
            {
                AppendError(sb, control, "ValueToCompare", control.ValueToCompare);
            }
            if (HasDangerousScript(control.AssociatedControlID))
            {
                AppendError(sb, control, "AssociatedControlID", control.AssociatedControlID);
            }
            if (HasDangerousScript(control.ControlToValidate))
            {
                AppendError(sb, control, "ControlToValidate", control.ControlToValidate);
            }
            if (HasDangerousScript(control.ErrorMessage))
            {
                AppendError(sb, control, "ErrorMessage", control.ErrorMessage);

                if (isSanitizeOutput)
                {
                    control.ErrorMessage = AspxTools.HtmlString(control.ErrorMessage);
                }
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);

                if (isSanitizeOutput)
                {
                    control.Text = AspxTools.HtmlString(control.Text);
                }
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.TemplatedWizardStep control)
        {
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.CompleteWizardStep control)
        {
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Content control)
        {
            if (HasDangerousScript(control.ContentPlaceHolderID))
            {
                AppendError(sb, control, "ContentPlaceHolderID", control.ContentPlaceHolderID);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Wizard control)
        {
            if (HasDangerousScript(control.CancelButtonImageUrl))
            {
                AppendError(sb, control, "CancelButtonImageUrl", control.CancelButtonImageUrl);
            }
            if (HasDangerousScript(control.CancelButtonText))
            {
                AppendError(sb, control, "CancelButtonText", control.CancelButtonText);
            }
            if (HasDangerousScript(control.CancelDestinationPageUrl))
            {
                AppendError(sb, control, "CancelDestinationPageUrl", control.CancelDestinationPageUrl);
            }
            if (HasDangerousScript(control.FinishCompleteButtonText))
            {
                AppendError(sb, control, "FinishCompleteButtonText", control.FinishCompleteButtonText);
            }
            if (HasDangerousScript(control.FinishDestinationPageUrl))
            {
                AppendError(sb, control, "FinishDestinationPageUrl", control.FinishDestinationPageUrl);
            }
            if (HasDangerousScript(control.FinishCompleteButtonImageUrl))
            {
                AppendError(sb, control, "FinishCompleteButtonImageUrl", control.FinishCompleteButtonImageUrl);
            }
            if (HasDangerousScript(control.FinishPreviousButtonText))
            {
                AppendError(sb, control, "FinishPreviousButtonText", control.FinishPreviousButtonText);
            }
            if (HasDangerousScript(control.FinishPreviousButtonImageUrl))
            {
                AppendError(sb, control, "FinishPreviousButtonImageUrl", control.FinishPreviousButtonImageUrl);
            }
            if (HasDangerousScript(control.StartNextButtonText))
            {
                AppendError(sb, control, "StartNextButtonText", control.StartNextButtonText);
            }
            if (HasDangerousScript(control.StartNextButtonImageUrl))
            {
                AppendError(sb, control, "StartNextButtonImageUrl", control.StartNextButtonImageUrl);
            }
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.StepNextButtonText))
            {
                AppendError(sb, control, "StepNextButtonText", control.StepNextButtonText);
            }
            if (HasDangerousScript(control.StepNextButtonImageUrl))
            {
                AppendError(sb, control, "StepNextButtonImageUrl", control.StepNextButtonImageUrl);
            }
            if (HasDangerousScript(control.StepPreviousButtonText))
            {
                AppendError(sb, control, "StepPreviousButtonText", control.StepPreviousButtonText);
            }
            if (HasDangerousScript(control.StepPreviousButtonImageUrl))
            {
                AppendError(sb, control, "StepPreviousButtonImageUrl", control.StepPreviousButtonImageUrl);
            }
            if (HasDangerousScript(control.SkipLinkText))
            {
                AppendError(sb, control, "SkipLinkText", control.SkipLinkText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.TableCell control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.CreateUserWizard control)
        {
            if (HasDangerousScript(control.Answer))
            {
                AppendError(sb, control, "Answer", control.Answer);
            }
            if (HasDangerousScript(control.AnswerLabelText))
            {
                AppendError(sb, control, "AnswerLabelText", control.AnswerLabelText);
            }
            if (HasDangerousScript(control.AnswerRequiredErrorMessage))
            {
                AppendError(sb, control, "AnswerRequiredErrorMessage", control.AnswerRequiredErrorMessage);
            }
            if (HasDangerousScript(control.CompleteSuccessText))
            {
                AppendError(sb, control, "CompleteSuccessText", control.CompleteSuccessText);
            }
            if (HasDangerousScript(control.ConfirmPasswordCompareErrorMessage))
            {
                AppendError(sb, control, "ConfirmPasswordCompareErrorMessage", control.ConfirmPasswordCompareErrorMessage);
            }
            if (HasDangerousScript(control.ConfirmPasswordLabelText))
            {
                AppendError(sb, control, "ConfirmPasswordLabelText", control.ConfirmPasswordLabelText);
            }
            if (HasDangerousScript(control.ConfirmPasswordRequiredErrorMessage))
            {
                AppendError(sb, control, "ConfirmPasswordRequiredErrorMessage", control.ConfirmPasswordRequiredErrorMessage);
            }
            if (HasDangerousScript(control.ContinueButtonImageUrl))
            {
                AppendError(sb, control, "ContinueButtonImageUrl", control.ContinueButtonImageUrl);
            }
            if (HasDangerousScript(control.ContinueButtonText))
            {
                AppendError(sb, control, "ContinueButtonText", control.ContinueButtonText);
            }
            if (HasDangerousScript(control.ContinueDestinationPageUrl))
            {
                AppendError(sb, control, "ContinueDestinationPageUrl", control.ContinueDestinationPageUrl);
            }
            if (HasDangerousScript(control.CreateUserButtonImageUrl))
            {
                AppendError(sb, control, "CreateUserButtonImageUrl", control.CreateUserButtonImageUrl);
            }
            if (HasDangerousScript(control.CreateUserButtonText))
            {
                AppendError(sb, control, "CreateUserButtonText", control.CreateUserButtonText);
            }
            if (HasDangerousScript(control.DuplicateEmailErrorMessage))
            {
                AppendError(sb, control, "DuplicateEmailErrorMessage", control.DuplicateEmailErrorMessage);
            }
            if (HasDangerousScript(control.DuplicateUserNameErrorMessage))
            {
                AppendError(sb, control, "DuplicateUserNameErrorMessage", control.DuplicateUserNameErrorMessage);
            }
            if (HasDangerousScript(control.EditProfileIconUrl))
            {
                AppendError(sb, control, "EditProfileIconUrl", control.EditProfileIconUrl);
            }
            if (HasDangerousScript(control.EditProfileText))
            {
                AppendError(sb, control, "EditProfileText", control.EditProfileText);
            }
            if (HasDangerousScript(control.EditProfileUrl))
            {
                AppendError(sb, control, "EditProfileUrl", control.EditProfileUrl);
            }
            if (HasDangerousScript(control.Email))
            {
                AppendError(sb, control, "Email", control.Email);
            }
            if (HasDangerousScript(control.EmailLabelText))
            {
                AppendError(sb, control, "EmailLabelText", control.EmailLabelText);
            }
            if (HasDangerousScript(control.EmailRegularExpression))
            {
                AppendError(sb, control, "EmailRegularExpression", control.EmailRegularExpression);
            }
            if (HasDangerousScript(control.EmailRegularExpressionErrorMessage))
            {
                AppendError(sb, control, "EmailRegularExpressionErrorMessage", control.EmailRegularExpressionErrorMessage);
            }
            if (HasDangerousScript(control.EmailRequiredErrorMessage))
            {
                AppendError(sb, control, "EmailRequiredErrorMessage", control.EmailRequiredErrorMessage);
            }
            if (HasDangerousScript(control.UnknownErrorMessage))
            {
                AppendError(sb, control, "UnknownErrorMessage", control.UnknownErrorMessage);
            }
            if (HasDangerousScript(control.HelpPageIconUrl))
            {
                AppendError(sb, control, "HelpPageIconUrl", control.HelpPageIconUrl);
            }
            if (HasDangerousScript(control.HelpPageText))
            {
                AppendError(sb, control, "HelpPageText", control.HelpPageText);
            }
            if (HasDangerousScript(control.HelpPageUrl))
            {
                AppendError(sb, control, "HelpPageUrl", control.HelpPageUrl);
            }
            if (HasDangerousScript(control.InstructionText))
            {
                AppendError(sb, control, "InstructionText", control.InstructionText);
            }
            if (HasDangerousScript(control.InvalidAnswerErrorMessage))
            {
                AppendError(sb, control, "InvalidAnswerErrorMessage", control.InvalidAnswerErrorMessage);
            }
            if (HasDangerousScript(control.InvalidEmailErrorMessage))
            {
                AppendError(sb, control, "InvalidEmailErrorMessage", control.InvalidEmailErrorMessage);
            }
            if (HasDangerousScript(control.InvalidPasswordErrorMessage))
            {
                AppendError(sb, control, "InvalidPasswordErrorMessage", control.InvalidPasswordErrorMessage);
            }
            if (HasDangerousScript(control.InvalidQuestionErrorMessage))
            {
                AppendError(sb, control, "InvalidQuestionErrorMessage", control.InvalidQuestionErrorMessage);
            }
            if (HasDangerousScript(control.MembershipProvider))
            {
                AppendError(sb, control, "MembershipProvider", control.MembershipProvider);
            }
            if (HasDangerousScript(control.PasswordHintText))
            {
                AppendError(sb, control, "PasswordHintText", control.PasswordHintText);
            }
            if (HasDangerousScript(control.PasswordLabelText))
            {
                AppendError(sb, control, "PasswordLabelText", control.PasswordLabelText);
            }
            if (HasDangerousScript(control.PasswordRegularExpression))
            {
                AppendError(sb, control, "PasswordRegularExpression", control.PasswordRegularExpression);
            }
            if (HasDangerousScript(control.PasswordRegularExpressionErrorMessage))
            {
                AppendError(sb, control, "PasswordRegularExpressionErrorMessage", control.PasswordRegularExpressionErrorMessage);
            }
            if (HasDangerousScript(control.PasswordRequiredErrorMessage))
            {
                AppendError(sb, control, "PasswordRequiredErrorMessage", control.PasswordRequiredErrorMessage);
            }
            if (HasDangerousScript(control.Question))
            {
                AppendError(sb, control, "Question", control.Question);
            }
            if (HasDangerousScript(control.QuestionLabelText))
            {
                AppendError(sb, control, "QuestionLabelText", control.QuestionLabelText);
            }
            if (HasDangerousScript(control.QuestionRequiredErrorMessage))
            {
                AppendError(sb, control, "QuestionRequiredErrorMessage", control.QuestionRequiredErrorMessage);
            }
            if (HasDangerousScript(control.SkipLinkText))
            {
                AppendError(sb, control, "SkipLinkText", control.SkipLinkText);
            }
            if (HasDangerousScript(control.UserName))
            {
                AppendError(sb, control, "UserName", control.UserName);
            }
            if (HasDangerousScript(control.UserNameLabelText))
            {
                AppendError(sb, control, "UserNameLabelText", control.UserNameLabelText);
            }
            if (HasDangerousScript(control.UserNameRequiredErrorMessage))
            {
                AppendError(sb, control, "UserNameRequiredErrorMessage", control.UserNameRequiredErrorMessage);
            }
            if (HasDangerousScript(control.CancelButtonImageUrl))
            {
                AppendError(sb, control, "CancelButtonImageUrl", control.CancelButtonImageUrl);
            }
            if (HasDangerousScript(control.CancelButtonText))
            {
                AppendError(sb, control, "CancelButtonText", control.CancelButtonText);
            }
            if (HasDangerousScript(control.CancelDestinationPageUrl))
            {
                AppendError(sb, control, "CancelDestinationPageUrl", control.CancelDestinationPageUrl);
            }
            if (HasDangerousScript(control.FinishCompleteButtonText))
            {
                AppendError(sb, control, "FinishCompleteButtonText", control.FinishCompleteButtonText);
            }
            if (HasDangerousScript(control.FinishDestinationPageUrl))
            {
                AppendError(sb, control, "FinishDestinationPageUrl", control.FinishDestinationPageUrl);
            }
            if (HasDangerousScript(control.FinishCompleteButtonImageUrl))
            {
                AppendError(sb, control, "FinishCompleteButtonImageUrl", control.FinishCompleteButtonImageUrl);
            }
            if (HasDangerousScript(control.FinishPreviousButtonText))
            {
                AppendError(sb, control, "FinishPreviousButtonText", control.FinishPreviousButtonText);
            }
            if (HasDangerousScript(control.FinishPreviousButtonImageUrl))
            {
                AppendError(sb, control, "FinishPreviousButtonImageUrl", control.FinishPreviousButtonImageUrl);
            }
            if (HasDangerousScript(control.StartNextButtonText))
            {
                AppendError(sb, control, "StartNextButtonText", control.StartNextButtonText);
            }
            if (HasDangerousScript(control.StartNextButtonImageUrl))
            {
                AppendError(sb, control, "StartNextButtonImageUrl", control.StartNextButtonImageUrl);
            }
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.StepNextButtonText))
            {
                AppendError(sb, control, "StepNextButtonText", control.StepNextButtonText);
            }
            if (HasDangerousScript(control.StepNextButtonImageUrl))
            {
                AppendError(sb, control, "StepNextButtonImageUrl", control.StepNextButtonImageUrl);
            }
            if (HasDangerousScript(control.StepPreviousButtonText))
            {
                AppendError(sb, control, "StepPreviousButtonText", control.StepPreviousButtonText);
            }
            if (HasDangerousScript(control.StepPreviousButtonImageUrl))
            {
                AppendError(sb, control, "StepPreviousButtonImageUrl", control.StepPreviousButtonImageUrl);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.CreateUserWizardStep control)
        {
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.CustomValidator control)
        {
            if (isSanitizeOutput)
            {
                control.ValidationGroup = AspxTools.JsStringUnquoted(control.ValidationGroup);
            }

            if (HasDangerousScript(control.ClientValidationFunction))
            {
                AppendError(sb, control, "ClientValidationFunction", control.ClientValidationFunction);

                if (isSanitizeOutput)
                {
                    control.ClientValidationFunction = AspxTools.JsStringUnquoted(control.ClientValidationFunction);
                }
            }
            if (HasDangerousScript(control.AssociatedControlID))
            {
                AppendError(sb, control, "AssociatedControlID", control.AssociatedControlID);
            }
            if (HasDangerousScript(control.ControlToValidate))
            {
                AppendError(sb, control, "ControlToValidate", control.ControlToValidate);
            }
            if (HasDangerousScript(control.ErrorMessage))
            {
                AppendError(sb, control, "ErrorMessage", control.ErrorMessage);

                if (isSanitizeOutput)
                {
                    control.ErrorMessage = AspxTools.HtmlString(control.ErrorMessage);
                }
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);

                if (isSanitizeOutput)
                {
                    control.Text = AspxTools.HtmlString(control.Text);
                }
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DataControlFieldCell control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DataControlFieldHeaderCell control)
        {
            if (HasDangerousScript(control.AbbreviatedText))
            {
                AppendError(sb, control, "AbbreviatedText", control.AbbreviatedText);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Image control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.ImageButton control)
        {
            if (HasDangerousScript(control.CommandName))
            {
                AppendError(sb, control, "CommandName", control.CommandName);
            }
            if (HasDangerousScript(control.CommandArgument))
            {
                AppendError(sb, control, "CommandArgument", control.CommandArgument);
            }
            if (HasDangerousScript(control.PostBackUrl))
            {
                AppendError(sb, control, "PostBackUrl", control.PostBackUrl);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.OnClientClick))
            {
                AppendError(sb, control, "OnClientClick", control.OnClientClick);

                if (isSanitizeOutput)
                {
                    // OnClientClick does not encode values. We manually encode
                    // any potentially dangerous  values when used, however, so 
                    // it seems likely for this block to be hit.
                    control.OnClientClick = AspxTools.HtmlString(control.OnClientClick);
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.LinkButton control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.PostBackUrl))
            {
                AppendError(sb, control, "PostBackUrl", control.PostBackUrl);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.OnClientClick))
            {
                AppendError(sb, control, "OnClientClick", control.OnClientClick);

                if (isSanitizeOutput)
                {
                    // OnClientClick does not encode values. We manually encode
                    // any potentially dangerous  values when used, however, so 
                    // it seems likely for this block to be hit.
                    control.OnClientClick = AspxTools.HtmlString(control.OnClientClick);
                }
            }

            if (isSanitizeOutput)
            {
                if (!control.HasControls())
                {
                    // Only override the text when the control has no children.
                    control.Text = AspxTools.HtmlString(control.Text);
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DataGridItem control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DataList control)
        {
            if (HasDangerousScript(control.Caption))
            {
                AppendError(sb, control, "Caption", control.Caption);
            }
            if (HasDangerousScript(control.DataKeyField))
            {
                AppendError(sb, control, "DataKeyField", control.DataKeyField);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DataListItem control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DetailsView control)
        {
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.Caption))
            {
                AppendError(sb, control, "Caption", control.Caption);
            }
            if (HasDangerousScript(control.EmptyDataText))
            {
                AppendError(sb, control, "EmptyDataText", control.EmptyDataText);
            }
            if (HasDangerousScript(control.FooterText))
            {
                AppendError(sb, control, "FooterText", control.FooterText);
            }
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DetailsViewRow control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DetailsViewPagerRow control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.DropDownList control)
        {
            if (HasDangerousScript(control.DataTextField))
            {
                AppendError(sb, control, "DataTextField", control.DataTextField);
            }
            if (HasDangerousScript(control.DataTextFormatString))
            {
                AppendError(sb, control, "DataTextFormatString", control.DataTextFormatString);
            }
            if (HasDangerousScript(control.DataValueField))
            {
                AppendError(sb, control, "DataValueField", control.DataValueField);
            }
            if (HasDangerousScript(control.SelectedValue))
            {
                AppendError(sb, control, "SelectedValue", control.SelectedValue);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }

            // Items are not controls in their own right; rather, they are used at render time to generate new controls.
            // Thus, we want to handle this as particular for Items of a DropDownList, rather than for ListItems of a general ListControl.
            foreach (ListItem item in control.Items)
            {
                if (HasDangerousScript(item.Text))
                {
                    AppendError(sb, control, "Text", item.Text); // This value is encoded in the <option> tag
                }
                if (HasDangerousScript(item.Value))
                {
                    AppendError(sb, control, "Value", item.Value); // This is encoded in the <option> value attribute
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.FileUpload control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.FormView control)
        {
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.Caption))
            {
                AppendError(sb, control, "Caption", control.Caption);
            }
            if (HasDangerousScript(control.EmptyDataText))
            {
                AppendError(sb, control, "EmptyDataText", control.EmptyDataText);
            }
            if (HasDangerousScript(control.FooterText))
            {
                AppendError(sb, control, "FooterText", control.FooterText);
            }
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.FormViewRow control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.FormViewPagerRow control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.GridView control)
        {
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.Caption))
            {
                AppendError(sb, control, "Caption", control.Caption);
            }
            if (HasDangerousScript(control.EmptyDataText))
            {
                AppendError(sb, control, "EmptyDataText", control.EmptyDataText);
            }
            if (HasDangerousScript(control.RowHeaderColumn))
            {
                AppendError(sb, control, "RowHeaderColumn", control.RowHeaderColumn);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }

            if (isSanitizeOutput)
            {
                foreach (System.Web.UI.WebControls.GridViewRow item in control.Rows)
                {
                    if (item.RowType == System.Web.UI.WebControls.DataControlRowType.DataRow)
                    {
                        foreach (TableCell cell in item.Cells)
                        {
                            if (cell.HasControls() ||
                                (cell is DataControlFieldCell && ((DataControlFieldCell)cell).ContainingField is TemplateField))
                            {
                                // This cell contains other user control or is part of a template.
                                // Setting text will wipe out all childs controls and its content.
                                continue;
                            }
                            if (!string.IsNullOrEmpty(cell.Text))
                            {
                                if (HasDangerousScript(cell.Text))
                                {
                                    AppendError(sb, control, "Cell", cell.Text);

                                    // 2/20/2009 dd - For unknown reason if the asp:BoundColumn is empty string or null, then .NET will set .Text="&nbsp;"
                                    // Therefore if we see this text then skip encoding.
                                    if (cell.Text != "&nbsp;")
                                    {
                                        cell.Text = AspxTools.HtmlString(cell.Text);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.GridViewRow control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.HiddenField control)
        {
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.HyperLink control)
        {
            if (HasDangerousScript(control.ImageUrl))
            {
                AppendError(sb, control, "ImageUrl", control.ImageUrl);
            }
            if (HasDangerousScript(control.NavigateUrl))
            {
                AppendError(sb, control, "NavigateUrl", control.NavigateUrl);
            }
            if (HasDangerousScript(control.Target))
            {
                AppendError(sb, control, "Target", control.Target);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.ImageMap control)
        {
            if (HasDangerousScript(control.Target))
            {
                AppendError(sb, control, "Target", control.Target);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.ListBox control)
        {
            if (HasDangerousScript(control.DataTextField))
            {
                AppendError(sb, control, "DataTextField", control.DataTextField);
            }
            if (HasDangerousScript(control.DataTextFormatString))
            {
                AppendError(sb, control, "DataTextFormatString", control.DataTextFormatString);
            }
            if (HasDangerousScript(control.DataValueField))
            {
                AppendError(sb, control, "DataValueField", control.DataValueField);
            }
            if (HasDangerousScript(control.SelectedValue))
            {
                AppendError(sb, control, "SelectedValue", control.SelectedValue);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }

            // Items are not controls in their own right; rather, they are used at render time to generate new controls.
            // Thus, we want to handle this as particular for Items of a ListBox, rather than for ListItems of a general ListControl.
            foreach (ListItem item in control.Items)
            {
                if (HasDangerousScript(item.Text))
                {
                    AppendError(sb, control, "Text", item.Text); // This value is encoded in the <option> tag
                }
                if (HasDangerousScript(item.Value))
                {
                    AppendError(sb, control, "Value", item.Value); // This is encoded in the <option> value attribute
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Localize control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Login control)
        {
            if (HasDangerousScript(control.CreateUserText))
            {
                AppendError(sb, control, "CreateUserText", control.CreateUserText);
            }
            if (HasDangerousScript(control.DestinationPageUrl))
            {
                AppendError(sb, control, "DestinationPageUrl", control.DestinationPageUrl);
            }
            if (HasDangerousScript(control.HelpPageText))
            {
                AppendError(sb, control, "HelpPageText", control.HelpPageText);
            }
            if (HasDangerousScript(control.InstructionText))
            {
                AppendError(sb, control, "InstructionText", control.InstructionText);
            }
            if (HasDangerousScript(control.FailureText))
            {
                AppendError(sb, control, "FailureText", control.FailureText);
            }
            if (HasDangerousScript(control.LoginButtonImageUrl))
            {
                AppendError(sb, control, "LoginButtonImageUrl", control.LoginButtonImageUrl);
            }
            if (HasDangerousScript(control.MembershipProvider))
            {
                AppendError(sb, control, "MembershipProvider", control.MembershipProvider);
            }
            if (HasDangerousScript(control.PasswordLabelText))
            {
                AppendError(sb, control, "PasswordLabelText", control.PasswordLabelText);
            }
            if (HasDangerousScript(control.PasswordRecoveryText))
            {
                AppendError(sb, control, "PasswordRecoveryText", control.PasswordRecoveryText);
            }
            if (HasDangerousScript(control.RememberMeText))
            {
                AppendError(sb, control, "RememberMeText", control.RememberMeText);
            }
            if (HasDangerousScript(control.TitleText))
            {
                AppendError(sb, control, "TitleText", control.TitleText);
            }
            if (HasDangerousScript(control.UserNameLabelText))
            {
                AppendError(sb, control, "UserNameLabelText", control.UserNameLabelText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.LoginName control)
        {
            if (HasDangerousScript(control.FormatString))
            {
                AppendError(sb, control, "FormatString", control.FormatString);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.LoginStatus control)
        {
            if (HasDangerousScript(control.LogoutImageUrl))
            {
                AppendError(sb, control, "LogoutImageUrl", control.LogoutImageUrl);
            }
            if (HasDangerousScript(control.LogoutPageUrl))
            {
                AppendError(sb, control, "LogoutPageUrl", control.LogoutPageUrl);
            }
            if (HasDangerousScript(control.LogoutText))
            {
                AppendError(sb, control, "LogoutText", control.LogoutText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Menu control)
        {
            if (HasDangerousScript(control.DynamicBottomSeparatorImageUrl))
            {
                AppendError(sb, control, "DynamicBottomSeparatorImageUrl", control.DynamicBottomSeparatorImageUrl);
            }
            if (HasDangerousScript(control.DynamicItemFormatString))
            {
                AppendError(sb, control, "DynamicItemFormatString", control.DynamicItemFormatString);
            }
            if (HasDangerousScript(control.DynamicPopOutImageUrl))
            {
                AppendError(sb, control, "DynamicPopOutImageUrl", control.DynamicPopOutImageUrl);
            }
            if (HasDangerousScript(control.DynamicPopOutImageTextFormatString))
            {
                AppendError(sb, control, "DynamicPopOutImageTextFormatString", control.DynamicPopOutImageTextFormatString);
            }
            if (HasDangerousScript(control.DynamicTopSeparatorImageUrl))
            {
                AppendError(sb, control, "DynamicTopSeparatorImageUrl", control.DynamicTopSeparatorImageUrl);
            }
            if (HasDangerousScript(control.ScrollDownImageUrl))
            {
                AppendError(sb, control, "ScrollDownImageUrl", control.ScrollDownImageUrl);
            }
            if (HasDangerousScript(control.ScrollDownText))
            {
                AppendError(sb, control, "ScrollDownText", control.ScrollDownText);
            }
            if (HasDangerousScript(control.ScrollUpImageUrl))
            {
                AppendError(sb, control, "ScrollUpImageUrl", control.ScrollUpImageUrl);
            }
            if (HasDangerousScript(control.ScrollUpText))
            {
                AppendError(sb, control, "ScrollUpText", control.ScrollUpText);
            }
            if (HasDangerousScript(control.SkipLinkText))
            {
                AppendError(sb, control, "SkipLinkText", control.SkipLinkText);
            }
            if (HasDangerousScript(control.StaticBottomSeparatorImageUrl))
            {
                AppendError(sb, control, "StaticBottomSeparatorImageUrl", control.StaticBottomSeparatorImageUrl);
            }
            if (HasDangerousScript(control.StaticItemFormatString))
            {
                AppendError(sb, control, "StaticItemFormatString", control.StaticItemFormatString);
            }
            if (HasDangerousScript(control.StaticPopOutImageUrl))
            {
                AppendError(sb, control, "StaticPopOutImageUrl", control.StaticPopOutImageUrl);
            }
            if (HasDangerousScript(control.StaticPopOutImageTextFormatString))
            {
                AppendError(sb, control, "StaticPopOutImageTextFormatString", control.StaticPopOutImageTextFormatString);
            }
            if (HasDangerousScript(control.StaticTopSeparatorImageUrl))
            {
                AppendError(sb, control, "StaticTopSeparatorImageUrl", control.StaticTopSeparatorImageUrl);
            }
            if (HasDangerousScript(control.Target))
            {
                AppendError(sb, control, "Target", control.Target);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.ObjectDataSource control)
        {
            if (HasDangerousScript(control.CacheKeyDependency))
            {
                AppendError(sb, control, "CacheKeyDependency", control.CacheKeyDependency);
            }
            if (HasDangerousScript(control.DataObjectTypeName))
            {
                AppendError(sb, control, "DataObjectTypeName", control.DataObjectTypeName);
            }
            if (HasDangerousScript(control.DeleteMethod))
            {
                AppendError(sb, control, "DeleteMethod", control.DeleteMethod);
            }
            if (HasDangerousScript(control.FilterExpression))
            {
                AppendError(sb, control, "FilterExpression", control.FilterExpression);
            }
            if (HasDangerousScript(control.InsertMethod))
            {
                AppendError(sb, control, "InsertMethod", control.InsertMethod);
            }
            if (HasDangerousScript(control.MaximumRowsParameterName))
            {
                AppendError(sb, control, "MaximumRowsParameterName", control.MaximumRowsParameterName);
            }
            if (HasDangerousScript(control.OldValuesParameterFormatString))
            {
                AppendError(sb, control, "OldValuesParameterFormatString", control.OldValuesParameterFormatString);
            }
            if (HasDangerousScript(control.SelectCountMethod))
            {
                AppendError(sb, control, "SelectCountMethod", control.SelectCountMethod);
            }
            if (HasDangerousScript(control.SelectMethod))
            {
                AppendError(sb, control, "SelectMethod", control.SelectMethod);
            }
            if (HasDangerousScript(control.SortParameterName))
            {
                AppendError(sb, control, "SortParameterName", control.SortParameterName);
            }
            if (HasDangerousScript(control.SqlCacheDependency))
            {
                AppendError(sb, control, "SqlCacheDependency", control.SqlCacheDependency);
            }
            if (HasDangerousScript(control.StartRowIndexParameterName))
            {
                AppendError(sb, control, "StartRowIndexParameterName", control.StartRowIndexParameterName);
            }
            if (HasDangerousScript(control.TypeName))
            {
                AppendError(sb, control, "TypeName", control.TypeName);
            }
            if (HasDangerousScript(control.UpdateMethod))
            {
                AppendError(sb, control, "UpdateMethod", control.UpdateMethod);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Panel control)
        {
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.PasswordRecovery control)
        {
            if (HasDangerousScript(control.AnswerLabelText))
            {
                AppendError(sb, control, "AnswerLabelText", control.AnswerLabelText);
            }
            if (HasDangerousScript(control.AnswerRequiredErrorMessage))
            {
                AppendError(sb, control, "AnswerRequiredErrorMessage", control.AnswerRequiredErrorMessage);
            }
            if (HasDangerousScript(control.GeneralFailureText))
            {
                AppendError(sb, control, "GeneralFailureText", control.GeneralFailureText);
            }
            if (HasDangerousScript(control.HelpPageText))
            {
                AppendError(sb, control, "HelpPageText", control.HelpPageText);
            }
            if (HasDangerousScript(control.MembershipProvider))
            {
                AppendError(sb, control, "MembershipProvider", control.MembershipProvider);
            }
            if (HasDangerousScript(control.Question))
            {
                AppendError(sb, control, "Question", control.Question);
            }
            if (HasDangerousScript(control.QuestionFailureText))
            {
                AppendError(sb, control, "QuestionFailureText", control.QuestionFailureText);
            }
            if (HasDangerousScript(control.QuestionInstructionText))
            {
                AppendError(sb, control, "QuestionInstructionText", control.QuestionInstructionText);
            }
            if (HasDangerousScript(control.QuestionLabelText))
            {
                AppendError(sb, control, "QuestionLabelText", control.QuestionLabelText);
            }
            if (HasDangerousScript(control.QuestionTitleText))
            {
                AppendError(sb, control, "QuestionTitleText", control.QuestionTitleText);
            }
            if (HasDangerousScript(control.SubmitButtonImageUrl))
            {
                AppendError(sb, control, "SubmitButtonImageUrl", control.SubmitButtonImageUrl);
            }
            if (HasDangerousScript(control.SuccessPageUrl))
            {
                AppendError(sb, control, "SuccessPageUrl", control.SuccessPageUrl);
            }
            if (HasDangerousScript(control.SuccessText))
            {
                AppendError(sb, control, "SuccessText", control.SuccessText);
            }
            if (HasDangerousScript(control.UserNameFailureText))
            {
                AppendError(sb, control, "UserNameFailureText", control.UserNameFailureText);
            }
            if (HasDangerousScript(control.UserNameInstructionText))
            {
                AppendError(sb, control, "UserNameInstructionText", control.UserNameInstructionText);
            }
            if (HasDangerousScript(control.UserNameLabelText))
            {
                AppendError(sb, control, "UserNameLabelText", control.UserNameLabelText);
            }
            if (HasDangerousScript(control.UserNameTitleText))
            {
                AppendError(sb, control, "UserNameTitleText", control.UserNameTitleText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.RadioButton control)
        {
            if (HasDangerousScript(control.GroupName))
            {
                AppendError(sb, control, "GroupName", control.GroupName);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.RadioButtonList control)
        {
            if (HasDangerousScript(control.DataTextField))
            {
                AppendError(sb, control, "DataTextField", control.DataTextField);
            }
            if (HasDangerousScript(control.DataTextFormatString))
            {
                AppendError(sb, control, "DataTextFormatString", control.DataTextFormatString);
            }
            if (HasDangerousScript(control.DataValueField))
            {
                AppendError(sb, control, "DataValueField", control.DataValueField);
            }
            if (HasDangerousScript(control.SelectedValue))
            {
                AppendError(sb, control, "SelectedValue", control.SelectedValue);
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }

            // Items are not controls in their own right; rather, they are used at render time to generate new controls.
            // Thus, we want to handle this as particular for Items of a RadioButtonList, rather than for ListItems of a general ListControl.
            foreach (ListItem item in control.Items)
            {
                if (HasDangerousScript(item.Text))
                {
                    AppendError(sb, control, "Text", item.Text); // This value is not encoded, and is placed directly in a <label> tag

                    if (isSanitizeOutput)
                    {
                        item.Text = AspxTools.HtmlString(item.Text);
                    }
                }
                if (HasDangerousScript(item.Value))
                {
                    AppendError(sb, control, "Value", item.Value); // This is encoded in the <input> value attribute
                }
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.RangeValidator control)
        {
            if (isSanitizeOutput)
            {
                /* ejm opm 464967
                 * Same as RequiredFieldValidator.InitialValue.
                 * Need to JS encode since these value are rendered in a script tag.
                 */

                control.MinimumValue = AspxTools.JsStringUnquoted(control.MinimumValue);
                control.MaximumValue = AspxTools.JsStringUnquoted(control.MaximumValue);
                control.ValidationGroup = AspxTools.JsStringUnquoted(control.ValidationGroup);
            }

            if (HasDangerousScript(control.MaximumValue))
            {
                AppendError(sb, control, "MaximumValue", control.MaximumValue);
            }
            if (HasDangerousScript(control.MinimumValue))
            {
                AppendError(sb, control, "MinimumValue", control.MinimumValue);
            }
            if (HasDangerousScript(control.AssociatedControlID))
            {
                AppendError(sb, control, "AssociatedControlID", control.AssociatedControlID);
            }
            if (HasDangerousScript(control.ControlToValidate))
            {
                AppendError(sb, control, "ControlToValidate", control.ControlToValidate);
            }
            if (HasDangerousScript(control.ErrorMessage))
            {
                AppendError(sb, control, "ErrorMessage", control.ErrorMessage);

                if (isSanitizeOutput)
                {
                    control.ErrorMessage = AspxTools.HtmlString(control.ErrorMessage);
                }
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);

                if (isSanitizeOutput)
                {
                    control.Text = AspxTools.HtmlString(control.Text);
                }
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.RegularExpressionValidator control)
        {
            if (isSanitizeOutput)
            {
                // ejm opm 464967
                // We can't JS encode this since this is a regular expression.
                // Instead, we'll just make sure this is an actual regular rexpression.
                // However, we can't allow any user input in the regular expression string OR we encode the user input before adding it to the string.
                control.ValidationExpression = LqbGrammar.DataTypes.RegularExpressionString.Create(control.ValidationExpression)?.ToString() ?? string.Empty;
                control.ValidationGroup = AspxTools.JsStringUnquoted(control.ValidationGroup);
            }

            if (HasDangerousScript(control.ValidationExpression))
            {
                AppendError(sb, control, "ValidationExpression", control.ValidationExpression);
            }

            if (HasDangerousScript(control.AssociatedControlID))
            {
                AppendError(sb, control, "AssociatedControlID", control.AssociatedControlID);
            }
            if (HasDangerousScript(control.ControlToValidate))
            {
                AppendError(sb, control, "ControlToValidate", control.ControlToValidate);
            }
            if (HasDangerousScript(control.ErrorMessage))
            {
                AppendError(sb, control, "ErrorMessage", control.ErrorMessage);

                if (isSanitizeOutput)
                {
                    control.ErrorMessage = AspxTools.HtmlString(control.ErrorMessage);
                }
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);

                if (isSanitizeOutput)
                {
                    control.Text = AspxTools.HtmlString(control.Text);
                }
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Repeater control)
        {
            if (HasDangerousScript(control.DataMember))
            {
                AppendError(sb, control, "DataMember", control.DataMember);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.RepeaterItem control)
        {

        }

        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.RequiredFieldValidator control)
        {
            if (isSanitizeOutput)
            {
                /* ejm opm 464967
                 * The InitialValue property for this control generated some JS and placed into some script tags.
                 * The value only has quotes and escape characters escaped. Everything else is copied raw.
                 * We'll always JS encode these strings since InitialValues shouldn't contain unsafe characters.
                 */

                control.InitialValue = AspxTools.JsStringUnquoted(control.InitialValue);
                control.ValidationGroup = AspxTools.JsStringUnquoted(control.ValidationGroup);
            }

            if (HasDangerousScript(control.InitialValue))
            {
                AppendError(sb, control, "InitialValue", control.InitialValue);
            }
            if (HasDangerousScript(control.AssociatedControlID))
            {
                AppendError(sb, control, "AssociatedControlID", control.AssociatedControlID);
            }
            if (HasDangerousScript(control.ControlToValidate))
            {
                AppendError(sb, control, "ControlToValidate", control.ControlToValidate);
            }
            if (HasDangerousScript(control.ErrorMessage))
            {
                AppendError(sb, control, "ErrorMessage", control.ErrorMessage);

                if (isSanitizeOutput)
                {
                    control.ErrorMessage = AspxTools.HtmlString(control.ErrorMessage);
                }
            }
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);

                if (isSanitizeOutput)
                {
                    control.Text = AspxTools.HtmlString(control.Text);
                }
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.SiteMapDataSource control)
        {
            if (HasDangerousScript(control.SiteMapProvider))
            {
                AppendError(sb, control, "SiteMapProvider", control.SiteMapProvider);
            }
            if (HasDangerousScript(control.StartingNodeUrl))
            {
                AppendError(sb, control, "StartingNodeUrl", control.StartingNodeUrl);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.SiteMapNodeItem control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.SiteMapPath control)
        {
            if (HasDangerousScript(control.PathSeparator))
            {
                AppendError(sb, control, "PathSeparator", control.PathSeparator);
            }
            if (HasDangerousScript(control.SkipLinkText))
            {
                AppendError(sb, control, "SkipLinkText", control.SkipLinkText);
            }
            if (HasDangerousScript(control.SiteMapProvider))
            {
                AppendError(sb, control, "SiteMapProvider", control.SiteMapProvider);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Substitution control)
        {
            if (HasDangerousScript(control.MethodName))
            {
                AppendError(sb, control, "MethodName", control.MethodName);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.TableFooterRow control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.TableHeaderCell control)
        {
            if (HasDangerousScript(control.Text))
            {
                AppendError(sb, control, "Text", control.Text);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.TableHeaderRow control)
        {
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.TextBox control)
        {
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.TreeView control)
        {
            if (HasDangerousScript(control.CollapseImageToolTip))
            {
                AppendError(sb, control, "CollapseImageToolTip", control.CollapseImageToolTip);
            }
            if (HasDangerousScript(control.CollapseImageUrl))
            {
                AppendError(sb, control, "CollapseImageUrl", control.CollapseImageUrl);
            }
            if (HasDangerousScript(control.ExpandImageToolTip))
            {
                AppendError(sb, control, "ExpandImageToolTip", control.ExpandImageToolTip);
            }
            if (HasDangerousScript(control.ExpandImageUrl))
            {
                AppendError(sb, control, "ExpandImageUrl", control.ExpandImageUrl);
            }
            if (HasDangerousScript(control.LineImagesFolder))
            {
                AppendError(sb, control, "LineImagesFolder", control.LineImagesFolder);
            }
            if (HasDangerousScript(control.NoExpandImageUrl))
            {
                AppendError(sb, control, "NoExpandImageUrl", control.NoExpandImageUrl);
            }
            if (HasDangerousScript(control.SkipLinkText))
            {
                AppendError(sb, control, "SkipLinkText", control.SkipLinkText);
            }
            if (HasDangerousScript(control.Target))
            {
                AppendError(sb, control, "Target", control.Target);
            }
            if (HasDangerousScript(control.DataSourceID))
            {
                AppendError(sb, control, "DataSourceID", control.DataSourceID);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.ValidationSummary control)
        {
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.ValidationGroup))
            {
                AppendError(sb, control, "ValidationGroup", control.ValidationGroup);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WizardStep control)
        {
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.Xml control)
        {
            if (HasDangerousScript(control.DocumentContent))
            {
                AppendError(sb, control, "DocumentContent", control.DocumentContent);
            }
            if (HasDangerousScript(control.DocumentSource))
            {
                AppendError(sb, control, "DocumentSource", control.DocumentSource);
            }
            if (HasDangerousScript(control.TransformSource))
            {
                AppendError(sb, control, "TransformSource", control.TransformSource);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.XmlDataSource control)
        {
            if (HasDangerousScript(control.CacheKeyDependency))
            {
                AppendError(sb, control, "CacheKeyDependency", control.CacheKeyDependency);
            }
            if (HasDangerousScript(control.Data))
            {
                AppendError(sb, control, "Data", control.Data);
            }
            if (HasDangerousScript(control.DataFile))
            {
                AppendError(sb, control, "DataFile", control.DataFile);
            }
            if (HasDangerousScript(control.Transform))
            {
                AppendError(sb, control, "Transform", control.Transform);
            }
            if (HasDangerousScript(control.TransformFile))
            {
                AppendError(sb, control, "TransformFile", control.TransformFile);
            }
            if (HasDangerousScript(control.XPath))
            {
                AppendError(sb, control, "XPath", control.XPath);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.AppearanceEditorPart control)
        {
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.BehaviorEditorPart control)
        {
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.CatalogZone control)
        {
            if (HasDangerousScript(control.EmptyZoneText))
            {
                AppendError(sb, control, "EmptyZoneText", control.EmptyZoneText);
            }
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.InstructionText))
            {
                AppendError(sb, control, "InstructionText", control.InstructionText);
            }
            if (HasDangerousScript(control.SelectedCatalogPartID))
            {
                AppendError(sb, control, "SelectedCatalogPartID", control.SelectedCatalogPartID);
            }
            if (HasDangerousScript(control.SelectTargetZoneText))
            {
                AppendError(sb, control, "SelectTargetZoneText", control.SelectTargetZoneText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.ConnectionsZone control)
        {
            if (HasDangerousScript(control.ConfigureConnectionTitle))
            {
                AppendError(sb, control, "ConfigureConnectionTitle", control.ConfigureConnectionTitle);
            }
            if (HasDangerousScript(control.ConnectToConsumerInstructionText))
            {
                AppendError(sb, control, "ConnectToConsumerInstructionText", control.ConnectToConsumerInstructionText);
            }
            if (HasDangerousScript(control.ConnectToConsumerText))
            {
                AppendError(sb, control, "ConnectToConsumerText", control.ConnectToConsumerText);
            }
            if (HasDangerousScript(control.ConnectToConsumerTitle))
            {
                AppendError(sb, control, "ConnectToConsumerTitle", control.ConnectToConsumerTitle);
            }
            if (HasDangerousScript(control.ConnectToProviderInstructionText))
            {
                AppendError(sb, control, "ConnectToProviderInstructionText", control.ConnectToProviderInstructionText);
            }
            if (HasDangerousScript(control.ConnectToProviderText))
            {
                AppendError(sb, control, "ConnectToProviderText", control.ConnectToProviderText);
            }
            if (HasDangerousScript(control.ConnectToProviderTitle))
            {
                AppendError(sb, control, "ConnectToProviderTitle", control.ConnectToProviderTitle);
            }
            if (HasDangerousScript(control.ConsumersTitle))
            {
                AppendError(sb, control, "ConsumersTitle", control.ConsumersTitle);
            }
            if (HasDangerousScript(control.ConsumersInstructionText))
            {
                AppendError(sb, control, "ConsumersInstructionText", control.ConsumersInstructionText);
            }
            if (HasDangerousScript(control.EmptyZoneText))
            {
                AppendError(sb, control, "EmptyZoneText", control.EmptyZoneText);
            }
            if (HasDangerousScript(control.ExistingConnectionErrorMessage))
            {
                AppendError(sb, control, "ExistingConnectionErrorMessage", control.ExistingConnectionErrorMessage);
            }
            if (HasDangerousScript(control.GetText))
            {
                AppendError(sb, control, "GetText", control.GetText);
            }
            if (HasDangerousScript(control.GetFromText))
            {
                AppendError(sb, control, "GetFromText", control.GetFromText);
            }
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.InstructionText))
            {
                AppendError(sb, control, "InstructionText", control.InstructionText);
            }
            if (HasDangerousScript(control.InstructionTitle))
            {
                AppendError(sb, control, "InstructionTitle", control.InstructionTitle);
            }
            if (HasDangerousScript(control.NewConnectionErrorMessage))
            {
                AppendError(sb, control, "NewConnectionErrorMessage", control.NewConnectionErrorMessage);
            }
            if (HasDangerousScript(control.NoExistingConnectionInstructionText))
            {
                AppendError(sb, control, "NoExistingConnectionInstructionText", control.NoExistingConnectionInstructionText);
            }
            if (HasDangerousScript(control.NoExistingConnectionTitle))
            {
                AppendError(sb, control, "NoExistingConnectionTitle", control.NoExistingConnectionTitle);
            }
            if (HasDangerousScript(control.ProvidersTitle))
            {
                AppendError(sb, control, "ProvidersTitle", control.ProvidersTitle);
            }
            if (HasDangerousScript(control.ProvidersInstructionText))
            {
                AppendError(sb, control, "ProvidersInstructionText", control.ProvidersInstructionText);
            }
            if (HasDangerousScript(control.SendText))
            {
                AppendError(sb, control, "SendText", control.SendText);
            }
            if (HasDangerousScript(control.SendToText))
            {
                AppendError(sb, control, "SendToText", control.SendToText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.DeclarativeCatalogPart control)
        {
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.WebPartsListUserControlPath))
            {
                AppendError(sb, control, "WebPartsListUserControlPath", control.WebPartsListUserControlPath);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.EditorZone control)
        {
            if (HasDangerousScript(control.EmptyZoneText))
            {
                AppendError(sb, control, "EmptyZoneText", control.EmptyZoneText);
            }
            if (HasDangerousScript(control.ErrorText))
            {
                AppendError(sb, control, "ErrorText", control.ErrorText);
            }
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.InstructionText))
            {
                AppendError(sb, control, "InstructionText", control.InstructionText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.ErrorWebPart control)
        {
            if (HasDangerousScript(control.ErrorMessage))
            {
                AppendError(sb, control, "ErrorMessage", control.ErrorMessage);
            }
            if (HasDangerousScript(control.AuthorizationFilter))
            {
                AppendError(sb, control, "AuthorizationFilter", control.AuthorizationFilter);
            }
            if (HasDangerousScript(control.CatalogIconImageUrl))
            {
                AppendError(sb, control, "CatalogIconImageUrl", control.CatalogIconImageUrl);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
            if (HasDangerousScript(control.HelpUrl))
            {
                AppendError(sb, control, "HelpUrl", control.HelpUrl);
            }
            if (HasDangerousScript(control.ImportErrorMessage))
            {
                AppendError(sb, control, "ImportErrorMessage", control.ImportErrorMessage);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.TitleIconImageUrl))
            {
                AppendError(sb, control, "TitleIconImageUrl", control.TitleIconImageUrl);
            }
            if (HasDangerousScript(control.TitleUrl))
            {
                AppendError(sb, control, "TitleUrl", control.TitleUrl);
            }
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.GenericWebPart control)
        {
            if (HasDangerousScript(control.CatalogIconImageUrl))
            {
                AppendError(sb, control, "CatalogIconImageUrl", control.CatalogIconImageUrl);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.TitleIconImageUrl))
            {
                AppendError(sb, control, "TitleIconImageUrl", control.TitleIconImageUrl);
            }
            if (HasDangerousScript(control.TitleUrl))
            {
                AppendError(sb, control, "TitleUrl", control.TitleUrl);
            }
            if (HasDangerousScript(control.AuthorizationFilter))
            {
                AppendError(sb, control, "AuthorizationFilter", control.AuthorizationFilter);
            }
            if (HasDangerousScript(control.HelpUrl))
            {
                AppendError(sb, control, "HelpUrl", control.HelpUrl);
            }
            if (HasDangerousScript(control.ImportErrorMessage))
            {
                AppendError(sb, control, "ImportErrorMessage", control.ImportErrorMessage);
            }
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.ImportCatalogPart control)
        {
            if (HasDangerousScript(control.BrowseHelpText))
            {
                AppendError(sb, control, "BrowseHelpText", control.BrowseHelpText);
            }
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.ImportedPartLabelText))
            {
                AppendError(sb, control, "ImportedPartLabelText", control.ImportedPartLabelText);
            }
            if (HasDangerousScript(control.PartImportErrorLabelText))
            {
                AppendError(sb, control, "PartImportErrorLabelText", control.PartImportErrorLabelText);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.UploadHelpText))
            {
                AppendError(sb, control, "UploadHelpText", control.UploadHelpText);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.LayoutEditorPart control)
        {
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.PageCatalogPart control)
        {
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.PropertyGridEditorPart control)
        {
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.UnauthorizedWebPart control)
        {
            if (HasDangerousScript(control.AuthorizationFilter))
            {
                AppendError(sb, control, "AuthorizationFilter", control.AuthorizationFilter);
            }
            if (HasDangerousScript(control.CatalogIconImageUrl))
            {
                AppendError(sb, control, "CatalogIconImageUrl", control.CatalogIconImageUrl);
            }
            if (HasDangerousScript(control.Description))
            {
                AppendError(sb, control, "Description", control.Description);
            }
            if (HasDangerousScript(control.HelpUrl))
            {
                AppendError(sb, control, "HelpUrl", control.HelpUrl);
            }
            if (HasDangerousScript(control.ImportErrorMessage))
            {
                AppendError(sb, control, "ImportErrorMessage", control.ImportErrorMessage);
            }
            if (HasDangerousScript(control.Title))
            {
                AppendError(sb, control, "Title", control.Title);
            }
            if (HasDangerousScript(control.TitleIconImageUrl))
            {
                AppendError(sb, control, "TitleIconImageUrl", control.TitleIconImageUrl);
            }
            if (HasDangerousScript(control.TitleUrl))
            {
                AppendError(sb, control, "TitleUrl", control.TitleUrl);
            }
            if (HasDangerousScript(control.BackImageUrl))
            {
                AppendError(sb, control, "BackImageUrl", control.BackImageUrl);
            }
            if (HasDangerousScript(control.DefaultButton))
            {
                AppendError(sb, control, "DefaultButton", control.DefaultButton);
            }
            if (HasDangerousScript(control.GroupingText))
            {
                AppendError(sb, control, "GroupingText", control.GroupingText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
            if (HasDangerousScript(control.CssClass))
            {
                AppendError(sb, control, "CssClass", control.CssClass);
            }
            if (HasDangerousScript(control.ToolTip))
            {
                AppendError(sb, control, "ToolTip", control.ToolTip);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.WebPartManager control)
        {
            if (HasDangerousScript(control.CloseProviderWarning))
            {
                AppendError(sb, control, "CloseProviderWarning", control.CloseProviderWarning);
            }
            if (HasDangerousScript(control.DeleteWarning))
            {
                AppendError(sb, control, "DeleteWarning", control.DeleteWarning);
            }
            if (HasDangerousScript(control.ExportSensitiveDataWarning))
            {
                AppendError(sb, control, "ExportSensitiveDataWarning", control.ExportSensitiveDataWarning);
            }
        }
        private void Sanitize(StringBuilder sb, bool isSanitizeOutput, System.Web.UI.WebControls.WebParts.WebPartZone control)
        {
            if (HasDangerousScript(control.EmptyZoneText))
            {
                AppendError(sb, control, "EmptyZoneText", control.EmptyZoneText);
            }
            if (HasDangerousScript(control.MenuCheckImageUrl))
            {
                AppendError(sb, control, "MenuCheckImageUrl", control.MenuCheckImageUrl);
            }
            if (HasDangerousScript(control.MenuLabelText))
            {
                AppendError(sb, control, "MenuLabelText", control.MenuLabelText);
            }
            if (HasDangerousScript(control.MenuPopupImageUrl))
            {
                AppendError(sb, control, "MenuPopupImageUrl", control.MenuPopupImageUrl);
            }
            if (HasDangerousScript(control.HeaderText))
            {
                AppendError(sb, control, "HeaderText", control.HeaderText);
            }
            if (HasDangerousScript(control.AccessKey))
            {
                AppendError(sb, control, "AccessKey", control.AccessKey);
            }
        }



    }
}
