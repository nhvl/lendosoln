﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Web;
using System.Xml.Linq;
using System.Linq;
using DataAccess;

namespace LendersOffice.HttpModule
{
    public class PerformanceMonitorItem
    {
        private class _TimingItem : IDisposable
        {
            private string description;
            private Stopwatch stopwatch;
            private bool logOnDispose = true;

            public _TimingItem(string desc)
            {
                stopwatch = Stopwatch.StartNew();
                this.description = desc;
            }

            public _TimingItem(string desc, bool shouldLogOnDispose)
                : this(desc)
            {
                this.logOnDispose = shouldLogOnDispose;
            }

            #region IDisposable Members

            public void Dispose()
            {
                stopwatch.Stop();

                if (this.logOnDispose)
                {
                    AddTimingDetailsToCurrent(this.description, stopwatch.ElapsedMilliseconds);
                }
            }

            #endregion
        }

        private const int MaxItemCount = 100000;

        public class _Item
        {
            public int ElapsedMs { get; private set; }
            public string Description { get; private set; }
            public long DurationMs { get; private set; }
            public int ThreadId { get; private set; }
            public _Item(int ms, string description, long duration)
            {
                this.ThreadId = Thread.CurrentThread.ManagedThreadId;
                this.ElapsedMs = ms;
                this.Description = description;
                this.DurationMs = duration;
            }
        }
        private const string ContextItemKey_PerformanceMonitorItem = "PerformanceMonitorItem";

        public ResponseSizeFilter SizeFilter;
        public long DurationInMs
        {
            get
            {
                return m_stopWatch.ElapsedMilliseconds;
            }
        }
        internal long GlobalDurationInMs
        {
            get { return m_globalStopWatch.ElapsedMilliseconds; }
        }
        public long ResponseLength { get; set; }

        public DateTime CreatedDate { get; private set; }
        public bool IsEnabledDebugOutput { get; set; }
        public bool IsEnabledPageSizeWarning { get; set; }
        public bool IsEnabledPageRenderTimeWarning { get; set; }
        public Guid UniqueId { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance's <see cref="sLId"/> has held an identifier
        /// for several loans, rather than one or zero.
        /// </summary>
        /// <remarks>
        /// We use this to decide if we can trust the value of <see cref="sLId"/> as a fall back for
        /// the <c>sLId</c> property in the IIS request, which is set through <see cref="SetCustomKeyValue"/>.
        /// </remarks>
        public bool HasProcessedSeveralLoans { get; private set; }

        private Guid _sLId;

        /// <summary>
        /// Gets or sets the most recent <see cref="CPageData.sLId"/> loaded in a loan file, used primarily for
        /// general logging (non-IIS).  It may not be representative of the current request's loan file.
        /// </summary>
        public Guid sLId
        {
            get
            {
                return this._sLId;
            }
            set
            {
                if (value != Guid.Empty && this._sLId != Guid.Empty && value != this._sLId)
                {
                    this.HasProcessedSeveralLoans = true;
                }

                this._sLId = value;
            }
        }

        private Stopwatch m_globalStopWatch = null; 
        private List<_Item> m_rawTimingDetailList = null;
        private Dictionary<string, string> m_customKeyValueDictionary = null;
        public IEnumerable<XElement> Queries
        {
            get { return m_queryElements.AsReadOnly(); }
        }

        public IEnumerable<XElement> FileDBElements
        {
            get { return m_fileDBElements; }
        }

        public void AddTimingDetail(string description, long durationInMs)
        {
            if (this.m_rawTimingDetailList.Count >= MaxItemCount)
            {
                // 11/3/2015 - dd - To prevent a massive log sitting in memory, we will introduce a cap on number
                // of items can be present in memory.
                return;
            }

            this.m_rawTimingDetailList.Add(new _Item((int) m_globalStopWatch.ElapsedMilliseconds, description, durationInMs));
        }

        public IEnumerable<_Item> RawTimingDetailList
        {
            get { return m_rawTimingDetailList; }
        }

        public IEnumerable<string> TimingDetailList
        {
            get 
            {
                List<string> list = new List<string>();

                foreach (var item in m_rawTimingDetailList)
                {
                    string time = string.Empty;
                    if (item.DurationMs > 0)
                    {
                        time = " Executed in " + item.DurationMs.ToString("#,#") + " ms.";
                    }
                    list.Add("[" + item.ElapsedMs.ToString("#,#") + "ms] - " + item.Description + time);

                }
                return list; 
            }
        }

        private List<XElement> m_queryElements;
        private List<XElement> m_fileDBElements;

        private Stopwatch m_stopWatch = null;
        private PerformanceMonitorItem()
        {
            UniqueId = Guid.NewGuid();
            m_rawTimingDetailList = new List<_Item>();
            IsEnabledPageSizeWarning = true;
            IsEnabledPageRenderTimeWarning = true;
            IsEnabledDebugOutput = true;
            CreatedDate = DateTime.Now;
            m_stopWatch = new Stopwatch();
            m_stopWatch.Start();
            m_globalStopWatch = new Stopwatch();
            m_globalStopWatch.Start();
            m_customKeyValueDictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            m_queryElements = new List<XElement>();
            m_fileDBElements = new List<XElement>();
        }

        public void AddQuery(XElement element)
        {
            if (this.m_queryElements.Count >= MaxItemCount)
            {
                // 11/3/2015 - dd - To prevent a massive log sitting in memory, we will introduce a cap on number
                // of items can be present in memory.
                return;
            }

            this.m_queryElements.Add(element);
        }

        internal class SqlConnectionCountItem
        {
            public string Description { get; set; }
            public int Count { get; set; }
        }

        private Dictionary<Guid, SqlConnectionCountItem> sqlConnectionDictionary = new Dictionary<Guid, SqlConnectionCountItem>();

        /// <summary>
        /// Gets a list of connection description where # of .Open() does not match # of .Close(). If count is positive mean it is a leak.
        /// If count is negative then it mean it call Close() too many time.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<SqlConnectionCountItem> GetPotentialConnectionLeaks()
        {
            List<SqlConnectionCountItem> list = new List<SqlConnectionCountItem>();

            foreach (var o in sqlConnectionDictionary)
            {
                // dd 10/5/2017 - Only return when count is positive number to indicate actual leak.
                if (o.Value.Count > 0)
                {
                    list.Add(o.Value);
                }
            }

            return list;
        }

        public void TrackSqlConnection(Guid id, string description, ConnectionState stage)
        {
            if (this.sqlConnectionDictionary.Count >= MaxItemCount)
            {
                //  dd - To prevent a massive log sitting in memory, we will introduce a cap on number
                // of items can be present in memory.
                return;
            }

            // 10/5/2017 - dd - Only counter when connection is open or close. Each time connection open we increase one and decrease counter
            // by one when connection is closed.
            int incrementValue = 0;

            if (stage == ConnectionState.Open)
            {
                incrementValue = 1;
            }
            else if (stage == ConnectionState.Closed)
            {
                incrementValue = -1;
            }

            SqlConnectionCountItem item = null;

            if (!sqlConnectionDictionary.TryGetValue(id, out item))
            {
                item = new SqlConnectionCountItem() { Description = description };
                sqlConnectionDictionary.Add(id, item);
            }
            item.Count += incrementValue;
        }

        public void AddFileDBGet(string key, string dbName, long bytes, long durationInMs, string context)
        {
            this.AddTimingDetail("FileDB.GetFile - [" + key + "] [" + dbName + "] [" + context + "] " + bytes.ToString("#,#") + " bytes", durationInMs);
            this.AddFileDBElement("Get", key, dbName, bytes, durationInMs, context);
        }

        public void AddFileDBPut(string key, string dbName, long bytes, long durationInMs, string context)
        {
            this.AddTimingDetail("FileDB.PutFile - [" + key + "] [" + dbName + "] [" + context + "] " + bytes.ToString("#,#") + " bytes", durationInMs);
            this.AddFileDBElement("Put", key, dbName, bytes, durationInMs, context);
        }

        private void AddFileDBElement(string op, string key, string dbName, long bytes, long durationInMs, string context)
        {
            if (this.m_fileDBElements.Count >= MaxItemCount)
            {
                // 11/3/2015 - dd - To prevent a massive log sitting in memory, we will introduce a cap on number
                // of items can be present in memory.
                return;
            }

            XElement el = new XElement("filedb",
                    new XAttribute("op", op),
                    new XAttribute("key", key),
                    new XAttribute("db", dbName),
                    new XAttribute("bytes", bytes),
                    new XAttribute("execute", durationInMs),
                    new XAttribute("context", context));
            this.m_fileDBElements.Add(el);
        }

        public void SetCustomKeyValue(string key, string value)
        {
            m_customKeyValueDictionary[key] = value;
        }
        public IEnumerable<KeyValuePair<string, string>> GetCustomKeyValueList()
        {
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
            foreach (var item in m_customKeyValueDictionary)
            {
                list.Add(new KeyValuePair<string, string>(item.Key, item.Value));
            }
            return list;
        }
        public void StopTiming()
        {
            m_stopWatch.Stop();

        }
        public void ResumeTiming()
        {
            m_stopWatch.Start();
        }

        public XDocument GenerateXDocument(string name)
        {
            XDocument xdoc = new XDocument();
            XElement root = new XElement("perf");
            root.SetAttributeValue("name", name);

            xdoc.Add(root);

            if (this.Queries != null)
            {
                XElement q = new XElement("queries");
                foreach (XElement n in this.Queries)
                {
                    q.Add(n);
                }
                root.Add(q);
            }

            if (this.FileDBElements != null)
            {
                XElement q = new XElement("filedbs");
                foreach (XElement n in this.FileDBElements)
                {
                    q.Add(n);
                }
                root.Add(q);
            }

            XElement timingElement = new XElement("timings");
            root.Add(timingElement);
            foreach (var o in this.RawTimingDetailList)
            {
                timingElement.Add(new XElement("item",
                    new XAttribute("elapsed", o.ElapsedMs.ToString()),
                    new XAttribute("desc", o.Description),
                    new XAttribute("duration", o.DurationMs.ToString())));
            }

            var potentialConnectionLeakList = GetPotentialConnectionLeaks();
            if (potentialConnectionLeakList != null && potentialConnectionLeakList.Any())
            {
                XElement connectionLeakElement = new XElement("connectionleaks");
                root.Add(connectionLeakElement);

                foreach (var o in potentialConnectionLeakList)
                {
                    connectionLeakElement.Add(new XElement("item",
                        new XAttribute("description", o.Description),
                        new XAttribute("count", o.Count.ToString())));
                        
                }

            }
            return xdoc;
        }

        public static PerformanceMonitorItem GetOrCreate()
        {
            HttpContext context = HttpContext.Current;
            if (null == context)
            {
                return null;
            }

            if (!context.Items.Contains(ContextItemKey_PerformanceMonitorItem))
            {
                PerformanceMonitorItem item = new PerformanceMonitorItem();
                context.Items[ContextItemKey_PerformanceMonitorItem] = item;
                return item;
            }

            return (PerformanceMonitorItem)context.Items[ContextItemKey_PerformanceMonitorItem];
        }

        public static void AddTimingDetailsToCurrent(string description, long durationInMs)
        {
            var item = Current;
            if (item != null)
            {
                item.AddTimingDetail(description, durationInMs);
            }
        }

        public static void AddFileDBGetToCurrent(string key, string dbName, string filePath, long durationInMs, string context)
        {
            var item = Current;

            if (item == null)
            {
                return;
            }

            long bytes = 0;

            try
            {
                if (!string.IsNullOrEmpty(filePath))
                {
                    FileInfo fi = new FileInfo(filePath);
                    bytes = fi.Length;
                }
            }
            catch (IOException)
            {
            }

            item.AddFileDBGet(key, dbName, bytes, durationInMs, context);
        }

        public static void AddFileDBPutToCurrent(string key, string dbName, string filePath, long durationInMs, string context)
        {
            var item = Current;

            if (item == null)
            {
                return;
            }

            long bytes = 0;

            try
            {
                if (!string.IsNullOrEmpty(filePath))
                {
                    FileInfo fi = new FileInfo(filePath);
                    bytes = fi.Length;
                }
            }
            catch (IOException)
            {
            }

            item.AddFileDBPut(key, dbName, bytes, durationInMs, context);
        }

        private static ThreadLocal<PerformanceMonitorItem> threadLocalPerformanceItem = new ThreadLocal<PerformanceMonitorItem>();

        public static PerformanceMonitorItem CreateNewForThread()
        {
            PerformanceMonitorItem item = new PerformanceMonitorItem();

            threadLocalPerformanceItem.Value = item;

            return item;
        }

        /// <summary>
        /// Return null if it is there is no HttpContext or no monitor item.
        /// </summary>
        public static PerformanceMonitorItem Current
        {
            get
            {
                PerformanceMonitorItem item = null;
                if (null != HttpContext.Current)
                {
                    item = HttpContext.Current.Items[ContextItemKey_PerformanceMonitorItem] as PerformanceMonitorItem;
                }
                else if (threadLocalPerformanceItem.IsValueCreated)
                {
                    item = threadLocalPerformanceItem.Value;
                }

                return item;

            }
        }

        public static void LogAndRestart(string label, Stopwatch sw)
        {
            long time = sw.ElapsedMilliseconds;
            
            sw.Reset();
            sw.Start();
            Log(label, time);
        }

        private static void Log(string label, long timeInMs)
        {
            var item = PerformanceMonitorItem.Current;
            if (item != null)
            {
                item.AddTimingDetail(label, timeInMs);
            }
        }

        public static IDisposable Time(string description)
        {
            return new _TimingItem(description);
        }

        public static IDisposable Time(string description, bool shouldLogOnDispose)
        {
            return new _TimingItem(description, shouldLogOnDispose);
        }

        /// <summary>
        /// Write the current performance monitor item into our log.
        /// </summary>
        /// <param name="name">Friendly name of the performance monitor item</param>
        public static void LogCurrent(string name)
        {
            var item = Current;

            if (item == null)
            {
                return;
            }

            XDocument xdoc = item.GenerateXDocument(name);
            Tools.LogInfo("PerformanceMonitorItem", xdoc.ToString());
        }
    }
}