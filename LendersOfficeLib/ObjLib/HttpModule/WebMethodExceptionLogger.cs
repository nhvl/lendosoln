﻿namespace LendersOffice.HttpModule
{
    using System;
    using System.IO;
    using System.Text;
    using System.Web;
    using DataAccess;

    /// <summary>
    /// A response filter that looks for Webmethod exceptions, captures the content and replaces it with a generic one.
    /// </summary>
    public class WebMethodExceptionLogger : Stream
    {
        /// <summary>
        /// The underlying stream that will be output to the client.
        /// </summary>
        private readonly Stream baseStream;

        /// <summary>
        /// The stream that will capture the output when we detect
        /// a web method exception.
        /// </summary>
        private MemoryStream capturedStream;

        /// <summary>
        /// The response encoding in use to turn the content back into a string.
        /// </summary>
        private Encoding currentEncode;

        /// <summary>
        /// A value indicating whether we are currently capturing. It should only be true
        /// when we detect a 500 json response.
        /// </summary>
        private bool isCapturing;

        /// <summary>
        /// A value indicating whether the stream has been closed.
        /// </summary>
        private bool isClosed;

        /// <summary>
        /// A value indicating whether the stream is being closed.
        /// </summary>
        private bool isClosing;

        /// <summary>
        /// A value indicating whether the class has been initialized. This is needed
        /// because we cannot detect web methods until the caller is writing the response.
        /// </summary>
        private bool isInitialized;

        /// <summary>
        /// The http response for the current instance.
        /// </summary>
        private HttpResponse response;

        /// <summary>
        /// Initializes a new instance of the <see cref="WebMethodExceptionLogger" /> class.
        /// </summary>
        /// <param name="response">The web response.</param>
        public WebMethodExceptionLogger(HttpResponse response)
        {
            this.baseStream = response.Filter;
            this.response = response;
        }

        /// <summary>
        /// Gets a value indicating whether the stream can be read.
        /// </summary>
        /// <value>The field is not used.</value>
        public override bool CanRead
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the stream can be seeked.
        /// </summary>
        /// <value>The field is not used.</value>
        public override bool CanSeek
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the stream can be written to.
        /// </summary>
        /// <value>The field is not used.</value>
        public override bool CanWrite
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets the length of the stream. This class does not implement it.
        /// </summary>
        /// <value>The field is not used.</value>
        public override long Length
        {
            get { throw new NotSupportedException(); }
        }

        /// <summary>
        /// Gets or sets the current position of the stream. This class does not implement it.
        /// </summary>
        /// <value>The field is not used.</value>
        public override long Position
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        /// <summary>
        /// Closes the stream. In this case if we are capturing the input we have
        /// put the class into a state to trigger new output.
        /// </summary>
        public override void Close()
        {
            if (!this.isCapturing)
            {
                this.baseStream.Close();
                return;
            }

            this.isClosing = true;
            this.Flush();
            this.isClosed = true;
            this.isClosing = false;
            this.baseStream.Close();
        }

        /// <summary>
        /// Flushes the stream. This is pass through if we are not capturing.
        /// </summary>
        public override void Flush()
        {
            if (!this.isCapturing)
            {
                this.baseStream.Flush();
                return;
            }

            if (this.isClosing && !this.isClosed)
            {
                string msg;
                if (this.capturedStream.Length >= int.MaxValue)
                {
                    msg = this.currentEncode.GetString(this.capturedStream.ToArray());
                }
                else
                {
                    msg = this.currentEncode.GetString(this.capturedStream.GetBuffer(), 0, (int)this.capturedStream.Length);
                }

                Tools.LogError(msg);
                byte[] data = this.currentEncode.GetBytes(@"{""Message"":""System Error"",""ExceptionType"":""GenericUserException""}");
                this.baseStream.Write(data, 0, data.Length);
                this.baseStream.Flush();
            }
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="buffer">The buffer that wont be read from.</param>
        /// <param name="offset">The offset that wont be used.</param>
        /// <param name="count">The count that wont be read from the offset.</param>
        /// <returns>Not supported exception.</returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="offset">The parameter is not used.</param>
        /// <param name="direction">The parameter is not used.</param>
        /// <returns>Throws exception.</returns>
        public override long Seek(long offset, System.IO.SeekOrigin direction)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not supported.
        /// </summary>
        /// <param name="length">The length that wont be set.</param>
        public override void SetLength(long length)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Writes from the given buffer starting at offset until count is reached.
        /// </summary>
        /// <param name="buffer">The buffer we will read from.</param>
        /// <param name="offset">The position we will start copying from.</param>
        /// <param name="count">The number of bytes starting at offset.</param>
        public override void Write(byte[] buffer, int offset, int count)
        {
            // Cant put this in the constructor because we wont know at that time whether we should capture.
            // Theres no point to capture if we know that it is not an error. By doing it at the first write we know the status code
            // has been updated and the json error has been set.
            if (!this.isInitialized)
            {
                if (this.response.StatusCode == 500 && this.response.Headers["jsonerror"] == "true")
                {
                    this.isCapturing = true;
                    this.capturedStream = new MemoryStream();
                    this.currentEncode = this.response.ContentEncoding;
                }

                this.isInitialized = true;
            }

            Stream stream = this.isCapturing ? this.capturedStream : this.baseStream;
            stream.Write(buffer, offset, count);
        }
    }
}