/// Author: David Dao

using System;
using System.IO;
namespace LendersOffice.HttpModule
{
	public abstract class AbstractHttpOutputFilter : Stream
	{
        private Stream m_baseStream;

		protected AbstractHttpOutputFilter(Stream baseStream)
		{
            m_baseStream = baseStream;
		}

        protected Stream BaseStream 
        {
            get { return m_baseStream; }
        }
        public override bool CanRead 
        {
            get { return false; }
        }
        public override bool CanSeek 
        {
            get { return false; }
        }
        public override bool CanWrite 
        {
            get 
            {
                if (null == m_baseStream)
                    return false;
                return m_baseStream.CanWrite;
            }
        }
        public override void Close() 
        {
            if (null == m_baseStream)
                return;

            m_baseStream.Close();
        }
        public override void Flush() 
        {
            if (null == m_baseStream)
                return;

            m_baseStream.Flush();
        }
        public override long Length 
        {
            get { throw new NotSupportedException(); }
        }
        public override long Position 
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }
        public override long Seek(long offset, System.IO.SeekOrigin direction) 
        {
            throw new NotSupportedException();
        }
        public override void SetLength(long length) 
        {
            throw new NotSupportedException();
        }
        public override int Read(byte[] buffer, int offset, int count) 
        {
            throw new NotSupportedException();
        }

	}
}
