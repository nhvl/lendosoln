﻿namespace LendersOffice.HttpModule
{
    using System;
    using System.Web;
    using LendersOffice.Constants;

    /// <summary>
    /// Add Cors headers for the ConstSite.LoSite. If there is no ConstSite.LoSite defined 
    /// then this should be no op.
    /// </summary>
    public class LoSiteCorsHttpModule : IHttpModule
    {
        /// <summary>
        /// The site who we want to send cors headers to.
        /// </summary>
        private string websiteUrl;

        /// <summary>
        /// Disposes the web handler. This is a no op.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Instantiates the http module.
        /// </summary>
        /// <param name="context">The current application context.</param>
        public void Init(HttpApplication context)
        {
            this.websiteUrl = ConstSite.LOSite;

            if (!string.IsNullOrEmpty(this.websiteUrl))
            {
                context.BeginRequest += this.Context_BeginRequest;
            }
        }

        /// <summary>
        /// Event fired when a web request ends. We check to see if a origin was passed in 
        /// if so we check to see if it matches our site config and if so we send over http headers.
        /// </summary>
        /// <param name="sender">The application.</param>
        /// <param name="e">The arguments.</param>
        private void Context_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication application = (HttpApplication)sender;
            HttpContext context = application.Context;
            this.ManageCorsHeaders(context);
        }

        /// <summary>
        /// Appends the cors headers if needed.
        /// </summary>
        /// <param name="context">The http context of the web request.</param>
        private void ManageCorsHeaders(HttpContext context)
        {
            // We do not want to send over a empty or null header.
            if (string.IsNullOrEmpty(this.websiteUrl))
            {
                return;
            }

            var request = context.Request;
            string origin = request.Headers["Origin"];

            // If there is no origin we dont need the cors headers.
            if (string.IsNullOrEmpty(origin))
            {
                return;
            }

            if (origin.StartsWith(this.websiteUrl, StringComparison.InvariantCultureIgnoreCase))
            {
                var response = context.Response;
                response.AddHeader("Access-Control-Allow-Origin", origin);
                response.AddHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
                response.AddHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
                response.AddHeader("Access-Control-Expose-Headers", "Content-Disposition");

                // This is needed so secure sends cookies to edocs when called via js. 
                response.AddHeader("Access-Control-Allow-Credentials", "true");
            }
        }
    }
}
