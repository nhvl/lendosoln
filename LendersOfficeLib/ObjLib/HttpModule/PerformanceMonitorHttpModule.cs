/// Author: David Dao

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Xml.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;

namespace LendersOffice.HttpModule
{
    /// <summary>
    /// Capture the time to serve request on server side and the number of bytes in the response.
    /// </summary>
    public class PerformanceMonitorHttpModule : AbstractHttpModule
    {
        private static string GenerateUniqueId()
        {
            Guid guid = Guid.NewGuid();

            return Convert.ToBase64String(guid.ToByteArray());
        }

        public override void Init(HttpApplication application)
        {
            base.Init(application);
        }

        protected override void Application_BeginRequest(HttpContext context) 
        {
            if (context.Request.Path.EndsWith(".axd"))
                return; // 11/5/2008 dd - Do not hook in PerformanceMonitor for WebResource.axd in .NET 2.0+

            if (Tools.GetLogCorrelationId() == Guid.Empty)
            {
                Tools.ResetLogCorrelationId();
            }

            ResponseSizeFilter filter = new ResponseSizeFilter(context.Response.Filter);
            context.Response.Filter = filter;

            PerformanceMonitorItem item = PerformanceMonitorItem.GetOrCreate();

            item.SizeFilter = filter;

            item.AddTimingDetail("PerformanceMonitorHttpModule - Application_BeginRequest " + DateTime.Now.ToString("hh:mm:ss.fff"), 0);

        }
        public const string UniqueClientIdCookie = "__lqbuniqueid";
        private static string GetUniqueClientID(HttpRequest request)
        {
            if (request == null)
            {
                return GenerateUniqueId();
            }
            HttpCookie cookie = request.Cookies[UniqueClientIdCookie];
            if (null != cookie)
            {
                return cookie.Value;
            }
            return GenerateUniqueId();
        }
        private static void SetUniqueClientID(HttpResponse response, string uniqueId)
        {
            HttpCookie cookie = new HttpCookie(UniqueClientIdCookie, uniqueId);
            cookie.HttpOnly = true;
            // 2/12/2008 dd - If the code run on production then require cookie to be "secure". "Secure" cookie will only transmit through SSL.
            if (ConstSite.IsSecureCookieRequired)
            {
                cookie.Secure = true;
            }
            cookie.Expires = DateTime.Now.AddMonths(3);

            try
            {
                response.Cookies.Add(cookie);
            }
            catch (HttpException)
            {
                // 12/30/2011 dd - Ignore Http exception when adding cookie.
                // Without this it may throw System.Web.HttpException: Server cannot modify cookies after HTTP headers have been sent.
            }
        }
        protected override void Application_EndRequest(HttpContext context) 
        {
            if (context.Request.Path.EndsWith(".axd"))
                return; // 11/5/2008 dd - Do not hook in PerformanceMonitor for WebResource.axd in .NET 2.0+
            try 
            {
                PerformanceMonitorItem item = PerformanceMonitorItem.Current;

                if (item != null)
                {
                    item.ResponseLength = item.SizeFilter.Length;
                }

                #region Generate IIS Information to Log.
                Dictionary<string, string> logDictionary = new Dictionary<string, string>();
                logDictionary.Add("StatusCode", context.Response.StatusCode.ToString());
                logDictionary.Add("DateTime", context.Timestamp.Ticks.ToString());
                logDictionary.Add("Server", ConstAppDavid.ServerName);
                logDictionary.Add("Environment", ConstStage.EnvironmentName);
                logDictionary.Add("ClientIP", RequestHelper.ClientIP);
                logDictionary.Add("Url", context.Request.Url.GetComponents(UriComponents.SchemeAndServer | UriComponents.Path, UriFormat.UriEscaped));
                logDictionary.Add("Query", context.Request.Url.Query);
                logDictionary.Add("UserAgent", context.Request.UserAgent);
                logDictionary.Add("LogId", DataAccess.Tools.GetLogCorrelationId().ToString());
                logDictionary.Add("Correlation", Tools.ConvertGuidToFriendlyBase64(Tools.GetLogCorrelationId()));
                string cookieContent = "None";
                if (context.Request.Cookies[FormsAuthentication.FormsCookieName] != null)
                {
                    HttpCookie cookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];
                    cookieContent = cookie.Value;
                    try
                    {
                        FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookieContent);
                        if (ticket != null)
                        {
                            cookieContent = ticket.UserData;
                            logDictionary.Add("IsAuthCookieExpired", ticket.Expired.ToString() + " " + ticket.Expiration.ToString());

                        }
                    }
                    catch (ArgumentException)
                    {
                        cookieContent = "ERROR:" + cookieContent;
                    }

                }

                logDictionary.Add("AuthCookie", cookieContent);

                if (context.Request.UrlReferrer != null)
                {
                    logDictionary.Add("Referrer", context.Request.UrlReferrer.ToString());
                }
                logDictionary.Add("RequestSize", context.Request.ContentLength >= context.Request.TotalBytes ? context.Request.ContentLength.ToString() : context.Request.TotalBytes.ToString());
                logDictionary.Add("HttpMethod", context.Request.HttpMethod);

                string uniqueClientId = GetUniqueClientID(context.Request);
                logDictionary.Add("UniqueClientID", uniqueClientId);
                SetUniqueClientID(context.Response, uniqueClientId);

                GenerateLendingQBFingerPrint(context);

                AbstractUserPrincipal principal = context.User as AbstractUserPrincipal;
                if (principal != null)
                {
                    logDictionary.Add("BrokerId", principal.BrokerId.ToString());
                    logDictionary.Add("UserId", principal.UserId.ToString());
                    if (principal.BrokerDB != null)
                    {
                        logDictionary.Add("CustomerCode", principal.BrokerDB.CustomerCode);
                    }

                    string userName = principal.LoginNm;
                    if (principal.Type == "P")
                    {
                        userName += " (P)";
                    }
                    logDictionary.Add("UserName", userName);
                }
                if (item != null)
                {
                    logDictionary.Add("ResponseSize", item.ResponseLength.ToString());
                    logDictionary.Add("RequestUniqueId", item.UniqueId.ToString());

                    foreach (var customItem in item.GetCustomKeyValueList())
                    {
                        if (logDictionary.ContainsKey(customItem.Key) == false)
                        {
                            logDictionary.Add(customItem.Key, customItem.Value);
                        }
                    }

                    if (!logDictionary.ContainsKey("sLId") && item.sLId != Guid.Empty && !item.HasProcessedSeveralLoans)
                    {
                        logDictionary.Add("sLId", item.sLId.ToString());
                    }
                }
                #endregion

                IEnumerable<PerformanceMonitorItem._Item> rawTimingList = new List<PerformanceMonitorItem._Item>();
                IEnumerable<XElement> el = null;
                IEnumerable<XElement> fileDBEl = null;
                IEnumerable<PerformanceMonitorItem.SqlConnectionCountItem> sqlConnectionLeakList = null;
                if (null != item)
                {
                    rawTimingList = item.RawTimingDetailList;
                    el = item.Queries;
                    fileDBEl = item.FileDBElements;
                    sqlConnectionLeakList = item.GetPotentialConnectionLeaks();
                }

                SendToIisLogMsMq(logDictionary, context, rawTimingList, el, fileDBEl, sqlConnectionLeakList);
            } 
            catch (Exception exc)
            {
                DataAccess.Tools.LogError(exc);
            }

            PerformanceStopwatch.ResetAll(); // 3/19/2012 dd - Reset at the end of each request.
        }

        /// <summary>
        /// Generate a .lendingqb.com domain cookie to identify specific client browser. This will be use in Amazon EDocs to make sure the
        /// browser request resource is the same browser that retrieve resource.
        /// </summary>
        /// <param name="context"></param>
        private void GenerateLendingQBFingerPrint(HttpContext context)
        {
            if (!context.Request.Url.Host.EndsWith(".lendingqb.com", StringComparison.OrdinalIgnoreCase))
            {
                // Only generate finger print cookie for *.lendingqb.com domain.
                return;
            }

            var cookie = context.Request.Cookies[ConstApp.CookieFingerPrint];

            string id = string.Empty;

            if (cookie == null)
            {
                id = GenerateUniqueId();
            }
            else
            {
                id = cookie.Value;
            }

            cookie = new HttpCookie(ConstApp.CookieFingerPrint, id);
            cookie.HttpOnly = true;
            cookie.Domain = ".lendingqb.com";
            cookie.Expires = DateTime.Now.AddMinutes(ConstApp.LOCookieExpiresInMinutes);
            cookie.Secure = ConstSite.IsSecureCookieRequired;

            try
            {
                context.Response.Cookies.Set(cookie);
            }
            catch (HttpException)
            {
                // 12/30/2011 dd - Ignore Http exception when adding cookie.
                // Without this it may throw System.Web.HttpException: Server cannot modify cookies after HTTP headers have been sent.
            }
        }

        private void SendToIisLogMsMq(Dictionary<string, string> dictionary, HttpContext context, IEnumerable<PerformanceMonitorItem._Item> rawTimingList, IEnumerable<XElement> el, IEnumerable<XElement> fileDBEl,
            IEnumerable<PerformanceMonitorItem.SqlConnectionCountItem> sqlConnectionLeakList)
        {
            if (null == dictionary)
            {
                return;
            }
            XDocument xdoc = new XDocument();
            XElement root = new XElement("iis_log");
            xdoc.Add(root);

            foreach (var item in dictionary)
            {
                if (string.IsNullOrEmpty(item.Value) == false)
                {
                    root.SetAttributeValue(item.Key, item.Value);
                }
            }

            // 1/25/2012 dd - Stop the time right before we send to message queue.
            long ms = (DateTime.Now.Ticks - context.Timestamp.Ticks) / 10000L;

            root.SetAttributeValue("TimeProcessing", ms.ToString());

            if (el != null)
            {
                XElement q = new XElement("queries");
                foreach (XElement n in el)
                {
                    q.Add(n);
                }
                root.Add(q);
            }

            if (fileDBEl != null)
            {
                XElement q = new XElement("filedbs");
                foreach (XElement n in fileDBEl)
                {
                    q.Add(n);
                }
                root.Add(q);
            }

            XElement timingElement = new XElement("timings");
            root.Add(timingElement);
            foreach (var item in rawTimingList)
            {
                timingElement.Add(new XElement("item",
                    new XAttribute("threadid", item.ThreadId.ToString()),
                    new XAttribute("elapsed", item.ElapsedMs.ToString()),
                    new XAttribute("desc", item.Description),
                    new XAttribute("duration", item.DurationMs.ToString())));
            }

            if (sqlConnectionLeakList != null && sqlConnectionLeakList.Any())
            {
                XElement connectionLeakElement = new XElement("connectionleaks");
                root.Add(connectionLeakElement);
                root.SetAttributeValue("leakcount", sqlConnectionLeakList.Count().ToString());

                StringBuilder sb = new StringBuilder();
                sb.Append("[CONNECTION_LEAK] ");
                foreach (var o in sqlConnectionLeakList)
                {
                    connectionLeakElement.Add(new XElement("item",
                        new XAttribute("description", o.Description),
                        new XAttribute("count", o.Count.ToString())));
                    sb.AppendLine($"{o.Description} - {o.Count}");
                }
                Tools.LogError(sb.ToString());
            }

            DebugHttpRequest(xdoc);
            if (string.IsNullOrEmpty(ConstStage.MSMQ_IISLog) == false)
            {

                try
                {
                    Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.MSMQ_IISLog, null, xdoc);
                }
                catch (MessageQueueException exc)
                {
                    DataAccess.Tools.LogWarning("Unable to log to message queue. xdoc size=" + xdoc.ToString().Length, exc);
                }
                catch (Exception exc)
                {
                    DataAccess.Tools.LogErrorWithCriticalTracking(exc);
                    // Swallow exception.
                }
            }
        }
        [Conditional("DEBUG")]
        private void DebugHttpRequest(XDocument doc)
        {
            int dbBytes = 0;
            int queriesCount = 0;
            int initSaveCount = 0;
            int initLoadCount = 0;
            int fileDbBytes = 0;
            int fileDbGetCount = 0;
            int fileDbPutCount = 0;

            int timeProcessing = GetSafeAttrInt(doc.Root, "TimeProcessing");

            string url = doc.Root.Attribute("Url").Value;

            XElement queries = doc.Root.Element("queries");

            if (queries != null)
            {
                foreach (var el in queries.Elements("query"))
                {
                    int byteCount = GetSafeAttrInt(el, "bytessent") + GetSafeAttrInt(el, "bytesreceived");
                    queriesCount++;
                    dbBytes += byteCount;
                }
            }

            queries = doc.Root.Element("filedbs");
            if (queries != null)
            {
                foreach (var el in queries.Elements("filedb"))
                {
                    int byteCount = GetSafeAttrInt(el, "bytes");
                    fileDbBytes += byteCount;
                    string op = el.Attribute("op").Value;
                    if (op == "Get")
                    {
                        fileDbGetCount++;
                    }
                    else if (op == "Put")
                    {
                        fileDbPutCount++;
                    }
                }
            }

            XElement timings = doc.Root.Element("timings");

            if (timings != null)
            {
                foreach (var el in timings.Elements("item"))
                {
                    string desc = el.Attribute("desc").Value;

                    if (desc.Contains(".InitSave()"))
                    {
                        initSaveCount++;
                    }

                    if (desc.Contains(".InitLoad()"))
                    {
                        initLoadCount++;
                    }
                }
            }
            
            var connectionLeakCount = int.Parse(doc.Root.Attribute("leakcount")?.Value ?? "0");

            string pageStatus = GetPageStatus(dbBytes, queriesCount, initSaveCount, initLoadCount, timeProcessing, fileDbBytes, connectionLeakCount);

            Tuple<string, string>[] statPairs =
            {
                Tuple.Create("SQL Bytes", dbBytes.ToString("#,0")),
                Tuple.Create("FileDB Bytes", fileDbBytes.ToString("#,0")),
                Tuple.Create("# Queries", queriesCount.ToString("#,0")),
                Tuple.Create("# InitLoad", initLoadCount.ToString()),
                Tuple.Create("# InitSave", initSaveCount.ToString()),
                Tuple.Create("FileDB.GetFile", fileDbGetCount.ToString()),
                Tuple.Create("FileDB.PutFile", fileDbPutCount.ToString()),
                Tuple.Create("Connection Leaks", connectionLeakCount.ToString()),
                Tuple.Create("Time", timeProcessing.ToString("#,#") + "ms"),
                Tuple.Create("Url", url)
            };

            string msg = pageStatus + ":: " + string.Join(", ", statPairs.Select(pair => pair.Item1 + "=" + pair.Item2)) + Environment.NewLine + doc;

            if (pageStatus == "GOOD")
            {
                Tools.LogInfo(msg);
            }
            else
            {
                Tools.LogWarning(msg);
            }
        }

        private static string GetPageStatus(int dbBytes, int queriesCount, int initSaveCount, int initLoadCount, int timeProcessing, int fileDbBytes, int connectionLeakCount)
        {
            // dd 8/6/2015 - Feel free to update the logic.
            var warnings = new List<string>();

            if (dbBytes > 300000 || queriesCount > 50)
            {
                warnings.Add("TOO MUCH SQL");
            }

            if (initSaveCount > 5)
            {
                warnings.Add("TOO MUCH InitSave");
            }

            if (initLoadCount > 5)
            {
                warnings.Add("TOO MUCH InitLoad");
            }

            if (timeProcessing > 5000)
            {
                warnings.Add("TOO SLOW");
            }

            if (fileDbBytes > 300000)
            {
                warnings.Add("TOO MUCH FILEDB");
            }

            if (connectionLeakCount > 0)
            {
                warnings.Add("CONNECTION LEAK");
            }

            return warnings.Any() ? string.Join(", ", warnings) : "GOOD";
        }

        private int GetSafeAttrInt(XElement el, string attrName)
        {
            int v = 0;
            XAttribute attr = el.Attribute(attrName);
            if (null != attr)
            {
                int.TryParse(attr.Value, out v);
            }
            return v;
        }
    }
}