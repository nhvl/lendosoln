﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Services.Protocols;
using DataAccess;
using System.IO;
using System.Xml;
using System.Diagnostics;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Webservices;

namespace LendersOffice.HttpModule
{
    public class LendingQBSoapExtension : SoapExtension
    {
        private static string[] IGNORE_EXCEPTION_LIST ={
                                                           "LendersOfficeApp.WebService.IntegrationInvalidLoanNumberException",
                                                           "LendersOffice.Security.AccessDenied",
                                                       };
        private Stopwatch m_stopwatch = null;
        private bool shouldWarnOnly = false;

        public override object GetInitializer(Type serviceType)
        {
            return null;
        }

        public override object GetInitializer(LogicalMethodInfo methodInfo, SoapExtensionAttribute attribute)
        {
            return null;
        }
        public override void Initialize(object initializer)
        {
            m_stopwatch = Stopwatch.StartNew();
        }

        public override Stream ChainStream(Stream stream)
        {
            return stream;
        }

        public override void ProcessMessage(SoapMessage message)
        {
            switch (message.Stage)
            {
                case SoapMessageStage.BeforeDeserialize:
                    break;
                case SoapMessageStage.AfterDeserialize:
                    break;
                case SoapMessageStage.BeforeSerialize:
                    if (message.Exception != null && message.Exception.InnerException is WarningOnlyWebserviceException)
                    {

                        var interceptedException = (message.Exception.InnerException as WarningOnlyWebserviceException).InnerException;
                        message.Exception = new SoapException(message.Exception.Message, message.Exception.Code, interceptedException);
                        shouldWarnOnly = true;
                    }
                    break;
                case SoapMessageStage.AfterSerialize:
                    if (message.Exception != null)
                    {
                        LogException(message);
                    }

                    m_stopwatch.Stop();
                    if (m_stopwatch.ElapsedMilliseconds > 10000)
                    {
                        Tools.LogInfo("Execute " + message.Url + "::" + message.MethodInfo + " in " + m_stopwatch.ElapsedMilliseconds + " ms.");
                    }

                    PerformanceMonitorItem item = PerformanceMonitorItem.Current;
                    if (item != null)
                    {
                        item.SetCustomKeyValue("SOAPMethod", message.MethodInfo.Name);
                    }
                    break;
                default:
                    throw new UnhandledEnumException(message.Stage);
            }
        }

        private void LogException(SoapMessage message)
        {
            SoapException soapException = message.Exception;
            Exception innerException = soapException.InnerException;

            if (innerException != null)
            {
                if (IGNORE_EXCEPTION_LIST.Contains(innerException.GetType().FullName))
                {
                    // NO NEED TO LOG.
                    return;
                }
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendLine(message.Url + ", " + message.SoapVersion);

            try
            {
                sb.AppendLine("Method: " + message.MethodInfo);
            }
            catch (NullReferenceException)
            {
                // 10/5/2011 dd - For some reason MethodInfo could throw NullReferenceException when namespace is invalid.
            }

            sb.AppendLine();
            sb.AppendLine(soapException.Message);

            if (innerException != null)
            {
                sb.AppendLine("InnerExceptionType: " + innerException.GetType().FullName);
                sb.AppendLine("InnerException: " + innerException.Message);
                sb.AppendLine("   Stack Trace: " + innerException.StackTrace);

                // consider making this recursive, but we have to watch out for overflowing the string builder and losing the log entirely.
                var innerInnerException = innerException.InnerException;
                if (innerInnerException != null)
                {
                    sb.AppendLine("InnerInnerExceptionType: " + innerInnerException.GetType().FullName);
                    sb.AppendLine("InnerInnerException: " + innerInnerException.Message);
                    sb.AppendLine("   InnerInner Stack Trace: " + innerInnerException.StackTrace);
                }
            }
            

            sb.AppendLine();
            sb.AppendLine(soapException.StackTrace);

            if (shouldWarnOnly)
            {
                Tools.LogWarning(sb.ToString());
            }
            else
            {
               Tools.LogError(sb.ToString());
            }
        }
    }
}
