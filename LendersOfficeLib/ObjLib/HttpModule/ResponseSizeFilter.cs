/// Author: David Dao

using System;
using System.IO;

namespace LendersOffice.HttpModule
{
	public class ResponseSizeFilter : AbstractHttpOutputFilter
	{
        private long m_total = 0;
		public ResponseSizeFilter(Stream originalFilter) : base(originalFilter)
		{
		}

        public override void Write(byte[] buffer, int offset, int count) 
        {
            m_total += count;
            if (null != this.BaseStream) 
            {
                this.BaseStream.Write(buffer, offset, count);
            }
        }

        public override long Length 
        {
            get { return m_total; }
        }
	}
}
