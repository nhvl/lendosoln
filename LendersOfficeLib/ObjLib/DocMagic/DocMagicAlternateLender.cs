﻿namespace LendersOffice.ObjLib.DocMagicLib
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using LendersOffice.Common;

    public class DocMagicAlternateLender
    {
        public DocMagicAlternateLender()
        {
            m_DocMagicAlternateLenderId = Guid.Empty;
        }

        public DocMagicAlternateLender(Guid brokerId, Guid id)
        {
            Load(brokerId, id);
        }

        private void Load(Guid brokerId, Guid id)
        {
            m_DocMagicAlternateLenderId = id;

            if (id == Guid.Empty)
            {
                return;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@DocMagicAlternateLenderId", id)
                                        };

            using (var dr = StoredProcedureHelper.ExecuteReader(brokerId, "DocMagic_RetrieveAlternateLenderById", parameters))
            {
                if (dr.Read())
                {
                    DocMagicAlternateLenderInternalId = dr["DocMagicAlternateLenderInternalId"].ToString();
                    LenderName = dr["LenderName"].ToString();
                    LenderAddress = dr["LenderAddress"].ToString();
                    LenderCity = dr["LenderCity"].ToString();
                    LenderState = dr["LenderState"].ToString();
                    LenderZip = dr["LenderZip"].ToString();
                    LenderCounty = dr["LenderCounty"].ToString();
                    LenderNonPersonEntityIndicator = (bool)dr["LenderNonPersonEntityIndicator"];
                    BeneficiaryName = dr["BeneficiaryName"].ToString();
                    BeneficiaryAddress = dr["BeneficiaryAddress"].ToString();
                    BeneficiaryCity = dr["BeneficiaryCity"].ToString();
                    BeneficiaryState = dr["BeneficiaryState"].ToString();
                    BeneficiaryZip = dr["BeneficiaryZip"].ToString();
                    BeneficiaryCounty = dr["BeneficiaryCounty"].ToString();
                    BeneficiaryNonPersonEntityIndicator = (bool)dr["BeneficiaryNonPersonEntityIndicator"];
                    LossPayeeName = dr["LossPayeeName"].ToString();
                    LossPayeeAddress = dr["LossPayeeAddress"].ToString();
                    LossPayeeCity = dr["LossPayeeCity"].ToString();
                    LossPayeeState = dr["LossPayeeState"].ToString();
                    LossPayeeZip = dr["LossPayeeZip"].ToString();
                    LossPayeeCounty = dr["LossPayeeCounty"].ToString();
                    MakePaymentsToName = dr["MakePaymentsToName"].ToString();
                    MakePaymentsToAddress = dr["MakePaymentsToAddress"].ToString();
                    MakePaymentsToCity = dr["MakePaymentsToCity"].ToString();
                    MakePaymentsToState = dr["MakePaymentsToState"].ToString();
                    MakePaymentsToZip = dr["MakePaymentsToZip"].ToString();
                    MakePaymentsToCounty = dr["MakePaymentsToCounty"].ToString();
                    WhenRecodedMailToName = dr["WhenRecodedMailToName"].ToString();
                    WhenRecodedMailToAddress = dr["WhenRecodedMailToAddress"].ToString();
                    WhenRecodedMailToCity = dr["WhenRecodedMailToCity"].ToString();
                    WhenRecodedMailToState = dr["WhenRecodedMailToState"].ToString();
                    WhenRecodedMailToZip = dr["WhenRecodedMailToZip"].ToString();
                    WhenRecodedMailToCounty = dr["WhenRecodedMailToCounty"].ToString();
                }
                else
                {
                    throw new CBaseException(ErrorMessages.FailedToLoad, "Unable to load alternate lender " + id);
                }
            }
        }

        public void Save(Guid brokerId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();
            string spName = "DocMagic_EditAlternateLender";
            if (m_DocMagicAlternateLenderId == Guid.Empty)
            {
                spName = "DocMagic_CreateAlternateLender";
                m_DocMagicAlternateLenderId = Guid.NewGuid();
                parameters.Add(new SqlParameter("@BrokerId", brokerId));
            }

            parameters.Add(new SqlParameter("@DocMagicAlternateLenderId", DocMagicAlternateLenderId));
            parameters.Add(new SqlParameter("@DocMagicAlternateLenderInternalId", DocMagicAlternateLenderInternalId));
            parameters.Add(new SqlParameter("@LenderName", LenderName));
            parameters.Add(new SqlParameter("@LenderAddress", LenderAddress));
            parameters.Add(new SqlParameter("@LenderCity", LenderCity));
            parameters.Add(new SqlParameter("@LenderState", LenderState));
            parameters.Add(new SqlParameter("@LenderZip", LenderZip));
            parameters.Add(new SqlParameter("@LenderCounty", LenderCounty));
            parameters.Add(new SqlParameter("@LenderNonPersonEntityIndicator", LenderNonPersonEntityIndicator));
            parameters.Add(new SqlParameter("@BeneficiaryName", BeneficiaryName));
            parameters.Add(new SqlParameter("@BeneficiaryAddress", BeneficiaryAddress));
            parameters.Add(new SqlParameter("@BeneficiaryCity", BeneficiaryCity));
            parameters.Add(new SqlParameter("@BeneficiaryState", BeneficiaryState));
            parameters.Add(new SqlParameter("@BeneficiaryZip", BeneficiaryZip));
            parameters.Add(new SqlParameter("@BeneficiaryCounty", BeneficiaryCounty));
            parameters.Add(new SqlParameter("@BeneficiaryNonPersonEntityIndicator", BeneficiaryNonPersonEntityIndicator));
            parameters.Add(new SqlParameter("@LossPayeeName", LossPayeeName));
            parameters.Add(new SqlParameter("@LossPayeeAddress", LossPayeeAddress));
            parameters.Add(new SqlParameter("@LossPayeeCity", LossPayeeCity));
            parameters.Add(new SqlParameter("@LossPayeeState", LossPayeeState));
            parameters.Add(new SqlParameter("@LossPayeeZip", LossPayeeZip));
            parameters.Add(new SqlParameter("@LossPayeeCounty", LossPayeeCounty));
            parameters.Add(new SqlParameter("@MakePaymentsToName", MakePaymentsToName));
            parameters.Add(new SqlParameter("@MakePaymentsToAddress", MakePaymentsToAddress));
            parameters.Add(new SqlParameter("@MakePaymentsToCity", MakePaymentsToCity));
            parameters.Add(new SqlParameter("@MakePaymentsToState", MakePaymentsToState));
            parameters.Add(new SqlParameter("@MakePaymentsToZip", MakePaymentsToZip));
            parameters.Add(new SqlParameter("@MakePaymentsToCounty", MakePaymentsToCounty));
            parameters.Add(new SqlParameter("@WhenRecodedMailToName", WhenRecodedMailToName));
            parameters.Add(new SqlParameter("@WhenRecodedMailToAddress", WhenRecodedMailToAddress));
            parameters.Add(new SqlParameter("@WhenRecodedMailToCity", WhenRecodedMailToCity));
            parameters.Add(new SqlParameter("@WhenRecodedMailToState", WhenRecodedMailToState));
            parameters.Add(new SqlParameter("@WhenRecodedMailToZip", WhenRecodedMailToZip));
            parameters.Add(new SqlParameter("@WhenRecodedMailToCounty", WhenRecodedMailToCounty));

            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, spName, parameters))
            {
                if (dr.HasRows)
                {
                    throw new CBaseException(ErrorMessages.FailedToSave, "Failed to save new alternate lender for brokerid:" + brokerId);
                }
            }
        }

        public static void DeleteAlternateLenderId(Guid brokerId, Guid id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocMagicAlternateLenderId", id)
                                        };
            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, "DocMagic_DeleteAlternateLender", parameters))
            {
                if (dr.HasRows)
                {
                    throw new CBaseException(ErrorMessages.FailedToDelete, "Unable to delete alternate lender " + id);
                }
            }
        }

        public static List<Tuple<string, Guid>> GetAlternateLenderListByBroker(Guid brokerId)
        {
            List<Tuple<string, Guid>> data = new List<Tuple<string, Guid>>();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId)
                                        };

            using (DbDataReader dr = StoredProcedureHelper.ExecuteReader(brokerId, "DocMagic_ListAlternateLenderByBroker", parameters ))
            {
                while (dr.Read())
                {
                    data.Add(new Tuple<string, Guid>(dr["LenderName"].ToString(), new Guid(dr["DocMagicAlternateLenderId"].ToString())));
                }
            }

            return data;
        }

        #region Fields
        private Guid m_DocMagicAlternateLenderId;

        public Guid DocMagicAlternateLenderId
        {
            get
            {
                return m_DocMagicAlternateLenderId;
            }
        }

        /// <summary>
        /// The internal ID for the alternate lender used on DocMagic's end.
        /// </summary>
        public string DocMagicAlternateLenderInternalId { get; set; }

        public string LenderName { get; set; }
        public string LenderAddress { get; set; }
        public string LenderCity { get; set; }
        public string LenderState { get; set; }
        public string LenderZip { get; set; }
        public string LenderCounty { get; set; }
        public bool LenderNonPersonEntityIndicator { get; set; }


        private string beneficiaryName;
        public string BeneficiaryName
        {
            get { return BeneficiaryCopyFromLender ? LenderName : beneficiaryName; }
            set { beneficiaryName = value; }
        }

        private string beneficiaryAddress;
        public string BeneficiaryAddress
        {
            get { return BeneficiaryCopyFromLender ? LenderAddress : beneficiaryAddress; }
            set { beneficiaryAddress = value; }
        }

        private string beneficiaryState;
        public string BeneficiaryState
        {
            get { return BeneficiaryCopyFromLender ? LenderState : beneficiaryState; }
            set { beneficiaryState = value; }
        }

        private string beneficiaryCity;
        public string BeneficiaryCity
        {
            get { return BeneficiaryCopyFromLender ? LenderCity : beneficiaryCity; }
            set { beneficiaryCity = value; }
        }

        private string beneficiaryZip;
        public string BeneficiaryZip
        {
            get { return BeneficiaryCopyFromLender ? LenderZip : beneficiaryZip; }
            set { beneficiaryZip = value; }
        }

        private string beneficiaryCounty;
        public string BeneficiaryCounty
        {
            get { return BeneficiaryCopyFromLender ? LenderCounty : beneficiaryCounty; }
            set { beneficiaryCounty = value; }
        }

        private bool beneficiaryNonPersonEntityIndicator;
        public bool BeneficiaryNonPersonEntityIndicator
        {
            get { return BeneficiaryCopyFromLender ? LenderNonPersonEntityIndicator : beneficiaryNonPersonEntityIndicator; }
            set { beneficiaryNonPersonEntityIndicator = value; }
        }

        private string lossPayeeName;
        public string LossPayeeName
        {
            get { return LossPayeeCopyFromLender ? LenderName : lossPayeeName; }
            set { lossPayeeName = value; }
        }

        private string lossPayeeAddress;
        public string LossPayeeAddress
        {
            get { return LossPayeeCopyFromLender ? LenderAddress : lossPayeeAddress; }
            set { lossPayeeAddress = value; }
        }

        private string lossPayeeCity;
        public string LossPayeeCity
        {
            get { return LossPayeeCopyFromLender ? LenderCity : lossPayeeCity; }
            set { lossPayeeCity = value; }
        }

        private string lossPayeeState;
        public string LossPayeeState
        {
            get { return LossPayeeCopyFromLender ? LenderState : lossPayeeState; }
            set { lossPayeeState = value; }
        }

        private string lossPayeeZip;
        public string LossPayeeZip
        {
            get { return LossPayeeCopyFromLender ? LenderZip : lossPayeeZip; }
            set { lossPayeeZip = value; }
        }

        private string lossPayeeCounty;
        public string LossPayeeCounty
        {
            get { return LossPayeeCopyFromLender ? LenderCounty : lossPayeeCounty; }
            set { lossPayeeCounty = value; }
        }

        private string makePaymentsToName;
        public string MakePaymentsToName
        {
            get { return MakePaymentsToCopyFromLender ? LenderName : makePaymentsToName; }
            set { makePaymentsToName = value; }
        }

        private string makePaymentsToAddress;
        public string MakePaymentsToAddress
        {
            get { return MakePaymentsToCopyFromLender ? LenderAddress : makePaymentsToAddress; }
            set { makePaymentsToAddress = value; }
        }

        private string makePaymentsToCity;
        public string MakePaymentsToCity
        {
            get { return MakePaymentsToCopyFromLender ? LenderCity : makePaymentsToCity; }
            set { makePaymentsToCity = value; }
        }

        private string makePaymentsToState;
        public string MakePaymentsToState
        {
            get { return MakePaymentsToCopyFromLender ? LenderState : makePaymentsToState; }
            set { makePaymentsToState = value; }
        }

        private string makePaymentsToZip;
        public string MakePaymentsToZip
        {
            get { return MakePaymentsToCopyFromLender ? LenderZip : makePaymentsToZip; }
            set { makePaymentsToZip = value; }
        }

        private string makePaymentsToCounty;
        public string MakePaymentsToCounty
        {
            get { return MakePaymentsToCopyFromLender ? LenderCounty : makePaymentsToCounty; }
            set { makePaymentsToCounty = value; }
        }

        private string whenRecodedMailToName;
        public string WhenRecodedMailToName
        {
            get { return WhenRecodedMailToCopyFromLender ? LenderName : whenRecodedMailToName; }
            set { whenRecodedMailToName = value; }
        }

        private string whenRecodedMailToAddress;
        public string WhenRecodedMailToAddress
        {
            get { return WhenRecodedMailToCopyFromLender ? LenderAddress : whenRecodedMailToAddress; }
            set { whenRecodedMailToAddress = value; }
        }

        private string whenRecodedMailToCity;
        public string WhenRecodedMailToCity
        {
            get { return WhenRecodedMailToCopyFromLender ? LenderCity : whenRecodedMailToCity; }
            set { whenRecodedMailToCity = value; }
        }

        private string whenRecodedMailToState;
        public string WhenRecodedMailToState
        {
            get { return WhenRecodedMailToCopyFromLender ? LenderState : whenRecodedMailToState; }
            set { whenRecodedMailToState = value; }
        }

        private string whenRecodedMailToZip;
        public string WhenRecodedMailToZip
        {
            get { return WhenRecodedMailToCopyFromLender ? LenderZip : whenRecodedMailToZip; }
            set { whenRecodedMailToZip = value; }
        }

        private string whenRecodedMailToCounty;
        public string WhenRecodedMailToCounty
        {
            get { return WhenRecodedMailToCopyFromLender ? LenderCounty : whenRecodedMailToCounty; }
            set { whenRecodedMailToCounty = value; }
        }


        public bool BeneficiaryNameReadOnly { get { return BeneficiaryCopyFromLender; } }
        public bool BeneficiaryAddressReadOnly { get { return BeneficiaryCopyFromLender; } }
        public bool BeneficiaryCityReadOnly { get { return BeneficiaryCopyFromLender; } }
        public bool BeneficiaryStateReadOnly { get { return BeneficiaryCopyFromLender; } }
        public bool BeneficiaryZipReadOnly { get { return BeneficiaryCopyFromLender; } }
        public bool BeneficiaryCountyReadOnly { get { return BeneficiaryCopyFromLender; } }
        public bool BeneficiaryNonPersonEntityIndicatorReadOnly { get { return BeneficiaryCopyFromLender; } }
        public bool LossPayeeNameReadOnly { get { return LossPayeeCopyFromLender; } }
        public bool LossPayeeAddressReadOnly { get { return LossPayeeCopyFromLender; } }
        public bool LossPayeeCityReadOnly { get { return LossPayeeCopyFromLender; } }
        public bool LossPayeeStateReadOnly { get { return LossPayeeCopyFromLender; } }
        public bool LossPayeeZipReadOnly { get { return LossPayeeCopyFromLender; } }
        public bool LossPayeeCountyReadOnly { get { return LossPayeeCopyFromLender; } }
        public bool MakePaymentsToNameReadOnly { get { return MakePaymentsToCopyFromLender; } }
        public bool MakePaymentsToAddressReadOnly { get { return MakePaymentsToCopyFromLender; } }
        public bool MakePaymentsToCityReadOnly { get { return MakePaymentsToCopyFromLender; } }
        public bool MakePaymentsToStateReadOnly { get { return MakePaymentsToCopyFromLender; } }
        public bool MakePaymentsToZipReadOnly { get { return MakePaymentsToCopyFromLender; } }
        public bool MakePaymentsToCountyReadOnly { get { return MakePaymentsToCopyFromLender; } }
        public bool WhenRecodedMailToNameReadOnly { get { return WhenRecodedMailToCopyFromLender; } }
        public bool WhenRecodedMailToAddressReadOnly { get { return WhenRecodedMailToCopyFromLender; } }
        public bool WhenRecodedMailToCityReadOnly { get { return WhenRecodedMailToCopyFromLender; } }
        public bool WhenRecodedMailToStateReadOnly { get { return WhenRecodedMailToCopyFromLender; } }
        public bool WhenRecodedMailToZipReadOnly { get { return WhenRecodedMailToCopyFromLender; } }
        public bool WhenRecodedMailToCountyReadOnly { get { return WhenRecodedMailToCopyFromLender; } }

        public bool BeneficiaryCopyFromLender { get; set; } = false;
        public bool LossPayeeCopyFromLender { get; set; } = false;
        public bool MakePaymentsToCopyFromLender { get; set; } = false;
        public bool WhenRecodedMailToCopyFromLender { get; set; } = false;

        #endregion
    }
}