﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;

namespace LendersOffice.ObjLib.DocMagicLib
{
    public static class DocMagicXpathMappingExtensions
    {
        /// <summary>
        /// Saves all of the provided DocMagic XPath mappings.
        /// </summary>
        /// <param name="toSave"></param>
        /// <returns></returns>
        public static void SaveAll(this ICollection<DocMagicXpathMapping> toSave)
        {
            foreach (var element in toSave)
            {
                element.Save();
            }
        }
    }

    public class DocMagicXpathMapping
    {
        public string XPath { get; private set; }
        public string DefaultURL { get; private set; }
        public string AltURL { get; private set; }
        public string URLDependsOnFieldId { get; private set; }
        public string FieldId { get; private set; }

        /// <summary>
        /// Retrieves all DocMagic XPath mappings currently in the database.
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<DocMagicXpathMapping> RetrieveAll()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("XPath", DBNull.Value)
                                        };
            DataTable dt = StoredProcedureHelper.ExecuteDataTable(DataSrc.LOShare, "DocMagic_XPath_Retrieve", parameters);

            foreach (DataRow dr in dt.Rows)
            {
                yield return new DocMagicXpathMapping(dr);
            }
        }

        /// <summary>
        /// Deletes all DocMagic XPath mappings currently in the database.
        /// </summary>
        public static void DeleteAll()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("XPath", DBNull.Value)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DocMagic_XPath_Delete", 0, parameters);
        }

        /// <summary>
        /// Returns a dictionary of XPath :: XPath Mapping. If any of the XPaths are not found, their values will be null.
        /// </summary>
        /// <param name="XPaths">A list of xpaths to look up</param>
        /// <returns></returns>
        public static Dictionary<string, DocMagicXpathMapping> Retrieve(HashSet<string> XPaths)
        {
            var ret = new Dictionary<string, DocMagicXpathMapping>(XPaths.Count());
            foreach (var xpath in XPaths)
            {
                ret.Add(xpath, Retrieve(xpath));
            }

            return ret;
        }

        /// <summary>
        /// Retrieves a particular XPath mapping. If none exists, returns null.
        /// </summary>
        /// <param name="XPath"></param>
        /// <returns>The mapping if it exists, null otherwise.</returns>
        public static DocMagicXpathMapping Retrieve(string XPath)
        {
            if (string.IsNullOrEmpty(XPath)) return null;

            DataSet ds = new DataSet();
            SqlParameter[] parameters = {
                                            new SqlParameter("XPath", XPath)
                                        };
            DataTable dt = StoredProcedureHelper.ExecuteDataTable(DataSrc.LOShare, "DocMagic_XPath_Retrieve", parameters);
            if (dt.Rows.Count == 1)
            {
                return new DocMagicXpathMapping(dt.Rows[0]);
            }
            else
            {
                return null;
            }
        }

        public DocMagicXpathMapping(DataRow dr)
        {
            XPath = dr["XPath"].ToString();
            DefaultURL = dr["DefaultURL"].ToString();
            AltURL = dr["AltURL"].ToString();
            URLDependsOnFieldId = dr["URLDependsOnFieldId"].ToString();
            FieldId = dr["FieldId"].ToString();
        }

        public DocMagicXpathMapping(string xpath, string defaulturl, string alturl, string urldependsonfieldid, string fieldid)
        {
            XPath = xpath;
            DefaultURL = defaulturl;
            AltURL = alturl;
            URLDependsOnFieldId = urldependsonfieldid;
            FieldId = fieldid;
        }

        public void Save()
        {
            var parms = GetParameters();

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "DocMagic_XPath_Save", 0, parms);
        }

        private SqlParameter[] GetParameters()
        {
            return new[] 
            { 
                new SqlParameter("@XPath", XPath), 
                new SqlParameter("@DefaultURL", DefaultURL), 
                new SqlParameter("@AltURL", AltURL), 
                new SqlParameter("@URLDependsOnFieldId", URLDependsOnFieldId), 
                new SqlParameter("@FieldId", FieldId) 
            };
        }
    }
}
