﻿namespace LendersOffice.ObjLib.DocMagicLib
{
    /// <summary>
    /// Result types when validating a DocMagic plan code.
    /// </summary>
    public enum ValidatePlanCodeResult
    {
        /// <summary>
        /// No conclusive results received on validity of the plan code.
        /// </summary>
        NoResult,

        /// <summary>
        /// The plan code is valid.
        /// </summary>
        Valid,

        /// <summary>
        /// The plan code is invalid.
        /// </summary>
        Invalid,
    }
}
