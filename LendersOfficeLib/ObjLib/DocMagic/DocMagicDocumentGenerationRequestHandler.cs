﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DataAccess;
using DocMagic.DsiDocRequest;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Conversions;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.Security;

namespace LendersOffice.ObjLib.DocMagicLib
{
    public sealed class DocMagicDocumentGenerationRequestHandler : IDocumentGenerationRequest
    {

        private bool m_EmailDocuments;
        private string m_EmailToAddress;
        private string m_EmailPassword;
        private bool   m_EmailNotifyOnRetrieval;
        private bool m_sendEdisclosures;
        private bool m_sendEDisclosuresAllowOnlineSigning;
        private Guid m_brokerId;

        private E_DocumentFormatType m_format = E_DocumentFormatType.PDF;
        private bool m_DsiFeatureEnabled;
        private string m_DsiAttention;
        private string m_DsiName;
        private string m_DsiPhone;
        private string m_DsiNotes;
        private string m_DsiStreet;
        private string m_DsiCity;
        private string m_DsiState;
        private string m_DsiZip;

        private string m_CustomerId;
        private string m_Userid;
        private string m_Password;
        private Guid m_LoanId;
        private string m_WebsheetNumber;
        private Guid m_appId; 
        private string m_sLNm;
        private string m_sHint = ""; 

        private ICollection<String> FatalErrors
        {
            get;
            set; 
        }

        private string PDFLocationKey
        {
            get;
            set; 
        }


        public string DocumentFormat
        {
            get { return Enum.GetName(typeof(E_DocumentFormatType), m_format); }
            set { m_format = (E_DocumentFormatType)Enum.Parse(typeof(E_DocumentFormatType), value); }
        }

        public DocMagicDocumentGenerationRequestHandler(Guid brokerId, string customerId, string userid, string password, Guid loanId, string sLNm, Guid primaryAppId, string websheetNumber)
        {
            m_CustomerId = customerId;
            m_Userid = userid;
            m_Password = password;
            m_LoanId = loanId;
            m_WebsheetNumber = websheetNumber;
            m_appId = primaryAppId;
            m_sLNm = sLNm;
            m_brokerId = brokerId;
            this.IndividualForms = new List<FormInfo>();
        }


        private E_ProcessPackageType m_packageType;
        public string PackageType
        {
            get
            {
                return Enum.GetName(typeof(E_ProcessPackageType), m_packageType);
            }
            set
            {
                m_packageType = (E_ProcessPackageType)Enum.Parse(typeof(E_ProcessPackageType), value);
            }
        }

        /// <remarks>
        /// This version of the document framework is deprecated, so we aren't worrying about making this nicer for now.
        /// </remarks>
        public LqbGrammar.DataTypes.DocumentIntegrationPackageName PackageName { get; set; }

        public bool IsMersRegistrationEnabled { get; set; }

        private bool IsPasswordRequired
        {
            get
            {
                return false == string.IsNullOrEmpty(m_EmailPassword);
            }
        }

        //Unused in DocMagic
        public string LoanProgramId { get; set; }


        public void SendEDisclosures(bool allowSigning)
        {
            m_sendEdisclosures = true;
            m_sendEDisclosuresAllowOnlineSigning = allowSigning;
        }

        /// <summary>
        /// Sets the eClose option in the document request.
        /// </summary>
        /// <param name="eCloseEnabled">A boolean indicating whether eClose is enabled.</param>
        /// <remarks>Legacy DocMagic does not support eClose.</remarks>
        public void SetEClose(bool eCloseEnabled)
        {
            return;
        }

        public bool IsSendEDisclosures
        {
            get { return this.m_sendEdisclosures; }
        }

        /// <summary>
        /// Gets a value indicating whether the document request has eClose selected.
        /// </summary>
        /// <remarks>Legacy DocMagic does not support eClose.</remarks>
        public bool IsEClosing => false;

        public bool IsESignEnabled
        {
            get { return this.m_sendEDisclosuresAllowOnlineSigning; }
        }

        public void EmailDocumentsTo(string email, string requiredPassword, bool notifyOnRetrieval, string hint)
        {
            m_EmailDocuments = true;
            m_EmailToAddress = email.Replace(" ", "");
            m_EmailPassword = requiredPassword;
            m_EmailNotifyOnRetrieval = notifyOnRetrieval;
            m_sHint = hint;
        }

        public bool IsEmailDocumentsSelected
        {
            get { return this.m_EmailDocuments; }
        }

        public void EnableDsiPrintAndDeliver(string attention, string name, string phone, string notes, string street, string city, string state, string zip)
        {
            m_DsiFeatureEnabled = true;
            m_DsiAttention = attention;
            m_DsiName = name;
            m_DsiPhone = phone;
            m_DsiNotes = notes;
            m_DsiStreet = street;
            m_DsiCity = city;
            m_DsiState = state;
            m_DsiZip = zip;
        }

        public bool IsManualFulfillment
        {
            get { return this.m_DsiFeatureEnabled; }
        }

        private List<Form> m_Forms;
        private void PopulateForms()
        {
            if (this.IndividualForms.Any())
            {
                m_Forms = (from b in this.IndividualForms
                           select new Form()
                           {
                               BorrowerNumber = b.BorrowerNumber,
                               InnerText = b.ID
                           }).ToList();
            }
        }

        public List<FormInfo> IndividualForms { get; set; }

        public DocumentVendorResult<IDocumentGenerationResult> RequestDocuments(bool isPreviewOrder, AbstractUserPrincipal principal)
        {
            // Get current user's email address
            string currentUserEmail = "";
            if (principal != null)
            {
                var emp = EmployeeDB.RetrieveById(m_brokerId, principal.EmployeeId);
                currentUserEmail = emp.Email;
            }

            this.PopulateForms();

            var process = DocMagicMismoRequest.CreateProcessOptions(
                m_packageType,
                m_Forms,
                m_sendEdisclosures,
                m_sendEDisclosuresAllowOnlineSigning,
                m_EmailDocuments,
                m_EmailToAddress,
                IsPasswordRequired,
                m_sHint, //password hint
                m_EmailPassword,
                m_EmailNotifyOnRetrieval,
                m_DsiFeatureEnabled,
                m_DsiAttention,
                m_DsiName,
                m_DsiStreet,
                m_DsiCity,
                m_DsiState,
                m_DsiZip,
                m_DsiPhone,
                m_DsiNotes,
                IsMersRegistrationEnabled,
                currentUserEmail, m_format);

            if (m_sendEdisclosures)
            {
                var emailPerson = new EmailPerson();
                emailPerson.EmailAddressList.Add(ConstStage.ESignEmailAddress);

                if (emailPerson.EmailAddressList.Count > 0 )
                {
                    process.EPortal = new EPortal();
                    process.EPortal.ClickSign = m_sendEDisclosuresAllowOnlineSigning ?
                        XmlSerializableCommon.E_YNIndicator.Y :
                        XmlSerializableCommon.E_YNIndicator.N;

                    var eventNotif = new EventNotification();
                    eventNotif.EmailPersonList.Add(emailPerson);
                    
                    process.EPortal.EventNotificationList.Add(eventNotif);                   
                }
            }
            
            DsiDocumentServerRequest request = DocMagicMismoRequest.CreateProcessRequest(
                  m_CustomerId,
                  m_Userid,
                  m_Password,
                  m_LoanId,
                  m_WebsheetNumber,
                  process);
           
            
            var response = DocMagicServer2.Submit(request);

            IDocumentGenerationResult result = null;
            if (response.Status == DocMagic.DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
            {
                // Fatal error encountered
                // Audit failed?
                // Process failed?
                HashSet<string> errors = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
             
                foreach (var msg in response.MessageList)
                {
                    if (msg.Type == DocMagic.DsiDocResponse.E_MessageType.Fatal)
                    {
                        errors.Add(msg.InnerText);
                    }
                }
                FatalErrors = errors; 
                var errorString = string.Join(Environment.NewLine, errors.ToArray());
                return DocumentVendorResult.Error(errorString, result);
            }

            if (m_sendEdisclosures)
            {
                TrackESignRequest();
            }

            if (response.WebsheetResponseList.Count == 0 )
            {
                Tools.LogErrorWithCriticalTracking("DocMagic did not return any websheet responses");
            }

            if (response.WebsheetResponseList.Count > 1)
            {
                Tools.LogErrorWithCriticalTracking("DocMagic returned " + response.WebsheetResponseList.Count + " ignoring all but first.");
            }

            result = new DocMagicDocumentGenerationResult(response, m_packageType, m_LoanId, m_appId, m_brokerId, m_format);

            var documentSuccessResult = DocumentVendorResult.Success(result);
            documentSuccessResult.TransactionId = Guid.NewGuid().ToString();
            return documentSuccessResult;
        }


        private void TrackESignRequest()
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocMagicAccountNumber", m_CustomerId),
                                            new SqlParameter("@sLId", m_LoanId),
                                            new SqlParameter("@sLNm", m_sLNm),
                                            new SqlParameter("@EmployeeId", PrincipalFactory.CurrentPrincipal.EmployeeId),
                                            new SqlParameter("@BrokerId", m_brokerId),
                                            new SqlParameter("@Username", m_Userid),
                                            new SqlParameter("@Password", EncryptionHelper.Encrypt(m_Password))
                                        };

            StoredProcedureHelper.ExecuteNonQuery(m_brokerId, "DocMagic_TrackESignRequest", 3, parameters);
        }

        public static IEnumerable<ESignRequestDetails> GetEsignRequestDetails(string accountNumber, string sLNm, string Username)
        {
            List<ESignRequestDetails> details = new List<ESignRequestDetails>(1);

            StringBuilder sb = new StringBuilder();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                // 4/21/2015 dd - I am unable to determine which database the record maybe in, therefore loop through all available database.
                SqlParameter[] parameters = {
                                            new SqlParameter("@sLNm", sLNm),
                                            new SqlParameter("@DocMagicAccountNumber", accountNumber),
                                            new SqlParameter("@Username", Username.ToLower())
                                        };

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, "DocMagic_FindESignRequest", parameters))
                {
                    while (reader.Read())
                    {
                        details.Add(new ESignRequestDetails(reader));
                    }
                }
            }
            return details;
        }

        public static void RemoveESignRequest(Guid brokerId, int id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@Id", id)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "DocMagic_DeleteESignRequest", 1, parameters);
        }
    }

    public sealed class ESignRequestDetails
    {
        public string DocMagicAccountNumber { get; private set; }
        public Guid sLId { get; private set; }
        public string sLNm { get; private set; }
        public Guid EmployeeId { get; private set; }
        public Guid BrokerId { get; private set; }
        public int Id { get; private set; }
        public string Username { get; private set; }
        private string m_encyptedPassword;
        public string Password {
            get { return EncryptionHelper.Decrypt(m_encyptedPassword); ;} 
            private set { 
                m_encyptedPassword = EncryptionHelper.Encrypt(value);
            } 
        }
        public DateTime RequestedD { get; private set; }

        public ESignRequestDetails(DbDataReader reader)
        {
            DocMagicAccountNumber = (string)reader["DocMagicAccountNumber"];
            sLId = (Guid)reader["sLId"];
            sLNm = (string)reader["sLNm"];
            BrokerId = (Guid)reader["BrokerId"];
            EmployeeId = (Guid)reader["EmployeeId"];
            Id = (int)reader["Id"];
            Username = (string)reader["Username"];
            m_encyptedPassword = (string)reader["Password"];
            RequestedD = (DateTime)reader["RequestedD"];
        }
    }


}
