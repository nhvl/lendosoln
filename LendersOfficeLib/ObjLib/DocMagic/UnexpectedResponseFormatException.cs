﻿//-----------------------------------------------------------------------
// <copyright file="UnexpectedResponseFormatException.cs" company="Meridianlink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <author>Scott Kibler</author>
// <summary>Used when we have an unexpected response from a third party most likely DM.</summary>
//-----------------------------------------------------------------------
namespace LendersOffice.ObjLib.DocMagicLib
{
    using System;
    using DataAccess;

    /// <summary>
    /// Used when we have an unexpected response from a third party most likely DM.
    /// </summary>
    public class UnexpectedResponseFormatException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnexpectedResponseFormatException"/> class.
        /// </summary>
        /// <param name="userMessage">Message to display to the user.</param>
        /// <param name="innerException">The exception that occurred when processing the response that shows it didn't have an expected format.</param>
        public UnexpectedResponseFormatException(string userMessage, Exception innerException) : base(userMessage, innerException)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnexpectedResponseFormatException"/> class.
        /// </summary>
        /// <param name="userMessage">Message to display to the user.</param>
        /// <param name="developerMessage">Message to display to the developer.</param>
        public UnexpectedResponseFormatException(string userMessage, string developerMessage) : base(userMessage, developerMessage)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UnexpectedResponseFormatException"/> class.
        /// </summary>
        /// <param name="userMessage">Message to display to the user.</param>
        public UnexpectedResponseFormatException(string userMessage) : base(userMessage, userMessage)
        {
        }

        /// <summary>
        /// Gets or sets the value of IsEmailDeveloper, that is - false.
        /// </summary>
        /// <value>False. Not true.</value>
        public override bool IsEmailDeveloper
        {
            get
            {
                return false;
            }

            set
            {
            }
        }
    }
}
