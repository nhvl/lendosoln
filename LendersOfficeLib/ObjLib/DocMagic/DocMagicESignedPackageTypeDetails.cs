﻿using System;

namespace LendersOffice.ObjLib.DocMagicLib
{
    public class DocMagicESignedPackageTypeDetails
    {
        public string PackageType;
        public string VersionID;
        public Guid EDocsID;
        public string PackageID;
    }
}
