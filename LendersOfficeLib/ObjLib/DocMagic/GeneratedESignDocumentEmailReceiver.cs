﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using CommonProjectLib.Email;
using DataAccess;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Security;
using LendingQBPop3Mail;

namespace LendersOffice.ObjLib.DocMagicLib
{
    public class GeneratedESignDocumentEmailReceiver : CommonProjectLib.Runnable.IRunnable
    {


        public string Description
        {
            get { return "Handles esign notifcations from DocMagic."; }
        }


        private string m_sEmailAddress;
        private string m_sEmailPassword;
        private string m_sEmailServer;
        private int m_iEmailServerPort;
        private bool m_EmailServerIsSsl;

        private Regex[] m_accountNumberRegex = new Regex[] {
            new Regex("<b>Account: ([^<]+)</b>"),
            new Regex(@"<span\s+id=""accountNumber"">([^<]+)</span>")
        };

        private Regex[] m_sLNmRegex = new Regex[] {
            new Regex("<b>Loan Number: ([^<]+)</b>"),
            new Regex(@"<span\s+id=""loanNumber"">([^<]+)</span>")

        };
        private Regex[] m_webSheetRegex = new Regex[] {
            new Regex("<b>Worksheet: ([^<]+)</b>"),
            new Regex(@"<span\s+id=""worksheetId"">([^<]+)</span>")

        };
        private Regex[] m_packageTypeRegex = new Regex[] {
            new Regex("<b>Package Type: ([^<]+)</b>"),
            new Regex(@"<span\s+id=""packageType"">([^<]+)</span>")
        };
        private Regex[] m_recipientRegex = new Regex[] {
            new Regex("<b>Recipient: ([^<]+)</b>"),
            new Regex(@"<span\s+id=""recipient"">([^<]+)</span>")
        };

        private Regex[] m_eSignSubjectRegex = new Regex[] { new Regex("Signature\\s+Request\\s+Completed\\s+[[]Loan\\s+Number:\\s+([^]]+)[]]"), new Regex("Electronic Document Delivery [[]Loan Number: ([^]]+)[]]") };
        private Regex[] m_eConsentReceivedSubjectRegex = new Regex[] { new Regex("Electronic Consent Obtained for ([^[]+) [[]Loan Number: ([^]]+)[]]") };
        private Regex[] m_eConsentDeclinedSubjectRegex = new Regex[] { new Regex("Consent Withdrawn - ([^[]+) [[]Loan Number: ([^]]+)[]]") };
        private Regex[] m_documentReviewCompletedSubjectRegex = new Regex[] { new Regex("Document Review Request Completed [[]Loan Number: ([^]]+)[]]"), new Regex("Initial Disclosures Provided [[]Loan Number: ([^]]+)[]]")  };

        private void Construct()
        {
            m_sEmailAddress = ConstStage.ESignEmailAddress;
            m_sEmailPassword = EncryptionHelper.Decrypt(ConstStage.ESignEncryptedEmailPassword);
            m_sEmailServer = ConstStage.EmailReceivingServer;
            m_iEmailServerPort = ConstStage.EmailReceivingServerPort;
            m_EmailServerIsSsl = ConstStage.EmailReceivingServerIsSsl;
            Thread.CurrentPrincipal = SystemUserPrincipal.TaskSystemUser;

            if (string.IsNullOrEmpty(m_sEmailServer) ||
                string.IsNullOrEmpty(m_sEmailPassword) ||
                string.IsNullOrEmpty(m_sEmailAddress))
            {
                string userMsg = "emailaddress, password and server cannot be empty";
                string devMsg = userMsg;
                throw new CBaseException(userMsg, devMsg);
            }
        }


        /// <summary>
        /// This is to store messages we've already attempted to process, so we want to suppress their messages and logging.
        /// We will still attempt to process them.
        /// </summary>
        private static HashSet<string> s_SilencedUniqueIDs = new HashSet<string>();

        #region IRunnable Members

        public void Run()
        {
            Construct();

            Tools.LogInfo("ESign Email Processor Started");

            Tools.SetupServicePointCallback();
            Tools.SetupServicePointManager();

            try
            {
                using (JMailEmailProcessor processor = new JMailEmailProcessor(
                m_sEmailAddress,
                m_sEmailPassword,
                m_sEmailServer,
                m_iEmailServerPort,
                m_EmailServerIsSsl
                ))
                {
                    int count = 0;

                    foreach (var message in processor.GetEmailMessages())
                    {
                        Trace.CorrelationManager.ActivityId = Guid.NewGuid();
                        string currentUniqueId = string.Empty;
                        bool deleteMessage = false;
                        if (message != null)
                        {
                            Tools.LogInfo("Esign", "Process email from=[" + message.From + "], to=[" + message.RecipientsString + "], subject=[" + message.Subject + "].");
                        }

                        try
                        {
                            currentUniqueId = processor.GetMessageUniqueId(count);

                            deleteMessage = (false == HandleMessage(message));

                            if (s_SilencedUniqueIDs.Contains(currentUniqueId) && deleteMessage)
                            {
                                s_SilencedUniqueIDs.Remove(currentUniqueId);
                            }
                        }
                        catch (Exception e)
                        {
                            if (s_SilencedUniqueIDs.Contains(currentUniqueId))
                            {
                                continue;
                            }

                            s_SilencedUniqueIDs.Add(currentUniqueId);
                            LogError(e, message);
                        }
                        finally
                        {
                            if (deleteMessage)
                            {
                                processor.DeleteMessage(count);
                            }
                        }
                        count++;
                    }

                    Tools.LogInfo($"ESign Email Processor Stopped. {count} emails processed.");
                }
            }
            catch (Exception exc)
            {
                Tools.LogError("[ESign] [Exception] Unknown issue when processing esign email.", exc);
                throw;
            }
        }

        /// <summary>
        /// Log error.
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="e"></param>
        /// <param name="message"></param>
        private void LogError(Exception e, Pop3Message message)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("[ESign] Issue processing esign email.");
            sb.AppendLine();
            if (message != null)
            {
                sb.AppendLine($"Email Subject: {message.Subject}");
                sb.AppendLine($"Email Data: {message.MailData}");
            }

            Tools.LogError(sb.ToString(), e);
        }

        private Match GetSuccessMathOrFailure(Regex[] regexes, string body)
        {
            if (regexes == null || regexes.Length == 0)
            {
                throw new ArgumentException("regexes is null or empty.");
            }
            Match m = null;

            foreach (Regex r in regexes)
            {
                m = r.Match(body);
                if (m.Success)
                {
                    break;
                }
            }

            return m;
        }

        private DocMagicEmailDetails ParseMessage(Pop3Message message, bool requireReceivedFor, bool isDocumentReviewCompleted)
        {
            
            Match accountNumberMatch = GetSuccessMathOrFailure(m_accountNumberRegex, message.HTMLBody);
            if (false == accountNumberMatch.Success || accountNumberMatch.Groups.Count < 2)
            {
                Tools.LogError("Could not parse docmagic email. No Account Number Found " + message.HTMLBody);
                return null;
            }
            string accountNumber = accountNumberMatch.Groups[1].Value.TrimWhitespaceAndBOM();

            Match sLNmMatch = GetSuccessMathOrFailure(m_sLNmRegex, message.HTMLBody);
            if (false == sLNmMatch.Success || sLNmMatch.Groups.Count < 2)
            {
                Tools.LogError("Could not parse docmagic email. No loan number found " + message.HTMLBody);
                return null;
            }
            string sLNm = sLNmMatch.Groups[1].Value.TrimWhitespaceAndBOM();

            Match wsMatch = GetSuccessMathOrFailure(m_webSheetRegex, message.HTMLBody);
            string worksheetNumber = string.Empty;
            if (false == wsMatch.Success || wsMatch.Groups.Count < 2)
            {
                //IR - OPM 207036: Worksheet number not necessary for Document Review. Allow processing to proceed regardless.
                if (!isDocumentReviewCompleted)
                {
                    Tools.LogError("Could not parse docmagic email. No worksheet number found " + message.HTMLBody);
                    return null;
                }
            }
            else
            {
                worksheetNumber = wsMatch.Groups[1].Value.TrimWhitespaceAndBOM();
            }

            Match ptMatch = GetSuccessMathOrFailure(m_packageTypeRegex, message.HTMLBody);
            if (false == ptMatch.Success || ptMatch.Groups.Count < 2)
            {
                Tools.LogError("Could not parse docmagic email. No package type found " + message.HTMLBody);
                return null;
            }
            string packageType = ptMatch.Groups[1].Value.TrimWhitespaceAndBOM();

            if (requireReceivedFor)
            {
                Match borrowerNameMatch = GetSuccessMathOrFailure(m_eConsentReceivedSubjectRegex, message.Subject);
                if (!borrowerNameMatch.Success)
                {
                    borrowerNameMatch = GetSuccessMathOrFailure(m_eConsentDeclinedSubjectRegex, message.Subject);
                }

                if (!borrowerNameMatch.Success || borrowerNameMatch.Groups.Count < 3)
                {
                    Tools.LogError("Could not parse docmagic email. No borrower name found " + message.Subject);
                    return null;
                }

                string receivedFor = borrowerNameMatch.Groups[1].Value;

                return new DocMagicEmailDetails(accountNumber, sLNm, worksheetNumber, packageType, receivedFor);
            }
            else
            {
                return new DocMagicEmailDetails(accountNumber, sLNm, worksheetNumber, packageType, String.Empty);
            }
        }

        /// <summary>
        /// returns true if need to handle the message again (i.e. don't delete if true)
        /// </summary>
        private bool HandleMessage(Pop3Message message) // returns false if should delete the message.
        {
            if (string.IsNullOrEmpty(message.Subject))
            {
                return false;
            }

            Match subjectMatch = GetSuccessMathOrFailure(m_eSignSubjectRegex, message.Subject);
            bool isESign = subjectMatch.Success; // true
            subjectMatch = GetSuccessMathOrFailure(m_eConsentReceivedSubjectRegex,message.Subject);
            bool isEConsentReceived = subjectMatch.Success; // false
            subjectMatch = GetSuccessMathOrFailure(m_eConsentDeclinedSubjectRegex,message.Subject);
            bool isEConsentDeclined = subjectMatch.Success; // false
            subjectMatch = GetSuccessMathOrFailure(m_documentReviewCompletedSubjectRegex,message.Subject);
            bool isDocumentReviewCompleted = subjectMatch.Success; // false

            if (!isESign && !isEConsentReceived && !isEConsentDeclined && !isDocumentReviewCompleted)
            {
                Tools.LogInfo("Esign", $"ESign message does not match any subject regex. Subject: {message.Subject}");
                return false;
            }

            DocMagicEmailDetails emailDetails = null;

            if (isESign || isDocumentReviewCompleted)
            {
                emailDetails = ParseMessage(message, false, isDocumentReviewCompleted);
            }
            else if (isEConsentReceived || isEConsentDeclined)
            {
                emailDetails = ParseMessage(message, true, false /*Not isDocumentReviewCompleted */);
            }

            if (emailDetails == null)
            {
                return true;
            }

            string username = "";
            foreach (var recipient in message.Recipients)
            {
                if (recipient.ReType == Pop3RecipientType.To)
                {
                    username = recipient.Email;
                    break;
                }
            }

            // if there is a bad DM setup, one email to support is enough, so we will delete the email, as we can't do anything with it anyway.  opm 136600
            if (string.Compare(username, m_sEmailAddress, true) == 0)
            {
                // Creating log for the email message for [ESign][BadDMSetup] without actually sending the email. opm 454882 
                string s = $"[ESign][BadDMSetup] Bad DM account setup Subject: {message.Subject} Account: {emailDetails.AccountNumber} LoanNumber: {emailDetails.SLNm} Username: {username}";

                Tools.LogWarning(s);
                return false;
            }

            var entries = DocMagicDocumentGenerationRequestHandler.GetEsignRequestDetails(
                emailDetails.AccountNumber, emailDetails.SLNm, username);

            ESignRequestDetails dbDetails = null;
            if (entries.Count() == 0)
            {
                // 8/11/2014 AV - 181861 [ESign][NoRequest] Did not find esign request - Just ignore.
                Tools.LogInfo("Esign", $"ESign request details not found. Subject: {message.Subject} Acct: {emailDetails.AccountNumber} sLNm: {emailDetails.SLNm} Username: {username}");
                return false;
            }
            else if (entries.Count() > 1)
            {
                IEnumerable<DateTime> requestDs = entries.Select(d => d.RequestedD);
                DateTime mostRecent = requestDs.Max();
                // This should really never return multiple entries for the same date,
                // but if it does just pick the first entry for now.
                IEnumerable<ESignRequestDetails> mostRecentRequest = entries.Where(d => d.RequestedD == mostRecent);
                if (mostRecentRequest.Count() > 1)
                {
                    Tools.LogWarning(
                        "Using first of multiple esign request matches for same date. " +
                        string.Format("Account: {0} LoanNumber: {1} Username: {2}",
                        emailDetails.AccountNumber, emailDetails.SLNm, username)
                        + Environment.NewLine
                        + "Message:" + Environment.NewLine + message.HTMLBody);
                }
                dbDetails = mostRecentRequest.First();

                // For now, do not remove the old request records. GF OPM 105770
            }
            else
            {
                dbDetails = entries.First();
            }

            if (BrokerDB.RetrieveById(dbDetails.BrokerId).BillingVersion != E_BrokerBillingVersion.PerTransaction)
            {
                Tools.LogInfo("Esign", $"Billing version is not PerTransaction");
                return false; // opm 180887.  If they once had this feature, but no longer have it, don't process the email.
            }

            if (isESign)
            {
                return HandleESignMessage(message, emailDetails, username, dbDetails);
            }
            else if (isEConsentReceived)
            {
                return HandleEDisclosureMessage(message, emailDetails, username,
                    dbDetails, E_EDisclosureDisclosureEventT.DM_EConsentReceived);
            }
            else if (isEConsentDeclined)
            {
                return HandleEDisclosureMessage(message, emailDetails, username,
                    dbDetails, E_EDisclosureDisclosureEventT.DM_EConsentDeclined);
            }
            else if (isDocumentReviewCompleted)
            {
                return HandleEDisclosureMessage(message, emailDetails, username,
                    dbDetails, E_EDisclosureDisclosureEventT.DM_DocumentReviewCompleted);
            }

            return false;
        }

        /// <summary>
        /// Trigger appropriate disclosure event for the loan. Assumes there is at
        /// least one entry in entries.
        /// </summary>
        /// <param name="message">The email message.</param>
        /// <param name="emailDetails">The message details.</param>
        /// <param name="username">The DocMagic username that generated the request.</param>
        /// <param name="details">The record for this e-disclosure request that was recorded in the DB.</param>
        /// <param name="disclosureT">The type of disclosure event that needs to be fired.</param>
        /// <returns>False if the message should be deleted. Otherwise, true.</returns>
        private bool HandleEDisclosureMessage(Pop3Message message, DocMagicEmailDetails emailDetails,
            string username, ESignRequestDetails details, E_EDisclosureDisclosureEventT disclosureT)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(details.sLId,
                typeof(GeneratedESignDocumentEmailReceiver));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.ProcessEDisclosureDisclosureTrigger(disclosureT, emailDetails, message.Date);
            dataLoan.Save();

            return false;
        }

        private bool HandleESignMessage(Pop3Message message, DocMagicEmailDetails emailDetails, 
            string username, ESignRequestDetails dbDetails)
        {
            DocMagicESignRequestMaker.GetAndUploadPDF(dbDetails, emailDetails.WorksheetNumber, emailDetails.PackageType);
            
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(dbDetails.sLId,
                typeof(GeneratedESignDocumentEmailReceiver));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.ProcessEDisclosureDisclosureTrigger(E_EDisclosureDisclosureEventT.DM_ESignCompleted,
                emailDetails, message.Date);
            dataLoan.Save();

            return false;
        }

        #endregion
    }

    public class DocMagicEmailDetails
    {
        public readonly string AccountNumber;
        public readonly string SLNm;
        public readonly string WorksheetNumber;
        public readonly string PackageType;
        public readonly string ReceivedFor;
        public DocMagicEmailDetails(string accountNumber, string sLNm, string worksheetNumber, string packageType, string receivedFor)
        {
            AccountNumber = accountNumber;
            SLNm = sLNm;
            WorksheetNumber = worksheetNumber;
            PackageType = packageType;
            ReceivedFor = receivedFor;
        }
    }
}
