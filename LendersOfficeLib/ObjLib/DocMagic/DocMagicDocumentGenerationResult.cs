﻿namespace LendersOffice.ObjLib.DocMagicLib
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using DataAccess;
    using DocMagic.DsiDocRequest;
    using EDocs;
    using Integration.DocumentCapture;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Common.SerializationTypes;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Edocs.DocMagic;
    using LendersOffice.Integration.DocumentVendor;
    using LendersOffice.ObjLib.Billing;
    using LendersOffice.Security;

    public sealed class DocMagicDocumentGenerationResult : IDocumentGenerationResult
    {
        private Guid m_LoanId { get; set; }
        private Guid m_appId { get; set; }
        private Guid m_brokerId { get; set; }

        private string HtmlResult_Section32Calculation { get; set; }
        private string HtmlResult_ImpoundAnalysis { get; set; }
        private string HtmlResult_GfeComparison { get; set; }
        private string HtmlResult_APR { get; set; }

        private int m_DocTypeId_APRPAYMENTSCHEDULE { get; set; }
        private int m_DocTypeId_IMPOUNDANALYIS { get; set; }
        private int m_DocTypeId_SECTION_32 { get; set; }
        private int m_DocTypeId_GFE_COMPARISON { get; set; }
        private int m_DocTypeId_ESIGNEDDOCUMENTS { get; set; }

        public DocMagicDocumentGenerationResult(DocMagic.DsiDocResponse.DsiDocumentServerResponse response, E_ProcessPackageType packageType, Guid loanid, Guid appid, Guid brokerid, E_DocumentFormatType format)
        {
            m_format = format;
            m_LoanId = loanid;
            m_appId = appid;
            m_brokerId = brokerid;

            m_packageType = packageType;
            var docTypes = EDocumentDocType.GetDocTypesByBroker(m_brokerId, E_EnforceFolderPermissions.True);

            foreach (DocType dt in docTypes)
            {
                switch (dt.DocTypeName.ToUpper())
                {
                    case "APRPAYMENTSCHEDULE":
                        m_DocTypeId_APRPAYMENTSCHEDULE = int.Parse(dt.DocTypeId);
                        break;
                    case "IMPOUNDANALYIS":
                        m_DocTypeId_IMPOUNDANALYIS = int.Parse(dt.DocTypeId);
                        break;
                    case "SECTION 32":
                        m_DocTypeId_SECTION_32 = int.Parse(dt.DocTypeId);
                        break;
                    case "GFE COMPARISON":
                        m_DocTypeId_GFE_COMPARISON = int.Parse(dt.DocTypeId);
                        break;
                    case "ESIGNEDDOCUMENTS":
                        m_DocTypeId_ESIGNEDDOCUMENTS = int.Parse(dt.DocTypeId);
                        break;
                    default:
                        break;
                }
            }

            this.DocCode = response.AppletResponse.WebsheetInfo.DocCode;
            HandleResponse(response);
        }

        // Opm 242144 gf - This value only applies to the LQBDocumentFramework.
        public decimal? DocVendorApr => null;

        public string DocCode { get; }

        public decimal? DocVendorLateChargePercent => null;

        public VendorProvidedDisclosureMetadata VendorProvidedDisclosureMetadata => null;

        private E_ProcessPackageType m_packageType;
        public string PackageType
        {
            get
            {
                return Enum.GetName(typeof(E_ProcessPackageType), m_packageType);
            }
            set
            {
                m_packageType = (E_ProcessPackageType)Enum.Parse(typeof(E_ProcessPackageType), value);
            }
        }

        /// <remarks>
        /// This version of the document framework is deprecated, so we aren't worrying about making this nicer for now.
        /// </remarks>
        public LqbGrammar.DataTypes.DocumentIntegrationPackageName PackageName
        {
            get { return LqbGrammar.DataTypes.DocumentIntegrationPackageName.Create(this.PackageType).ForceValue(); }
        }


        private void HandleResponse(DocMagic.DsiDocResponse.DsiDocumentServerResponse response)
        {
            BrokerDB db = BrokerDB.RetrieveById(m_brokerId);
            var websheetResponse = response.WebsheetResponseList[0];

            if (db.DocMagicDefaultDocTypeID.HasValue == false || m_DocTypeId_APRPAYMENTSCHEDULE == -1 || m_DocTypeId_IMPOUNDANALYIS == -1 || m_DocTypeId_SECTION_32 == -1 || m_DocTypeId_GFE_COMPARISON == -1)
            {
                throw CBaseException.GenericException("PTM NOT setup correctly for client!!! need to add more doctypes.");
            }

            HandleAudit(websheetResponse.Audit);

            var docSet = websheetResponse.Process.DocSet;

            if (m_format == E_DocumentFormatType.PDF)
            {
                int defaultDoctype = db.DocMagicDefaultDocTypeID.Value;

                List<string> pdfKeys = new List<string>();
                foreach (var item in docSet.DocumentList)
                {
                    string pdfPath = Utilities.Base64StringToFile(item.EmbeddedContent.InnerText, "docmagic.pdf");

                    string key = Guid.NewGuid().ToString();
                    FileDBTools.WriteFile(E_FileDB.Temp, key, pdfPath);
                    pdfKeys.Add(key);
                    if (db.AutoSaveDocMagicGeneratedDocs)
                    {
                        var useCaptureMethod = db.EnableKtaIntegration
                            && db.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.DocumentCapture;
                        var useCaptureFallbackMethod = false;

                        if (useCaptureMethod)
                        {
                            if (OcrEnabledBranchGroupManager.IsLoanBranchGroupEnabledForOcr(db, m_LoanId))
                            {
                                var parameters = KtaDocumentUploadParameters.FromUserPrincipal(PrincipalFactory.CurrentPrincipal);
                                parameters.LoanId = m_LoanId;
                                parameters.ApplicationId = m_appId;
                                parameters.DocumentSource = E_EDocumentSource.GeneratedDocs;
                                parameters.PdfPath = pdfPath;

                                var result = KtaUtilities.UploadDocument(parameters);
                                if (result.Success)
                                {
                                    continue;
                                }

                                var messageData = $"for Doc Magic HandleResponse loan {m_LoanId}, app {m_appId} ";
                                if (result.MissingCredentials)
                                {
                                    // Allow falling back to existing upload behavior only when service credentials are missing.
                                    useCaptureFallbackMethod = true;
                                    Tools.LogError("Missing service credentials " + messageData);
                                }
                                else
                                {
                                    throw new CBaseException(ErrorMessages.Generic, "Error encountered " + messageData + result.ErrorMessage);
                                }
                            }
                            else
                            {
                                useCaptureFallbackMethod = true;
                            }
                        }

                        bool canUseSplitDocsForThisSigningType = db.DocMagicSplitUnsignedDocs;
                        bool useSplitDocsAsPrimaryMethod = db.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.Split;
                        bool useSplitDocsAsFallbackMethod = useCaptureFallbackMethod && db.BackupDocumentSavingOption == E_DocMagicDocumentSavingOption.Split;

                        var useSplitDocsMethod = canUseSplitDocsForThisSigningType && (useSplitDocsAsPrimaryMethod || useSplitDocsAsFallbackMethod);

                        if (useSplitDocsMethod)
                        {
                            DocMagicPDFManager.UploadDocument(PrincipalFactory.CurrentPrincipal, m_LoanId, m_appId, "", "Autosaved: " + DateTime.Now.ToShortTimeString(), pdfPath);
                        }
                        else
                        {
                            // At this point we haven't saved the documents using Capture or Split Docs. Save as Single Document
                            // regardless of the save method settings to ensure the documents are not lost.
                            CreateEdoc(pdfPath, defaultDoctype);
                        }
                    }
                }

                PDFLocationKey = AutoExpiredTextCache.AddToCache(ObsoleteSerializationHelper.JsonSerialize(pdfKeys), TimeSpan.FromHours(1));
            }
            else
            {
                if (docSet.DocumentList.Count > 1)
                {
                    Tools.LogErrorWithCriticalTracking("We got more than one document from doc magic returning first one only.");
                }
   
                var item = docSet.DocumentList[0];
                string path = Utilities.Base64StringToFile(item.EmbeddedContent.InnerText, "docmagic.docmaster");
                string cache = AutoExpiredTextCache.AddToCache(BinaryFileHelper.ReadAllBytes(path), TimeSpan.FromHours(1));
                
                var anonObj = new Dictionary<string, string>();
                anonObj.Add("userid", PrincipalFactory.CurrentPrincipal.UserId.ToString());
                anonObj.Add("datapath", cache);
                PDFLocationKey = AutoExpiredTextCache.AddToCache(ObsoleteSerializationHelper.JsonSerialize(anonObj), TimeSpan.FromHours(1));
 
            }

            if (m_packageType == E_ProcessPackageType.Closing)
            {
                #region av opm 88157   Lock Document Generation after Closing Documents are Drawn 7/19/2012
                CPageData data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(m_LoanId, typeof(DocMagicDocumentGenerationResult));
                data.InitSave(ConstAppDavid.SkipVersionCheck);
                data.sDocMagicHaveClosingDocsBeenGenerated = true;
                data.Save();
                #endregion

                if (data.sLoanFileT == E_sLoanFileT.Sandbox || data.sLoanFileT == E_sLoanFileT.Test)
                {
                    return;
                }

                try
                {
                    PerTransactionBilling.BillLoan(m_LoanId, E_BillingReason.ClosingDocs, E_DocumentVendor.DocMagic);
                }
                catch (ArgumentException)
                {
                    Tools.LogBug("A broker without PTB or No Billing enabled requested closing docs, loan id is [" + m_LoanId + "]");
                }
            }
        }

        public string ErrorMessage { get { return string.Empty; } }

        public string ViewUrl
        {
            get
            {

                string url = "DocumentViewer.aspx?loanid=" + m_LoanId;
                if (false == string.IsNullOrEmpty(HtmlResult_APR))
                {
                    url += "&a=" + HtmlResult_APR;
                }
                if (false == string.IsNullOrEmpty(HtmlResult_Section32Calculation))
                {
                    url += "&s=" + HtmlResult_Section32Calculation;
                }
                if (false == string.IsNullOrEmpty(HtmlResult_GfeComparison))
                {
                    url += "&g=" + HtmlResult_GfeComparison;
                }
                if (false == string.IsNullOrEmpty(HtmlResult_ImpoundAnalysis))
                {
                    url += "&i=" + HtmlResult_ImpoundAnalysis;
                }
                url += "&d=" + PDFLocationKey;
                return url;
            }
        }

        public string DownloadUrl
        {
            get
            {
                return "DocMagicDocumentGenerationDBK.aspx?loanid=" + m_LoanId + "&dbkKey=" + PDFLocationKey;
            }
        }

        public bool DocsAreForDownload
        {
            get
            {
                return DocumentFormat == E_DocumentFormatType.DBK;
            }
        }

        public bool DocsAreForViewing
        {
            get
            {
                return DocumentFormat == E_DocumentFormatType.PDF;
            }
        }

        private E_DocumentFormatType m_format = E_DocumentFormatType.PDF;
        public E_DocumentFormatType DocumentFormat
        {
            get { return m_format; }
            set { m_format = value; }
        }

        public string PdfFileDbKey
        {
            get
            {
                var cacheResult = AutoExpiredTextCache.GetFromCache(this.PDFLocationKey);
                if (!string.IsNullOrWhiteSpace(cacheResult))
                {
                    var pdfFileDbKeys = ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<string>>(cacheResult);
                    if (pdfFileDbKeys.Count > 0)
                    {
                        // The first key is expected to be the requested document.
                        return pdfFileDbKeys[0];
                    }
                }

                return string.Empty;
            }
        }

        private string PDFLocationKey
        {
            get;
            set;
        }

        public bool IsDisclosurePackage
        {
            get
            {
                return m_packageType == E_ProcessPackageType.Predisclosure || m_packageType == E_ProcessPackageType.Redisclosure;
            }
        }

        public bool IsInitialDisclosurePackage
        {
            get
            {
                return m_packageType == E_ProcessPackageType.Predisclosure;
            }
        }

        public bool IsClosingPackage
        {
            get
            {
                return m_packageType == E_ProcessPackageType.Closing;
            }
        }

        public bool CreatedArchiveInUnknownStatus { get; set; }
        public ClosingCostArchive.E_ClosingCostArchiveType? UnknownArchiveType  { get; set; }
        public string UnknownArchiveDate { get; set; }
        public bool UnknownArchiveWasSourcedFromPendingCoC { get; set; }
        public bool HasBothCDAndLEArchiveInUnknownStatus { get; set; }

        #region Sanitizing the HTML we get from DocMagic
        //I know that cleaning HTML with regexes is the path to cthonic madness,
        //but the only parser we have is MSHTML and that's even worse.

        //We'll use this regex to add clear: both to the layers, otherwise some floating stuff messes up the pdf.
        private static readonly Regex ClearBoth = new Regex(@"\.layer\s*{", RegexOptions.Compiled);

        //One of the pages has layers with restricted height, which again messes up the pdf. Get rid of that.
        private static readonly Regex Height = new Regex(@"\.layer\s*{(.*)\s*height\s*:\s*\d+px;", RegexOptions.Compiled);

        //By removing both visbiliity: hidden and display: none, everything shows up
        private static readonly Regex DisplayAll = new Regex(@"(visibility\s*:\s*hidden|display\s*:\s*none)\s*;", RegexOptions.Compiled);

        //I'd never even seen this before, but it causes problems with the pdf.
        private static readonly Regex ClearPageBreaks = new Regex(@"page-break-before\s*:\s*always;", RegexOptions.Compiled);

        //For some reason they thought it was necessary to use position:absolute (it's not), and it messes things up.
        //I bet this was a bugfix for some ancient version of IE, actually.
        private static readonly Regex ClearPosition = new Regex(@"position\s*:\s*absolute\s*;", RegexOptions.Compiled);

        //We'll want to nuke the javascript in the pdf.
        private static readonly Regex RemoveScripts = new Regex(@"<script .*</script>", RegexOptions.Compiled | RegexOptions.Singleline);

        //A couple of the pages we get start with a table that controls what layers are visible; 
        //we want to get rid of that for the PDF.
        private static string RemoveControlTable(string html)
        {
            //The table starts right after the opening body tag, so find the body
            int bodyStart = html.IndexOf("<body");
            int bodyEnd = html.IndexOf(">", bodyStart);
            //Then find the end of the first table
            int firstTableEnd = html.IndexOf("</table>", bodyStart);
            //And then end of the second table (it's made up of two nested tables)
            int secondTableEnd = html.IndexOf("</table>", firstTableEnd);

            //And get rid of them.
            html = html.Remove(bodyEnd, (secondTableEnd - bodyEnd));
            return html;
        }

        //And of course we want to get rid of all their buttons
        private static string RemoveButtons(string html)
        {
            //The table that contains the buttons is pretty much impossible to find on its own,
            //but the buttons themselves are all <a> elements with class button. 
            string buttonFingerprint = @"<a class=""button""";
            bool hasButtons = html.IndexOf(buttonFingerprint) > 0;
            while (hasButtons)
            {
                int nextButton = html.IndexOf(buttonFingerprint);

                //Since we found a button, go back and nuke its table.
                //LastIndexOf actually does a backwards search (in this case, for the opening <table> tag)
                //which makes sense if you think about it but is pretty unintuitive.
                int tableStart = html.LastIndexOf("<table", nextButton);
                int tableEnd = html.IndexOf(@"</table>", nextButton);
                html = html.Remove(tableStart, tableEnd - tableStart);
                hasButtons = html.IndexOf(buttonFingerprint) > 0;
            }
            return html;
        }

        //The order of these guys matters (one of the html pages doesn't clear position), so check that when you change it.
        private static readonly Func<string, string>[] HtmlCleaners = new Func<string, string>[]
            {
                (html) => ClearPosition.Replace(html, ""),
                (html) => RemoveButtons(html),
                (html) => ClearBoth.Replace(html, ".layer { clear:both;"),
            };

        private static readonly Func<string, string>[] PdfCleaners = new Func<string, string>[]
            {
                (html) => RemoveButtons(html),
                (html) => ClearPosition.Replace(html, ""),
                (html) => DisplayAll.Replace(html, ""),
                (html) => RemoveScripts.Replace(html, ""),
                (html) => ClearBoth.Replace(html, ".layer { clear:both;"),
                (html) => Height.Replace(html, @".layer {$1"),
                //The pdf looks nicer like this
                (html) => ClearPageBreaks.Replace(html, "page-break-after: always;"),
            };

        private static readonly Func<string, string>[] Section32AndAPRExtras = new Func<string, string>[]
            {
                (html) => RemoveControlTable(html)
            };


        private static string Sanitize(string input, IEnumerable<Func<string, string>> Sanitizers)
        {
            foreach (var f in Sanitizers)
            {
                input = f(input);
            }
            return input;
        }
        #endregion

        private void HandleAudit(DocMagic.DsiDocResponse.Audit audit)
        {
            BrokerDB db = BrokerDB.RetrieveById(m_brokerId);
            if (audit == null ||
                m_format == E_DocumentFormatType.DBK)
            {
                return;
            }

            if (null != audit.AprPaymentCalculation && false == String.IsNullOrEmpty(audit.AprPaymentCalculation.AprPaymentCalculationHtml))
            {
                HtmlResult_APR = ProcessDocument(audit.AprPaymentCalculation.AprPaymentCalculationHtml, m_DocTypeId_APRPAYMENTSCHEDULE, db.AutoSaveDocMagicGeneratedDocs);
            }
            if (null != audit.Section32Calculation && false == String.IsNullOrEmpty(audit.Section32Calculation.Section32CalculationHtml))
            {
                HtmlResult_Section32Calculation = ProcessDocument(audit.Section32Calculation.Section32CalculationHtml, m_DocTypeId_SECTION_32, db.AutoSaveDocMagicGeneratedDocs);
            }
            if (null != audit.ImpoundAnalysis && false == String.IsNullOrEmpty(audit.ImpoundAnalysis.ImpoundAnalysisHtml))
            {
                HtmlResult_ImpoundAnalysis = ProcessDocument(audit.ImpoundAnalysis.ImpoundAnalysisHtml, m_DocTypeId_IMPOUNDANALYIS, db.AutoSaveDocMagicGeneratedDocs);
            }
            if (false == String.IsNullOrEmpty(audit.GfeComparisonHtml))
            {
                HtmlResult_GfeComparison = ProcessDocument(audit.GfeComparisonHtml, m_DocTypeId_GFE_COMPARISON, db.AutoSaveDocMagicGeneratedDocs);
            }
        }

        /// <summary>
        /// Sanitize, add to cache, create edoc
        /// </summary>
        /// <param name="html"></param>
        private string ProcessDocument(string html, int docTypeId, bool autoSaveDocMagicGeneratedDocs)
        {
            string rawHtml = html;

            //This one actually needs the position:absolute in order to look good as html,
            //So we'll skip that one (it's first)
            string dispHtml = Sanitize(rawHtml, HtmlCleaners);
            string resultCache = AutoExpiredTextCache.AddToCache(dispHtml, TimeSpan.FromHours(1));

            //We still need it for pdf though
            if (autoSaveDocMagicGeneratedDocs)
            {
                string pdfHtml = Sanitize(rawHtml, PdfCleaners);
                GenerateAndCreateEdoc(pdfHtml, docTypeId);
            }

            return resultCache;
        }

        private void GenerateAndCreateEdoc(string html, int docTypeId)
        {
            var path = GeneratePdf(html);
            CreateEdoc(path, docTypeId);
        }

        private void CreateEdoc(string pdfPath, int docTypeId)
        {
            BrokerDB db = BrokerDB.RetrieveById(m_brokerId);
            if (db.AutoSaveDocMagicGeneratedDocs)
            {
                var repository = EDocumentRepository.GetSystemRepository(m_brokerId);
                var edoc = repository.CreateDocument(E_EDocumentSource.GeneratedDocs);
                edoc.AppId = m_appId;
                edoc.PublicDescription = "Autosaved: " + DateTime.Now.ToShortTimeString();
                edoc.LoanId = m_LoanId;
                edoc.DocumentTypeId = docTypeId;
                edoc.IsUploadedByPmlUser = PrincipalFactory.CurrentPrincipal.Type == "P";
                edoc.UpdatePDFContentOnSave(pdfPath);
                edoc.Save(PrincipalFactory.CurrentPrincipal.UserId);
            }
        }

        private string GeneratePdf(string html)
        {
            byte[] pdf = CPageBase.ConvertToPdf(html);
            string tempFile = TempFileUtils.NewTempFilePath() + ".pdf";
            BinaryFileHelper.WriteAllBytes(tempFile, pdf);
            return tempFile;
        }
    }//*/
}
