﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Admin;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Constants;
using EDocs;
using System.IO;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Edocs.DocMagic;
using System.Xml;
using System.Net;
using System.Text.RegularExpressions;
using LendersOffice.Common;
using LendersOffice.Integration.DocumentCapture;
using System.Web.Script.Serialization;

namespace LendersOffice.ObjLib.DocMagicLib
{
    public static class DocMagicESignRequestMaker
    {
        private static readonly Dictionary<string, string> x_expectedXMLPackageTypeByEmailPackageType = new Dictionary<string, string>()
                {
                    {"Underwriting", "Underwriting"}, // (email, xml) pairs
                    {"EDisclosure", "Predisclosure"}, // Initial disclosure
                    {"Pre-Closing", "PreClosing"},
                    {"Redisclosure", "Redisclosure"},
                    {"Adverse Action/Denial", "Adverse"},
                    {"Processing", "Processing"},
                    {"Prequalification", "Prequalification"},
                    // NOTE THIS IS MISSING [POINT OF SALE]/[LOAN APPLICATION] // Unknown.
                };
        private static readonly object BadPackageCountLockObj = new object();
        private static int BadPackageCountThreadDangerous = 0;
        private static int BadPackageCount
        {
            get { lock (BadPackageCountLockObj) { return BadPackageCountThreadDangerous; } }
            set { lock (BadPackageCountLockObj) { BadPackageCountThreadDangerous = value; } }
        }        

        public static void GetAndUploadPDF(ESignRequestDetails details, string worksheetNumber, string packageType)
        {
            var employee = EmployeeDB.RetrieveById(details.BrokerId, details.EmployeeId);
            var auth = new DocMagicSavedAuthenticationForPTM(employee.UserID, employee.BranchID, employee.BrokerID);
            // if the account details from the time of request don't match the employee's current account details, use the request details.
            if (string.Compare(details.Username, auth.UserName, true) != 0)
            {
                auth.UserName = details.Username;
                auth.Password = details.Password; // SK - DON'T SAVE AUTH  after this.:-)
            }

            string DOCMAGIC_BETA_SERVER_URL = "https://stage-www.docmagic.com";
            string DOCMAGIC_PRODUCTION_SERVER_URL = "https://www.docmagic.com";
            string baseURL = DOCMAGIC_PRODUCTION_SERVER_URL;
            if (auth.CustomerId == "231856")
            {
                if (string.Compare(auth.UserName, "LQBDocMagicProduction@gmail.com", true) != 0)
                {
                    baseURL = DOCMAGIC_BETA_SERVER_URL;
                }
            }

            // request the token
            string tokenURL = baseURL + "/webservices/esign/tokens";
            string tokenRequest = string.Format( // NOTE THE xmlns is key, and it's not spec'd.
@"<DocMagicESignRequest originatorType=""ThirdPartyServer"" xmlns=""http://www.docmagic.com/2011/schemas"">
 <Authentication>
  <AccountIdentifier>{0}</AccountIdentifier>
  <AccountUserPassword>{1}</AccountUserPassword>
  <AccountUserName>{2}</AccountUserName>
 </Authentication>
</DocMagicESignRequest>",
                auth.CustomerId, auth.Password, auth.UserName);

            string tokenResponse = SubmitRequest(tokenURL, "POST", null, tokenRequest);

            // use token to get packages
            string tokenValue;

            try
            {
                tokenValue = GetTokenValue(tokenResponse);
            }
            catch (UnexpectedResponseFormatException e)
            {
                Tools.LogError($"Got unexpected esign token response for user {auth.UserName}: '{tokenResponse}'", e);
                throw;
            }

            string authValue = "Bearer " + tokenValue;
            string packagesURL = baseURL + "/webservices/esign/api/v1/packages";
            string packagesResponse = SubmitRequest(packagesURL, "GET", authValue, null);

            // get the loan and the old packages for comparison.
            var dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(details.sLId, typeof(DocMagicESignRequestMaker));
            dataLoan.InitLoad();

            string sDocMagicDownloadedDocsJSON = dataLoan.sDocMagicDownloadedDocsJSON;
            var seenSignedPackagesForThisLoan =  ObsoleteSerializationHelper.JavascriptJsonDeserializer<List<DocMagicESignedPackageTypeDetails>>(sDocMagicDownloadedDocsJSON);
            if (seenSignedPackagesForThisLoan == null)
            {
                seenSignedPackagesForThisLoan = new List<DocMagicESignedPackageTypeDetails>();
            }
            // get the signed packages that are new (may be more than one, or none.)
            var unseenSignedPackages = GetPackageIDs(packagesResponse, details.DocMagicAccountNumber, details.sLNm, worksheetNumber, packageType,
                    seenSignedPackagesForThisLoan);

            // upload the pdf for those packages, delete the "associated" esign request details.
            var newEDocsCount = 0;
            foreach (var unseenSignedPackage in unseenSignedPackages)
            {
                string packageURL = packagesURL + "/" + unseenSignedPackage.PackageID + "/documents";
                string packageResponse = SubmitRequest(packageURL, "GET", authValue, null);

                // upload it.
                string pdfContent = GetPDFContent(packageResponse);
                unseenSignedPackage.EDocsID = UploadPDF(pdfContent, employee, details.BrokerId, details.sLId, employee.UserID);
                if (unseenSignedPackage.EDocsID != Guid.Empty)
                {
                    newEDocsCount++;
                }
            }

            // Want to leave the e-sign request in the DB because we still
            // need the details if a borrower changes their e-consent status.
            // GF OPM 105770.
            //DocMagicDocumentGenerationRequestHandler.RemoveESignRequest(details.Id);


            // add newly acquired packages to list of known packages in database.
            seenSignedPackagesForThisLoan.AddRange(unseenSignedPackages);
            sDocMagicDownloadedDocsJSON = ObsoleteSerializationHelper.JavascriptJsonSerialize(seenSignedPackagesForThisLoan);

            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sDocMagicDownloadedDocsJSON = sDocMagicDownloadedDocsJSON;
            if (newEDocsCount > 0)
            {
                dataLoan.sHasESignedDocumentsT = E_sHasESignedDocumentsT.Yes; // may want to change this to be in the EDocs creation...
            }
            if (
                    (string.Compare(packageType, "initial disclosure", true) == 0)
                || (string.Compare(packageType, "redisclosure", true) == 0)
                )
            {
                dataLoan.sTilGfeRd = CDateTime.Create(DateTime.Now);
            }
            dataLoan.Save();
        }

        private static readonly HashSet<string> docTypeNamesRequiredForUploadPDF = new HashSet<string>(
            new string[] { "APRPAYMENTSCHEDULE", "IMPOUNDANALYIS", "SECTION 32", "GFE COMPARISON", "ESIGNEDDOCUMENTS" });

        private static Guid UploadPDF(string pdfContentAsString, EmployeeDB employee, Guid brokerId, Guid loanId, Guid userId)
        {
            // taken from DocumentGenerationRequestHelper
            // upload it to edocs for the doctype id.. initiate a barcode scanner if needed.

            BrokerDB brokerDb = BrokerDB.RetrieveById(brokerId);
            if (!brokerDb.AutoSaveDocMagicGeneratedDocs)
            {
                return Guid.Empty;
            }

            if (!brokerDb.DocMagicDefaultDocTypeID.HasValue)
            {
                var msg = "Need to fix " + brokerDb.CustomerCode + "'s settings.  They do not have a DocMagic default doctype selected.";
                throw CBaseException.GenericException(msg);
            }

            var docTypes = EDocumentDocType.GetDocTypesByBroker(brokerId, E_EnforceFolderPermissions.False);

            var docTypeIdByName = new Dictionary<string, int>(5);

            foreach (DocType docType in docTypes)
            {
                var upperCaseDocTypeName = docType.DocTypeName.ToUpper();

                if (DocMagicESignRequestMaker.docTypeNamesRequiredForUploadPDF.Contains(upperCaseDocTypeName))
                {
                    docTypeIdByName[upperCaseDocTypeName] = docType.Id;
                }
            } 

            var errorMessages = new List<string>();
            foreach(var requiredDocTypeName in DocMagicESignRequestMaker.docTypeNamesRequiredForUploadPDF)
            { 
                if(!docTypeIdByName.ContainsKey(requiredDocTypeName))
                {
                    errorMessages.Add("missing doctype: " + requiredDocTypeName);
                    continue;   
                }

                if (-1 == docTypeIdByName[requiredDocTypeName])
                {
                    errorMessages.Add("docType: " + requiredDocTypeName + " has doctype id of -1.");
                }                
            }

            if (errorMessages.Any())
            {
                throw CBaseException.GenericException("Need to fix " + brokerDb.CustomerCode + "'s doctypes.  " + string.Join(", ", errorMessages));
            }


            //HandleAudit(websheetResponse.Audit); //?


            int esignedDocTypeId = docTypeIdByName["ESIGNEDDOCUMENTS"];


            string pdfPath = Utilities.Base64StringToFile(pdfContentAsString, "docmagic");

            List<string> pdfKeys = new List<string>();
            string key = Guid.NewGuid().ToString();
            FileDBTools.WriteFile(E_FileDB.Temp, key, pdfPath);
            pdfKeys.Add(key);

            JavaScriptSerializer js = new JavaScriptSerializer();
            string pdflocationkey = AutoExpiredTextCache.AddToCache(js.Serialize(pdfKeys), TimeSpan.FromHours(1));

            // get the app id.
            Guid appID = Guid.Empty;
            var SqlParameters = new List<SqlParameter>();
            SqlParameters.Add(new SqlParameter("@LoanId", loanId));
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "App_GetPrimaryAppIdFromLoanId", SqlParameters))
            {
                if (reader.Read())
                {
                    appID = (Guid)reader["aAppId"];
                }
            }

            // use app id to upload doc.
            Guid edocGuid = Guid.Empty;

            var useCaptureMethod = brokerDb.EnableKtaIntegration
                && brokerDb.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.DocumentCapture;
            var useCaptureFallbackMethod = false;

            if (useCaptureMethod)
            {
                if (OcrEnabledBranchGroupManager.IsLoanBranchGroupEnabledForOcr(brokerDb, loanId))
                {
                    var parameters = KtaDocumentUploadParameters.FromEmployee(employee);
                    parameters.LoanId = loanId;
                    parameters.ApplicationId = appID;
                    parameters.CustomerCode = brokerDb.CustomerCode;
                    parameters.DocumentSource = E_EDocumentSource.GeneratedDocs;
                    parameters.PdfPath = pdfPath;

                    var result = KtaUtilities.UploadDocument(parameters);
                    if (result.Success)
                    {
                        return edocGuid;
                    }

                    var messageData = $"for DocMagic Esign Request Maker UploadPDF loan {loanId}, app {appID} ";
                    if (result.MissingCredentials)
                    {
                        // Allow falling back to existing upload behavior only when service credentials are missing.
                        Tools.LogError($"Missing service credentials " + messageData);
                        useCaptureFallbackMethod = true;
                    }
                    else
                    {
                        throw new CBaseException(ErrorMessages.Generic, "Error encountered " + messageData + result.ErrorMessage);
                    }
                }
                else
                {
                    useCaptureFallbackMethod = true;
                }
            }

            bool canUseSplitDocsForThisSigningType = brokerDb.DocMagicSplitESignedDocs;
            bool useSplitDocsAsPrimaryMethod = brokerDb.DocMagicDocumentSavingOption == E_DocMagicDocumentSavingOption.Split;
            bool useSplitDocsAsFallbackMethod = useCaptureFallbackMethod && brokerDb.BackupDocumentSavingOption == E_DocMagicDocumentSavingOption.Split;

            var useSplitDocsMethod = canUseSplitDocsForThisSigningType && (useSplitDocsAsPrimaryMethod || useSplitDocsAsFallbackMethod);

            if (useSplitDocsMethod)
            {
                DocMagicPDFManager.UploadDocument(userId, brokerId, loanId, appID, "", "Autosaved: " + DateTime.Now.ToShortTimeString(), pdfPath);
            }
            else
            {
                // At this point we haven't saved the documents using Capture or Split Docs. Save as Single Document
                // regardless of the save method settings to ensure the documents are not lost.
                edocGuid = CreateEdoc(pdfPath, esignedDocTypeId, appID, brokerId, loanId, employee.ID);
            }

            return edocGuid;
        }

        private static Guid CreateEdoc(string pdfPath, int docTypeId, Guid appID, Guid brokerID, Guid LoanID, Guid employeeID)
        {
            var employee = EmployeeDB.RetrieveById(brokerID, employeeID);
            var repository = EDocumentRepository.GetSystemRepository(brokerID);
            var edoc = repository.CreateDocument(E_EDocumentSource.GeneratedDocs);
            edoc.AppId = appID;
            edoc.PublicDescription = "Autosaved: " + DateTime.Now.ToShortTimeString();
            edoc.LoanId = LoanID;
            edoc.DocumentTypeId = docTypeId;
            edoc.IsUploadedByPmlUser = employee.UserType == 'P';
            edoc.UpdatePDFContentOnSave(pdfPath);
            edoc.Save(employee.UserID);

            return edoc.DocumentId;
        }

        private static string GetPDFContent(string packageResponse)
        {
            XmlDocument xmdoc = new XmlDocument();
            xmdoc.LoadXml(packageResponse);
            var xmlnsmgr = new XmlNamespaceManager(xmdoc.NameTable);
            xmlnsmgr.AddNamespace("x", "http://www.docmagic.com/2011/schemas");  // this is important to use.
            var pdfNode = SelectSingleNode(xmdoc, "//x:DocumentPackageContent", xmlnsmgr);
            var base64pdfContent = pdfNode.InnerText;
            return base64pdfContent;
        }
        private static XmlNode SelectSingleNode(XmlNode xmdoc, string xpath, XmlNamespaceManager xmlnsmgr)
        {
            var nodes = xmdoc.SelectNodes(xpath, xmlnsmgr);
            if (nodes.Count > 1)
            {
                throw new UnexpectedResponseFormatException(string.Format("found multiple nodes with xpath {0}, only expecting one.", xpath));
            }
            else if (nodes.Count == 0)
            {
                throw new UnexpectedResponseFormatException(string.Format("found no nodes with xpath {0}, expecting one.", xpath));
            }
            return nodes[0];
        }
        private static void LogBadPackageListing(string reason, string accountNumber, string loanNumber, string websheetNumber, string packageType, 
            XmlNode packageListing)
        {
            BadPackageCount++;

            if ((BadPackageCount % ConstStage.PeriodOfLogsInErrorsOfDMBadPackages) == 1
                || MathUtilities.IsPowerOfTwo(BadPackageCount))
            {
                Tools.LogErrorWithCriticalTracking(
                        reason + Environment.NewLine
                        + "for the " + BadPackageCount + " time." + Environment.NewLine
                        + "accountNumber: '" + accountNumber + "'" + Environment.NewLine
                        + "loanNumber: '" + loanNumber + "'" + Environment.NewLine
                        + "websheetNumber: '" + websheetNumber + "'" + Environment.NewLine
                        + "packageType: '" + packageType + "'" + Environment.NewLine
                        + packageListing.OuterXml
                        );
            }
        }

        private static IEnumerable<DocMagicESignedPackageTypeDetails> GetPackageIDs(string packagesResponse, string accountNumber, string loanNumber, string websheetNumber, string packageType,
            IEnumerable<DocMagicESignedPackageTypeDetails> signedPackagesForThisLoan)
        {
            var edocUploadedPackages = signedPackagesForThisLoan.Where((x) => x.PackageType == packageType);

            XmlDocument xmdoc = new XmlDocument();
            xmdoc.LoadXml(packagesResponse);
            var xmlnsmgr = new XmlNamespaceManager(xmdoc.NameTable);
            xmlnsmgr.AddNamespace("x", "http://www.docmagic.com/2011/schemas");  // this is important to use.
            var packageListings = xmdoc.SelectNodes("//x:PackageListing", xmlnsmgr);
            var unseenPackages = new List<DocMagicESignedPackageTypeDetails>();
            bool skipPackageType = false;
            string expectedXMLPackageType = "";
            if (!x_expectedXMLPackageTypeByEmailPackageType.ContainsKey(packageType))
            {
                Tools.LogWarning(packageType + " is not in the dictionary of known package types.");
                skipPackageType = true;
            }
            else
            {
                expectedXMLPackageType = x_expectedXMLPackageTypeByEmailPackageType[packageType];
            }
            foreach (XmlNode packageListing in packageListings)
            {
                if (packageListing.Attributes["packageStatusType"] == null)
                {
                    LogBadPackageListing("package listing was missing packageStatusType attribute.", accountNumber, loanNumber, websheetNumber,
                            packageType, packageListing);
                    continue;
                }
                else if (packageListing.Attributes["packageStatusType"].Value != "Signed")
                {
                    continue;
                }
                if (!skipPackageType)
                {
                    if (packageListing.Attributes["packageType"] == null)
                    {
                        LogBadPackageListing("package listing was missing packageType attribute.", accountNumber, loanNumber, websheetNumber,
                            packageType, packageListing);
                        continue;
                    }
                    else if(packageListing.Attributes["packageType"].Value != expectedXMLPackageType)
                    {
                        continue;
                    }
                }
                var originatorReferenceIdNode = packageListing.SelectSingleNode("x:OriginatorReferenceIdentifier", xmlnsmgr);
                if(originatorReferenceIdNode == null)
                {
                    LogBadPackageListing("package listing was missing OriginatorReferenceIdentifier subnode.", accountNumber, loanNumber, websheetNumber,
                                packageType, packageListing);
                    continue;
                }
                else if (String.Compare(originatorReferenceIdNode.InnerText, loanNumber, true) != 0)
                {
                    continue;
                }
                var originatorSystemIdNode = packageListing.SelectSingleNode("x:OriginatorSystemIdentifier", xmlnsmgr);
                if (originatorSystemIdNode == null)
                {
                    LogBadPackageListing("package listing was missing OriginatorSystemIdentifier subnode.", accountNumber, loanNumber, websheetNumber,
                                packageType, packageListing);
                    continue;
                }
                else if (originatorSystemIdNode.InnerText != accountNumber + "_" + websheetNumber)
                {
                    continue;
                }
                // only want the packages with packageids we haven't seen/uploaded before.
                string packageID = SelectSingleNode(packageListing, "x:PackageIdentifier", xmlnsmgr).InnerText;
                string versionID = SelectSingleNode(packageListing, "x:VersionIdentifier", xmlnsmgr).InnerText;
                if (edocUploadedPackages.Where((x) => x.PackageID == packageID).Count() != 0)
                {
                    continue;
                }

                unseenPackages.Add(new DocMagicESignedPackageTypeDetails()
                {
                    PackageID = packageID,
                    VersionID = versionID,
                    PackageType = packageType
                });
            }
            if (unseenPackages.Count < 1)
            {            
                Tools.LogWarning(string.Format("no packages were signed with  account number {0}, loan number {1}, websheet number {2}, and package type {3}, but an email was received",
                    accountNumber, loanNumber, websheetNumber, packageType));
            }
            if (unseenPackages.Count > 1)
            {   
                Tools.LogWarning("More than one package was signed with " + string.Format("account number {0}, loan number {1}, websheet number {2}, package type {3}",
                    accountNumber, loanNumber, websheetNumber, packageType) + " Will upload them all.");
            }


            return unseenPackages;
        }
        
        private static string GetTokenValue(string tokenResponse)
        {
            XmlDocument xmdoc = new XmlDocument();
            xmdoc.LoadXml(tokenResponse);
            var xmlnsmgr = new XmlNamespaceManager(xmdoc.NameTable);
            xmlnsmgr.AddNamespace("x", "http://www.docmagic.com/2011/schemas");  // this is important to use.

            var node = SelectSingleNode(xmdoc, "//x:TokenValue", xmlnsmgr);
            
            var base64token = node.InnerText;
            return base64token;
        }        

        private static string SubmitRequest(string url, string method, string authorizationValue, string content)
        {
            if (method != "GET" && method != "POST")
            {
                throw new ArgumentException("must use GET or POST in method parameter of MakeRequest, not " + method);
            }

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
            webRequest.Method = method;
            if (!string.IsNullOrEmpty(content))
            {
                webRequest.ContentType = "text/xml"; // OPM 23648 - DocMagic indicates that the content type should always be text/xml and not application/x-www-form-urlencoded as it previously was.

                byte[] bytes = System.Text.Encoding.UTF8.GetBytes(content);
                using (Stream stream = webRequest.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }

            }
            if (!string.IsNullOrEmpty(authorizationValue))
            {
                webRequest.Headers.Add("Authorization", authorizationValue);
            }


            // Mask out the password in PaulBunyan log
            string logString = "no content";
            if (content != null)
            {
                logString = Regex.Replace(content, "<AccountUserPassword>[^ ]+</AccountUserPassword>", "<AccountUserPassword>******</AccountUserPassword>");
            }
            StringBuilder debugStringBuilder = new StringBuilder("ESign url: " + url);
            debugStringBuilder.AppendLine("Request:" + Environment.NewLine + logString);
            debugStringBuilder.AppendLine();
            Tools.LogInfo(debugStringBuilder.ToString()); // Log the request. That way we'll at least get the request if the response is too big.



            StringBuilder sb = new StringBuilder();
            using (WebResponse webResponse = webRequest.GetResponse())
            {
                using (Stream stream = webResponse.GetResponseStream())
                {
                    byte[] buffer = new byte[60000];
                    int size = stream.Read(buffer, 0, buffer.Length);

                    while (size > 0)
                    {
                        string chunk = System.Text.Encoding.UTF8.GetString(buffer, 0, size);
                        sb.Append(chunk);
                        size = stream.Read(buffer, 0, buffer.Length);
                    }
                }
            }
            string xmlResponse = sb.ToString();
            return xmlResponse;
        }
    }
}
