using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Common;
using System.Data;
using LendersOfficeApp.los.RatePrice;
using System.Xml;
using LendersOffice.Conversions;
using LendersOffice.Security;
using CommonProjectLib.Common.Lib;
using LendersOffice.Admin;
using LendersOffice.Constants;
using LendersOffice.Integration.DocumentVendor;
using LendersOffice.RatePrice.FileBasedPricing;
using LendersOffice.RatePrice;

namespace LendersOffice.ObjLib.DocMagicLib
{

    public class DocMagicPlanCode
    {
        // This one is set when there was a tie (multiple winners), or no applicate code.
        // User needs to look at result and choose.
        public static readonly Guid PendingUserChoice = new Guid("55555555-5555-5555-5555-555555555555");

        // This one is for custom plan codes that we don't have in our db
        public static readonly Guid CustomPlanCode = new Guid("11111111-1111-1111-1111-111111111111");

        private DocMagicPlanCode() { } //Private constructor.  External caller should use the Create/Retrieve/List methods to get instance.

        /// <summary>
        /// Construct object using data from file based snapshot.
        /// </summary>
        /// <param name="item"></param>
        private DocMagicPlanCode(FileBasedDocMagicPlanCode item)
        {
            this.PlanCodeId = item.PlanCodeId;
            this.PlanCodeNm = item.PlanCodeNm;
            this.PlanCodeDesc = item.PlanCodeDesc;
            this.RuleXmlContent = item.RuleXmlContent;
            this.InvestorNm = string.IsNullOrEmpty(item.InvestorNm) ? "GENERIC" : item.InvestorNm;
        }

        public Guid   PlanCodeId   { get; set; }
        public string PlanCodeNm   { get; set; }
        public string PlanCodeDesc { get; set; }
        public string InvestorNm   { get; set; }

        private bool m_isNew = false;

        private string m_ruleXmlContent;
        public string RuleXmlContent
        {
            get { return m_ruleXmlContent; }
            set
            {
                string errorMessage;
                if (ValidateRuleContent(value, out errorMessage))
                {
                    m_ruleXmlContent = value;
                }
                else
                {
                    throw new CBaseException("Invalid Rule Content", errorMessage);
                }
            }
        }

        public static bool ValidateRuleContent (string content, out string message)
        {
            XmlDocument xmlDocEval = new XmlDocument();
            try
            {
                CCondition condition = CCondition.CreateTestCondition(content);
                condition.EvaluateTest(xmlDocEval);
                message = "Rule condition validates.";
                return true;
            }
            catch (CBaseException ex)
            {
                message = CBaseException.GetVerboseMessage(ex);
                return false;
            }
        }

        /// <summary>
        /// Validates that the data which will be put into a plan code will work. We can't take a regular DocMagicPlanCode object, because
        /// they do rules validation when you set the rule contents.
        /// </summary>
        /// <param name="codes">a tuple whose first string is the investor name, and whose second string is the rule content</param>
        /// <returns>A map of index in original list => error list for the plan codes</returns>
        public static Dictionary<int, List<string>> ValidatePlanCodeData(BrokerDB broker, IList<Tuple<string, string, string>> codeData)
        {
            var investorNames = GetInvestorNames(broker);
            var result = new Dictionary<int, List<string>>();
            var planCodeNames = new HashSet<string>();

            for (int i = 0; i < codeData.Count; i++)
            {
                var code = new { InvestorName = codeData[i].Item1, RuleXml = codeData[i].Item2, Name = codeData[i].Item3.TrimWhitespaceAndBOM()};
                var errors = new List<string>(3);
                string error;

                if (!ValidateRuleContent(code.RuleXml, out error))
                {
                    errors.Add(error);
                }

                if (!investorNames.Contains(code.InvestorName))
                {
                    errors.Add(string.Format("Investor name [{0}] not found.", code.InvestorName));
                }

                if (!planCodeNames.Add(code.Name))
                {
                    errors.Add(string.Format("Plan code [{0}] was already used.", code.Name));
                }

                if (errors.Any())
                {
                    result[i] = errors;
                }
            }

            return result;
        }

        private static HashSet<string> GetInvestorNames(BrokerDB broker)
        {
            var list = InvestorNameUtils.ListAllInvestorNames(broker);

            HashSet<string> names = new HashSet<string>(){"GENERIC"};

            foreach (var item in list)
            {
                string name = item.InvestorName;
                var synonyms = GetInvestorSynonyms(name);
                foreach (var syn in synonyms.Where(syn => !names.Add(syn)))
                {
                    Tools.LogWarning(string.Format("Investor name synonym collision at [{0}], check InvestorNameSynonyms in stage config.", syn));
                }
            }

            return names;
        }

        /// <summary>
        /// Create a new DocMagicPlanCode. It is not committed to DB until Save() is called.
        /// </summary>
        public static DocMagicPlanCode Create(string planCodeNm, string planCodeDesc, string investorNm, string ruleXmlContent )
        {
            return new DocMagicPlanCode()
            {
                PlanCodeNm      = planCodeNm
                , PlanCodeDesc    = planCodeDesc
                , InvestorNm      = investorNm
                , RuleXmlContent  = ruleXmlContent
                , m_isNew = true
            };
        }

        /// <summary>
        /// Create a new DocMagicPlanCode. It is not committed to DB until Save() is called.
        /// </summary>
        public static DocMagicPlanCode Create()
        {
            return new DocMagicPlanCode()
            {
                m_isNew = true
            };
        }

        /// <summary>
        /// Retrieve docmagic plan code.
        /// </summary>
        public static DocMagicPlanCode Retrieve(BrokerDB broker, Guid planCodeId)
        {
            if (ConstStage.UseLpeDatabaseForPricing)
            {
                using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc,
                    "RetrieveDocMagicPlanCodeById", new SqlParameter("PlanCodeId", planCodeId)))
                {
                    if (!reader.Read())
                    {
                        throw new PlanCodeNotFoundException(planCodeId);
                    }

                    return ReadPlanCode(reader);
                }
            }
            else
            {
                var snapshot = broker == null ? FileBasedSnapshot.RetrieveLatestManualImport() : broker.RetrieveLatestManualImport();
                foreach (var item in snapshot.GetDocMagicPlanCodeList())
                {
                    if (item.PlanCodeId == planCodeId)
                    {
                        return new DocMagicPlanCode(item);
                    }
                }

                throw new PlanCodeNotFoundException(planCodeId);
            }
        }

        /// <summary>
        /// Lists All DocMagic plan codes.
        /// </summary>
        public static List<DocMagicPlanCode> ListAllDocMagicPlanCodes(BrokerDB broker, bool onlyApprovedPlanCodes)
        {
            List<DocMagicPlanCode> planCodes = new List<DocMagicPlanCode>();

            if (ConstStage.UseLpeDatabaseForPricing)
            {
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListDocMagicPlanCodes", new SqlParameter("@ListAll", true)))
                {
                    while (reader.Read())
                    {
                        planCodes.Add(ReadPlanCode(reader));
                    }
                }
            }
            else
            {
                // 7/26/2017 - dd - Load doc magic plan code from file snapshot.
                var snapshot = broker == null ? FileBasedSnapshot.RetrieveLatestManualImport() : broker.RetrieveLatestManualImport();

                foreach (var item in snapshot.GetDocMagicPlanCodeList())
                {
                    planCodes.Add(new DocMagicPlanCode(item));
                }
            }

            if (onlyApprovedPlanCodes)
            {
                BrokerDB curbroker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                if (curbroker.IsOnlyAllowApprovedDocMagicPlanCodes)
                {
                    List<string> onlyAllowTheseCodes = DocMagicPlanCodeEvaluator.GetApprovedCodes(curbroker);
                    planCodes.RemoveAll(p => !onlyAllowTheseCodes.Contains(p.PlanCodeNm));
                }
            }

            return planCodes;
        }

        /// <summary>
        /// Lists All DocMagic plan codes.
        /// </summary>
        public static List<DocMagicPlanCode> ListAllDocMagicPlanCodes(BrokerDB broker)
        {
            return ListAllDocMagicPlanCodes(broker, false);
        }


        /// <summary>
        /// Lists All DocMagic plan codes by investor.
        /// </summary>
        public static List<DocMagicPlanCode> ListDocMagicPlanCodes(BrokerDB broker, string investorName, bool onlyApprovedPlanCodes)
        {
            List<DocMagicPlanCode> planCodes = new List<DocMagicPlanCode>();

            var synonyms = GetInvestorSynonyms(investorName);

            FileBasedSnapshot snapshot = null;
            IEnumerable<FileBasedDocMagicPlanCode> snapshotDocMagicPlanCodeList = null;

            if (!ConstStage.UseLpeDatabaseForPricing)
            {
                snapshot = broker == null ? FileBasedSnapshot.RetrieveLatestManualImport() : broker.RetrieveLatestManualImport();
                snapshotDocMagicPlanCodeList = snapshot.GetDocMagicPlanCodeList();
            }

            foreach (var name in synonyms)
            {
                if (ConstStage.UseLpeDatabaseForPricing)
                {
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "ListDocMagicPlanCodes"
                        , name != null ? new SqlParameter("@InvestorNm", name) : new SqlParameter("@InvestorNm", DBNull.Value)))
                    {
                        while (reader.Read())
                        {
                            planCodes.Add(ReadPlanCode(reader));
                        }
                    }
                }
                else
                {
                    foreach (var item in snapshotDocMagicPlanCodeList)
                    {
                        if (string.IsNullOrEmpty(name) && string.IsNullOrEmpty(item.InvestorNm))
                        {
                            planCodes.Add(new DocMagicPlanCode(item));
                        }
                        else if (item.InvestorNm != null && item.InvestorNm.Equals(name, StringComparison.OrdinalIgnoreCase))
                        {
                            planCodes.Add(new DocMagicPlanCode(item));
                        }
                    }
                }
            }

            if (onlyApprovedPlanCodes)
            {
                BrokerDB curBroker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
                if (curBroker.IsOnlyAllowApprovedDocMagicPlanCodes)
                {
                    List<string> onlyAllowTheseCodes = DocMagicPlanCodeEvaluator.GetApprovedCodes(curBroker);
                    planCodes.RemoveAll(p => !onlyAllowTheseCodes.Contains(p.PlanCodeNm));
                }
            }

            return planCodes;
        }

        public static List<string> GetInvestorSynonyms(string InvestorName)
        {
            if (InvestorName == null) return new List<string> { null };

            InvestorName = InvestorName.TrimWhitespaceAndBOM();
            return ConstStage.InvestorNameSynonyms.FirstOrDefault(a => a.Contains(InvestorName, StringComparer.CurrentCultureIgnoreCase)) ??
                                                   new List<string> { InvestorName };
        }


        /// <summary>
        /// Lists All DocMagic plan codes by investor.
        /// </summary>
        public static List<DocMagicPlanCode> ListDocMagicPlanCodes(BrokerDB broker, string investorName)
        {
            return ListDocMagicPlanCodes(broker, investorName, false /* Only Approved*/);
        }
        private static DocMagicPlanCode ReadPlanCode(DbDataReader reader)
        {
            return new DocMagicPlanCode()
                {
                    PlanCodeId          = (Guid) reader["PlanCodeId"]
                    , PlanCodeNm        = reader["PlanCodeNm"].ToString()
                    , PlanCodeDesc      = reader["PlanCodeDesc"].ToString()
                    , InvestorNm        = reader["InvestorNm"].ToString()
                    , m_ruleXmlContent  = reader["RuleXmlContent"].ToString()
                };
        }


        /// <summary>
        /// Delete plan code.
        /// </summary>
        /// <returns>True if a code was deleted, otherwise false.</returns>
        public static bool Delete(Guid planCodeId)
        {
            if (!ConstStage.UseLpeDatabaseForPricing)
            {
                // dd 7/26/2017 - Prevent accidental save on environment does not have this info in LPE database.
                throw new NotSupportedException("Cannot modify DocMagicPlanCode in this environment.");
            }

            return StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, "DeleteDocMagicPlanCode",3, new SqlParameter("@PlanCodeId", planCodeId)) > 0;
        }

        /// <summary>
        /// Commit this instance to the database.
        /// </summary>
        public void Save()
        {
            if (!ConstStage.UseLpeDatabaseForPricing)
            {
                // dd 7/26/2017 - Prevent accidental save on environment does not have this info in LPE database.
                throw new NotSupportedException("Cannot modify DocMagicPlanCode in this environment.");
            }

            SqlParameter planCodeParam;
            string procedure;
            if (m_isNew)
            {
                planCodeParam = new SqlParameter("@PlanCodeId", SqlDbType.UniqueIdentifier, 16);
                planCodeParam.Direction = ParameterDirection.Output;
                procedure = "CreateDocMagicPlanCode";
            }
            else
            {
                planCodeParam = new SqlParameter("@PlanCodeId", PlanCodeId);
                procedure = "UpdateDocMagicPlanCode";
            }
            List<SqlParameter> parameters = new List<SqlParameter>(
                new SqlParameter[]
                {
                    new SqlParameter("@PlanCodeNm", PlanCodeNm)
                    , new SqlParameter("@PlanCodeDesc", PlanCodeDesc)
                    , InvestorNm != null ? new SqlParameter("@InvestorNm", InvestorNm) : new SqlParameter("@InvestorNm", DBNull.Value )
                    , new SqlParameter("@RuleXmlContent", RuleXmlContent)
                    , planCodeParam
                });

            int result = StoredProcedureHelper.ExecuteNonQuery(DataSrc.LpeSrc, procedure, 3, parameters.ToArray());

            if (m_isNew)
            {
                PlanCodeId = (Guid)planCodeParam.Value;
                m_isNew = false;
            }
        }

        /// <summary>
        /// Set the plan code and register it with DM.
        /// </summary>
        /// <param name="sLId"></param>
        /// <param name="planCodeId"></param>
        public static void SetPlanCode(BrokerDB broker, Guid sLId, Guid planCodeId)
        {
            if (planCodeId != DocMagicPlanCode.PendingUserChoice
                && planCodeId != Guid.Empty)
            {
                DocMagicPlanCode planCode = DocMagicPlanCode.Retrieve(broker, planCodeId);

                if ( ValidatePlanCode(planCode.PlanCodeNm) != ValidatePlanCodeResult.Valid)
                {
                    // Do not set invalid plan code.
                    // This is rare unexpected case.
                    Tools.LogBug("User selected a plan code which did not validate for this loan, plan code was: " + planCode.PlanCodeNm +
                        ", id is: " + planCodeId +
                        ", rule xml is: " + Environment.NewLine + planCode.RuleXmlContent);
                    return;
                }
            }

            CPageData data = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(DocMagicPlanCode));
            data.InitSave(LendersOffice.Constants.ConstAppDavid.SkipVersionCheck);
            data.ByPassFieldSecurityCheck = true;
            data.sDocMagicPlanCodeId = planCodeId;
            data.Save();
        }

        public static ValidatePlanCodeResult ValidatePlanCode(BrokerDB broker, Guid planCodeId)
        {
            if (planCodeId == DocMagicPlanCode.PendingUserChoice
                || planCodeId == Guid.Empty)
            {
                return ValidatePlanCodeResult.Valid;
            }

            DocMagicPlanCode planCode = DocMagicPlanCode.Retrieve(broker, planCodeId);
            return ValidatePlanCode(planCode.PlanCodeNm);
        }

        /// <summary>
        /// Register it with DM.
        /// </summary>
        /// <param name="sLId"></param>
        /// <param name="planCodeId"></param>
        public static ValidatePlanCodeResult ValidatePlanCode(string planCodeName)
        {
            if ( planCodeName == string.Empty ) return ValidatePlanCodeResult.Valid;

            // OPM 85883. Because not all user logins have permission to call AddPlanCode,
            // we use the lender's DM login.
            BrokerDB broker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);

            if (broker.IsOnlyAllowApprovedDocMagicPlanCodes)
            {
                return ValidatePlanCodeResult.Valid;  // OPM 88138. No need to hit DM.  This code already exists at the lender.
            }

            if (string.IsNullOrEmpty(broker.DocMagicCustomerId)
                || string.IsNullOrEmpty(broker.DocMagicUserName)
                || string.IsNullOrEmpty(broker.DocMagicPassword.Value))
            {
                Tools.LogError(string.Format("Cannot call AddPlanToLender.  Broker {0} does not have DM login info.", broker.Name));
                return ValidatePlanCodeResult.Invalid;
            }

            var request = DocMagicMismoRequest.CreateAddPlanToLenderRequest(
                broker.DocMagicCustomerId,
                broker.DocMagicUserName,
                broker.DocMagicPassword.Value,
                planCodeName);

            try
            {
                var response = DocMagicServer2.Submit(request);

                if (response.Status == DocMagic.DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
                {
                    // This should be very rare case. They rejected the plan code we sent.
                    StringBuilder errors = new StringBuilder();
                    foreach (var msg in response.MessageList)
                    {
                        if (msg.Type == DocMagic.DsiDocResponse.E_MessageType.Fatal)
                        {
                            errors.Append(msg.InnerText + " ");
                        }
                    }

                    // Cannot make this save.  This plan code is bad.
                    Tools.LogError("AddPlanToLender fails for: "
                        + planCodeName
                        + ". Error: " + errors.ToString());
                    return ValidatePlanCodeResult.Invalid;
                }
                else
                {
                    return ValidatePlanCodeResult.Valid;
                }
            }
            catch (System.Net.WebException)
            {
                // No need to log, the Submit call already logs the exception. 
                return ValidatePlanCodeResult.NoResult;
            }
        }
    }

    public class PlanCodeNotFoundException : CBaseException
    {
        public PlanCodeNotFoundException(Guid missingPlanCode)
            : base("Plan Code Not Found", "Cannot find plan code: " + missingPlanCode.ToString())
        { }
    }


    public class DocMagicPlanCodeEvaluator
    {

        /// <summary>
        /// Evaluate all doc plan rules for the loan file to determine the match.
        /// </summary>
        /// <param name="sLid"></param>
        /// <returns>The list of Eligible plan codes. It will be a single item if there is no tie, empty if there is no match.</returns>
        public static List<DocMagicPlanCode> CalculateApplicablePlanCodes(Guid sLid)
        {
            if (!DocumentVendorFactory.CurrentVendors().Any(a => a.Skin.AutomaticPlanCodes)) return new List<DocMagicPlanCode>();

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLid, typeof(DocMagicPlanCodeEvaluator));
            dataLoan.InitLoad();
            dataLoan.ByPassFieldSecurityCheck = true; // We have to do this to access InvestorNm.  Not displayed to user.

            // PML Transform must be done for keywords to work correctly.
            // Note that we are not going to save and commit the transformed data.
            E_CalcModeT originalMode = dataLoan.CalcModeT;
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            dataLoan.CalcModeT = originalMode;

            // OPM 84422.  If the lender does not allow investor plan codes, only run generic.
            string sLpInvestorNm = dataLoan.BrokerDB.AllowInvestorDocMagicPlanCodes ? dataLoan.sLpInvestorNm : null;

            return Evaluate(dataLoan.BrokerDB, sLpInvestorNm, dataLoan.sSymbolTableForPriceRule);
       }

        /// <summary>
        /// Calculate the doc magic plan code
        /// </summary>
        /// <param name="sLid"></param>
        /// <returns></returns>
        public static Guid CalculateDocMagicPlanCode(Guid sLid)
        {
            if (!DocumentVendorFactory.CurrentVendors().Any(a => a.Skin.AutomaticPlanCodes)) return DocMagicPlanCode.CustomPlanCode;

            CPageData dataLoan = CPageData.CreatePmlPageDataUsingSmartDependency(sLid, typeof(DocMagicPlanCodeEvaluator));
            dataLoan.InitLoad();
            dataLoan.ByPassFieldSecurityCheck = true; // We have to do this to access InvestorNm. Not displayed to user.

            // PML Transform must be done for keywords to work correctly.
            // Note that we are not going to save and commit the transformed data.
            E_CalcModeT originalMode = dataLoan.CalcModeT;
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            dataLoan.CalcModeT = originalMode;

            // OPM 84422.  If the lender does not allow investor plan codes, only run generic.
            string sLpInvestorNm = dataLoan.BrokerDB.AllowInvestorDocMagicPlanCodes ? dataLoan.sLpInvestorNm : null;

            return CalculateDocMagicPlanCode(dataLoan.BrokerDB, sLpInvestorNm, dataLoan.sSymbolTableForPriceRule);
        }

        /// <summary>
        /// Calculate the doc magic plan code
        /// </summary>
        /// <param name="sLid"></param>
        /// <param name="shouldTransform"></param>
        public static Guid CalculateDocMagicPlanCode(BrokerDB broker, string sLpInvestorNm, CSymbolTable sSymbolTableForPriceRule)
        {
            List<DocMagicPlanCode> result = Evaluate(broker, sLpInvestorNm, sSymbolTableForPriceRule);

            if (result.Count == 0)
            {
                // There was no applicable result.  This should be unlikely.
                //Tools.LogInfo("NEW DM PLAN CODE: NO RESULT!");
                return DocMagicPlanCode.PendingUserChoice;
            }
            else if (result.Count == 1)
            {
                // There is a single winner
                //Tools.LogInfo("NEW DM PLAN CODE: " + result[0].PlanCodeId + ": " + result[0].PlanCodeNm);
                return result[0].PlanCodeId;
            }
            else
            {
                // Tie situation
                //Tools.LogInfo("NEW DM PLAN CODE: TIE!");
                //foreach (DocMagicPlanCode plan in result)
                //{
                //    Tools.LogInfo("WINNER: " + plan.PlanCodeId + ": " + plan.PlanCodeNm);
                //}
                return DocMagicPlanCode.PendingUserChoice;
            }
        }

        /// <summary>
        /// Evaluate all doc plan rules for the loan file to determine the match.
        /// </summary>
        /// <returns>The list of Eligible plan codes. It will be a single item if there is no tie, empty if there is no match.</returns>
        private static List<DocMagicPlanCode> Evaluate(BrokerDB broker, string sLpInvestorNm, CSymbolTable sSymbolTableForPriceRule)
        {
            List<DocMagicPlanCode> potentialPlanCodes = DocMagicPlanCode.ListDocMagicPlanCodes(broker, sLpInvestorNm);

            IList<string> onlyAllowTheseCodes = null;
            BrokerDB curBroker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);

            if (curBroker.IsOnlyAllowApprovedDocMagicPlanCodes)
            {
                onlyAllowTheseCodes = GetApprovedCodes(curBroker);
                potentialPlanCodes.RemoveAll(p => !onlyAllowTheseCodes.Contains(p.PlanCodeNm));
            }

            List<DocMagicPlanCode> winners = FindMatch(potentialPlanCodes, sSymbolTableForPriceRule);
            if (winners.Count > 0 || sLpInvestorNm == null)
            {
                // Found a result or this was a Generic run.
                return winners;
            }
            else
            {
                // There were no winners at the investor level.  Try generic.
                potentialPlanCodes = DocMagicPlanCode.ListDocMagicPlanCodes(broker, null);

                if (onlyAllowTheseCodes != null)
                {
                    potentialPlanCodes.RemoveAll(p => !onlyAllowTheseCodes.Contains(p.PlanCodeNm));
                }

                return FindMatch(potentialPlanCodes, sSymbolTableForPriceRule);
            }
        }

        private static List<DocMagicPlanCode> FindMatch(List<DocMagicPlanCode> potentialCodes, CSymbolTable sSymbolTableForPriceRule)
        {
            List<DocMagicPlanCode> winningCodes = new List<DocMagicPlanCode>();
            int winningKeywordCount = 0;

            foreach (DocMagicPlanCode code in potentialCodes)
            {
                int keywordCount;
                if (ExecuteRule(sSymbolTableForPriceRule, code.RuleXmlContent, out keywordCount))
                {
                    if (keywordCount > winningKeywordCount)
                    {
                        // New winner
                        winningCodes.Clear();
                        winningCodes.Add(code);
                        winningKeywordCount = keywordCount;
                    }
                    else if (keywordCount == winningKeywordCount)
                    {
                        // Tie
                        winningCodes.Add(code);
                    }
                }
            }
            return winningCodes;
        }

        private static bool ExecuteRule(CSymbolTable symbolTable, string ruleCondition, out int keywordsUsed)
        {
            // Performance Optimization:
            // Maybe pass in the current winning number of keywords,
            // so we can skip the execution when we know a higher number
            // has already been found successful.
            try
            {
                string xmlEval = CRuleEvalTool.ReplaceForEval(ruleCondition, symbolTable);
                XmlDocument xmlDocEval = new XmlDocument();
                XmlNode exprNode = CRuleEvalTool.CreateAnEvalNode(xmlDocEval, xmlEval);
                keywordsUsed = CountFields(exprNode);
                decimal rawResult = CRuleEvalTool.ExpressionDispatch(exprNode, 0, symbolTable);
                //Tools.LogInfo("Result: " + rawResult + ". Keywords Used: " + keywordsUsed + ". rule: " + ruleCondition);
                return rawResult == 1;
            }
            catch (CEvaluationException exc)
            {
                keywordsUsed = 0;
                exc.AllowVerboseDisplay = true; // OPM 228439. Allow dev message to show here so we can see the dev rule exception.
                Tools.LogErrorWithCriticalTracking("Unable to execute rule for DocMagicPlanCode: " + ruleCondition, exc);
                return false; // 4/27/2012 dd - Unable to execute rule.
            }
        }

        private static int CountFields(XmlNode expression)
        {
            if (expression.HasChildNodes)
            {
                System.Collections.Generic.HashSet<string> keywordsSeen = new System.Collections.Generic.HashSet<string>();

                XmlNodeReader reader = new XmlNodeReader(expression);
                try
                {
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            switch (reader.Name)
                            {
                                case "FieldName":
                                case "FunctionIntParams":
                                case "FunctionStringParams":
                                    keywordsSeen.Add(reader.ReadElementContentAsString());
                                    break;
                            }
                        }
                    }
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }

                return keywordsSeen.Count;
            }
            return 0;
        }

        public static List<string> GetApprovedCodes(BrokerDB broker)
        {

            // OPM 88138. Because not all user logins have permission to call LoadCurrentPlans,
            // we use the lender's DM login.
            //BrokerDB broker = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId);
            if (broker.IsOnlyAllowApprovedDocMagicPlanCodes == false)
                Tools.LogBug("Bug: Lender does not filter unapproved codes, but the system is loading them: " + broker.Name);

            if (string.IsNullOrEmpty(broker.DocMagicCustomerId)
                || string.IsNullOrEmpty(broker.DocMagicUserName)
                || string.IsNullOrEmpty(broker.DocMagicPassword.Value))
            {
                throw new CBaseException(ErrorMessages.Generic, string.Format("Cannot call LoadCurrentPlans.  Broker {0} does not have DM login info.", broker.Name));
            }

            var request = DocMagicMismoRequest.CreateLoadCurrentPlansRequest(
                broker.DocMagicCustomerId,
                broker.DocMagicUserName,
                broker.DocMagicPassword.Value);

            var response = DocMagicServer2.Submit(request);

            if (response.Status == DocMagic.DsiDocResponse.E_DsiDocumentServerResponseStatus.Failure)
            {
                StringBuilder errors = new StringBuilder();
                foreach (var msg in response.MessageList)
                {
                    if (msg.Type == DocMagic.DsiDocResponse.E_MessageType.Fatal)
                    {
                        errors.Append(msg.InnerText + " ");
                    }
                }

                // Cannot move forward.  Fatal error.
                throw new CBaseException(ErrorMessages.Generic, string.Format("Fatal error in LoadCurrentPlans: " + errors.ToString(), broker.Name));
            }

            if (!response.PlanResponseList.Any() || !response.PlanResponseList.First().PlanList.Any())
            {
                return new List<string>(); // No codes
            }

            return  new List<string>(
             from pc in response.PlanResponseList.First().PlanList
             select pc.Code);
        }
    }
}
