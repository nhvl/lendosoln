﻿// <copyright file="LoanError.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   8/25/2014 11:19:33 AM 
// </summary>
namespace LendersOffice.ObjLib.BatchOperationError
{
    using System;
    using System.Xml.Schema;
    using System.Xml.Serialization;

    /// <summary>
    /// Represents a generic loan error.
    /// </summary>
    public class LoanError : IXmlSerializable
    {
        /// <summary>
        /// Initializes a new instance of the LoanError class. Intended mainly 
        /// for use with XmlSerializer.
        /// </summary>
        public LoanError()
        {
        }

        /// <summary>
        /// Initializes a new instance of the LoanError class.
        /// </summary>
        /// <param name="loanId">The id of the loan.</param>
        /// <param name="loanNumber">The loan number.</param>
        /// <param name="errorMessage">The error message.</param>
        public LoanError(Guid loanId, string loanNumber, string errorMessage)
        {
            this.LoanId = loanId;
            this.LoanNumber = loanNumber;
            this.ErrorMessage = errorMessage;
        }

        /// <summary>
        /// Gets the loan id.
        /// </summary>
        /// <value>The loan id.</value>
        public Guid LoanId { get; private set; }

        /// <summary>
        /// Gets the loan number.
        /// </summary>
        /// <value>The loan number.</value>
        public string LoanNumber { get; private set; }
        
        /// <summary>
        /// Gets the error message.
        /// </summary>
        /// <value>The error message.</value>
        public string ErrorMessage { get; private set; }

        /// <summary>
        /// IXmlSerializable method. Always returns null.
        /// </summary>
        /// <returns>Returns null.</returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Deserializes the XML in reader into this LoanError instance.
        /// </summary>
        /// <param name="reader">The XmlReader to read from.</param>
        public void ReadXml(System.Xml.XmlReader reader)
        {
            this.LoanId = new Guid(reader.GetAttribute("LoanId"));
            this.LoanNumber = reader.GetAttribute("LoanNumber");
            this.ErrorMessage = reader.GetAttribute("ErrorMessage");
            reader.ReadStartElement();
        }

        /// <summary>
        /// Serializes this instance to XML.
        /// </summary>
        /// <param name="writer">The XmlWriter to write to.</param>
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("LoanId", this.LoanId.ToString());
            writer.WriteAttributeString("LoanNumber", this.LoanNumber ?? string.Empty);
            writer.WriteAttributeString("ErrorMessage", this.ErrorMessage ?? string.Empty);
        }
    }
}
