﻿// <copyright file="BatchOperationErrorOfT.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   8/25/2014 11:08:11 AM 
// </summary>
namespace LendersOffice.ObjLib.BatchOperationError
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Schema;
    using System.Xml.Serialization;
    using DataAccess;

    /// <summary>
    /// Provides generic batch error functionality.
    /// </summary>
    /// <typeparam name="T">The type of the errors.</typeparam>
    public class BatchOperationError<T> : IXmlSerializable where T : IXmlSerializable
    {
        /// <summary>
        /// The number of minutes this should be saved in the cache.
        /// </summary>
        private const int CacheTimeInMinutes = 10;

        /// <summary>
        /// A list of the errors that occurred during the batch operation.
        /// </summary>
        private List<T> errors;

        /// <summary>
        /// Initializes a new instance of the BatchOperationError class.
        /// </summary>
        public BatchOperationError()
        {
            this.errors = new List<T>();
            this.Description = "";
        }

        /// <summary>
        /// Initializes a new instance of the BatchOperationError class.
        /// </summary>
        /// <param name="description">Friendly description for the errors that took place.</param>
        public BatchOperationError(string description)
        {
            this.errors = new List<T>();
            this.Description = description;
        }

        /// <summary>
        /// Gets the friendly description for this batch operation error.
        /// </summary>
        /// <value>The description for the batch operation.</value>
        public string Description { get; private set; }

        /// <summary>
        /// Gets an enumerable of the errors that occurred during the batch operation.
        /// </summary>
        /// <value>
        /// An enumerable of the errors that occurred during the batch operation.
        /// </value>
        public IEnumerable<T> Errors
        {
            get { return this.errors; }
        }

        /// <summary>
        /// Gets a value indicating whether any errors occurred.
        /// </summary>
        /// <value>True if there are any errors.</value>
        public bool HasErrors
        {
            get { return this.Errors.Any(); }
        }

        /// <summary>
        /// Adds an error.
        /// </summary>
        /// <param name="error">The error to add.</param>
        public void Add(T error)
        {
            this.errors.Add(error);
        }

        /// <summary>
        /// Saves this instance to the cache. It will expire in 10 minutes.
        /// </summary>
        /// <returns>Returns the key to access the cached instance.</returns>
        public string SaveToCache()
        {
            var serializer = new XmlSerializer(typeof(BatchOperationError<T>));
            var sb = new StringBuilder();
            using (var writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, this);
            }

            return AutoExpiredTextCache.AddToCache(sb.ToString(), TimeSpan.FromMinutes(CacheTimeInMinutes));
        }

        /// <summary>
        /// IXmlSerializable method. Always returns null.
        /// </summary>
        /// <returns>Returns null.</returns>
        public XmlSchema GetSchema()
        {
            return null;
        }

        /// <summary>
        /// Deserializes the XML into objects of type T and adds them to the Errors property.
        /// </summary>
        /// <param name="reader">The XmlReader to read from.</param>
        public void ReadXml(XmlReader reader)
        {
            this.Description = reader.GetAttribute("Description");

            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();
            if (isEmptyElement)
            {
                return;
            }

            var serializer = new XmlSerializer(typeof(T));
            while (reader.NodeType == XmlNodeType.Element)
            {
                T error = (T)serializer.Deserialize(reader);
                this.Add(error);
            }

            reader.ReadEndElement();
        }

        /// <summary>
        /// Serializes this instance to XML.
        /// </summary>
        /// <param name="writer">The XmlWriter to write to.</param>
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("Description", this.Description);

            var serializer = new XmlSerializer(typeof(T));

            foreach (T error in this.errors)
            {
                serializer.Serialize(writer, error);
            }
        }
    }
}
