﻿// <copyright file="BatchOperationError.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   8/27/2014 2:53:10 PM 
// </summary>
namespace LendersOffice.ObjLib.BatchOperationError
{
    using System.IO;
    using System.Xml.Serialization;
    using DataAccess;

    /// <summary>
    /// Static class for dealing with generic BatchOperationError instances.
    /// </summary>
    public static class BatchOperationError
    {
        /// <summary>
        /// Loads an instance of BatchOperationError from the cache.
        /// </summary>
        /// <typeparam name="T">The type of the errors.</typeparam>
        /// <param name="cacheId">The id of the serialized BatchOperationError in the cache.</param>
        /// <returns>A new instance of the BatchOperationError class.</returns>
        /// <exception cref="CBaseException">
        /// Thrown when cache retrieval fails due to bad key or cache expiration.
        /// </exception>
        public static BatchOperationError<T> LoadFromCache<T>(string cacheId) where T : IXmlSerializable
        {
            var serializer = new XmlSerializer(typeof(BatchOperationError<T>));

            string serializedInstance = AutoExpiredTextCache.GetFromCache(cacheId);

            if (string.IsNullOrEmpty(serializedInstance))
            {
                throw CBaseException.GenericException("The instance was no longer in the cache.");
            }

            using (var reader = new StringReader(serializedInstance))
            {
                return (BatchOperationError<T>)serializer.Deserialize(reader);
            }
        }
    }
}
