/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisDwebSubmitRequest : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "DWEB_SUBMIT_REQUEST"; }
        }

        private string m_mornetPlusCasefileIdentifier;
        private List<XisCasefileInstitution> m_casefileInstitutionList = new List<XisCasefileInstitution>();
        private XisSoftwareProvider m_softwareProvider;
        private XisCredit m_credit;
        private XisOptions m_options;


        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        public void AddCasefileInstitution(XisCasefileInstitution casefileInstitution)
        {
            m_casefileInstitutionList.Add(casefileInstitution);
        }
        public XisSoftwareProvider SoftwareProvider 
        {
            get { return m_softwareProvider; }
            set { m_softwareProvider = value; }
        }
        public XisCredit Credit 
        {
            get { return m_credit; }
            set { m_credit = value; }
        }
        public XisOptions Options 
        {
            get { return m_options; }
            set { m_options = value; }
        }
        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "MORNETPlusCasefileIdentifier", m_mornetPlusCasefileIdentifier);
            WriteElement(writer, m_casefileInstitutionList);
            WriteElement(writer, m_softwareProvider);
            WriteElement(writer, m_options);
            WriteElement(writer, m_credit);
        }
	}
}
