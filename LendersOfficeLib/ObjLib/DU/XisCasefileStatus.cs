﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.DU
{
    /// <summary>
    /// This class will parse the STATUS_FILE section in the XIS Casefile Export response.
    /// </summary>
    public class XisCasefileStatus
    {
        public string BrokerInstitutionIdentifier { get; private set; }
        public string BrokerInstitutionName { get; private set; }
        public string CasefileLastUpdateDate { get; private set; }
        public string InvestorLoanIdentifier { get; private set; }
        public string LenderCaseIdentifier { get; private set; }
        public string LenderInstitutionIdentifier { get; private set; }
        public string LenderInstitutionName { get; private set; }
        public string SubmitterUserIdentifier { get; private set; }
        public string CreditReportStatusCode { get; private set; }
        public string CustomDesktopUnderwriterRecommendationCode { get; private set; }
        public string CustomDesktopUnderwriterStatusCode { get; private set; }
        public string DesktopOriginatorSubmissionStatusCode { get; private set; }
        public string DesktopOriginatorSubmissionTypeCode { get; private set; }
        public string DesktopUnderwriterRecommendationCode { get; private set; }
        public string DesktopUnderwriterStatusCode { get; private set; }
        public string MortgageTypeCode { get; private set; }
        public string PrimaryBorrowerLastName { get; private set; }
        public string ProductCode { get; private set; }

        public XisCasefileStatus(string xml)
        {
            if (string.IsNullOrEmpty(xml))
                return;

            Parse(xml);
        }

        private void Parse(string xml)
        {
            XmlDocument doc = Tools.CreateXmlDoc(xml);
            XmlElement transactionInformationElement = (XmlElement) doc.SelectSingleNode("//CASEFILE_STATUS/TRANSACTION_INFORMATION");

            if (null == transactionInformationElement)
            {
                throw new CBaseException(ErrorMessages.Generic, "//CASEFILE_STATUS/TRANSACTION_INFORMATION is not found in " + xml);
            }

            BrokerInstitutionIdentifier = transactionInformationElement.GetAttribute("BrokerInstitutionIdentifier");
            BrokerInstitutionName = transactionInformationElement.GetAttribute("BrokerInstitutionName");
            CasefileLastUpdateDate = transactionInformationElement.GetAttribute("CasefileLastUpdateDate");
            InvestorLoanIdentifier = transactionInformationElement.GetAttribute("InvestorLoanIdentifier");
            LenderCaseIdentifier = transactionInformationElement.GetAttribute("LenderCaseIdentifier");
            LenderInstitutionIdentifier = transactionInformationElement.GetAttribute("LenderInstitutionIdentifier");
            LenderInstitutionName = transactionInformationElement.GetAttribute("LenderInstitutionName");
            SubmitterUserIdentifier = transactionInformationElement.GetAttribute("SubmitterUserIdentifier");

            XmlElement decisionInformationElement = (XmlElement)doc.SelectSingleNode("//CASEFILE_STATUS/DECISION_INFORMATION");
            if (null == decisionInformationElement)
            {
                throw new CBaseException(ErrorMessages.Generic, "//CASEFILE_STATUS/DECISION_INFORMATION is not found in " + xml);
            }
            CreditReportStatusCode = decisionInformationElement.GetAttribute("CreditReportStatusCode");
            CustomDesktopUnderwriterRecommendationCode = decisionInformationElement.GetAttribute("CustomDesktopUnderwriterRecommendationCode");
            CustomDesktopUnderwriterStatusCode = decisionInformationElement.GetAttribute("CustomDesktopUnderwriterStatusCode");
            DesktopOriginatorSubmissionStatusCode = decisionInformationElement.GetAttribute("DesktopOriginatorSubmissionStatusCode");
            DesktopOriginatorSubmissionTypeCode = decisionInformationElement.GetAttribute("DesktopOriginatorSubmissionTypeCode");
            DesktopUnderwriterRecommendationCode = decisionInformationElement.GetAttribute("DesktopUnderwriterRecommendationCode");
            DesktopUnderwriterStatusCode = decisionInformationElement.GetAttribute("DesktopUnderwriterStatusCode");
            MortgageTypeCode = decisionInformationElement.GetAttribute("MortgageTypeCode");
            PrimaryBorrowerLastName = decisionInformationElement.GetAttribute("PrimaryBorrowerLastName");
            ProductCode = decisionInformationElement.GetAttribute("ProductCode");


        }
    }
}
