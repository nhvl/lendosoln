﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.DU;

namespace LendersOffice.DU
{
    public class XisEarlyCheckReturnFile : AbstractXisXmlNode
    {

        protected override string ElementName
        {
            get { return "_RETURN_FILE"; }
        }

        public E_XisEarlyCheckReturnFileType Type { get; set; }
        public E_XisEarlyCheckReturnFileFormatType FormatType { get; set; }

        protected override void GenerateXmlContent(System.Xml.XmlWriter writer)
        {
            WriteAttribute(writer, "_Type", Type.ToString("D"));
            WriteAttribute(writer, "_FormatType", FormatType.ToString("D"));
        }
    }

    public enum E_XisEarlyCheckReturnFileType
    {
        EarlyCheckResults = 128,
        EarlyCheckSummaryResults = 129,
        EarlyCheckXmlSummaryResultsWinzip = 130,
        EarlyCheckXmlSummaryGzip = 131,
        EarlyCheckLoanLevelResults = 132
    }

    public enum E_XisEarlyCheckReturnFileFormatType
    {
        Html = 3,
        Xml = 5
    }
}
