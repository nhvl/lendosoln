﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;

namespace LendersOffice.DU
{
    public class FnmaXisGetCreditAgenciesResponse : AbstractFnmaXisResponse
    {
        public FnmaXisGetCreditAgenciesResponse()
            : base(Guid.Empty)
        {
        }

        public FnmaXisGetCreditAgenciesResponse(string rawData) :
            base(rawData) { }

        private List<KeyValuePair<string, string>> m_creditAgencyList = new List<KeyValuePair<string,string>>();
        public IEnumerable<KeyValuePair<string, string>> GetCreditAgencyList()
        {
            return m_creditAgencyList;
        }

        public string ExportToXml()
        {
            XDocument xdoc = new XDocument();
            XElement root = new XElement("cra");
            xdoc.Add(new XComment("Generated on " + DateTime.Now));
            xdoc.Add(root);

            foreach (var o in GetCreditAgencyList().OrderBy(o => o.Value))
            {
                XElement item = new XElement("item");
                item.SetAttributeValue("name", o.Value + " (" + o.Key.PadLeft(3, '0') + ")");
                item.SetAttributeValue("id", o.Key.PadLeft(3, '0'));
                root.Add(item);
            }
            return xdoc.ToString();


        }
        protected override void ParseControlOutput(System.Xml.XmlDocument doc)
        {
            XmlNodeList nodeList = doc.SelectNodes("/GET_CREDIT_AGENCY_RESPONSE/CREDIT_BUREAU");

            foreach (XmlElement el in nodeList) {
                m_creditAgencyList.Add(new KeyValuePair<string,string>(el.GetAttribute("_Number"), el.GetAttribute("_Name")));
            }

        }

        public override bool HasBusinessError
        {
            get { return false; }
        }

        public override string BusinessErrorMessage
        {
            get { return string.Empty; }
        }

        public override string MornetPlusCasefileIdentifier
        {
            get { return string.Empty; }
        }

        protected override string FileDbKeyPrefix
        {
            get { return "FNMA_GETCREDITAGENCIES"; }
        }
    }
}
