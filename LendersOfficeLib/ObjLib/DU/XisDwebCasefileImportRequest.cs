/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace LendersOffice.DU
{
    public class XisDwebCasefileImportRequest : AbstractXisXmlNode
    {
        protected override string ElementName 
        {
            get { return "DWEB_CASEFILE_IMPORT_REQUEST"; }
        }

        private string m_mornetPlusCasefileIdentifier;
        private string m_validationType;
        private List<XisCasefileInstitution> m_casefileInstitutionList = new List<XisCasefileInstitution>();

        private XisSoftwareProvider m_softwareProvider = new XisSoftwareProvider();
        private XisDataFile m_dataFile = new XisDataFile();

        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        public string ValidationType 
        {
            get { return m_validationType; }
            set { m_validationType = value; }
        }
        public void AddCasefileInstitution(XisCasefileInstitution casefileInstitution) 
        {
            m_casefileInstitutionList.Add(casefileInstitution);
        }
        

        public XisSoftwareProvider SoftwareProvider 
        {
            get { return m_softwareProvider; }
        }
        public XisDataFile DataFile 
        {
            get { return m_dataFile; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "MORNETPlusCasefileIdentifier", m_mornetPlusCasefileIdentifier);
            WriteAttribute(writer, "ValidationType", m_validationType);
            WriteElement(writer, m_casefileInstitutionList);
            WriteElement(writer, m_softwareProvider);
            WriteElement(writer, m_dataFile);
        }

	}
}
