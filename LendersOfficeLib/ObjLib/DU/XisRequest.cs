/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisRequest : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "REQUEST"; }
        }
        
        private string m_requestDateTime;
        private string m_internalAccountIdentifier;
        private string m_loginAccountIdentifier;
        private string m_loginAccountPassword;
        private XisRequestData m_requestData;

        public string RequestDateTime 
        {
            get { return m_requestDateTime; }
            set { m_requestDateTime = value; }
        }
        public string InternalAccountIdentifier 
        {
            get { return m_internalAccountIdentifier; }
            set { m_internalAccountIdentifier = value; }
        }
        public string LoginAccountIdentifier 
        {
            get { return m_loginAccountIdentifier; }
            set { m_loginAccountIdentifier = value; }
        }
        public string LoginAccountPassword 
        {
            get { return m_loginAccountPassword; }
            set { m_loginAccountPassword = value; }
        }
        public XisRequestData RequestData 
        {
            get { return m_requestData; }
            set { m_requestData = value; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "RequestDateTime", m_requestDateTime);
            WriteAttribute(writer, "InternalAccountIdentifier", m_internalAccountIdentifier);
            WriteAttribute(writer, "LoginAccountIdentifier", m_loginAccountIdentifier);
            WriteAttribute(writer, "LoginAccountPassword", m_loginAccountPassword);
            WriteElement(writer, m_requestData);
        }
	}
}
