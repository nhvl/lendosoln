/// Author: David Dao

using System;
using System.Collections.Generic;
using System.Xml;

using DataAccess;
using LendersOffice.CreditReport.FannieMae;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Constants;
using LendersOffice.ObjLib.Task;
using LendersOffice.Reminders;
using LendersOffice.CreditReport;
using LendersOffice.Integration.TotalScorecard;
using System.Globalization;
using System.Xml.Linq;
using LendersOffice.Common;

namespace LendersOffice.DU
{
	public class FnmaXisDuUnderwriteResponse : AbstractFnmaXisResponse
	{
        private bool m_hasBusinessError = false;
        private bool m_isLoginError = false;
        private string m_businessErrorMessage = null;
        private string m_mornetPlusCasefileIdentifier;
        private string m_recommendationDescription = string.Empty;
        private string m_lenderInstitutionIdentifier = string.Empty;

        protected override string FileDbKeyPrefix 
        {
            get { return "FNMA_DU_UNDERWRITE"; }
        }
        public override bool HasBusinessError 
        {
            get { return m_hasBusinessError; }
        }
        public override string BusinessErrorMessage 
        {
            get { return m_businessErrorMessage; }
        }
        public FnmaXisDuUnderwriteResponse(Guid sLId) : base(sLId) 
        {
        }
		public FnmaXisDuUnderwriteResponse(string rawData) : base(rawData)
		{
		}
        public string FnmaFindingHtml 
        {
            get { return GetFannieMaePart("UNDERWRITING_FINDINGS_HTML"); }
        }
        public string FnmaFindingHtmlNew
        {
            get { return GetFannieMaePart("UNDERWRITING_FINDINGS_NEW_HTML"); }
        }
        public override string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
        }
        public override string LenderInstitutionIdentifier
        {
            get { return m_lenderInstitutionIdentifier; }
        }
        public override string UnderwritingRecommendationDescription 
        {
            get { return m_recommendationDescription; }
        }

        public override bool IsLoginError
        {
            get
            {
                return this.m_isLoginError;
            }
        }

        public DateTime UnderwritingResultsDateTime
        {
            get
            {
                DateTime dateTime = DateTime.MinValue;
                string xml = FnmaFindingXml;
                if (!string.IsNullOrEmpty(UnderwritingRecommendationDescription) && !string.IsNullOrEmpty(xml))
                {
                    XmlDocument doc = Tools.CreateXmlDoc(xml);
                    XmlElement responseHeader = (XmlElement)doc.SelectSingleNode("//CodifiedFindings/ResponseHeader");
                    if (responseHeader != null)
                    {
                        string underwritingResultsDateTime = responseHeader.GetAttribute("UnderwritingResultsDateTime");
                        if (!DateTime.TryParseExact
                            (underwritingResultsDateTime, "yyyyMMdd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
                        {
                            Tools.LogInfo("FnmaXisDuUnderwriteResponse::UnderwritingResultsDateTime [" + underwritingResultsDateTime + "] cannot be parsed into DateTime via format yyyyMMdd HH:mm:ss.");
                        }
                    }
                }
                return dateTime;
            }
        }
        public string FnmaFindingXml 
        {
            get { return GetFannieMaePart("UNDERWRITING_FINDINGS_XML"); }
        }
        public string UnderwritingFindingsPdf
        {
            get { return GetFannieMaePart("UNDERWRITING_FINDINGS_PDF"); }
        }

        public string CreditReportId
        {
            get; private set;
        }

        public string CreditProviderAgency
        {
            get
            {
                string ret = string.Empty;

                string xml = FnmaFindingXml;

                if (string.IsNullOrEmpty(xml))
                {
                    return string.Empty;
                }
                XmlDocument doc = Tools.CreateXmlDoc(xml);

                XmlElement el = (XmlElement)doc.SelectSingleNode("//CodifiedFindings/CreditReportSummary/CreditReport");

                if (null != el)
                {
                    return el.GetAttribute("AgencyIdentifier");
                }
                return string.Empty;
            }
        }

        public bool HasCreditReport()
        {
            bool hasCreditReportPrintFile = false;
            bool hasCreditReportMismoFile = false;
            if (FnmaPartList != null)
            {
                foreach (MultiPartItem part in FnmaPartList)
                {
                    if (part.Name.Contains("CREDITREPORT_PRINTFILE_"))
                    {
                        hasCreditReportPrintFile = true;
                    }
                    else if (part.Name.Contains("CREDITREPORT_MISMOFILE_"))
                    {
                        hasCreditReportMismoFile = true;
                    }
                }
            }

            return hasCreditReportPrintFile && hasCreditReportMismoFile;
        }

        public XmlDocument RetrieveMismoCreditReportBySsn(string ssn)
        {
            ssn = ssn.Replace("-", ""); // Mismo trim all the dash in their ssn.

            string viewableCredit = GetFannieMaePart("CREDITREPORT_PRINTFILE_" + ssn);
            string mismoCredit = GetFannieMaePart("CREDITREPORT_MISMOFILE_" + ssn);

            string htmlCredit = string.Format("<html><body><pre style='font-family:courier new'>{0}</pre></body></html>", viewableCredit.Replace((char)12, ' '));

            XmlDocument doc = FannieMae_CreditReportRequest.MergeFannieMaeCreditReport(mismoCredit, htmlCredit);

            return doc;
        }

        protected override void ParseRoutingOutput()
        {
            base.ParseRoutingOutput();
            if (this.ErrorMessage == ErrorMessages.FNMA_InvalidDUAuthentication)
            {
                this.m_isLoginError = true;
            }
        }

        protected override void ParseControlOutput(XmlDocument doc) 
        {

            XmlElement duUnderwriteResponseEl = (XmlElement) doc.SelectSingleNode("//DU_UNDERWRITE_RESPONSE");
            m_mornetPlusCasefileIdentifier = duUnderwriteResponseEl.GetAttribute("MORNETPlusCasefileIdentifier");
            m_lenderInstitutionIdentifier = duUnderwriteResponseEl.GetAttribute("LenderInstitutionIdentifier");
            XmlElement el = (XmlElement) doc.SelectSingleNode("//DU_UNDERWRITE_RESPONSE/STATUS");
            if (null != el) 
            {
                string condition = el.GetAttribute("_Condition");
                if (condition == "FAILURE") 
                {
                    m_hasBusinessError = true;
                    m_businessErrorMessage = el.GetAttribute("_Description");
                } 
                else 
                {
                    XmlElement creditElement = (XmlElement)doc.SelectSingleNode("//DU_UNDERWRITE_RESPONSE/CREDIT_INFORMATION");
                    if (creditElement != null)
                    {
                        this.CreditReportId = creditElement.GetAttribute("CreditReportIdentifier").Trim(); // DU sometimes adds trailing whitespace.
                    }
                    XmlElement recommendationEl = (XmlElement) doc.SelectSingleNode("//DU_UNDERWRITE_RESPONSE/_RECOMMENDATION");
                    if (null != recommendationEl) 
                    {
                        m_recommendationDescription = recommendationEl.GetAttribute("RecommendationDescription").TrimWhitespaceAndBOM(); //Remove trailing whitespace
                    }
                }
            } 
            else 
            {
                Tools.LogBug("There is no //DU_UNDERWRITE_RESPONSE/STATUS in FNMA Response.");
            }

        }

        public void ImportToLoanFile(Guid sLId, AbstractUserPrincipal principal, bool isImportDuFindings, bool isImportCreditReport, bool isCopyLiabilitiesFromCreditReport)
        {
            bool isImportDuFindingsAsConditions = BrokerDB.RetrieveById(principal.BrokerId).IsImportDoDuLpFindingsAsConditions;

            if (isImportDuFindings == false)
            {
                isImportDuFindingsAsConditions = false;
            }
            ImportToLoanFile(sLId, principal, isImportDuFindings, isImportDuFindingsAsConditions, isImportCreditReport, isCopyLiabilitiesFromCreditReport);
        }
        private void ImportToLoanFile(Guid sLId, AbstractUserPrincipal principal, bool isImportDuFindings, bool isImportFindingsAsConditions, bool isImportCreditReport, bool isCopyLiabilitiesFromCreditReport)
        {
            this.SaveToFileDB(sLId);

            CPageData dataLoan = null;
            if (this.HasAnyError)
            {
                // 6/11/2013 dd - Save Error message in DU Findings HTML so user can view it later.
                dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(FnmaXisDuUnderwriteResponse));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.sDuFindingsHtml = this.ErrorInHtml;
                // 6/17/2013 dd - If there is an error in DU we should set 3rd Party AUS response to N/A.
                dataLoan.sProd3rdPartyUwResultT = E_sProd3rdPartyUwResultT.NA;
                dataLoan.Save();

                return;
            }
            UpdateFromFnmaAuditItem audit = new UpdateFromFnmaAuditItem(principal, false, isImportDuFindings, isImportCreditReport);
            AuditManager.RecordAudit(sLId, audit);

            dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(FnmaXisDuUnderwriteResponse));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.SaveXisDuUnderwriteResponse(this);
            List<FnmaMessage> findingsList = ConvertDuFindings(FnmaFindingXml);

            #region Convert DU Findings to Conditions
            if (isImportFindingsAsConditions && findingsList.Count > 0)
            {

                BrokerDB currentBroker = BrokerDB.RetrieveById(principal.BrokerId);

                if (currentBroker.IsUseNewTaskSystem)
                {
                    IConditionImporter set = dataLoan.GetConditionImporter("DU");
                    set.IsSkipCategoryCheck = true; // 9/7/2011 dd - OPM 70845 don't check category for condition uniqueness.
                    foreach (var msg in findingsList)
                    {
                        set.AddUniqueAus(msg.MessageText, msg.MessageIdentifier);
                    }
                }
                else
                {
                    LoanConditionSetObsolete conditions = new LoanConditionSetObsolete();
                    conditions.RetrieveAll(sLId, true);

                    foreach (var msg in findingsList)
                    {
                        conditions.AddUniqueCondition("DU", msg.MessageText);
                    }
                    conditions.Save(principal.BrokerId);
                }
            }
            #endregion

            List<string> specialFeatureCodes = ConvertDuSpecialFeatureCodes(FnmaFindingXml);
            dataLoan.sProdIsDuRefiPlus = specialFeatureCodes.Exists(c => c.Equals(ConstAppDavid.FannieMae_SpecialFeatureCode_DURefiPlus));
            #region Import Special Feature Codes from DU Findings // OPM 107995
            if (isImportDuFindings)
            {
                dataLoan.sIsDuUw = true; // ejm opm 468065 - We want to set this for seamless DU only when importing findings.
                dataLoan.sGseDeliveryFeature1Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 0);
                dataLoan.sGseDeliveryFeature2Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 1);
                dataLoan.sGseDeliveryFeature3Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 2);
                dataLoan.sGseDeliveryFeature4Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 3);
                dataLoan.sGseDeliveryFeature5Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 4);
                dataLoan.sGseDeliveryFeature6Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 5);
                dataLoan.sGseDeliveryFeature7Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 6);
                dataLoan.sGseDeliveryFeature8Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 7);
                dataLoan.sGseDeliveryFeature9Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 8);
                dataLoan.sGseDeliveryFeature10Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 9);
            }
            #endregion

            #region Import Credit Report
            if (isImportCreditReport)
            {
                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    XmlDocument doc = this.RetrieveMismoCreditReportBySsn(dataApp.aBSsn);

                    if (null != doc)
                    {
                        FannieMae_CreditReportResponse fannieMaeCreditResponse = new FannieMae_CreditReportResponse(doc);

                        CreditReportUtilities.SaveXmlCreditReport(fannieMaeCreditResponse, principal, sLId, dataApp.aAppId, ConstAppDavid.DummyFannieMaeServiceCompany, Guid.Empty, E_CreditReportAuditSourceT.ImportFromDoDu);
                        if (isCopyLiabilitiesFromCreditReport)
                        {
                            dataApp.ImportLiabilitiesFromCreditReport(true, true);
                        }
                        dataApp.ImportCreditScoresFromCreditReport();
                        // 6/17/2009 dd - Clear all existing public records before reimport.
                        dataApp.aPublicRecordCollection.ClearAll();
                        dataApp.aPublicRecordCollection.Flush();
                        dataApp.ImportPublicRecordsFromCreditReport();
                        
                    }

                }
            }
            #endregion

            dataLoan.Save();

            // 2/4/2010 dd - OPM 45329 - Run TOTAL interface if the following are true.
            // Lender is turn on.
            // Loan is FHA loans
            // There is finding from DU.
            BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
            if (brokerDB.HasTotalScorePreviewFeature && dataLoan.sLT == E_sLT.FHA)
            {
                CreditRiskEvaluation.GenerateTemporaryTotalScoreCertificate(sLId);
            }


        }

        private string SetOrClearSpecialFeatureCode(List<string> SFCodes, int index)
        {
            return (SFCodes.Count > index) ? SFCodes[index] : "";
        }
	}
}
