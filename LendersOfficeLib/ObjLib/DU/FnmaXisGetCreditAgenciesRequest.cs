﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.DU
{
    public class FnmaXisGetCreditAgenciesRequest : AbstractDuXisRequest
    {
        public FnmaXisGetCreditAgenciesRequest()
            : base(Guid.Empty)
        {
        }
        public override string FnmaProductFunctionName
        {
            get { return "GetCreditAgencies"; }
        }

        public override string FnmaProductName
        {
            get { return "Credit"; }
        }

        public override string FnmaProductVersionNumber
        {
            get { return "2.0"; }
        }

        protected override void CreateControlInput(System.Xml.XmlWriter writer)
        {

        }

        public override AbstractFnmaXisResponse CreateResponse(string rawData)
        {
            return new FnmaXisGetCreditAgenciesResponse(rawData);
        }
    }
}
