/// Author: David Dao

using System;
using System.Collections;
using System.IO;
using System.Web;
using System.Text;
using System.Xml;

using LendersOffice.Constants;
using LendersOffice.Conversions;
using System.Collections.Generic;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.DU
{
    public enum E_FnmaXisRequestBusinessInputT
    {
        MornetPlus_Loan = 0,
        Mismo_Delivery = 1
    }
    public abstract class AbstractFnmaXisRequest
    {
        public abstract string FnmaProductFunctionName { get; }
        public abstract string FnmaProductName { get; }
        public abstract string FnmaProductVersionNumber { get; }

        protected virtual bool IsGenerateBusinessInputSection 
        {
            get { return true; }
        }

        public E_FnmaXisRequestBusinessInputT BusinessInputT { get; set; }

        private string m_fannieMaeMORNETUserID;
        private string m_fannieMaeMORNETPassword;
        private string m_lenderInstitutionId = "";

        private string m_globallyUniqueIdentifier = null;
        private Guid m_sLId;
        private int m_contentLength = 0;
        public int ContentLength 
        {
            get { return m_contentLength; }
        }
        public Guid sLId { get { return m_sLId; } }
		public AbstractFnmaXisRequest(Guid sLId)
		{
            m_sLId = sLId;
            // 10/4/2013 dd - Default to FNMA 3.2 format.
            BusinessInputT = E_FnmaXisRequestBusinessInputT.MornetPlus_Loan;
		}
        public string FannieMaeMORNETUserID 
        {
            get { return m_fannieMaeMORNETUserID; }
            //OPM 77169: Users will frequently copy/paste their username in, which leads to trailing or leading spaces.
            set { m_fannieMaeMORNETUserID = value.TrimWhitespaceAndBOM(); }
        }
        public string FannieMaeMORNETPassword 
        {
            get 
            {
                if (null == m_fannieMaeMORNETPassword)
                    return "";
                else
                    return m_fannieMaeMORNETPassword.TrimWhitespaceAndBOM(); 
            }
            set 
            { 
                m_fannieMaeMORNETPassword = value.TrimWhitespaceAndBOM(); 
            }
        }
        public string LenderInstitutionId
        {
            get { return m_lenderInstitutionId; }
            set { m_lenderInstitutionId = value; }
        }
        public string GloballyUniqueIdentifier 
        {
            get { return m_globallyUniqueIdentifier; }
            set { m_globallyUniqueIdentifier = value; }
        }

        // 7/13/2011 dd - Set this value to FNMA 3.2 content and sLId to Guid.Empty will use this value to transmit to DU.
        public string FnmaContent
        {
            get;
            set;
        }
        public abstract string Url 
        {
            get;
        }
        public void SetupAuthentication(System.Net.HttpWebRequest request) 
        {
            request.ContentType = "multipart/form-data; boundary=" + ConstAppDavid.FannieMae_MimeSeparator;
        }
        public byte[] GeneratePostContent() 
        {
            using (MemoryStream stream = new MemoryStream(5000)) 
            {
                StreamWriter streamWriter = new StreamWriter(stream);
                streamWriter.AutoFlush = true;

                GenerateMultipartSectionSeparator(streamWriter, "RI");
                CreateRoutingInput(stream);
                streamWriter.Write(Environment.NewLine + Environment.NewLine);

                if (m_globallyUniqueIdentifier == null || m_globallyUniqueIdentifier == "") 
                {
                    GenerateMultipartSectionSeparator(streamWriter, "CI");
                    CreateControlInput(stream);
                    streamWriter.Write(Environment.NewLine + Environment.NewLine);

                    if (IsGenerateBusinessInputSection) 
                    {
                        switch (BusinessInputT)
                        {
                            case E_FnmaXisRequestBusinessInputT.MornetPlus_Loan:
                                GenerateMultipartSectionSeparator(streamWriter, "MORNETPLUS_LOAN");
                                streamWriter.Write(CreateMornetPlusLoan());
                                streamWriter.Write(Environment.NewLine + Environment.NewLine);

                                break;
                            case E_FnmaXisRequestBusinessInputT.Mismo_Delivery:
                                GenerateMultipartSectionSeparator(streamWriter, "MISMO_DELIVERY");
                                UlddExporter exporter = new UlddExporter(m_sLId, E_sGseDeliveryTargetT.FannieMae);

                                streamWriter.Write(exporter.Export());
                                streamWriter.Write(Environment.NewLine + Environment.NewLine);

                                break;
                            default:
                                throw new UnhandledEnumException(BusinessInputT);
                        }
                    }
                }

                streamWriter.Write("--" + ConstAppDavid.FannieMae_MimeSeparator + "--");

                m_contentLength = (int) stream.Position;
                return stream.GetBuffer();
            }
        }


        private void CreateRoutingInput(Stream stream) 
        {
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM

            XmlWriter writer = XmlWriter.Create(stream, writerSettings);
            writer.WriteStartDocument();

            XisRequestGroup requestGroup = new XisRequestGroup();
            XisRequest request = requestGroup.Request;
            
            request.RequestDateTime        = DateTime.Now.ToString();
            request.LoginAccountIdentifier = m_fannieMaeMORNETUserID;
            request.LoginAccountPassword   = m_fannieMaeMORNETPassword;
            
            XisRequestData requestData = new XisRequestData();
            request.RequestData = requestData;

            requestData.GloballyUniqueIdentifier = m_globallyUniqueIdentifier;
            if (m_globallyUniqueIdentifier == null || m_globallyUniqueIdentifier == "") 
            {
                XisFnmProduct fnmProduct = new XisFnmProduct();
                requestData.FnmProduct = fnmProduct;

                fnmProduct.FunctionName  = FnmaProductFunctionName;
                fnmProduct.Name          = FnmaProductName;
                fnmProduct.VersionNumber = FnmaProductVersionNumber;
            }

            requestGroup.GenerateXml(writer);
            writer.Flush();

        }


        private void CreateControlInput(Stream stream) 
        {
            XmlWriterSettings writerSettings = new XmlWriterSettings();
            writerSettings.Encoding = new UTF8Encoding(false); // Skip BOM

            XmlWriter writer = XmlWriter.Create(stream, writerSettings);
            writer.WriteStartDocument();
            CreateControlInput(writer);
            writer.Flush();
        }
        protected abstract void CreateControlInput(XmlWriter writer);

        private string CreateMornetPlusLoan() 
        {
            if (m_sLId != Guid.Empty)
            {
                FannieMae32Exporter fnmaExport = new FannieMae32Exporter(m_sLId);
                fnmaExport.sFannieInstitutionId = m_lenderInstitutionId;
                fnmaExport.IsDUDOExport = true;

                byte[] content = fnmaExport.Export();

                m_errorMessageList.AddRange(fnmaExport.ErrorMessages);
                return System.Text.UTF8Encoding.UTF8.GetString(content);
            }
            else
            {
                return FnmaContent;
            }
        }

        private List<string> m_errorMessageList = new List<string>();
        public bool HasError
        {
            get { return m_errorMessageList.Count > 0; }
        }
        public IEnumerable<string> ErrorMessages
        {
            get { return m_errorMessageList; }
        }

        private void GenerateMultipartSectionSeparator(StreamWriter writer, string name) 
        {
            writer.WriteLine("--" + ConstAppDavid.FannieMae_MimeSeparator);
            writer.WriteLine(@"Content-Disposition: form-data; name=""" + name + @""";");
            writer.WriteLine("");

        }

        public abstract AbstractFnmaXisResponse CreateResponse(string rawData);
	}
}
