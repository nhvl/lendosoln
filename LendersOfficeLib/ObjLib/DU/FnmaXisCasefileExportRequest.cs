/// Author: David Dao

using System;
using System.Collections;
using System.Xml;


namespace LendersOffice.DU
{
    public enum E_CasefileExportRequestT
    {
        UnderwritingFindings = 0,
        EarlyCheckLoanLevel = 1
    }
	public class FnmaXisCasefileExportRequest : AbstractDuXisRequest
	{
		public FnmaXisCasefileExportRequest(Guid sLId) : base(sLId)
		{
            ExportRequestT = E_CasefileExportRequestT.UnderwritingFindings;
		}

        public E_CasefileExportRequestT ExportRequestT { get; set; }

        public override string FnmaProductName 
        {
            get { return "Casefile"; }
        }
        public override string FnmaProductFunctionName 
        {
            get { return "Export"; }
        }
        public override string FnmaProductVersionNumber 
        {
            get { return "2.0"; }
        }

        protected override bool IsGenerateBusinessInputSection 
        {
            get { return false; } // Export request does not need MORNETPLUS_LOAN section
        }

        private string m_mornetPlusCasefileIdentifier;
        private string m_lenderInstitutionIdentifier;

        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        public string LenderInstitutionIdentifier 
        {
            get { return m_lenderInstitutionIdentifier; }
            set { m_lenderInstitutionIdentifier = value; }
        }

        protected override void CreateControlInput(XmlWriter writer) 
        {
            XisCasefileExportRequest casefileExportRequest = new XisCasefileExportRequest();
            casefileExportRequest.LenderInstitutionIdentifier = m_lenderInstitutionIdentifier;
            casefileExportRequest.MornetPlusCasefileIdentifier = m_mornetPlusCasefileIdentifier;

            if (this.ExportRequestT == E_CasefileExportRequestT.EarlyCheckLoanLevel)
            {
                casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.EarlyCheckLoanLevelResults, E_XisExportReturnFileFormatType.HTML);
                casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.EarlyCheckLoanLevelResults, E_XisExportReturnFileFormatType.XML);
            }
            else
            {
                // 10/4/2013 dd - Add return type when export Desktop Underwriting findings.
                XisExportReturnFile returnFile = new XisExportReturnFile();

                returnFile.Type = E_XisExportReturnFileType._1003WorkingData;
                returnFile.FormatType = E_XisExportReturnFileFormatType.FixedDataFormat;
                returnFile.VersionNumber = "3.2";
                returnFile.Name = "MORNETPLUS_LOAN";
                casefileExportRequest.AddReturnFile(returnFile);


                if (m_lenderInstitutionIdentifier != null && m_lenderInstitutionIdentifier != "")
                {
                    // 10/25/2007 dd - For DO account, FannieMae requires a valid LenderInstitutionIdentifier (must be Lender type) to 
                    // retrieve DU Findings
                    casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.DUFindings, E_XisExportReturnFileFormatType.HTML);
                    casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.FHAFindings, E_XisExportReturnFileFormatType.HTML);
                    casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.VAFindings, E_XisExportReturnFileFormatType.HTML);

                }
                else
                {
                    // 11/9/2007 dd - For DO account use this file type to retrieve findings.
                    casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.RecentFindings502, E_XisExportReturnFileFormatType.HTML);

                }

                //            casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.ExtendedUWData, E_XisExportReturnFileFormatType.XML);
                casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.DUFindingsXml, E_XisExportReturnFileFormatType.XML);
                casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.CreditData, E_XisExportReturnFileFormatType.FormattedText);
                casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.CreditData, E_XisExportReturnFileFormatType.XML);
                casefileExportRequest.AddReturnFile(E_XisExportReturnFileType.CasefileStatus, E_XisExportReturnFileFormatType.XML);
            }
            casefileExportRequest.GenerateXml(writer);
        }

        public override AbstractFnmaXisResponse CreateResponse(string rawData) 
        {
            return new FnmaXisCasefileExportResponse(rawData);
        }
	}
}
