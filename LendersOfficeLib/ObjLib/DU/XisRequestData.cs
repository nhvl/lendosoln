/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisRequestData : AbstractXisXmlNode
	{
        private string m_globallyUniqueIdentifier;
        private XisFnmProduct m_fnmProduct;

        public string GloballyUniqueIdentifier 
        {
            get { return m_globallyUniqueIdentifier; }
            set { m_globallyUniqueIdentifier = value; }
        }
        public XisFnmProduct FnmProduct 
        {
            get { return m_fnmProduct; }
            set { m_fnmProduct = value; }
        }

        protected override string ElementName 
        {
            get { return "REQUEST_DATA"; }
        }
        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "_GloballyUniqueIdentifier", m_globallyUniqueIdentifier);
            WriteElement(writer, m_fnmProduct);
        }
	}
}
