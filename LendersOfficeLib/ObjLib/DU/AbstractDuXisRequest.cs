/// Author: David Dao

using System;
using LendersOffice.Constants;

namespace LendersOffice.DU
{
	public abstract class AbstractDuXisRequest : AbstractFnmaXisRequest
	{
        public AbstractDuXisRequest(Guid sLId) : base(sLId) 
        {
        }
        public override string Url 
        {
            get 
            {
                if (FannieMaeMORNETUserID == ConstStage.DUTestUserName || FannieMaeMORNETUserID == ConstAppDavid.FannieMae_TestDoUserId || FannieMaeMORNETUserID == ConstAppDavid.FannieMae_TestDoUserId2
                    || FannieMaeMORNETUserID == "d4bbnbnd" || FannieMaeMORNETUserID == ConstAppDavid.FannieMae_TestDuUserId)
                    return ConstAppDavid.FannieMae_BetaServer;
                else
                    return ConstAppDavid.FannieMae_ProductionServer;
            }
        }

	}
}
