/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisCasefileImportRequest : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "CASEFILE_IMPORT_REQUEST"; }
        }

        private XisDataFile m_dataFile = new XisDataFile();
        private string m_mornetPlusCasefileIdentifier;
        private string m_lenderInstitutionIdentifier;
        private string m_lenderCaseIdentifier;

        public XisDataFile DataFile 
        {
            get { return m_dataFile; }
        }
        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        public string LenderInstitutionIdentifier 
        {
            get { return m_lenderInstitutionIdentifier; }
            set { m_lenderInstitutionIdentifier = value; }
        }
        public string LenderCaseIdentifier 
        {
            get { return m_lenderCaseIdentifier; }
            set { m_lenderCaseIdentifier = value; }
        }

		public XisCasefileImportRequest()
		{
            // Set default value for DataFile 
            m_dataFile.Type = "1"; // 1003
            m_dataFile.FormatType = "1"; // 1003
            m_dataFile.VersionNumber = "3.2";
            m_dataFile.Name = "MORNETPLUS_LOAN";
		}
        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "MORNETPlusCasefileIdentifier", m_mornetPlusCasefileIdentifier);
            WriteAttribute(writer, "LenderInstitutionIdentifier", m_lenderInstitutionIdentifier);
            WriteAttribute(writer, "LenderCaseIdentifier", m_lenderCaseIdentifier);
            WriteElement(writer, m_dataFile);
        }

	}
}
