/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisFannieMaeUnderwritingEngine : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "FANNIEMAE_UNDERWRITING_ENGINE"; }
        }
            
        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "_Name", "DU");
        }
	}
}
