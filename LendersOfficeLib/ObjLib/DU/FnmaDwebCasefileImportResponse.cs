/// Author: David Dao

using System;
using System.Xml;
using System.Text;

namespace LendersOffice.DU
{
	public class FnmaDwebCasefileImportResponse : AbstractFnmaXisResponse
	{
        private bool m_hasBusinessError = false;
        private string m_businessErrorMessage = null;
        private string m_mornetPlusCasefileIdentifier = null;
        private bool m_isLoginError = false;

        public override string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
        }
        protected override string FileDbKeyPrefix 
        {
            get { return "FNMA_DWEB_CASEFILEIMPORT"; }
        }
        public override bool HasBusinessError 
        {
            get { return m_hasBusinessError; }
        }
        public override string BusinessErrorMessage 
        {
            get { return m_businessErrorMessage; }
        }

        public FnmaDwebCasefileImportResponse(Guid sLId) : base(sLId) 
        {
        }
		public FnmaDwebCasefileImportResponse(string rawData) : base(rawData)
		{
		}

        public override bool IsLoginError 
        {
            get { return m_isLoginError; }
        }
        private string GetValidationResults()
        {
            string validationResults = GetFannieMaePart("VALIDATION_RESULTS");
            
            if (string.IsNullOrEmpty(validationResults))
                return string.Empty;

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(validationResults);

            XmlNodeList list = doc.SelectNodes("//RESULT_DETAILS");
            StringBuilder sb = new StringBuilder();

            foreach (XmlElement el in list)
            {
                sb.AppendFormat("{0}: {1} - {2}.{3}", el.GetAttribute("_FieldId"), el.GetAttribute("_Description"), el.GetAttribute("_Message"), Environment.NewLine);
            }

            return sb.ToString();
        }
        protected override void ParseControlOutput(XmlDocument doc) 
        {
            XmlElement responseEl = (XmlElement) doc.SelectSingleNode("DWEB_CASEFILE_IMPORT_RESPONSE");

            m_mornetPlusCasefileIdentifier = responseEl.GetAttribute("MORNETPlusCasefileIdentifier");


            XmlNodeList statusList = responseEl.SelectNodes("STATUS");

            if (statusList.Count > 0) 
            {
                m_businessErrorMessage = "";
                foreach (XmlElement statusEl in statusList) 
                {
                    // 10/10/2007 dd - DU could return multiple error messages.
                    string condition = statusEl.GetAttribute("_Condition").ToUpper();
                    string errorCode = statusEl.GetAttribute("_Code");
                    if (errorCode == "DI001") 
                    {
                        m_isLoginError = true;
                    }
                    if (condition != "COMPLETE") 
                    {
                        m_hasBusinessError = true;
                        m_businessErrorMessage += statusEl.GetAttribute("_Description") + Environment.NewLine + Environment.NewLine;

                        if (errorCode == "DI011")
                        {
                            m_businessErrorMessage += GetValidationResults();
                        }
                    } 
                }
            } 
            else 
            {
                m_hasBusinessError = false;
                m_businessErrorMessage = "";
            }
        }
	}
}
