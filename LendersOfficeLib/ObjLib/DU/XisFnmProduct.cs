/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisFnmProduct : AbstractXisXmlNode
	{
        private XisConnection m_connection = new XisConnection();

        public string Name { get; set;}
        public string FunctionName { get; set;}
        public string VersionNumber { get; set;}
        public XisConnection Connection 
        {
            get { return m_connection; }
            set { m_connection = value; }
        }

        protected override string ElementName 
        {
            get { return "FNM_PRODUCT"; }
        }
        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            if (Name == "Credit" && FunctionName == "GetCreditAgencies")
            {
                // 10/30/2013 dd - GetCreditAgencies is synchronous method.
                Connection.ModeIdentifier = E_XisConnectionModeIdentifier.Synchronous;
            }
            WriteAttribute(writer, "_Name", Name);
            WriteAttribute(writer, "_FunctionName", FunctionName);
            WriteAttribute(writer, "_VersionNumber", VersionNumber);
            WriteElement(writer, Connection);
        }
	}
}
