/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class FnmaXisCasefileImportResponse : AbstractFnmaXisResponse
	{
        private bool m_hasBusinessError = false;
        private string m_businessErrorMessage = null;
        private string m_mornetPlusCasefileIdentifier = null;

        protected override string FileDbKeyPrefix 
        {
            get { return "FNMA_CASEFILE_IMPORT"; }
        }
        public override string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
        }

        public override bool HasBusinessError 
        {
            get { return m_hasBusinessError; }
        }
        public override string BusinessErrorMessage 
        {
            get { return m_businessErrorMessage; }
        }

        public FnmaXisCasefileImportResponse(Guid sLId) : base(sLId) 
        {
        }
		public FnmaXisCasefileImportResponse(string rawData) : base(rawData)
		{
		}
        protected override void ParseControlOutput(XmlDocument doc) 
        {
            XmlElement responseEl = (XmlElement) doc.SelectSingleNode("//CASEFILE_IMPORT_RESPONSE");
            m_mornetPlusCasefileIdentifier = responseEl.GetAttribute("MORNETPlusCasefileIdentifier");
            XmlElement statusEl = (XmlElement) responseEl.SelectSingleNode("//STATUS");
            if (null != statusEl) 
            {
                string condition = statusEl.GetAttribute("_Condition");
                if (condition == "FAILURE") 
                {
                    m_hasBusinessError = true;
                    m_businessErrorMessage = statusEl.GetAttribute("_Description");
                } 
            } 
            else 
            {
                m_hasBusinessError = true;
                m_businessErrorMessage = ""; // TODO: What should the generic message be? Is this scenario even possible.
            }
        }
	}
}
