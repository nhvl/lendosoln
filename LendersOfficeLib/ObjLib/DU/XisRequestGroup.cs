/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisRequestGroup : AbstractXisXmlNode
	{
        private string m_mismoVersionId = "2.1";
        private XisRequest m_request = new XisRequest();

        protected override string ElementName 
        { 
            get { return "REQUEST_GROUP"; }
        }

        public XisRequest Request 
        {
            get { return m_request; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "MISMOVersionID", m_mismoVersionId);
            WriteElement(writer, m_request);
        }
	}
}
    