/// Author: David Dao

using System;
using System.Collections;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisCasefileInstitution : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "CASEFILE_INSTITUTION"; }
        }

        private string m_lenderCaseIdentifier;
        private string m_institutionIdentifier;
        private string m_institutionName;
        private string m_institutionType;

        public string LenderCaseIdentifier 
        {
            get { return m_lenderCaseIdentifier; }
            set { m_lenderCaseIdentifier = value; }
        }
        public string InstitutionIdentifier 
        {
            get { return m_institutionIdentifier; }
            set { m_institutionIdentifier = value; }
        }
        public string InstitutionName 
        {
            get { return m_institutionName; }
            set { m_institutionName = value; }
        }
        public string InstitutionType 
        {
            get { return m_institutionType; }
            set { m_institutionType = value; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "LenderCaseIdentifier", m_lenderCaseIdentifier);
            WriteAttribute(writer, "InstitutionIdentifier", m_institutionIdentifier);
            WriteAttribute(writer, "InstitutionName", m_institutionName);
            WriteAttribute(writer, "InstitutionTYpe", m_institutionType);
        }

	}
}
