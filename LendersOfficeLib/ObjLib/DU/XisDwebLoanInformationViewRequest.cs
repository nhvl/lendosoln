/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisDwebLoanInformationViewRequest : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "DWEB_LOAN_INFORMATION_VIEW_REQUEST"; }
        }
        private string m_mornetPlusCasefileIdentifier;
        private List<XisCasefileInstitution> m_casefileInstitutionList = new List<XisCasefileInstitution>();

        private XisSoftwareProvider m_softwareProvider = null;
        private XisOptions m_options = null;

        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        public void AddCasefileInstitution(XisCasefileInstitution casefileInstitution) 
        {
            m_casefileInstitutionList.Add(casefileInstitution);
        }
        

        public XisSoftwareProvider SoftwareProvider 
        {
            get { return m_softwareProvider; }
            set { m_softwareProvider = value; }
        }
        public XisOptions Options 
        {
            get { return m_options; }
            set { m_options = value; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "MORNETPlusCasefileIdentifier", m_mornetPlusCasefileIdentifier);
            WriteElement(writer, m_casefileInstitutionList);
            WriteElement(writer, m_softwareProvider);
            WriteElement(writer, m_options);
        }
	}
}
