/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisConnection : AbstractXisXmlNode
	{

        private E_XisConnectionModeIdentifier m_modeIdentifier = E_XisConnectionModeIdentifier.Asynchronous;
        private string m_postBackURLIdentifier;

        public E_XisConnectionModeIdentifier ModeIdentifier 
        {
            get { return m_modeIdentifier; }
            set { m_modeIdentifier = value; }
        }
        public string PostBackURLIdentifier 
        {
            get { return m_postBackURLIdentifier; }
            set { m_postBackURLIdentifier = value; }
        }

        protected override string ElementName 
        {
            get { return "CONNECTION"; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "_ModeIdentifier", m_modeIdentifier);
            WriteAttribute(writer, "PostBackURLIdentifier", m_postBackURLIdentifier);
        }
	}

    public enum E_XisConnectionModeIdentifier 
    {
        Asynchronous,
        Synchronous,
        Postback
    }
}
