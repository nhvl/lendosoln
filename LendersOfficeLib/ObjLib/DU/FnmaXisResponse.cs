/// Author: David Dao

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using DataAccess;
using LendersOffice.Common;
namespace LendersOffice.DU
{
    public abstract class AbstractFnmaXisResponse
    {
        private IEnumerable<MultiPartItem> m_fnmaPartList = null;

        private bool m_isValid = true;

        public AbstractFnmaXisResponse(Guid sLId)
        {
            // Load object from FileDB. After using this construct need to check IsValid property to see if load was successful.
            m_isValid = false;

            string rawData = null;
            string dbFileKey = GetFileDbKey(sLId);

            Action<FileInfo> readHandler = delegate (FileInfo fi)
            {
                using (StreamReader stream = new StreamReader(fi.FullName))
                {
                    rawData = stream.ReadToEnd();
                }
            };

            try
            {
                FileDBTools.UseFile(E_FileDB.Normal, dbFileKey, readHandler);
            }
            catch (FileNotFoundException)
            {
                // NO-OP
            }

            if (null != rawData)
            {
                Initialize(rawData);
                m_isValid = true;
            }

        }
        public AbstractFnmaXisResponse(string rawData)
        {
            Initialize(rawData);
        }
        public bool IsValid
        {
            get { return m_isValid; }
        }
        private void Initialize(string rawData)
        {
            RawData = rawData;
            m_fnmaPartList = MultiPartParser.Parse(rawData);

            ParseRoutingOutput();

            if (IsReady && !HasError)
            {
                string xml = GetFannieMaePart("CO");
                if ("" != xml)
                {
                    XmlDocument doc = Tools.CreateXmlDoc(xml);
                    ParseControlOutput(doc);
                }
            }
        }

        protected virtual void ParseRoutingOutput()
        {
            IsReady = true;

            string xml = GetFannieMaePart("RO");
            if ("" == xml)
                return;

            XmlDocument doc = Tools.CreateXmlDoc(xml);

            XmlElement el = (XmlElement)doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/STATUS");
            
            string condition = el.GetAttribute("_Condition");
            if (condition == "ERROR")
            {
                HasError = true;
                ErrorMessage = el.GetAttribute("_Description");
                if (el.GetAttribute("_Code") == "1")
                {
                    // Convert to a better description for user.
                    ErrorMessage = ErrorMessages.FNMA_InvalidDUAuthentication;
                }
            }
            else if (condition == "PROCESSING")
            {
                IsReady = false;
            }

            el = (XmlElement)doc.SelectSingleNode("//RESPONSE_GROUP/RESPONSE/RESPONSE_DATA");
            GloballyUniqueIdentifier = el.GetAttribute("_GloballyUniqueIdentifier");
        }

        protected abstract void ParseControlOutput(XmlDocument doc);

        public virtual bool IsLoginError
        {
            get { return false; }
        }
        public bool IsReady
        {
            get; private set;
        }
        public string GloballyUniqueIdentifier
        {
            get; private set;
        }
        public bool HasError
        {
            get;
            private set;
        }
        public abstract bool HasBusinessError
        {
            get;
        }
        public abstract string BusinessErrorMessage
        {
            get;
        }
        public virtual string UnderwritingRecommendationDescription
        {
            get { return string.Empty; }
        }
        public abstract string MornetPlusCasefileIdentifier
        {
            get;
        }
        public virtual string LenderInstitutionIdentifier
        {
            get { return string.Empty; }
        }
        public string ErrorMessage
        {
            get; private set;
        }

        public string RawData
        {
            get; private set;
        }

        public string FnmaStatusLog
        {
            get { return GetFannieMaePart("MP_STATUS_LOG"); }
        }

        public bool HasAnyError
        {
            get { return HasError || HasBusinessError; }
        }
        public string ErrorInHtml
        {
            get
            {
                string htmlBodyFormat = "<html><body style=\"color:red\">{0}</body></html>";
                if (this.HasError)
                {
                    return string.Format(htmlBodyFormat, ErrorMessage);
                }
                else if (this.HasBusinessError)
                {
                    return string.Format(htmlBodyFormat, BusinessErrorMessage + Environment.NewLine + "<pre>" + FnmaStatusLog + "</pre>");
                } 
                else 
                {
                    return string.Empty;
                }
            }
        }

        protected string GetFannieMaePart(string partName)
        {
            if (FnmaPartList != null)
            {
                foreach (MultiPartItem part in FnmaPartList)
                {
                    if (part.Name == partName)
                        return part.Content;
                }
            }
            return "";
        }
        protected IEnumerable<MultiPartItem> FnmaPartList
        {
            get { return m_fnmaPartList; }
        }

        protected abstract string FileDbKeyPrefix
        {
            get;
        }

        // 10/1/2013 dd - DO NOT MODIFY THIS ARRAY.
        private static readonly string[] PreOptimizeFileDbKeyPrefixList = {
                                                                          "FNMA_CASEFILE_IMPORT",
                                                                          "FNMA_DU_UNDERWRITE",
                                                                          "FNMA_CASEFILE_EXPORT",
                                                                          "FNMA_DWEB_CASEFILESTATUS"
                                                                      };
        private string GetFileDbKey(Guid sLId)
        {
            string dbFileKey = string.Empty;

            // 10/1/2013 dd - Per FileDB documentation the proper usage is to have the unique aspect of key in front 
            // of the name. See https://intranet/Projects/FileDB
            //
            // However there was 4 FileDbKeyPrefix that does not follow the above recommendation. To avoid breaking
            // existing file we keep it as is.
            // 
            // DO NOT MODIFY the PreOptimizeFileDbKeyPrefixList
            if (PreOptimizeFileDbKeyPrefixList.Contains(FileDbKeyPrefix, StringComparer.OrdinalIgnoreCase))
            {
                dbFileKey = FileDbKeyPrefix + "_" + sLId.ToString("N");
            }
            else
            {
                dbFileKey = sLId.ToString("N") + "_" + FileDbKeyPrefix;
            }
            return dbFileKey;
        }
        public void SaveToFileDB(Guid sLId)
        {
            string dbFileKey = GetFileDbKey(sLId);
            byte[] data = System.Text.Encoding.UTF8.GetBytes(MaskPassword(RawData));
            FileDBTools.WriteData(E_FileDB.Normal, dbFileKey, data);
        }

        private string MaskPassword(string str)
        {
            return Regex.Replace(str, " LoginAccountPassword=\"[^ ]+\"", " LoginAccountPassword=\"******\"");
        }

        protected List<FnmaMessage> ConvertDuFindings(string fnmaFindingsXml)
        {
            List<FnmaMessage> list = new List<FnmaMessage>();

            if (string.IsNullOrEmpty(fnmaFindingsXml))
            {
                return list;
            }
            XmlDocument doc = Tools.CreateXmlDoc(fnmaFindingsXml);

            XmlNodeList nodeList = doc.SelectNodes("/CodifiedFindings/CasefileMessages/Message");

            foreach (XmlElement el in nodeList)
            {
                string messageIdentifier = el.GetAttribute("MessageIdentifier");
                // 10/30/2007 dd - We are currently only convert message with SeverityTypeCode=2 or 5
                string severityCode = el.GetAttribute("MessageSeverityTypeCode");
                string categoryCode = el.GetAttribute("MessageCategoryTypeCode");
                if (severityCode == "2" || severityCode == "5" || (severityCode == "1" && categoryCode == "1"))
                {
                    XmlElement lenderMessageEl = (XmlElement)el.SelectSingleNode("LenderMessageText/Line");

                    XmlNodeList tableRowList = el.SelectNodes("LenderMessageText/TableRow");
                    if (tableRowList.Count > 0)
                    {
                        string desc = lenderMessageEl.GetAttribute("MessageText");

                        XmlElement headerRow = (XmlElement)tableRowList[0];

                        for (int i = 1; i < tableRowList.Count; i++)
                        {
                            XmlElement dataRow = (XmlElement)tableRowList[i];
                            int j = 0;
                            StringBuilder sb = new StringBuilder();
                            foreach (XmlElement columnNode in headerRow.ChildNodes)
                            {

                                string data = ((XmlElement)dataRow.ChildNodes[j++]).GetAttribute("MessageText");
                                sb.AppendFormat("{0}: {1}{2}", columnNode.GetAttribute("MessageText"), data, Environment.NewLine);

                            }
                            list.Add(new FnmaMessage(messageIdentifier, desc + Environment.NewLine + sb));
                        }

                    }
                    else
                    {
                        list.Add(new FnmaMessage(messageIdentifier, lenderMessageEl.GetAttribute("MessageText")));
                    }


                }

            }

            return list;
        }

        protected List<string> ConvertDuSpecialFeatureCodes(string sFNMAFindingsXML)
        {
            List<string> SFCodeList = new List<string>();

            if (!string.IsNullOrEmpty(sFNMAFindingsXML))
            {
                XmlDocument doc = Tools.CreateXmlDoc(sFNMAFindingsXML);

                XmlNodeList SFCodeNodes = doc.SelectNodes("/CodifiedFindings/SpecialFeatureCodes/SpecialFeatureCode");

                foreach (XmlElement code in SFCodeNodes)
                {
                    string sSFCode = code.GetAttribute("SpecialFeatureCode");
                    if (!string.IsNullOrEmpty(sSFCode))
                    {
                        SFCodeList.Add(sSFCode);
                    }
                }
            }
            return SFCodeList;
        }

        public E_sProd3rdPartyUwResultT DuResult
        {
            get
            {
                switch (this.UnderwritingRecommendationDescription.ToLower().TrimWhitespaceAndBOM())
                {
                    case "refer/eligible":
                        return E_sProd3rdPartyUwResultT.DU_ReferEligible;
                    case "out of scope":
                        return E_sProd3rdPartyUwResultT.OutOfScope;
                    case "approve/eligible":
                        return E_sProd3rdPartyUwResultT.DU_ApproveEligible;
                    case "refer/ineligible":
                        return E_sProd3rdPartyUwResultT.DU_ReferIneligible;
                    case "approve/ineligible":
                        return E_sProd3rdPartyUwResultT.DU_ApproveIneligible;
                    case "refer with caution":
                        return E_sProd3rdPartyUwResultT.DU_ReferWCautionEligible;
                    case "refer":
                        return E_sProd3rdPartyUwResultT.DU_ReferEligible;
                    case "ea-i/eligible":
                        return E_sProd3rdPartyUwResultT.DU_EAIEligible;
                    case "ea-ii/eligible":
                        return E_sProd3rdPartyUwResultT.DU_EAIIEligible;
                    case "ea-iii/eligible":
                        return E_sProd3rdPartyUwResultT.DU_EAIIIEligible;
                    case "ea-iii/ineligible":
                    case "ea-ii/ineligible":
                    case "ea-i/ineligible":
                    case "unknown":
                    case "deny":
                    case "error":
                    case "complete":
                    case "approve":
                    case "ineligible":
                    case "complete with warning":
                    case "refer w caution/i":
                    case "refer w caution/ii":
                    case "refer w caution/iii":
                    case "refer w caution/iv":
                    case "resubmit":
                    case "expanded approval/i":
                    case "expanded approval/ii":
                    case "expanded approval/iii":
                    case "expanded approval/iv":
                    case "ea-iv/eligible":
                    case "ea-iv/ineligible":
                    case "rwc-iv/eligible":
                    case "rwc-iv/ineligible":
                    default:
                        // These are the case we do not have mapping in E_sProd3rdPartyUwResultT

                        return E_sProd3rdPartyUwResultT.NA;
                }

            }
        }
    }

    public class FnmaMessage
    {
        public string MessageIdentifier { get; set; }
        public string MessageText { get; set; }
        public FnmaMessage(string messageIdentifier, string messageText)
        {
            MessageIdentifier = messageIdentifier;
            MessageText = messageText;
        }
    }
}