/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisCasefileExportRequest : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "CASEFILE_EXPORT_REQUEST"; }
        }
        private string m_mornetPlusCasefileIdentifier;
        private string m_lenderInstitutionIdentifier;
        private List<XisExportReturnFile> m_returnFileList = new List<XisExportReturnFile>();

        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        public string LenderInstitutionIdentifier 
        {
            get { return m_lenderInstitutionIdentifier; }
            set { m_lenderInstitutionIdentifier = value; }
        }
        public void AddReturnFile(XisExportReturnFile returnFile)
        {
            m_returnFileList.Add(returnFile);
        }
        public void AddReturnFile(E_XisExportReturnFileType type, E_XisExportReturnFileFormatType formatType) 
        {
            XisExportReturnFile returnFile = new XisExportReturnFile();
            returnFile.Type = type;
            returnFile.FormatType = formatType;
            AddReturnFile(returnFile);
        }

		public XisCasefileExportRequest()
		{
		}
        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "MORNETPlusCasefileIdentifier", m_mornetPlusCasefileIdentifier);
            WriteAttribute(writer, "LenderInstitutionIdentifier", m_lenderInstitutionIdentifier);
            WriteElement(writer, m_returnFileList);
        }
	}
}
