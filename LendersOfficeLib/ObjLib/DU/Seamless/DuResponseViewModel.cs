﻿namespace LendersOffice.ObjLib.DU.Seamless
{
    using System.Collections.Generic;
    using Conversions.Aus;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    /// <summary>
    /// Stores the data model for the UI of the SeamlessLpaSummary.html page.
    /// </summary>
    public class DuResponseViewModel
    {
        /// <summary>
        /// Gets or sets the response status to return to the UI.
        /// </summary>
        /// <value>Returned status.</value>
        [JsonConverter(typeof(StringEnumConverter))]
        public AusResponseStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the error message to go along with an error status, whether returned from the vendor or our system.
        /// </summary>
        /// <value>Error message.</value>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Gets or sets the Fannie Mae - generated ID for identifying an existing request.
        /// </summary>
        /// <value>Fannie ID.</value> 
        public string FannieId { get; set; }

        /// <summary>
        /// Gets or sets the case ID to identify the loan file in the Desktop Underwriter system.
        /// </summary>
        /// <value>DU Case ID.</value>
        public string DuCaseId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether an access denied error was encountered when trying to generate a request.
        /// </summary>
        /// <value>Whether access was denied.</value>
        public bool AccessDenied { get; set; }

        /// <summary>
        /// Gets or sets a log returned from the Desktop Underwriter system to detail why an error occurred.
        /// </summary>
        /// <value>DU response log.</value>
        public string DuResponseLog { get; set; }

        /// <summary>
        /// Gets or sets a mapping of (key, value) pairs to be displayed in the summary table.
        /// </summary>
        /// <value>Summary table contents.</value>
        public Dictionary<string, string> DuResultsSummary { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the automated underwriting was a success.
        /// </summary>
        /// <value>Whether underwriting was successful.</value>
        public bool UnderwritingSuccess { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to import the DU findings to the loan file.
        /// </summary>
        /// <value>Whether to import DU findings.</value>
        public bool? ImportAusFindings { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to import the credit report information to the loan file.
        /// </summary>
        /// <value>Whether to import credit report information.</value>
        public bool? ImportCreditReport { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to import liabilities from the credit report to the loan file.
        /// </summary>
        /// <value>Whether to import liabilities from the credit report.</value>
        public bool? ImportLiabilities { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the response contains a credit report.
        /// </summary>
        public bool HasCreditReport { get; set; }
    }
}
