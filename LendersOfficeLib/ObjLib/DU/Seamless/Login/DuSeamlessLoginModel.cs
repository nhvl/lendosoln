﻿namespace LendersOffice.ObjLib.DU.Seamless.Login
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using Common;
    using Conversions.Aus;
    using Conversions.Templates;
    using DataAccess;
    using LendersOffice.DU;
    using LendersOffice.Security;
    using ServiceCredential;

    /// <summary>
    /// Represents the page model for the SeamlessDU\SeamlessDuLogin.html page.
    /// </summary>
    public class DuSeamlessLoginModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DuSeamlessLoginModel"/> class. Creates the model for the new DU Seamless login page.
        /// This class is serialized to JSON for acting as a model for the SeamlessDuLogin.html page.
        /// </summary>
        /// <param name="loanData">The loan data object to use in creating the model.</param>
        /// <param name="user">The user requesting the page.</param>
        public DuSeamlessLoginModel(CPageData loanData, AbstractUserPrincipal user)
        {
            FnmaXisDuUnderwriteResponse underwriteResponse = new FnmaXisDuUnderwriteResponse(loanData.sLId);
            this.CreditInfo = new AusCreditOrderingInfo(loanData, user, AUSType.DesktopUnderwriter);
            this.CreditInfo.CraProviderId = underwriteResponse.IsValid ? underwriteResponse.CreditProviderAgency : user.DuAutoCraId;

            this.ImportLiabilitiesFromCreditReport = false;

            bool hasDuAutoLogin = false;
            string duAutoLogin = null;
            if (user.BrokerDB.UsingLegacyDUCredentials)
            {
                hasDuAutoLogin = user.HasDuAutoLogin;
                duAutoLogin = hasDuAutoLogin ? user.DuLastLoginNm : null;
            }
            else
            {
                var savedCredential = ServiceCredential.ListAvailableServiceCredentials(user, loanData.sBranchId, ServiceCredentialService.AusSubmission)
                                            .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter);
                hasDuAutoLogin = savedCredential != null;
            }

            this.DuCredentials = new DuCredentialModel(loanData.sDuCaseId, loanData.sDuLenderInstitutionId, hasDuAutoLogin, duAutoLogin);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DuSeamlessLoginModel"/> class.
        /// </summary>
        public DuSeamlessLoginModel()
        {
        }

        /// <summary>
        /// Gets or sets a container which holds credentials for the user to login to DU.
        /// </summary>
        /// <value>Du credentials.</value>
        public DuCredentialModel DuCredentials
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether the liabilities from the retrieved credit report should be imported into the 1003 loan application.
        /// </summary>
        /// <value>Whether to import credit report values to the 1003.</value>
        public bool ImportLiabilitiesFromCreditReport
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a container which holds information for getting a credit report.
        /// </summary>
        /// <value>Credit Info.</value>
        public AusCreditOrderingInfo CreditInfo
        {
            get; set;
        }

        /// <summary>
        /// Generates a JSON string to send to the UI, with all the data necessary to display the Seamless DU Login page.
        /// The data includes information about the status of the loan edit, an instance of <see cref="DuSeamlessLoginModel"/>, and enum information for <see cref="CreditReportType"/>.
        /// </summary>
        /// <param name="userPrincipal">The user requesting the Seamless DU Login page.</param>
        /// <param name="loanData">The loan file data to use for loading page info.</param>
        /// <param name="isPml">Whether the requesting page is in the PML website.</param>
        /// <returns>A JSON string for sending back to the UI.</returns>
        public static string GenerateLoginModelJson(AbstractUserPrincipal userPrincipal, CPageData loanData, bool isPml)
        {
            var auditResult = LoanDataAuditor.RunAudit(
                loanData.sLId, 
                DuSeamlessAuditConditions.Singleton,
                isPml);
            return SerializationHelper.JsonNetAnonymousSerialize(new
            {
                // Allow "Internal" users to view the page without passing the audit, in case they're testing something unrelated and don't have a handy loan file.
                ignoreAudit = userPrincipal.HasPermission(Permission.CanModifyLoanPrograms),
                auditSuccess = auditResult.LoanErrors.Count == 0 && auditResult.BorrowerAudits.All(borrower => borrower.Errors.Count == 0),
                pagemodel = new DuSeamlessLoginModel(loanData, userPrincipal),
                creditProviderOptions = DUServer.GetCreditProviderList(),
                
                // Serialize the CreditReportType enum in a way so that it can be easily used in the javascript. "creditReportOptions.OrderNew.description", "creditReportOptions.OrderNew.value" etc.
                creditReportOptions = Enum.GetValues(typeof(CreditReportType)).Cast<CreditReportType>().Except(new List<CreditReportType> { CreditReportType.Blank })
                    .ToDictionary(option => option.ToString("G"), option => new { value = (int)option, description = EnumUtilities.GetDescription(option) })
            });
        }

        /// <summary>
        /// Saves the relevant DU inputs from the user to the loan file.
        /// </summary>
        /// <param name="loanData">A loan data object that has been prepared with the proper dependencies and had <see cref="CPageData.InitSave(int)"/> called.</param>
        public void ApplyToLoanFile(CPageData loanData)
        {
            loanData.sDuCaseId = this.DuCredentials.DuCasefileId;
            loanData.sDuLenderInstitutionId = this.DuCredentials.DuLenderInstitutionId;
        }

        /// <summary>
        /// Converts this model object to a DU Underwriting request for sending to DU.
        /// </summary>
        /// <param name="user">The <see cref="AbstractUserPrincipal"/> submitting the request.</param>
        /// <param name="loanData">The InitLoaded loan data with the proper dependencies to load application info and sLId from.</param>
        /// <param name="fannieId">The Fannie Mae identifier for checking on an existing request.</param>
        /// <returns>A request ready to be sent through <see cref="DUServer.Submit(AbstractFnmaXisRequest, bool)"/>.</returns>
        public FnmaXisDuUnderwriteRequest ConvertToDuUnderwritingRequest(AbstractUserPrincipal user, CPageData loanData, string fannieId)
        {
            FnmaXisDuUnderwriteRequest request = new FnmaXisDuUnderwriteRequest(loanData.sLId);
            request.CopyLiabilitiesIndicator = this.ImportLiabilitiesFromCreditReport;
            request.SkipCreditSection = this.CreditInfo.CreditReportOption == CreditReportType.UsePrevious || this.CreditInfo.CreditReportOption == CreditReportType.NoReport;
            request.CraProviderNumber = string.IsNullOrEmpty(user.DuAutoCraId) ? this.CreditInfo.CraProviderId : user.DuAutoCraId;
            request.CraLoginName = string.IsNullOrEmpty(user.DuAutoCraLogin) ? this.CreditInfo.CraUserId : user.DuAutoCraLogin;
            request.CraPassword = string.IsNullOrEmpty(user.DuAutoCraPassword) ? this.CreditInfo.CraPassword : user.DuAutoCraPassword;
            request.GloballyUniqueIdentifier = fannieId;
            request.UseLegacyFindingsFormat = loanData.sLT == E_sLT.FHA || loanData.sLT == E_sLT.VA || user.BrokerDB.UsingLegacyFindingsForSeamlessDU;

            if (user.BrokerDB.UsingLegacyDUCredentials)
            {
                request.FannieMaeMORNETUserID = user.HasDuAutoLogin ? user.DuAutoLoginNm : this.DuCredentials.DuUserId;
                request.FannieMaeMORNETPassword = user.HasDuAutoLogin ? user.DuAutoPassword : this.DuCredentials.DuPassword;
            }
            else
            {
                var savedCredential = ServiceCredential.ListAvailableServiceCredentials(user, loanData.sBranchId, ServiceCredentialService.AusSubmission)
                                            .FirstOrDefault(credential => credential.ChosenAusOption.HasValue && credential.ChosenAusOption.Value == AusOption.DesktopUnderwriter);

                request.FannieMaeMORNETUserID = savedCredential != null ? savedCredential.UserName : this.DuCredentials.DuUserId;
                request.FannieMaeMORNETPassword = savedCredential != null ? savedCredential.UserPassword.Value : this.DuCredentials.DuPassword;
            }

            if (string.IsNullOrEmpty(fannieId))
            {
                loanData.SetFormatTarget(FormatTarget.FannieMaeMornetPlus);
                foreach (AusCreditOrderingInfo.AusCreditAppData appInput in this.CreditInfo.Applications)
                {
                    CAppData app = loanData.GetAppData(appInput.ApplicationId);
                    if (app == null)
                    {
                        continue;
                    }

                    request.AddCreditInformation(
                        new XisCreditInformation(
                            app.aBSsn,
                            appInput.CraResubmitId,
                            app.aHasSpouse ? E_XisCreditInformationRequestType.Joint : E_XisCreditInformationRequestType.Individual));
                }
            }

            return request;
        }

        /// <summary>
        /// Represents the model for the credentials section of the page, with values necessary for DU login.
        /// </summary>
        public class DuCredentialModel
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="DuCredentialModel"/> class.
            /// </summary>
            /// <param name="duCasefileId">The DU casefile ID.</param>
            /// <param name="duLenderInstitutionId">The lender's institution ID with DU.</param>
            /// <param name="hasDuAutoLogin">Whether the user has auto-credentials for DU.</param>
            /// <param name="duUserId">The user ID to login to DU (if not auto-populated).</param>
            /// <param name="duPassword">The password to login to DU (if not auto-populated).</param>
            public DuCredentialModel(string duCasefileId, string duLenderInstitutionId, bool hasDuAutoLogin, string duUserId = null, string duPassword = null)
            {
                this.DuCasefileId = duCasefileId;
                this.DuLenderInstitutionId = duLenderInstitutionId;
                this.HasDuAutoLogin = hasDuAutoLogin;
                this.DuUserId = duUserId;
                this.DuPassword = duPassword;
            }

            /// <summary>
            /// Gets or sets the DU casefile ID for DU submission.
            /// </summary>
            /// <value>DU casefile ID.</value>
            public string DuCasefileId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the lender's institution ID with DU.
            /// </summary>
            /// <value>Lender institution ID.</value>
            public string DuLenderInstitutionId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets a value indicating whether the user has a DU Auto-login credential in LendingQB.
            /// If there is an auto-login, the login fields should not be displayed.
            /// </summary>
            /// <value>Whether the user has DU auto-login.</value>
            public bool HasDuAutoLogin
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the DU login name to fill in for the DU User ID.
            /// </summary>
            /// <value>The DU login name to use.</value>
            public string DuUserId
            {
                get;
                set;
            }

            /// <summary>
            /// Gets or sets the DU password to use for authenticating with DU.
            /// Should only come from UI submissions, not be sent to the browser.
            /// </summary>
            /// <value>The DU password.</value>
            public string DuPassword
            {
                get;
                set;
            }
        }
    }
}
