﻿namespace LendersOffice.ObjLib.DU.Seamless
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Common;
    using Conversions.Aus;
    using DataAccess;
    using Integration.Underwriting;
    using LendersOffice.DU;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Submits DU requests for Seamless DU and returns JSON page information to the UI.
    /// </summary>
    public static class DuRequestSubmission
    {
        /// <summary>
        /// A regex that specifies a pattern for lines to be removed from the log string before displaying to the user.
        /// </summary>
        private static Regex basicSensitiveInfoRegex = new Regex("(UserId:|InstId:|User:).*\\n", RegexOptions.IgnoreCase);

        /// <summary>
        /// Submit a DU Underwriting request to DU and returns the response. Will try 20(!!!) times to receive a 'Ready' response.
        /// </summary>
        /// <param name="userPrincipal">The user making the submission request.</param>
        /// <param name="loanId">The ID of the loan data object to save a successful response to.</param>
        /// <param name="request">The request to submit to DU.</param>
        /// <returns>The response if successful. Result with an exception otherwise.</returns>
        public static Result<FnmaXisDuUnderwriteResponse> SubmitWithTimeout(AbstractUserPrincipal userPrincipal, Guid loanId, AbstractFnmaXisRequest request)
        {
            for (int i = 0; i < 20; i++)
            {
                var result = Submit(userPrincipal, loanId, request);

                if (result.HasError)
                {
                    return result;
                }
                else
                {
                    var response = result.Value;
                    if (response.IsReady)
                    {
                        return result;
                    }
                    else
                    {
                        request.GloballyUniqueIdentifier = response.GloballyUniqueIdentifier;
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000);
                    }
                }
            }

            return Result<FnmaXisDuUnderwriteResponse>.Failure(new TimeoutException("Could not get DU response in timely manner."));
        }

        /// <summary>
        /// Submit a DU Underwriting request to DU and return JSON page information for the UI.
        /// </summary>
        /// <param name="userPrincipal">The user making the submission request.</param>
        /// <param name="loanId">The ID of the loan data object to save a successful response to.</param>
        /// <param name="request">The request to submit to DU.</param>
        /// <param name="reportType">The credit report type.</param>
        /// <returns>A JSON string to send to the DU Seamless Summary page.</returns>
        public static string SubmitAndCreateViewModel(AbstractUserPrincipal userPrincipal, Guid loanId, AbstractFnmaXisRequest request, CreditReportType reportType)
        {
            var responseViewModel = new DuResponseViewModel
            {
                Status = AusResponseStatus.Error,
                ErrorMessage = string.Empty,
                DuCaseId = string.Empty,
                DuResponseLog = null,
                DuResultsSummary = null,
                FannieId = null,
                AccessDenied = false,
                UnderwritingSuccess = false
            };

            var result = Submit(userPrincipal, loanId, request);
            if (result.HasError)
            {
                var error = result.Error;
                if (error is AccessDenied)
                {
                    responseViewModel.AccessDenied = true;
                }
                else if (error is CBaseException)
                {
                    var cbaseException = error as CBaseException;
                    responseViewModel.Status = AusResponseStatus.Error;
                    responseViewModel.ErrorMessage = cbaseException.UserMessage;
                    Tools.LogErrorWithCriticalTracking("Unable to submit to DU.", cbaseException);
                }
                else
                {
                    responseViewModel.Status = AusResponseStatus.Error;
                    responseViewModel.ErrorMessage = ErrorMessages.Generic;
                    Tools.LogErrorWithCriticalTracking("Unable to submit to DU.", error);
                }
            }
            else
            {
                var response = result.Value;
                if (response.IsReady)
                {
                    if (response.IsLoginError)
                    {
                        responseViewModel.Status = AusResponseStatus.LoginError;
                        responseViewModel.ErrorMessage = response.ErrorMessage;
                    }
                    else if (response.HasAnyError)
                    {
                        responseViewModel.Status = AusResponseStatus.Error;
                        responseViewModel.ErrorMessage = response.ErrorMessage ?? response.BusinessErrorMessage;
                        responseViewModel.DuResponseLog = RemoveSensitiveInfoFromLog(response.FnmaStatusLog, userPrincipal);
                    }
                    else
                    {
                        responseViewModel.Status = AusResponseStatus.Done;
                        responseViewModel.DuResultsSummary = new Dictionary<string, string>()
                        {
                            { "Case ID", response.MornetPlusCasefileIdentifier },
                            { "Results Date", Tools.GetDateTimeDescription(TimeZoneInfo.ConvertTime(response.UnderwritingResultsDateTime, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"), TimeZoneInfo.FindSystemTimeZoneById("Pacific Standard Time"))) },
                            { "Underwriting Recommendation", response.UnderwritingRecommendationDescription },
                            { "Credit Report ID", response.CreditReportId }
                        };
                        responseViewModel.UnderwritingSuccess = true;
                        responseViewModel.ImportAusFindings = userPrincipal.BrokerDB.ImportAusFindingsByDefault;
                        responseViewModel.ImportLiabilities = userPrincipal.BrokerDB.ImportAus1003ByDefault;
                        responseViewModel.DuResponseLog = RemoveSensitiveInfoFromLog(response.FnmaStatusLog, userPrincipal);

                        responseViewModel.HasCreditReport = response.HasCreditReport();
                        responseViewModel.ImportCreditReport = responseViewModel.HasCreditReport && (reportType == CreditReportType.Reissue || reportType == CreditReportType.OrderNew) && userPrincipal.BrokerDB.ImportAusCreditReportByDefault;
                    }
                }
                else
                {
                    responseViewModel.Status = AusResponseStatus.Processing;
                }

                responseViewModel.FannieId = response.GloballyUniqueIdentifier;
            }

            return SerializationHelper.JsonNetAnonymousSerialize(responseViewModel);
        }

        /// <summary>
        /// Submits the DU request.
        /// </summary>
        /// <param name="userPrincipal">The principal.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="request">The request.</param>
        /// <returns>The response if successful. Exception if not.</returns>
        private static Result<FnmaXisDuUnderwriteResponse> Submit(AbstractUserPrincipal userPrincipal, Guid loanId, AbstractFnmaXisRequest request)
        {
            try
            {
                FnmaXisDuUnderwriteResponse response = (FnmaXisDuUnderwriteResponse)DUServer.Submit(request);
                var result = ProcessResponse(response, userPrincipal, loanId, request);

                return result;
            }
            catch (AccessDenied exc)
            {
                return Result<FnmaXisDuUnderwriteResponse>.Failure(exc);
            }
            catch (CBaseException exc)
            {
                return Result<FnmaXisDuUnderwriteResponse>.Failure(exc);
            }
        }

        /// <summary>
        /// Does a bit of post processing to a successful response.
        /// </summary>
        /// <param name="response">The response.</param>
        /// <param name="userPrincipal">The user principal.</param>
        /// <param name="loanId">The loan id.</param>
        /// <param name="request">The request.</param>
        /// <returns>The response wrapped in a Result.</returns>
        private static Result<FnmaXisDuUnderwriteResponse> ProcessResponse(FnmaXisDuUnderwriteResponse response, AbstractUserPrincipal userPrincipal, Guid loanId, AbstractFnmaXisRequest request)
        {
            if (response.IsReady && !response.IsLoginError && !response.HasAnyError)
            {
                // Save most recent response to FileDB for retrieval later.
                response.SaveToFileDB(loanId);

                // Record AUS Run in AUS Ordering Dashboard
                if (!string.IsNullOrEmpty(response.MornetPlusCasefileIdentifier)
                    && !string.IsNullOrEmpty(response.UnderwritingRecommendationDescription))
                {
                    AusResultHandler.CreateSystemDuOrder(
                        request.sLId,
                        response.MornetPlusCasefileIdentifier,
                        response.UnderwritingRecommendationDescription,
                        userPrincipal);
                }
            }

            return Result<FnmaXisDuUnderwriteResponse>.Success(response);
        }

        /// <summary>
        /// Removes the potentially sensitive "UserID:" or "InstID:" message from the DU underwriting recommendation description message.
        /// </summary>
        /// <param name="log">The log content to remove sensitive information from.</param>
        /// <param name="user">The user who the sensitive info is being stripped for.</param>
        /// <returns>The string with sensitive info removed.</returns>
        private static string RemoveSensitiveInfoFromLog(string log, AbstractUserPrincipal user)
        {
            // Replace line endings to help viewing in notepad
            string processedLog = new Regex("(\\r)?\\n").Replace(log, Environment.NewLine);
            processedLog = basicSensitiveInfoRegex.Replace(processedLog, string.Empty);
            if (user != null && (user.HasDuAutoLogin || !string.IsNullOrEmpty(user.DuAutoCraLogin) || !string.IsNullOrEmpty(user.DuAutoCraPassword)))
            {
                var sensitiveStrings = new string[] { user.DuAutoLoginNm, user.DuAutoPassword, user.DuAutoCraLogin, user.DuAutoCraPassword }.Where(sensitiveString => !string.IsNullOrEmpty(sensitiveString));
                Regex autoLoginRegex = new Regex($"({string.Join("|", sensitiveStrings)})");
                processedLog = autoLoginRegex.Replace(processedLog, "****");
            }

            return processedLog;
        }
    }
}
