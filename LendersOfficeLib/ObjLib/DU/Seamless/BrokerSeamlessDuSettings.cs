﻿namespace LendersOffice.ObjLib.DU.Seamless
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Describes settings for Seamless DU as defined in a Broker's "EnableNewSeamlessDu" temp option.
    /// Retrieve this object with the <see cref="BrokerDB.GetSeamlessDuSettings()"/> method.
    /// </summary>
    public class BrokerSeamlessDuSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerSeamlessDuSettings"/> class. Describing the settings for a particular broker.
        /// </summary>
        /// <param name="brokerId">The Broker ID of the broker, for retrieving OC groups.</param>
        /// <param name="seamlessDuEnabled">Whether Seamless DU is enabled for the broker.</param>
        /// <param name="originatingCompanyFallbackGroup">The originating company group name for originating companies with the fallback functionality (both seamless and non-seamless).</param>
        /// <param name="employeeFallbackGroup">The employee group name with the fallback functionality (both seamless and non-seamless).</param>
        public BrokerSeamlessDuSettings(Guid brokerId, bool seamlessDuEnabled, string originatingCompanyFallbackGroup = null, string employeeFallbackGroup = null)
        {
            this.SeamlessDuEnabled = seamlessDuEnabled;
            this.EmployeeFallbackGroupName = employeeFallbackGroup;

            this.FallbackOriginatingCompanyIds = new HashSet<Guid>();

            var selectedPmlBrokerGroup = (from pmlBrokerGroup in GroupDB.GetAllGroups(brokerId, GroupType.PmlBroker)
                                          where string.Equals(pmlBrokerGroup.Name, originatingCompanyFallbackGroup, StringComparison.OrdinalIgnoreCase)
                                          select pmlBrokerGroup).FirstOrDefault();

            if (selectedPmlBrokerGroup != null)
            {
                this.FallbackOriginatingCompanyIds = new HashSet<Guid>(from pmlBrokerMember in GroupDB.GetPmlBrokersWithGroupMembership(brokerId, selectedPmlBrokerGroup.Id)
                                                                       where pmlBrokerMember.IsInGroup == 1
                                                                       select pmlBrokerMember.PmlBrokerId);
            }
            else
            {
                DataAccess.Tools.LogWarning($"[Broker Seamless DU Settings] Cannot find OC group '{originatingCompanyFallbackGroup}' for broker {brokerId}.");
            }
        }

        /// <summary>
        /// Gets a value indicating whether Seamless Du is enabled for the BrokerDB.
        /// </summary>
        /// <value>Whether Seamless DU is enabled.</value>
        public bool SeamlessDuEnabled
        {
            get; private set;
        }

        /// <summary>
        /// Gets or sets a list of originating company IDs for which the non-seamless 
        /// DU interface should remain active alongside the seamless one.
        /// </summary>
        private HashSet<Guid> FallbackOriginatingCompanyIds
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the name of the Employee Group for which the non-seamless
        /// DU interface should remain active alongside the seamless one.
        /// </summary>
        private string EmployeeFallbackGroupName
        {
            get; set;
        }

        /// <summary>
        /// Determines whether or not the fallback functionality of non-seamless DU is available for a given user.
        /// </summary>
        /// <param name="user">The user to check.</param>
        /// <returns>Whether fallback functionality is enabled.</returns>
        public bool IsDuFallbackEnabled(AbstractUserPrincipal user)
        {
            return (this.FallbackOriginatingCompanyIds != null && user.Type == "P" && user.PmlBrokerId != Guid.Empty && this.FallbackOriginatingCompanyIds.Contains(user.PmlBrokerId))
                || (!string.IsNullOrWhiteSpace(this.EmployeeFallbackGroupName) && user.IsInEmployeeGroup(this.EmployeeFallbackGroupName));
        }
    }
}
