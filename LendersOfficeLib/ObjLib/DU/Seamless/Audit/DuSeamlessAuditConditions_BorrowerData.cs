﻿namespace LendersOffice.ObjLib.DU.Seamless
{
    using System.Collections.Generic;
    using Conversions.Templates;
    using DataAccess;

    /// <summary>
    /// Defines a static class for retrieving predefined sets of DU audit conditions.
    /// This partial class implementation contains conditions for Borrower data auditing.
    /// </summary>
    public partial class DuSeamlessAuditConditions : ILoanAuditConditions
    {
        /// <summary>
        /// Common display message for missing 1003 page 3 questions a-l.
        /// </summary>
        private const string QuestionDisplayMessage = "This question requires an answer for DU Submission.";

        /// <summary>
        /// Gets the Borrower Data conditions needed for DU export.
        /// </summary>
        /// <returns>
        /// All of the conditions needed to validate a <see cref="CAppData"/> 
        /// for DU validation.
        /// </returns>
        /// <remarks>
        /// This data comes from the file "DU Seamless Data Audit table.xlsx" 
        ///   (https://mlintranet/pages/viewpage.action?pageId=7104538).
        /// The app data retrieval methods here assume that 
        ///   <see cref="CAppData.BorrowerModeT"/> has been set to Borrower or 
        ///   Coborrower appropriately for the audit.
        /// </remarks>
        public IEnumerable<IAuditCondition<CAppData>> GetBorrowerDataConditions()
        {
            return new List<DuSeamlessAuditSingleCondition<CAppData>>
            {
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03A-030",
                    dataPointName: "Applicant Social Security Number",
                    valueToDisplay: (app) => app.aSsn,
                    requiredIf: (app) => app.aTypeT == E_aTypeT.Individual || app.aTypeT == E_aTypeT.CoSigner,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBSsn)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03A-040",
                    dataPointName: "Applicant First Name",
                    valueToDisplay: (app) => app.aFirstNm,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBFirstNm)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03A-060",
                    dataPointName: "Applicant Last Name",
                    valueToDisplay: (app) => app.aLastNm,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBLastNm)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03A-120",
                    dataPointName: "No. of Dependents",
                    valueToDisplay: (app) => app.aDependNum_rep,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBDependNum)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03A-150",
                    dataPointName: "Date of Birth",
                    valueToDisplay: (app) => app.aDob_rep,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBDob)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03C-040",
                    dataPointName: "Residence Street Address",
                    valueToDisplay: (app) => app.aAddr,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBAddr)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03C-050",
                    dataPointName: "Residence City",
                    valueToDisplay: (app) => app.aCity,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBCity)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03C-060",
                    dataPointName: "Residence State",
                    valueToDisplay: (app) => app.aState,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBState)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03C-070",
                    dataPointName: "Residence Zip Code",
                    valueToDisplay: (app) => app.aZip,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBZip)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "03C-100",
                    dataPointName: "No. Yrs. At Residence",
                    valueToDisplay: (app) => app.aAddrYrs,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CAppData.aBAddrYrs)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-030",
                    dataPointName: "a. Are there any outstanding judgments against you?",
                    valueToDisplay: (app) => app.aDecJudgment,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecJudgment)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-040",
                    dataPointName: "b. Have you been declared bankrupt within the past 7 years?",
                    valueToDisplay: (app) => app.aDecBankrupt,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecBankrupt)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-050",
                    dataPointName: "c. Have you had property foreclosed upon or given title or deed in lieu thereof in the last 7 years?",
                    valueToDisplay: (app) => app.aDecForeclosure,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecForeclosure)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-060",
                    dataPointName: "d. Are you a party to a lawsuit?",
                    valueToDisplay: (app) => app.aDecLawsuit,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecLawsuit)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-070",
                    dataPointName: "e. Have you directly or indirectly been obligated on any loan…",
                    valueToDisplay: (app) => app.aDecObligated,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecObligated)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-080",
                    dataPointName: "f. Are you presently delinquent or in default on any Federal debt…",
                    valueToDisplay: (app) => app.aDecDelinquent,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecDelinquent)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-090",
                    dataPointName: "g. Are you obligated to pay alimony child support or separate maintenance?",
                    valueToDisplay: (app) => app.aDecAlimony,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecAlimony)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-100",
                    dataPointName: "h. Is any part of the down payment borrowed?",
                    valueToDisplay: (app) => app.aDecBorrowing,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecBorrowing)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-110",
                    dataPointName: "i. Are you a co-maker or endorser on a note?",
                    valueToDisplay: (app) => app.aDecEndorser,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecEndorser)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "#N/A",
                    dataPointName: "j. Are you a U.S. citizen?",
                    valueToDisplay: (app) => app.aDecCitizen,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecCitizen)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "#N/A",
                    dataPointName: "k. Are you a permanent resident alien?",
                    valueToDisplay: (app) => app.aDecResidency,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecResidency)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-130",
                    dataPointName: "l. Do you intend to occupy…",
                    valueToDisplay: (app) => app.aDecOcc,
                    displayMessage: QuestionDisplayMessage,
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecOcc)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-140",
                    dataPointName: "m. Have you had an ownership interest…",
                    valueToDisplay: (app) => app.aDecPastOwnership,
                    requiredIf: (app) => app.aDecOcc == "Y",
                    displayMessage: "This question requires an answer from borrowers intending to occupy the property",
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecPastOwnership)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-150",
                    dataPointName: "m. (1) What type of property…",
                    valueToDisplay: (app) => EnumUtilities.GetDescription(app.aDecPastOwnedPropT),
                    requiredIf: (app) => app.aDecOcc == "Y" && app.aDecPastOwnership == "Y",
                    displayMessage: "This question requires an answer from borrowers with ownership interests",
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecPastOwnedPropT)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "08A-160",
                    dataPointName: "m. (2) How did you hold title…",
                    valueToDisplay: (app) => EnumUtilities.GetDescription(app.aDecPastOwnedPropTitleT),
                    requiredIf: (app) => app.aDecOcc == "Y" && app.aDecPastOwnership == "Y",
                    displayMessage: "This question requires an answer from borrowers with ownership interests",
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aBDecPastOwnedPropTitleT)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "GOD-030",
                    dataPointName: "Total Tax",
                    valueToDisplay: (app) => app.BorrowerModeT == E_BorrowerModeT.Borrower ? app.aVaBTotITax_rep : app.aVaCTotITax_rep,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CAppData.aVaBTotITax)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "ADS-030",
                    dataPointName: "Other Hispanic or Latino Description",
                    valueToDisplay: (app) => app.aOtherHispanicOrLatinoDescription,
                    valueIsValid: (app) => !app.aOtherHispanicOrLatinoDescription.Contains(":"),
                    requiredIf: (app) => false,
                    displayMessage: "Other Hispanic or Latino Description cannot contain a colon (:) for DU submission. Please correct this value.",
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aOtherHispanicOrLatinoDescription)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "ADS-030",
                    dataPointName: "Other American Indian or Alaskan Native Description",
                    valueToDisplay: (app) => app.aOtherAmericanIndianDescription,
                    valueIsValid: (app) => !app.aOtherAmericanIndianDescription.Contains(":"),
                    requiredIf: (app) => false,
                    displayMessage: "Other American Indian or Alaskan Native Description cannot contain a colon (:) for DU submission. Please correct this value.",
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aOtherAmericanIndianDescription)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "ADS-030",
                    dataPointName: "Other Pacific Islander Description",
                    valueToDisplay: (app) => app.aOtherPacificIslanderDescription,
                    valueIsValid: (app) => !app.aOtherPacificIslanderDescription.Contains(":"),
                    requiredIf: (app) => false,
                    displayMessage: "Other Pacific Islander Description cannot contain a colon (:) for DU submission. Please correct this value.",
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aOtherPacificIslanderDescription)),
                new DuSeamlessAuditSingleCondition<CAppData>(
                    fannieFieldId: "ADS-030",
                    dataPointName: "Other Asian Description",
                    valueToDisplay: (app) => app.aOtherAsianDescription,
                    valueIsValid: (app) => !app.aOtherAsianDescription.Contains(":"),
                    requiredIf: (app) => false,
                    displayMessage: "Other Asian Description cannot contain a colon (:) for DU submission. Please correct this value.",
                    page: LoanEditorPage.Application1003Page3,
                    lqbFieldId: nameof(CAppData.aOtherAsianDescription)),
            };
        }
    }
}
