﻿namespace LendersOffice.ObjLib.DU.Seamless
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Conversions.Templates;
    using DataAccess;

    /// <summary>
    /// Defines a static class for retrieving predefined sets of audit conditions.
    /// This partial class implementation contains conditions for Loan data auditing.
    /// </summary>
    public partial class DuSeamlessAuditConditions : ILoanAuditConditions
    {
        /// <summary>
        /// Initializes static members of the <see cref="DuSeamlessAuditConditions"/> class.
        /// Initializes the singleton instance.
        /// </summary>
        static DuSeamlessAuditConditions()
        {
            Singleton = new DuSeamlessAuditConditions();
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="DuSeamlessAuditConditions"/> class from being created.
        /// </summary>
        private DuSeamlessAuditConditions()
        {
        }

        /// <summary>
        /// Gets the singleton instance of this class.
        /// </summary>
        /// <value>A singleton.</value>
        public static DuSeamlessAuditConditions Singleton { get; }

        /// <summary>
        /// Gets the Loan Data conditions needed for DU export.
        /// </summary>
        /// <returns>All of the conditions needed to validate a <see cref="CPageData"/> for DU validation.</returns>
        /// <remarks>This data comes from the file "DU Seamless Data Audit table.xlsx".</remarks>
        public IEnumerable<IAuditCondition<CPageData>> GetLoanDataConditions()
        {
            return new List<DuSeamlessAuditSingleCondition<CPageData>>
                {
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "01A-020",
                        dataPointName: "Mortgage Applied For",
                        valueToDisplay: (data) => EnumUtilities.GetDescription(data.sLT),
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sLT)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "01A-060",
                        dataPointName: "Loan Amount",
                        valueToDisplay: (data) => data.sLAmtCalc_rep,
                        valueIsValid: (data) => data.sLAmtCalc != 0m,
                        displayMessage: "$0.00 is not a valid Loan Amount",
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: "sLAmt"),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "01A-070",
                        dataPointName: "Interest Rate",
                        valueToDisplay: (data) => data.sNoteIR_rep,
                        valueIsValid: (data) => data.sNoteIR > 0,
                        displayMessage: "Interest Rate must be positive (non-zero).",
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sNoteIR)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "01A-080",
                        dataPointName: "Term (No. of Months)",
                        valueToDisplay: (data) => data.sTerm_rep,
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sTerm)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "01A-090",
                        dataPointName: "Amortization Type",
                        valueToDisplay: (data) => data.sFinMethodPrintAsOther ? "Other" : EnumUtilities.GetDescription(data.sFinMethT),
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(E_sFinMethT)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02A-030",
                        dataPointName: "Property City",
                        valueToDisplay: (data) => data.sSpCity,
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sSpCity)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02A-040",
                        dataPointName: "Property State",
                        valueToDisplay: (data) => data.sSpState,
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sSpState)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02A-050 ",
                        dataPointName: "Property Zip Code",
                        valueToDisplay: (data) => data.sSpZip,
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sSpZip)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02A-070",
                        dataPointName: "No. of Units",
                        valueToDisplay: (data) => data.sUnitsNum_rep,
                        valueIsValid: (data) => data.sUnitsNum > 0,
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sUnitsNum)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02B-030",
                        dataPointName: "Purpose of Loan",
                        valueToDisplay: (data) => EnumUtilities.GetDescription(data.sLPurposeT),
                        valueIsValid: (data) => data.sLPurposeT != E_sLPurposeT.Construct,
                        displayMessage: "\"Construction\" is not an eligible loan purpose selection. Construction-only loans are not supported.",
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sLPurposeT)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02B-030",
                        dataPointName: "Purpose of Loan",
                        valueToDisplay: (data) => EnumUtilities.GetDescription(data.sLPurposeT),
                        valueIsValid: (data) =>
                            !((data.sLT == E_sLT.FHA || data.sLT == E_sLT.VA)
                              && (data.sLPurposeT == E_sLPurposeT.ConstructPerm
                               || data.sLPurposeT == E_sLPurposeT.Other
                               || data.sLPurposeT == E_sLPurposeT.Construct)),
                        displayMessage: "\"Construction\", \"Construction Perm\", and \"Other\" are not eligible selections for FHA or VA loans.",
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sLPurposeT)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02B-050",
                        dataPointName: "Property will be",
                        valueToDisplay: (data) => EnumUtilities.GetDescription(data.GetAppData(0).aOccT),
                        valueIsValid: (data) =>
                            !((data.sLT == E_sLT.FHA || data.sLT == E_sLT.VA)
                             && (data.GetAppData(0).aOccT == E_aOccT.Investment
                              || data.GetAppData(0).aOccT == E_aOccT.SecondaryResidence)),
                        displayMessage: "\"Secondary Residence\" and \"Investment\" are not eligible selections for FHA or VA loans.",
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CAppData.aOccT)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02B-070",
                        dataPointName: "Estate will be held in",
                        valueToDisplay: (data) => EnumUtilities.GetDescription(data.sEstateHeldT),
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sEstateHeldT)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02D-070",
                        dataPointName: "Purpose of Refinance",
                        valueToDisplay: (data) => data.sRefPurpose,
                        requiredIf: (data) => data.sIsRefinancing && !data.sIsConstructionLoan,
                        valueIsValid: (data) => !(data.sLT == E_sLT.Conventional && data.sRefPurpose.Equals("no cash-out rate/term", System.StringComparison.OrdinalIgnoreCase)),
                        displayMessage: "\"No cash-out rate/term\" is not an elgible selection for Conventional loan files, use \"Limited Cash-Out Rate/Term\".",
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sRefPurpose)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02E-030",
                        dataPointName: "Down Payment Amount",
                        valueToDisplay: (data) => string.Join(", ", data.sDUDwnPmtSrc.Select(pmt => pmt.Amount_rep)),
                        requiredIf: (data) => data.sIsUseDUDwnPmtSrc,
                        page: LoanEditorPage.FannieAddendum,
                        lqbFieldId: "sDUDownPmtSrcs"),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02E-020",
                        dataPointName: "Down Payment Type Code",
                        valueToDisplay: (data) => string.Join(", ", InvalidDownPayments(data).Select(pmt => $"{pmt.Amount_rep}{(string.IsNullOrWhiteSpace(pmt.Source) ? string.Empty : $" ({pmt.Source})")}")),
                        requiredIf: (data) => false, // value to display is a list of invalid payments, so it will be empty when they're all valid
                        valueIsValid: (data) => data.sLT != E_sLT.FHA || !data.sIsUseDUDwnPmtSrc || !InvalidDownPayments(data).Any(),
                        displayMessage: "\"Gift funds\" and \"Fha - gift - source n/a\" are not eligible selections for FHA loans.",
                        page: LoanEditorPage.FannieAddendum,
                        lqbFieldId: "sDUDownPmtSrcs"),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02E-030",
                        dataPointName: "Down Payment Amount",
                        valueToDisplay: (data) => data.sEquityCalc_rep,
                        requiredIf: (data) => !data.sIsUseDUDwnPmtSrc,
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: "sEquityCalc"),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "02E-020",
                        dataPointName: "Down Payment Type Code",
                        valueToDisplay: (data) => data.sDwnPmtSrc,
                        requiredIf: (data) => !data.sIsUseDUDwnPmtSrc && (data.sLT == E_sLT.FHA && !data.sIsRefinancing),
                        valueIsValid: (data) => data.sLT != E_sLT.FHA
                                            || data.sIsUseDUDwnPmtSrc
                                            || !(data.sDwnPmtSrc.Equals("gift funds", StringComparison.OrdinalIgnoreCase)
                                              || data.sDwnPmtSrc.Equals("fha - gift - source n/a", StringComparison.OrdinalIgnoreCase)),
                        displayMessage: "\"Gift funds\" and \"Fha - gift - source n/a\" are not eligible selections for FHA loans.",
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sDwnPmtSrc)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        dataPointName: "Sales Concessions",
                        valueToDisplay: (data) => data.sFannieSalesConcessions_rep,
                        requiredIf: (data) => data.sLPurposeT == E_sLPurposeT.Purchase,
                        page: LoanEditorPage.FannieAddendum,
                        lqbFieldId: nameof(CPageData.sFannieSalesConcessions)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "07A-020",
                        dataPointName: "a. Purchase price",
                        valueToDisplay: (data) => data.sPurchPrice_rep,
                        requiredIf: (data) => data.sLPurposeT == E_sLPurposeT.Purchase,
                        valueIsValid: (data) => data.sLPurposeT != E_sLPurposeT.Purchase || data.sPurchPrice > 0,
                        page: LoanEditorPage.Application1003Page1,
                        lqbFieldId: nameof(CPageData.sPurchPrice)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "07A-050",
                        dataPointName: "d. Refinance (Inc. debts to be paid off)",
                        valueToDisplay: (data) => data.sRefPdOffAmt1003_rep,
                        requiredIf: (data) => data.sIsRefinancing,
                        page: LoanEditorPage.Application1003Page3,
                        lqbFieldId: nameof(CPageData.sRefPdOffAmt1003)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        dataPointName: "Proposed Housing Expenses",
                        valueToDisplay: (data) => data.sMonthlyPmt_rep,
                        valueIsValid: (data) => data.sMonthlyPmt != 0,
                        displayMessage: "$0.00 is not a valid Monthly Payment.",
                        page: LoanEditorPage.Application1003Page2,
                        lqbFieldId: nameof(CPageData.sMonthlyPmt)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "10B-030",
                        dataPointName: "Loan Originator’s Name",
                        valueToDisplay: (data) => data.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject)?.PreparerName,
                        page: LoanEditorPage.Application1003Page3,
                        lqbFieldId: "LoanOfficerName"),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "99B-040",
                        dataPointName: "Property Appraised Value",
                        valueToDisplay: (data) => data.sApprVal_rep,
                        page: LoanEditorPage.ThisLoanInfo,
                        lqbFieldId: nameof(CPageData.sApprVal)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "99B-050",
                        dataPointName: "Buydown Rate",
                        valueToDisplay: (data) => data.sIsLineOfCredit ? null : data.sAmortTable?.Items?[0]?.Rate_rep,
                        requiredIf: (data) => !data.sIsLineOfCredit && data.sBuydown,
                        page: LoanEditorPage.LoanTerms,
                        tpoPage: LoanEditorPage.NoPage,
                        lqbFieldId: nameof(CPageData.sBuydwnR1)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "ADS-030",
                        dataPointName: "Loan Originator ID",
                        valueToDisplay: (data) => data.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject).LoanOriginatorIdentifier,
                        page: LoanEditorPage.Application1003Page3,
                        lqbFieldId: "LoanOfficerLoanOriginatorIdentifier"),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "ADS-030",
                        dataPointName: "Loan Origination Company ID",
                        valueToDisplay: (data) => data.GetPreparerOfForm(E_PreparerFormT.App1003Interviewer, E_ReturnOptionIfNotExist.ReturnEmptyObject).CompanyLoanOriginatorIdentifier,
                        page: LoanEditorPage.Application1003Page3,
                        lqbFieldId: "LoanOfficerCompanyLoanOriginatorIdentifier"),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "ADS-030",
                        dataPointName: "Number of Financed Properties",
                        valueToDisplay: (data) => data.sNumFinancedProperties_rep,
                        valueIsValid: (data) => data.sNumFinancedProperties > 0,
                        page: LoanEditorPage.PmlSummary,
                        lqbFieldId: nameof(CPageData.sNumFinancedProperties)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                        fannieFieldId: "LNC-020",
                        dataPointName: "Lien Type Code",
                        valueToDisplay: (data) => EnumUtilities.GetDescription(data.sLienPosT),
                        page: LoanEditorPage.ThisLoanInfo,
                        lqbFieldId: nameof(CPageData.sLienPosT)),

                // For FHA loans, only Co-op is not allowed
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LNC-040",
                    dataPointName: "Subject Property Type Code",
                    valueToDisplay: (data) => EnumUtilities.GetDescription(data.sFannieSpT),
                    valueIsValid: (data) => !(data.sLT == E_sLT.FHA && data.sFannieSpT == E_sFannieSpT.CoOp),
                    displayMessage: "\"Co-Op\" is not a valid selection for FHA loans.",
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sFannieSpT)),

                // For VA loans, there are more disallowed property type codes, and a different display message.
                // requiredIf always returns false to prevent duplicate blank-field errors with the FHA condition (though that might not be possible with an enum field).
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LNC-040",
                    dataPointName: "Subject Property Type Code",
                    requiredIf: (data) => false, // Prevent duplicate messages
                    valueToDisplay: (data) => EnumUtilities.GetDescription(data.sFannieSpT),
                    valueIsValid: (data) =>
                        !(data.sLT == E_sLT.VA
                            && (data.sFannieSpT == E_sFannieSpT.CoOp
                            || data.sFannieSpT == E_sFannieSpT.HighRiseCondo
                            || data.sFannieSpT == E_sFannieSpT.DetachedCondo
                            || data.sFannieSpT == E_sFannieSpT.ManufacturedCondoPudCoop)),
                    displayMessage: "\"Co-Op\", \"High-Rise Condo\", \"Detached Condo\", and \"Manufactured home/condo/pud/coop\" are not valid selections for VA loans.",
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sFannieSpT)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LNC-100",
                    dataPointName: "Negative Amortization Limit Percent",
                    valueToDisplay: (data) => data.sPmtAdjMaxBalPc_rep,
                    requiredIf: (data) => data.sFinMethT == E_sFinMethT.ARM,
                    page: LoanEditorPage.LoanTerms,
                    tpoPage: LoanEditorPage.NoPage,
                    lqbFieldId: nameof(CPageData.sPmtAdjMaxBalPc)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LNC-110",
                    dataPointName: "Balloon Indicator",
                    valueToDisplay: (data) => data.sGfeIsBalloon_rep,
                    valueIsValid: (data) =>
                        !((data.sLT == E_sLT.FHA || data.sLT == E_sLT.VA || data.sLT == E_sLT.UsdaRural)
                            && data.sGfeIsBalloon),
                    displayMessage: "Balloons are not eligible for FHA or VA loans.",
                    page: /* GFE or 1008 as sBalloonPmt. Neither visible in PML. */ null,
                    tpoPage: LoanEditorPage.NoPage,
                    lqbFieldId: nameof(CPageData.sGfeIsBalloon)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "PCH-020",
                    dataPointName: "Mortgage Term",
                    valueToDisplay: (data) => data.sDue_rep,
                    requiredIf: (data) => data.sGfeIsBalloon,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CPageData.sDue)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "ARM-020",
                    dataPointName: "ARM Index Value",
                    valueToDisplay: (data) => data.sRAdjIndexR_rep,
                    requiredIf: (data) => data.sFinMethT == E_sFinMethT.ARM,
                    page: LoanEditorPage.LoanTerms,
                    lqbFieldId: nameof(CPageData.sRAdjIndexR)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "ARM-040",
                    dataPointName: "ARM Index Margin",
                    valueToDisplay: (data) => data.sRAdjMarginR_rep,
                    requiredIf: (data) => data.sFinMethT == E_sFinMethT.ARM,
                    page: LoanEditorPage.LoanTerms,
                    lqbFieldId: nameof(CPageData.sRAdjMarginR)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "ARM-050",
                    dataPointName: "ARM Qualifying Rate",
                    valueToDisplay: (data) => data.sQualIR_rep,
                    requiredIf: (data) => data.sFinMethT == E_sFinMethT.ARM,
                    page: LoanEditorPage.LoanTerms,
                    lqbFieldId: nameof(CPageData.sQualIR)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LEA-020",
                    dataPointName: "FHA Lender Identifier",
                    valueToDisplay: (data) => data.sFHALenderIdCode,
                    requiredIf: (data) => data.sLT == E_sLT.FHA && data.sFhaLenderIdT == E_sFhaLenderIdT.RegularFhaLender,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sFHALenderIdCode)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LEA-030",
                    dataPointName: "FHA Sponsor Identifier",
                    valueToDisplay: (data) => data.sFHASponsorAgentIdCode,
                    requiredIf: (data) => data.sLT == E_sLT.FHA && data.sFhaLenderIdT == E_sFhaLenderIdT.SponsoredOriginatorEIN,
                    page: LoanEditorPage.FannieAddendum,
                    tpoPage: LoanEditorPage.NoPage,
                    lqbFieldId: nameof(CPageData.sFHASponsorAgentIdCode)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LEA-040",
                    dataPointName: "Sponsored Originator EIN",
                    valueToDisplay: (data) => data.sSponsoredOriginatorEIN,
                    requiredIf: (data) => data.sLT == E_sLT.FHA && data.sFhaLenderIdT == E_sFhaLenderIdT.SponsoredOriginatorEIN,
                    page: LoanEditorPage.FannieAddendum,
                    tpoPage: LoanEditorPage.NoPage,
                    lqbFieldId: nameof(CPageData.sSponsoredOriginatorEIN)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "GOA-030",
                    dataPointName: "MCC",
                    valueToDisplay: (data) => data.sMortgageCreditCertificateTotal_rep,
                    requiredIf: (data) => data.sLT == E_sLT.FHA || data.sLT == E_sLT.VA,
                    page: LoanEditorPage.Application1003Page2,
                    lqbFieldId: "a_0"),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "GOA-040",
                    dataPointName: "Seller Concessions",
                    valueToDisplay: (data) => data.sLT == E_sLT.FHA ? data.sFHASellerContributionExceedingMaximum_rep : data.sFHASellerContribution_rep,
                    requiredIf: (data) => data.sLT == E_sLT.FHA || data.sLT == E_sLT.VA,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sFHASellerContribution)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "GOA-110",
                    dataPointName: "Type of Refinance",
                    valueToDisplay: GetRefinanceType,
                    requiredIf: (data) => data.sLT == E_sLT.FHA || data.sLT == E_sLT.VA,
                    page: LoanEditorPage.Application1003Page1,
                    lqbFieldId: nameof(CPageData.sRefPurpose)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "GOB-020",
                    dataPointName: "FHA Section of the Act Codes",
                    valueToDisplay: (data) => data.sFHAHousingActSection,
                    requiredIf: (data) => data.sLT == E_sLT.FHA,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sFHAHousingActSection)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "GOB-050",
                    dataPointName: "MIP Refund Amount",
                    valueToDisplay: (data) => data.sFHASalesConcessions_rep,
                    requiredIf: (data) => data.sLT == E_sLT.FHA,
                    page: /* LoanEditorPage.FhaCombinedRefi */ null,
                    tpoPage: LoanEditorPage.NoPage,
                    lqbFieldId: nameof(CPageData.sFHASalesConcessions)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "GOC-030",
                    dataPointName: "Entitlement Amount",
                    valueToDisplay: (data) => data.GetAppData(0).aVaEntitleAmt_rep,
                    requiredIf: (data) => data.sLT == E_sLT.VA,
                    page: LoanEditorPage.ThisLoanInfo,
                    tpoPage: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CAppData.aVaEntitleAmt)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "GOC-040",
                    dataPointName: "Monthly Maintenance",
                    valueToDisplay: (data) => data.sVaProMaintenancePmt_rep.TrimWhitespaceAndBOM(),
                    requiredIf: (data) => data.sLT == E_sLT.VA,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sVaProMaintenancePmt)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "GOC-050",
                    dataPointName: "Monthly Utilities",
                    valueToDisplay: (data) => data.sVaProUtilityPmt_rep.TrimWhitespaceAndBOM(),
                    requiredIf: (data) => data.sLT == E_sLT.VA,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sVaProUtilityPmt)),
                    new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LMD-030",
                    dataPointName: "Community Lending Product",
                    valueToDisplay: (data) => EnumUtilities.GetDescription(data.sFannieCommunityLendingT),
                    requiredIf: (data) => data.sIsCommunityLending,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sFannieCommunityLendingT)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LMD-035",
                    dataPointName: "Community Seconds Repayment Structure",
                    valueToDisplay: (data) => EnumUtilities.GetDescription(data.sFannieCommunitySecondsRepaymentStructureT),
                    requiredIf: (data) => data.sIsCommunityLending && data.sIsCommunitySecond,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sFannieCommunitySecondsRepaymentStructureT)),
                new DuSeamlessAuditSingleCondition<CPageData>(
                    fannieFieldId: "LMD-050",
                    dataPointName: "Community Seconds",
                    valueToDisplay: (data) => data.sIsCommunitySecond ? "Y" : "N",
                    requiredIf: (data) => data.sIsCommunityLending,
                    page: LoanEditorPage.FannieAddendum,
                    lqbFieldId: nameof(CPageData.sIsCommunitySecond))
            };
        }

        /// <summary>
        /// Gets the list of invalid DU Down Payment sources in <see cref="CPageData.sDUDwnPmtSrc"/>.
        /// </summary>
        /// <param name="data">The loan to get the down payment source list from.</param>
        /// <returns>A list of invalid (gift fund) down payment sources.</returns>
        private static IEnumerable<DownPaymentGift> InvalidDownPayments(CPageData data)
        {
            return data.sDUDwnPmtSrc.Where(src =>
                    src.Amount > 0
                 && (src.Source.Equals("gift funds", StringComparison.OrdinalIgnoreCase)
                 || src.Source.Equals("fha - gift - source n/a", StringComparison.OrdinalIgnoreCase)));
        }

        /// <summary>
        /// Determines the value to display for GOA-110 "Type of Refinance".
        /// </summary>
        /// <param name="data">The loan data to get refinance type from.</param>
        /// <returns>A string describing the type of refinance.</returns>
        private static string GetRefinanceType(CPageData data)
        {
            if (data.sLPurposeT == E_sLPurposeT.VaIrrrl)
            {
                return "Interest Rate Reduction Refinance Loan";
            }

            if (data.sFHAPurposeIsStreamlineRefi)
            {
                if (data.sFHAPurposeIsStreamlineRefiWithoutAppr)
                {
                    return "Streamline without Appraisal";
                }
                else
                {
                    return "Streamline Appraisal";
                }
            }

            if (data.sTotalScoreRefiT == E_sTotalScoreRefiT.FHAToFHANonStreamline)
            {
                return "Prior FHA";
            }
            else
            {
                return "Full Documentation";
            }
        }
    }
}
