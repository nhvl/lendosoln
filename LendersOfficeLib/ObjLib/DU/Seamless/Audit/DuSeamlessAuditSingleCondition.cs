﻿namespace LendersOffice.ObjLib.DU.Seamless
{
    using System;
    using Conversions.Templates;
    using DataAccess;

    /// <summary>
    /// A single condition to check as part of the DU Seamless data audit.
    /// </summary>
    /// <typeparam name="TData">
    /// Type of the data input for determining required and valid conditions.
    /// E.g. <see cref="CPageData"/>, <see cref="CAppData"/>.
    /// </typeparam>
    public class DuSeamlessAuditSingleCondition<TData> : AuditCondition<TData>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DuSeamlessAuditSingleCondition{TData}"/> class.
        /// </summary>
        /// <param name="dataPointName">The user-friendly name of the data point for which the condition is being checked.</param>
        /// <param name="valueToDisplay">
        /// A function which retrieves the value from a <typeparamref name="TData"/> instance, to display to the user in an error.</param>
        /// <param name="requiredIf">
        /// A function which uses a <typeparamref name="TData"/> to determine whether or not the <see cref="ValueToDisplay"/> is required to be non-empty.
        /// Defaults to always true (always required).
        /// </param>
        /// <param name="valueIsValid">
        /// A function which returns whether or not the field's value is valid, based on <typeparamref name="TData"/> data.
        /// Defaults to always true (any value valid).
        /// </param>
        /// <param name="displayMessage">
        /// The message to display if the value is invalid. 
        /// Note that a different message is shown for required fields that are missing.</param>
        /// <param name = "requiredMessage" >
        /// The message to display if the value is required but missing. 
        /// Note that a different message is shown for invalid fields.</param>
        /// <param name="fannieFieldId">
        /// The Fannie Mae 3.2 field ID for the field (for reference).
        /// </param>
        /// <param name="lqbFieldId">
        /// The LQB field ID to use for determining the element to highlight when clicking a link to correct an error.
        /// </param>
        /// <param name="page">
        /// The page information necessary for generating a URL to link for viewing and fixing any condition failure.
        /// </param>
        /// <param name="tpoPage">
        /// The page information for generating a URL to link in the TPO portal. Use this if the TPO portal page is different from the main loan editor.
        /// If the TPO portal doesn't have a field anywhere, use <see cref="LoanEditorPage.NoPage"/> to represent that fact.
        /// </param>
        public DuSeamlessAuditSingleCondition(
            string dataPointName,
            Func<TData, string> valueToDisplay,
            Func<TData, bool> requiredIf = null,
            Func<TData, bool> valueIsValid = null,
            string displayMessage = null,
            string requiredMessage = null,
            string fannieFieldId = null,
            string lqbFieldId = null,
            LoanEditorPage page = null,
            LoanEditorPage tpoPage = null) : base(dataPointName, valueToDisplay, requiredIf, valueIsValid, displayMessage, requiredMessage, lqbFieldId, page, tpoPage)
        {
            this.Fannie32FieldId = fannieFieldId;
        }

        /// <summary>
        /// Gets the FNMA 3.2 field ID for the field.
        /// </summary>
        /// <value>FNMA 3.2 field ID.</value>
        public string Fannie32FieldId
        {
            get; private set;
        }

        /// <summary>
        /// Gets an optional name for the integration to display in errors.
        /// </summary>
        /// <value>Integration name.</value>
        protected override string IntegrationName
        {
            get
            {
                return "DU";
            }
        }
    }
}