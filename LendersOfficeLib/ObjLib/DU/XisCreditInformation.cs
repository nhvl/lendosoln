/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisCreditInformation : AbstractXisXmlNode
	{
        public XisCreditInformation(string ssn, string creditReportIdentifier, E_XisCreditInformationRequestType creditRequestType)
        {
            this.Ssn = ssn;
            this.CreditReportIdentifier = creditReportIdentifier;
            this.CreditRequestType = creditRequestType;
        }
        protected override string ElementName 
        {
            get { return "CREDIT_INFORMATION"; }
        }

        public string Ssn { get; set;}
        public string CreditReportIdentifier { get; set;}
        public E_XisCreditInformationRequestType CreditRequestType { get; set;}

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "SSN", Ssn);
            WriteAttribute(writer, "CreditReportIdentifier", CreditReportIdentifier);
            WriteAttribute(writer, "CreditRequestType", CreditRequestType.ToString("D"));
        }
	}
    public enum E_XisCreditInformationRequestType 
    {
        Individual = 0,
        Joint = 1
    }
}
