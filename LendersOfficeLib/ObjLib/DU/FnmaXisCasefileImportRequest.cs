/// Author: David Dao

using System;
using System.Xml;
using LendersOffice.Constants;

namespace LendersOffice.DU
{
	public class FnmaXisCasefileImportRequest : AbstractDuXisRequest
	{
		public FnmaXisCasefileImportRequest(Guid sLId) : base(sLId)
		{
		}

        public override string FnmaProductName 
        {
            get { return "Casefile"; }
        }
        public override string FnmaProductFunctionName 
        {
            get { return "Import"; }
        }
        public override string FnmaProductVersionNumber 
        {
            get { return "2.0"; }
        }

        private string m_mornetPlusCasefileIdentifier;
        private string m_lenderInstitutionIdentifier;
        private string m_lenderCaseIdentifier;

        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        public string LenderInstitutionIdentifier 
        {
            get { return m_lenderInstitutionIdentifier; }
            set { m_lenderInstitutionIdentifier = value; }
        }
        public string LenderCaseIdentifier 
        {
            get { return m_lenderCaseIdentifier; }
            set { m_lenderCaseIdentifier = value; }
        }
        
        protected override void CreateControlInput(XmlWriter writer) 
        {
            XisCasefileImportRequest casefileImportRequest = new XisCasefileImportRequest();
            casefileImportRequest.LenderCaseIdentifier = m_lenderCaseIdentifier;
            casefileImportRequest.LenderInstitutionIdentifier = m_lenderInstitutionIdentifier;
            casefileImportRequest.MornetPlusCasefileIdentifier = m_mornetPlusCasefileIdentifier;

            casefileImportRequest.GenerateXml(writer);
        }
        public override AbstractFnmaXisResponse CreateResponse(string rawData) 
        {
            return new FnmaXisCasefileImportResponse(rawData);
        }
	}
}
