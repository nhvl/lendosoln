/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisDuUnderwriteRequest : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "DU_UNDERWRITE_REQUEST"; }
        }

        private XisCredit m_credit;
        private XisFannieMaeUnderwritingEngine m_fannieMaeUnderwritingEngine = new XisFannieMaeUnderwritingEngine();
        private List<XisDuReturnFile> m_returnFileList = new List<XisDuReturnFile>();
        private XisSoftwareProvider m_softwareProvider = new XisSoftwareProvider();
        private XisDataFile m_dataFile = new XisDataFile();

        public XisCredit Credit 
        {
            get { return m_credit; }
            set { m_credit = value; }
        }
        public void Add(E_XisDuReturnFileType returnFileType)
        {
            m_returnFileList.Add(new XisDuReturnFile(returnFileType));
        }
        public XisSoftwareProvider SoftwareProvider 
        {
            get { return m_softwareProvider; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteElement(writer, m_credit);
            WriteElement(writer, m_fannieMaeUnderwritingEngine);
            WriteElement(writer, m_returnFileList);
            WriteElement(writer,m_softwareProvider);
            WriteElement(writer, m_dataFile);
        }
	}
}
