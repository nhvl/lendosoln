﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.Constants;
using LendersOffice.Audit;
using System.Xml.Linq;
using System.Xml.XPath;

namespace LendersOffice.DU
{
    public class FnmaXisEarlyCheckResponse : AbstractFnmaXisResponse
    {
        private bool m_hasBusinessError = false;
        private string m_businessErrorMessage = null;
        private string m_mornetPlusCasefileIdentifier = null;


        public override bool HasBusinessError
        {
            get { return m_hasBusinessError; }
        }

        public override string BusinessErrorMessage
        {
            get { return m_businessErrorMessage; }
        }

        public override string MornetPlusCasefileIdentifier
        {
            get { return m_mornetPlusCasefileIdentifier; }
        }

        protected override string FileDbKeyPrefix
        {
            get { return "FNMA_EARLYCHECK"; }
        }

        //public string EarlyCheckResultsXml
        //{
        //    get { return GetFannieMaePart("EARLYCHECKRESULTS_XML"); }
        //}
        //public string EarlyCheckResultsHtml
        //{
        //    get { return GetFannieMaePart("EARLYCHECKRESULTS_HTML"); }
        //}
        public string EarlyCheckLoanLevelResultsHtml
        {
            get { return GetFannieMaePart("EARLYCHECKLOANLEVELRESULTS_HTML"); }
        }
        public string EarlyCheckLoanLevelResultsXml
        {
            get { return GetFannieMaePart("EARLYCHECKLOANLEVELRESULTS_XML"); }
        }
        public string EarlyCheckSummaryXml
        {
            get { return GetFannieMaePart("EARLYCHECKSUMMARY_XML"); }
        }
        public string EarlyCheckSummaryHtml
        {
            get { return GetFannieMaePart("EARLYCHECKSUMMARY_HTML"); }
        }
        public FnmaXisEarlyCheckResponse(Guid sLId)
            : base(sLId)
        {

        }
        public FnmaXisEarlyCheckResponse(string rawData)
            : base(rawData)
        {
        }

        private List<string> m_validationIdList = null;
        public IEnumerable<string> ValidationIdList
        {
            get
            {
                if (m_validationIdList == null)
                {
                    m_validationIdList = new List<string>() ;

                    string xml = EarlyCheckSummaryXml;
                    if (string.IsNullOrEmpty(xml) == false)
                    {
                        Tools.LogInfo(xml);
                        XDocument xdoc = XDocument.Parse(xml);
                        foreach (XElement el in xdoc.XPathSelectElements("//EarlyCheckSummaryResults/LoanLevelEdits/Casefile"))
                        {
                            Tools.LogInfo(el.ToString());
                            XElement validationElement = el.Element("ValidationIdentifier");
                            if (null != validationElement)
                            {
                                m_validationIdList.Add(validationElement.Value);
                            }
                        }
                    }
                }
                return m_validationIdList;
            }
        }

        protected override void ParseControlOutput(XmlDocument doc)
        {
            XmlElement responseEl = (XmlElement)doc.SelectSingleNode("//EARLYCHECK_CHECK_RESPONSE");

            XmlElement statusEl = (XmlElement)responseEl.SelectSingleNode("//STATUS");
            if (null != statusEl)
            {
                string condition = statusEl.GetAttribute("_Condition");
                if (condition == "FAILURE")
                {
                    m_hasBusinessError = true;
                    m_businessErrorMessage = statusEl.GetAttribute("_Description");
                }
            }
            else
            {
                m_hasBusinessError = true;
                m_businessErrorMessage = ""; // TODO: What should the generic message be? Is this scenario even possible.
            }
        }

        public void ImportToLoanFile(Guid sLId, AbstractUserPrincipal principal)
        {
            if (this.IsReady == false)
            {
                return;
            }
            this.SaveToFileDB(sLId);
            if (this.HasAnyError == false)
            {
                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(FnmaXisEarlyCheckResponse));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.SaveFannieMaeEarlyCheckResultsHtmlToEdocs();
                dataLoan.sFnmaEarlyCheckFirstRunD = CDateTime.Create(DateTime.Now);
                dataLoan.Save();
            }

            AbstractAuditItem audit = new FannieMaeEarlyCheckAuditItem(principal);
            AuditManager.RecordAudit(sLId, audit);
        }

    }
}
