﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace LendersOffice.DU
{
    public class XisEarlyCheckCheckRequest : AbstractXisXmlNode
    {
        protected override string ElementName
        {
            get { return "EARLYCHECK_CHECK_REQUEST"; }
        }

        private XisSoftwareProvider m_softwareProvider = new XisSoftwareProvider();
        private XisDataFile m_dataFile = new XisDataFile();
        private List<XisEarlyCheckReturnFile> m_returnFileList = new List<XisEarlyCheckReturnFile>();

        public XisEarlyCheckCheckRequest()
        {
            m_dataFile.Type = "1003";
            m_dataFile.Name = "MORNETPLUS_LOAN";
            m_dataFile.VersionNumber = "3.2";
        }

        public string MornetPlusCasefileIdentifier { get; set; }
        public string LenderInstitutionIdentifier { get; set; }
        public string LenderCaseIdentifier { get; set; }

        public void Add(E_XisEarlyCheckReturnFileType fileType, E_XisEarlyCheckReturnFileFormatType formatType)
        {
            XisEarlyCheckReturnFile returnFile = new XisEarlyCheckReturnFile();
            returnFile.Type = fileType;
            returnFile.FormatType = formatType;
            m_returnFileList.Add(returnFile);

        }
        public XisSoftwareProvider SoftwareProvider
        {
            get { return m_softwareProvider; }
        }
        public XisDataFile DataFile
        {
            get { return m_dataFile; }
        }

        protected override void GenerateXmlContent(XmlWriter writer)
        {
            WriteAttribute(writer, "MORNETPlusCasefileIdentifier", MornetPlusCasefileIdentifier);
            WriteAttribute(writer, "LenderInstitutionIdentifier", LenderInstitutionIdentifier);
            WriteAttribute(writer, "LenderCaseIdentifier", LenderCaseIdentifier);
            WriteElement(writer, m_softwareProvider);
            WriteElement(writer, m_dataFile);
            WriteElement(writer, m_returnFileList);
        }
    }
}
