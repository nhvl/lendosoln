/// Author: David Dao

using System;
using System.Xml;
using DataAccess;
using LendersOffice.Constants;

namespace LendersOffice.DU
{

    public class FnmaDwebCasefileStatusResponse : AbstractFnmaXisResponse
    {
        private bool m_hasBusinessError = false;
        private string m_businessErrorMessage = null;
        private string m_mornetPlusCasefileIdentifier = null;
        private string m_underwritingRecommendationDescription = "";
        private string m_productName = "";

        private string m_lastUnderwritingDate = "";
        private string m_underwritingSubmissionType = "";
        private string m_desktopOriginatorSubmissionStatus = "";
        private string m_updateDate = "";
        private string m_allCreditStatusDescription = "";

        private string m_underwritingStatusDescription = "";

        private string m_originatorInstitutionIdentifier = "";
        private string m_originatorInstitutionName = "";
        private string m_lenderInstitutionIdentifier = "";
        private string m_lenderInstitutionName = "";

        public string OriginatorInstitutionIdentifier 
        {
            get { return m_originatorInstitutionIdentifier; }
        }
        public string OriginatorInstitutionName
        {
            get { return m_originatorInstitutionName; }
        }

        public override string LenderInstitutionIdentifier 
        {
            get { return m_lenderInstitutionIdentifier; }
        }
        public string LenderInstitutionName
        {
            get { return m_lenderInstitutionName; }
        }
        public override string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
        }
        protected override string FileDbKeyPrefix 
        {
            get { return "FNMA_DWEB_CASEFILESTATUS"; }
        }
        public override bool HasBusinessError 
        {
            get { return m_hasBusinessError; }
        }
        public override string BusinessErrorMessage 
        {
            get { return m_businessErrorMessage; }
        }
        public override string UnderwritingRecommendationDescription 
        {
            get { return m_underwritingRecommendationDescription; }
        }
        public string ProductName 
        {
            get { return m_productName; }
        }
        public string LastUnderwritingDate 
        {
            get { return m_lastUnderwritingDate; }
        }

        public string UnderwritingSubmissionType 
        {
            get { return m_underwritingSubmissionType; }
        }
        public string DesktopOriginatorSubmissionStatus 
        {
            get { return m_desktopOriginatorSubmissionStatus; }
        }
        public string UpdateDate 
        {
            get { return m_updateDate; }
        }
        public string AllCreditStatusDescription 
        {
            get { return m_allCreditStatusDescription; }
        }
        public string UnderwritingStatusDescription 
        {
            get { return m_underwritingStatusDescription; }
        }

        public FnmaDwebCasefileStatusResponse(Guid sLId) : base(sLId) 
        {
        }
        public FnmaDwebCasefileStatusResponse(string rawData) : base(rawData)
        {
        }
        protected override void ParseControlOutput(XmlDocument doc) 
        {
            XmlElement responseEl = (XmlElement) doc.SelectSingleNode("DWEB_CASEFILE_STATUS_RESPONSE");

            m_mornetPlusCasefileIdentifier = responseEl.GetAttribute("MORNETPlusCasefileIdentifier");


            XmlNodeList statusList = responseEl.SelectNodes("STATUS");

            if (statusList.Count > 0) 
            {
                m_businessErrorMessage = "";
                foreach (XmlElement statusEl in statusList) 
                {
                    // 10/10/2007 dd - DU could return multiple error messages.
                    string condition = statusEl.GetAttribute("_Condition").ToUpper();
                    if (condition != "COMPLETE") 
                    {
                        m_hasBusinessError = true;
                        m_businessErrorMessage += statusEl.GetAttribute("_Description") + Environment.NewLine + Environment.NewLine;
                    } 
                }
            } 
            else 
            {
                m_hasBusinessError = false;
                m_businessErrorMessage = "";
            }

            XmlElement casefileSummaryEl = (XmlElement) responseEl.SelectSingleNode("CASEFILE_SUMMARY");
            if (null != casefileSummaryEl) 
            {
                m_underwritingRecommendationDescription = casefileSummaryEl.GetAttribute("UnderwritingRecommendationDescription");
                m_productName = casefileSummaryEl.GetAttribute("ProductName");
                m_lastUnderwritingDate = casefileSummaryEl.GetAttribute("LastUnderwritingDate");
                m_underwritingStatusDescription = casefileSummaryEl.GetAttribute("UnderwritingStatusDescription");
                m_underwritingSubmissionType = casefileSummaryEl.GetAttribute("UnderwritingSubmissionType");
                m_desktopOriginatorSubmissionStatus = casefileSummaryEl.GetAttribute("DesktopOriginatorSubmissionStatus");

                XmlElement casefileEl = (XmlElement) casefileSummaryEl.SelectSingleNode("CASEFILE");
                if (null != casefileEl) 
                {
                    m_updateDate = casefileEl.GetAttribute("_UpdateDate");
                    m_allCreditStatusDescription = casefileEl.GetAttribute("_AllCreditStatusDescription");
                }
            }

            XmlElement lenderCasefileInstitutionEl = (XmlElement) responseEl.SelectSingleNode("CASEFILE_INSTITUTION[@InstitutionType='Lender']");
            if (null != lenderCasefileInstitutionEl) 
            {
                m_lenderInstitutionIdentifier = lenderCasefileInstitutionEl.GetAttribute("InstitutionIdentifier");
                m_lenderInstitutionName = lenderCasefileInstitutionEl.GetAttribute("InstitutionName");
            }
            XmlElement originatorCasefileInstitutionEl = (XmlElement) responseEl.SelectSingleNode("CASEFILE_INSTITUTION[@InstitutionType='Originator']");
            if (null != originatorCasefileInstitutionEl) 
            {
                m_originatorInstitutionIdentifier = originatorCasefileInstitutionEl.GetAttribute("InstitutionIdentifier");
                m_originatorInstitutionName = originatorCasefileInstitutionEl.GetAttribute("InstitutionName");
            }
        }

        public void SaveToLoanFile(Guid sLId) 
        {
            this.SaveToFileDB(sLId);

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(FnmaDwebCasefileStatusResponse));
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            dataLoan.sAusRecommendation = this.UnderwritingRecommendationDescription;
            // 10/18/2007 dd - Temporary hack to set DU Recommendation to sProd3rdPartyUwResultT.

            E_sProd3rdPartyUwResultT result = this.DuResult;
            Tools.LogInfo("Setting the result " + this.DuResult);
            if (result != E_sProd3rdPartyUwResultT.NA)
            {
                dataLoan.sProd3rdPartyUwResultT = result;
            }

            dataLoan.sAusFindingsPull = true;
            dataLoan.Save();


        }
    }
}
