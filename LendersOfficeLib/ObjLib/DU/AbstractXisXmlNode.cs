/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace LendersOffice.DU
{
	public abstract class AbstractXisXmlNode
	{
        protected abstract string ElementName 
        {
            get;
        }
        public void GenerateXml(XmlWriter writer) 
        {
            writer.WriteStartElement(ElementName);
            GenerateXmlContent(writer);
            writer.WriteEndElement();
        }

        protected abstract void GenerateXmlContent(XmlWriter writer);

        protected void WriteAttribute(XmlWriter writer, string attr, string value) 
        {
            if (null != value && "" != value)
                writer.WriteAttributeString(attr, value);
        }
        protected void WriteAttribute(XmlWriter writer, string attr, Enum value)
        {
            writer.WriteAttributeString(attr, value.ToString());
        }
        protected void WriteElement(XmlWriter writer, string elName, string value) 
        {
            if (null != value && "" != value)
                writer.WriteElementString(elName, value);
        }
        protected void WriteElement(XmlWriter writer, AbstractXisXmlNode node) 
        {
            if (null != node)
                node.GenerateXml(writer);
        }
        protected void WriteElement<T>(XmlWriter writer, List<T> list) where T : AbstractXisXmlNode
        {
            if (list != null)
            {
                foreach (AbstractXisXmlNode node in list)
                {
                    WriteElement(writer, node);
                }
            }
        }

	}
}
