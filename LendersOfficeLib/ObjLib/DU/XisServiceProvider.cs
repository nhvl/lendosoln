/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisServiceProvider : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "SERVICE_PROVIDER"; }
        }

        public string Name { get; set;}
        public string AccountNumber { get; set;}
        public string Password { get; set;}

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "_Name", Name);
            WriteAttribute(writer, "_AccountNumber", AccountNumber);
            WriteAttribute(writer, "_Password", Password);
        }
	}
}
