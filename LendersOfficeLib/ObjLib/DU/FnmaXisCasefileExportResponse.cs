/// Author: David Dao

using System;
using System.Collections;
using System.Xml;
using LendersOffice.Common;
using LendersOffice.CreditReport.FannieMae;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.CreditReport;
using LendersOffice.Constants;
using LendersOffice.Reminders;
using System.Text;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.Conversions;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Integration.TotalScorecard;
using LendersOffice.ObjLib.Task;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace LendersOffice.DU
{
	public class FnmaXisCasefileExportResponse : AbstractFnmaXisResponse
	{
        private bool m_parseHtml = false;
        private bool m_hasBusinessError = false;
        private string m_businessErrorMessage = null;
        private string m_mornetPlusCasefileIdentifier = null;
        private FannieMaeImportSource m_fannieMaeImportSource = FannieMaeImportSource.LendersOffice;

        public FannieMaeImportSource FannieMaeImportSource
        {
            get { return m_fannieMaeImportSource; }
            set { m_fannieMaeImportSource = value; }
        }

        protected override string FileDbKeyPrefix 
        {
            get { return "FNMA_CASEFILE_EXPORT"; }
        }
        public override string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
        }
        public override bool HasBusinessError 
        {
            get { return m_hasBusinessError; }
        }
        public override string BusinessErrorMessage 
        {
            get { return m_businessErrorMessage; }
        }
        public FnmaXisCasefileExportResponse(Guid sLId) : base(sLId) 
        {
        }
		public FnmaXisCasefileExportResponse(string rawData) : base(rawData)
		{
		}


        protected override void ParseControlOutput(XmlDocument doc) 
        {
            XmlElement responseEl = (XmlElement) doc.SelectSingleNode("//CASEFILE_EXPORT_RESPONSE");
            m_mornetPlusCasefileIdentifier = responseEl.GetAttribute("MORNETPlusCasefileIdentifier");

            XmlElement statusEl = (XmlElement) responseEl.SelectSingleNode("//STATUS");
            if (null != statusEl) 
            {
                string condition = statusEl.GetAttribute("_Condition");
                if (condition == "FAILURE") 
                {
                    m_hasBusinessError = true;
                    m_businessErrorMessage = statusEl.GetAttribute("_Description");
                } 
            } 
            else 
            {
                m_hasBusinessError = true;
                m_businessErrorMessage = ""; // TODO: What should the generic message be? Is this scenario even possible.
            }
        }

        public bool ParseHtmlAsFallBack
        {
            get { return m_parseHtml; }
            set { m_parseHtml = value; }
        }

        public string Fnma32Content 
        {
            get { return GetFannieMaePart("1-1"); }
        }
        public string FnmaFindingsHtml 
        {
            get { return GetFannieMaePart("4-3"); }
        }
        public string FnmaPreliminaryFindingsHtml 
        {
            get { return GetFannieMaePart("29-3"); }
        }
        public string FnmaFHAFindings 
        {
            get { return GetFannieMaePart("15-3"); }
        }
        public string FnmaVAFindings 
        {
            get { return GetFannieMaePart("14-3"); }
        }
        public string FnmaFindingsXml 
        {
            get { return GetFannieMaePart("11-5"); }
        }

        public string FnmaStatusXml
        {
            get { return GetFannieMaePart("STATUS_FILE"); }
        }

        private bool HasFindings
        {
            get { return !string.IsNullOrEmpty(FnmaFindingsXml); }
        }

        public override string UnderwritingRecommendationDescription
        {
            get
            {
                string caseFileStatusXml = FnmaStatusXml;

                //Tools.LogInfo("Antonio :  P: " + (m_parseHtml && string.IsNullOrEmpty(FnmaVAFindings) == false));
                //Tools.LogInfo("Antonio :  ResV: " + FnmaVAFindings);
                //Tools.LogInfo("Antonio :  ResF: " + FnmaFHAFindings);
                //Tools.LogInfo("Antonio :  ResH: " + FnmaFindingsHtml);
                //Tools.LogInfo("Antonio :  ResFh: " + FnmaPreliminaryFindingsHtml);
                
                string recommendation = string.Empty;
                string sFindingsRec = string.Empty;
                if (string.IsNullOrEmpty(caseFileStatusXml) == false)
                {
                    // 12/3/2012 dd - Try to look for finding information from the CASEFILE_STATUS element first.
                    XmlDocument doc = Tools.CreateXmlDoc(caseFileStatusXml);

                    XmlNode node = doc.SelectSingleNode("/CASEFILE_STATUS/DECISION_INFORMATION");
                    if (node != null)
                    {
                        XmlAttribute attr = node.Attributes["DesktopUnderwriterRecommendationCode"];
                        if (attr != null)
                        {
                            recommendation = ConvertToDesktopUnderwriterRecommendationCode(attr.Value);
                        }
                    }

                    // OPM 118449 - If the casefile status recommendation is 5/unknown then double-check the findings for the actual recommendation
                    if (recommendation.Equals("Unknown", StringComparison.OrdinalIgnoreCase) && HasFindings)
                    {
                        sFindingsRec = RetrieveRecommendationFromFindings();
                        if (!string.IsNullOrEmpty(sFindingsRec))
                        {
                            recommendation = sFindingsRec;
                        }
                    }
                }
                else if (HasFindings)
                {
                    sFindingsRec = RetrieveRecommendationFromFindings();
                    if (!string.IsNullOrEmpty(sFindingsRec))
                    {
                        recommendation = sFindingsRec;
                    }
                }
                //else if (string.IsNullOrEmpty(caseFileStatusXml) == false)
                //{
                //    XmlDocument doc = Tools.CreateXmlDoc(caseFileStatusXml);

                //    XmlNode node = doc.SelectSingleNode("/CASEFILE_STATUS/DECISION_INFORMATION");
                //    if (node != null)
                //    {
                //        recommendation = ConvertToDesktopUnderwriterRecommendationCode(node.Attributes["DesktopUnderwriterRecommendationCode"].Value);
                //    }
                //}
                else if (m_parseHtml && string.IsNullOrEmpty(FnmaVAFindings) == false)
                {
                    Regex searchStringRegex = new Regex("<b>The recommendation for this case is:(.*?)</b>");
                    Match m = searchStringRegex.Match(FnmaVAFindings);

                    if (m.Success && m.Groups.Count >= 1)
                    {
                        recommendation = m.Groups[1].Value.TrimWhitespaceAndBOM();
                    }
                    else
                    {
                        Tools.LogErrorWithCriticalTracking(FnmaVAFindings);
                    }
                }

                return recommendation;
            }
        }
        private string ConvertToDesktopUnderwriterRecommendationCode(string code)
        {
            switch (code) 
            {
                case "1":
                    return "Refer/Eligible";
                case "2":
                    return "Out of Scope";
                case "3":
                    return "Approve/Eligible";
                case "4":
                    return "Refer/Ineligible";
                case "5":
                    return "Unknown";
                case "6":
                    return "Approve/Ineligible";
                case "7":
                    return "Refer With Caution";
                case "10":
                    return "Error";
                case "18":
                    return "Refer W Caution/IV";
                case "24":
                    return "EA-I/Eligible";
                case "25":
                    return "EA-I/Ineligible";
                case "26":
                    return "EA-II/Eligible";
                case "27":
                    return "EA-II/Ineligible";
                case "28":
                    return "EA-III/Eligible";
                case "29":
                    return "EA-III/Ineligible";
                default:
                    return string.Empty;

            }
        }

        private string RetrieveRecommendationFromFindings()
        {
            string sRecommendation = string.Empty;
            XmlDocument doc = Tools.CreateXmlDoc(FnmaFindingsXml);
            XmlNode node = doc.SelectSingleNode("/CodifiedFindings/RecommendationCode");

            if (node != null)
            {
                XmlAttribute attr = node.Attributes["UnderwritingRecommendationDescription"];
                if (attr != null)
                {
                    sRecommendation = attr.Value;
                }
            }
            return sRecommendation;
        }

        private Hashtable m_creditReportHash = new Hashtable();
        private bool m_isCreditReportHashInitialized = false;

        private void InitializeCreditReportHash() 
        {
            if (null != FnmaPartList) 
            {
                Hashtable printFileHash = new Hashtable(2);
                Hashtable mismoFileHash = new Hashtable(2);
                foreach (MultiPartItem item in FnmaPartList) 
                {
                    if (item.FileName.StartsWith("CREDITREPORT_PRINTFILE_")) 
                    {
                        string ssn = item.FileName.Substring(23);
                        printFileHash[ssn] = string.Format("<html><body><pre style='font-family:courier new'>{0}</pre></body></html>", item.Content.Replace((char) 12, ' '));
                    } 
                    else if (item.FileName.StartsWith("CREDITREPORT_MISMOFILE_")) 
                    {
                        string ssn = item.FileName.Substring(23);
                        mismoFileHash[ssn] = item.Content;
                    }
                }
                foreach (string key in mismoFileHash.Keys) 
                {
                    string printFileContent = (string) printFileHash[key];
                    string mismoFileContent = (string) mismoFileHash[key];
                    XmlDocument doc = FannieMae_CreditReportRequest.MergeFannieMaeCreditReport(mismoFileContent, printFileContent);
                    m_creditReportHash[key] = doc;
                    

                }
            }
            m_isCreditReportHashInitialized = true;

        }


        public XmlDocument RetrieveMismoCreditReportBySsn(string ssn) 
        {
            ssn = ssn.Replace("-", ""); // Mismo trim all dash in their ssn.

            if (!m_isCreditReportHashInitialized) 
            {
                InitializeCreditReportHash();
            }

            return (XmlDocument) m_creditReportHash[ssn];
        }
        public bool IsDuRefiPlus
        {
            get
            {
                string xml = FnmaFindingsXml;
                if (string.IsNullOrEmpty(xml))
                {
                    return false;
                }

                XmlDocument doc = Tools.CreateXmlDoc(xml);
                // 4/8/2009 dd - DU Refi Plus Special Code.
                string xPath = string.Format("/CodifiedFindings/SpecialFeatureCodes/SpecialFeatureCode[@SpecialFeatureCode='{0}']", ConstAppDavid.FannieMae_SpecialFeatureCode_DURefiPlus);
                XmlNode node = doc.SelectSingleNode(xPath);

                return null != node;
            }
        }
        private List<FnmaMessage> ListFindingsToConvert() 
        {
            return ConvertDuFindings(FnmaFindingsXml);
        }

        private void ImportToLoanFile(Guid sLId, AbstractUserPrincipal principal, bool isImport1003, bool isImportDuFindings, bool isImportFindingsAsConditions, bool isImportCreditReport) 
        {
            this.SaveToFileDB(sLId);
            try
            {
                UpdateFromFnmaAuditItem audit = new UpdateFromFnmaAuditItem(principal, isImport1003, isImportDuFindings, isImportCreditReport);
                AuditManager.RecordAudit(sLId, audit);

                if (isImport1003)
                {
                    LendersOffice.Conversions.FannieMae32Importer fnma32Importer = new LendersOffice.Conversions.FannieMae32Importer(m_fannieMaeImportSource);
                    fnma32Importer.IsByPassPermission = true;
                    fnma32Importer.ImportIntoExisting(this.Fnma32Content, sLId);
                }

                CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(sLId, typeof(FnmaXisCasefileExportResponse));
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                dataLoan.SaveXisCasefileExportResponse(this, isImportDuFindings);
                dataLoan.sProdIsDuRefiPlus = IsDuRefiPlus;

                //XisCasefileStatus status = new XisCasefileStatus(this.FnmaStatusXml);
                //dataLoan.sDuLenderInstitutionId = status.LenderInstitutionIdentifier;

                List<FnmaMessage> findingsList = ListFindingsToConvert();

                #region Convert DU Findings to Conditions
                if (isImportFindingsAsConditions && findingsList.Count > 0)
                {

                    BrokerDB currentBroker = BrokerDB.RetrieveById(principal.BrokerId);

                    if (currentBroker.IsUseNewTaskSystem)
                    {
                        IConditionImporter set = dataLoan.GetConditionImporter("DU");
                        set.IsSkipCategoryCheck = true; // 9/7/2011 dd - OPM 70845 don't check category for condition uniqueness.
                        foreach (var msg in findingsList)
                        {
                            set.AddUniqueAus(msg.MessageText, msg.MessageIdentifier);
                        }
                    }
                    else
                    {
                        LoanConditionSetObsolete conditions = new LoanConditionSetObsolete();
                        conditions.RetrieveAll(sLId, true);

                        foreach (var msg in findingsList)
                        {
                            conditions.AddUniqueCondition("DU", msg.MessageText);
                        }
                        conditions.Save(principal.BrokerId);
                    }
                }
                #endregion

                #region Import Special Feature Codes from DU Findings // OPM 107995
                if (isImportDuFindings)
                {
                    List<string> specialFeatureCodes = ConvertDuSpecialFeatureCodes(FnmaFindingsXml);

                    dataLoan.sGseDeliveryFeature1Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 0);
                    dataLoan.sGseDeliveryFeature2Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 1);
                    dataLoan.sGseDeliveryFeature3Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 2);
                    dataLoan.sGseDeliveryFeature4Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 3);
                    dataLoan.sGseDeliveryFeature5Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 4);
                    dataLoan.sGseDeliveryFeature6Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 5);
                    dataLoan.sGseDeliveryFeature7Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 6);
                    dataLoan.sGseDeliveryFeature8Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 7);
                    dataLoan.sGseDeliveryFeature9Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 8);
                    dataLoan.sGseDeliveryFeature10Id = SetOrClearSpecialFeatureCode(specialFeatureCodes, 9);
                }
                #endregion

                if (isImportCreditReport)
                {
                    for (int i = 0; i < dataLoan.nApps; i++)
                    {
                        CAppData dataApp = dataLoan.GetAppData(i);
                        XmlDocument doc = this.RetrieveMismoCreditReportBySsn(dataApp.aBSsn);

                        if (null != doc)
                        {
                            FannieMae_CreditReportResponse fannieMaeCreditResponse = new FannieMae_CreditReportResponse(doc);

                            CreditReportUtilities.SaveXmlCreditReport(fannieMaeCreditResponse, principal, sLId, dataApp.aAppId, ConstAppDavid.DummyFannieMaeServiceCompany, Guid.Empty, E_CreditReportAuditSourceT.ImportFromDoDu);
                            if (m_fannieMaeImportSource == FannieMaeImportSource.H4H || m_fannieMaeImportSource == FannieMaeImportSource.AUD)
                            {
                                // 12/18/2009 dd - OPM 43623. When reimport from DO/DU for H4H, we want to reimport liability
                                // from credit report to set the reconcile status of tradeline in file.

                                dataApp.ImportLiabilitiesFromCreditReport(true, false);
                            }
                            dataApp.ImportCreditScoresFromCreditReport();
                            // 6/17/2009 dd - Clear all existing public records before reimport.
                            dataApp.aPublicRecordCollection.ClearAll();
                            dataApp.aPublicRecordCollection.Flush();
                            dataApp.ImportPublicRecordsFromCreditReport();
                        }

                    }

                }
                dataLoan.Save();

                // 2/4/2010 dd - OPM 45329 - Run TOTAL interface if the following are true.
                // Lender is turn on.
                // Loan is FHA loans
                // There is finding from DU.
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
                if (brokerDB.HasTotalScorePreviewFeature && dataLoan.sLT == E_sLT.FHA)
                {
                    try
                    {
                        CreditRiskEvaluation.GenerateTemporaryTotalScoreCertificate(sLId);
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError(exc);
                    }
                }
            }
            catch (Exception exc)
            {
                Tools.LogError(exc);
                throw new CBaseException("Unable to import data to loan file.", exc);
            }

        }

        public void ImportToLoanFile(Guid sLId, AbstractUserPrincipal principal, bool isImport1003, bool isImportDuFindings, bool isImportCreditReport) 
        {
            bool isImportDuFindingsAsConditions = BrokerDB.RetrieveById(PrincipalFactory.CurrentPrincipal.BrokerId).IsImportDoDuLpFindingsAsConditions;
            
            //If we're not importing DU findings at all, we never want to import the conditions.
            if (isImportDuFindings == false)
            {
                isImportDuFindingsAsConditions = false;
            }
            ImportToLoanFile(sLId, principal, isImport1003, isImportDuFindings, isImportDuFindingsAsConditions, isImportCreditReport);
        }

        private string SetOrClearSpecialFeatureCode(List<string> SFCodes, int index)
        {
            return (SFCodes.Count > index) ? SFCodes[index] : "";
        }
	}
}
