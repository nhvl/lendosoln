/// Author: David Dao

using System;
using System.Xml;
using LendersOffice.Constants;

namespace LendersOffice.DU
{
	public class FnmaDwebCasefileImportRequest : AbstractDwebXisRequest
	{
		public FnmaDwebCasefileImportRequest(bool isDo, Guid sLId) : base(isDo, sLId)
		{
		}
        public override string FnmaProductFunctionName 
        {
            get { return "CasefileImport"; }
        }

        private string m_mornetPlusCasefileIdentifier;

        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }

        private XisCasefileInstitution m_xisCasefileInstitution = new XisCasefileInstitution();

        public XisCasefileInstitution XisCasefileInstitution
        {
            get { return m_xisCasefileInstitution; }
        }
        protected override void CreateControlInput(XmlWriter writer) 
        {
            XisDwebCasefileImportRequest dwebCasefileImportRequest = new XisDwebCasefileImportRequest();
            dwebCasefileImportRequest.ValidationType = "Import";
            dwebCasefileImportRequest.MornetPlusCasefileIdentifier = m_mornetPlusCasefileIdentifier;
            dwebCasefileImportRequest.SoftwareProvider.AccountNumber = ConstAppDavid.FannieMae_SoftwareProviderCode;
            dwebCasefileImportRequest.AddCasefileInstitution(m_xisCasefileInstitution);
            dwebCasefileImportRequest.DataFile.Type = "1";
            dwebCasefileImportRequest.DataFile.FormatType = "1";
            dwebCasefileImportRequest.DataFile.VersionNumber = "3.2";
            dwebCasefileImportRequest.DataFile.Name = "MORNETPLUS_LOAN";

            dwebCasefileImportRequest.GenerateXml(writer);
            
        }
        public override AbstractFnmaXisResponse CreateResponse(string rawData) 
        {
            return new FnmaDwebCasefileImportResponse(rawData);
        }
	}
}
