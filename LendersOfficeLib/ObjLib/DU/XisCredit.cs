/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;

using System.Xml;

namespace LendersOffice.DU
{
	public class XisCredit : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "CREDIT"; }
        }
        private XisServiceProvider m_serviceProvider = new XisServiceProvider();
        private List<XisCreditInformation> m_creditInformationList = new List<XisCreditInformation>();

        public E_XisCreditCopyLiabilitiesIndicator CopyLiabilitiesIndicator { get; set;}
        public XisServiceProvider ServiceProvider 
        {
            get { return m_serviceProvider; }
        }
        public void AddCreditInformation(XisCreditInformation creditInformation) 
        {
            m_creditInformationList.Add(creditInformation);
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "CopyLiabilitiesIndicator", CopyLiabilitiesIndicator.ToString("D"));
            WriteElement(writer, m_serviceProvider);
            WriteElement(writer, m_creditInformationList);
        }

	}
    public enum E_XisCreditCopyLiabilitiesIndicator 
    {
        False = 0,
        True = 1
    }
}
