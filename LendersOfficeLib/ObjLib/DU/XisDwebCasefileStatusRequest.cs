/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisDwebCasefileStatusRequest : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "DWEB_CASEFILE_STATUS_REQUEST"; }
        }

        private string m_mornetPlusCasefileIdentifier;
        private List<XisCasefileInstitution> m_casefileInstitutionList = new List<XisCasefileInstitution>();
        private XisSoftwareProvider m_softwareProvider = new XisSoftwareProvider();

        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        public void AddCasefileInstitution(XisCasefileInstitution casefileInstitution) 
        {
            m_casefileInstitutionList.Add(casefileInstitution);
        }
        public XisSoftwareProvider SoftwareProvider 
        {
            get { return m_softwareProvider; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "MORNETPlusCasefileIdentifier", m_mornetPlusCasefileIdentifier);
            WriteElement(writer, m_casefileInstitutionList);
            WriteElement(writer, m_softwareProvider);
        }

	}
}
