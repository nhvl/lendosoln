/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class FnmaDwebLogoutRequest : AbstractDwebXisRequest
	{
        public override string FnmaProductFunctionName 
        {
            get { return "Logout"; }
        }
		public FnmaDwebLogoutRequest(bool isDo, Guid sLId) : base(isDo, sLId)
		{
		}
        protected override void CreateControlInput(XmlWriter writer) 
        {

        }
        protected override bool IsGenerateBusinessInputSection 
        {
            get { return false; } // Export request does not need MORNETPLUS_LOAN section
        }
        public override AbstractFnmaXisResponse CreateResponse(string rawData) 
        {
            return null;
        }
	}
}
