/// Author: David Dao

using System;
using System.Xml;
namespace LendersOffice.DU
{
	public class XisDataFile : AbstractXisXmlNode
	{
        public XisDataFile()
        {
            // 7/27/2012 dd - Set Default Value
            Type = "1003 Text";
            VersionNumber = "3.2";
            Name = "MORNETPLUS_LOAN";
        }
        protected override string ElementName 
        {
            get { return "DATA_FILE"; }
        }

        public string Type { get; set;}
        public string FormatType { get; set;}
        public string VersionNumber { get; set;}
        public string Name { get; set;}

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "_Type", Type);
            WriteAttribute(writer, "_FormatType", FormatType);
            WriteAttribute(writer, "_VersionNumber", VersionNumber);
            WriteAttribute(writer, "_Name", Name);
        }


    }
}
