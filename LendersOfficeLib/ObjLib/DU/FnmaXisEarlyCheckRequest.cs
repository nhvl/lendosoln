﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using LendersOffice.Constants;

namespace LendersOffice.DU
{
    public class FnmaXisEarlyCheckRequest : AbstractDuXisRequest
    {
        public FnmaXisEarlyCheckRequest(Guid sLId)
            : base(sLId)
        {
            //this.BusinessInputT = E_FnmaXisRequestBusinessInputT.Mismo_Delivery;

        }

        public override string FnmaProductFunctionName
        {
            get { return "Check"; }
        }

        public override string FnmaProductName
        {
            get { return "earlycheck"; }
        }

        public override string FnmaProductVersionNumber
        {
            get { return "1.0"; }
        }
        public string MornetPlusCasefileIdentifier { get; set; }
        public string LenderCaseIdentifier { get; set; }
        protected override void CreateControlInput(XmlWriter writer)
        {
            XisEarlyCheckCheckRequest earlyCheckRequest = new XisEarlyCheckCheckRequest();

            if (this.BusinessInputT == E_FnmaXisRequestBusinessInputT.Mismo_Delivery)
            {
                earlyCheckRequest.DataFile.Type = "ULDD";
                earlyCheckRequest.DataFile.VersionNumber = "3.0";
                earlyCheckRequest.DataFile.Name = "MISMO_DELIVERY";

                earlyCheckRequest.Add(E_XisEarlyCheckReturnFileType.EarlyCheckSummaryResults, E_XisEarlyCheckReturnFileFormatType.Xml);
                earlyCheckRequest.Add(E_XisEarlyCheckReturnFileType.EarlyCheckSummaryResults, E_XisEarlyCheckReturnFileFormatType.Html);

            }
            else if (this.BusinessInputT == E_FnmaXisRequestBusinessInputT.MornetPlus_Loan)
            {
                earlyCheckRequest.DataFile.Type = "1003";
                earlyCheckRequest.DataFile.VersionNumber = "3.2";
                earlyCheckRequest.DataFile.Name = "MORNETPLUS_LOAN";

                earlyCheckRequest.Add(E_XisEarlyCheckReturnFileType.EarlyCheckLoanLevelResults, E_XisEarlyCheckReturnFileFormatType.Xml);
                earlyCheckRequest.Add(E_XisEarlyCheckReturnFileType.EarlyCheckLoanLevelResults, E_XisEarlyCheckReturnFileFormatType.Html);

            }


            earlyCheckRequest.SoftwareProvider.AccountNumber = ConstAppDavid.FannieMae_SoftwareProviderCode_DU_EarlyCheck;

            earlyCheckRequest.MornetPlusCasefileIdentifier = MornetPlusCasefileIdentifier;
            earlyCheckRequest.LenderCaseIdentifier = LenderCaseIdentifier;
            earlyCheckRequest.LenderInstitutionIdentifier = LenderInstitutionId;

            earlyCheckRequest.GenerateXml(writer);
        }

        public override AbstractFnmaXisResponse CreateResponse(string rawData)
        {
            return new FnmaXisEarlyCheckResponse(rawData);
        }
    }
}
