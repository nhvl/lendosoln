/// Author: David Dao

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using LendersOffice.Constants;
namespace LendersOffice.DU
{
	public class FnmaXisDuUnderwriteRequest : AbstractDuXisRequest
	{
		public FnmaXisDuUnderwriteRequest(Guid sLId) : base(sLId)
		{
		}
        public override string FnmaProductName 
        {
            get { return "DU"; }
        }
        public override string FnmaProductFunctionName 
        {
            get { return "Underwrite"; }
        }
        public override string FnmaProductVersionNumber
        {
            get { return "2.0"; }
        }

        private List<XisCreditInformation> m_creditInformationList = new List<XisCreditInformation>();

        public bool SkipCreditSection { get; set;}
        public bool CopyLiabilitiesIndicator { get; set;}
        public string CraProviderNumber { get; set;}
        public string CraLoginName { get; set;}
        public string CraPassword { get; set;}
        public bool UseLegacyFindingsFormat { get; set; } = true;

        public void AddCreditInformation(XisCreditInformation creditInformation)
        {
            m_creditInformationList.Add(creditInformation);
        }

        protected override void CreateControlInput(XmlWriter writer) 
        {
            XisDuUnderwriteRequest duUnderwritingRequest = new XisDuUnderwriteRequest();

            List<E_XisDuReturnFileType> returnFileTypes = new List<E_XisDuReturnFileType>
            {
                E_XisDuReturnFileType.CreditReportPrintFile,
                E_XisDuReturnFileType.CreditReportMismoFile
            };
            if (UseLegacyFindingsFormat)
            {
                returnFileTypes.Add(E_XisDuReturnFileType.UnderwritingFingingsHtml);
                returnFileTypes.Add(E_XisDuReturnFileType.UnderwritingFindingsXml);
            }
            else
            {
                returnFileTypes.Add(E_XisDuReturnFileType.UnderwritingFindingsHtmlNew);
                returnFileTypes.Add(E_XisDuReturnFileType.UnderwritingFindingsXml);
                returnFileTypes.Add(E_XisDuReturnFileType.UnderwritingFindingsPdf);
            }

            if (!SkipCreditSection) 
            {
                XisCredit credit = new XisCredit();
                duUnderwritingRequest.Credit = credit;

                credit.CopyLiabilitiesIndicator = CopyLiabilitiesIndicator ? E_XisCreditCopyLiabilitiesIndicator.True : E_XisCreditCopyLiabilitiesIndicator.False;
                credit.ServiceProvider.Name = CraProviderNumber;
                credit.ServiceProvider.AccountNumber = CraLoginName;
                credit.ServiceProvider.Password = CraPassword;

                foreach (XisCreditInformation creditInformation in m_creditInformationList) 
                {
                    credit.AddCreditInformation(creditInformation);
                }
                
            }
            foreach (E_XisDuReturnFileType type in returnFileTypes) 
            {
                duUnderwritingRequest.Add(type);
            }
            duUnderwritingRequest.SoftwareProvider.AccountNumber = ConstAppDavid.FannieMae_SoftwareProviderCode_DU_EarlyCheck;


            duUnderwritingRequest.GenerateXml(writer);
        }
        public override AbstractFnmaXisResponse CreateResponse(string rawData) 
        {
            return new FnmaXisDuUnderwriteResponse(rawData);
        }
	}
}
