/// Author: David Dao

using System;
using LendersOffice.Constants;

namespace LendersOffice.DU
{

	public abstract class AbstractDwebXisRequest : AbstractFnmaXisRequest
	{
		public AbstractDwebXisRequest(bool isDO, Guid sLId) : base(sLId)
		{
            IsDo = isDO;
		}

        public bool IsDo { get; private set; } = false;

        public override string Url 
        {
            get 
            {
                if (IsDo) 
                {
                    if (FannieMaeMORNETUserID == ConstAppDavid.FannieMae_TestDoUserId || FannieMaeMORNETUserID == ConstAppDavid.FannieMae_TestDoUserId2)
                        return ConstAppDavid.FannieMae_Dweb_DoBetaServer;
                    else
                        return ConstAppDavid.FannieMae_Dweb_DoProductionServer;
                } 
                else 
                {
                    if (FannieMaeMORNETUserID == ConstStage.DUTestUserName)
                        return ConstAppDavid.FannieMae_Dweb_DuBetaServer;
                    else 
                        return ConstAppDavid.FannieMae_Dweb_DuProductionServer;
                }
            }
        }
        public override string FnmaProductName 
        {
            get { return "DWEB"; }
        }
        public override string FnmaProductVersionNumber 
        {
            get { return "2.0"; }
        }
	}
}
