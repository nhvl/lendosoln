/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisSoftwareProvider : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "SOFTWARE_PROVIDER"; }
        }

        public string AccountNumber { get; set;}

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "_AccountNumber", AccountNumber);
        }
	}
}
