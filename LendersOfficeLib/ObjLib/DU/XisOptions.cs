/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisOptions : AbstractXisXmlNode
	{
        protected override string ElementName 
        {
            get { return "OPTIONS"; }
        }

        private string m_downloadEnable;

        public string DownloadEnable 
        {
            get { return m_downloadEnable; }
            set { m_downloadEnable = value; }
        }
        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "DownloadEnable", m_downloadEnable);
        }
	}
}
