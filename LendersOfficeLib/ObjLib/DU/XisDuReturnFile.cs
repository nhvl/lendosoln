/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisDuReturnFile : AbstractXisXmlNode
	{
        public XisDuReturnFile(E_XisDuReturnFileType type)
        {
            this.FileType = type;
        }
        public E_XisDuReturnFileType FileType { get; private set;}

        protected override string ElementName 
        {
            get { return "_RETURN_FILE"; }
        }

        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "_FileType", FileType.ToString("D"));
        }
	}
    public enum E_XisDuReturnFileType 
    {
        UnderwritingFindingsText = 2,
        UnderwritingFingingsHtml = 3,
        UnderwritingFindingsXml = 5,
        CreditReportPrintFile = 6,
        CreditReportFlatFile = 7,
        MornetPlusLoan = 8,
        CreditReportMismoFile = 12,
        UnderwritingFindingsHtmlNew = 16,
        UnderwritingFindingsPdf = 17

    }
}
