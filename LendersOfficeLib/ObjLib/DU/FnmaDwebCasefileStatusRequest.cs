/// Author: David Dao

using System;
using System.Xml;
using LendersOffice.Constants;

namespace LendersOffice.DU
{
	public class FnmaDwebCasefileStatusRequest : AbstractDwebXisRequest
	{
		public FnmaDwebCasefileStatusRequest(bool isDo, Guid sLId) : base(isDo, sLId)
		{
		}

        public override string FnmaProductFunctionName 
        {
            get { return "CasefileStatus"; }
        }
        private string m_mornetPlusCasefileIdentifier;

        public string MornetPlusCasefileIdentifier 
        {
            get { return m_mornetPlusCasefileIdentifier; }
            set { m_mornetPlusCasefileIdentifier = value; }
        }
        protected override bool IsGenerateBusinessInputSection 
        {
            get { return false; } // Export request does not need MORNETPLUS_LOAN section
        }
        protected override void CreateControlInput(XmlWriter writer) 
        {
            XisDwebCasefileStatusRequest dwebCasefileStatusRequest = new XisDwebCasefileStatusRequest();
            dwebCasefileStatusRequest.MornetPlusCasefileIdentifier = m_mornetPlusCasefileIdentifier;
            dwebCasefileStatusRequest.SoftwareProvider.AccountNumber = ConstAppDavid.FannieMae_SoftwareProviderCode;
            dwebCasefileStatusRequest.AddCasefileInstitution(new XisCasefileInstitution());

            dwebCasefileStatusRequest.GenerateXml(writer);
            
        }
        public override AbstractFnmaXisResponse CreateResponse(string rawData) 
        {
            return new FnmaDwebCasefileStatusResponse(rawData);
        }
	}
}
