namespace LendersOffice.DU
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.Linq;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.HttpModule;
    using LendersOffice.Integration.Underwriting;
    using Security;

    public class FnmaInvalidDataException : CBaseException
    {
        public FnmaInvalidDataException(IEnumerable<string> errorMessages)
            : base(string.Empty, "FNMA 3.2 Contains Illegal Character")
        {
            this.IsEmailDeveloper = false;
            StringBuilder sb = new StringBuilder();

            foreach (var s in errorMessages)
            {
                sb.AppendLine(s);
            }
            this.UserMessage = sb.ToString();
        }

    }

    /// <summary>
    /// Submits to DU. Only should be used for non-seamless. Seamless DU submissions should be routed through <see cref="ObjLib.DU.Seamless.DuRequestSubmission"/>.
    /// </summary>
	public class DUServer
	{
        //public static int TempVersion = 0;
        public static IEnumerable<KeyValuePair<string, string>> GetCreditProviderList()
        {
            // 6/17/2013 dd - Retrieve a list of active FannieMae CRA Provider List.
            // Right now just load from the XML file. In future we may use FannieMae web service
            // to pull the list.

            List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();


            Assembly assembly = typeof(DUServer).Assembly;

            using (Stream stream = assembly.GetManifestResourceStream("LendersOffice.ObjLib.DU.FannieMaeCra.xml")) 
            {
                if (stream == null)
                {
                    throw new GenericUserErrorMessageException("Embedded resource LendersOfficeLib.ObjLib.DU.FannieMaeCra.xml is not found.");
                }
                XDocument xdoc = XDocument.Load(XmlReader.Create(stream));
                foreach (XElement item in xdoc.Root.Elements("item"))
                {
                    list.Add(new KeyValuePair<string, string>(item.Attribute("id").Value, item.Attribute("name").Value));
                }
            }

            return list;
        }

        public static void RegenerateCreditProviderList()
        {
            // 10/30/2013 dd - Due to lack of time, I just dump xml to PB.
            // TEST
            FnmaXisGetCreditAgenciesRequest getCreditRequest = new FnmaXisGetCreditAgenciesRequest();
            getCreditRequest.FannieMaeMORNETUserID = ConstStage.DUTestUserName;
            getCreditRequest.FannieMaeMORNETPassword = ConstStage.DUTestPassword;

            FnmaXisGetCreditAgenciesResponse getCreditResponse = (FnmaXisGetCreditAgenciesResponse)DUServer.Submit(getCreditRequest);

            // 10/30/2013 dd - Copy the content of the PB log to FannieMaeCra.xml
            Tools.LogInfo(getCreditResponse.ExportToXml());
        }
        //private static int s_counter = 0;
        //private static string s_lastFilePrefix = string.Empty;

        private static string MaskPassword(string str) 
        {
            return Regex.Replace(str, " LoginAccountPassword=\"[^ ]+\"", " LoginAccountPassword=\"******\"");
        }
        public static AbstractFnmaXisResponse Submit(AbstractFnmaXisRequest request, bool suppressAusResultLogging = false) 
        {
            // OPM 245347 - Check Workflow permissions (Throws AccessDenied exception).
            if (request.sLId != Guid.Empty)
            {
                if (request is AbstractDwebXisRequest && ((AbstractDwebXisRequest)request).IsDo)
                {
                    LendersOffice.Security.AuthServiceHelper.CheckLoanAuthorization(request.sLId, LendersOffice.ConfigSystem.Operations.WorkflowOperations.RunDo);
                }
                else
                {
                    LendersOffice.Security.AuthServiceHelper.CheckLoanAuthorization(request.sLId, LendersOffice.ConfigSystem.Operations.WorkflowOperations.RunDu);
                }
            }

            // 3/21/2008 dd - Exclude the overall time we take to connect to DO/DU.
            PerformanceMonitorItem monitorItem = PerformanceMonitorItem.Current;

            if (null != monitorItem)
            {
                monitorItem.StopTiming();
            }
            int maxRetries = 5;
            AbstractFnmaXisResponse response = null;

            //if (request.sLId != Guid.Empty)
            //{
            //    CPageData dataLoan = new CFullAccessPageData(request.sLId, new string[] { "sLenderCaseNum"});
            //    dataLoan.InitLoad();
            //    if (s_lastFilePrefix != dataLoan.sLenderCaseNum)
            //    {
            //        s_lastFilePrefix = dataLoan.sLenderCaseNum;
            //        s_counter = 0;
            //    }
            //}
            if(request.sLId != Guid.Empty)
            {
                // ideally we should have our own WebRequest that runs workflow before sending anything outside our company.
                CPageData dataLoan = new CFullAccessPageData(request.sLId, new string[] {});
                dataLoan.InitLoad();
                if(E_sLoanFileT.QuickPricer2Sandboxed == dataLoan.sLoanFileT)
                {
                    var usrMsg = "You may not access DU Server on quickpricer sandboxed loans.";
                    var devMsg = "Programming error.  User tried to submit to DU when sLoanFileT was QuickPricer2Sandboxed for slid: " + dataLoan.sLId;
                    throw new CBaseException(usrMsg, devMsg);
                }
            }
            for (int numOfRetries = 0; numOfRetries < maxRetries; numOfRetries++)
            {
                response = null;

                try
                {
                    HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(request.Url);
                    webRequest.KeepAlive = false; // 11/4/2004 dd - DO NOT REMOVE THIS LINE. Else you will get "Cannot access a disposed object named "System.Net.TlsStream"" error randomly.
                    webRequest.ContentType = "application/x-www-form-urlencoded";
                    request.SetupAuthentication(webRequest);
                    webRequest.Method = "POST";

                    byte[] bytes = request.GeneratePostContent();
                    webRequest.ContentLength = request.ContentLength;
                    Tools.LogInfo("DUServer", "#" + numOfRetries + " Url=[" + request.Url + "]. Request Length=" + bytes.Length);

                    if (Tools.IsLogLargeRequest(bytes.Length))
                    {
                        string pbRequestString = System.Text.Encoding.UTF8.GetString(bytes, 0, request.ContentLength);
                        Tools.LogInfo("DUServerRequest", MaskPassword(pbRequestString));

                    }
                    else
                    {
                        Tools.LogInfo("DUServerRequest", bytes.Length + " bytes exceed threshold for logging.");
                    }

                    if (request.HasError)
                    {
                        FnmaInvalidDataException exc = new FnmaInvalidDataException(request.ErrorMessages);

                        Tools.LogInfo("DUServerRequets", "Contains Error: " + exc.UserMessage);
                        throw exc;

                    }
                    using (Stream stream = webRequest.GetRequestStream())
                    {
                        stream.Write(bytes, 0, request.ContentLength);
                    }

                    WebResponse webResponse = webRequest.GetResponse();

                    StringBuilder sb = new StringBuilder();
                    using (Stream stream = webResponse.GetResponseStream())
                    {
                        byte[] buffer = new byte[60000];
                        int size = stream.Read(buffer, 0, buffer.Length);
                        while (size > 0)
                        {
                            sb.Append(System.Text.Encoding.UTF8.GetString(buffer, 0, size));
                            size = stream.Read(buffer, 0, buffer.Length);
                        }

                    }

                    string responseString = sb.ToString();

                    if (Tools.IsLogLargeRequest(responseString.Length))
                    {
                        Tools.LogInfo("DUServerResponse", MaskPassword(responseString));
                    }
                    else
                    {
                        Tools.LogInfo("DUServerResponse", responseString.Length + " bytes exceed threshold for logging.");
                    }

                    
                    webResponse.Close();

                    response = request.CreateResponse(responseString);
                    break;// 8/26/2009 dd - End normally. Break out of retries loop.

                }
                catch (WebException exc)
                {
                    Tools.LogWarning(exc);
                    if (numOfRetries == maxRetries - 1)
                    {
                        throw new GenericUserErrorMessageException(exc);
                    }
                    Tools.SleepAndWakeupRandomlyWithin(200, 5000);
                }
                catch (FnmaInvalidDataException)
                {
                    throw;
                }
                catch (Exception exc)
                {
                    Tools.LogError(exc);
                    throw new GenericUserErrorMessageException(exc);
                }
                
            }
            if (null != monitorItem)
            {
                monitorItem.ResumeTiming();
            }
            
            if (!suppressAusResultLogging && request.sLId != Guid.Empty && response != null && response is FnmaDwebCasefileStatusResponse)
            {
                var findings = response as FnmaDwebCasefileStatusResponse;

                if (!string.IsNullOrEmpty(findings.MornetPlusCasefileIdentifier)
                    && !string.IsNullOrEmpty(findings.UnderwritingRecommendationDescription))
                {
                    AusResultHandler.CreateSystemDuOrder(
                        request.sLId,
                        findings.MornetPlusCasefileIdentifier,
                        findings.UnderwritingRecommendationDescription,
                        PrincipalFactory.CurrentPrincipal);
                }
            }

            return response;
        }

        public static AbstractFnmaXisResponse SubmitSynchronous(AbstractFnmaXisRequest request)
        {
            if (request == null)
            {
                throw new CBaseException(ErrorMessages.Generic, "request object is null");
            }

            for (int i = 0; i < 20; i++)
            {
                AbstractFnmaXisResponse response = Submit(request);

                if (response.IsReady)
                {
                    return response;
                }
                else
                {
                    request.GloballyUniqueIdentifier = response.GloballyUniqueIdentifier;
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000);
                }
            }
            throw new TimeoutException("Could not get DU response in timely manner.");
        }
	}
}
