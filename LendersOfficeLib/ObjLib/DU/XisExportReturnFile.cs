/// Author: David Dao

using System;
using System.Xml;

namespace LendersOffice.DU
{
	public class XisExportReturnFile : AbstractXisXmlNode
	{

        private E_XisExportReturnFileType m_type;
        private E_XisExportReturnFileFormatType m_formatType;
        private string m_versionNumber;
        private string m_name;

        protected override string ElementName 
        {
            get { return "_RETURN_FILE"; }
        }

        public E_XisExportReturnFileType Type 
        {
            get { return m_type; }
            set { m_type = value; }
        }
        public E_XisExportReturnFileFormatType FormatType 
        {
            get { return m_formatType; }
            set { m_formatType = value; }
        }
        public string VersionNumber 
        {
            get { return m_versionNumber; }
            set { m_versionNumber = value; }
        }
        public string Name 
        {
            get { return m_name; }
            set { m_name = value; }
        }

		public XisExportReturnFile()
		{
		}
        protected override void GenerateXmlContent(XmlWriter writer) 
        {
            WriteAttribute(writer, "_Type", m_type.ToString("D"));
            WriteAttribute(writer, "_FormatType", m_formatType.ToString("D"));
            WriteAttribute(writer, "_VersionNumber", m_versionNumber);
            WriteAttribute(writer, "_Name", m_name);
        }
	}
    public enum E_XisExportReturnFileType 
    {
        _1003WorkingData = 1,
        CreditData = 3,
        DUFindings = 4,
        GEFindings = 7,
        RFCFindings = 8,
        SPFindings = 9,
        ValidationResultsFile = 10,
        DUFindingsXml = 11,
        CasefileDetails = 12,
        CasefileNotes = 13,
        VAFindings = 14,
        FHAFindings = 15,
        NewAmerica = 16,
        Countrywide = 17,
        PmiMi = 18,
        DatafeedInput = 21,
        CDUFindings = 29,
        ExtendedUWData = 104,
        EarlyCheckLoanLevelResults = 132,
        RecentFindings502 = 502, // Preliminary findings
        RecentFindings503 = 503, // Recent findings
        CasefileStatus = 999

    }
    public enum E_XisExportReturnFileFormatType 
    {
        FixedDataFormat = 1,
        FormattedText = 2,
        HTML = 3,
        Binary = 4,
        XML = 5
    }
}
