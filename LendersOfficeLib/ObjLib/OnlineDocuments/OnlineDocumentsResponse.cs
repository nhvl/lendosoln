/// Author: David Dao

using System;

namespace LendersOffice.OnlineDocuments
{
	public class OnlineDocumentsResponse
	{
        private string m_customerReturnUrl;
        private string m_lossesXml;
        private string m_loginUrl;
        private string m_resultText;
        private int m_result;

		public OnlineDocumentsResponse(string customerReturnUrl, string lossesXml, string loginUrl, string resultText, int result)
		{
            m_customerReturnUrl = customerReturnUrl;
            m_lossesXml = lossesXml;
            m_loginUrl = loginUrl;
            m_resultText = resultText;
            m_result = result;
		}

        public string CustomerReturnUrl 
        {
            get { return m_customerReturnUrl; }
        }
        public string LossesXml 
        {
            get { return m_lossesXml; }
        }
        public string LoginUrl 
        {
            get { return m_loginUrl; }
        }
        public string ResultText 
        {
            get { return m_resultText; }
        }
        public int Result 
        {
            get { return m_result; }
        }
	}
}
