/// Author: David Dao

using System;
using System.IO;
using LendersOffice.Constants;
using System.Web;
using System.Net;
using LendersOffice.Conversions.Closing231;
namespace LendersOffice.OnlineDocuments
{
	public class OnlineDocumentsRequest
	{
        private int m_contentLength = 0;
        private string m_customerCode = ConstAppDavid.OnlineDocuments_TestCustomerCode;
        private int m_scenarioID = ConstAppDavid.OnlineDocuments_TestScenarioID;
        private string m_loginName = ConstAppDavid.OnlineDocuments_TestUserName;
        private string m_password = ConstAppDavid.OnlineDocuments_TestPassword;
        private Guid m_sLId;
        private string m_lenderLoanIdentifier;

        public Guid sLId 
        {
            get { return m_sLId; }
            set { m_sLId = value; }
        }
        public string LenderLoanIdentifier 
        {
            get { return m_lenderLoanIdentifier; }
            set { m_lenderLoanIdentifier = value; }
        }

        public string CustomerCode 
        {
            get { return m_customerCode; }
            set { m_customerCode = value; }
        }
        public int ScenarioID 
        {
            get { return m_scenarioID; }
            set { m_scenarioID = value; }
        }
        public string LoginName 
        {
            get { return m_loginName; }
            set { m_loginName = value; }
        }
        public string Password 
        {
            get { return m_password; }
            set { m_password = value; }
        }

        public string Url 
        {
            get 
            {
                if (m_customerCode == ConstAppDavid.OnlineDocuments_TestCustomerCode && m_loginName == ConstAppDavid.OnlineDocuments_TestUserName 
                    && m_password == ConstAppDavid.OnlineDocuments_TestPassword) 
                {
                    return ConstAppDavid.OnlineDocuments_TestUrl;
                }
                else 
                {
                    return ConstAppDavid.OnlineDocuments_ProductionUrl;
                }
            }
        }
		public OnlineDocumentsRequest()
		{
		}

        public int ContentLength  
        {
            get { return m_contentLength; }
        }
        public string MismoInput 
        {
            get 
            {
                Closing231Exporter exporter = new Closing231Exporter(m_sLId, m_lenderLoanIdentifier);
                string xml = exporter.Export();
                return xml;
            }
        }

	}
}
