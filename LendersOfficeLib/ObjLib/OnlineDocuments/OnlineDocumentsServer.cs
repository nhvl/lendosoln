/// Author: David Dao

using System;

using DataAccess;
using LendersOffice.com.onlineexpressweb.www;
namespace LendersOffice.OnlineDocuments
{
    public class OnlineDocumentsServer
    {
        public static OnlineDocumentsResponse Submit(OnlineDocumentsRequest request) 
        {

            MismoImportWebService webService = new MismoImportWebService();
            webService.Url = request.Url;

            try 
            {
                string customerReturnUrl = "";
                string lossesXml = "";
                string loginUrl = "";
                string resultText = "";

                string requestXml = request.MismoInput;
                Tools.LogInfo("RequestXml=" + requestXml);
                int result = webService.OEWebAcceptMismoString(requestXml, request.CustomerCode, request.ScenarioID, request.LoginName, 
                    request.Password, customerReturnUrl, ref lossesXml, ref loginUrl, ref resultText);
                Tools.LogInfo("Response ResultCode=" + result + ", ResultText=" + resultText);
                Tools.LogInfo("LossesXml=" + lossesXml);

                return new OnlineDocumentsResponse(customerReturnUrl, lossesXml, loginUrl, resultText, result);
            } 
            catch (Exception exc) 
            {
                Tools.LogError(exc);
            }
            return null;
        }
    }
}

