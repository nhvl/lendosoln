﻿// <copyright file="OriginatorCompensationPlan.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: geoffreyf
//    Date:   7/3/2014 10:29:39 AM 
// </summary>
namespace LendersOffice.ObjLib.OriginatorCompensation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;

    /// <summary>
    /// A simple container for an originator compensation plan.
    /// </summary>
    public class OriginatorCompensationPlan
    {
        /// <summary>
        /// The base value to use with the percentage.
        /// </summary>
        private E_PercentBaseT baseT = E_PercentBaseT.LoanAmount;

        /// <summary>
        /// Gets or sets the percentage of the loan amount that will be added to
        /// the fixed amount.
        /// </summary>
        /// <value>
        /// The percentage of the loan amount that will be added to the fixed amount.
        /// </value>
        public decimal Percent { get; set; }

        /// <summary>
        /// Gets or sets the base type for the originator compensation. Allowable
        /// values are E_PercentBaseT.LoanAmount and E_PercentBaseT.TotalLoanAmount.
        /// </summary>
        /// <value>
        /// The loan amount that should be used when calculating the non-fixed amount.
        /// </value>
        public E_PercentBaseT BaseT 
        { 
            get 
            { 
                return this.baseT; 
            }

            set
            {
                if (value != E_PercentBaseT.LoanAmount && value != E_PercentBaseT.TotalLoanAmount)
                {
                    throw new ArgumentOutOfRangeException();
                }

                this.baseT = value;
            }
        }

        /// <summary>
        /// Gets or sets the minimum originator compensation amount before adding
        /// the fixed amount.
        /// </summary>
        /// <value>
        /// The minimum originator compensation before adding the fixed amount.
        /// </value>
        public decimal MinAmount { get; set; }

        /// <summary>
        /// Gets or sets the maximum originator compensation amount before adding
        /// the fixed amount.
        /// </summary>
        /// <value>
        /// The maximum originator compensation before adding the fixed amount.
        /// </value>
        public decimal MaxAmount { get; set; }

        /// <summary>
        /// Gets or sets the fixed amount that will be added to the percentage
        /// of the base amount.
        /// </summary>
        /// <value>
        /// The fixed amount that will be added to the percentage
        /// of the base amount.
        /// </value>
        public decimal FixedAmount { get; set; }
    }
}
