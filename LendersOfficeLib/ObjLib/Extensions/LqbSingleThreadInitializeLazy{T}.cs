﻿namespace LendersOffice.ObjLib.Extensions
{
    using System;
    using System.Threading;

    /// <summary>
    /// Provides a wrapper for <see cref="Lazy{T}"/> combining
    /// the allowed retry on error behavior of <see cref="LazyThreadSafetyMode.PublicationOnly"/>
    /// with locking to ensure only one thread can initialize
    /// <see cref="Lazy{T}.Value"/>.
    /// </summary>
    /// <typeparam name="T">
    /// The type of object that is being lazily initialized.
    /// </typeparam>
    public class LqbSingleThreadInitializeLazy<T>
    {
        /// <summary>
        /// Provide locking for initialization.
        /// </summary>
        private readonly object lazyLock = new object();

        /// <summary>
        /// The wrapped instance.
        /// </summary>
        private readonly Lazy<T> wrapped;

        /// <summary>
        /// Initializes a new instance of the <see cref="LqbSingleThreadInitializeLazy{T}"/>
        /// class.
        /// </summary>
        /// <param name="valueFactory">
        /// The factory creating values of type <typeparamref name="T"/>.
        /// </param>
        public LqbSingleThreadInitializeLazy(Func<T> valueFactory)
        {
            this.wrapped = new Lazy<T>(valueFactory, LazyThreadSafetyMode.PublicationOnly);
        }

        /// <summary>
        /// Gets a value that indicates whether a value has been created 
        /// for this <see cref="LqbSingleThreadInitializeLazy{T}"/> instance.
        /// </summary>
        public bool IsValueCreated => this.wrapped.IsValueCreated;

        /// <summary>
        /// Gets the lazily initialized value of the current 
        /// <see cref="LqbSingleThreadInitializeLazy{T}"/> instance.
        /// </summary>
        public T Value
        {
            get
            {
                if (this.wrapped.IsValueCreated)
                {
                    return this.wrapped.Value;
                }

                lock (this.lazyLock)
                {
                    return this.wrapped.Value;
                }
            }
        }
    }
}
