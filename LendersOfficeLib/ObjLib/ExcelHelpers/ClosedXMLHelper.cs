﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFormat.OpenXml.Drawing.Charts;
using ClosedXML.Excel;

namespace LendersOffice.ObjLib.ExcelHelpers
{
    public static  class ClosedXMLHelper
    {
        public static System.Data.DataTable GenerateDataTable(string filepath, int worksheetNumber)
        {
            XLWorkbook workbook = new XLWorkbook(filepath, XLEventTracking.Disabled);
            IXLWorksheet worksheet = workbook.Worksheet(1);


            IXLRange range = worksheet.Range(worksheet.FirstCellUsed(), worksheet.LastCellUsed());

            int col = range.ColumnCount();
            int row = range.RowCount();

            System.Data.DataTable datatable = new System.Data.DataTable();

            for (int i =1; i <= col; i++)
            {
                IXLCell column = worksheet.Cell(1, i);
                datatable.Columns.Add(column.Value.ToString());
            }

            
            foreach (var item in range.Rows(p=>true).Skip(1))
            {
                var array = new object[col];
                for (int y = 1; y <= col; y++)
                {
                    array[y - 1] = item.Cell(y).Value;
                }
                datatable.Rows.Add(array);
            }
            return datatable;
        }
    }
}
