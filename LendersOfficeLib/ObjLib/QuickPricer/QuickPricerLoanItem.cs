﻿using DataAccess;
using System;
using System.Data.Common;
using System.Data.SqlClient;
namespace LendersOffice.QuickPricer
{
    public class QuickPricerLoanItem
    {
        public E_sLPurposeT sLPurposeTPe { get; set; }
        public bool? sIsCreditQualifying { get; set; }
        public string sHouseValPe_rep { get; set; }
        public string sLAmtCalcPe_rep { get; set; }
        public string sApprValPe_rep { get; set; }
        public string sSpStatePe { get; set; }
        public string sSpCounty { get; set; }
        public string sSpCity { get; set; }
        public string sSpZip { get; set; }
        public string sCreditScoreType1_rep { get; set; }
        public E_sProdSpT sProdSpT { get; set; }
        public E_sProdSpStructureT sProdSpStructureT
        {
            get
            {
                // 10/29/2010 dd - OPM 54855 - Automatically perform best guess for structure type base on sProdSpT.
                switch (sProdSpT)
                {
                    case E_sProdSpT.SFR:
                    case E_sProdSpT.PUD:
                    case E_sProdSpT.Manufactured:
                    case E_sProdSpT.Modular:
                        return E_sProdSpStructureT.Detached;
                    case E_sProdSpT.TwoUnits:
                    case E_sProdSpT.ThreeUnits:
                    case E_sProdSpT.FourUnits:
                    case E_sProdSpT.Rowhouse:
                    case E_sProdSpT.Condo:
                    case E_sProdSpT.CoOp:
                    case E_sProdSpT.Townhouse:
                    case E_sProdSpT.Commercial:
                    case E_sProdSpT.MixedUse:
                        return E_sProdSpStructureT.Attached;
                    default:
                        throw new UnhandledEnumException(sProdSpT);
                }
            }
        }
        public E_sOccT sOccTPe { get; set; }

        //opm 32479 fs 07/21/09
        public string sDownPmtPcPe_rep { get; set; }
        public string sEquityPe_rep { get; set; }
        public string sLtvROtherFinPe_rep { get; set; }
        public string sProOFinBalPe_rep { get; set; }
        public string sCltvRPe_rep { get; set; }
        public bool sHas2ndFinPe { get; set; }
        public bool sProdImpound { get; set; }

        // 10/28/2011 dd - Request by Pacific Union to allow DTI factor to be include in Quick Pricer
        public string sTransmOMonPmtPe_rep { get; set; }
        public string sPrimAppTotNonspIPe_rep { get; set; }
        // 7/16/2015 ir - OPM 220009
        public string sAppTotLiqAsset_rep { get; set; }

        public string sNumFinancedProperties { get; set; }

        // 6/27/2012 dd - Addition fields to Quick Pricer for REMN integration.
        public string sProOHExpPe_rep { get; set; }
        public string sProdCondoStories_rep { get; set; }
        public E_sFhaCondoApprovalStatusT? sFhaCondoApprovalStatusT { get; set; }
        public bool? sProdIsSpInRuralArea { get; set; }
        public bool? sProdIsCondotel { get; set; }
        public bool? sProdIsNonwarrantableProj { get; set; }
        public bool? sProdIsFhaMipFinanced { get; set; }
        public bool? sProdIsVaFundingFinanced { get; set; }
        public bool? sProdIsUsdaRuralHousingFeeFinanced { get; set; }
        public bool? sIsSelfEmployed { get; set; }
        public bool? sGfeIsTPOTransaction { get; set; } // Hijack to use as HomeFixer product
        //public bool? sLenderFeeBuyoutRequested { get; set; } //4/2/2014 ir - sLenderFeeBuyoutRequested Required for REMN PML0200, OPM 175882
        public E_sLenderFeeBuyoutRequestedT? sLenderFeeBuyoutRequestedT { get; set; } //8/13/2014 je - Changed sLenderFeeBuyoutRequested to dropdown/Enum sLenderFeeBuyoutRequestedT, OPM 186233
        public bool? sIsCSelfEmployed { get; set; }
        public E_aProdCitizenT? aProdBCitizenT { get; set; }
        public E_aProdCitizenT? aProdCCitizenT { get; set; }
        public E_sLtv80TestResultT sLtv80TestResultT { get; set; }
        public E_sProdMIOptionT sProdMIOptionT { get; set; }
        public E_sProdConvMIOptionT sProdConvMIOptionT { get; set; }
        public bool sIsIOnlyPe { get; set; }
        public string sLeadSrcDesc { get; set; } // 11/4/2013 dd - OPM 139128 - Allow REMN integration to pass in broker information.
        // 12/12/2012 BB - Fields for Stockton QP (OPM 107321)
        public bool bUseBestPricing { get; set; }

        public string sProdRLckdDays_rep { get; set; }

        public bool sProdFilterDue10Yrs { get; set; }
        public bool sProdFilterDue15Yrs { get; set; }
        public bool sProdFilterDue20Yrs { get; set; }
        public bool sProdFilterDue25Yrs { get; set; }
        public bool sProdFilterDue30Yrs { get; set; }
        public bool sProdFilterDue40Yrs { get; set; }   //OUTDATED. Case 65859
        public bool sProdFilterDueOther { get; set; }
        public bool sProdFilterFinMethFixed { get; set; }
        public bool sProdFilterFinMethLess1YrArm { get; set; }  //OUTDATED. Case 65859
        public bool sProdFilterFinMeth2YrsArm { get; set; } //OUTDATED. Case 65859
        public bool sProdFilterFinMeth3YrsArm { get; set; }
        public bool sProdFilterFinMeth5YrsArm { get; set; }
        public bool sProdFilterFinMeth7YrsArm { get; set; }
        public bool sProdFilterFinMeth10YrsArm { get; set; }
        public bool sProdFilterFinMethOther { get; set; }
        public bool sProdIncludeNormalProc { get; set; }
        public bool sProdIsDuRefiPlus { get; set; }
        public bool sProdIncludeMyCommunityProc { get; set; }
        public bool sProdIncludeHomePossibleProc { get; set; }
        public bool sProdIncludeFHATotalProc { get; set; }
        public bool sProdIncludeVAProc { get; set; }
        public bool sProdIncludeUSDARuralProc { get; set; }
        public bool sProdFilterPmtTPI { get; set; }
        public bool sProdFilterPmtTIOnly { get; set; }
        public E_sProd3rdPartyUwResultT? sProd3rdPartyUwResultT { get; set; }
        public bool UseLoanValueForsProd3rdPartyUwResultT { get; set; } = false;
        public string sCaseAssignmentD { get; set; } // 1/13/2014 dd - OPM 148898 - Allow webservice to set Case File Assignment Date for quick pricer
        public string sCustomPMLField1 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField2 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField3 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField4 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField5 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField6 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField7 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField8 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField9 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField10 { get; set; } // 1/13/2014 dd - Allow Quick Pricer to set custom field.
        public string sCustomPMLField11 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField12 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField13 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField14 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField15 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField16 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField17 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField18 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField19 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public string sCustomPMLField20 { get; set; } // 8/17/2018 jk - moar custom fields for quick pricer
        public E_sConvSplitMIRT sConvSplitMIRT { get; set; } // OPM 180562.  
        public string sEstCloseD_rep { get; set; } // OPM - 187119.
        public bool DisableFeeService { get; set; } // OPM 236392 - Allow Fee Service to be disabled from Loan.asmx.

        // public property in order for XmlSerialization to work.
        public string ManualCashoutAmt { get; set;}
        public void SetCashoutAmt(string _amt)
        {
            // 2/19/2014 - dd - eOPM 510739 - Allow Zillow quick pricer to set the cashout amount directly.
            ManualCashoutAmt = _amt;
        }
        public string sProdCashoutAmt_rep
        {
            get
            {
                if (sLPurposeTPe == E_sLPurposeT.RefinCashout)
                {
                    if (string.IsNullOrEmpty(ManualCashoutAmt) == false)
                    {
                        return ManualCashoutAmt;
                    }
                    else
                    {
                        return "$1.00";
                    }
                }
                else
                {
                    return "$0.00";
                }
            }
        }
        public bool IsDebugMode { get; set; }

        public E_sProdCalcEntryT sProdCalcEntryT {get; set;}

        public string sLtvRPe_rep { get; set; }


        public E_sLienPosT? sLienPosTPe { get; set; }
        public E_sSubFinT? sSubFinT { get; set; }
        public string sSubFinPe_rep { get; set; }

        public bool IsHELOCRequest
        {
            get
            {
                // 10/25/2013 dd - Use sSubFinT to determine if we should perform HELOC pricing.
                if (sSubFinT.HasValue)
                {
                    return sSubFinT.Value == E_sSubFinT.Heloc;
                }
                return false;
            }
        }
        public QuickPricerLoanItem()
        {
            bUseBestPricing = true;
        }

        /// <summary>
        /// This method is use in Quick Pricer webservice. This method does not require user to fill out the % or the down payment and sProdCalcEntryT.
        /// This method will automatically calculate the down payment (sEquityPe) and then calculate respective %.
        /// </summary>
        /// <param name="sHouseValPe"></param>
        /// <param name="sLAmtCalcPe"></param>
        /// <param name="sProOFinBalPe"></param>
        public void SetLoanInformation(string sHouseValPe, string sLAmtCalcPe, string sProOFinBalPe)
        {
            decimal _sHouseValPe = 0;
            decimal _sLAmtCalcPe = 0;
            decimal _sProOFinBalPe = 0;
            decimal _sEquityPe = 0;

            if (!string.IsNullOrEmpty(sHouseValPe))
            {
                if (!decimal.TryParse(sHouseValPe, out _sHouseValPe))
                {
                    _sHouseValPe = 0;
                }
                if (_sHouseValPe < 0)
                    _sHouseValPe = 0; // 8/17/2009 dd - Negative House value is not allow.
            }
            if (!string.IsNullOrEmpty(sLAmtCalcPe))
            {
                if (!decimal.TryParse(sLAmtCalcPe, out _sLAmtCalcPe))
                {
                    _sLAmtCalcPe = 0;
                }
                if (_sLAmtCalcPe < 0)
                    _sLAmtCalcPe = 0; // 8/17/2009 dd - Negative Loan Amount is not allow.
            }

            if (!string.IsNullOrEmpty(sProOFinBalPe))
            {
                if (!decimal.TryParse(sProOFinBalPe, out _sProOFinBalPe))
                {
                    _sProOFinBalPe = 0;
                }
                if (_sProOFinBalPe < 0)
                    _sProOFinBalPe = 0; // 8/17/2009 dd - Negative second loan amount is not allow.
            }

            if (_sHouseValPe == 0)
            {

                sHouseValPe_rep = "0.00";
                sLAmtCalcPe_rep = "0.00";
                sLtvRPe_rep = "0.00";
                sEquityPe_rep = "0.00";
                sDownPmtPcPe_rep = "0.00";
                sProOFinBalPe_rep = "0.00";
                sLtvROtherFinPe_rep = "0.00";
                sCltvRPe_rep = "0.00";
            }
            else
            {

                _sEquityPe = _sHouseValPe - _sLAmtCalcPe - _sProOFinBalPe;

                sHouseValPe_rep = ToMoneyString(_sHouseValPe);
                sLAmtCalcPe_rep = ToMoneyString(_sLAmtCalcPe);
                sLtvRPe_rep = ToRateString(_sLAmtCalcPe / _sHouseValPe * 100);

                if ((_sLAmtCalcPe / _sHouseValPe) > .8M)
                {
                    sLtv80TestResultT = E_sLtv80TestResultT.Over80;
                }

                sEquityPe_rep = ToMoneyString(_sEquityPe);
                sDownPmtPcPe_rep = ToRateString(_sEquityPe / _sHouseValPe * 100);

                sProOFinBalPe_rep = ToMoneyString(_sProOFinBalPe);
                sLtvROtherFinPe_rep = ToRateString(_sProOFinBalPe / _sHouseValPe * 100);
                sHas2ndFinPe = _sProOFinBalPe > 0;
                sCltvRPe_rep = ToRateString((_sLAmtCalcPe + _sProOFinBalPe) / _sHouseValPe * 100);
                sProdCalcEntryT = E_sProdCalcEntryT.LAmt;

            }
        }

        private string ToRateString(decimal value)
        {
            return Math.Round(value, 3).ToString();
        }
        private string ToMoneyString(decimal value)
        {
            return Math.Round(value, 2).ToString();
        }

        public void getCityCountyState()
        {
            string _zipCode = sSpZip;
            string _city = "";
            string _county = "";
            string _state = "";

            if (String.IsNullOrEmpty(_zipCode) || !System.Text.RegularExpressions.Regex.IsMatch(_zipCode, "[0-9]{5}"))
            {
                return;
            }

            bool isValid = Tools.GetCountyCityStateByZip(_zipCode, out _county, out _city, out _state);

            if (isValid)
            {

                sSpCity = string.IsNullOrEmpty(sSpCity) ? _city : sSpCity;
                sSpCounty = string.IsNullOrEmpty(sSpCounty) ? _county : sSpCounty;

                // 2/28/2012 dd - eOPM 338104 - We will always use the state information from our database.
                sSpStatePe = _state;
            }
        }

        private bool fromConsumerPortal = false;

        /// <summary>
        /// This property is used in Quick Pricer webservice to identify whether the call is being generated from Consumer Portal or not. 
        /// By default, it's false
        /// </summary>
        public bool FromConsumerPortal
        {
            get { return fromConsumerPortal; }
            set { fromConsumerPortal = value; }
        }
    }

}
