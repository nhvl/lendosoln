﻿namespace LendersOffice.ObjLib.QuickPricer
{
    using System.Linq;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.ConversationLog;
    using LendersOffice.Email;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using LqbGrammar.Drivers.ConversationLog;
    using LqbGrammar.Exceptions;

    /// <summary>
    /// Non-QM QuickPricer Helper class.
    /// </summary>
    public class NonQmQpHelper
    {
        /// <summary>
        /// Sends an email regarding the Non-QM QuickPricer lead submitted to the email address entered on the Originator Portal Configuration page.
        /// </summary>
        /// <param name="principal">Abstract user principal.</param>
        /// <param name="nonQmQpData">Non-QM QuickPricer data.</param>
        /// <param name="loanNm">Lead's name.</param>
        public static void SendEmailToConfiguredAddrForNonQmQp(AbstractUserPrincipal principal, NonQmQpData nonQmQpData, string loanNm)
        {
            if (string.IsNullOrWhiteSpace(principal.BrokerDB.OpNonQmMsgReceivingEmail))
            {
                return;
            }

            string brokerName = principal.BrokerDB.Name;

            if (principal.Type == "P")
            {
                brokerName = PmlBroker.RetrievePmlBrokerNameById(principal.PmlBrokerId, principal.BrokerId);
            }

            var email = new CBaseEmail(principal.BrokerDB.BrokerID)
            {
                From = ConstStage.DefaultDoNotReplyAddress,
                Subject = loanNm + " - " + nonQmQpData.BFirstNm + " " + nonQmQpData.BLastNm + " - " + brokerName,
                DisclaimerType = E_DisclaimerType.NORMAL,
                To = principal.BrokerDB.OpNonQmMsgReceivingEmail,
                Message = nonQmQpData.MsgToLender
            };

            email.Send();
        }

        /// <summary>
        /// Posts a message to the conversation log regarding the Non-QM QuickPricer lead submitted to the category set on the Originator Portal Configuration page.
        /// </summary>
        /// <param name="principal">Abstract user principal.</param>
        /// <param name="nonQmQpData">Non-QM QuickPricer data.</param>
        /// <param name="loanRefNm">Lead's reference number.</param>
        public static void PostConLogMsgForNonQmQP(AbstractUserPrincipal principal, NonQmQpData nonQmQpData, string loanRefNm)
        {
            var categoryId = principal.BrokerDB.OpConLogCategoryId;

            if (categoryId == -1 || string.IsNullOrWhiteSpace(nonQmQpData.MsgToLender))
            {
                return;
            }

            var securityToken = SecurityService.CreateToken();

            var identifier = ResourceIdentifier.CreateForLoanFile(securityToken.BrokerId, loanRefNm);
            if (identifier == null)
            {
                throw new LqbGrammar.Exceptions.ServerException(ErrorMessage.SystemError);
            }

            var resourceId = new ResourceId()
            {
                Type = ResourceType.LoanFile,
                Identifier = identifier.Value
            };

            var categoryReference = ConversationLogHelper.GetAllCategories(securityToken, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort).FirstOrDefault(c => c.Identity.Id == categoryId)?.Identity;

            if (categoryReference == null)
            {
                throw new LqbGrammar.Exceptions.DeveloperException(ErrorMessage.BadConfiguration);
            }
            
            var comment = new Comment()
            {
                Commenter = PersonName.CreateWithReplace(principal.FirstName, principal.LastName).Value,
                CommenterId = UserAccountIdentifier.Create(principal.UserId.ToString()).Value,
                Value = CommentLogEntry.Create(nonQmQpData.MsgToLender).Value,
                Identity = new CommentReference()

                // the below properties get set on Post:
                // Created = LqbEventDate.Create(DateTime.Now.ToString()).Value
                // Depth
                // Identity's Id.                 
            };

            ConversationLogHelper.Post(securityToken, resourceId, categoryReference, comment, null, ConstStage.ConversationLogIP, ConstStage.ConversationLogPort, bypassPermissionCheck: true);
        }
    }
}
