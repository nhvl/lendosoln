﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LendersOfficeApp.los.RatePrice;
using DataAccess;

namespace LendersOffice.QuickPricer
{
    public class QuickPricerResultRateItemComparer : IComparer<QuickPricerResultRateItem>
    {
        #region IComparer<QuickPricerResultRateItem> Members

        public int Compare(QuickPricerResultRateItem x, QuickPricerResultRateItem y)
        {
            int ret = x.PointValue.CompareTo(y.PointValue);
            if (ret == 0)
            {
                decimal r0 = 0;
                decimal r1 = 0;
                decimal.TryParse(x.Rate, out r0);
                decimal.TryParse(y.Rate, out r1);

                return r0.CompareTo(r1);
            }
            return ret;
        }

        #endregion
    }

    public class QuickPricerResultLoanProgram
    {
        private string m_name;
        private int m_term;
        private int m_due;
        private int m_lRAdj1stCapMon;
        private E_sFinMethT m_finMethT;
        private string m_productType = "";
        private E_sLT m_lT;
        private int m_iOnlyMon;
        private string m_sInvestorName = "";
        private bool m_IsBlockedRateLockSubmission = false;

        private List<QuickPricerResultRateItem> m_list;

        public QuickPricerResultLoanProgram(string name)
        {
            m_name = name;
            m_list = new List<QuickPricerResultRateItem>();
        }
        public QuickPricerResultLoanProgram(RateMergeGroup rateMergeGroup)
        {
            m_name = rateMergeGroup.Name;
            m_term = rateMergeGroup.lTerm;
            m_due = rateMergeGroup.lDue;
            m_lRAdj1stCapMon = rateMergeGroup.lRAdj1stCapMon;
            m_productType = rateMergeGroup.lProductType;
            m_finMethT = rateMergeGroup.lFinMethT;
            m_list = new List<QuickPricerResultRateItem>();
            m_lT = rateMergeGroup.lLT;
            m_iOnlyMon = rateMergeGroup.lIOnlyMon;
        }
        public int Term
        {
            get { return m_term; }
        }
        public int Due
        {
            get { return m_due; }
        }
        public int RAdj1stCapMon
        {
            get { return m_lRAdj1stCapMon; }
        }
        public string ProductType
        {
            get { return m_productType; }
        }
        public E_sFinMethT FinMethT
        {
            get { return m_finMethT; }
        }
        public E_sLT LoanType
        {
            get { return m_lT; }
        }
        public int IOnlyMon
        {
            get { return m_iOnlyMon; }
        }
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        public bool IsBlockedRateLockSubmission
        {
            get { return m_IsBlockedRateLockSubmission; }
            set { m_IsBlockedRateLockSubmission = value; }
        }
        public List<QuickPricerResultRateItem> RateOptions
        {
            get { return m_list; }
        }
        public string InvestorName
        {
            get { return m_sInvestorName; }
            set { m_sInvestorName = value; }
        }

    }
}
