﻿namespace LendersOffice.ObjLib.QuickPricer
{
    /// <summary>
    /// Manually entered housing history.
    /// </summary>
    public enum ManuallyEnteredHousingHistory
    {
        _0x30 = 0,
        _1x30 = 1,
        _0x60 = 2,
        _0x90 = 3,
        _1x90Plus = 4
    }
}
