﻿using System.Collections.Generic;
using System.Text;
using LendersOffice.Common;
using System;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.AntiXss;
using System.Collections;
using LendersOffice.ObjLib.PriceGroups;

namespace LendersOffice.QuickPricer
{
    public class QuickPricerResult
    {
        private List<QuickPricerResultLoanProgram> m_programList = new List<QuickPricerResultLoanProgram>();

        private List<CApplicantPriceXml> m_applicantPriceList = new List<CApplicantPriceXml>();

        private List<CApplicantPriceXml> m_ineligibleLoanProgramList = new List<CApplicantPriceXml>();

        public List<QuickPricerResultLoanProgram> LoanPrograms
        {
            get { return m_programList; }
        }

        public List<CApplicantPriceXml> IneligibleLoanProgramList
        {
            get { return m_ineligibleLoanProgramList; }
        }

        public void AddIneligible(CApplicantPriceXml applicantPrice)
        {
            if (null == applicantPrice)
            {
                return;
            }
            if (applicantPrice.Status == E_EvalStatus.Eval_Ineligible)
            {
                m_ineligibleLoanProgramList.Add(applicantPrice);
            }
        }
        public CApplicantPriceXml GetApplicantPrice(Guid lLpTemplateId)
        {
            foreach (var o in m_applicantPriceList)
            {
                if (o.lLpTemplateId == lLpTemplateId)
                {
                    return o;
                }
            }
            return null;
        }
        public void AddApplicantPrice(CApplicantPriceXml applicantPrice)
        {
            if (null == applicantPrice)
                return;

            foreach (CApplicantPriceXml o in m_applicantPriceList)
            {
                if (o.lLpTemplateId == applicantPrice.lLpTemplateId)
                    return; // ApplicantPrice already existed in the list.
            }
            m_applicantPriceList.Add(applicantPrice);
        }
        public string DebugInfo
        {
            get;
            set;
        }
        public string DetailTimingInfo { get; set; }
        public Guid RequestId { get; set; }
        private bool m_hasError = false;
        private string m_errorMessage = "";
        private DateTime m_timeStamp;
        private string m_priceGroupName = "";

        public bool HasError
        {
            get { return m_hasError; }
            set { m_hasError = value; }
        }
        public string ErrorMessage
        {
            get { return m_errorMessage; }
            set { m_errorMessage = value; }
        }
        public string PriceGroupName
        {
            get { return m_priceGroupName; }
            set { m_priceGroupName = value; }
        }
        public PriceGroup PriceGroup
        {
            get;
            set;
        }
        public DateTime TimeStamp
        {
            get { return m_timeStamp; }
            set { m_timeStamp = value; }
        }
        private int GetIndex(Guid lLpTemplateId)
        {
            int ret = -1;
            for (int i = 0; i < m_applicantPriceList.Count; i++)
            {
                if (m_applicantPriceList[i].lLpTemplateId == lLpTemplateId)
                    return i;
            }
            return ret;
        }
        public string ToJavascriptObject(bool displayIn100)
        {
            StringBuilder sb = new StringBuilder();

            string header = "POINT";
            if (displayIn100)
            {
                header = "PRICE";
            }

            sb.Append("[");
            sb.Append(SafeJsonString(Tools.GetDateTimeDescription(m_timeStamp))); //0
            sb.Append(",[\"RATE\",\""+  header +"\",\"PAYMENT\",\"\",\"\"]"); // 1
            sb.Append(",");
            ToJson(sb, m_programList); // 2
            sb.Append(",");
            sb.Append(SafeJsonString(m_priceGroupName)); // 3 - Price Group. TODO
            sb.Append(",");
            ToJson(sb, m_applicantPriceList, displayIn100); //4
            sb.Append("]");

            return sb.ToString();
        }

        public string ConvertIneligibleListToJavascriptObject()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("[");
            for (int i = 0; i < m_ineligibleLoanProgramList.Count; i++)
            {
                if (i != 0)
                {
                    sb.Append(",");

                }
                CApplicantPriceXml product = m_ineligibleLoanProgramList[i];
                sb.Append("{\"Name\":" + SafeJsonString(product.lLpTemplateNm));
                sb.Append(",\"Reasons\":" + ObsoleteSerializationHelper.JavascriptJsonSerialize(product.DisqualifiedRuleList));
                sb.Append("}");
            }
            sb.Append("]");
            return sb.ToString();
        }
        private void ToJson(StringBuilder sb, List<CApplicantPriceXml> list, bool displayIn100)
        {
            sb.Append("[");
            for (int i = 0; i < list.Count; i++)
            {
                CApplicantPriceXml o = list[i];
                if (i != 0)
                    sb.Append(",");
                ToJson(sb, o, displayIn100);
            }
            sb.Append("]");

        }
        private void ToJson(StringBuilder sb, List<QuickPricerResultLoanProgram> list)
        {
            sb.Append("[");
            for (int i = 0; i < list.Count; i++)
            {
                QuickPricerResultLoanProgram o = list[i];
                if (i != 0)
                    sb.Append(",");
                ToJson(sb, o);
            }
            sb.Append("]");
        }
        private void ToJson(StringBuilder sb, List<QuickPricerResultRateItem> list)
        {
            sb.Append("[");
            for (int i = 0; i < list.Count; i++)
            {
                QuickPricerResultRateItem o = list[i];
                if (i != 0)
                    sb.Append(",");
                ToJson(sb, o);
            }
            sb.Append("]");
        }
        private void ToJson(StringBuilder sb, QuickPricerResultLoanProgram item)
        {
            sb.Append("[");
            sb.Append(SafeJsonString(item.Name));
            sb.Append(",");
            sb.Append(item.IsBlockedRateLockSubmission ? "\"1\"": "\"0\"");
            sb.Append(",");
            ToJson(sb, item.RateOptions);
            sb.Append("]");
        }
        private void ToJson(StringBuilder sb, QuickPricerResultRateItem item)
        {
            sb.AppendFormat("[{0},{1},{2},{3},{4},{5},{6}]",
                SafeJsonString(item.Rate),
                SafeJsonString(item.Point),
                SafeJsonString(item.Payment),
                SafeJsonString(item.Dti),
                AspxTools.JsNumeric(GetIndex(item.lLpTemplateId)), 
                SafeJsonString(item.OpCode),
                SafeJsonString(item.IsExpired? "1" : "0"));
        }
        private void ToJson(StringBuilder sb, CApplicantPriceXml item, bool displayIn100)
        {
            sb.AppendFormat("[{0},", SafeJsonString(item.lLpTemplateNm));
            sb.Append("[");
            ToJsonAdjustRateList(sb, item.AdjustDescs, displayIn100); // 0
            sb.Append("]]");
        }
        private void ToJsonAdjustRateList(StringBuilder sb, List<CAdjustItem> list, bool displayIn100)
        {
            sb.Append("[");
            for (int i = 0; i < list.Count; i++)
            {
                CAdjustItem o = (CAdjustItem)list[i];
                if (i != 0)
                    sb.Append(",");
                ToJson(sb, o, displayIn100);
            }
            sb.Append("]");
        }
        private void ToJson(StringBuilder sb, CAdjustItem item, bool displayIn100)
        {
            sb.Append("[");
            sb.Append(SafeJsonString(item.Rate)); //0
            sb.Append(",");
            sb.Append(SafeJsonString(CAdjustItem.AdjustFeeInResult(displayIn100, item.Fee))); // 1
            sb.Append(",");
            sb.Append(SafeJsonString(item.Description)); // 2
            sb.Append(",");
            sb.Append(SafeJsonString(item.OptionCode)); //3
            sb.Append("]");
        }

        private string SafeJsonString(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return "\"\"";
            }
            return ObsoleteSerializationHelper.JavascriptJsonSerialize(s);
        }

    }
}
