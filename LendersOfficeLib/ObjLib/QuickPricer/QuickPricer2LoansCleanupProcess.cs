﻿// <copyright file="QuickPricer2LoansCleanupProcess.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   7/16/2014
// </summary>
namespace LendersOffice.ObjLib.QuickPricer
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Actually deletes (as in wipes from DBs and from FileDB) loans that are determined to no longer be needed.<para></para>
    /// The reason we have an *actual* deletion is because trying to reuse these loans instead could potentially cause hard to debug issues<para></para>
    /// (like missing a column when duplicating the loan).  I would prefer not to have two different loan creation pathways as well, which<para></para>
    /// would be the case if instead we reused loans.<para></para>
    /// The loans need to be cleaned up so as not to flood the system and bog it down.<para></para>
    /// There is potential to miss things during the cleanup, but a separate process can cleanup anything that is missed.<para></para>
    /// </summary>
    public class QuickPricer2LoansCleanupProcess : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Gets the description for runnable interface.
        /// </summary>
        /// <value>Finds no longer used 'in use' sandboxed loans and deletes them.</value>
        public string Description
        {
            get { return "Finds no longer used 'in use' sandboxed loans and deletes them."; }
        }

        /// <summary>
        /// Deletes loans that are thought to no longer be needed.
        /// </summary>
        public void Run()
        {
            if (DateTime.Now.Hour >= 23 || DateTime.Now.Hour < 3)
            {
                Tools.LogInfo("Not skipping QuickPricer2LoansCleanupProcess.");
            }
            else
            {
                Tools.LogInfo("Skipping QuickPricer2LoansCleanupProcess as can only run between 11pm and 3am.");
                return;
            }

            var oldThreadPrincipal = Thread.CurrentPrincipal;
            Thread.CurrentPrincipal = SystemUserPrincipal.QP2SystemUser;

            // if we disable a broker, we should consider cleaning their loans too.
            foreach (var brokerID in Tools.GetAllActiveBrokers()) 
            {
                var broker = BrokerDB.RetrieveById(brokerID);
                if (broker.QuickPricerTemplateId == Guid.Empty)
                {
                    Tools.LogInfo("Skipping " + broker.Name + " " + broker.BrokerID + " because they have no quickpricer templateid");
                    continue;
                }
                
                this.DeleteOldInUseLoansAtBroker(broker);
                this.DeleteUnusedLoansNotMatchingQPTemplateForBroker(broker);
            }

            Thread.CurrentPrincipal = oldThreadPrincipal;

            // TODO: delete loans whose creation process was aborted. *But*, do not delete them if they are mid-creation.
            // flag all loans that are in quickpricer sandboxed mode but aren't in the pool (abandoned).  possibly another process b/c it could be slower.
            // there's a problem with finding "abandoned loans"... what if the creation process is creating a loan and hasn't added it to the pool yet.
            // --> Need to filter by some sort of date of creation on the loan too.  (see sCreatedD in loan_file_[a/cache] with default of getdate()
        }

        /// <summary>
        /// Looks at the minimum number of fields to determine if there are abandoned loans to clean up. These loans are invalid and tend to be old.
        /// </summary>
        /// <param name="brokerId">The broker identifier to target.</param>
        /// <param name="deletionEndDate">The date of when to stop deletion.</param>
        public void CleanupAbandonedLoans(Guid brokerId, DateTime deletionEndDate)
        {
            Thread.CurrentPrincipal = SystemUserPrincipal.QP2SystemUser;
            LinkedList<Guid> loansBelongingToBroker = new LinkedList<Guid>();
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerId);
            DbConnectionInfo con = DbConnectionInfo.GetConnectionInfo(brokerId);
            var parameters = new SqlParameter[] { new SqlParameter("@BrokerId", brokerId), new SqlParameter("@DeleteBeforeDate", deletionEndDate) };
            using (var reader = StoredProcedureHelper.ExecuteReader(con, "QUICKPRICER_2_FIND_ABANDONED_LOANS", parameters))
            {
                while (reader.Read())
                {
                    Guid b = (Guid)reader["sbrokerid"];
                    if (b != brokerId)
                    {
                        Tools.LogError($"Mismatching BrokerId {brokerId} != {b}");
                        return;
                    }

                    loansBelongingToBroker.AddLast((Guid)reader["slid"]);
                }
            }

            Console.WriteLine($"Gathered {loansBelongingToBroker.Count} to delete for broker {brokerDB.CustomerCode} - {brokerDB.Name} - {brokerId}.");

            bool deleted;
            string reasonDenied;
            foreach (var slid in loansBelongingToBroker)
            {
                try
                {
                    deleted = QuickPricer2LoanPoolManager.TryBestEffortDeleteLoan(brokerDB, slid, out reasonDenied);
                    if (!deleted)
                    {
                        if (reasonDenied == "File has a rate_monitor and is a valid loan, so will not delete.")
                        {
                            // we expect not to delete "inuse" valid qp2 files with rate monitors on them for some brokers.
                            Console.WriteLine($"Skipping file {slid} because {reasonDenied}");
                        }
                        else
                        {
                            Console.WriteLine("Could not delete " + slid + " because " + reasonDenied);
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Deleted file {slid}");
                    }
                }
                catch (SqlException e)
                {
                    // skip for now, we'll run across this loan again and again so there's no point in blocking the process
                    Console.WriteLine("Failed to delete " + slid + " at broker ", e);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to delete " + slid, e);
                    throw; // fixing build.
                }
            }
        }

        /// <summary>
        /// Actually deletes "in use" loans that are sufficiently old at the given broker. <para></para>
        /// It does not merely mark the loan invalid. <para></para>
        /// </summary>
        /// <param name="broker">The broker whose loans we'll delete.</param>
        private void DeleteOldInUseLoansAtBroker(BrokerDB broker)
        {
            foreach (var loanID in QuickPricer2LoanPoolManager.FindOldInUseLoansAtBroker(broker))
            {
                Trace.CorrelationManager.ActivityId = loanID;
                string reasonDenied;
                var deleted = false;
                try
                {
                    deleted = QuickPricer2LoanPoolManager.TryBestEffortDeleteLoan(broker, loanID, out reasonDenied);
                    if (!deleted)
                    {
                        if (reasonDenied == "File has a rate_monitor and is a valid loan, so will not delete.")
                        {
                            // we expect not to delete "inuse" valid qp2 files with rate monitors on them for some brokers.
                            Tools.LogInfo(string.Format(
                                "skipping deletion of {0} at {1} because {2}",
                                loanID, // {0}
                                broker.AsSimpleStringForLog(), // {1}
                                reasonDenied)); // {2}
                        }
                        else
                        {
                            Tools.LogErrorWithCriticalTracking("Could not delete " + loanID + " at broker " + broker.AsSimpleStringForLog() + " because " + reasonDenied);
                        }
                    }
                }
                catch (SqlException e)
                {
                    // skip for now, we'll run across this loan again and again so there's no point in blocking the process
                    Tools.LogErrorWithCriticalTracking("Failed to delete " + loanID + " at broker " + broker.AsSimpleStringForLog(), e);
                }
                catch (Exception e)
                {
                    Tools.LogErrorWithCriticalTracking("Failed to delete " + loanID + " at broker " + broker.AsSimpleStringForLog(), e);
                    throw; // fixing build.
                }
            }
        }

        /// <summary>
        /// Actually deletes unused loans that don't match the broker's quick pricer template (file version or id). <para></para>
        /// It does not merely mark the loan invalid. <para></para>
        /// </summary>
        /// <param name="broker">The broker whose loans we'll delete.</param>
        private void DeleteUnusedLoansNotMatchingQPTemplateForBroker(BrokerDB broker)
        {
            foreach (var loanID in QuickPricer2LoanPoolManager.FindUnusedLoansNotMatchingQPTemplateAtBroker(broker))
            {
                Trace.CorrelationManager.ActivityId = loanID;
                string reasonDenied;
                var deleted = false;

                try
                {
                    deleted = QuickPricer2LoanPoolManager.TryBestEffortDeleteLoan(broker, loanID, out reasonDenied);
                    if (!deleted)
                    {
                        Tools.LogErrorWithCriticalTracking("Could not delete " + loanID + " at broker " + broker.AsSimpleStringForLog() + " because " + reasonDenied);
                    }
                }
                catch (SqlException e)
                {
                    // skip for now, we'll run across this loan again and again so there's no point in blocking the process
                    Tools.LogErrorWithCriticalTracking("Failed to delete " + loanID + " at broker " + broker.AsSimpleStringForLog(), e);
                }
                catch (Exception e)
                {
                    Tools.LogErrorWithCriticalTracking("Failed to delete " + loanID + " at broker " + broker.AsSimpleStringForLog(), e);
                    throw; // fixing build.
                }
            }
        }
    }
}
