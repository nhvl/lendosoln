﻿// <copyright file="QuickPricer2LoansCreationInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   9/4/2014
// </summary>
namespace LendersOffice.ObjLib.QuickPricer
{
    using System;

    /// <summary>
    /// A small class intended for QuickPricer 2.0 loan creation queue.
    /// </summary>
    public class QuickPricer2LoansCreationInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="QuickPricer2LoansCreationInfo" /> class.
        /// </summary>
        /// <param name="brokerID">The id of the broker/lender.</param>
        /// <param name="userID">NULL or the id of the user.</param>
        /// <param name="userType">NULL or the type of the user.</param>
        /// <param name="numLoansToCreatePerUser">NULL (the minimum) or the number of loans for the user(s).</param>
        public QuickPricer2LoansCreationInfo(
            Guid brokerID,
            Guid? userID,
            string userType,
            int? numLoansToCreatePerUser)
        {
            this.BrokerID = brokerID;
            this.UserID = userID;
            this.UserType = userType;
            this.NumLoansToCreatePerUser = numLoansToCreatePerUser;
            if (userID == null)
            {
                this.IsForBrokerCreation = true;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickPricer2LoansCreationInfo" /> class. <para></para>
        /// Empty constructor for serialization.
        /// </summary>
        public QuickPricer2LoansCreationInfo() 
        { 
        }

        /// <summary>
        /// Gets or sets a value indicating whether if creating for an lender's users or just an individual user. <para></para>
        /// Not intended to be set (except by deserializer).  <para></para>
        /// If true, creates queue processor will create loans for all users with the feature enabled.<para></para>
        /// If false, UserID/UserType should not be null.<para></para>
        /// </summary>
        /// <value>Gets or sets a value indicating whether if creating for an lender's users or just an individual user.</value>
        public bool IsForBrokerCreation { get; set; }

        /// <summary>
        /// Gets or sets - The ID of the broker to create the loans at.
        /// </summary>
        /// <value> The guid of the lender.</value>
        public Guid BrokerID { get; set; }

        /// <summary>
        /// Gets or sets - The ID of the user to create the loans at, or null if creating for the broker's users.
        /// </summary>
        /// <value>Null iff creating for the lender's users.</value>
        public Guid? UserID { get; set; }

        /// <summary>
        /// Gets or sets - The type of the user to create the loans at, or null if creating for the broker's users.
        /// </summary>
        /// <value> Null iff creating for the lender's users.</value>
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets - The number of loans to create per user.  Only used when creating for an individual user.  <para></para>
        /// For lender's, always creates the minimum per-user.
        /// </summary>
        /// <value>Not needed for creating for users at lender.</value>
        public int? NumLoansToCreatePerUser { get; set; }
    }
}
