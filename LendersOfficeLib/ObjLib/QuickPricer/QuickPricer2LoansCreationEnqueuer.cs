﻿// <copyright file="QuickPricer2LoansCreationEnqueuer.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   9/8/2014
// </summary>
namespace LendersOffice.ObjLib.QuickPricer
{
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// Sole purpose is enqueueing quickpricer 2.0 loan creation.
    /// </summary>
    public static class QuickPricer2LoansCreationEnqueuer
    {
        /// <summary>
        /// Put loan creation on the queue for the given user.  If numLoansToCreate is null, only makes the minimum.
        /// </summary>
        /// <param name="principal">The principal to create the loans for.</param>
        /// <param name="numLoansToCreate">Null (the minimum) or a number to create (pool size won't exceed max).</param>
        /// <returns>Returns true if no errors occurred in enqueueing.</returns>
        public static bool EnqueueUserLoanCreation(AbstractUserPrincipal principal, int? numLoansToCreate)
        {
            if (!principal.IsActuallyUsePml2AsQuickPricer)
            {
                return false;
            }

            var qp2lci = new QuickPricer2LoansCreationInfo(principal.BrokerId, principal.UserId, principal.Type, numLoansToCreate);
            Enqueue(qp2lci);

            return true;
        }

        /// <summary>
        /// Put loan creation on the queue for the given lender.  Creates the minimum for each user.
        /// </summary>
        /// <param name="broker">The broker to create the loans for.</param>
        /// <returns>Returns true if no errors occurred in enqueueing.</returns>
        public static bool EnqueueBrokerLoanCreation(BrokerDB broker)
        {
            if (E_Pml2AsQuickPricerMode.Disabled == broker.Pml2AsQuickPricerMode)
            {
                return false;
            }

            var qp2lci = new QuickPricer2LoansCreationInfo(broker.BrokerID, null, null, null);
            Enqueue(qp2lci);

            return true;
        }

        /// <summary>
        /// Helper method that actually takes what we want to do and puts it on the queue in (json) serialized way.
        /// </summary>
        /// <param name="qp2lci">What we want to do with the loan creation.</param>
        private static void Enqueue(QuickPricer2LoansCreationInfo qp2lci)
        {
            // we don't actually need subject1, but it could be useful later for inspecting the message queue.
            var subject1 = qp2lci.IsForBrokerCreation ? qp2lci.BrokerID.ToString() : qp2lci.UserID.Value.ToString();
            QuickPricer2LoansCreationQueueHandler.DBMessageQueue.Send(subject1, ObsoleteSerializationHelper.JavascriptJsonSerialize(qp2lci));
        }
    }
}
