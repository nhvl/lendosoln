﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LendersOffice.Security;
using DataAccess;
using LendersOffice.DistributeUnderwriting;
using LendersOffice.Common;
using System.Collections;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.Admin;
using LendersOffice.Constants;
using System.Text;
using LendersOffice.ObjLib.PriceGroups;
using System.Xml.Linq;

namespace LendersOffice.QuickPricer
{

    public static class QuickPricerEngine
    {
        private class LoanDictionaryItem
        {
            public string sProdLpePriceGroupName { get; set; }
            public E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT { get; set; }
            public E_sOriginatorCompensationLenderFeeOptionT sOriginatorCompensationLenderFeeOptionT { get; set; }
            public PriceGroup sPriceGroup { get; set; }

            public string ToXml()
            {
                XElement el = new XElement("item",
                    new XAttribute("sProdLpePriceGroupName", string.IsNullOrEmpty(sProdLpePriceGroupName) ? "" : sProdLpePriceGroupName),
                    new XAttribute("sOriginatorCompensationPaymentSourceT", sOriginatorCompensationPaymentSourceT.ToString("D")),
                    new XAttribute("sOriginatorCompensationLenderFeeOptionT", sOriginatorCompensationLenderFeeOptionT.ToString("D")),
                    new XAttribute("sPriceGroupId", sPriceGroup == null ? Guid.Empty : sPriceGroup.ID),
                    new XAttribute("BrokerId", sPriceGroup == null ? Guid.Empty : sPriceGroup.BrokerID)
                    );
                return el.ToString();
            }

            public static LoanDictionaryItem Create(string xml)
            {
                XElement el = XElement.Parse(xml);

                LoanDictionaryItem item = new LoanDictionaryItem();
                item.sProdLpePriceGroupName = el.Attribute("sProdLpePriceGroupName").Value;
                item.sOriginatorCompensationPaymentSourceT = (E_sOriginatorCompensationPaymentSourceT) Enum.Parse(typeof(E_sOriginatorCompensationPaymentSourceT),  
                    el.Attribute("sOriginatorCompensationPaymentSourceT").Value);
                item.sOriginatorCompensationLenderFeeOptionT = (E_sOriginatorCompensationLenderFeeOptionT) Enum.Parse(typeof(E_sOriginatorCompensationLenderFeeOptionT), 
                    el.Attribute("sOriginatorCompensationLenderFeeOptionT").Value);

                Guid priceGroupId = new Guid(el.Attribute("sPriceGroupId").Value);
                Guid brokerId = new Guid(el.Attribute("BrokerId").Value);
                if (priceGroupId != Guid.Empty)
                {
                    item.sPriceGroup = PriceGroup.RetrieveByID(priceGroupId, brokerId);
                }

                return item;
            }
        }

        private static void CacheLoanItem(Guid key, LoanDictionaryItem item)
        {
            AutoExpiredTextCache.AddToCache(item.ToXml(), TimeSpan.FromMinutes(10), key.ToString().ToLower());
        }

        private static LoanDictionaryItem GetCacheItem(Guid key)
        {
            string xml = AutoExpiredTextCache.GetFromCache(key.ToString().ToLower());
            return LoanDictionaryItem.Create(xml);
        }
        private static void RemoveCacheItem(Guid key)
        {
            AutoExpiredTextCache.ExpireCache(key.ToString().ToLower());
        }
        /// <summary>
        /// Submit to get pricing. 
        /// </summary>
        /// <returns>RequestId to check if result is ready. If Guid.Empty is returned then it has error.</returns>
        public static Guid Submit(AbstractUserPrincipal userPrincipal, QuickPricerLoanItem loanItem)
        {
            // Copy values from QuickPricerTemplate and LoanItem to the loan.
            string sProdLpePriceGroupName = null;
            E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT = E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified;
            E_sOriginatorCompensationLenderFeeOptionT sOriginatorCompensationLenderFeeOptionT;
            PriceGroup sPriceGroup;

            Guid requestId = SubmitImpl(userPrincipal, loanItem, out sProdLpePriceGroupName, out sOriginatorCompensationPaymentSourceT, out sOriginatorCompensationLenderFeeOptionT, out sPriceGroup);
            
            if (requestId != ConstAppDavid.LPE_NoProductRequestId)
            {
                CacheLoanItem(requestId, new LoanDictionaryItem() { sProdLpePriceGroupName = sProdLpePriceGroupName, sOriginatorCompensationPaymentSourceT = sOriginatorCompensationPaymentSourceT, sOriginatorCompensationLenderFeeOptionT = sOriginatorCompensationLenderFeeOptionT, sPriceGroup = sPriceGroup });
            }

            if (loanItem.IsDebugMode)
            {
                Tools.LogInfo("QuickPricerDebugSubmission"
                    + Environment.NewLine + Environment.NewLine + "requestid: " + requestId
                    + Environment.NewLine + Environment.NewLine + "scenario: " + ObsoleteSerializationHelper.JsonSerialize(loanItem));
            }
            return requestId;

        }

        public static bool IsResultReady(Guid requestId)
        {
            return LpeDistributeResults.IsResultAvailable(requestId);
        }
        public static QuickPricerResult SubmitSynchronous(AbstractUserPrincipal userPrincipal, QuickPricerLoanItem loanItem)
        {
            return SubmitSynchronous(userPrincipal, loanItem, false);
        }
        public static QuickPricerResult SubmitSynchronous(AbstractUserPrincipal userPrincipal, QuickPricerLoanItem loanItem, bool isDebugMode)
        {
            Guid requestId = Submit(userPrincipal, loanItem);

            int count = 0;
            while (!IsResultReady(requestId))
            {
                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(2000); // Sleep for 2 seconds
                // To prevent infinite loop, we will throw Timeout exception if it exceed 5 minutes.
                if (count == 60)
                {
                    throw new TimeoutException();
                }
                count++;
            }

            return GetResult(userPrincipal, requestId, isDebugMode, loanItem.bUseBestPricing, loanItem.FromConsumerPortal, loanItem.IsHELOCRequest);
        }
        public static QuickPricerResult GetResult(AbstractUserPrincipal userPrincipal, Guid requestId)
        {
            return GetResult(userPrincipal, requestId, false);
        }
        public static QuickPricerResult GetResult(AbstractUserPrincipal userPrincipal, Guid requestId, bool isDebugMode)
        {
            return GetResult(userPrincipal, requestId, isDebugMode, bUseBestPricing: true, fromConsumerPortal: false, hideConsumerPortalInterestOnlyIndicator: true);
        }
        public static QuickPricerResult GetResult(AbstractUserPrincipal userPrincipal, Guid requestId, bool isDebugMode, bool bUseBestPricing, bool fromConsumerPortal, bool hideConsumerPortalInterestOnlyIndicator)
        {
            if (!IsResultReady(requestId))
                throw new CBaseException(ErrorMessages.Generic, "Result is not ready but retrieve.");

            string sProdLpePriceGroupNm = null;
            E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT = E_sOriginatorCompensationPaymentSourceT.SourceNotSpecified;
            E_sOriginatorCompensationLenderFeeOptionT sOriginatorCompensationLenderFeeOptionT = E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
            PriceGroup sPriceGroup = null;
            if (requestId != ConstAppDavid.LPE_NoProductRequestId)
            {
                LoanDictionaryItem item = GetCacheItem(requestId);
                sProdLpePriceGroupNm = item.sProdLpePriceGroupName;
                sOriginatorCompensationPaymentSourceT = item.sOriginatorCompensationPaymentSourceT;
                sOriginatorCompensationLenderFeeOptionT = item.sOriginatorCompensationLenderFeeOptionT;
                sPriceGroup = item.sPriceGroup;
                RemoveCacheItem(requestId);
            }

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultItem(requestId);

            QuickPricerResult quickPricerResult = new QuickPricerResult();
            quickPricerResult.RequestId = requestId;
            quickPricerResult.TimeStamp = Tools.GetDBCurrentDateTime();
            quickPricerResult.PriceGroupName = sProdLpePriceGroupNm;
            quickPricerResult.PriceGroup = sPriceGroup;

            if (requestId == ConstAppDavid.LPE_NoProductRequestId)
            {
                // 5/18/2013 dd - No need to process further since there are no result.
                return quickPricerResult;
            }
            if (null == resultItem)
            {
                quickPricerResult.HasError = true;
                quickPricerResult.ErrorMessage = ErrorMessages.Generic;
            }
            else if (resultItem.IsErrorMessage)
            {
                //resultItem.ErrorMessage;
                quickPricerResult.HasError = true;
                quickPricerResult.ErrorMessage = resultItem.ErrorMessage;
            }
            else if (null == resultItem.Results)
            {
                quickPricerResult.HasError = true;
                quickPricerResult.ErrorMessage = ErrorMessages.Generic;
            }


            if (quickPricerResult.HasError)
            {
                return quickPricerResult;
            }

            ArrayList results = resultItem.Results;
            if (userPrincipal == null)
            {
                Tools.LogError("User principal is null.");
            }
            BrokerDB brokerDB = BrokerDB.RetrieveById(userPrincipal.BrokerId);
            if (brokerDB == null )
            {
                Tools.LogError("Cannot pull out brokerid " + userPrincipal.BrokerId);
            }
            RateMergeBucket rateMergeBucket = new RateMergeBucket(brokerDB.IsShowRateOptionsWorseThanLowerRate);
            
            // 2/3/2016 BS - OPM 237125. If the request is from consumer portal, keep pricing in "points" format regardless of the price group setting.
            // Otherwise it follows price group setting
            bool displayPmlFeeIn100Format = false;
            if (!fromConsumerPortal)
            {
                displayPmlFeeIn100Format = sPriceGroup.DisplayPmlFeeIn100Format;
            }
            
            ArrayList eligibleProducts = new ArrayList(results.Count);

            StringBuilder sb = new StringBuilder();
            foreach (object o in results)
            {
                CApplicantPriceXml product = o as CApplicantPriceXml;

                if (null == product)
                    continue;

                if (isDebugMode)
                {
                    sb.AppendLine(product.lLpTemplateNm + ". Result=" + product.Status);
                    if (product.Status == E_EvalStatus.Eval_Ineligible)
                    {
                        foreach (var reason in product.DenialReasons)
                        {
                            sb.AppendLine("    DenialReasons:" + reason);
                        }
                        foreach (var reason in product.DisqualifiedRuleList)
                        {
                            sb.AppendLine("    DisqualifiedRule:" + reason);
                        }
                    }
                    else if (product.Status == E_EvalStatus.Eval_InsufficientInfo)
                    {
                        foreach (var reason in product.DeveloperErrors)
                        {
                            sb.AppendLine("    DeveloperError:" + reason);
                        }
                    }
                }
                if (product.Status == E_EvalStatus.Eval_Eligible)
                {
                    // 1/15/2009 dd - We only display eligible product in Quick Pricer.
                    if (bUseBestPricing)
                        rateMergeBucket.Add(product);
                    else
                        eligibleProducts.Add(product);
                }
                else if (product.Status == E_EvalStatus.Eval_Ineligible)
                {
                    quickPricerResult.AddIneligible(product);
                }

            }

            string pointRep;
            if (bUseBestPricing)
            {
                #region RateMerge // rateMergeBucket will only have merge groups when using Best Pricing
                foreach (RateMergeGroup mergeGroup in rateMergeBucket.RateMergeGroups)
                {
                    if (mergeGroup.RateOptions.Count == 0)
                        continue;

                    QuickPricerResultLoanProgram lp = new QuickPricerResultLoanProgram(mergeGroup);
                    if (fromConsumerPortal && lp.IOnlyMon > 0 && !hideConsumerPortalInterestOnlyIndicator)
                    {
                        lp.Name = lp.Name + " INTEREST ONLY";
                    }

                    foreach (CApplicantRateOption rateOption in mergeGroup.RateOptions)
                    {
                        if (rateOption.IsDisqualified) continue; // OPM 125215.  Quick pricer should filter out per-rate denials.

                        lp.IsBlockedRateLockSubmission = rateOption.IsBlockedRateLockSubmission; // If one rate is expired in the group that mean all rates in this group are expired.

                        pointRep = FormatPoint(sOriginatorCompensationPaymentSourceT, brokerDB.IsUsePriceIncludingCompensationInPricingResult,
                            sOriginatorCompensationLenderFeeOptionT, displayPmlFeeIn100Format, rateOption.PointIncludingOriginatorCompIn100_rep,
                            rateOption.PointIncludingOriginatorComp_rep, rateOption.PointIn100_rep, rateOption.Point_rep);

                        lp.RateOptions.Add(new QuickPricerResultRateItem(rateOption.Rate_rep, pointRep, rateOption.Apr_rep, rateOption.Margin_rep, rateOption.FirstPmtAmt_rep, rateOption.BottomRatio_rep, rateOption.LpTemplateId, rateOption.PerOptionAdjStr
                            , rateOption.BreakEvenPoint, rateOption.Reserves, rateOption.ClosingCost, rateOption.PrepaidCharges, rateOption.NonPrepaidCharges, rateOption.ClosingCostBreakdown, rateOption.IsRateExpired
                            ));
                        quickPricerResult.AddApplicantPrice(rateOption.ApplicantPrice);
                    }

                    if (lp.RateOptions.Count() != 0)
                        quickPricerResult.LoanPrograms.Add(lp);
                }
                #endregion
            }
            else
            {
                #region NonMerge // eligibleProducts will only have products if not using Best Pricing
                foreach (object o in eligibleProducts)
                {
                    CApplicantPriceXml eligibleproduct = o as CApplicantPriceXml;
                    if (eligibleproduct.ApplicantRateOptions.Count() == 0)
                        continue;

                    // Use a dummy merge group to parse the term, product type, finance method, and other data from the loan product template
                    // Set the lp name to the loan product name since this is not rate merge/Best Pricing
                    RateMergeGroup RMG = new RateMergeGroup(eligibleproduct, brokerDB.IsShowRateOptionsWorseThanLowerRate);
                    QuickPricerResultLoanProgram lp = new QuickPricerResultLoanProgram(RMG);
                    lp.Name = eligibleproduct.lLpTemplateNm;
                    lp.InvestorName = eligibleproduct.lLpInvestorNm;

                    foreach (CApplicantRateOption rOptNonMerge in eligibleproduct.ApplicantRateOptions)
                    {
                        if (rOptNonMerge.IsDisqualified) continue; // OPM 125215.  Quick pricer should filter out per-rate denials.

                        rOptNonMerge.SetApplicantPrice(eligibleproduct);
                        lp.IsBlockedRateLockSubmission = rOptNonMerge.IsBlockedRateLockSubmission; // If one rate is cannot be submitted all of them cannot be submitted.

                        pointRep = FormatPoint(sOriginatorCompensationPaymentSourceT, brokerDB.IsUsePriceIncludingCompensationInPricingResult,
                            sOriginatorCompensationLenderFeeOptionT, displayPmlFeeIn100Format, rOptNonMerge.PointIncludingOriginatorCompIn100_rep,
                            rOptNonMerge.PointIncludingOriginatorComp_rep, rOptNonMerge.PointIn100_rep, rOptNonMerge.Point_rep);

                        lp.RateOptions.Add(new QuickPricerResultRateItem(rOptNonMerge.Rate_rep, pointRep, rOptNonMerge.Apr_rep, rOptNonMerge.Margin_rep, rOptNonMerge.FirstPmtAmt_rep, rOptNonMerge.BottomRatio_rep, rOptNonMerge.LpTemplateId, rOptNonMerge.PerOptionAdjStr
                            ,rOptNonMerge.BreakEvenPoint, rOptNonMerge.Reserves , rOptNonMerge.ClosingCost, rOptNonMerge.PrepaidCharges, rOptNonMerge.NonPrepaidCharges, rOptNonMerge.ClosingCostBreakdown, rOptNonMerge.IsRateExpired
                            ));
                        quickPricerResult.AddApplicantPrice(rOptNonMerge.ApplicantPrice);
                    }

                    if (lp.RateOptions.Count() != 0)
                        quickPricerResult.LoanPrograms.Add(lp);
                }
                #endregion
            }
            if (isDebugMode)
            {
                quickPricerResult.DebugInfo = sb.ToString();
            }
            quickPricerResult.DetailTimingInfo = resultItem.DebugDetail;
            return quickPricerResult;
        }

        private static string FormatPoint(E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT, bool IsUsePriceIncludingCompensationInPricingResult,
            E_sOriginatorCompensationLenderFeeOptionT sOriginatorCompensationLenderFeeOptionT, bool bDisplayPmlFeeIn100Format, string sPointIncludingOriginatorCompIn100,
            string sPointIncludingOriginatorComp, string sPointIn100, string sPoint)
        {
            string pointRep;
            if (sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid
                && IsUsePriceIncludingCompensationInPricingResult // OPM 64253
                && sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees
                )
            {
                if (bDisplayPmlFeeIn100Format)
                {
                    pointRep = sPointIncludingOriginatorCompIn100;
                }
                else
                {
                    pointRep = sPointIncludingOriginatorComp;
                }
            }
            else
            {
                if (bDisplayPmlFeeIn100Format)
                {
                    pointRep = sPointIn100;
                }
                else
                {
                    pointRep = sPoint;
                }
            }
            return pointRep;
        }

        private static Guid SubmitImpl(AbstractUserPrincipal userPrincipal, QuickPricerLoanItem loanItem, out string sProdLpePriceGroupNm, out E_sOriginatorCompensationPaymentSourceT sOriginatorCompensationPaymentSourceT, out E_sOriginatorCompensationLenderFeeOptionT sOriginatorCompensationLenderFeeOptionT, out PriceGroup sPriceGroup)
        {

            BrokerDB brokerDB = BrokerDB.RetrieveById(userPrincipal.BrokerId);

            Guid quickPricerTemplateId = brokerDB.QuickPricerTemplateId;

            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(quickPricerTemplateId, typeof(QuickPricerEngine));
            dataLoan.InitLoad();
            dataLoan.PrepareQuickPricerLoan(loanItem);

            // 7/14/2013 dd - Need to change calc mode to PriceMyLoan so 
            // sOriginatorCompensationLenderFeeOptionT will return a desire value.
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

            sPriceGroup = dataLoan.sPriceGroup;
            sProdLpePriceGroupNm = dataLoan.sProdLpePriceGroupNm;
            sOriginatorCompensationPaymentSourceT = dataLoan.sOriginatorCompensationPaymentSourceT;
            sOriginatorCompensationLenderFeeOptionT = dataLoan.sOriginatorCompensationLenderFeeOptionT;
            // TODO: Perform validation.

            LoanProgramByInvestorSet lpSet = dataLoan.GetLoanProgramsToRunLpe(userPrincipal, E_sLienQualifyModeT.ThisLoan, Guid.Empty, true);


            E_RenderResultModeT eRenderMode = loanItem.bUseBestPricing ? E_RenderResultModeT.BestPricesPerProgram : E_RenderResultModeT.Regular;

            // 7/13/2013 dd - OLD Method.

            //return DistributeUnderwritingEngine.SubmitToEngine(userPrincipal, sLId, lpSet, E_sLienQualifyModeT.ThisLoan, dataLoan.sProdLpePriceGroupId, "", eRenderMode, false /*isUsePricePolicyDll*/, 
            //    E_sPricingModeT.RegularPricing, Guid.Empty, 0, 0, 0, 0);

            return DistributeUnderwritingEngine.SubmitToEngineForQuickPricer(userPrincipal, quickPricerTemplateId, lpSet, dataLoan.sProdLpePriceGroupId, eRenderMode, loanItem /*loanItem*/);
        }


    }
}
