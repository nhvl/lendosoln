﻿// <copyright file="QuickPricer2LoansCreationQueueHandler.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   9/4/2014
// </summary>
namespace LendersOffice.ObjLib.QuickPricer
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.Security;

    /// <summary>
    /// Creates loans in QuickPricer 2 loan pool depending on message.<para></para>
    /// TODO: No longer user this class, instead use <see cref="CancellableQuickPricer2LoansCreationQueueHandler"/>.
    /// </summary>
    public class QuickPricer2LoansCreationQueueHandler : CommonProjectLib.Runnable.IRunnable
    {
        /// <summary>
        /// Initializes static members of the <see cref="QuickPricer2LoansCreationQueueHandler" /> class.<para></para>
        /// </summary>
        static QuickPricer2LoansCreationQueueHandler()
        {
            DBMessageQueue = new DBMessageQueue(ConstMsg.QuickPricer2LoansCreationQueue);
            ClassDescription = "Adds loans to QuickPricer 2 loan pool depending on message.";
            Name = "QuickPricer2LoansCreation";
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickPricer2LoansCreationQueueHandler" /> class.
        /// </summary>
        public QuickPricer2LoansCreationQueueHandler()
        {
        }

        /// <summary>
        /// Gets the message queue that we enqueue to / dequeue from.
        /// </summary>
        /// <value>The QuickPricer 2 loan creation queue.</value>
        public static DBMessageQueue DBMessageQueue
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the description of what the class does.
        /// </summary>
        /// <value>The description of what the class does.</value>
        public string Description
        {
            get { return ClassDescription; }
        }

        /// <summary>
        /// Gets or sets - The description of what the class does.
        /// </summary>
        /// <value>The description of what the class does.</value>
        private static string ClassDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets - A shorter name for the queue.
        /// </summary>
        /// <value>A short name for the queue.</value>
        private static string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Loops until the queue is empty.  Creates loans for users / lender's users depending on what's been enqueued.
        /// </summary>
        public void Run()
        {
            DBMessage msg;

            while (true)
            {
                try
                {
                    msg = DBMessageQueue.Receive();

                    if (msg == null)
                    {
                        return;
                    }
                }
                catch (DBMessageQueueException e)
                {
                    if (e.MessageQueueErrorCode != DBMessageQueueErrorCode.IOTimeout && e.MessageQueueErrorCode != DBMessageQueueErrorCode.EmptyQueue)
                    {
                        Tools.LogError("[" + Name + "] DBMQException" + e.Message);
                        continue;
                    }
                    else
                    {
                        // Queue is empty or not responding, either way wait.
                        return;
                    }
                }

                try
                {
                    Trace.CorrelationManager.ActivityId = new Guid();
                    var parsedData = ParseDBMessage(msg);

                    if (!ProcessQuickPricer2LoansCreationInfo(parsedData))
                    {
                        // at some point it might be better to put this on a separate queue, but for now following the convention.
                        DBMessageQueue.Send(msg.Subject1, msg.Data, msg.Subject2, msg.Priority);
                    }
                }
                catch (Exception e)
                {
                    Tools.LogErrorWithCriticalTracking("[" + Name + "] Error", e);
                    throw; // for unit test.
                }
            }
        }
       
        /// <summary>
        /// Helper method for turning the DBMessage in the queue into something we understand.  <para></para> 
        /// Uses the same protocol for deserialization that enqueueing uses, namely json.
        /// </summary>
        /// <param name="msg">The DBMessage to process.</param>
        /// <returns>An object that the processor / library understands instead of just a string.</returns>
        private static QuickPricer2LoansCreationInfo ParseDBMessage(DBMessage msg)
        {
            return ObsoleteSerializationHelper.JavascriptJsonDeserializer<QuickPricer2LoansCreationInfo>(msg.Data);
        }

        /// <summary>
        /// Creates loans for user or for every user at the lender depending on what was enqueued.
        /// </summary>
        /// <param name="qp2lci">The object with the info of what to create and for whom.</param>
        /// <returns>True if successful.</returns>
        private static bool ProcessQuickPricer2LoansCreationInfo(QuickPricer2LoansCreationInfo qp2lci)
        {
            bool succeeded = false;
            try
            {
                if (!qp2lci.IsForBrokerCreation)
                {
                    // goal of this is to queue up some loans when the user has the feature enabled / or they are close to running out.
                    var principal = (AbstractUserPrincipal)PrincipalFactory.Create(qp2lci.BrokerID, qp2lci.UserID.Value, qp2lci.UserType, true, false);

                    if (principal == null)
                    {
                        Tools.LogErrorWithCriticalTracking(
                            "QuickPricer2LoansCreationQueueHandler had user with" +
                            string.Format(
                            "ID {0} and type {1}",
                            qp2lci.UserID.Value, // {0}
                            qp2lci.UserType) + // {1}
                            "which yielded a null principal.");
                        return false;
                    }
                    else if (!principal.IsActuallyUsePml2AsQuickPricer)
                    {
                        Tools.LogErrorWithCriticalTracking(
                            "QuickPricer2LoansCreationQueueHandler had user with" +
                            string.Format(
                            "ID {0} and type {1}",
                            qp2lci.UserID.Value, // {0}
                            qp2lci.UserType) + // {1} 
                            "but IsActuallyUsePml2AsQuickPricer was false.");

                        // opm 198981, this happens when a user has it disabled, so we can ignore these.
                        // is it the only way this can happen... that idk, but IsActuallyUsePml2AsQuickPricer should be the most up-to-date in terms of intent.
                        return true;
                    }
                    else
                    {
                        QuickPricer2LoanPoolManager.CreateSomePooledLoansForUser(principal, qp2lci.NumLoansToCreatePerUser);
                        return true;
                    }
                }
                else
                {
                    // goal of this is to trigger this when the broker enables the feature / or the quickpricer template changes.
                    var creator = new QuickPricer2LoansCreationProcess();
                    creator.CreateLoansForBroker(BrokerDB.RetrieveById(qp2lci.BrokerID));
                }

                succeeded = true;
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                throw; // for unit test.
            }

            return succeeded;
        }
    }
}
