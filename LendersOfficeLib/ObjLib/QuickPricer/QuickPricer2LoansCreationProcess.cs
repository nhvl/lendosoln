﻿// <copyright file="QuickPricer2LoansCreationProcess.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   7/16/2014
// </summary>
namespace LendersOffice.ObjLib.QuickPricer
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Migration;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Creates pooled loans for the brokers that need them.
    /// </summary>
    public class QuickPricer2LoansCreationProcess : CommonProjectLib.Runnable.IRunnable
    {   
        /// <summary>
        /// Gets the description for runnable interface.
        /// </summary>
        /// <value>Create loans for QuickPricer 2.0.</value>
        public string Description
        {
            get { return "Create loans for QuickPricer 2.0."; }
        }

        /// <summary>
        /// Creates pooled loans for the brokers that need them.
        /// </summary>
        public void Run()
        {
            var stopwatchForAllBrokers = new Stopwatch();
            stopwatchForAllBrokers.Start();
            Tools.LogInfo("EVENT: Starting run of QuickPricer2LoansCreationProcess.");

            var countOfProcessedBrokers = 0;
            foreach (var brokerID in QuickPricer2FeatureHelper.GetActiveBrokersWithFeatureNotDisabled())
            {
                var broker = BrokerDB.RetrieveById(brokerID);
                if (E_Pml2AsQuickPricerMode.Disabled == broker.Pml2AsQuickPricerMode)
                {
                    var msg = "GetActiveBrokersWithFeatureNotDisabled returned broker: " +
                         broker.AsSimpleStringForLog() +
                         " who has Pml2AsQuickPricerMode (Use QP 2.0?) set to disabled ";

                    Tools.LogErrorWithCriticalTracking(
                        msg,
                        new Exception(msg));
                    continue;
                }

                try
                {
                    this.CreateLoansForBroker(broker);
                    countOfProcessedBrokers++;
                }
                catch (AccessDenied ad)
                {
                    var msg = "CreateLoansForBroker" + " got access denied for broker: " +
                                 broker.AsSimpleStringForLog();
                    Tools.LogErrorWithCriticalTracking(msg, ad);
                }
                catch (CBaseException cbe)
                {
                    var msg = "CreateLoansForBroker" + " failed for broker: " +
                              broker.AsSimpleStringForLog();
                    Tools.LogErrorWithCriticalTracking(msg, cbe);
                }
                catch (Exception e)
                {
                    var msg = "CreateLoansForBroker" + " failed for broker: " +
                              broker.AsSimpleStringForLog();
                    Tools.LogErrorWithCriticalTracking(msg, e);
                    throw; // fixing build.
                }
            }

            stopwatchForAllBrokers.Stop();
            Tools.LogInfo(string.Format(
                    "TIMING: Run of QuickPricer2LoansCreationProcess took {0} for {1} brokers.",
                    Tools.TimeSpanAsString(stopwatchForAllBrokers.Elapsed),
                    countOfProcessedBrokers));
        }       

        /// <summary>
        /// Pools some number of loans for the specified broker (such that they have the minimum).
        /// </summary>
        /// <param name="broker">The broker to add the loans for.</param>
        public void CreateLoansForBroker(BrokerDB broker)
        {
            var countOfProcessedPUsers = 0;
            var countOfProcessedBUsers = 0;
            var numberOfPooledLoansCreated = 0;
            var stopwatchForBroker = new Stopwatch();

            stopwatchForBroker.Start();
            foreach (var userInfo in QuickPricer2FeatureHelper.GetActiveQP2UsersAtBroker(broker))
            {
                // note, the below can return null for inactive users, however the above returns only active users.
                var principal = (AbstractUserPrincipal)PrincipalFactory.Create(broker.BrokerID, userInfo.UserID, userInfo.UserType, true, false);

                if (principal == null)
                {
                    var refreshedBroker = BrokerDB.RetrieveByIdForceRefresh(broker.BrokerID);
                    if (refreshedBroker.Status == 0)
                    {
                        stopwatchForBroker.Stop();
                        Tools.LogInfo(string.Format(
                            "TIMING: Run of QuickPricer2LoansCreationProcess took {0} for broker {1} with {2} P users and {3} B users.  Created {4} loans." +
                            "Exiting_early since the broker is no longer active (status = 0).",
                            Tools.TimeSpanAsString(stopwatchForBroker.Elapsed), // 0
                            broker.AsSimpleStringForLog(), // 1
                            countOfProcessedPUsers, // 2
                            countOfProcessedBUsers, // 3
                            numberOfPooledLoansCreated)); // 4

                        return;
                    }

                    var msg = "GetQP2UsersAtBroker returned user: "
                        + userInfo.ToSimpleStringForLog()
                        + " at broker '" + broker.BrokerID + "' ("
                        + broker.CustomerCode + ") but that user yields a null principal.";

                    Tools.LogErrorWithCriticalTracking(
                        msg,
                        new Exception(msg));

                    continue;
                }

                if (!principal.IsActuallyUsePml2AsQuickPricer)
                {
                    if (broker.Pml2AsQuickPricerMode != principal.BrokerDB.Pml2AsQuickPricerMode)
                    {
                        stopwatchForBroker.Stop();
                        Tools.LogInfo(string.Format(
                            "TIMING: Run of QuickPricer2LoansCreationProcess took {0} for broker {1} with {2} P users and {3} B users.  Created {4} loans.  "
                            + "Exited early as broker had Pml2AsQuickPricerMode change from {5} to {6}.",
                            Tools.TimeSpanAsString(stopwatchForBroker.Elapsed), // 0
                            principal.BrokerDB.AsSimpleStringForLog(), // 1
                            countOfProcessedPUsers, // 2
                            countOfProcessedBUsers, // 3
                            numberOfPooledLoansCreated, // 4
                            broker.Pml2AsQuickPricerMode, // 5
                            principal.BrokerDB.Pml2AsQuickPricerMode)); // 6

                        // opm 246003.  We need to return early if a broker has the feature changed mid-run.
                        return;
                    }

                    var msg = "GetQP2UsersAtBroker returned user: "
                        + userInfo.ToSimpleStringForLog() 
                        + " at broker '" + broker.BrokerID + "' ("
                        + broker.CustomerCode + ") but that user does not have IsActuallyUsePml2AsQuickPricer";

                    Tools.LogErrorWithCriticalTracking(
                        msg,
                        new Exception(msg));

                    continue;
                }

                try
                {
                    if (userInfo.UserType == "P")
                    {
                        if (!principal.CanAccessTpoPortal())
                        {
                            continue;
                        }

                        PmlBroker pB = PmlBroker.RetrievePmlBrokerById(principal.PmlBrokerId, principal.BrokerId);

                        // We're creating the minimum number of loans needed for each user, so create loans for
                        // each of the user's portal modes as well.
                        if ((principal.HasRole(E_RoleT.Pml_LoanOfficer) || principal.HasRole(E_RoleT.Pml_BrokerProcessor)) 
                            && pB.Roles.Contains(E_OCRoles.Broker))
                        {
                            principal.PortalMode = E_PortalMode.Broker;
                            numberOfPooledLoansCreated += QuickPricer2LoanPoolManager.CreateSomePooledLoansForUser(principal, null);
                            countOfProcessedPUsers++;
                        }

                        if (principal.HasRole(E_RoleT.Pml_Secondary) && pB.Roles.Contains(E_OCRoles.MiniCorrespondent))
                        {
                            principal.PortalMode = E_PortalMode.MiniCorrespondent;
                            numberOfPooledLoansCreated += QuickPricer2LoanPoolManager.CreateSomePooledLoansForUser(principal, null);
                            countOfProcessedPUsers++;
                        }

                        if (principal.HasRole(E_RoleT.Pml_Secondary) && pB.Roles.Contains(E_OCRoles.Correspondent))
                        {
                            principal.PortalMode = E_PortalMode.Correspondent;
                            numberOfPooledLoansCreated += QuickPricer2LoanPoolManager.CreateSomePooledLoansForUser(principal, null);
                            countOfProcessedPUsers++;
                        }
                    }
                    else
                    {
                        numberOfPooledLoansCreated += QuickPricer2LoanPoolManager.CreateSomePooledLoansForUser(principal, null);
                        countOfProcessedBUsers++;
                    }
                }
                catch (AccessDenied ad)
                {
                    var msg = "CreateSomePooledLoansForUser failed for user (got access denied): "
                       + userInfo.ToSimpleStringForLog();
                    Tools.LogErrorWithCriticalTracking(msg, ad);
                }
                catch (CBaseException cbe)
                {
                    var msg = "CreateSomePooledLoansForUser failed for user: "
                        + userInfo.ToSimpleStringForLog();
                    Tools.LogErrorWithCriticalTracking(msg, cbe);
                }
                catch (Exception e)
                {
                    var msg = "CreateSomePooledLoansForUser failed for user: "
                        + userInfo.ToSimpleStringForLog();
                    Tools.LogErrorWithCriticalTracking(msg, e);
                    throw;
                }
            }

            stopwatchForBroker.Stop();
            Tools.LogInfo(string.Format(
                "TIMING: Run of QuickPricer2LoansCreationProcess took {0} for broker {1} with {2} P users and {3} B users.  Created {4} loans.",
                Tools.TimeSpanAsString(stopwatchForBroker.Elapsed), // 0
                broker.AsSimpleStringForLog(), // 1
                countOfProcessedPUsers, // 2
                countOfProcessedBUsers, // 3
                numberOfPooledLoansCreated)); // 4
        }
    }
}
