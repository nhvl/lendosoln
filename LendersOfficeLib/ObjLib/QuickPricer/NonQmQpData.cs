﻿namespace LendersOffice.ObjLib.QuickPricer
{
    using System;
    using DataAccess;

    /// <summary>
    /// Non-QM QuickPricer data holder.
    /// </summary>
    public class NonQmQpData
    {
        /// <summary>
        /// Gets or sets the loan's id.
        /// </summary>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the borrower first name.
        /// </summary>
        public string BFirstNm { get; set; }

        /// <summary>
        /// Gets or sets the borrower last name.
        /// </summary>
        public string BLastNm { get; set; }

        /// <summary>
        /// Gets or sets the loan tempalte id.
        /// </summary>
        public Guid LpTemplateId { get; set; }

        /// <summary>
        /// Gets or sets the rate for the selected program.
        /// </summary>
        public decimal Rate { get; set; }

        /// <summary>
        /// Gets or sets the point for the selected program.
        /// </summary>
        public decimal Point { get; set; }

        /// <summary>
        /// Gets or sets the rate option id for the selected program.
        /// </summary>
        public string RateOptionId { get; set; }

        /// <summary>
        /// Gets or sets the loan amount.
        /// </summary>
        public string LAmtCalcPe { get; set; }

        /// <summary>
        /// Gets or sets the LTV.
        /// </summary>
        public string LtvRPe { get; set; }

        /// <summary>
        /// Gets or sets the loan purpose.
        /// </summary>
        public E_sLPurposeT LPurposeT { get; set; }

        /// <summary>
        /// Gets or sets the occupancy type.
        /// </summary>
        public E_sOccT OccTPe { get; set; }

        /// <summary>
        /// Gets or sets the property type.
        /// </summary>
        public E_sProdSpT ProdSpT { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string SpStatePe { get; set; }

        /// <summary>
        /// Gets or sets the estimated credit score.
        /// </summary>
        public int CreditScoreEstimatePe { get; set; }

        /// <summary>
        /// Gets or sets the manually entered housing history.
        /// </summary>
        public ManuallyEnteredHousingHistory ManuallyEnteredHousingHistory { get; set; }

        /// <summary>
        /// Gets or sets the manually entered housing events.
        /// </summary>
        public ManuallyEnteredHousingEvents ManuallyEnteredHousingEvents { get; set; }

        /// <summary>
        /// Gets or sets the manually entered bankruptcy.
        /// </summary>
        public ManuallyEnteredBankruptcy ManuallyEnteredBankruptcy { get; set; }

        /// <summary>
        /// Gets or sets the borrower's citizenship status.
        /// </summary>
        public E_aProdCitizenT ProdBCitizenT { get; set; }

        /// <summary>
        /// Gets or sets the document type.
        /// </summary>
        public E_sProdDocT ProdDocT { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the borrower is self employed.
        /// </summary>
        public bool IsSelfEmployed { get; set; }

        /// <summary>
        /// Gets or sets the message sent to the lender.
        /// </summary>
        public string MsgToLender { get; set; }
    }
}
