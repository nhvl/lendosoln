﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;

namespace LendersOffice.QuickPricer
{
    public class QuickPricerResultRateItem
    {
        private string m_rate;
        private string m_point;
        private string m_apr;
        private string m_margin;
        private string m_payment;
        private string m_dti;
        private Guid m_lLpTemplateId = Guid.Empty;
        private string m_opCode;

        private string m_breakEven;
        private string m_reserves;
        private string m_closingCost;
        private string m_prepaidCost;
        private string m_nonPrepaidCost;
        private IEnumerable<RespaFeeItem> m_closingCostBreakdown;
        private bool m_isExpired;

        public QuickPricerResultRateItem(string rate, string point, string apr, string margin, string payment, string dti, Guid lLpTemplateId, string opCode
            , string breakEven, string reserves, string closingCost, string prepaidCost, string nonPrepaidCost, IEnumerable<RespaFeeItem> closingCostBreakdown, bool isExpired)
        {
            m_rate = rate;
            m_point = point;
            m_apr = apr;
            m_margin = margin;
            m_payment = payment;
            m_dti = dti;
            m_lLpTemplateId = lLpTemplateId;
            m_opCode = opCode;
            m_breakEven = breakEven;
            m_reserves = reserves;
            m_closingCost = closingCost;
            m_prepaidCost = prepaidCost;
            m_nonPrepaidCost = nonPrepaidCost;
            m_closingCostBreakdown = closingCostBreakdown;
            m_isExpired = isExpired;
        }

        public bool IsExpired
        {
            get { return m_isExpired; }
        }

        public string OpCode
        {
            get { return m_opCode; }
        }

        public string Rate
        {
            get 
            {
                return m_rate;
            }
        }
        public decimal RateValue
        {
            get
            {
                decimal v;
                if (decimal.TryParse(Rate, out v) == false)
                {
                    v = 0;
                }
                return v;
            }
        }
        public string Point
        {
            get { return m_point; }
            set { m_point = value; } // 12/6/2011 dd - Allow this option to set point value.
        }
        public decimal PointValue
        {
            get
            {
                decimal v;
                if (decimal.TryParse(Point, out v) == false)
                {
                    v = 0;
                }
                return v;
            }
        }

        public string Apr
        {
            get { return m_apr; }
        }
        public string Margin
        {
            get { return m_margin; }
        }
        public string Payment
        {
            get { return m_payment; }
        }
        public string Dti
        {
            get { return m_dti; }
        }
        public Guid lLpTemplateId
        {
            get { return m_lLpTemplateId; }
        }

        public string BreakEven
        {
            get { return m_breakEven; }
        }
        public string Reserves
        {
            get { return m_reserves; }
        }
        public string ClosingCost
        {
            get { return m_closingCost; }
        }
        public string PrepaidCost
        {
            get { return m_prepaidCost; }
        }
        public string NonPrepaidCost
        {
            get { return m_nonPrepaidCost; }
        }
        public IEnumerable<RespaFeeItem> ClosingCostBreakDown
        {
            get { return m_closingCostBreakdown.OrderBy(p => int.Parse(p.HudLine)); }
        }


        public CLoanProductData LoanProductData
        {
            get
            {
                CLoanProductData productData = new CLoanProductData(lLpTemplateId);
                productData.InitLoad();
                return productData;
            }
        }
    }
}
