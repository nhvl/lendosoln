﻿// <copyright file="RateMonitorScenarioInfo.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   9/15/2014
// </summary>
namespace LendersOffice.ObjLib.QuickPricer
{
    using System;
    using DataAccess;

    /// <summary>
    /// A storage class for information about the rate monitor scenario tied to a quickpricer 2 loan.
    /// </summary>
    public class RateMonitorScenarioInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RateMonitorScenarioInfo" /> class.
        /// </summary>
        /// <param name="brokerID">The id of the lender of interest.</param>
        /// <param name="loanID">The loan with the rate monitors.</param>
        /// <param name="userID">The user who created the scenario.</param>
        /// <param name="name">The user-given name of the rate monitor scenario.</param>
        /// <param name="branchChannelT">The associated loan's branch channel type.</param>
        /// <param name="correspondentProcessT">The associated loan's correspondent process type.</param>
        public RateMonitorScenarioInfo(
            Guid brokerID,
            Guid loanID,
            Guid userID,
            string name,
            E_BranchChannelT branchChannelT,
            E_sCorrespondentProcessT correspondentProcessT)
        {
            this.BrokerID = brokerID;
            this.LoanID = loanID;
            this.UserIdOfUsingUser = userID;
            this.Name = name;
            this.CorrespondentProcessT = correspondentProcessT;
            this.BranchChannelT = branchChannelT;
        }

        /// <summary>
        /// Gets the id of the lender of the loan.
        /// </summary>
        /// <value>The guid of the lender.</value>
        public Guid BrokerID { get; private set; }

        /// <summary>
        /// Gets the id of the loan of the scenario.
        /// </summary>
        /// <value>The guid of the loan.</value>
        public Guid LoanID { get; private set; }

        /// <summary>
        /// Gets the id of the user of the loan / scenario.
        /// </summary>
        /// <value>The guid of the user.</value>
        public Guid UserIdOfUsingUser { get; private set; }

        /// <summary>
        /// Gets the user-defined name for the scenario.
        /// </summary>
        /// <value>The string form of the scenario name.</value>
        public string Name { get; private set; }

        /// <summary>
        /// Gets or sets the scenario's correspondent process type.
        /// </summary>
        /// <value>The scenario's correspondent process.</value>
        public E_sCorrespondentProcessT CorrespondentProcessT { get; set; }

        /// <summary>
        /// Gets or sets the scenario's branch channel type.
        /// </summary>
        /// <value>The scenario's branch channel.</value>
        public E_BranchChannelT BranchChannelT { get; set; }
    }
}
