﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.Audit;
using LendersOffice.CreditReport;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Constants;
using System.Diagnostics;

namespace LendersOffice.QuickPricer
{
    public static class QuickPricerLoanPoolManager
    {
        public static Guid GetUnusedLoan(AbstractUserPrincipal principal)
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {

                BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);

                Guid quickPricerTemplateId = broker.QuickPricerTemplateId;

                if (Guid.Empty == quickPricerTemplateId)
                {
                    throw new CBaseException("Quick Pricer Template is not set. ", "Quick Pricer Template is not set");
                }

                Guid sLId = Guid.Empty;

                SqlParameter[] parameters = { new SqlParameter("@BrokerId", principal.BrokerId) };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(principal.BrokerId, "QuickPricerLoanPool_GetUnusedLoanId", parameters))
                {
                    if (reader.Read())
                    {
                        sLId = (Guid)reader["sLId"];
                    }
                }
                if (sLId == Guid.Empty)
                {
                    // 1/29/2009 dd - Duplicate new file from QuickPricerTemplateId
                    CLoanFileCreator fileCreator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.LeadCreate);
                    sLId = fileCreator.CreateQP1SandboxFile(quickPricerTemplateID: quickPricerTemplateId);

                    parameters = new SqlParameter[] {
                        new SqlParameter("@BrokerId", principal.BrokerId),
                        new SqlParameter("@sLId", sLId)
                    };
                    StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "QuickPricerLoanPool_InsertUsedLoan", 3, parameters);
                }
                else
                {
                    // 1/29/2009 dd - Using loan from pool.
                    parameters = new SqlParameter[] {
                        new SqlParameter("@Src_sLId", quickPricerTemplateId),
                        new SqlParameter("@Dest_sLId", sLId)
                    };

                    StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "QuickPricerLoanPool_DuplicateLoan", 3, parameters);

                }


                return sLId;
            }
            finally
            {
                Tools.LogInfo("QuickPricerLoanPoolManager.GetUnusedLoan executed in " + sw.ElapsedMilliseconds + "ms.");
            }
        }
        public static void ReleaseToPool(AbstractUserPrincipal principal, Guid sLId)
        {
            if (sLId == Guid.Empty)
            {
                return; // 7/13/2013 dd - NO-OP if sLId is Empty
            }
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                CPageData dataLoan = new CQuickPricerLoanPoolData(sLId);
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                for (int i = 0; i < dataLoan.nApps; i++)
                {
                    CAppData dataApp = dataLoan.GetAppData(i);
                    if (i != 0)
                    {
                        // 1/28/2009 dd - Delete all none primary app.
                        dataLoan.DelApp(i);
                        dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                        continue;
                    }
                    else if (i == 0)
                    {
                        dataApp.aLiaCollection.ClearAll();
                        dataApp.aAssetCollection.ClearAll();
                        dataApp.aReCollection.ClearAll();
                        dataApp.aBEmpCollection.ClearAll();
                        dataApp.aCEmpCollection.ClearAll();
                        CreditReportServer.DeleteCreditReport(dataLoan.sBrokerId, dataApp.aAppId);
                    }
                }
                dataLoan.sAgentCollection.sAgentDataSetClear();
                dataLoan.Save();
                
                if (ConstStage.IsEnabledDeleteQPPoolFileDBContent)
                {
                    LargeFieldStorage.DO_NOT_USE_UnlessYouAreAntonio(sLId);
                }

                SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", principal.BrokerId),
                                            new SqlParameter("@sLId", sLId)
                                        };
                StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "QuickPricerLoanPool_ReleaseLoan", 3, parameters);

            }
            finally
            {
                Tools.LogInfo("QuickPricerLoanPoolManager.ReleaseToPool executed in " + sw.ElapsedMilliseconds + "ms.");
            }
        }

    }
}
