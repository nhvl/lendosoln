﻿// <copyright file="QuickPricer2LoanPoolManager.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   7/16/2014
// </summary>
namespace LendersOffice.ObjLib.QuickPricer
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Net.Sockets;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Stores the names of the stored procedures that are related to the pooling mechanism. <para></para>
    /// Procedures should be named QuickPricer_2_LOAN_USAGE_[EnumName].
    /// </summary>
    public enum QuickPricer2StoredProcedure
    {
        /// <summary>
        /// AddUnusedSandboxedLoanToPool = 0.
        /// </summary>
        AddUnusedSandboxedLoanToPool = 0,

        /// <summary>
        /// AddInUseSandboxedLoanToPool = 1.
        /// </summary>
        AddInUseSandboxedLoanToPool = 1,

        /// <summary>
        /// GetSandboxedUnusedLoanAndMarkAsInUse = 2.
        /// </summary>
        GetSandboxedUnusedLoanAndMarkAsInUse = 2,

        /// <summary>
        /// GetNumUnusedLoans = 3.
        /// </summary>
        GetNumUnusedLoans = 3,

        /// <summary>
        /// RemoveFromPool = 4.
        /// </summary>
        RemoveFromPool = 4,

        /// <summary>
        /// FindOldInUseLoansAtBroker = 5.
        /// </summary>
        FindOldInUseLoansAtBroker = 5,

        /// <summary>
        /// FindUnusedLoansNotMatchingQPTemplateAtBroker = 6.
        /// </summary>
        FindUnusedLoansNotMatchingQPTemplateAtBroker = 6,
        
        /// <summary>
        /// CanDeleteLoan = 7.
        /// </summary>
        CanDeleteLoan = 7,

        /// <summary>
        /// BestEffortDeleteLoan = 7.
        /// </summary>
        BestEffortDeleteLoan = 8,

        /// <summary>
        /// LoanHasAnyPresence = 9.
        /// </summary>
        LoanHasAnyPresence = 9,

        /// <summary>
        /// GetRateMonitorNameByLoanID = 10.
        /// </summary>
        GetRateMonitorNameByLoanID = 10,

        /// <summary>
        /// GetRateMonitoredLoansByUserID = 11.
        /// </summary>
        GetRateMonitoredLoansByUserID = 11,

        /// <summary>
        /// DeleteMonitoredScenario = 12.
        /// </summary>
        DeleteMonitoredScenario = 12
    }
    
    /// <summary>
    /// Static methods related to QuickPricer 2.0 Loan Pool Management.
    /// </summary>
    public static class QuickPricer2LoanPoolManager
    {
        /// <summary>
        /// In the DB, table is "QUICKPRICER_2_LOAN_USAGE".
        /// </summary>
        private const string QPTableName = "QUICKPRICER_2_LOAN_USAGE";

        /// <summary>
        /// Creates a sandboxed loan at the user's broker from their current quickpricer template. <para></para>
        /// Intended to be called by a process. <para></para>
        /// </summary>
        /// <param name="principal">The principal of the user to add the loan to.</param>
        /// <returns>The id of the newly created loan.</returns>
        public static Guid CreateSandboxedUnusedLoan(AbstractUserPrincipal principal)
        {
            return CreateSandboxedLoan(principal, false);
        }

        /// <summary>
        /// Creates a sandboxed loan at the current user's broker from their current quickpricer template. <para></para>
        /// Intended to be called in an app thread with a user. <para></para>
        /// </summary>
        /// <returns>The id of the newly created loan.</returns>
        public static Guid CreateSandboxedInUseLoan()
        {
            var principal = PrincipalFactory.CurrentPrincipal;
            return CreateSandboxedLoan(principal, true);
        }

        /// <summary>
        /// Checks for an unused sandboxed loan at the principal's broker and marks it in use. <para></para>
        /// </summary>
        /// <returns>The id of the loan that was marked in use.</returns>
        public static Guid GetSandboxedUnusedLoanAndMarkAsInUse()
        {
            var currentPrincipal = PrincipalFactory.CurrentPrincipal;
            if (currentPrincipal == null || currentPrincipal is InternalUserPrincipal)
            {
                throw new AccessDenied("Internal / null principals do no have access to mark unused loans as in use.");
            }

            var brokerID = currentPrincipal.BrokerId;
            var broker = currentPrincipal.BrokerDB;
            var quickPricerTemplateID = broker.QuickPricerTemplateId;

            E_BranchChannelT branchChannel = E_BranchChannelT.Blank;
            E_sCorrespondentProcessT correspondentProcess = E_sCorrespondentProcessT.Blank;

            switch (currentPrincipal.PortalMode)
            {
                case E_PortalMode.Broker:
                    branchChannel = E_BranchChannelT.Wholesale;
                    break;
                case E_PortalMode.MiniCorrespondent:
                    branchChannel = E_BranchChannelT.Correspondent;
                    correspondentProcess = E_sCorrespondentProcessT.MiniCorrespondent;
                    break;
                case E_PortalMode.Correspondent:
                    branchChannel = E_BranchChannelT.Correspondent;
                    PmlBroker pB = PmlBroker.RetrievePmlBrokerById(currentPrincipal.PmlBrokerId, currentPrincipal.BrokerId);

                    if (pB.UnderwritingAuthority == E_UnderwritingAuthority.Delegated)
                    {
                        correspondentProcess = E_sCorrespondentProcessT.Delegated;
                    }
                    else
                    {
                        correspondentProcess = E_sCorrespondentProcessT.PriorApproved;
                    }

                    break;
                case E_PortalMode.Retail:
                    branchChannel = E_BranchChannelT.Retail;
                    break;
                case E_PortalMode.Blank:
                    break;
                default:
                    throw new UnhandledEnumException(currentPrincipal.PortalMode);
            }

            var loanIDParam = new SqlParameter("@LoanID", SqlDbType.UniqueIdentifier);
            loanIDParam.IsNullable = true;
            loanIDParam.Direction = ParameterDirection.Output;

            var sqlParameters = new List<SqlParameter>() 
            {
                // would be better to provide the file version if we could cache it, for speed.
                new SqlParameter("@BrokerID", brokerID),
                new SqlParameter("@QuickPricerTemplateID", quickPricerTemplateID),
                new SqlParameter("@UserIDofUsingUser", currentPrincipal.UserId),
                new SqlParameter("@BranchChannelT", branchChannel),
                new SqlParameter("@CorrespondentProcessT", correspondentProcess),
                new SqlParameter("@IsWholesale", branchChannel == E_BranchChannelT.Wholesale),
                loanIDParam // output parameter.
            };

            try
            {
                var sprocName = GetSprocName(QuickPricer2StoredProcedure.GetSandboxedUnusedLoanAndMarkAsInUse);

                // ignoring numAffected as it can be high.
                int numAffected = StoredProcedureHelper.ExecuteNonQuery(brokerID, sprocName, 0, sqlParameters); // no retries since sleep is slow.

                // this happens when there's no pooled loan.
                if (loanIDParam.Value == DBNull.Value)
                {
                    Tools.LogInfo(
                        "QUICKPRICER_2_LOAN_USAGE_GetSandboxedUnusedLoanAndMarkAsInUse couldn't find an unused loan for these parameters: " 
                        + string.Join(", ", sqlParameters.Select(p => p.AsSimpleFormatString())));

                    QuickPricer2LoansCreationEnqueuer.EnqueueUserLoanCreation(currentPrincipal, 2);
                    return CreateSandboxedInUseLoan();
                }

                // at this point loanIDParam had an actual value so
                // we found a pooled loan and marked it in use so
                // give them the minimum number of pooled loans needed. 
                QuickPricer2LoansCreationEnqueuer.EnqueueUserLoanCreation(currentPrincipal, null);
            }
            catch (SqlException)
            {
                // if they ran out of loans, put a couple more on the queue.
                QuickPricer2LoansCreationEnqueuer.EnqueueUserLoanCreation(currentPrincipal, 2);
                return CreateSandboxedInUseLoan();
            }

            Guid loanId = (Guid)loanIDParam.Value;

            // OPM 243549 - Reset Agents/Relationships before returning loanId from pool (+2 loan saves).
            CLoanFileCreator.RefreshLoanAssignments(currentPrincipal, loanId, E_UnderwritingAuthority.PriorApproved);

            return loanId;
        }

        /// <summary>
        /// If numLoansToCreateOrDefault is null, creates enough pooled loans so that there are at least ConstStage.MinNumUnusedQP2LoansToHaveOnHandPerUser (1) <para></para>
        /// Else: creates the specified number of loans (so user has at most ConstStage.MaxNumUnusedQP2LoansToHaveOnHandPerUser).
        /// </summary>
        /// <param name="principal">The principal of the user to create the loans for.</param>
        /// <param name="numLoansToCreateOrDefault">The number of loans to create or null if the minimum.</param>
        /// <returns>The number of loans it created.</returns>
        public static int CreateSomePooledLoansForUser(AbstractUserPrincipal principal, int? numLoansToCreateOrDefault)
        {
            AllowUseOrThrow(principal, false);

            if (numLoansToCreateOrDefault.HasValue && numLoansToCreateOrDefault.Value < 0)
            {
                throw new Exception("Cannot create a negative number of loans.");
            }

            // ideally want as low # of loans per user... possibly by slowly increasing the conststage bit.
            var currentNumUnusedLoans = GetNumberOfUnusedLoans(principal);
            int numUnusedLoansToCreate = 0;
            int maxUnusedLoansToCreate = Math.Max(0, ConstStage.MaxNumUnusedQP2LoansToHaveOnHandPerUser - currentNumUnusedLoans);

            if (numLoansToCreateOrDefault.HasValue)
            {
                numUnusedLoansToCreate = Math.Min(numLoansToCreateOrDefault.Value, maxUnusedLoansToCreate);
            }
            else
            {
                numUnusedLoansToCreate = Math.Max(0, ConstStage.MinNumUnusedQP2LoansToHaveOnHandPerUser - currentNumUnusedLoans);
            }

            if (numUnusedLoansToCreate > 0)
            {
                var numLoansCreated = 0;
                while (numLoansCreated < numUnusedLoansToCreate)
                {
                    Guid loanID = Guid.Empty;
                    var timeTakenToCreateLoan = Tools.GetTimeSpan(() => loanID = CreateSandboxedUnusedLoan(principal));
                    
                    Tools.LogInfo(string.Format(
                        "Created sandboxed unused loan {0} for user {1} at {2} took {3}",
                        loanID,         // 0
                        principal,      // 1
                        DateTime.Now,   // 2
                        Tools.TimeSpanAsString(timeTakenToCreateLoan)));  // 3

                    numLoansCreated++;
                }

                return numLoansCreated;
            }
            else if (numUnusedLoansToCreate == 0)
            {
                return 0; // no loans needed to be created for this user.
            }
            else
            {
                // if 0 > numUnusedLoansToCreate
                Tools.LogError(
                    "Somehow numUnusedLoansToCreate was less than 0." + Environment.NewLine +
                    string.Format(                         
                         "numLoansToCreateOrDefault: '{0}',  currentNumUnusedLoans '{1}', principal: '{2}'",
                         numLoansToCreateOrDefault.HasValue ? numLoansToCreateOrDefault.Value.ToString() : "NULL", // {0}
                         currentNumUnusedLoans.ToString(), // {1}
                         principal.ToUserDataString())); // {2}
                return 0;
            }
        }

        /// <summary>
        /// Removes the loan from the pool, but does not mark the loan as invalid. <para></para>
        /// </summary>
        /// <param name="principal">The principal of the user who was using the loan.</param>
        /// <param name="loanID">The id of the loan to remove from the pool.</param>
        public static void RemoveFromPool(AbstractUserPrincipal principal, Guid loanID)
        {
            // don't need to check anything here... ?
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@LoanID", loanID),
                new SqlParameter("@BrokerID", principal.BrokerId),
                new SqlParameter("@UserIDofUsingUser", principal.UserId)
            };

            var sprocName = GetSprocName(QuickPricer2StoredProcedure.RemoveFromPool);

            var numAffected = StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, sprocName, 2, sqlParameters);
        }

        /// <summary>
        /// Finds "in use" loans that are older than ConstStage.QuickPricer2InUseLoanExpirationDays (2).
        /// </summary>
        /// <param name="broker">The broker whose loans we're finding.</param>
        /// <returns>The list of loan ids.</returns>
        public static IEnumerable<Guid> FindOldInUseLoansAtBroker(BrokerDB broker)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", broker.BrokerID),
                new SqlParameter("@QuickPricer2InUseLoanExpirationDays", ConstStage.QuickPricer2InUseLoanExpirationDays)
            };

            var loanIDs = new List<Guid>();
            using (var reader 
                = StoredProcedureHelper.ExecuteReader(
                    broker.BrokerID,
                    GetSprocName(QuickPricer2StoredProcedure.FindOldInUseLoansAtBroker),
                    sqlParameters))
            {
                while (reader.Read())
                {
                    loanIDs.Add((Guid)reader["LoanID"]);
                }
            }

            return loanIDs;
        }

        /// <summary>
        /// Finds pooled loans that don't match the broker's current quickpricer template.
        /// </summary>
        /// <param name="broker">The broker whose loans we're finding.</param>
        /// <returns>The list of loan ids.</returns>
        public static IEnumerable<Guid> FindUnusedLoansNotMatchingQPTemplateAtBroker(BrokerDB broker)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", broker.BrokerID),
                new SqlParameter("@QuickPricerTemplateID", broker.QuickPricerTemplateId)
            };

            var loanIDs = new List<Guid>();
            using (var reader
                = StoredProcedureHelper.ExecuteReader(
                    broker.BrokerID,
                    GetSprocName(QuickPricer2StoredProcedure.FindUnusedLoansNotMatchingQPTemplateAtBroker),
                    sqlParameters))
            {
                while (reader.Read())
                {
                    loanIDs.Add((Guid)reader["LoanID"]);
                }
            }

            return loanIDs;
        }
        
        /// <summary>
        /// Attempts to *actually* delete the loan specified at the given broker. <para></para>
        /// First it checks we can really delete the loan. <para></para>
        /// Then it looks for associated files and deletes those <para></para>
        /// Lastly, it deletes the file from the DB <para></para>
        /// Note, the DB may be modified or may have additional tables where the loan's presence is felt but isn't deleted.  <para></para>
        /// This is a best guess deletion (see opm 189298 for more details on how the guess was made.)<para></para>
        /// </summary>
        /// <param name="broker">The broker whose loan we're deleting.</param>
        /// <param name="loanID">The loan to delete.</param>
        /// <param name="reasonDenied">The reason the deletion won't take place.</param>
        /// <returns>True if successfully deleted the loan.</returns>
        public static bool TryBestEffortDeleteLoan(BrokerDB broker, Guid loanID, out string reasonDenied)
        {
            var principal = (SystemUserPrincipal)PrincipalFactory.CurrentPrincipal;
            reasonDenied = "not determined";
            int numAffected = -1;
            IEnumerable<FileDBEntryInfo> fileDBEntryInfosFromDB;

            if (principal != SystemUserPrincipal.QP2SystemUser)
            {
                reasonDenied = "You are not logged in properly to delete loans.";
                return false;
            }

            string reasonCantDelete = string.Empty;
            IEnumerable<FileDBEntryInfo> dummyFileDBEntryInfos; // we won't use these, we will instead use the one from the deletion sproc.

            if (!CanDeleteLoan(broker, loanID, true, true, true, out reasonCantDelete, out dummyFileDBEntryInfos))
            {
                reasonDenied = reasonCantDelete;
                return false;
            }

            Tools.LogInfo("Deleting_loan: " + loanID + " at Broker: " + broker.Name + " " + broker.BrokerID);

            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", broker.BrokerID),
                new SqlParameter("@LoanID", loanID),
                new SqlParameter("@BrokerQuickPricerTemplateID", broker.QuickPricerTemplateId),
                new SqlParameter("@BrokerPMLTemplateID", broker.PmlLoanTemplateID),
                new SqlParameter("@QuickPricer2InUseLoanExpirationDays", ConstStage.QuickPricer2InUseLoanExpirationDays), 
                new SqlParameter("@IWillDeleteFromTransient", true),
                new SqlParameter("@IWillDeleteAllConditionsFromFileDB", true),
                new SqlParameter("@IWillDeleteAllHardCodedFileDBFiles", true)
            };

            // Delete from Main DB.
            using (var reader = StoredProcedureHelper.ExecuteReaderAndLogPrinting(
                broker.BrokerID,
                GetSprocName(QuickPricer2StoredProcedure.BestEffortDeleteLoan),
                sqlParameters))
            {
                fileDBEntryInfosFromDB = GetFileDBEntryInfosFromDB(reader);
            }
            
            var transientSqlParameters = new SqlParameter[]
            {
                new SqlParameter("@LoanID", loanID),
                new SqlParameter("@IAlreadyDeletedFromRegularDB", true),
                new SqlParameter("@IWillDeleteFromFileDB", true)
            };

            // Delete from Transient DB.
            numAffected = StoredProcedureHelper.ExecuteNonQuery(
                    DataSrc.LOTransient,
                    "TRANSIENT_DB_DeleteLoanPresence",
                    2,
                    transientSqlParameters);
            
            // Delete Files.
            var filesToDeleteInFileB = GetAllowedFileDBEntryInfosForQuickPricer2(loanID).Union(fileDBEntryInfosFromDB);
            foreach (var fileDBEntryInfo in filesToDeleteInFileB)
            {
                var fileDBType = fileDBEntryInfo.FileDBType;
                var fileKey = fileDBEntryInfo.Key;
                try
                {
                    FileDBTools.Delete(fileDBType, fileKey);
                }
                catch (SocketException se)
                {
                    var msg = string.Format(
                        "During_deletion - unable to delete at least one filedb files for fileDBType '{2}', loanID '{0}', filedbkey '{1}'",
                        loanID, // {0}
                        fileKey, // {1}
                        fileDBType.ToString("g")) + // {2}
                        Environment.NewLine +
                        ErrorMessages.SocketExceptionWhenUsingFileDB3_ConfigurationFix;
                    Tools.LogError(msg, se);
                    throw;
                }
            }

            Tools.LogInfo("Deleted_loan: " + loanID + " at Broker: " + broker.Name + " " + broker.BrokerID);
            reasonDenied = string.Empty;
            return true;
        }

        /// <summary>
        /// Checks we can really delete the loan. <para></para>
        /// </summary>
        /// <param name="broker">The broker whose loan we're deleting.</param>
        /// <param name="loanID">The loan to delete.</param>
        /// <param name="iWillDeleteFromTransient">You can't delete unless you promise to delete all the transient db associated rows.</param>
        /// <param name="iWillDeleteAllHardCodedFileDBFiles">You can't delete unless you promise to delete all the filedb files.</param>
        /// <param name="iWillDeleteAllConditionsFromFileDB">You can't delete unless you promise to delete the associated conditions from the fileDB.</param>
        /// <param name="reasonCantDeleteLoan">Describes why you can't delete the loan, or empty.</param>
        /// <param name="fileDBEntryInfos">The entries from the database that need to be deleted from the filedb.</param>
        /// <returns>True only if we can delete the loan.</returns>
        public static bool CanDeleteLoan(
            BrokerDB broker, 
            Guid loanID, 
            bool iWillDeleteFromTransient,  // should there be anything there.
            bool iWillDeleteAllHardCodedFileDBFiles, // e.g. pinStates, etc.
            bool iWillDeleteAllConditionsFromFileDB, // should there be any.
            out string reasonCantDeleteLoan, // string.Empty if you can delete the loan.
            out IEnumerable<FileDBEntryInfo> fileDBEntryInfos) 
        {
            bool canDeleteLoan = false;
            fileDBEntryInfos = new List<FileDBEntryInfo>();
            
            var abstractPrincipal = PrincipalFactory.CurrentPrincipal;
            if (!(abstractPrincipal is SystemUserPrincipal))
            {
                reasonCantDeleteLoan = "You are not logged in properly to be able to delete loans.";
                
                return false;
            }
            
            var principal = (SystemUserPrincipal)abstractPrincipal;

            if (principal != SystemUserPrincipal.QP2SystemUser)
            {
                reasonCantDeleteLoan = "You are not logged in correctly to be able to delete loans.";
                return false;
            }

            var canDeleteLoanParameter = new SqlParameter("@CanDeleteLoan", SqlDbType.Bit);
            canDeleteLoanParameter.Direction = ParameterDirection.Output;
            var reasonCantDeleteLoanParameter = new SqlParameter("@ReasonCantDeleteLoan", SqlDbType.VarChar, 1000);
            reasonCantDeleteLoanParameter.Direction = ParameterDirection.Output;

            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", broker.BrokerID),
                new SqlParameter("@LoanID", loanID),
                new SqlParameter("@BrokerQuickPricerTemplateID", broker.QuickPricerTemplateId),
                new SqlParameter("@BrokerPMLTemplateID", broker.PmlLoanTemplateID),
                new SqlParameter("@QuickPricer2InUseLoanExpirationDays", ConstStage.QuickPricer2InUseLoanExpirationDays),  
                new SqlParameter("@IWillDeleteFromTransient", iWillDeleteFromTransient),
                new SqlParameter("@IWillDeleteAllConditionsFromFileDB", iWillDeleteAllConditionsFromFileDB),
                new SqlParameter("@IWillDeleteAllHardCodedFileDBFiles", iWillDeleteAllHardCodedFileDBFiles),
                canDeleteLoanParameter,
                reasonCantDeleteLoanParameter
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(
                    broker.BrokerID,
                    GetSprocName(QuickPricer2StoredProcedure.CanDeleteLoan),
                    sqlParameters))
            {
                fileDBEntryInfos = GetFileDBEntryInfosFromDB(reader);
            }
            
            canDeleteLoan = (bool)canDeleteLoanParameter.Value;
            reasonCantDeleteLoan = (string)reasonCantDeleteLoanParameter.Value;

            if (canDeleteLoan)
            {
                // TODO: Update this when FileDBTools.Exists exists.
                var filesNotToDelete = GetDisallowedFileDBEntryInfosForQuickPricer2(loanID);
                foreach (var fileDBEntryInfo in filesNotToDelete)
                {
                    var fileDBType = fileDBEntryInfo.FileDBType;
                    var fileKey = fileDBEntryInfo.Key;
                    try
                    {
                        var dummyBytes = FileDBTools.ReadData(fileDBType, fileKey);
                        reasonCantDeleteLoan = fileKey + " existed in the fileDBType " + fileDBType.ToString("g") + " when it shouldn't have";
                        return false;
                    }
                    catch (FileNotFoundException)
                    {
                        // TODO: When they add the FileDB.Exists method, use that instead of this try catch block. 
                    }
                    catch (SocketException se)
                    {
                        var msg = string.Format(
                            "During_deletion_check - unable to check file status of at least one filedb files for loanID '{0}', filedbkey '{1}', fileDBType '{2}'",
                            loanID,
                            fileKey,
                            fileDBType.ToString("g")) + Environment.NewLine + ErrorMessages.SocketExceptionWhenUsingFileDB3_ConfigurationFix;
                        Tools.LogError(msg, se);
                        throw;
                    }
                }
            }
            
            return canDeleteLoan;            
        }

        /// <summary>
        /// Checks to see if a loan is present anywhere in the main DB, transient DB, or file DB.
        /// </summary>
        /// <param name="broker">The broker the loan belongs to.</param>
        /// <param name="loanID">The id of the loan in question.</param>
        /// <param name="placesWhereItsPresent">Comma separated list that may begin with a comma.</param>
        /// <returns>Returns true if the loan is present in the main DB, transient DB, or FileDB.</returns>
        public static bool LoanHasAnyPresence(
            BrokerDB broker,
            Guid loanID,
            out string placesWhereItsPresent)
        {
            bool hasPresence = false;
            placesWhereItsPresent = string.Empty;

            var transientHasPresenceParameter = new SqlParameter("@HasPresence", SqlDbType.Bit);
            transientHasPresenceParameter.Direction = ParameterDirection.Output;
            var transientTablesWhereItsPresentParameter = new SqlParameter("@TablesWhereItsPresent", SqlDbType.VarChar, 1000);
            transientTablesWhereItsPresentParameter.Direction = ParameterDirection.Output;
            var transientSqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", broker.BrokerID),
                new SqlParameter("@LoanID", loanID),
                transientHasPresenceParameter,
                transientTablesWhereItsPresentParameter,
            };

            int numAffected = StoredProcedureHelper.ExecuteNonQuery(
                DataSrc.LOTransient,
                "TRANSIENT_DB_LoanHasAnyPresence",
                2,
                transientSqlParameters);
            hasPresence = hasPresence || (bool)transientHasPresenceParameter.Value;
            placesWhereItsPresent += (string)transientTablesWhereItsPresentParameter.Value;

            var hasPresenceParameter = new SqlParameter("@HasPresence", SqlDbType.Bit);
            hasPresenceParameter.Direction = ParameterDirection.Output;
            var tablesWhereItsPresentParameter = new SqlParameter("@TablesWhereItsPresent", SqlDbType.VarChar, 1000);
            tablesWhereItsPresentParameter.Direction = ParameterDirection.Output;
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", broker.BrokerID),
                new SqlParameter("@LoanID", loanID),
                hasPresenceParameter,
                tablesWhereItsPresentParameter,
            };

            numAffected = StoredProcedureHelper.ExecuteNonQuery(
                broker.BrokerID,
                GetSprocName(QuickPricer2StoredProcedure.LoanHasAnyPresence),
                2,
                sqlParameters);

            hasPresence = hasPresence || (bool)hasPresenceParameter.Value;
            placesWhereItsPresent += (string)tablesWhereItsPresentParameter.Value;
            
            var allFileDBFileEntries = GetDisallowedFileDBEntryInfosForQuickPricer2(loanID).Union(
                GetAllowedFileDBEntryInfosForQuickPricer2(loanID));

            foreach (var fileDBEntryInfo in allFileDBFileEntries)
            {
                var fileDBType = fileDBEntryInfo.FileDBType;
                var fileKey = fileDBEntryInfo.Key;
                try
                {
                    var dummyBytes = FileDBTools.ReadData(fileDBType, fileKey);
                    hasPresence = true;
                    placesWhereItsPresent += ", " + fileDBEntryInfo;
                }
                catch (FileNotFoundException)
                {
                    // TODO: When they add the FileDB.Exists method, use that instead of this try catch block. 
                }
                catch (SocketException se)
                {
                    var msg = string.Format(
                        "During_presence_check - unable to check file status of at least one filedb files for loanID '{0}', filedbkey '{1}', fileDBType '{2}",
                        loanID,
                        fileKey,
                        fileDBType.ToString("g")) + Environment.NewLine + ErrorMessages.SocketExceptionWhenUsingFileDB3_ConfigurationFix;
                    Tools.LogError(msg, se);
                    throw;
                }
            }

            return hasPresence;
        }

        /// <summary>
        /// Gets the rate monitor name associated with the loan.  Assumes the file must be in use.
        /// </summary>
        /// <param name="brokerID">The id of the lender where the loan is at.</param>
        /// <param name="loanID">The loan in question.</param>
        /// <param name="userIDOfUsingUser">The id of the user.</param>
        /// <returns>The name of the rate monitor scenario.</returns>
        public static string GetRateMonitorNameByLoanID(Guid brokerID, Guid loanID, Guid userIDOfUsingUser)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", brokerID),
                new SqlParameter("@LoanID", loanID),
                new SqlParameter("@UserIDOfUsingUser", userIDOfUsingUser)
            };

            using (var reader = StoredProcedureHelper.ExecuteReader(
                brokerID,
                GetSprocName(QuickPricer2StoredProcedure.GetRateMonitorNameByLoanID),
                sqlParameters))
            {
                if (reader.Read())
                {
                    return reader.AsObject<string>("RateMonitorName");
                }
                else
                {
                    throw new ArgumentException("Could not find loan with specified parameters." +
                        string.Join(", ", sqlParameters.Select((p) => p.AsSimpleFormatString()).ToArray()));
                }
            }
        }

        /// <summary>
        /// Gets quickpricer 2.0 pooled "in use" loans with rate monitor scenario names for the given broker and user.
        /// </summary>
        /// <param name="brokerID">The id of the lender of interest.</param>
        /// <param name="userID">The id of the user of interest.</param>
        /// <returns>An enumeration of scenarios.</returns>
        public static IEnumerable<RateMonitorScenarioInfo> GetRateMonitoredLoansByUserID(Guid brokerID, Guid userID)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", brokerID),
                new SqlParameter("@UserIDOfUsingUser", userID)
            };

            var scenarios = new List<RateMonitorScenarioInfo>();
            using (var reader = StoredProcedureHelper.ExecuteReader(
                brokerID,
                GetSprocName(QuickPricer2StoredProcedure.GetRateMonitoredLoansByUserID),
                sqlParameters))
            {
                while (reader.Read())
                {
                    var scenario = new RateMonitorScenarioInfo(
                        brokerID, 
                        (Guid)reader["LoanID"],
                         userID,
                        (string)reader["RateMonitorName"],
                        (E_BranchChannelT)((int)reader["sBranchChannelT"]),
                        (E_sCorrespondentProcessT)((int)reader["sCorrespondentProcessT"]));
                    scenarios.Add(scenario);
                }
            }

            return scenarios;
        }

        /// <summary>
        /// Nullifies the monitor name and deletes all rate monitors associated with the loan.
        /// </summary>
        /// <param name="principal">The current principal.</param>
        /// <param name="loanID">The id of the loan to scrap the scenarios.</param>
        public static void DeleteMonitoredScenario(AbstractUserPrincipal principal, Guid loanID)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", principal.BrokerId),
                new SqlParameter("@UserID", principal.UserId),
                new SqlParameter("@LoanID", loanID)
            };

            int numAffected = StoredProcedureHelper.ExecuteNonQuery(
                principal.BrokerId,
                GetSprocName(QuickPricer2StoredProcedure.DeleteMonitoredScenario),
                0,
                sqlParameters);
        }

        /// <summary>
        /// Gets the fileDB entries from the reader, which can come from either the can delete loan stored procedure <para></para>
        /// or the best effort delete loan stored procedure.
        /// </summary>
        /// <param name="reader">Comes from either the can delete loan or the best effort delete loan stored procedures.</param>
        /// <returns>The list of entries in the filedb.</returns>
        private static IEnumerable<FileDBEntryInfo> GetFileDBEntryInfosFromDB(DbDataReader reader)
        {
            string tableName;
            Guid fileDBGuid;
            var fileDBEntryInfos = new List<FileDBEntryInfo>();

            while (reader.Read())
            {
                tableName = (string)reader["TableName"];
                fileDBGuid = (Guid)reader["FileDBGuid"];

                switch (tableName.ToUpper())
                {
                    case "CONDITION":
                        fileDBEntryInfos.Add(
                            new FileDBEntryInfo(
                                E_FileDB.Normal,
                                fileDBGuid.ToString("N")));
                        break;
                    case "DISCUSSION_LOG":
                        fileDBEntryInfos.Add(
                            new FileDBEntryInfo(
                                E_FileDB.Normal,
                                fileDBGuid.ToString("N").ToUpper()));
                        break;
                    default:
                        throw new NotSupportedException(string.Format(
                            "It is not clear what to do with the filedbkey {0} from table {1} ",
                            fileDBGuid,  // {0}
                            tableName)); // {1}
                }
            }

            reader.NextResult(); // this allows the sproc to continue execution with the logging.
            return fileDBEntryInfos;
        }

        /// <summary>
        /// Tells if the QuickPricer 2.0 file is expected to have the specified type of file.
        /// </summary>
        /// <param name="keyType">The file type.</param>
        /// <returns>True only if we expect the loan to have that type of file.</returns>
        private static bool IsQP2FileAllowedToHaveFileDBEntryOfType(E_FileDBKeyType keyType)
        {
            switch (keyType)
            {
                case E_FileDBKeyType.Normal_Loan: // has case E_FileDBKeyType.aLiaXmlContent: case E_FileDBKeyType.aAssetXmlContent: and sCondXmlContent sections based on those keys.
                case E_FileDBKeyType.Normal_Loan_PricingStateXml:
                case E_FileDBKeyType.Temp_Loan_LPEFirstResult:
                case E_FileDBKeyType.Temp_Loan_LPESecondResult:
                case E_FileDBKeyType.Normal_Loan_audit: // will just delete audits instead of trying to prevent them for maintentance / performance opm 198983
                case E_FileDBKeyType.Temp_Loan_backup_audit:
                case E_FileDBKeyType.Normal_Loan_sAgentXmlContent:
                case E_FileDBKeyType.Normal_Loan_sPmlCertXmlContent:
                case E_FileDBKeyType.Normal_Loan_sClosingCostSetJsonContent:
                case E_FileDBKeyType.Normal_Loan_sProdPmlDataUsedForLastPricingXmlContent:
                case E_FileDBKeyType.Normal_Loan_sPreparerXmlContent:
                    return true;
                case E_FileDBKeyType.Normal_Loan_approval_pdf:
                case E_FileDBKeyType.Normal_Loan_COMPLIANCE_EAGLE_REPORT_PDF:
                case E_FileDBKeyType.Normal_Loan_COMPLIANCE_EASE_REPORT_PDF:
                case E_FileDBKeyType.Normal_Loan_FNMA_DWEB_CASEFILEIMPORT:
                case E_FileDBKeyType.Normal_Loan_FNMA_EARLYCHECK:
                case E_FileDBKeyType.Normal_Loan_FNMA_GETCREDITAGENCIES:
                case E_FileDBKeyType.Normal_Loan_GLOBAL_DMS_INFO_XML:
                case E_FileDBKeyType.Normal_Loan_suspense_pdf:
                case E_FileDBKeyType.Normal_Loan_ratelock_pdf:
                case E_FileDBKeyType.Normal_Loan_pmlsummary_pdf:
                case E_FileDBKeyType.Normal_Loan_CLOSING_COST_MIGRATION_ARCHIVE:
                case E_FileDBKeyType.Normal_Loan_coc_archive:
                case E_FileDBKeyType.Normal_Loan_DU_FINDINGS_HTML:
                case E_FileDBKeyType.Normal_Loan_FNMA_CASEFILE_EXPORT:
                case E_FileDBKeyType.Normal_Loan_FNMA_CASEFILE_IMPORT:
                case E_FileDBKeyType.Normal_Loan_FNMA_DU_UNDERWRITE:
                case E_FileDBKeyType.Normal_Loan_FNMA_DWEB_CASEFILESTATUS:
                case E_FileDBKeyType.Normal_Loan_FREDDIE_XML:
                case E_FileDBKeyType.Normal_Loan_gfe_archive:
                case E_FileDBKeyType.Normal_Loan_pmlactivity:
                case E_FileDBKeyType.Temp_Loan_COMPLIANCE_EAGLE_REPORT_PDF:                    
                case E_FileDBKeyType.Normal_Loan_AutoDisclosureAuditHtml:
                case E_FileDBKeyType.Normal_ClosingCostArchive:
                case E_FileDBKeyType.Normal_LpePriceGroup_Content:
                case E_FileDBKeyType.Normal_FeeServiceRevision_File:
                case E_FileDBKeyType.Normal_Loan_sTpoRequestForInitialDisclosureGenerationEventsJsonContent:
                case E_FileDBKeyType.Normal_Loan_sTpoRequestForRedisclosureGenerationEventsJsonContent:
                case E_FileDBKeyType.Normal_Loan_sTpoRequestForInitialClosingDisclosureGenerationEventsJsonContent:
                case E_FileDBKeyType.Edms_CustomPdf_Form:
                case E_FileDBKeyType.Normal_CustomNonPdf_Form:
                case E_FileDBKeyType.Normal_Loan_sFHACaseQueryResultXmlContent:
                case E_FileDBKeyType.Normal_Loan_sFHACaseNumberResultXmlContent:
                case E_FileDBKeyType.Normal_Loan_sFHACAVIRSResultXmlContent:
                case E_FileDBKeyType.Normal_Loan_sTotalScoreCertificateXmlContent:
                case E_FileDBKeyType.Normal_Loan_sTotalScoreTempCertificateXmlContent:
                    return false;
                default:
                    throw new UnhandledEnumException(keyType);
            }
        }

        /// <summary>
        /// The keys of files that a QuickPricer 2.0 loan file *is* expected to have.
        /// </summary>
        /// <param name="loanID">The loan in question.</param>
        /// <returns>The file db keys.</returns>
        private static IEnumerable<FileDBEntryInfo> GetAllowedFileDBEntryInfosForQuickPricer2(Guid loanID)
        {
            var fileDBEntryInfos = new List<FileDBEntryInfo>();

            foreach (E_FileDBKeyType loanKeyType in Tools.SpecificallyLoanFileDBKeyTypes)
            {
                if (IsQP2FileAllowedToHaveFileDBEntryOfType(loanKeyType))
                {
                    fileDBEntryInfos.Add(Tools.GetFileDBEntryInfoForLoan(loanKeyType, loanID));
                }
            }

            return fileDBEntryInfos.ToArray();
        }

        /// <summary>
        /// The keys of files that a QuickPricer 2.0 loan file is *not* expected to have.
        /// </summary>
        /// <param name="loanID">The loan in question.</param>
        /// <returns>The file db keys.</returns>
        private static IEnumerable<FileDBEntryInfo> GetDisallowedFileDBEntryInfosForQuickPricer2(Guid loanID)
        {
            var fileDBEntryInfos = new List<FileDBEntryInfo>();

            foreach (E_FileDBKeyType loanKeyType in Tools.SpecificallyLoanFileDBKeyTypes)
            {
                if (!IsQP2FileAllowedToHaveFileDBEntryOfType(loanKeyType))
                {
                    fileDBEntryInfos.Add(Tools.GetFileDBEntryInfoForLoan(loanKeyType, loanID));
                }
            }

            return fileDBEntryInfos.ToArray();
        }

        /// <summary>
        /// From the enum, returns the actual stored procedure name to be used in queries. <para></para>
        /// </summary>
        /// <param name="sproc">The QuickPricer2StoredProcedure type to use.</param>
        /// <returns>The procedure name to be used in queries.</returns>
        private static string GetSprocName(QuickPricer2StoredProcedure sproc)
        {
            return QPTableName + "_" + sproc.ToString("G");
        }

        /// <summary>
        /// The principal's broker must have a QuickPricer template and must not have QuickPricer 2.0 disabled. <para></para>
        /// If marking in use, the principal passed in must match the current principal. <para></para>
        /// </summary>
        /// <param name="principal">The principal to add the loan to.</param>
        /// <param name="markInUse">Whether or not to mark the loan as in use.</param>
        private static void AllowUseOrThrow(AbstractUserPrincipal principal, bool markInUse)
        {
            var broker = BrokerDB.RetrieveById(principal.BrokerId);
            var quickPricerTemplateID = broker.QuickPricerTemplateId;
            if (quickPricerTemplateID == Guid.Empty)
            {
                throw new AccessDenied("You may not create a quickpricer sandboxed loan without a quickpricer template chosen.");
            }

            if (broker.Status != 1)
            {
                throw new AccessDenied("You may not create a quickpricer sandboxed loan for an inactive lender.");
            }

            if (E_Pml2AsQuickPricerMode.Disabled == broker.Pml2AsQuickPricerMode)
            {
                throw new AccessDenied("You may not create a quickpricer sandboxed loan when the feature is disabled at the lender-level.");
            }

            if (!principal.IsActuallyUsePml2AsQuickPricer)
            {
                throw new AccessDenied("You may not create a quickpricer sandboxed loan when the feature is disabled for you.");
            }

            var currentPrincipal = PrincipalFactory.CurrentPrincipal;
            if (markInUse)
            {
                if (currentPrincipal == null || currentPrincipal is InternalUserPrincipal)
                {
                    throw new AccessDenied("Internal / null principal may not create a quickpricer2 loan and mark it as in use.");
                }

                if (currentPrincipal.BrokerId != principal.BrokerId)
                {
                    throw CBaseException.GenericException("Programming error.  Tried to create QP2 loan in broker outside principal's broker.");
                }

                if (currentPrincipal.UserId != principal.UserId)
                {
                    throw CBaseException.GenericException("Cannot create in use loan for another user.");
                }
            }
        }

        /// <summary>
        /// Creates a sandboxed loan at the specified broker from their current quickpricer template. <para></para>
        /// If mark as in use, marks as in use by the current user (which should match what is supplied). <para></para>
        /// Do not call with markInUse = true when not currently a user principal. <para></para>
        /// </summary>
        /// <param name="principal">The principal to add the loan to.</param>
        /// <param name="markInUse">False unless someone is expecting to use the loan right away.</param>
        /// <returns>The id of the loan that was created.</returns>
        private static Guid CreateSandboxedLoan(AbstractUserPrincipal principal, bool markInUse)
        {
            AllowUseOrThrow(principal, markInUse);

            var broker = BrokerDB.RetrieveById(principal.BrokerId);
            var quickPricerTemplateID = broker.QuickPricerTemplateId;

            var loanFileCreator = CLoanFileCreator.GetCreator(principal, E_LoanCreationSource.FromTemplate); // source type not actually audited.
            Guid loanID = Guid.Empty;
            try
            {
                loanID = loanFileCreator.CreateQP2SandboxFile(quickPricerTemplateID: quickPricerTemplateID);

                Tools.TransformData(loanID, true, true, E_LpeRunModeT.Full, true); // old quickpricer loans were transformed with lperunmode full.
            }
            catch (Exception)
            {
                markInUse = true;
                throw;
            }
            finally
            {
                if (loanID != Guid.Empty)
                {
                    AddLoanToPool(principal, loanID, quickPricerTemplateID, markInUse);
                }
            }

            return loanID;
        }

        /// <summary>
        /// Adds a previously created loan to the pool.
        /// </summary>
        /// <param name="principal">The principal to add the loan to.</param>
        /// <param name="loanID">The id of the loan to add to the pool.</param>
        /// <param name="quickPricerTemplateID">The id of the quickpricer template the loan came from.</param>
        /// <param name="markInUse">Whether or not to mark the loan as being used by the current user.</param>
        private static void AddLoanToPool(AbstractUserPrincipal principal, Guid loanID, Guid quickPricerTemplateID, bool markInUse)
        {
            AllowUseOrThrow(principal, markInUse);

            var sqlParameters = new List<SqlParameter>() 
            {
                new SqlParameter("@BrokerID", principal.BrokerId),
                new SqlParameter("@LoanID", loanID),
                new SqlParameter("@QuickPricerTemplateID", quickPricerTemplateID),
                new SqlParameter("@UserIDofUsingUser", principal.UserId)
            };

            var sprocName = markInUse 
                ? GetSprocName(QuickPricer2StoredProcedure.AddInUseSandboxedLoanToPool)
                : GetSprocName(QuickPricer2StoredProcedure.AddUnusedSandboxedLoanToPool);

            var numAffected = StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, sprocName, 3, sqlParameters);
        }

        /// <summary>
        /// Gets the number of unused quickpricer 2.0 loans matching the broker's quickpricer template for the supplied user.
        /// </summary>
        /// <param name="principal">The principal whose unused loans we'll find.</param>
        /// <returns>The number of unused loans.</returns>
        private static int GetNumberOfUnusedLoans(AbstractUserPrincipal principal)
        {
            E_BranchChannelT branchChannel = E_BranchChannelT.Blank;
            E_sCorrespondentProcessT correspondentProcess = E_sCorrespondentProcessT.Blank;

            switch (principal.PortalMode)
            {
                case E_PortalMode.Broker:
                    branchChannel = E_BranchChannelT.Wholesale;
                    break;
                case E_PortalMode.MiniCorrespondent:
                    branchChannel = E_BranchChannelT.Correspondent;
                    correspondentProcess = E_sCorrespondentProcessT.MiniCorrespondent;
                    break;
                case E_PortalMode.Correspondent:
                    branchChannel = E_BranchChannelT.Correspondent;
                    PmlBroker pB = PmlBroker.RetrievePmlBrokerById(principal.PmlBrokerId, principal.BrokerId);

                    if (pB.UnderwritingAuthority == E_UnderwritingAuthority.Delegated)
                    {
                        correspondentProcess = E_sCorrespondentProcessT.Delegated;
                    }
                    else
                    {
                        correspondentProcess = E_sCorrespondentProcessT.PriorApproved;
                    }

                    break;
                case E_PortalMode.Retail:
                    branchChannel = E_BranchChannelT.Retail;
                    break;
                case E_PortalMode.Blank:
                    break;
                default:
                    throw new UnhandledEnumException(principal.PortalMode);
            }

            var sqlParameters = new SqlParameter[]
            {
                // TODO: add the file version if we had it cached.   
                new SqlParameter("@BrokerID", principal.BrokerId),
                new SqlParameter("@QuickPricerTemplateID", principal.BrokerDB.QuickPricerTemplateId),
                new SqlParameter("@UserIdOfUsingUser", principal.UserId),
                new SqlParameter("@BranchChannelT", branchChannel),
                new SqlParameter("@CorrespondentProcessT", correspondentProcess),
                new SqlParameter("@IsWholesale", branchChannel == E_BranchChannelT.Wholesale)
            };

            return (int)StoredProcedureHelper.ExecuteScalar(principal.BrokerId, GetSprocName(QuickPricer2StoredProcedure.GetNumUnusedLoans), sqlParameters);
        }
    }
}
