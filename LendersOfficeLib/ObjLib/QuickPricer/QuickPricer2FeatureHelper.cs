﻿// <copyright file="QuickPricer2FeatureHelper.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: Scott Kibler
//    Date:   8/18/2014
// </summary>
namespace LendersOffice.ObjLib.QuickPricer
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Constants;

    /// <summary>
    /// Stores the names of the stored procedures that are related to the pooling mechanism. <para></para>
    /// Procedures should be named QuickPricer_2_LOAN_USAGE_[EnumName].
    /// </summary>
    public enum QuickPricer2FeatureStoredProcedure
    {
        /// <summary>
        /// FindBroker = 0.
        /// </summary>
        FindBroker = 0,

        /// <summary>
        /// FindAbandonedLoans = 1.
        /// </summary>
        FindAbandonedLoans = 1,
    }

    /// <summary>
    /// Methods to help understand the state of the QuickPricer 2.0 feature, including: <para></para>
    /// Users, brokers, loans, etc. <para></para>
    /// </summary>
    public static class QuickPricer2FeatureHelper
    {
        /// <summary>
        /// QUICKPRICER_2_FEATURE <para></para>
        /// Any stored procedures associated with this class will start with this prefix. <para></para>
        /// </summary>
        private const string SprocPrefix = "QUICKPRICER_2_FEATURE";

        /// <summary>
        /// Gets all active brokers with QuickPricer 2.0 mode set to Yes, that is, all users would have it enabled.
        /// </summary>
        /// <returns>The list of broker ids.</returns>
        public static IEnumerable<Guid> GetActiveBrokersWithFeatureEnabledForAllUsers()
        {
            return GetActiveBrokersWithPml2AsQuickPricerMode(E_Pml2AsQuickPricerMode.Enabled);
        }

        /// <summary>
        /// Gets all active brokers with QuickPricer 2.0 mode set to No, that is, all users would have it disabled.
        /// </summary>
        /// <returns>The list of broker ids.</returns>
        public static IEnumerable<Guid> GetActiveBrokersWithFeatureNotDisabled()
        {
            var brokerIDs = new List<Guid>();

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                var sqlParameters = new SqlParameter[]
                {
                    new SqlParameter("@Pml2AsQuickPricerModeExcluded", E_Pml2AsQuickPricerMode.Disabled)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, GetSprocName(QuickPricer2FeatureStoredProcedure.FindBroker), sqlParameters))
                {
                    while (reader.Read())
                    {
                        brokerIDs.Add((Guid)reader["BrokerID"]);
                    }
                }
            }

            return brokerIDs;
        }

        /// <summary>
        /// Gets all active brokers with QuickPricer 2.0 mode set to Per-User, that is, the feature would only be turned on <para></para>
        /// on user-specific basis.
        /// </summary>
        /// <returns>The list of broker ids.</returns>
        public static IEnumerable<Guid> GetActiveBrokersWithFeatureEnabledPerUser()
        {
            return GetActiveBrokersWithPml2AsQuickPricerMode(E_Pml2AsQuickPricerMode.PerUser);
        }

        /// <summary>
        /// Gets all active brokers with QuickPricer 2.0 mode set to the mode passed in.
        /// </summary>
        /// <param name="qp2mode">The mode yes, no, or per user.</param>
        /// <returns>The list of broker ids.</returns>
        public static IEnumerable<Guid> GetActiveBrokersWithPml2AsQuickPricerMode(E_Pml2AsQuickPricerMode qp2mode)
        {
            var brokerIDs = new List<Guid>();
            string procedureName = GetSprocName(QuickPricer2FeatureStoredProcedure.FindBroker);

            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                var sqlParameters = new SqlParameter[]
                    {
                        new SqlParameter("@Pml2AsQuickPricerModeIncluded", qp2mode)
                };

                using (var reader = StoredProcedureHelper.ExecuteReader(connInfo, procedureName, sqlParameters))
                {
                    while (reader.Read())
                    {
                        brokerIDs.Add((Guid)reader["BrokerID"]);
                    }
                }
            }

            return brokerIDs;
        }

        /// <summary>
        /// Returns some user info for each user that would have IsActuallyUsePml2AsQuickPricer enabled. <para></para>
        /// That is, it checks if they have QP and NewPML enabled in addition to having QP2 enabled.
        /// </summary>
        /// <param name="broker">The broker to get the users from.</param>
        /// <returns>User info of the users with quickpricer, new pml, and quickpricer 2.0 enabled.</returns>
        public static IEnumerable<LightWeightUserInfo> GetActiveQP2UsersAtBroker(BrokerDB broker)
        {
            var sqlParameters = new SqlParameter[]
            {
                new SqlParameter("@BrokerID", broker.BrokerID),
                new SqlParameter("@BrokerHasQPEnabled", broker.IsQuickPricerEnable),
                new SqlParameter("@BrokerHasNewPMLEnabled", broker.IsNewPmlUIEnabled),
                new SqlParameter("@BrokerHasQP2EnabledForAll", E_Pml2AsQuickPricerMode.Enabled == broker.Pml2AsQuickPricerMode),
                new SqlParameter("@AllowQuickPricer2InTPOPortal", ConstStage.AllowQuickPricer2InTPOPortal)
            };
            var userInfos = new List<LightWeightUserInfo>();
            using (var reader = StoredProcedureHelper.ExecuteReader(broker.BrokerID, "Users_GetQP2UsersAtBroker", sqlParameters))
            {
                while (reader.Read())
                {
                    userInfos.Add(new LightWeightUserInfo((Guid)reader["UserID"], (string)reader["Type"], (string)reader["LoginNm"]));
                }
            }

            return userInfos;
        }

        /// <summary>
        /// Gets a list of QuickPricer 2.0 Sandboxed loans that are considered to be abandoned, that is, they do not have an entry <para></para>
        /// in the pool. <para></para>
        /// </summary>
        /// <param name="customTimeoutInSeconds">Defaults to 240.</param>
        /// <returns>A list of loans as light weight info.</returns>
        public static IEnumerable<LightWeightLoanInfo> GetAbandonedSandboxedLoans(int? customTimeoutInSeconds)
        {
            var sqlParameters = new SqlParameter[]
            {
            };
            var loanInfos = new List<LightWeightLoanInfo>();
            using (var reader = StoredProcedureHelper.ExecuteReader(
                DataSrc.LOShareROnly,
                GetSprocName(QuickPricer2FeatureStoredProcedure.FindAbandonedLoans),
                customTimeoutInSeconds.HasValue ? customTimeoutInSeconds.Value : 240,
                sqlParameters))
            {
                while (reader.Read())
                {
                    loanInfos.Add(
                        new LightWeightLoanInfo(
                            (Guid)reader["LoanID"],
                            reader.AsNullableStruct<Guid>("BrokerID"),
                            reader.AsObject<string>("LoanName"),
                            reader.AsNullableStruct<bool>("IsValid")));
                }
            }

            return loanInfos;
        }

        /// <summary>
        /// Gets the full stored procedure name given the type.
        /// </summary>
        /// <param name="sproc">The type of the stored procedure.</param>
        /// <returns>The string value of the stored procedure full name.</returns>
        private static string GetSprocName(QuickPricer2FeatureStoredProcedure sproc)
        {
            return SprocPrefix + "_" + sproc.ToString("g");
        }

        /// <summary>
        /// A small storage class for loan info.
        /// </summary>
        public class LightWeightLoanInfo
        {   
            /// <summary>
            /// Initializes a new instance of the <see cref="LightWeightLoanInfo"/> class.
            /// </summary>
            /// <param name="loanID">The id of the loan.</param>
            /// <param name="brokerID">The id of the broker for the loan, if it is known.</param>
            /// <param name="loanName">The name of the loan, if it is known.</param>
            /// <param name="isValid">The validity of the loan, if it is known.</param>
            public LightWeightLoanInfo(Guid loanID, Guid? brokerID, string loanName,  bool? isValid)
            {
                this.LoanID = loanID;
                this.BrokerID = brokerID;
                this.LoanName = loanName;
                this.IsValid = isValid;
            }

            /// <summary>
            /// Gets the id of the loan.
            /// </summary>
            /// <value>The id of the loan.</value>
            public Guid LoanID { get; private set; }

            /// <summary>
            /// Gets the id of the broker that the loan belongs to.
            /// </summary>
            /// <value>The id of the broker for the loan, if it is known.</value>
            public Guid? BrokerID { get; private set; }

            /// <summary>
            /// Gets the name of the loan.
            /// </summary>
            /// <value>The name of the loan, if it is known.</value>
            public string LoanName { get; private set; }

            /// <summary>
            /// Gets the validity status of the loan.
            /// </summary>
            /// <value>The validity of the loan, if it is known.</value>
            public bool? IsValid { get; private set; }
        }

        /// <summary>
        /// A small storage class for user info.
        /// </summary>
        public class LightWeightUserInfo
        {
            /// <summary>
            /// Initializes a new instance of the <see cref="LightWeightUserInfo"/> class.
            /// </summary>
            /// <param name="userID">The id of the user.</param>
            /// <param name="userType">The type of the user. ('B', 'P', etc.).</param>
            /// <param name="loginName">The login name of the user.</param>
            public LightWeightUserInfo(Guid userID, string userType, string loginName)
            {
                this.UserID = userID;
                this.UserType = userType;
                this.LoginName = loginName;
            }

            /// <summary>
            /// Gets the id of the user.
            /// </summary>
            /// <value>The id of the user.</value>
            public Guid UserID { get; private set; }

            /// <summary>
            /// Gets the type of the user. ('B', 'P', etc.).
            /// </summary>
            /// <value>The type of the user.</value>
            public string UserType { get; private set; }

            /// <summary>
            /// Gets the LoginName of the user.
            /// </summary>
            /// <value>The login name of the user.</value>
            public string LoginName { get; private set; }

            /// <summary>
            /// Summarizes the user info.  Intended for use with logging.
            /// </summary>
            /// <returns>A string of the user info summary.</returns>
            public string ToSimpleStringForLog()
            {
                return string.Format(
                    "Type: '{0}'\tUserID: '{1}'\tLoginName:'{2}'",
                    this.UserType, // {0}
                    this.UserID, // {1}
                    this.LoginName);
            }
        }
    }
}
