﻿namespace LendersOffice.ObjLib.QuickPricer
{
    /// <summary>
    /// Manually entered bankruptcy.
    /// </summary>
    public enum ManuallyEnteredBankruptcy
    {
        None = 0,
        Within12Months = 1,
        Within24Months = 2,
        Within36Months = 3,
        Within48Months = 4
    }
}
