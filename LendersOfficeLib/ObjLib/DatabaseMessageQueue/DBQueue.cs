using System;
using System.Threading; 
using System.Data;
using System.Collections;
using System.Data.Common;
using System.Data.SqlTypes;
using DataAccess;

namespace LendersOffice.ObjLib.DatabaseMessageQueue
{
	/// <summary>
	/// Represents a unique messaging queue. The id pertains to the id of the queue in the database. This class also contains a static method which is executed
	/// by a thread repeatedly. The thread updates the archiving hashtable that will keep track of which queue requires archive. 
	/// av 09-11-07
	/// </summary>
	public class DBQueue 
	{

		#region instance variables

		/// <summary>
		/// The queue id of this queue. It should match the database. 
		/// </summary>
		private int m_QueueId;

		/// <summary>
		/// The name of the queue should match the name in the database, but it doesnt have to. 
		/// </summary>
		private string m_QueueName; 
		#endregion

		#region constructors
		/// <summary>
		/// Create a new queue. 
		/// </summary>
		/// <param name="QueueId">The integer queue id of the message queue. This should match the database.</param>
		/// <param name="QueueName">The name of the queue. It doesnt have to match the database.</param>
		public DBQueue(int QueueId, string QueueName) 
		{
			m_QueueId = QueueId;
			m_QueueName = QueueName;
		}
		#endregion 

		#region Properties 
		/// <summary>
		/// Gets the queue id.
		/// </summary>
		public int Id 
		{
			get { return m_QueueId; }
		}

		/// <summary>
		/// Gets the name of the queue.
		/// </summary>
		public string Name 
		{
			get { return m_QueueName;}
		}	

		/// <summary>
		/// Returns whether the messages should be archived. This is updated from the database periodically.
		/// </summary>
		public bool RequiresArchive
		{
			get 
			{
				return false;
			}
		}

		/// <summary>
		/// Gets the next priority the message queue should try to receive. The priority should only be set when retrieving a message from the queue
		/// and it does not equal the priority requested.
		/// </summary>
		public int Priority 
		{
			get 
			{
				return 1; 
			}

		
		}
		#endregion


		#region public methods 
		/// <summary>
		/// Toggles the archiving to the passed in value. This takes about 5 minutes to update in the program. 
		/// The time span can be changed in ConstMsg. 
		/// </summary>
		/// <param name="Archive">Whether archiving should be turned on or off.</param>
		public void ToggleArchiving()
		{
	
		}

	
		/// <summary>
		/// Creates the queue with the given name and sets the default archiving value.
		/// </summary>
		/// <param name="Name">The name of the queue to be inserted into the database.</param>
		/// <param name="RequiresArchive"> Bool indicating whether the queue should archive messages.</param></param>
		/// <returns></returns>
		public static int CreateQueue(string Name, bool RequiresArchive) 
		{
			int QueueId; 
			using ( var Connection = DbAccessUtils.GetConnection(DataSrc.LOShare) ) 
			{
				using ( var Command = Connection.CreateCommand()) 
				{
                    Command.CommandText = "QCreate";
                    Command.CommandType  = CommandType.StoredProcedure; 
					Command.Parameters.Add( new System.Data.SqlClient.SqlParameter("@Name", Name));
					Command.Parameters.Add( new System.Data.SqlClient.SqlParameter("@RequiresArchive", RequiresArchive));
					Command.Parameters.Add(new System.Data.SqlClient.SqlParameter("RETURN_VALUE", System.Data.SqlDbType.Int ));
					Command.Parameters[2].Direction = ParameterDirection.ReturnValue;
					Connection.Open();
					Command.ExecuteNonQuery();
					QueueId = (int) Command.Parameters[2].Value;
				}
			}

			return QueueId; 
		}

		/// <summary>
		/// Deletes the specified q. All existing messages will be dropped, please clean out the queue yourself first. 
		/// </summary>
		/// <param name="Id">The id of the queue to drop.</param>
		/// <returns></returns>
		public static void DropQueue( int Id ) 
		{
            using (var Connection = DbAccessUtils.GetConnection(DataSrc.LOShare)) 
			{
				using ( var Command = Connection.CreateCommand() ) 
				{
                    Command.CommandText = "QDrop";
                    Command.CommandType = CommandType.StoredProcedure; 
					Command.Parameters.Add(new System.Data.SqlClient.SqlParameter("@QueueId", Id) );
					Connection.Open();
					Command.ExecuteNonQuery();
				}
			}
		}

		/// <summary>
		/// Returns a dataset containing all the queues defined in the queue table. 
		/// </summary>
		public static DataSet GetQueues() 
		{
			DataSet QueueList = new DataSet();

            using (var Connection = DbAccessUtils.GetConnection(DataSrc.LOShare)) 
			{
				using( var Command = Connection.CreateCommand() ) 
				{
                    Command.CommandText = "QList";
                    Command.CommandType = CommandType.StoredProcedure; 
					Connection.Open(); 

					using ( DbDataAdapter DataAdapter = DbAccessUtils.GetDbProvider().CreateDataAdapter() ) 
					{
                        DataAdapter.SelectCommand = Command;
						DataAdapter.Fill(QueueList);
					}

				}
			}

			return QueueList; 
		}
		#endregion

	}
}
