namespace LendersOffice.ObjLib.DatabaseMessageQueue
{
    using System;
    using System.IO;
    using System.Text;
    using DataAccess;

    public class DBMessage  
	{
		#region Private Variables 
		private string m_sSubject1;
		private string m_sData; 
		private string m_sSubject2;
		private short m_Priority;
		private long m_Id; 
		private string m_sDBData; 
		private DateTime m_InsertionTime; 

		private StorageType m_StorageType; 
		#endregion

		#region Properties
		/// <summary>
		/// Gets the data. 
		/// </summary>
		public string Data 
		{
			get{ return m_sData; }
		}

		/// <summary>
		/// Gets a stream to the data.
		/// </summary>
		public Stream BodyStream 
		{
			get { return new MemoryStream( UnicodeEncoding.UTF8.GetBytes( m_sData ) ); } 
		}

		/// <summary>
		/// Gets the id.
		/// </summary>
		public long Id
		{
			get { return m_Id; }
		}

		/// <summary>
		/// Gets the label.
		/// </summary>
		public string Subject2 
		{
			get { return m_sSubject2; }
		}
		/// <summary>
		/// Gets the DBData
		/// </summary>
		public string DBData 
		{
			get { return m_sDBData; }
		}

		/// <summary>
		/// Gets the insertion time.
		/// </summary>
		public DateTime InsertionTime 
		{
			get { return m_InsertionTime; }
		}

		public short Priority 
		{
			get { return m_Priority; }
		}

		public string Subject1 
		{
			get { return m_sSubject1; }
		}
		/// <summary>
		/// Gets the data location type in string format. 
		/// </summary>
		public string DataLocation 
		{
			get { return m_StorageType == StorageType.Database ? ConstMsg.DB_LOC : ConstMsg.FILEDB_LOC; } 
		}
		#endregion


		#region constructor
		/// <summary>
		/// Creates a new Database Message object. This method should be used only by the messagueue send operation. 
		/// </summary>
		/// <param name="Subject">String thats no more than 300 characters long</param>
		/// <param name="Data">String any size. The data that less than 5500 will be stored int he db directly, others will be stored in filedb.</param>
		/// <param name="Label">String thats no more than 1000</param>
		/// <param name="Priority">short ranging from 1 to 5</param>
		public DBMessage(string Subject, string Data, string Label, short Priority)
		{
			init(Subject, Label, Priority); 
			m_sData = Data; 
			SaveData(); 
		}

		/// <summary>
		/// Creates a new Database Message object. This method should only be used by the receive operation.
		/// </summary>
		public DBMessage(long Id, string Subject, string DBData, string Label, short Priority, string DataLoc, bool RequiresArchive, bool FetchData, DateTime InsertionTime) 
		{	m_Id = Id; 
			init(Subject, Label, Priority); 
			m_InsertionTime = InsertionTime; 
			RetrieveData(DBData, RequiresArchive, FetchData, DataLoc); 

		}

		#endregion

		/// <summary>
		/// Initializes common private variables 
		/// </summary>
		private void init(string Subject1, string Subject2, short Priority) 
		{
			m_sSubject1 = Subject1;
			m_sSubject2 = Subject2; 
			m_Priority = Priority;
		}
		/// <summary>
		/// Stores the data in the database or in the file database.
		/// </summary>
		private void SaveData() 
		{
			if ( m_sData.Length > ConstMsg.DATA_MAX_LENGTH  ) 
			{
				m_StorageType = StorageType.FileDB; 
				m_sDBData = CreateFile(m_sData);
			} 
			else 
			{
				m_StorageType = StorageType.Database; 
				m_sDBData = m_sData; 
			}

		}

		/// <summary>
		/// Retrieves the data from the database.
		/// </summary>
		/// <param name="DBData"></param>
		private void RetrieveData(string DBData, bool RequiresArchive, bool FetchData, string DataLoc) 
		{
			if ( DataLoc.ToLower() == ConstMsg.FILEDB_LOC ) 
			{
				m_StorageType = StorageType.FileDB; 
				m_sData = RetrieveFile(DBData);
				
				if (! RequiresArchive ) 
				{
					m_sDBData = DBData;
					DeleteFile();	
				}

			}
			else 
			{
				m_StorageType = StorageType.Database;
				m_sData = DBData;
				m_sDBData = DBData;
			}
		}		



		/// <summary>
		/// Creates a file and puts in FileDB. 
		/// </summary>
		/// <param name="message">The data to store in the file.</param>
		/// <returns>The key to the file.</returns>
		private string CreateFile( string message) 
		{
			string fileDBKey =  Guid.NewGuid().ToString("N") + ConstMsg.FILE_SUFFIX;
            byte[] data = UnicodeEncoding.UTF8.GetBytes(message);
            FileDBTools.WriteData(E_FileDB.Temp, fileDBKey, data);

			return fileDBKey; 
		}
			
		/// <summary>
		/// Deletes the specified file from the FileDB3 database.
		/// </summary>
		/// <param name="key">The key to the file to delete.</param>
		public void DeleteFile() 
		{
            FileDBTools.Delete(E_FileDB.Temp, m_sDBData);
		}
	

		/// <summary>
		/// Retrieves the file specified by the parameters.
		/// </summary>
		/// <param name="key">The key of the file to retrieve.</param>
		/// <returns>Empty string or a string representation of the file</returns>
		private string  RetrieveFile( string Key) 
		{
			string data = String.Empty;

            try
            {
                FileDBTools.UseFile(E_FileDB.Temp, Key, (fileInfo) =>
                {
                    using (StreamReader reader = new StreamReader(fileInfo.FullName, UnicodeEncoding.UTF8))
                    {
                        data = reader.ReadToEnd();
                    }
                });
            }
            catch (FileNotFoundException)
            {
                throw new DBMessageQueueException("Message Has been Processed", DBMessageQueueErrorCode.MessageProcessed);
            }

			return data;
		}

		/// <summary>
		/// If the given DataLoc string is fildb then the give key is deleted from file database.
		/// </summary>
		/// <param name="DataLoc">String containing the locaton ( either db or filedb) </param>
		/// <param name="Key">The name of the file in the file database. </param>
		static public void DeleteData( string DataLoc, string Key ) 
		{
			if ( DataLoc.ToLower() == ConstMsg.FILEDB_LOC ) 
			{
                FileDBTools.Delete(E_FileDB.Temp, Key);
			}
		}
	}

	public class DBMessageQueueException : ApplicationException 
	{
		private DBMessageQueueErrorCode m_MessageQueueErrorCode;
		
		public DBMessageQueueErrorCode MessageQueueErrorCode 
		{
			get{ return m_MessageQueueErrorCode; }
		}

		public DBMessageQueueException(string Message, DBMessageQueueErrorCode Id)  : base ( Message ) 
		{
			m_MessageQueueErrorCode = Id; 					
		}
	}

	public enum DBMessageQueueErrorCode 
	{
		IOTimeout = 0, 
		MessageProcessed = 1, 
		NonExistantQueue = 2,
		EmptyQueue = 3,
		MessageSendingError = 4,
		MaxRetries = 5,
		UnexpectedError = 6
	}
}