using System;
using System.Threading;


namespace LendersOffice.ObjLib.DatabaseMessageQueue
{
	public class ConstMsg
	{
		public ConstMsg()
		{
		}
		
		public static readonly	  int QueueMaxRetries		  =  5;
		public static readonly    int ReaderWriterLockTimeOut = -1; 
		public static readonly	  int MaxMsgRetry			  = 5;  
		public static readonly	  int SleepTime				  = 200; //200 ms

		public static readonly	  int DATA_MAX_LENGTH			= 5500;
		public static readonly	  int SUBJECT1_MAX_LENGTH		= 300;
		public static readonly	  int SUBJECT2_MAX_LENGTH		= 300; 
		public static readonly	short DEFAULT_MESSAGE_PRIORITY	= 2;
		public static readonly	short MAX_MESSAGE_PRIORITY		= 5;

		public static readonly string FILE_SUFFIX				= "_LOQUEUE";

		public static readonly  int[] PRIORITY_MAX_COUNT = new int[] { 0,20,15,9,5,3 };  // Priority 0 1 2 3 4 5 

		public static readonly string SCHEMA_INSERTIONTIME   = "InsertionTime"; 
		public static readonly string SCHEMA_REMOVALTIME     = "RemovalTime";
		public static readonly string SCHEMA_MESSAGE_ID      = "MessageId"; 
		public static readonly string SCHEMA_PRIORITY	     = "Priority"; 
		public static readonly string SCHEMA_SUBJECT1		 = "Subject1"; 
		public static readonly string SCHEMA_DATALOC	     = "DataLoc";
		public static readonly string SCHEMA_SUBJECT2	     = "Subject2"; 
		public static readonly string SCHEMA_DATA		     = "Data"; 
		public static readonly string SCHEMA_QUEUEID		 = "QueueId";
		public static readonly string SCHEMA_REQUIRESARCHIVE = "RequiresArchive";





		public static readonly int ARCHIVING_HASH_TIMEOUT = -1; //no timeout
		
		public static string DB_LOC = "db"; 
		public static string FILEDB_LOC = "filedb"; 

		#region Queues 
		public static readonly DBQueue RoleQueue = new DBQueue((int) QueueType.Role, "Role");
		public static readonly DBQueue TaskQueue = new DBQueue((int) QueueType.Task, "Task");
        public static readonly DBQueue Task2011Queue = new DBQueue((int)QueueType.Task2011, "Task2011");
		public static readonly DBQueue EmailQueue= new DBQueue((int) QueueType.Email,"Email");
		public static readonly DBQueue NHCQueue  = new DBQueue((int) QueueType.NHC, "Nhc" );
        public static readonly DBQueue H4HQueue = new DBQueue((int)QueueType.H4H, "H4H");
        public static readonly DBQueue EDocsQueue = new DBQueue((int)QueueType.EDocs, "EDocs");
        public static readonly DBQueue PNGConversionQueue = new DBQueue((int)QueueType.PNGConversion, "PNGConversion");
        public static readonly DBQueue PNGConversionPMLQueue = new DBQueue((int)QueueType.PNGConversionPML, "PNGConversionPML");
        public static readonly DBQueue PNGConversionFaxQueue = new DBQueue((int)QueueType.PNGConversionFax, "PNGConversionFax");
        public static readonly DBQueue AccessControlQueue = new DBQueue((int)QueueType.AccessControl, "AccessControl");
        public static readonly DBQueue DocMagicDocSplitterQueue = new DBQueue((int)QueueType.DocMagicDocSplitter, "DocMagicDocSplitter");
        public static readonly DBQueue WorkflowCacheUpdaterQueue = new DBQueue((int)QueueType.WorkflowCacheUpdater, "WorkflowCacheUpdater");
        public static readonly DBQueue DocMagicAutoSubmitQueue = new DBQueue((int)QueueType.DocMagicAutoSubmit, "DocMagicAutoSubmit");
        public static readonly DBQueue ComplianceEaseAutoSubmitQueue = new DBQueue((int)QueueType.ComplianceEaseAutoSubmit, "ComplianceEaseAutoSubmit");
        public static readonly DBQueue ComplianceEaseAutoSubmitSecondaryQueue = new DBQueue((int)QueueType.ComplianceEaseAutoSubmitSecondary, "ComplianceEaseAutoSubmitSecondary");
        public static readonly DBQueue ComplianceEagleAutoSubmitQueue = new DBQueue((int)QueueType.ComplianceEagleAutoSubmit, "ComplianceEagleAutoSubmit");
        public static readonly DBQueue CopyEDocQueue = new DBQueue((int)QueueType.CopyEDoc, "CopyEDoc");
        public static readonly DBQueue DisclosureQueue = new DBQueue((int)QueueType.Disclosure, "Disclosure");
        public static readonly DBQueue GdmsAutoSaveDocsQueue = new DBQueue((int)QueueType.GDMSAutoSaveDocs, "GdmsAutoSaveDocs");
        public static readonly DBQueue QuickPricer2LoansCreationQueue = new DBQueue((int)QueueType.QuickPricer2LoansCreation, "QuickPricer2Creation");
        public static readonly DBQueue ComplianceEaseAutoSubmitTestQueue = new DBQueue((int)QueueType.ComplianceEaseAutoSubmitTest, "ComplianceEaseAutoSubmitTest");
        public static readonly DBQueue AutoPriceEligibilityQueue = new DBQueue((int)QueueType.AutoPriceEligibility, "AutoPriceEligibility");

        #endregion

        public static readonly TimeSpan RequiresArchiveUpdater = new TimeSpan(0,0,5,0); //Refresh archive values every five minutes. 
	}

	/// <summary>
	/// Each entry should have a matching entry in the database with the same id.
	/// </summary>
	public enum QueueType:int
	{
		Role = 1,
		Email = 2,
		Task = 3,
		NHC = 4,
        Zillow = 5,
        AccessControl = 37,
        H4H = 36,
        Dummy = 35,
        EDocs = 38,
        PNGConversion = 46,
        PNGConversionPML = 47,
        PNGConversionFax = 48,
        Task2011 = 49,
        DocMagicDocSplitter = 50,
        WorkflowCacheUpdater = 51,
        DocMagicAutoSubmit = 52,
        ComplianceEaseAutoSubmit = 53,
        ComplianceEaseAutoSubmitSecondary = 55,
        CopyEDoc = 56,
        Disclosure = 57,
        GDMSAutoSaveDocs = 58,
        ComplianceEagleAutoSubmit = 60,
        QuickPricer2LoansCreation = 62,
        ComplianceEaseAutoSubmitTest = 63,
        AutoPriceEligibility = 65
    }

	/// <summary>
	/// Names the different types of places the data can be stored.
	/// </summary>
	public enum StorageType:int 
	{
		Database = 0,
		FileDB = 1
	}



	

}
