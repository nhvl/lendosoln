﻿namespace LendersOffice.ObjLib.DatabaseMessageQueue
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents a set of statistics on a particular queue.
    /// </summary>
    [DataContract]
    public class DBMessageQueueStatistic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DBMessageQueueStatistic" /> class.
        /// </summary>
        public DBMessageQueueStatistic()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DBMessageQueueStatistic" /> class.
        /// </summary>
        /// <param name="id">The id of the queue.</param>
        /// <param name="name">The name of the queue.</param>
        /// <param name="count">The count of the queue.</param>
        /// <param name="oldestMessageInSeconds">The age of the oldest message in seconds.</param>
        /// <param name="environment">The environment the queue is for.</param>
        /// <param name="timeFetched">The time the queue was fetched in PST.</param>
        public DBMessageQueueStatistic(int id, string name, int count, int oldestMessageInSeconds, string environment, DateTime timeFetched)
        {
            this.Id = id;
            this.Name = name;
            this.Count = count;
            this.OldestMessageAgeInSeconds = oldestMessageInSeconds;
            this.Timestamp = timeFetched.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            this.Environment = environment;
        }

        /// <summary>
        /// Gets the id of the queue.
        /// </summary>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the name of the queue.
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; private set; }

        /// <summary>
        /// Gets the number of messages in the queue.
        /// </summary>
        [DataMember(Name = "count")]
        public int Count { get; private set; }

        /// <summary>
        /// Gets the age of the oldest message in the queue in seconds.
        /// </summary>
        [DataMember(Name = "oldest_message_age_in_seconds")]
        public int OldestMessageAgeInSeconds { get; private set; }

        /// <summary>
        /// Gets the name of the environment.
        /// </summary>
        [DataMember(Name = "env")]
        public string Environment { get; private set; }

        /// <summary>
        /// Gets the timestamp in utc.
        /// </summary>
        [DataMember(Name = "@timestamp")]
        public string Timestamp { get; private set; }
    }
}