namespace LendersOffice.ObjLib.DatabaseMessageQueue
{
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using Sleeping;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    /// <summary>
    /// Implements a Message Queue using the database as the place to store the information. The MessageQueue can handle both
    /// asynchronous and synchronous calls. If a synchronous call comes in the thread waits for any asynchronous calls that were
    /// running to finish and then takes the queue lock. The Queue uses a shared lock between other MessageQueues which
    /// are created from the same Queue. The queues should be define
    /// in the ConstMsg class. 
    /// 
    /// In order to use the message queue, a few variables have to be set first.  ConnectionString has to be set to 
    /// the database that has the tables. FileDbSite code has to also be set. The message queue does not use filedb 
    /// unless the messages are larger than  DATA_MAX_LENGTH(5500) in ConstMsg. Also, 
    /// the last thing that has to be done is the execution of the background thread that updates the archive values.
    /// This thread has to be started before any queue is used! To start the thread Queue, please add this line of code 
    /// DBMessageQueue.DBMessage.Start();' to the projects global.aspx load method. 
    /// av - 09-10-2007
    /// </summary>
    public sealed class DBMessageQueue 
	{

		#region instance variables
		private DBQueue m_Queue;
		/// <summary>
		/// a pointer to the method that acquires the lock  
		/// </summary>
		private delegate void Lock();  
		/// <summary>
		/// a pointer to the method that releases the lock 
		/// </summary>
		private delegate void Unlock();  
		#endregion

		#region properties

		/// <summary>
		/// Retrieves an enumerator to the set of all the db messages in the queue at the time. 
		/// </summary>
		/// <returns>Enumerator to the objects in the queue.</returns>
		public IEnumerator GetEnumerator() 
		{
			return Entries.GetEnumerator();
		}


		/// <summary>
		/// Returns the collection of all the db messages in the queue at the time. This does not remove them from the queue.
		/// </summary>
		public IList Entries 
		{
			get {  return GetMessagesSynchronized(); }
		}
		
		#endregion

		#region constructor
		/// <summary>
		/// Initializes a new instance of the MessageQueue. 
		/// </summary>
		/// <param name="Queue"> DBQueue from ConstMsg.</param>
		public DBMessageQueue(DBQueue Queu) 
		{
			m_Queue = Queu;
		}

		#endregion
		
		#region Lock Methods 
		private void AcquireReaderLock() 
		{
			//m_Queue.ReaderWriterLock.AcquireReaderLock(ConstMsg.ReaderWriterLockTimeOut);
		}
		private void AcquireWriterLock() 
		{
			//m_Queue.ReaderWriterLock.AcquireWriterLock(ConstMsg.ReaderWriterLockTimeOut);
		}
		private void ReleaseReaderLock() 
		{
			//m_Queue.ReaderWriterLock.ReleaseReaderLock(); 
		}
		private void ReleaseWriterLock() 
		{
			//m_Queue.ReaderWriterLock.ReleaseWriterLock(); 
		}
		#endregion 

		#region public asynchronized methods
		/// <summary>
		/// Retrieves all the messages from the message queue without deleting them. This method does not wait until 
		/// other methods are done reading causing messages to be missing ( Those that may have been handled somewhere else. )
		/// </summary>
		/// <returns>An array with all the messages in the queue that have not been handled.</returns>
		public DBMessage[] GetMessages()
		{
			return SecureGetMessages(new Lock(AcquireReaderLock), new Unlock(ReleaseReaderLock));
		}

		/// <summary>
		/// Receives a message from the queue.
		/// </summary>
		/// <returns>An object representting the message from the queue.</returns>
		public DBMessage Receive() 
		{
			return SecureReceive(new Lock(AcquireReaderLock), new Unlock(ReleaseReaderLock));
		}


		/// <summary>
		/// Retrieves a list of all the messages in the queue at a certain time and then tries to retrieve each one by id. 
		/// Another instance of this class using the same Queue object can still remove the message.
		/// </summary>
		/// <returns>An array of DBMessages.</returns>
		public DBMessage[] ReceiveAll() 
		{
			return SecureReceivedAll(new Lock(AcquireReaderLock), new Unlock(ReleaseReaderLock));
		}

        public DBMessage[] PeekN(int n)
        {
            return SecurePeekN(new Lock(AcquireReaderLock), new Unlock(ReleaseReaderLock), ConstMsg.DEFAULT_MESSAGE_PRIORITY, n);
        }

        public void DequeueRange(long inclusiveStartId, long inclusiveEndId)
        {
            SecureDequeueRange(new Lock(AcquireReaderLock), new Unlock(ReleaseReaderLock), inclusiveStartId, inclusiveEndId, ConstMsg.DEFAULT_MESSAGE_PRIORITY);
        }

        public DBMessage[] PeekBySubject()
        {
            return SecurePeekBySubject(new Lock(AcquireReaderLock), new Unlock(ReleaseReaderLock), ConstMsg.DEFAULT_MESSAGE_PRIORITY);
        }

        public void DequeueBySubject(string subject1, long inclusiveStartId, long inclusiveEndId)
        {
            SecureDequeueBySubject(new Lock(AcquireReaderLock), new Unlock(ReleaseReaderLock), subject1, inclusiveStartId, inclusiveEndId, ConstMsg.DEFAULT_MESSAGE_PRIORITY);
        }

	
		/// <summary>
		/// Retrieves the message with the given id. If the message does not exist an exception is thrown.
		/// </summary>
		/// <param name="id">The id of the wanted message.</param>
		/// <returns>The message with the given id.</returns>
		public DBMessage ReceiveById(long id) 
		{
			return SecureReceiveById(new Lock(AcquireReaderLock), new Unlock(ReleaseReaderLock), id);
		}
        ///// <summary>
        ///// Turns the given information into a message and adds it to the queue.
        ///// </summary>
        ///// <param name="Subject1">Small text about the message.</param>
        ///// <param name="Data">Object to store in the queue. This method calls ToString() on the object.</param>
        ///// <returns>The id of the message.</returns>
        //public long Send(string Subject1, object Data) 
        //{
        //    return Send(Subject1, Data.ToString());
        //}


		/// <summary>
		/// Turns the given information into a message and adds it to the queue.
		/// </summary>
		/// <param name="Subject1">Short text about the message.</param>
		/// <param name="Data">Object to store in the queue. This method calls ToString() on the object.</param>
		/// <param name="Subject2">2nd level description ( 300 ) </param>
		/// <param name="Priority">A short from 1 to 5</param>
		/// <returns>The id of the message.</returns>
		public long Send(string Subject1, string Data, string Subject2, short Priority ) 
		{
			return Send(Subject1, Data, Priority,Subject2, false );
		}

		/// <summary>
		/// Turns the given information into a message and adds it to the queue.
		/// </summary>
		/// <param name="Subject1">A description of the message. (300)</param>
		/// <param name="Data">The body of the message. This can be any length.</param>
		/// <param name="Subject2">Secondary description</param>
		/// <returns>The id of the message.</returns>
		public long Send(string Subject1, string Data, string Subject2) 
		{
			return Send(Subject1, Data, -1 ,Subject2, false);
		}
        /// <summary>
        /// Will add the given item only if Subject1 is not already there.
        /// </summary>
        /// <param name="Subject1"></param>
        /// <param name="Data"></param>
        /// <param name="Subject2"></param>
        /// <returns></returns>
        public long SendIfNew(string Subject1, string Data, string Subject2)
        {
            return Send(Subject1, Data, -1, Subject2, true);
        }

		/// <summary>
		/// Turns the given information into a message and adds it to the queue.
		/// </summary>
		/// <param name="Subject1">Descriont about the message. (300 char)</param>
		/// <param name="Body">The message itself. </param>
		/// <returns>The id of the message.</returns>
		public long Send(string Subject1, string Data )
		{
			return Send(Subject1, Data, -1, String.Empty, false);
		}

		/// <summary>
		/// Turns the given information into a message and adds it to the queue.
		/// </summary>
		/// <param name="Subject1">Short descriont of the message ( 300 char)</param>
		/// <param name="Body">The message itself.</param>
		/// <param name="Priority">A short from 1(high) to 5(low).</param>
		/// <returns>The id of the message in the queue.</returns>
		public long Send(string Subject1, string Body, short Priority )
		{
			return Send(Subject1, Body, Priority, String.Empty, false);
		}
		/// <summary>
		/// Turns the given information into a message and adds it to the queue.
		/// </summary>
		/// <param name="Body"> The message itself.</param>
		/// <param name="Subject2">Secondary description</param>
		/// <param name="Priority">A short from 1(high) to 5(low).</param>
		/// <param name="Subject1">A small description about the message.</param>
		/// <returns>The id of the message in the queue. -1 if there is no data or the Subject1 is empty.</returns>
		private long Send(string Subject1, string Body, short Priority, string Subject2, bool enforceUniqueSubject1 ) 
		{

			if ( Body.Length == 0 || Subject1.Length == 0) 
			{
				return -1L; 
			}
			if ( Priority < 1 )  //if the priority is less than one reset it to default 
				Priority = ConstMsg.DEFAULT_MESSAGE_PRIORITY; 
			else if ( Priority > ConstMsg.MAX_MESSAGE_PRIORITY )  //if the user gave a  number thats too big set it to the highest number
				Priority = ConstMsg.MAX_MESSAGE_PRIORITY; 

			int CurrentAttemptNumber = 0; 
			int SleepTime = ConstMsg.SleepTime;

			DBMessage message;
			try 
			{
				message = new DBMessage(Subject1, Body, Subject2, Priority);
			} 
			catch ( Exception e) 
			{
				Tools.LogError("Failed to create the message! Sent is giving up.",e); 
				throw;
			}
		
			Hashtable SqlParams = new Hashtable(8); 
			SqlParams.Add("QueueId", new SqlParameter("@QueueId", m_Queue.Id)); 
			SqlParams.Add("Subject1", new SqlParameter("@Subject1", Subject1));
			SqlParams.Add("Data", new SqlParameter("@Data", message.DBData));
			SqlParams.Add("Priority", new SqlParameter("@Priority", Priority));
			SqlParams.Add("Subject2", new SqlParameter("@Subject2", Subject2));
			SqlParams.Add("MessageId", new SqlParameter("@MessageId", SqlDbType.BigInt));
			SqlParams.Add("ReturnValue", new SqlParameter("RETURN_VALUE", SqlDbType.Int));
			SqlParams.Add("DataLoc", new SqlParameter("@DataLoc", message.DataLocation));
            SqlParams.Add("EnforceUniqueSubject1", new SqlParameter("@EnforceUniqueSubject1", enforceUniqueSubject1));

			(SqlParams["MessageId"] as SqlParameter).Direction = ParameterDirection.Output;
			(SqlParams["ReturnValue"] as SqlParameter).Direction = ParameterDirection.ReturnValue;
			
			long id = 0;

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare)) 
			{
				connection.Open();
				using( DbCommand command = connection.CreateCommand())
				{
                    command.CommandText = "QSend";
                    command.Connection = connection;
					command.CommandType = CommandType.StoredProcedure; 
					foreach ( SqlParameter param in SqlParams.Values ) 
						command.Parameters.Add(param);
					do 
					{
						try 
						{
							command.ExecuteNonQuery();
							id = (long)  ( SqlParams["MessageId"] as SqlParameter ).Value;
						}
						catch (Exception e)
						{
							Tools.LogError(e);
							if ( CurrentAttemptNumber++ > ConstMsg.MaxMsgRetry ) 
							{
								Tools.LogError("QUEUE " + m_Queue.Name +" Could Not Send Message", e);
								throw;
							}
							LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(ConstMsg.SleepTime);
						}
					}while ( id < 1 ); 
				}
			}
		
			return id;
		}
		#endregion

		#region public synchronized methods 
		/// <summary>
		/// Clears the queue. 
		/// </summary>
		public void ClearQueue()
		{
			AcquireWriterLock(); 
			DataSet ds = this.UnsecurePeekQueue(true); 
			if ( ds.Tables.Count > 0 ) 
			{
				foreach ( DataRow row in ds.Tables[0].Rows ) 
				{	
					try 
					{
						//if its in filedb we dont need to fetch the data
						UnsecureReceiveById((long)row[ConstMsg.SCHEMA_MESSAGE_ID],false);
					}
					
					catch ( DBMessageQueueException ) 
					{
						//failed getting a message but its ok. This should not happen unless there are other applications
						//eating from the queue. 
						continue; 
					}
				}
			} 

			ReleaseWriterLock();
		}


		/// <summary>
		/// Gets the messages from the queue without deleting them. This method waits until other running methods are finished
		/// before it accesses the queue.
		/// </summary>
		/// <returns>A complete list of the messages in the queue when all other methods are done.</returns>
		public DBMessage[] GetMessagesSynchronized()
		{	
			return SecureGetMessages(new Lock(AcquireWriterLock), new Unlock(ReleaseWriterLock));
		}

		/// <summary>
		/// Locks up the queue and retrieves a message.
		/// </summary>
		/// <returns>The message retrieved.</returns>
		public DBMessage ReceiveSynchronized() 
		{
			return SecureReceive(new Lock(AcquireWriterLock), new Unlock(ReleaseWriterLock));
		}
		
		/// <summary>
		/// Retrieves a list of all the messages in the queue when it is called, it waits until any other things accessing the queue stop.
		/// </summary>
		/// <returns>An array containing all the messages in the queue.</returns>
		public DBMessage[] ReceiveAllSynchronized()
		{
			return  SecureReceivedAll(new Lock(AcquireWriterLock), new Unlock(ReleaseWriterLock));
		}
		/// <summary>
		/// Retrieves the message with the given id. This method waits until others are finished before taking the  message out.
		/// </summary>
		/// <param name="id">The id of the wanted message.</param>
		/// <returns> The message with the given id.</returns>
		public DBMessage ReceiveByIdSynchronized(long id) 
		{
			return SecureReceiveById(new Lock(AcquireWriterLock), new Unlock(ReleaseWriterLock), id);
		}
		#endregion

		#region private unsecure methods

		/////////////////////////THESE METHODS SHOULD ONLY CALL  METHODS IN THIS REGION!///////////////////////////////////

        public int Count
        {
            get
            {
                using (var con = DbAccessUtils.GetConnection(DataSrc.LOShare))
                {
                    con.Open();
                    using (var cmd = con.CreateCommand())
                    {
                        cmd.CommandText = "Q_Count";
                        cmd.Connection = con;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id));
                        return (int)cmd.ExecuteScalar();
                    }
                }
            }
        }

        private DBMessage[] UnsecurePeekAllN(short priority, int n)
        {
            DataSet set = UnsecurePeekN(priority, n);

            // This is basically just copy-paste of UnsecurePeekAllBySubject. Should refactor these.
            if (set.Tables.Count == 0)
            {
                return new DBMessage[] { };
            }

            ArrayList messages = new ArrayList(set.Tables[0].Rows.Count);

            foreach (DataRow row in set.Tables[0].Rows)
            {
                try
                {
                    messages.Add(new DBMessage(

                        (long)row[ConstMsg.SCHEMA_MESSAGE_ID],
                        (string)row[ConstMsg.SCHEMA_SUBJECT1],
                        (string)row[ConstMsg.SCHEMA_DATA],
                        (string)row[ConstMsg.SCHEMA_SUBJECT2],
                        Convert.ToInt16(row[ConstMsg.SCHEMA_PRIORITY]),
                        (string)row[ConstMsg.SCHEMA_DATALOC],
                        true, //RequiresArchive  is true here all the time because we are only getting the messages. 
                        //If this is false the constructor will erase filedb data.
                        true,
                        (DateTime)row[ConstMsg.SCHEMA_INSERTIONTIME]));

                }

                catch (DBMessageQueueException dbmqe)
                {
                    if (dbmqe.MessageQueueErrorCode == DBMessageQueueErrorCode.MessageProcessed)
                        continue;
                    else
                        Tools.LogError("Error in UnsecurePeekAllN", dbmqe);
                }
                catch (Exception e)
                {
                    Tools.LogError("Error in UnsecurePeekAllN ", e);
                }
            }

            return (DBMessage[])messages.ToArray(typeof(DBMessage));
        }

        private DBMessage[] UnsecurePeekAllBySubject(short priority)
        {
            DataSet set = UnsecurePeekBySubject(priority);
            if (set.Tables.Count == 0)
            {
                return new DBMessage[] { };
            }


            ArrayList messages = new ArrayList(set.Tables[0].Rows.Count);

            foreach (DataRow row in set.Tables[0].Rows)
            {
                try
                {
                    messages.Add(new DBMessage(

                        (long)row[ConstMsg.SCHEMA_MESSAGE_ID],
                        (string)row[ConstMsg.SCHEMA_SUBJECT1],
                        (string)row[ConstMsg.SCHEMA_DATA],
                        (string)row[ConstMsg.SCHEMA_SUBJECT2],
                        Convert.ToInt16(row[ConstMsg.SCHEMA_PRIORITY]),
                        (string)row[ConstMsg.SCHEMA_DATALOC],
                        true, //RequiresArchive  is true here all the time because we are only getting the messages. 
                        //If this is false the constructor will erase filedb data.
                        true, 
                        (DateTime)row[ConstMsg.SCHEMA_INSERTIONTIME]));

                }

                catch (DBMessageQueueException dbmqe)
                {
                    if (dbmqe.MessageQueueErrorCode == DBMessageQueueErrorCode.MessageProcessed)
                        continue;
                    else
                        Tools.LogError("Error in UnsecurePeekAllBySubject", dbmqe);
                }
                catch (Exception e)
                {
                    Tools.LogError( "Error in UnsecurePeekAllBySubject ", e);
                }
            }

            return (DBMessage[])messages.ToArray(typeof(DBMessage));
        }

        private void DeleteRangeWithSubjectImpl(string subject1, long startIdInclusive, long endIdInclusive, short priority)
        {
            using (var con = DbAccessUtils.GetConnection(DataSrc.LOShare))
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Q_DeleteRangeBySubject";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id));
                    cmd.Parameters.Add(new SqlParameter("@Archive", m_Queue.RequiresArchive));
                    cmd.Parameters.Add(new SqlParameter("@StartMsgId", startIdInclusive));
                    cmd.Parameters.Add(new SqlParameter("@EndMsgId", endIdInclusive));
                    cmd.Parameters.Add(new SqlParameter("@Subject1", subject1));
                    cmd.Parameters.Add(new SqlParameter("@Priority", priority));
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void DeleteRangeImpl(long startIdInclusive, long endIdInclusive, short priority)
        {
            using (var con = DbAccessUtils.GetConnection(DataSrc.LOShare))
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "Q_DeleteRange";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id));
                    cmd.Parameters.Add(new SqlParameter("@Archive", m_Queue.RequiresArchive));
                    cmd.Parameters.Add(new SqlParameter("@StartMsgId", startIdInclusive));
                    cmd.Parameters.Add(new SqlParameter("@EndMsgId", endIdInclusive));
                    cmd.Parameters.Add(new SqlParameter("@Priority", priority));
                    cmd.ExecuteNonQuery();
                }
            }
        }


		/// <summary>
		/// Returns a list of messages in the queue without deleting them. 
		/// </summary>
		/// <returns>An array of messages in the queue.</returns>
		private DBMessage[] UnsecureGetMessages(bool FetchData) 
		{
			DataSet messageIds = UnsecurePeekQueue(false);
			if ( messageIds.Tables.Count == 0 ) 
				return new DBMessage[] { };

			ArrayList messages = new  ArrayList(messageIds.Tables[0].Rows.Count);

			foreach ( DataRow row in messageIds.Tables[0].Rows ) 
			{
				try 
				{
					messages.Add( new DBMessage(

						(long)   row[ConstMsg.SCHEMA_MESSAGE_ID],
						(string) row[ConstMsg.SCHEMA_SUBJECT1],
						(string) row[ConstMsg.SCHEMA_DATA],
						(string) row[ConstMsg.SCHEMA_SUBJECT2],	
						Convert.ToInt16(row[ConstMsg.SCHEMA_PRIORITY]), 
						(string) row[ConstMsg.SCHEMA_DATALOC],
						true, //RequiresArchive  is true here all the time because we are only getting the messages. 
							  //If this is false the constructor will erase filedb data.
						true, (DateTime) row[ConstMsg.SCHEMA_INSERTIONTIME]));

				}
		
				catch ( DBMessageQueueException dbmqe ) 
				{
					if ( dbmqe.MessageQueueErrorCode == DBMessageQueueErrorCode.MessageProcessed )
						continue; 
					else 
						Tools.LogError("Error in UnsecureGetmessages",dbmqe);
				}
				catch (Exception e)
				{
					Tools.LogError("Error in UnsecureGetMessages ",e);
				}
			}
			return (DBMessage[])messages.ToArray(typeof(DBMessage));
		}
		/// <summary>
		/// Retrieves a Dataset containing all the MessageIds of the entries for the specified queue at the time the method was called. 
		/// </summary>
		/// <param name="IdOnly">Boolean value that if its true will only return the ids of the messages 
		/// if its false it will return the entire message row.
		/// </param>
		/// <returns>A dataset with the messages in the message queue at the point of the call.</returns>
		private DataSet UnsecurePeekQueue(bool IdOnly)  
		{
		
			DataSet PeekSet = new DataSet();
			try 
			{
                using (var sqlConnection = DbAccessUtils.GetConnection(DataSrc.LOShare))
				{
					sqlConnection.Open();
					using (var cm = sqlConnection.CreateCommand()) 
					{
                        cm.CommandText = "QPeek";
                        cm.CommandType = CommandType.StoredProcedure;
						cm.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id));
						cm.Parameters.Add(new SqlParameter("@ById", IdOnly));

						using ( var adapter = DbAccessUtils.GetDbProvider().CreateDataAdapter()) 
						{
                            adapter.SelectCommand = cm;
							adapter.Fill(PeekSet);	
						}
					}
				}
			}
			catch(Exception e)
			{
				Tools.LogError("Error peeking the queue.",e);
				throw;	
			}

			return PeekSet; 
		}

        private DataSet UnsecurePeekN(short priority, int n)
        {
            DataSet peekSet = new DataSet();
            try
            {
                using (var sqlConnection = DbAccessUtils.GetConnection(DataSrc.LOShare))
                {
                    sqlConnection.Open();
                    using (var cm = sqlConnection.CreateCommand())
                    {
                        cm.CommandText = "Q_GetNMessages";
                        cm.CommandType = CommandType.StoredProcedure;
                        cm.Parameters.Add(new SqlParameter("@N", n));
                        cm.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id));
                        cm.Parameters.Add(new SqlParameter("@Priority", priority));

                        using (var adapter = DbAccessUtils.GetDbProvider().CreateDataAdapter())
                        {
                            adapter.SelectCommand = cm;
                            adapter.Fill(peekSet);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                if (e.Message.StartsWith("A network-related or instance-specific error occurred while establishing a connection to SQL Server"))
                {
                    // 2017-10-04 - We randomly have issue of open sql connection. Therefore to avoid the code continuous retry, we are going to throttle the
                    // code.
                    Random rnd = new Random();
                    int milliseconds = rnd.Next(3000, 5000); // Sleep randomly from 3 to 5 seconds.
                    SystemSleeper.Instance.Sleep(milliseconds);
                }

                Tools.LogError("Error Receiving by UnsecurePeekN",e);
                throw;
            }

            return peekSet;
        }

        private DataSet UnsecurePeekBySubject(short priority)
        {
            DataSet peekSet = new DataSet();
            try
            {
                using (var sqlConnection = DbAccessUtils.GetConnection(DataSrc.LOShare))
                {
                    sqlConnection.Open();
                    using (var cm = sqlConnection.CreateCommand())
                    {
                        cm.CommandText = "Q_GetTopSubject";
                        cm.CommandType = CommandType.StoredProcedure;
                        cm.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id));
                        cm.Parameters.Add(new SqlParameter("@Priority", priority));

                        using (var adapter = DbAccessUtils.GetDbProvider().CreateDataAdapter())
                        {
                            adapter.SelectCommand = cm;
                            adapter.Fill(peekSet);
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                if (e.Message.StartsWith("A network-related or instance-specific error occurred while establishing a connection to SQL Server"))
                {
                    // 2017-10-04 - We randomly have issue of open sql connection. Therefore to avoid the code continuous retry, we are going to throttle the
                    // code.
                    Random rnd = new Random();
                    int milliseconds = rnd.Next(3000, 5000); // Sleep randomly from 3 to 5 seconds.
                    SystemSleeper.Instance.Sleep(milliseconds);
                }
                Tools.LogError("Error Receiving by UnsecurePeekBySubject",e);
                throw;
            }

            return peekSet;
        }

		/// <summary>
		/// Retrieves the  next message from the database.
		/// </summary>
		/// <param name="FetchData"> If this is set to 1(true) then if the record is stored in filedb it will be retrieved. 
		/// This is useful when the message is being deleted and we do not need to fetch from filedb before deleting the file.</param>
		/// <returns>A Database message.</returns	
        private DBMessage UnsecureReceive(bool FetchData)
        {
            bool RequiresArchive = m_Queue.RequiresArchive;
            DBMessage message = null;
            DataSet ds = new DataSet();
            SqlParameter returnValue = new SqlParameter("RETURN_VALUE", SqlDbType.Int);
            int ReturnCode = -100;

            try
            {
                using (var sqlConnection = DbAccessUtils.GetConnection(DataSrc.LOShare))
                {
                    sqlConnection.Open();
                    using (var cm = sqlConnection.CreateCommand())
                    {
                        cm.CommandType = CommandType.StoredProcedure;
                        cm.CommandText = "QReceive";
                        Hashtable SqlParams = new Hashtable(4);
                        SqlParams.Add("QueueId", new SqlParameter("@QueueId", m_Queue.Id));
                        SqlParams.Add("Archive", new SqlParameter("@Archive", RequiresArchive));
                        SqlParams.Add("Priority", new SqlParameter("@Priority", m_Queue.Priority));
                        SqlParams.Add("ReturnValue", returnValue);
                        returnValue.Direction = ParameterDirection.ReturnValue;

                        foreach (SqlParameter param in SqlParams.Values)
                        {
                            cm.Parameters.Add(param);
                        }

                        using (var sda = DbAccessUtils.GetDbProvider().CreateDataAdapter())
                        {
                            sda.SelectCommand = cm;
                            sda.Fill(ds);
                            ReturnCode = (int)(SqlParams["ReturnValue"] as SqlParameter).Value;
                        }
                    }
                }
            }
            catch (SqlException exception)
            {
                if (exception.Message.StartsWith("A network-related or instance-specific error occurred while establishing a connection to SQL Server"))
                {
                    // 2017-10-04 - We randomly have issue of open sql connection. Therefore to avoid the code continuous retry, we are going to throttle the
                    // code.
                    Random rnd = new Random();
                    int milliseconds = rnd.Next(3000, 5000); // Sleep randomly from 3 to 5 seconds.
                    SystemSleeper.Instance.Sleep(milliseconds);
                    throw new DBMessageQueueException(exception.ToString(), DBMessageQueueErrorCode.IOTimeout);

                }

                if (returnValue == null || returnValue.Value == null || returnValue.Value == DBNull.Value)
                {
                    ReturnCode = -999;
                }
                else
                {
                    ReturnCode = (int)returnValue.Value;
                }
                switch (ReturnCode)
                {
                    case -1:
                        throw new DBMessageQueueException("Sql Error In Stored Procedure QReceive", DBMessageQueueErrorCode.UnexpectedError);
                    case -2:
                        return message;   //queue is empty
                    case -3:
                        return message; //message has been processed
                    default:
                        throw new DBMessageQueueException("Unexpected return value " + ReturnCode.ToString() + " " + exception.ToString(), DBMessageQueueErrorCode.UnexpectedError);
                }
            }

            if (ReturnCode != 0)
            {
                throw new DBMessageQueueException("No Exception / Non Zero return code " + ReturnCode, DBMessageQueueErrorCode.EmptyQueue);
            }
            try
            {
                if (ds.Tables.Count > 0)
                {
                    DataRow reader = ds.Tables[0].Rows[0];
                    message = new DBMessage((long)reader["MessageId"],
                        (string)reader[ConstMsg.SCHEMA_SUBJECT1],
                        (string)reader[ConstMsg.SCHEMA_DATA],
                        (string)reader[ConstMsg.SCHEMA_SUBJECT2],
                        Convert.ToInt16(reader[ConstMsg.SCHEMA_PRIORITY]),
                        (string)reader[ConstMsg.SCHEMA_DATALOC],
                        RequiresArchive, FetchData, (DateTime)reader[ConstMsg.SCHEMA_INSERTIONTIME]);
                }
            }
            catch (Exception e)  // I doubt this will ever be reached. If so stick the message back in the queue.
            {
                DataRow reader = ds.Tables[0].Rows[0];  //we dont want to stick back a method that was already processed 
                if (e is DBMessageQueueException && ((DBMessageQueueException)e).MessageQueueErrorCode != DBMessageQueueErrorCode.MessageProcessed)
                    Send((string)reader[ConstMsg.SCHEMA_SUBJECT1], (string)reader[ConstMsg.SCHEMA_DATA], (string)reader[ConstMsg.SCHEMA_SUBJECT2], Convert.ToInt16(reader[ConstMsg.SCHEMA_PRIORITY]));
                Tools.LogError(e);
                throw e;
            }
            return message;
        }


		/// <summary>
		/// Retrieves a list of messages in the queue deleting them in the process.
		/// </summary>
		/// <returns>An array containing all the messages that are in the queue.</returns>
		private DBMessage[] UnsecureReceiveAll() 
		{
			DataSet messageIds = this.UnsecurePeekQueue(true);
			if ( messageIds.Tables.Count == 0 ) 
				return new DBMessage[] { };

			ArrayList messages = new ArrayList(messageIds.Tables[0].Rows.Count); //we may have gaps which is why im using the arraylist
			foreach( DataRow row in messageIds.Tables[0].Rows )  
			{	
				try 
				{
					messages.Add(UnsecureReceiveById( (long)row[ConstMsg.SCHEMA_MESSAGE_ID], true));
				}
				catch( DBMessageQueueException e ) 
				{
					if ( e.MessageQueueErrorCode != DBMessageQueueErrorCode.MessageProcessed && e.MessageQueueErrorCode != DBMessageQueueErrorCode.EmptyQueue ) 
					{
						throw e; 
					}
				} 
				catch ( Exception e ) 
				{	
					throw e;
				}

			}
			return (DBMessage[]) messages.ToArray(typeof(DBMessage));
		}
		/// <summary>
		/// Receives a message by id. 
		/// </summary>
		/// <param name="Id">The id of the message.</param>
		/// <param name="FetchData">Tells the method to load the data from filedb if its there.</param>
		/// <returns>A message representing the one with the given id or an exception is thrown if the message is not there.</returns>
		private DBMessage UnsecureReceiveById(long Id, bool FetchData) 
		{
			DBMessage message = null;
			bool RequiresArchive = m_Queue.RequiresArchive;
			int ReturnCode = -100; 

			using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare)) 
			{
				connection.Open();
                using (var command = connection.CreateCommand())
				{
                    command.CommandText = "QReceiveById";
                    command.CommandType = CommandType.StoredProcedure; 
					command.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id));
					command.Parameters.Add(new SqlParameter("@MessageId", Id));
					command.Parameters.Add(new SqlParameter("RETURN_VALUE", SqlDbType.Int));
					command.Parameters.Add(new SqlParameter("@Archive", RequiresArchive ));

					command.Parameters[2].Direction = ParameterDirection.ReturnValue;
					
					DataSet ds = new DataSet();
					try 
					{
						using (var sda = DbAccessUtils.GetDbProvider().CreateDataAdapter()) 
						{
                            sda.SelectCommand = command;
							sda.Fill(ds);
						}
					
					} 
					catch( Exception e) 
					{
						ReturnCode = (int)command.Parameters[2].Value;
					
						switch ( ReturnCode ) 
						{
							case -1: 
								throw new DBMessageQueueException(e.Message, DBMessageQueueErrorCode.UnexpectedError );
							
							case -2:
								throw new DBMessageQueueException(e.Message, DBMessageQueueErrorCode.EmptyQueue); 
							
							case -3: 
								throw new DBMessageQueueException(e.Message, DBMessageQueueErrorCode.MessageProcessed);
							case -100: 
								throw new DBMessageQueueException(e.Message, DBMessageQueueErrorCode.MessageProcessed);
							
						}
					}

					try 
					{
						if ( ds.Tables.Count >  0) 
						{
							DataRow reader = ds.Tables[0].Rows[0]; 
							message = new DBMessage(Id,
								reader[ConstMsg.SCHEMA_SUBJECT1].ToString(), 
								reader[ConstMsg.SCHEMA_DATA].ToString(),
								reader[ConstMsg.SCHEMA_SUBJECT2].ToString(),
								Convert.ToInt16(reader[ConstMsg.SCHEMA_PRIORITY]),
								reader[ConstMsg.SCHEMA_DATALOC].ToString(), 
								RequiresArchive, FetchData, (DateTime) reader[ConstMsg.SCHEMA_INSERTIONTIME]);
						}
						else 
							throw new DBMessageQueueException("No data returned from SP QReceiveById Q:" + m_Queue.Name, DBMessageQueueErrorCode.UnexpectedError);
					}
					catch( Exception e) 
					{
						if ( e is DBMessageQueueException &&  ((DBMessageQueueException) e ).MessageQueueErrorCode  != DBMessageQueueErrorCode.MessageProcessed ) 
						{
							DataRow reader = ds.Tables[0].Rows[0];  //we dont want to stick back a method that was already processed 
							if ( e is DBMessageQueueException &&  ((DBMessageQueueException) e ).MessageQueueErrorCode  != DBMessageQueueErrorCode.MessageProcessed ) 
								Send((string) reader[ConstMsg.SCHEMA_SUBJECT1],(string) reader[ConstMsg.SCHEMA_DATA],(string) reader[ConstMsg.SCHEMA_SUBJECT2],Convert.ToInt16( ConstMsg.SCHEMA_PRIORITY));
							Tools.LogError(e);
							throw e;
						}
					}
				}

				
			}

			return message;

		}
		#endregion

		#region private secured methods
		/// <summary>
		/// Retrieves all the Messages from the queue at the point of the call. This waits until any other instances of 
		/// the message queue using the same queue finish accessing it.  
		/// </summary>
		/// <param name="TakeLock"> A delegate to a function that takes the lock.</param>
		/// <param name="ReleaseLock"> A delegate to a function that releases the lock.</param>
		/// <returns>An arraylist of DBMessage with all the messages in the queue at a specific time.</returns>
		private DBMessage[] SecureGetMessages(Lock TakeLock, Unlock ReleaseLock)
		{
			TakeLock();
			DBMessage[] messages;
			try 
			{
				messages = UnsecureGetMessages(true);
			}
			catch( Exception e )
			{
				ReleaseLock();
				Tools.LogError(e.Message);
				throw; 
			}

			ReleaseLock();
			return messages;
		}	

		/// <summary>
		/// Adds locking to the Receive calls. This method takes two delegates representing the methods that 
		/// are needed to call when locking. 
		/// </summary>
		/// <param name="TakeLock">A function pointer to the locking method.</param>
		/// <param name="ReleaseLock"> A function pointer to the unlocking method.</param>
		/// <returns>The message with the specified id or the first message.</returns>
		private DBMessage SecureReceive(Lock TakeLock, Unlock ReleaseLock) 
		{
			TakeLock();
			DBMessage message = null;
			try 
			{
				message = UnsecureReceive(true);
				
			}
			catch( DBMessageQueueException  ) 
			{
				ReleaseLock(); 
				throw;
			}
			catch( Exception e )
			{

				Tools.LogError(e);
				ReleaseLock();		
				throw;
			}


			ReleaseLock();
			return message;

		}

        private DBMessage[] SecurePeekN(Lock TakeLock, Unlock ReleaseLock, short priority, int n)
        {
            DBMessage[] messages;
            try
            {
                TakeLock();
                messages = UnsecurePeekAllN(priority, n);
            }
            finally
            {
                ReleaseLock();
            }

            return messages;
        }

        private void SecureDequeueRange(Lock takeLock, Unlock releaseLock, long start, long end, short priority)
        {
            try
            {
                takeLock();
                DeleteRangeImpl(start, end, priority);
            }
            finally
            {
                releaseLock();
            }
        }

        private DBMessage[] SecurePeekBySubject(Lock TakeLock, Unlock ReleaseLock, short priority)
        {
            DBMessage[] messages;
            try
            {
                TakeLock();
                messages = UnsecurePeekAllBySubject(priority);
            }
            finally
            {
                ReleaseLock();
            }

            return messages;
        }

        private void SecureDequeueBySubject( Lock takeLock, Unlock releaseLock, string subject1, long start, long end, short priority)
        {
            try 
            {
                takeLock();
                DeleteRangeWithSubjectImpl(subject1, start, end, priority);
            }
            finally
            {
             releaseLock();
            }
        }


		/// <summary>
		/// Adds locks to receive in order to allow synchronization.
		/// </summary>
		/// <param name="TakeLock">A delegate pointing to a function that takes the lock.</param>
		/// <param name="ReleaseLock"> A delegate pointing to the a function that releases the lock</param>
		/// <returns> An array of the messages in the database.</returns>
		private DBMessage[] SecureReceivedAll(Lock TakeLock, Unlock ReleaseLock) 
		{
			TakeLock();
			DBMessage[] messages;
			try 
			{	
				messages = UnsecureReceiveAll();
			} 
			catch ( Exception e)
			{
				ReleaseLock();
				Tools.LogError( "Error Receiving All",e);
				throw; 
			}

			ReleaseLock();
			return messages;

		}

		private DBMessage SecureReceiveById(Lock TakeLock, Unlock ReleaseLock, long id) 
		{
			TakeLock();
			DBMessage message = null;
			try 
			{
				message = UnsecureReceiveById(id, true);
				
			}
			catch( Exception e )
			{
				Tools.LogError(e);
				ReleaseLock();		
				throw;
			}

			ReleaseLock();
			return message;

		}
		#endregion 

		#region archive methods 
		/// <summary>
		/// Clears the archive belonging to this instance queue.
		/// </summary>
		public void ClearArchive() 
		{

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare))
			{
				connection.Open();
				using (var command = connection.CreateCommand()) 
				{
                    command.CommandText = "QDropArchiveMessage";
                    command.CommandType = CommandType.StoredProcedure; 
					command.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id ));

					using ( DbDataReader reader = command.ExecuteReader() ) 
					{
						while ( reader.Read() ) 
						{
							DBMessage.DeleteData(reader[ConstMsg.SCHEMA_DATALOC].ToString(),reader[ConstMsg.SCHEMA_DATA].ToString() );
						}
					}
				}
			}
		}

		/// <summary>
		/// Moves the message with the given id from the archive table to the message queue table.
		/// </summary>
		/// <param name="MessageId">The id of the message to be copied.</param>
		public void MoveArchiveMsgToQueue(long MessageId) 
		{
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare))
			{
				connection.Open();
				using (var command = connection.CreateCommand()) 
				{
                    command.CommandText = "QMoveToMessage";
                    command.CommandType = CommandType.StoredProcedure; 
					command.Parameters.Add(new SqlParameter("@MessageId", MessageId));
					command.ExecuteNonQuery();
				}
			}
		}

		/// <summary>
		/// Deletes the given message id from the archive.
		/// </summary>
		/// <param name="MessageId">The id of the message to delete</param>
		public void DeleteFromArchive(long MessageId) 
		{
			using( var connection =  DbAccessUtils.GetConnection(DataSrc.LOShare))
			{
				connection.Open();
				using (var command = connection.CreateCommand()) 
				{
                    command.CommandText = "QDropArchiveMessage";
                    command.CommandType = CommandType.StoredProcedure; 
					command.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id ));
					command.Parameters.Add(new SqlParameter("@MessageId", MessageId));

					DataSet ds = new DataSet();
					using (var sda = DbAccessUtils.GetDbProvider().CreateDataAdapter()) 
					{
                        sda.SelectCommand = command;
						sda.Fill(ds); 
						if ( ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 ) 
						{
							DataRow row = ds.Tables[0].Rows[0];
							DBMessage.DeleteData(row[ConstMsg.SCHEMA_DATALOC].ToString(), row[ConstMsg.SCHEMA_DATA].ToString());
						}
					}
				}
			}
		}

		/// <summary>
		/// Gets the archived messages in the queue.
		/// </summary>
		/// <param name="size"> The number of archive messages that should be retrieved.</param>
		/// <returns>A dataset that matches the table of messages belonging to this queue. </returns>
		public DataSet GetArchive(int Size) 
		{
			
			DataSet ds = new DataSet();
            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare)) 
			{
				connection.Open();
				using (var command = connection.CreateCommand()) 
				{
					command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "QGetArchive";
                    command.Parameters.Add(new SqlParameter("@QueueId", m_Queue.Id ));
					command.Parameters.Add(new SqlParameter("@Size", Size ));
					
					using (var adapter = DbAccessUtils.GetDbProvider().CreateDataAdapter()) 
					{
                        adapter.SelectCommand = command;
						adapter.Fill(ds);
					}
				}
			}

			return ds;
		}

		#endregion 

        #region static

        public static List<DBMessageQueueStatistic> GetQueueCounts()
        {
            var stats = new List<DBMessageQueueStatistic>();
            string environment = "secure";
            DateTime timeretrieved = DateTime.Now;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "Q_STATUS"))
            {
                while (reader.Read())
                {
                    int id = (int)reader["QueueId"];
                    string name = (string)reader["QueueName"];
                    int count = 0;
                    int oldestMessageAgeInSeconds = 0;

                    if (reader["OldestMessageInsertionTime"] != DBNull.Value)
                    {
                        count = (int)reader["cnt"];
                        oldestMessageAgeInSeconds = Convert.ToInt32(DateTime.Now.Subtract((DateTime)reader["OldestMessageInsertionTime"]).TotalSeconds);
                    }

                    var stat = new DBMessageQueueStatistic(id, name, count, oldestMessageAgeInSeconds, environment, timeretrieved);
                    stats.Add(stat); ;
                }
            }
            return stats;
        }

        #endregion
    }
}
;