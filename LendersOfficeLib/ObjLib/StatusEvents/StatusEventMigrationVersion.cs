﻿namespace LendersOffice.ObjLib.StatusEvents
{
    /// <summary>
    /// Status event migration version enum.
    /// </summary>
    public enum StatusEventMigrationVersion
    {
        NotMigrated = 0,  
        InitialMigration = 1,
        MigrationWithUserName = 2
    }
}
