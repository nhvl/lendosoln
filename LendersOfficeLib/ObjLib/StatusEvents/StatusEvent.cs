﻿namespace LendersOffice.ObjLib.StatusEvents
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.Linq;
    using System.Xml.Linq;
    using DataAccess;
    using DataAccess.Utilities;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Security;

    /// <summary>
    /// A utility class for managing Status Events.
    /// </summary>
    public class StatusEvent
    {     
        /// <summary>
        /// Gets a map between Date Ids and their corresponding statuses.
        /// </summary>
        private static Dictionary<string, E_sStatusT> dateIdToStatus = new Dictionary<string, E_sStatusT>()
        {
                { "sOpenedD", E_sStatusT.Loan_Open },
                { "sPreQualD", E_sStatusT.Loan_Prequal },
                { "sPreApprovD", E_sStatusT.Loan_Preapproval },
                { "sSubmitD", E_sStatusT.Loan_Registered },
                { "sApprovD", E_sStatusT.Loan_Approved },
                { "sDocsD", E_sStatusT.Loan_Docs },
                { "sFundD", E_sStatusT.Loan_Funded },
                { "sOnHoldD", E_sStatusT.Loan_OnHold },
                { "sSuspendedD", E_sStatusT.Loan_Suspended },
                { "sCanceledD", E_sStatusT.Loan_Canceled },
                { "sRejectD", E_sStatusT.Loan_Rejected },
                { "sClosedD", E_sStatusT.Loan_Closed },
                { "sLeadD", E_sStatusT.Lead_New },
                { "sLoanSubmittedD", E_sStatusT.Loan_LoanSubmitted },
                { "sUnderwritingD", E_sStatusT.Loan_Underwriting },
                ////{ "sOpenedD", E_sStatusT.Loan_WebConsumer },
                { "sLeadCanceledD", E_sStatusT.Lead_Canceled },
                { "sLeadDeclinedD", E_sStatusT.Lead_Declined },
                { "sLeadOtherD", E_sStatusT.Lead_Other },
                ////{ "sOpenedD", E_sStatusT.Loan_Other },
                { "sRecordedD", E_sStatusT.Loan_Recorded },
                { "sShippedToInvestorD", E_sStatusT.Loan_Shipped },
                { "sClearToCloseD", E_sStatusT.Loan_ClearToClose },
                { "sProcessingD", E_sStatusT.Loan_Processing },
                { "sFinalUnderwritingD", E_sStatusT.Loan_FinalUnderwriting },
                { "sDocsBackD", E_sStatusT.Loan_DocsBack },
                { "sFundingConditionsD", E_sStatusT.Loan_FundingConditions },
                { "sFinalDocsD", E_sStatusT.Loan_FinalDocs },
                { "sLPurchaseD", E_sStatusT.Loan_LoanPurchased },
                { "sPreProcessingD", E_sStatusT.Loan_PreProcessing },
                { "sDocumentCheckD", E_sStatusT.Loan_DocumentCheck },
                { "sDocumentCheckFailedD", E_sStatusT.Loan_DocumentCheckFailed },
                { "sPreUnderwritingD", E_sStatusT.Loan_PreUnderwriting },
                { "sConditionReviewD", E_sStatusT.Loan_ConditionReview },
                { "sPreDocQCD", E_sStatusT.Loan_PreDocQC },
                { "sDocsOrderedD", E_sStatusT.Loan_DocsOrdered },
                { "sDocsDrawnD", E_sStatusT.Loan_DocsDrawn },
                { "sSuspendedByInvestorD", E_sStatusT.Loan_InvestorConditions },
                { "sCondSentToInvestorD", E_sStatusT.Loan_InvestorConditionsSent },
                { "sReadyForSaleD", E_sStatusT.Loan_ReadyForSale },
                { "sSubmittedForPurchaseReviewD", E_sStatusT.Loan_SubmittedForPurchaseReview },
                { "sInPurchaseReviewD", E_sStatusT.Loan_InPurchaseReview },
                { "sPrePurchaseConditionsD", E_sStatusT.Loan_PrePurchaseConditions },
                { "sSubmittedForFinalPurchaseD", E_sStatusT.Loan_SubmittedForFinalPurchaseReview },
                { "sInFinalPurchaseReviewD", E_sStatusT.Loan_InFinalPurchaseReview },
                { "sClearToPurchaseD", E_sStatusT.Loan_ClearToPurchase },
                { "sPurchasedD", E_sStatusT.Loan_Purchased },
                { "sCounterOfferD", E_sStatusT.Loan_CounterOffer },
                { "sWithdrawnD", E_sStatusT.Loan_Withdrawn },
                { "sArchivedD", E_sStatusT.Loan_Archived }
        };

        /// <summary>
        /// Gets the list of field ids for status dates.
        /// </summary>
        private static List<string> DateFieldIds
        {
            get
            {
                return new List<string>()
                {
                     "sLPurchaseD",
                     "sProcessingD",
                     "sFinalUnderwritingD",
                     "sDocsBackD",
                     "sOpenedD",
                     "sFundingConditionsD",
                     "sFinalDocsD",
                     "sPreQualD",
                     "sPreApprovD",
                     "sSubmitD",
                     "sApprovD",
                     "sDocsD",
                     "sFundD",
                     "sOnHoldD",
                     "sSuspendedD",
                     "sRejectD",
                     "sClosedD",
                     "sLeadD",
                    "sLoanSubmittedD",
                     "sUnderwritingD",
                     "sCanceledD",
                     "sRecordedD",
                     "sShippedToInvestorD",
                     "sClearToCloseD",
                     "sPreProcessingD",
                    "sDocumentCheckD",
                     "sDocumentCheckFailedD",
                     "sPreUnderwritingD",
                     "sConditionReviewD",
                     "sPreDocQCD",
                     "sDocsOrderedD",
                     "sDocsDrawnD",
                     "sReadyForSaleD",
                    "sSubmittedForPurchaseReviewD",
                     "sInPurchaseReviewD",
                     "sPrePurchaseConditionsD",
                     "sSubmittedForFinalPurchaseD",
                     "sInFinalPurchaseReviewD",
                    "sClearToPurchaseD",
                     "sPurchasedD",
                     "sCounterOfferD",
                     "sWithdrawnD",
                     "sArchivedD",
                     "sSuspendedByInvestorD",
                     "sCondSentToInvestorD",
                     "sLeadDeclinedD",
                     "sLeadCanceledD",
                     "sLeadOtherD"
                };
            }
        }
   
        /// <summary>
        /// Retrieves all status events belonging to a loan.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <returns>A list of StatusEventItems for the loan.</returns>
        public static List<StatusEventItem> RetrieveAllLoanEvents(Guid loanId, Guid brokerId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId)
            };

            List<StatusEventItem> items = new List<StatusEventItem>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "STATUS_EVENT_GET", parameters))
            {
                while (reader.Read())
                {
                    items.Add(new StatusEventItem()
                    {
                        Id = (int)reader["Id"],
                        LoanId = (Guid)reader["LoanId"],
                        StatusT = (E_sStatusT)reader["StatusT"],
                        StatusD = CDateTime.Create((DateTime)reader["StatusD"]),
                        Timestamp = CDateTime.Create((DateTime)reader["Timestamp"]),
                        IsExclude = (bool)reader["IsExclude"],
                        UserName = Convert.IsDBNull(reader["UserName"]) ? "Unknown" : reader["UserName"].ToString()
                    });
                }
            }

            return items;
        }

        /// <summary>
        /// Removes the Status Event records for the given loan.
        /// </summary>
        /// <param name="loanId">The loan's id.</param>
        /// <param name="brokerId">The loan's broker's id.</param>
        public static void RemoveAllStatusEvents(Guid loanId, Guid brokerId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@BrokerId", brokerId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "STATUS_EVENT_RemoveAll", 3, parameters);
        }

        /// <summary>
        /// This method returns the number of touches that occured for a status for the given list of status events.
        /// </summary>
        /// <param name="eventItems">The list of status events.</param>
        /// <param name="status">The status to count the number of touches for.</param>
        /// <returns>Returns the number of touches that have occured for an event that are not excluded.</returns>
        public static int RetrieveNumTouches(List<StatusEventItem> eventItems, E_sStatusT status)
        {
            return eventItems.Where(eventItem => eventItem.StatusT == status && !eventItem.IsExclude).Count();
        }

        /// <summary>
        /// Adds a Status Event to the table for the associated loan.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="statusT">The status type.</param>
        /// <param name="statusD">The status date representation.</param>
        /// <param name="userName">The User name.</param>
        /// <param name="timeStamp">The date the event was created.</param>
        public static void AddStatusEvent(Guid loanId, Guid brokerId, E_sStatusT statusT, string statusD, string userName, DateTime? timeStamp = null)
        {
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@IsExclude", false),
                new SqlParameter("@Timestamp", timeStamp.HasValue ? timeStamp.Value.ToString("yyyy-MM-dd HH:mm:ss") : DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")),
                new SqlParameter("@StatusD", statusD),
                new SqlParameter("@StatusT", statusT),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@UserName", userName)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "STATUS_EVENT_ADD", 3, parameters);
        }

        /// <summary>
        /// Converts a status event into an XElement.
        /// </summary>
        /// <param name="eventItem">The event that will be converted.</param>
        /// <returns>An Xelement with the relevant Xml data.</returns>
        public static XElement ConvertStatusEventToXElement(StatusEventItem eventItem)
        {
            var statusTNode = new XElement("StatusT", (int)eventItem.StatusT);
            var statusDNode = new XElement("StatusD", eventItem.StatusD);
            var timestampNode = new XElement("Timestamp", eventItem.Timestamp.ToStringWithTime("yyyy-MM-dd HH:mm:ss"));
            var isExcludeNode = new XElement("IsExclude", eventItem.IsExclude);
            var userNameNode = new XElement("UserName", eventItem.UserName);
            var xml = new XElement("statusEvent", statusTNode, statusDNode, timestampNode, isExcludeNode, userNameNode);
            return xml;
        }

        /// <summary>
        /// Adds a Status Event to the table for the associated loan.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="statusEventItems">The list of status event items to add.</param>
        public static void AddStatusEventMultiple(Guid loanId, Guid brokerId, List<StatusEventItem> statusEventItems)
        {
            if (statusEventItems.Count > 0)
            {
                var rootXml = new XElement("root");
                foreach (var xmlNode in statusEventItems.Select(eventItem => StatusEvent.ConvertStatusEventToXElement(eventItem)))
                {
                    rootXml.Add(xmlNode);
                }

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@LoanId", loanId),
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@newEventsXml", rootXml.ToString(SaveOptions.DisableFormatting))
                };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "STATUS_EVENT_AddMultiple", 3, parameters);
            }
        }

        /// <summary>
        /// Duplicates the status events from the source loan and associates them with the destination loan.
        /// </summary>
        /// <param name="sourceId">The id of the source loan.</param>
        /// <param name="destinationId">The id of the destination loan.</param>
        /// <param name="brokerId">The broker id.</param>
        public static void DuplicateStatusEvents(Guid sourceId, Guid destinationId, Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@sourceId", sourceId),
                new SqlParameter("@destinationId", destinationId),
                new SqlParameter("@brokerId", brokerId)
            };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "STATUS_EVENT_Duplicate", 3, parameters);
        }

        /// <summary>
        /// Updates the the IsExclude field for the given ids.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="dirtyIds">The list of ids for events that will have their IsExclude value toggled.</param>
        public static void UpdateExclude(Guid loanId, Guid brokerId, List<int> dirtyIds)
        {
            var xmlValues = dirtyIds.Select(id => new XElement("id", id));
            SqlParameter[] parameters =
            {
                new SqlParameter("@loanId", loanId),
                new SqlParameter("@brokerId", brokerId),
                new SqlParameter("@dirtyIdXml", new XElement("root", xmlValues).ToString(SaveOptions.DisableFormatting))
            };

            var numUpdated = StoredProcedureHelper.ExecuteNonQuery(brokerId, "STATUS_EVENT_ToggleExclude", 3, parameters);
            List<string> cacheFields = new List<string> { "sNumTouchesPreProcessing", "sNumTouchesProcessing", "sNumTouchesSubmitted", "sNumTouchesConditionReview" };
            Tools.UpdateCacheTable(loanId, cacheFields, false);
        }

        /// <summary>
        /// Update the loan to have user names in their status events.
        /// </summary>
        /// <param name="loanId">The loan id.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="statusEventItems">Status event items to update.</param>
        public static void UpdateUserNameMultiple(Guid loanId, Guid brokerId, List<StatusEventItem> statusEventItems)
        {
            if (statusEventItems.Count > 0)
            {
                var statusEventUserNameUpdateTableTypeDictionary = new Dictionary<string, Type>()
                {
                    { "Id", typeof(int) },
                    { "UserName", typeof(string) }
                };

                var statusEventUserNameUpdateTableType = DbAccessUtils.InitializeTableValueDataTable("StatusEventUserNameUpdateTableType", statusEventUserNameUpdateTableTypeDictionary);
                foreach (StatusEventItem item in statusEventItems)
                {
                    statusEventUserNameUpdateTableType.Rows.Add(item.Id, item.UserName);
                }

                List<SqlParameter> parameters = new List<SqlParameter>()
                {
                    new SqlParameter("@LoanId", loanId),
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@StatusEventUserNames", statusEventUserNameUpdateTableType)
                };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "STATUS_EVENT_UpdateUserNameMultiple", 3, parameters);
            }
        }

        /// <summary>
        /// This method parses a loan's Audit History, and creates StatusEventItems from it.
        /// </summary>
        /// <param name="loanId">The loan's id.</param>
        /// <returns>Returns a bool determining whether the migration was successful or not.</returns>
        public static bool MigrateStatusEvents(Guid loanId)
        {
            CPageData dataLoan = CPageData.CreateUsingSmartDependencyWithSecurityBypass(loanId, typeof(StatusEvent));
            dataLoan.InitSave(Constants.ConstAppDavid.SkipVersionCheck);

            if (dataLoan.sStatusEventMigrationVersion == StatusEventMigrationVersion.MigrationWithUserName)
            {
                return false;
            }
            else
            {
                var auditList = AuditManager.RetrieveAuditList(loanId)
                    .Where(auditItem => auditItem.CategoryT == E_AuditItemCategoryT.FieldChange || auditItem.CategoryT == E_AuditItemCategoryT.LoanStatus)
                    .OrderBy(auditItem => auditItem, new AuditItemReorderComparer());

                List<StatusEventItem> newEventItems = new List<StatusEventItem>();

                var statusOrder = CPageBase.s_sStatusT_Order;
                E_sStatusT? lastStatus = statusOrder[0];
                    
                Dictionary<E_sStatusT, string> statusDateMap = new Dictionary<E_sStatusT, string>();

                foreach (LightWeightAuditItem auditItem in auditList)
                {
                    if (auditItem.CategoryT == E_AuditItemCategoryT.LoanStatus && auditItem.Description.Contains("to \""))
                    {
                        E_sStatusT? newStatus = LoanStatus.GetStatusFromDescription(auditItem.NewValue);
                        string statusDateRep = string.Empty;

                        if (newStatus.HasValue)
                        {
                            statusDateMap.TryGetValue(newStatus.Value, out statusDateRep);

                            if (string.IsNullOrEmpty(statusDateRep))
                            {
                                if (newStatus.Value == E_sStatusT.Lead_New || dataLoan.sStatusEventMigrationVersion == StatusEventMigrationVersion.InitialMigration || dataLoan.sIsStatusEventMigrated)
                                {
                                    // Lead dates are not tracked because the loan only tracks fields if the loan is past
                                    // the loan_open status.
                                    statusDateRep = auditItem.Timestamp.ToString();
                                    statusDateMap.Add(newStatus.Value, statusDateRep);
                                }
                                else
                                {
                                    throw new CBaseException("Cannot migrate the loan due to missing data.", "The loan statuses was changed with an empty value in the corresponding loan status: " + LoanStatus.GetStatusDescription(newStatus.Value));
                                }
                            }

                            StatusEventItem newItem = new StatusEventItem()
                            {
                                LoanId = loanId,
                                BrokerId = dataLoan.sBrokerId,
                                IsExclude = false,
                                Timestamp = CDateTime.Create(auditItem.Timestamp),
                                StatusT = newStatus.Value,
                                StatusD = CDateTime.Create(statusDateRep, dataLoan.m_convertLos),
                                UserName = auditItem.UserName
                            };
                            newEventItems.Add(newItem);

                            lastStatus = newStatus;
                        }
                    }
                    else if (string.Equals(auditItem.Description, "Loan Creation") || string.Equals(auditItem.Description, "Lead Creation"))
                    {
                        var loanStatus = auditItem.Description == "Lead Creation" ? E_sStatusT.Lead_New : E_sStatusT.Loan_Open;

                        if (statusDateMap.ContainsKey(loanStatus))
                        {
                            statusDateMap[loanStatus] = auditItem.Timestamp.ToString();
                        }
                        else
                        {
                            statusDateMap.Add(loanStatus, auditItem.Timestamp.ToString());
                        }

                        if (loanStatus == E_sStatusT.Lead_New && !statusDateMap.ContainsKey(E_sStatusT.Loan_Open))
                        {
                            // The audit history will not update the loan_open date if the lead of changed to a loan
                            // within the same day, so we need to set this now just in case.
                            statusDateMap.Add(E_sStatusT.Loan_Open, auditItem.Timestamp.ToString());
                        }

                        StatusEventItem newItem = new StatusEventItem()
                        {
                            LoanId = loanId,
                            BrokerId = dataLoan.sBrokerId,
                            IsExclude = false,
                            Timestamp = CDateTime.Create(auditItem.Timestamp),
                            StatusT = loanStatus,
                            StatusD = CDateTime.Create(auditItem.Timestamp),
                            UserName = auditItem.UserName
                        };

                        newEventItems.Add(newItem);
                    }
                    else if (!string.IsNullOrEmpty(auditItem.FieldId) && dateIdToStatus.Keys.Contains(auditItem.FieldId))
                    {
                        E_sStatusT newStatus;

                        if (dateIdToStatus.TryGetValue(auditItem.FieldId, out newStatus))
                        {
                            if (statusDateMap.ContainsKey(newStatus))
                            {
                                statusDateMap[newStatus] = auditItem.NewValue;
                            }
                            else
                            {
                                statusDateMap.Add(newStatus, auditItem.NewValue);
                            }
                        }
                    }
                }

                if (dataLoan.sStatusEventMigrationVersion == StatusEventMigrationVersion.InitialMigration || dataLoan.sIsStatusEventMigrated)
                {
                    var existingEventItems = RetrieveAllLoanEvents(loanId, dataLoan.sBrokerId).OrderBy(i => i.Timestamp.DateTimeForComputationWithTime).ToList();
                    newEventItems = newEventItems.OrderBy(i => i.Timestamp.DateTimeForComputationWithTime).ToList();

                    if (existingEventItems.Count != newEventItems.Count)
                    {
                        var userMsg = "This loan cannot be migrated due to mismatch betwwen the current status events and current audit history. Please contact LendingQB support if you need to migrate this loan.";
                        var devMsg = "There's a mismatch between status events and the audit history while trying to migrate from status events without user names to the version with the user names.";
                        throw new CBaseException(userMsg, devMsg);
                    }

                    for (int i = 0; i < existingEventItems.Count; i++)
                    {
                        existingEventItems[i].UserName = newEventItems[i].UserName;
                    }

                    UpdateUserNameMultiple(loanId, dataLoan.sBrokerId, existingEventItems);
                }
                else 
                {
                    AddStatusEventMultiple(loanId, dataLoan.sBrokerId, newEventItems);
                }

                dataLoan.sStatusEventMigrationVersion = StatusEventMigrationVersion.MigrationWithUserName;
                dataLoan.Save();
                return true;
            }
        }

        /// <summary>
        /// Compares two audit items.  This tries to adjust for the case where loan status event items get timestamped
        /// before their corresponding date change event items.
        /// </summary>
        public class AuditItemReorderComparer : IComparer<LightWeightAuditItem>
        {
            /// <summary>
            /// Compares two audit items.  If they are within 5000000 ticks of each other, and they're different audit types,
            /// move the status event audit to the end.
            /// </summary>
            /// <param name="auditItemA">The first audit item.</param>
            /// <param name="auditItemB">The second item.</param>
            /// <returns>An integer representing the order.</returns>
            public int Compare(LightWeightAuditItem auditItemA, LightWeightAuditItem auditItemB)
            {
                var tickDifference = auditItemA.Timestamp.Ticks - auditItemB.Timestamp.Ticks;

                if (Math.Abs(tickDifference) < 5000000)
                {
                    if (auditItemA.CategoryT < auditItemB.CategoryT)
                    {
                        return -1;
                    }
                    else if (auditItemA.CategoryT > auditItemB.CategoryT)
                    {
                        return 1;
                    }
                }

                if (tickDifference > 0)
                {
                    return 1;
                }
                else if (tickDifference < 0)
                {
                    return -1;
                }

                return 0;
            }
        }
    }
}
