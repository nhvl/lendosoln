﻿namespace LendersOffice.ObjLib.StatusEvents
{
    using System;
    using DataAccess;

    /// <summary>
    /// This class is a container for the StatusEvent information retrieved from the database.
    /// </summary>
    public class StatusEventItem
    {
        /// <summary>
        /// Gets or sets the event's id.
        /// </summary>
        /// <value>A Guid value.</value>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the corresponding loan id for the event.
        /// </summary>
        /// <value>The loan's id.</value>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the corresponding broker id for the event.
        /// </summary>
        /// <value>The broker's id.</value>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the status the loan was reverted to for this event.
        /// </summary>
        /// <value>The loan status enum.</value>
        public E_sStatusT StatusT { get; set; }

        /// <summary>
        /// Gets the description of the event's status.
        /// </summary>
        /// <value>A string represenation of the event status.</value>
        public string StatusDescription
        {
            get
            {
                return DataAccess.Utilities.LoanStatus.GetStatusDescription(this.StatusT);
            }
        }

        /// <summary>
        /// Gets or sets the status' coreresponding date.
        /// </summary>
        /// <value>A CDateTime value.</value>
        public CDateTime StatusD { get; set; }

        /// <summary>
        /// Gets the string representation of the StatusD.
        /// </summary>
        /// <value>A string represenation.</value>
        public string StatusD_rep
        {
            get
            {
                return this.StatusD.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the time the status was reverted.
        /// </summary>
        /// <value>A CDateTime value.</value>
        public CDateTime Timestamp { get; set; }

        /// <summary>
        /// Gets the string representation of the Timestamp.
        /// </summary>
        /// <value>A string represenation.</value>
        public string Timestamp_rep
        {
            get
            {
                return this.Timestamp.DateTimeForComputationWithTime.ToString("g") + " PDT";
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the event should be excluded from the number
        /// of times an event has been reverted to.
        /// </summary>
        /// <value>A boolean value.</value>
        public bool IsExclude { get; set; }

        /// <summary>
        /// Gets or sets the name of the user that created the status event.
        /// </summary>
        public string UserName { get; set; }
    }
}
