﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using DataAccess;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Text;
using System.IO;

namespace LendersOffice.PmlSnapshot
{
    public abstract class AbstractPmlSnapshot : IXmlSerializable
    {
        protected abstract string TagName { get; }
        public string Id
        {
            get { return GetString("Id"); }
            set { SetString("Id", value); }
        }

        public virtual string FriendlyUniqueDescription
        {
            get { return Id; }
        }
        private Dictionary<string, string> m_hash = new Dictionary<string, string>();

        protected void SetString(string fieldId, string value)
        {
            m_hash[fieldId] = value;
        }
        protected string GetString(string fieldId)
        {
            string value;
            if (!m_hash.TryGetValue(fieldId, out value))
            {
                value = string.Empty;
            }
            return value;
        }

        protected void SetBool(string fieldId, bool value)
        {
            m_hash[fieldId] = value ? "Yes" : "No";
        }
        protected bool GetBool(string fieldId)
        {
            string value;
            if (!m_hash.TryGetValue(fieldId, out value))
            {
                value = "No";
            }
            return value == "Yes";
        }

        protected void SetEnum(string fieldId, Enum value)
        {
            m_hash[fieldId] = value.ToString("D");
        }
        protected int GetEnum(string fieldId)
        {
            string value = GetString(fieldId);

            int ret;
            if (!int.TryParse(value, out ret))
            {
                ret = 0;
            }
            return ret;
        }

        protected void SetGuid(string fieldId, Guid value)
        {
            SetString(fieldId, value.ToString());
        }
        protected Guid GetGuid(string fieldId)
        {
            string v = GetString(fieldId);
            try
            {
                return new Guid(v);
            }
            catch
            {
                return Guid.Empty;
            }
        }

        protected void SetDateTime(string fieldId, DateTime dt)
        {
            m_hash[fieldId] = dt.Ticks.ToString();
        }
        protected DateTime GetDateTime(string fieldId)
        {

            string value = GetString(fieldId);

            long ticks;
            if (!long.TryParse(value, out ticks))
            {
                return DateTime.Now;
            }
            return new DateTime(ticks);
        }
        protected virtual bool IsSkipCompare(string fieldId)
        {
            return false;
        }
        protected virtual void OnFieldModified(string fieldId)
        {
            return;
        }
        public virtual void Compare(List<FieldTrackInfo> resultList, AbstractPmlSnapshot o)
        {
            Dictionary<string, bool> visited = new Dictionary<string, bool>();

            foreach (var current in m_hash)
            {
                if (IsSkipCompare(current.Key))
                {
                    visited.Add(current.Key, true);
                    continue;
                }
                string compareValue;
                if (o.m_hash.TryGetValue(current.Key, out compareValue))
                {
                    
                    if (!current.Value.Equals(compareValue))
                    {
                        resultList.Add(new FieldTrackInfo(current.Key, current.Value, compareValue, E_FieldTrackInfoTypeT.Normal, 0));
                        OnFieldModified(current.Key);
                    }
                }
                else
                {
                    resultList.Add(new FieldTrackInfo(current.Key, current.Value, string.Empty, E_FieldTrackInfoTypeT.Obsolete, 0));
                    OnFieldModified(current.Key);

                }
                visited.Add(current.Key, true);
            }
            foreach (var current in o.m_hash)
            {
                if (visited.ContainsKey(current.Key) || IsSkipCompare(current.Key))
                {
                    continue;
                }
                
                resultList.Add(new FieldTrackInfo(current.Key, string.Empty, current.Value, E_FieldTrackInfoTypeT.New, 0));
                OnFieldModified(current.Key);

            }
        }

        protected virtual void WriteAdditionalXml(XmlWriter writer)
        {

        }

        protected virtual void ReadAdditionalXml(XmlReader reader)
        {
        }

        public string GenerateXml()
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;

            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                WriteXml(writer);
            }
            return sb.ToString();
        }

        public void LoadFromXml(string xml)
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ConformanceLevel = ConformanceLevel.Fragment;
            using (XmlReader reader = XmlReader.Create(new StringReader(xml), settings))
            {
                ReadXml(reader);
            }
        }
        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            throw new NotImplementedException();
        }

        public void ReadXml(XmlReader reader)
        {
            while (reader.NodeType != XmlNodeType.Element || reader.Name != TagName)
            {
                reader.Read();
                if (reader.EOF) return;
            }
            for (int i = 0; i < reader.AttributeCount; i++)
            {
                reader.MoveToAttribute(i);
                m_hash[reader.Name] = reader.Value;
            }
            reader.MoveToElement(); // Moves the reader back to the current node.
            if (reader.IsEmptyElement) return;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name == TagName) return;

                if (reader.NodeType != XmlNodeType.Element) continue;

                ReadAdditionalXml(reader);
            }

        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement(TagName);

            foreach (var o in m_hash)
            {
                writer.WriteAttributeString(o.Key, o.Value);
            }
            WriteAdditionalXml(writer);

            writer.WriteEndElement();
        }

        #endregion
    }

}
