﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;
using System.Xml;
using LendersOffice.Security;

namespace LendersOffice.PmlSnapshot
{
    public class DataLoanPmlSnapshot : AbstractPmlSnapshot
    {
        public bool Option_PmlRequireEstResidualIncome { get; set; }
        public bool Option_IsAsk3rdPartyUwResultInPml { get; set; }
        public bool Option_sIsStandAlone2ndLien { get; set; }

        protected override bool IsSkipCompare(string fieldId)
        {
            if (fieldId == "Timestamp" || fieldId == "UserName")
            {
                return true;
            }

            if (fieldId == "sProdEstimatedResidualI")
            {
                return !Option_PmlRequireEstResidualIncome;
            }
            else if (fieldId == "sProd3rdPartyUwProcessingT" || fieldId == "sProdIncludeMyCommunityProc"
                || fieldId == "sProdIncludeHomePossibleProc" || fieldId == "sProdIncludeNormalProc"
                || fieldId == "sProdIncludeFHATotalProc" || fieldId == "sProdIncludeVAProc")
            {
                return !Option_IsAsk3rdPartyUwResultInPml;
            }
            else if (fieldId == "sLpIsNegAmortOtherLien" || fieldId == "sOtherLFinMethT" || fieldId == "sProOFinPmtPe")
            {
                return !Option_sIsStandAlone2ndLien;
            }

            AbstractUserPrincipal principal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null != principal)
            {
                if (principal.IsPricingMultipleAppsSupported)
                {
                    // 5/1/2009 dd - If multiple apps support then skip these fields.
                    string[] skipFields = { 
                                              "sTransmOMonPmtPe","sProdCrManualNonRolling30MortLateCount","sProdCrManual60MortLateCount","sProdCrManual90MortLateCount"
                                              , "sProdCrManual120MortLateCount", "sProdCrManual150MortLateCount", "sProdCrManual30MortLateCount","sProdCrManualRolling60MortLateCount"
                                              , "sProdCrManualRolling90MortLateCount", "sProdCrManualForeclosureHas", "sProdCrManualForeclosureRecentFileMon", "sProdCrManualForeclosureRecentFileYr"
                                              , "sProdCrManualForeclosureRecentStatusT", "sProdCrManualForeclosureRecentSatisfiedMon", "sProdCrManualForeclosureRecentSatisfiedYr"
                                              , "sProdCrManualBk7Has", "sProdCrManualBk7RecentFileMon", "sProdCrManualBk7RecentFileYr","sProdCrManualBk7RecentStatusT"
                                              , "sProdCrManualBk7RecentSatisfiedMon", "sProdCrManualBk7RecentSatisfiedYr", "sProdCrManualBk13Has", "sProdCrManualBk13RecentFileMon"
                                              , "sProdCrManualBk13RecentFileYr", "sProdCrManualBk13RecentStatusT", "sProdCrManualBk13RecentSatisfiedMon", "sProdCrManualBk13RecentSatisfiedYr"
                                              , "sOpNegCfPe"
                                          };
                    if (skipFields.Contains(fieldId))
                    {
                        return true;
                    }
                    
                }
                else
                {
                }
            }
            return false;
        }
        public DateTime Timestamp 
        {
            get { return GetDateTime("Timestamp"); }
            set { SetDateTime("Timestamp", value); }
        }
        public string UserName 
        {
            get { return GetString("UserName"); }
            set { SetString("UserName", value); }
        }

        public DataLoanPmlSnapshot()
        {
            Timestamp = DateTime.Now;
            UserName = PrincipalFactory.CurrentPrincipal.DisplayName;
            Option_IsAsk3rdPartyUwResultInPml = false;
            Option_PmlRequireEstResidualIncome = false;
            Option_sIsStandAlone2ndLien = false;
        }

        protected override string TagName
        {
            get { return "DataLoan"; }
        }

        #region Public Get/Set Properties
        public bool sIsSelfEmployed
        {
            get { return GetBool("sIsSelfEmployed"); }
            set { SetBool("sIsSelfEmployed", value); }
        }
        public string sTransmOMonPmtPe_rep
        {
            get { return GetString("sTransmOMonPmtPe"); }
            set { SetString("sTransmOMonPmtPe", value); }
        }
        public string sOpNegCfPe_rep
        {
            get { return GetString("sOpNegCfPe"); }
            set { SetString("sOpNegCfPe", value); }
        }
        public string sSpAddr
        {
            get { return GetString("sSpAddr"); }
            set { SetString("sSpAddr", value); }
        }
        public string sSpZip
        {
            get { return GetString("sSpZip"); }
            set { SetString("sSpZip", value); }
        }
        public string sSpCity
        {
            get { return GetString("sSpCity"); }
            set { SetString("sSpCity", value); }
        }
        public string sSpStatePe
        {
            get { return GetString("sSpStatePe"); }
            set { SetString("sSpStatePe", value); }
        }
        public string sSpCounty
        {
            get { return GetString("sSpCounty"); }
            set { SetString("sSpCounty", value); }
        }
        public E_sOccT sOccTPe
        {
            get { return (E_sOccT) GetEnum("sOccTPe"); }
            set { SetEnum("sOccTPe", value); }
        }
        public string sProRealETxPe_rep
        {
            get { return GetString("sProRealETxPe"); }
            set { SetString("sProRealETxPe", value); }
        }
        public string sProOHExpPe_rep
        {
            get { return GetString("sProOHExpPe"); }
            set { SetString("sProOHExpPe", value); }
        }
        public string sOccRPe_rep
        {
            get { return GetString("sOccRPe"); }
            set { SetString("sOccRPe", value); }
        }
        public string sSpGrossRentPe_rep
        {
            get { return GetString("sSpGrossRentPe"); }
            set { SetString("sSpGrossRentPe", value); }
        }
        public E_sProdSpT sProdSpT
        {
            get { return (E_sProdSpT) GetEnum("sProdSpT"); }
            set { SetEnum("sProdSpT", value); }
        }
        public string sProdCondoStories_rep
        {
            get { return GetString("sProdCondoStories"); }
            set { SetString("sProdCondoStories", value); }
        }
        public bool sProdIsSpInRuralArea
        {
            get { return GetBool("sProdIsSpInRuralArea"); }
            set { SetBool("sProdIsSpInRuralArea", value); }
        }
        public bool sProdIsCondotel
        {
            get { return GetBool("sProdIsCondotel"); }
            set { SetBool("sProdIsCondotel", value); }
        }
        public bool sProdIsNonwarrantableProj
        {
            get { return GetBool("sProdIsNonwarrantableProj"); }
            set { SetBool("sProdIsNonwarrantableProj", value); }
        }

        public E_sLPurposeT sLPurposeTPe
        {
            get { return (E_sLPurposeT) GetEnum("sLPurposeTPe"); }
            set { SetEnum("sLPurposeTPe", value); }
        }
        public string sProdCashoutAmt_rep
        {
            get { return GetString("sProdCashoutAmt"); }
            set { SetString("sProdCashoutAmt", value); }
        }
        public bool sHas1stTimeBuyerPe
        {
            get { return GetBool("sHas1stTimeBuyerPe"); }
            set { SetBool("sHas1stTimeBuyerPe", value); }
        }
        public bool sIsIOnlyPe
        {
            get { return GetBool("sIsIOnlyPe"); }
            set { SetBool("sIsIOnlyPe", value); }
        }
        public bool sProdImpound
        {
            get { return GetBool("sProdImpound"); }
            set { SetBool("sProdImpound", value); }
        }
        public E_sProdMIOptionT sProdMIOptionT
        {
            get { return (E_sProdMIOptionT) GetEnum("sProdMIOptionT"); }
            set { SetEnum("sProdMIOptionT", value); }
        }
        public E_sProdDocT sProdDocT
        {
            get { return (E_sProdDocT) GetEnum("sProdDocT"); }
            set { SetEnum("sProdDocT", value); }
        }
        public string sPrimAppTotNonspIPe_rep
        {
            get { return GetString("sPrimAppTotNonspIPe"); }
            set { SetString("sPrimAppTotNonspIPe", value); }
        }
        public string sProdPpmtPenaltyMon_rep
        {
            get { return GetString("sProdPpmtPenaltyMon"); }
            set { SetString("sProdPpmtPenaltyMon", value); }
        }
        public string sProdRLckdDays_rep
        {
            get { return GetString("sProdRLckdDays"); }
            set { SetString("sProdRLckdDays", value); }
        }
        public string sProdAvailReserveMonths_rep
        {
            get { return GetString("sProdAvailReserveMonths"); }
            set { SetString("sProdAvailReserveMonths", value); }
        }
        public string sHouseValPe_rep
        {
            get { return GetString("sHouseValPe"); }
            set { SetString("sHouseValPe", value); }
        }
        public string sDownPmtPcPe_rep
        {
            get { return GetString("sDownPmtPcPe"); }
            set { SetString("sDownPmtPcPe", value); }
        }
        public string sEquityPe_rep
        {
            get { return GetString("sEquityPe"); }
            set { SetString("sEquityPe", value); }
        }
        public string sLtvRPe_rep
        {
            get { return GetString("sLtvRPe"); }
            set { SetString("sLtvRPe", value); }
        }
        public string sLAmtCalcPe_rep
        {
            get { return GetString("sLAmtCalcPe"); }
            set { SetString("sLAmtCalcPe", value); }
        }
        public string sCltvRPe_rep
        {
            get { return GetString("sCltvRPe"); }
            set { SetString("sCltvRPe", value); }
        }
        public string sLtvROtherFinPe_rep
        {
            get { return GetString("sLtvROtherFinPe"); }
            set { SetString("sLtvROtherFinPe", value); }
        }
        public string sProOFinBalPe_rep
        {
            get { return GetString("sProOFinBalPe"); }
            set { SetString("sProOFinBalPe", value); }
        }
        public string sProdPpmtPenaltyMon2ndLien_rep
        {
            get { return GetString("sProdPpmtPenaltyMon2ndLien"); }
            set { SetString("sProdPpmtPenaltyMon2ndLien", value); }
        }
        public E_sProd3rdPartyUwResultT sProd3rdPartyUwResultT
        {
            get { return (E_sProd3rdPartyUwResultT) GetEnum("sProd3rdPartyUwResultT"); }
            set { SetEnum("sProd3rdPartyUwResultT", value); }
        }
        public string sProdCrManualNonRolling30MortLateCount_rep
        {
            get { return GetString("sProdCrManualNonRolling30MortLateCount"); }
            set { SetString("sProdCrManualNonRolling30MortLateCount", value); }
        }
        public string sProdCrManual60MortLateCount_rep
        {
            get { return GetString("sProdCrManual60MortLateCount"); }
            set { SetString("sProdCrManual60MortLateCount", value); }
        }
        public string sProdCrManual90MortLateCount_rep
        {
            get { return GetString("sProdCrManual90MortLateCount"); }
            set { SetString("sProdCrManual90MortLateCount", value); }
        }
        public string sProdCrManual120MortLateCount_rep
        {
            get { return GetString("sProdCrManual120MortLateCount"); }
            set { SetString("sProdCrManual120MortLateCount", value); }
        }
        public string sProdCrManual150MortLateCount_rep
        {
            get { return GetString("sProdCrManual150MortLateCount"); }
            set { SetString("sProdCrManual150MortLateCount", value); }
        }
        public string sProdCrManual30MortLateCount_rep
        {
            get { return GetString("sProdCrManual30MortLateCount"); }
            set { SetString("sProdCrManual30MortLateCount", value); }
        }
        public string sProdCrManualRolling60MortLateCount_rep
        {
            get { return GetString("sProdCrManualRolling60MortLateCount"); }
            set { SetString("sProdCrManualRolling60MortLateCount", value); }
        }
        public string sProdCrManualRolling90MortLateCount_rep
        {
            get { return GetString("sProdCrManualRolling90MortLateCount"); }
            set { SetString("sProdCrManualRolling90MortLateCount", value); }
        }
        public bool sProdCrManualForeclosureHas
        {
            get { return GetBool("sProdCrManualForeclosureHas"); }
            set { SetBool("sProdCrManualForeclosureHas", value); }
        }
        public string sProdCrManualForeclosureRecentFileMon_rep
        {
            get { return GetString("sProdCrManualForeclosureRecentFileMon"); }
            set { SetString("sProdCrManualForeclosureRecentFileMon", value); }
        }
        public string sProdCrManualForeclosureRecentFileYr_rep
        {
            get { return GetString("sProdCrManualForeclosureRecentFileYr"); }
            set { SetString("sProdCrManualForeclosureRecentFileYr", value); }
        }
        public E_sProdCrManualDerogRecentStatusT sProdCrManualForeclosureRecentStatusT
        {
            get { return (E_sProdCrManualDerogRecentStatusT) GetEnum("sProdCrManualForeclosureRecentStatusT"); }
            set { SetEnum("sProdCrManualForeclosureRecentStatusT", value); }
        }
        public string sProdCrManualForeclosureRecentSatisfiedMon_rep
        {
            get { return GetString("sProdCrManualForeclosureRecentSatisfiedMon"); }
            set { SetString("sProdCrManualForeclosureRecentSatisfiedMon", value); }
        }
        public string sProdCrManualForeclosureRecentSatisfiedYr_rep
        {
            get { return GetString("sProdCrManualForeclosureRecentSatisfiedYr"); }
            set { SetString("sProdCrManualForeclosureRecentSatisfiedYr", value); }
        }
        public bool sProdCrManualBk7Has
        {
            get { return GetBool("sProdCrManualBk7Has"); }
            set { SetBool("sProdCrManualBk7Has", value); }
        }
        public string sProdCrManualBk7RecentFileMon_rep
        {
            get { return GetString("sProdCrManualBk7RecentFileMon"); }
            set { SetString("sProdCrManualBk7RecentFileMon", value); }
        }
        public string sProdCrManualBk7RecentFileYr_rep
        {
            get { return GetString("sProdCrManualBk7RecentFileYr"); }
            set { SetString("sProdCrManualBk7RecentFileYr", value); }
        }
        public E_sProdCrManualDerogRecentStatusT sProdCrManualBk7RecentStatusT
        {
            get { return (E_sProdCrManualDerogRecentStatusT) GetEnum("sProdCrManualBk7RecentStatusT"); }
            set { SetEnum("sProdCrManualBk7RecentStatusT", value); }
        }
        public string sProdCrManualBk7RecentSatisfiedMon_rep
        {
            get { return GetString("sProdCrManualBk7RecentSatisfiedMon"); }
            set { SetString("sProdCrManualBk7RecentSatisfiedMon", value); }
        }
        public string sProdCrManualBk7RecentSatisfiedYr_rep
        {
            get { return GetString("sProdCrManualBk7RecentSatisfiedYr"); }
            set { SetString("sProdCrManualBk7RecentSatisfiedYr", value); }
        }
        public bool sProdCrManualBk13Has
        {
            get { return GetBool("sProdCrManualBk13Has"); }
            set { SetBool("sProdCrManualBk13Has", value); }
        }
        public string sProdCrManualBk13RecentFileMon_rep
        {
            get { return GetString("sProdCrManualBk13RecentFileMon"); }
            set { SetString("sProdCrManualBk13RecentFileMon", value); }
        }
        public string sProdCrManualBk13RecentFileYr_rep
        {
            get { return GetString("sProdCrManualBk13RecentFileYr"); }
            set { SetString("sProdCrManualBk13RecentFileYr", value); }
        }
        public E_sProdCrManualDerogRecentStatusT sProdCrManualBk13RecentStatusT
        {
            get { return (E_sProdCrManualDerogRecentStatusT) GetEnum("sProdCrManualBk13RecentStatusT"); }
            set { SetEnum("sProdCrManualBk13RecentStatusT", value); }
        }
        public string sProdCrManualBk13RecentSatisfiedMon_rep
        {
            get { return GetString("sProdCrManualBk13RecentSatisfiedMon"); }
            set { SetString("sProdCrManualBk13RecentSatisfiedMon", value); }
        }
        public string sProdCrManualBk13RecentSatisfiedYr_rep
        {
            get { return GetString("sProdCrManualBk13RecentSatisfiedYr"); }
            set { SetString("sProdCrManualBk13RecentSatisfiedYr", value); }
        }
        public bool sProdHasHousingHistory
        {
            get { return GetBool("sProdHasHousingHistory"); }
            set { SetBool("sProdHasHousingHistory", value); }
        }

        public bool sProdFilterDue10Yrs
        {
            get { return GetBool("sProdFilterDue10Yrs"); }
            set { SetBool("sProdFilterDue10Yrs", value); }
        }
        public bool sProdFilterDue15Yrs
        {
            get { return GetBool("sProdFilterDue15Yrs"); }
            set { SetBool("sProdFilterDue15Yrs", value); }
        }
        public bool sProdFilterDue20Yrs
        {
            get { return GetBool("sProdFilterDue20Yrs"); }
            set { SetBool("sProdFilterDue20Yrs", value); }
        }
        public bool sProdFilterDue25Yrs
        {
            get { return GetBool("sProdFilterDue25Yrs"); }
            set { SetBool("sProdFilterDue25Yrs", value); }
        }
        public bool sProdFilterDue30Yrs
        {
            get { return GetBool("sProdFilterDue30Yrs"); }
            set { SetBool("sProdFilterDue30Yrs", value); }
        }
        public bool sProdFilterDue40Yrs //OUTDATED. Case 65859
        {
            get { return GetBool("sProdFilterDue40Yrs"); }
            set { SetBool("sProdFilterDue40Yrs", value); }
        }
        public bool sProdFilterDueOther
        {
            get { return GetBool("sProdFilterDueOther"); }
            set { SetBool("sProdFilterDueOther", value); }
        }
        public bool sProdFilterFinMethFixed
        {
            get { return GetBool("sProdFilterFinMethFixed"); }
            set { SetBool("sProdFilterFinMethFixed", value); }
        }
        public bool sProdFilterFinMethOptionArm
        {
            get { return GetBool("sProdFilterFinMethOptionArm"); }
            set { SetBool("sProdFilterFinMethOptionArm", value); }
        }
        public bool sProdFilterFinMethLess1YrArm  //OUTDATED. Case 65859
        {
            get { return GetBool("sProdFilterFinMethLess1YrArm"); }
            set { SetBool("sProdFilterFinMethLess1YrArm", value); }
        }
        public bool sProdFilterFinMeth2YrsArm   //OUTDATED. Case 65859
        {
            get { return GetBool("sProdFilterFinMeth2YrsArm"); }
            set { SetBool("sProdFilterFinMeth2YrsArm", value); }
        }
        public bool sProdFilterFinMeth3YrsArm
        {
            get { return GetBool("sProdFilterFinMeth3YrsArm"); }
            set { SetBool("sProdFilterFinMeth3YrsArm", value); }
        }
        public bool sProdFilterFinMeth5YrsArm
        {
            get { return GetBool("sProdFilterFinMeth5YrsArm"); }
            set { SetBool("sProdFilterFinMeth5YrsArm", value); }
        }
        public bool sProdFilterFinMeth7YrsArm
        {
            get { return GetBool("sProdFilterFinMeth7YrsArm"); }
            set { SetBool("sProdFilterFinMeth7YrsArm", value); }
        }
        public bool sProdFilterFinMeth10YrsArm
        {
            get { return GetBool("sProdFilterFinMeth10YrsArm"); }
            set { SetBool("sProdFilterFinMeth10YrsArm", value); }
        }
        public bool sProdFilterFinMethOther
        {
            get { return GetBool("sProdFilterFinMethOther"); }
            set { SetBool("sProdFilterFinMethOther", value); }
        }

        public bool sProdFilterPmtTPI
        {
            get { return GetBool("sProdFilterPmtTPI"); }
            set { SetBool("sProdFilterPmtTPI", value); }
        }

        public bool sProdFilterPmtTIOnly
        {
            get { return GetBool("sProdFilterPmtTIOnly"); }
            set { SetBool("sProdFilterPmtTIOnly", value); }
        }

        public string sProdEstimatedResidualI_rep
        {
            get { return GetString("sProdEstimatedResidualI"); }
            set { SetString("sProdEstimatedResidualI", value); }
        }
        public bool sLpIsNegAmortOtherLien
        {
            get { return GetBool("sLpIsNegAmortOtherLien"); }
            set { SetBool("sLpIsNegAmortOtherLien", value); }
        }
        public E_sFinMethT sOtherLFinMethT
        {
            get { return (E_sFinMethT)GetEnum("sOtherLFinMethT"); }
            set { SetEnum("sOtherLFinMethT", value); }
        }
        public string sProOFinPmtPe_rep
        {
            get { return GetString("sProOFinPmtPe"); }
            set { SetString("sProOFinPmtPe", value); }
        }
        public bool sProdIncludeMyCommunityProc
        {
            get { return GetBool("sProdIncludeMyCommunityProc"); }
            set { SetBool("sProdIncludeMyCommunityProc", value); }
        }
        public bool sProdIncludeHomePossibleProc
        {
            get { return GetBool("sProdIncludeHomePossibleProc"); }
            set { SetBool("sProdIncludeHomePossibleProc", value); }
        }
        public bool sProdIncludeNormalProc
        {
            get { return GetBool("sProdIncludeNormalProc"); }
            set { SetBool("sProdIncludeNormalProc", value); }
        }
        public bool sProdIncludeFHATotalProc
        {
            get { return GetBool("sProdIncludeFHATotalProc"); }
            set { SetBool("sProdIncludeFHATotalProc", value); }
        }
        public bool sProdIncludeVAProc
        {
            get { return GetBool("sProdIncludeVAProc"); }
            set { SetBool("sProdIncludeVAProc", value); }
        }
        #endregion


        private AbstractListPmlSnapshot<DataAppPmlSnapshot> m_dataAppList = new AbstractListPmlSnapshot<DataAppPmlSnapshot>("aAppId");

        /// <summary>
        /// Create dataAppPmlSnapshot and add to this object.
        /// </summary>
        /// <returns></returns>
        public DataAppPmlSnapshot CreateDataApp(string id)
        {
            DataAppPmlSnapshot o = new DataAppPmlSnapshot();
            o.Id = id;
            m_dataAppList.Add(o);
            return o;
        }

        public List<FieldTrackInfo> GetDifferences(DataLoanPmlSnapshot o)
        {
            List<FieldTrackInfo> list = new List<FieldTrackInfo>();

            this.Compare(list, o);
            m_dataAppList.Compare(list, o.m_dataAppList);

            return list;
        }

        private void Handle_aAppId(PmlSnapshotDiffResult result, FieldTrackInfo dataAppDiff)
        {
            switch (dataAppDiff.FieldTracKInfoTypeT)
            {
                case E_FieldTrackInfoTypeT.New:
                case E_FieldTrackInfoTypeT.Obsolete:

                    result.Add(dataAppDiff);
                    break;
                case E_FieldTrackInfoTypeT.Normal:
                    foreach (var dataAppField in dataAppDiff.List)
                    {
                        dataAppField.RecordIndex = dataAppDiff.RecordIndex;
                        result.Add(dataAppField);
                    }
                    break;
            }
            return;
        }
        public PmlSnapshotDiffResult Diff(DataLoanPmlSnapshot o)
        {
            List<FieldTrackInfo> list = GetDifferences(o);

            PmlSnapshotDiffResult result = new PmlSnapshotDiffResult(this.Timestamp, this.UserName);

            foreach (var diff in list)
            {
                if (diff.FieldId == "aAppId")
                {
                    Handle_aAppId(result, diff);
                }
                else
                {
                    result.Add(diff);
                }
            }

            return result;

        }
        protected override void WriteAdditionalXml(XmlWriter writer)
        {
            m_dataAppList.WriteXml(writer);
        }
        protected override void ReadAdditionalXml(XmlReader reader)
        {
            m_dataAppList.ReadXml(reader);
        }
    }

}
