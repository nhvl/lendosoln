﻿
namespace LendersOffice.PmlSnapshot
{
    public class LiabilityPmlSnapshot : AbstractPmlSnapshot
    {
        public override string FriendlyUniqueDescription
        {
            get
            {
                return ComNm;
            }
        }
        protected override string TagName
        {
            get { return "Liability"; }
        }
        public string ComNm
        {
            get { return GetString("aLiaCollection_ComNm"); }
            set { SetString("aLiaCollection_ComNm", value); }
        }
        public string Bal_rep
        {
            get { return GetString("aLiaCollection_Bal"); }
            set { SetString("aLiaCollection_Bal", value); }
        }
        public string Pmt_rep
        {
            get { return GetString("aLiaCollection_Pmt"); }
            set { SetString("aLiaCollection_Pmt", value); }
        }
        public bool WillBePdOff
        {
            get { return GetBool("aLiaCollection_WillBePdOff"); }
            set { SetBool("aLiaCollection_WillBePdOff", value); }
        }
        public bool UsedInRatio
        {
            get { return GetBool("aLiaCollection_UsedInRatio"); }
            set { SetBool("aLiaCollection_UsedInRatio", value); }
        }

    }

}
