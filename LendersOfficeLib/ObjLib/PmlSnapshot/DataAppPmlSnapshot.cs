﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;
using System.Xml;
using LendersOffice.Security;

namespace LendersOffice.PmlSnapshot
{
    public class DataAppPmlSnapshot : AbstractPmlSnapshot
    {
        protected override string TagName
        {
            get { return "DataApp"; }
        }

        public override string FriendlyUniqueDescription
        {
            get
            {
                return Tools.ComposeFullName(aBFirstNm, aBMidNm, aBLastNm, aBSuffix);
            }
        }
        protected override bool IsSkipCompare(string fieldId)
        {
            AbstractUserPrincipal principal = System.Threading.Thread.CurrentPrincipal as AbstractUserPrincipal;
            if (null != principal)
            {
                if (principal.IsPricingMultipleAppsSupported)
                {


                }
                else
                {
                    // 5/1/2009 dd - If legacy mode support then skip these fields.
                    string[] skipFields = { 
                                              "aTransmOMonPmtPe","aProdCrManualNonRolling30MortLateCount","aProdCrManual60MortLateCount","aProdCrManual90MortLateCount"
                                              , "aProdCrManual120MortLateCount", "aProdCrManual150MortLateCount", "aProdCrManual30MortLateCount","aProdCrManualRolling60MortLateCount"
                                              , "aProdCrManualRolling90MortLateCount", "aProdCrManualForeclosureHas", "aProdCrManualForeclosureRecentFileMon", "aProdCrManualForeclosureRecentFileYr"
                                              , "aProdCrManualForeclosureRecentStatusT", "aProdCrManualForeclosureRecentSatisfiedMon", "aProdCrManualForeclosureRecentSatisfiedYr"
                                              , "aProdCrManualBk7Has", "aProdCrManualBk7RecentFileMon", "aProdCrManualBk7RecentFileYr","aProdCrManualBk7RecentStatusT"
                                              , "aProdCrManualBk7RecentSatisfiedMon", "aProdCrManualBk7RecentSatisfiedYr", "aProdCrManualBk13Has", "aProdCrManualBk13RecentFileMon"
                                              , "aProdCrManualBk13RecentFileYr", "aProdCrManualBk13RecentStatusT", "aProdCrManualBk13RecentSatisfiedMon", "aProdCrManualBk13RecentSatisfiedYr"
                                              , "aOpNegCfPe", "aBIsSelfEmplmt", "aCIsSelfEmplmt", "aProdCCitizenT"
                                          };
                    if (skipFields.Contains(fieldId))
                    {
                        return true;
                    }
                }
            }
            return false;

        }
        #region Public Get/Set Properties
        public string aBFirstNm
        {
            get { return GetString("aBFirstNm"); }
            set { SetString("aBFirstNm", value); }
        }
        public string aBMidNm
        {
            get { return GetString("aBMidNm"); }
            set { SetString("aBMidNm", value); }
        }
        public string aBLastNm
        {
            get { return GetString("aBLastNm"); }
            set { SetString("aBLastNm", value); }
        }
        public string aBSuffix
        {
            get { return GetString("aBSuffix"); }
            set { SetString("aBSuffix", value); }
        }
        public string aBSsn
        {
            get { return GetString("aBSsn"); }
            set { SetString("aBSsn", value); }
        }
        public string aBExperianScorePe_rep
        {
            get { return GetString("aBExperianScorePe"); }
            set { SetString("aBExperianScorePe", value); }
        }
        public string aBTransUnionScorePe_rep
        {
            get { return GetString("aBTransUnionScorePe"); }
            set { SetString("aBTransUnionScorePe", value); }
        }
        public string aBEquifaxScorePe_rep
        {
            get { return GetString("aBEquifaxScorePe"); }
            set { SetString("aBEquifaxScorePe", value); }
        }
        public E_aProdCitizenT aProdBCitizenT
        {
            get { return (E_aProdCitizenT)GetEnum("aProdBCitizenT"); }
            set { SetEnum("aProdBCitizenT", value); }
        }
        public E_aProdCitizenT aProdCCitizenT
        {
            get { return (E_aProdCitizenT)GetEnum("aProdCCitizenT"); }
            set { SetEnum("aProdCCitizenT", value); }
        }
        public bool aBIsSelfEmplmt
        {
            get { return GetBool("aBIsSelfEmplmt"); }
            set { SetBool("aBIsSelfEmplmt", value); }
        }
        public bool aCIsSelfEmplmt
        {
            get { return GetBool("aCIsSelfEmplmt"); }
            set { SetBool("aCIsSelfEmplmt", value); }
        }
        public bool aBHasSpouse
        {
            get { return GetBool("aBHasSpouse"); }
            set { SetBool("aBHasSpouse", value); }
        }
        public string aCFirstNm
        {
            get { return GetString("aCFirstNm"); }
            set { SetString("aCFirstNm", value); }
        }
        public string aCMidNm
        {
            get { return GetString("aCMidNm"); }
            set { SetString("aCMidNm", value); }
        }
        public string aCLastNm
        {
            get { return GetString("aCLastNm"); }
            set { SetString("aCLastNm", value); }
        }
        public string aCSuffix
        {
            get { return GetString("aCSuffix"); }
            set { SetString("aCSuffix", value); }
        }
        public string aCSsn
        {
            get { return GetString("aCSsn"); }
            set { SetString("aCSsn", value); }
        }
        public string aCExperianScorePe_rep
        {
            get { return GetString("aCExperianScorePe"); }
            set { SetString("aCExperianScorePe", value); }
        }
        public string aCTransUnionScorePe_rep
        {
            get { return GetString("aCTransUnionScorePe"); }
            set { SetString("aCTransUnionScorePe", value); }
        }
        public string aCEquifaxScorePe_rep
        {
            get { return GetString("aCEquifaxScorePe"); }
            set { SetString("aCEquifaxScorePe", value); }
        }
        public bool aIsBorrSpousePrimaryWageEarner
        {
            get { return GetBool("aIsBorrSpousePrimaryWageEarner"); }
            set { SetBool("aIsBorrSpousePrimaryWageEarner", value); }
        }
        public string aPresOHExpPe_rep
        {
            get { return GetString("aPresOHExpPe"); }
            set { SetString("aPresOHExpPe", value); }
        }
        public string aPublicRecordXmlContent
        {
            get { return GetString("aPublicRecordXmlContent"); }
            set { SetString("aPublicRecordXmlContent", value); }
        }
        public Guid aCreditReportFileDbKey
        {
            get { return GetGuid("aCreditReportFileDbKey"); }
            set { SetGuid("aCreditReportFileDbKey", value); }
        }
        public string aTransmOMonPmtPe_rep
        {
            get { return GetString("aTransmOMonPmtPe"); }
            set { SetString("aTransmOMonPmtPe", value); }
        }
        public string aOpNegCfPe_rep
        {
            get { return GetString("aOpNegCfPe"); }
            set { SetString("aOpNegCfPe", value); }
        }
        public string aProdCrManualNonRolling30MortLateCount_rep
        {
            get { return GetString("aProdCrManualNonRolling30MortLateCount"); }
            set { SetString("aProdCrManualNonRolling30MortLateCount", value); }
        }
        public string aProdCrManual60MortLateCount_rep
        {
            get { return GetString("aProdCrManual60MortLateCount"); }
            set { SetString("aProdCrManual60MortLateCount", value); }
        }
        public string aProdCrManual90MortLateCount_rep
        {
            get { return GetString("aProdCrManual90MortLateCount"); }
            set { SetString("aProdCrManual90MortLateCount", value); }
        }
        public string aProdCrManual120MortLateCount_rep
        {
            get { return GetString("aProdCrManual120MortLateCount"); }
            set { SetString("aProdCrManual120MortLateCount", value); }
        }
        public string aProdCrManual150MortLateCount_rep
        {
            get { return GetString("aProdCrManual150MortLateCount"); }
            set { SetString("aProdCrManual150MortLateCount", value); }
        }
        public string aProdCrManual30MortLateCount_rep
        {
            get { return GetString("aProdCrManual30MortLateCount"); }
            set { SetString("aProdCrManual30MortLateCount", value); }
        }
        public string aProdCrManualRolling60MortLateCount_rep
        {
            get { return GetString("aProdCrManualRolling60MortLateCount"); }
            set { SetString("aProdCrManualRolling60MortLateCount", value); }
        }
        public string aProdCrManualRolling90MortLateCount_rep
        {
            get { return GetString("aProdCrManualRolling90MortLateCount"); }
            set { SetString("aProdCrManualRolling90MortLateCount", value); }
        }
        public bool aProdCrManualForeclosureHas
        {
            get { return GetBool("aProdCrManualForeclosureHas"); }
            set { SetBool("aProdCrManualForeclosureHas", value); }
        }
        public string aProdCrManualForeclosureRecentFileMon_rep
        {
            get { return GetString("aProdCrManualForeclosureRecentFileMon"); }
            set { SetString("aProdCrManualForeclosureRecentFileMon", value); }
        }
        public string aProdCrManualForeclosureRecentFileYr_rep
        {
            get { return GetString("aProdCrManualForeclosureRecentFileYr"); }
            set { SetString("aProdCrManualForeclosureRecentFileYr", value); }
        }
        public E_sProdCrManualDerogRecentStatusT aProdCrManualForeclosureRecentStatusT
        {
            get { return (E_sProdCrManualDerogRecentStatusT)GetEnum("aProdCrManualForeclosureRecentStatusT"); }
            set { SetEnum("aProdCrManualForeclosureRecentStatusT", value); }
        }
        public string aProdCrManualForeclosureRecentSatisfiedMon_rep
        {
            get { return GetString("aProdCrManualForeclosureRecentSatisfiedMon"); }
            set { SetString("aProdCrManualForeclosureRecentSatisfiedMon", value); }
        }
        public string aProdCrManualForeclosureRecentSatisfiedYr_rep
        {
            get { return GetString("aProdCrManualForeclosureRecentSatisfiedYr"); }
            set { SetString("aProdCrManualForeclosureRecentSatisfiedYr", value); }
        }
        public bool aProdCrManualBk7Has
        {
            get { return GetBool("aProdCrManualBk7Has"); }
            set { SetBool("aProdCrManualBk7Has", value); }
        }
        public string aProdCrManualBk7RecentFileMon_rep
        {
            get { return GetString("aProdCrManualBk7RecentFileMon"); }
            set { SetString("aProdCrManualBk7RecentFileMon", value); }
        }
        public string aProdCrManualBk7RecentFileYr_rep
        {
            get { return GetString("aProdCrManualBk7RecentFileYr"); }
            set { SetString("aProdCrManualBk7RecentFileYr", value); }
        }
        public E_sProdCrManualDerogRecentStatusT aProdCrManualBk7RecentStatusT
        {
            get { return (E_sProdCrManualDerogRecentStatusT)GetEnum("aProdCrManualBk7RecentStatusT"); }
            set { SetEnum("aProdCrManualBk7RecentStatusT", value); }
        }
        public string aProdCrManualBk7RecentSatisfiedMon_rep
        {
            get { return GetString("aProdCrManualBk7RecentSatisfiedMon"); }
            set { SetString("aProdCrManualBk7RecentSatisfiedMon", value); }
        }
        public string aProdCrManualBk7RecentSatisfiedYr_rep
        {
            get { return GetString("aProdCrManualBk7RecentSatisfiedYr"); }
            set { SetString("aProdCrManualBk7RecentSatisfiedYr", value); }
        }
        public bool aProdCrManualBk13Has
        {
            get { return GetBool("aProdCrManualBk13Has"); }
            set { SetBool("aProdCrManualBk13Has", value); }
        }
        public string aProdCrManualBk13RecentFileMon_rep
        {
            get { return GetString("aProdCrManualBk13RecentFileMon"); }
            set { SetString("aProdCrManualBk13RecentFileMon", value); }
        }
        public string aProdCrManualBk13RecentFileYr_rep
        {
            get { return GetString("aProdCrManualBk13RecentFileYr"); }
            set { SetString("aProdCrManualBk13RecentFileYr", value); }
        }
        public E_sProdCrManualDerogRecentStatusT aProdCrManualBk13RecentStatusT
        {
            get { return (E_sProdCrManualDerogRecentStatusT)GetEnum("aProdCrManualBk13RecentStatusT"); }
            set { SetEnum("aProdCrManualBk13RecentStatusT", value); }
        }
        public string aProdCrManualBk13RecentSatisfiedMon_rep
        {
            get { return GetString("aProdCrManualBk13RecentSatisfiedMon"); }
            set { SetString("aProdCrManualBk13RecentSatisfiedMon", value); }
        }
        public string aProdCrManualBk13RecentSatisfiedYr_rep
        {
            get { return GetString("aProdCrManualBk13RecentSatisfiedYr"); }
            set { SetString("aProdCrManualBk13RecentSatisfiedYr", value); }
        }

        #endregion

        private bool m_isCreditReportModified = false;
        protected override void OnFieldModified(string fieldId)
        {
            if (fieldId == "aCreditReportFileDbKey")
            {
                m_isCreditReportModified = true;
            }
        }

        private AbstractListPmlSnapshot<LiabilityPmlSnapshot> m_liabilities = new AbstractListPmlSnapshot<LiabilityPmlSnapshot>("aLiaCollection");
        public void AddLiability(LiabilityPmlSnapshot o)
        {
            m_liabilities.Add(o);
        }

        public override void Compare(List<FieldTrackInfo> resultList, AbstractPmlSnapshot o)
        {
            base.Compare(resultList, o);
            if (!m_isCreditReportModified)
            {
                // 4/17/2009 dd - Only compare liabilities when credit report does not get modify.
                m_liabilities.Compare(resultList, ((DataAppPmlSnapshot)o).m_liabilities);
            }
        }
        protected override void WriteAdditionalXml(XmlWriter writer)
        {
            m_liabilities.WriteXml(writer);
        }
        protected override void ReadAdditionalXml(XmlReader reader)
        {
            m_liabilities.ReadXml(reader);
        }

    }

}
