﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccess;
using System.Text;

namespace LendersOffice.PmlSnapshot
{

    public class PmlSnapshotDiffResult
    {

        public DateTime OriginalTimestamp { get; private set; }
        public string OriginalUserName { get; private set; }

        public PmlSnapshotDiffResult(DateTime timestamp, string userName)
        {
            OriginalTimestamp = timestamp;
            OriginalUserName = userName;
        }
        private int m_count = 0;
        public List<string> Categories
        {
            get { return new List<string> { "Step 1 - Credit", "Step 2 - Applicant", "Step 3 - Property & Loan" }; }
        }
        private Dictionary<string, List<FieldTrackInfo>> m_hash = new Dictionary<string, List<FieldTrackInfo>>();
        public List<FieldTrackInfo> GetDiffByCategory(string category)
        {
            List<FieldTrackInfo> list;

            if (!m_hash.TryGetValue(category, out list))
            {
                list = new List<FieldTrackInfo>();
                m_hash.Add(category, list);
            }
            return list;
        }
        public void Add(FieldTrackInfo item)
        {
            m_count++;
            var o = GetDiffByCategory(item.Category);
            o.Add(item);
        }
        public int Count
        {
            get { return m_count; }
        }

        public string GetDebugString()
        {
            StringBuilder sb = new StringBuilder();
            int index = 0;
            foreach (var list in m_hash.Values)
            {
                foreach (var info in list)
                {
                    sb.AppendFormat("[{0}] - {1} : Old=[{2}], New=[{3}]{4}", index, info.FieldId, info.OldValue, info.NewValue, Environment.NewLine);
                    index++;
                }
            }
            return sb.ToString();
        }
    }
}
