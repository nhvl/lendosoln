﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using DataAccess;
using System.Xml.Serialization;
using System.Xml.Schema;

namespace LendersOffice.PmlSnapshot
{
    public class AbstractListPmlSnapshot<T> : Dictionary<string, T>, IXmlSerializable where T : AbstractPmlSnapshot, new()
    {
        private string m_keyName;
        public AbstractListPmlSnapshot(string keyName)
        {
            m_keyName = keyName;
        }
        public void Add(T o)
        {
            this.Add(o.Id, o);
        }
        public void Compare(List<FieldTrackInfo> resultList, AbstractListPmlSnapshot<T> newList)
        {
            Dictionary<string, bool> visited = new Dictionary<string, bool>();

            int recordIndex = 1;
            foreach (var o in this)
            {
                
                T newValue;
                if (newList.TryGetValue(o.Key, out newValue))
                {
                    FieldTrackInfo f = new FieldTrackInfo(m_keyName, string.Empty, string.Empty, E_FieldTrackInfoTypeT.Normal, recordIndex);
                    o.Value.Compare(f.List, newValue);
                    if (f.List.Count > 0)
                    {
                        resultList.Add(f);
                    }
                }
                else
                {
                    resultList.Add(new FieldTrackInfo(m_keyName, o.Value.FriendlyUniqueDescription, string.Empty, E_FieldTrackInfoTypeT.Obsolete, recordIndex));
                }
                visited.Add(o.Key, true);
                recordIndex++;

            }
            foreach (var o in newList)
            {
                if (visited.ContainsKey(o.Key))
                {
                    continue;
                }
                resultList.Add(new FieldTrackInfo(m_keyName, string.Empty, o.Value.FriendlyUniqueDescription, E_FieldTrackInfoTypeT.New, recordIndex));
                recordIndex++;

            }
        }

        #region IXmlSerializable Members

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            T o = new T();
            o.ReadXml(reader);
            this.Add(o);
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (var o in this)
            {
                o.Value.WriteXml(writer);
            }
        }

        #endregion
    }

}
