﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Security;
using LqbGrammar.DataTypes;

namespace LendersOffice.LoanSearch
{
    public class LendingQBSearchFilter
    {        
        public Guid sLId { get; set; }
        public string sLNm { get; set; }
        public string sLRefNm { get; set; }
        public string aBLastNm { get; set; }
        public string aBFirstNm { get; set; }
        public string SsnLastFour { get; set; }
        public string sSpAddr { get; set; }
        public string sMersMin { get; set; }
        public string sAgencyCaseNum { get; set; }
        public string sLenLNum { get; set; }
        public string sInvestLNum { get; set; }
        public string sSubservicerLoanNm { get; set; }
        public Guid sBranchId { get; set; }
        public int sStatusT { get; set; }
        public int sStatusDateT { get; set; }
        public int sLT { get; set; }
        public bool IsTemplate { get; set; }
        public bool IsTest { get; set; }
        /// <summary>
        /// setting this to true makes it ignore islead bit.
        /// </summary>
        public bool IncludeLeadAndLoanStatuses { get; set; }
        public bool IsLead { get; set; }
        public bool? sIsRemn { get; set; }
        public bool UseStatusT = true;

        public E_PortalMode PortalMode { get; set; }
        public Dictionary<E_sCorrespondentProcessT, HashSet<E_sStatusT>> sStatusTDictionary { get; set; }
        public E_sCorrespondentProcessT? sCorrespondentProcess { get; set; } = E_sCorrespondentProcessT.Blank;

        public E_FilterStatusDateT FilterStatusDateT { get; set; }
        public int LoanAssignedToT { get; set; }
        public bool IsDuplicateSearch { get; set; }

        /// <summary>
        /// this is used if sStatusT == -1, and if it's not null or empty.
        /// </summary>
        public HashSet<E_sStatusT> sStatusTFilters { get; set; }

        public bool IsEmptySearchFilter
        {
            get
            {
                return sLId == Guid.Empty && string.IsNullOrEmpty(sLNm) && string.IsNullOrEmpty(aBLastNm)
                    && string.IsNullOrEmpty(aBFirstNm) && string.IsNullOrEmpty(SsnLastFour)
                    && string.IsNullOrEmpty(sSpAddr) && sBranchId == Guid.Empty
                    && sStatusT == -1 && sStatusDateT == 0 && sLT == -1 && IsTemplate == false
                    && IsTest == false
                    && string.IsNullOrEmpty(sMersMin) && string.IsNullOrEmpty(sAgencyCaseNum)
                    && string.IsNullOrEmpty(sLenLNum) && string.IsNullOrEmpty(sInvestLNum)
                    && m_employeeFilterDictionary.Count == 0 && string.IsNullOrEmpty(sSubservicerLoanNm)
                    && (sStatusTFilters == null || sStatusTFilters.Count == 0)
                    && string.IsNullOrEmpty(sLRefNm);
            }
        }

        private Dictionary<E_RoleT, Guid> m_employeeFilterDictionary = new Dictionary<E_RoleT, Guid>();
        public void AddEmployeeFilter(E_RoleT roleType, Guid employeeId)
        {
            m_employeeFilterDictionary[roleType] = employeeId;
        }

        public IEnumerable<KeyValuePair<E_RoleT, Guid>> GetEmployeeFilterList()
        {
            List<KeyValuePair<E_RoleT, Guid>> list = new List<KeyValuePair<E_RoleT, Guid>>();

            foreach (var o in m_employeeFilterDictionary)
            {
                list.Add(o);
            }
            return list;
        }
        public override string ToString()
        {
            if (sLId != Guid.Empty)
            {
                return "sLId = " + sLId;
            }

            StringBuilder sb = new StringBuilder();
            
            sb.AppendFormat("sLNm={0}, sLRefNm={1},aBLastNm={2},aBFirstNm={3},SsnLastFour={4},sSpAddr={5},sBranchId={6},sStatusT={7},sStatusDateT={8},sLT={9},IsTemplate={10},IsLead={11}",
                sLNm, sLRefNm, aBLastNm, aBFirstNm, SsnLastFour, sSpAddr, sBranchId, sStatusT, sStatusDateT, sLT, IsTemplate, IsLead);

            foreach (var o in GetEmployeeFilterList())
            {
                string fieldId = Role.GetLoanFileCacheFieldName(o.Key);
                sb.Append("," + fieldId + "=" + o.Value);
            }
            sb.AppendFormat(",sMersMin={0},sAgencyCaseNum={1},sLenLNum={2},sInvestLNum={3},sSubservicerLoanNm={4}",
                sMersMin, sAgencyCaseNum, sLenLNum, sInvestLNum, sSubservicerLoanNm);
            return sb.ToString();
        }

    }
}
