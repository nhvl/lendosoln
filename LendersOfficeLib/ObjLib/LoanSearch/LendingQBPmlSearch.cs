﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LendersOffice.Security;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Data;
using LendersOffice.Common;
using LendersOffice.Admin;
using LendersOffice.ConfigSystem;
using LendersOffice.Constants;
using System.Xml;
using CommonLib;
using LendersOffice.ConfigSystem.Operations;
using LqbGrammar.DataTypes;

namespace LendersOffice.LoanSearch
{
    public static class LendingQBPmlSearch
    {
        public static DataSet Search(AbstractUserPrincipal principal, LendingQBSearchFilter searchFilter, int maxRowCount, int? topRowsToPull = null)
        {
            List<string> selectColumns = new List<string>()
            {
                	"fc.sLId" ,
                    "0 AS CanWrite",
                    "0 AS CanRead",
                    "0 AS CanReRun",
                    "0 AS OpenLinkType",
                    "0 AS InScope",                    
					"fc.sBrokerId" ,
					"fc.sBranchId" ,
					"fc.sLNm" ,
					"fc.sPrimBorrowerFullNm" ,
					"fc.sStatusT" ,
					"fc.sLienPosT" ,
					"fc.sOpenedD" ,
					"fc.sNoteIRSubmitted" ,
					"fc.IsTemplate" ,
					"fc.IsValid" ,
					"fc.sEmployeeManagerId" ,
					"fc.sEmployeeUnderwriterId" ,
					"fc.sEmployeeLockDeskId" ,
					"fc.sEmployeeProcessorId" ,
					"fc.sEmployeeLoanOpenerId" ,
					"fc.sEmployeeLoanRepId" ,
					"fc.sEmployeeLenderAccExecId" ,
					"fc.sEmployeeRealEstateAgentId" ,
					"fc.sEmployeeCallCenterAgentId" ,
                    "fc.sEmployeeCloserId" ,
                    "fc.sEmployeeBrokerProcessorId",
					"fc.PmlExternalManagerEmployeeId" ,
                    "fc.sRateLockStatusT",
                    "'' AS sRateLockStatusT_rep",
                    "fc.PmlExternalBrokerProcessorManagerEmployeeId",
                    "fc.sPmlBrokerId",
                    "fc.sStatusD",
                    "fc.sRLckdExpiredD",
                    "dbo.GetLoanStatusName(fc.sStatusT) AS LoanStatus", 
                    "fc.sLAmtCalc", 
                    "fc.sPmlSubmitStatusT", 
                    "fc.sLpTemplateId",
                    "fc.sEmployeeLoanRepName",
                    "fc.sEmployeeBrokerProcessorName",
                    "fc2.sActiveTasksDisplay",
                    "fc2.sLenderCaseNum",
                    "fc.sLT",
                    "fc.sSpAddr",
                    "fc2.sFinalLAmt", 
                    "fc2.sEmployeeExternalSecondaryId",
                    "fc2.sEmployeeExternalSecondaryName",
                    "fc2.sEmployeeExternalPostCloserId",
                    "fc2.sEmployeeExternalPostCloserName",
                    "fc2.PmlExternalSecondaryManagerEmployeeId",
                    "fc2.PmlExternalPostCloserManagerEmployeeId",
                    "fc4.sSuspenseNoticePdfLastSavedD",
                    "fc4.sApprovalCertPdfLastSavedD",
                    "fc4.sRateLockConfirmationPdfLastSavedD",
                    "fc4.aBSsnLastFour"
            };

            if (searchFilter.IsDuplicateSearch)
            {
                selectColumns.Add("a.aAppId");
                selectColumns.Add("a.aBFirstNm");
                selectColumns.Add("a.aBLastNm");
                selectColumns.Add("a.aBSsnEncrypted");
                selectColumns.Add("a.aCFirstNm");
                selectColumns.Add("a.aCLastNm");
                selectColumns.Add("a.aCSsnEncrypted");
                selectColumns.Add("fc.aBFirstNm AS loan_aBFirstNm");
                selectColumns.Add("fc.aBLastNm AS loan_aBLastNm");
                selectColumns.Add("fc4.sEncryptionMigrationVersion");
                selectColumns.Add("fc4.sEncryptionKey");
            }
            else
            {
                selectColumns.Add("fc.aBLastNm");
                selectColumns.Add("fc.aBFirstNm");
            }

            if (!ConstStage.UseNewTpoPortalPipelineTaskRetrieval)
            {
                selectColumns.Add("fc3.sNumOfActiveTasksOnly");
                selectColumns.Add("fc3.sNumOfActiveConditionOnly");
                selectColumns.Add("fc3.sNumOfActiveDueTodayTasksOnly");
                selectColumns.Add("fc3.sNumOfActiveDueTodayConditionOnly");
                selectColumns.Add("fc3.sNumOfActivePastDueConditionOnly");
                selectColumns.Add("fc3.sNumOfActivePastDueTasksOnly");
            }

            List<SqlParameter> parameters = new List<SqlParameter>();

            List<string> whereClauses = new List<string>();

            #region System Default Filter
            whereClauses.Add("fc.IsValid = 1");
            whereClauses.Add("fc.IsTemplate = 0");
            whereClauses.Add("fc.sBrokerId = @sBrokerId");
            whereClauses.Add("fc.sLoanFileT = 0"); // opm 191384 only include non-sandboxed, non-test files.

            parameters.Add(new SqlParameter("@sBrokerId", principal.BrokerId));
            #endregion

            #region PmlLevelAccess Filter
            // Searching for duplicates is now restricted to only loans that the user has access.  EM 12/4/14 Case 197057
            if (searchFilter.sLId != Guid.Empty)
            {
                whereClauses.Add("fc.sLId = @LoanId");
                parameters.Add(new SqlParameter("@LoanId", searchFilter.sLId));
            }

            if (principal.PortalMode == E_PortalMode.Retail)
            {
                selectColumns.Add("fc.sEmployeeProcessorName");

                const string filterIndividualAccessClause = @" (fc.sEmployeeManagerId = @EmployeeId
												OR fc.sEmployeeProcessorId = @EmployeeId OR fc.sEmployeeLoanRepId = @EmployeeId
												OR fc.sEmployeeLoanOpenerId = @EmployeeId OR fc.sEmployeeCallCenterAgentId = @EmployeeId
												OR fc.sEmployeeRealEstateAgentId = @EmployeeId OR fc.sEmployeeLenderAccExecId = @EmployeeId
												OR fc.sEmployeeLockDeskId = @EmployeeId OR fc.sEmployeeUnderwriterId = @EmployeeId
                                                OR fc.sEmployeeCloserId = @EmployeeId OR fc3.sEmployeeShipperId = @EmployeeId
                                                OR fc3.sEmployeeFunderId = @EmployeeId OR fc3.sEmployeePostCloserId = @EmployeeId
                                                OR fc3.sEmployeeInsuringId = @EmployeeId OR fc3.sEmployeeCollateralAgentId = @EmployeeId
                                                OR fc3.sEmployeeDocDrawerId = @EmployeeId OR fc3.sEmployeeCreditAuditorId = @EmployeeId 
                                                OR fc3.sEmployeeDisclosureDeskId = @EmployeeId OR fc3.sEmployeeJuniorProcessorId = @EmployeeId 
                                                OR fc3.sEmployeeJuniorUnderwriterId = @EmployeeId OR fc3.sEmployeeLegalAuditorId = @EmployeeId 
                                                OR fc3.sEmployeeLoanOfficerAssistantId = @EmployeeId OR fc3.sEmployeePurchaserId = @EmployeeId 
                                                OR fc3.sEmployeeQCComplianceId = @EmployeeId OR fc3.sEmployeeSecondaryId = @EmployeeId 
                                                OR fc3.sEmployeeServicingId = @EmployeeId ) ";

                var assignedToAnyone = searchFilter.LoanAssignedToT == 0;
                if (assignedToAnyone && principal.HasPermission(Permission.BrokerLevelAccess))
                {
                    // No-Op.
                }
                else if (assignedToAnyone && principal.HasPermission(Permission.BranchLevelAccess))
                {
                    whereClauses.Add("( fc.sBranchId = @BranchId OR " + filterIndividualAccessClause + ")");

                    parameters.Add(new SqlParameter("@BranchId", principal.BranchId));
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                }
                else
                {
                    whereClauses.Add(filterIndividualAccessClause);
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                }
            }
            else
            {
                if (searchFilter.LoanAssignedToT == 1 || principal.PmlLevelAccess == DataAccess.E_PmlLoanLevelAccess.Individual)
                {
                    whereClauses.Add("(fc.sEmployeeLoanRepId=@EmployeeId OR fc.sEmployeeBrokerProcessorId=@EmployeeId OR fc2.sEmployeeExternalSecondaryId=@EmployeeId OR fc2.sEmployeeExternalPostCloserId=@EmployeeId)");
                    whereClauses.Add("fc.sPmlBrokerId = @PmlBrokerId");
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                    parameters.Add(new SqlParameter("@PmlBrokerId", principal.PmlBrokerId));
                }
                else if (principal.PmlLevelAccess == DataAccess.E_PmlLoanLevelAccess.Supervisor)
                {
                    whereClauses.Add("( ((fc.sEmployeeLoanRepId=@EmployeeId OR fc.sEmployeeBrokerProcessorId=@EmployeeId OR fc2.sEmployeeExternalSecondaryId=@EmployeeId OR fc2.sEmployeeExternalPostCloserId=@EmployeeId) AND fc.sPmlBrokerId=@PmlBrokerId) OR fc.PmlExternalManagerEmployeeId=@EmployeeId OR fc.PmlExternalBrokerProcessorManagerEmployeeId =@EmployeeId OR fc2.PmlExternalSecondaryManagerEmployeeId=@EmployeeId OR fc2.PmlExternalPostCloserManagerEmployeeId=@EmployeeId)");
                    parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
                    parameters.Add(new SqlParameter("@PmlBrokerId", principal.PmlBrokerId));

                }
                else if (principal.PmlLevelAccess == DataAccess.E_PmlLoanLevelAccess.Corporate)
                {
                    whereClauses.Add("fc.sPmlBrokerId=@PmlBrokerId");
                    parameters.Add(new SqlParameter("@PmlBrokerId", principal.PmlBrokerId));
                }
                else
                {
                    throw new UnhandledEnumException(principal.PmlLevelAccess);
                }
            }
            #endregion

            #region FirstName Filter
            if (string.IsNullOrEmpty(searchFilter.aBFirstNm) == false && searchFilter.IsDuplicateSearch == false)
            {
                whereClauses.Add("fc.aBFirstNm LIKE @aBFirstNm");
                parameters.Add(new SqlParameter("@aBFirstNm", searchFilter.aBFirstNm + "%"));
            }
            #endregion

            #region LastName Filter
            if (string.IsNullOrEmpty(searchFilter.aBLastNm) == false && searchFilter.IsDuplicateSearch == false)
            {
                whereClauses.Add("fc.aBLastNm LIKE @aBLastNm");
                parameters.Add(new SqlParameter("@aBLastNm", searchFilter.aBLastNm + "%"));
            }
            #endregion

            #region SSN Filter
            if (string.IsNullOrEmpty(searchFilter.SsnLastFour) == false && searchFilter.IsDuplicateSearch == false)
            {
                whereClauses.Add("(fc4.aBSsnLastFour = @SsnLastFour OR fc4.aCSsnLastFour = @SsnLastFour)");
                parameters.Add(new SqlParameter("@SsnLastFour", searchFilter.SsnLastFour));
            }
            #endregion

            #region sSpAddr Filter
            if (string.IsNullOrEmpty(searchFilter.sSpAddr) == false && searchFilter.IsDuplicateSearch == false)
            {
                whereClauses.Add("fc.sSpAddr LIKE @sSpAddr");
                parameters.Add(new SqlParameter("@sSpAddr", searchFilter.sSpAddr + "%"));
            }
            #endregion

            #region sLNm Filter
            if (string.IsNullOrEmpty(searchFilter.sLNm) == false)
            {
                whereClauses.Add("fc.sLNm LIKE @sLNm");
                parameters.Add(new SqlParameter("@sLNm", searchFilter.sLNm + "%"));
            }
            #endregion

            #region sStatusD Filter
            DateTime sStatusD = DateTime.Now;
            if (searchFilter.sStatusDateT == -1 || searchFilter.sStatusDateT == 0)
            {
                sStatusD = DateTime.MinValue;
            }
            else if (searchFilter.sStatusDateT == 1)
            {
                // Last 7 Days.
                sStatusD = sStatusD.AddDays(-7);
            }
            else if (searchFilter.sStatusDateT == 2)
            {
                // Last Month
                sStatusD = sStatusD.AddMonths(-1);
            }
            else if (searchFilter.sStatusDateT == 3)
            {
                // Last 2 Months
                sStatusD = sStatusD.AddMonths(-2);
            }
            else if (searchFilter.sStatusDateT == 4)
            {
                // Last 3 months.
                sStatusD = sStatusD.AddMonths(-3);
            }
            else
            {
                throw new GenericUserErrorMessageException("Unhandle sStatusDateT in PML. Value=" + searchFilter.sStatusDateT);
            }
            if (sStatusD != DateTime.MinValue)
            {
                whereClauses.Add("fc.sStatusD >= @sStatusD");
                parameters.Add(new SqlParameter("@sStatusD", sStatusD));
            }
            #endregion

            #region sStatusT Filter
            //A Status has been selected.  Load the loans with that status.
            if (searchFilter.sStatusT != -1)
            {
                whereClauses.Add("fc.sStatusT = @sStatusT");
                parameters.Add(new SqlParameter("@sStatusT", searchFilter.sStatusT));

                if (searchFilter.PortalMode == E_PortalMode.Broker)
                {
                    whereClauses.Add("(fc2.sBranchChannelT = @sBranchChannelT OR fc2.sBranchChannelT = " + (int)E_BranchChannelT.Blank + ")");
                    parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Wholesale));
                }
                else if (searchFilter.PortalMode == E_PortalMode.MiniCorrespondent)
                {
                    whereClauses.Add("sBranchChannelT = @sBranchChannelT");
                    parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Correspondent));

                    whereClauses.Add("sCorrespondentProcessT = @sCorrespondentProcessT1");
                    parameters.Add(new SqlParameter("@sCorrespondentProcessT1", E_sCorrespondentProcessT.MiniCorrespondent));
                }
                else if (searchFilter.PortalMode == E_PortalMode.Correspondent)
                {
                    whereClauses.Add("sBranchChannelT = @sBranchChannelT");
                    parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Correspondent));

                    if (searchFilter.sCorrespondentProcess.HasValue)
                    {
                        whereClauses.Add("sCorrespondentProcessT = @sCorrespondentProcessT");
                        parameters.Add(new SqlParameter("@sCorrespondentProcessT", searchFilter.sCorrespondentProcess.Value));
                    }
                }
            }
            else
            {
                //No loan status has been selected.  Load the loans for every status belonging to that channel.
                if (searchFilter.sStatusTDictionary == null)
                {
                    if (searchFilter.UseStatusT)
                    {
                        if (searchFilter.sStatusTFilters != null && searchFilter.sStatusTFilters.Count != 0)
                        {
                            whereClauses.Add("fc.sStatusT in (" +
                                string.Join(", ", searchFilter.sStatusTFilters.Select(s => ((int)s).ToString()).ToArray())
                                + ")");
                        }
                    }

                    if (searchFilter.PortalMode == E_PortalMode.Broker)
                    {
                        whereClauses.Add("(fc2.sBranchChannelT = @sBranchChannelT OR fc2.sBranchChannelT = " + (int)E_BranchChannelT.Blank + ")");
                        parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Wholesale));
                    }
                    else if (searchFilter.PortalMode == E_PortalMode.MiniCorrespondent)
                    {
                        whereClauses.Add("sBranchChannelT = @sBranchChannelT");
                        parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Correspondent));

                        whereClauses.Add("sCorrespondentProcessT = @sCorrespondentProcessT1");
                        parameters.Add(new SqlParameter("@sCorrespondentProcessT1", E_sCorrespondentProcessT.MiniCorrespondent));
                    }
                    else if (searchFilter.PortalMode == E_PortalMode.Correspondent)
                    {
                        whereClauses.Add("sBranchChannelT = @sBranchChannelT");
                        parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Correspondent));

                        if (searchFilter.sCorrespondentProcess.HasValue)
                        {
                            whereClauses.Add("sCorrespondentProcessT = @sCorrespondentProcessT");
                            parameters.Add(new SqlParameter("@sCorrespondentProcessT", searchFilter.sCorrespondentProcess.Value));
                        }
                    }
                }
                else
                {
                    string ORclause = "((";

                    whereClauses.Add("sBranchChannelT = @sBranchChannelT");
                    parameters.Add(new SqlParameter("@sBranchChannelT", E_BranchChannelT.Correspondent));
                    int uniqueIdentifier = 0;
                    foreach (E_sCorrespondentProcessT process in searchFilter.sStatusTDictionary.Keys)
                    {
                        if (!ORclause.Equals("(("))
                        {
                            ORclause += " OR (";
                        }

                        ORclause += "sCorrespondentProcessT = @sCorrespondentProcessT" + uniqueIdentifier;
                        parameters.Add(new SqlParameter("@sCorrespondentProcessT" + uniqueIdentifier, process));

                        if (searchFilter.UseStatusT)
                        {
                            ORclause += " AND " + "fc.sStatusT in (" +
                            string.Join(", ", searchFilter.sStatusTDictionary[process].Select(s => ((int)s).ToString()).ToArray())
                            + "))";
                        }
                        else
                        {
                            ORclause += ")";
                        }

                        uniqueIdentifier++;
                    }

                    ORclause += ")";
                    whereClauses.Add(ORclause);
                }
            }
            #endregion

            #region Duplicate Filter
            if (searchFilter.IsDuplicateSearch)
            {
                // OPM 32225. When looking for duplicates, we have to look across all applications for a match, not just first.
                whereClauses.Add("((a.aBFirstNm = @DuplicateFirst AND a.aBLastNm = @DuplicateLast) OR (a.aCFirstNm = @DuplicateFirst AND a.aCLastNm = @DuplicateLast))");
                parameters.Add(new SqlParameter("@DuplicateFirst", searchFilter.aBFirstNm));
                parameters.Add(new SqlParameter("@DuplicateLast", searchFilter.aBLastNm));
            }
            #endregion

            #region Append columns that require for executing engine.
            IEnumerable<string> executingEngineFieldDependencyList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(principal.BrokerId,
                WorkflowOperations.ReadLoanOrTemplate,
                WorkflowOperations.WriteLoanOrTemplate,
                WorkflowOperations.RunPmlToStep3,
                WorkflowOperations.RunPmlForRegisteredLoans,
                WorkflowOperations.RunPmlForAllLoans,
                WorkflowOperations.RequestRateLockRequiresManualLock,
                WorkflowOperations.UseOldSecurityModelForPml,
                WorkflowOperations.ProtectField,
                WorkflowOperations.WriteField
            );

            foreach (var o in executingEngineFieldDependencyList)
            {
                // 9/25/2010 dd - Only append field in loan file cache table.
                CFieldDbInfoList fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;
                CFieldDbInfo field = fieldDbInfoList.Get(o);
                if (null == field || field.IsCachedField == false)
                {
                    continue; // 9/25/2010 dd - These fields must belong to loan file cache.
                }
                string prefix = "";
                if (field.m_cacheTable == E_DbCacheTable.Loan_Cache_1)
                {
                    prefix = "fc.";
                }
                else if (field.m_cacheTable == E_DbCacheTable.Loan_Cache_2)
                {
                    prefix = "fc2.";
                }
                else if (field.m_cacheTable == E_DbCacheTable.Loan_Cache_3)
                {
                    prefix = "fc3.";
                }
                else
                {
                    continue; // Unable to handle if field is not in loan_file_cach1 and loan_file_cache_2
                }

                string fieldName = prefix + o; // We prefix the column name with fc.
                if (selectColumns.Contains(fieldName, StringComparer.OrdinalIgnoreCase) == false)
                {
                    selectColumns.Add(fieldName);
                }
            }
            #endregion

            // 9/9/2016 - 247847 - EM - We're always retrieving the maximum  number of loans, 100, 
            // since Workflow permissions can reduce the number of loans visible, and cause inconsistences when viewing different amounts of loans.
            // 9/21/2016 ejm 289924 - We sometimes want to pull more rows from the query than we want to display since we expect some loans to be hidden. The extra rows will 
            //                        hopefully ensure we get the maxRowCount number of rows to check.
            int numberToPull = topRowsToPull.HasValue ? topRowsToPull.Value : maxRowCount;
            StringBuilder sql = new StringBuilder();
            sql.Append("SELECT TOP " + numberToPull + " " + string.Join(",", selectColumns.ToArray()));
            if (!searchFilter.IsDuplicateSearch)
            {
                sql.Append(" FROM Loan_File_Cache fc with(nolock) JOIN Loan_File_Cache_2 fc2 with (nolock) on fc2.sLid = fc.sLid JOIN LOAN_FILE_CACHE_3 fc3 WITH(NOLOCK) on fc3.slid = fc.slid JOIN LOAN_FILE_CACHE_4 fc4 WITH(NOLOCK) on fc4.sLId = fc.sLId WHERE ");
            }
            else
            {
                sql.Append(" FROM Application_A a with(nolock) JOIN Loan_File_Cache fc with(nolock) on fc.sLid = a.sLId JOIN Loan_File_Cache_2 fc2 with (nolock) on fc2.sLid = a.sLid JOIN LOAN_FILE_CACHE_3 fc3 WITH(NOLOCK) on fc3.slid = a.slid JOIN LOAN_FILE_CACHE_4 fc4 WITH(NOLOCK) on fc4.sLId = a.sLId WHERE ");
            }

            sql.Append(string.Join(" AND ", whereClauses.ToArray()) + " ORDER BY sStatusD DESC");

            try
            {
                DataSet ds = new DataSet();

                DBSelectUtility.FillDataSet(principal.BrokerId, ds, sql.ToString(), null, parameters);

                DataTable dataTable = ds.Tables[0];

                int numberOfAccessibleLoans = 0;
                foreach (DataRow dataRow in dataTable.Rows)
                {
                    if(numberOfAccessibleLoans >= maxRowCount)
                    {
                        dataRow.Delete();
                        continue;
                    }

                    LoanValueEvaluator valueEvaluator = new LoanValueEvaluator((Guid)dataRow["sLId"], dataRow, executingEngineFieldDependencyList, null);
                    valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

                    bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
                    bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
                    bool canRunPmlToStep3 = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlToStep3, valueEvaluator);
                    bool canRunPmlForAllPrograms = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForAllLoans, valueEvaluator);
                    bool canRunPmlForOriginalProgram = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForRegisteredLoans, valueEvaluator);
                    bool canRequestLock = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RequestRateLockRequiresManualLock, valueEvaluator);
                    bool useOldEngine = LendingQBExecutingEngine.CanPerform(WorkflowOperations.UseOldSecurityModelForPml, valueEvaluator);
                    bool hasFieldWritePermission = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteField, valueEvaluator);

                    //	If a PML user has Write: Field-level privilege AND Run PML privilege then display the “open” link in their pipeline, within the PML search result, and in the expandable loan number link in their task tab
                    canWrite = canWrite || ((canRunPmlToStep3 || canRunPmlForAllPrograms || canRunPmlForOriginalProgram) && hasFieldWritePermission);

                    // 12/30/2010 dd - OPM 61052 - If user does not have read and write access to loan file then do not include in the search result.
                    // 4/10/2012 dd - OPM 81933 - If user has no read then remove from result.
                    if (canRead == false)
                    {
                        dataRow.Delete();
                        continue;
                    }

                    numberOfAccessibleLoans++;

                    E_sRateLockStatusT sRateLockStatusT = (E_sRateLockStatusT)dataRow["sRateLockStatusT"];
                    E_sStatusT sStatusT = (E_sStatusT)dataRow["sStatusT"];

                    decimal sNoteIRSubmitted = decimal.MinusOne;
                    if (dataRow["sNoteIRSubmitted"] != DBNull.Value)
                    {
                        sNoteIRSubmitted = (decimal)dataRow["sNoteIRSubmitted"];
                    }

                    E_sLienPosT sLienPosT = (E_sLienPosT)dataRow["sLienPosT"];
                    Guid sLpTemplateId = (Guid)dataRow["sLpTemplateId"];

                    string sRateLockStatusT_rep;
                    switch (sRateLockStatusT)
                    {
                        case E_sRateLockStatusT.NotLocked:
                            sRateLockStatusT_rep = "";
                            break;
                        case E_sRateLockStatusT.Locked:
                            sRateLockStatusT_rep = "Locked";
                            break;
                        case E_sRateLockStatusT.LockRequested:
                            sRateLockStatusT_rep = "Lock Requested";
                            break;
                        case E_sRateLockStatusT.LockSuspended:
                            sRateLockStatusT_rep = "Lock Suspended";
                            break;
                        default:
                            throw new UnhandledEnumException(sRateLockStatusT);
                    }

                    dataRow["aBSsnLastFour"] = (dataRow["aBSsnLastFour"].ToString());
                    dataRow["sRateLockStatusT_rep"] = sRateLockStatusT_rep;
                    dataRow["CanRead"] = canRead;
                    dataRow["CanWrite"] = canWrite;
                    dataRow["LoanStatus"] = CPageBase.sStatusT_map_rep(sStatusT);   //av opm 61054  Update loan status displayed in the PML pipeline

                    if (searchFilter.IsDuplicateSearch)
                    {
                        dataRow["InScope"] = IsInScope(dataRow, principal) ? 1 : 0;
                    }
                    else
                    {
                        dataRow["InScope"] = 1; // The query already filtered out out-of-scope loans
                    }

                    bool hasProgramRegistered = sLpTemplateId != Guid.Empty;
                    E_OpenLinkType newEgineLinkTypeResult = GetOpenLinkType(canRunPmlForAllPrograms, canRunPmlForOriginalProgram, canRunPmlToStep3, canWrite, canRequestLock, hasProgramRegistered);

                    E_OpenLinkType oldEngineLinkTypeResult = GetOpenLinkType(sStatusT, canWrite);
                    if (oldEngineLinkTypeResult == E_OpenLinkType.NONE)
                    {
                        oldEngineLinkTypeResult = CanRerun(sStatusT, sLienPosT, sRateLockStatusT, sNoteIRSubmitted, sLpTemplateId) ? E_OpenLinkType.USERPERMISSION : E_OpenLinkType.NONE;
                    }

                    dataRow["OpenLinkType"] = useOldEngine ? oldEngineLinkTypeResult : newEgineLinkTypeResult;
                }

                if (ConstStage.UseNewTpoPortalPipelineTaskRetrieval)
                {
                    // Calling AcceptChanges() will actually remove the rows that
                    // were marked for deletion above. This will remove the need
                    // to check the status of the rows in the code that follows
                    // and prevent operations on deleted rows that will throw
                    // exceptions. gf opm 226393
                    dataTable.AcceptChanges();

                    var loanIds = dataTable.AsEnumerable()
                        .Select(r => (Guid)r["sLId"]);

                    var taskDataSet = GetAssignedTaskDataSet(loanIds, principal.BrokerId, principal.UserId);

                    AddTaskDataToLoanData(ds, taskDataSet);
                }

                return ds;
            }
            catch (Exception exc)
            {
                Tools.LogError(sql.ToString(), exc);
                throw;
            }
        }

        /// <summary>
        /// Gets a data set of the active tasks and conditions assigned to the
        /// given user and given loan ids.
        /// </summary>
        /// <param name="loanIds">The loan ids the tasks must belong to.</param>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="userId">The user id.</param>
        /// <returns>
        /// A data set containing the number of active tasks, active conditions,
        /// due tasks, and due conditions. If loanIds is empty, returns null.
        /// </returns>
        private static DataSet GetAssignedTaskDataSet(IEnumerable<Guid> loanIds, Guid brokerId, Guid userId)
        {
            if (!loanIds.Any())
            {
                return null;
            }

            var loanIdsFormattedForQuery = loanIds.Select(
                g => string.Format("'{0}'", g.ToString()));

            var query = string.Format(
                "select loanid, taskiscondition, count(*) as numitems, " +
                "sum(case when taskduedate < @Tomorrow then 1 else 0 end) as numdue " +
                "from task with(nolock) " +
                "where loanid in ({0}) and taskassigneduserid = @Userid and taskstatus = 0 and brokerid = @sBrokerid and CondIsDeleted = 0 " +
                "group by loanid, taskiscondition",
                string.Join(",", loanIdsFormattedForQuery.ToArray()));

            var taskDataSet = new DataSet();

            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@sBrokerId", brokerId));
            parameters.Add(new SqlParameter("@UserId", userId));
            parameters.Add(new SqlParameter("@Tomorrow", DateTime.Today.AddDays(1)));

            DBSelectUtility.FillDataSet(brokerId, taskDataSet, query, null, parameters);

            return taskDataSet;
        }

        /// <summary>
        /// Merge the task and condition information into the loan data set.
        /// </summary>
        /// <param name="loanDataSet">The data set containing the loan information.</param>
        /// <param name="taskDataSet">The data set containing the task information.</param>
        private static void AddTaskDataToLoanData(DataSet loanDataSet, DataSet taskDataSet)
        {
            loanDataSet.Tables[0].Columns.Add("sNumOfActiveTasksOnly", typeof(int));
            loanDataSet.Tables[0].Columns.Add("sNumOfActiveDueTodayTasksOnly", typeof(int));
            loanDataSet.Tables[0].Columns.Add("sNumOfActiveConditionOnly", typeof(int));
            loanDataSet.Tables[0].Columns.Add("sNumOfActiveDueTodayConditionOnly", typeof(int));
            // Placeholders
            loanDataSet.Tables[0].Columns.Add("sNumOfActivePastDueTasksOnly", typeof(int));
            loanDataSet.Tables[0].Columns.Add("sNumOfActivePastDueConditionOnly", typeof(int));

            if (taskDataSet == null)
            {
                return;
            }

            var taskConditionInfoByLoanId = taskDataSet.Tables[0].AsEnumerable()
                .ToLookup(
                    r => (Guid)r["loanid"],
                    r => new { TaskIsCondition = (bool)r["taskiscondition"], NumItems = (int)r["numitems"], NumDue = (int)r["numdue"] });

            foreach (DataRow dataRow in loanDataSet.Tables[0].Rows)
            {
                var loanId = (Guid)dataRow["sLId"];

                var associatedTaskItems = taskConditionInfoByLoanId[loanId];

                foreach (var item in associatedTaskItems)
                {
                    if (item.TaskIsCondition)
                    {
                        dataRow["sNumOfActiveConditionOnly"] = item.NumItems;
                        dataRow["sNumOfActiveDueTodayConditionOnly"] = item.NumDue;
                    }
                    else
                    {
                        dataRow["sNumOfActiveTasksOnly"] = item.NumItems;
                        dataRow["sNumOfActiveDueTodayTasksOnly"] = item.NumDue;
                    }
                }
            }
        }

        public static XmlDocument SearchReturnXml(AbstractUserPrincipal principal, LendingQBSearchFilter searchFilter, int maxRowCount)
        {
            DataSet ds = Search(principal, searchFilter, maxRowCount);

            DataView dv = ds.Tables[0].DefaultView;

            XmlDocument xmlDoc = new XmlDocument();
            XmlElement rootElement = xmlDoc.CreateElement("Pml_Loans");
            xmlDoc.AppendChild(rootElement);

            foreach (DataRowView rv in dv)
            {

                XmlElement loanElement = xmlDoc.CreateElement("Loan");

                XmlElement el = xmlDoc.CreateElement("Loan_Number");
                el.InnerText = SafeConvert.ToString(rv["sLNm"]);
                loanElement.AppendChild(el);

                BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
                if (broker.IsUseNewTaskSystem)
                {
                    el = xmlDoc.CreateElement("Tasks");
                    el.InnerText = SafeConvert.ToString(rv["sActiveTasksDisplay"]);
                    loanElement.AppendChild(el);
                }

                el = xmlDoc.CreateElement("Last_Name");
                el.InnerText = SafeConvert.ToString(rv["aBLastNm"]);
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("First_Name");
                el.InnerText = SafeConvert.ToString(rv["aBFirstNm"]);
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("SSN");
                el.InnerText = SafeConvert.ToString(rv["aBSsnLastFour"]);
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Loan_Amount");
                el.InnerText = SafeConvert.ToString(string.Format("{0:C0}", rv["sLAmtCalc"]));
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Loan_Status");
                el.InnerText = SafeConvert.ToString(rv["LoanStatus"]);
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Status_Date");
                DateTime d = SafeConvert.ToDateTime(rv["sStatusD"]);
                el.InnerText = d == DateTime.MinValue ? string.Empty : d.ToShortDateString();
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Lock_Status");
                el.InnerText = SafeConvert.ToString(rv["sRateLockStatusT_rep"]);
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Lock_Exp_Date");
                d = SafeConvert.ToDateTime(rv["sRLckdExpiredD"]);
                el.InnerText = (d == DateTime.MinValue ? string.Empty : d.ToShortDateString());
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Loan_Officer");
                el.InnerText = SafeConvert.ToString(rv["sEmployeeLoanRepName"]);
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Broker_Processor");
                el.InnerText = SafeConvert.ToString(rv["sEmployeeBrokerProcessorName"]);
                loanElement.AppendChild(el);

                int sLT = SafeConvert.ToInt(rv["sLT"]);
                string sLT_rep = string.Empty;
                if (sLT == (int)E_sLT.Conventional)
                {
                    sLT_rep = "Conventional";
                }
                else if (sLT == (int)E_sLT.FHA)
                {
                    sLT_rep = "FHA";
                }
                else if (sLT == (int)E_sLT.VA)
                {
                    sLT_rep = "VA";
                }
                else if (sLT == (int)E_sLT.UsdaRural)
                {
                    sLT_rep = "USDA/Rural Housing";
                }
                else if (sLT == (int)E_sLT.Other)
                {
                    sLT_rep = "Other";
                }

                el = xmlDoc.CreateElement("Loan_Type");
                el.InnerText = sLT_rep;
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Lender_Case_Num");
                el.InnerText = SafeConvert.ToString(rv["sLenderCaseNum"]);
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Total_Loan_Amount");
                el.InnerText = SafeConvert.ToString(string.Format("{0:C0}", rv["sFinalLAmt"]));
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Correspondent_Loan_Opener");
                el.InnerText = SafeConvert.ToString(rv["sEmployeeExternalSecondaryName"]);
                loanElement.AppendChild(el);

                el = xmlDoc.CreateElement("Correspondent_Processor");
                el.InnerText = SafeConvert.ToString(rv["sEmployeeExternalPostCloserName"]);
                loanElement.AppendChild(el);

                rootElement.AppendChild(loanElement);
            }
            return xmlDoc;
        }

        private static bool CanRerun(E_sStatusT sStatusT, E_sLienPosT sLienPosT, E_sRateLockStatusT sRateLockStatusT, decimal sNoteIRSubmitted, Guid sLpTemplateId)
        {
            // 7/21/2006 dd - OPM 6165
            // 10/04/06 mf - OPM 7622 Do not clobber 'status' link if float request
            // 06/11/08 mf - OPM 18996 All clients can re-run float submissions
            bool isRequestRateFloat = sNoteIRSubmitted == 0;
            if (isRequestRateFloat &&
                sLienPosT == E_sLienPosT.First &&
                sStatusT != E_sStatusT.Loan_Open &&
                sRateLockStatusT != E_sRateLockStatusT.Locked &&
                sLpTemplateId != Guid.Empty)
            {
                return true;
            }
            return false;

        }

        private static bool IsInScope(DataRow dataRow, AbstractUserPrincipal principal)
        {
            // OPM 32225

            // 1. Corporate-level access sees all
            E_PmlLoanLevelAccess accessLevel = principal.PmlLevelAccess;
            if (accessLevel == DataAccess.E_PmlLoanLevelAccess.Corporate)
                return true;

            Guid? assignedLoId = GetGuid(dataRow["sEmployeeLoanRepId"]);
            Guid? assignedBpId = GetGuid(dataRow["sEmployeeBrokerProcessorId"]);
            Guid? assignedExternalSecondaryId = GetGuid(dataRow["sEmployeeExternalSecondaryId"]);
            Guid? assignedExternalPostCloserId = GetGuid(dataRow["sEmployeeExternalPostCloserId"]);
            Guid? supervisorId = GetGuid(dataRow["PmlExternalBrokerProcessorManagerEmployeeId"]);
            Guid? secondarySupervisorId = GetGuid(dataRow["PmlExternalSecondaryManagerEmployeeId"]);
            Guid? postCloserSupervisorId = GetGuid(dataRow["PmlExternalPostCloserManagerEmployeeId"]);

            // 2. Assigned as LO
            if (assignedLoId == principal.EmployeeId)
                return true;

            // 3. Assigned as BP
            if (assignedBpId == principal.EmployeeId)
                return true;

            // Assigned as External Secondary
            if (assignedExternalSecondaryId == principal.EmployeeId)
            {
                return true;
            }

            // Assigned as External Post-Closer
            if (assignedExternalPostCloserId == principal.EmployeeId)
            {
                return true;
            }

            // 4. Supervisor of assigned LO or BP
            if (accessLevel == E_PmlLoanLevelAccess.Supervisor && supervisorId.HasValue
                && (supervisorId == assignedLoId || supervisorId == assignedBpId))
                return true;

            // Supervisor of assigned Corr. Loan Opener or Corr. Processor
            bool isSupervisorOfExternalSecondary = secondarySupervisorId.HasValue &&
                secondarySupervisorId == principal.EmployeeId;

            bool isSupervisorOfExternalPostCloser = postCloserSupervisorId.HasValue &&
                postCloserSupervisorId == principal.EmployeeId;

            if (accessLevel == E_PmlLoanLevelAccess.Supervisor &&
                (isSupervisorOfExternalSecondary || isSupervisorOfExternalPostCloser))
            {
                return true;
            }

            return false;
        }

        private static Guid? GetGuid(object o)
        {
            if (o != DBNull.Value)
                return (Guid?)o;

            return (Guid?)null;
        }

        private static E_OpenLinkType GetOpenLinkType(E_sStatusT statusT, bool canWrite)
        {
            if (statusT == E_sStatusT.Loan_Open && canWrite)
            {
                return E_OpenLinkType.OPEN;
            }

            return E_OpenLinkType.NONE;
        }


        private static E_OpenLinkType GetOpenLinkType(bool canRunPmlAll, bool canRunPmlOri, bool canRunPmlToStep3, bool canWrite, bool canRequestRateLock, bool hasProductRegistered)
        {

            if (canRunPmlAll == false && canRunPmlOri == false && canRunPmlToStep3 == true)
            {
                return canRunPmlToStep3 ? E_OpenLinkType.OPEN : E_OpenLinkType.NONE;
            }

            if (canRunPmlAll == false && canRunPmlOri == false && canRunPmlToStep3 == false)
            {
                return E_OpenLinkType.NONE;
            }

            if (canWrite)
            {
                return E_OpenLinkType.OPEN;
            }

            if (canRequestRateLock)
            {
                return E_OpenLinkType.USERPERMISSION;
            }

            if (canRunPmlOri == true && canRunPmlAll == false)
            {
                return hasProductRegistered ? E_OpenLinkType.NONE : E_OpenLinkType.OPEN;
            }

            if (canRunPmlAll == true)
            {
                return E_OpenLinkType.OPEN;
            }

            return E_OpenLinkType.NONE;
        }

        public enum E_OpenLinkType
        {
            OPEN,
            USERPERMISSION,
            NONE
        }
    }
}
