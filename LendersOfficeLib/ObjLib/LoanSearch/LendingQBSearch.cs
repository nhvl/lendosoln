﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using LendersOffice.Common;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.Admin;
using LendersOffice.ConfigSystem;
using LendersOffice.Constants;
using System.Xml;
using LendersOffice.ConfigSystem.Operations;

namespace LendersOffice.LoanSearch
{
    public class LendingQBSearch
    {
        public static DataSet Search(AbstractUserPrincipal principal, LendingQBSearchFilter searchFilter, string orderByClause, int startRow, int endRow, out int totalNumberOfLoans)
        {

            string mainSqlFormat = @"
WITH PagedLoans AS (SELECT TEMPTABLE.*, ROW_NUMBER() OVER (ORDER BY {0}) AS RowNumber, COUNT(*) OVER () AS TotalRowCount
FROM
(
{1}
) TEMPTABLE)
SELECT * FROM PagedLoans WHERE RowNumber BETWEEN @StartRow AND @EndRow ORDER BY {0}";

            List<string> selectColumns = new List<string>()
            {
                "LOAN_FILE_CACHE.sLId",
                "0 as CanRead",
                "0 as CanWrite",
                "0 as CanRunPml",
                // Fields require for loan access info start
                "sBrokerId" ,
			    "sBranchId" ,
			    "sLNm" ,
			    "sPrimBorrowerFullNm" ,
			    "sStatusT" ,
			    "sLienPosT" ,
			    "sOpenedD" ,
			    "sNoteIRSubmitted" ,
			    "IsTemplate" ,
			    "IsValid" ,
			    "sEmployeeManagerId" ,
			    "sEmployeeUnderwriterId" ,
			    "sEmployeeLockDeskId" ,
			    "sEmployeeProcessorId" ,
			    "sEmployeeLoanOpenerId" ,
			    "sEmployeeLoanRepId" ,
			    "sEmployeeLenderAccExecId" ,
			    "sEmployeeRealEstateAgentId" ,
			    "sEmployeeCallCenterAgentId" ,
                "sEmployeeCloserId" ,
                "sEmployeeBrokerProcessorId",
			    "PmlExternalManagerEmployeeId" ,
                "sRateLockStatusT",
                "sPmlBrokerId",
                "PmlExternalBrokerProcessorManagerEmployeeId",
                "PmlExternalSecondaryManagerEmployeeId",
                "PmlExternalPostCloserManagerEmployeeId",
                // Field require for loan access info end.
                "dbo.GetLoanTypeName(sLT) AS LoanType",
			    "dbo.GetLoanStatusName(sStatusT) AS LoanStatus",
			    "sSpAddr + ', ' + sSpCity + ', ' + sSpState + ' ' + sSpZip AS PropertyAddress",
			    "aBLastNm",
			    "aBFirstNm",
                "aBSsnLastFour",
			    "sEmployeeLoanRepName",
			    "sEmployeeLenderAccExecName",
			    "sStatusD",   
                "BRANCH.BranchNm AS sBranchNm",
                "sSpAddr",
                "sLAmtCalc",
                "sMersMin",
                "sAgencyCaseNum",
                "sLenLNum",
                "sLenderCaseNum",
                "sInvestLNum",
                "sInvestorLockLoanNum",
                "sPurchaseAdviceSummaryInvLoanNm",
                "sSubservicerLoanNm",
                "sRLckdExpiredD",
                "sEstCloseD",
                "sLtvR",
                "sLRefNm"
            };

            List<SqlParameter> parameters = new List<SqlParameter>();

            SqlParameter startRowParameter = new SqlParameter("@StartRow", SqlDbType.BigInt);
            startRowParameter.Value = startRow;

            parameters.Add(startRowParameter);
            parameters.Add(new SqlParameter("@EndRow", endRow));


            List<string> whereClauses = new List<string>();

            #region System Default Filter
            whereClauses.Add("sBrokerId = @sBrokerId"); // 9/8/2010 dd - Default where clauses
            parameters.Add(new SqlParameter("@sBrokerId", principal.BrokerId));

            whereClauses.Add("IsValid = 1");

            if (searchFilter.IsTest)
            {
                whereClauses.Add("sLoanFileT=" + E_sLoanFileT.Test.ToString("D"));
            }
            else
            {
                whereClauses.Add("(sLoanFileT is NULL OR sLoanFileT=" + E_sLoanFileT.Loan.ToString("D") + ")");
            }
            #endregion

            #region IsTemplate, Lead, orLoan Filter
            whereClauses.Add("IsTemplate = @IsTemplate");
            parameters.Add(new SqlParameter("@IsTemplate", searchFilter.IsTemplate));
            {
                #region LoanorLead
                E_sStatusT[] statusList;

                if (searchFilter.IncludeLeadAndLoanStatuses)
                {
                    statusList = LoanStatuses.Union(LeadStatuses).ToArray();
                }
                else if (!searchFilter.IsLead)
                {
                    statusList = LoanStatuses.ToArray();
                }
                else
                {
                    statusList = LeadStatuses.ToArray();
                }
                string[] statusValueList = new string[statusList.Length];
                for (int i = 0; i < statusList.Length; i++)
                {
                    statusValueList[i] = statusList[i].ToString("D");
                }

                whereClauses.Add("sStatusT IN (" + string.Join(",", statusValueList) + ")");
            }


                #endregion
            #endregion

            #region User Access Level Filter
            string filterIndividualAccessClause = @" (sEmployeeManagerId = @EmployeeId
												OR sEmployeeProcessorId = @EmployeeId OR sEmployeeLoanRepId = @EmployeeId
												OR sEmployeeLoanOpenerId = @EmployeeId OR sEmployeeCallCenterAgentId = @EmployeeId
												OR sEmployeeRealEstateAgentId = @EmployeeId OR sEmployeeLenderAccExecId = @EmployeeId
												OR sEmployeeLockDeskId = @EmployeeId OR sEmployeeUnderwriterId = @EmployeeId
                                                OR sEmployeeCloserId = @EmployeeId OR sEmployeeShipperId = @EmployeeId
                                                OR sEmployeeFunderId = @EmployeeId OR sEmployeePostCloserId = @EmployeeId
                                                OR sEmployeeInsuringId = @EmployeeId OR sEmployeeCollateralAgentId = @EmployeeId
                                                OR sEmployeeDocDrawerId = @EmployeeId OR sEmployeeCreditAuditorId = @EmployeeId 
                                                OR sEmployeeDisclosureDeskId = @EmployeeId OR sEmployeeJuniorProcessorId = @EmployeeId 
                                                OR sEmployeeJuniorUnderwriterId = @EmployeeId OR sEmployeeLegalAuditorId = @EmployeeId 
                                                OR sEmployeeLoanOfficerAssistantId = @EmployeeId OR sEmployeePurchaserId = @EmployeeId 
                                                OR sEmployeeQCComplianceId = @EmployeeId OR sEmployeeSecondaryId = @EmployeeId 
                                                OR sEmployeeServicingId = @EmployeeId OR sEmployeeBrokerProcessorId = @EmployeeId
                                                OR sEmployeeExternalSecondaryId = @EmployeeId OR sEmployeeExternalPostCloserId = @EmployeeId) ";

            if (principal.HasPermission(Permission.BrokerLevelAccess))
            {
                // 9/9/2010 dd - If user select a specific branch then filter to only the selected branch.
                if (searchFilter.sBranchId != Guid.Empty)
                {
                    whereClauses.Add("sBranchId = @sBranchId");
                    parameters.Add(new SqlParameter("@sBranchId", searchFilter.sBranchId));
                }
            }
            else if (principal.HasPermission(Permission.BranchLevelAccess))
            {
                // Branch-level access gets you team access even if
                // that loan is not in your branch.  OPM 178119

                string teamWhere = "";
                if (principal.GetTeams().Count != 0)
                {
                    string teamList = "";
                    foreach (var team in principal.GetTeams())
                    {
                        teamList += (teamList.Length != 0 ? ", " : " ") + DbAccessUtils.SQLString(team.ToString());
                    }

                    teamWhere = string.Format(" ( sTeamLenderAccExecId IN {0} or sTeamLockDeskId IN {0} or sTeamUnderwriterId IN {0} or sTeamRealEstateAgentId IN {0} or sTeamCallCenterAgentId IN {0} or sTeamLoanOpenerId IN {0} or sTeamLoanRepId IN {0} or sTeamProcessorId IN {0} or sTeamManagerId IN {0} or sTeamCloserId IN {0} or sTeamShipperId IN {0} or sTeamFunderId IN {0} or sTeamPostCloserId IN {0} or sTeamInsuringId IN {0} or sTeamCollateralAgentId IN {0} or sTeamDocDrawerId IN {0} or sTeamCreditAuditorId IN {0} or sTeamDisclosureDeskId IN {0} or sTeamJuniorProcessorId IN {0} or sTeamJuniorUnderwriterId IN {0} or sTeamLegalAuditorId IN {0} or sTeamLoanOfficerAssistantId IN {0} or sTeamPurchaserId IN {0} or sTeamQCComplianceId IN {0} or sTeamSecondaryId IN {0} or sTeamServicingId IN {0} )",
                         " ( " + teamList + " ) ");
                }

                if (teamWhere.Length == 0)
                {
                    whereClauses.Add("( sBranchId = @BranchId OR " + filterIndividualAccessClause + ")");
                }
                else
                {
                    whereClauses.Add("( sBranchId = @BranchId OR " + teamWhere + " OR " + filterIndividualAccessClause + ")");
                }

                parameters.Add(new SqlParameter("@BranchId", principal.BranchId));
                parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            }
            else if (principal.HasPermission(Permission.TeamLevelAccess) && principal.GetTeams().Count != 0)
            {
                string teamList = "";
                foreach (var team in principal.GetTeams())
                {
                    teamList += (teamList.Length != 0 ? ", " : " ") + DbAccessUtils.SQLString(team.ToString());
                }

                string teamWhere = string.Format(" ( sTeamLenderAccExecId IN {0} or sTeamLockDeskId IN {0} or sTeamUnderwriterId IN {0} or sTeamRealEstateAgentId IN {0} or sTeamCallCenterAgentId IN {0} or sTeamLoanOpenerId IN {0} or sTeamLoanRepId IN {0} or sTeamProcessorId IN {0} or sTeamManagerId IN {0} or sTeamCloserId IN {0} or sTeamShipperId IN {0} or sTeamFunderId IN {0} or sTeamPostCloserId IN {0} or sTeamInsuringId IN {0} or sTeamCollateralAgentId IN {0} or sTeamDocDrawerId IN {0} or sTeamCreditAuditorId IN {0} or sTeamDisclosureDeskId IN {0} or sTeamJuniorProcessorId IN {0} or sTeamJuniorUnderwriterId IN {0} or sTeamLegalAuditorId IN {0} or sTeamLoanOfficerAssistantId IN {0} or sTeamPurchaserId IN {0} or sTeamQCComplianceId IN {0} or sTeamSecondaryId IN {0} or sTeamServicingId IN {0} )",
                     " ( " + teamList + " ) ");
                
                whereClauses.Add(" ( " + teamWhere + " OR " + filterIndividualAccessClause + " ) ");
                parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            }

            else
            {
                whereClauses.Add(filterIndividualAccessClause);
                parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            }
            #endregion

            #region sLNm, aBLastNm, aBFirstNm, sSpAddr Filters
            // 9/9/2010 dd - Each of these fields will perform LIKE filter if it end with '*'.
            KeyValuePair<string, string>[] partialMatchFilter = {
                                                                    new KeyValuePair<string, string>("sLNm", searchFilter.sLNm),
                                                                    new KeyValuePair<string, string>("aBLastNm", searchFilter.aBLastNm),
                                                                    new KeyValuePair<string, string>("aBFirstNm", searchFilter.aBFirstNm),
                                                                    new KeyValuePair<string, string>("sSpAddr", searchFilter.sSpAddr)
                                                                };

            foreach (var o in partialMatchFilter)
            {
                if (string.IsNullOrEmpty(o.Value) == false)
                {
                    if (o.Value.EndsWith("*"))
                    {
                        whereClauses.Add(o.Key + " LIKE @" + o.Key);
                        parameters.Add(new SqlParameter("@" + o.Key, "%" + o.Value.Substring(0, o.Value.Length - 1) + "%"));
                    }
                    else
                    {
                        whereClauses.Add(o.Key + " = @" + o.Key);
                        parameters.Add(new SqlParameter("@" + o.Key, o.Value));
                    }
                }
            }
            #endregion

            #region sLRefNm Filter
            if (!string.IsNullOrEmpty(searchFilter.sLRefNm))
            {
                whereClauses.Add("sLRefNm = @sLRefNm");
                parameters.Add(new SqlParameter("@sLRefNm", searchFilter.sLRefNm));
            }
            #endregion

            #region SsnLastFour Filter
            if (string.IsNullOrEmpty(searchFilter.SsnLastFour) == false)
            {
                whereClauses.Add("(aBSsnLastFour = @SsnLastFour OR aCSsnLastFour = @SsnLastFour)");
                parameters.Add(new SqlParameter("@SsnLastFour", searchFilter.SsnLastFour));
            }
            #endregion

            #region sStatusT Filter
            if (searchFilter.sStatusT != -1)
            {
                // -1 = Any status
                // -2 = Active loan
                // -3 = Inactive loan
                // -4 = Active lead
                // -5 = Inactive lead
                if (searchFilter.sStatusT >= 0)
                {
                    whereClauses.Add("sStatusT = @sStatusT");
                    parameters.Add(new SqlParameter("@sStatusT", searchFilter.sStatusT));
                }
                else
                {
                    E_sStatusT[] statusList = null;
                    if (searchFilter.sStatusT == -2)
                    {
                        statusList = new E_sStatusT[] {
                            E_sStatusT.Loan_Open, E_sStatusT.Loan_Prequal, E_sStatusT.Loan_Preapproval, E_sStatusT.Loan_Registered, E_sStatusT.Loan_Approved,
                            E_sStatusT.Loan_Docs, E_sStatusT.Loan_Funded, E_sStatusT.Loan_OnHold, E_sStatusT.Loan_LoanSubmitted, E_sStatusT.Loan_Underwriting,
                            E_sStatusT.Loan_Other, E_sStatusT.Loan_Recorded, E_sStatusT.Loan_Shipped, E_sStatusT.Loan_ClearToClose,
                            E_sStatusT.Loan_Processing, E_sStatusT.Loan_FinalUnderwriting, E_sStatusT.Loan_DocsBack, E_sStatusT.Loan_FundingConditions,
                            E_sStatusT.Loan_FinalDocs, E_sStatusT.Loan_LoanPurchased, 
                            E_sStatusT.Loan_PreProcessing,    
                            E_sStatusT.Loan_DocumentCheck,
                            E_sStatusT.Loan_DocumentCheckFailed,
                            E_sStatusT.Loan_PreUnderwriting,
                            E_sStatusT.Loan_ConditionReview,
                            E_sStatusT.Loan_PreDocQC,
                            E_sStatusT.Loan_DocsOrdered,
                            E_sStatusT.Loan_DocsDrawn,
                            E_sStatusT.Loan_InvestorConditions,       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                            E_sStatusT.Loan_InvestorConditionsSent,   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                            E_sStatusT.Loan_ReadyForSale,
                            E_sStatusT.Loan_SubmittedForPurchaseReview,
                            E_sStatusT.Loan_InPurchaseReview,
                            E_sStatusT.Loan_PrePurchaseConditions,
                            E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
                            E_sStatusT.Loan_InFinalPurchaseReview,
                            E_sStatusT.Loan_ClearToPurchase,
                            E_sStatusT.Loan_Purchased,        // don't confuse with the old E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                                    
                        };
                    }
                    else if (searchFilter.sStatusT == -3)
                    {
                        statusList = new E_sStatusT[] {
                            E_sStatusT.Loan_Suspended, E_sStatusT.Loan_Canceled, E_sStatusT.Loan_Rejected, E_sStatusT.Loan_Closed,
                            E_sStatusT.Loan_CounterOffer,
                            E_sStatusT.Loan_Withdrawn,
                            E_sStatusT.Loan_Archived   
                        };
                    }
                    else if (searchFilter.sStatusT == -4)
                    {
                        statusList = new E_sStatusT[] {
                            E_sStatusT.Lead_New, E_sStatusT.Lead_Other
                        };
                    }
                    else if (searchFilter.sStatusT == -5)
                    {
                        statusList = new E_sStatusT[] {
                            E_sStatusT.Lead_Canceled, E_sStatusT.Lead_Declined
                        };
                    }
                    else
                    {
                        throw new GenericUserErrorMessageException("Unhandle sStatusT in Search Screen. sStatusT=" + searchFilter.sStatusT);
                    }
                    string[] statusValueList = new string[statusList.Length];
                    for (int i = 0; i < statusList.Length; i++)
                    {
                        statusValueList[i] = statusList[i].ToString("D");
                    }
                    whereClauses.Add("sStatusT IN (" + string.Join(",", statusValueList) + ")");
                }
            }
            #endregion

            #region sLT Filter
            if (searchFilter.sLT >= 0)
            {
                whereClauses.Add("sLT = @sLT");
                parameters.Add(new SqlParameter("@sLT", searchFilter.sLT));
            }
            #endregion

            #region sStatusDateT Filter
            if (searchFilter.sStatusDateT != 0)
            {
                DateTime startDate = DateTime.Now.Date;
                DateTime endDate = DateTime.Now;

                if (searchFilter.sStatusDateT == 1)
                {
                    // Last 7 days.
                    startDate = startDate.AddDays(-7);
                }
                else if (searchFilter.sStatusDateT == 2)
                {
                    // This month
                    startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                }
                else if (searchFilter.sStatusDateT == 3)
                {
                    // Last month.
                    startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-1);
                    endDate = startDate.AddMonths(1).AddDays(-1);
                }
                else if (searchFilter.sStatusDateT == 4)
                {
                    // Last 2 month
                    startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-2);
                    endDate = startDate.AddMonths(2).AddDays(-1);

                }
                else if (searchFilter.sStatusDateT == 5)
                {
                    // Last 3 month
                    startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-3);
                    endDate = startDate.AddMonths(3).AddDays(-1);

                }
                else if (searchFilter.sStatusDateT == 6)
                {
                    // Year to date
                    startDate = new DateTime(DateTime.Now.Year, 1, 1);
                }
                else if (searchFilter.sStatusDateT == 7)
                {
                    // Older than 3 months.
                    startDate = SmallDateTime.MinValue;
                    endDate = DateTime.Now.AddMonths(-3).AddDays(0 - DateTime.Now.Day);
                    endDate = new DateTime(endDate.Year, endDate.Month, endDate.Day, 23, 59, 0);
                }
                else if (searchFilter.sStatusDateT == 8)
                {
                    // Previous year.
                    startDate = new DateTime(DateTime.Now.Year - 1, 1, 1);
                    endDate = new DateTime(DateTime.Now.Year - 1, 12, 31);
                }
                else if (searchFilter.sStatusDateT == 9)
                {
                    startDate = startDate.AddDays(-90);
                }
                else if (searchFilter.sStatusDateT == 10)
                {
                    // Today
                    startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                }
                else
                {
                    throw new GenericUserErrorMessageException("Unsupport sStatusDateT in Loan Search screen. sStatusDateT=" + searchFilter.sStatusDateT);
                }

                whereClauses.Add("sStatusD BETWEEN @StartDate AND @EndDate");
                parameters.Add(new SqlParameter("@StartDate", startDate));
                parameters.Add(new SqlParameter("@EndDate", endDate));
            }
            #endregion

            #region Employee Filter
            foreach (var o in searchFilter.GetEmployeeFilterList())
            {
                string fieldId = Role.GetLoanFileCacheFieldName(o.Key);
                // sEmployeeXXXXXId = EmployeeId
                whereClauses.Add(string.Format("{0} = @{0}", fieldId));
                parameters.Add(new SqlParameter("@" + fieldId, o.Value));
            }
            #endregion

            if (string.IsNullOrEmpty(searchFilter.sMersMin) == false)
            {
                whereClauses.Add("sMersMin = @sMersMin");
                parameters.Add(new SqlParameter("@sMersMin", searchFilter.sMersMin));
            }
            if (string.IsNullOrEmpty(searchFilter.sAgencyCaseNum) == false)
            {
                whereClauses.Add("sAgencyCaseNum = @sAgencyCaseNum");
                parameters.Add(new SqlParameter("@sAgencyCaseNum", searchFilter.sAgencyCaseNum));
            }
            if (string.IsNullOrEmpty(searchFilter.sLenLNum) == false)
            {
                whereClauses.Add("(sLenLNum = @sLenLNum OR sLenderCaseNum = @sLenLNum)");
                parameters.Add(new SqlParameter("@sLenLNum", searchFilter.sLenLNum));
            }
            if (string.IsNullOrEmpty(searchFilter.sInvestLNum) == false)
            {
                whereClauses.Add("(sInvestLNum = @sInvestLNum OR sInvestorLockLoanNum = @sInvestLNum OR sPurchaseAdviceSummaryInvLoanNm = @sInvestLNum)");
                parameters.Add(new SqlParameter("@sInvestLNum", searchFilter.sInvestLNum));
            }

            // 1/8/2014 gf - opm 130487
            if (string.IsNullOrEmpty(searchFilter.sSubservicerLoanNm) == false)
            {
                whereClauses.Add("sSubservicerLoanNm = @sSubservicerLoanNm");
                parameters.Add(new SqlParameter("@sSubservicerLoanNm", searchFilter.sSubservicerLoanNm));
            }

            #region Append columns that require for executing engine.
            IEnumerable<string> executingEngineFieldDependencyList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(principal.BrokerId,
    WorkflowOperations.ReadLoanOrTemplate,
    WorkflowOperations.WriteLoanOrTemplate,
    WorkflowOperations.RunPmlForAllLoans,
    WorkflowOperations.RunPmlForRegisteredLoans,
    WorkflowOperations.RunPmlToStep3,
    WorkflowOperations.WriteField,
    WorkflowOperations.ProtectField
);

            foreach (var o in executingEngineFieldDependencyList)
            {
                // 9/25/2010 dd - Only append field in loan file cache table.
                CFieldDbInfoList fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;
                CFieldDbInfo field = fieldDbInfoList.Get(o);
                if (null == field || field.IsCachedField == false)
                {
                    continue; // 9/25/2010 dd - These fields must belong to loan file cache.
                }
                if (selectColumns.Contains(o, StringComparer.OrdinalIgnoreCase) == false)
                {
                    selectColumns.Add(o);
                }
            }
            #endregion

            string sql = "SELECT " + string.Join(",", selectColumns.ToArray()) + " FROM LOAN_FILE_CACHE WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId=LOAN_FILE_CACHE_2.sLId JOIN LOAN_FILE_CACHE_3 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId=LOAN_FILE_CACHE_3.sLId JOIN LOAN_FILE_CACHE_4 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId=LOAN_FILE_CACHE_4.sLId JOIN BRANCH WITH(NOLOCK) ON LOAN_FILE_CACHE.sBranchId=BRANCH.BranchID WHERE " + string.Join(" AND ", whereClauses.ToArray());

            string mainSql = string.Format(mainSqlFormat, orderByClause, sql);

            DataSet ds = new DataSet();
            // NOTE: the only changes in this file are switching from old method call to the new method call.  The new method has already been proven to work, so I'm not going to bother creating unit tests here.
            DBSelectUtility.FillDataSet(principal.BrokerId, ds, mainSql, null, parameters);

            DataTable table = ds.Tables[0];
            //opm 61096  Exception thrown in search when user does not have access to first loan in results  av 1/3/11
            if (table.Rows.Count > 0)
            {
                totalNumberOfLoans = (int)table.Rows[0]["TotalRowCount"];
            }
            else
            {
                totalNumberOfLoans = 0;
            }

            foreach (DataRow row in table.Rows)
            {
                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator((Guid)row["sLId"], row, executingEngineFieldDependencyList, null);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

                bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
                bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
                bool canRunPml = LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForAllLoans, valueEvaluator) ||
                    LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlForRegisteredLoans, valueEvaluator) ||
                    LendingQBExecutingEngine.CanPerform(WorkflowOperations.RunPmlToStep3, valueEvaluator);
                bool hasFieldWritePermission = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteField, valueEvaluator);
                // 12/30/2010 dd - OPM 61052 - If user does not have read and write access to loan file then do not include in the search result.
                // 4/10/2012 dd - OPM 81933 - If user has no read then do not return.
                if (canRead == false)
                {
                    row.Delete();
                    continue;
                }

                if (searchFilter.sIsRemn.HasValue && searchFilter.sIsRemn.Value)
                {
                    try
                    {
                        Guid sBranchId = (Guid)row["sBranchId"];
                        if (sBranchId == new Guid("2E05BFB6-A78E-49CD-B665-13FE8FC07031") || sBranchId == new Guid("1E989430-F030-4AAC-99A6-348074001341"))
                        {
                            row.Delete();
                            continue;
                        }
                    }
                    catch (Exception e)
                    {
                        Tools.LogError(e);
                    }
                }

                row["CanRead"] = canRead;
                row["CanWrite"] = canWrite || hasFieldWritePermission;
                row["CanRunPml"] = canRunPml;
            }
            ds.AcceptChanges();  //opm 61096  Exception thrown in search when user does not have access to first loan in results  av 1/3/11
            return ds;

        }

        private static IEnumerable<E_sStatusT> LoanStatuses = 
            new E_sStatusT[] {
                            E_sStatusT.Loan_Open,
                                    E_sStatusT.Loan_Prequal,
                                    E_sStatusT.Loan_Registered,
                                    E_sStatusT.Loan_Processing,
                                    E_sStatusT.Loan_Preapproval,
                                    E_sStatusT.Loan_LoanSubmitted,
                                    E_sStatusT.Loan_Underwriting,
                                    E_sStatusT.Loan_Approved,
                                    E_sStatusT.Loan_FinalUnderwriting,
                                    E_sStatusT.Loan_ClearToClose,
                                    E_sStatusT.Loan_Docs,
                                    E_sStatusT.Loan_DocsBack,
                                    E_sStatusT.Loan_FundingConditions,
                                    E_sStatusT.Loan_Funded,
                                    E_sStatusT.Loan_Recorded,
                                    E_sStatusT.Loan_FinalDocs,
                                    E_sStatusT.Loan_Closed,
                                    E_sStatusT.Loan_Shipped,
                                    E_sStatusT.Loan_LoanPurchased,
                                    E_sStatusT.Loan_OnHold,
                                    E_sStatusT.Loan_Canceled,
                                    E_sStatusT.Loan_Rejected,
                                    E_sStatusT.Loan_Suspended,
                                    E_sStatusT.Loan_Other,
                                    E_sStatusT.Loan_PreProcessing,    // start sk 1/6/2014 opm 145251
                                    E_sStatusT.Loan_DocumentCheck,
                                    E_sStatusT.Loan_DocumentCheckFailed,
                                    E_sStatusT.Loan_PreUnderwriting,
                                    E_sStatusT.Loan_ConditionReview,
                                    E_sStatusT.Loan_PreDocQC,
                                    E_sStatusT.Loan_DocsOrdered,
                                    E_sStatusT.Loan_DocsDrawn,
                                    E_sStatusT.Loan_InvestorConditions,       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                                    E_sStatusT.Loan_InvestorConditionsSent,   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                                    E_sStatusT.Loan_ReadyForSale,
                                    E_sStatusT.Loan_SubmittedForPurchaseReview,
                                    E_sStatusT.Loan_InPurchaseReview,
                                    E_sStatusT.Loan_PrePurchaseConditions,
                                    E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
                                    E_sStatusT.Loan_InFinalPurchaseReview,
                                    E_sStatusT.Loan_ClearToPurchase,
                                    E_sStatusT.Loan_Purchased,        // don't confuse with the old E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                                    E_sStatusT.Loan_CounterOffer,
                                    E_sStatusT.Loan_Withdrawn,
                                    E_sStatusT.Loan_Archived    
                        };

        private static IEnumerable<E_sStatusT> LeadStatuses = new E_sStatusT[] {
                            E_sStatusT.Lead_New,
                            E_sStatusT.Lead_Canceled,
                            E_sStatusT.Lead_Declined,
                            E_sStatusT.Lead_Other
                        };
        

        public static DataSet ListUnassignedLoans(AbstractUserPrincipal principal)
        {
            // 9/26/2010 dd - List loans that are not assign to loan officer nor loan processor
            List<string> selectColumns = new List<string>()
            {
                "LOAN_FILE_CACHE.sLId", 
                "0 AS CanRead",
                "0 AS CanWrite",
                "dbo.GetLoanTypeName(sLT) AS LoanType",
                "dbo.GetLoanStatusName(sStatusT) AS LoanStatus",
                "sLNm",
                "sPrimBorrowerFullNm",
                "aBSsnLastFour",
                "aBFirstNm",
                "aBLastNm",
                "sStatusD",
                "BRANCH.BranchNm AS sBranchNm",
            };

            List<SqlParameter> parameters = new List<SqlParameter>();
            List<string> whereClauses = new List<string>();

            #region System Default Filter
            whereClauses.Add("sBrokerId=@sBrokerId");
            parameters.Add(new SqlParameter("@sBrokerId", principal.BrokerId));

            whereClauses.Add("IsTemplate=0");
            whereClauses.Add("IsValid=1");
            whereClauses.Add("(sLoanFileT is NULL OR sLoanFileT=" + E_sLoanFileT.Loan.ToString("D") + ")");
            #endregion

            #region Loan Status Filter
            E_sStatusT[] sStatusTList = { E_sStatusT.Loan_Open, E_sStatusT.Loan_Prequal, E_sStatusT.Loan_Preapproval };
            string sStatusListInFilter = "sStatusT IN (";
            for (int i = 0; i < sStatusTList.Length; i++)
            {
                if (i != 0)
                {
                    sStatusListInFilter += ",";
                }
                sStatusListInFilter += sStatusTList[i].ToString("D");
            }
            sStatusListInFilter += ")";

            whereClauses.Add(sStatusListInFilter);

            #endregion

            #region No Loan Officer or Broker Assign To The Loan
            whereClauses.Add("(sEmployeeProcessorID IS NULL OR sEmployeeProcessorID = '00000000-0000-0000-0000-000000000000')");
            whereClauses.Add("(sEmployeeLoanRepID IS NULL OR sEmployeeLoanRepID = '00000000-0000-0000-0000-000000000000')");
            #endregion

            #region User Access Level Filter
            string filterIndividualAccessClause = @" (sEmployeeManagerId = @EmployeeId
												OR sEmployeeProcessorId = @EmployeeId OR sEmployeeLoanRepId = @EmployeeId
												OR sEmployeeLoanOpenerId = @EmployeeId OR sEmployeeCallCenterAgentId = @EmployeeId
												OR sEmployeeRealEstateAgentId = @EmployeeId OR sEmployeeLenderAccExecId = @EmployeeId
												OR sEmployeeLockDeskId = @EmployeeId OR sEmployeeUnderwriterId = @EmployeeId
                                                OR sEmployeeCloserId = @EmployeeId OR sEmployeeShipperId = @EmployeeId
                                                OR sEmployeeFunderId = @EmployeeId OR sEmployeePostCloserId = @EmployeeId
                                                OR sEmployeeInsuringId = @EmployeeId OR sEmployeeCollateralAgentId = @EmployeeId
                                                OR sEmployeeDocDrawerId = @EmployeeId OR sEmployeeCreditAuditorId = @EmployeeId 
                                                OR sEmployeeDisclosureDeskId = @EmployeeId OR sEmployeeJuniorProcessorId = @EmployeeId 
                                                OR sEmployeeJuniorUnderwriterId = @EmployeeId OR sEmployeeLegalAuditorId = @EmployeeId 
                                                OR sEmployeeLoanOfficerAssistantId = @EmployeeId OR sEmployeePurchaserId = @EmployeeId 
                                                OR sEmployeeQCComplianceId = @EmployeeId OR sEmployeeSecondaryId = @EmployeeId 
                                                OR sEmployeeServicingId = @EmployeeId) ";

            if (principal.HasPermission(Permission.BrokerLevelAccess))
            {
                // NO-OP
            }
            else if (principal.HasPermission(Permission.BranchLevelAccess))
            {
                whereClauses.Add("( sBranchId=@BranchId OR " + filterIndividualAccessClause + ")");
                parameters.Add(new SqlParameter("@BranchId", principal.BranchId));
                parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            }
            else
            {
                whereClauses.Add(filterIndividualAccessClause);
                parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            }
            #endregion

            #region Append columns that require for executing engine.
            IEnumerable<string> executingEngineFieldDependencyList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(principal.BrokerId,
    WorkflowOperations.ReadLoanOrTemplate,
    WorkflowOperations.WriteLoanOrTemplate
);

            foreach (var o in executingEngineFieldDependencyList)
            {
                // 9/25/2010 dd - Only append field in loan file cache table.
                CFieldDbInfoList fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;
                CFieldDbInfo field = fieldDbInfoList.Get(o);
                if (null == field || field.IsCachedField == false)
                {
                    continue; // 9/25/2010 dd - These fields must belong to loan file cache.
                }
                if (selectColumns.Contains(o, StringComparer.OrdinalIgnoreCase) == false)
                {
                    selectColumns.Add(o);
                }
            }
            #endregion

            string sql = "SELECT TOP 100 " + string.Join(",", selectColumns.ToArray()) + " FROM LOAN_FILE_CACHE with(nolock) JOIN LOAN_FILE_CACHE_2 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_2.sLId JOIN LOAN_FILE_CACHE_3 WITH(NOLOCK) ON LOAN_FILE_CACHE_3.sLId = LOAN_FILE_CACHE.sLId JOIN LOAN_FILE_CACHE_4 WITH(NOLOCK) ON LOAN_FILE_CACHE_4.sLId = LOAN_FILE_CACHE.sLId JOIN BRANCH WITH(NOLOCK) ON LOAN_FILE_CACHE.sBranchId =BRANCH.BranchId WHERE "
                + string.Join(" AND ", whereClauses.ToArray()) + " ORDER BY sStatusD Desc";

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(principal.BrokerId, ds, sql, null, parameters);

            DataTable table = ds.Tables[0];


            foreach (DataRow row in table.Rows)
            {

                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator((Guid)row["sLId"], row, executingEngineFieldDependencyList, null);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));


                bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
                bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
                row["CanRead"] = canRead;
                row["CanWrite"] = canWrite;

                if (canRead == false)
                {
                    // Remove row.
                    row.Delete();
                }
            }

            return ds;
        }

        public static DataSet ListUnassignedLeads(AbstractUserPrincipal principal)
        {
            // 9/26/2010 dd - List lead that are not assign to call center agent
            List<string> selectColumns = new List<string>()
            {
                "LOAN_FILE_CACHE.sLId", 
                "0 AS CanRead",
                "0 AS CanWrite",
                "dbo.GetLoanTypeName(sLT) AS LoanType",
                "dbo.GetLoanStatusName(sStatusT) AS LoanStatus",
                "sLNm",
                "sPrimBorrowerFullNm",
                "aBSsnLastFour",
                "aBFirstNm",
                "aBLastNm",
                "sStatusD",
                "BRANCH.BranchNm AS sBranchNm",
            };

            List<SqlParameter> parameters = new List<SqlParameter>();
            List<string> whereClauses = new List<string>();

            #region System Default Filter
            whereClauses.Add("sBrokerId=@sBrokerId");
            parameters.Add(new SqlParameter("@sBrokerId", principal.BrokerId));

            whereClauses.Add("IsTemplate=0");
            whereClauses.Add("IsValid=1");
            whereClauses.Add("(sLoanFileT is NULL OR sLoanFileT=" + E_sLoanFileT.Loan.ToString("D") + ")");
            #endregion

            #region Loan Status Filter
            whereClauses.Add("sStatusT=" + E_sStatusT.Lead_New.ToString("D"));

            #endregion

            #region No CallCenterAgent To The Loan
            whereClauses.Add("(sEmployeeCallCenterAgentId IS NULL OR sEmployeeCallCenterAgentId = '00000000-0000-0000-0000-000000000000')");
            #endregion

            #region User Access Level Filter
            string filterIndividualAccessClause = @" (sEmployeeManagerId = @EmployeeId
												OR sEmployeeProcessorId = @EmployeeId OR sEmployeeLoanRepId = @EmployeeId
												OR sEmployeeLoanOpenerId = @EmployeeId OR sEmployeeCallCenterAgentId = @EmployeeId
												OR sEmployeeRealEstateAgentId = @EmployeeId OR sEmployeeLenderAccExecId = @EmployeeId
												OR sEmployeeLockDeskId = @EmployeeId OR sEmployeeUnderwriterId = @EmployeeId
                                                OR sEmployeeCloserId = @EmployeeId OR sEmployeeShipperId = @EmployeeId
                                                OR sEmployeeFunderId = @EmployeeId OR sEmployeePostCloserId = @EmployeeId
                                                OR sEmployeeInsuringId = @EmployeeId OR sEmployeeCollateralAgentId = @EmployeeId
                                                OR sEmployeeDocDrawerId = @EmployeeId OR sEmployeeCreditAuditorId = @EmployeeId 
                                                OR sEmployeeDisclosureDeskId = @EmployeeId OR sEmployeeJuniorProcessorId = @EmployeeId 
                                                OR sEmployeeJuniorUnderwriterId = @EmployeeId OR sEmployeeLegalAuditorId = @EmployeeId 
                                                OR sEmployeeLoanOfficerAssistantId = @EmployeeId OR sEmployeePurchaserId = @EmployeeId 
                                                OR sEmployeeQCComplianceId = @EmployeeId OR sEmployeeSecondaryId = @EmployeeId 
                                                OR sEmployeeServicingId = @EmployeeId) ";

            if (principal.HasPermission(Permission.BrokerLevelAccess))
            {
                // NO-OP
            }
            else if (principal.HasPermission(Permission.BranchLevelAccess))
            {
                whereClauses.Add("( sBranchId=@BranchId OR " + filterIndividualAccessClause + ")");
                parameters.Add(new SqlParameter("@BranchId", principal.BranchId));
                parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            }
            else
            {
                whereClauses.Add(filterIndividualAccessClause);
                parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            }
            #endregion

            #region Append columns that require for executing engine.
            IEnumerable<string> executingEngineFieldDependencyList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(principal.BrokerId,
    WorkflowOperations.ReadLoanOrTemplate,
    WorkflowOperations.WriteLoanOrTemplate
);

            foreach (var o in executingEngineFieldDependencyList)
            {
                // 9/25/2010 dd - Only append field in loan file cache table.
                CFieldDbInfoList fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;
                CFieldDbInfo field = fieldDbInfoList.Get(o);
                if (null == field || field.IsCachedField == false)
                {
                    continue; // 9/25/2010 dd - These fields must belong to loan file cache.
                }
                if (selectColumns.Contains(o, StringComparer.OrdinalIgnoreCase) == false)
                {
                    selectColumns.Add(o);
                }
            }
            #endregion

            string sql = "SELECT TOP 100 " + string.Join(",", selectColumns.ToArray()) + " FROM LOAN_FILE_CACHE WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_2.sLId JOIN LOAN_FILE_CACHE_3 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_3.sLId JOIN LOAN_FILE_CACHE_4 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_4.sLId JOIN BRANCH WITH(NOLOCK) ON LOAN_FILE_CACHE.sBranchId = BRANCH.BranchId WHERE "
                + string.Join(" AND ", whereClauses.ToArray()) + " ORDER BY sStatusD Desc";

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(principal.BrokerId, ds, sql, null, parameters);

            DataTable table = ds.Tables[0];


            foreach (DataRow row in table.Rows)
            {

                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator((Guid)row["sLId"], row, executingEngineFieldDependencyList, null);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

                bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
                bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
                row["CanRead"] = canRead;
                row["CanWrite"] = canWrite;

                if (canRead == false)
                {
                    // Remove row.
                    row.Delete();
                }
            }

            return ds;
        }

        public static DataSet ListMyLeads(AbstractUserPrincipal principal)
        {
            // 9/26/2010 dd - List lead that are not assign to call center agent
            List<string> selectColumns = new List<string>()
            {
                "LOAN_FILE_CACHE.sLId", 
                "0 AS CanRead",
                "0 AS CanWrite",
                "dbo.GetLoanTypeName(sLT) AS LoanType",
                "sLNm",
                "sPrimBorrowerFullNm",
                "aBSsnLastFour",
                "aBFirstNm",
                "aBLastNm",
                "sStatusD",
                "aBHPhone",
            };

            List<SqlParameter> parameters = new List<SqlParameter>();
            List<string> whereClauses = new List<string>();

            #region System Default Filter
            whereClauses.Add("sBrokerId=@sBrokerId");
            parameters.Add(new SqlParameter("@sBrokerId", principal.BrokerId));

            whereClauses.Add("IsTemplate=0");
            whereClauses.Add("IsValid=1");
            whereClauses.Add("(sLoanFileT is NULL OR sLoanFileT=" + E_sLoanFileT.Loan.ToString("D") + ")");
            #endregion

            #region Loan Status Filter
            whereClauses.Add("sStatusT=" + E_sStatusT.Lead_New.ToString("D"));

            #endregion

            #region User Access Level Filter
            string filterIndividualAccessClause = @" (sEmployeeManagerId = @EmployeeId
												OR sEmployeeProcessorId = @EmployeeId OR sEmployeeLoanRepId = @EmployeeId
												OR sEmployeeLoanOpenerId = @EmployeeId OR sEmployeeCallCenterAgentId = @EmployeeId
												OR sEmployeeRealEstateAgentId = @EmployeeId OR sEmployeeLenderAccExecId = @EmployeeId
												OR sEmployeeLockDeskId = @EmployeeId OR sEmployeeUnderwriterId = @EmployeeId
                                                OR sEmployeeCloserId = @EmployeeId OR sEmployeeShipperId = @EmployeeId
                                                OR sEmployeeFunderId = @EmployeeId OR sEmployeePostCloserId = @EmployeeId
                                                OR sEmployeeInsuringId = @EmployeeId OR sEmployeeCollateralAgentId = @EmployeeId
                                                OR sEmployeeDocDrawerId = @EmployeeId OR sEmployeeCreditAuditorId = @EmployeeId 
                                                OR sEmployeeDisclosureDeskId = @EmployeeId OR sEmployeeJuniorProcessorId = @EmployeeId 
                                                OR sEmployeeJuniorUnderwriterId = @EmployeeId OR sEmployeeLegalAuditorId = @EmployeeId 
                                                OR sEmployeeLoanOfficerAssistantId = @EmployeeId OR sEmployeePurchaserId = @EmployeeId 
                                                OR sEmployeeQCComplianceId = @EmployeeId OR sEmployeeSecondaryId = @EmployeeId 
                                                OR sEmployeeServicingId = @EmployeeId) ";

            whereClauses.Add(filterIndividualAccessClause);
            parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            #endregion

            #region Append columns that require for executing engine.
            IEnumerable<string> executingEngineFieldDependencyList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(principal.BrokerId,
    WorkflowOperations.ReadLoanOrTemplate,
    WorkflowOperations.WriteLoanOrTemplate
);

            foreach (var o in executingEngineFieldDependencyList)
            {
                // 9/25/2010 dd - Only append field in loan file cache table.
                CFieldDbInfoList fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;
                CFieldDbInfo field = fieldDbInfoList.Get(o);
                if (null == field || field.IsCachedField == false)
                {
                    continue; // 9/25/2010 dd - These fields must belong to loan file cache.
                }
                if (selectColumns.Contains(o, StringComparer.OrdinalIgnoreCase) == false)
                {
                    selectColumns.Add(o);
                }
            }
            #endregion

            string sql = "SELECT TOP 100 " + string.Join(",", selectColumns.ToArray()) + " FROM LOAN_FILE_CACHE WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_2.sLId JOIN LOAN_FILE_CACHE_3 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_3.sLId JOIN LOAN_FILE_CACHE_4 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_4.sLId JOIN BRANCH WITH(NOLOCK) ON LOAN_FILE_CACHE.sBranchId=BRANCH.BranchId WHERE "
                + string.Join(" AND ", whereClauses.ToArray()) + " ORDER BY sStatusD Desc";

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(principal.BrokerId, ds, sql, null, parameters);

            DataTable table = ds.Tables[0];


            foreach (DataRow row in table.Rows)
            {

                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator((Guid)row["sLId"], row, executingEngineFieldDependencyList, null);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));

                bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
                bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
                row["CanRead"] = canRead;
                row["CanWrite"] = canWrite;

                if (canRead == false)
                {
                    // Remove row.
                    row.Delete();
                }
            }

            return ds;

        }

        public static string ListRecentLoansForMobileInXmlFormat(AbstractUserPrincipal principal)
        {
            // 11/8/2011 dd - This method will be use in our mobile application to return most recent active loans.

            // 12/28/2011 dd - Specs update - Only list active loans that are assign to user.
            List<string> selectColumns = new List<string>()
            {
                "LOAN_FILE_CACHE.sLId",
                "0 AS CanRead",
                "0 AS CanWrite",
                "dbo.GetLoanStatusName(sStatusT) AS LoanStatus",
                "sLNm",
                "aBFirstNm",
                "aBLastNm",
                "sRLckdExpiredD",
                "sRateLockStatusT",
                "sLAmtCalc",
                "sLatestActivityType",
                "COALESCE(sLatestActivityDateForMobileApp, sStatusD) AS sLastActivityD", 
            };

            List<SqlParameter> parameters = new List<SqlParameter>();
            List<string> whereClauses = new List<string>();

            #region System Default Filter
            whereClauses.Add("sBrokerId=@sBrokerId");
            parameters.Add(new SqlParameter("@sBrokerId", principal.BrokerId));

            whereClauses.Add("IsTemplate=0");
            whereClauses.Add("IsValid=1");
            whereClauses.Add("(sLoanFileT is NULL OR sLoanFileT=" + E_sLoanFileT.Loan.ToString("D") + ")");
            #endregion


            #region User Access Level Filter
            string filterIndividualAccessClause = @" (sEmployeeManagerId = @EmployeeId
												OR sEmployeeProcessorId = @EmployeeId OR sEmployeeLoanRepId = @EmployeeId
												OR sEmployeeLoanOpenerId = @EmployeeId OR sEmployeeCallCenterAgentId = @EmployeeId
												OR sEmployeeRealEstateAgentId = @EmployeeId OR sEmployeeLenderAccExecId = @EmployeeId
												OR sEmployeeLockDeskId = @EmployeeId OR sEmployeeUnderwriterId = @EmployeeId
                                                OR sEmployeeCloserId = @EmployeeId OR sEmployeeShipperId = @EmployeeId
                                                OR sEmployeeFunderId = @EmployeeId OR sEmployeePostCloserId = @EmployeeId
                                                OR sEmployeeInsuringId = @EmployeeId OR sEmployeeCollateralAgentId = @EmployeeId
                                                OR sEmployeeDocDrawerId = @EmployeeId OR sEmployeeCreditAuditorId = @EmployeeId 
                                                OR sEmployeeDisclosureDeskId = @EmployeeId OR sEmployeeJuniorProcessorId = @EmployeeId 
                                                OR sEmployeeJuniorUnderwriterId = @EmployeeId OR sEmployeeLegalAuditorId = @EmployeeId 
                                                OR sEmployeeLoanOfficerAssistantId = @EmployeeId OR sEmployeePurchaserId = @EmployeeId 
                                                OR sEmployeeQCComplianceId = @EmployeeId OR sEmployeeSecondaryId = @EmployeeId 
                                                OR sEmployeeServicingId = @EmployeeId) ";

            whereClauses.Add(filterIndividualAccessClause);
            parameters.Add(new SqlParameter("@EmployeeId", principal.EmployeeId));
            #endregion

            #region Active Loan Filter
            E_sStatusT[] statusList = new E_sStatusT[] {
                            E_sStatusT.Loan_Open, E_sStatusT.Loan_Prequal, E_sStatusT.Loan_Preapproval, E_sStatusT.Loan_Registered, E_sStatusT.Loan_Approved,
                            E_sStatusT.Loan_Docs, E_sStatusT.Loan_OnHold, E_sStatusT.Loan_LoanSubmitted, E_sStatusT.Loan_Underwriting, E_sStatusT.Loan_Other,
                            E_sStatusT.Loan_ClearToClose, E_sStatusT.Loan_Processing,
                            E_sStatusT.Loan_FinalUnderwriting, E_sStatusT.Loan_DocsBack, E_sStatusT.Loan_FundingConditions, E_sStatusT.Loan_FinalDocs,
                            E_sStatusT.Loan_PreProcessing,    
                            E_sStatusT.Loan_DocumentCheck,
                            E_sStatusT.Loan_DocumentCheckFailed,
                            E_sStatusT.Loan_PreUnderwriting,
                            E_sStatusT.Loan_ConditionReview,
                            E_sStatusT.Loan_PreDocQC,
                            E_sStatusT.Loan_DocsOrdered,
                            E_sStatusT.Loan_DocsDrawn,
                            E_sStatusT.Loan_InvestorConditions,       // this is "promoting the date to a status". tied to sSuspendedByInvestorD
                            E_sStatusT.Loan_InvestorConditionsSent,   // this is "promoting the date to a status". tied to sCondSentToInvestorD
                            E_sStatusT.Loan_ReadyForSale,
                            E_sStatusT.Loan_SubmittedForPurchaseReview,
                            E_sStatusT.Loan_InPurchaseReview,
                            E_sStatusT.Loan_PrePurchaseConditions,
                            E_sStatusT.Loan_SubmittedForFinalPurchaseReview,
                            E_sStatusT.Loan_InFinalPurchaseReview,
                            E_sStatusT.Loan_ClearToPurchase,
                            E_sStatusT.Loan_Purchased,        // don't confuse with the old E_sStatusT.Loan_LoanPurchased, which is now Loan Sold in the UI.
                        };

            string[] statusValueList = new string[statusList.Length];
            for (int i = 0; i < statusList.Length; i++)
            {
                statusValueList[i] = statusList[i].ToString("D");
            }
            whereClauses.Add("sStatusT IN (" + string.Join(",", statusValueList) + ")");
            #endregion

            #region Append columns that require for executing engine.
            IEnumerable<string> executingEngineFieldDependencyList = LendingQBExecutingEngine.GetDependencyFieldsByOperation(principal.BrokerId,
    WorkflowOperations.ReadLoanOrTemplate,
    WorkflowOperations.WriteLoanOrTemplate
);

            foreach (var o in executingEngineFieldDependencyList)
            {
                // 9/25/2010 dd - Only append field in loan file cache table.
                CFieldDbInfoList fieldDbInfoList = CFieldDbInfoList.TheOnlyInstance;
                CFieldDbInfo field = fieldDbInfoList.Get(o);
                if (null == field || field.IsCachedField == false)
                {
                    continue; // 9/25/2010 dd - These fields must belong to loan file cache.
                }
                if (selectColumns.Contains(o, StringComparer.OrdinalIgnoreCase) == false)
                {
                    selectColumns.Add(o);
                }
            }
            #endregion

            string sql = "SELECT TOP 50 " + string.Join(",", selectColumns.ToArray()) + " FROM LOAN_FILE_CACHE WITH(NOLOCK) JOIN LOAN_FILE_CACHE_2 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_2.sLId JOIN LOAN_FILE_CACHE_3 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_3.sLId JOIN LOAN_FILE_CACHE_4 WITH(NOLOCK) ON LOAN_FILE_CACHE.sLId = LOAN_FILE_CACHE_4.sLId JOIN BRANCH WITH(NOLOCK) ON LOAN_FILE_CACHE.sBranchId=BRANCH.BranchId WHERE "
                + string.Join(" AND ", whereClauses.ToArray()) + " ORDER BY sLastActivityD Desc";

            DataSet ds = new DataSet();
            DBSelectUtility.FillDataSet(principal.BrokerId, ds, sql, null, parameters);

            DataTable table = ds.Tables[0];

            foreach (DataRow row in table.Rows)
            {

                LoanValueEvaluator valueEvaluator = new LoanValueEvaluator((Guid)row["sLId"], row, executingEngineFieldDependencyList, null);
                valueEvaluator.SetEvaluatingPrincipal(ExecutingEnginePrincipal.CreateFrom(principal));


                bool canRead = LendingQBExecutingEngine.CanPerform(WorkflowOperations.ReadLoanOrTemplate, valueEvaluator);
                bool canWrite = LendingQBExecutingEngine.CanPerform(WorkflowOperations.WriteLoanOrTemplate, valueEvaluator);
                row["CanRead"] = canRead;
                row["CanWrite"] = canWrite;

                if (canRead == false)
                {
                    // Remove row.
                    row.Delete();
                }
            }
            table.AcceptChanges();
            // Generate Xml
            XmlDocument document = new XmlDocument();

            XmlElement root = document.CreateElement("list");
            document.AppendChild(root);
            foreach (DataRow row in table.Rows)
            {

                XmlElement item = document.CreateElement("loan");
                root.AppendChild(item);

                item.SetAttribute("sLNm", row.GetSafeValue("sLNm", string.Empty));
                item.SetAttribute("LoanStatus", row.GetSafeValue("LoanStatus", string.Empty));
                item.SetAttribute("sLAmtCalc", row.GetSafeValue("sLAmtCalc", 0M).ToString());
                DateTime sRLckdExpiredD = row.GetSafeValue("sRLckdExpiredD", DateTime.MinValue);

                item.SetAttribute("sRLckdExpiredD", sRLckdExpiredD == DateTime.MinValue ? "" : sRLckdExpiredD.ToString("MM/dd/yyyy"));
                item.SetAttribute("aBFirstNm", row.GetSafeValue("aBFirstNm", string.Empty));
                item.SetAttribute("aBLastNm", row.GetSafeValue("aBLastNm", string.Empty));
                item.SetAttribute("sLastActivityD", row.GetSafeValue("sLastActivityD", DateTime.MinValue).ToString("MM/dd/yyyy"));
                item.SetAttribute("sLatestActivityType", row.GetSafeValue("sLatestActivityType", 0).ToString());
                item.SetAttribute("sRateLockStatusT", row.GetSafeValue("sRateLockStatusT", 0).ToString());
            }
            return document.OuterXml;

        }


    }
}
