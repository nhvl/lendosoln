﻿// <copyright file="ApplicantDetails.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.LoanSearch.Basic
{
    using System;

    /// <summary>
    /// Borrower information for a given application.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Loan File Field Names.")]
    public class ApplicantDetails
    {
        /// <summary>
        /// Gets or sets the application identifier.
        /// <value>The application identifier.</value>
        /// </summary>
        /// <value>The application Id.</value>
        public Guid aAppId { get; set; }

        /// <summary>
        /// Gets or sets the borrowers first name on the application.
        /// </summary>
        /// <value>The first name of the borrower.</value>
        public string aBFirstNm { get; set; }

        /// <summary>
        /// Gets or sets the borrowers last name on the application.
        /// </summary>
        /// <value>The last name of the borrower.</value>
        public string aBLastNm { get; set; }

        /// <summary>
        /// Gets or sets the coborrowers first name on the application.
        /// </summary>
        /// <value>The coborrowers first name.</value>
        public string aCFirstNm { get; set; }

        /// <summary>
        /// Gets or sets the coborrowers last name on the application.
        /// </summary>
        /// <value>The coborrowers last name.</value>
        public string aCLastNm { get; set; }

        /// <summary>
        /// Gets or sets the borrowers social security number.
        /// </summary>
        /// <value>The borrowers social security number.</value>
        public string aBSsn { get; set; }

        /// <summary>
        /// Gets or sets the coborrowers social security number.
        /// </summary>
        /// <value>The coborrowers social security number.</value>
        public string aCSsn { get; set; }

        /// <summary>
        /// Gets the last four digits of the borrower's social security number.
        /// </summary>
        /// <value>The last four digits of the borrowers social security number.</value>
        public string aBSsnLastFour => LqbGrammar.DataTypes.SocialSecurityNumber.Create(this.aBSsn)?.LastFour ?? string.Empty;

        /// <summary>
        /// Gets the last four digits of the coborrower's social security number.
        /// </summary>
        /// <value>The last four digits of the coborrowers social security number.</value>
        public string aCSsnLastFour => LqbGrammar.DataTypes.SocialSecurityNumber.Create(this.aCSsn)?.LastFour ?? string.Empty;

        /// <summary>
        /// Gets or sets the email of the borrower.
        /// </summary>
        public string aBEmail { get; set; }

        /// <summary>
        /// Gets or sets the email of the coborrower.
        /// </summary>
        public string aCEmail { get; set; }

        /// <summary>
        /// Gets or sets the cellphone of the borrower.
        /// </summary>
        public string aBCellphone { get; set; }

        /// <summary>
        /// Gets or sets the cellphone of the coborrower.
        /// </summary>
        public string aCCellphone { get; set; }

        /// <summary>
        /// Gets or sets the home phone of the borrower.
        /// </summary>
        public string aBHPhone { get; set; }

        /// <summary>
        /// Gets or sets the home phone of the coborrower.
        /// </summary>
        public string aCHPhone { get; set; }

        /// <summary>
        /// Gets or sets the work phone of the borrower.
        /// </summary>
        public string aBBusPhone { get; set; }

        /// <summary>
        /// Gets or sets the work phone of the coborrower.
        /// </summary>
        public string aCBusPhone { get; set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return string.Format("aAppId:{0} aBNm:{1} {2} aCNm: {3} {4} ", this.aAppId, this.aBFirstNm, this.aBLastNm, this.aCFirstNm, this.aCLastNm);
        }
    }
}
