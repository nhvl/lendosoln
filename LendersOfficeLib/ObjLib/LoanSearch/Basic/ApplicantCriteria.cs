﻿// <copyright file="ApplicantCriteria.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.LoanSearch.Basic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;

    /// <summary>
    /// A POCO with the applicant details used for filtering loans.
    /// </summary>
    public class ApplicantCriteria
    {
        /// <summary>
        /// Gets or sets the first name of the applicant to filter by.
        /// </summary>
        /// <value>The first name of the applicant.</value>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name of the applicant to filter by.
        /// </summary>
        /// <value>The last name of the applicant.</value>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the social security number of the applicant to filter by.
        /// </summary>
        /// <value>The SSN of the applicant.</value>
        public string SSN { get; set; }

        /// <summary>
        /// Gets or sets the last 4 digits of the SSN of the applicant.
        /// </summary>
        public string Last4SSN { get; set; }

        /// <summary>
        /// Gets or sets the email of the applicant.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the phone number of the applicant.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets a value indicating whether the given firstname, lastname and social match the 
        /// current app data. All the criteria that is null is ignored and considered to match regardless
        /// of the data being compared to.
        /// </summary>
        /// <param name="firstName">The firstname to compare to.</param>
        /// <param name="lastName">The last name to compare to.</param>
        /// <param name="social">The social to compare to.</param>
        /// <param name="email">The email to compare to.</param>
        /// <param name="phoneNumbers">The possible phone numbers to compare to.</param>
        /// <returns>True if the data matches the current criteria.</returns>
        public bool DoesDataMatch(string firstName, string lastName, string social, string email, IEnumerable<string> phoneNumbers)
        {
            var ssn = LqbGrammar.DataTypes.SocialSecurityNumber.Create(social);
            var firstNameMatch = this.DoesStringMatch(this.FirstName, firstName);
            var lastNameMatch = this.DoesStringMatch(this.LastName, lastName);
            var emailMatch = this.DoesStringMatch(this.Email, email);
            var phoneMatch = phoneNumbers.CoalesceWithEmpty().Any((phoneToCheck) => this.DoPhoneNumbersMatch(this.Phone, phoneToCheck));
            var ssnMatch = this.DoesStringMatch(this.SSN, social);
            var last4SsnMatch = this.DoesStringMatch(this.Last4SSN, ssn.HasValue ? ssn.Value.LastFour : null);
            return firstNameMatch &&
                    lastNameMatch &&
                    emailMatch &&
                    phoneMatch &&
                    ssnMatch &&
                    last4SsnMatch;
        }

        /// <summary>
        /// Gets a value indicating whether any of the data for the applicant criteria was filled out.
        /// </summary>
        /// <returns>True if the name or ssn was filled out.</returns>
        public bool HasData()
        {
            return !string.IsNullOrEmpty(this.FirstName) || !string.IsNullOrEmpty(this.LastName) || !string.IsNullOrEmpty(this.SSN) ||
                !string.IsNullOrEmpty(this.Last4SSN) || !string.IsNullOrEmpty(this.Email) || !string.IsNullOrEmpty(this.Phone);
        }

        /// <summary>
        /// Gets a value indicating whether the criteria is undefined and the data matches it.
        /// </summary>
        /// <param name="criteria">The data to compare to.</param>
        /// <param name="data">The current data.</param>
        /// <returns>True if criteria is undefined or true if data matches criteria.</returns>
        private bool DoesStringMatch(string criteria, string data)
        {
            if (string.IsNullOrEmpty(criteria))
            {
                return true; 
            }

            return criteria.Equals(data, System.StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Checks phone numbers to see if they match, regardless of format.
        /// </summary>
        /// <param name="phoneCriteria">The phone number in the search criteria.</param>
        /// <param name="phoneToCheck">The phone number from the app.</param>
        /// <returns>True if matched, false otherwise.</returns>
        private bool DoPhoneNumbersMatch(string phoneCriteria, string phoneToCheck)
        {
            if (string.IsNullOrEmpty(phoneCriteria))
            {
                return true;
            }

            string formattedPhoneCriteria;
            string formattedPhoneToCheck;
            return DataAccess.Tools.ToPhoneFormat(phoneCriteria, out formattedPhoneCriteria) && DataAccess.Tools.ToPhoneFormat(phoneToCheck, out formattedPhoneToCheck) &&
                   this.DoesStringMatch(formattedPhoneCriteria, formattedPhoneToCheck);
        }
    }
}
