﻿// <copyright file="LoanCriteria.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.LoanSearch.Basic
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Specifies filtering criteria for loan searching.
    /// </summary>
    public class LoanCriteria
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoanCriteria"/> class.
        /// </summary>
        public LoanCriteria()
        {
            this.ApplicantCriterias = new List<ApplicantCriteria>();
        }

        /// <summary>
        /// Gets or sets the property address.
        /// </summary>
        /// <value>The property address.</value>
        public string PropertyAddress { get; set; }

        /// <summary>
        /// Gets or sets the property city.
        /// </summary>
        /// <value>The property city.</value>
        public string PropertyCity { get; set; }

        /// <summary>
        /// Gets or sets the property state.
        /// </summary>
        /// <value>The property state.</value>
        public string PropertyState { get; set; }

        /// <summary>
        /// Gets or sets the property zip code.
        /// </summary>
        /// <value>The property zip code.</value>
        public string PropertyZip { get; set; }

        /// <summary>
        /// Gets or sets the date that a loan's status must have been modified by.
        /// </summary>
        public DateTime? StatusModifiedByDate { get; set; }

        /// <summary>
        /// Gets or sets the loan app filtering criteria.
        /// </summary>
        /// <value>The application filtering criteria.</value>
        public IList<ApplicantCriteria> ApplicantCriterias { get; set; }

        /// <summary>
        /// Calculates whether there is any loan data to filter by.
        /// </summary>
        /// <returns>True if any of the property data is not null or empty.</returns>
        public bool IsLoanDataSpecified()
        {
            if (!string.IsNullOrEmpty(this.PropertyAddress))
            {
                return true;
            }

            if (!string.IsNullOrEmpty(this.PropertyCity))
            {
                return true;
            }

            if (!string.IsNullOrEmpty(this.PropertyState))
            {
                return true;
            }

            if (!string.IsNullOrEmpty(this.PropertyZip))
            {
                return true;
            }

            if (this.StatusModifiedByDate.HasValue)
            {
                return true;
            }

            return false;
        }
    }
}
