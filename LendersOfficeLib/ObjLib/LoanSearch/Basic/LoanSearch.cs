﻿// <copyright file="LoanSearch.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.LoanSearch.Basic
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Text;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Drivers.Encryption;
    using LendersOffice.ObjLib.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Finds loans given LoXml. The results are client specific and bypasses workflow. 
    /// Only actual loans are returned. No loan templates or sandbox files.
    /// </summary>
    public class LoanSearch
    {
        /// <summary>
        /// The database connection to use for the queries.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The database connection to use for the queries.
        /// </summary>
        private DbConnectionInfo connectionInfo;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoanSearch"/> class.
        /// </summary>
        /// <param name="brokerId">The broker identifier.</param>
        public LoanSearch(Guid brokerId)
        {
            this.brokerId = brokerId;
            this.connectionInfo = DbConnectionInfo.GetConnectionInfo(this.brokerId);
        }

        /// <summary>
        /// Searches for loans that match the given loan criteria.
        /// </summary>
        /// <param name="criteria">Criteria that the result loans have to match.</param>
        /// <returns>A set of loans that matches the given criteria.</returns>
        public IEnumerable<LoanDetails> Search(LoanCriteria criteria)
        {
            using (LendersOffice.HttpModule.PerformanceMonitorItem.Time("LoanSearch::Search"))
            {
                if (criteria.IsLoanDataSpecified())
                {
                    return this.FindLoansByLoanDataAndApplicants(criteria);
                }

                if (criteria.ApplicantCriterias == null || criteria.ApplicantCriterias.Count == 0)
                {
                    return new LoanDetails[0];
                }

                return this.FindLoansByApplicants(criteria.ApplicantCriterias);
            }
        }

        /// <summary>
        /// Searches the data store for a list of loans that has applications that match applicant information only.
        /// </summary>
        /// <param name="criterias">The applicants the loans must match.</param>
        /// <returns>A set of loans and the matching applications.</returns>
        private IEnumerable<LoanDetails> FindLoansByApplicants(IList<ApplicantCriteria> criterias)
        {
            string sql = this.BuildAppQuery(criterias);
            Tools.LogInfo("LoanSearchApplicantQuery " + sql);

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@BrokerId", this.brokerId));
            for (int i = 0; i < criterias.Count; i++)
            {
                ApplicantCriteria criteria = criterias[i];

                if (!string.IsNullOrEmpty(criteria.FirstName))
                {
                    parameters.Add(new SqlParameter("@FirstName" + i, criteria.FirstName));
                }

                if (!string.IsNullOrEmpty(criteria.LastName))
                {
                    parameters.Add(new SqlParameter("@LastName" + i, criteria.LastName));
                }
            }

            // NOTE: The somwhat awkward above construction of sql is correct
            Dictionary<Guid, LoanDetails> loansById = new Dictionary<Guid, LoanDetails>();
            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
                Action<IDataReader> readHandler = delegate(IDataReader reader)
                {
                    while (reader.Read())
                    {
                        records.Add(reader.ToDictionary());
                    }
                };

                DBSelectUtility.ProcessDBData(this.connectionInfo, sql, null, parameters, readHandler);

                loansById = this.ParseAndPopulateLoanDetails(records);
            }
            catch (LqbGrammar.Exceptions.LqbException e) when (e.InnerException is SqlException)
            {
                throw new CBaseException("Internal error.", e.InnerException);
            }
            catch (SqlException e)
            {
                throw new CBaseException("Internal error.", e);
            }

            LoanSearchUtilities.FilterLoansByAppData(criterias, loansById);

            return loansById.Values;
        }

        /// <summary>
        /// Generates a sql statement to search for applications that match the given applicant criteria.
        /// The parameterized parameters are @FirstName{index}, @LastName{index}. Index is zero based.
        /// </summary>
        /// <param name="criterias">The set of applicants that is being searched for.</param>
        /// <returns>A parameterized sql statement.</returns>
        private string BuildAppQuery(IList<ApplicantCriteria> criterias)
        {
            StringBuilder sb = new StringBuilder("select ");
            sb.Append("c.sLid, a.aAppId, a.aBFirstNm, a.aBLastNm, a.aCFirstNm, a.aCLastNm, a.aBSsnEncrypted, a.aCSsnEncrypted, a.aBEmail, a.aCEmail, a.aBHPhone, a.aCHPhone, a.aBBusPhone, a.aCBusPhone, b.aBCellphone, b.aCCellphone,");
            sb.Append("c.slnm, c.sLPurposeT, c.sStatusT, c.sStatusD, c.sLienPosT, c.sSpAddr, c.sSpCity, c.sSpState, c.sSpZip, c.sSpCounty, c2.sGseSpT,");
            sb.Append("c4.sEncryptionKey, c4.sEncryptionMigrationVersion, c4.sPrimaryAppId ");
            sb.Append("from application_a a join application_b b on a.aAppId=b.aAppId ");
            sb.Append("join loan_file_cache c on a.slid = c.slid ");
            sb.Append("join loan_file_cache_4 c4 on a.slid = c4.slid join loan_file_cache_2 c2 on c4.slid=c2.slid ");
            sb.Append("where c.sbrokerid = @BrokerId and  c.istemplate = 0 and c.isvalid = 1 and c.sloanfilet = 0");

            bool hasBorrowerClauses = false;
            for (int i = 0; i < criterias.Count; i++)
            {
                ApplicantCriteria criteria = criterias[i];
                if (i != 0)
                {
                    sb.Append("OR ");
                }

                if (!string.IsNullOrEmpty(criteria.FirstName) || !string.IsNullOrEmpty(criteria.LastName))
                {
                    if (!hasBorrowerClauses)
                    {
                        sb.Append(" and ( ");
                        hasBorrowerClauses = true;
                    }

                    sb.Append("( ");
                    sb.Append(this.GenerateBorrowerClause(E_BorrowerModeT.Borrower, criteria, i));
                    sb.Append("OR ");
                    sb.Append(this.GenerateBorrowerClause(E_BorrowerModeT.Coborrower, criteria, i));
                    sb.Append(") ");
                }
            }

            if (hasBorrowerClauses)
            {
                sb.Append(")");
            }

            return sb.ToString();    
        }

        /// <summary>
        /// Generates a sql clause to compare the applicant data from the criteria to the borrower fields specified by
        /// the mode.
        /// </summary>
        /// <param name="mode">The borrower mode that is used to specify which fields to check against.</param>
        /// <param name="criteria">The criteria the app must match.</param>
        /// <param name="index">The current criteria index.</param>
        /// <returns>A where clause. For example (aBFirstNm = @FirstName AND aBLastName = @LastName).</returns>
        private string GenerateBorrowerClause(E_BorrowerModeT mode, ApplicantCriteria criteria, int index)
        {
            char borrowerIdentifier;

            switch (mode)
            {
                case E_BorrowerModeT.Borrower:
                    borrowerIdentifier = 'B';
                    break;
                case E_BorrowerModeT.Coborrower:
                    borrowerIdentifier = 'C';
                    break;
                default:
                    throw new UnhandledEnumException(mode);
            }

            StringBuilder sb = new StringBuilder("(");
            bool needAnd = false;

            if (!string.IsNullOrEmpty(criteria.FirstName))
            {
                sb.AppendFormat("a.a{0}FirstNm = @FirstName{1} ", borrowerIdentifier, index);
                needAnd = true;
            }

            if (!string.IsNullOrEmpty(criteria.LastName))
            {
                if (needAnd)
                {
                    sb.AppendFormat("AND ");
                }

                sb.AppendFormat("a.a{0}LastNm = @LastName{1} ", borrowerIdentifier, index);
                needAnd = true;
            }

            sb.Append(") ");

            return sb.ToString();
        }

        /// <summary>
        /// Gets a set of loans where the loan data and applicant data matches the loan criteria.
        /// data. 
        /// </summary>
        /// <param name="criteria">The loan criteria containing the property data to filter by.</param>
        /// <returns>Null if there is no property data specified or a dictionary matching the results.</returns>
        private IEnumerable<LoanDetails> FindLoansByLoanDataAndApplicants(LoanCriteria criteria)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("@BrokerId", this.brokerId));

            if (!string.IsNullOrEmpty(criteria.PropertyAddress))
            {
                parameters.Add(new SqlParameter("@sSpAddr", criteria.PropertyAddress));
            }

            if (!string.IsNullOrEmpty(criteria.PropertyCity))
            {
                parameters.Add(new SqlParameter("@sSpCity", criteria.PropertyCity));
            }

            if (!string.IsNullOrEmpty(criteria.PropertyState))
            {
                parameters.Add(new SqlParameter("@sSpState", criteria.PropertyState));
            }

            if (!string.IsNullOrEmpty(criteria.PropertyZip))
            {
                parameters.Add(new SqlParameter("@sSpZip", criteria.PropertyZip));
            }

            if (criteria.StatusModifiedByDate.HasValue)
            {
                parameters.Add(new SqlParameter("@sStatusD", criteria.StatusModifiedByDate.Value));
            }

            Dictionary<Guid, LoanDetails> loansById = new Dictionary<Guid, LoanDetails>();

            try
            {
                List<Dictionary<string, object>> records = new List<Dictionary<string, object>>();
                using (IDataReader reader = StoredProcedureHelper.ExecuteReader(this.connectionInfo, "LoanSearchByPropertyAddress", parameters))
                {
                    while (reader.Read())
                    {
                        records.Add(reader.ToDictionary());
                    }
                }

                loansById = this.ParseAndPopulateLoanDetails(records);
            }
            catch (SqlException e)
            {
                throw new CBaseException("Internal error.", e);
            }

            return LoanSearchUtilities.FilterLoansByAppData(criteria.ApplicantCriterias, loansById);
        }

        /// <summary>
        /// Parses data from reader and fills a dictionary of loan IDs to loan details.
        /// </summary>
        /// <param name="records">A list of dictionaries representing data records from the DB.</param>
        /// <returns>A dictionary of loan IDs to loan details.</returns>
        private Dictionary<Guid, LoanDetails> ParseAndPopulateLoanDetails(IEnumerable<Dictionary<string, object>> records)
        {
            Dictionary<Guid, LoanDetails> loansById = new Dictionary<Guid, LoanDetails>();

            foreach (IReadOnlyDictionary<string, object> record in records)
            {
                LoanDetails details;
                Guid loanId = (Guid)record["sLId"];
                if (!loansById.TryGetValue(loanId, out details))
                {
                    details = new LoanDetails();
                    details.sLienPosT = (E_sLienPosT)record["sLienPosT"];
                    details.sLId = (Guid)record["sLId"];
                    details.sLNm = (string)record["sLNm"];
                    details.sLPurposeT = (E_sLPurposeT)record["sLPurposeT"];
                    details.sStatusT = (E_sStatusT)record["sStatusT"];
                    details.sStatusD = record["sStatusD"] as DateTime?;
                    details.sGseSpT = (E_sGseSpT)record["sGseSpT"];
                    details.sSpAddr = (string)record["sSpAddr"];
                    details.sSpCity = (string)record["sSpCity"];
                    details.sSpState = (string)record["sSpState"];
                    details.sSpZip = (string)record["sSpZip"];
                    details.sSpCounty = (string)record["sSpCounty"];
                    details.sPrimaryAppId = record.GetValueOrNull("sPrimaryAppId") as Guid?;
                    details.Applicants = new List<ApplicantDetails>();
                    loansById.Add(loanId, details);
                }

                ApplicantDetails appDetails = LoanSearchUtilities.ParseApplicantDetails(record);

                details.Applicants.Add(appDetails);
            }

            return loansById;
        }
    }
}
