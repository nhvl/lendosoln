﻿namespace LendersOffice.ObjLib.LoanSearch.Basic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using LendersOffice.Common;
    using LendersOffice.ObjLib.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Utilities class for LoanSearch and DuplicateFinder.
    /// </summary>
    public static class LoanSearchUtilities
    {
        /// <summary>
        /// Parses data from reader and fills a ApplicantDetails object.
        /// </summary>
        /// <param name="record">A dictionary representing a data record from the DB.</param>
        /// <returns>An ApplicantDetails object.</returns>
        public static ApplicantDetails ParseApplicantDetails(IReadOnlyDictionary<string, object> record)
        {
            ApplicantDetails appDetails = new ApplicantDetails();
            appDetails.aAppId = (Guid)record["aAppId"];
            appDetails.aBFirstNm = (string)record["aBFirstNm"];
            appDetails.aCFirstNm = (string)record["aCFirstNm"];
            appDetails.aBLastNm = (string)record["aBLastNm"];
            appDetails.aCLastNm = (string)record["aCLastNm"];
            appDetails.aBEmail = record.GetValueOrNull("aBEmail") as string;
            appDetails.aCEmail = record.GetValueOrNull("aCEmail") as string;
            appDetails.aBCellphone = record.GetValueOrNull("aBCellphone") as string;
            appDetails.aCCellphone = record.GetValueOrNull("aCCellphone") as string;
            appDetails.aBHPhone = record.GetValueOrNull("aBHPhone") as string;
            appDetails.aCHPhone = record.GetValueOrNull("aCHPhone") as string;
            appDetails.aBBusPhone = record.GetValueOrNull("aBBusPhone") as string;
            appDetails.aCBusPhone = record.GetValueOrNull("aCBusPhone") as string;

            EncryptionMigrationVersion encryptionMigrationVersion = EncryptionMigrationVersion.Unmigrated;
            object migrationVersionDb = record["sEncryptionMigrationVersion"];
            if (migrationVersionDb != DBNull.Value)
            {
                encryptionMigrationVersion = (EncryptionMigrationVersion)Enum.ToObject(typeof(EncryptionMigrationVersion), migrationVersionDb);
            }

            byte[] borrowerSsnEncrypted = record["aBSsnEncrypted"] as byte[];
            byte[] coborrowerSsnEncrypted = record["aCSsnEncrypted"] as byte[];

            if (borrowerSsnEncrypted != null || coborrowerSsnEncrypted != null)
            {
                Guid keyId = (Guid)record["sEncryptionKey"];
                EncryptionKeyIdentifier? encryptionKey = EncryptionKeyIdentifier.Create(keyId);

                appDetails.aBSsn = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKey.Value, borrowerSsnEncrypted);
                appDetails.aCSsn = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKey.Value, coborrowerSsnEncrypted);
            }

            return appDetails;
        }

    /// <summary>
    /// Matches each application in the criteria with the given results. If the result app is not present in the criteria it is removed.
    /// If there are no apps in the criteria the result set is returned as is.  The matching is based on borrower OR coborrower.
    /// </summary>
    /// <param name="criteria">The criteria for the mapping.</param>
    /// <param name="loanDetails">The matching data.</param>
    /// <returns>LoanDetails with only matching apps.</returns>
    public static IEnumerable<LoanDetails> FilterLoansByAppData(IList<ApplicantCriteria> criteria, Dictionary<Guid, LoanDetails> loanDetails)
        {
            if (criteria == null || criteria.Count == 0)
            {
                return loanDetails.Values;
            }

            List<Guid> loansToRemove = new List<Guid>();

            foreach (LoanDetails loan in loanDetails.Values)
            {
                for (int i = loan.Applicants.Count - 1; i >= 0; i--)
                {
                    bool hasMatch = false;
                    ApplicantDetails app = loan.Applicants[i];

                    foreach (ApplicantCriteria appCritera in criteria)
                    {
                        if (appCritera.DoesDataMatch(app.aBFirstNm, app.aBLastNm, app.aBSsn, app.aBEmail, new List<string>() { app.aBCellphone, app.aBHPhone, app.aBBusPhone })
                            || appCritera.DoesDataMatch(app.aCFirstNm, app.aCLastNm, app.aCSsn, app.aCEmail, new List<string>() { app.aCCellphone, app.aCHPhone, app.aCBusPhone }))
                        {
                            hasMatch = true;
                            break;
                        }
                    }

                    if (!hasMatch)
                    {
                        loan.Applicants.RemoveAt(i);
                    }
                }

                if (loan.Applicants.Count == 0)
                {
                    loansToRemove.Add(loan.sLId);
                }
            }

            loansToRemove.ForEach(sLId => loanDetails.Remove(sLId));
            return loanDetails.Values;
        }
    }
}
