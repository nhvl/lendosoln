﻿// <copyright file="LoanDetails.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   1/02/2016 9:58:19 AM 
// </summary>
namespace LendersOffice.ObjLib.LoanSearch.Basic
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Details about a loan file.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Loan File Field Names.")]
    public class LoanDetails
    {
        /// <summary>
        /// Gets or sets the loan files loan number.
        /// </summary>
        /// <value>The loan number.</value>
        public string sLNm { get; set; }

        /// <summary>
        /// Gets or sets the loan files purpose.
        /// </summary>
        /// <value>The purpose of the loan file.</value>
        public E_sLPurposeT sLPurposeT { get; set; }

        /// <summary>
        /// Gets or sets the loan files status.
        /// </summary>
        /// <value>The loan files status.</value>
        public E_sStatusT sStatusT { get; set; }

        /// <summary>
        /// Gets or sets the loan files status date.
        /// </summary>
        /// <value>The loan files status date.</value>
        public DateTime? sStatusD { get; set; }

        /// <summary>
        /// Gets or sets the lien position of the file.
        /// </summary>
        /// <value>Gets or sets the lien position.</value>
        public E_sLienPosT sLienPosT { get; set; }

        /// <summary>
        /// Gets or sets the property type.
        /// </summary>
        public E_sGseSpT sGseSpT { get; set; }

        /// <summary>
        /// Gets or sets the property street address.
        /// </summary>
        public string sSpAddr { get; set; }

        /// <summary>
        /// Gets or sets the property city.
        /// </summary>
        public string sSpCity { get; set; }

        /// <summary>
        /// Gets or sets the property state.
        /// </summary>
        public string sSpState { get; set; }

        /// <summary>
        /// Gets or sets the property zip.
        /// </summary>
        public string sSpZip { get; set;  }

        /// <summary>
        /// Gets or sets the property county.
        /// </summary>
        public string sSpCounty { get; set; }

        /// <summary>
        /// Gets or sets the id of the primary app.
        /// </summary>
        public Guid? sPrimaryAppId { get; set; }

        /// <summary>
        /// Gets or sets a list of applicants for the loan search result.
        /// </summary>
        /// <value>A list of applicants for the given search loan search result.</value>
        public IList<ApplicantDetails> Applicants { get; set; }

        /// <summary>
        /// Gets or sets the loan identifier.
        /// </summary>
        /// <value>The identifier for the loan.</value>
        public Guid sLId { get; set; }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            var sb = new System.Text.StringBuilder();
            sb.AppendFormat("Loan ID:{0} sLNm:{1} Apps: ", this.sLId, this.sLNm);
            foreach (ApplicantDetails details in this.Applicants)
            {
                sb.AppendFormat("{0} ", details);
            }

            return sb.ToString();
        }
    }
}
