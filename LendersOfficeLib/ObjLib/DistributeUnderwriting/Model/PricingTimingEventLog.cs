﻿namespace LendersOffice.DistributeUnderwriting.Model
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Represents individual pricing timing event log.
    /// This class primary purpose is to serialize and deserialize to json. Do not add any logics to this.
    /// </summary>
    [DataContract]
    public class PricingTimingEventLog
    {
        /// <summary>
        /// Gets or sets the time in UTC that the log message created.
        /// </summary>
        /// <remarks>
        /// 2/9/2017 - dd - We currently store log in ElasticSearch and utilize Kibana for quick query.
        /// ElasticSearch/Kibana expect the date time in UTC and in ISO 8601 format yyyy-MM-ddTHH:mm:ss.fffZ
        /// The current default JSON.net serializer serialize datetime to a 6 digits milliseconds and could break the Kibana.
        /// Therefore to avoid implement detail or behavior of 3rd party (JSON.net) I will convert the string manually.
        /// In code please use: log.TimeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");.
        /// </remarks>
        /// <value>The time in UTC that the log message created.</value>
        [DataMember(Name = "timestamp")]
        public string Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the server name.
        /// </summary>
        /// <value>The server name.</value>
        [DataMember(Name = "server")]
        public string Server { get; set; }

        /// <summary>
        /// Gets or sets the customer client code.
        /// </summary>
        /// <value>The customer client code.</value>
        [DataMember(Name = "customer_code")]
        public string CustomerCode { get; set; }

        /// <summary>
        /// Gets or sets the user login name.
        /// </summary>
        /// <value>The user login name.</value>
        [DataMember(Name = "user")]
        public string User { get; set; }

        /// <summary>
        /// Gets or sets the loan id.
        /// </summary>
        /// <value>The loan id.</value>
        [DataMember(Name = "slid")]
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets timing type.
        /// </summary>
        /// <value>The timing type.</value>
        [DataMember(Name = "timing_type")]
        public string TimingType { get; set; }

        /// <summary>
        /// Gets or sets pricing type.
        /// </summary>
        /// <value>The pricing type.</value>
        [DataMember(Name = "pricing_type")]
        public string PricingType { get; set; }

        /// <summary>
        /// Gets or sets the id of the pricing batch request.
        /// </summary>
        /// <value>The id of the pricing batch request.</value>
        [DataMember(Name = "request_batch_id")]
        public Guid RequestBatchId { get; set; }

        /// <summary>
        /// Gets or sets the id of the pricing request.
        /// </summary>
        /// <value>The id of the pricing request.</value>
        [DataMember(Name = "request_id")]
        public string RequestId { get; set; }

        /// <summary>
        /// Gets or sets the number of products.
        /// </summary>
        /// <value>The number of products.</value>
        [DataMember(Name = "num_of_products")]
        public int NumberOfProducts { get; set; }

        /// <summary>
        /// Gets or sets the total time in milliseconds.
        /// </summary>
        /// <value>The total time in milliseconds.</value>
        [DataMember(Name = "duration")]
        public int Duration { get; set; }

        /// <summary>
        /// Gets or sets the duration of time request wait in queue before pick up.
        /// </summary>
        /// <value>The duration of time request wait in queue before pick up.</value>
        [DataMember(Name = "s_wait_in_queue")]
        public int ServerWaitInQueue { get; set; }

        /// <summary>
        /// Gets or sets the duration of pricing execution.
        /// </summary>
        /// <value>Duration of pricing execution.</value>
        [DataMember(Name = "s_pricing_execute")]
        public int ServerPricingExecute { get; set; }

        /// <summary>
        /// Gets or sets the duration from user click on a button to receive request.
        /// </summary>
        /// <value>The duration from user click on a button to receive request.</value>
        [DataMember(Name = "c_wait_result")]
        public int ClientWaitForResult { get; set; }

        /// <summary>
        /// Gets or sets the duration for render result on client side.
        /// </summary>
        /// <value>The duration for render result on client side.</value>
        [DataMember(Name = "c_render_result")]
        public int ClientRenderResult { get; set; }

        /// <summary>
        /// Gets or sets the detail message.
        /// </summary>
        /// <value>The detail message.</value>
        [DataMember(Name = "details")]
        public string Details { get; set; }

        /// <summary>
        /// Gets or sets the correlation id for log.
        /// </summary>
        /// <value>The correlation id for log.</value>
        [DataMember(Name = "correlation")]
        public string Correlation { get; set; }
    }
}