using DataAccess;
using LendersOffice.Common;
namespace LendersOffice.DistributeUnderwriting
{
	public class DistributeTimeoutException : CBaseException
	{
        public override string EmailSubjectCode 
        {
            get { return "LPE_RESULT_TIMEOUT"; }
        }
		public DistributeTimeoutException() : base(JsMessages.LpeResultsTimeout, JsMessages.LpeResultsTimeout)
		{
		}
	}
}
