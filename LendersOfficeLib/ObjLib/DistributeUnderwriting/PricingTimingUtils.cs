﻿namespace LendersOffice.DistributeUnderwriting
{
    using System;
    using System.Messaging;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.DistributeUnderwriting.Model;

    /// <summary>
    /// Helper class for pricing timing log.
    /// </summary>
    public static class PricingTimingUtils
    {
        /// <summary>
        /// Create an empty pricing timing event. Populate the timestamp and server.
        /// </summary>
        /// <returns>A timing log.</returns>
        public static PricingTimingEventLog CreateEventLog()
        {
            PricingTimingEventLog eventLog = new PricingTimingEventLog();
            eventLog.Timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ");
            eventLog.Server = ConstAppDavid.ServerName;
            eventLog.Correlation = Tools.ConvertGuidToFriendlyBase64(Tools.GetLogCorrelationId());
            return eventLog;
        }

        /// <summary>
        /// Record event log to persistent log storage.
        /// </summary>
        /// <param name="eventLog">The timing log.</param>
        public static void Record(PricingTimingEventLog eventLog)
        {
            if (eventLog != null)
            {
                if (string.IsNullOrEmpty(ConstStage.MSMQ_ElasticSearch) == false)
                {
                    DateTime dt;

                    if (DateTime.TryParse(eventLog.Timestamp, out dt) == false)
                    {
                        Tools.LogError($"Unable to parse timestamp. Value=[{eventLog.Timestamp}]");
                        return;
                    }

                    dt = dt.ToUniversalTime();

                    // 2/10/2017 - dd - Search in ES will be case sensitive for text field. Therefore I normalize the case for these 3 special fields.
                    eventLog.Server = eventLog.Server?.ToUpper();
                    eventLog.CustomerCode = eventLog.CustomerCode?.ToUpper();
                    eventLog.User = eventLog.User?.ToLower();

                    string indexName = $"lqb_pricing_timing-{dt.Year}.{dt.DayOfYear / 7}/timing";

                    try
                    {
                        Drivers.Gateways.MessageQueueHelper.SendJSON(ConstStage.MSMQ_ElasticSearch, indexName, eventLog);
                    }
                    catch (MessageQueueException exc)
                    {
                        Tools.LogErrorWithCriticalTracking(exc);

                        // Continue to process.
                    }
                }
            }
        }
    }
}