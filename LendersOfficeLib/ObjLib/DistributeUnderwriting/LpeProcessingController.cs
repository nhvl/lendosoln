/// Author: David Dao

using System;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading;
using DataAccess;
using LendersOffice.Common;

namespace LendersOffice.DistributeUnderwriting
{
	public class LpeProcessingController
	{
        private const int NumOfRetries = 5;

        internal static LpeTaskMessage GetTask() 
        {
            int index = 0;

            //Add a corelation id for pricing to tie logs together
            Tools.ResetLogCorrelationId();

            while (index < NumOfRetries)
            {
                try
                {
                    using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "LPE_REQUEST_GetNextTask"))
                    {
                        if (reader.Read())
                        {
                            Guid lpeRequestBatchId = (Guid)reader["LpeRequestBatchId"];
                            string lperequestxml = reader["LpeRequestXmlContent"].ToString();
                            LpeTaskMessage task = (LpeTaskMessage)SerializationHelper.XmlDeserialize(lperequestxml, typeof(LpeTaskMessage));
                            int lpeRequestCurrentPartInBatch = (int)reader["LpeRequestCurrentPartInBatch"];
                            int lpeRequestNumberOfRequestsInBatch = (int)reader["LpeRequestNumberOfRequestsInBatch"];
                            DateTime lpeRequestExpirationDate = (DateTime)reader["LpeRequestExpiredDate"];

                            DateTime dbTime = (DateTime)reader["CurrentDBTime"];

                            task.Header.CurrentPartInBatch = lpeRequestCurrentPartInBatch;
                            task.Header.NumberOfRequestsInBatch = lpeRequestNumberOfRequestsInBatch;
                            task.Header.ExpirationDate = lpeRequestExpirationDate;
                            task.Header.ReceivedFromDB = dbTime;

                            task.Header.TempMessageLabel = new MessageLabel(lpeRequestBatchId, lpeRequestCurrentPartInBatch, lpeRequestNumberOfRequestsInBatch, lpeRequestExpirationDate);
                            task.Header.LpeRequestIntId = (long)reader["LpeRequestIntId"];

                            if (task.RequestData.LogId != Guid.Empty)
                            {
                                Tools.SetCorrelationId(task.RequestData.LogId);
                            }

                            return task;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
                catch (Exception exc)
                {
                    Tools.LogWarning(exc + ". Retries GetTask() #" + index);
                    index++;
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(121); // Sleep for 121 ms. Why 121? No idea. dd.
                }
            }

            return null;
        }

        internal static void UpdateTaskStatus(long lpeRequestIntId, string status) 
        {
            int index = 0;

            while (index < NumOfRetries)
            {
                try
                {
                    if (status.Equals("Successful", StringComparison.OrdinalIgnoreCase))
                    {
                        SqlParameter[] parameters = { new SqlParameter("@LpeRequestIntId", lpeRequestIntId) };
                        StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_RemoveRequestByIntId", 2, parameters);

                    }
                    else if (status.Equals("Error", StringComparison.OrdinalIgnoreCase))
                    {
                        StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_REQUEST_RemoveProcessingD", 2,
                            new SqlParameter("@LpeRequestIntId", lpeRequestIntId));
                    }
                    return;
                }
                catch (Exception exc)
                {
                    Tools.LogWarning(exc + ". Retries UpdateTaskStatus() #" + index);
                    index++;
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(121);
                }
            }
        }
	}
}
