namespace LendersOffice.DistributeUnderwriting
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Xsl;
    using CommonProjectLib.Common.Lib;
    using DataAccess;
    using EDocs;
    using LendersOffice.Admin;
    using LendersOffice.Audit;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.ObjLib.ConsumerPortal;
    using LendersOffice.ObjLib.Relationships;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Reminders;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;
    using LendersOfficeApp.newlos.Template;
    using Conversions;

    public class ApplyLoanWorkerUnit
	{
        public XmlDocument Process(LpeTaskMessage task) 
        {
            XmlDocument results = new XmlDocument();
            XmlElement elRoot = results.CreateElement("results");
            results.AppendChild(elRoot);

            switch (task.Header.LpeTaskT) 
            {
                case E_LpeTaskT.SubmitRate:
                    ApplyLoan(task);
                    break;
                case E_LpeTaskT.SubmitRate80_20:
                    SubmitRate80_20(task);
                    break;
                case E_LpeTaskT.TemporaryAcceptFirstLoan:
                    TemporaryAcceptFirstLoan(task);
                    break;
                case E_LpeTaskT.VerifyFirstLoanProgram:
                    VerifyLoanProgram(task, elRoot);
                    break;
                case E_LpeTaskT.PerformInternalPricingAction:
                    PerformInternalPricingAction(task);
                    break;
                default:
                    throw new UnhandledEnumException(task.Header.LpeTaskT);

            }
            return results;
        }

        private void PerformInternalPricingAction(LpeTaskMessage task)
        {
            Guid userID = task.Header.UserId;
            Guid employeeID = task.Header.EmployeeId;
            Guid brokerID = task.Header.BrokerId;
            Guid loanID = task.RequestData.LoanId;
            Guid productID = (Guid)task.RequestData.ProductIdList[0];
            string userName = task.Header.UserName;
            string requestedRate = task.Options.SelectedRateFormat;
            bool isRequestingRateLock = task.RequestData.IsRequestingRateLock;

            bool isPerformAddCondition = task.Options.IsAddConditionOption; 
            bool isPerformSetPricing = task.Options.IsSetRateLockPricingOption;

            CPageData dataLoan = null;
            CApplicantPrice product = null;
            decimal? parRate = null;
            
            if (isPerformSetPricing && task.Options.PricingModeT == E_sPricingModeT.InternalBrokerPricing)
            {
                dataLoan = new CSubmitSingleLoanData(loanID);
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.sPricingModeT = task.Options.PricingModeT;

                XDocument parRateDebugInfo;
                XmlDocument parRateExtendedDebugInfo;
                decimal calculatedParRate;

                product = RetrieveLoanProductAndMergeGroupParRate(brokerID, task, dataLoan, productID, out calculatedParRate, out parRateDebugInfo, out parRateExtendedDebugInfo);
                parRate = calculatedParRate;
            }
            else
            {
                product = RetrieveLoanProduct(brokerID, loanID, productID, E_sLienQualifyModeT.ThisLoan, task.Options, task.Header.LpeRequestBatchId);
            }
            
            if (product == null)
            {
                CBaseException exc = new CBaseException("INELIGIBLE:Program is no longer available", "INELIGIBLE:Program is no longer available");
                exc.IsEmailDeveloper = false;
                throw exc;
            }
            if (!((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal).HasPermission(Permission.CanApplyForIneligibleLoanPrograms))
            {
                // 12/9/2005 dd - Only throw this exception if user doesn't have permission to apply for ineligible loan programs.
                if (product.Status == E_EvalStatus.Eval_Ineligible)
                {
                    string userErrorMsg = "<ul>";
                    foreach (Toolbox.CStipulation s in product.DenialReasons)
                    {
                        userErrorMsg += "<li>" + Utilities.SafeJsString(s.Description);
                    }
                    userErrorMsg += "</ul>";

                    CBaseException exc = new CBaseException("INELIGIBLE:" + userErrorMsg, "INELIGIBLE:" + userErrorMsg);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }
            }

            // Add Condition from Loan Program.
            if (isPerformAddCondition)
            {
                ProcessStipulations(brokerID, loanID, product, string.Empty, userName, employeeID);
            }

            if (isPerformSetPricing)
            {
                if (dataLoan == null)
                {
                    dataLoan = new CSubmitSingleLoanData(loanID);
                    dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                    dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                    dataLoan.sPricingModeT = task.Options.PricingModeT;
                }

                // OPM 188838 08.07.14 mf. Get the adjustment codes applicable to the selected option.
                bool isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
                        && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                bool isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                    dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

                string compMode;
                CApplicantRateOption rateOption = FindSelectedRateOption(product.ApplicantRateOptions, requestedRate,
                    dataLoan.BrokerDB, isLenderPaidOriginatorCompAndAdditionToFees, isBorrowerPaidOriginatorCompAndIncludedInFees, out compMode, dataLoan.sPricingModeT == E_sPricingModeT.InternalInvestorPricing );

                if (null == rateOption)
                {
                    string devMsg = BuildDevErrorMsgRateOptionNotFound(product, requestedRate, task);
                    throw new CBaseException(ErrorMessages.UnableToFindRateOption, devMsg);
                }
                
                dataLoan.SetInternalRateLockPricing(
                    product, 
                    task.Options.SelectedRate, 
                    task.Options.SelectedFee, 
                    rateOption.QRate, 
                    rateOption.Margin, 
                    rateOption.TeaserRate,
                    parRate, 
                    rateOption.PerOptionCodes);

                dataLoan.Save();
            }
        }
        private CApplicantRateOption FindSelectedRateOption(CApplicantRateOption[] list, decimal requestedRate, decimal requestedPoint,
            BrokerDB currentBroker, bool isLenderPaidOriginatorCompAndAdditionToFees, bool isBorrowerPaidOriginatorCompAndIncludedInFees)
        {
            string pointRepMode = string.Empty; ;
            if (currentBroker.IsUsePriceIncludingCompensationInPricingResult && isLenderPaidOriginatorCompAndAdditionToFees)
            {
                // OPM 64253
                pointRepMode = "PointIncludingOriginatorComp";
            }

            foreach (var rateOption in list)
            {
                decimal expectedPoint = rateOption.Point;
                decimal expectedPoint_ = rateOption.Point_; // OPM 148176. If lender does not round, sometimes we are mixing rounded with unrounded.  For quick hotfix, check either.  Better change is deeper in engine.

                if (pointRepMode == "PointIncludingOriginatorComp")
                {
                    decimal.TryParse(rateOption.PointIncludingOriginatorComp_rep, out expectedPoint);
                }
                else if (pointRepMode == "PointLessOriginatorComp")
                {
                    decimal.TryParse(rateOption.PointLessOriginatorComp_rep, out expectedPoint);
                }

                if (rateOption.Rate == requestedRate && ( expectedPoint == requestedPoint || expectedPoint_ == requestedPoint) )
                {
                    return rateOption;
                }
            }
            return null;
        }

        private CApplicantRateOption FindSelectedRateOption(CApplicantRateOption[] list, string requestedRate,
            BrokerDB currentBroker, bool isLenderPaidOriginatorCompAndAdditionToFees, bool isBorrowerPaidOriginatorCompAndIncludedInFees, out string pointRepMode, bool isInvestorPricing) 
        {
            // 7/13/2005 dd - Requested rate is in following format. {rate}:{point}:{rateid}
            pointRepMode = string.Empty;
            string[] parts = requestedRate.Split(':');
            if (parts.Length < 3)
                return null;

            string rate = parts[0];
            string point = parts[1];
            string id = parts[2];


            // OPM 64253.  If lender has a special option turned on, we want to 
            // display the point value as adjusted for originator comp.
            //string pointRepMode = string.Empty; ;
            if (isInvestorPricing)
            {
                // OPM 188838. No-op.  Investor pricing is exclusive of LO Comp.
            }
            else if (currentBroker.IsUsePriceIncludingCompensationInPricingResult && isLenderPaidOriginatorCompAndAdditionToFees)
            {
                // OPM 64253
                pointRepMode = "PointIncludingOriginatorComp";
            }

            foreach (CApplicantRateOption rateOption in list) 
            {
                string expectedPoint = rateOption.Point_rep;

                if (pointRepMode == "PointIncludingOriginatorComp")
                {
                    expectedPoint = rateOption.PointIncludingOriginatorComp_rep;
                }
                else if (pointRepMode == "PointLessOriginatorComp")
                {
                    expectedPoint = rateOption.PointLessOriginatorComp_rep;
                }

                if (string.IsNullOrEmpty(id) || id == "0.000,0.000")
                {
                    if (rateOption.Rate_rep == rate && expectedPoint == point)
                    {
                        rateOption.IsRateChange = false;
                        return rateOption;
                    }
                }
                else if (rateOption.RateOptionId == id) 
                {


                    if (rateOption.Rate_rep != rate || expectedPoint != point) 
                    {
                        rateOption.IsRateChange = true;
                        rateOption.OldRate = rate;
                        rateOption.OldPoint = point;
                    } 
                    else 
                    {
                        rateOption.IsRateChange = false;
                    }

                    return rateOption;
                }
            }


            return null;
        }


        // Call this one when your rate was not found. Return null if there was not a rate change.
        private CApplicantRateOption FindSelectedRateOptionForunavailableRate(CApplicantRateOption[] list, string requestedRate,
            BrokerDB currentBroker, bool isLenderPaidOriginatorCompAndAdditionToFees, bool isBorrowerPaidOriginatorCompAndIncludedInFees, out string compMode)
        {
            compMode = string.Empty;
            // 7/13/2005 dd - Requested rate is in following format. {rate}:{point}:{rateid}
            string[] parts = requestedRate.Split(':');
            if (parts.Length < 3)
                return null;

            string rate = parts[0];
            string point = parts[1];
            string id = parts[2];


            // OPM 64253.  If lender has a special option turned on, we want to 
            // display the point value as adjusted for originator comp.
            string pointRepMode = string.Empty; ;
            if (currentBroker.IsUsePriceIncludingCompensationInPricingResult && isLenderPaidOriginatorCompAndAdditionToFees)
            {
                // OPM 64253
                compMode = pointRepMode = "PointIncludingOriginatorComp";
            }

            foreach (CApplicantRateOption rateOption in list)
            {
                string expectedPoint = rateOption.Point_rep;

                if (pointRepMode == "PointIncludingOriginatorComp")
                {
                    expectedPoint = rateOption.PointIncludingOriginatorComp_rep;
                }
                else if (pointRepMode == "PointLessOriginatorComp")
                {
                    expectedPoint = rateOption.PointLessOriginatorComp_rep;
                }

                if (string.IsNullOrEmpty(id) || id == "0.000,0.000")
                {
                    return null;
                }
                else if (rateOption.RateOptionId == id)
                {
                    if (rateOption.Rate_rep != rate || expectedPoint != point)
                    {
                        rateOption.IsRateChange = true;
                        rateOption.OldRate = rate;
                        rateOption.OldPoint = point;
                        return rateOption;
                    }
                }
            }


            return null;
        }



        private void ApplyLoan(LpeTaskMessage task) 
        {
            Guid userID               = task.Header.UserId;
            Guid employeeID           = task.Header.EmployeeId;
            Guid brokerID             = task.Header.BrokerId;
            Guid loanID               = task.RequestData.LoanId;
            Guid productID            = (Guid) task.RequestData.ProductIdList[0];
            string userName           = task.Header.UserName;
            string requestedRate      = task.Options.SelectedRateFormat;
            bool isRequestingRateLock = task.RequestData.IsRequestingRateLock;
            Guid priceGroupID         = task.Options.PriceGroupId;
            bool hasPreviousRegisteredCert;

            CPageData dataLoan = new CSubmitSingleLoanData(loanID);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            decimal newParRate;
            XDocument parRateDebugInfo;
            XmlDocument parRateExtendedDebugInfo;
            CApplicantPrice product = RetrieveLoanProductAndMergeGroupParRate(brokerID, task, dataLoan, productID, out newParRate, out parRateDebugInfo, out parRateExtendedDebugInfo);

            var UserCanApplyForIneligibleLoanPrograms = ((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal).HasPermission(Permission.CanApplyForIneligibleLoanPrograms);
            if (!UserCanApplyForIneligibleLoanPrograms) 
            {
                // 12/9/2005 dd - Only throw this exception if user doesn't have permission to apply for ineligible loan programs.
                if (product.Status == E_EvalStatus.Eval_Ineligible) 
                {
                    string userErrorMsg = "<ul>";
                    foreach (Toolbox.CStipulation s in product.DenialReasons) 
                    {
                        userErrorMsg += "<li>" + Utilities.SafeJsString(s.Description);
                    }
                    userErrorMsg += "</ul>";

                    CBaseException exc = new CBaseException("INELIGIBLE:" + userErrorMsg, "INELIGIBLE:" + userErrorMsg);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }
            }

            if (isRequestingRateLock && product.IsBlockedRateLockSubmission) 
            {
                // 12/13/2007 dd - Do not allow to submit rate lock if IsBlockedRateLockSubmission is true.
                CBaseException exc = new CBaseException(product.RateLockSubmissionUserWarningMessage, product.RateLockSubmissionDevWarningMessage);
                exc.IsEmailDeveloper = false;
                throw exc;
            }

            bool isBorrowerPaidOriginatorCompAndIncludedInFees;
            bool isLenderPaidOriginatorCompAndAdditionToFees;
            E_LpeRunModeT lpeRunModeT;
            CApplicantRateOption rateOption;
            string compMode;

                DateTime dt = DateTime.MinValue;
                try
                {

                    dt = new DateTime(task.Options.DebugResultStartTicks);
                }
                catch (ArgumentOutOfRangeException)
                {
                }


                if (string.IsNullOrEmpty(task.Options.UniqueChecksum) == false)
                {
                    // 1/21/2011 dd - We are doing case sensitive string search here.
                    if (task.Options.UniqueChecksum.Equals(product.UniqueChecksum, StringComparison.Ordinal) == false)
                    {
                   
                        // Rates are different, but we still will need to know how they
                        // change in case it is due to DTI.


                        isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
                                && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                        isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                        dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

                        lpeRunModeT = dataLoan.lpeRunModeT; // 5/7/2009 dd - Store value to indicate if the loan was re-run for rate lock.


                        rateOption = FindSelectedRateOptionForunavailableRate(product.ApplicantRateOptions, requestedRate
                            , dataLoan.BrokerDB, isLenderPaidOriginatorCompAndAdditionToFees, isBorrowerPaidOriginatorCompAndIncludedInFees, out compMode);

                        if (rateOption != null)
                        {
                            string repPoint = rateOption.Point_rep;

                            if (compMode == "PointIncludingOriginatorComp")
                            {
                                repPoint = rateOption.PointIncludingOriginatorComp_rep;
                            }
                            else if (compMode == "PointLessOriginatorComp")
                            {
                                repPoint = rateOption.PointLessOriginatorComp_rep;
                            }

                            string rateOptionChange = string.Format("RATE_CHANGE_ERR:{0}|{1}|{2}|{3}|{4}", rateOption.OldRate, rateOption.OldPoint, rateOption.Rate_rep, repPoint, rateOption.RateOptionId);
                            throw new CInvalidRateVersionException(task.Options.UniqueChecksum, product.UniqueChecksum, loanID, dt, rateOptionChange);
                        }
                        else
                            throw new CInvalidRateVersionException(task.Options.UniqueChecksum, product.UniqueChecksum, loanID, dt);
                    }
                    else
                    {
                        if (dataLoan.BrokerDB.ShowQMStatusInPml2 && !(UserCanApplyForIneligibleLoanPrograms && product.Status == E_EvalStatus.Eval_Ineligible))
                        {
                            // 12/26/2013 dd - OPM 148164 - If QM is not display on PML result then disable this par rate check.
                            // 12/27/2016 mf - OPM 242747 - Also exclude submissions from users who can and do submit through ineligible.
                            decimal oldParRate;
                            if (decimal.TryParse(task.Options.RateMergeParRate, out oldParRate) && oldParRate != newParRate)
                            {
                                StringBuilder sb = new StringBuilder();
                                sb.Append("Par rate discrepancy. Old Par: " + oldParRate + ". New Par: " + newParRate + ".");

                                if (parRateDebugInfo == null && parRateExtendedDebugInfo == null)
                                {
                                    sb.Append(" Debug info is null.");
                                }

                                if (parRateDebugInfo != null)
                                {
                                    sb.Append(Environment.NewLine + "Dumping debug info:" + Environment.NewLine + parRateDebugInfo.ToString());
                                }

                                if (parRateExtendedDebugInfo != null)
                                {
                                    string tempFilePath = TempFileUtils.NewTempFilePath();
                                    TextFileHelper.WriteString(tempFilePath, parRateExtendedDebugInfo.OuterXml, false);
                                    string fileDbKey = "ApplyLoanExtendedDebugInfo_" + Guid.NewGuid().ToString("N");
                                    AutoExpiredTextCache.AddFileToCache(tempFilePath, TimeSpan.FromDays(8), fileDbKey);

                                    sb.Append(Environment.NewLine + "Extended debug Info stored in temp file " + tempFilePath + " with key " + fileDbKey);
                                }

                                Tools.LogInfo(sb.ToString());

                                throw new CInvalidRateVersionException(task.Options.UniqueChecksum + " par=" + oldParRate, product.UniqueChecksum + " par=" + newParRate, loanID, dt);
                            }
                        }
                    }
                }
            
            dataLoan = new CSubmitSingleLoanData(loanID); 
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            hasPreviousRegisteredCert = dataLoan.sPmlCertXmlContent.Value.Length > 0;

            if (task.Options.PricingState != null)
            {
                dataLoan.SetPricingState(task.Options.PricingState);
            }

            isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
    && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
            isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

            lpeRunModeT = dataLoan.lpeRunModeT; // 5/7/2009 dd - Store value to indicate if the loan was re-run for rate lock.

            rateOption = FindSelectedRateOption(product.ApplicantRateOptions, requestedRate
                , dataLoan.BrokerDB, isLenderPaidOriginatorCompAndAdditionToFees, isBorrowerPaidOriginatorCompAndIncludedInFees, out compMode, false /* Investor Mode */);

            if (null == rateOption) 
            {

                string devMsg = BuildDevErrorMsgRateOptionNotFound(product, requestedRate, task);
                CBaseException exc = new CBaseException(ErrorMessages.UnableToFindRateOption, devMsg);
                exc.IsEmailDeveloper = false; // 3/20/2013 dd - There is nothing SDE can do.
                throw exc;
            }

            if (rateOption.IsRateChange) 
            {
                string repPoint = rateOption.Point_rep;

                if (compMode == "PointIncludingOriginatorComp")
                {
                    repPoint = rateOption.PointIncludingOriginatorComp_rep;
                }
                else if (compMode == "PointLessOriginatorComp")
                {
                    repPoint = rateOption.PointLessOriginatorComp_rep;
                }

                CBaseException exc = new CBaseException(string.Format("RATE_CHANGE_ERR:{0}|{1}|{2}|{3}|{4}", rateOption.OldRate, rateOption.OldPoint, rateOption.Rate_rep, repPoint, rateOption.RateOptionId), "RATE_CHANGE_ERR");
                exc.IsEmailDeveloper = false; // Don't email developer
                throw exc;

            }

       
            dataLoan.SubmitFromPML(product, rateOption, isRequestingRateLock);

            dataLoan.sQMParR_rep = newParRate.ToString();
            if (newParRate <= rateOption.Rate)
            {
                dataLoan.sGfeDiscountPointFProps = LosConvert.GfeItemProps_UpdateBF(dataLoan.sGfeDiscountPointFProps, false);
            }

            if (dataLoan.sPml1stSubmitD_rep == "") 
            {

                dataLoan.sPml1stSubmitD = CDateTime.Create(DateTime.Now);
            }

            dataLoan.Save();

            // Hook in the underwriter.
            SetAssignmentsOnSubmit(
                brokerID,
                employeeID,
                dataLoan.sLId,
                isRequestingRateLock,
                priceGroupID,
                dataLoan.sBranchChannelT,
                dataLoan.sCorrespondentProcessT);           
            
            if (!isRequestingRateLock) 
            {
                requestedRate = "0.000:0.000:0.000,0.000";
            }
            bool isLoadStipFromCondition = lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct && isRequestingRateLock;
            string sPmlCertXmlContent = SaveCertificate(dataLoan.sLId, product, requestedRate, null, isRequestingRateLock, isLoadStipFromCondition, priceGroupID, newParRate);

            // 9/11/2013 AV - 126693 Do not auto-populate PML conditions on registered loans at lock request
            if (hasPreviousRegisteredCert)
            {
                // 5/7/2009 dd - OPM 29758 - User is submit for rate lock on the previously float loan.
                // We do not want to convert the stipulation to condition in this scenario.
            }
            else
            {
                ProcessStipulations(brokerID, dataLoan.sLId, product, dataLoan.sEmployeeLenderAccExecEmail, userName, employeeID);
            }

            string first_lpResult = LoadResultHtmlFromFileDB("LPEFirstResult_" + dataLoan.sLId.ToString("N"));
            string second_lpResult = LoadResultHtmlFromFileDB("LPESecondResult_" + dataLoan.sLId.ToString("N"));

            string auditAction = string.Empty;
            // 7/29/2009 dd - Add more description audit event on submitting. OPM 32212.
            if (isRequestingRateLock)
            {
                // This could be either Register & Lock or just Lock.

                if (lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                {
                    auditAction = ConstAppDavid.AuditEvent_PmlSubmission_Lock;
                }
                else
                {
                    auditAction = ConstAppDavid.AuditEvent_PmlSubmission_RegisteredLock;
                }
            }
            else
            {
                auditAction = ConstAppDavid.AuditEvent_PmlSubmission_Registered;
            }
            LoanSubmissionAuditItem auditItem = new LoanSubmissionAuditItem((AbstractUserPrincipal) System.Threading.Thread.CurrentPrincipal, 
                auditAction,
                product.lLpTemplateNm, 
                rateOption.Rate_rep, 
                rateOption.Point_rep, 
                dataLoan.sProThisMPmt_rep, 
                rateOption.Margin_rep, 
                RenderCertificateToHtml(brokerID, sPmlCertXmlContent),
                null, null, null, null, null, null,
                dataLoan.sQualBottomR_rep, isRequestingRateLock, first_lpResult, second_lpResult, parRateDebugInfo);

            AuditManager.RecordAudit(dataLoan.sLId, auditItem);

            if (brokerID.Equals(new Guid("e9f006c9-cb83-4531-8925-08fccf4add63")))
            {
                Tools.LogInfo("OPM_225020: A Lock Submission Audit Event was created for First Tech loan with Loan Id:  " + dataLoan.sLId.ToString() + " by User Id: " + userID.ToString() +
                    " with the following Stack Trace: \n" +
                    Environment.StackTrace);
            }
        }        

        private void TemporaryAcceptFirstLoan(LpeTaskMessage task) 
        {

            Guid userID               = task.Header.UserId;
            Guid brokerID             = task.Header.BrokerId;
            Guid loanID               = task.RequestData.LoanId;
            Guid productID            = (Guid) task.RequestData.ProductIdList[0];
            string requestedRate      = task.Options.SelectedRateFormat;

            E_CalcModeT calcModeT     = E_CalcModeT.PriceMyLoan;

            string sLpeRateOptionIdOf1stLienIn8020 = "";
            try 
            {
                string[] parts = requestedRate.Split(':');
                sLpeRateOptionIdOf1stLienIn8020 = parts[2];
            } 
            catch {}

            CApplicantPrice product = RetrieveLoanProduct(brokerID, loanID, productID, E_sLienQualifyModeT.ThisLoan, task.Options, task.Header.LpeRequestBatchId);

            CPageData dataLoan = new CSubmitMainLoanFromLOData(loanID);
            dataLoan.CalcModeT = calcModeT;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            bool isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
&& dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
            bool isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

            string compMode;
            CApplicantRateOption rateOption = FindSelectedRateOption(product.ApplicantRateOptions, requestedRate,
                dataLoan.BrokerDB, isLenderPaidOriginatorCompAndAdditionToFees, isBorrowerPaidOriginatorCompAndIncludedInFees, out compMode, false /* Investor Mode */);

            if (null == rateOption) 
            {
                string devMsg = BuildDevErrorMsgRateOptionNotFound(product, requestedRate, task);
                throw new CBaseException(ErrorMessages.UnableToFindRateOption, devMsg);
            }



            decimal rate = rateOption.Rate;
            decimal point = rateOption.Point_;

            if (rateOption.IsRateChange) 
            {
                // 3/29/2007 dd - External OPM 29546 - If rate or fee changed (due to DTI) then we want to store the requested rate and display "Rate Change" during confirmation stage.
                try 
                {
                    rate = decimal.Parse(rateOption.OldRate);
                } 
                catch {}
                try 
                {
                    point = decimal.Parse(rateOption.OldPoint);
                } 
                catch {}
            }

            // 3/2/2005 dd - When doing 80/20, assume 80 loan to be rate lock such that during confirmation page it will
            // show interest rate for 80 loan.
            dataLoan.SubmitMainLoanFromPML(true, product, rate, point, rateOption.Margin, rateOption.QRate, rateOption.TeaserRate, true /* isRequestingRateLock */, rateOption.PerOptionCodes);
            dataLoan.sLpeRateOptionIdOf1stLienIn8020 = sLpeRateOptionIdOf1stLienIn8020;
            dataLoan.Save();
        }

        public CPageData TemporaryAcceptFirstLoanWithoutSave(LpeTaskMessage task, DataAccess.LoanComparison.PricingState state = null) 
        {
            Guid userID               = task.Header.UserId;
            Guid brokerID             = task.Header.BrokerId;
            Guid loanID               = task.RequestData.LoanId;
            Guid productID            = (Guid) task.RequestData.ProductIdList[0];
            string requestedRate      = task.Options.SelectedRateFormat;

            E_CalcModeT calcModeT     = E_CalcModeT.PriceMyLoan;

            string sLpeRateOptionIdOf1stLienIn8020 = "";
            try 
            {
                string[] parts = requestedRate.Split(':');
                sLpeRateOptionIdOf1stLienIn8020 = parts[2];
            } 
            catch {}

            CPageData dataLoan = new CSubmitMainLoanFromLOData2(loanID);
            dataLoan.CalcModeT = calcModeT;
            dataLoan.AllowLoadWhileQP2Sandboxed = true;
            dataLoan.InitLoad();

            if (state != null)
            {
                dataLoan.SetPricingState(state);
            }

            CApplicantPrice product = RetrieveLoanProduct(brokerID, productID, dataLoan, E_sLienQualifyModeT.ThisLoan, task.Options);

            bool isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
&& dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
            bool isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

            string compMode;
            CApplicantRateOption rateOption = FindSelectedRateOption(product.ApplicantRateOptions, requestedRate,
                dataLoan.BrokerDB, isLenderPaidOriginatorCompAndAdditionToFees, isBorrowerPaidOriginatorCompAndIncludedInFees, out compMode, false /* Investor Mode */);

            if (null == rateOption) 
            {
                string devMsg = BuildDevErrorMsgRateOptionNotFound(product, requestedRate, task);
                throw new CBaseException(ErrorMessages.UnableToFindRateOption, devMsg);
            }


            decimal rate = rateOption.Rate;
            decimal point = rateOption.Point_;

            if (rateOption.IsRateChange) 
            {
                // 3/29/2007 dd - External OPM 29546 - If rate or fee changed (due to DTI) then we want to store the requested rate and display "Rate Change" during confirmation stage.
                try 
                {
                    rate = decimal.Parse(rateOption.OldRate);
                } 
                catch {}
                try 
                {
                    point = decimal.Parse(rateOption.OldPoint);
                } 
                catch {}
            }
            // 3/2/2005 dd - When doing 80/20, assume 80 loan to be rate lock such that during confirmation page it will
            // show interest rate for 80 loan.
            dataLoan.SubmitMainLoanFromPML(true, product, rate, point, rateOption.Margin, rateOption.QRate, rateOption.TeaserRate, true /* isRequestingRateLock */, rateOption.PerOptionCodes);
            dataLoan.sLpeRateOptionIdOf1stLienIn8020 = sLpeRateOptionIdOf1stLienIn8020;
            dataLoan.ClearPricingCache(); // Clean up the pricing cache since we are going to be reusing this instance.
            dataLoan.InvalidateCache();
            return dataLoan;

        }


        private void VerifyLoanProgram(LpeTaskMessage task, XmlElement elRoot) 
        {   

            Guid userID               = task.Header.UserId;
            Guid brokerID             = task.Header.BrokerId;
            Guid loanID               = task.RequestData.LoanId;
            string sProOFinPmt        = task.RequestData.sProOFinPmt;

            CPageData dataLoan = null;

            if (task.TemporaryLpeTaskMessageXml != "") 
            {
                dataLoan = TemporaryAcceptFirstLoanWithoutSave((LpeTaskMessage) SerializationHelper.XmlDeserialize(task.TemporaryLpeTaskMessageXml, typeof(LpeTaskMessage)));
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
            } 
            else 
            {
                dataLoan = new CVerifyFirstLoanProgram(loanID);
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                dataLoan.SetFormatTarget(FormatTarget.PrintoutImportantFields);
                dataLoan.InitLoad();
            }

            dataLoan.sProOFinPmtPe_rep = sProOFinPmt;

            CApplicantPrice applicantPrice = RetrieveLoanProduct(brokerID, task.Options.FirstLienLpProgramId, dataLoan, E_sLienQualifyModeT.ThisLoan, task.Options);

            if (null != applicantPrice) 
            {
                XmlElement el = elRoot.OwnerDocument.CreateElement("CApplicantPrices");
                elRoot.AppendChild(el);

                el.AppendChild(CApplicantPriceXml.ToXmlElement(applicantPrice, elRoot.OwnerDocument));
            }
        }

        private string BuildDevErrorMsgRateOptionNotFound(CApplicantPrice product, string requestedRate, LpeTaskMessage task) 
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendFormat("Unable to submit rateoption. {0}Requested Rate={1},{0}ProductId={2},{0}ProductName={3},{0}BrokerID={4},{0}UserID={5},{0}LoanID={6}",
                Environment.NewLine, requestedRate, product.lLpTemplateId, product.lLpTemplateNm, task.Header.BrokerId, task.Header.UserId, task.RequestData.LoanId);
            sb.Append(Environment.NewLine).Append("Rate Option List");
            foreach (CApplicantRateOption o in product.ApplicantRateOptions) 
            {
                sb.AppendFormat("{0}Rate={1}, Point={2}, RateOptionId={3}", Environment.NewLine, o.Rate_rep, o.Point_rep, o.RateOptionId);

            }
            return sb.ToString();
        }

        private void SubmitRate80_20(LpeTaskMessage task)
        {
            Guid userID = task.Header.UserId;
            Guid brokerID = task.Header.BrokerId;
            Guid loanID = task.RequestData.LoanId;
            Guid productID = (Guid)task.RequestData.ProductIdList[0];
            Guid employeeID = task.Header.EmployeeId;
            string userName = task.Header.UserName;
            string requestedRate = task.Options.SelectedRateFormat;
            bool isRequestingRateLock = task.RequestData.IsRequestingRateLock;
            Guid priceGroupID = task.Options.PriceGroupId;

            Guid firstLienLpId = task.Options.FirstLienLpProgramId;
            decimal firstLienNoteRate = task.Options.FirstLienNoteRate;
            decimal firstLienPoint = task.Options.FirstLienPoint;

            decimal secondLienNoteRate = task.Options.SelectedRate;
            decimal secondLienPoint = task.Options.SelectedFee;
            bool hasPreviousRegisteredCert = false;

            CPageData dataLoan = new CSubmitSingleLoanData(loanID);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitLoad();
            hasPreviousRegisteredCert = dataLoan.sPmlCertXmlContent.Value.Length > 0;


            bool isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
    && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
            bool isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;

            BrokerDB currentBroker = BrokerDB.RetrieveById(brokerID);

            #region Run Pricing for 2nd lien.
            CApplicantPrice secondLienProduct = null;
            CApplicantRateOption secondLienRateOption = new CApplicantRateOption(Guid.Empty, "", "", "0.0", "0.0", "0.0", "0.0", 0, "0.0", "0.0", "0.0", "0", "0", "0", "0", "0", "0", 0, 0, 0);
            decimal secondLienParRate = 0.0M;

            if (productID != Guid.Empty)
            {
                task.Options.SelectedRate = secondLienNoteRate;
                task.Options.SelectedFee = secondLienPoint;
                secondLienProduct = RetrieveLoanProduct(brokerID, productID, dataLoan, E_sLienQualifyModeT.OtherLoan, task.Options, out secondLienParRate);

                if (null != secondLienProduct)
                {
                    Tools.Assert(secondLienProduct.lLienPosT == E_sLienPosT.Second, "Oops, wrong loan product");
                }
                if (string.IsNullOrEmpty(task.Options.SecondLienUniqueChecksum) == false)
                {
                    // 1/21/2011 dd - We are doing case sensitive string search here.
                    if (task.Options.SecondLienUniqueChecksum.Equals(secondLienProduct.UniqueChecksum, StringComparison.Ordinal) == false)
                    {
                        DateTime dt = DateTime.MinValue;
                        try
                        {

                            dt = new DateTime(task.Options.DebugResultStartTicks);
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                        }
                        throw new CInvalidRateVersionException(task.Options.SecondLienUniqueChecksum, secondLienProduct.UniqueChecksum, loanID, dt);
                    }
                }

                secondLienRateOption = FindSelectedRateOption(secondLienProduct.ApplicantRateOptions, secondLienNoteRate, secondLienPoint,
                    currentBroker, isLenderPaidOriginatorCompAndAdditionToFees, isBorrowerPaidOriginatorCompAndIncludedInFees);

                if (null == secondLienRateOption)
                {

                    string devMsg = BuildDevErrorMsgRateOptionNotFound(secondLienProduct, secondLienNoteRate + ":" + secondLienPoint, task);
                    throw new CBaseException(ErrorMessages.UnableToFindRateOption, devMsg);
                }

            }




            #endregion

            #region Run Pricing for 1st Lien.

            task.Options.SelectedRate = firstLienNoteRate;
            task.Options.SelectedFee = firstLienPoint;

            CPageData firstLienDataLoan = new CSubmitSingleLoanData(loanID);
            firstLienDataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            firstLienDataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            firstLienDataLoan.sProOFinPmtPe_rep = secondLienRateOption.QualPmt_rep;

            decimal firstLienParRate;
            XDocument parRateDebugInfo;
            CApplicantPrice firstLienProduct = RetrieveLoanProductAndMergeGroupParRate(brokerID, task, firstLienDataLoan, firstLienLpId, out firstLienParRate, out parRateDebugInfo);

            if (firstLienProduct == null)
            {
                CBaseException exc = new CBaseException("INELIGIBLE:Program is no longer available", "INELIGIBLE:Program is no longer available");
                exc.IsEmailDeveloper = false;
                throw exc;
            }
            if (string.IsNullOrEmpty(task.Options.UniqueChecksum) == false)
            {
                // 1/21/2011 dd - We are doing case sensitive string search here.
                if (task.Options.UniqueChecksum.Equals(firstLienProduct.UniqueChecksum, StringComparison.Ordinal) == false)
                {
                    DateTime dt = DateTime.MinValue;
                    try
                    {

                        dt = new DateTime(task.Options.DebugResultStartTicks);
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                    }
                    throw new CInvalidRateVersionException(task.Options.UniqueChecksum, firstLienProduct.UniqueChecksum, loanID, dt);
                }
            }
            

            if (!((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal).HasPermission(Permission.CanApplyForIneligibleLoanPrograms))
            {
                // 12/9/2005 dd - Only throw this exception if user doesn't have permission to apply for ineligible loan programs.
                if (firstLienProduct.Status == E_EvalStatus.Eval_Ineligible)
                {
                    string userErrorMsg = "<ul>";
                    foreach (Toolbox.CStipulation s in firstLienProduct.DenialReasons)
                    {
                        userErrorMsg += "<li>" + Utilities.SafeJsString(s.Description);
                    }
                    userErrorMsg += "</ul>";

                    CBaseException exc = new CBaseException("INELIGIBLE:" + userErrorMsg, "INELIGIBLE:" + userErrorMsg);
                    exc.IsEmailDeveloper = false;
                    throw exc;
                }
            }

            if (isRequestingRateLock && firstLienProduct.IsBlockedRateLockSubmission)
            {
                // 12/13/2007 dd - Do not allow to submit rate lock if IsBlockedRateLockSubmission is true.
                CBaseException exc = new CBaseException(firstLienProduct.RateLockSubmissionUserWarningMessage, firstLienProduct.RateLockSubmissionDevWarningMessage);
                exc.IsEmailDeveloper = false;
                throw exc;
            }

            CApplicantRateOption firstLienRateOption = FindSelectedRateOption(firstLienProduct.ApplicantRateOptions, firstLienNoteRate, firstLienPoint,
                currentBroker, isLenderPaidOriginatorCompAndAdditionToFees, isBorrowerPaidOriginatorCompAndIncludedInFees);

            if (null == firstLienRateOption)
            {

                string devMsg = BuildDevErrorMsgRateOptionNotFound(firstLienProduct, firstLienNoteRate + ":" + firstLienPoint, task);
                throw new CBaseException(ErrorMessages.UnableToFindRateOption, devMsg);
            }


            #endregion

            #region Save 1st and 2nd loan data.
            E_LpeRunModeT lpeRunModeT = firstLienDataLoan.lpeRunModeT; // 5/7/2009 dd - Store value to indicate if the loan was re-run for rate lock.


            firstLienDataLoan.SubmitFromPML(firstLienProduct, firstLienRateOption, isRequestingRateLock);

            CPageData secondLoan = null;


            if (firstLienDataLoan.sLinkedLoanInfo.IsLoanLinked)
            {
                // 7/17/2006 dd - Retrieve existing 2nd loan. This case only happen when user re-run in float mode.
                secondLoan = firstLienDataLoan.SpinOffLoanForExistingSubsequent2ndLienLoan(firstLienDataLoan.sLinkedLoanInfo.LinkedLId);
            }
            else
            {
                secondLoan = firstLienDataLoan.SpinOffLoanForNewSubsequent2ndLienLoan((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal);
            }

            // 8/13/2014 gf - Include the MI from the first lien when setting the 1st lien monthly pmt.
            secondLoan.sRemain1stMPmt = firstLienDataLoan.m_convertLos.ToMoney(firstLienRateOption.QualPmt_rep) + firstLienDataLoan.sProMIns;
            secondLoan.SubmitFromPML(secondLienProduct, secondLienRateOption, isRequestingRateLock);

            firstLienDataLoan.Confirm1stLienLoanTentativelyAccepted(
                secondLoan.sIsOptionArm ? secondLoan.sProThisMQual_rep : secondLoan.sProThisMPmt_rep,
                secondLoan.sFinMethT,
                secondLoan.sQualIR != 0 ? secondLoan.sIsQRateIOnly : secondLoan.sIsIOnly,
                isRequestingRateLock);

            //opm 90938 
            bool isHeloc = secondLienProduct == null ? secondLoan.sSubFinT == E_sSubFinT.Heloc : secondLienProduct.lLpProductType.Equals("HELOC", StringComparison.OrdinalIgnoreCase);

            #region OPM 74369
            if (isHeloc)
            {
                firstLienDataLoan.sSubFin = firstLienDataLoan.sSubFinPe;
                firstLienDataLoan.sIsOFinCreditLineInDrawPeriod = true;
            }
            else
            {
                firstLienDataLoan.sSubFin = firstLienDataLoan.sConcurSubFin = firstLienDataLoan.sProOFinBalPe;
                firstLienDataLoan.sIsOFinCreditLineInDrawPeriod = false;
            }
            #endregion

            firstLienDataLoan.sONewFinCc = secondLoan.GetOtherFinancingCostsForLinkedLoan();
            secondLoan.sONewFinCc = firstLienDataLoan.GetOtherFinancingCostsForLinkedLoan();

            // gf opm 243023
            firstLienDataLoan.sSubFinIR = secondLoan.sNoteIR;
            firstLienDataLoan.sSubFinTerm = secondLoan.sTerm;

            // OPM 246240
            firstLienDataLoan.sQMParR_rep = firstLienParRate.ToString();
            secondLoan.sQMParR_rep = secondLienParRate.ToString();

            firstLienDataLoan.Save();
            secondLoan.Save();
            #endregion


            // Hook in the underwriter.
            SetAssignmentsOnSubmit(
                brokerID,
                employeeID,
                firstLienDataLoan.sLId,
                isRequestingRateLock,
                priceGroupID,
                firstLienDataLoan.sBranchChannelT,
                firstLienDataLoan.sCorrespondentProcessT);

            SetAssignmentsOnSubmit(
                brokerID,
                employeeID,
                secondLoan.sLId,
                isRequestingRateLock,
                priceGroupID,
                secondLoan.sBranchChannelT,
                secondLoan.sCorrespondentProcessT);

            string firstLoanRequestedRate = firstLienNoteRate + ":" + firstLienPoint + ":0.000,0.000";
            requestedRate = secondLienNoteRate + ":" + secondLienPoint + ":0.000,0.000";
            if (!isRequestingRateLock)
            {
                firstLoanRequestedRate = "0.000:0.000:0.000,0.000";
                requestedRate = "0.000:0.000:0.000,0.000";

            }
            bool isLoadStipFromCondition = lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct && isRequestingRateLock;
            string firstLoan_sPmlCertXmlContent = SaveCertificate(firstLienDataLoan.sLId, firstLienProduct, firstLoanRequestedRate, null, isRequestingRateLock, isLoadStipFromCondition, priceGroupID, 0); // 12/2/2005 dd - OPM 3338
            string secondLoan_sPmlCertXmlContent = SaveCertificate(secondLoan.sLId, secondLienProduct, requestedRate, firstLienDataLoan.sLNm, isRequestingRateLock, isLoadStipFromCondition, priceGroupID, 0); // 12/2/2005 dd - OPM 3338

            // 9/16/2013 AV - 126693 Do not auto-populate PML conditions on registered loans at lock request
            if (hasPreviousRegisteredCert)
            {
                // 5/7/2009 dd - OPM 29758 - User is submit for rate lock on the previously float loan.
                // We do not want to convert the stipulation to condition in this scenario.
                //Tools.LogError("Skip Convert Stip to Condition 80/20 scenario");
            }
            else
            {
                ProcessStipulations(brokerID, firstLienDataLoan.sLId, firstLienProduct, firstLienDataLoan.sEmployeeLenderAccExecEmail, userName, employeeID);
                ProcessStipulations(brokerID, secondLoan.sLId, secondLienProduct, secondLoan.sEmployeeLenderAccExecEmail, userName, employeeID);
            }

            #region Record Audit Event
            string first_lpResult = LoadResultHtmlFromFileDB("LPEFirstResult_" + firstLienDataLoan.sLId.ToString("N"));
            string second_lpResult = LoadResultHtmlFromFileDB("LPESecondResult_" + firstLienDataLoan.sLId.ToString("N"));

            string auditAction = string.Empty;
            // 7/29/2009 dd - Add more description audit event on submitting. OPM 32212.
            if (isRequestingRateLock)
            {
                // This could be either Register & Lock or just Lock.
                if (lpeRunModeT == E_LpeRunModeT.OriginalProductOnly_Direct)
                {
                    auditAction = ConstAppDavid.AuditEvent_PmlSubmission_Lock;
                }
                else
                {
                    auditAction = ConstAppDavid.AuditEvent_PmlSubmission_RegisteredLock;
                }
            }
            else
            {
                auditAction = ConstAppDavid.AuditEvent_PmlSubmission_Registered;
            }
            LoanSubmissionAuditItem auditItem = new LoanSubmissionAuditItem((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal,
                auditAction,
                firstLienDataLoan.sLpTemplateNm,
                firstLienRateOption.Rate_rep,
                firstLienRateOption.Point_rep,
                firstLienDataLoan.sProThisMPmt_rep,
                firstLienRateOption.Margin_rep,
                RenderCertificateToHtml(brokerID, firstLoan_sPmlCertXmlContent),
                null == secondLienProduct ? CPageBase.SubmitManuallyProgramName : secondLienProduct.lLpTemplateNm,
                secondLienRateOption.Rate_rep,
                secondLienRateOption.Point_rep,
                secondLoan.sProThisMPmt_rep,
                secondLienRateOption.Margin_rep,
                RenderCertificateToHtml(brokerID, secondLoan_sPmlCertXmlContent),
                firstLienDataLoan.sQualBottomR_rep,
                isRequestingRateLock,
                first_lpResult,
                second_lpResult,
                parRateDebugInfo
                );

            AuditManager.RecordAudit(firstLienDataLoan.sLId, auditItem);
            // In 80/20 record the certificate in 2nd loan also.
            AuditManager.RecordAudit(secondLoan.sLId, auditItem);
            #endregion

            if (brokerID.Equals(new Guid("e9f006c9-cb83-4531-8925-08fccf4add63")))
            {
                Tools.LogInfo("OPM_225020: A Lock Submission Audit Event was created for First Tech loan with Loan Id:  "
                    + firstLienDataLoan.sLId.ToString()
                    + " by User Id: " + userID.ToString() + ". \n"
                    + "Task Info \n"
                    + "Created Date: " + task.Header.CreatedDate.ToString() + " \n"
                    + "LpeTaskT : " + task.Header.LpeTaskT.ToString() + ". \n"
                    + "LpeRequestBatchId: " + task.Header.LpeRequestBatchId.ToString() + ". \n"
                    + "LpeRequestIntId: " + task.Header.LpeRequestIntId + ". \n"
                    + "Number of Requests in Batch: " + task.Header.NumberOfRequestsInBatch + ". \n"
                    + "Stack Trace: \n" +
                    Environment.StackTrace);
            }
        }

        private CApplicantPrice RetrieveLoanProduct(Guid brokerID, Guid productID, CPageData dataLoan, E_sLienQualifyModeT lienQualifyModeT, CLpRunOptions options) 
        {
            HashSet<Guid> onlyTheseProdIds = new HashSet<Guid>();
            onlyTheseProdIds.Add(productID);

            CPriceGenerator priceGenerator = new CPriceGenerator(dataLoan, onlyTheseProdIds,options, lienQualifyModeT);
            CApplicantPrice product = priceGenerator.GetApplicantPriceList().ElementAt(0);

            return product;

        }
        private CApplicantPrice RetrieveLoanProduct(Guid brokerID, Guid productID, CPageData dataLoan, E_sLienQualifyModeT lienQualifyModeT, CLpRunOptions options, out decimal parRate)
        {
            HashSet<Guid> onlyTheseProdIds = new HashSet<Guid>();
            onlyTheseProdIds.Add(productID);
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerID);
            CPriceGenerator priceGenerator = new CPriceGenerator(brokerDB, dataLoan.sLId, onlyTheseProdIds, options, lienQualifyModeT, quickPricerLoanItem: null, titleQuotes: null);
            CApplicantPrice product = priceGenerator.GetApplicantPriceList().ElementAt(0);

            parRate = GetParRate(dataLoan, priceGenerator, productID);

            return product;
        }
        private CApplicantPrice RetrieveLoanProduct(Guid brokerID, Guid loanID, Guid productID, E_sLienQualifyModeT lienQualifyModeT, CLpRunOptions options, Guid lpeRequestBatchId) 
        {
            HashSet<Guid> onlyTheseProdIds = new HashSet<Guid>();
            onlyTheseProdIds.Add(productID);
            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerID);
            CPriceGenerator priceGenerator = new CPriceGenerator(brokerDB, loanID, onlyTheseProdIds, options, lienQualifyModeT, quickPricerLoanItem: null, titleQuotes: null);

            CApplicantPrice product = priceGenerator.GetApplicantPriceList().ElementAt(0);

            return product;

        }
        public static decimal GetParRate(CPageData dataLoan, CPriceGenerator results, Guid productId)
        {
            XDocument doc;
            XmlDocument exDoc;
            return GetParRate(dataLoan, results, productId, out doc, out exDoc);
        }

        public static decimal GetParRate(Tuple<RateMergeGroupKey, Decimal> overrideParRate, RateMergeGroup mergeGroup, E_OriginatorCompPointSetting pointSetting)
        {
            if (overrideParRate == null)
            {
                return mergeGroup.GetQMParRate(pointSetting);
            }
            else if (overrideParRate.Item1.Equals(mergeGroup.rateMergeGroupKey))
            {
                return overrideParRate.Item2;
            }
            else
            {
                Tools.LogErrorWithCriticalTracking("The par rate merge key does not belong to this group. Unexpected.");
                return mergeGroup.GetQMParRate(pointSetting);
            }
        }

        public static Tuple<RateMergeGroupKey, Decimal> GetParRateOverride(List<UnderwritingResultItem> resultItems, BrokerDB broker, bool includeAllProgramsInResults, E_OriginatorCompPointSetting pointSetting)
        {
            Tuple<RateMergeGroupKey, Decimal> overrideParRate = null;

            //Not enough info to determine the par rate -- looks like we have more info available
            if (resultItems.Count == 1 && resultItems.First() != null && resultItems.First().Results.Count == 1 && resultItems.First().FullResults.Count() > 1)
            {
                RateMergeBucket parBucket = new RateMergeBucket(broker.IsShowRateOptionsWorseThanLowerRate, includeAllProgramsInResults);

                foreach (CApplicantPriceXml xml in resultItems.First().FullResults)
                {
                    if (xml.Status == E_EvalStatus.Eval_Eligible)
                    {
                        parBucket.Add(xml);
                    }
                }

                if (parBucket.RateMergeGroups.Count == 1)
                {
                    foreach (RateMergeGroup mergeGroup in parBucket.RateMergeGroups)
                    {
                        if (mergeGroup.RateOptions.Count == 0)
                            continue;

                        decimal parRate = mergeGroup.GetQMParRate(pointSetting);
                        overrideParRate = Tuple.Create(mergeGroup.rateMergeGroupKey, parRate);
                    }

                    if (overrideParRate == null)
                    {
                        Tools.LogErrorWithCriticalTracking("Could not determine par rate. Should not happen");
                    }
                }
            }
            return overrideParRate;
        }

        public static decimal GetParRate(CPageData dataLoan, CPriceGenerator results, Guid productId, out XDocument debugInfo, out XmlDocument extendedDebugInfo)
        {
            E_OriginatorCompPointSetting pointSetting = E_OriginatorCompPointSetting.UsePoint;

            //todo review this logic...
            if (dataLoan.sIsQualifiedForOriginatorCompensation)
            {
                if (dataLoan.BrokerDB.IsUsePriceIncludingCompensationInPricingResult && dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                    dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees)
                {
                    pointSetting = E_OriginatorCompPointSetting.UsePointWithOriginatorComp;
                }
            }

            RateMergeBucket rateMergeBucket = new RateMergeBucket(dataLoan.BrokerDB.IsShowRateOptionsWorseThanLowerRate);
            RateMergeGroupKey key = null;
            foreach (CApplicantPrice price in results.GetApplicantPriceList())
            {
                CApplicantPriceXml priceXml = CApplicantPriceXml.From(price);
                if (price.lLpTemplateId == productId)
                {
                    key = new RateMergeGroupKey(priceXml);
                    Tools.LogInfo("Matching Product found. Id: " + price.lLpTemplateId + ". Status: " + price.Status.ToString());
                }

                if (price.Status == E_EvalStatus.Eval_Eligible)
                {
                    rateMergeBucket.Add(priceXml);
                }
            }

            extendedDebugInfo = GetParRateDebugXml(productId, pointSetting, results, rateMergeBucket);
            if (key == null)
            {
                string tempFilePath = TempFileUtils.NewTempFilePath();
                TextFileHelper.WriteString(tempFilePath, extendedDebugInfo.OuterXml, false);
                string fileDbKey = "GetParRateDebugInfo_" + Guid.NewGuid().ToString("N");
                AutoExpiredTextCache.AddFileToCache(tempFilePath, TimeSpan.FromDays(8), fileDbKey);
                Tools.LogInfo("ApplyLoanWorkerUnit.GetParRate: Could not find matching product. ProductId = " + productId.ToString()
                    + Environment.NewLine + "Debug Info stored in temp file " + tempFilePath + " with key " + fileDbKey);

                debugInfo = null;
                return 0;
            }

            RateMergeGroup g = rateMergeBucket.GetGroup(key);
            if (g == null)
            {    
                string tempFilePath = TempFileUtils.NewTempFilePath();
                TextFileHelper.WriteString(tempFilePath, extendedDebugInfo.OuterXml, false);
                string fileDbKey = "GetParRateDebugInfo_" + Guid.NewGuid().ToString("N");
                AutoExpiredTextCache.AddFileToCache(tempFilePath, TimeSpan.FromDays(8), fileDbKey);
                Tools.LogInfo("ApplyLoanWorkerUnit.GetParRate: Could not find matching Rate Merge Group. ProductId = " + productId.ToString()
                    + Environment.NewLine + "Debug Info stored in temp file " + tempFilePath + " with key " + fileDbKey);

                debugInfo = null;
                return 0;
            }

            debugInfo =  new XDocument(new XElement("RateMergeInfo", new XAttribute("Name", g.Name)));
            XElement root = debugInfo.Root;
            var winner = g.GetQMParRate(pointSetting);
            foreach (var rateOption in g.RateOptions)
            {
                if (rateOption.IsRateMergeWinner)
                {
                    var rateXml = new XElement("RateOption",
                        new XAttribute("Name", rateOption.LpTemplateNm),
                        new XAttribute("Point", rateOption.GetPoint_rep(pointSetting)),
                        new XAttribute("Rate", rateOption.Rate_rep));

                    if (winner == rateOption.Rate)
                    {
                        rateXml.Add(new XAttribute("Winner", "Winner"));
                    }

                    root.Add(rateXml);
                }
            }

            return winner;
        }

        private static XmlDocument GetParRateDebugXml(Guid productId, E_OriginatorCompPointSetting pointSetting, CPriceGenerator results, RateMergeBucket rateMergeBucket)
        {
            XmlDocument doc = new XmlDocument();
            XmlElement root = doc.CreateElement("ParRateDebugInfo");
            doc.AppendChild(root);

            XmlAttribute IdAttr = doc.CreateAttribute("ProductId");
            IdAttr.Value = productId.ToString();
            root.Attributes.Append(IdAttr);

            XmlAttribute PointSetAttr = doc.CreateAttribute("PointSetting");
            PointSetAttr.Value = pointSetting.ToString();
            root.Attributes.Append(PointSetAttr);
            
            // Output price generator results.
            if (results != null)
            {
                XmlElement prices = doc.CreateElement("CApplicantPrices");
                root.AppendChild(prices);

                foreach (CApplicantPrice price in results.GetApplicantPriceList())
                {
                    prices.AppendChild(CApplicantPriceXml.ToXmlElement(price, doc));
                }
            }

            // Output rage merge bucket.
            if (rateMergeBucket != null)
            {
                XmlElement bucket = doc.CreateElement("RateMergeBucket");
                root.AppendChild(bucket);

                foreach (RateMergeGroup g in rateMergeBucket.RateMergeGroups)
                {
                    XmlElement group = doc.CreateElement("RateMergeGroup");
                    bucket.AppendChild(group);

                    foreach (var rateOption in g.AllRateOptions)
                    {
                        XmlElement option = doc.CreateElement("RateOption");
                        group.AppendChild(option);

                        XmlAttribute NameAttr = doc.CreateAttribute("Name");
                        NameAttr.Value = rateOption.LpTemplateNm;
                        option.Attributes.Append(NameAttr);

                        XmlAttribute PointAttr = doc.CreateAttribute("Point");
                        PointAttr.Value = rateOption.GetPoint_rep(pointSetting);
                        option.Attributes.Append(PointAttr);

                        XmlAttribute RateAttr = doc.CreateAttribute("Rate");
                        RateAttr.Value = rateOption.Rate_rep;
                        option.Attributes.Append(RateAttr);

                        if (rateOption.IsRateMergeWinner)
                        {
                            XmlAttribute winner = doc.CreateAttribute("Winner");
                            winner.Value = "Winner";
                            option.Attributes.Append(winner);
                        }
                    }
                }
            }

            return doc;
        }

        private CApplicantPrice RetrieveLoanProductAndMergeGroupParRate(Guid brokerID, LpeTaskMessage task, CPageData dataLoan, Guid productID, out decimal parRate, out XDocument debugInfo)
        {
            XmlDocument exDoc;
            return RetrieveLoanProductAndMergeGroupParRate(brokerID, task, dataLoan, productID, out parRate, out debugInfo, out exDoc);
        }

        private CApplicantPrice RetrieveLoanProductAndMergeGroupParRate(Guid brokerID, LpeTaskMessage task, CPageData dataLoan, Guid productID, out decimal parRate, out XDocument debugInfo, out XmlDocument extendedDebugInfo)
        {
            HashSet<Guid> onlyTheseProdIds = new HashSet<Guid>();
            onlyTheseProdIds.Add(productID);

            onlyTheseProdIds.UnionWith(dataLoan.GetLoanProgramsForParRateCheck((AbstractUserPrincipal)System.Threading.Thread.CurrentPrincipal).OnlyTheseProdIds);

            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerID);

            CPriceGenerator priceGenerator = CPriceGenerator.ExecutePricing(brokerDB, task, onlyTheseProdIds);
            parRate = GetParRate(dataLoan, priceGenerator, productID, out debugInfo, out extendedDebugInfo);


            foreach (CApplicantPrice product in priceGenerator.GetApplicantPriceList())
            {
                if (product.lLpTemplateId == productID)
                {
                    return product;
                }
            }

            return null;
        }



        private void DetermineAssignedUserAndOwnerUser(
            Guid sBrokerId,
            Guid sLId, 
            out Guid assignedUserId,
            out Guid ownerUserId,
            out Guid hiddenConditionAssignedUserId,
            out Guid hiddenConditionOwnerUserId,
            Guid lpeRunningEmployeeId,
            Dictionary<Guid,Guid> hash,
            out bool isBranchAssociatedWithNewCP)
        {
            // 5/12/2011 dd - Logic will determines the assigned user id and owner user id for condition that is not hidden.
            // Specs: svn://11.12.13.13:9123/LendersOffice/Doc/trunk/LoPmlSystem/32231_TaskSystem/TaskFrontEndSpec.docx
            // Section 7.5
            //      If the condition is not hidden then:
            //          - Assign to the loan officer.  If there is no loan officer then set as "to be assigned" to 
            //             the loan officer.
            //          - Set the owner as the Processor.
            //          - If there is no processor assigned to the loan file then look at the loan officer�s 
            //              relationships for the processor.
            //          - If there is no processor from relationships then make the underwriter who is assigned 
            //              to the loan file the task owner.
            //          - If there is no assigned underwriter then get the underwriter from the loan officer�s relationships.
            //          - If there is no underwriter in the loan officer�s relationships then set the task as 
            //              "to be owned by� the underwriter.
            //
            // Logic determines the assigned user id and owner user id for condition that is hidden.
            //      - Assign the underwriter to the task and set them as the owner.
            //      - If there is no underwriter per the logic above then set the task as "to be assigned" and "to be owned by"
            //          the underwriter.

            // Per OPM 69471, we are changing the assignment logic.  Now, we have a flow chart.
            // 0. If condition is hidden, then Assign to UW if there is one, otherwise assign to UW role
            // 1. Else, if processor is assigned, assign to the processor
            // 2. Else, if a loan officer is assigned, and they have an associated processor, assign to processor.
            // 3. Else, if a loan officer is assigned, assign to the LO
            // 4. Else, assign to the importing user. (User running pricing)

            assignedUserId = Guid.Empty;
            ownerUserId = Guid.Empty;
            hiddenConditionAssignedUserId = Guid.Empty;
            hiddenConditionOwnerUserId = Guid.Empty;

            Guid assignedEmployeeId = Guid.Empty;
            Guid ownerEmployeeId = Guid.Empty;
            Guid hiddenConditionAssignedEmployeeId = Guid.Empty;
            Guid hiddenConditionOwnerEmployeeId = Guid.Empty;

            CPageData dataLoan = new CEmployeeData(sLId);
            dataLoan.InitLoad();
            bool hasAssignedUnderwriter = dataLoan.sEmployeeUnderwriterId != Guid.Empty;

            assignedEmployeeId = dataLoan.sEmployeeLoanRepId;

            #region Logic to determine non-hidden condition
            EmployeeDB loanOfficerEmployee = null;
            if (dataLoan.sEmployeeProcessorId != Guid.Empty)
            {
                assignedEmployeeId = dataLoan.sEmployeeProcessorId; // 1.
            }
            else
            {

                if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
                {
                    // 5/12/2011 dd - Look at Processor Relationship

                    loanOfficerEmployee = new EmployeeDB(dataLoan.sEmployeeLoanRepId, sBrokerId);
                    loanOfficerEmployee.Retrieve();
                    if (loanOfficerEmployee.ProcessorEmployeeID != Guid.Empty)
                    {
                        assignedEmployeeId = loanOfficerEmployee.ProcessorEmployeeID; // 2.
                    }
                    else
                    {
                        assignedEmployeeId = dataLoan.sEmployeeLoanRepId; //3.
                    }
                }
                else
                {
                    assignedEmployeeId = lpeRunningEmployeeId; //4.
                }
            }

            #region determine owner based on  changes from opm 80491 av 
            if (hasAssignedUnderwriter)
            {
                ownerEmployeeId = dataLoan.sEmployeeUnderwriterId;
            }
            else
            {
                if (dataLoan.sEmployeeLoanRepId != Guid.Empty)
                {
                    if (loanOfficerEmployee == null)
                    {
                        loanOfficerEmployee = new EmployeeDB(dataLoan.sEmployeeLoanRepId, sBrokerId);
                        loanOfficerEmployee.Retrieve();
                    }

                    if (loanOfficerEmployee.UnderwriterEmployeeID != Guid.Empty)
                    {
                        ownerEmployeeId = loanOfficerEmployee.UnderwriterEmployeeID;
                    }
                }
            }
            #endregion

            #endregion

            #region Logic to determine hidden condition
            if (hasAssignedUnderwriter)
            {
                hiddenConditionAssignedEmployeeId = dataLoan.sEmployeeUnderwriterId; // 0
                hiddenConditionOwnerEmployeeId = dataLoan.sEmployeeUnderwriterId;
            }
            #endregion
        
            assignedUserId = ConvertEmployeeIdToUserId(sBrokerId, assignedEmployeeId, hash);
            ownerUserId = ConvertEmployeeIdToUserId(sBrokerId, ownerEmployeeId, hash);
            hiddenConditionAssignedUserId = ConvertEmployeeIdToUserId(sBrokerId, hiddenConditionAssignedEmployeeId, hash);
            hiddenConditionOwnerUserId = ConvertEmployeeIdToUserId(sBrokerId, hiddenConditionOwnerEmployeeId, hash);

            // 6/13/2014 gf - Piggy backing this branch db check here in order 
            // to avoid hitting the db more than necessary.
            var branchDb = new BranchDB(dataLoan.sBranchId, dataLoan.sBrokerId);
            branchDb.Retrieve();
            isBranchAssociatedWithNewCP = branchDb.ConsumerPortalId.HasValue;
        }

        private Guid ConvertEmployeeIdToUserId(Guid brokerId, Guid employeeId, Dictionary<Guid, Guid> hash)
        {
            if (employeeId == Guid.Empty)
            {
                return Guid.Empty;
            }
            Guid userId = Guid.Empty;
            if (hash.TryGetValue(employeeId, out userId) == false)
            {
                EmployeeDB employee = new EmployeeDB(employeeId, brokerId);
                if (employee.Retrieve())
                {
                    userId = employee.UserID;
                }
                hash.Add(employeeId, userId);
            }
            return userId;

        }

        private void ConvertStipulationToDocumentRequest(
            Guid brokerId,
            Guid loanId,
            Guid employeeId,
            Toolbox.CStipulation stip,
            Dictionary<string, string> docTypeIdsByName)
        {
            if (stip == null)
            {
                throw new ArgumentNullException("stip");
            }
            else if (stip.AssignedToRole == null || stip.AssignedToRole.Value != E_RoleT.Consumer)
            {
                throw CBaseException.GenericException("This method should not be " +
                    "called when the stip is not assigned to the consumer.");
            }
            else if (string.IsNullOrEmpty(stip.RequiredDocTypeName))
            {
                var errorMessage = "Encountered stipulation without required doc type " +
                    "name. All stips with an assigned to value of consumer are " +
                    "expected to specify the doc type name.";
                Tools.LogError(errorMessage);
                return;
            }

            PendingDocumentRequest pendingDocumentRequest = null;

            if (stip.ApplicationId.HasValue)
            {
                pendingDocumentRequest = new LendersOffice.ObjLib.ConsumerPortal.PendingDocumentRequest(
                    DocumentRequestType.ReceiveDocumentFromBorrower,
                    loanId,
                    stip.ApplicationId.Value,
                    brokerId,
                    employeeId,
                    stip.Description);
            }
            else
            {
                // This probably represents a programming error. It could also 
                // be an issue on the SAE side.
                var errorMessage = "Encountered a doc request stipulation without " +
                "an associated application.";
                Tools.LogErrorWithCriticalTracking(errorMessage);
                return;
            }

            string id;

            if (docTypeIdsByName.TryGetValue(stip.RequiredDocTypeName, out id))
            {
                pendingDocumentRequest.SetDocType(int.Parse(id));
            }
            else
            {
                var errorMessage = string.Format(
                    "Unable to locate doc type name {0} for broker {1}. Not creating stip.",
                    stip.RequiredDocTypeName,
                    brokerId);
                Tools.LogError(errorMessage);
                return;
            }

            pendingDocumentRequest.Save();
        }

        private void ProcessStipulations(Guid brokerID, Guid sLId, CApplicantPrice product, string lenderAccExecEmail, string userName, Guid employeeId)
        {
            // 10/8/2004 dd - Copy stipulations from loan product to Conditions.
            // 10/8/2004 dd - Reason this part occur after the save and after send email to lender account exec email is
            // Lender might care more about who try to request a rate lock than if stipulations failed convert to conditions.
            // IF later we decide to include this section as part of whole transaction then we just move it up before dataLoan.Save();
            if (null == product)
                return; // Loan is submitted for manual underwriting.

            BrokerDB brokerDB = BrokerDB.RetrieveById(brokerID);

            if (brokerDB.IsUseNewTaskSystem)
            {
                // Load up all current condition.
                List<Task> conditionList = Task.GetAllConditionsByLoanId(brokerID, sLId);

                // Load up all Condition Category.
                Dictionary<string, ConditionCategory> conditionCategoryDictionary =
                    ConditionCategory.GetCategories(brokerID).ToDictionary(o => o.Category, StringComparer.OrdinalIgnoreCase);

                Toolbox.CSortedListOfGroups stipColl = product.Stips;

                Guid assignedUserId = Guid.Empty;
                Guid ownerUserId = Guid.Empty;
                Guid hiddenConditionAssignedUserId = Guid.Empty;
                Guid hiddenConditionOwnerUserId = Guid.Empty;
                Dictionary<Guid, Guid> hash = new Dictionary<Guid, Guid>();
                bool isBranchAssociatedWithNewCP = false;
                bool addedNewCondition = false;

                DetermineAssignedUserAndOwnerUser(
                    brokerID,
                    sLId,
                    out assignedUserId,
                    out ownerUserId,
                    out hiddenConditionAssignedUserId,
                    out hiddenConditionOwnerUserId,
                    employeeId,
                    hash,
                    out isBranchAssociatedWithNewCP);

                LoanAssignments loanAssignments = new LoanAssignments(brokerID, sLId);

                Guid? toBeAssignedRole = null;
                Guid? toBeOwnerRole = null;
                Guid? hiddenConditionToBeAssignedRole = null;
                Guid? hiddenConditionToBeOwnerRole = null;

                if (assignedUserId == Guid.Empty)
                {
                    toBeAssignedRole = CEmployeeFields.s_LoanRepRoleId;
                }
                if (ownerUserId == Guid.Empty)
                {
                    toBeOwnerRole = CEmployeeFields.s_UnderwriterRoleId;
                }
                if (hiddenConditionAssignedUserId == Guid.Empty)
                {
                    hiddenConditionToBeAssignedRole = CEmployeeFields.s_UnderwriterRoleId;
                }
                if (hiddenConditionOwnerUserId == Guid.Empty)
                {
                    hiddenConditionToBeOwnerRole = CEmployeeFields.s_UnderwriterRoleId;
                }

                var docTypes = EDocumentDocType.GetDocTypesByBroker(brokerID, E_EnforceFolderPermissions.True).ToDictionary(p => p.DocTypeName, p => p.DocTypeId, StringComparer.OrdinalIgnoreCase);

                // Only load these if necessary.
                IEnumerable<ReadOnlyPendingDocumentRequest> pendingDocumentRequests = null;
                IEnumerable<ReadOnlyConsumerActionItem> consumerActionItems = null;

                foreach (string category in stipColl.Keys)
                {
                    ConditionCategory conditionCategory = GetConditionCategory(brokerDB, category, conditionCategoryDictionary);

                    foreach (Toolbox.CStipulation stip in stipColl.GetGroupByKeyAndSort(category))
                    {
                        // 6/12/2014 gf - opm 174148, we now assume that any
                        // stip created by the SAE which is assigned to the
                        // consumer is creating a document request.
                        if (stip.AssignedToRole.HasValue && stip.AssignedToRole.Value == E_RoleT.Consumer)
                        {
                            // These are loaded here avoid unnecessarily hitting the database.
                            if (pendingDocumentRequests == null || consumerActionItems == null)
                            {
                                pendingDocumentRequests = PendingDocumentRequestHandler.GetRequestsForLoanId(sLId, brokerID);
                                consumerActionItems = ReadOnlyConsumerActionItem.GetConsumerActionItems(brokerID, sLId);
                            }

                            if (brokerDB.IsEnableNewConsumerPortal && !isBranchAssociatedWithNewCP)
                            {
                                continue;
                            }
                            else if (stip.ApplicationId.HasValue)
                            {
                                bool isDuplicateOfPendingRequest = pendingDocumentRequests
                                    .Any(r => r.ApplicationId == stip.ApplicationId.Value && r.Description == stip.Description);
                                bool isDuplicateOfConsumerActionItem = consumerActionItems
                                    .Any(i => i.AppId == stip.ApplicationId.Value && i.RequestDescription == stip.Description);

                                if (isDuplicateOfPendingRequest || isDuplicateOfConsumerActionItem)
                                {
                                    continue;
                                }
                            }

                            ConvertStipulationToDocumentRequest(brokerID, sLId, employeeId, stip, docTypes);
                        }
                        else if (conditionList.Exists(o => o.TaskSubject == stip.DescriptionForCondition && o.CondCategoryId.Value == conditionCategory.Id))
                        {
                            // Check for duplication.
                            continue;
                        }
                        else
                        {
                            Task task = Task.Create(sLId, brokerID);
                            task.EnforcePermissions = false; // Task system user does not need permission check.

                            task.CondCategoryId = conditionCategory.Id;
                            task.TaskSubject = stip.DescriptionForCondition;
                            task.TaskPermissionLevelId = conditionCategory.DefaultTaskPermissionLevelId;
                            task.TaskIsCondition = true;
                            task.TaskStatus = E_TaskStatus.Active;
                            task.CondIsHidden = false;
                            task.TaskCreatedByUserId = SystemUserPrincipal.TaskSystemUser.UserId;
                            task.TaskDueDateCalcField = "sEstCloseD";
                            task.TaskDueDateLocked = false;

                            string requiredDocTypeId;
                            if (!string.IsNullOrEmpty(stip.RequiredDocTypeName) && docTypes.TryGetValue(stip.RequiredDocTypeName, out requiredDocTypeId))
                            {
                                task.CondRequiredDocTypeId = int.Parse(requiredDocTypeId);
                            }

                            if (stip.AssignedToRole.HasValue)
                            {
                                Role r = Role.Get(stip.AssignedToRole.Value);
                                task.TaskToBeAssignedRoleId = r.Id;
                                if (loanAssignments.HasAssignment(r.RoleT))
                                {
                                    task.TaskAssignedUserId = ConvertEmployeeIdToUserId(brokerID, loanAssignments[r.Desc].EmployeeId, hash);
                                }
                            }
                            else
                            {
                                task.TaskAssignedUserId = assignedUserId;
                                task.TaskToBeAssignedRoleId = toBeAssignedRole;
                            }

                            if (stip.OwnerRole.HasValue)
                            {
                                Role r = Role.Get(stip.OwnerRole.Value);
                                task.TaskToBeOwnerRoleId = r.Id;

                                if (loanAssignments.HasAssignment(r.Desc))
                                {
                                    task.TaskOwnerUserId = ConvertEmployeeIdToUserId(brokerID, loanAssignments[r.Desc].EmployeeId, hash);
                                }
                            }
                            else
                            {
                                task.TaskOwnerUserId = ownerUserId;
                                task.TaskToBeOwnerRoleId = toBeOwnerRole;
                            }
                            task.Save(false);
                            addedNewCondition = true;
                        }
                    }
                }

                stipColl = product.HiddenStips;
                foreach (string category in stipColl.Keys)
                {
                    ConditionCategory conditionCategory = GetConditionCategory(brokerDB, category, conditionCategoryDictionary);

                    foreach (Toolbox.CStipulation stip in stipColl.GetGroupByKeyAndSort(category))
                    {
                        // 6/12/2014 gf - opm 174148, we now assume that any
                        // stip created by the SAE which is assigned to the
                        // consumer is creating a document request.
                        if (stip.AssignedToRole.HasValue && stip.AssignedToRole.Value == E_RoleT.Consumer)
                        {
                            // These are loaded here avoid unnecessarily hitting the database.
                            if (pendingDocumentRequests == null || consumerActionItems == null)
                            {
                                pendingDocumentRequests = PendingDocumentRequestHandler.GetRequestsForLoanId(sLId, brokerID);
                                consumerActionItems = ReadOnlyConsumerActionItem.GetConsumerActionItems(brokerID,sLId);
                            }

                            if (brokerDB.IsEnableNewConsumerPortal && !isBranchAssociatedWithNewCP)
                            {
                                continue;
                            }
                            else if (stip.ApplicationId.HasValue)
                            {
                                bool isDuplicateOfPendingRequest = pendingDocumentRequests
                                    .Any(r => r.ApplicationId == stip.ApplicationId.Value && r.Description == stip.Description);
                                bool isDuplicateOfConsumerActionItem = consumerActionItems
                                    .Any(i => i.AppId == stip.ApplicationId.Value && i.RequestDescription == stip.Description);

                                if (isDuplicateOfPendingRequest || isDuplicateOfConsumerActionItem)
                                {
                                    continue;
                                }
                            }

                            ConvertStipulationToDocumentRequest(brokerID, sLId, employeeId, stip, docTypes);
                        }
                        else if (conditionList.Exists(o => o.TaskSubject == stip.DescriptionForCondition && o.CondCategoryId.Value == conditionCategory.Id))
                        {
                            // Check for duplication.
                            continue;
                        }
                        else
                        {
                            Task task = Task.Create(sLId, brokerID);
                            task.EnforcePermissions = false; // Task system user does not need permission check.
                            task.TaskPermissionLevelId = conditionCategory.DefaultTaskPermissionLevelId;
                            task.CondCategoryId = conditionCategory.Id;
                            task.TaskSubject = stip.DescriptionForCondition;
                            task.TaskIsCondition = true;
                            task.TaskStatus = E_TaskStatus.Active;
                            task.CondIsHidden = true;

                            task.TaskCreatedByUserId = SystemUserPrincipal.TaskSystemUser.UserId;
                            task.TaskDueDateCalcField = "sEstCloseD";
                            task.TaskDueDateLocked = false;


                            string requiredDocTypeId;
                            if (!string.IsNullOrEmpty(stip.RequiredDocTypeName) && docTypes.TryGetValue(stip.RequiredDocTypeName, out requiredDocTypeId))
                            {
                                task.CondRequiredDocTypeId = int.Parse(requiredDocTypeId);
                            }

                            if (stip.AssignedToRole.HasValue)
                            {
                                Role r = Role.Get(stip.AssignedToRole.Value);
                                task.TaskToBeAssignedRoleId = r.Id;
                                if (loanAssignments.HasAssignment(r.RoleT))
                                {
                                    task.TaskAssignedUserId = ConvertEmployeeIdToUserId(brokerID, loanAssignments[r.Desc].EmployeeId, hash);
                                }
                            }
                            else
                            {
                                task.TaskAssignedUserId = hiddenConditionAssignedUserId;
                                task.TaskToBeAssignedRoleId = hiddenConditionToBeAssignedRole;
                            }

                            if (stip.OwnerRole.HasValue)
                            {
                                Role r = Role.Get(stip.OwnerRole.Value);
                                task.TaskToBeOwnerRoleId = r.Id;

                                if (loanAssignments.HasAssignment(r.Desc))
                                {
                                    task.TaskOwnerUserId = ConvertEmployeeIdToUserId(brokerID, loanAssignments[r.Desc].EmployeeId, hash);
                                }

                            }
                            else
                            {
                                task.TaskOwnerUserId = hiddenConditionOwnerUserId;
                                task.TaskToBeOwnerRoleId = hiddenConditionToBeOwnerRole;
                            }

                            task.Save(false);
                            addedNewCondition = true;
                        }
                    }
                }

                TaskUtilities.EnqueueTasksDueDateUpdate(brokerID, sLId);

                if (addedNewCondition)
                {
                    TaskUtilities.BumpLoanConditionSummaryVersionAndEnqueue(brokerID, sLId);
                }
            }
            else
            {
                try
                {
                    LoanConditionSetObsolete conditions = new LoanConditionSetObsolete();
                    conditions.RetrieveAll(sLId, true);

                    Toolbox.CSortedListOfGroups stipColl = product.Stips;

                    foreach (string category in stipColl.Keys)
                    {
                        foreach (Toolbox.CStipulation stip in stipColl.GetGroupByKeyAndSort(category))
                        {
                            conditions.AddUniqueCondition(category, stip.DescriptionForCondition);
                        }
                    }

                    stipColl = product.HiddenStips;
                    foreach (string category in stipColl.Keys)
                    {
                        foreach (Toolbox.CStipulation stip in stipColl.GetGroupByKeyAndSort(category))
                        {
                            conditions.AddUniqueHiddenCondition(category, stip.DescriptionForCondition);
                        }
                    }


                    conditions.Save(brokerID);
                }
                catch (Exception exc)
                {
                    string brokerName = userName;
                    Tools.LogErrorWithCriticalTracking("Fail to convert stips to conditions. Notify Lender if necessary. Broker: " + brokerName + ", LenderExcEmail " + lenderAccExecEmail + ", sLId=" + sLId, exc);
                    throw;
                }
            }
        }

        private ConditionCategory GetConditionCategory(BrokerDB brokerDB, string category, Dictionary<string, ConditionCategory> conditionCategoryDictionary)
        {
            ConditionCategory conditionCategory = null;
            if (conditionCategoryDictionary.TryGetValue(category, out conditionCategory) == false)
            {
                // 5/11/2011 dd - Create new condition category.
                conditionCategory = new ConditionCategory(brokerDB.BrokerID);
                conditionCategory.Category = category;
                if (brokerDB.DefaultTaskPermissionLevelIdForImportedCategories.HasValue)
                {
                    conditionCategory.DefaultTaskPermissionLevelId = brokerDB.DefaultTaskPermissionLevelIdForImportedCategories.Value;
                }
                conditionCategory.Save();
                conditionCategoryDictionary.Add(conditionCategory.Category, conditionCategory);
            }
            return conditionCategory;
        }
        private string GetCertificate(CPageData dataLoan, CApplicantPrice product, string requestedRate, string first_sLNm, bool isRateLockRequested, bool isLoadStipFromCondition, Guid priceGroupID, decimal parRate) 
        {

            using (MemoryStream stream = new MemoryStream(10000)) 
            {
                XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);

                CertificateXmlData.Generate(writer, dataLoan, product, requestedRate, E_sLienQualifyModeT.ThisLoan, System.Threading.Thread.CurrentPrincipal as LendersOffice.Security.AbstractUserPrincipal, first_sLNm, isRateLockRequested, isLoadStipFromCondition, priceGroupID, parRate);
                writer.Flush();

                stream.Seek(0, SeekOrigin.Begin);
                return System.Text.Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int) stream.Length);
            }

        }

        #region Render Certificate to HTML

        private string RenderCertificateToHtml(Guid brokerId, string xml) 
        {
            string xsltLocation = LendersOffice.ObjLib.Resource.ResourceManager.Instance.GetResourcePath(LendersOffice.ObjLib.Resource.ResourceType.PmlCertificateXslt, brokerId);

            string ret = "";
            try 
            {

                ret = XslTransformHelper.Transform(xsltLocation, xml, XsltParams);
            } 
            catch (Exception exc) 
            {
                // Only log error to us but don't want user to see error.
                Tools.LogError(exc);
            }
            return ret;


        }
        private XsltArgumentList XsltParams 
        {
            get 
            {
                string css = LendersOffice.ObjLib.Resource.ResourceManager.Instance.GetGenericResourceContents(LendersOffice.ObjLib.Resource.ResourceType.CertificateCss);
                XsltArgumentList args = new XsltArgumentList();
                args.AddParam("VirtualRoot", "", "");
                args.AddParam("GenericStyle", "", css);
                args.AddParam("Email", "", "True");
                return args;  
            }
        }
      
        #endregion

        /// <summary>
        /// /// WARNING: THERE IS ANOTHER DUPLICATE METHOD IN $PML/main/ConfirmationPage.cs
        /// </summary>
        /// <param name="submittingEmployeeId"></param>
        /// <param name="loanId"></param>
        private void SetAssignmentsOnSubmit(
            Guid brokerId,
            Guid submittingEmployeeId ,
            Guid loanId,
            bool isRequestingRateLock,
            Guid priceGroupID,
            E_BranchChannelT channelT,
            E_sCorrespondentProcessT correspondentProcessT)
        {
            // 8/25/2005 kb - Well, we need to do this assignment to
            // get underwriters hooked up.  Know this: having an uw
            // assigned typically locks out everyone else.  Some
            // lenders may not want to have their ae's locked out
            // when the loan is submitted with a float request.
            //
            // Also, we set the new assignments on the primary loan,
            // so that 80/20 submissions copy the assignments after
            // we fix them up.

            String whileDoing = "Assigning employee relationships.";

            try
            {
                var relationshipAssignmentHandler = new RelationshipAssignmentHandler(
                    brokerId,
                    loanId,
                    submittingEmployeeId);

                relationshipAssignmentHandler.AssignSubmissionRelationships(
                    priceGroupID,
                    isRequestingRateLock,
                    channelT,
                    correspondentProcessT);
            }
            catch (Exception e)
            {
                Tools.LogError("Failed to set assignments on submit for " + loanId + " while '" + whileDoing + "'.", e);
            }
        }



        private string SaveCertificate(Guid sLId, CApplicantPrice product, string requestedRate, string first_sLNm, bool isRateLockRequested, bool isLoadStipFromCondition, Guid priceGroupID, decimal parRate) 
        {
            // 12/5/2005 dd - The reason I have to do a separate save for sPmlCertXmlContent is because after assign the underwriter, the file might
            // get lock out to "P" user. Therefore as a work around I have to load the dataobject that will by pass security check and only save
            // certificate
            CPageData dataLoan = new CCertificateNoPermissionCheckData(sLId);
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            
            string xml = GetCertificate(dataLoan, product, requestedRate, first_sLNm, isRateLockRequested, isLoadStipFromCondition, priceGroupID, parRate);

            dataLoan.sPmlCertXmlContent = xml;
            dataLoan.Save();

            return xml;
        }

        private string LoadResultHtmlFromFileDB(string name) 
        {
            string ret = "";

            Action<FileInfo> readHandler = delegate (FileInfo fi)
            {
                using (StreamReader stream = new StreamReader(fi.FullName))
                {
                    ret = stream.ReadToEnd();
                }
            };

            try
            {
                FileDBTools.UseFile(E_FileDB.Temp, name, readHandler);
            }
            catch (FileNotFoundException)
            {
                // NO-OP
            }

            FileDBTools.Delete(E_FileDB.Temp, name);

            return ret;
        }
	}
}