using System;
using System.Collections;
using System.Xml;
using System.Xml.Serialization;
using DataAccess;
using LendersOffice.Security;
using LendersOffice.QuickPricer;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.DistributeUnderwriting
{
    public enum E_LpeTaskT 
    {
        SubmitRate = 0, // Submit Single Loan
        TemporaryAcceptFirstLoan = 1,
        SubmitRate80_20 = 2,
        VerifyFirstLoanProgram = 3,
        RunEngine = 4,
        RenderCertificate = 5,
        PerformInternalPricingAction = 6
    }

    [XmlRoot("task")]
    public class LpeTaskMessage 
    {
        private LpeTaskMessageHeader m_header = new LpeTaskMessageHeader();
        private CLpRunOptions m_options = new CLpRunOptions();
        private LpeTaskMessageRequestData m_requestData = new LpeTaskMessageRequestData();
        private string m_temporaryLpeTaskMessageXml = "";
        
        public LpeTaskMessage() {}

        public LpeTaskMessage(AbstractUserPrincipal principal, Guid requestId, E_LpeTaskT lpeTaskT, DateTime createdDate, 
            CLpRunOptions options, Guid loanId, ArrayList productIdList, bool isRequestingRateLock, string sProOFinPmt, 
            bool skipRateAdjError, E_sLienQualifyModeT sLienQualifyModeT, string investorName, string productCode, Guid logId) 
        {
            m_header = new LpeTaskMessageHeader(principal, requestId, lpeTaskT, createdDate, investorName, productCode);
            m_options = options;
            m_requestData = new LpeTaskMessageRequestData(loanId, productIdList, isRequestingRateLock, sProOFinPmt, skipRateAdjError, sLienQualifyModeT, investorName, logId);
        }

        public LpeTaskMessageHeader Header 
        {
            get { return m_header; }
            set { m_header = value; }
        }

        public CLpRunOptions Options 
        {
            get { return m_options; }
            set { m_options = value; }
        }

        public LpeTaskMessageRequestData RequestData 
        {
            get { return m_requestData; }
            set { m_requestData = value; }
        }

        [XmlElement]
        public string TemporaryLpeTaskMessageXml 
        {
            get { return m_temporaryLpeTaskMessageXml; }
            set { m_temporaryLpeTaskMessageXml = value; }
        }

        public QuickPricerLoanItem QuickPricerLoanItem { get; set; }

    }

    [XmlRoot("header")]
    public class LpeTaskMessageHeader 
    {
        private Guid m_brokerId;
        private Guid m_userId;
        private Guid m_employeeId;
        private Guid m_lpeRequestBatchId;
        private string m_userName;
        private DateTime m_createdDate;
        private DateTime m_expirationDate;
        private E_ApplicationT m_applicationT;
        private E_LpeTaskT m_lpeTaskT;
        private MessageLabel m_tempMessageLabel;
        private string m_investorName;
        private string m_productCode;

        private DateTime m_receivedFromDB;

        private int m_currentPartInBatch = 1;
        private int m_numberOfRequestsInBatch = 1;

        public LpeTaskMessageHeader() {}

        public LpeTaskMessageHeader(AbstractUserPrincipal principal, Guid lpeRequestBatchId, E_LpeTaskT lpeTaskT, DateTime createdDate, string investorName, string productCode) 
        {
            m_brokerId = principal.BrokerId;
            m_userId = principal.UserId;
            m_employeeId = principal.EmployeeId;
            m_userName = principal.DisplayName;
            m_applicationT = principal.ApplicationType;

            m_lpeRequestBatchId = lpeRequestBatchId;
            m_createdDate = createdDate;
            m_lpeTaskT = lpeTaskT;
            m_investorName = investorName;
            m_productCode = productCode;
        }

        [XmlAttribute]
        public bool IsRejectedPreviously { get; set; }

        [XmlAttribute]
        public int CurrentPartInBatch 
        {
            get { return m_currentPartInBatch; }
            set { m_currentPartInBatch = value; }
        }
    
        [XmlAttribute]
        public int NumberOfRequestsInBatch 
        {
            get { return m_numberOfRequestsInBatch; }
            set { m_numberOfRequestsInBatch = value; }
        }


        [XmlAttribute]
        public Guid BrokerId 
        {
            get { return m_brokerId; }
            set { m_brokerId = value; }
        }

        [XmlAttribute]
        public Guid UserId 
        {
            get { return m_userId; }
            set { m_userId = value; }
        }

        [XmlAttribute]
        public Guid EmployeeId 
        {
            get { return m_employeeId; }
            set { m_employeeId = value; }
        }

        [XmlAttribute]
        public Guid LpeRequestBatchId 
        {
            get { return m_lpeRequestBatchId; }
            set { m_lpeRequestBatchId = value; }
        }

        [XmlAttribute]
        public string UserName 
        {
            get { return m_userName; }
            set { m_userName = value; }
        }

        [XmlAttribute]
        public E_LpeTaskT LpeTaskT 
        {
            get { return m_lpeTaskT; }
            set { m_lpeTaskT = value; }
        }

        [XmlAttribute]
        public E_ApplicationT ApplicationT 
        {
            get { return m_applicationT; }
            set { m_applicationT = value; }
        }

        [XmlAttribute]
        public DateTime CreatedDate 
        {
            get { return m_createdDate; }
            set { m_createdDate = value; }
        }

        private bool m_isSyncExpiredDate = false;

        [XmlIgnore]
        public bool IsSyncExpiredDate 
        {
            get { return m_isSyncExpiredDate; }
            set { m_isSyncExpiredDate = value; }
        }

        private static Hashtable s_expiredDateHash = new Hashtable();
        private static object s_expiredDateHashLock = new object();

        private static DateTime GetExpiredDateByBatchId(Guid batchId) 
        {
            lock (s_expiredDateHashLock) 
            {
                DateTime ret = DateTime.MinValue;
                if (s_expiredDateHash.Contains(batchId)) 
                {
                    ret = (DateTime) s_expiredDateHash[batchId];
                }
                return ret;
            }
        }

        private static void PutExpiredDate(Guid batchId, DateTime dt) 
        {
            lock (s_expiredDateHashLock) 
            {
                s_expiredDateHash[batchId] = dt;
            }
        }

        [XmlAttribute]
        public DateTime ExpirationDate 
        {
            get 
            {
                if (IsSyncExpiredDate) 
                {
                    DateTime dt = GetExpiredDateByBatchId(LpeRequestBatchId);
                    if (dt == DateTime.MinValue) 
                    {
                        dt = m_expirationDate;
                        PutExpiredDate(LpeRequestBatchId, dt);
                    }
                    return dt;
                } 
                else 
                {
                    return m_expirationDate; 
                }
            }
            set 
            { 
                if (IsSyncExpiredDate) 
                {
                    PutExpiredDate(LpeRequestBatchId, value);
                } 
                else 
                {
                    m_expirationDate = value; 
                }
            }
        }

        [XmlAttribute]
        public string InvestorName 
        {
            get { return m_investorName; }
            set { m_investorName = value; }
        }

        [XmlAttribute]
        public string ProductCode 
        {
            get { return m_productCode; }
            set { m_productCode = value; }
        }
     

        [XmlIgnore]
        public string ProcessedServerList 
        {
            get 
            {
                string ret = "";
                foreach (string str in m_processedServerList) 
                {
                    ret += str + ",";
                }
                return ret;
            }
        }

        [XmlIgnore]
        public int ProcessedServersCount 
        {
            get { return m_processedServerList.Count; }
        }

        private string m_matchResult = "";

        [XmlIgnore]
        public string MatchResult 
        {
            get { return m_matchResult; }
            set { m_matchResult = value; }
        }

        private string m_lastCalcServerId = "";

        [XmlIgnore]
        public string LastCalcServerId 
        {
            get { return m_lastCalcServerId; }
        }


        private ArrayList m_processedServerList = new ArrayList();
        public void SetTriedBy(string calcServerId) 
        {
            m_lastCalcServerId = calcServerId;
            if (!m_processedServerList.Contains(calcServerId))
                m_processedServerList.Add(calcServerId);
        }

        public bool IsProcessedBy(string calcServerId) 
        {
            return m_processedServerList.Contains(calcServerId);
        }

        [XmlIgnore]
        public DateTime ReceivedFromDB 
        {
            get { return m_receivedFromDB; }
            set { m_receivedFromDB = value; }
        }

        [XmlIgnore]
        public MessageLabel TempMessageLabel 
        {
            get { return m_tempMessageLabel; }
            set { m_tempMessageLabel = value; }
        }

        [XmlAttribute]
        public string TempMessageLabelString 
        {
            get 
            {
                if (null != m_tempMessageLabel)
                    return m_tempMessageLabel.ToString(); 
                return "";
            }
            set { m_tempMessageLabel = MessageLabel.Parse(value); }
        }

        private DateTime m_timeInQueue; // For statistical purpose only.
        private DateTime m_timeFirstTouchByController = DateTime.MinValue; // For statistical purpose. This is the time controller first look at this request for calc server matching.
        private DateTime m_timeOutQueue; // For statistical purpose only.
        private DateTime m_timePickByCalcServer; // For statistical purpose only.

        [XmlAttribute]
        public DateTime TimeFirstTouchByController 
        {
            get { return m_timeFirstTouchByController; }
            set { m_timeFirstTouchByController = value; }
        }
        [XmlAttribute]
        public DateTime TimeInQueue 
        {
            get { return m_timeInQueue; }
            set { m_timeInQueue = value; }
        }

        [XmlAttribute]
        public DateTime TimeOutQueue 
        {
            get { return m_timeOutQueue; }
            set { m_timeOutQueue = value; }
        }

        [XmlAttribute]
        public DateTime TimePickByCalcServer 
        {
            get { return m_timePickByCalcServer; }
            set { m_timePickByCalcServer = value; }
        }

        private long m_lpeRequestIntId = 0;
        [XmlAttribute]
        public long LpeRequestIntId 
        {
            get { return m_lpeRequestIntId; }
            set { m_lpeRequestIntId = value; }
        }

    }

    public class LpeTaskMessageRequestData 
    {
        private Guid m_loanId;
        private bool m_isRequestingRateLock;
        private string m_sProOFinPmt;
        private bool m_skipRateAdjError;
        private ArrayList m_productIdList = new ArrayList();
        private E_sLienQualifyModeT m_sLienQualifyModeT = E_sLienQualifyModeT.ThisLoan;
        private string m_investorName = "";
        private Guid logId = Guid.Empty;

        public LpeTaskMessageRequestData() 
        {
        }

        public LpeTaskMessageRequestData(Guid loanId, ArrayList productIdList, bool isRequestingRateLock, string sProOFinPmt, bool skipRateAdjError,
            E_sLienQualifyModeT sLienQualifyModeT, string investorName, Guid logId) 
        {
            m_loanId = loanId;
            m_productIdList = productIdList;
            m_isRequestingRateLock = isRequestingRateLock;
            m_sProOFinPmt = sProOFinPmt;
            m_skipRateAdjError = skipRateAdjError;
            m_sLienQualifyModeT = sLienQualifyModeT;
            m_investorName = investorName;
            this.logId = logId;
        }

        [XmlAttribute]
        public Guid LoanId 
        {
            get { return m_loanId; }
            set { m_loanId = value; }
        }

        [XmlAttribute]
        public bool IsRequestingRateLock 
        {
            get { return m_isRequestingRateLock; }
            set { m_isRequestingRateLock = value; }
        }

        [XmlAttribute]
        public string sProOFinPmt 
        {
            get { return m_sProOFinPmt; }
            set { m_sProOFinPmt = value; }
        }

        [XmlAttribute]
        public bool SkipRateAdjError 
        {
            get { return m_skipRateAdjError; }
            set { m_skipRateAdjError = value; }
        }

        [XmlArray]
        [XmlArrayItem("productid", typeof(Guid))]
        public ArrayList ProductIdList 
        {
            get { return m_productIdList; }
        }

        [XmlAttribute]
        public E_sLienQualifyModeT sLienQualifyModeT 
        { 
            get { return m_sLienQualifyModeT; }
            set { m_sLienQualifyModeT = value; }
        }

        [XmlAttribute]
        public string InvestorName 
        {
            get { return m_investorName; }
            set { m_investorName = value; }
        }

        [XmlAttribute]
        public Guid LogId
        {
            get { return this.logId; }
            set { this.logId = value; }
        }
    }

}
