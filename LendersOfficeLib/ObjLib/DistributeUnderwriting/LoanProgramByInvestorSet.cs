/// Author: David Dao
namespace LendersOffice.DistributeUnderwriting
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using DataAccess;
    using DataAccess.LoanComparison;
    using LendersOffice.Admin;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.PriceGroups;
    using LendersOffice.ManualInvestorProductExpiration;
    using LendersOfficeApp.los.RatePrice;

    public class LoanProgramByInvestorSet
	{
        private Hashtable m_hashtable = new Hashtable(10);
        private List<Guid> m_onlyTheseProdIds = new List<Guid>();
        private Dictionary<string, HashSet<E_sLT>> productCodeToLoanType = new Dictionary<string, HashSet<E_sLT>>(StringComparer.OrdinalIgnoreCase);

        public List<Guid> OnlyTheseProdIds
        {
            get
            {
                // 9/28/2009 dd - Only use in Google Pricing Mode.
                return m_onlyTheseProdIds;
            }
        }
        
        public void Add(Guid lLpTemplateId, string lLpInvestorNm, string productCode, bool preserveProductCode = false, E_sLT? loanType = null) 
        {

            if (null == lLpInvestorNm)
                lLpInvestorNm = "";

            if (null == productCode)
                productCode = "";

            if (!preserveProductCode && !ConstStage.IsGroupLpeTaskByProductCode) 
            {
                productCode = "";
            }

            if (preserveProductCode && loanType.HasValue)
            {
                HashSet<E_sLT> typeHash;
                if (!productCodeToLoanType.TryGetValue(productCode, out typeHash))
                {
                    typeHash = new HashSet<E_sLT>();
                    productCodeToLoanType.Add(productCode, typeHash);
                }

                typeHash.Add(loanType.Value);
            }

            Hashtable productCodeHash = (Hashtable) m_hashtable[lLpInvestorNm];

            if (null == productCodeHash) 
            {
                productCodeHash = new Hashtable();
                m_hashtable[lLpInvestorNm] = productCodeHash;
            }

            ArrayList list = (ArrayList) productCodeHash[productCode];
            if (null == list) 
            {
                list = new ArrayList();
                productCodeHash[productCode] = list;
            }

            list.Add(lLpTemplateId);

            this.m_onlyTheseProdIds.Add(lLpTemplateId);

        }

        public ICollection InvestorList 
        {
            get { return m_hashtable.Keys; }
        }

        public Hashtable LoanProgramListByInvestor(string lLpInvestorNm) 
        {
            return (Hashtable) m_hashtable[lLpInvestorNm];
        }

        public int LoanProgramsCount 
        {
            get { return m_onlyTheseProdIds.Count; }
        }

        public Dictionary<string, HashSet<E_sLT>> ProductCodesToLoanType
        {
            get
            {
                return this.productCodeToLoanType;
            }
        }

        /// <summary>
        /// Returns the product ids of all programs in the same pricegroup that belong to the same rate merge group as the given product.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="priceGroupId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public static IEnumerable<Guid> GetProgramsInMergeGroup(DataSrc src, BrokerDB broker, Guid priceGroupId, Guid productId)
        {
            HashSet<Guid> programIds = new HashSet<Guid>() { productId };
            List<TemplateRowData> cache = new List<TemplateRowData>();
            RateMergeGroupKey key = null;
            Guid brokerId = broker.BrokerID;
            #region populate cache
            foreach (TemplateRowData row in TemplateRowData.List(src, broker, priceGroupId))
            {
                cache.Add(row);
                if (row.lLpTemplateId == productId)
                {
                    key = row.Key;
                }
            }
            #endregion
            
            List<InvestorProductItem> manualExcludeProductList = CLoanProgramsFromDb.GetManualExcludeProductList(brokerId, GetManualExcludeLockPolicyId(brokerId, priceGroupId));

            if (key != null)
            {
                foreach (var row in cache)
                {
                    if (key.Equals(row.Key) && !CLoanProgramsFromDb.IsContains(manualExcludeProductList, row.lLpInvestorNm, row.productCode))
                    {
                        programIds.Add(row.lLpTemplateId);
                    }
                }
            }

            return programIds;
        }

        /// <summary>
        /// Obtains the lock policy ID to be used for manually excluded product lists.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker.
        /// </param>
        /// <param name="priceGroupId">
        /// The ID of the price group.
        /// </param>
        /// <returns>
        /// The ID of the price group's lock policy if the price group is defined.<para></para>
        /// If no price group is specified and the broker has a default lock policy, that value is used.<para></para>
        /// Otherwise, no lock policy is specified and all manually disabled programs are used.
        /// </returns>
        private static Guid GetManualExcludeLockPolicyId(Guid brokerId, Guid priceGroupId)
        {
            if (priceGroupId != Guid.Empty)
            {
                var lockPolicy = PriceGroup.RetrieveByID(priceGroupId, brokerId).LockPolicyID;
                if (lockPolicy.HasValue)
                {
                    return lockPolicy.Value;
                }
            }

            var broker = BrokerDB.RetrieveById(brokerId);
            if (broker.DefaultLockPolicyID.HasValue)
            {
                return broker.DefaultLockPolicyID.Value;
            }

            return Guid.Empty;
        }

        /// <summary>
        /// Returns the programs grouped by their rate merge key.
        /// </summary>
        /// <param name="src">The data source to look into.</param>
        /// <param name="broker">The broker whose programs we are investigating.</param>
        /// <returns>The programs grouped by the rate merge key.</returns>
        public List<LoanProgramByInvestorSet> GetSetsByRateMergeGroup(DataSrc src, BrokerDB broker)
        {
            List<TemplateRowData> data =TemplateRowData.GetPrograms(src, broker, this.OnlyTheseProdIds);
            var rateMergeGroups = data.ToLookup(p => p.Key, p => p);
            List<LoanProgramByInvestorSet> sets = new List<LoanProgramByInvestorSet>();
            foreach (var grouping in rateMergeGroups)
            {
                var set = new LoanProgramByInvestorSet(); 
                foreach (TemplateRowData row in grouping)
                {
                    set.Add(row.lLpTemplateId, row.lLpInvestorNm, row.productCode);
                }
                sets.Add(set);
            }

            return sets;
        }
	}
}
