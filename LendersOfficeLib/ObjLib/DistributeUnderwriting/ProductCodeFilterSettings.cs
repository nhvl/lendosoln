﻿namespace LendersOffice.ObjLib.DistributeUnderwriting
{
    using System;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using LendersOffice.Admin;
    using LendersOffice.Security;

    /// <summary>
    /// Holds the product code filter availability settings.
    /// </summary>
    public class ProductCodeFilterSettings
    {
        /// <summary>
        /// The broker id.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// Whether the temp option is actually enabled or not.
        /// </summary>
        private bool tempOptionEnabled = false;

        /// <summary>
        /// The employee group to look for, if it exists.
        /// </summary>
        private string employeeGroup;

        /// <summary>
        /// The branch group to look for, if it exists.
        /// </summary>
        private string branchGroup;

        /// <summary>
        /// Mapping from employee id to associated employee groups.
        /// </summary>
        private Dictionary<Guid, List<Admin.Group>> employeeIdToMemberGroups = new Dictionary<Guid, List<Admin.Group>>();

        /// <summary>
        /// Mapping from branch id to associated branch groups.
        /// </summary>
        private Dictionary<Guid, List<Admin.Group>> branchIdToMemberGroups = new Dictionary<Guid, List<Admin.Group>>();

        /// <summary>
        /// Initializes a new instance of the <see cref="ProductCodeFilterSettings"/> class.
        /// </summary>
        /// <param name="brokerId">The broker id.</param>
        /// <param name="match">The regex match from the temp options.</param>
        public ProductCodeFilterSettings(Guid brokerId, Match match)
        {
            this.brokerId = brokerId;

            if (match.Success)
            {
                this.tempOptionEnabled = true;

                this.employeeGroup = match.Groups["EmployeeGroup"].Value;
                this.branchGroup = match.Groups["BranchGroup"].Value;
            }
        }

        /// <summary>
        /// Checks if the user is able to access and use the product code filters.
        /// </summary>
        /// <param name="principal">The principal to check against.</param>
        /// <returns>True if the filters are enabled, false otherwise.</returns>
        public bool IsEnabled(AbstractUserPrincipal principal)
        {
            if (this.tempOptionEnabled)
            {
                bool isBUser = principal.Type.Equals("b", StringComparison.OrdinalIgnoreCase);
                bool isPUser = principal.Type.Equals("p", StringComparison.OrdinalIgnoreCase);

                if ((isBUser && string.IsNullOrEmpty(this.employeeGroup) && string.IsNullOrEmpty(this.branchGroup)) ||
                    (isPUser && string.IsNullOrEmpty(this.branchGroup)))
                {
                    return true;
                }

                if (isBUser)
                {
                    if (!string.IsNullOrEmpty(this.employeeGroup))
                    {
                        if (this.CheckGroups(principal.EmployeeId, this.employeeGroup, this.employeeIdToMemberGroups, GroupDB.ListInclusiveGroupForEmployee))
                        {
                            return true;
                        }
                    }

                    if (!string.IsNullOrEmpty(this.branchGroup))
                    {
                        if (this.CheckGroups(principal.BranchId, this.branchGroup, this.branchIdToMemberGroups, GroupDB.ListInclusiveGroupForBranch))
                        {
                            return true;
                        }
                    }
                }
                else if (isPUser)
                {
                    if (!string.IsNullOrEmpty(this.branchGroup))
                    {
                        if (this.CheckGroups(principal.BranchId, this.branchGroup, this.branchIdToMemberGroups, GroupDB.ListInclusiveGroupForBranch))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Checks if the the specified group is in the available groups.
        /// </summary>
        /// <param name="idToSearchWith">The id to use when looking up available groups.</param>
        /// <param name="groupToSearchFor">The group to check the available groups against.</param>
        /// <param name="idToGroupMappings">The previously cached mapping from id to available groups.</param>
        /// <param name="groupRetrievalFunction">The retrieval function for groups in case the cache doesn't have it.</param>
        /// <returns>True if the group is in the available groups, false otherwise.</returns>
        private bool CheckGroups(Guid idToSearchWith, string groupToSearchFor, Dictionary<Guid, List<Admin.Group>> idToGroupMappings, Func<Guid, Guid, List<Admin.Group>> groupRetrievalFunction)
        {
            List<Admin.Group> foundGroups;
            if (!idToGroupMappings.TryGetValue(idToSearchWith, out foundGroups))
            {
                foundGroups = groupRetrievalFunction(this.brokerId, idToSearchWith);
                idToGroupMappings.Add(idToSearchWith, foundGroups);
            }

            foreach (var group in foundGroups)
            {
                if (group.GroupName.Equals(groupToSearchFor, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
