/// Author: David Dao

using System.Collections.Generic;
using System.Xml;
using LendersOffice.Admin;
using LendersOffice.Common;
using DataAccess;
using LendersOfficeApp.los.RatePrice;
using System.Collections;
using System;
using LendersOffice.ObjLib.PriceGroups;
using LendersOffice.ObjLib.LockPolicies;
namespace LendersOffice.DistributeUnderwriting
{
	public class LpeTaskProcessor
	{
        public static XmlDocument Execute(LpeTaskMessage task)
        {

            if (null == task)
                throw new GenericUserErrorMessageException("LpeTaskMessage is null in LpeTaskProcessor::Execute");

            #region request setup - clean out the cached thread stuff because the cache should be one per request not once per thread life. 
            // SEE OPM 140484
            CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.PriceGroupKey(task.Options.PriceGroupId), null);

            BrokerDB.RemoveBrokerFromCache(task.Header.BrokerId); 

            if (task.Options.PriceGroupId != Guid.Empty)
            {
                var priceGroup = PriceGroup.RetrieveByID(task.Options.PriceGroupId, task.Header.BrokerId);
                if (priceGroup.LockPolicyID.HasValue)
                {
                    CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.LockPolicyKey(priceGroup.LockPolicyID.Value), null);
                }

                CurrentRequestOrThreadCache.SetRequestOrThreadObjectByName(GeneratedKeyForRequestOrThreadCache.LockPolicyClosureListKey(task.Header.BrokerId), null);
            }
            #endregion

            XmlDocument result = null;

            switch (task.Header.LpeTaskT) 
            {
                case E_LpeTaskT.RunEngine:
                case E_LpeTaskT.RenderCertificate:
                    UnderwritingWorkerUnit underwriting = new UnderwritingWorkerUnit();
                    result = underwriting.Process(task);
                    break;
                case E_LpeTaskT.SubmitRate:
                case E_LpeTaskT.SubmitRate80_20:
                case E_LpeTaskT.TemporaryAcceptFirstLoan:
                case E_LpeTaskT.VerifyFirstLoanProgram:
                case E_LpeTaskT.PerformInternalPricingAction:
                    ApplyLoanWorkerUnit applyLoan = new ApplyLoanWorkerUnit();
                    result = applyLoan.Process(task);
                    break;
                default:
                    throw new UnhandledEnumException(task.Header.LpeTaskT);

            }
            return result;
        }

	}
}
