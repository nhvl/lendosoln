///
/// Author: David Dao
/// 
namespace LendersOffice.DistributeUnderwriting
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Threading;
    using System.Xml;
    using DataAccess;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.Gateways;
    using LendersOffice.Email;
    using LendersOffice.RatePrice.FileBasedPricing;
    using LendersOffice.Security;
    using LendersOfficeApp.los.RatePrice;
    using LendersOfficeApp.newlos.Template;
    using LendersOfficeApp.RatePrice.XmlResultPricingCmpUtils;

    public class UnderwritingWorkerUnit
    {
        static int s_pricingCmpCount = 0;
        static int s_pricingCmpSuccess = 0;
        static int s_pricingCmpException = 0;
        static int s_pricingOkForSameOrder = 0;

        static object s_lock = new object();

        private string RemoveDebugPricingLogNode(XmlDocument result)
        {
            string pricingDebugLog = string.Empty;

            XmlElement elePricingDebugLog = (XmlElement)result.SelectSingleNode("//DebugPricingLog");
            if (elePricingDebugLog != null)
            {
                if (elePricingDebugLog.ChildNodes.Count > 0 && (elePricingDebugLog.ChildNodes[0] is XmlCDataSection))
                {
                    pricingDebugLog = elePricingDebugLog.ChildNodes[0].Value;
                }
                elePricingDebugLog.ParentNode.RemoveChild(elePricingDebugLog);
            }

            return pricingDebugLog;
        }

        void CmpWithHistoryOption(LpeTaskMessage task, XmlDocument mainResult)
        {
            string debugPricingLogForMainResult = ConstSite.EnableFilebasedCmp ? RemoveDebugPricingLogNode(mainResult) : string.Empty;

            if (ConstSite.EnableFilebasedCmp == false)
            {
                return;
            }

            RatePrice.HybridLoanProgramSetOption option = task.Options.HistoricalPricingOption;
            if (option.IsHistoricalMode())
            {
                return;
            }

            Guid loanId = task.RequestData.LoanId;
            bool cmpResult = false;
            bool cmpResultForSameOrder = false;
            bool isThreadAbort = false;
            bool isException = false;
            string inputLog = string.Empty;
            try
            {
                var broker = BrokerDB.RetrieveById(task.Header.BrokerId);
                //Create new HybridLoanProgramSetOption object
                RatePrice.HybridLoanProgramSetOption historyOpt = RatePrice.HybridLoanProgramSetOption.GetCurrentSnapshotOption(broker);
                historyOpt.RateOptions = ConstSite.RateOptionHistory;
                historyOpt.LoanProgramTemplate = ConstSite.LoanProgramTemplateHistory;
                historyOpt.ApplicablePolicies = ConstSite.LoanProgramTemplateHistory;
                historyOpt.SnapshotContentKey = broker.GetLatestReleaseInfo().FileKey.Value;

                task.Options.HistoricalPricingOption = historyOpt;


                // log "input data"
                StringBuilder sb = new StringBuilder();

                sb.AppendFormat("Filebased Options: LoanProgramTemplate {0}, RateOption {1}, ApplicablePolicies {2}{3}",
                                task.Options.HistoricalPricingOption.LoanProgramTemplate,
                                task.Options.HistoricalPricingOption.RateOptions,
                                task.Options.HistoricalPricingOption.ApplicablePolicies,
                                Environment.NewLine);

                if (ConstSite.ManualCompareEnabled)
                {
                    sb.AppendFormat("***Waring: ConstSite.ManualCompareEnabled: {0}{1}", ConstSite.ManualCompareEnabled, Environment.NewLine);
                }

                sb.AppendFormat("PriceGroupId: {0}{1}", task.Options.PriceGroupId, Environment.NewLine);
                sb.AppendLine($"BrokerId: {task.Header.BrokerId}");
                sb.AppendLine($"UserId: {task.Header.UserId}");
                sb.AppendLine($"FileKey: {historyOpt.SnapshotContentKey}");
                sb.AppendLine($"OrgFileKey: {option.SnapshotContentKey}");

                // we want to get same order when application calls GetLoanProgramSet(priceGroupId, onlyTheseProdIds)
                HashSet<Guid> onlyTheseProdIds = new HashSet<Guid>();
                foreach (Guid id in task.RequestData.ProductIdList)
                {
                    onlyTheseProdIds.Add(id);
                }

                sb.AppendFormat("Loan product(s) : {0}", onlyTheseProdIds.Count).AppendLine();
                foreach (Guid productId in onlyTheseProdIds)
                {
                    sb.AppendFormat("   {0}{1}", productId, Environment.NewLine);
                }
                sb.AppendLine();

                inputLog = sb.ToString();
                sb.Clear();

                // Now RunEngine WithHistoryOption()
                Tools.LogInfo("**** start RunEngine WithHistoryOption(), loanId " + loanId.ToString());
                XmlDocument result = RunEngine(task);
                Tools.LogInfo("**** end RunEngine WithHistoryOption(), loanId " + loanId.ToString());

                string pricingDebugLog = RemoveDebugPricingLogNode(result);

                string outputStr = "";
                cmpResult = XmlResultPricingCmp.Compare(mainResult.OuterXml, result.OuterXml, out outputStr);

                string resultFname1 = string.Empty;
                string resultFname2 = string.Empty;
                string cmpResultFname = string.Empty;
                string errSignalFile = string.Empty;
                string folder = string.Empty;
                if (ConstSite.ManualCompareEnabled)
                {
                    folder = ConstSite.FilebasedFolder;
                }

                if (cmpResult == false)
                {
                    folder = Path.Combine(ConstSite.FilebasedFolder, "Result_" +  Guid.NewGuid().ToString() );
                }

                string logMsg = string.Empty;
                if (string.IsNullOrEmpty(folder) == false)
                {
                    if (Directory.Exists(folder) == false)
                    {
                        Directory.CreateDirectory(folder);
                    }

                    resultFname1 = Path.Combine(folder,  "pricingResult.xml");
                    resultFname2 = Path.Combine(folder, "pricingResult-Filebased.xml");
                    cmpResultFname = Path.Combine(folder, "CmpResult.txt");
                    errSignalFile = Path.Combine(folder, "HaveError.txt");

                    FileOperationHelper.Delete(resultFname1);
                    FileOperationHelper.Delete(resultFname2);
                    FileOperationHelper.Delete(cmpResultFname);
                    FileOperationHelper.Delete(errSignalFile);

                    XmlResultPricingCmp.XmlToFile(resultFname1, mainResult.OuterXml);
                    XmlResultPricingCmp.XmlToFile(resultFname2, result.OuterXml);

                    sb.AppendFormat("With loanId {4}, two following Xml Pricing Result files are {0}.{1}{1}{2}{1}{3}{1}{1}",
                                   (cmpResult ? "same" : "different"), Environment.NewLine, resultFname1, resultFname2, loanId);
                    sb.Append(inputLog);

                    if (cmpResult == false)
                    {
                        sb.AppendFormat("{0}{0}The DatabaseSnapshot vs FilebasedSnapshot error(s){0}{1}{0}", Environment.NewLine, outputStr); 
                    }
                    else if( string.IsNullOrEmpty(outputStr) == false)
                    {
                        sb.AppendFormat("{0}{0}The comparison log:{0}{1}{0}", Environment.NewLine, outputStr);
                    }

                    logMsg = sb.ToString();

                    // Append Filebased pricingDebugLog 
                    sb.AppendLine("*********** FilebasedSnapshot Pricing Log ***********").AppendLine();
                    sb.AppendLine(pricingDebugLog).AppendLine();

                    sb.AppendLine("*********** DatabaseSnapshot Pricing Log ***********").AppendLine();
                    sb.AppendLine(debugPricingLogForMainResult).AppendLine();

                    TextFileHelper.WriteString(cmpResultFname, sb.ToString(), false);

                    // release memory asap
                    sb.Clear();
                    pricingDebugLog = string.Empty;
                    debugPricingLogForMainResult = string.Empty;
                    result = null;

                    if (cmpResult == false && historyOpt.UsingSamePolicyOrderEnabled == false)
                    {
                        TextFileHelper.WriteString(errSignalFile, "The Xml results are different.", false); // create file HaveError.txt

                        //try to compare with same policy order
                        historyOpt.UsingSamePolicyOrderEnabled = true;

                        Tools.LogInfo("**** start RunEngine WithHistoryOption() with same policy order, loanId " + loanId.ToString());
                        XmlDocument resultForSameOrder = RunEngine(task);
                        Tools.LogInfo("**** end RunEngine WithHistoryOption() with same policy order, loanId " + loanId.ToString());

                        string pricingDebugLogForSameOrder = RemoveDebugPricingLogNode(resultForSameOrder);

                        string resultFname3 = Path.Combine(folder, "pricingResult-Filebased-samePolicyOrder.xml");
                        XmlResultPricingCmp.XmlToFile(resultFname3, resultForSameOrder.OuterXml);

                        cmpResultForSameOrder = XmlResultPricingCmp.Compare(mainResult.OuterXml, resultForSameOrder.OuterXml, out outputStr);

                        sb.AppendFormat("With loanId {4}, two following Xml Pricing Result files are {0}.{1}{1}{2}{1}{3}{1}{1}",
                                       (cmpResultForSameOrder ? "same" : "different"), Environment.NewLine, resultFname1, resultFname3, loanId);

                        sb.AppendFormat("{0}{0}The comparison log:{0}{1}{0}", Environment.NewLine, outputStr);

                        sb.AppendLine("*********** FilebasedSnapshot Pricing Log ***********").AppendLine();
                        sb.AppendLine(pricingDebugLogForSameOrder).AppendLine();

                        string fname = Path.Combine(folder, cmpResultForSameOrder ? "CmpResult-OkForSamePolicyOrder.txt" : "CmpResult-FailForSamePolicyOrder.txt");
                        TextFileHelper.WriteString(fname, sb.ToString(), false);

                        sb.Clear();

                        logMsg = logMsg + Environment.NewLine + "If use same policy order, the new result : " + (cmpResultForSameOrder ? "same" : "different");
                    }
                }
                else
                {
                    logMsg = "With loanId " + loanId + ", two Xml Pricing Results are same";
                }

                if (cmpResult)
                {
                    Tools.LogInfo(logMsg);
                }
                else
                {
                    Tools.LogError(logMsg);

                }


                if (cmpResult == false && ConstSite.ManualCompareEnabled == false 
                    && string.IsNullOrEmpty(ConstSite.FilebasedDeveloperEmail) == false) //need to email
                {
                    CBaseEmail email = new CBaseEmail (ConstAppDavid.SystemBrokerGuid)
                    {
                        Subject = String.Format("XmlPrincingResults are different for loanId {0}.", loanId),

                        From = ConstStage.DefaultDoNotReplyAddress,
                        To = ConstSite.FilebasedDeveloperEmail,
                        Bcc = "",
                        Message = logMsg,
                        IsHtmlEmail = false,
                        DisclaimerType = LendersOffice.Common.E_DisclaimerType.NORMAL
                    };

                    EmailUtilities.SendEmail(email);
                }

            }
            catch( Exception ex)
            {
                Tools.LogError("CmpWithHistoryOption() failed", ex);
                cmpResult = false;
                isThreadAbort = (ex is ThreadAbortException);
                isException = true;
                bool isSystemException = (ex is SystemException);

                string exceptionContent = "With loanId " + loanId + ", CmpWithHistoryOption() failed." + Environment.NewLine
                                      + inputLog + Environment.NewLine
                                      + "Exception type: " + ex.GetType().FullName + Environment.NewLine + Environment.NewLine
                                      + "Error message : " + ex.Message + Environment.NewLine + ex.StackTrace;

                string exceptionType = ex.GetType().FullName;
                int idx = exceptionType.LastIndexOf(".");
                string fnamePrefix = (idx >= 0) ? "Result" + exceptionType.Substring(idx + 1) + "_" : "ResultException_";

                string exceptionLogFile = Path.Combine(ConstSite.FilebasedFolder, fnamePrefix + Guid.NewGuid().ToString() + ".txt");
                TextFileHelper.WriteString(exceptionLogFile, exceptionContent, false);

                if (isThreadAbort == false)
                {
                    if (string.IsNullOrEmpty(ConstSite.FilebasedDeveloperEmail) == false && isSystemException)
                    {
                        CBaseEmail email = new CBaseEmail (ConstAppDavid.SystemBrokerGuid)
                        {
                            Subject = String.Format("[Exception] CmpWithHistoryOption() has some error for loanId {0}.", loanId),

                            From = ConstStage.DefaultDoNotReplyAddress,
                            To = ConstSite.FilebasedDeveloperEmail,
                            Bcc = "",
                            Message = exceptionContent,
                            IsHtmlEmail = false,
                            DisclaimerType = LendersOffice.Common.E_DisclaimerType.NORMAL
                        };

                        EmailUtilities.SendEmail(email);
                    }
                }

                if ( !isSystemException)
                {
                    throw;
                }
            }
            finally
            {
                task.Options.HistoricalPricingOption = option;

                if (isThreadAbort == false)
                {
                    lock (s_lock)
                    {
                        s_pricingCmpCount++;
                        if (cmpResult)
                        {
                            s_pricingCmpSuccess++;
                        }
                        else if (isException)
                        {
                            s_pricingCmpException++;
                        }
                        else if(cmpResultForSameOrder)
                        {
                            s_pricingOkForSameOrder++;
                        }

                        if ((s_pricingCmpCount % 10) == 0 || s_pricingCmpCount == 1 || ConstSite.ManualCompareEnabled)
                        {
                            int failCount = s_pricingCmpCount - s_pricingCmpSuccess - s_pricingCmpException;
                            double failPercent = 100.0 * failCount / s_pricingCmpCount;
                            double exceptionPercent = 100.0 * s_pricingCmpException / s_pricingCmpCount;
                            double successIfSamePolicyOrderPercent = 100.0 * s_pricingOkForSameOrder / s_pricingCmpCount;

                            string statisticMsg = string.Format("Run {0:N0} filebased pricings, {1:N0} failed ({2:0.##}%), {3:N0} exception ({4:0.##}%), {5:N0} success ({6:0.##}%)",
                                                                s_pricingCmpCount,
                                                                failCount, failPercent,
                                                                s_pricingCmpException, exceptionPercent,
                                                                s_pricingCmpSuccess, 100 - failPercent- exceptionPercent);
                            statisticMsg += ", success if same policy order: " + successIfSamePolicyOrderPercent.ToString("0.##") + "%";
                            Tools.LogInfo(statisticMsg);
                            FileBasedSnapshot.WriteToSummaryFile(statisticMsg);
                        }
                    }
                }
            }
        }

        public XmlDocument Process(LpeTaskMessage task)
        {
            XmlDocument result = null;

            switch (task.Header.LpeTaskT)
            {
                case E_LpeTaskT.RunEngine:
                    result = RunEngine(task);
                    CmpWithHistoryOption(task, result);                        
                    break;
                case E_LpeTaskT.RenderCertificate:
                    result = RenderCertificate(task);
                    break;
                default:
                    throw new CBaseException(ErrorMessages.Generic, "Unhandle LpeTaskT=" + task.Header.LpeTaskT + " in UnderwritingWorkerUnit.Process");
            }
            return result;
        }

        private XmlDocument RunEngine(LpeTaskMessage task)
        {
            Stopwatch debugStopwatch = Stopwatch.StartNew();
            using (PerformanceStopwatch.Start("UnderwritingWorkerUnit.RunEngine"))
            {
                HashSet<Guid> onlyTheseProdIds = new HashSet<Guid>();
                foreach (Guid id in task.RequestData.ProductIdList)
                {
                    onlyTheseProdIds.Add(id);
                }

                // 1/3/2014 AV - 148173 QM Par Rate Inconsistent Between RunPMLForRegisteredLoans And PML Submission
                //if pricing was requested for a single program then compute the par rate in case we need it earlier.
                Guid? selectedProductId = null;
                if (task.RequestData.ProductIdList.Count == 1 && !task.Options.IsAutoProcess)
                {
                    selectedProductId = (Guid)task.RequestData.ProductIdList[0];
                    BrokerDB brokerDB = BrokerDB.RetrieveById(task.Header.BrokerId);
                    foreach (Guid id in LoanProgramByInvestorSet.GetProgramsInMergeGroup(brokerDB.GetLatestReleaseInfo().DataSrcForSnapshot,
                        brokerDB, task.Options.PriceGroupId, selectedProductId.Value))
                    {
                        onlyTheseProdIds.Add(id);
                    }
                }
      
      
                CPriceGenerator priceGenerator = null;

                using (PerformanceStopwatch.Start("UnderwritingWorkerUnit.RunEngine - Executing"))
                {
                    if (task.TemporaryLpeTaskMessageXml != "")
                    {
                        // 7/18/2006 dd - In the rerun scenario, run TemporaryAcceptFirstLoan.
                        ApplyLoanWorkerUnit worker = new ApplyLoanWorkerUnit();
                        CPageData dataLoan = worker.TemporaryAcceptFirstLoanWithoutSave((LpeTaskMessage)SerializationHelper.XmlDeserialize(task.TemporaryLpeTaskMessageXml, typeof(LpeTaskMessage)), task.Options.PricingState);

                        priceGenerator = new CPriceGenerator(dataLoan, onlyTheseProdIds,
                            task.Options,
                            task.RequestData.sLienQualifyModeT);
                    }
                    else
                    {
                        BrokerDB brokerDB = BrokerDB.RetrieveById(task.Header.BrokerId);

                        priceGenerator = CPriceGenerator.ExecutePricing(brokerDB, task, onlyTheseProdIds);
                    }
                }

                XmlDocument doc = new XmlDocument();
                XmlElement root = doc.CreateElement("results");
                doc.AppendChild(root);
         
                // 1/3/2014 AV - 148173 QM Par Rate Inconsistent Between RunPMLForRegisteredLoans And PML Submission
                // In the scenario when the user only has run submitted program then I decided to include all the other programs
                // in the rate merge group in another node in the xml CApplicantPricesNotRequested
                // PML 2.0 will look at this node in said scenario when generating the results to compute the par rate. Everything else should ignore it.

                XmlElement elApplicantPrices = doc.CreateElement("CApplicantPrices");
                root.AppendChild(elApplicantPrices);
                
                XmlElement debugApplicantPrices = doc.CreateElement("CApplicantPricesNotRequested");
                root.AppendChild(debugApplicantPrices);
               
                StringBuilder sb = new StringBuilder();
                using (PerformanceStopwatch.Start("UnderwritingWorkerUnit.RunEngine - Convert CApplicantPrice to XML"))
                {
                    foreach (CApplicantPrice p in priceGenerator.GetApplicantPriceList())
                    {
                        if (p == null)
                        {
                            continue;
                        }

                        XmlElement holder = elApplicantPrices;
                        if (selectedProductId.HasValue && selectedProductId.Value != p.lLpTemplateId)
                        {
                            holder = debugApplicantPrices;
                        }
                        
                        XmlElement el = CApplicantPriceXml.ToXmlElement(p, doc);
                        holder.AppendChild(el);

                        if (ConstSite.EnableFilebasedCmp)
                        {
                            sb.AppendLine($"{Environment.NewLine}CApplicantPrice: {p.Status}{Environment.NewLine}ExecutedSequence: {p.ExecutedSequence}");
                            sb.AppendLine(p.InternalDebugInfo);
                        }
                    }
                }

                if (ConstSite.EnableFilebasedCmp && ( string.IsNullOrEmpty(priceGenerator.PricingDebugLog) == false || sb.Length > 0) )
                {
                    if (string.IsNullOrEmpty(priceGenerator.PricingDebugLog) == false)
                    {
                        sb.Insert(0, Environment.NewLine);
                        sb.Insert(0, priceGenerator.PricingDebugLog);
                        sb.AppendLine();
                    }

                    XmlElement elDebugPricingLog = doc.CreateElement("DebugPricingLog");
                    root.AppendChild(elDebugPricingLog);

                    sb.Replace("]]>", "]] >"); //  ]]> cannot appear within a CDATA section
                    elDebugPricingLog.AppendChild(doc.CreateCDataSection(sb.ToString()));
                }

                if (ConstSite.DisplayPolicyAndRuleIdOnLpeResult)
                {
                    Tools.LogInfo(PerformanceStopwatch.ReportOutput);
                }

                return doc;
            }


        }

        private XmlDocument RenderCertificate(LpeTaskMessage task)
        {
            HashSet<Guid> onlyTheseProdIds = new HashSet<Guid>();

            if (task.RequestData.ProductIdList.Count > 1)
            {
                Tools.LogBug("Render Certificate called with more than one program.");
            }

            Guid programId = (Guid)task.RequestData.ProductIdList[0];
            BrokerDB brokerDB = BrokerDB.RetrieveById(task.Header.BrokerId);
            var programs = LoanProgramByInvestorSet.GetProgramsInMergeGroup(brokerDB.GetLatestReleaseInfo().DataSrcForSnapshot,
                brokerDB, task.Options.PriceGroupId,programId) ;

            foreach (Guid id in programs)
            {
                onlyTheseProdIds.Add(id);
            }


            decimal parRate;
            CPageData dataLoan = null;
            CPriceGenerator priceGenerator = null;

            if (task.TemporaryLpeTaskMessageXml != "")
            {
                // 7/18/2006 dd - In the rerun scenario, run TemporaryAcceptFirstLoan.
                LpeTaskMessage tmpLpeTaskMessage = (LpeTaskMessage)SerializationHelper.XmlDeserialize(task.TemporaryLpeTaskMessageXml, typeof(LpeTaskMessage));

                ApplyLoanWorkerUnit worker = new ApplyLoanWorkerUnit();
                dataLoan = worker.TemporaryAcceptFirstLoanWithoutSave(tmpLpeTaskMessage);
                priceGenerator = new CPriceGenerator(dataLoan, onlyTheseProdIds, task.Options, task.RequestData.sLienQualifyModeT);

                // 7/19/2006 dd - THIS IS VERY INEFFICIENT. However since CPriceGenerator create unwanted side effect to dataloan, we must rerun TemporaryAcceptFirstLoan to restore the wanted state.
                dataLoan = worker.TemporaryAcceptFirstLoanWithoutSave(tmpLpeTaskMessage);
                parRate = ApplyLoanWorkerUnit.GetParRate(dataLoan, priceGenerator, (Guid)task.RequestData.ProductIdList[0]);
            }
            else
            {


                priceGenerator = CPriceGenerator.ExecutePricing(brokerDB, task, onlyTheseProdIds);

                dataLoan = new CCertificateData(task.RequestData.LoanId);
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
                dataLoan.AllowLoadWhileQP2Sandboxed = true;
                dataLoan.InitLoad();
                if (task.Options.PricingState != null)
                {
                    dataLoan.SetPricingState(task.Options.PricingState);
                }
                parRate = ApplyLoanWorkerUnit.GetParRate(dataLoan, priceGenerator, (Guid)task.RequestData.ProductIdList[0]);
            }
            CApplicantPrice product = null;

            foreach (CApplicantPrice tempProduct in priceGenerator.GetApplicantPriceList())
            {
                if (tempProduct.lLpTemplateId == programId)
                {
                    product = tempProduct;
                }
            }

            if (product == null)
            {
                throw CBaseException.GenericException("Did not find program in results.");
            }


            if (string.IsNullOrEmpty(task.Options.UniqueChecksum) == false)
            {
                // 1/21/2011 dd - We are doing case sensitive string search here.
                if (
                    (task.RequestData.sLienQualifyModeT == E_sLienQualifyModeT.OtherLoan &&
                        task.Options.SecondLienUniqueChecksum.Equals(product.UniqueChecksum, StringComparison.Ordinal) == false)
                    &&
                    task.Options.UniqueChecksum.Equals(product.UniqueChecksum, StringComparison.Ordinal) == false)
                {
                    AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;

                    if (product.Status == E_EvalStatus.Eval_Ineligible && (principal != null && principal.HasPermission(Permission.CanApplyForIneligibleLoanPrograms) == false))
                    {
                        // 3/18/2011 dd - eOPM 281108 - When a user does not have permission to apply for ineligible rate 
                        // and program is ineligible then we do not want to perform checksum check. The reason is
                        // during previous result the pricing may not run through all the adjustment therefore will
                        // display incorrect message to user.

                        // NO-OP
                    }
                    else
                    {
                        DateTime dt = DateTime.MinValue;
                        try
                        {

                            dt = new DateTime(task.Options.DebugResultStartTicks);
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                        }
                        throw new CInvalidRateVersionException(task.Options.UniqueChecksum, product.UniqueChecksum, dataLoan.sLId, dt);
                    }
                }
            }

            bool isLoadStipFromCondition = false;
            dataLoan.sPricingModeT = task.Options.PricingModeT;

            switch (dataLoan.sPricingModeT)
            {
                case E_sPricingModeT.InternalBrokerPricing:
                case E_sPricingModeT.InternalInvestorPricing:
                case E_sPricingModeT.EligibilityBrokerPricing:
                case E_sPricingModeT.EligibilityInvestorPricing:
                    isLoadStipFromCondition = false; // 1/19/2011 dd - Certificate for internal pricing always display stipulations.
                    break;
                case E_sPricingModeT.Undefined:
                case E_sPricingModeT.RegularPricing:
                    if (dataLoan.sLpTemplateId != Guid.Empty && dataLoan.sNoteIRSubmitted == 0 &&
                        dataLoan.sRateLockStatusT != E_sRateLockStatusT.Locked)
                    {
                        // 5/7/2009 dd - When view certificate in rerun mode, alway display condition from loan.
                        isLoadStipFromCondition = true;
                    }

                    break;

                default:
                    throw new UnhandledEnumException(dataLoan.sPricingModeT);
            }
            
            this.SubmitLoanToRenderCertificate(task, dataLoan, product);

            XmlDocument doc = new XmlDocument();
            using (MemoryStream stream = new MemoryStream(10000))
            {

                XmlTextWriter writer = new XmlTextWriter(stream, System.Text.Encoding.ASCII);

                CertificateXmlData.Generate(writer, dataLoan, product, task.Options.SelectedRateFormat, task.RequestData.sLienQualifyModeT, System.Threading.Thread.CurrentPrincipal as LendersOffice.Security.AbstractUserPrincipal, null,
                    false /* isRateLockRequested - This is render certificate therefore ratelock request is false */, isLoadStipFromCondition,
                    task.Options.PriceGroupId, parRate);
                writer.Flush();

                stream.Seek(0, SeekOrigin.Begin);
                doc.Load(stream);

            }


            return doc;
        }

        /// <summary>
        /// Submits the loan from PML to set data for the cert.
        /// </summary>
        /// <param name="task">
        /// The pricing request.
        /// </param>
        /// <param name="dataLoan">
        /// The loan to submit from PML.
        /// </param>
        /// <param name="product">
        /// The selected pricing product.
        /// </param>
        private void SubmitLoanToRenderCertificate(LpeTaskMessage task, CPageData dataLoan, CApplicantPrice product)
        {
            if (dataLoan.sIsRenovationLoan)
            {
                // Use the product and rate option when generating a cert for renovation loans.
                string unused = null;
                var requestedRate = task.Options.SelectedRateFormat;
                var selectedRateOption = CertificateXmlData.GetRateOptionForCertificate(product, requestedRate, out unused, out unused, out unused);

                if (selectedRateOption != null)
                {
                    // In the unlikely event that the rate option is null, we'll fall back to the existing behavior.
                    dataLoan.SubmitFromPML(product, selectedRateOption, isRequestingRateLock: false);
                    return;
                }
            }

            dataLoan.SubmitFromPML(product, 0, 0, 0, 0, 0, false, null);
        }
    }
}
