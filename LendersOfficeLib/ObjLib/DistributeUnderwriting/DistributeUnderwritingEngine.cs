///
/// Author: David Dao
/// 
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Messaging;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using DataAccess;
using DataAccess.FeeService;
using DataAccess.LoanComparison;
using DataAccess.LoanValidation;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.QuickPricer;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using LendersOffice.RatePrice;
using CommonProjectLib.Common.Lib;
using LqbGrammar.DataTypes;

namespace LendersOffice.DistributeUnderwriting
{

    public static class DistributeUnderwritingEngine
    {
        public static string RetrieveLpTemplateName(Guid lLpTemplateId)
        {
            string investorName = "";
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveLoanProgramInfoById", new SqlParameter("@lLpTemplateId", lLpTemplateId)))
            {
                if (reader.Read())
                {
                    investorName = (string)reader["lLpTemplateNm"];
                }
            }
            return investorName;

        }

        public static LoanProgramByInvestorSet RetrieveLoanProgram(Guid lpTemplateId, bool preserveProductCode = false)
        {
            string investorName = string.Empty;
            string productCode = string.Empty;
            string templateName = string.Empty;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveLoanProgramInfoById", new SqlParameter("@lLpTemplateId", lpTemplateId)))
            {
                if (reader.Read())
                {
                    templateName = (string)reader["lLpTemplateNm"];
                    investorName = (string)reader["lLpInvestorNm"];
                    productCode = (string)reader["ProductCode"];
                }
            }

            LoanProgramByInvestorSet set = new LoanProgramByInvestorSet();
            set.Add(lpTemplateId, investorName, productCode, preserveProductCode);

            return set;
        }

        private static string RetrieveInvestorName(Guid lLpTemplateId)
        {

            string investorName = "";
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LpeSrc, "RetrieveLoanProgramInfoById", new SqlParameter("@lLpTemplateId", lLpTemplateId)))
            {
                if (reader.Read())
                {
                    investorName = (string)reader["lLpInvestorNm"];
                }
            }
            return investorName;

        }
        public static void RecordPricingTotalTime(Guid requestId, string pricingType, int totalTimeInMs)
        {
            if (string.IsNullOrEmpty(ConstStage.MSMQ_PricingLog) == false)
            {
                // 12/16/2011 dd - Send the statistic information to separate database for analysis.
                XDocument xdoc = new XDocument(new XElement("item_updated",
                    new XAttribute("RequestId", requestId.ToString()),
                    new XAttribute("PricingType", pricingType),
                    new XAttribute("TotalTime", totalTimeInMs.ToString())));

                try
                {
                    Drivers.Gateways.MessageQueueHelper.SendXML(ConstStage.MSMQ_PricingLog, null, xdoc);
                }
                catch (Exception exc)
                {
                    Tools.LogError("RecordPricingTotalTime", exc);
                }
            }

        }
        
        public static void PerformInternalPricingAction(AbstractUserPrincipal principal, Guid sLId, Guid productId, E_sPricingModeT pricingModeT,
            decimal rate, decimal fee, bool isSetRateLockPricingOption, bool isAddConditionOption, HybridLoanProgramSetOption historicalOptions)
        {
            CPageData dataLoan = new CGetLoanProgramsToRunLpeData(sLId);
            dataLoan.InitLoad();
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.sPricingModeT = pricingModeT;

            Guid lpePriceGroupId = Guid.Empty;

            switch (pricingModeT)
            {
                case E_sPricingModeT.InternalBrokerPricing:
                case E_sPricingModeT.EligibilityBrokerPricing:
                    lpePriceGroupId = dataLoan.sProdLpePriceGroupId;
                    break;
                case E_sPricingModeT.InternalInvestorPricing:
                case E_sPricingModeT.EligibilityInvestorPricing:
                    lpePriceGroupId = dataLoan.sInvestorLockLpePriceGroupId;
                    break;
                default:
                    throw new UnhandledEnumException(pricingModeT);
            }


            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();
            string investorName = RetrieveInvestorName(productId);

            lpSet.Add(productId, investorName, "");

            bool bGetAllReasons = principal.HasPermission(LendersOffice.Security.Permission.CanApplyForIneligibleLoanPrograms);
            string selectedRate = rate + ":" + fee + ":0.000,0.000";
            Guid requestId = Process(E_LpeTaskT.PerformInternalPricingAction, principal, sLId, lpSet, bGetAllReasons, selectedRate,
                false /* isRequestingRateLock" */, "" /* sProOFinPmt */, false /* skipRateAdjError */, E_sLienQualifyModeT.ThisLoan, lpePriceGroupId, "" /* version */,
                "" , "0" /*debugResultStartTicks */, E_RenderResultModeT.Regular,
                pricingModeT, Guid.Empty, 0, 0, 0, isSetRateLockPricingOption, isAddConditionOption, "" /*uniqueChecksum*/, 0, "" /* secondLienUniqueChecksum */,null, null,
                null /* quickPricerLoanItem */, null, historicalOptions, null);

            LpeDistributeResults.RetrieveResultsSynchronous(requestId);
        }

        public static XmlDocument RequestInternalPricingResultInXml(AbstractUserPrincipal principal, Guid sLId, E_sPricingModeT internalPricingModeT, bool? isBestPriceEnabled)
        {
            Stopwatch sw = Stopwatch.StartNew();
            XmlDocument doc = new XmlDocument();
            Guid requestId = Guid.Empty;
            try
            {
                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);

                //ir - OPM 187708, allow user to specify isBestPriceEnabled in xml. Default to BrokerDB object otherwise
                if (isBestPriceEnabled.HasValue == false)
                {
                    isBestPriceEnabled = brokerDB.IsBestPriceEnabled; // If broker has rate merge enable then use it as default.
                }


                bool isBorrowerPaidOriginatorCompAndIncludedInFees = false;
                bool isLenderPaidOriginatorCompAndAdditionToFees = false;
                
                requestId = RequestInternalPricingResult(principal, sLId, internalPricingModeT, out isBorrowerPaidOriginatorCompAndIncludedInFees,
                    out isLenderPaidOriginatorCompAndAdditionToFees);

                UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);

                if (null == resultItem)
                {
                    throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
                }
                if (resultItem.IsErrorMessage)
                {
                    throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
                }

                doc = CreatePricingResultsXml(
                    resultItem, 
                    principal, 
                    brokerDB, 
                    isBestPriceEnabled.Value, 
                    isBorrowerPaidOriginatorCompAndIncludedInFees, 
                    isLenderPaidOriginatorCompAndAdditionToFees,
                    isQualifiedForOriginatorCompensation: false, 
                    isUsePml2: false, 
                    isShowQMData: false);

            }
            catch (CBaseException exc)
            {
                doc = new XmlDocument();
                XmlElement pricingResultsElement = doc.CreateElement("PricingResults");
                doc.AppendChild(pricingResultsElement);
                pricingResultsElement.SetAttribute("Status", "Error");


                XmlElement errorElement = doc.CreateElement("Error");
                pricingResultsElement.AppendChild(errorElement);
                errorElement.SetAttribute("Message", exc.UserMessage);
                Tools.LogErrorWithCriticalTracking(exc);
            }
            catch (Exception exc)
            {
                doc = new XmlDocument();
                XmlElement pricingResultsElement = doc.CreateElement("PricingResults");
                doc.AppendChild(pricingResultsElement);
                pricingResultsElement.SetAttribute("Status", "Error");


                XmlElement errorElement = doc.CreateElement("Error");
                pricingResultsElement.AppendChild(errorElement);
                errorElement.SetAttribute("Message", ErrorMessages.Generic);
                Tools.LogErrorWithCriticalTracking(exc);
            }
            sw.Stop();
            DistributeUnderwritingEngine.RecordPricingTotalTime(requestId, "API_RequestInternalPricingResultInXml", (int)sw.ElapsedMilliseconds);
            return doc;
        }
        public static Guid RequestInternalPricingResult(AbstractUserPrincipal principal, Guid sLId, E_sPricingModeT internalPricingModeT)
        {
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return RequestInternalPricingResult(principal, sLId, internalPricingModeT, options);
        }
        public static Guid RequestInternalPricingResult(AbstractUserPrincipal principal, Guid sLId, E_sPricingModeT internalPricingModeT,
            HybridLoanProgramSetOption historicalOptions, bool isAutoProcess = false)
        {
            bool isBorrowerPaidOriginatorCompAndIncludedInFees = false;
            bool isLenderPaidOriginatorCompAndAdditionToFees = false;
            return RequestInternalPricingResult(principal, sLId, internalPricingModeT, historicalOptions, out isBorrowerPaidOriginatorCompAndIncludedInFees,
            out isLenderPaidOriginatorCompAndAdditionToFees, isAutoProcess: isAutoProcess);
        }

        public static Guid RequestInternalPricingResult(AbstractUserPrincipal principal, Guid sLId, E_sPricingModeT internalPricingModeT,
            out bool isBorrowerPaidOriginatorCompAndIncludedInFees, out bool isLenderPaidOriginatorCompAndAdditionToFees)
        {
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return RequestInternalPricingResult(principal, sLId, internalPricingModeT,
                options, out isBorrowerPaidOriginatorCompAndIncludedInFees,
                out isLenderPaidOriginatorCompAndAdditionToFees);
        }
        public static Guid RequestInternalPricingResult(AbstractUserPrincipal principal, Guid sLId, E_sPricingModeT internalPricingModeT, 
            HybridLoanProgramSetOption historicalOptions,
            out bool isBorrowerPaidOriginatorCompAndIncludedInFees,
            out bool isLenderPaidOriginatorCompAndAdditionToFees,
            bool isAutoProcess = false)
        {
            //BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
            // 2/17/2011 dd - OPM - Always perform PML transformation and save to loan file when we run internal pricing mode.
            // OPM 247399.  Considering this transform a system action rather than user save/load
            // should not be rejected due to permissions.  Users do not see this data.
            CPageData dataLoan = new CGetLoanProgramsToRunLpeDataBypassAccessControl(sLId);
            dataLoan.ByPassFieldSecurityCheck = true;
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.TransformDataToPml(E_TransformToPmlT.RerunForRateLockRequest);
            dataLoan.Save();
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;
            dataLoan.sPricingModeT = internalPricingModeT;

            isBorrowerPaidOriginatorCompAndIncludedInFees = false;
            isLenderPaidOriginatorCompAndAdditionToFees = false;

            if (dataLoan.sIsQualifiedForOriginatorCompensation)
            {
                isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
&& dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                    dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
            }

            Guid lpePriceGroupId = Guid.Empty;

            switch (internalPricingModeT)
            {
                case E_sPricingModeT.InternalBrokerPricing:
                case E_sPricingModeT.EligibilityBrokerPricing:
                    lpePriceGroupId = dataLoan.sProdLpePriceGroupId;
                    break;
                case E_sPricingModeT.InternalInvestorPricing:
                case E_sPricingModeT.EligibilityInvestorPricing:
                    lpePriceGroupId = dataLoan.sInvestorLockLpePriceGroupId;
                    break;
                default:
                    throw new UnhandledEnumException(internalPricingModeT);
            }

            LoanProgramByInvestorSet lpSet;
            if (isAutoProcess)
            {
                // OPM 247399. Automated process is just for the single registered program. 
                lpSet = DistributeUnderwritingEngine.RetrieveLoanProgram(dataLoan.sLpTemplateId);

            }
            else
            {
                lpSet = principal.UseInternalPricerV2 
                    ? dataLoan.GetLoanProgramsToRunForHistoricalPricing(principal)
                    : dataLoan.GetLoanProgramsToRunInternalPricing();
            }

            E_RenderResultModeT renderResultModeT = E_RenderResultModeT.Regular;

            Guid requestId = SubmitToEngine(principal, sLId, lpSet, E_sLienQualifyModeT.ThisLoan, lpePriceGroupId,
                "" /* tempLpeTaskMessageXml */, renderResultModeT, internalPricingModeT,
                Guid.Empty, 0, 0, 0, 0, historicalOptions, isAutoProcess: isAutoProcess);

            return requestId;
        }

        public static void RegisterLoanProgram(AbstractUserPrincipal principal, Guid sLId, Guid lLpTemplateId, decimal requestedRate, decimal requestedFee, bool isRequestingRateLock)
        {
            RegisterLoanProgram(principal, sLId, lLpTemplateId, requestedRate, requestedFee, "0.000,0.000", isRequestingRateLock);
        }
        public static void RegisterLoanProgram(AbstractUserPrincipal principal, Guid sLId, Guid lLpTemplateId, decimal requestedRate, decimal requestedFee, string rateOptionId, bool isRequestingRateLock)
        {
            string version = string.Empty;
            string uniqueChecksum = string.Empty;
            CPageData dataLoan = new CGetLoanProgramsToRunLpeData(sLId);
            dataLoan.InitLoad();

            if (!principal.HasPermission(Permission.CanSubmitWithoutCreditReport)
                && !dataLoan.sAllAppsHasCreditReportOnFile)
            {
                throw new CBaseException(ErrorMessages.Generic, ErrorMessages.PML.InsufficientPermissionToSubmitWithoutCredit);
            }
            
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
            Guid lpePriceGroupId = dataLoan.sProdLpePriceGroupId;

            string requestedRateString = requestedRate + ":" + requestedFee + ":" + rateOptionId;
            Guid requestId = SubmitRate(principal, sLId, lLpTemplateId, requestedRateString, isRequestingRateLock, lpePriceGroupId, version, string.Empty, E_RenderResultModeT.Regular, uniqueChecksum, null, null);

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);

            if (null == resultItem)
            {
                throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
            }
            if (resultItem.IsErrorMessage)
            {
                throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
            }

            // This needs to be done, and doesn't come from UI data, so might as well give it full access.
            dataLoan = new CFullAccessPageData(sLId, new string[] { "sfUpdate_sProMInsFieldsPer110559" });
            dataLoan.ByPassFieldSecurityCheck = true;  // might as well make it faster.
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

            if (dataLoan.sLT == E_sLT.FHA)
            {
                dataLoan.Update_sProMInsFieldsPer110559();
                dataLoan.Save();
            }
        }

        public static XmlDocument GetRegularPricingResultInXml(AbstractUserPrincipal principal, Guid sLId, Guid sProdLpePriceGroupId)
        {
            // 3/8/2011 dd - This method will always run pricing in regular mode. This method design to use in DualTest.

            CPageData dataLoan = new CGetLoanProgramsToRunLpeData(sLId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
            dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);

            if (sProdLpePriceGroupId != Guid.Empty)
            {
                dataLoan.sProdLpePriceGroupId = sProdLpePriceGroupId;
            }
            dataLoan.Save();

            dataLoan.InitLoad();
            dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

            LoanProgramByInvestorSet lpSet = dataLoan.GetLoanProgramsToRunLpe(principal, E_sLienQualifyModeT.ThisLoan, Guid.Empty);

            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            Guid requestId = SubmitToEngine(principal, sLId, lpSet, E_sLienQualifyModeT.ThisLoan, dataLoan.sProdLpePriceGroupId,
                "" /* tempLpeTaskMessageXml */, E_RenderResultModeT.Regular, E_sPricingModeT.RegularPricing,
                Guid.Empty, 0, 0, 0, 0, options);

            UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);

            if (null == resultItem)
            {
                throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
            }
            if (resultItem.IsErrorMessage)
            {
                throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
            }

            ArrayList results = resultItem.Results;

            if (null == results)
            {
                throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem.Results is null");
            }

            List<CApplicantPriceXml> eligibleList = new List<CApplicantPriceXml>();
            List<CApplicantPriceXml> ineligibleList = new List<CApplicantPriceXml>();
            List<CApplicantPriceXml> insufficientList = new List<CApplicantPriceXml>();

            foreach (object o in results)
            {
                CApplicantPriceXml product = o as CApplicantPriceXml;
                if (null == product)
                    continue;

                switch (product.Status)
                {
                    case E_EvalStatus.Eval_Eligible:
                        eligibleList.Add(product);
                        break;
                    case E_EvalStatus.Eval_Ineligible:
                        ineligibleList.Add(product);
                        break;
                    case E_EvalStatus.Eval_InsufficientInfo:
                        insufficientList.Add(product);
                        break;
                    default:
                        throw new UnhandledEnumException(product.Status);
                }

            }

            eligibleList.Sort(new CApplicantPriceXmlNameComparer());
            ineligibleList.Sort(new CApplicantPriceXmlNameComparer());
            insufficientList.Sort(new CApplicantPriceXmlNameComparer());

            XmlDocument doc = new XmlDocument();

            XmlElement pricingResultsElement = doc.CreateElement("PricingResults");
            doc.AppendChild(pricingResultsElement);
            foreach (var list in new[] { eligibleList, ineligibleList, insufficientList })
            {
                foreach (var o in list)
                {
                    XmlElement programElement = doc.CreateElement("Program");
                    pricingResultsElement.AppendChild(programElement);

                    programElement.SetAttribute("Name", o.lLpTemplateNm);
                    programElement.SetAttribute("lLpTemplateId", o.lLpTemplateId.ToString());
                    programElement.SetAttribute("lLpInvestorNm", o.lLpInvestorNm);
                    programElement.SetAttribute("Status", o.Status.ToString());

                    if (o.Status == E_EvalStatus.Eval_Eligible || o.Status == E_EvalStatus.Eval_Ineligible)
                    {
                        if (o.ApplicantRateOptions != null)
                        {
                            foreach (var rate in o.ApplicantRateOptions)
                            {
                                XmlElement rateOptionElement = doc.CreateElement("RateOption");
                                programElement.AppendChild(rateOptionElement);

                                rateOptionElement.SetAttribute("Rate", rate.Rate_rep);
                                rateOptionElement.SetAttribute("Point", rate.Point_rep);
                                rateOptionElement.SetAttribute("APR", rate.Apr_rep);
                                rateOptionElement.SetAttribute("Margin", rate.Margin_rep);
                                rateOptionElement.SetAttribute("Payment", rate.FirstPmtAmt_rep);
                                rateOptionElement.SetAttribute("DTI", rate.BottomRatio_rep == "-1.000" || rate.BottomRatio_rep == "" ? "N/A" : rate.BottomRatio_rep);
                                rateOptionElement.SetAttribute("MaxDTI", rate.MaxDti_rep == "-1.000" || rate.MaxDti_rep == "" ? "N/A" : rate.MaxDti_rep);
                                rateOptionElement.SetAttribute("QualRate", rate.QRate_rep == "" ? rate.Rate_rep : rate.QRate_rep);
                                if (rate.IsBlockedRateLockSubmission)
                                {
                                    rateOptionElement.SetAttribute("Expired", "True");
                                }

                            }
                        }
                    }
                    if (o.Status == E_EvalStatus.Eval_Ineligible || o.Status == E_EvalStatus.Eval_InsufficientInfo)
                    {

                        foreach (var msg in o.DisqualifiedRuleList)
                        {
                            XmlElement disqualifyRuleElement = doc.CreateElement("DisqualifiedRule");
                            programElement.AppendChild(disqualifyRuleElement);

                            disqualifyRuleElement.InnerText = msg;
                        }
                    }
                }
            }
            return doc;
        }
        public static XmlDocument GetPricingResultInXml(
            AbstractUserPrincipal principal, 
            Guid sLId, 
            bool alwaysUseRegularPricing, 
            bool isIncludeOriginatorCompensation, 
            bool isUsePml2,
            bool allowQp2File = false)
        {
            if (principal is ConsumerPortalUserPrincipal)
            {
                // 6/30/2013 dd - Need to convert ConsumerPortalUserPrincipal to BrokerUserPrincipal
                // for distribute pricing to work.
                principal = ((ConsumerPortalUserPrincipal)principal).GetImpersonatePrincipal();
            }

            if (isUsePml2 
                && !principal.IsNewPmlUIEnabled 
                && !principal.BrokerDB.IsNewPmlUIEnabled )
            {
                throw new GenericUserErrorMessageException("1.0 User cannot price in 2.0 mode.");
            }

            Stopwatch sw = Stopwatch.StartNew();

            // 12/11/2008 - dd - OPM 25734 - Allow 3rd party to run PML and get result in XML format.
            // This method is temporary store here untill I find a better place for it.
            // 12/11/2008 - dd - Only support 1st result lien.
            // Schema for pricing results will be
            // <PricingResults Status="OK">
            //    <Program Name="30 YRS FIXED" Status="Eligible">
            //        <RateOption Rate="6.000" Point="1.000" Payment="1200.12" QualRate="6.000" Margin="" DTI="23.234" MaxDTI="" APR="6.123" />
            //        <RateOption Rate="6.125" Point="1.125" Payment="1200.12" QualRate="6.000" Margin="" DTI="23.234" MaxDTI="" APR="6.123" />
            //    </Program>
            //    <Program Name="30 YRS FIXED" Status="Ineligible" />
            // </PricingResults>
            //
            // If there are errors then
            // <PricingResults Status="Error">
            //    <Error Message="Error Message." />
            // </PricingResults>

            XmlDocument doc = new XmlDocument();
            Guid requestId = Guid.Empty;
            try
            {

                BrokerDB brokerDB = BrokerDB.RetrieveById(principal.BrokerId);
                bool isBestPriceEnabled = false;

                if (alwaysUseRegularPricing == false)
                {
                    // 1/17/2012 dd - If always use regular pricing option is off then determine if lender
                    // has best price enable or not.
                    isBestPriceEnabled = brokerDB.IsBestPriceEnabled; // If broker has rate merge enable then use it as default.
                }

                CPageData dataLoan = new CGetLoanProgramsToRunLpeDataBypassAccessControl(sLId);
                dataLoan.AllowSaveWhileQP2Sandboxed = allowQp2File;
                dataLoan.AllowLoadWhileQP2Sandboxed = allowQp2File;
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
                dataLoan.CalcModeT = E_CalcModeT.LendersOfficePageEditing;
                dataLoan.TransformDataToPml(E_TransformToPmlT.FromScratch);
                dataLoan.RecalculateClosingFeeConditions(false); // OPM 140122.  
                dataLoan.Save();

                dataLoan.InitLoad();
                dataLoan.CalcModeT = E_CalcModeT.PriceMyLoan;

                bool isBorrowerPaidOriginatorCompAndIncludedInFees = false;
                bool isLenderPaidOriginatorCompAndAdditionToFees = false;
                bool isQualifiedForOriginatorCompensation = false;
                bool isShowQMData =
                     ( dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent
                        || principal.BrokerDB.DisplayQMResultsForCorrespondentFiles)
                     && (principal.BrokerDB.ShowQMStatusInPml2 
                        || EmployeeDB.RetrieveById(principal.BrokerId, principal.EmployeeId).ShowQMStatusInPml2);

                if (dataLoan.sIsQualifiedForOriginatorCompensation)
                {
                    isBorrowerPaidOriginatorCompAndIncludedInFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.BorrowerPaid
    && dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.IncludedInLenderFees;
                    isLenderPaidOriginatorCompAndAdditionToFees = dataLoan.sOriginatorCompensationPaymentSourceT == E_sOriginatorCompensationPaymentSourceT.LenderPaid &&
                        dataLoan.sOriginatorCompensationLenderFeeOptionT == E_sOriginatorCompensationLenderFeeOptionT.InAdditionToLenderFees;
                    isQualifiedForOriginatorCompensation = true;
                }

                // I don't think Pml 2.0 has hit the validation rules since commit fe2ffd3c9.
                // When the user is explicitly calling PML 2.0 through the web services, the 
                // desired behavior is to keep the behavior consistent through the UI and web
                // servies. The calls to the v1 pricing web services have always called the
                // validation regardless of whether the lender is using PML 2.0, so I will leave
                // that behavior in place. gf 9/29/16
                if (!isUsePml2)
                {
                    // Perform validation.
                    LoanValidationResultCollection validationResultCollection = dataLoan.ValidateForRunningPricing(principal);

                    if (validationResultCollection.ErrorCount != 0)
                    {
                        XmlElement pricingResultsElement = doc.CreateElement("PricingResults");
                        doc.AppendChild(pricingResultsElement);

                        StringBuilder sb = new StringBuilder();
                        pricingResultsElement.SetAttribute("Status", "Error");
                        foreach (LoanValidationResult validationResult in validationResultCollection.ErrorList)
                        {

                            XmlElement errorElement = doc.CreateElement("Error");
                            pricingResultsElement.AppendChild(errorElement);
                            errorElement.SetAttribute("Message", validationResult.Message);

                        }
                        return doc;
                    }
                }

                LoanProgramByInvestorSet lpSet = dataLoan.GetLoanProgramsToRunLpe(principal, E_sLienQualifyModeT.ThisLoan, Guid.Empty);

                E_RenderResultModeT renderResultModeT = isBestPriceEnabled ? E_RenderResultModeT.BestPricesPerProgram : E_RenderResultModeT.Regular;
                var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
                requestId = SubmitToEngine(principal, sLId, lpSet, E_sLienQualifyModeT.ThisLoan, dataLoan.sProdLpePriceGroupId,
                    "" /* tempLpeTaskMessageXml */, renderResultModeT, E_sPricingModeT.RegularPricing,
                    Guid.Empty, 0, 0, 0, 0, options);

                UnderwritingResultItem resultItem = LpeDistributeResults.RetrieveResultsSynchronous(requestId);

                if (null == resultItem)
                {
                    throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem is null");
                }
                if (resultItem.IsErrorMessage)
                {
                    throw new CBaseException(resultItem.ErrorMessage, resultItem.ErrorMessage);
                }

                doc = CreatePricingResultsXml(
                    resultItem, 
                    principal, 
                    brokerDB, 
                    isBestPriceEnabled, 
                    isBorrowerPaidOriginatorCompAndIncludedInFees, 
                    isLenderPaidOriginatorCompAndAdditionToFees, 
                    isQualifiedForOriginatorCompensation,
                    isUsePml2,
                    isShowQMData);

                if (isIncludeOriginatorCompensation)
                {
                    // 7/8/2013 dd - OPM 118621 - Include Originator Compensation via API.
                    XmlElement rootElement = doc.DocumentElement;

                    if (!dataLoan.sHasOriginatorCompensationPlan && !dataLoan.BrokerDB.ApplyCompWhenDocCheckDateEntered)
                    {
                        dataLoan.ApplyCompensationFromCurrentOriginator();
                    }

                    rootElement.SetAttribute("sOriginatorCompensationPercent", dataLoan.sOriginatorCompensationPercent_rep);
                    rootElement.SetAttribute("sOriginatorCompensationPaymentSourceT", dataLoan.sOriginatorCompensationPaymentSourceT.ToString("D"));
                    rootElement.SetAttribute("sOriginatorCompensationTotalAmount", dataLoan.sOriginatorCompensationTotalAmount_rep);
                    rootElement.SetAttribute("sOriginatorCompensationMinAmount", dataLoan.sOriginatorCompensationMinAmount_rep);
                    rootElement.SetAttribute("sOriginatorCompensationMaxAmount", dataLoan.sOriginatorCompensationMaxAmount_rep);
                    rootElement.SetAttribute("sOriginatorCompensationFixedAmount", dataLoan.sOriginatorCompensationFixedAmount_rep);
                    rootElement.SetAttribute("sOriginatorCompensationEffectiveD", dataLoan.sOriginatorCompensationEffectiveD_rep);
                }
            }
            catch (CBaseException exc)
            {
                doc = new XmlDocument();
                XmlElement pricingResultsElement = doc.CreateElement("PricingResults");
                doc.AppendChild(pricingResultsElement);
                pricingResultsElement.SetAttribute("Status", "Error");


                XmlElement errorElement = doc.CreateElement("Error");
                pricingResultsElement.AppendChild(errorElement);
                errorElement.SetAttribute("Message", exc.UserMessage);
                Tools.LogErrorWithCriticalTracking(exc);
            }
            catch (Exception exc)
            {
                doc = new XmlDocument();
                XmlElement pricingResultsElement = doc.CreateElement("PricingResults");
                doc.AppendChild(pricingResultsElement);
                pricingResultsElement.SetAttribute("Status", "Error");


                XmlElement errorElement = doc.CreateElement("Error");
                pricingResultsElement.AppendChild(errorElement);
                errorElement.SetAttribute("Message", ErrorMessages.Generic);
                Tools.LogErrorWithCriticalTracking(exc);
            }
            sw.Stop();
            DistributeUnderwritingEngine.RecordPricingTotalTime(requestId, "API_GetPricingResultInXml", (int)sw.ElapsedMilliseconds);
            return doc;
        }
        private static XmlDocument CreatePricingResultsXml(
            UnderwritingResultItem resultItem, 
            AbstractUserPrincipal principal, 
            BrokerDB brokerDB, 
            bool isBestPriceEnabled, 
            bool isBorrowerPaidOriginatorCompAndIncludedInFees, 
            bool isLenderPaidOriginatorCompAndAdditionToFees, 
            bool isQualifiedForOriginatorCompensation,
            bool isUsePml2,
            bool isShowQMData)
        {
            ArrayList results = resultItem.Results;
            if (null == results)
            {
                throw new CBaseException(ErrorMessages.Generic, "UnderwritingResultItem.Results is null");
            }

            XmlDocument doc = new XmlDocument();
            XmlElement pricingResultsElement = doc.CreateElement("PricingResults");
            doc.AppendChild(pricingResultsElement);

            List<CApplicantPriceXml> eligibleList = new List<CApplicantPriceXml>();
            List<CApplicantPriceXml> ineligibleList = new List<CApplicantPriceXml>();
            List<CApplicantPriceXml> insufficientList = new List<CApplicantPriceXml>();

            foreach (object o in results)
            {
                CApplicantPriceXml product = o as CApplicantPriceXml;
                if (null == product)
                    continue;

                switch (product.Status)
                {
                    case E_EvalStatus.Eval_Eligible:
                        eligibleList.Add(product);
                        break;
                    case E_EvalStatus.Eval_Ineligible:
                        ineligibleList.Add(product);
                        break;
                    case E_EvalStatus.Eval_InsufficientInfo:
                        insufficientList.Add(product);
                        break;
                    case E_EvalStatus.Eval_HideFromResult:
                        // ignore opm 125857 and discussion with BB.
                        break;
                    default:
                        throw new UnhandledEnumException(product.Status);
                }

            }

            eligibleList.Sort(new CApplicantPriceXmlNameComparer());
            ineligibleList.Sort(new CApplicantPriceXmlNameComparer());
            insufficientList.Sort(new CApplicantPriceXmlNameComparer());

            XmlElement programElement = null;
            decimal parRate = 0;
            Dictionary<Guid, CApplicantPriceXml> uniqueApplicantPriceInResult = new Dictionary<Guid, CApplicantPriceXml>();
            if (isBestPriceEnabled)
            {
                bool includeAllProgramsInResults = isUsePml2 && (!brokerDB.IsAlwaysUseBestPrice || PrincipalFactory.CurrentPrincipal.HasPermission(Permission.AllowBypassAlwaysUseBestPrice));
                RateMergeBucket rateMergeBucket = new RateMergeBucket(brokerDB.IsShowRateOptionsWorseThanLowerRate, includeAllProgramsInResults);
                for (int i = 0; i < eligibleList.Count; i++)
                {
                    rateMergeBucket.Add(eligibleList[i]);
                }

                Tuple<RateMergeGroupKey, Decimal> overrideParRate = null;

                E_OriginatorCompPointSetting pointSetting = E_OriginatorCompPointSetting.UsePoint;
                if (isUsePml2)
                {
                    // OPM 241783
                    // Unfortunately, this QM data is calculated outside the engine
                    // in the result UI.  Long term, this should be refactored so it
                    // can be better centralized.
                    if (brokerDB.IsUsePriceIncludingCompensationInPricingResult && isQualifiedForOriginatorCompensation)
                    {
                        // OPM 64253
                        pointSetting = E_OriginatorCompPointSetting.UsePointWithOriginatorComp;
                    }
                    else
                    {
                        pointSetting = E_OriginatorCompPointSetting.UsePoint;
                    }

                    overrideParRate = ApplyLoanWorkerUnit.GetParRateOverride(new List<UnderwritingResultItem>(new[] { resultItem }), brokerDB, includeAllProgramsInResults, pointSetting);
                }

                foreach (RateMergeGroup mergeGroup in rateMergeBucket.RateMergeGroups)
                {
                    if (mergeGroup.RateOptions.Count == 0)
                        continue;
                    
                    if (isUsePml2)
                    {
                       parRate = ApplyLoanWorkerUnit.GetParRate(overrideParRate, mergeGroup, pointSetting);
                    }

                    programElement = doc.CreateElement("Program");
                    pricingResultsElement.AppendChild(programElement);

                    programElement.SetAttribute("Name", mergeGroup.Name);
                    programElement.SetAttribute("Status", "Eligible");
                    programElement.SetAttribute("Term", mergeGroup.lTerm.ToString());
                    programElement.SetAttribute("Due", mergeGroup.lDue.ToString());
                    programElement.SetAttribute("RAdj1stCapMon", mergeGroup.lRAdj1stCapMon.ToString());
                    programElement.SetAttribute("ProductType", mergeGroup.lProductType);
                    programElement.SetAttribute("FinMethT", mergeGroup.lFinMethT.ToString());
                    programElement.SetAttribute("RateSheetDownloadEndD", mergeGroup.lRateSheetDownloadEndD_rep);
                    programElement.SetAttribute("ARMIndex", mergeGroup.lRAdjIndexR_rep);
                    programElement.SetAttribute("ARMIndexName", mergeGroup.lArmIndexNameVstr);
                    programElement.SetAttribute("RAdjCapMon", mergeGroup.lRAdjCapMon.ToString());
                    programElement.SetAttribute("RAdj1stCapR", mergeGroup.lRadj1stCapR_rep);
                    programElement.SetAttribute("RAdjCapR", mergeGroup.lRAdjCapR_rep);
                    programElement.SetAttribute("RAdjLifeCapR", mergeGroup.lRAdjLifeCapR_rep);

                    foreach (CApplicantRateOption rateOption in mergeGroup.RateOptions)
                    {
                        if (uniqueApplicantPriceInResult.ContainsKey(rateOption.ApplicantPrice.lLpTemplateId) == false)
                        {
                            uniqueApplicantPriceInResult.Add(rateOption.ApplicantPrice.lLpTemplateId, rateOption.ApplicantPrice);
                        }

                        XmlElement rateOptionElement = doc.CreateElement("RateOption");
                        programElement.AppendChild(rateOptionElement);
                        SetRateOptionXml(
                            rateOptionElement,
                            doc,
                            rateOption,
                            brokerDB,
                            principal,
                            isBorrowerPaidOriginatorCompAndIncludedInFees,
                            isLenderPaidOriginatorCompAndAdditionToFees,
                            isShowQMData,
                            parRate,
                            isUsePml2);
                    }
                }
            }
            else
            {
                foreach (CApplicantPriceXml applicantPrice in eligibleList)
                {
                    if (uniqueApplicantPriceInResult.ContainsKey(applicantPrice.lLpTemplateId) == false)
                    {
                        uniqueApplicantPriceInResult.Add(applicantPrice.lLpTemplateId, applicantPrice);
                    }
                    programElement = doc.CreateElement("Program");
                    pricingResultsElement.AppendChild(programElement);

                    programElement.SetAttribute("Name", applicantPrice.lLpTemplateNm);
                    programElement.SetAttribute("Status", "Eligible");
                    programElement.SetAttribute("Term", applicantPrice.lTerm_rep);
                    programElement.SetAttribute("Due", applicantPrice.lDue_rep);
                    programElement.SetAttribute("RAdj1stCapMon", applicantPrice.lRadj1stCapMon_rep);
                    programElement.SetAttribute("ProductType", applicantPrice.lLpProductType);
                    programElement.SetAttribute("FinMethT", applicantPrice.lFinMethT.ToString());
                    programElement.SetAttribute("RateSheetDownloadEndD", applicantPrice.lRateSheetDownloadEndD_rep);
                    programElement.SetAttribute("ARMIndex", applicantPrice.lArmIndexNameVstr);
                    programElement.SetAttribute("RAdjCapMon", applicantPrice.lRAdjCapMon_rep);
                    programElement.SetAttribute("RAdj1stCapR", applicantPrice.lRadj1stCapR_rep);
                    programElement.SetAttribute("RAdjCapR", applicantPrice.lRAdjCapR_rep);
                    programElement.SetAttribute("RAdjLifeCapR", applicantPrice.lRAdjLifeCapR_rep);
                    foreach (CApplicantRateOption rateOption in applicantPrice.ApplicantRateOptions)
                    {
                        rateOption.SetApplicantPrice(applicantPrice);
                        XmlElement rateOptionElement = doc.CreateElement("RateOption");
                        programElement.AppendChild(rateOptionElement);
                        SetRateOptionXml(
                            rateOptionElement,
                            doc,
                            rateOption,
                            brokerDB,
                            principal,
                            isBorrowerPaidOriginatorCompAndIncludedInFees,
                            isLenderPaidOriginatorCompAndAdditionToFees,
                            isShowQMData,
                            parRate,
                            isUsePml2);
                    }
                }
            }

            foreach (CApplicantPriceXml applicantPrice in ineligibleList)
            {

                programElement = doc.CreateElement("Program");
                pricingResultsElement.AppendChild(programElement);

                programElement.SetAttribute("Name", applicantPrice.lLpTemplateNm);
                programElement.SetAttribute("Status", "Ineligible");
                programElement.SetAttribute("Term", applicantPrice.lTerm_rep);
                programElement.SetAttribute("Due", applicantPrice.lDue_rep);
                programElement.SetAttribute("RAdj1stCapMon", applicantPrice.lRadj1stCapMon_rep);
                programElement.SetAttribute("FinMethT", applicantPrice.lFinMethT.ToString());
                programElement.SetAttribute("Reason", applicantPrice.DisqualifiedRules);
                programElement.SetAttribute("ARMIndex", applicantPrice.lRAdjIndexR_rep);
                programElement.SetAttribute("ARMIndexName", applicantPrice.lArmIndexNameVstr);
                programElement.SetAttribute("RAdjCapMon", applicantPrice.lRAdjCapMon_rep);
                programElement.SetAttribute("RAdj1stCapR", applicantPrice.lRadj1stCapR_rep);
                programElement.SetAttribute("RAdjCapR", applicantPrice.lRAdjCapR_rep);
                programElement.SetAttribute("RAdjLifeCapR", applicantPrice.lRAdjLifeCapR_rep);

                if (principal.HasPermission(LendersOffice.Security.Permission.CanApplyForIneligibleLoanPrograms))
                {
                    if (uniqueApplicantPriceInResult.ContainsKey(applicantPrice.lLpTemplateId) == false)
                    {
                        uniqueApplicantPriceInResult.Add(applicantPrice.lLpTemplateId, applicantPrice);
                    }

                    foreach (CApplicantRateOption rateOption in applicantPrice.ApplicantRateOptions)
                    {
                        rateOption.SetApplicantPrice(applicantPrice);
                        XmlElement rateOptionElement = doc.CreateElement("RateOption");
                        programElement.AppendChild(rateOptionElement);
                        SetRateOptionXml(
                            rateOptionElement,
                            doc,
                            rateOption,
                            brokerDB,
                            principal,
                            isBorrowerPaidOriginatorCompAndIncludedInFees,
                            isLenderPaidOriginatorCompAndAdditionToFees,
                            isShowQMData,
                            parRate,
                            isUsePml2);
                    }
                }
            }
            foreach (CApplicantPriceXml applicantPrice in insufficientList)
            {

                programElement = doc.CreateElement("Program");
                pricingResultsElement.AppendChild(programElement);

                programElement.SetAttribute("Name", applicantPrice.lLpTemplateNm);
                programElement.SetAttribute("Status", "Insufficient");
                programElement.SetAttribute("Term", applicantPrice.lTerm_rep);
                programElement.SetAttribute("Due", applicantPrice.lDue_rep);
                programElement.SetAttribute("RAdj1stCapMon", applicantPrice.lRadj1stCapMon_rep);
                programElement.SetAttribute("FinMethT", applicantPrice.lFinMethT.ToString());
                programElement.SetAttribute("Reason", applicantPrice.DisqualifiedRules);
                programElement.SetAttribute("ARMIndex", applicantPrice.lArmIndexNameVstr);
                programElement.SetAttribute("RAdjCapMon", applicantPrice.lRAdjCapMon_rep);
                programElement.SetAttribute("RAdj1stCapR", applicantPrice.lRadj1stCapR_rep);
                programElement.SetAttribute("RAdjCapR", applicantPrice.lRAdjCapR_rep);
                programElement.SetAttribute("RAdjLifeCapR", applicantPrice.lRAdjLifeCapR_rep);
            }

            // 9/7/2011 dd - OPM 70579 - Add adjustments to the results.
            XmlElement adjustmentListElement = doc.CreateElement("AdjustmentsTable");
            pricingResultsElement.AppendChild(adjustmentListElement);

            Predicate<CAdjustItem> allResultDataIsBlank = a => string.IsNullOrEmpty(a.Rate)
                && string.IsNullOrEmpty(a.Fee)
                && string.IsNullOrEmpty(a.Margin)
                && string.IsNullOrEmpty(a.Description);

            foreach (CApplicantPriceXml applicantPrice in uniqueApplicantPriceInResult.Values)
            {
                XmlElement adjustment = doc.CreateElement("Adjustment");
                adjustment.SetAttribute("lLpTemplateId", applicantPrice.lLpTemplateId.ToString());
                foreach (CAdjustItem item in applicantPrice.AdjustDescs)
                {
                    if (brokerDB.IsUsePerRatePricing && item.OptionCode != string.Empty)
                    {
                        continue; // We add these in a different section
                    }
                    else if (allResultDataIsBlank(item))
                    {
                        continue;
                    }

                    XmlElement adjustmentItem = doc.CreateElement("AdjustmentItem");
                    adjustment.AppendChild(adjustmentItem);
                    adjustmentItem.SetAttribute("Rate", item.Rate);
                    adjustmentItem.SetAttribute("Point", item.Fee);
                    adjustmentItem.SetAttribute("Margin", item.Margin);
                    adjustmentItem.SetAttribute("Description", item.Description);
                }

                if (isUsePml2 && ( principal.HasRole( E_RoleT.LockDesk ) 
                    || principal.HasPermission(Permission.CanModifyLoanPrograms) ) )
                {
                    // OPM 241783.  Adding hidden adjustments in this new mode.
                    foreach (CAdjustItem item in applicantPrice.HiddenAdjustDescs)
                    {
                        if (brokerDB.IsUsePerRatePricing && item.OptionCode != string.Empty)
                        {
                            continue; // We add these in a different section
                        }
                        else if (allResultDataIsBlank(item))
                        {
                            continue;
                        }

                        XmlElement adjustmentItem = doc.CreateElement("AdjustmentItem");
                        adjustment.AppendChild(adjustmentItem);
                        adjustmentItem.SetAttribute("Rate", item.Rate);
                        adjustmentItem.SetAttribute("Point", item.Fee);
                        adjustmentItem.SetAttribute("Margin", item.Margin);
                        adjustmentItem.SetAttribute("Description", item.Description);
                    }
                }

                if (adjustment.HasChildNodes)
                {
                    adjustmentListElement.AppendChild(adjustment);
                }
            }

            if (brokerDB.IsUsePerRatePricing)
            {
                // 10.16.12. mf. 87119. To avoid breaking existing integration, we add a new section for perrate adjustments.

                XmlElement perRateAdjustmentListElement = doc.CreateElement("RateAdjustmentsTable");

                pricingResultsElement.AppendChild(perRateAdjustmentListElement);
                foreach (CApplicantPriceXml applicantPrice in uniqueApplicantPriceInResult.Values)
                {
                    XmlElement adjustment = doc.CreateElement("Adjustment");
                    adjustment.SetAttribute("lLpTemplateId", applicantPrice.lLpTemplateId.ToString());
                    foreach (CAdjustItem item in applicantPrice.AdjustDescs)
                    {
                        if (item.OptionCode == string.Empty)
                        {
                            continue; // We add these in a different section
                        }
                        else if (allResultDataIsBlank(item))
                        {
                            continue;
                        }

                        XmlElement adjustmentItem = doc.CreateElement("AdjustmentItem");
                        adjustment.AppendChild(adjustmentItem);
                        adjustmentItem.SetAttribute("Rate", item.Rate);
                        adjustmentItem.SetAttribute("Point", item.Fee);
                        adjustmentItem.SetAttribute("Margin", item.Margin);
                        adjustmentItem.SetAttribute("Description", item.Description);
                        adjustmentItem.SetAttribute("Code", item.OptionCode);
                    }

                    if (isUsePml2 && (principal.HasRole(E_RoleT.LockDesk)
                        || principal.HasPermission(Permission.CanModifyLoanPrograms)))
                    {
                        // OPM 241783.  Adding hidden per-rate adjustments in this new mode.
                        foreach (CAdjustItem item in applicantPrice.HiddenAdjustDescs)
                        {
                            if (item.OptionCode == string.Empty)
                                continue; // We add these in a different section
                            else if (allResultDataIsBlank(item))
                            {
                                continue;
                            }

                            XmlElement adjustmentItem = doc.CreateElement("AdjustmentItem");
                            adjustment.AppendChild(adjustmentItem);
                            adjustmentItem.SetAttribute("Rate", item.Rate);
                            adjustmentItem.SetAttribute("Point", item.Fee);
                            adjustmentItem.SetAttribute("Margin", item.Margin);
                            adjustmentItem.SetAttribute("Description", item.Description);
                            adjustmentItem.SetAttribute("Code", item.OptionCode);
                        }
                    }

                    if (adjustment.HasChildNodes)
                    {
                        perRateAdjustmentListElement.AppendChild(adjustment);
                    }
                }
            }

            XmlElement conditionTableElement = doc.CreateElement("ConditionsTable");
            pricingResultsElement.AppendChild(conditionTableElement);

            foreach (CApplicantPriceXml applicantPrice in uniqueApplicantPriceInResult.Values)
            {
                XmlElement conditions = doc.CreateElement("Conditions");
                conditions.SetAttribute("lLpTemplateId", applicantPrice.lLpTemplateId.ToString());
                foreach (string category in applicantPrice.Stips.Keys)
                {
                    foreach (Toolbox.CStipulation stip in applicantPrice.Stips.GetGroupByKeyAndSort(category))
                    {
                        XmlElement conditionItem = doc.CreateElement("ConditionItem");
                        conditions.AppendChild(conditionItem);
                        conditionItem.SetAttribute("Category", category);
                        conditionItem.InnerText = stip.DescriptionForCondition;
                    }

                    if (isUsePml2 && (principal.HasPermission(Permission.CanViewHiddenInformation)
                        || principal.HasPermission(Permission.CanModifyLoanPrograms)))
                    {
                        // OPM 241783.  Adding hidden conditions in this new mode.
                        foreach (Toolbox.CStipulation stip in applicantPrice.HiddenStips.GetGroupByKeyAndSort(category))
                        {
                            XmlElement conditionItem = doc.CreateElement("ConditionItem");
                            conditions.AppendChild(conditionItem);
                            conditionItem.SetAttribute("Category", category);
                            conditionItem.InnerText = stip.DescriptionForCondition;
                        }
                    }
                }

                if (conditions.HasChildNodes)
                {
                    conditionTableElement.AppendChild(conditions);
                }
            }

            pricingResultsElement.SetAttribute("Status", "OK");

            return doc;
        }
        private static void SetRateOptionXml(
            XmlElement el,
            XmlDocument doc,
            CApplicantRateOption rateOption,
            BrokerDB broker,
            AbstractUserPrincipal principal,
            bool isBorrowerPaidOriginatorCompAndIncludedInFees,
            bool isLenderPaidOriginatorCompAndAdditionToFees,
            bool showQMData,
            decimal parRate,
            bool isUsePml2)
        {
            // OPM 64253.  If lender has a special option turned on, we want to 
            // display the point value as adjusted for originator comp.
            string pointRep;
            if (broker.IsUsePriceIncludingCompensationInPricingResult && isLenderPaidOriginatorCompAndAdditionToFees)
            {
                // OPM 64253
                pointRep = rateOption.PointIncludingOriginatorComp_rep;
                el.SetAttribute("LenderPaidOriginatorComp", rateOption.OriginatorComp_rep);
            }
            else
            {
                pointRep = rateOption.Point_rep;
            }
            el.SetAttribute("Rate", rateOption.Rate_rep);
            el.SetAttribute("Point", pointRep);
            el.SetAttribute("APR", rateOption.Apr_rep);
            el.SetAttribute("Margin", rateOption.Margin_rep);
            el.SetAttribute("Payment", rateOption.FirstPmtAmt_rep);
            el.SetAttribute("DTI", rateOption.BottomRatio_rep == "-1.000" || rateOption.BottomRatio_rep == "" ? "N/A" : rateOption.BottomRatio_rep);
            el.SetAttribute("MaxDTI", rateOption.MaxDti_rep == "-1.000" || rateOption.MaxDti_rep == "" ? "N/A" : rateOption.MaxDti_rep);
            el.SetAttribute("QualRate", rateOption.QRate_rep == "" ? rateOption.Rate_rep : rateOption.QRate_rep);
            el.SetAttribute("QualPmt", rateOption.QualPmt_rep);
            el.SetAttribute("MortgageInsuranceMonthlyPayment", rateOption.sProMIns);
            if (rateOption.IsBlockedRateLockSubmission)
            {
                el.SetAttribute("Expired", "True");
            }
            el.SetAttribute("Description", rateOption.LpTemplateNm);
            el.SetAttribute("lLpTemplateId", rateOption.LpTemplateId.ToString());

            if (principal.HasPermission(Permission.CanViewHiddenInformation)
                || principal.HasPermission(Permission.CanModifyLoanPrograms))
            {
                el.SetAttribute("lLpInvestorNm", rateOption.LpInvestorNm);
            }

            el.SetAttribute("RateOptionId", rateOption.RateOptionId);

            if (isUsePml2)
            {
                // OPM 241783
                el.SetAttribute("BestPrice", rateOption.IsRateMergeWinner.ToString());

                if (broker.EnableCashToCloseAndReservesDebugInPML)
                {
                    el.SetAttribute("CashToClose", rateOption.LoanData.sTransNetCash);
                }
            }

            if (broker.IsUsePerRatePricing)
            {
                // 10.16.12. mf. 87119.  When in per-rate mode, we add the attributes that indicate
                // the per rate eligibility and applicable per-rate adjustments.
                el.SetAttribute("Status", rateOption.IsDisqualified ? "Unavailable" : "Available");
                el.SetAttribute("UnavailableReason", rateOption.DisqualReason);
                el.SetAttribute("Adjustments", rateOption.PerOptionAdjStr);

                // OPM 130497
                if (rateOption.IsDisableLock)
                {
                    el.SetAttribute("DisableLock", "True");
                }
            }

            // OPM 122257. Include new pml items in xml if in PML2.0 mode.
            if (broker.IsNewPmlUIEnabled)
            {
                el.SetAttribute("TotalClosingCost", rateOption.ClosingCost);
                el.SetAttribute("PrepaidClosingCost", rateOption.PrepaidCharges);
                el.SetAttribute("NonPrepaidClosingCost", rateOption.NonPrepaidCharges);
                el.SetAttribute("BreakEven", rateOption.BreakEvenPoint);
                el.SetAttribute("Reserves", rateOption.Reserves);

                if (rateOption.ClosingCostBreakdown != null && rateOption.ClosingCostBreakdown.Count != 0)
                {
                    XmlElement feeList = doc.CreateElement("ClosingCostList");
                    el.AppendChild(feeList);
                    foreach (var fee in rateOption.ClosingCostBreakdown.OrderBy(p => int.Parse(p.HudLine)))
                    {
                        XmlElement feeEl = doc.CreateElement("ClosingCost");
                        if (isUsePml2)
                        {
                            // OPM 241783
                            feeEl.SetAttribute("LoanEstimateSection", Tools.GetIntegratedDisclosureString(fee.DisclosureSectionT));
                        }
                        feeEl.SetAttribute("Line", fee.HudLine);
                        feeEl.SetAttribute("Description", fee.Description);
                        feeEl.SetAttribute("Amount", fee.Fee.ToString("C"));
                        feeEl.SetAttribute("Source", fee.DescriptionSource);
                        feeList.AppendChild(feeEl);
                    }
                }
            }

            if (isUsePml2)
            {
                // OPM 241783.  The conditionals in here are meant to mirror
                // what the GetResultsInNewUI page renders to user.
                if (broker.EnableCashToCloseAndReservesDebugInPML)
                {
                    XmlElement cashToCloseList = doc.CreateElement("CashToCloseCalculation");
                    el.AppendChild(cashToCloseList);

                    Action<string, string, string> addLineItem = (dotLine, description, amount) =>
                      {
                          XmlElement lineEl = doc.CreateElement("LineItem");
                          lineEl.SetAttribute("DOTLine", dotLine);
                          lineEl.SetAttribute("Description", description);
                          lineEl.SetAttribute("Amount", amount);
                          cashToCloseList.AppendChild(lineEl);
                      };

                    addLineItem("a", "Purchase Price", rateOption.LoanData.sPurchPrice);
                    addLineItem("b", "Alterations, improvements, repairs", rateOption.LoanData.sAltCost);
                    addLineItem("c", "Land (if acquired separately)", rateOption.LoanData.sLandCost);
                    addLineItem("d", "Refi (incl. debts to be paid off)", rateOption.LoanData.sRefPdOffAmt1003);
                    addLineItem("e", "Estimated prepaid items", rateOption.LoanData.sTotEstPp1003);

                    if (rateOption.LoanData.sIsIncludeProrationsIn1003Details
                        && rateOption.LoanData.sIsIncludeProrationsInTotPp)
                    {
                        addLineItem(String.Empty, "Prepaids and Reserves", rateOption.LoanData.sTotEstPp);
                        addLineItem(String.Empty, "Prorations paid by borrower", rateOption.LoanData.sTotalBorrowerPaidProrations);
                    }

                    addLineItem("f", "Estimated closing costs", rateOption.LoanData.sTotEstCcNoDiscnt1003);

                    if (rateOption.LoanData.sIsIncludeONewFinCcInTotEstCc)
                    {
                        addLineItem(String.Empty, "This loan closing costs", rateOption.LoanData.sTotEstCcNoDiscnt);
                        addLineItem(String.Empty, "Other financing closing costs", rateOption.LoanData.sONewFinCc);
                    }

                    addLineItem("g", "PMI, MIP, Funding Fee", rateOption.LoanData.sFfUfmip1003);
                    addLineItem("h", "Discount (if Borrower will pay)", rateOption.LoanData.sLDiscnt1003);
                    addLineItem("i", "Total costs (add items a to h)", rateOption.LoanData.sTotTransC);
                    addLineItem("j", "Subordinate financing", rateOption.LoanData.sONewFinBal);
                    addLineItem("k", "Borrower's closing costs paid by Seller", rateOption.LoanData.sTotCcPbs);
                    addLineItem("l", rateOption.LoanData.sOCredit1Desc, rateOption.LoanData.sOCredit1Amt);
                    addLineItem("l", rateOption.LoanData.sOCredit2Desc, rateOption.LoanData.sOCredit2Amt);
                    addLineItem("l", rateOption.LoanData.sOCredit3Desc, rateOption.LoanData.sOCredit3Amt);
                    addLineItem("l", rateOption.LoanData.sOCredit4Desc, rateOption.LoanData.sOCredit4Amt);
                    addLineItem("l", "Lender Credit", rateOption.LoanData.sOCredit5Amt);

                    if (!rateOption.LoanData.sIsIncludeONewFinCcInTotEstCc)
                    {
                        addLineItem("l", "Other financing closing costs", rateOption.LoanData.sONewFinCc);
                    }

                    if (!(rateOption.LoanData.sIsIncludeProrationsIn1003Details
                            && rateOption.LoanData.sIsIncludeProrationsInTotPp))
                    {
                        addLineItem("m", "Loan amount (exclude PMI, MIP, FF financed)", rateOption.LoanData.sLAmtCalc);
                    }

                    if (rateOption.LoanData.sIsIncludeONewFinCcInTotEstCc
                        && (rateOption.LoanData.sIsIncludeProrationsIn1003Details
                            && rateOption.LoanData.sIsIncludeProrationsInTotPp))
                    {
                        addLineItem("m", "Loan amount (exclude PMI, MIP, FF financed)", rateOption.LoanData.sLAmtCalc);
                    }

                    addLineItem("n", "PMI, MIP, Funding Fee financed", rateOption.LoanData.sFfUfmipFinanced);
                    addLineItem("o", "Loan amount (add m & n)", rateOption.LoanData.sFinalLAmt);
                    addLineItem("p", "Cash from / to Borr (subtract j, k, l & o from i)", rateOption.LoanData.sTransNetCash);

                    XmlElement reservesCalculation = doc.CreateElement("ReservesCalculation");
                    el.AppendChild(reservesCalculation);
                    reservesCalculation.SetAttribute("TotalLiquidAssets", rateOption.LoanData.sTotLiquidAssets);
                    reservesCalculation.SetAttribute("CashFromBorrower", rateOption.LoanData.sTransNetCashNonNegative);
                    reservesCalculation.SetAttribute("QualifyingPITIA", rateOption.LoanData.sMonthlyPmt);
                }

                if (showQMData && rateOption.QMData != null)
                {
                    // OPM 241783.
                    var calculator = new QMStatusCalculator(parRate, rateOption.Apr_rep, rateOption.QMData, new LosConvert(), rateOption.Rate);

                    XmlElement qualifiedMortgage = doc.CreateElement("QualifiedMortgage");
                    el.AppendChild(qualifiedMortgage);
                    qualifiedMortgage.SetAttribute("APR", rateOption.Apr_rep);
                    qualifiedMortgage.SetAttribute("APRRateSpread", calculator.sQMAprRSpread_rep);
                    qualifiedMortgage.SetAttribute("APRRateSpreadDescription", calculator.sQMAprRSpreadCalcDesc);
                    qualifiedMortgage.SetAttribute("ParRate", calculator.sQMParR_rep);
                    qualifiedMortgage.SetAttribute("ParRateSpread", calculator.sQMParRSpread_rep);
                    qualifiedMortgage.SetAttribute("ParRateSpreadDescription", calculator.sQMParRSpreadCalcDesc);
                    qualifiedMortgage.SetAttribute("ExcessUpfrontMIPAmount", calculator.sQMExcessUpfrontMIP_rep);
                    qualifiedMortgage.SetAttribute("ExcessUpfrontMIPDescription", calculator.sQMExcessUpfrontMIPCalcDesc);
                    qualifiedMortgage.SetAttribute("ExcessDiscountPointsPercentage", calculator.sQMExcessDiscntFPc_rep);
                    qualifiedMortgage.SetAttribute("ExcessDiscountPointsAmount", calculator.sQMExcessDiscntF_rep);
                    qualifiedMortgage.SetAttribute("ExcessDiscountPointsDescription", calculator.sQMExcessDiscntFCalcDesc);
                    qualifiedMortgage.SetAttribute("QMTotalLoanAmount", calculator.sQMLAmt_rep);
                    qualifiedMortgage.SetAttribute("QMTotalLoanAmountDescription", calculator.sQMLAmtCalcDesc);
                    qualifiedMortgage.SetAttribute("AllowableQMFeesAmount", calculator.sQMMaxPointAndFeesAllowedAmt_rep);
                    qualifiedMortgage.SetAttribute("TotalQMFeeAmount", calculator.sQMTotFeeAmount_rep);
                    qualifiedMortgage.SetAttribute("OriginatorCompensationAmount", calculator.sGfeOriginatorCompF_rep);
                    qualifiedMortgage.SetAttribute("MaximumPrepaymentPenaltyAmount", calculator.sQMMaxPrePmntPenalty_rep);
                    qualifiedMortgage.SetAttribute("TotalQMFeesAmount", calculator.sQMTotFeeAmt_rep);
                    qualifiedMortgage.SetAttribute("MeetsPointsAndFeesTest", calculator.sQMLoanPassesPointAndFeesTest.ToString());
                    qualifiedMortgage.SetAttribute("DoesNotHaveNegAmOrIOFeatures", calculator.sQMLoanDoesNotHaveNegativeAmort.ToString());
                    qualifiedMortgage.SetAttribute("DoesNotHaveBalloonFeature", calculator.sQMLoanDoesNotHaveBalloonFeature.ToString());
                    qualifiedMortgage.SetAttribute("DoesNotHaveAmortizationInExcessOf30Years", calculator.sQMLoanDoesNotHaveAmortTermOver30Yr.ToString());
                    qualifiedMortgage.SetAttribute("HasQMDTILessThan43Percent", calculator.sQMLoanHasDtiLessThan43Pc.ToString());
                    qualifiedMortgage.SetAttribute("QMStatus", calculator.GetQMStatus().ToString());
                    qualifiedMortgage.SetAttribute("HigherPricedIndicator", calculator.GetHPMLStatus().ToString());
                }
            }
        }


        public static Guid SubmitRate80_20(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate, bool isRequestingRateLock,
            Guid lpePriceGroupId, Guid firstLoanProductId, decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt, bool skipRateAdjError, string version, string sTempLpeTaskMessageXml,
            string debugResultStartTicks, E_RenderResultModeT renderResultModeT, string firstLienUniqueChecksum, string secondLienUniqueChecksum, List<DenialException> denialExceptions)
        {

            if (isRequestingRateLock && principal != null && principal.BrokerDB.BrokerID.Equals(new Guid("e9f006c9-cb83-4531-8925-08fccf4add63")))
            {
                Tools.LogInfo("OPM_225020: A First-Tech user is requesting a rate lock for an 80/20 loan in PML.  Loan Id: " + loanId.ToString() + ", User Id: " + principal.UserId.ToString() +
                    " with the following Stack Trace: \n" +
                    Environment.StackTrace);
            }

            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();

            string investorName = "";

            if (productId != Guid.Empty)
            {
                investorName = RetrieveInvestorName(productId);// Only use investor of the 2nd loan product.
            }
            else
            {
                // 10/12/2006 dd - If user select manual 2nd then we will pass the request to server that could
                // handle investor from first loan program.
                investorName = RetrieveInvestorName(firstLoanProductId);
            }
            lpSet.Add(productId, investorName, ""); // 4/23/2007 dd - Unknown product code.
            lpSet.Add(firstLoanProductId, investorName, ""); // 4/23/2007 dd - Unknown product code/
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return Process(E_LpeTaskT.SubmitRate80_20, principal, loanId, lpSet, true /* bGetAllReasons */, requestedRate,
                isRequestingRateLock, "", skipRateAdjError, E_sLienQualifyModeT.ThisLoan, lpePriceGroupId, version,
                sTempLpeTaskMessageXml, debugResultStartTicks, renderResultModeT,
                E_sPricingModeT.RegularPricing, firstLoanProductId, firstLienNoteRate, firstLienPoint, firstLienMPmt, false, false, firstLienUniqueChecksum, 0, secondLienUniqueChecksum, null, null, null, null, options,denialExceptions);
        }

        public static Guid VerifyFirstLoan(AbstractUserPrincipal principal, Guid loanId, Guid firstLienLpId, decimal firstLienRate, decimal firstLienPoint, string sProOFinPmt, string sTempLpeTaskMessageXml,
            string debugResultStartTicks, E_RenderResultModeT renderResultModeT)
        {
            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();

            string investorName = RetrieveInvestorName(firstLienLpId);
            lpSet.Add(firstLienLpId, investorName, "");
            string requestedRate = firstLienRate + ":" + firstLienPoint + ":0.000,0.000";
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return Process(E_LpeTaskT.VerifyFirstLoanProgram, principal, loanId, lpSet, true /* bGetAllReasons */
                                                                                                                 ,
                requestedRate /* selectedRate */, true /* isRequestingRateLock */, sProOFinPmt, false /* skipRateAdjError */, E_sLienQualifyModeT.ThisLoan,
                PriceGroupGetter.GetPriceGroupID(loanId), "" /* version */, sTempLpeTaskMessageXml, debugResultStartTicks, renderResultModeT,
                E_sPricingModeT.RegularPricing, firstLienLpId, firstLienRate, firstLienRate, 0, false, false, "", 0, "", null, null, null, null,
                options, null);
        }
        public class PriceGroupGetter
        {
            public static Guid GetPriceGroupID(Guid loanID)
            {
                var dataLoan = CPageData.CreateUsingSmartDependency(loanID, typeof(PriceGroupGetter));
                dataLoan.InitLoad();
                return dataLoan.sProdLpePriceGroupId;
            }
        }


        public static Guid SubmitRate(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate, bool isRequestingRateLock,
            Guid lpePriceGroupId, string version, string debugResultStartTicks, E_RenderResultModeT renderResultModeT, string uniqueChecksum, string parRate, List<DenialException> denialExceptions)
        {
            return SubmitRate(principal, loanId, productId, requestedRate, isRequestingRateLock, lpePriceGroupId, version, debugResultStartTicks, renderResultModeT, uniqueChecksum, null, parRate, denialExceptions);
        }

        public static Guid SubmitRate(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate, bool isRequestingRateLock,
            Guid lpePriceGroupId, string version, string debugResultStartTicks, E_RenderResultModeT renderResultModeT, string uniqueChecksum, PricingState pinState, string parRate, List<DenialException> denialExceptions)
        {
            if (isRequestingRateLock && principal != null && principal.BrokerDB.BrokerID.Equals(new Guid("e9f006c9-cb83-4531-8925-08fccf4add63")))
            {
                Tools.LogInfo("OPM_225020: A First-Tech user is requesting a rate lock for an 80/20 loan in PML.  Loan Id: " + loanId.ToString() + ", User Id: " + principal.UserId.ToString() +
                    " with the following Stack Trace: \n" +
                    Environment.StackTrace);
            }

            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();

            string investorName = RetrieveInvestorName(productId);
            lpSet.Add(productId, investorName, "");
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return Process(E_LpeTaskT.SubmitRate, principal, loanId, lpSet, true /* bGetAllReasons */, requestedRate, isRequestingRateLock, "" /* sProOFinPmt */,
                false /* skipRateAdjError */, E_sLienQualifyModeT.ThisLoan, lpePriceGroupId, version,
                "" /* tempLpeTaskMessageXml */, debugResultStartTicks, renderResultModeT, E_sPricingModeT.RegularPricing,
                Guid.Empty, 0, 0, 0, false, false, uniqueChecksum, 0, "" /* secondLienUniqueChecksum */, pinState, null,
                null /* quickPricerLoanItem */, parRate, options, denialExceptions);

        }
        public static Guid TemporaryAcceptFirstLoan(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate, Guid lpePriceGroupId,
            string version, string debugResultStartTicks, E_RenderResultModeT renderResultModeT)
        {
            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();

            string investorName = RetrieveInvestorName(productId);
            lpSet.Add(productId, investorName, "");

            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return Process(E_LpeTaskT.TemporaryAcceptFirstLoan, principal, loanId, lpSet, true /* bGetAllReasons */, requestedRate,
                true /* isRequestingRateLock */, "" /* sProOFinPmt */, false /* skipRateAdjError */, E_sLienQualifyModeT.ThisLoan, lpePriceGroupId,
                version, "" /* tempLpeTaskMessageXml */, debugResultStartTicks, renderResultModeT,
                E_sPricingModeT.RegularPricing, Guid.Empty, 0, 0, 0, false, false, "", 0, "", null, null, null, null,
                options, null);

        }

        public static string GenerateTemporaryAcceptFirstLoanMessage(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate,
            Guid lpePriceGroupId, string version, string debugResultStartTicks, E_RenderResultModeT renderResultModeT)
        {
            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();
            string investorName = RetrieveInvestorName(productId);
            lpSet.Add(productId, investorName, "");

            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);

            List<LpeTaskMessage> messages = ConstructLpeTaskMessages(
                lpeTaskT: E_LpeTaskT.TemporaryAcceptFirstLoan,
                principal: principal,
                loanId: loanId,
                lpSet: lpSet,
                bGetAllReasons: true,
                selectedRate: requestedRate,
                isRequestingRateLock: true,
                sProOFinPmt: "",
                skipRateAdjError: false,
                sLienQualifyModeT: E_sLienQualifyModeT.ThisLoan,
                lpePriceGroupId: lpePriceGroupId,
                version: version,
                tempLpeTaskMessageXml: "",
                debugResultStartTicks: debugResultStartTicks,
                renderResultModeT: renderResultModeT,
                pricingModeT: E_sPricingModeT.RegularPricing,
                firstLienLpId: Guid.Empty,
                firstLienNoteRate: 0,
                firstLienPoint: 0,
                firstLienMPmt: 0,
                isSetRateLockPricingOption: false,
                isAddConditionOption: false,
                uniqueChecksum: "",
                secondLienMPmt: 0,
                secondLienUniqueChecksum: "",
                state: null,
                newRateLockDays: null,
                quickPricerLoanItem: null,
                ratemergeparrate: null,
                historicalOptions: options,
                denialExceptions: null,
                isForTest: false,
                isAutoProcess: false);

            return SerializationHelper.XmlSerialize(messages[0]);
        }

        public static void SaveTemporaryAcceptFirstLoanMessage(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate,
            Guid lpePriceGroupId, string version, string debugResultStartTicks, E_RenderResultModeT renderResultModeT)
        {
            CPageData dataLoan = new CTempLpeTaskMessageXmlData(loanId);
            dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);
            dataLoan.sTempLpeTaskMessageXml = GenerateTemporaryAcceptFirstLoanMessage(principal, loanId, productId, requestedRate,
                lpePriceGroupId, version, debugResultStartTicks, renderResultModeT);
            dataLoan.Save();
        }

        public static Guid SubmitToEngineForRenderCertificate(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate,
            E_sLienQualifyModeT sLienQualifyModeT, Guid lpePriceGroupId, string sTempLpeTaskMessageXml, string version, E_RenderResultModeT renderResultModeT,
            E_sPricingModeT pricingModeT, string uniqueChecksum, PricingState state, string parrate)
        {
            bool bGetAllReasons = true;

            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();
            string investorName = RetrieveInvestorName(productId);
            lpSet.Add(productId, investorName, "");

            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return Process(E_LpeTaskT.RenderCertificate, principal, loanId, lpSet, bGetAllReasons, requestedRate, false /* isRequestingRateLock */,
                "" /* sProOFinPmt */, false /* skipRateAdjError */, sLienQualifyModeT, lpePriceGroupId, version, sTempLpeTaskMessageXml,
                "0" /* debugResultStartTicks */, renderResultModeT, pricingModeT,
                Guid.Empty, 0, 0, 0, false, false, uniqueChecksum, 0, "" /* SecondLienUniqueChecksum */, state, null, null, parrate,
                options, null);
        }

        public static Guid SubmitToEngineForRenderCertificate(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate,
            E_sLienQualifyModeT sLienQualifyModeT, Guid lpePriceGroupId, string sTempLpeTaskMessageXml, string version, E_RenderResultModeT renderResultModeT,
            E_sPricingModeT pricingModeT, string uniqueChecksum, string parrate, HybridLoanProgramSetOption historicalOptions)
        {
            bool bGetAllReasons = true;

            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();
            string investorName = RetrieveInvestorName(productId);
            lpSet.Add(productId, investorName, "");


            return Process(E_LpeTaskT.RenderCertificate, principal, loanId, lpSet, bGetAllReasons, requestedRate, false /* isRequestingRateLock */,
                "" /* sProOFinPmt */, false /* skipRateAdjError */, sLienQualifyModeT, lpePriceGroupId, version, sTempLpeTaskMessageXml,
                "0" /* debugResultStartTicks */, renderResultModeT, pricingModeT,
                Guid.Empty, 0, 0, 0, false, false, uniqueChecksum, 0, "" /* SecondLienUniqueChecksum */, null, null,
                null /* quickPricerLoanItem */, parrate,
                historicalOptions, null);


        }

        public static Guid SubmitToEngineForRenderCertificate(AbstractUserPrincipal principal, Guid loanId, Guid productId, string requestedRate,
            E_sLienQualifyModeT sLienQualifyModeT, Guid lpePriceGroupId, string sTempLpeTaskMessageXml, string version, E_RenderResultModeT renderResultModeT,
            E_sPricingModeT pricingModeT, string uniqueChecksum, Guid firstLienLpId, decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt, PricingState state, string parRate)
        {
            bool bGetAllReasons = true;

            LoanProgramByInvestorSet lpSet = new LoanProgramByInvestorSet();
            string investorName = RetrieveInvestorName(productId);
            lpSet.Add(productId, investorName, "");

            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return Process(E_LpeTaskT.RenderCertificate, principal, loanId, lpSet, bGetAllReasons, requestedRate, false /* isRequestingRateLock */,
                "" /* sProOFinPmt */, false /* skipRateAdjError */, sLienQualifyModeT, lpePriceGroupId, version, sTempLpeTaskMessageXml,
                "0" /* debugResultStartTicks */, renderResultModeT, pricingModeT,
                firstLienLpId, //first lien lp id  
                firstLienNoteRate,  //first lien note rate
                firstLienPoint, //first lien  point
                firstLienMPmt, //first lien  mrate
                false, //is set rate lock pricing option 
                false, //add condition as option
                uniqueChecksum,
                0,  //second lien mpayment
                "" /* SecondLienUniqueChecksum */, state, null, null, parRate, options, null); //pricintg state
        }

        public static Guid SubmitPinToEngine(AbstractUserPrincipal principal, Guid loanId, Guid lpePriceGroupId, string tempLpeTaskMessageXml, PricingState state, LoanProgramByInvestorSet lpSet,
            Guid firstLienLpId, decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt)
        {
            E_sLienQualifyModeT sLienQualifyModeT = E_sLienQualifyModeT.OtherLoan;
            E_sPricingModeT pricingMode = E_sPricingModeT.RegularPricing;
            E_RenderResultModeT renderResultModeT = E_RenderResultModeT.BestPricesPerProgram;
            bool bGetAllReasons = principal.HasPermission(LendersOffice.Security.Permission.CanApplyForIneligibleLoanPrograms);
            decimal secondLienMPmt = 0;
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return Process(E_LpeTaskT.RunEngine, principal, loanId, lpSet, bGetAllReasons, "", false, "", false, sLienQualifyModeT, lpePriceGroupId, "",
      tempLpeTaskMessageXml, "0", renderResultModeT, pricingMode, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, false, false, "", secondLienMPmt, "", state, null,
      null /* quickPricerLoanItem */, null, options, null);
        }

        public static Guid SubmitPinToEngine(AbstractUserPrincipal principal, Guid loanId, Guid lpePriceGroupId, PricingState state, LoanProgramByInvestorSet lpSet, decimal secondLienMPmt )
        {
            E_sLienQualifyModeT sLienQualifyModeT = E_sLienQualifyModeT.ThisLoan;
            string tempLpeTaskMessageXml = "";
            E_RenderResultModeT renderResultModeT = E_RenderResultModeT.BestPricesPerProgram;

            E_sPricingModeT pricingMode = E_sPricingModeT.RegularPricing;
            Guid firstLienLpId = Guid.Empty;
            decimal firstLienNoteRate = 0;
            decimal firstLienPoint = 0;
            decimal firstLienMPmt = 0;
            bool bGetAllReasons = principal.HasPermission(LendersOffice.Security.Permission.CanApplyForIneligibleLoanPrograms);
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);

            return Process(E_LpeTaskT.RunEngine, principal, loanId, lpSet, bGetAllReasons, "", false, "", false, sLienQualifyModeT, lpePriceGroupId, "",
                tempLpeTaskMessageXml, "0", renderResultModeT, pricingMode, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, false, false, "", secondLienMPmt, "", state,null,
                null /* quickPricerLoanItem */, null, options, null);
        }

        public static Guid SubmitToEngine(AbstractUserPrincipal principal, Guid loanId, LoanProgramByInvestorSet lpSet,
            E_sLienQualifyModeT sLienQualifyModeT, Guid lpePriceGroupId, string tempLpeTaskMessageXml, E_RenderResultModeT renderResultModeT,
            E_sPricingModeT pricingModeT, Guid firstLienLpId, decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt, decimal secondLienMPmt,
            HybridLoanProgramSetOption historicalOptions, bool runMigrations = true, bool isAutoProcess = false)
        {
            bool bGetAllReasons = principal.HasPermission(LendersOffice.Security.Permission.CanApplyForIneligibleLoanPrograms);

            return Process(E_LpeTaskT.RunEngine, principal, loanId, lpSet, bGetAllReasons, "" /* selectedRate */,
                false /* isRequestingRateLock" */, "" /* sProOFinPmt */, false /* skipRateAdjError */, sLienQualifyModeT, lpePriceGroupId, "" /* version */,
                tempLpeTaskMessageXml, "0" /* debugResultStartTicks */, renderResultModeT,
                pricingModeT, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, false, false, "" /* uniqueChecksum */, secondLienMPmt, "" /* secondLienUniqueChecksum */, null, null,
                null /* quickPricerLoanItem */, null, historicalOptions, null, runMigrations: runMigrations, isAutoProcess:isAutoProcess);
        }

        public static Guid SubmitToEngineForQuickPricer(AbstractUserPrincipal principal, Guid loanId,
                                                        LoanProgramByInvestorSet lpSet, 
                                                        Guid lpePriceGroupId, 
                                                        E_RenderResultModeT renderResultModeT,  
                                                        QuickPricerLoanItem quickPricerLoanItem)
        {
            bool bGetAllReasons = principal.HasPermission(LendersOffice.Security.Permission.CanApplyForIneligibleLoanPrograms);
            var options = HybridLoanProgramSetOption.GetCurrentSnapshotOption(principal.BrokerDB);
            return Process(E_LpeTaskT.RunEngine, principal,  loanId, lpSet, bGetAllReasons, "" /* selectedRate */,
                            false /* isRequestingRateLock" */, "" /* sProOFinPmt */, false /* skipRateAdjError */,
                            E_sLienQualifyModeT.ThisLoan /* sLienQualifyModeT */, lpePriceGroupId, "" /* version */,
                            "" /*tempLpeTaskMessageXml */, "0" /* debugResultStartTicks */, renderResultModeT,
                            E_sPricingModeT.RegularPricing /* pricingModeT */, Guid.Empty /* firstLienLpId */, 0 /* firstLienNoteRate */, 
                            0 /* firstLienPoint */, 0 /* firstLienMPmt */, false, false, "" /* uniqueChecksum */,
                            0 /* secondLienMPmt */, "" /* secondLienUniqueChecksum */, null, null, quickPricerLoanItem, null, options, null);

        }

        public static Guid SubmitToEngineForAutoLock(AbstractUserPrincipal principal, Guid loanId, LoanProgramByInvestorSet lpSet,
            E_sLienQualifyModeT sLienQualifyModeT, Guid lpePriceGroupId, E_RenderResultModeT renderResultModeT, int? rateLockDays,
            HybridLoanProgramSetOption historicalOptions)
        {

            return Process(E_LpeTaskT.RunEngine, principal, loanId, lpSet, false /* bGetAllReasons*/, "" /* selectedRate */,
                false /* isRequestingRateLock" */, "" /* sProOFinPmt */, false /* skipRateAdjError */, sLienQualifyModeT, lpePriceGroupId, "" /* version */,
                "" /*tempLpeTaskMessageXml*/, "0" /* debugResultStartTicks */, renderResultModeT,
                E_sPricingModeT.InternalBrokerPricing, Guid.Empty /*firstLienLpId*/, 0 /*firstLienNoteRate*/, 0 /*firstLienPoint*/, 
                0 /*firstLienMPmt*/, false, false, "" /* uniqueChecksum */, 0 /*secondLienMPmt*/, "" /* secondLienUniqueChecksum */, null, rateLockDays,
                null /* quickPricerLoanItem */, null, historicalOptions, null);
        }

        private static List<LpeTaskMessage> ConstructLpeTaskMessages(E_LpeTaskT lpeTaskT, AbstractUserPrincipal principal, Guid loanId, LoanProgramByInvestorSet lpSet,
            bool bGetAllReasons, string selectedRate, bool isRequestingRateLock, string sProOFinPmt,
            bool skipRateAdjError, E_sLienQualifyModeT sLienQualifyModeT, Guid lpePriceGroupId, string version, string tempLpeTaskMessageXml,
            string debugResultStartTicks, E_RenderResultModeT renderResultModeT, E_sPricingModeT pricingModeT,
            Guid firstLienLpId, decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt, bool isSetRateLockPricingOption,
            bool isAddConditionOption, string uniqueChecksum, decimal secondLienMPmt, string secondLienUniqueChecksum, PricingState state, int? newRateLockDays,
            QuickPricerLoanItem quickPricerLoanItem, string ratemergeparrate, HybridLoanProgramSetOption historicalOptions, List<DenialException> denialExceptions, bool isForTest, bool isAutoProcess)
        {
            if (lpeTaskT == E_LpeTaskT.SubmitRate80_20 && principal.BrokerId.Equals(new Guid("e9f006c9-cb83-4531-8925-08fccf4add63")))
            {
                Tools.LogInfo("OPM_225020: This task is created to place the following loan through the Submission Process for 80/20 loans.  Loan Id: " 
                    + loanId.ToString() + ", User Id: " + principal.UserId.ToString() +
                    " with the following Stack Trace: \n" +
                    Environment.StackTrace);
            }

            Guid requestId = Guid.NewGuid();

            List<LpeTaskMessage> returnTaskList = null;

            CLpRunOptions lpRunOptions = LoadLpRunOptions(principal, bGetAllReasons, lpePriceGroupId, 
                selectedRate, debugResultStartTicks, renderResultModeT, lpeTaskT, pricingModeT, 
                firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, 
                isSetRateLockPricingOption, isAddConditionOption, ratemergeparrate, historicalOptions);

            lpRunOptions.Version = version;
            lpRunOptions.UniqueChecksum = uniqueChecksum;
            lpRunOptions.SecondLienMPmt = secondLienMPmt;
            lpRunOptions.SecondLienUniqueChecksum = secondLienUniqueChecksum;
            lpRunOptions.PricingState = state;
            lpRunOptions.RateLockDays = newRateLockDays;
            lpRunOptions.DenialExceptions = denialExceptions;
            lpRunOptions.IsUsePricingSummaryDebug = isForTest;
            lpRunOptions.IsAutoProcess = isAutoProcess;
            
            DateTime dt = Tools.GetDBCurrentDateTime();
            DateTime expirationDate = dt.AddMinutes(ConstAppDavid.LPE_NumberOfMinutesBeforeMessageExpire + 1);

            ICollection investorList = lpSet.InvestorList;


            ArrayList invalidIdsList = new ArrayList();
            returnTaskList = new List<LpeTaskMessage>(1);
            ArrayList mainList = new ArrayList();
            string firstInvestorName = null;

            foreach (string investorName in investorList)
            {
                if (null == firstInvestorName)
                    firstInvestorName = investorName;

                Hashtable productCodeHash = lpSet.LoanProgramListByInvestor(investorName);

                foreach (string productCode in productCodeHash.Keys)
                {
                    ArrayList productList = (ArrayList)productCodeHash[productCode];
                    if (productList.Count == 1 && productCodeHash.Count == 1)
                    {
                        // 1/23/2007 dd - Only figure out the missing ids when rerun/apply/preview
                        foreach (Guid id in productList)
                        {
                            if (lpRunOptions.GetPriceGroupDetailsFor(id) == null)
                            {
                                invalidIdsList.Add(id);
                            }
                        }
                    }

                    mainList.AddRange(productList);
                }
            }

            Guid correlationId = Tools.GetLogCorrelationId(); 

            LpeTaskMessage msg = new LpeTaskMessage(principal, requestId, lpeTaskT, dt, lpRunOptions, loanId,
                mainList, isRequestingRateLock, sProOFinPmt, skipRateAdjError, sLienQualifyModeT, firstInvestorName, "", correlationId);
            msg.Header.ExpirationDate = expirationDate;
            msg.TemporaryLpeTaskMessageXml = tempLpeTaskMessageXml;
            msg.QuickPricerLoanItem = quickPricerLoanItem;
            returnTaskList.Add(msg);

            return returnTaskList;
        }

        private static Guid Process(E_LpeTaskT lpeTaskT, AbstractUserPrincipal principal, Guid loanId, LoanProgramByInvestorSet lpSet,
            bool bGetAllReasons, string selectedRate, bool isRequestingRateLock, string sProOFinPmt,
            bool skipRateAdjError, E_sLienQualifyModeT sLienQualifyModeT, Guid lpePriceGroupId, string version, string tempLpeTaskMessageXml,
            string debugResultStartTicks, E_RenderResultModeT renderResultModeT, E_sPricingModeT pricingModeT,
            Guid firstLienLpId, decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt, bool isSetRateLockPricingOption,
            bool isAddConditionOption, string uniqueChecksum, decimal secondLienMPmt, string secondLienUniqueChecksum, PricingState state, int? newRateLockDays,
            QuickPricerLoanItem quickPricerLoanItem, string mergeGroupParrate, HybridLoanProgramSetOption historicalOptions, List<DenialException> denialExceptions,
            bool isForTest = false, bool runMigrations = true, bool isAutoProcess = false)
        {

            if (null == lpSet || lpSet.LoanProgramsCount == 0)
            {
                // If there are no product list to run pricing engine then return special GUID that will return isComplete.
                return ConstAppDavid.LPE_NoProductRequestId;
            }

            if (runMigrations)
            {
                BrokerDB brokerDb = BrokerDB.RetrieveById(principal.BrokerId);

                // OPM 209347 - Verify that loan has LO/Secondary (dependent on branch channel).
                CPageData dataLoan = new NotEnforceAccessControlPageData(loanId, new string[] { "sClosingCostFeeVersionT", "sTestLoanFileEnvironment",
                "sLinkedLoanInfo", "sBranchChannelT", "sEmployeeLoanRepId", "sEmployeeExternalSecondaryId" });
                dataLoan.ByPassFieldSecurityCheck = true;
                dataLoan.AllowSaveWhileQP2Sandboxed = true;
                dataLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                if (!brokerDb.BypassEmployeeAssignmentRestrictionForPricing &&
                    quickPricerLoanItem == null && dataLoan.sLoanFileT != E_sLoanFileT.QuickPricer2Sandboxed &&
                    ((dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent && dataLoan.sEmployeeExternalSecondaryId == Guid.Empty) ||
                    (dataLoan.sBranchChannelT != E_BranchChannelT.Correspondent && dataLoan.sEmployeeLoanRepId == Guid.Empty)))
                {
                    string msg = "assignment is required in order to run PML.";
                    msg = (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent ? "Secondary " : "Loan Officer ") + msg;

                    throw new CBaseException(msg, msg);
                }

                // Verify Lo/Secondary on linked loan
                CPageData secondLoan = null;
                if (dataLoan.sLinkedLoanInfo.IsLoanLinked)
                {
                    secondLoan = new NotEnforceAccessControlPageData(dataLoan.sLinkedLoanInfo.LinkedLId, new string[] { "sClosingCostFeeVersionT",
                "sBranchChannelT", "sEmployeeLoanRepId", "sEmployeeExternalSecondaryId" });
                    secondLoan.ByPassFieldSecurityCheck = true;
                    secondLoan.AllowSaveWhileQP2Sandboxed = true;
                    secondLoan.InitSave(ConstAppDavid.SkipVersionCheck);

                    if (!brokerDb.BypassEmployeeAssignmentRestrictionForPricing &&
                        quickPricerLoanItem == null && secondLoan.sLoanFileT != E_sLoanFileT.QuickPricer2Sandboxed &&
                        ((secondLoan.sBranchChannelT == E_BranchChannelT.Correspondent && secondLoan.sEmployeeExternalSecondaryId == Guid.Empty) ||
                        (secondLoan.sBranchChannelT != E_BranchChannelT.Correspondent && secondLoan.sEmployeeLoanRepId == Guid.Empty)))
                    {
                        string msg = "assignment is required on 2nd Lien Loan in order to run PML.";
                        msg = (dataLoan.sBranchChannelT == E_BranchChannelT.Correspondent ? "Secondary " : "Loan Officer ") + msg;

                        throw new CBaseException(msg, msg);
                    }
                }

                // OPM 209347 - Migrate Legacy Loans if New Fee Service is enabled.
                if (brokerDb.IsEnableNewFeeService)
                {
                    if (pricingModeT == E_sPricingModeT.Undefined || pricingModeT == E_sPricingModeT.RegularPricing)
                    {
                        // OPM 217783 - Only migrate if Fee Service file contains new rules.
                        FeeServiceRevision currentRevision = FeeServiceApplication.GetCurrentRevision(principal.BrokerId, dataLoan.sTestLoanFileEnvironment.EnableFeeServiceTest);
                        if (currentRevision != null && currentRevision.FeeTemplates.Any(t => t.GetTemplateFields().Any(f => f.RuleType != FeeServiceRuleType.Legacy)))
                        {
                            E_sClosingCostFeeVersionT minVersion = brokerDb.MigrateToLegacyButMigratedOnPricing ? E_sClosingCostFeeVersionT.LegacyButMigrated : E_sClosingCostFeeVersionT.ClosingCostFee2015;
                            if (dataLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                            {
                                ClosingCostFeeMigration.Migrate(principal, loanId, minVersion, true);
                            }
                            else if (dataLoan.sClosingCostFeeVersionT < minVersion)
                            {
                                dataLoan.sClosingCostFeeVersionT = minVersion;
                                dataLoan.Save();
                            }

                            // OPM 227914 - Migrate linked loan if it exists.
                            if (secondLoan != null)
                            {
                                if (secondLoan.sClosingCostFeeVersionT == E_sClosingCostFeeVersionT.Legacy)
                                {
                                    ClosingCostFeeMigration.Migrate(principal, dataLoan.sLinkedLoanInfo.LinkedLId, minVersion, true);
                                }
                                else if (secondLoan.sClosingCostFeeVersionT < minVersion)
                                {
                                    secondLoan.sClosingCostFeeVersionT = minVersion;
                                    secondLoan.Save();
                                }
                            }
                        }
                    }
                }
            }

            List<LpeTaskMessage> taskList = ConstructLpeTaskMessages(lpeTaskT, principal, 
                loanId, lpSet, bGetAllReasons, selectedRate, isRequestingRateLock,
                sProOFinPmt, skipRateAdjError, sLienQualifyModeT, lpePriceGroupId, 
                version, tempLpeTaskMessageXml, debugResultStartTicks, renderResultModeT,
                pricingModeT, firstLienLpId, firstLienNoteRate, firstLienPoint, 
                firstLienMPmt, isSetRateLockPricingOption, isAddConditionOption, uniqueChecksum,
                secondLienMPmt, secondLienUniqueChecksum, state, newRateLockDays,
                quickPricerLoanItem, mergeGroupParrate, historicalOptions, denialExceptions, isForTest, isAutoProcess);

            int numberOfRequests = null == taskList ? 0 : taskList.Count;
            if (numberOfRequests == 0)
                throw new GenericUserErrorMessageException("There is no LpeTaskMessage request.");
 
            Guid lpeRequestBatchId = taskList[0].Header.LpeRequestBatchId;
            DateTime expirationDate = taskList[0].Header.ExpirationDate;
            for (int i = 0; i < numberOfRequests; i++)
            {
                LpeTaskMessage msg = taskList[i];
                msg.Header.NumberOfRequestsInBatch = numberOfRequests;
                if (DistributeUnderwritingSettings.ServerT == E_ServerT.RequestOnly)
                {

                    string xml = SerializationHelper.XmlSerialize(msg);
                    string messageType = "";
                    switch (lpeTaskT)
                    {
                        case E_LpeTaskT.RunEngine:
                            messageType = "PriceRequest";
                            break;
                        case E_LpeTaskT.RenderCertificate:
                            messageType = "PreviewRequest";
                            break;
                        case E_LpeTaskT.SubmitRate:
                        case E_LpeTaskT.SubmitRate80_20:
                        case E_LpeTaskT.TemporaryAcceptFirstLoan:
                        case E_LpeTaskT.VerifyFirstLoanProgram:
                        case E_LpeTaskT.PerformInternalPricingAction:
                            messageType = "SingleSubmit";
                            break;
                        default:
                            throw new UnhandledEnumException(lpeTaskT);
                    }

                    try
                    {
                        SqlParameter[] parameters = {
                                                        new SqlParameter("@LpeRequestBatchId", lpeRequestBatchId),
                                                        new SqlParameter("@LpeRequestXmlContent", xml),
                                                        new SqlParameter("@LpeRequestCurrentPartInBatch", i + 1),
                                                        new SqlParameter("@LpeRequestNumberOfRequestsInBatch", numberOfRequests),
                                                        new SqlParameter("@LpeRequestType", messageType),
                                                        new SqlParameter("@ExpirationMinutes", ConstAppDavid.LPE_NumberOfMinutesBeforeMessageExpire),
                                                        new SqlParameter("@InvestorName",msg.RequestData.InvestorName)
                                                    };
                        StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_InsertRequestMessage", 5, parameters);
                    }
                    catch (Exception exc)
                    {
                        Tools.LogError("Unable to insert message to queue.", exc);
                        throw new GenericUserErrorMessageException("Unable to insert LPE message to LPE_Request table.");
                    }

                }
                else
                {

                    lock (s_mainCounterIndexLock)
                    {
                        // 5/23/2007 dd - We need to fake a Identity column on LpeAuthor where request does not insert to DB.
                        taskList[i].Header.LpeRequestIntId = s_mainCounterIndex;
                        s_mainCounterIndex++;
                    }

                    long startTicks = DateTime.Now.Ticks;

                    XmlDocument result = LpeTaskProcessor.Execute(taskList[i]);

                    long endTicks = DateTime.Now.Ticks;
                    long durationInMs = (endTicks - startTicks) / 10000L;

                    MessageLabel label = new MessageLabel(lpeRequestBatchId, i + 1, numberOfRequests, expirationDate);

                    taskList[i].Header.TempMessageLabel = label;
                    LpeDistributeResults.Send(taskList[i], result, durationInMs);


                }

            }
            return lpeRequestBatchId;

        }
        private static long s_mainCounterIndex = 0;
        private static object s_mainCounterIndexLock = new object();


        /// <summary>
        /// Return # of ms that client script SHOULD invoke IsResultAvailable. THIS IS EXPERIMENTAL METHOD.
        /// Scenario, we don't want to hard code the polling time in the javascript, because it will be different depend on 
        /// number of loan products. For example: if number of loan product is 1 then polling should take place every 200ms, but
        /// if number of loan products is 100 then polling should take place every 2 - 5 seconds etc...
        /// </summary>
        /// <param name="requestID"></param>
        /// <returns></returns>
        public static int GetSuggestedPollingInterval(Guid requestID)
        {
            if (requestID == ConstAppDavid.LPE_NoProductRequestId)
                return 10;

            return 1000; // 1/6/2011 dd - Simplify life and make the polling interval to be 1 second for all.
        }

        private static CLpRunOptions LoadLpRunOptions(AbstractUserPrincipal principal, bool bGetAllReasons, Guid lpePriceGroupId,
            string requestedRateString, string debugResultStartTicks, E_RenderResultModeT renderResultModeT, E_LpeTaskT lpeTaskT,
            E_sPricingModeT pricingModeT, Guid firstLienLpId, decimal firstLienNoteRate, decimal firstLienPoint, decimal firstLienMPmt,
            bool isSetRateLockPricingOption, bool isAddConditionOption, string ratemergeparrate,
            HybridLoanProgramSetOption historicalOptions)
        {
            
            // Parse out requested rate and requested fee. Requested rate is in following format. {rate}:{point}
            decimal selectedRate = 0.0M;
            decimal selectedFee = 0.0M;
            string selectedRateOptionId = "";

            try
            {
                if (null != requestedRateString && "" != requestedRateString)
                {
                    string[] parts = requestedRateString.Split(':');
                    if (parts.Length >= 2)
                    {
                        selectedRate = decimal.Parse(parts[0]);

                        selectedFee = decimal.Parse(parts[1]);

                        if (parts.Length >= 3)
                        {
                            selectedRateOptionId = parts[2];
                        }
                    }
                }

            }
            catch { }

            bool bSnapupRate1Eighth;
            int nLockPeriodAdj;

            BrokerDB broker = BrokerDB.RetrieveById(principal.BrokerId);
            bSnapupRate1Eighth = broker.IsRoundUpLpeRateTo1Eighth;
            nLockPeriodAdj = broker.LpeLockPeriodAdj;
            string lpePriceGroupName = "";
            bool roundUpLpeFee = broker.RoundUpLpeFee;
            decimal roundUpLpeFeeInterval = broker.RoundUpLpeFeeInterval;
            SHA256Checksum lpePriceGroupContentKey = SHA256Checksum.Invalid;

            if (lpePriceGroupId != Guid.Empty)
            {
                var pg = LendersOffice.ObjLib.PriceGroups.PriceGroup.RetrieveByID(lpePriceGroupId, principal.BrokerId);
                if (pg != null)
                {
                    lpePriceGroupName = pg.Name;
                    roundUpLpeFee = pg.IsRoundUpLpeFee;
                    roundUpLpeFeeInterval = pg.RoundUpLpeFeeToInterval;
                    lpePriceGroupContentKey = pg.ContentKey;
                }
            }


            bool isRateOptionsWorseThanLowerRateShown = true;

            if (ConstStage.IsEnableRateFilterForOPM_130011)
            {
                isRateOptionsWorseThanLowerRateShown = broker.IsShowRateOptionsWorseThanLowerRate;
            }
            else
            {
                // Original logic.

                if (renderResultModeT == E_RenderResultModeT.Regular)
                {
                    // 7/1/2009 dd - OPM 24899
                    isRateOptionsWorseThanLowerRateShown = broker.IsShowRateOptionsWorseThanLowerRate;
                }
            }

            CLpRunOptions lpRunOptions = new CLpRunOptions(principal.BrokerId, bGetAllReasons, bSnapupRate1Eighth, nLockPeriodAdj, lpePriceGroupName, lpePriceGroupId, lpePriceGroupContentKey,
                selectedRate, selectedFee, selectedRateOptionId,
                roundUpLpeFee, roundUpLpeFeeInterval, principal.IsPricingMultipleAppsSupported, isRateOptionsWorseThanLowerRateShown,
                lpeTaskT, pricingModeT, firstLienLpId, firstLienNoteRate, firstLienPoint, firstLienMPmt, isSetRateLockPricingOption, isAddConditionOption, ratemergeparrate, historicalOptions);


            // 9/4/2013 dd - OPM 137164
            if (principal is ConsumerPortalUserPrincipal)
            {
                lpRunOptions.RunPmlRequestSourceT = E_RunPmlRequestSourceT.ConsumerPortal;
            }
            else
            {
                lpRunOptions.RunPmlRequestSourceT = principal.IsOriginalPrincipalAConsumer ? E_RunPmlRequestSourceT.ConsumerPortal : E_RunPmlRequestSourceT.Regular;
            }

            long ticks = 0L;

            if (string.IsNullOrEmpty(debugResultStartTicks) == false)
            {
                long.TryParse(debugResultStartTicks, out ticks);
            }

            lpRunOptions.DebugResultStartTicks = ticks;

            return lpRunOptions;


        }


    }
}
