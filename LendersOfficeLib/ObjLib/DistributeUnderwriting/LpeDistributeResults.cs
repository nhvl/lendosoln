///
/// Author: David Dao
/// 
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Xml;
using DataAccess;
using LendersOffice.Constants;
namespace LendersOffice.DistributeUnderwriting
{

    public class LpeDistributeResults
	{
        public static void Send(LpeTaskMessage task, XmlDocument results, long durationInMs) 
        {
            // 11/18/2005 dd - Debug info contains 2 parts. First part is the Message.Label
            // Second part is extra information in following format {Task}|{CreatedDate}|{UserId}|{LoanId}|{ProductCount}|{durationInMs}
            // 5/8/2007 dd - New extra debug information is in following format  {Task}|{CreatedDate}|{ServerName}|{Investor}|{ProductCount}|{durationInMs}
            string debugInfo = string.Format("{0}^{1}|{2}|Calc{3}|{4}|#LP={5}|{6}ms",
                task.Header.TempMessageLabel, task.Header.LpeTaskT, task.Header.CreatedDate, ConstAppDavid.ServerName, task.RequestData.InvestorName, task.RequestData.ProductIdList.Count, durationInMs);

            List<SqlParameter> parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@RequestBatchId", task.Header.LpeRequestBatchId));
            parameters.Add(new SqlParameter("@RequestNumberOfRequestsInBatch", task.Header.NumberOfRequestsInBatch));
            parameters.Add(new SqlParameter("@ResultDebugInfo", debugInfo));
            parameters.Add(new SqlParameter("@RequestExpirationD", task.Header.ExpirationDate.AddMinutes(10)));
            parameters.Add(new SqlParameter("@LpeRequestIntId", task.Header.LpeRequestIntId));

            SaveResultsToTempFileDB("LpeResult_" + task.Header.LpeRequestIntId, results);

            uint resultIndex = GetResultTableIndex(task.Header.LpeRequestBatchId);
            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_InsertResult_" + resultIndex, 1, parameters);
        }
        public static void Send(LpeTaskMessage task, Exception exc) 
        {
            Tools.LogInfo("LpeDistributeResults::Send  Exception=" + exc.ToString());

            string errorMessage = "";

            if (exc is CBaseException)
                errorMessage = ((CBaseException) exc).UserMessage;
            else
                errorMessage = exc.Message;

            XmlDocument result = new XmlDocument();
            XmlElement rootElement = result.CreateElement("results");

            result.AppendChild(rootElement);

            XmlElement errorElement = result.CreateElement("error");
            rootElement.AppendChild(errorElement);

            errorElement.InnerText = errorMessage;

            // OPM 69509. We still need this data even in this error
            XmlElement rateChange = result.CreateElement("ratechange");
            rootElement.AppendChild(rateChange);
            if (exc is LendersOfficeApp.los.RatePrice.CInvalidRateVersionException)
            {
                rateChange.InnerText = ((LendersOfficeApp.los.RatePrice.CInvalidRateVersionException)exc).RateOptionChange;
                Tools.LogInfo(String.Concat("[CInvalidRateVersionException] ", rateChange.InnerText ?? "-empty-"));
            }
            else
            {
                rateChange.InnerText = "";
            }

            Send(task, result, 0);
        }

        /// <summary>
        /// UI must poll this method to check if underwriting result is ready for processed.
        /// </summary>
        /// <param name="requestID"></param>
        /// <returns></returns>
        public static bool IsResultAvailable(Guid lpeRequestBatchId) 
        {
            if (lpeRequestBatchId == ConstAppDavid.LPE_NoProductRequestId) 
                return true;
            else 
            {
                SqlParameter[] parameters = { new SqlParameter("@RequestBatchId", lpeRequestBatchId) };
                uint resultIndex = GetResultTableIndex(lpeRequestBatchId);
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReaderWithRetries(DataSrc.LOTransient, "LPE_IsResultReady_" + resultIndex, 3, parameters)) 
                {
                    if (reader.Read()) 
                    {
                        int currentCount = (int) reader["CurrentCount"];

                        if (currentCount == 0)
                            return false;

                        int numberOfRequestsInBatch = (int) reader["RequestNumberOfRequestsInBatch"];

                        return currentCount == numberOfRequestsInBatch;
                    } 
                }
                
            }
            return false;
        }

        /// <summary>
        /// Retrieve the result with the given ID. By default this method will then delete the result.
        /// </summary>
        /// <param name="lpeRequestBatchID">The id of the request.</param>
        /// <param name="deleteRequest">Disable the default behavior of deleting the result.</param>
        /// <returns></returns>
        public static UnderwritingResultItem RetrieveResultItem(Guid lpeRequestBatchID, bool deleteRequest = true) 
        {
            if (lpeRequestBatchID == ConstAppDavid.LPE_NoProductRequestId)
                return new UnderwritingResultItem(lpeRequestBatchID);
            else 
            {

                UnderwritingResultItem item = null;

                uint resultIndex = GetResultTableIndex(lpeRequestBatchID);

                SqlParameter[] parameters = { new SqlParameter("@RequestBatchId", lpeRequestBatchID) };

                List<long> tempList = new List<long>(50);
                List<DateTime> resultDoneDList = new List<DateTime>(50);


                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOTransient, "LPE_RetrieveResultByRequestID_" + resultIndex, parameters)) 
                {
                    while (reader.Read()) 
                    {
                        long lpeRequestIntId = (long) reader["LpeRequestIntId"];
                        DateTime resultDoneD = (DateTime) reader["ResultDoneD"];

                        tempList.Add(lpeRequestIntId);
                        resultDoneDList.Add(resultDoneD);
                    }
                }

                for (int i = 0; i < tempList.Count; i++)
                {
                    long lpeRequestIntId = tempList[i];
                    DateTime resultDoneD = resultDoneDList[i];

                    XmlDocument results = null;

                    results = LoadResultFromTempFileDB("LpeResult_" + lpeRequestIntId);


                    if (null == item) 
                    {
                        item = new UnderwritingResultItem(lpeRequestBatchID);    
                    }
                    item.AddResults(results, resultDoneD);
                }

                if (deleteRequest)
                {
                    try
                    {
                        if (null != item)
                        {
                            parameters = new SqlParameter[] { new SqlParameter("@RequestBatchId", lpeRequestBatchID) };
                            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOTransient, "LPE_RemoveResultsByBatchId_" + resultIndex, 1, parameters);

                            for (int i = 0; i < tempList.Count; i++)
                            {
                                long lpeRequestIntId = tempList[i];
                                FileDBTools.Delete(E_FileDB.Temp, "LpeResult_" + lpeRequestIntId);
                            }
                        }
                    }
                    catch
                    {
                        // 4/12/2007 dd - Ignore all exceptions.
                    }
                }
                return item;

            }
        }

        private static XmlDocument LoadResultFromTempFileDB(string fileDbKey) 
        {
            XmlDocument xmlDoc = new XmlDocument();

            FileDBTools.UseFile(E_FileDB.Temp, fileDbKey, (fi) =>
            {
                XmlTextReader xmlReader = null;
                try
                {
                    xmlReader = new XmlTextReader(new FileStream(fi.FullName, FileMode.Open, FileAccess.Read));
                    xmlReader.XmlResolver = null;
                    xmlDoc.Load(xmlReader);
                }
                finally
                {
                    if (null != xmlReader)
                        xmlReader.Close();
                }

            });


            return xmlDoc;
        }

        private static void SaveResultsToTempFileDB(string fileDbKey, XmlDocument doc) 
        {
            string path = TempFileUtils.NewTempFilePath() + ".xml";
            doc.Save(path);
            Stopwatch sw = Stopwatch.StartNew();
            FileDBTools.WriteFile(E_FileDB.Temp, fileDbKey, path);
            sw.Stop();
        }


        public static uint GetResultTableIndex(Guid lpeRequestBatchId) 
        {
            // Need to take Math.Abs otherwise it could return negative value.
            return (uint) lpeRequestBatchId.GetHashCode() % 10;
        }
        public static UnderwritingResultItem RetrieveResultsSynchronous(Guid lpeRequestBatchId) 
        {
            int timeout = DistributeUnderwritingEngine.GetSuggestedPollingInterval(lpeRequestBatchId);

            DateTime expirationDate = DateTime.Now.AddMinutes(ConstAppDavid.LPE_NumberOfMinutesBeforeMessageExpire); // Result time out is 1 minute.

            while (!LpeDistributeResults.IsResultAvailable(lpeRequestBatchId)) 
            {
                if (DateTime.Now.CompareTo(expirationDate) > 0) 
                {
                    throw new DistributeTimeoutException();
                }

                try 
                {
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(timeout);
                } 
                catch {}
            }
            return LpeDistributeResults.RetrieveResultItem(lpeRequestBatchId);
        }

	}
}
