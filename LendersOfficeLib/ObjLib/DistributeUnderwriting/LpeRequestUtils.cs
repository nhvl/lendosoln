///
/// Author: David Dao
///


namespace LendersOffice.DistributeUnderwriting
{
    public class LpeRequestUtils 
    {
        public static void RemoveRequestMessage(LpeTaskMessage task) 
        {
            LpeProcessingController.UpdateTaskStatus(task.Header.LpeRequestIntId, "Successful");
        }

        public static void ResetMessageToNewStatus(LpeTaskMessage task) 
        {
            LpeProcessingController.UpdateTaskStatus(task.Header.LpeRequestIntId, "Error");
        }
    }
}
