using System;
using System.Collections;
using System.Text;
using System.Xml;
using LendersOfficeApp.los.RatePrice;
using System.Collections.Generic;
using LendersOffice.Admin;
using DataAccess;
using LendersOffice.Security;

namespace LendersOffice.DistributeUnderwriting
{
    public class UnderwritingResultItem 
    {
        private class NoEqualLongComparer : IComparer 
        {
            public int Compare(object x, object y) 
            {
                // 5/31/2007 dd - Compare in descending order.
                long dt0 = (long) x;
                long dt1 = (long) y;

                if (dt0 < dt1)
                    return 1;
                else if (dt0 > dt1)
                    return -1;
                else
                    return 1;
            }
        }

        private ArrayList m_results = null;
        /// <summary>
        /// 1/3/2014 AV - 148173 QM Par Rate Inconsistent Between RunPMLForRegisteredLoans And PML Submission
        /// Contains All the applicant prices, including non selected
        /// </summary>
        private List<CApplicantPriceXml> m_fullResults = null;
        private bool m_isErrorMessage = false;

        private string m_errorMessage = "";
        private string m_rateChange = "";

        private long m_calcServerStartTicks = long.MaxValue;
        private long m_calcServerEndTicks = long.MinValue;
        private long m_batchCreatedTicks = long.MaxValue;
        private int m_calcServerCount = 0;

        private SortedList m_detailTransactionList = new SortedList(new NoEqualLongComparer());

        public UnderwritingResultItem(Guid requestID) 
        {
            m_results = new ArrayList();
            m_fullResults = new List<CApplicantPriceXml>();
        }

        public bool IsErrorMessage 
        {
            get { return m_isErrorMessage; }
        }

        public string ErrorMessage 
        {
            get { return m_errorMessage; }
        }

        public string RateChange
        {
            get { return m_rateChange; }
        }

        public string DetailTransactionMessage 
        {
            get { 
                StringBuilder sb = new StringBuilder();
                foreach (string str in m_detailTransactionList.Values) 
                {
                    sb.Append(str);
                }
                return sb.ToString();
            }
        }

        public DateTime BatchCreatedDate 
        {
            get 
            {
                if (m_batchCreatedTicks != long.MaxValue) 
                {
                    return new DateTime(m_batchCreatedTicks);
                }
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// This returns total time spend calc server process requests. Since we are in distribute mode, the start time
        /// will be the earliest time pick up by calc server. The end time will be the lastest time finish by calc server.
        /// 5/31/07 - dd. I changed the excutation duration is the earliest time a request insert to a database and a latest time finish by calc server.
        /// </summary>
        public long CalcServerExecutionDurationInMs 
        {
            get { return (m_calcServerEndTicks - m_calcServerStartTicks) / 10000L; }
        }

        public int CalcServerCount 
        {
            get { return m_calcServerCount; }
        }
        public string DebugDetail
        {
            get;
            private set;
        }
        public void AddResults(XmlDocument results, DateTime resultDoneD) 
        {

            XmlElement timingDebugElement = (XmlElement) results.SelectSingleNode("//results/TimingDebug[1]");

            if (null != timingDebugElement) 
            {
                // Stage 0 - When LpeRequest created.
                // Stage 1 - When worker thread begin to work on LpeRequest.
                // Stage 2 - When worker thread finish work on LpeRequest.
                // Stage 3 - When result are save to FileDB and insert into Result DB table.
                long startRequestTicks = long.Parse(timingDebugElement.GetAttribute("start_request")); // Stage 0.
                long startTicks = long.Parse(timingDebugElement.GetAttribute("start")); // Stage 1.
                long endTicks = long.Parse(timingDebugElement.GetAttribute("end")); // Stage 2.
                long resultDoneTicks = resultDoneD.Ticks; // Stage 3.

                int numOfProducts = int.Parse(timingDebugElement.GetAttribute("num_of_products"));
                long duration = (endTicks - startTicks) / 10000L;
                long avgMsPerLp = 0;
                if (numOfProducts != 0) 
                {
                    avgMsPerLp = duration / numOfProducts;
                }

                m_batchCreatedTicks = startRequestTicks;
                string investor = timingDebugElement.GetAttribute("investor");
                string currentPart = timingDebugElement.GetAttribute("current_part");
                string threadName = timingDebugElement.GetAttribute("thread");
                string productCode = timingDebugElement.GetAttribute("product_code");
                string lperequestintid = timingDebugElement.GetAttribute("lperequestintid");
                DebugDetail = timingDebugElement.GetAttribute("detail");
                if (startRequestTicks < m_calcServerStartTicks)
                    m_calcServerStartTicks = startRequestTicks;

                if (m_calcServerEndTicks < resultDoneTicks) 
                    m_calcServerEndTicks = resultDoneTicks;

                m_detailTransactionList.Add(endTicks, string.Format("<tr><td>#{0}</td><td>Id={1}</td><td>{2}</td><td>0-1={3}ms</td><td>1-2={4}ms</td><td>2-3={5}ms</td><td>0-3={6}ms</td><td>#LP={7}</td><td>ms/LP={8}ms</td></tr>",
                    currentPart, // 0
                    threadName, // 1
                    lperequestintid, // 2
                    (startTicks - startRequestTicks) / 10000L, // 3
                    (endTicks - startTicks) / 10000L, // 4
                    (resultDoneTicks - endTicks) / 10000L, // 5
                    (resultDoneTicks - startRequestTicks) / 10000L, // 6
                    numOfProducts, // 7
                    avgMsPerLp // 8
                    ));

            }

            XmlNodeList nodeList = results.SelectNodes("//results/CApplicantPrices/CApplicantPriceXml");
            XmlElement errorElement = (XmlElement)results.SelectSingleNode("//results/error[1]");
            XmlElement rateChangeElement = (XmlElement)results.SelectSingleNode("//results/ratechange[1]");
            XmlNodeList nonSelectedNodeList = results.SelectNodes("//results/CApplicantPricesNotRequested/CApplicantPriceXml");

            if (null != nonSelectedNodeList && nonSelectedNodeList.Count > 0)
            {
                foreach (XmlElement el in nonSelectedNodeList)
                {
                    CApplicantPriceXml o = CApplicantPriceXml.Parse(el);
                    m_fullResults.Add(o);
                }
            }

            if (null != nodeList && nodeList.Count > 0) 
            {
                foreach (XmlElement el in nodeList) 
                {
                    CApplicantPriceXml o = CApplicantPriceXml.Parse(el);
                    m_results.Add(o);
                    m_fullResults.Add(o);
                }
            } 
            else if (null != errorElement) 
            {
                m_isErrorMessage = true;
                m_errorMessage = errorElement.InnerText;
                if ( null != rateChangeElement )
                    m_rateChange = rateChangeElement.InnerText;
            }
            else 
            {
                // If we are unable to parse result then just dump the whole XML to results.
                // 3/3/2009 dd - Under what scenario does it reach this code? // 4/27/2009 dd - PML Certificate
                m_results.Add(results);
            }

        }

        /// <summary>
        /// Returns the list of selected and non selected results. This is used for 
        /// determining the par rate in pml 2.0 single program mode.
        /// </summary>
        public IEnumerable<CApplicantPriceXml> FullResults
        {
            get { return m_fullResults; }
        }

        // TODO: Need to retire this property.
        public ArrayList Results 
        {
            get { return m_results; }
        }

        public XmlDocument CertificateXmlDocument
        {
            get
            {
                return m_results[0] as XmlDocument;
            }
        }
        public IEnumerable<CApplicantPriceXml> GetApplicantPriceList()
        {
            List<CApplicantPriceXml> list = new List<CApplicantPriceXml>();

            foreach (object o in m_results)
            {
                CApplicantPriceXml product = o as CApplicantPriceXml;
                if (product != null)
                {
                    list.Add(product);
                }
            }
            return list;
        }

        private IEnumerable<CApplicantPriceXml> GetLoanProgramListByStatus(E_EvalStatus status)
        {
            List<CApplicantPriceXml> list = new List<CApplicantPriceXml>();

            foreach (object o in m_results)
            {
                CApplicantPriceXml product = o as CApplicantPriceXml;
                if (product != null && product.Status == status)
                {
                    list.Add(product);
                }

            }
            return list;

        }
        public IEnumerable<CApplicantPriceXml> GetEligibleLoanProgramList()
        {
            return GetLoanProgramListByStatus(E_EvalStatus.Eval_Eligible);
        }
        public IEnumerable<CApplicantPriceXml> GetIneligibleLoanProgramList()
        {
            return GetLoanProgramListByStatus(E_EvalStatus.Eval_Ineligible);
        }

        public IEnumerable<CApplicantPriceXml> GetInsufficientLoanProgramList()
        {
            return GetLoanProgramListByStatus(E_EvalStatus.Eval_InsufficientInfo);
        }

        public IEnumerable<CApplicantPriceXml> GetHiddenLoanProgramList()
        {
            return GetLoanProgramListByStatus(E_EvalStatus.Eval_HideFromResult);
        }
        public IEnumerable<RateMergeGroup> GetRateMergeResult(BrokerDB brokerDB)
        {
            RateMergeBucket rateMergeBucket = new RateMergeBucket(brokerDB.IsShowRateOptionsWorseThanLowerRate);

            foreach (var product in GetEligibleLoanProgramList())
            {
                rateMergeBucket.Add(product);
            }

            List<RateMergeGroup> list = new List<RateMergeGroup>();
            foreach (RateMergeGroup mergeGroup in rateMergeBucket.RateMergeGroups)
            {
                if (mergeGroup.RateOptions.Count == 0)
                {
                    continue;
                }
                list.Add(mergeGroup);
            }
            return list;
        }


        public static XmlDocument DedupAdjustmentDescription(XmlDocument doc)
        {
            #region // 2/28/2012 dd - OPM 78785 Merge visible adjustments with the same description on certificate.

            XmlNodeList adjustmentList = doc.SelectNodes("//pricing/adjustments/adjustment");
            if (adjustmentList != null)
            {
                Dictionary<string, List<string>> _codeOverlap = null; //Lazy-load this guy--only needed if a dupe exists in per rate.
                Dictionary<string, XmlNode> dictionary = new Dictionary<string, XmlNode>(StringComparer.OrdinalIgnoreCase);
                int count = adjustmentList.Count;
                for (int i = 0; i < count; i++)
                {
                    var node = adjustmentList.Item(i);
                    var descriptionNode = node.SelectSingleNode("description");
                    if (descriptionNode != null)
                    {
                        var description = descriptionNode.InnerText;
                        if (dictionary.ContainsKey(description) == false)
                        {
                            dictionary.Add(description, node);
                        }
                        else
                        {
                            bool needPerRateCleanup = false;
                            if (UsePerRate)
                            {
                                // OPM 93648 mf. 10/03/12
                                // In per-rate, we have to make sure this dupe applies to exactly the
                                // same rate options, or it should not be merged.
                                string originalCode = dictionary[description].Attributes["perOptionAdjStr"] ==  null ? ""
                                    : dictionary[description].Attributes["perOptionAdjStr"].Value;
                                
                                string thisCode = node.Attributes["perOptionAdjStr"] == null ? ""
                                    : node.Attributes["perOptionAdjStr"].Value;

                                if (originalCode != string.Empty || thisCode != string.Empty)
                                {
                                    if (originalCode.Length == 0 || thisCode.Length == 0)
                                        continue; // Cannot merge a per-rate and non-per-rate.

                                    if (_codeOverlap == null)
                                        _codeOverlap = FindPerRateOverlap(doc.SelectNodes("//pricing/rate_options/rate_option"));

                                    if (!DoCodesApplyToSameRateSet(_codeOverlap, originalCode, thisCode))
                                    {
                                        continue; // Cannot merge--these adjustments have the same name, but do not apply to all the same adjustments.
                                    }
                                    needPerRateCleanup = true; // Meaning we just merged two per-rate adjusts.
                                }
                            }

                            // 2/28/2012 dd - Description is duplicate merge rate, margin, fee, qrate, teaser
                            var rate = node.SelectSingleNode("rate").InnerText;
                            var margin = node.SelectSingleNode("margin").InnerText;
                            var fee = node.SelectSingleNode("fee").InnerText;
                            var qrate = node.SelectSingleNode("qrate").InnerText;
                            var teaser = node.SelectSingleNode("teaser").InnerText;

                            var representativeNode = dictionary[description];

                            AddValue(representativeNode, "rate", rate);
                            AddValue(representativeNode, "margin", margin);
                            AddValue(representativeNode, "fee", fee);
                            AddValue(representativeNode, "qrate", qrate);
                            AddValue(representativeNode, "teaser", teaser);

                            node.ParentNode.RemoveChild(node);

                            if (UsePerRate && needPerRateCleanup)
                            {
                                // We just merged a per-rate adjustment into another,
                                // so we have to remove references to the old adjustment
                                string removedCode = node.Attributes["perOptionAdjStr"].Value;
                                foreach (XmlNode rateOption in doc.SelectNodes("//pricing/rate_options/rate_option"))
                                {
                                    XmlAttribute optionList = rateOption.Attributes["perOptionAdjStr"];

                                    // To avoid having a gap, we have to bump up the rest of the adjustments
                                    string result = ""; // Not enough concats to be worth string builder overhead.
                                    foreach (string code in optionList.InnerText.Split(','))
                                    {
                                        if (code != string.Empty && code != removedCode)
                                        {
                                            string replacement;
                                            if (ShouldReplace(code, removedCode, out replacement))
                                            {
                                                result += result.Length == 0 ? replacement : "," + replacement;
                                            }
                                            else
                                            {
                                                result += result.Length == 0 ? code : "," + code;
                                            }
                                        }
                                    }
                                    optionList.InnerText = result;
                                }

                                // Do the same bump in the adjustment list in the same way to remove the gaps.
                                foreach (XmlNode rateOption in doc.SelectNodes("//pricing/adjustments/adjustment"))
                                {
                                    string adjCode = rateOption.Attributes["perOptionAdjStr"].Value;
                                    string replacement;

                                    if ( ShouldReplace(adjCode, removedCode, out replacement))
                                    {
                                        rateOption.Attributes["perOptionAdjStr"].Value = replacement;
                                    }
                                }
                                
                                // Rebuild the map because we just merged and removed.
                                _codeOverlap = FindPerRateOverlap(doc.SelectNodes("//pricing/rate_options/rate_option"));
                            }
                        }
                    }
                }
                
                // Merging is complete, if any adjustment showed as 0, remove it.
                for (int i = 0; i < count; i++)
                {
                    var node = adjustmentList.Item(i);
                    if (IsZeroAdjustment(node.SelectSingleNode("rate"))
                        && IsZeroAdjustment(node.SelectSingleNode("margin"))
                        && IsZeroAdjustment(node.SelectSingleNode("fee"))
                        && IsZeroAdjustment(node.SelectSingleNode("qrate"))
                        && IsZeroAdjustment(node.SelectSingleNode("teaser")))
                    {
                        // This node has no adjustments.
                        node.ParentNode.RemoveChild(node);
                    }
                }

            }
            #endregion
            return doc;

        }

        private static bool IsZeroAdjustment(XmlNode adjustment)
        {
            if (adjustment == null) return true;

            string adjValue = adjustment.InnerText;

            if (string.IsNullOrEmpty(adjValue) || adjValue.Replace(".","").Replace("0","").Replace("%","").Length == 0 )
            {
                return true;
            }

            return false;
        }

        private static void AddValue(XmlNode node, string elementName, string value)
        {
            if (string.IsNullOrEmpty(value) || value == "0.000%")
            {
                return;
            }

            decimal v = 0;
            decimal.TryParse(value.Replace("%", ""), out v);

            if (v != 0)
            {
                decimal currentValue = 0;
                var el = node.SelectSingleNode(elementName);
                if (el != null)
                {
                    var str = el.InnerText;
                    decimal.TryParse(str.Replace("%", ""), out currentValue);
                    currentValue += v;
                    el.InnerText = currentValue.ToString("0.000") + "%";// 2/28/2012 dd - DO NOT ADD % to 0.000% because .NET will multiple by 100.
                }

            }

        }

        private static bool UsePerRate
        {
            get 
            {
                AbstractUserPrincipal user = PrincipalFactory.CurrentPrincipal;
                if (user == null)
                {
                    Tools.LogError("Unknown user principal in DedupAdjustmentDescription.");
                    return false;  // Default to old way.
                }
                else
                {
                   BrokerDB broker = BrokerDB.RetrieveById(user.BrokerId);
                   return broker.IsUsePerRatePricing;
                }

            }
        }

        private static bool ShouldReplace(string currentCode, string removedCode, out string replacement)
        {
            char currentChar = char.Parse(currentCode);
            char removedChar = char.Parse(removedCode);

            if (currentChar <= removedChar)
            {
                replacement = null;
                return false;
            }

            replacement = new string((char)( currentChar - 1),1);
            return true;
        }

        // Find the list of per-rate codes that apply to the identical list of rate options
        private static Dictionary<string,List<string>> FindPerRateOverlap(XmlNodeList adjustments)
        {
            // Key=optionCode, Value=list of optionCodes that apply to exactly the same set of rates.
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            HashSet<string> adjCodes = new HashSet<string>();
            List<string> rateAdjStrs = new List<string>();

            foreach (XmlNode node in adjustments)
            {
                string codeStr = node.Attributes["perOptionAdjStr"].Value;
                rateAdjStrs.Add(codeStr);
                foreach (string code in codeStr.Split(','))
                {
                    adjCodes.Add(code);
                }
            }

            HashSet<string> processedCodes = new HashSet<string>();
            foreach (string code in adjCodes)
            {
                if (processedCodes.Contains(code)) continue;

                HashSet<string> knownGroups = new HashSet<string>(adjCodes);
                foreach (string codeList in rateAdjStrs)
                {
                    foreach (string optionList in rateAdjStrs)
                    {
                        if (optionList.Contains(code) == true)
                        {
                            knownGroups.IntersectWith(optionList.Split(','));
                        }
                        else
                        {
                            knownGroups.RemoveWhere(s => optionList.Contains(s));
                        }
                    }
                }
                if (knownGroups.Count > 1)
                {
                    knownGroups.Remove(code);

                    List<string> grouping = new List<string>(knownGroups);
                    result.Add(code, grouping);

                    processedCodes.Add(code);
                    //grouping.ForEach(s => processedCodes.Add(s));
                }
            }
            
            return result;
        }

        private static bool DoCodesApplyToSameRateSet(Dictionary<string,List<string>> codeMap,  string firstCode, string secondCode )
        {
            if (codeMap.ContainsKey(firstCode))
            {
                return (codeMap[firstCode].Contains(secondCode));
            }
            else if (codeMap.ContainsKey(secondCode))
            {
                return (codeMap[secondCode].Contains(firstCode));
            }
            return false;
        }

    }

}
