/// Author: David Dao

using System;
using System.Threading;
using LendersOffice.Common;

namespace LendersOffice.DistributeUnderwriting
{
	public class YieldCpuForLoadingLPE : IYieldCpu
	{

        private const int WAIT_INTERVAL = 5000; // Sleep for 5 seconds
        private const int MAX_SECONDS_ALLOW = 240; // Maximum allow yield is 4 minutes

        private int m_minSecondsBetweenYields;
        private DateTime m_nextPossibleYieldTime = DateTime.MinValue;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minSecondsBetweenYields">If yield invokes sooner than value specify it will be ignore. Minimum value is 60 seconds</param>
		public YieldCpuForLoadingLPE(int minSecondsBetweenYields)
		{
            if (minSecondsBetweenYields < 60)
                minSecondsBetweenYields = 60;

            m_minSecondsBetweenYields = minSecondsBetweenYields;

		}

        public int Yield(int maxSeconds) 
        {
            try 
            {
                if (maxSeconds <= 0)
                    return 0; // 8/2/2006 dd - Invalid arguments, return immediately.

                if (maxSeconds > MAX_SECONDS_ALLOW)
                    maxSeconds = MAX_SECONDS_ALLOW;

                if (DateTime.Now < m_nextPossibleYieldTime)
                    return 0; // 8/2/2006 dd - Last yield was within minimum range.

                if (LendersOfficeApp.los.RatePrice.CalcOnlyServerInfo.Busy == false)
                    return 0;

                DateTime startTime = DateTime.Now;
                DateTime maxYieldTime = startTime.AddSeconds(maxSeconds);


                while (LendersOfficeApp.los.RatePrice.CalcOnlyServerInfo.Busy) 
                {
                    LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(WAIT_INTERVAL);

                    if (DateTime.Now > maxYieldTime)
                        break;
                }
                m_nextPossibleYieldTime = DateTime.Now.AddSeconds(m_minSecondsBetweenYields);
                int duration = (int) (DateTime.Now - startTime).TotalSeconds;

                return duration;
            } 
            catch 
            {
                return 0; // 8/2/2006 dd - Any exception return 0.
            }
        }
	}
}
