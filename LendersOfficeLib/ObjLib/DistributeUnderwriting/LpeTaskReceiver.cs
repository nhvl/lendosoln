/// Author: David Dao
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.HttpModule;
using LendersOffice.Security;
using LendersOfficeApp.los.RatePrice;
using Toolbox;
using LqbGrammar.Drivers.SecurityEventLogging;

namespace LendersOffice.DistributeUnderwriting
{
    public class LpeTaskReceiver
	{
        #region Static Properties and Methods

        private static bool s_isRunning = false;
        private static object s_isRunningLock = new object();
        private static List<LpeTaskReceiver> s_threadList = null;
        public static bool IsRunning 
        {
            get 
            {
                lock (s_isRunningLock) 
                {
                    return s_isRunning; 
                }
            }
        }

        public static void Start()
        {
            // 10/23/2006 dd - Only start this thread on calc server.
            if (!DistributeUnderwritingSettings.RunUnderwritingTasksReceiverThread)
            {
                return;
            }

            LpeDataProvider.StartUp(); //Spin Up the Snapshot Loading Threads. 

            lock (s_isRunningLock)
            {
                if (s_isRunning)
                {
                    return; // Thread already running.
                }

                s_isRunning = true;
            }

            s_threadList = new List<LpeTaskReceiver>();
            int threadCount = ConstSite.UnderwritingTaskThreadCount;

            for (int i = 0; i < threadCount; i++)
            {
                LpeTaskReceiver o = new LpeTaskReceiver();
                Thread th = new Thread(new ThreadStart(o.Execute));
                th.Name = $"{ConstAppDavid.ServerName}.{i}";
                th.Start();
                s_threadList.Add(o);
            }
        }

        public static void Stop() 
        {
            lock (s_isRunningLock) 
            {
                s_isRunning = false;
                s_threadList = null;
            }
        }

        #endregion

        /// <summary>
        /// Gets the next pricing task to execute. This will wait for the snapshot to finish loading if the 
        /// other thread is busy doing it. Returns null if there are no task.
        /// </summary>
        /// <returns>The next task to execute, or null if there are no more.</returns>
        public LpeTaskMessage GetNextTask()
        {
            IYieldCpu yieldCpu = new YieldCpuForLoadingLPE(8 * 60); // 8 minutes;
            yieldCpu.Yield(30);

            LpeTaskMessage task = null;

            E_LpeTaskAllowedT taskAllow = SnapshotControler.TasksAllowed;

            switch (taskAllow)
            {
                case E_LpeTaskAllowedT.None:
                    // NO-OP
                    break;
                case E_LpeTaskAllowedT.GetResultNonSSRejectedOnly:
                case E_LpeTaskAllowedT.All:
                    task = LpeProcessingController.GetTask();
                    break;
                default:
                    throw new UnhandledEnumException(taskAllow);
            }

            return task;
        }

        public void RunTask(LpeTaskMessage lpeTask)
        {
            PerformanceMonitorItem.CreateNewForThread();
            PerformanceStopwatch.ResetAll();
            AbstractUserPrincipal principal = null;

            DateTime startTime = Tools.GetDBCurrentDateTime();
            long startTicks = startTime.Ticks;
            Stopwatch executionStopwatch = new Stopwatch();
            string performanceStopwatchDetails = string.Empty;
            try
            {
                E_ApplicationT applicationType = lpeTask.Header.ApplicationT;
                principal = PrincipalFactory.Create(lpeTask.Header.BrokerId, lpeTask.Header.UserId,
                    applicationType == E_ApplicationT.LendersOffice ? "B" : "P", true /* allowDuplicateLogin */, true /* isStoreToCookie */);

                Thread.CurrentPrincipal = principal;

                XmlDocument result = null;

                using (var workerTiming = new WorkerExecutionTiming("CalcServerPerf"))
                {
                    string customerCode = (principal != null) ? principal.BrokerDB.CustomerCode : string.Empty;

                    // description = pricingType;customerCode;username;numProduct
                    string workerItemDesc = string.Join(";",
                                                        lpeTask.Header.LpeTaskT.ToString(),
                                                        customerCode,
                                                        lpeTask.Header.UserName,
                                                        lpeTask.RequestData.ProductIdList.Count.ToString());

                    using (var itemTiming = workerTiming.RecordItem(workerItemDesc, lpeTask.Header.CreatedDate))
                    {
                        try
                        {
                            executionStopwatch.Start();
                            result = LpeTaskProcessor.Execute(lpeTask);
                            executionStopwatch.Stop();
                        }
                        catch (Exception exc)
                        {
                            itemTiming.RecordError(exc);
                            throw;
                        }
                    }
                }

                long endTicks = Tools.GetDBCurrentDateTime().Ticks;

                long durationInMs = (endTicks - startTicks) / 10000L;

                if (null != result)
                {
                    performanceStopwatchDetails = PerformanceStopwatch.ReportOutput;
                    // 8/28/2006 dd - Records statistics
                    XmlElement el = result.CreateElement("TimingDebug");
                    el.SetAttribute("start", startTicks.ToString());
                    el.SetAttribute("end", endTicks.ToString());
                    el.SetAttribute("calc_server_id", Thread.CurrentThread.Name);
                    el.SetAttribute("investor", lpeTask.RequestData.InvestorName);
                    el.SetAttribute("product_code", lpeTask.Header.ProductCode);
                    el.SetAttribute("current_part", lpeTask.Header.CurrentPartInBatch.ToString());
                    el.SetAttribute("total_part", lpeTask.Header.NumberOfRequestsInBatch.ToString());
                    el.SetAttribute("thread", Thread.CurrentThread.Name);
                    el.SetAttribute("num_of_products", lpeTask.RequestData.ProductIdList.Count.ToString());
                    el.SetAttribute("start_request", lpeTask.Header.CreatedDate.Ticks.ToString());
                    el.SetAttribute("server_list", lpeTask.Header.ProcessedServerList);
                    el.SetAttribute("time_in_queue", lpeTask.Header.TimeInQueue.Ticks.ToString());
                    el.SetAttribute("time_first_touch", lpeTask.Header.TimeFirstTouchByController.Ticks.ToString());
                    el.SetAttribute("time_pick_by_calc", lpeTask.Header.TimePickByCalcServer.Ticks.ToString());
                    el.SetAttribute("lperequestintid", lpeTask.Header.LpeRequestIntId.ToString());
                    el.SetAttribute("detail", performanceStopwatchDetails);
                    result.ChildNodes[0].AppendChild(el);

                }

                try
                {

                    LpeDistributeResults.Send(lpeTask, result, durationInMs);
                    LpeRequestUtils.RemoveRequestMessage(lpeTask);
                }
                catch (Exception exc)
                {
                    Tools.LogErrorWithCriticalTracking("Unable to insert result to DB.", exc);
                }
            }
            catch (CUnderwritingUnavailableException exc)
            {
                Tools.LogError(string.Format("LPE:{0} sLId={1}. Unable to process this message. Sleep this thread and try again later.", lpeTask.Header.LpeTaskT, lpeTask.RequestData.LoanId), exc);

                LpeRequestUtils.ResetMessageToNewStatus(lpeTask);

                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(10000); // Sleep for 10 seconds

            }
            catch (CInvalidRateVersionException exc)
            {
                // 6/5/2007 dd - We no longer try to see if other calc server has old version. We return exception immediately.
                // However since we handle up at the UI level, we should never encounter this situation.
                HandleException(lpeTask, exc, false);

            }
            catch (CBaseException exc)
            {
                HandleException(lpeTask, exc, true);
            }
            catch (Exception exc)
            {
                CBaseException baseException = new GenericUserErrorMessageException(exc);
                HandleException(lpeTask, baseException, true);
            }
            finally
            {
                DateTime endTime = Tools.GetDBCurrentDateTime();
                LogTiming(principal, lpeTask, startTime, endTime, (int)executionStopwatch.ElapsedMilliseconds, performanceStopwatchDetails);
                PerformanceMonitorItem.LogCurrent("Pricing");
            }
        }

        private void Execute() 
        {
            while (IsRunning) 
            {
                try
                {
                    LpeTaskMessage lpeTask = this.GetNextTask();

                    if (lpeTask == null)
                    {
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(1000);
                    }
                    else
                    {
                        RunTask(lpeTask);
                    }
                }
                catch (Exception exc)
                {
                    Tools.LogError($"{Thread.CurrentThread.Name}: Exception :: {exc.ToString()}");
                }
            }
        }

        private void HandleException(LpeTaskMessage task, CBaseException exc, bool isLogPB) 
        {
            if (isLogPB)
                Tools.LogErrorWithCriticalTracking(exc);
                
            LpeRequestUtils.RemoveRequestMessage(task);
            LpeDistributeResults.Send(task, exc);
        }

        private void LogTiming(AbstractUserPrincipal principal, LpeTaskMessage task, DateTime startDate, DateTime endDate, int executionTime, string performanceStopwatchDetails)
        {
            if (task == null)
            {
                return;
            }

            var timeWait = startDate - task.Header.CreatedDate;
            var totalTime = endDate - startDate;

            StringBuilder detailLog = new StringBuilder();
            detailLog.AppendLine($"{startDate} - {endDate} - Total Execution Time - {totalTime.TotalMilliseconds:0,0}ms. # of products: {task.RequestData.ProductIdList.Count}");
            detailLog.AppendLine();
            detailLog.AppendLine(performanceStopwatchDetails);
            detailLog.AppendLine();

            var item = PerformanceMonitorItem.Current;

            if (item != null)
            {
                XDocument xdoc = item.GenerateXDocument("pricing");
                string pricingLog = xdoc.ToString();

                // 2/13/2017 - dd - Put the restriction on the size of the log.
                if (pricingLog.Length > 50000 && (detailLog.Length + pricingLog.Length) > 1000000)
                {
                    detailLog.AppendLine("<perf>" + pricingLog.Substring(0, 50000) + " (truncate)</perf>");
                }
                else
                {
                    detailLog.AppendLine(pricingLog);
                }
            }

            var eventLog = PricingTimingUtils.CreateEventLog();
            eventLog.TimingType = "server";

            if (principal != null)
            {
                eventLog.User = principal.LoginNm;
                eventLog.CustomerCode = principal.BrokerDB.CustomerCode;
            }


            eventLog.LoanId = task.RequestData.LoanId;
            eventLog.RequestBatchId = task.Header.LpeRequestBatchId;
            eventLog.RequestId = task.Header.LpeRequestIntId.ToString();
            eventLog.PricingType = task.Header.LpeTaskT.ToString();
            if(task.Options.IsAutoProcess && eventLog.PricingType == "RunEngine")
            {
                eventLog.PricingType = "RunEngineAuto";
            }

            eventLog.Duration = Convert.ToInt32(totalTime.TotalMilliseconds);
            eventLog.NumberOfProducts = task.RequestData.ProductIdList.Count;

            eventLog.ServerWaitInQueue = Convert.ToInt32(timeWait.TotalMilliseconds);
            eventLog.ServerPricingExecute = executionTime;

            eventLog.Details = detailLog.ToString();

            PricingTimingUtils.Record(eventLog);
        }
	}

}