using LendersOffice.Constants;
namespace LendersOffice.DistributeUnderwriting
{
    public static class DistributeUnderwritingSettings
    {
        private volatile static E_ServerT serverType; 

        static DistributeUnderwritingSettings()
        {
            DistributeUnderwritingSettings.serverType = ConstSite.UnderwritingServerType;
        }

        public static void OverrideCalcMode(E_ServerT serverT)
        {
            DistributeUnderwritingSettings.serverType = serverT;
        }

        public static E_ServerT ServerT 
        {
            get { return DistributeUnderwritingSettings.serverType; }
        }

        public static bool RunUnderwritingTasksReceiverThread 
        {
            get { return DistributeUnderwritingSettings.serverType == E_ServerT.CalcOnly ; }
        }

	}
}
