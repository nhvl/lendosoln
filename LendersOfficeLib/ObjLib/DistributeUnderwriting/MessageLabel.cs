using System;
using DataAccess;
using LendersOffice.Common;
namespace LendersOffice.DistributeUnderwriting
{
    public class MessageLabel 
    {
        private Guid m_requestID;
        private int m_currentPart;
        private int m_numberOfRequests;
        private DateTime m_expirationDate;

        public MessageLabel(Guid requestID, int currentPart, int numberOfRequests, DateTime expirationDate) 
        {
            m_requestID = requestID;
            m_currentPart = currentPart;
            m_numberOfRequests = numberOfRequests;
            m_expirationDate = expirationDate;
        }
        public int CurrentPart 
        {
            get { return m_currentPart; }
        }

        public override string ToString() 
        {
            return string.Format("{0}|{1}|{2}|{3}", m_requestID, m_currentPart, m_numberOfRequests, m_expirationDate);
        }

        public static MessageLabel Parse(string str) 
        {
            if (null == str || str == "")
                return null;
            // 3/17/2005 dd - Label has following format.  {requestID}|{currentPart}|{NumberOfRequests}
            // 3/30/2005 dd - Add expiration time to label. {requestID}|{currentPart}|{NumberOfRequests}|{Expiration}
            string[] parts = str.Split('|');

            if (parts.Length != 4) 
            {
                throw new CBaseException(ErrorMessages.Generic, "MessageLabel.Parse: args is invalid format. str=" + str);
            }
            Guid requestID = Guid.Empty;
            try 
            {
                requestID = new Guid(parts[0]);
            } 
            catch {}
            int currentPart = 0;
            try 
            {
                currentPart = int.Parse(parts[1]);
            } 
            catch {}
            int numberOfRequests = 0;
            try 
            {
                numberOfRequests = int.Parse(parts[2]);
            } 
            catch {}
            DateTime expirationDate = DateTime.MaxValue;
            try 
            {
                expirationDate = DateTime.Parse(parts[3]); // Add on 3/30/2005
            } 
            catch {}

            return new MessageLabel(requestID, currentPart, numberOfRequests, expirationDate);
        }
    }

}
