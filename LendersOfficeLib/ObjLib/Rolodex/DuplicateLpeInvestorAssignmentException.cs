﻿// <copyright file="DuplicateLpeInvestorAssignmentException.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: David Dao
//    Date:   6/12/2014 10:36:53 AM 
// </summary>
namespace LendersOffice.ObjLib.Rolodex
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataAccess;

    /// <summary>
    /// Exception class for when there is more than one LPE investor is assign to Investor rolodex.
    /// </summary>
    public class DuplicateLpeInvestorAssignmentException : CBaseException
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DuplicateLpeInvestorAssignmentException" /> class.
        /// </summary>
        public DuplicateLpeInvestorAssignmentException()
            : base(
            "Error: This investor is already assigned to another contact entry. Only one contact entry per investor.",
                "Error: This investor is already assigned to another contact entry. Only one contact entry per investor.")
        {
        }
    }
}