﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.ObjLib.Rolodex
{
    public enum E_InvestorStatus
    {
        Inactive = 0,
        Active = 1
    }
}
