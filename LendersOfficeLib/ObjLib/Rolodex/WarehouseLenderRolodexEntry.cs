﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using LqbGrammar.DataTypes;

namespace LendersOffice.ObjLib.Rolodex
{
    public sealed class WarehouseLenderRolodexEntry
    {
        private int? m_Id;
        private Guid m_BrokerId;
        private string m_WarehouseLenderName;
        private E_WarehouseLenderStatus m_Status;
        private string m_FannieMaePayeeId;
        private string m_FreddieMacPayeeId;
        private string m_MersOrganizationId;
        private string m_PayToBankName;
        private string m_PayToCity;
        private string m_PayToState;
        private string m_PayToABANum;
        private string m_PayToAccountName;
        private string m_PayToAccountNum;
        private string m_FurtherCreditToAccountName;
        private string m_FurtherCreditToAccountNum;
        private string m_MainCompanyName;
        private string m_MainContactName;
        private string m_MainAttention;
        private string m_MainEmail;
        private string m_MainAddress;
        private string m_MainCity;
        private string m_MainState;
        private string m_MainZip;
        private string m_MainPhone;
        private string m_MainFax;
        private string m_CollateralPackageToAttention;
        private string m_CollateralPackageToContactName;
        private bool m_CollateralPackageToUseMainAddress;
        private string m_CollateralPackageToEmail;
        private string m_CollateralPackageToAddress;
        private string m_CollateralPackageToCity;
        private string m_CollateralPackageToState;
        private string m_CollateralPackageToZip;
        private string m_CollateralPackageToPhone;
        private string m_CollateralPackageToFax;
        private string m_ClosingPackageToAttention;
        private string m_ClosingPackageToContactName;
        private bool m_ClosingPackageToUseMainAddress;
        private string m_ClosingPackageToEmail;
        private string m_ClosingPackageToAddress;
        private string m_ClosingPackageToCity;
        private string m_ClosingPackageToState;
        private string m_ClosingPackageToZip;
        private string m_ClosingPackageToPhone;
        private string m_ClosingPackageToFax;
        private List<WarehouseLenderAdvanceClassification> m_advanceClassifications;
        public int? Id
        {
            get { return m_Id; }
            private set { m_Id = value; }
        }
        public Guid BrokerId
        {
            get { return m_BrokerId; } 
            private set { m_BrokerId = value; }
        }
        public E_WarehouseLenderStatus Status
        { 
            get { return m_Status; }
            set { m_Status = value; }
        }
        public string WarehouseLenderName
        { 
            get { return m_WarehouseLenderName; }
            set { m_WarehouseLenderName = value; }
        }
        public string FannieMaePayeeId
        { 
            get { return m_FannieMaePayeeId; } 
            set { m_FannieMaePayeeId = value; }
        }

        public string FreddieMacPayeeId
        {
            get
            {
                return this.m_FreddieMacPayeeId;
            }

            set
            {
                this.m_FreddieMacPayeeId = value;
            }
        }

        public string WarehouseLenderFannieMaeId { get; set; }

        public string WarehouseLenderFreddieMacId { get; set; }

        public string MersOrganizationId
        { 
            get { return m_MersOrganizationId; } 
            set { m_MersOrganizationId = value; } 
        }
        public string PayToBankName
        { 
            get { return m_PayToBankName; } 
            set { m_PayToBankName = value; }
        }
        public string PayToCity 
        { get { return m_PayToCity; } 
            set { m_PayToCity = value; } 
        }
        public string PayToState 
        { 
            get { return m_PayToState; } 
            set { m_PayToState = value; }
        }
        public Sensitive<string> PayToABANum 
        {
            get { return this.m_PayToABANum; } 
            set { this.m_PayToABANum = value.Value; } 
        }
        public string PayToAccountName
        { 
            get { return m_PayToAccountName; }
            set { m_PayToAccountName = value; } 
        }
        public Sensitive<string> PayToAccountNum
        { 
            get { return this.m_PayToAccountNum; }
            set { this.m_PayToAccountNum = value.Value; } 
        }
        public string FurtherCreditToAccountName 
        { 
            get { return m_FurtherCreditToAccountName; }
            set { m_FurtherCreditToAccountName = value; } 
        }
        public Sensitive<string> FurtherCreditToAccountNum
        {
            get { return this.m_FurtherCreditToAccountNum; } 
            set { this.m_FurtherCreditToAccountNum = value.Value; }
        }
        public string MainCompanyName
        {
            get { return m_MainCompanyName; } 
            set { m_MainCompanyName = value; } 
        }
        public string MainContactName
        {
            get { return m_MainContactName; } 
            set { m_MainContactName = value; }
        }
        public string MainAttention
        {
            get { return m_MainAttention; }
            set { m_MainAttention = value; } 
        }
        public string MainEmail
        {
            get { return m_MainEmail; }
            set { m_MainEmail = value; } 
        }
        public string MainAddress 
        {
            get { return m_MainAddress; }
            set { m_MainAddress = value; } 
        }
        public string MainCity 
        { 
            get { return m_MainCity; }
            set { m_MainCity = value; } 
        }
        public string MainState 
        {
            get { return m_MainState; } 
            set { m_MainState = value; } 
        }
        public string MainZip
        {
            get { return m_MainZip; }
            set { m_MainZip = value; } 
        }
        public string MainPhone 
        { 
            get { return m_MainPhone; } 
            set { m_MainPhone = value; } 
        }
        public string MainFax 
        { 
            get { return m_MainFax; }
            set { m_MainFax = value; }
        }
        public string CollateralPackageToAttention
        { 
            get { return m_CollateralPackageToAttention; } 
            set { m_CollateralPackageToAttention = value; } 
        }
        public string CollateralPackageToContactName
        {
            get { return m_CollateralPackageToContactName; } 
            set { m_CollateralPackageToContactName = value; }
        }
        public bool CollateralPackageToUseMainAddress
        { 
            get { return m_CollateralPackageToUseMainAddress; } 
            set { m_CollateralPackageToUseMainAddress = value; } 
        }
        public string CollateralPackageToEmail
        { 
            get { return m_CollateralPackageToEmail; } 
            set { m_CollateralPackageToEmail = value; }
        }
        public string CollateralPackageToAddress 
        {
            get { return m_CollateralPackageToUseMainAddress ? m_MainAddress : m_CollateralPackageToAddress; }
            set { m_CollateralPackageToAddress = value; } 
        }
        public string CollateralPackageToCity
        {
            get { return m_CollateralPackageToUseMainAddress ? m_MainCity : m_CollateralPackageToCity; } 
            set { m_CollateralPackageToCity = value; } 
        }
        public string CollateralPackageToState 
        {
            get { return m_CollateralPackageToUseMainAddress ? m_MainState : m_CollateralPackageToState; } 
            set { m_CollateralPackageToState = value; }
        }
        public string CollateralPackageToZip 
        { 
            get { return m_CollateralPackageToUseMainAddress ? m_MainZip : m_CollateralPackageToZip; } 
            set { m_CollateralPackageToZip = value; }
        }
        public string CollateralPackageToPhone 
        {
            get { return m_CollateralPackageToUseMainAddress ? m_MainPhone : m_CollateralPackageToPhone; } 
            set { m_CollateralPackageToPhone = value; } 
        }
        public string CollateralPackageToFax
        {
            get { return m_CollateralPackageToUseMainAddress ? m_MainFax : m_CollateralPackageToFax; } 
            set { m_CollateralPackageToFax = value; } 
        }
        public string ClosingPackageToAttention
        {
            get { return m_ClosingPackageToAttention; }
            set { m_ClosingPackageToAttention = value; }
        }
        public string ClosingPackageToContactName
        {
            get { return m_ClosingPackageToContactName; }
            set { m_ClosingPackageToContactName = value; }
        }
        public bool ClosingPackageToUseMainAddress
        {
            get { return m_ClosingPackageToUseMainAddress; }
            set { m_ClosingPackageToUseMainAddress = value; }
        }
        public string ClosingPackageToEmail
        {
            get { return m_ClosingPackageToEmail; }
            set { m_ClosingPackageToEmail = value; }
        }
        public string ClosingPackageToAddress
        {
            get { return m_ClosingPackageToUseMainAddress ? m_MainAddress : m_ClosingPackageToAddress; }
            set { m_ClosingPackageToAddress = value; }
        }
        public string ClosingPackageToCity
        {
            get { return m_ClosingPackageToUseMainAddress ? m_MainCity : m_ClosingPackageToCity; }
            set { m_ClosingPackageToCity = value; }
        }
        public string ClosingPackageToState
        {
            get { return m_ClosingPackageToUseMainAddress ? m_MainState : m_ClosingPackageToState; }
            set { m_ClosingPackageToState = value; }
        }
        public string ClosingPackageToZip
        {
            get { return m_ClosingPackageToUseMainAddress ? m_MainZip : m_ClosingPackageToZip; }
            set { m_ClosingPackageToZip = value; }
        }
        public string ClosingPackageToPhone
        {
            get { return m_ClosingPackageToUseMainAddress ? m_MainPhone : m_ClosingPackageToPhone; }
            set { m_ClosingPackageToPhone = value; }
        }
        public string ClosingPackageToFax
        {
            get { return m_ClosingPackageToUseMainAddress ? m_MainFax : m_ClosingPackageToFax; }
            set { m_ClosingPackageToFax = value; }
        }
        public List<WarehouseLenderAdvanceClassification> AdvanceClassifications
        {
            get {
                if (m_advanceClassifications == null)
                    m_advanceClassifications = new List<WarehouseLenderAdvanceClassification>();
                return m_advanceClassifications;
            }
            set { m_advanceClassifications = value; }
        }
        private string AdvanceClassificationsXmlContent
        {
            get
            {
                var ser = new XmlSerializer(typeof(List<WarehouseLenderAdvanceClassification>));
                StringBuilder sb = new StringBuilder();
                using (XmlWriter writer = XmlWriter.Create(sb))
                {
                    ser.Serialize(writer, AdvanceClassifications);
                }
                return sb.ToString();
            }
            set 
            {
                if (string.IsNullOrEmpty(value))
                    return;
                var ser = new XmlSerializer(typeof(List<WarehouseLenderAdvanceClassification>));
                using (StringReader reader = new StringReader(value))
                {
                    AdvanceClassifications = (List<WarehouseLenderAdvanceClassification>)ser.Deserialize(reader);
                }                
            }
        }
        public bool IsNew
        {
            get;
            set;
        }
        internal WarehouseLenderRolodexEntry(Guid brokerId)
        {
            m_BrokerId = brokerId;
            IsNew = true;
        }
        internal WarehouseLenderRolodexEntry(DbDataReader reader)
        {
            Id = (int)reader["WarehouseLenderRolodexId"]; 
            BrokerId = new Guid(reader["BrokerId"].ToString());
            WarehouseLenderName = reader["WarehouseLenderName"].ToString();
            Status = (E_WarehouseLenderStatus)reader["Status"];
            FannieMaePayeeId = reader["FannieMaePayeeId"].ToString();
            FreddieMacPayeeId = reader.AsNullableString("FreddieMacPayeeId") ?? string.Empty;
            this.WarehouseLenderFannieMaeId = reader.AsNullableString("WarehouseLenderFannieMaeId");
            this.WarehouseLenderFreddieMacId = reader.AsNullableString("WarehouseLenderFreddieMacId");
            MersOrganizationId = reader["MersOrganizationId"].ToString();
            PayToBankName = reader["PayToBankName"].ToString();
            PayToCity = reader["PayToCity"].ToString();
            PayToState = reader["PayToState"].ToString();
            PayToABANum = reader["PayToABANum"].ToString();
            PayToAccountName = reader["PayToAccountName"].ToString();
            PayToAccountNum = reader["PayToAccountNum"].ToString();
            FurtherCreditToAccountName = reader["FurtherCreditToAccountName"].ToString();
            FurtherCreditToAccountNum = reader["FurtherCreditToAccountNum"].ToString();
            MainCompanyName = reader["MainCompanyName"].ToString();
            MainContactName = reader["MainContactName"].ToString();
            MainAttention = reader["MainAttention"].ToString();
            MainEmail = reader["MainEmail"].ToString();
            MainAddress = reader["MainAddress"].ToString();
            MainCity = reader["MainCity"].ToString();
            MainState = reader["MainState"].ToString();
            MainZip = reader["MainZip"].ToString();
            MainPhone = reader["MainPhone"].ToString();
            MainFax = reader["MainFax"].ToString();
            CollateralPackageToAttention = reader["CollateralPackageToAttention"].ToString();
            CollateralPackageToContactName = reader["CollateralPackageToContactName"].ToString();
            CollateralPackageToUseMainAddress = (bool)reader["CollateralPackageToUseMainAddress"];
            CollateralPackageToEmail = reader["CollateralPackageToEmail"].ToString();
            CollateralPackageToAddress = reader["CollateralPackageToAddress"].ToString();
            CollateralPackageToCity = reader["CollateralPackageToCity"].ToString();
            CollateralPackageToState = reader["CollateralPackageToState"].ToString();
            CollateralPackageToZip = reader["CollateralPackageToZip"].ToString();
            CollateralPackageToPhone = reader["CollateralPackageToPhone"].ToString();
            CollateralPackageToFax = reader["CollateralPackageToFax"].ToString();
            ClosingPackageToAttention = reader["ClosingPackageToAttention"].ToString();
            ClosingPackageToContactName = reader["ClosingPackageToContactName"].ToString();
            ClosingPackageToUseMainAddress = (bool)reader["ClosingPackageToUseMainAddress"];
            ClosingPackageToEmail = reader["ClosingPackageToEmail"].ToString();
            ClosingPackageToAddress = reader["ClosingPackageToAddress"].ToString();
            ClosingPackageToCity = reader["ClosingPackageToCity"].ToString();
            ClosingPackageToState = reader["ClosingPackageToState"].ToString();
            ClosingPackageToZip = reader["ClosingPackageToZip"].ToString();
            ClosingPackageToPhone = reader["ClosingPackageToPhone"].ToString();
            ClosingPackageToFax = reader["ClosingPackageToFax"].ToString();
            AdvanceClassificationsXmlContent = reader["AdvanceClassificationsXmlContent"].ToString();
        }

        public void Save()
        {
            string storedProcedureName = "WAREHOUSE_LENDER_ROLODEX_ADD";
            List<SqlParameter> parameters = new List<SqlParameter> {
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@WarehouseLenderName", WarehouseLenderName),
                new SqlParameter("@Status", Status),
                new SqlParameter("@FannieMaePayeeId", FannieMaePayeeId),
                new SqlParameter("@FreddieMacPayeeId", FreddieMacPayeeId),
                new SqlParameter("@WarehouseLenderFannieMaeId", this.WarehouseLenderFannieMaeId),
                new SqlParameter("@WarehouseLenderFreddieMacId", this.WarehouseLenderFreddieMacId),
                new SqlParameter("@MersOrganizationId", MersOrganizationId),
                new SqlParameter("@PayToBankName", PayToBankName),
                new SqlParameter("@PayToCity", PayToCity),
                new SqlParameter("@PayToState", PayToState),
                new SqlParameter("@PayToABANum", PayToABANum.Value),
                new SqlParameter("@PayToAccountName", PayToAccountName),
                new SqlParameter("@PayToAccountNum", PayToAccountNum.Value),
                new SqlParameter("@FurtherCreditToAccountName", FurtherCreditToAccountName),
                new SqlParameter("@FurtherCreditToAccountNum", FurtherCreditToAccountNum.Value),
                new SqlParameter("@MainCompanyName", MainCompanyName),
                new SqlParameter("@MainContactName", MainContactName),
                new SqlParameter("@MainAttention", MainAttention),
                new SqlParameter("@MainEmail", MainEmail),
                new SqlParameter("@MainAddress", MainAddress),
                new SqlParameter("@MainCity", MainCity),
                new SqlParameter("@MainState", MainState),
                new SqlParameter("@MainZip", MainZip),
                new SqlParameter("@MainPhone", MainPhone),
                new SqlParameter("@MainFax", MainFax),
                new SqlParameter("@CollateralPackageToAttention", CollateralPackageToAttention),
                new SqlParameter("@CollateralPackageToContactName", CollateralPackageToContactName),
                new SqlParameter("@CollateralPackageToUseMainAddress", CollateralPackageToUseMainAddress),
                new SqlParameter("@CollateralPackageToEmail", CollateralPackageToEmail),
                new SqlParameter("@CollateralPackageToAddress", CollateralPackageToAddress),
                new SqlParameter("@CollateralPackageToCity", CollateralPackageToCity),
                new SqlParameter("@CollateralPackageToState", CollateralPackageToState),
                new SqlParameter("@CollateralPackageToZip", CollateralPackageToZip),
                new SqlParameter("@CollateralPackageToPhone", CollateralPackageToPhone),
                new SqlParameter("@CollateralPackageToFax", CollateralPackageToFax),
                new SqlParameter("@ClosingPackageToAttention", ClosingPackageToAttention),
                new SqlParameter("@ClosingPackageToContactName", ClosingPackageToContactName),
                new SqlParameter("@ClosingPackageToUseMainAddress", ClosingPackageToUseMainAddress),
                new SqlParameter("@ClosingPackageToEmail", ClosingPackageToEmail),
                new SqlParameter("@ClosingPackageToAddress", ClosingPackageToAddress),
                new SqlParameter("@ClosingPackageToCity", ClosingPackageToCity),
                new SqlParameter("@ClosingPackageToState", ClosingPackageToState),
                new SqlParameter("@ClosingPackageToZip", ClosingPackageToZip),
                new SqlParameter("@ClosingPackageToPhone", ClosingPackageToPhone),
                new SqlParameter("@ClosingPackageToFax", ClosingPackageToFax),
                new SqlParameter("@AdvanceClassificationsXmlContent", AdvanceClassificationsXmlContent)
            };

            if (m_Id.HasValue)
            {
                storedProcedureName = "WAREHOUSE_LENDER_ROLODEX_UPDATE";
                parameters.Add(new SqlParameter("@Id", m_Id.Value));
            }

            object result = StoredProcedureHelper.ExecuteScalar(this.BrokerId, storedProcedureName, parameters);

            if (!m_Id.HasValue)
            {
                m_Id = (int)result;
                IsNew = false;
            }
        }
        public static IEnumerable<WarehouseLenderRolodexEntry> GetAll(Guid brokerId, E_WarehouseLenderStatus? status)
        {
            return GetImpl(brokerId, null, status).OrderBy(p => p.WarehouseLenderName);
        }

        public static WarehouseLenderRolodexEntry Get(Guid brokerId, int investorId)
        {
            if (investorId < 0)
            {
                return new WarehouseLenderRolodexEntry(brokerId);
            }
            return GetImpl(brokerId, investorId, null).FirstOrDefault();
        }

        private static IEnumerable<WarehouseLenderRolodexEntry> GetImpl(Guid brokerId, int? investorId, E_WarehouseLenderStatus? status)
        {
            List<WarehouseLenderRolodexEntry> entries = new List<WarehouseLenderRolodexEntry>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "WAREHOUSE_LENDER_ROLODEX_GET", new List<SqlParameter>() 
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@Id", investorId),
                new SqlParameter("@Status", status) //this is only used when investor id is null. -av 1/2/2012
            }))
            {
                while (reader.Read())
                {
                    entries.Add(new WarehouseLenderRolodexEntry(reader));
                }
            }
            return entries;
        }
    }
    public class WarehouseLenderAdvanceClassification
    {
        private string m_description;
        private decimal m_maxFundingPercent;

        public string Description
        {
            get { return m_description; }
            set { m_description = value; }
        }
        public decimal MaxFundingPercent
        {
            get { return m_maxFundingPercent; }
            set { m_maxFundingPercent = value; }
        }
        public string MaxFundingPercent_rep
        {
            get { return m_maxFundingPercent.ToString("F3") + "%"; }
            set { m_maxFundingPercent = decimal.Parse(value.Replace("%","")); }
        }

    }
    public enum E_WarehouseLenderStatus
    {
        Inactive = 0,
        Active = 1
    }
}
