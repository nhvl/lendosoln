﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;
using LendersOffice.RatePrice;
using LendersOffice.Admin;

namespace LendersOffice.ObjLib.Rolodex
{
    public sealed class InvestorRolodexEntry
    {
        private int? m_Id;

        private string m_PaymentToAddress;
        private string m_PaymentToState;
        private string m_PaymentToCity;
        private string m_PaymentToZip;
        private string m_PaymentToPhone;
        private string m_PaymentToFax;

        private string m_LossPayeeAddress;
        private string m_LossPayeeState;
        private string m_LossPayeeCity;
        private string m_LossPayeeZip;
        private string m_LossPayeePhone;
        private string m_LossPayeeFax;

        public int? Id
        {
            get
            {
                return m_Id;
            }
            private set 
            { 
                m_Id = value ;  
            }
        }

        public Guid BrokerId { get; private set;}
        public string SellerId { get; set;}
        public string InvestorName { get; set;}

        public string MERSOrganizationId { get; set;}
        public E_InvestorStatus Status { get; set;}
        public string CompanyName { get; set;}
        public string MainAttention { get; set;}
        public string MainAddress { get; set;}
        public string MainState { get; set;}
        public string MainCity { get; set;}
        public string MainZip { get; set;}
        public string MainAddrMultiLines
        {
            get { return Tools.FormatAddress(MainAddress, MainCity, MainState, MainZip); }
        }

        public string MainPhone { get; set;}
        public string MainFax { get; set;}
        public string MainContactName { get; set;}
        public string MainEmail { get; set;}


        public string NoteShipToAttention { get; set;}

        public string NoteShipToAddress { get; set;}
        public string NoteShipToState { get; set;}
        public string NoteShipToCity { get; set;}
        public string NoteShipToZip { get; set;}
        public string NoteShipToAddrMultiLines
        {
            get { return Tools.FormatAddress(NoteShipToAddress, NoteShipToCity, NoteShipToState, NoteShipToZip); }
        }

        public string NoteShipToPhone { get; set;}
        public string NoteShipToFax { get; set;}
        public string NoteShipToContactName { get; set;}
        public string NoteShipToEmail { get; set; }

        public bool NoteShipToUseMainAdddress { get; set;}
        public string PaymentToAttention {get; set; }


        public string PaymentToAddress
        {
            get
            {
                if (this.PaymentToUseMainAdddress)
                {
                    return this.MainAddress;
                }
                return m_PaymentToAddress;
            }
            set
            {
                m_PaymentToAddress = value;
            }
        }

        public string PaymentToState
        {
            get
            {
                if (this.PaymentToUseMainAdddress)
                {
                    return this.MainState;
                }
                return m_PaymentToState;
            }
            set
            {
                m_PaymentToState = value;
            }
        }

        public string PaymentToCity
        {
            get
            {
                if (this.PaymentToUseMainAdddress)
                {
                    return this.MainCity;
                }
                return m_PaymentToCity;
            }
            set
            {
                m_PaymentToCity = value;
            }
        }

        public string PaymentToZip
        {
            get
            {
                if (this.PaymentToUseMainAdddress)
                {
                    return this.MainZip;
                }
                return m_PaymentToZip;
            }
            set
            {
                m_PaymentToZip = value;
            }
        }

        public string PaymentToAddrMultiLines
        {
            get
            {
                return Tools.FormatAddress(m_PaymentToAddress, m_PaymentToCity, m_PaymentToState, m_PaymentToZip);
            }
        }

        public string PaymentToPhone
        {
            get
            {
                if (this.PaymentToUseMainAdddress)
                {
                    return this.MainPhone;
                }
                return m_PaymentToPhone;
            }
            set
            {
                m_PaymentToPhone = value;
            }
        }

        public string PaymentToFax
        {
            get
            {
                if (this.PaymentToUseMainAdddress)
                {
                    return this.MainFax;
                }
                return m_PaymentToFax;
            }
            set
            {
                m_PaymentToFax = value;
            }
        }

        public string PaymentToContactName { get; set;}
        public string PaymentToEmail { get; set;}

        public bool PaymentToUseMainAdddress { get; set;}

        public string LossPayeeAttention { get; set; }

        public string LossPayeeAddress
        {
            get
            {
                if (this.LossPayeeUseMainAdddress)
                {
                    return this.MainAddress;
                }
                return m_LossPayeeAddress;
            }
            set
            {
                m_LossPayeeAddress = value;
            }
        }
        
        public string LossPayeeState
        {
            get
            {
                if (this.LossPayeeUseMainAdddress)
                {
                    return this.MainState;
                }
                return m_LossPayeeState;
            }
            set
            {
                m_LossPayeeState = value;
            }
        }

        public string LossPayeeCity
        {
            get
            {
                if (this.LossPayeeUseMainAdddress)
                {
                    return this.MainCity;
                }
                return m_LossPayeeCity;
            }
            set
            {
                m_LossPayeeCity = value;
            }
        }

        public string LossPayeeZip
        {
            get
            {
                if (this.LossPayeeUseMainAdddress)
                {
                    return this.MainZip;
                }
                return m_LossPayeeZip;
            }
            set
            {
                m_LossPayeeZip = value;
            }
        }

        public string LossPayeeAddrMultiLine
        {
            get
            {
                return Tools.FormatAddress(m_LossPayeeAddress, m_LossPayeeCity, m_LossPayeeState, m_LossPayeeZip);
            }
        }

        public string LossPayeePhone
        {
            get
            {
                if (this.LossPayeeUseMainAdddress)
                {
                    return this.MainPhone;
                }
                return m_LossPayeePhone;
            }
            set
            {
                m_LossPayeePhone = value;
            }
        }

        public string LossPayeeFax
        {
            get
            {
                
                if (this.LossPayeeUseMainAdddress)
                {
                    return this.MainFax;
                }

                return m_LossPayeeFax;
            }
            set
            {
                m_LossPayeeFax = value;
            }
        }
        
        public string LossPayeeContactName { get; set;}
        
        public string LossPayeeEmail { get; set;}
        public bool LossPayeeUseMainAdddress { get; set;}

        public E_sHmdaPurchaser2004T HmdaPurchaser2004T { get; set;}
        public HmdaPurchaser2015T HmdaPurchaser2015T { get; set;}
        public E_InvestorRolodexType InvestorRolodexType { get; set;}

        public bool IsNew { get; private set; }

        /// <summary>
        /// Gets Id of the Lpe Investor Id. See OPM 177694.
        /// </summary>
        /// <value>Id of the Lpe Investor Id.</value>
        public int LpeInvestorId { get; set; }

        internal InvestorRolodexEntry(Guid brokerId)
        {
            this.BrokerId = brokerId;
            IsNew = true;
            LpeInvestorId = -1;
        }

        private InvestorRolodexEntry(DbDataReader reader)
        {
            Id = (int)reader["InvestorRolodexId"]; 
            BrokerId = new Guid(reader["BrokerId"].ToString());
            SellerId = reader["SellerId"].ToString();
            InvestorName = reader["InvestorName"].ToString();
            MERSOrganizationId = reader["MERSOrganizationId"].ToString();
            Status = (E_InvestorStatus)reader["Status"];
            CompanyName = reader["CompanyName"].ToString();
            MainAttention = reader["MainAttention"].ToString();
            MainAddress = reader["MainAddress"].ToString();
            MainState = reader["MainState"].ToString();
            MainCity = reader["MainCity"].ToString();
            MainZip = reader["MainZip"].ToString();
            MainPhone = reader["MainPhone"].ToString();
            MainFax = reader["MainFax"].ToString();
            MainContactName = reader["MainContactName"].ToString();
            MainEmail = reader["MainEmail"].ToString();
            NoteShipToAttention = reader["NoteShipToAttention"].ToString();
            NoteShipToAddress = reader["NoteShipToAddress"].ToString();
            NoteShipToState = reader["NoteShipToState"].ToString();
            NoteShipToCity = reader["NoteShipToCity"].ToString();
            NoteShipToZip = reader["NoteShipToZip"].ToString();
            NoteShipToPhone = reader["NoteShipToPhone"].ToString();
            NoteShipToFax = reader["NoteShipToFax"].ToString();
            NoteShipToContactName = reader["NoteShipToContactName"].ToString();
            NoteShipToEmail = reader["NoteShipToEmail"].ToString();
            NoteShipToUseMainAdddress = (bool)reader["NoteShipToUseMainAdddress"];
            PaymentToAttention = reader["PaymentToAttention"].ToString();
            PaymentToAddress = reader["PaymentToAddress"].ToString();
            PaymentToState = reader["PaymentToState"].ToString();
            PaymentToCity = reader["PaymentToCity"].ToString();
            PaymentToZip = reader["PaymentToZip"].ToString();
            PaymentToPhone = reader["PaymentToPhone"].ToString();
            PaymentToFax = reader["PaymentToFax"].ToString();
            PaymentToContactName = reader["PaymentToContactName"].ToString();
            PaymentToEmail = reader["PaymentToEmail"].ToString();
            PaymentToUseMainAdddress = (bool)reader["PaymentToUseMainAdddress"];
            LossPayeeAttention = reader["LossPayeeAttention"].ToString();
            LossPayeeAddress = reader["LossPayeeAddress"].ToString();
            LossPayeeState = reader["LossPayeeState"].ToString();
            LossPayeeCity = reader["LossPayeeCity"].ToString();
            LossPayeeZip = reader["LossPayeeZip"].ToString();
            LossPayeePhone = reader["LossPayeePhone"].ToString();
            LossPayeeFax = reader["LossPayeeFax"].ToString();
            LossPayeeContactName = reader["LossPayeeContactName"].ToString();
            LossPayeeEmail = reader["LossPayeeEmail"].ToString();
            LossPayeeUseMainAdddress = (bool)reader["LossPayeeUseMainAdddress"];
            HmdaPurchaser2004T = (E_sHmdaPurchaser2004T)reader["HmdaPurchaser2004T"];
            HmdaPurchaser2015T = (HmdaPurchaser2015T)reader["HmdaPurchaser2015T"];
            InvestorRolodexType = (E_InvestorRolodexType)reader["InvestorRolodexType"];
            LpeInvestorId = (int)reader["LpeInvestorId"];
        }

        public void Save()
        {
            // 6/12/2014 dd - Per case 177694 - A PML Lender can only associate with one investor rolodex entry.
            if (LpeInvestorId > 0)
            {
                SqlParameter[] dupParameters = {
                                                   new SqlParameter("@BrokerId", this.BrokerId),
                                                   new SqlParameter("@ExcludeId", this.Id),
                                                   new SqlParameter("@LpeInvestorId", this.LpeInvestorId)
                                               };
                bool isDuplicate = false;
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "INVESTOR_ROLODEX_CheckDuplicateLpeInvestorId", dupParameters))
                {
                    if (reader.Read())
                    {
                        isDuplicate = true;
                    }
                }
                if (isDuplicate)
                {
                    throw new DuplicateLpeInvestorAssignmentException();
                }
            }
            string storedProcedureName = "INVESTOR_ROLODEX_ADD";
            List<SqlParameter> parameters = new List<SqlParameter> {
                new SqlParameter("@BrokerId", BrokerId),
                new SqlParameter("@SellerId", SellerId),
                new SqlParameter("@InvestorName", InvestorName),
                new SqlParameter("@MERSOrganizationId", MERSOrganizationId),
                new SqlParameter("@Status", Status),
                new SqlParameter("@CompanyName", CompanyName),
                new SqlParameter("@MainAttention", MainAttention),
                new SqlParameter("@MainAddress", MainAddress),
                new SqlParameter("@MainState", MainState),
                new SqlParameter("@MainCity", MainCity),
                new SqlParameter("@MainZip", MainZip),
                new SqlParameter("@MainPhone", MainPhone),
                new SqlParameter("@MainFax", MainFax),
                new SqlParameter("@MainContactName", MainContactName),
                new SqlParameter("@MainEmail", MainEmail),
                new SqlParameter("@NoteShipToAttention", NoteShipToAttention),
                new SqlParameter("@NoteShipToAddress", NoteShipToAddress),
                new SqlParameter("@NoteShipToState", NoteShipToState),
                new SqlParameter("@NoteShipToCity", NoteShipToCity),
                new SqlParameter("@NoteShipToZip", NoteShipToZip),
                new SqlParameter("@NoteShipToPhone", NoteShipToPhone),
                new SqlParameter("@NoteShipToFax", NoteShipToFax),
                new SqlParameter("@NoteShipToContactName", NoteShipToContactName),
                new SqlParameter("@NoteShipToEmail", NoteShipToEmail),
                new SqlParameter("@NoteShipToUseMainAdddress", NoteShipToUseMainAdddress),
                new SqlParameter("@PaymentToAttention", PaymentToAttention),
                new SqlParameter("@PaymentToAddress", PaymentToAddress),
                new SqlParameter("@PaymentToState", PaymentToState),
                new SqlParameter("@PaymentToCity", PaymentToCity),
                new SqlParameter("@PaymentToZip", PaymentToZip),
                new SqlParameter("@PaymentToPhone", PaymentToPhone),
                new SqlParameter("@PaymentToFax", PaymentToFax),
                new SqlParameter("@PaymentToContactName", PaymentToContactName),
                new SqlParameter("@PaymentToEmail", PaymentToEmail),
                new SqlParameter("@PaymentToUseMainAdddress", PaymentToUseMainAdddress),
                new SqlParameter("@LossPayeeAttention", LossPayeeAttention),
                new SqlParameter("@LossPayeeAddress", LossPayeeAddress),
                new SqlParameter("@LossPayeeState", LossPayeeState), 
                new SqlParameter("@LossPayeeCity", LossPayeeCity),
                new SqlParameter("@LossPayeeZip", LossPayeeZip),
                new SqlParameter("@LossPayeePhone", LossPayeePhone),
                new SqlParameter("@LossPayeeFax", LossPayeeFax),
                new SqlParameter("@LossPayeeContactName", LossPayeeContactName),
                new SqlParameter("@LossPayeeEmail", LossPayeeEmail),
                new SqlParameter("@LossPayeeUseMainAdddress", LossPayeeUseMainAdddress),
                new SqlParameter("@HmdaPurchaser2004T",HmdaPurchaser2004T),
                new SqlParameter("@HmdaPurchaser2015T",HmdaPurchaser2015T),
                new SqlParameter("@InvestorRolodexType", InvestorRolodexType),
                new SqlParameter("@LpeInvestorId", LpeInvestorId),
            };

            if (Id.HasValue)
            {
                storedProcedureName = "INVESTOR_ROLODEX_UPDATE";
                parameters.Add(new SqlParameter("@Id", Id.Value));
            }

            object result = StoredProcedureHelper.ExecuteScalar(this.BrokerId, storedProcedureName, parameters);

            if (!Id.HasValue)
            {
                Id = (int)result;
                IsNew = false;
            }
        }

        public static IEnumerable<InvestorRolodexEntry> GetAll(Guid brokerId, E_InvestorStatus? status)
        {
            return GetImpl(brokerId, null, status).OrderBy(p => p.InvestorName);
        }

        public static InvestorRolodexEntry Get(Guid brokerId, int investorId)
        {
            if (investorId < 0)
            {
                return new InvestorRolodexEntry(brokerId);
            }
            return GetImpl(brokerId, investorId, null).FirstOrDefault();
        }

        public static IEnumerable<KeyValuePair<int, string>> ListActiveLpeInvestor(BrokerDB broker)
        {
            return InvestorNameUtils.ListActiveLpeInvestor(broker);
        }

        /// <summary>
        /// Look in investor rolodex contact and see if any tie to the investor from LPE.
        /// </summary>
        /// <param name="brokerId">Id of the broker to check.</param>
        /// <param name="lpeInvestorName">Investor Name from LPE investor list.</param>
        /// <param name="investorRolodexId">Id of the investor rolodex contact.</param>
        /// <returns>whether there is a investor rolodex tie to investor.</returns>
        public static bool GetInvestorRolodexByLpeInvestorName(BrokerDB broker, string lpeInvestorName, out int investorRolodexId)
        {
            investorRolodexId = -1;

            if (string.IsNullOrEmpty(lpeInvestorName))
            {
                return false;
            }

            // 6/12/2014 dd - Because of LPE Investor information is store on different database
            //                than investor rolodex. Therefore I have to perform the look up in two steps.

            var investorNameList = InvestorNameUtils.ListActiveInvestorNames(broker);

            bool isFound = false;
            int lpeInvestorId = -1;

            foreach (var item in investorNameList)
            {
                if (item.InvestorName.Equals(lpeInvestorName, StringComparison.OrdinalIgnoreCase))
                {
                    isFound = true;
                    lpeInvestorId = item.InvestorId;
                    break;
                }
            }

            if (isFound == false)
            {
                return isFound;
            }


            SqlParameter[] parameters = new SqlParameter[] {
                                            new SqlParameter("@BrokerId", broker.BrokerID),
                                            new SqlParameter("@LpeInvestorId", lpeInvestorId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(broker.BrokerID, "INVESTOR_ROLODEX_FindByLpeInvestorId", parameters))
            {
                if (reader.Read())
                {
                    isFound = true;
                    investorRolodexId = (int)reader["InvestorRolodexId"];
                }
            }
            return isFound;
        }

        private static IEnumerable<InvestorRolodexEntry> GetImpl(Guid brokerId, int? investorId, E_InvestorStatus? status)
        {
            List<InvestorRolodexEntry> entries = new List<InvestorRolodexEntry>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "INVESTOR_ROLODEX_GET", new List<SqlParameter>()
            {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@Id", investorId),
                new SqlParameter("@Status", status) //this is only used when investor id is null. -av 1/2/2012
            }))
            {
                while (reader.Read())
                {
                    entries.Add(new InvestorRolodexEntry(reader));
                }
            }
            return entries;
        }
    }
}
