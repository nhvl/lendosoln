﻿namespace LendersOffice.ObjLib.Rolodex
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using DataAccess;
    using LendersOffice.Security;

    /// <summary>
    /// This class exports a Broker's investors as a CSV temp file.
    /// </summary>
    public class InvestorExporter
    {
        /// <summary>
        /// An array for mapping field names with their respective friendly descriptions, ordered by how
        /// they're supposed to appear in the csv file, and with the function for retrieving the value from the
        /// investor object.
        /// </summary>
        private static Tuple<string, string, Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>>[] exporterMapping = new[]
        {
            Tuple.Create("InvestorRolodexId", "Rolodex ID", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.Id)),
            Tuple.Create("InvestorRolodexType", "Type", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.InvestorRolodexType == E_InvestorRolodexType.Investor ? "Investor" : "Subservicer")),
            Tuple.Create("Status", "Status", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.Status)),
            Tuple.Create("InvestorName", "Investor Name", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.InvestorName)),
            Tuple.Create(
                "LpeInvestorId",
                "PML Investor",
                new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) =>
                    lpeInvestors.FirstOrDefault(lpeInvestor => investor.LpeInvestorId == lpeInvestor.Key).Value ?? string.Empty)),
            Tuple.Create("SellerId", "Seller Id", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.SellerId)),
            Tuple.Create("MERSOrganizationId", "MERS Organization ID", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MERSOrganizationId)),
            Tuple.Create("HmdaPurchaser2015T", "HMDA Purchaser Type 2015", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => Tools.GetHmdaPurchaser2015TDescription(investor.HmdaPurchaser2015T))),
            Tuple.Create("CompanyName", "Company Name", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.CompanyName)),
            Tuple.Create("MainAttention", "Main Attention", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainAttention)),
            Tuple.Create("MainAddress", "Main Address", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainAddress)),
            Tuple.Create("MainState", "Main State", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainState)),
            Tuple.Create("MainCity", "Main City", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainCity)),
            Tuple.Create("MainZip", "Main Zip", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainZip)),
            Tuple.Create("MainPhone", "Main Phone", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainPhone)),
            Tuple.Create("MainFax", "Main Fax", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainFax)),
            Tuple.Create("MainContactName", "Main Contact Name", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainContactName)),
            Tuple.Create("MainEmail", "Main Email", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.MainEmail)),
            Tuple.Create("NoteShipToAttention", "Note Ship To Attention", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToAttention)),
            Tuple.Create("NoteShipToAddress", "Note Ship To Address", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToAddress)),
            Tuple.Create("NoteShipToState", "Note Ship To State", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToState)),
            Tuple.Create("NoteShipToCity", "Note Ship To City", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToCity)),
            Tuple.Create("NoteShipToZip", "Note Ship To Zip", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToZip)),
            Tuple.Create("NoteShipToPhone", "Note Ship To Phone", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToPhone)),
            Tuple.Create("NoteShipToFax", "Note Ship To Fax", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToFax)),
            Tuple.Create("NoteShipToContactName", "Note Ship To Contact Name", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToContactName)),
            Tuple.Create("NoteShipToEmail", "Note Ship To Email", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToEmail)),
            Tuple.Create("NoteShipToUseMainAdddress", "Note Ship To Use Main Address", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.NoteShipToUseMainAdddress)),
            Tuple.Create("PaymentToAttention", "Payment To Attention", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToAttention)),
            Tuple.Create("PaymentToAddress", "Payment To Address", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToAddress)),
            Tuple.Create("PaymentToState", "Payment To State", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToState)),
            Tuple.Create("PaymentToCity", "Payment To City", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToCity)),
            Tuple.Create("PaymentToZip", "Payment To Zip", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToZip)),
            Tuple.Create("PaymentToPhone", "Payment To Phone", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToPhone)),
            Tuple.Create("PaymentToFax", "Payment To Fax", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToFax)),
            Tuple.Create("PaymentToContactName", "Payment To Contact Name", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToContactName)),
            Tuple.Create("PaymentToEmail", "Payment To Email", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToEmail)),
            Tuple.Create("PaymentToUseMainAdddress", "Payment To Use Main Address", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.PaymentToUseMainAdddress)),
            Tuple.Create("LossPayeeAttention", "Loss Payee Attention", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeAttention)),
            Tuple.Create("LossPayeeAddress", "Loss Payee Address", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeAddress)),
            Tuple.Create("LossPayeeState", "Loss Payee State", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeState)),
            Tuple.Create("LossPayeeCity", "Loss Payee City", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeCity)),
            Tuple.Create("LossPayeeZip", "Loss Payee Zip", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeZip)),
            Tuple.Create("LossPayeePhone", "Loss Payee Phone", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeePhone)),
            Tuple.Create("LossPayeeFax", "Loss Payee Fax", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeFax)),
            Tuple.Create("LossPayeeContactName", "Loss Payee Contact Name", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeContactName)),
            Tuple.Create("LossPayeeEmail", "Loss Payee Email", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeEmail)),
            Tuple.Create("LossPayeeUseMainAdddress", "Loss Payee Use Main Address", new Func<InvestorRolodexEntry, IEnumerable<KeyValuePair<int, string>>, object>((investor, lpeInvestors) => investor.LossPayeeUseMainAdddress)),
        };

        /// <summary>
        /// Generates a temporary file with the investor CSV, and returns the file's path.
        /// </summary>
        /// <param name="principal">The principal used to determine what broker to retrieve the investor data from.</param>
        /// <returns>The temporary csv file's path.</returns>
        public static string GenerateInvestorsCsv(AbstractUserPrincipal principal)
        {
            var tempFilePath = TempFileUtils.NewTempFilePath();
            var investors = InvestorRolodexEntry.GetAll(principal.BrokerId, null);
            var lpeInvestors = InvestorRolodexEntry.ListActiveLpeInvestor(principal.BrokerDB);

            StringBuilder csvBuilder = new StringBuilder();

            using (StreamWriter writer = new StreamWriter(tempFilePath, false))
            {
                writer.WriteLine(string.Join(",", InvestorExporter.exporterMapping.Select(mapping => mapping.Item2)));

                foreach (InvestorRolodexEntry investor in investors)
                {
                    foreach (var mapping in exporterMapping)
                    {
                        string value = mapping.Item3(investor, lpeInvestors).ToString();
                        writer.Write("\"" + value + "\",");
                    }

                    writer.WriteLine();
                }
            }

            return tempFilePath;
        }
    }
}
