﻿// <copyright file="ContactsExporter.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
//    Author: antoniov
//    Date:   10/31/2014 3:26:22 PM 
// </summary>
namespace LendersOffice.ObjLib.Rolodex
{
    using System;
    using System.Data;
    using System.Data.Common;
using System.Data.SqlClient;
    using System.IO;
    using System.Text;
    using DataAccess;
    using LendersOffice.Rolodex;

    /// <summary>
    /// Exports contacts.
    /// </summary>
    public class ContactsExporter 
    {
        /// <summary>
        /// The broker id whose contacts will be exported.
        /// </summary>
        private Guid brokerId;

        /// <summary>
        /// The name filter to limit the results.
        /// </summary>
        private string nameFilter;

        /// <summary>
        /// Reduce the result set to the selected agent filter or all if value is null.
        /// </summary>
        private E_AgentRoleT? agentFilter; 

        /// <summary>
        /// Initializes a new instance of the <see cref="ContactsExporter" /> class.
        /// </summary>
        /// <param name="brokerId">The broker identifier for the contacts that will be exported.</param>
        /// <param name="nameFilter">The name filter if any that will be used to reduce the set.</param>
        /// <param name="agentFilter">The agent type to reduce set to.</param>
        public ContactsExporter(Guid brokerId, string nameFilter, E_AgentRoleT? agentFilter)
        {
            this.brokerId = brokerId;
            this.nameFilter = nameFilter;
            this.agentFilter = agentFilter;
        }

        /// <summary>
        /// Exports the contacts for the broker to a comma separated file. 
        /// </summary>
        /// <returns>The path of the file where the results were written to.</returns>
        public string Export()
        {
            string tempFilePath = TempFileUtils.NewTempFilePath();
            
            SqlParameter[] parameters = 
            {
                new SqlParameter("@BrokerID", this.brokerId), 
                new SqlParameter("@NameFilter", this.nameFilter), 
                new SqlParameter("@TypeFilter", this.agentFilter) 
            };

            DataSet ds = new DataSet();

            DataSetHelper.Fill(ds, this.brokerId, "ListRolodexByBrokerIdForExport", parameters);

            System.Text.StringBuilder csv = new System.Text.StringBuilder();

            DataTable table = ds.Tables[0];

            DataColumn typeColumn = table.Columns.Add("Agent Type", typeof(string));
            typeColumn.SetOrdinal(0);

            foreach (DataRow row in table.Rows)
            {
                E_AgentRoleT role = (E_AgentRoleT)row["Type"];
                row[typeColumn] = RolodexDB.GetAgentType(role, string.Empty);
            }

            table.Columns.Remove("Type");

            using (StreamWriter sw = new StreamWriter(tempFilePath, false, Encoding.UTF8))
            {
                int columnCount = table.Columns.Count;
                int lastColumnIndex = columnCount - 1;
                DataColumn column;

                for (int i = 0; i < lastColumnIndex; i++)
                {
                    column = table.Columns[i];

                    sw.Write(column.ColumnName);
                    sw.Write(",");
                }

                column = table.Columns[lastColumnIndex];

                sw.WriteLine(column.ColumnName);

                DataRow currentRow;
                int lastRowIndex = table.Rows.Count - 1;
                for (int i = 0; i <= lastRowIndex; i++)
                {
                    currentRow = table.Rows[i];
                    for (int j = 0; j < lastColumnIndex; j++)
                    {
                        sw.Write(this.FormatToCsvCell(currentRow[j].ToString()));
                        sw.Write(",");
                    }

                    sw.WriteLine(this.FormatToCsvCell(currentRow[lastColumnIndex].ToString()));
                }

                currentRow = table.Rows[lastRowIndex];
                sw.Write(this.FormatToCsvCell(currentRow[lastColumnIndex].ToString()));
            }

            return tempFilePath;
        }

        /// <summary>
        /// Turn a string into a CSV cell output.
        /// </summary>
        /// <param name="str">String to output.</param>
        /// <returns>The comma separated value cell formatted string.</returns>
        private string FormatToCsvCell(string str)
        {
            bool mustQuote = str.Contains(",") || str.Contains("\"") || str.Contains("\r") || str.Contains("\n");

            if (mustQuote)
            {
                StringBuilder sb = new StringBuilder();
                
                sb.Append("\"");

                foreach (char nextChar in str)
                {
                    sb.Append(nextChar);
                    
                    if (nextChar == '"')
                    {
                        sb.Append("\"");
                    }
                }

                sb.Append("\"");

                return sb.ToString();
            }

            return str;
        }
    }
}