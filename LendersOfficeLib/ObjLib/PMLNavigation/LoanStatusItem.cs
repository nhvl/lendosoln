﻿// <copyright file="LoanStatusItem.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 10/20/2016
// </summary>
namespace LendersOffice.ObjLib.PMLNavigation
{
    /// <summary>
    /// Provides a simple container for loan status data.
    /// </summary>
    public class LoanStatusItem
    {
        /// <summary>
        /// Gets or sets the value for the name of the status.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name of the status.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value for the date of the status.
        /// </summary>
        /// <value>
        /// The <see cref="string"/>
        /// </value>
        public string Date { get; set; }

        /// <summary>
        /// Gets or sets the value for the status tooltip.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> tooltip.
        /// </value>
        public string Tooltip { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan is
        /// currently in this status.
        /// </summary>
        /// <value>
        /// True if the loan is in this status, false otherwise.
        /// </value>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this status is collapsed.
        /// </summary>
        /// <value>
        /// True if the status is collapsed, false otherwise.
        /// </value>
        public bool Collapsed { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the loan has completed
        /// the status.
        /// </summary>
        /// <value>
        /// True if the loan has completed the status, false otherwise.
        /// </value>
        public bool Complete { get; set; }
    }
}
