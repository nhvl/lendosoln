﻿namespace LendersOffice.ObjLib.PMLNavigation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using DataAccess;
    using LendersOffice.Constants;

    /// <summary>
    /// Provides a container for TPO loan navigation header 
    /// generic framework link alignment settings.
    /// </summary>
    public class BrokerTpoLoanNavigationHeaderLinkAlignmentSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerTpoLoanNavigationHeaderLinkAlignmentSettings"/>
        /// class using the specified <paramref name="match"/>.
        /// </summary>
        /// <param name="match">
        /// The match from the broker's temp option XML.
        /// </param>
        public BrokerTpoLoanNavigationHeaderLinkAlignmentSettings(Match match)
        {
            this.AlignmentSettings = ConstApp.TpoLoanNavigationPageNames.ToDictionary(
                key => key, 
                value => TpoNavigationLinkAlignment.Left,
                StringComparer.OrdinalIgnoreCase);

            if (!match.Success || match.Groups.Count == 1)
            {
                return;
            }

            this.SetAlignment(match.Groups["CenterAlignment"], TpoNavigationLinkAlignment.Center);
            this.SetAlignment(match.Groups["RightAlignment"], TpoNavigationLinkAlignment.Right);
        }

        /// <summary>
        /// Gets or sets the dictionary of page ID - link alignment settings.
        /// </summary>
        /// <value>
        /// The link alignment settings.
        /// </value>
        private Dictionary<string, TpoNavigationLinkAlignment> AlignmentSettings { get; set; }

        /// <summary>
        /// Obtains the alignment setting for the specified <paramref name="pageName"/>.
        /// </summary>
        /// <param name="pageName">
        /// The name of the page.
        /// </param>
        /// <returns>
        /// The alignment setting.
        /// </returns>
        public TpoNavigationLinkAlignment this[string pageName] => this.AlignmentSettings[pageName];

        /// <summary>
        /// Sets the specified <paramref name="alignment"/> for the pages in the specified <paramref name="group"/>.
        /// </summary>
        /// <param name="group">
        /// The group containing page names.
        /// </param>
        /// <param name="alignment">
        /// The alignment to set.
        /// </param>
        private void SetAlignment(Group group, TpoNavigationLinkAlignment alignment)
        {
            if (!group.Success)
            {
                return;
            }

            var applicablePages = group.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var page in applicablePages)
            {
                var pageName = page.Trim();
                if (!this.AlignmentSettings.ContainsKey(pageName))
                {
                    Tools.LogWarning("[TPO Alignment Settings] Cannot set " + alignment + " alignment for unknown page " + pageName);
                    continue;
                }

                this.AlignmentSettings[pageName] = alignment;
            }
        }
    }
}
