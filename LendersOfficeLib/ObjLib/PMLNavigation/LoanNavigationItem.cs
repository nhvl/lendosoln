﻿// <copyright file="LoanNavigationItem.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 10/20/2016
// </summary>
namespace LendersOffice.ObjLib.PMLNavigation
{
    /// <summary>
    /// Provides a simple container for loan navigation items.
    /// </summary>
    public class LoanNavigationItem
    {
        /// <summary>
        /// Gets or sets the value for the name of the navigation item.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name of the navigation item.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the value for the url of the navigation item.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> url of the navigation item.
        /// </value>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the navigation item is selected.
        /// </summary>
        /// <value>
        /// True if the navigation item is selected, false otherwise.
        /// </value>
        public bool Selected { get; set; }
    }
}
