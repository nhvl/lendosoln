﻿// <copyright file="AgentData.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 10/20/2016
// </summary>
namespace LendersOffice.ObjLib.PMLNavigation
{
    /// <summary>
    /// Provides a simple container for agent data.
    /// </summary>
    public class AgentData
    {
        /// <summary>
        /// Gets or sets the value for the agent role id.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> role id.
        /// </value>
        public string RoleId { get; set; }

        /// <summary>
        /// Gets or sets the value for the name of the agent role.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name of the agent's role.
        /// </value>
        public string RoleName { get; set; }

        /// <summary>
        /// Gets or sets the value for the name of the agent.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> name of the agent.
        /// </value>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the value for the email of the agent.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> email of the agent.
        /// </value>
        public string Email { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the value for the phone of the agent.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> phone of the agent.
        /// </value>
        public string Phone { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the value for the link to assign the agent.
        /// </summary>
        /// <value>
        /// The <see cref="string"/> link to assign the agent.
        /// </value>
        public string AssignmentLink { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets a value indicating whether the role has been assigned.
        /// </summary>
        /// <value>
        /// True if the role has been assigned, false otherwise.
        /// </value>
        public bool Assigned { get; set; }
    }
}
