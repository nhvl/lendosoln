﻿// <copyright file="BrokerTpoLoanNavigationSettings.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 10/20/2016
// </summary>
namespace LendersOffice.ObjLib.PMLNavigation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Admin;

    /// <summary>
    /// Provides a simple container for broker loan navigation settings.
    /// </summary>
    public class BrokerTpoLoanNavigationSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BrokerTpoLoanNavigationSettings"/> class.
        /// </summary>
        /// <param name="brokerId">
        /// The <see cref="Guid"/> of the broker for the settings.
        /// </param>
        /// <param name="enableRetailMode">
        /// True if the broker enables the retail mode of the portal,
        /// false otherwise.
        /// </param>
        /// <param name="match">
        /// The <see cref="Match"/> for the broker's temp option content.
        /// </param>
        public BrokerTpoLoanNavigationSettings(Guid brokerId, bool enableRetailMode, Match match)
        {
            this.HasDisablingTempOption = match.Success;

            if (!match.Success || match.Groups.Count == 1 || string.IsNullOrWhiteSpace(match.Groups[1].Value))
            {
                return;
            }

            this.ProcessOcGroup(brokerId, match);

            if (enableRetailMode)
            {
                this.ProcessBranchGroup(brokerId, match);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the settings 
        /// have the disabling temp option.
        /// </summary>
        /// <value>
        /// True if the settings have the disabled temp option, 
        /// false otherwise.
        /// </value>
        public bool HasDisablingTempOption { get; }

        /// <summary>
        /// Gets the set of originating companies that have the setting enabled.
        /// </summary>
        /// <value>
        /// The set of originating companies that have the setting enabled.
        /// </value>
        private HashSet<Guid> EnabledOriginatingCompanyIds { get; } = new HashSet<Guid>();

        /// <summary>
        /// Gets the set of branches that have the setting enabled.
        /// </summary>
        /// <value>
        /// The set of branches that have the setting enabled.
        /// </value>
        private HashSet<Guid> EnabledBranchIds { get; } = new HashSet<Guid>();

        /// <summary>
        /// Determines whether the originating company with the specified 
        /// <paramref name="pmlBrokerId"/> has the setting enabled.
        /// </summary>
        /// <param name="pmlBrokerId">
        /// The <see cref="Guid"/> for the originating company.
        /// </param>
        /// <returns>
        /// True if the originating company has the setting enabled,
        /// false otherwise.
        /// </returns>
        public bool IsEnabledForOriginatingCompany(Guid pmlBrokerId)
        {
            return this.EnabledOriginatingCompanyIds.Contains(pmlBrokerId);
        }

        /// <summary>
        /// Determines whether the branch with the specified 
        /// <paramref name="branchId"/> has the setting enabled.
        /// </summary>
        /// <param name="branchId">
        /// The <see cref="Guid"/> for the branch.
        /// </param>
        /// <returns>
        /// True if the branch has the setting enabled,
        /// false otherwise.
        /// </returns>
        public bool IsEnabledForBranch(Guid branchId)
        {
            return this.EnabledBranchIds.Contains(branchId);
        }

        /// <summary>
        /// Processes the matched OC group, adding the member originating companies.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the temp option.
        /// </param>
        /// <param name="match">
        /// The regular expression match.
        /// </param>
        private void ProcessOcGroup(Guid brokerId, Match match)
        {
            var selectedPmlBrokerGroup = (from pmlBrokerGroup in GroupDB.GetAllGroups(brokerId, GroupType.PmlBroker)
                                          where string.Equals(pmlBrokerGroup.Name, match.Groups[1].Value, StringComparison.OrdinalIgnoreCase)
                                          select pmlBrokerGroup).FirstOrDefault();

            if (selectedPmlBrokerGroup == null)
            {
                DataAccess.Tools.LogWarning($"[TPO Loan Navigation] Cannot find OC group '{match.Groups[1].Value}' for broker {brokerId} using match {match.Value}.");
            }
            else
            {
                this.EnabledOriginatingCompanyIds.UnionWith(from pmlBrokerMember in GroupDB.GetPmlBrokersWithGroupMembership(brokerId, selectedPmlBrokerGroup.Id)
                                                            where pmlBrokerMember.IsInGroup == 1
                                                            select pmlBrokerMember.PmlBrokerId);
            }
        }

        /// <summary>
        /// Processes the matched branch group, adding the member branches.
        /// </summary>
        /// <param name="brokerId">
        /// The ID of the broker for the temp option.
        /// </param>
        /// <param name="match">
        /// The regular expression match.
        /// </param>
        private void ProcessBranchGroup(Guid brokerId, Match match)
        {
            var selectedBranchGroup = (from branchGroup in GroupDB.GetAllGroups(brokerId, GroupType.Branch)
                                       where string.Equals(branchGroup.Name, match.Groups[1].Value, StringComparison.OrdinalIgnoreCase)
                                       select branchGroup).FirstOrDefault();

            if (selectedBranchGroup == null)
            {
                DataAccess.Tools.LogWarning($"[TPO Loan Navigation] Cannot find branch group '{match.Groups[1].Value}' for broker {brokerId} using match {match.Value}.");
            }
            else
            {
                this.EnabledBranchIds.UnionWith(from branchMember in GroupDB.GetBranchesWithGroupMembership(brokerId, selectedBranchGroup.Id)
                                                where branchMember.IsInGroup == 1
                                                select branchMember.BranchId);
            }
        }
    }
}
