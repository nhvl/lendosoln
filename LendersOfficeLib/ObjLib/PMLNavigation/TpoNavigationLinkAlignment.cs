﻿namespace LendersOffice.ObjLib.PMLNavigation
{
    /// <summary>
    /// Represents the alignment of generic framework vendor links
    /// in the TPO portal.
    /// </summary>
    public enum TpoNavigationLinkAlignment
    {
        /// <summary>
        /// The links are left-aligned.
        /// </summary>
        Left = 0,

        /// <summary>
        /// The links are center-aligned.
        /// </summary>
        Center = 1,

        /// <summary>
        /// The links are right-aligned.
        /// </summary>
        Right = 2
    }
}
