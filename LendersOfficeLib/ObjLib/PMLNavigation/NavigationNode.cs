﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using DataAccess;
using System.IO;
using System.Xml;
using CommonProjectLib.Common.Lib;
using LendersOffice.AntiXss;
using LendersOffice.Common;
using LendersOffice.Security;
using static LendersOffice.ObjLib.PMLNavigation.NavigationConfiguration;

namespace LendersOffice.ObjLib.PMLNavigation
{
    public enum E_NavTarget
    {
        NewWindow = 0,
        WithinPage = 1,
        Self = 2
    }

    /// <summary>
    /// Represents the data structure used to render the navigation tree config ui. 
    /// DynaTree is the plugin in use https://code.google.com/p/dynatree/ 
    /// </summary>
    public sealed class DynaTreeNode
    {
        public string key { get; set; }
        public string title { get; set; }
        public bool isFolder { get; set; }
        public string addClass { get; set; }
        public string ExternalName { get; set; }
        public string InternalName { get; set; }
        public string ConfigHref { get; set; }
        public E_NavTarget ConfigTarget { get; set; }
        public bool IsSystem { get; set; }
        public bool IsEditableSystemPage { get; set; }
        public bool CanBeRenamed { get; set; }
        public bool IsDisabledByConfigSetting { get; set; }
        public Guid Id { get; set; }
        public string AdditionalData { get; set; }
        public bool ProvideSystemAccess { get; set; }
        public bool AllowBrokerMode { get; set; }
        public bool AllowCorrMode { get; set; }
        public bool AllowMiniCorrMode { get; set; }
        public bool AllowRetailMode { get; set; }
        public bool AllowLoanOfficer { get; set; }
        public bool AllowProcessor { get; set; }
        public bool AllowSecondary { get; set; }
        public bool AllowPostCloser { get; set; }
        public bool AllowSupervisor { get; set; }
        public bool AllowAccountExecutive { get; set; }
        public bool AllowAny { get; set; }
        public List<DynaTreeNode> children { get; set; }
        public bool AllowSafeHtml { get; set; }
    }

    public abstract class NavigationNode : IXmlSerializable
    {
        public string InternalName { get; set; }
        public string ExternalName { get; set; }
        public void SetNames(string name)
        {
            InternalName = name;
            ExternalName = name;
        }
        public Guid Id { get; set; }


        #region IXmlSerializable Members

        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public virtual void ReadXml(System.Xml.XmlReader reader)
        {
            reader.MoveToContent();
            InternalName = reader.GetAttribute("InternalName");
            ExternalName = reader.GetAttribute("ExternalName");
            Id = new Guid(reader.GetAttribute("Id"));
        }

        public virtual void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("InternalName", InternalName);
            writer.WriteAttributeString("ExternalName", ExternalName);
            writer.WriteAttributeString("Id", Id.ToString());
        }
        #endregion

        public virtual DynaTreeNode GetDynaTreeJSONObject(Action<DynaTreeNode> configurer, Func<DynaTreeNode, bool> filterer)
        {
            DynaTreeNode dt = new DynaTreeNode();
            dt.title = AspxTools.HtmlString(InternalName);
            dt.InternalName = InternalName;
            dt.ExternalName = ExternalName;
            dt.Id = Id;
            return dt;
        }

        public virtual void LoadFrom(DynaTreeNode node)
        {
            InternalName = node.InternalName;
            ExternalName = node.ExternalName;
            Id = node.Id;
        }

        public abstract void Traverse(int level, Action<int, NavigationNode> visit);

    }

    public sealed class FolderNode : NavigationNode
    {
        public List<NavigationNode> Children { get; set; }

        public FolderNode()
        {
            Children = new List<NavigationNode>();
        }

        public override void ReadXml(System.Xml.XmlReader reader)
        {
            base.ReadXml(reader);

            bool isEmptyElement = reader.IsEmptyElement;
            reader.ReadStartElement();

            if (!isEmptyElement)
            {
                while (reader.NodeType != System.Xml.XmlNodeType.EndElement)
                {
                    NavigationNode node; 
                    if (reader.Name == "PageNode")
                    {
                         node = new PageNode();
                    }
                    else if (reader.Name == "FolderNode")
                    {
                        node = new FolderNode();
                    }
                    else
                    {
                        throw CBaseException.GenericException("Unhandled element " + reader.Name);
                    }
                    node.ReadXml(reader);
                    Children.Add(node);
                }
                reader.ReadEndElement();
            }
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            base.WriteXml(writer);

            foreach (NavigationNode node in Children)
            {
                if (node is PageNode)
                {
                    writer.WriteStartElement("PageNode");
                }
                else if (node is FolderNode)
                {
                    writer.WriteStartElement("FolderNode");
                }

                else
                {
                    throw CBaseException.GenericException("Unhandled node.");
                }

                node.WriteXml(writer);
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Generates a dynatree object to be used by the config or the nav UI.
        /// Configurer action allows you to make changes to the newly made folder while
        /// filterer allows you to check permissions on the page objects
        /// </summary>
        /// <param name="configurer">Allows configuration of dynatree object</param>
        /// <param name="filterer">filterer(pageId) returns false page is not added to dynanode</param>
        /// <returns>Folder with children</returns>
        public override DynaTreeNode GetDynaTreeJSONObject(Action<DynaTreeNode> configurer, Func<DynaTreeNode, bool> filterer)
        {
            var obj = base.GetDynaTreeJSONObject(configurer, null);
            obj.isFolder = true;
            obj.children = new List<DynaTreeNode>();
            foreach (var child in Children)
            {
                var newChild = child.GetDynaTreeJSONObject(configurer, filterer);

                if (filterer == null || filterer(newChild))
                {
                    obj.children.Add(newChild);
                }
            }
            configurer(obj);
            return obj;
        }


        public override void LoadFrom(DynaTreeNode node)
        {
            base.LoadFrom(node);
            if (node.children != null)
            {
                foreach (var child in node.children)
                {
                    NavigationNode navNode;
                    if (child.isFolder)
                    {
                        FolderNode folderNode = new FolderNode();
                        navNode = folderNode;
                    }
                    else
                    {
                        PageNode pageNode = new PageNode();
                        navNode = pageNode;
                    }
                    navNode.LoadFrom(child);
                    Children.Add(navNode);
                }
            }
        }

        public override void Traverse(int level, Action<int,NavigationNode> visit)
        {
            visit(level, this);

            foreach (var child in Children)
            {
                child.Traverse(level+1, visit);
            }
        }

        public IEnumerable<DynaTreeNode> NodesRecursive(Func<DynaTreeNode, bool> filter)
        {
            var nodes = new List<DynaTreeNode>();
            this.Traverse(0, (level, navNode) =>
            {
                var node = navNode.GetDynaTreeJSONObject(p => { }, null);
                if (filter(node))
                {
                    nodes.Add(node);
                }
            });

            return nodes;
        }

    }

    public sealed class PageNode : NavigationNode
    {
        public string Url { get; set; }
        public E_NavTarget Target { get; set; }
        public bool ProvideSystemAccess { get; set; }

        public bool AllowBrokerMode { get; set; }
        public bool AllowCorrMode { get; set; }
        public bool AllowMiniCorrMode { get; set; }
        public bool AllowRetailMode { get; set; }

        public bool AllowLoanOfficer { get; set; }
        public bool AllowProcessor { get; set; }
        public bool AllowSecondary { get; set; }
        public bool AllowPostCloser { get; set; }
        public bool AllowSupervisor { get; set; }
        public bool AllowAccountExecutive { get; set; }
        public bool AllowAny { get; set; }

        public override void Traverse(int level, Action<int,NavigationNode> visit)
        {
            visit(level, this);
        }

        public override void WriteXml(System.Xml.XmlWriter writer)
        {
            base.WriteXml(writer);
            writer.WriteAttributeString("Url", Url);
            writer.WriteAttributeString("Target", Target.ToString("d"));
            writer.WriteAttributeString("ProvideSystemAccess", this.ProvideSystemAccess.ToString());

            writer.WriteAttributeString("AllowCorrMode", AllowCorrMode.ToString());
            writer.WriteAttributeString("AllowMiniCorrMode", AllowMiniCorrMode.ToString());
            writer.WriteAttributeString("AllowBrokerMode", AllowBrokerMode.ToString());
            writer.WriteAttributeString("AllowRetailMode", this.AllowRetailMode.ToString());

            writer.WriteAttributeString("AllowLoanOfficer", AllowLoanOfficer.ToString());
            writer.WriteAttributeString("AllowProcessor", AllowProcessor.ToString());
            writer.WriteAttributeString("AllowSecondary", AllowSecondary.ToString());
            writer.WriteAttributeString("AllowPostCloser", AllowPostCloser.ToString());
            writer.WriteAttributeString("AllowSupervisor", AllowSupervisor.ToString());
            writer.WriteAttributeString("AllowAccountExecutive", AllowAccountExecutive.ToString());
            writer.WriteAttributeString("AllowAny", AllowAny.ToString());
        }

        public override void ReadXml(XmlReader reader)
        {
            base.ReadXml(reader);
            Url = reader.GetAttribute("Url");
            Target = (E_NavTarget)(int.Parse(reader.GetAttribute("Target")));
            this.ProvideSystemAccess = bool.Parse(reader.GetAttribute("ProvideSystemAccess") ?? "False");

            if (reader.GetAttribute("AllowCorrMode") != null)
            {
                AllowCorrMode = bool.Parse(reader.GetAttribute("AllowCorrMode"));
                AllowMiniCorrMode = bool.Parse((reader.GetAttribute("AllowMiniCorrMode")));
                AllowBrokerMode = bool.Parse(reader.GetAttribute("AllowBrokerMode"));
            }

            if (reader.GetAttribute("AllowRetailMode") != null)
            {
                this.AllowRetailMode = bool.Parse(reader.GetAttribute("AllowRetailMode"));
            }

            if (reader.GetAttribute("AllowLoanOfficer") != null)
            {
                AllowLoanOfficer = bool.Parse(reader.GetAttribute("AllowLoanOfficer"));
                AllowProcessor = bool.Parse((reader.GetAttribute("AllowProcessor")));
                AllowSecondary = bool.Parse(reader.GetAttribute("AllowSecondary"));
                AllowPostCloser = bool.Parse(reader.GetAttribute("AllowPostCloser"));
                AllowSupervisor = bool.Parse(reader.GetAttribute("AllowSupervisor"));
            }

            if (reader.GetAttribute("AllowAccountExecutive") != null)
            {
                AllowAccountExecutive = bool.Parse(reader.GetAttribute("AllowAccountExecutive"));
                AllowAny = bool.Parse(reader.GetAttribute("AllowAny"));
            }
            else
            {
                // only create defaults for specific system pages
                if (!SystemNode.BelongsToSystemNodeThatCantBeHidden(this.Id))
                {
                    AllowCorrMode = true;
                    AllowMiniCorrMode = true;
                    AllowBrokerMode = true;
                    AllowRetailMode = true;

                    AllowLoanOfficer = true;
                    AllowProcessor =  true;
                    AllowSecondary = true;
                    AllowPostCloser = true;
                    AllowSupervisor = true;
                    AllowAccountExecutive = true;
                    AllowAny = true;
                }
            }

            Boolean isEmptyElement = reader.IsEmptyElement; // (1)
            reader.ReadStartElement();

            if (!isEmptyElement)
            {
                reader.ReadEndElement();
            }
        }

        public override DynaTreeNode GetDynaTreeJSONObject(Action<DynaTreeNode> configurer, Func<DynaTreeNode, bool> filterer)
        {
            var obj = base.GetDynaTreeJSONObject(configurer, filterer);
            obj.ConfigHref = Url;
            obj.ConfigTarget= Target;
            obj.ProvideSystemAccess = ProvideSystemAccess;
            obj.AllowBrokerMode = AllowBrokerMode;
            obj.AllowCorrMode = AllowCorrMode;
            obj.AllowMiniCorrMode = AllowMiniCorrMode;
            obj.AllowRetailMode = this.AllowRetailMode;
            obj.AllowLoanOfficer = AllowLoanOfficer;
            obj.AllowProcessor = AllowProcessor;
            obj.AllowSecondary = AllowSecondary;
            obj.AllowPostCloser = AllowPostCloser;
            obj.AllowSupervisor = AllowSupervisor;
            obj.AllowAccountExecutive = AllowAccountExecutive;
            obj.AllowAny = AllowAny;
            configurer(obj);
            return obj;
        }

        public override void LoadFrom(DynaTreeNode node)
        {
            base.LoadFrom(node);
            this.Target = node.ConfigTarget;
            this.ProvideSystemAccess = node.ProvideSystemAccess;
            this.Url = node.ConfigHref;
            this.AllowBrokerMode = node.AllowBrokerMode;
            this.AllowCorrMode = node.AllowCorrMode;
            this.AllowMiniCorrMode = node.AllowMiniCorrMode;
            this.AllowRetailMode = node.AllowRetailMode;
            this.AllowLoanOfficer = node.AllowLoanOfficer;
            this.AllowProcessor = node.AllowProcessor;
            this.AllowSecondary = node.AllowSecondary;
            this.AllowPostCloser = node.AllowPostCloser;
            this.AllowSupervisor = node.AllowSupervisor;
            this.AllowAccountExecutive = node.AllowAccountExecutive;
            this.AllowAny = node.AllowAny;
        }

    }


    public sealed class NavigationConfiguration
    {
        public static class SystemNode
        {
            public static readonly Guid NavigationLink =  new Guid("11111111-1111-1111-1111-000000000000");
            public static readonly Guid LaunchQuickPricerLink = new Guid("11111111-1111-1111-1111-000000000001");
            public static readonly Guid ImportLoan = new Guid("11111111-1111-1111-1111-000000000002");
            public static readonly Guid PipelineFolder = new Guid("11111111-1111-1111-1111-000000000003");
            public static readonly Guid Loans = new Guid("11111111-1111-1111-1111-000000000004");
            public static readonly Guid Tasks = new Guid("11111111-1111-1111-1111-000000000005");
            public static readonly Guid CreatePurchaseLoan = new Guid("11111111-1111-1111-1111-000000000006");
            public static readonly Guid CreateRefiLoan = new Guid("11111111-1111-1111-1111-000000000007");
            public static readonly Guid Create2ndLien = new Guid("11111111-1111-1111-1111-000000000008");
            public static readonly Guid Dashboard = new Guid("11111111-1111-1111-1111-000000000009");
            public static readonly Guid CreateLoanFolder = new Guid("11111111-1111-1111-1111-000000000010");
            public static readonly Guid QuickpricerFolder = new Guid("11111111-1111-1111-1111-000000000011");
            public static readonly Guid ConditionLink = new Guid("11111111-1111-1111-1111-000000000012");
            public static readonly Guid CreateHeloc1stLien = new Guid("11111111-1111-1111-1111-000000000013");
            public static readonly Guid CreateHeloc2ndLien = new Guid("11111111-1111-1111-1111-000000000014");
            public static readonly Guid LaunchQuickPricer2Link = new Guid("11111111-1111-1111-1111-000000000015");
            public static readonly Guid QuickPricerRateMonitorsLink = new Guid("11111111-1111-1111-1111-000000000016");
            public static readonly Guid PortalMode = new Guid("11111111-1111-1111-1111-000000000017");
            public static readonly Guid ManageUsers = new Guid("11111111-1111-1111-1111-000000000018");
            public static readonly Guid MyProfile = new Guid("11111111-1111-1111-1111-000000000019");
            public static readonly Guid Leads = new Guid("11111111-1111-1111-1111-000000000020");
            public static readonly Guid CreateLeadFolder = new Guid("11111111-1111-1111-1111-000000000021");
            public static readonly Guid ImportLead = new Guid("11111111-1111-1111-1111-000000000022");
            public static readonly Guid CreatePurchaseLead = new Guid("11111111-1111-1111-1111-000000000023");
            public static readonly Guid CreateRefiLead = new Guid("11111111-1111-1111-1111-000000000024");
            public static readonly Guid CreateHeloc1stLienLead = new Guid("11111111-1111-1111-1111-000000000025");
            public static readonly Guid Create2ndLienLead = new Guid("11111111-1111-1111-1111-000000000026");
            public static readonly Guid CreateHeloc2ndLienLead = new Guid("11111111-1111-1111-1111-000000000027");
            public static readonly Guid OriginatingCompanyServiceCredentials = new Guid("11111111-1111-1111-1111-000000000028");
            public static readonly Guid NonQmToolkitFolder = new Guid("11111111-1111-1111-1111-000000000029");
            public static readonly Guid NonQmQp = new Guid("11111111-1111-1111-1111-000000000030");
            public static readonly Guid BankStatementIncomeCalculator = new Guid("11111111-1111-1111-1111-000000000031");
            public static readonly Guid ScenarioRequestForm = new Guid("11111111-1111-1111-1111-000000000032");

            /// <summary>
            /// Item 1 - Name
            /// Item 2 IsFolder
            /// Item 3 ParentFolderId
            /// Item 4 Rank - This is basically the order in which to create them for a blank tree.
            /// </summary>
            private static Dictionary<Guid, Tuple<string, bool, Guid, int>> x_Nodes = new FriendlyDictionary<Guid, Tuple<string,bool, Guid, int>>()
            {
                { NavigationLink, Tuple.Create("Navigation Links", true, Guid.Empty, 0) },
                { PortalMode, Tuple.Create("Portal:", false,NavigationLink, 1)},
                { Dashboard, Tuple.Create("Dashboard", false, NavigationLink, 2) },
                
                { PipelineFolder, Tuple.Create("Pipelines", true, NavigationLink, 20) },
                { Loans, Tuple.Create("Loans", false, PipelineFolder, 21) },
                { Leads, Tuple.Create("Leads", false , PipelineFolder, 22) },
                { Tasks, Tuple.Create("Tasks", false, PipelineFolder, 23) },
                { ConditionLink, Tuple.Create("Conditions", false , PipelineFolder, 24) },

                { QuickpricerFolder, Tuple.Create("QuickPricer", true, NavigationLink, 30) },
                { LaunchQuickPricerLink,  Tuple.Create("Launch Quick Pricer", false, QuickpricerFolder, 31) },
                { LaunchQuickPricer2Link,  Tuple.Create("Launch Quick Pricer", false, QuickpricerFolder, 32) },
                { QuickPricerRateMonitorsLink, Tuple.Create("Monitored Scenarios", false, QuickpricerFolder, 33) },                

                { CreateLeadFolder, Tuple.Create("Create New Lead", true, NavigationLink, 40) },
                { ImportLead, Tuple.Create("Import lead file", false, CreateLeadFolder, 41) },
                { CreatePurchaseLead, Tuple.Create("Create purchase lead", false, CreateLeadFolder, 42) },
                { CreateRefiLead, Tuple.Create("Create refinance lead", false, CreateLeadFolder, 43) },
                { CreateHeloc1stLienLead, Tuple.Create("Create HELOC 1st Lien Lead", false, CreateLeadFolder, 44) },
                { Create2ndLienLead, Tuple.Create("Create 2nd lien (standalone) lead", false, CreateLeadFolder, 45) },
                { CreateHeloc2ndLienLead, Tuple.Create("Create HELOC 2nd Lien (standalone) Lead", false, CreateLeadFolder, 46) },

                { CreateLoanFolder, Tuple.Create("Create New Loan", true, NavigationLink, 50) },
                { ImportLoan, Tuple.Create("Import loan file", false, CreateLoanFolder, 51) },
                { CreatePurchaseLoan, Tuple.Create("Create purchase loan", false, CreateLoanFolder, 52) },
                { CreateRefiLoan, Tuple.Create("Create refinance loan", false, CreateLoanFolder, 53) },
                { CreateHeloc1stLien, Tuple.Create("Create HELOC 1st Lien", false, CreateLoanFolder, 54) },
                { Create2ndLien, Tuple.Create("Create 2nd lien (standalone)", false, CreateLoanFolder, 55) },
                { CreateHeloc2ndLien, Tuple.Create("Create HELOC 2nd Lien (standalone)", false, CreateLoanFolder, 56) },

                { ManageUsers, Tuple.Create("Manage Users", false,NavigationLink, 101) },
                { OriginatingCompanyServiceCredentials, Tuple.Create("Service Credentials", false, NavigationLink, 102) },
                { MyProfile, Tuple.Create("My Profile", false, NavigationLink, 103) },

                { NonQmToolkitFolder, Tuple.Create("Non-QM Toolkit", true, NavigationLink, 120) },
                { NonQmQp, Tuple.Create("Non-QM Quick Pricer", false, NonQmToolkitFolder, 121) },
                { BankStatementIncomeCalculator, Tuple.Create("Bank Statement Income Submission", false, NonQmToolkitFolder, 122) },
                { ScenarioRequestForm, Tuple.Create("Scenario Request Form", false, NonQmToolkitFolder, 123) },
            };

            public static NavigationNode GetNewSystemNode(Guid id)
            {
                NavigationNode node;
                if (IsFolder(id))
                {
                    node = new FolderNode();
                }
                else
                {
                    node = new PageNode();
                }

                node.Id = id;
                node.InternalName = GetSystemNodeName(id);
                node.ExternalName = GetSystemNodeName(id);
                return node;
            }

            public static bool BelongsToSystemNode(Guid id)
            {
                return x_Nodes.ContainsKey(id);
            }

            public static bool BelongsToSystemNodeThatCantBeHidden(Guid id)
            {
                if (id.EqualsOneOf(Dashboard, ManageUsers, MyProfile, PortalMode, NavigationLink, PipelineFolder, QuickpricerFolder, CreateLeadFolder))  
                {
                    return true;
                }

                return false;
            }

            public static bool BelongsToSystemNodeThatCanBeRenamed(Guid id)
            {
                if (id.EqualsOneOf(NonQmToolkitFolder, NonQmQp, BankStatementIncomeCalculator, ScenarioRequestForm))
                {
                    return true;
                }

                return false;
            }

            public static bool IsNonQmRelated(Guid id)
            {
                if (id.EqualsOneOf(NonQmToolkitFolder, NonQmQp, BankStatementIncomeCalculator, ScenarioRequestForm))
                {
                    return true;
                }

                return false;
            }

            public static string GetSystemNodeName(Guid id)
            {
                return x_Nodes[id].Item1;
            }

            /// <summary>
            /// Returns The system ids based on ordering rank. 
            /// </summary>
            /// <returns></returns>
            public static IEnumerable<Guid> GetSystemIds()
            {
                return x_Nodes.Keys.OrderBy(p => x_Nodes[p].Item4);
            }

            /// <summary>
            /// Basically iterates trough root node structure and adds each visited node to a dictionary 
            /// At the end it iterates through all system ids  in rank order (0 Root, 1st Level Folders, Pages/Folder) 
            /// If its missing it adds it in at the end of the parent. 
            /// </summary>
            /// <param name="rootNode"></param>
            public static void MigrateConfig(FolderNode rootNode, bool isNonQmOpEnabled)
            {
                if (rootNode.Id != NavigationLink)
                {
                    throw CBaseException.GenericException("Failure - Root Node is not Navigation Links");
                }

                Dictionary<Guid, NavigationNode> visitedNodes = new FriendlyDictionary<Guid, NavigationNode>();

                Action<int, NavigationNode> action = (level, node) =>
                {
                    visitedNodes.Add(node.Id, node);
                };

                rootNode.Traverse(0, action);

                foreach (var systemId in GetSystemIds())
                {
                    if (visitedNodes.ContainsKey(systemId))
                    {
                        continue;
                    }

                    if (isNonQmOpEnabled || !BelongsToSystemNodeThatCanBeRenamed(systemId))
                    {
                        NavigationNode node = GetNewSystemNode(systemId);
                        var parentFolder = (FolderNode)visitedNodes[x_Nodes[systemId].Item3];
                        parentFolder.Children.Add(node);
                        visitedNodes.Add(node.Id, node);    // This is necessary to allow new system folders to be added.
                    }                    
                }
            }

            public static bool IsFolder(Guid id)
            {
                return x_Nodes[id].Item2;
            }

            public static int Count()
            {
                return x_Nodes.Count;
            }

            public static FolderNode GetSystemNodeStructure()
            {
                FolderNode rootNode = (FolderNode)GetNewSystemNode(NavigationLink);
                Dictionary<Guid, FolderNode> folders = new FriendlyDictionary<Guid, FolderNode>()
                {
                    { NavigationLink, rootNode }
                };

                foreach (var item in GetSystemIds())
                {
                    if (item == NavigationLink)
                    {
                        continue; // we already made the root 
                    }

                    Tuple<string, bool, Guid, int> entry = x_Nodes[item];
                    NavigationNode currentNode = GetNewSystemNode(item);

                    if (entry.Item2)
                    {
                        folders.Add(item, (FolderNode)currentNode);
                    }

                    folders[entry.Item3].Children.Add(currentNode);
                }

                return rootNode;

            }
        }

        private FolderNode m_rootNode;


        public NavigationConfiguration()
        {
            m_rootNode = SystemNode.GetSystemNodeStructure();
        }

        public void ReadFromXml(string xml, bool isNonQmOpEnabled)
        {
            if (!string.IsNullOrEmpty(xml))
            {
                m_rootNode = new FolderNode();
                using (StringReader sr = new StringReader(xml))
                using (XmlReader reader = XmlReader.Create(sr))
                {
                    reader.MoveToContent();
                    m_rootNode.ReadXml(reader);
                }
                SystemNode.MigrateConfig(m_rootNode, isNonQmOpEnabled);
            }
        }

        public static bool ValidateUrl(string url)
        {
            return Tools.IsValidUrl(url, new string[] { 
                Uri.UriSchemeHttp,
                Uri.UriSchemeHttps,
                Uri.UriSchemeMailto
               });
        }

        public void ReadFromDynaTreeJson(string json, bool isNonQmOpEnabled)
        {
            DynaTreeNode node = ObsoleteSerializationHelper.JavascriptJsonDeserializer<DynaTreeNode>(json.Substring(1, json.Length - 2));
            m_rootNode = new FolderNode();
            m_rootNode.LoadFrom(node);
            SystemNode.MigrateConfig(m_rootNode, isNonQmOpEnabled);
        }

        public void ReadFromDynaTreeNodeJson(string json, bool isNonQmOpEnabled)
        {
            DynaTreeNode node = ObsoleteSerializationHelper.JavascriptJsonDeserializer<DynaTreeNode>(json);
            m_rootNode = new FolderNode();
            m_rootNode.LoadFrom(node);
            SystemNode.MigrateConfig(m_rootNode, isNonQmOpEnabled);
        }


        /// <summary>
        /// Add level checking
        /// </summary>
        /// <returns></returns>
        private bool Validate(bool isNonQmOpEnabled)
        {
            bool isValid = true; 
            List<Guid> systemIds = new List<Guid>();
            Action<int, NavigationNode> visitAction = (x,p) =>
            {
                if (!isValid)
                {
                    return; 
                }

                if (p.Id == Guid.Empty)
                {
                    isValid = false;
                    return;
                }

                FolderNode fn = p as FolderNode;
                PageNode pg = p as PageNode;

                if (SystemNode.BelongsToSystemNode(p.Id))
                {
                    if (!isNonQmOpEnabled && SystemNode.IsNonQmRelated(p.Id))
                    {
                        return;
                    }

                    systemIds.Add(p.Id);

                    if (!SystemNode.BelongsToSystemNodeThatCanBeRenamed(p.Id))
                    {
                        p.InternalName = p.ExternalName = SystemNode.GetSystemNodeName(p.Id);
                    }

                    if (fn == null && SystemNode.IsFolder(p.Id))
                    {
                        isValid = false;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(p.InternalName.TrimWhitespaceAndBOM()))
                    {
                        isValid = false;
                        return;
                    }
                    if (string.IsNullOrEmpty(p.ExternalName.TrimWhitespaceAndBOM()))
                    {
                        isValid = false;
                        return;
                    }

                    if (pg != null)
                    {
                        if (!ValidateUrl(pg.Url))
                        {
                            isValid = false;
                            return;
                        }
                    }
                }
            };

            m_rootNode.Traverse(0,visitAction);

            int actualSystemNodeCount = SystemNode.Count();

            if (!isNonQmOpEnabled)
            {
                actualSystemNodeCount -= SystemNode.GetSystemIds().Where(n => SystemNode.IsNonQmRelated(n)).Count();
            }

            if (systemIds.Count != actualSystemNodeCount)
            {
                isValid = false;
            }

            return isValid;            
        }

        public string SerializeNavigation(bool isNonQmOpEnabled)
        {
            if (!Validate(isNonQmOpEnabled))
            {
                throw new CBaseException("Invalid navigation state. Please reload.", "Could not validate tree");
            }
            
            StringBuilder sb = new StringBuilder();

            using (StringWriter sw = new StringWriter(sb))
            using (XmlWriter writer = XmlWriter.Create(sw))
            {
                writer.WriteStartElement("FolderNode");
                m_rootNode.WriteXml(writer);
                writer.WriteEndElement();
            }
            return sb.ToString();
        }

        public FolderNode RootNode { get { return m_rootNode; } }

        public IEnumerable<DynaTreeNode> GetNavigation(Action<DynaTreeNode> configurer, Func<DynaTreeNode, bool> isPageNodeAccessible)
        {

            Action<DynaTreeNode> baseconfigurer = p => {
                if (SystemNode.BelongsToSystemNode(p.Id) && ! SystemNode.BelongsToSystemNodeThatCanBeRenamed(p.Id))
                {
                    p.title = p.ExternalName = SystemNode.GetSystemNodeName(p.Id);
                }

                configurer(p);
            };

            return m_rootNode.GetDynaTreeJSONObject(baseconfigurer, isPageNodeAccessible).children;
        }

        public IEnumerable<Tuple<Guid, string>> GetPageNodeNames()
        {
            List<Tuple<Guid, string>> items = new List<Tuple<Guid, string>>();
            Stack<string> nameParts = new Stack<string>();
            Action<int,NavigationNode> visit = (x,p) =>
            {
                while (nameParts.Count > x)
                {
                    nameParts.Pop();
                }

                nameParts.Push(p.InternalName);
                
                if (p is PageNode)
                {
                    if (!SystemNode.BelongsToSystemNode(p.Id))
                    {
                        items.Add(Tuple.Create(p.Id, string.Join("::", nameParts.Reverse().Skip(1).ToArray())));
                    }
                }
            };

            m_rootNode.Traverse(0, visit);
            return items;
        }

        public string GetDynaTraceJSON()
        {
            var currentBroker = PrincipalFactory.CurrentPrincipal?.BrokerDB;

            Action<DynaTreeNode> configurer = p =>
            {
                if (SystemNode.BelongsToSystemNode(p.Id))
                {
                    p.IsSystem = true;
                    p.addClass = "system";

                    if (SystemNode.BelongsToSystemNodeThatCanBeRenamed(p.Id))
                    {
                        p.CanBeRenamed = true;
                    }
                    else
                    {
                        p.ExternalName = SystemNode.GetSystemNodeName(p.Id);
                        p.title = p.ExternalName;
                    }                    

                    if (!SystemNode.BelongsToSystemNodeThatCantBeHidden(p.Id))
                    {
                        p.IsEditableSystemPage = true;
                    }
                }
                else
                {
                    p.title = AspxTools.HtmlString(p.InternalName);
                }

                if (currentBroker == null)
                {
                    return;
                }

                if (p.Id == SystemNode.Tasks && !currentBroker.IsShowPipelineOfTasksForAllLoansInTpoPortal)
                {
                    p.IsDisabledByConfigSetting = true;
                    p.IsEditableSystemPage = false;
                    p.CanBeRenamed = false;
                }
                else if (p.Id == SystemNode.ConditionLink && !currentBroker.IsShowPipelineOfConditionsForAllLoansInTpoPortal)
                {
                    p.IsDisabledByConfigSetting = true;
                    p.IsEditableSystemPage = false;
                    p.CanBeRenamed = false;
                }
                else if (p.Id == SystemNode.Leads)
                {
                    p.title = "Leads (Retail users only)";
                }
                else if (p.Id == SystemNode.CreateLeadFolder)
                {
                    p.title = "Create New Lead (Retail users only)";
                }
            };

            var obj = m_rootNode.GetDynaTreeJSONObject(configurer, null );
            string data = ObsoleteSerializationHelper.JavascriptJsonSerialize(obj);
            return data;
        }
    }

}
