﻿// <copyright file="TaskConditionCount.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 10/20/2016
// </summary>
namespace LendersOffice.ObjLib.PMLNavigation
{
    using System;
    using System.Data.Common;
using System.Data.SqlClient;

    /// <summary>
    /// Provides a simple wrapper and retrieval mechanism for
    /// the number of tasks and conditions on a loan file.
    /// </summary>
    public class TaskConditionCount
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TaskConditionCount"/> class.
        /// </summary>
        /// <param name="brokerId">
        /// The id of the broker for the loan.
        /// </param>
        /// <param name="loanId">
        /// The id of the loan.
        /// </param>
        /// <param name="userId">
        /// The id of the assigned user for the tasks and conditions on the loan.
        /// </param>
        public TaskConditionCount(Guid brokerId, Guid loanId, Guid userId)
        {
            this.BrokerId = brokerId;
            this.LoanId = loanId;
            this.UserId = userId;
        }

        /// <summary>
        /// Gets the number of active tasks on the loan.
        /// </summary>
        /// <value>
        /// The number of active tasks on the loan.
        /// </value>
        public int TaskCount { get; private set; }

        /// <summary>
        /// Gets the number of active conditions on the loan.
        /// </summary>
        /// <value>
        /// The number of active conditions on the loan.
        /// </value>
        public int ConditionCount { get; private set; }

        /// <summary>
        /// Gets the id of the broker for the loan.
        /// </summary>
        /// <value>
        /// The id of the broker for the loan.
        /// </value>
        private Guid BrokerId { get; }

        /// <summary>
        /// Gets the id of the loan.
        /// </summary>
        /// <value>
        /// The id of the loan.
        /// </value>
        private Guid LoanId { get; }

        /// <summary>
        /// Gets the id of the assigned user for the tasks and conditions on the loan.
        /// </summary>
        /// <value>
        /// The id of the assigned user for the tasks and conditions on the loan.
        /// </value>
        private Guid UserId { get; }

        /// <summary>
        /// Retrieves the number of active tasks and conditions on the loan.
        /// </summary>
        public void Retrieve()
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@AssignedUserId", this.UserId)
            };

            using (var reader = DataAccess.StoredProcedureHelper.ExecuteReader(this.BrokerId, "TASK_GetActiveTaskCount", parameters))
            {
                if (reader.Read())
                {
                    this.TaskCount = Convert.ToInt32(reader["TaskCount"]);
                    this.ConditionCount = Convert.ToInt32(reader["ConditionCount"]);
                }
            }
        }
    }
}
