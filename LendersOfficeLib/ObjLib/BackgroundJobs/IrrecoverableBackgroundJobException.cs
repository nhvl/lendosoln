﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;

    /// <summary>
    /// Represents an exception that occurred when processing
    /// a background job that is not recoverable and will 
    /// invalidate the job.
    /// </summary>
    public class IrrecoverableBackgroundJobException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IrrecoverableBackgroundJobException"/>
        /// class with the specified error message.
        /// </summary>
        /// <param name="message">
        /// The error message.
        /// </param>
        public IrrecoverableBackgroundJobException(string message)
            : base(message)
        {
        }
    }
}
