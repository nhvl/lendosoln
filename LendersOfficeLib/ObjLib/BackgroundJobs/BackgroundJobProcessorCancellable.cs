﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System.Diagnostics.CodeAnalysis;
    using System.Threading;
    using Common;
    using CommonProjectLib.Runnable;
    using DataAccess;

    /// <summary>
    /// Cancellable version of the background job processor.
    /// </summary>
    public class BackgroundJobProcessorCancellable : ICancellableRunnable
    {
        /// <summary>
        /// Gets the description of this IRunnable.
        /// </summary>
        /// <value>The description of this IRunnable.</value>
        public string Description
        {
            get
            {
                return "Pulls jobs from the BACKGROUND_JOBS database table to process.";
            }
        }

        /// <summary>
        /// Pulls a job that needs to be ran from the BACKGROUND_JOBS DB table and runs it.
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        public void Run(CancellationToken cancellationToken)
        {
            using (var workerTiming = new WorkerExecutionTiming("BackgroundJobProcessorCancellable"))
            {
                Tools.InitializeLargeStatics();

                BackgroundJobProcessor processor = new BackgroundJobProcessor();
                while (!cancellationToken.IsCancellationRequested)
                {
                    var ranJob = processor.RunProcessor(workerTiming);
                    if (!ranJob)
                    {
                        return;
                    }
                }
            }
        }
    }
}
