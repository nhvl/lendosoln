﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System.Collections.Generic;
    using LendersOffice.Conversions.LoanProspector;

    /// <summary>
    /// Wrapper class for results from an Seamless LPA polling job.
    /// </summary>
    public class SeamlessLpaPollJobResult : BackgroundJobResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SeamlessLpaPollJobResult"/> class.
        /// </summary>
        /// <param name="status">The status of the job.</param>
        /// <param name="jobId">The job id.</param>
        private SeamlessLpaPollJobResult(ProcessingStatus status, int jobId)
            : base(status, jobId)
        {
        }

        /// <summary>
        /// Gets the polling interval in seconds.
        /// </summary>
        /// <value>The polling interval in seconds.</value>
        public int PollingIntervalInSeconds
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the async id from Freddie.
        /// </summary>
        /// <value>The async id from Freddie.</value>
        public string PollAsyncId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the number of polling attempts done for the job.
        /// </summary>
        /// <value>The number of polling attempts done for the job.</value>
        public int PollAttemptCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the <see cref="LpaRequestResult"/> obtained once the job was completed.
        /// </summary>
        /// <value>The result once the job was completed.</value>
        public LpaRequestResult CompletedRequestResult
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets a list of errors if the job errored out.
        /// </summary>
        /// <value>The list of errors if the job errored out.</value>
        public List<string> InvalidErrorResult
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates a result wrapper for a job that has errored out.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <param name="errors">The list of errors.</param>
        /// <returns>The wrapper class for an errored job.</returns>
        public static SeamlessLpaPollJobResult CreateErrorResult(int jobId, List<string> errors)
        {
            return new SeamlessLpaPollJobResult(ProcessingStatus.Invalid, jobId)
            {
                InvalidErrorResult = errors
            };
        }

        /// <summary>
        /// Creates a result wrapper for a job that has successfully completed.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <param name="result">The request result from completing a job..</param>
        /// <returns>The wrapper class for a completed job.</returns>
        public static SeamlessLpaPollJobResult CreateCompletedResult(int jobId, LpaRequestResult result)
        {
            return new SeamlessLpaPollJobResult(ProcessingStatus.Completed, jobId)
            {
                CompletedRequestResult = result
            };
        }

        /// <summary>
        /// Creates a result wrapper for a job that is still being processed.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <param name="pollAsyncId">The async id from Freddie.</param>
        /// <param name="pollingAttemptCount">The attempt count.</param>
        /// <param name="pollingIntervalInSeconds">The polling interval in seconds.</param>
        /// <returns>The wrapper class for an incomplete job.</returns>
        public static SeamlessLpaPollJobResult CreateIncompleteResult(int jobId, string pollAsyncId, int pollingAttemptCount, int pollingIntervalInSeconds)
        {
            return new SeamlessLpaPollJobResult(ProcessingStatus.Incomplete, jobId)
            {
                PollAsyncId = pollAsyncId,
                PollAttemptCount = pollingAttemptCount,
                PollingIntervalInSeconds = pollingIntervalInSeconds
            };
        }
    }
}
