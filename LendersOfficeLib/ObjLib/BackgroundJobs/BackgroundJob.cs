﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using Adapter;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Abstract class for background jobs to be ran in the background job processor.
    /// </summary>
    public abstract class BackgroundJob
    {
        /// <summary>
        /// A filter string for use during developement. The filter is added to all instances of <see cref="BackgroundJob"/> where
        /// AddFilter returns true. The <see cref="BackgroundJobProcessor"/> will only retrieve those jobs that include this filter.
        /// </summary>
        /// <remarks>
        /// This field exists only for testing purposes. Overrides of this field should NEVER be committed.
        /// PLEASE REJECT ANY PRs WHERE THIS FIELD HAS BEEN OVERRIDDEN.
        /// </remarks>
        public const string DevJobFilter = null;

        /// <summary>
        /// The stored procedure to create an entry in the BACKGROUND_JOBS table.
        /// </summary>
        private static readonly StoredProcedureName BackgroundJobsCreateSp = StoredProcedureName.Create("BACKGROUND_JOBS_Create").Value;

        /// <summary>
        /// The stored procedure to edit an entry in the BACKGROUND_JOBS table.
        /// </summary>
        private static readonly StoredProcedureName BackgroundJobsEditJobSp = StoredProcedureName.Create("BACKGROUND_JOBS_EditJob").Value;

        /// <summary>
        /// Initializes a new instance of the <see cref="BackgroundJob"/> class. This will create a completely new job.
        /// </summary>
        /// <param name="nextRunDate">The next run date.</param>
        /// <param name="expirationDate">The expiration date, if there is one.</param>
        /// <param name="brokerId">The ID of the broker this job was generated for.</param>
        /// <param name="loanId">The ID of the loan this job was generated for.</param>
        /// <param name="userId">The ID of the user that initiated this job.</param>
        protected BackgroundJob(DateTime nextRunDate, DateTime expirationDate, Guid? brokerId, Guid? loanId, Guid? userId)
        {
            // New job. These shouldn't have IDs yet until they are saved to the DB. 
            this.IsNew = true;
            this.JobId = null;
            this.PublicJobId = null;
            this.ProcessingStatus = ProcessingStatus.Incomplete;
            this.NextRunDate = nextRunDate;
            this.ExpirationDate = expirationDate;
            this.DateCreated = DateTime.Now;
            this.LastRunStartDate = null;
            this.ExceptionCount = 0;
            this.BrokerId = brokerId;
            this.LoanId = loanId;
            this.UserId = userId;
            this.CorrelationId = Tools.GetLogCorrelationId();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BackgroundJob"/> class.
        /// </summary>
        /// <param name="reader">The reader to read from.</param>
        protected BackgroundJob(IDataReader reader)
        {
            this.JobId = (int)reader["JobId"];
            this.PublicJobId = (Guid)reader["PublicJobId"];
            this.ProcessingStatus = (ProcessingStatus)reader["ProcessingStatus"];
            this.NextRunDate = (DateTime)reader["NextRunDate"];
            this.ExpirationDate = (DateTime)reader["ExpirationDate"];
            this.DateCreated = (DateTime)reader["DateCreated"];
            this.MaxExceptionCount = reader.GetNullableInt("MaxExceptionCount");
            this.ExceptionCount = (int)reader["ExceptionCount"];
            this.DateCompleted = reader.AsNullableDateTime("DateCompleted");
            this.BrokerId = reader.AsNullableGuid("BrokerId");
            this.LoanId = reader.AsNullableGuid("LoanId");
            this.CorrelationId = reader.AsNullableGuid("CorrelationId");
            this.UserId = reader.AsNullableGuid("UserId");

            var lastRunStartDate = reader["LastRunStartDate"];
            this.LastRunStartDate = lastRunStartDate == DBNull.Value ? (DateTime?)null : (DateTime)lastRunStartDate;
        }

        /// <summary>
        /// Gets the job id.
        /// </summary>
        /// <value>The job id.</value>
        public int? JobId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the job id that can be exposed to clients.
        /// </summary>
        /// <value>The public job id.</value>
        public Guid? PublicJobId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the processing status.
        /// </summary>
        /// <value>The processing status.</value>
        public ProcessingStatus ProcessingStatus
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the job type.
        /// </summary>
        /// <value>The job type.</value>
        public abstract JobType JobType
        {
            get;
        }

        /// <summary>
        /// Gets the date the job was created.
        /// </summary>
        /// <value>The date the job was created.</value>
        public DateTime DateCreated
        {
            get;
        }

        /// <summary>
        /// Gets or sets the next date to run the job.
        /// </summary>
        /// <value>The next run date.</value>
        public DateTime NextRunDate
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the start date of the last time this job was ran.
        /// </summary>
        /// <value>The start date the last time this job was ran.</value>
        /// <remarks>
        /// Note that this only gets updated when a job is successfully pulled from the queue.
        /// </remarks>
        public DateTime? LastRunStartDate
        {
            get;
        }

        /// <summary>
        /// Gets the date the job was completed.
        /// </summary>
        public DateTime? DateCompleted
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the expected run time for the job. Will be used to determine if a job in limbo can be retried. 
        /// This can be null if you don't want to retry the job if it has reached a limbo state.
        /// </summary>
        /// <value>The expected run time for the job.</value>
        public abstract int? ExpectedRunTimeInSeconds
        {
            get;
        }

        /// <summary>
        /// Gets a value indicating whether this job can be retried if has been stuck in the <see cref="ProcessingStatus.Processing"/> limbo status
        /// for longer than its expected run time.
        /// </summary>
        /// <value>Whether the job can be retried if in limbo status.</value>
        /// <remarks>
        /// This is meant for instances where Continuous is killed in the middle of a job.
        /// </remarks>
        public abstract bool CanRetryIfInLimbo
        {
            get;
        }

        /// <summary>
        /// Gets or sets the expiration date of the job.
        /// </summary>
        /// <value>The expiration date of the job.</value>
        public DateTime ExpirationDate
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the maximum number of times a job can run into an exception before giving up.
        /// </summary>
        /// <remarks>
        /// If this exists, the Background Job Processor will use this and <see cref="ExpirationDate"/> to determine if we can put a failed job back into the queue.
        /// </remarks>
        public virtual int? MaxExceptionCount
        {
            get;
        }

        /// <summary>
        /// Gets or sets the number of times this job has run into uncaught exceptions.
        /// </summary>
        /// <remarks>
        /// This will increment everytime the job runs into an uncaught exception.
        /// Will be used in conjunction with <see cref="ExpirationDate"/> and <see cref="MaxExceptionCount"/>, if it exists, to invalidate jobs that keep erroring out.
        /// </remarks>
        public int ExceptionCount
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets a string that contains the job type, id, and public job id. 
        /// Commonly used for logging purposes.
        /// </summary>
        public string IdentifierString
        {
            get
            {
                return $"{this.JobType.ToString()}[ID:{this.JobId} PublicJobID:{this.PublicJobId}]";
            }
        }

        /// <summary>
        /// Gets the Broker ID for the broker this job was created for.
        /// </summary>
        /// <remarks>
        /// Used for logging purposes.
        /// Note: This isn't nullable. System jobs should use the system broker ID.
        /// </remarks>
        public Guid? BrokerId { get; }

        /// <summary>
        /// Gets the Loan ID for the loan this job was created for.
        /// </summary>
        /// <remarks>
        /// Used for logging purposes.
        /// </remarks>
        public Guid? LoanId { get; }

        /// <summary>
        /// Gets the Correlation ID used by the logging framework to link related logs.
        /// </summary>
        /// <remarks>
        /// Used for logging purposes.
        /// </remarks>
        public Guid? CorrelationId { get; }

        /// <summary>
        /// Gets the User ID of the user that initiated this job.
        /// </summary>
        /// <remarks>
        /// Used for logging purposes.
        /// </remarks>
        public Guid? UserId { get; }

        /// <summary>
        /// Gets a value indicating whether we want to add the dev filter to this Job on creation.
        /// </summary>
        /// <remarks>
        /// This field exists only for testing purposes. Overrides of this field should NEVER be committed.
        /// PLEASE REJECT ANY PRs WHERE THIS FIELD HAS BEEN OVERRIDDEN.
        /// </remarks>
        protected virtual bool AddDevFilter { get; } = false;

        /// <summary>
        /// Gets or sets a value indicating whether or not this background job is new.
        /// </summary>
        /// <value>Whether the background job is new.</value>
        protected bool IsNew
        {
            get;
            set;
        }

        /// <summary>
        /// Runs the job. Will set <see cref="DateCompleted"/> if the job is marked as <see cref="ProcessingStatus.Completed"/> or <see cref="ProcessingStatus.Invalid"/>.
        /// </summary>
        public void RunJob()
        {
            this.ProcessingStatus = this.PerformAndUpdateJob();
            if (this.ProcessingStatus == ProcessingStatus.Completed || this.ProcessingStatus == ProcessingStatus.Invalid)
            {
                this.DateCompleted = DateTime.Now;
            }
            else if (this.ProcessingStatus == ProcessingStatus.Incomplete)
            {
                // Job is going back into the queue. Determine the next run date
                this.NextRunDate = this.DetermineNextRunDate(DateTime.Now);
            }

            this.SaveToDb();
            this.PostSaveActions();
        }

        /// <summary>
        /// Determines the next run date for the job.
        /// </summary>
        /// <param name="startTime">The start time to use when determining the next run date.</param>
        /// <returns>The next run date.</returns>
        public abstract DateTime DetermineNextRunDate(DateTime startTime);

        /// <summary>
        /// Increments the exception count.
        /// </summary>
        public void IncrementExceptionCounter()
        {
            this.ExceptionCount++;
        }

        /// <summary>
        /// Saves the job to the DB.
        /// </summary>
        protected void SaveToDb()
        {
            using (DbConnection conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            {
                conn.OpenWithRetry();

                using (DbTransaction transaction = conn.BeginTransaction())
                {
                    try
                    {
                        this.SaveBasicJobInfo(conn, transaction);
                        this.SaveSpecificJobInfo(conn, transaction);
                        transaction.Commit();
                        this.IsNew = false;
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        Tools.LogError(e);
                        throw;
                    }
                }
            }
        }

        /// <summary>
        /// Runs any actions after the full save. Examples might be deleting a FileDb entry.
        /// </summary>
        protected virtual void PostSaveActions()
        {
            // No-op for base implementation.
        }

        /// <summary>
        /// Performs whatever actions the job needs. This should also update whatever it needs for saving later.
        /// </summary>
        /// <returns>
        /// The processing status after performing the job.
        /// </returns>
        /// <remarks>
        /// Any exceptions that get thrown from this method will bubble up to the job processor. The processor will put it back in the queue if it hasn't expired
        /// or mark it as invalid if it has. 
        /// If you don't want this behavior when an exception is thrown, make sure to handle it properly in this method and don't rethrow it.
        /// </remarks>
        protected abstract ProcessingStatus PerformAndUpdateJob();

        /// <summary>
        /// Saves the specific job info to the DB.
        /// </summary>
        /// <param name="conn">The DB connection..</param>
        /// <param name="transaction">The DB transaction.</param>
        protected abstract void SaveSpecificJobInfo(DbConnection conn, DbTransaction transaction);

        /// <summary>
        /// Saves the basic job info to the BACKGROUND_JOBS table.
        /// </summary>
        /// <param name="conn">The DB connection.</param>
        /// <param name="transaction">The DB transaction.</param>
        private void SaveBasicJobInfo(DbConnection conn, DbTransaction transaction)
        {
            StoredProcedureName storedProcedureName;

            var parameters = new List<SqlParameter>();
            parameters.Add(new SqlParameter("@ProcessingStatus", this.ProcessingStatus));
            parameters.Add(new SqlParameter("@NextRunDate", this.NextRunDate));

            SqlParameter idParameter;
            if (this.IsNew)
            {
                // New job. We should assign it a public job id now.
                this.PublicJobId = Guid.NewGuid();

                parameters.Add(new SqlParameter("@JobType", this.JobType));
                parameters.Add(new SqlParameter("@PublicJobId", this.PublicJobId.Value));
                parameters.Add(new SqlParameter("@ExpirationDate", this.ExpirationDate));
                parameters.Add(new SqlParameter("@DateCreated", this.DateCreated));
                parameters.Add(new SqlParameter("@LastRunStartDate", this.LastRunStartDate));
                parameters.Add(new SqlParameter("@ExpectedRunTimeInSeconds", this.ExpectedRunTimeInSeconds));
                parameters.Add(new SqlParameter("@CanRetryIfInLimbo", this.CanRetryIfInLimbo));
                parameters.Add(new SqlParameter("@MaxExceptionCount", this.MaxExceptionCount));
                parameters.Add(new SqlParameter("@ExceptionCount", this.ExceptionCount));
                parameters.Add(new SqlParameter("@BrokerId", this.BrokerId));
                parameters.Add(new SqlParameter("@LoanId", this.LoanId));
                parameters.Add(new SqlParameter("@CorrelationId", this.CorrelationId));
                parameters.Add(new SqlParameter("@UserId", this.UserId));

                // OPM 474898 - Dev job filter should only be added to jobs created/run on dev environment.
                if (ConstAppDavid.CurrentServerLocation.EqualsOneOf(ServerLocation.Development, ServerLocation.LocalHost)
                    && !string.IsNullOrEmpty(DevJobFilter) && this.AddDevFilter)
                {
                    parameters.Add(new SqlParameter("@Filter", BackgroundJob.DevJobFilter));
                }

                idParameter = new SqlParameter("@JobId", SqlDbType.Int);
                idParameter.Direction = ParameterDirection.Output;

                storedProcedureName = BackgroundJobsCreateSp;
            }
            else
            {
                idParameter = new SqlParameter("@JobId", this.JobId.Value);
                parameters.Add(new SqlParameter("@DateCompleted", this.DateCompleted));
                storedProcedureName = BackgroundJobsEditJobSp;
            }

            parameters.Add(idParameter);

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, storedProcedureName, parameters, TimeoutInSeconds.Thirty);

            if (this.IsNew)
            {
                this.JobId = (int)idParameter.Value;
            }
        }
    }
}