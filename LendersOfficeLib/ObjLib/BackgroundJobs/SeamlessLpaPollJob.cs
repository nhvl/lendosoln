﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using Common;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LendersOffice.Conversions.LoanProspector;
    using LendersOffice.ObjLib.Conversions.Aus;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// The background job to poll for Seamless LPA.
    /// </summary>
    public class SeamlessLpaPollJob : BackgroundJob
    {
        /// <summary>
        /// SP to add a Seamless LPA Polling job to the SEAMLESS_LPA_POLL_JOBS table.
        /// </summary>
        private static readonly StoredProcedureName SeamlessLpaPollJobsCreateSp = StoredProcedureName.Create("SEAMLESS_LPA_POLL_JOBS_Create").Value;

        /// <summary>
        /// SP to edit an existing job in the SEAMLESS_LPA_POLL_JOBS table.
        /// </summary>
        private static readonly StoredProcedureName SeamlessLpaPollJobsEditSp = StoredProcedureName.Create("SEAMLESS_LPA_POLL_JOBS_Edit").Value;

        /// <summary>
        /// SP to get an existing job in the SEAMLESS_LPA_POLL_JOBS table.
        /// </summary>
        private static readonly StoredProcedureName SeamlessLpaPollJobsGetJobByJobIdSp = StoredProcedureName.Create("SEAMLESS_LPA_POLL_JOBS_GetJobByJobId").Value;

        /// <summary>
        /// SP to get an existing job in the SEAMLESS_LPA_POLL_JOBS table using the public id.
        /// </summary>
        private static readonly StoredProcedureName SeamlessLpaPollJobsGetJobByPublicIdSp = StoredProcedureName.Create("SEAMLESS_LPA_POLL_JOBS_GetJobByPublicId").Value;

        /// <summary>
        /// The guid portion of the results file db key.
        /// </summary>
        private Guid? resultsFileDbKey = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="SeamlessLpaPollJob"/> class. For new jobs.
        /// </summary>
        /// <param name="nextRunDate">The next run date.</param>
        /// <param name="expirationDate">The expiration date for the job.</param>
        /// <param name="brokerId">The ID of the broker this job was generated for.</param>
        /// <param name="loanId">The ID of the loan this job is for.</param>
        /// <param name="userId">The ID of the user that initiated this job.</param>
        private SeamlessLpaPollJob(DateTime nextRunDate, DateTime expirationDate, Guid brokerId, Guid loanId, Guid userId)
            : base(nextRunDate, expirationDate, brokerId, loanId, userId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SeamlessLpaPollJob"/> class.
        /// </summary>
        /// <param name="reader">The reader to read from.</param>
        private SeamlessLpaPollJob(IDataReader reader)
            : base(reader)
        {
            this.PollAsyncId = (string)reader["PollAsyncId"];
            this.PollingIntervalInSeconds = (int)reader["PollingIntervalInSeconds"];
            this.PollingAttemptCount = (int)reader["PollingAttemptCount"];
            this.UserType = (string)reader["UserType"];
            this.LpaUsername = (string)reader["LpaUsername"];
            this.LpaUserPassword = EncryptionHelper.DecryptLpaUserPassword((byte[])reader["LpaUserPassword"]);
            this.LpaSellerNumber = (string)reader["LpaSellerNumber"];
            this.LpaSellerPassword = EncryptionHelper.DecryptLpaUserPassword((byte[])reader["LpaSellerPassword"]);
            this.resultsFileDbKey = reader["ResultFileDbKey"] == DBNull.Value ? (Guid?)null : (Guid)reader["ResultFileDbKey"];
            this.CreditReportType = reader.GetNullableIntEnum<CreditReportType>("CreditReportType");
        }

        /// <summary>
        /// Gets the async id sent back by Freddie.
        /// </summary>
        /// <value>The async id.</value>
        public string PollAsyncId
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the polling attempt counter. Stops polling after a certain amount.
        /// </summary>
        /// <value>The retry counter.</value>
        public int PollingAttemptCount
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the polling interval in seconds.
        /// </summary>
        /// <value>The polling interval in seconds.</value>
        public int PollingIntervalInSeconds
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the user type of the user who did the initial submit.
        /// </summary>
        /// <value>The user type of the user who did the intial submit.</value>
        public string UserType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the LPA username used in the initial submit.
        /// </summary>
        /// <value>The LPA username used in the initial submit.</value>
        public string LpaUsername
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the LPA user password used in the initial submit.
        /// </summary>
        /// <value>The LPA user password used in the initial submit.</value>
        public string LpaUserPassword
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the LPA seller number.
        /// </summary>
        /// <value>The LPA seller number.</value>
        public string LpaSellerNumber
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the LPA seller password.
        /// </summary>
        /// <value>The LPA seller password.</value>
        public string LpaSellerPassword
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the credit report type, if this was created from a submission request.
        /// </summary>
        public CreditReportType? CreditReportType
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the job type.
        /// </summary>
        /// <value>The job type.</value>
        public override JobType JobType
        {
            get
            {
                return JobType.SeamlessLpaPolling;
            }
        }

        /// <summary>
        /// Gets the time in seconds we expect this job to run.
        /// </summary>
        /// <value>The time in seconds we expect this job to run.</value>
        public override int? ExpectedRunTimeInSeconds
        {
            get
            {
                // Give it 5 minutes.
                return 5 * 60;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this job can be retried if has been stuck in the <see cref="ProcessingStatus.Processing"/> limbo status
        /// for longer than its expected run time. 
        /// </summary>
        /// <value>Whether the job can be retried if in limbo status.</value>
        public override bool CanRetryIfInLimbo
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Creates an Seamless LPA Poll job and adds it to the DB queue.
        /// </summary>
        /// <param name="asyncId">The async id sent back by the initial submit.</param>
        /// <param name="pollingIntervalSeconds">The polling interval in seconds.</param>
        /// <param name="requestData">The request data used in the Seamless LPA submission.</param>
        /// <returns>The public job id of the created job.</returns>
        public static Guid AddSeamlessLpaPollJob(string asyncId, int pollingIntervalSeconds, LpaRequestData requestData)
        {
            var now = DateTime.Now;
            DateTime nextRunDate = now.AddSeconds(pollingIntervalSeconds);
            DateTime expirationDate = now.AddSeconds(pollingIntervalSeconds * Constants.ConstStage.SeamlessLpaPollingRetryLimit).AddHours(12);
            SeamlessLpaPollJob job = new SeamlessLpaPollJob(nextRunDate, expirationDate, requestData.Principal.BrokerId, requestData.LoanId, requestData.Principal.UserId);
            job.PollAsyncId = asyncId;
            job.UserType = requestData.Principal.Type;
            job.LpaUsername = requestData.Username;
            job.LpaUserPassword = requestData.Password;
            job.LpaSellerNumber = requestData.LpaSellerNumber;
            job.LpaSellerPassword = requestData.LpaSellerPassword;
            job.PollingIntervalInSeconds = pollingIntervalSeconds;
            job.PollingAttemptCount = 0;
            job.CreditReportType = (requestData as LpaSubmitRequestData)?.CraInfo?.CreditReportOption;

            job.SaveToDb();
            return job.PublicJobId.Value;
        }

        /// <summary>
        /// Checks on the Seamless LPA polling job and retrieves the results.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <returns>The job results. Null if no job found. ProcessingStatus is Invalid if an error occured, Complete if the job was completed, Incomplete if the job is still being processed.</returns>
        public static SeamlessLpaPollJobResult Check(Guid publicJobId)
        {
            SeamlessLpaPollJob job = GetSeamlessLpaPollJob(publicJobId);

            if (job == null)
            {
                Tools.LogError($"Unable to find job with id {publicJobId}. May be expired.");
                return null;
            }

            switch (job.ProcessingStatus)
            {
                case ProcessingStatus.Invalid:
                    var errors = FileDBTools.ReadJsonNetObject<List<string>>(E_FileDB.Normal, GetFullFileDbKey(job.resultsFileDbKey.Value));
                    return SeamlessLpaPollJobResult.CreateErrorResult(job.JobId.Value, errors);
                case ProcessingStatus.Completed:
                    var requestResults = FileDBTools.ReadJsonNetObjectWithTypeHandling<LpaRequestResult>(E_FileDB.Normal, GetFullFileDbKey(job.resultsFileDbKey.Value));
                    return SeamlessLpaPollJobResult.CreateCompletedResult(job.JobId.Value, requestResults);
                case ProcessingStatus.Processing:
                    return SeamlessLpaPollJobResult.CreateIncompleteResult(job.JobId.Value, job.PollAsyncId, job.PollingAttemptCount, job.PollingIntervalInSeconds);
                case ProcessingStatus.Incomplete:
                    if (DateTime.Now > job.ExpirationDate)
                    {
                        return SeamlessLpaPollJobResult.CreateErrorResult(job.JobId.Value, new List<string>() { "Unable to retrieve results. Polling period has expired." });
                    }
                    else
                    {
                        return SeamlessLpaPollJobResult.CreateIncompleteResult(job.JobId.Value, job.PollAsyncId, job.PollingAttemptCount, job.PollingIntervalInSeconds);
                    }

                default:
                    throw new UnhandledEnumException(job.ProcessingStatus);
            }
        }

        /// <summary>
        /// Gets the Seamless LPA Poll job from the DB.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <returns>The seamless LPA poll job. Returns null if not found.</returns>
        public static SeamlessLpaPollJob GetSeamlessLpaPollJob(int jobId)
        {
            SqlParameter[] paramters = new SqlParameter[]
            {
                new SqlParameter("@JobId", jobId)
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, SeamlessLpaPollJobsGetJobByJobIdSp, paramters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new SeamlessLpaPollJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the Seamless LPA Poll job from the DB using a transaction and public job id.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <returns>The seamless LPA poll job. Returns null if not found.</returns>
        public static SeamlessLpaPollJob GetSeamlessLpaPollJob(Guid publicJobId)
        {
            SqlParameter[] paramters = new SqlParameter[]
            {
                new SqlParameter("@PublicJobId", publicJobId)
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, SeamlessLpaPollJobsGetJobByPublicIdSp, paramters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new SeamlessLpaPollJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Determines the next run date for the job.
        /// </summary>
        /// <param name="startTime">The start time to use when determining the next run date.</param>
        /// <returns>The next run date.</returns>
        public override DateTime DetermineNextRunDate(DateTime startTime)
        {
            return startTime.AddSeconds(this.PollingIntervalInSeconds);
        }

        /// <summary>
        /// Sends a poll request to Freddie and then saves the response to FileDb for later retrieval.
        /// </summary>
        /// <returns>The status of the job after it has completed running.</returns>
        protected override ProcessingStatus PerformAndUpdateJob()
        {
            Guid fileDbKey = Guid.NewGuid();
            string fullFileDbKey = GetFullFileDbKey(fileDbKey);
            this.resultsFileDbKey = fileDbKey;

            var principal = PrincipalFactory.Create(this.BrokerId.Value, this.UserId.Value, this.UserType, allowDuplicateLogin: true, isStoreToCookie: false);
            if (principal == null)
            {
                FileDBTools.WriteJsonNetObject(E_FileDB.Normal, fullFileDbKey, new List<string>() { ErrorMessages.Generic });
                Tools.LogError($"Invalid principal. JobId: {this.JobId.Value} BrokerId: {this.BrokerId} UserId: {this.UserId} UserType: {this.UserType}.");
                return ProcessingStatus.Invalid;
            }

            var requestData = new LpaPollRequestData(this.LpaUsername, this.LpaUserPassword, this.LpaSellerNumber, this.LpaSellerPassword, this.LoanId.Value, principal, this.PollAsyncId, this.PollingAttemptCount, this.PublicJobId.Value, this.CreditReportType);
            string error;
            if (!requestData.Validate(out error))
            {
                FileDBTools.WriteJsonNetObject(E_FileDB.Normal, fullFileDbKey, new List<string>() { ErrorMessages.Generic });
                Tools.LogError($"Bad request data. JobId: {this.JobId.Value} LpaUsername: {this.LpaUsername} LoanId: {this.LoanId} PollAsyncId: {this.PollAsyncId}");
                return ProcessingStatus.Invalid;
            }

            LpaRequestHandler requestHandler = new LpaRequestHandler(requestData);
            var results = requestHandler.SubmitRequest();
            if (results is LpaRequestPartialResult)
            {
                var partialResults = results as LpaRequestPartialResult;

                // Another polling result. Update the date and polling interval in case it has changed.
                this.PollingIntervalInSeconds = partialResults.PollingInterval;
                this.PollAsyncId = partialResults.AsyncId;
                this.PollingAttemptCount++;

                this.resultsFileDbKey = null;
                return ProcessingStatus.Incomplete;
            }
            else
            {
                FileDBTools.WriteJsonNetObjectWithTypeHandling(E_FileDB.Normal, fullFileDbKey, results);
                return ProcessingStatus.Completed;
            }
        }

        /// <summary>
        /// Saves the Seamless LPA Poll job specific info to the DB.
        /// </summary>
        /// <param name="conn">The DB connection.</param>
        /// <param name="transaction">The DB transaction.</param>
        protected override void SaveSpecificJobInfo(DbConnection conn, DbTransaction transaction)
        {
            SqlParameter[] parameters;
            StoredProcedureName storedProcedureName;
            if (this.IsNew)
            {
                parameters = new SqlParameter[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@PollAsyncId", this.PollAsyncId),
                    new SqlParameter("@PollingIntervalInSeconds", this.PollingIntervalInSeconds),
                    new SqlParameter("@PollingAttemptCount", this.PollingAttemptCount),
                    new SqlParameter("@LoanId", this.LoanId),
                    new SqlParameter("@BrokerId", this.BrokerId),
                    new SqlParameter("@UserId", this.UserId),
                    new SqlParameter("@UserType", this.UserType),
                    new SqlParameter("@LpaUsername", this.LpaUsername),
                    new SqlParameter("@LpaUserPassword", EncryptionHelper.EncryptLpaUserPassword(this.LpaUserPassword)),
                    new SqlParameter("@LpaSellerNumber", this.LpaSellerNumber),
                    new SqlParameter("@LpaSellerPassword", EncryptionHelper.EncryptLpaUserPassword(this.LpaSellerPassword)),
                    new SqlParameter("@CreditReportType", this.CreditReportType)
                };

                storedProcedureName = SeamlessLpaPollJobsCreateSp;
            }
            else
            {
                parameters = new SqlParameter[]
                {
                    new SqlParameter("@ResultFileDbKey", this.resultsFileDbKey),
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@PollingAttemptCount", this.PollingAttemptCount),
                    new SqlParameter("@PollingIntervalInSeconds", this.PollingIntervalInSeconds)
                };

                storedProcedureName = SeamlessLpaPollJobsEditSp;
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, storedProcedureName, parameters, TimeoutInSeconds.Thirty);
        }

        /// <summary>
        /// Constructs the full file db key for the results.
        /// </summary>
        /// <param name="fileDbKey">The GUID portion of the file db key.</param>
        /// <returns>The full file DB key.</returns>
        private static string GetFullFileDbKey(Guid fileDbKey)
        {
            return $"{fileDbKey.ToString("N")}_SeamlessLpaPollJobResult";
        }
    }
}
