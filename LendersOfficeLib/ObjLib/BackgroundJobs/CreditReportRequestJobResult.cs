﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.CreditReport;
    using LendersOffice.Security;

    /// <summary>
    /// Class holding results from a Credit Report Request job.
    /// </summary>
    public class CreditReportRequestJobResult : BackgroundJobResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportRequestJobResult"/> class.
        /// </summary>
        /// <param name="status">The job status.</param>
        /// <param name="jobId">The job id.</param>
        private CreditReportRequestJobResult(ProcessingStatus status, int jobId)
            : base(status, jobId)
        {
        }

        /// <summary>
        /// Gets the credit request data used for the job.
        /// </summary>
        public CreditRequestData CreditRequestData
        {
            get; private set;
        }

        /// <summary>
        /// Gets the raw credit report xml response.
        /// </summary>
        public string RawResponse
        {
            get; private set;
        }

        /// <summary>
        /// Gets the principal used for the job.
        /// </summary>
        public AbstractUserPrincipal Principal
        {
            get; private set;
        }

        /// <summary>
        /// Gets the error messages produced while running the job.
        /// </summary>
        public IEnumerable<string> ErrorMessages
        {
            get; private set;
        }

        /// <summary>
        /// Creates results based on the job.
        /// </summary>
        /// <param name="job">The job to create results on.</param>
        /// <returns>The job result object.</returns>
        public static CreditReportRequestJobResult CreateResult(CreditReportRequestJob job)
        {
            var status = job.ProcessingStatus;
            var result = new CreditReportRequestJobResult(status, job.JobId.Value);
            switch (status)
            {
                case ProcessingStatus.Invalid:
                    result.ErrorMessages = job.GetResultErrorMessages() ?? new List<string>() { LqbGrammar.DataTypes.ErrorMessage.SystemError.ToString() };
                    break;
                case ProcessingStatus.Incomplete:
                    if (DateTime.Now > job.ExpirationDate)
                    {
                        result = new CreditReportRequestJobResult(ProcessingStatus.Invalid, job.JobId.Value)
                        {
                            ErrorMessages = new List<string>() { "Credit Report request has timed out." }
                        };
                    }

                    break;
                case ProcessingStatus.Processing:
                    // No need to do anything for this.
                    break;
                case ProcessingStatus.Completed:
                    result.CreditRequestData = job.CreditRequestData.Value;
                    result.RawResponse = job.GetResultRawCreditReportResponse();
                    result.Principal = job.Principal.Value;
                    break;
                default:
                    throw new UnhandledEnumException(status);
            }

            return result;
        }
    }
}
