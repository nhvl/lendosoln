﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    /// <summary>
    /// The generic processing status for each job in the background job processor.
    /// </summary>
    public enum ProcessingStatus
    {
        /// <summary>
        /// Job is invalid and could not be ran.
        /// </summary>
        Invalid = 0,

        /// <summary>
        /// The job has not been picked up yet.
        /// </summary>
        Incomplete = 1,

        /// <summary>
        /// The job is being processed by a runner.
        /// </summary>
        Processing = 2,

        /// <summary>
        /// The job has been completed.
        /// </summary>
        Completed = 3
    }
}
