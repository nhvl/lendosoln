﻿namespace LendersOffice.ObjLib.BackgroundJobs.VOA
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class holding the results for a VOA job.
    /// </summary>
    public class VoaJobResult : BackgroundJobResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="VoaJobResult"/> class.
        /// </summary>
        /// <param name="status">The status of the job.</param>
        /// <param name="jobId">The job id.</param>
        public VoaJobResult(ProcessingStatus status, int jobId)
            : base(status, jobId)
        {
        }

        /// <summary>
        /// Gets or sets the order ids of the orders made.
        /// </summary>
        public IEnumerable<int> OrderIds
        {
            get; set;
        }

        /// <summary>
        /// Gets or sets the errors encountered during the request.
        /// </summary>
        public IEnumerable<string> Errors
        {
            get; set;
        }
    }
}
