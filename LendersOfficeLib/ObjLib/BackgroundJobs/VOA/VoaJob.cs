﻿namespace LendersOffice.ObjLib.BackgroundJobs.VOA
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Linq;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Integration.VOXFramework;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Job for handling VOA requests.
    /// </summary>
    public class VOAJob : BackgroundJob
    {
        /// <summary>
        /// The raw request data. This will be basic data that will be used to reconstruct the request data.
        /// </summary>
        private Lazy<VOAJobRequestData> jobRequestData;

        /// <summary>
        /// The guid portion of the FileDB key that holds the serialized request data.
        /// </summary>
        private Guid? jobRequestDataFileDbGuidId;

        /// <summary>
        /// The guid portion of the response FileDb key.
        /// </summary>
        private Guid? responseFileDbGuidId;

        /// <summary>
        /// The retry interval in seconds.
        /// </summary>
        private int retryIntervalInSeconds;

        /// <summary>
        /// Initializes a new instance of the <see cref="VOAJob"/> class.
        /// </summary>
        /// <param name="nextRunDate">The next run date.</param>
        /// <param name="expirationDate">The expiration date.</param>
        /// <param name="requestData">The actual request data.</param>
        /// <param name="retryIntervalInSeconds">The retry interval in seconds.</param>
        private VOAJob(DateTime nextRunDate, DateTime expirationDate, VOARequestData requestData, int retryIntervalInSeconds)
            : base(nextRunDate, expirationDate, requestData.Principal.BrokerId, requestData.LoanId, requestData.Principal.UserId)
        {
            var convertedData = this.ToJobRequestData(requestData);
            this.jobRequestData = new Lazy<VOAJobRequestData>(() => convertedData);
            this.retryIntervalInSeconds = retryIntervalInSeconds;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VOAJob"/> class.
        /// </summary>
        /// <param name="reader">The reader.</param>
        private VOAJob(IDataReader reader)
            : base(reader)
        {
            this.retryIntervalInSeconds = (int)reader["RetryIntervalInSeconds"];
            this.responseFileDbGuidId = reader.AsNullableGuid("ResponseFileDbGuidId");
            this.jobRequestDataFileDbGuidId = reader.AsNullableGuid("JobRequestDataFileDbGuidId");
            this.jobRequestData = new Lazy<VOAJobRequestData>(() =>
            {
                if (!this.jobRequestDataFileDbGuidId.HasValue)
                {
                    return null;
                }

                return this.GetJobRequestData(this.jobRequestDataFileDbGuidId.Value);
            });
        }

        /// <summary>
        /// Gets the job type.
        /// </summary>
        public override JobType JobType => JobType.VoaJob;

        /// <summary>
        /// Gets the expected run time in seconds.
        /// </summary>
        public override int? ExpectedRunTimeInSeconds => 5 * 60;

        /// <summary>
        /// Gets a value indicating whether this job can be tried if it falls into limbo.
        /// </summary>
        public override bool CanRetryIfInLimbo => false;

        /// <summary>
        /// Adds a VOA job to the queue.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        /// <param name="pollingIntervalSeconds">The polling interval the caller will use to poll for job status.</param>
        /// <param name="maxPollingAttempts">The max attempts the caller will use to poll for job status.</param>
        /// <returns>The public job id.</returns>
        public static Guid AddJob(VOARequestData requestData, int pollingIntervalSeconds, int maxPollingAttempts)
        {
            var nextRunDate = DateTime.Now;
            var expirationDate = nextRunDate.AddSeconds(pollingIntervalSeconds * maxPollingAttempts).AddMinutes(10);

            var job = new VOAJob(nextRunDate, expirationDate, requestData, pollingIntervalSeconds);
            job.SaveToDb();

            return job.PublicJobId.Value;
        }

        /// <summary>
        /// Checks the VOA job and returns appropriate job results.
        /// </summary>
        /// <param name="publicJobId">The public id of the job.</param>
        /// <returns>The job results.</returns>
        public static VoaJobResult CheckJob(Guid publicJobId)
        {
            var job = GetJob(publicJobId);
            return job.ToJobResult();
        }

        /// <summary>
        /// Determines the next run date if this job needs to go back to the queue.
        /// </summary>
        /// <param name="startTime">The time to start calculating the next run date from.</param>
        /// <returns>The next run date.</returns>
        public override DateTime DetermineNextRunDate(DateTime startTime) => startTime.AddSeconds(this.retryIntervalInSeconds);

        /// <summary>
        /// Gets the VOA job.
        /// </summary>
        /// <param name="id">The job id.</param>
        /// <returns>The VOA job. Null if not found.</returns>
        internal static VOAJob GetJob(int id)
        {
            return GetJob(new SqlParameter("@JobId", id));
        }

        /// <summary>
        /// Perform and update the job.
        /// </summary>
        /// <returns>The status of the job after processing attempt.</returns>
        protected override ProcessingStatus PerformAndUpdateJob()
        {
            var currentPrincipal = System.Threading.Thread.CurrentPrincipal;
            var userPrincipal = PrincipalFactory.RetrievePrincipalForUser(this.jobRequestData.Value.BrokerId, this.jobRequestData.Value.UserId, this.jobRequestData.Value.UserType);
            var requestData = this.ToRequestData(this.jobRequestData.Value, userPrincipal);
            string error;
            if (!requestData.ValidateRequestData(out error))
            {
                this.responseFileDbGuidId = this.SaveErrorResponse(new List<string>() { error });
                return ProcessingStatus.Invalid;
            }

            VOARequestHandler requestHandler = new VOARequestHandler(requestData);
            try
            {
                VOXRequestResults result = null;
                using (var principalHandler = new TemporaryPrincipalUseHelper(currentPrincipal, userPrincipal))
                {
                    result = requestHandler.SubmitRequest();
                }

                if (result?.Errors != null && result.Errors.Any())
                {
                    this.responseFileDbGuidId = this.SaveErrorResponse(result.Errors.ToList());
                    return ProcessingStatus.Invalid;
                }

                if (result?.SubsequentRequestData != null)
                {
                    this.jobRequestDataFileDbGuidId = this.SaveJobRequestData(this.ToJobRequestData(result.SubsequentRequestData as VOARequestData));
                    return ProcessingStatus.Incomplete;
                }

                var orders = result?.Orders;
                List<int> orderIds = orders.Select((order) => order.OrderId).ToList();
                this.responseFileDbGuidId = this.SaveOrderIds(orderIds);
                return ProcessingStatus.Completed;
            }
            catch (CBaseException exc)
            {
                // Going to err on the side of caution and not bother retrying on failure since there is a payment attached to this order.
                Tools.LogError($"[VoaJob] JobId: {this.JobId}", exc);
                this.responseFileDbGuidId = this.SaveErrorResponse(new List<string>() { exc.UserMessage });
                return ProcessingStatus.Invalid;
            }
        }

        /// <summary>
        /// Saves the job info specific to this VOA job.
        /// </summary>
        /// <param name="conn">The DB connection info.</param>
        /// <param name="transaction">The DB transaction.</param>
        protected override void SaveSpecificJobInfo(DbConnection conn, DbTransaction transaction)
        {
            StoredProcedureName sp;
            SqlParameter[] parameters = null;
            if (this.IsNew)
            {
                this.jobRequestDataFileDbGuidId = this.SaveJobRequestData(this.jobRequestData.Value);

                parameters = new SqlParameter[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@JobRequestDataFileDbGuidId", this.jobRequestDataFileDbGuidId.Value),
                    new SqlParameter("@RetryIntervalInSeconds", this.retryIntervalInSeconds)
                };

                sp = StoredProcedureName.Create("VOA_JOBS_Create").GetValueOrDefault();
            }
            else
            {
                parameters = new SqlParameter[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@ResponseFileDbGuidId", this.responseFileDbGuidId.HasValue ? this.responseFileDbGuidId.Value : (Guid?)null),
                    new SqlParameter("@JobRequestDataFileDbGuidId", this.jobRequestDataFileDbGuidId.Value)
                };

                sp = StoredProcedureName.Create("VOA_JOBS_Update").GetValueOrDefault();
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, sp, parameters, TimeoutInSeconds.Default);
        }

        /// <summary>
        /// Gets the VOA job.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <returns>The VOA job. Null if not found.</returns>
        private static VOAJob GetJob(Guid publicJobId)
        {
            return GetJob(new SqlParameter("@PublicJobId", publicJobId));
        }

        /// <summary>
        /// Gets the VOA job.
        /// </summary>
        /// <param name="idParameter">The sql parameter of the id.</param>
        /// <returns>The VOA job. Null if not found.</returns>
        private static VOAJob GetJob(SqlParameter idParameter)
        {
            SqlParameter[] parameters = new SqlParameter[] { idParameter };
            StoredProcedureName retrieveSp = StoredProcedureName.Create("VOA_JOBS_Retrieve").GetValueOrDefault();

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, retrieveSp, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new VOAJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Creates a job result from the job.
        /// </summary>
        /// <returns>The job result.</returns>
        private VoaJobResult ToJobResult()
        {
            VoaJobResult jobResult = new VoaJobResult(this.ProcessingStatus, this.JobId.Value);
            switch (this.ProcessingStatus)
            {
                case ProcessingStatus.Invalid:
                    jobResult.Errors = this.responseFileDbGuidId.HasValue ? this.GetErrorResponse(this.responseFileDbGuidId.Value) : new List<string>() { LqbGrammar.DataTypes.ErrorMessage.SystemError.ToString() };
                    break;
                case ProcessingStatus.Incomplete:
                    if (DateTime.Now > this.ExpirationDate)
                    {
                        jobResult = new VoaJobResult(ProcessingStatus.Invalid, this.JobId.Value);
                        jobResult.Errors = new List<string>() { "VOA request has timed out." };
                    }

                    break;
                case ProcessingStatus.Processing:
                    break;
                case ProcessingStatus.Completed:
                    jobResult.OrderIds = this.GetOrderIds(this.responseFileDbGuidId.Value);
                    break;
                default:
                    throw new UnhandledEnumException(this.ProcessingStatus);
            }

            return jobResult;
        }

        /// <summary>
        /// Gets the full FileDb key for the response.
        /// </summary>
        /// <param name="guidId">The guid portion of the FileDb key.</param>
        /// <returns>The FileDb key.</returns>
        private string GetResponseFileDbKey(Guid guidId)
        {
            return $"{guidId.ToString("N")}_VoaJobResponse";
        }

        /// <summary>
        /// Saves the error response to FileDb.
        /// </summary>
        /// <param name="errors">The errors to save.</param>
        /// <returns>The guid portion of the FileDb key used.</returns>
        private Guid SaveErrorResponse(List<string> errors)
        {
            Guid newGuidId = Guid.NewGuid();
            string fileDbKey = this.GetResponseFileDbKey(newGuidId);
            FileDBTools.WriteJsonNetObject(E_FileDB.Normal, fileDbKey, errors);

            return newGuidId;
        }

        /// <summary>
        /// Gets the error response from FileDb.
        /// </summary>
        /// <param name="guidId">The guid portion of the FileDb key.</param>
        /// <returns>The errors saved.</returns>
        private List<string> GetErrorResponse(Guid guidId)
        {
            string fileDbKey = this.GetResponseFileDbKey(guidId);
            return FileDBTools.ReadJsonNetObject<List<string>>(E_FileDB.Normal, fileDbKey);
        }

        /// <summary>
        /// Saves the ids of the orders generated to FileDb.
        /// </summary>
        /// <param name="orderIds">The order ids.</param>
        /// <returns>The guid portion of the FileDb key used.</returns>
        private Guid SaveOrderIds(List<int> orderIds)
        {
            Guid newGuidId = Guid.NewGuid();
            string fileDbKey = this.GetResponseFileDbKey(newGuidId);
            FileDBTools.WriteJsonNetObject(E_FileDB.Normal, fileDbKey, orderIds);

            return newGuidId;
        }

        /// <summary>
        /// Gets the order ids saved to FileDb.
        /// </summary>
        /// <param name="guidId">The guid portion of the FileDb key.</param>
        /// <returns>The order ids.</returns>
        private List<int> GetOrderIds(Guid guidId)
        {
            string fileDbKey = this.GetResponseFileDbKey(guidId);
            return FileDBTools.ReadJsonNetObject<List<int>>(E_FileDB.Normal, fileDbKey);
        }

        /// <summary>
        /// Constructs the full file db key for the job request data.
        /// </summary>
        /// <param name="guidId">The guid portion of the key.</param>
        /// <returns>The full file db key.</returns>
        private string GetJobRequestDataFileDbKey(Guid guidId)
        {
            return $"{guidId.ToString("N")}_VoaJobRequestData";
        }

        /// <summary>
        /// Saves the job request data to FileDb.
        /// </summary>
        /// <param name="jobRequestData">The job request data.</param>
        /// <returns>The guid portion of the file db key.</returns>
        private Guid SaveJobRequestData(VOAJobRequestData jobRequestData)
        {
            Guid newGuidId = Guid.NewGuid();
            string fileDbKey = this.GetJobRequestDataFileDbKey(newGuidId);
            FileDBTools.WriteJsonNetObject(E_FileDB.Normal, fileDbKey, jobRequestData);

            return newGuidId;
        }

        /// <summary>
        /// Gets the job request data from FileDb.
        /// </summary>
        /// <param name="guidId">The guid id.</param>
        /// <returns>The job request data.</returns>
        private VOAJobRequestData GetJobRequestData(Guid guidId)
        {
            string fileDbKey = this.GetJobRequestDataFileDbKey(guidId);
            return FileDBTools.ReadJsonNetObject<VOAJobRequestData>(E_FileDB.Normal, fileDbKey);
        }

        /// <summary>
        /// Extracts the raw data from the VOA request data for serialization.
        /// </summary>
        /// <param name="requestData">The actual request data.</param>
        /// <returns>The raw request data that can be properly serialized.</returns>
        private VOAJobRequestData ToJobRequestData(VOARequestData requestData)
        {
            return new VOAJobRequestData()
            {
                AccountHistory = requestData.AccountHistoryOption,
                AccountId = requestData.AccountId,
                AssetsToInclude = requestData.AssetsToInclude,
                BillingCardNumber = requestData.BillingCardNumber,
                BillingCity = requestData.BillingCity,
                BillingCVV = requestData.BillingCVV,
                BillingExpirationMonth = requestData.BillingExpirationMonth,
                BillingExpirationYear = requestData.BillingExpirationYear,
                BillingFirstName = requestData.BillingFirstName,
                BillingLastName = requestData.BillingLastName,
                BillingMiddleName = requestData.BillingMiddleName,
                BillingState = requestData.BillingState,
                BillingStreetAddress = requestData.BillingStreetAddress,
                BillingZipcode = requestData.BillingZipcode,
                BorrowerAppId = requestData.BorrowerAppId,
                BorrowerAuthDocs = requestData.BorrowerAuthDocs,
                BorrowerType = requestData.BorrowerType,
                LenderServiceId = requestData.LenderServiceId,
                LoanId = requestData.LoanId,
                NotificationEmail = requestData.NotificationEmail,
                Password = requestData.Password,
                PayWithCreditCard = requestData.PayWithCreditCard,
                RefreshPeriod = requestData.RefreshPeriodOption,
                Username = requestData.Username,
                VerifiesBorrower = requestData.VerifiesBorrower,
                UserId = requestData.Principal.UserId,
                BrokerId = requestData.Principal.BrokerId,
                UserType = requestData.Principal.Type,
                RequestType = requestData.RequestType,
                PreviousOrderId = requestData.PreviousOrder?.OrderId
            };
        }

        /// <summary>
        /// Transforms the job request data to the normal VOA request data.
        /// </summary>
        /// <param name="jobRequestData">The job request data.</param>
        /// <param name="principal">The principal of the user making the request.</param>
        /// <returns>The VOA request data.</returns>
        private VOARequestData ToRequestData(VOAJobRequestData jobRequestData, AbstractUserPrincipal principal)
        {
            if (jobRequestData.RequestType == VOXRequestT.Initial)
            {
                return new VOARequestData(principal, jobRequestData.LenderServiceId, jobRequestData.LoanId, jobRequestData.RequestType)
                {
                    AccountHistoryOption = jobRequestData.AccountHistory,
                    AccountId = jobRequestData.AccountId,
                    AssetsToInclude = jobRequestData.AssetsToInclude,
                    BillingExpirationYear = jobRequestData.BillingExpirationYear,
                    BillingCardNumber = jobRequestData.BillingCardNumber,
                    BillingCity = jobRequestData.BillingCity,
                    BillingCVV = jobRequestData.BillingCVV,
                    BillingExpirationMonth = jobRequestData.BillingExpirationMonth,
                    BillingFirstName = jobRequestData.BillingFirstName,
                    BillingMiddleName = jobRequestData.BillingMiddleName,
                    BillingLastName = jobRequestData.BillingLastName,
                    BillingState = jobRequestData.BillingState,
                    BillingStreetAddress = jobRequestData.BillingStreetAddress,
                    BorrowerAuthDocs = jobRequestData.BorrowerAuthDocs.ToList(),
                    NotificationEmail = jobRequestData.NotificationEmail,
                    Password = jobRequestData.Password,
                    PayWithCreditCard = jobRequestData.PayWithCreditCard,
                    RefreshPeriodOption = jobRequestData.RefreshPeriod,
                    Username = jobRequestData.Username,
                    BillingZipcode = jobRequestData.BillingZipcode,
                    VerifiesBorrower = jobRequestData.VerifiesBorrower,
                    BorrowerAppId = jobRequestData.BorrowerAppId,
                    BorrowerType = jobRequestData.BorrowerType
                };
            }
            else
            {
                var previousOrder = VOAOrder.GetOrderForLoan(jobRequestData.BrokerId, jobRequestData.LoanId, jobRequestData.PreviousOrderId.Value);
                return new VOARequestData(principal, jobRequestData.LoanId, jobRequestData.RequestType, previousOrder)
                {
                    AccountId = jobRequestData.AccountId,
                    Username = jobRequestData.Username,
                    Password = jobRequestData.Password
                };
            }
        }
    }
}
