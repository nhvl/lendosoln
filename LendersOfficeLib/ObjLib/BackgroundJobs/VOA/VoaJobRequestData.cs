﻿namespace LendersOffice.ObjLib.BackgroundJobs.VOA
{
    using System;
    using System.Collections.Generic;
    using DataAccess;
    using LendersOffice.Integration.VOXFramework;

    /// <summary>
    /// Data for VOA requests that can be serialized. Will be used to reconstruct the request data later on.
    /// </summary>
    public class VOAJobRequestData
    {
        /// <summary>
        /// Gets or sets the loan id.
        /// </summary>
        public Guid LoanId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the broker id.
        /// </summary>
        public Guid BrokerId { get; set; }

        /// <summary>
        /// Gets or sets the user type.
        /// </summary>
        public string UserType { get; set; }

        /// <summary>
        /// Gets or sets the request type.
        /// </summary>
        public VOXRequestT RequestType { get; set; }

        /// <summary>
        /// Gets or sets the lender service id.
        /// </summary>
        public int LenderServiceId { get; set; }

        /// <summary>
        /// Gets or sets the notification email.
        /// </summary>
        public string NotificationEmail { get; set; }

        /// <summary>
        /// Gets or sets the account history option.
        /// </summary>
        public int AccountHistory { get; set; }

        /// <summary>
        /// Gets or sets the refresh period.
        /// </summary>
        public int RefreshPeriod { get; set; }

        /// <summary>
        /// Gets or sets the account id.
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// Gets or sets the username.
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this was paid with credit card.
        /// </summary>
        public bool PayWithCreditCard { get; set; }

        /// <summary>
        /// Gets or sets the first name for billing.
        /// </summary>
        public string BillingFirstName { get; set; }

        /// <summary>
        /// Gets or sets the middle name for billing.
        /// </summary>
        public string BillingMiddleName { get; set; }

        /// <summary>
        /// Gets or sets the last name for billing.
        /// </summary>
        public string BillingLastName { get; set; }

        /// <summary>
        /// Gets or sets the card number for billing.
        /// </summary>
        public string BillingCardNumber { get; set; }

        /// <summary>
        /// Gets or sets the CVV for billing.
        /// </summary>
        public string BillingCVV { get; set; }

        /// <summary>
        /// Gets or sets the street address for billing.
        /// </summary>
        public string BillingStreetAddress { get; set; }

        /// <summary>
        /// Gets or sets the city for billing.
        /// </summary>
        public string BillingCity { get; set; }

        /// <summary>
        /// Gets or sets the state for billing.
        /// </summary>
        public string BillingState { get; set; }

        /// <summary>
        /// Gets or sets the zipcode for billing.
        /// </summary>
        public string BillingZipcode { get; set; }

        /// <summary>
        /// Gets or sets the expiration month for billing.
        /// </summary>
        public int BillingExpirationMonth { get; set; }

        /// <summary>
        /// Gets or sets the expiration year for billing.
        /// </summary>
        public int BillingExpirationYear { get; set; }

        /// <summary>
        /// Gets or sets the borrower auth docs.
        /// </summary>
        public IEnumerable<VOXBorrowerAuthDoc> BorrowerAuthDocs { get; set; }

        /// <summary>
        /// Gets or sets the app id of ordering via borrower.
        /// </summary>
        public Guid? BorrowerAppId { get; set; }

        /// <summary>
        /// Gets or sets the borrower type if ordering via borrower.
        /// </summary>
        public E_BorrowerModeT? BorrowerType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this is an order using a borrower or assets.
        /// </summary>
        public bool VerifiesBorrower { get; set; }

        /// <summary>
        /// Gets or sets the assets for the order.
        /// </summary>
        public IEnumerable<VOAServiceAssetData> AssetsToInclude { get; set; }

        /// <summary>
        /// Gets or sets the previous order id.
        /// </summary>
        public int? PreviousOrderId { get; set; }
    }
}
