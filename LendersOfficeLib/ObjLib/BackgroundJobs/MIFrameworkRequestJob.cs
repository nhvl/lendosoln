﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using Drivers.SqlServerDB;
    using Integration.MortgageInsurance;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// MI Framework request job processor.
    /// </summary>
    public class MIFrameworkRequestJob : BackgroundJob
    {
        /// <summary>
        /// How many minutes to add to the start date of this job until it counts as being expired. 
        /// </summary>
        private const int MinutesUntilTimeout = 20;

        /// <summary>
        /// The MI request data passed to the job processor.
        /// </summary>
        private MIRequestData requestData = null;

        /// <summary>
        /// The FileDB Guid id for the results, once we get them.
        /// </summary>
        private Guid? resultsFileDbId = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="MIFrameworkRequestJob"/> class.
        /// </summary>
        /// <param name="nextRunDate">The next run date for this job.</param>
        /// <param name="expirationDate">The expiration date for the job.</param>
        /// <param name="requestData">The MI request data.</param>
        private MIFrameworkRequestJob(DateTime nextRunDate, DateTime expirationDate, MIRequestData requestData)
            : base(nextRunDate, expirationDate, requestData.Principal.BrokerId, requestData.LoanId, requestData.Principal.UserId)
        {
            this.resultsFileDbId = null;
            this.requestData = requestData;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MIFrameworkRequestJob"/> class from the DB.
        /// </summary>
        /// <param name="reader">The reader with DB data.</param>
        private MIFrameworkRequestJob(IDataReader reader)
            : base(reader)
        {
            this.resultsFileDbId = reader.AsNullableGuid("ResultsFileDbId");
            this.requestData = MIRequestData.CreateFromReader(reader);
        }

        /// <summary>
        /// Gets a value indicating whether the job can be retried if in the limbo status.
        /// </summary>
        public override bool CanRetryIfInLimbo => true;

        /// <summary>
        /// Gets the expected run time for the request.
        /// </summary>
        public override int? ExpectedRunTimeInSeconds => 5 * 60;

        /// <summary>
        /// Gets the number of times the job can run into exceptions before giving up.
        /// </summary>
        public override int? MaxExceptionCount => 10;

        /// <summary>
        /// Gets the job type.
        /// </summary>
        public override JobType JobType => JobType.MIFrameworkRequest;

        /// <summary>
        /// Adds an MI Framework Request job to the queue.
        /// </summary>
        /// <param name="requestData">The request data.</param>
        /// <returns>The public job id.</returns>
        public static Guid AddMIFrameworkRequestJob(MIRequestData requestData)
        {
            var nextRunDate = DateTime.Now;
            var expirationDate = nextRunDate.AddMinutes(MinutesUntilTimeout);

            var job = new MIFrameworkRequestJob(nextRunDate, expirationDate, requestData);
            job.SaveToDb();

            return job.PublicJobId.Value;
        }

        /// <summary>
        /// Checks the job status.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <returns>MI request results. Can be Error, Processing, or Completed results.</returns>
        public static MIRequestResult CheckJob(Guid publicJobId)
        {
            var job = GetMIFrameworkRequestJob(publicJobId);

            if (job == null)
            {
                Tools.LogError($"[MIFrameworkRequestJob] PublicJobId: {publicJobId}. Unable to find job.");
                return MIRequestResult.CreateErrorResult(new List<string>() { "Unable to retrieve results." }); 
            }

            switch (job.ProcessingStatus)
            {
                case ProcessingStatus.Completed:
                    var fileDbKey = job.GenerateFileDbKey(isRequest: false, guidPortion: job.resultsFileDbId.Value);
                    var results = FileDBTools.ReadJsonNetObjectWithTypeHandling<MIRequestResult>(E_FileDB.Normal, fileDbKey);
                    FileDBTools.Delete(E_FileDB.Normal, fileDbKey);

                    return results;
                case ProcessingStatus.Processing:
                    return MIRequestResult.CreateProcessingResult(publicJobId);
                case ProcessingStatus.Incomplete:
                    if (DateTime.Now > job.ExpirationDate)
                    {
                        Tools.LogError($"[MIFrameworkRequestJob] JobId: {job.JobId}. Job never picked up and has expired.");
                        return MIRequestResult.CreateErrorResult(new List<string>() { "MI Order request has timed out." });
                    }

                    return MIRequestResult.CreateProcessingResult(publicJobId);
                case ProcessingStatus.Invalid:
                    return MIRequestResult.CreateErrorResult(new List<string>() { "MI Order request could not be completed." });
                default:
                    throw new UnhandledEnumException(job.ProcessingStatus);
            }
        }

        /// <summary>
        /// Determines the next run date if the job needs to be ran again. 
        /// </summary>
        /// <param name="startTime">The start time to calculate off of.</param>
        /// <returns>The next run date.</returns>
        public override DateTime DetermineNextRunDate(DateTime startTime) => startTime.AddSeconds(this.requestData.PollingIntervalInSeconds);

        /// <summary>
        /// Gets the MI Framework Request job using the job id.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <returns>The MI Framework Request job.</returns>
        internal static MIFrameworkRequestJob GetMIFrameworkRequestJob(int jobId)
        {
            return GetMIFrameworkRequestJob(new SqlParameter("@JobId", jobId));
        }

        /// <summary>
        /// Runs the MI request.
        /// </summary>
        /// <returns>The status of the job after it has completed running.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Must set the job to invalid if any error encountered.")]
        protected override ProcessingStatus PerformAndUpdateJob()
        {
            MIRequestHandler handler = new MIRequestHandler(this.requestData);
            var results = handler.SubmitSynchronously(checkWorkflow: false); // Workflow should have been checked before adding the job, if it was supposed to be checked.

            try
            {
                // Once we get to this point, we've completed the order so any failures should be treated as irrecoverable since we don't want to retry this order if FileDB stuff fails.
                this.resultsFileDbId = Guid.NewGuid();
                var fileDbKey = this.GenerateFileDbKey(isRequest: false, guidPortion: this.resultsFileDbId.Value);
                FileDBTools.WriteJsonNetObjectWithTypeHandling(E_FileDB.Normal, fileDbKey, results);

                return ProcessingStatus.Completed;
            }
            catch (Exception exc)
            {
                Tools.LogError($"[MIFrameworkRequestJob] JobId: {this.JobId}", exc);
                return ProcessingStatus.Invalid;
            }
        }

        /// <summary>
        /// Saves the MI Framework Request job details to the DB.
        /// </summary>
        /// <param name="conn">The DB connection.</param>
        /// <param name="transaction">The DB transaction.</param>
        protected override void SaveSpecificJobInfo(DbConnection conn, DbTransaction transaction)
        {
            SqlParameter[] parameters;
            if (this.IsNew)
            {
                parameters = new SqlParameter[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@LoanId", this.LoanId),
                    new SqlParameter("@ApplicationId", this.requestData.ApplicationId),
                    new SqlParameter("@BrokerId", this.BrokerId),
                    new SqlParameter("@UserId", this.UserId),
                    new SqlParameter("@UserType", this.requestData.Principal.Type),
                    new SqlParameter("@BranchId", this.requestData.BranchId),
                    new SqlParameter("@VendorId", this.requestData.VendorId),
                    new SqlParameter("@PremiumType", this.requestData.PremiumType),
                    new SqlParameter("@UpfrontPremiumMismoValue", this.requestData.SplitPremiumMismoValue),
                    new SqlParameter("@Refundability", this.requestData.Refundability),
                    new SqlParameter("@UFMIPFinanced", this.requestData.UFMIPFinanced),
                    new SqlParameter("@MasterPolicyNumber", this.requestData.MasterPolicyNumber),
                    new SqlParameter("@CoveragePercent", this.requestData.CoveragePercent),
                    new SqlParameter("@RenewalType", this.requestData.RenewalType),
                    new SqlParameter("@PremiumAtClosing", this.requestData.PremiumAtClosing),
                    new SqlParameter("@IsRelocationLoan", this.requestData.IsRelocationLoan),
                    new SqlParameter("@IsQuote", this.requestData.IsQuote),
                    new SqlParameter("@QuoteNumber", this.requestData.QuoteNumber),
                    new SqlParameter("@OriginalQuoteNumber", this.requestData.OriginalQuoteNumber),
                    new SqlParameter("@PollingIntervalInSeconds", this.requestData.PollingIntervalInSeconds),
                    new SqlParameter("@DelegationType", this.requestData.DelegationType),
                    new SqlParameter("@RequestType", this.requestData.RequestType)
                };

                var storedProcedureName = StoredProcedureName.Create("MI_FRAMEWORK_REQUEST_JOBS_Create").GetValueOrDefault();
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, storedProcedureName, parameters, TimeoutInSeconds.Thirty);
            }
            else if (this.resultsFileDbId.HasValue)
            {
                parameters = new SqlParameter[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@ResultsFileDbId", this.resultsFileDbId)
                };

                var storedProcedureName = StoredProcedureName.Create("MI_FRAMEWORK_REQUEST_JOBS_Edit").GetValueOrDefault();
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, storedProcedureName, parameters, TimeoutInSeconds.Thirty);
            }
        }

        /// <summary>
        /// Gets the job via the public job id.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <returns>The MI framework request job.</returns>
        private static MIFrameworkRequestJob GetMIFrameworkRequestJob(Guid publicJobId)
        {
            return GetMIFrameworkRequestJob(new SqlParameter("@PublicJobId", publicJobId));
        }

        /// <summary>
        /// Gets the MI Framework Request job.
        /// </summary>
        /// <param name="idParameter">The id sql parameter.</param>
        /// <returns>The MI Framework Request job.</returns>
        private static MIFrameworkRequestJob GetMIFrameworkRequestJob(SqlParameter idParameter)
        {
            SqlParameter[] parameters = new SqlParameter[] { idParameter };
            StoredProcedureName retrieveSp = StoredProcedureName.Create("MI_FRAMEWORK_REQUEST_JOBS_Retrieve").GetValueOrDefault();

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, retrieveSp, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new MIFrameworkRequestJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Generates the FileDB key.
        /// </summary>
        /// <param name="isRequest">Whether to generate the key for the request or the result.</param>
        /// <param name="guidPortion">The Guid portion of the id.</param>
        /// <returns>The string FileDB key.</returns>
        private string GenerateFileDbKey(bool isRequest, Guid guidPortion)
        {
            if (isRequest)
            {
                return $"{guidPortion.ToString("N")}_MIRequestJobRequestData";
            }
            else
            {
                return $"{guidPortion.ToString("N")}_MIRequestJobResponseData";
            }
        }
    }
}
