﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using LendersOffice.QueryProcessor;

    /// <summary>
    /// Provides a wrapper for custom report background job statuses.
    /// </summary>
    public class CustomReportJobStatus
    {
        /// <summary>
        /// Gets or sets a value indicating whether the job has an error.
        /// </summary>
        public bool HasError { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user the permissions
        /// to run the report.
        /// </summary>
        public bool UserLacksReportRunPermission { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the job is complete.
        /// </summary>
        public bool IsComplete { get; set; }

        /// <summary>
        /// Gets or sets the query ID for the job.
        /// </summary>
        public Guid QueryId { get; set; }

        /// <summary>
        /// Gets or sets the completed report.
        /// </summary>
        public Report CompletedReport { get; set; }
    }
}
