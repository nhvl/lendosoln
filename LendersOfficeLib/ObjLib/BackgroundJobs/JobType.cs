﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    /// <summary>
    /// The jobs handled by the background job processor.
    /// </summary>
    public enum JobType
    {
        /// <summary>
        /// No job. Effectively an error type.
        /// </summary>
        None = 0,

        /// <summary>
        /// Polling job for Seamless LPA.
        /// </summary>
        SeamlessLpaPolling = 1,

        /// <summary>
        /// Custom report job.
        /// </summary>
        CustomReport = 2,

        /// <summary>
        /// Task Automation Job.
        /// </summary>
        TaskAutomation = 3,

        /// <summary>
        /// MI Framework request job.
        /// </summary>
        MIFrameworkRequest = 4,

        /// <summary>
        /// Credit ordering job.
        /// </summary>
        CreditReportRequest = 5,

        /// <summary>
        /// PDF generation job.
        /// </summary>
        PdfGenerationJob = 6,

        /// <summary>
        /// VOA request job.
        /// </summary>
        VoaJob = 7
    }
}
