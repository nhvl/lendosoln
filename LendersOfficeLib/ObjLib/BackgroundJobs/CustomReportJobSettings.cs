﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using DataAccess;
    using Reports;

    /// <summary>
    /// Provides a container for custom report job settings.
    /// </summary>
    public class CustomReportJobSettings
    {
        /// <summary>
        /// Gets or sets the report ID for the job.
        /// </summary>
        public Guid QueryId { get; set; }

        /// <summary>
        /// Gets or sets alternate sorting for the report, if supplied.
        /// </summary>
        public string AlternateSorting { get; set; }

        /// <summary>
        /// Gets or sets the scope of the report run.
        /// </summary>
        public E_ReportExtentScopeT ReportExtent { get; set; }

        /// <summary>
        /// Gets or sets the run type for the custom report.
        /// </summary>
        public CustomReportRunType ReportRunType { get; set; }
    }
}
