﻿namespace LendersOffice.ObjLib.BackgroundJobs.TaskAutomation
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;

    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.ObjLib.Task;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Background job to handle task automation.
    /// </summary>
    public class TaskAutomationJob : BackgroundJob
    {
        /// <summary>
        /// Stored procedure name for saving a task automation job.
        /// </summary>
        private static readonly StoredProcedureName SaveTaskAutomationJobSp = StoredProcedureName.Create("TASK_AUTOMATION_JOB_Save").Value;

        /// <summary>
        /// Stored procedure name for getting a task automation job by job ID.
        /// </summary>
        private static readonly StoredProcedureName GetTaskAutomationJobByJobIdSp = StoredProcedureName.Create("TASK_AUTOMATION_JOB_GetByJobId").Value;

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskAutomationJob"/> class. This will create a completely new job.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="taskId">The Task ID.</param>
        /// <param name="taskAutomationJobType">Task automation job type.</param>
        private TaskAutomationJob(Guid brokerId, Guid loanId, string taskId, TaskAutomationJobType taskAutomationJobType)
            : base(DateTime.Now, DateTime.Now.AddHours(12), brokerId, loanId, SystemUserPrincipal.TaskSystemUser.UserId)
        {
            this.TaskId = taskId;
            this.TaskAutomationJobType = taskAutomationJobType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TaskAutomationJob"/> class.
        /// </summary>
        /// <param name="reader">The reader to read from.</param>
        private TaskAutomationJob(IDataReader reader)
            : base(reader)
        {
            this.TaskId = (string)reader["TaskId"];
            this.TaskAutomationJobType = (TaskAutomationJobType)reader["TaskAutomationJobType"];
        }

        /// <summary>
        /// Gets the task ID.
        /// </summary>
        public string TaskId { get; private set; }

        /// <summary>
        /// Gets the type of task automation that this job is trying to perform.
        /// </summary>
        public TaskAutomationJobType TaskAutomationJobType { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this job can be retried if has been stuck in the <see cref="ProcessingStatus.Processing"/> limbo status
        /// for longer than its expected run time.
        /// </summary>
        /// <value>Whether the job can be retried if in limbo status.</value>
        /// <remarks>
        /// This is meant for instances where Continuous is killed in the middle of a job.
        /// </remarks>
        public override bool CanRetryIfInLimbo => true;

        /// <summary>
        /// Gets the expected run time for the job. Will be used to determine if a job in limbo can be retried. 
        /// This can be null if you don't want to retry the job if it has reached a limbo state.
        /// </summary>
        /// <value>The expected run time for the job.</value>
        public override int? ExpectedRunTimeInSeconds => 5 * 60;

        /// <summary>
        /// Gets the job type.
        /// </summary>
        /// <value>The job type.</value>
        public override JobType JobType => JobType.TaskAutomation;

        /// <summary>
        /// Creates a new task automation job.
        /// </summary>
        /// <param name="brokerId">Broker ID.</param>
        /// <param name="loanId">The loan ID.</param>
        /// <param name="taskId">The Task ID.</param>
        /// <param name="taskAutomationJobType">Task automation job type.</param>
        /// <returns>
        /// The public job ID of the created job.
        /// </returns>
        public static Guid CreateTaskAutomationJob(Guid brokerId, Guid loanId, string taskId, TaskAutomationJobType taskAutomationJobType)
        {
            var nextRunDate = DateTime.Now;
            var expirationDate = nextRunDate.AddHours(12);

            TaskAutomationJob job = new TaskAutomationJob(brokerId, loanId, taskId, taskAutomationJobType);

            job.SaveToDb();
            return job.PublicJobId.Value;
        }

        /// <summary>
        /// Gets a task automation background job by job ID.
        /// </summary>
        /// <param name="jobId">The ID of the job.</param>
        /// <returns>
        /// An instance of <see cref="TaskAutomationJob"/>.
        /// </returns>
        public static TaskAutomationJob GetTaskAutomationJob(int jobId)
        {
            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@JobId", jobId)
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, GetTaskAutomationJobByJobIdSp, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new TaskAutomationJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Determines the next run date for the job.
        /// </summary>
        /// <param name="startTime">The start time to use when determining the next run date.</param>
        /// <returns>The next run date.</returns>
        public override DateTime DetermineNextRunDate(DateTime startTime)
        {
            return startTime.AddSeconds(ConstStage.TaskAutomationJobRetryWaitInterval);
        }

        /// <summary>
        /// Performs whatever actions the job needs. This should also update whatever it needs for saving later.
        /// </summary>
        /// <returns>
        /// The processing status after performing the job.
        /// </returns>
        /// <remarks>
        /// Any exceptions that get thrown from this method will bubble up to the job processor. The processor will put it back in the queue if it hasn't expired
        /// or mark it as invalid if it has. 
        /// If you don't want this behavior when an exception is thrown, make sure to handle it properly in this method and don't rethrow it.
        /// </remarks>
        protected override ProcessingStatus PerformAndUpdateJob()
        {
            try
            {
                Task task = Task.RetrieveWithoutPermissionCheck(this.BrokerId.Value, this.TaskId);
                task.SetPrincipal(SystemUserPrincipal.TaskSystemUser);  // Could set Thread.CurrentPrincipal instead.

                if (task.TaskStatus == E_TaskStatus.Active && this.TaskAutomationJobType == TaskAutomationJobType.AutoResolve)
                {
                    task.Resolve();
                }
                else if (task.TaskStatus == E_TaskStatus.Active && this.TaskAutomationJobType == TaskAutomationJobType.AutoClose)
                {
                    task.ResolveAndClose();
                }
                else if (task.TaskStatus == E_TaskStatus.Resolved && this.TaskAutomationJobType == TaskAutomationJobType.AutoClose)
                {
                    task.Close();
                }
                else
                {
                    // In all other cases, there is nothing left to be done. Just log warning that task status could not be changed.
                    Tools.LogWarning($"TaskAutomationJob had nothing to do. JobId=[{this.PublicJobId}]. TaskId=[{task.TaskId}]. TaskStatus=[{task.TaskStatus_rep}]. TaskAutomationJobType=[{this.TaskAutomationJobType}].");
                }

                return ProcessingStatus.Completed;
            }
            catch (TaskPermissionException exp)
            {
                Tools.LogWarning($"TaskAutomationJob failed due to permission issue. Will not retry. JobId=[{this.PublicJobId}].", exp);

                return ProcessingStatus.Invalid;
            }
        }

        /// <summary>
        /// Saves the specific job info to the DB.
        /// </summary>
        /// <param name="conn">The DB connection..</param>
        /// <param name="transaction">The DB transaction.</param>
        protected override void SaveSpecificJobInfo(DbConnection conn, DbTransaction transaction)
        {
            // We don't ever need to update the job specific fields.
            if (!this.IsNew)
            {
                return;
            }

            SqlParameter[] parameters = new SqlParameter[]
            {
                new SqlParameter("@JobId", this.JobId.Value),
                new SqlParameter("@BrokerId", this.BrokerId),
                new SqlParameter("@LoanId", this.LoanId),
                new SqlParameter("@TaskId", this.TaskId),
                new SqlParameter("@TaskAutomationJobType", this.TaskAutomationJobType),
            };

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, SaveTaskAutomationJobSp, parameters, TimeoutInSeconds.Thirty);
        }
    }
}
