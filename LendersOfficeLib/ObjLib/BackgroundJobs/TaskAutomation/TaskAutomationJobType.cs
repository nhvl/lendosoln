﻿namespace LendersOffice.ObjLib.BackgroundJobs.TaskAutomation
{
    /// <summary>
    /// Type of task automation task.
    /// </summary>
    public enum TaskAutomationJobType
    {
        AutoResolve = 0,
        AutoClose = 1
    }
}
