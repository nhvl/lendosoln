﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LendersOffice.Constants;
    using LendersOffice.QueryProcessor;
    using LendersOffice.Reports;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Background job to run a custom report.
    /// </summary>
    public class CustomReportJob : BackgroundJob
    {        
        /// <summary>
        /// Procedure name to create a custom report background job.
        /// </summary>
        private static readonly StoredProcedureName CreateProcedureName = StoredProcedureName.Create("CUSTOM_REPORT_JOBS_Create").Value;

        /// <summary>
        /// Procedure name to edit a custom report background job.
        /// </summary>
        private static readonly StoredProcedureName EditProcedureName = StoredProcedureName.Create("CUSTOM_REPORT_JOBS_Edit").Value;

        /// <summary>
        /// Procedure name to get a custom report background job by private job id.
        /// </summary>
        private static readonly StoredProcedureName GetByJobIdProcedureName = StoredProcedureName.Create("CUSTOM_REPORT_JOBS_GetJobByJobId").Value;

        /// <summary>
        /// Procedure name to get a custom report background job by public job id.
        /// </summary>
        private static readonly StoredProcedureName GetByPublicJobIdProcedureName = StoredProcedureName.Create("CUSTOM_REPORT_JOBS_GetJobByPublicId").Value;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomReportJob"/> class.
        /// </summary>
        /// <param name="nextRunDate">
        /// The next run date.
        /// </param>
        /// <param name="expirationDate">
        /// The expiration date for the job.
        /// </param>
        /// <param name="brokerId">The ID of the broker this job was generated for.</param>
        /// <param name="userId">The ID of the User that initiated this job.</param>
        private CustomReportJob(DateTime nextRunDate, DateTime expirationDate, Guid brokerId, Guid userId)
            : base(nextRunDate, expirationDate, brokerId, null, userId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomReportJob"/> class.
        /// </summary>
        /// <param name="reader">
        /// The data reader to load from.
        /// </param>
        private CustomReportJob(IDataReader reader)
            : base(reader)
        {
            this.QueryId = (Guid)reader["QueryId"];
            this.UserType = (string)reader["UserType"];
            this.PortalMode = (E_PortalMode)Convert.ToInt32(reader["PortalMode"]);
            this.AlternateSorting = reader["AlternateSorting"] as string;
            this.ReportExtent = (E_ReportExtentScopeT)Convert.ToInt32(reader["ReportExtent"]);
            this.ReportRunType = (CustomReportRunType)Convert.ToInt32(reader["ReportRunType"]);
            this.UserLacksReportRunPermission = (bool)reader["UserLacksReportRunPermission"];
            this.PollingIntervalInSeconds = (int)reader["PollingIntervalInSeconds"];
            this.ResultFileDbKey = reader["ResultFileDbKey"] as Guid?;
        }

        /// <summary>
        /// Gets the job type.
        /// </summary>
        public override JobType JobType => JobType.CustomReport;

        /// <summary>
        /// Gets the time in seconds that the job is expected to run.
        /// </summary>
        public override int? ExpectedRunTimeInSeconds => 5 * 60;

        /// <summary>
        /// Gets a value indicating whether this job can be retried if in limbo.
        /// </summary>
        public override bool CanRetryIfInLimbo => true;

        /// <summary>
        /// Gets or sets the query ID for the job.
        /// </summary>
        private Guid QueryId { get; set; }

        /// <summary>
        /// Gets or sets the type of the user running the report.
        /// </summary>
        private string UserType { get; set; }

        /// <summary>
        /// Gets or sets the portal mode of the user.
        /// </summary>
        private E_PortalMode PortalMode { get; set; }

        /// <summary>
        /// Gets or sets alternate sorting for the report, if supplied.
        /// </summary>
        private string AlternateSorting { get; set; }

        /// <summary>
        /// Gets or sets the scope of the report run.
        /// </summary>
        private E_ReportExtentScopeT ReportExtent { get; set; }

        /// <summary>
        /// Gets or sets the run type for the custom report.
        /// </summary>
        private CustomReportRunType ReportRunType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user lacks the permissions
        /// required to run the report.
        /// </summary>
        private bool UserLacksReportRunPermission { get; set; }

        /// <summary>
        /// Gets or sets the number of seconds between successive polling calls.
        /// </summary>
        private int PollingIntervalInSeconds { get; set; }

        /// <summary>
        /// Gets or sets the FileDB key for the results.
        /// </summary>
        private Guid? ResultFileDbKey { get; set; }

        /// <summary>
        /// Creates a new custom report job.
        /// </summary>
        /// <param name="principal">
        /// The user creating the job.
        /// </param>
        /// <param name="settings">
        /// The settings for the custom report job.
        /// </param>
        /// <returns>
        /// The public job ID of the created job.
        /// </returns>
        public static Guid CreateCustomReportJob(AbstractUserPrincipal principal, CustomReportJobSettings settings)
        {
            var nextRunDate = DateTime.Now;
            var expirationDate = nextRunDate.AddHours(12);

            var job = new CustomReportJob(nextRunDate, expirationDate, principal.BrokerId, principal.UserId)
            {
                QueryId = settings.QueryId,
                UserType = principal.Type,
                PortalMode = principal.PortalMode,
                AlternateSorting = settings.AlternateSorting,
                ReportExtent = settings.ReportExtent,
                ReportRunType = settings.ReportRunType,
                PollingIntervalInSeconds = ConstStage.CustomReportBackgroundJobPollingIntervalSeconds
            };

            job.SaveToDb();
            return job.PublicJobId.Value;
        }

        /// <summary>
        /// Gets a custom report background job by job ID.
        /// </summary>
        /// <param name="jobId">
        /// The ID of the job.
        /// </param>
        /// <returns>
        /// The custom report background job.
        /// </returns>
        public static CustomReportJob GetCustomReportBackgroundJob(int jobId)
        {
            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@JobId", jobId)
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, GetByJobIdProcedureName, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new CustomReportJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Checks the job status of a custom report background job.
        /// </summary>
        /// <param name="publicJobId">
        /// The public job ID.
        /// </param>
        /// <returns>
        /// The custom report background job status.
        /// </returns>
        public static CustomReportJobStatus CheckJobStatus(Guid publicJobId)
        {
            var job = GetCustomReportBackgroundJob(publicJobId);
            if (job == null)
            {
                Tools.LogError($"[CustomReportBackgroundJob] IsJobComplete returned a null job for public ID {publicJobId}.");
                return new CustomReportJobStatus()
                {
                    HasError = true,
                    IsComplete = false
                };
            }

            bool hasError = false, userLacksReportRunPermission = false, isComplete = false;
            Report completedReport = null;

            switch (job.ProcessingStatus)
            {
                case ProcessingStatus.Invalid:
                    hasError = true;
                    isComplete = false;
                    userLacksReportRunPermission = job.UserLacksReportRunPermission;
                    break;

                case ProcessingStatus.Incomplete:
                    hasError = DateTime.Now > job.ExpirationDate;
                    isComplete = false;
                    break;

                case ProcessingStatus.Processing:
                    hasError = false;
                    isComplete = false;
                    break;

                case ProcessingStatus.Completed:
                    var fullFileDbKey = GetFullFileDbKey(job.ResultFileDbKey.Value);
                    completedReport = FileDBTools.ReadDataText(E_FileDB.Temp, fullFileDbKey);

                    hasError = false;
                    isComplete = true;
                    break;

                default:
                    throw new UnhandledEnumException(job.ProcessingStatus);
            }

            return new CustomReportJobStatus()
            {
                HasError = hasError,
                UserLacksReportRunPermission = userLacksReportRunPermission,
                IsComplete = isComplete,
                QueryId = job.QueryId,
                CompletedReport = completedReport
            };
        }

        /// <summary>
        /// Determines the next run date for the job.
        /// </summary>
        /// <param name="startTime">
        /// The start time to use when determining the next run date.
        /// </param>
        /// <returns>The next run date.</returns>
        public override DateTime DetermineNextRunDate(DateTime startTime) => startTime.AddSeconds(this.PollingIntervalInSeconds);

        /// <summary>
        /// Performs the background job and updates the job's status.
        /// </summary>
        /// <returns>The status of the job after it has completed running.</returns>
        protected override ProcessingStatus PerformAndUpdateJob()
        {
            var principal = PrincipalFactory.RetrievePrincipalForUser(this.BrokerId.Value, this.UserId.Value, this.UserType);
            if (principal == null)
            {
                Tools.LogError($"[CustomReportBackgroundJob] Invalid principal. JobId: {this.JobId.Value} QueryId: {this.QueryId} BrokerId: {this.BrokerId} UserId: {this.UserId}.");
                return ProcessingStatus.Invalid;
            }

            var query = Query.GetQuery(principal.BrokerId, this.QueryId);
            if (query == null)
            {
                Tools.LogError($"[CustomReportBackgroundJob] Invalid report ID. JobId: {this.JobId.Value} QueryId: {this.QueryId} BrokerId: {this.BrokerId} UserId: {this.UserId}.");
                return ProcessingStatus.Invalid;
            }

            this.UserLacksReportRunPermission = !query.CanUserRun(principal);
            if (this.UserLacksReportRunPermission)
            {
                Tools.LogInfo($"[CustomReportBackgroundJob] User does not have permission to run report. JobId: {this.JobId.Value} QueryId: {this.QueryId} BrokerId: {this.BrokerId} UserId: {this.UserId}.");
                return ProcessingStatus.Invalid;
            }

            var reporting = new LoanReporting();

            Report reportResult = null;
            switch (this.ReportRunType)
            {
                case CustomReportRunType.Pipeline:
                    reportResult = this.RunPipelineReport(reporting, principal, query);
                    break;

                default:
                    throw new IrrecoverableBackgroundJobException($"[CustomReportBackgroundJob] Invalid report run type. JobId: {this.JobId.Value} QueryId: {this.QueryId} BrokerId: {this.BrokerId} UserId: {this.UserId} ReportRunType: {this.ReportRunType}.");
            }

            if (reportResult == null)
            {
                Tools.LogError($"[CustomReportBackgroundJob] Report run returned null. JobId: {this.JobId.Value} QueryId: {this.QueryId} BrokerId: {this.BrokerId} UserId: {this.UserId}.");
                return ProcessingStatus.Invalid;
            }

            var fileDbGuidKey = Guid.NewGuid();
            var fullFileDbKey = GetFullFileDbKey(fileDbGuidKey);

            this.ResultFileDbKey = fileDbGuidKey;
            FileDBTools.WriteData(E_FileDB.Temp, fullFileDbKey, reportResult);
            return ProcessingStatus.Completed;
        }

        /// <summary>
        /// Saves job specific info to the DB.
        /// </summary>
        /// <param name="conn">
        /// The DB connection.
        /// </param>
        /// <param name="transaction">
        /// The DB transaction.
        /// </param>
        protected override void SaveSpecificJobInfo(DbConnection conn, DbTransaction transaction)
        {
            SqlParameter[] parameters = null;
            StoredProcedureName storedProcedureName;
            if (this.IsNew)
            {
                storedProcedureName = CreateProcedureName;
                parameters = new[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@QueryId", this.QueryId),
                    new SqlParameter("@BrokerId", this.BrokerId),
                    new SqlParameter("@UserId", this.UserId),
                    new SqlParameter("@UserType", this.UserType),
                    new SqlParameter("@PortalMode", this.PortalMode),
                    new SqlParameter("@AlternateSorting", this.AlternateSorting) { IsNullable = true },
                    new SqlParameter("@ReportExtent", this.ReportExtent),
                    new SqlParameter("@ReportRunType", this.ReportRunType),
                    new SqlParameter("@PollingIntervalInSeconds", this.PollingIntervalInSeconds)
                };
            }
            else
            {
                storedProcedureName = EditProcedureName;
                parameters = new[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@ResultFileDbKey", this.ResultFileDbKey) { IsNullable = true },
                    new SqlParameter("@UserLacksReportRunPermission", this.UserLacksReportRunPermission)
                };
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, storedProcedureName, parameters, TimeoutInSeconds.Thirty);
        }

        /// <summary>
        /// Gets a custom report background job by job ID.
        /// </summary>
        /// <param name="publicJobId">
        /// The public job ID.
        /// </param>
        /// <returns>
        /// The custom report background job.
        /// </returns>
        private static CustomReportJob GetCustomReportBackgroundJob(Guid publicJobId)
        {
            SqlParameter[] paramters = new[]
            {
                new SqlParameter("@PublicJobId", publicJobId)
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, GetByPublicJobIdProcedureName, paramters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new CustomReportJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the full FileDB key for the report result.
        /// </summary>
        /// <param name="partialKey">
        /// The partial key.
        /// </param>
        /// <returns>
        /// The full key.
        /// </returns>
        private static string GetFullFileDbKey(Guid partialKey) => $"{partialKey.ToString("N")}_CustomReportJobResult";

        /// <summary>
        /// Runs a pipeline report.
        /// </summary>
        /// <param name="reporting">
        /// The loan reporting reference.
        /// </param>
        /// <param name="principal">
        /// The user running the report.
        /// </param>
        /// <param name="query">
        /// The query to run.
        /// </param>
        /// <returns>
        /// The pipeline report.
        /// </returns>
        private Report RunPipelineReport(LoanReporting reporting, AbstractUserPrincipal principal, Query query)
        {
            if (!string.Equals("B", this.UserType, StringComparison.OrdinalIgnoreCase))
            {
                // Restrict P-user pipelines to the current portal mode.
                var portalModeCondition = this.GetPortalModeCondition(this.PortalMode);
                query.Relates.Add(portalModeCondition);
            }

            return reporting.Pipeline(query, principal, this.AlternateSorting, this.ReportExtent);
        }

        /// <summary>
        /// Adds additional conditions that restrict the loans displayed in the
        /// pipeline report based on the portal mode of the user.
        /// </summary>
        /// <param name="portalMode">
        /// The portal mode of the user.
        /// </param>
        /// <returns>
        /// The list of conditions.
        /// </returns>
        private Conditions GetPortalModeCondition(E_PortalMode portalMode)
        {
            var conditions = new Conditions(E_ConditionsType.Or);

            conditions.Add(new Condition()
            {
                Type = E_ConditionType.Eq,
                Id = nameof(CPageData.sBranchChannelT),
                Argument = new Argument(E_ArgumentType.Const, E_BranchChannelT.Blank.ToString("D"))
            });

            switch (portalMode)
            {
                case E_PortalMode.Correspondent:
                    conditions.Add(this.GetCorrespondentConditions());
                    break;

                case E_PortalMode.MiniCorrespondent:
                    conditions.Add(this.GetMiniCorrConditions());
                    break;

                case E_PortalMode.Broker:
                    conditions.Add(new Condition()
                    {
                        Type = E_ConditionType.Eq,
                        Id = nameof(CPageData.sBranchChannelT),
                        Argument = new Argument(E_ArgumentType.Const, E_BranchChannelT.Wholesale.ToString("D"))
                    });
                    break;

                case E_PortalMode.Retail:
                case E_PortalMode.Blank:
                default:
                    throw new IrrecoverableBackgroundJobException($"Portal mode '{portalMode}' is unhandled when retrieving a portal mode report condition. JobId: {this.JobId.Value} QueryId: {this.QueryId} BrokerId: {this.BrokerId} UserId: {this.UserId} ReportRunType: {this.ReportRunType}.");
            }

            return conditions;
        }

        /// <summary>
        /// Adds conditions for the Correspondent portal mode: the loan's
        /// channel is correspondent and the loan's correspondent process
        /// is one of blank, delegated, or prior approved.
        /// </summary>
        /// <returns>
        /// The Correspondent portal mode conditions.
        /// </returns>
        private Conditions GetCorrespondentConditions()
        {
            var corrConditions = new Conditions(E_ConditionsType.An);

            corrConditions.Add(new Condition()
            {
                Type = E_ConditionType.Eq,
                Id = nameof(CPageData.sBranchChannelT),
                Argument = new Argument(E_ArgumentType.Const, E_BranchChannelT.Correspondent.ToString("D"))
            });

            var processTypeConditions = new Conditions(E_ConditionsType.Or);

            processTypeConditions.Add(new Condition()
            {
                Type = E_ConditionType.Eq,
                Id = nameof(CPageData.sCorrespondentProcessT),
                Argument = new Argument(E_ArgumentType.Const, E_sCorrespondentProcessT.Blank.ToString("D"))
            });

            processTypeConditions.Add(new Condition()
            {
                Type = E_ConditionType.Eq,
                Id = nameof(CPageData.sCorrespondentProcessT),
                Argument = new Argument(E_ArgumentType.Const, E_sCorrespondentProcessT.Delegated.ToString("D"))
            });

            processTypeConditions.Add(new Condition()
            {
                Type = E_ConditionType.Eq,
                Id = nameof(CPageData.sCorrespondentProcessT),
                Argument = new Argument(E_ArgumentType.Const, E_sCorrespondentProcessT.PriorApproved.ToString("D"))
            });

            corrConditions.Add(processTypeConditions);
            return corrConditions;
        }

        /// <summary>
        /// Adds conditions for the Mini-Correspondent portal mode: the loan's
        /// channel is correspondent and the loan's correspondent process
        /// is mini-correspondent.
        /// </summary>
        /// <returns>
        /// The Mini-Correspondent portal mode conditions.
        /// </returns>
        private Conditions GetMiniCorrConditions()
        {
            var miniCorrConditions = new Conditions(E_ConditionsType.An);

            miniCorrConditions.Add(new Condition()
            {
                Type = E_ConditionType.Eq,
                Id = nameof(CPageData.sBranchChannelT),
                Argument = new Argument(E_ArgumentType.Const, E_BranchChannelT.Correspondent.ToString("D"))
            });

            miniCorrConditions.Add(new Condition()
            {
                Type = E_ConditionType.Eq,
                Id = nameof(CPageData.sCorrespondentProcessT),
                Argument = new Argument(E_ArgumentType.Const, E_sCorrespondentProcessT.MiniCorrespondent.ToString("D"))
            });

            return miniCorrConditions;
        }
    }
}
