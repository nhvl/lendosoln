﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    /// <summary>
    /// Gets the type of PDF generation job.
    /// </summary>
    public enum PdfGenerationJobType
    {
        /// <summary>
        /// The job will generate a single PDF.
        /// </summary>
        SinglePdf = 0,

        /// <summary>
        /// The job will generate a set of PDFs in a batch.
        /// </summary>
        BatchPdf = 1,

        /// <summary>
        /// The job will generate a Payment Statement PDF.
        /// </summary>
        PaymentStatementPdf = 2
    }
}
