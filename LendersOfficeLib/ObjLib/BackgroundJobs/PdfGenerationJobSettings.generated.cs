﻿// Generated via GenerateRecordClass.ttinclude
namespace LendersOffice.ObjLib.BackgroundJobs
{
    /// <summary>
    /// Encapsulates data for an asynchronous PDF generation job.
    /// </summary>
    public partial class PdfGenerationJobSettings
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PdfGenerationJobSettings"/> class.
        /// </summary>
        /// <param name="pdfGenerationJobType">The type of PDF generation job.</param>
        /// <param name="pdfName">The name of the PDF for single-PDF jobs.</param>
        /// <param name="serializedData">Serialized data for the PDF generation job.</param>
        /// <param name="encryptionKeyId">The ID of the encryption key for the password.</param>
        /// <param name="encryptedPassword">The encrypted password.</param>
        public PdfGenerationJobSettings(PdfGenerationJobType pdfGenerationJobType, string pdfName, string serializedData, System.Guid encryptionKeyId, byte[] encryptedPassword)
        {
            this.PdfGenerationJobType = pdfGenerationJobType;
            this.PdfName = pdfName;
            this.SerializedData = serializedData;
            this.EncryptionKeyId = encryptionKeyId;
            this.EncryptedPassword = encryptedPassword;
        }

        /// <summary>
        /// Gets the type of PDF generation job.
        /// </summary>
        public PdfGenerationJobType PdfGenerationJobType { get; }

        /// <summary>
        /// Gets the name of the PDF for single-PDF jobs.
        /// </summary>
        public string PdfName { get; }

        /// <summary>
        /// Gets serialized data for the PDF generation job.
        /// </summary>
        public string SerializedData { get; }

        /// <summary>
        /// Gets the ID of the encryption key for the password.
        /// </summary>
        public System.Guid EncryptionKeyId { get; }

        /// <summary>
        /// Gets the encrypted password.
        /// </summary>
        public byte[] EncryptedPassword { get; }
    }
}