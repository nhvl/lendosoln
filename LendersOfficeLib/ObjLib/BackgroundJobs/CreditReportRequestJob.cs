﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using CreditReport;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Job that runs Credit Report requests.
    /// </summary>
    public class CreditReportRequestJob : BackgroundJob
    {
        /// <summary>
        /// The guid id for the credit request data.
        /// </summary>
        private Guid? creditRequestDataFileDbGuidId;

        /// <summary>
        /// Whether the BJP should consider non-ready results as incomplete.
        /// </summary>
        private bool shouldReissueOnResponseNotReady;

        /// <summary>
        /// The retry interval in seconds.
        /// </summary>
        private int retryIntervalInSeconds;

        /// <summary>
        /// The max retry count in case this job is reissuing on non-ready results.
        /// </summary>
        private int? maxRetryCount;

        /// <summary>
        /// The number of attempts running this job.
        /// </summary>
        private int attemptCount;

        /// <summary>
        /// The post save action to take.
        /// </summary>
        private Action postSaveAction = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportRequestJob"/> class.
        /// </summary>
        /// <param name="nextRunDate">The next run date.</param>
        /// <param name="expirationDate">The expiration date.</param>
        /// <param name="creditRequestData">The credit request data.</param>
        /// <param name="principal">The principal of the user initiating the request.</param>
        /// <param name="retryIntervalInSeconds">The retry interval in seconds.</param>
        /// <param name="shouldReissueOnResponseNotReady">Whether the BJP should consider non-ready results as incomplete.</param>
        /// <param name="maxRetryCount">The maximum number of retries when reissuing on non-ready results.</param>
        /// <param name="loanId">The ID of the loan this request is for.</param>
        private CreditReportRequestJob(DateTime nextRunDate, DateTime expirationDate, CreditRequestData creditRequestData, AbstractUserPrincipal principal, int retryIntervalInSeconds, bool shouldReissueOnResponseNotReady, int? maxRetryCount, Guid loanId)
            : base(nextRunDate, expirationDate, principal.BrokerId, loanId, principal.UserId)
        {
            this.retryIntervalInSeconds = retryIntervalInSeconds;
            this.shouldReissueOnResponseNotReady = shouldReissueOnResponseNotReady;
            this.maxRetryCount = maxRetryCount;
            this.attemptCount = 0;
            this.Principal = new Lazy<AbstractUserPrincipal>(() => principal);
            this.CreditRequestData = new Lazy<CreditRequestData>(() => creditRequestData);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CreditReportRequestJob"/> class.
        /// </summary>
        /// <param name="reader">The data reader.</param>
        private CreditReportRequestJob(IDataReader reader)
            : base(reader)
        {
            this.shouldReissueOnResponseNotReady = (bool)reader["ShouldReissueOnResponseNotReady"];
            this.retryIntervalInSeconds = (int)reader["RetryIntervalInSeconds"];
            this.maxRetryCount = reader.AsNullableInt("MaxRetryCount");
            this.attemptCount = (int)reader["AttemptCount"];
            this.CreditReportRequestResultFileDbGuidId = reader.AsNullableGuid("CreditReportResponseFileDbGuidId");

            var userType = reader.AsNullableString("UserType");
            this.Principal = new Lazy<AbstractUserPrincipal>(() =>
            {
                if (this.BrokerId.HasValue && this.UserId.HasValue && !string.IsNullOrEmpty(userType))
                {
                    return PrincipalFactory.RetrievePrincipalForUser(this.BrokerId.Value, this.UserId.Value, userType);
                }

                return null;
            });

            this.creditRequestDataFileDbGuidId = reader.AsNullableGuid("CreditRequestDataFileDbGuidId");
            this.CreditRequestData = new Lazy<CreditRequestData>(() =>
            {
                return this.GetCreditRequestDataFromFileDb(this.creditRequestDataFileDbGuidId);
            });
        }

        /// <summary>
        /// Gets the credit request data in a Lazy.
        /// </summary>
        public Lazy<CreditRequestData> CreditRequestData { get; private set; }

        /// <summary>
        /// Gets the guid id of the credit report response.
        /// </summary>
        public Guid? CreditReportRequestResultFileDbGuidId { get; private set; }

        /// <summary>
        /// Gets the principal of the user who made this request.
        /// </summary>
        public Lazy<AbstractUserPrincipal> Principal { get; private set; }

        /// <summary>
        /// Gets the job type.
        /// </summary>
        public override JobType JobType => JobType.CreditReportRequest;

        /// <summary>
        /// Gets a value indicating whether this job can be retried if it falls into the limbo state.
        /// </summary>
        public override bool CanRetryIfInLimbo => true;

        /// <summary>
        /// Gets the expected run time in seconds.
        /// </summary>
        public override int? ExpectedRunTimeInSeconds => 5 * 60;

        /// <summary>
        /// Gets the maximum exception count before giving up.
        /// </summary>
        public override int? MaxExceptionCount => 10;

        /// <summary>
        /// Adds a credit report request job to the queue.
        /// </summary>
        /// <param name="creditRequestData">The credit request data.</param>
        /// <param name="principal">The principal of the user initiating the request.</param>
        /// <param name="retryIntervalInSeconds">The interval between retry attempts, for both errors and non-ready results, if applicable.</param>
        /// <param name="shouldReissueOnResponseNotReady">Should reissue on response not ready.</param>
        /// <param name="maxRetryCount">The max retry attempts for non-ready results, if applicable.</param>
        /// <param name="loanId">The ID of the loan this request is for.</param>
        /// <returns>The public job id.</returns>
        public static Guid AddCreditReportRequestJob(CreditRequestData creditRequestData, AbstractUserPrincipal principal, int retryIntervalInSeconds, bool shouldReissueOnResponseNotReady, int? maxRetryCount, Guid loanId)
        {
            var nextRunDate = DateTime.Now;
            var expirationDate = shouldReissueOnResponseNotReady ? nextRunDate.AddHours(12) : nextRunDate.AddMinutes(20); // Give it a longer expiration date if retrying if report is not ready.
            var job = new CreditReportRequestJob(nextRunDate, expirationDate, creditRequestData, principal, retryIntervalInSeconds, shouldReissueOnResponseNotReady, maxRetryCount, loanId);
            job.SaveToDb();

            return job.PublicJobId.Value;
        }

        /// <summary>
        /// Checks the credit report request job.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <returns>Credit report request job results.</returns>
        public static CreditReportRequestJobResult CheckJob(Guid publicJobId)
        {
            var job = GetCreditReportRequestJob(publicJobId);
            if (job == null)
            {
                Tools.LogError($"[CreditReportRequestJob] PublicJobId: {publicJobId}. Unable to find job.");
                return null;
            }

            return CreditReportRequestJobResult.CreateResult(job);
        }

        /// <summary>
        /// Gets the next run time if needed.
        /// </summary>
        /// <param name="startTime">The starting time.</param>
        /// <returns>The next run time.</returns>
        public override DateTime DetermineNextRunDate(DateTime startTime) => startTime.AddSeconds(this.retryIntervalInSeconds);

        /// <summary>
        /// Gets the job using the job id.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <returns>The credit report request job.</returns>
        internal static CreditReportRequestJob GetCreditReportRequestJob(int jobId)
        {
            return GetCreditReportRequestJob(new SqlParameter("@JobId", jobId));
        }

        /// <summary>
        /// Get the raw credit report xml response from FileDb.
        /// </summary>
        /// <returns>The raw credit report xml response.</returns>
        internal string GetResultRawCreditReportResponse()
        {
            var fullKey = this.GetCreditReportResultFileDbKey(this.CreditReportRequestResultFileDbGuidId.Value);
            return FileDBTools.ReadDataText(E_FileDB.Normal, fullKey);
        }

        /// <summary>
        /// Gets the error message results from FileDb.
        /// </summary>
        /// <returns>The error message results. Null if there were no error messages saved by the job.</returns>
        internal IEnumerable<string> GetResultErrorMessages()
        {
            if (!this.CreditReportRequestResultFileDbGuidId.HasValue)
            {
                return null;
            }

            var fullKey = this.GetCreditReportResultFileDbKey(this.CreditReportRequestResultFileDbGuidId.Value);
            return FileDBTools.ReadJsonNetObjectWithTypeHandling<IEnumerable<string>>(E_FileDB.Normal, fullKey);
        }

        /// <summary>
        /// Runs the post save action.
        /// </summary>
        protected override void PostSaveActions() => this.postSaveAction?.Invoke();

        /// <summary>
        /// Runs the credit report request.
        /// </summary>
        /// <returns>The processing status after running the job.</returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Must set the job to invalid if any error encountered.")]
        protected override ProcessingStatus PerformAndUpdateJob()
        {
            this.attemptCount++;

            var requestData = this.CreditRequestData.Value;
            var requestHandler = new CreditReportRequestHandler(requestData, this.Principal.Value);

            var result = requestHandler.SubmitSynchronously();
            if (result.Status == CreditReportRequestResultStatus.Failure)
            {
                this.CreditReportRequestResultFileDbGuidId = this.SaveErrorMessagesToFileDb(result.ErrorMessages);
                return ProcessingStatus.Invalid;
            }
            else if (result.Status == CreditReportRequestResultStatus.Completed && result.CreditReportResponse != null && !result.CreditReportResponse.HasError && !result.CreditReportResponse.IsReady && this.shouldReissueOnResponseNotReady)
            {
                if (!this.maxRetryCount.HasValue || this.attemptCount > this.maxRetryCount)
                {
                    var errorMessages = new List<string>() { "Unable to retrieve credit report. Exceeded maximum number of attempts" };
                    this.CreditReportRequestResultFileDbGuidId = this.SaveErrorMessagesToFileDb(errorMessages);

                    return ProcessingStatus.Invalid;
                }

                var creditResponse = result.CreditReportResponse;
                if (requestData.CreditProtocol == CreditReportProtocol.Mcl)
                {
                    requestData.ReportID = creditResponse.ReportID;
                    requestData.MclRequestType = CreditReport.Mcl.MclRequestType.Get;
                }
                else if (requestData.CreditProtocol == CreditReportProtocol.InfoNetwork)
                {
                    requestData.ReportID = creditResponse.ReportID;
                    requestData.RequestActionType = CreditReport.Mismo.E_CreditReportRequestActionType.StatusQuery;
                }

                var oldCreditRequestDataFileDbGuidId = this.creditRequestDataFileDbGuidId.Value;
                this.postSaveAction = () => this.DeleteCreditRequestData(oldCreditRequestDataFileDbGuidId);

                this.creditRequestDataFileDbGuidId = this.SaveCreditRequestDataToFileDb(requestData);

                return ProcessingStatus.Incomplete;
            }
            else
            {
                try
                {
                    var rawResponseXml = result.RawCreditReportResponse;
                    this.CreditReportRequestResultFileDbGuidId = this.SaveRawCreditReportResponseXml(rawResponseXml);

                    return ProcessingStatus.Completed;
                }
                catch (Exception exc)
                {
                    Tools.LogError($"[CreditReportRequestJob] JobId: {this.JobId}", exc);
                    this.CreditReportRequestResultFileDbGuidId = this.SaveErrorMessagesToFileDb(new List<string>() { ErrorMessage.SystemError.ToString() });
                    return ProcessingStatus.Invalid;
                }
            }
        }

        /// <summary>
        /// Saves the job data to the DB.
        /// </summary>
        /// <param name="conn">The DB connection info.</param>
        /// <param name="transaction">The DB transaction.</param>
        protected override void SaveSpecificJobInfo(DbConnection conn, DbTransaction transaction)
        {
            SqlParameter[] parameters;
            if (this.IsNew)
            {
                this.creditRequestDataFileDbGuidId = this.SaveCreditRequestDataToFileDb(this.CreditRequestData.Value);

                parameters = new SqlParameter[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@RetryIntervalInSeconds", this.retryIntervalInSeconds),
                    new SqlParameter("@ShouldReissueOnResponseNotReady", this.shouldReissueOnResponseNotReady),
                    new SqlParameter("@MaxRetryCount", this.maxRetryCount),
                    new SqlParameter("@CreditRequestDataFileDbGuidId", this.creditRequestDataFileDbGuidId),
                    new SqlParameter("@BrokerId", this.BrokerId),
                    new SqlParameter("@UserId", this.UserId),
                    new SqlParameter("@UserType", this.Principal.Value.Type),
                    new SqlParameter("@AttemptCount", this.attemptCount)
                };

                StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, StoredProcedureName.Create("CREDIT_REPORT_REQUEST_JOBS_Create").GetValueOrDefault(), parameters, TimeoutInSeconds.Thirty);
                this.IsNew = false;
            }
            else
            {
                parameters = new SqlParameter[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@CreditRequestDataFileDbGuidId", this.creditRequestDataFileDbGuidId),
                    new SqlParameter("@CreditReportResponseFileDbGuidId", this.CreditReportRequestResultFileDbGuidId),
                    new SqlParameter("@AttemptCount", this.attemptCount)
                };

                StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, StoredProcedureName.Create("CREDIT_REPORT_REQUEST_JOBS_Edit").GetValueOrDefault(), parameters, TimeoutInSeconds.Thirty);
            }
        }

        /// <summary>
        /// Gets the credit report request job using the public job id.
        /// </summary>
        /// <param name="publicJobId">The public job id.</param>
        /// <returns>The credit report request job.</returns>
        private static CreditReportRequestJob GetCreditReportRequestJob(Guid publicJobId)
        {
            return GetCreditReportRequestJob(new SqlParameter("@PublicJobId", publicJobId));
        }

        /// <summary>
        /// Gets the credit report request job.
        /// </summary>
        /// <param name="idParameter">The id given.</param>
        /// <returns>The credit report request job.</returns>
        private static CreditReportRequestJob GetCreditReportRequestJob(SqlParameter idParameter)
        {
            SqlParameter[] parameters = new SqlParameter[] { idParameter };
            StoredProcedureName retrieveSp = StoredProcedureName.Create("CREDIT_REPORT_REQUEST_JOBS_Retrieve").GetValueOrDefault();

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, retrieveSp, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new CreditReportRequestJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Creates the full FileDB key for the credit request data.
        /// </summary>
        /// <param name="guidId">The guid id.</param>
        /// <returns>The full FileDB key.</returns>
        private string GetCreditRequestDataFileDbKey(Guid guidId)
        {
            return $"{guidId.ToString("N")}_CreditRequestData";
        }

        /// <summary>
        /// Saves the credit request data to file db.
        /// </summary>
        /// <param name="creditRequestData">The credit request data.</param>
        /// <returns>The guid portion of the id.</returns>
        private Guid SaveCreditRequestDataToFileDb(CreditRequestData creditRequestData)
        {
            Guid guidId = Guid.NewGuid();
            var fullKey = this.GetCreditRequestDataFileDbKey(guidId);
            FileDBTools.WriteJsonNetObjectWithTypeHandling(E_FileDB.Normal, fullKey, creditRequestData);

            return guidId;
        }

        /// <summary>
        /// Gets the credit request data from FileDB.
        /// </summary>
        /// <param name="guidId">The guid id.</param>
        /// <returns>The credit request data.</returns>
        private CreditRequestData GetCreditRequestDataFromFileDb(Guid? guidId)
        {
            if (!guidId.HasValue)
            {
                return null;
            }

            var fullKey = this.GetCreditRequestDataFileDbKey(guidId.Value);
            return FileDBTools.ReadJsonNetObject<CreditRequestData>(E_FileDB.Normal, fullKey);
        }

        /// <summary>
        /// Deletes the outdated credit request data from FileDb.
        /// </summary>
        /// <param name="guidId">The guid portion of the file db key.</param>
        private void DeleteCreditRequestData(Guid guidId)
        {
            var fullKey = this.GetCreditRequestDataFileDbKey(guidId);
            FileDBTools.Delete(E_FileDB.Normal, fullKey);
        }

        /// <summary>
        /// Gets the full FileDB key for the credit report request results.
        /// </summary>
        /// <param name="guidId">The guid id.</param>
        /// <returns>The full FileDB key.</returns>
        private string GetCreditReportResultFileDbKey(Guid guidId)
        {
            return $"{guidId.ToString("N")}_CreditReportRequestResult";
        }

        /// <summary>
        /// Saves the list of error messages to FileDb for retrieval later.
        /// </summary>
        /// <param name="errorMessages">The error messages to save.</param>
        /// <returns>The guid used when saving to FileDb.</returns>
        private Guid SaveErrorMessagesToFileDb(IEnumerable<string> errorMessages)
        {
            var newGuid = Guid.NewGuid();
            var fullFileDbKey = this.GetCreditReportResultFileDbKey(newGuid);
            FileDBTools.WriteJsonNetObjectWithTypeHandling(E_FileDB.Normal, fullFileDbKey, errorMessages);

            return newGuid;
        }

        /// <summary>
        /// Saves the raw response xml to FileDb for retrieval later.
        /// </summary>
        /// <param name="rawResponseXml">The raw response xml.</param>
        /// <returns>The guid used when saving to FileDb.</returns>
        private Guid SaveRawCreditReportResponseXml(string rawResponseXml)
        {
            var newGuid = Guid.NewGuid();
            var fullFileDbKey = this.GetCreditReportResultFileDbKey(newGuid);
            FileDBTools.WriteData(E_FileDB.Normal, fullFileDbKey, rawResponseXml);

            return newGuid;
        }
    }
}
