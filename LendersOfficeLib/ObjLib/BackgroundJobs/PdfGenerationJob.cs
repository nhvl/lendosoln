﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Data;
    using System.Data.Common;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Pdf.Async;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// A PDF generation job.
    /// </summary>
    public class PdfGenerationJob : BackgroundJob
    {
        /// <summary>
        /// Procedure name to create a PDF generation background job.
        /// </summary>
        private static readonly StoredProcedureName CreateProcedureName = StoredProcedureName.Create("PDF_GENERATION_JOBS_Create").Value;

        /// <summary>
        /// Procedure name to edit a PDF generation background job.
        /// </summary>
        private static readonly StoredProcedureName EditProcedureName = StoredProcedureName.Create("PDF_GENERATION_JOBS_Edit").Value;

        /// <summary>
        /// Procedure name to get a PDF generation background job by private job id.
        /// </summary>
        private static readonly StoredProcedureName GetByJobIdProcedureName = StoredProcedureName.Create("PDF_GENERATION_JOBS_GetJobByJobId").Value;

        /// <summary>
        /// Procedure name to get a PDF generation background job by public job id.
        /// </summary>
        private static readonly StoredProcedureName GetByPublicJobIdProcedureName = StoredProcedureName.Create("PDF_GENERATION_JOBS_GetJobByPublicId").Value;

        /// <summary>
        /// Initializes a new instance of the <see cref="PdfGenerationJob"/> class.
        /// </summary>
        /// <param name="nextRunDate">
        /// The next run date.
        /// </param>
        /// <param name="expirationDate">
        /// The expiration date for the job.
        /// </param>
        /// <param name="brokerId">The ID of the broker this job was generated for.</param>
        /// <param name="loanId">The ID of the loan this request is for.</param>
        /// <param name="userId">The ID of the user that initiated the job.</param>
        private PdfGenerationJob(DateTime nextRunDate, DateTime expirationDate, Guid brokerId, Guid? loanId, Guid userId)
            : base(nextRunDate, expirationDate, brokerId, loanId, userId)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PdfGenerationJob"/> class.
        /// </summary>
        /// <param name="reader">
        /// The data reader to load from.
        /// </param>
        private PdfGenerationJob(IDataReader reader)
            : base(reader)
        {
            this.UserType = (string)reader["UserType"];
            this.PdfGenerationJobType = (PdfGenerationJobType)Convert.ToInt32(reader["PdfGenerationJobType"]);
            this.PdfName = reader["PdfName"] as string;
            this.SerializedData = (string)reader["SerializedData"];
            this.EncryptionKeyId = (Guid)reader["EncryptionKeyId"];
            this.EncryptedPassword = reader["EncryptedPassword"] as byte[];
            this.ResultFileDbKey = reader["ResultFileDbKey"] as Guid?;
            this.PollingIntervalInSeconds = (int)reader["PollingIntervalInSeconds"];
        }

        /// <summary>
        /// Gets the job type.
        /// </summary>
        public override JobType JobType => JobType.PdfGenerationJob;

        /// <summary>
        /// Gets the time in seconds that the job is expected to run.
        /// </summary>
        public override int? ExpectedRunTimeInSeconds => 5 * 60;

        /// <summary>
        /// Gets a value indicating whether this job can be retried if in limbo.
        /// </summary>
        public override bool CanRetryIfInLimbo => true;

        /// <summary>
        /// Gets the FileDB key for the generated PDF.
        /// </summary>
        public Guid? ResultFileDbKey { get; private set; }

        /// <summary>
        /// Gets or sets the type of the user running the report.
        /// </summary>
        private string UserType { get; set; }

        /// <summary>
        /// Gets or sets the type of PDF generation job.
        /// </summary>
        private PdfGenerationJobType PdfGenerationJobType { get; set; }

        /// <summary>
        /// Gets or sets the name of the PDF for single-PDF jobs.
        /// </summary>
        private string PdfName { get; set; }

        /// <summary>
        /// Gets or sets serialized data for the job.
        /// </summary>
        private string SerializedData { get; set; }

        /// <summary>
        /// Gets or sets the encryption key ID for the PDF password.
        /// </summary>
        private Guid EncryptionKeyId { get; set; }

        /// <summary>
        /// Gets or sets the encrypted password for the PDF.
        /// </summary>
        private byte[] EncryptedPassword { get; set; }

        /// <summary>
        /// Gets or sets the number of seconds between successive polling calls.
        /// </summary>
        private int PollingIntervalInSeconds { get; set; }

        /// <summary>
        /// Creates a new PDF creation job.
        /// </summary>
        /// <param name="principal">
        /// The user creating the job.
        /// </param>
        /// <param name="settings">
        /// The settings for the custom report job.
        /// </param>
        /// <param name="loanId">The ID of the loan this request is for.</param>
        /// <returns>
        /// The public job ID of the created job.
        /// </returns>
        public static Guid CreatePdfGenerationJob(AbstractUserPrincipal principal, PdfGenerationJobSettings settings, Guid? loanId)
        {
            var nextRunDate = DateTime.Now;
            var expirationDate = nextRunDate.AddHours(12);

            var job = new PdfGenerationJob(nextRunDate, expirationDate, principal.BrokerId, loanId, principal.UserId)
            {
                UserType = principal.Type,
                PdfGenerationJobType = settings.PdfGenerationJobType,
                PdfName = settings.PdfName,
                SerializedData = settings.SerializedData,
                EncryptionKeyId = settings.EncryptionKeyId,
                EncryptedPassword = settings.EncryptedPassword,
                PollingIntervalInSeconds = ConstStage.PdfGenerationBackgroundJobPollingIntervalSeconds
            };

            job.SaveToDb();
            return job.PublicJobId.Value;
        }

        /// <summary>
        /// Gets a PDF generation background job by job ID.
        /// </summary>
        /// <param name="jobId">
        /// The ID of the job.
        /// </param>
        /// <returns>
        /// The PDF generation background job.
        /// </returns>
        public static PdfGenerationJob GetPdfGenerationJob(int jobId)
        {
            SqlParameter[] parameters = new[]
            {
                new SqlParameter("@JobId", jobId)
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, GetByJobIdProcedureName, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new PdfGenerationJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a PDF generation background job by public job ID.
        /// </summary>
        /// <param name="publicJobId">
        /// The public ID of the job.
        /// </param>
        /// <returns>
        /// The PDF generation background job.
        /// </returns>
        public static PdfGenerationJob GetPdfGenerationJob(Guid publicJobId)
        {
            SqlParameter[] paramters = new SqlParameter[]
            {
                new SqlParameter("@PublicJobId", publicJobId)
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, GetByPublicJobIdProcedureName, paramters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new PdfGenerationJob(reader);
                }
            }

            return null;
        }

        /// <summary>
        /// Determines the next run date for the job.
        /// </summary>
        /// <param name="startTime">
        /// The start time to use when determining the next run date.
        /// </param>
        /// <returns>The next run date.</returns>
        public override DateTime DetermineNextRunDate(DateTime startTime) => startTime.AddSeconds(this.PollingIntervalInSeconds);

        /// <summary>
        /// Performs the background job and updates the job's status.
        /// </summary>
        /// <returns>The status of the job after it has completed running.</returns>
        protected override ProcessingStatus PerformAndUpdateJob()
        {
            var currentPrincipal = System.Threading.Thread.CurrentPrincipal;

            AbstractUserPrincipal userPrincipal;
            if (!this.BrokerId.HasValue || this.BrokerId == Guid.Empty || this.BrokerId == ConstAppDavid.SystemBrokerGuid)
            {
                userPrincipal = SystemUserPrincipal.TaskSystemUser;
            }
            else
            {
                userPrincipal = PrincipalFactory.RetrievePrincipalForUser(this.BrokerId.Value, this.UserId.Value, this.UserType);
            }

            if (userPrincipal == null)
            {
                Tools.LogError($"[PdfGenerationJob] Invalid principal. JobId: {this.JobId.Value} PdfGenerationJobType: {this.PdfGenerationJobType} BrokerId: {this.BrokerId} UserId: {this.UserId}.");
                return ProcessingStatus.Invalid;
            }

            using (var principalHelper = new TemporaryPrincipalUseHelper(currentPrincipal, userPrincipal))
            {
                return this.PerformAndUpdateJobImpl();
            }
        }

        /// <summary>
        /// Saves job specific info to the DB.
        /// </summary>
        /// <param name="conn">
        /// The DB connection.
        /// </param>
        /// <param name="transaction">
        /// The DB transaction.
        /// </param>
        protected override void SaveSpecificJobInfo(DbConnection conn, DbTransaction transaction)
        {
            SqlParameter[] parameters = null;
            StoredProcedureName procedureName;

            if (this.IsNew)
            {
                procedureName = CreateProcedureName;
                parameters = new[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@BrokerId", this.BrokerId),
                    new SqlParameter("@UserId", this.UserId),
                    new SqlParameter("@UserType", this.UserType),
                    new SqlParameter("@PdfGenerationJobType", this.PdfGenerationJobType),
                    new SqlParameter("@PdfName", this.PdfName) { IsNullable = true },
                    new SqlParameter("@SerializedData", this.SerializedData),
                    new SqlParameter("@EncryptionKeyId", this.EncryptionKeyId),
                    new SqlParameter("@EncryptedPassword", this.EncryptedPassword) { IsNullable = true },
                    new SqlParameter("@PollingIntervalInSeconds", this.PollingIntervalInSeconds)
                };
            }
            else if (this.ResultFileDbKey.HasValue)
            {
                procedureName = EditProcedureName;
                parameters = new[]
                {
                    new SqlParameter("@JobId", this.JobId.Value),
                    new SqlParameter("@ResultFileDbKey", this.ResultFileDbKey.Value),
                };
            }
            else
            {
                // No-op
                return;
            }

            StoredProcedureDriverHelper.ExecuteNonQuery(conn, transaction, procedureName, parameters, TimeoutInSeconds.Thirty);
        }

        /// <summary>
        /// Performs the background job and updates the job's status.
        /// </summary>
        /// <returns>The status of the job after it has completed running.</returns>
        private ProcessingStatus PerformAndUpdateJobImpl()
        {
            var jobHelper = new AsyncPdfJobHelper();
            var generationHelper = new AsyncPdfGenerationHelper();

            var encryptionKeyIdentifier = EncryptionKeyIdentifier.Create(this.EncryptionKeyId).Value;
            var password = Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyIdentifier, this.EncryptedPassword);

            AsyncPdfGenerationResult result;
            switch (this.PdfGenerationJobType)
            {
                case PdfGenerationJobType.SinglePdf:
                    var arguments = jobHelper.GetPdfArgumentsFromSerializedData(this.SerializedData);
                    result = generationHelper.GenerateSinglePdf(this.PdfName, arguments, password);
                    break;

                case PdfGenerationJobType.BatchPdf:
                    var printList = jobHelper.GetPdfPrintListFromSerializedData(this.SerializedData);
                    result = generationHelper.GenerateBatchPdf(printList, password);
                    break;

                case PdfGenerationJobType.PaymentStatementPdf:
                    var options = jobHelper.GetPaymentStatementPdfOptionsFromSerializedData(this.SerializedData);
                    result = generationHelper.GeneratePaymentStatementPdf(options);
                    break;
                default:
                    throw new IrrecoverableBackgroundJobException($"[PdfGenerationJob] Invalid PdfGenerationJobType. JobId: {this.JobId.Value} PdfGenerationJobType: {this.PdfGenerationJobType}");
            }

            switch (result.Status)
            {
                case AsyncPdfGenerationStatus.Processing:
                    return ProcessingStatus.Processing;

                case AsyncPdfGenerationStatus.Complete:
                    this.ResultFileDbKey = result.FileDbKey.Value;
                    return ProcessingStatus.Completed;

                case AsyncPdfGenerationStatus.Error:
                    Tools.LogError($"[PdfGenerationJob] Unable to generate PDF. JobId: {this.JobId.Value} Error message: {result.ErrorMessage}");
                    return ProcessingStatus.Invalid;

                default:
                    throw new IrrecoverableBackgroundJobException($"[PdfGenerationJob] Invalid result status. JobId: {this.JobId.Value} Result status: {result.Status}");
            }
        }
    }
}
