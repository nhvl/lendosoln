﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Base class for all background job results.
    /// </summary>
    public class BackgroundJobResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BackgroundJobResult"/> class.
        /// </summary>
        /// <param name="status">The status of the job.</param>
        /// <param name="jobId">The job id.</param>
        protected BackgroundJobResult(ProcessingStatus status, int jobId)
        {
            this.ProcessingStatus = status;
            this.JobId = jobId;
        }

        /// <summary>
        /// Gets the status of the job.
        /// </summary>
        /// <value>The status of the job.</value>
        public ProcessingStatus ProcessingStatus
        {
            get;
        }

        /// <summary>
        /// Gets the job id.
        /// </summary>
        /// <value>The job id.</value>
        public int JobId
        {
            get;
        }
    }
}
