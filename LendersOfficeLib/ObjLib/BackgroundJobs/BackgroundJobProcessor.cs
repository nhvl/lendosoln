﻿namespace LendersOffice.ObjLib.BackgroundJobs
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using Common;
    using CommonProjectLib.Runnable;
    using DataAccess;
    using Drivers.SqlServerDB;
    using LendersOffice.Constants;
    using LendersOffice.ObjLib.BackgroundJobs.TaskAutomation;
    using LendersOffice.ObjLib.Logging;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Process the background jobs in the DB table queue.
    /// </summary>
    public class BackgroundJobProcessor : IRunnable
    {
        /// <summary>
        /// SP for getting the next job and marking it as being processed.
        /// </summary>
        private static readonly StoredProcedureName BackgroundJobsGetNextJobAndMarkSp = StoredProcedureName.Create("BACKGROUND_JOBS_GetNextJobAndMark").Value;

        /// <summary>
        /// SP for changing the processing status of a job.
        /// </summary>
        private static readonly StoredProcedureName BackgroundJobsChangeProcessingStatusSp = StoredProcedureName.Create("BACKGROUND_JOBS_ChangeProcessingStatus").Value;

        /// <summary>
        /// SP for adding a job that has errored out back into the queue.
        /// </summary>
        private static readonly StoredProcedureName BackgroundJobsResetOnError = StoredProcedureName.Create("BACKGROUND_JOBS_ResetOnError").Value;

        /// <summary>
        /// Gets the description of this IRunnable.
        /// </summary>
        /// <value>The description of this IRunnable.</value>
        public string Description
        {
            get
            {
                return "Pulls jobs from the BACKGROUND_JOBS database table to process.";
            }
        }

        /// <summary>
        /// Pulls a job that needs to be ran from the BACKGROUND_JOBS DB table and runs it.
        /// </summary>
        public void Run()
        {
            using (var workerTiming = new WorkerExecutionTiming("BackgroundJobProcessor"))
            {
                // We want to take the app start up penalty before we start pulling jobs from the queue.
                // This way, as long as the multiple BackgroundJobProcessors stagger their startup penalty time,
                // we're always processing some items in the queue.
                Tools.InitializeLargeStatics();
                this.RunProcessor(workerTiming);
            }
        }

        /// <summary>
        /// Runs the processor. Exposed for the cancellable version.
        /// </summary>
        /// <param name="timer">The timer for this runnable.</param>
        /// <returns>Whether or not the processor successfully completed a job with this call.</returns>
        [SuppressMessage(category: "LendingQBStyleCop.LendingQBCustomRules", checkId: "LB1003:CannotSwallowGeneralException", Justification = "Must set the job to invalid if any error encountered.")]
        internal bool RunProcessor(WorkerExecutionTiming timer)
        {
            BackgroundJob job = null;
            int? jobId;
            JobType? jobType;
            var jobFound = this.PullJobAndMark(out jobId, out jobType);

            if (jobFound)
            {
                try
                {
                    // Job at this point is in the Processing status.
                    job = this.GetJob(jobId.Value, jobType.Value);
                }
                catch (Exception e)
                {
                    // I am choosing to not put this back in the queue to prevent the scenario where we keep putting this job back in the queue
                    // and it keeps being chosen as the job to run. 
                    // We don't have any sort of job info like we do in the next try-catch so we can't determine what status to place it into and
                    // what the next run time will be. 
                    Tools.LogError($"[BackgroundJobProcessor] Error pulling job specific data for job with JobId {jobId}.", e);
                    this.ModifyJobProcessingStatus(ProcessingStatus.Invalid, jobId.Value);
                    return false;
                }

                if (job == null)
                {
                    // Job wasn't found in its specific table. We'll mark the entry in the BACKGROUND_JOBS table as invalid and log some error.
                    Tools.LogError($"[BackgroundJobProcessor] Job with JobId {jobId} not found.");
                    this.ModifyJobProcessingStatus(ProcessingStatus.Invalid, jobId.Value);
                    return false;
                }

                string jobDescription = job.IdentifierString;
                using (var itemTimer = timer.RecordItem(jobDescription, job.DateCreated))
                {
                    try
                    {
                        DateTime jobStart = DateTime.Now;
                        Stopwatch sw = Stopwatch.StartNew();

                        // Set logging context using job info.
                        LoggingContext.CreateNewThreadContext();
                        LoggingContext.Current.LoanId = job.LoanId;
                        LoggingContext.Current.CorrelationId = job.CorrelationId;

                        if (job.UserId.HasValue)
                        {
                            LoggingContext.Current.UserName = Tools.GetUserNameByUserId(job.UserId.Value, job.BrokerId.Value);
                        }

                        // Job has been found and it's been marked as taken already when it was first pulled from the queue. We can go ahead and process it.
                        job.RunJob();
                        sw.Stop();
                        Tools.LogInfo($"[BackgroundJobProcessor] {jobDescription}. Start at {jobStart.ToString()} and executed in {sw.ElapsedMilliseconds.ToString("#,#")} ms.");

                        return true;
                    }
                    catch (Exception e)
                    {
                        job.IncrementExceptionCounter();

                        Tools.LogError(e);
                        itemTimer.RecordError(e);

                        // For jobs that error, we want to check if it has reached its expiration date or if the exception count has reached the max attempt limit. If so, invalidate it. Otherwise, let it retry.
                        if (DateTime.Now > job.ExpirationDate || 
                            (job.MaxExceptionCount.HasValue && job.ExceptionCount >= job.MaxExceptionCount.Value))
                        {
                            Tools.LogError($"[BackgroundJobProcessor] {jobDescription} threw an exception and has expired or reached the exception limit.");
                            this.ModifyJobProcessingStatus(ProcessingStatus.Invalid, job.JobId.Value);
                        }
                        else if (e is IrrecoverableBackgroundJobException)
                        {
                            Tools.LogError($"[BackgroundJobProcessor] {jobDescription} encountered an irrecoverable error: {e.Message}");
                            this.ModifyJobProcessingStatus(ProcessingStatus.Invalid, job.JobId.Value);
                        }
                        else
                        {
                            var nextRunDate = job.DetermineNextRunDate(DateTime.Now);
                            SqlParameter[] paramters = new SqlParameter[]
                            {
                                new SqlParameter("@JobId", job.JobId.Value),
                                new SqlParameter("@NextRunDate", nextRunDate),
                                new SqlParameter("@ExceptionCount", job.ExceptionCount)
                            };

                            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
                            {
                                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, BackgroundJobsResetOnError, paramters, TimeoutInSeconds.Thirty);
                            }

                            Tools.LogInfo($"[BackgroundJobProcessor] {jobDescription} added back to queue due to error. Next run date: {nextRunDate.ToString()}.");
                        }

                        return false;
                    }
                    finally
                    {
                        LoggingContext.ClearThreadContext();
                    }
                }
            }
            else
            {
                // No job found in the main BACKGROUND_JOBS table. We can quit out.
                return false;
            }
        }

        /// <summary>
        /// Looks for the job in its specific table based on the JobType.
        /// </summary>
        /// <param name="jobId">The job id.</param>
        /// <param name="jobType">The job type.</param>
        /// <returns>The background job if found. Null if not.</returns>
        private BackgroundJob GetJob(int jobId, JobType jobType)
        {
            switch (jobType)
            {
                case JobType.SeamlessLpaPolling:
                    return SeamlessLpaPollJob.GetSeamlessLpaPollJob(jobId);
                case JobType.CustomReport:
                    return CustomReportJob.GetCustomReportBackgroundJob(jobId);
                case JobType.TaskAutomation:
                    return TaskAutomationJob.GetTaskAutomationJob(jobId);
                case JobType.MIFrameworkRequest:
                    return MIFrameworkRequestJob.GetMIFrameworkRequestJob(jobId);
                case JobType.CreditReportRequest:
                    return CreditReportRequestJob.GetCreditReportRequestJob(jobId);
                case JobType.PdfGenerationJob:
                    return PdfGenerationJob.GetPdfGenerationJob(jobId);
                case JobType.VoaJob:
                    return VOA.VOAJob.GetJob(jobId);
                default:
                    throw new UnhandledEnumException(jobType);
            }
        }

        /// <summary>
        /// Pulls a job from the BACKGROUND_JOBS DB table and marks it as <see cref="ProcessingStatus.Processing"/> if found.
        /// </summary>
        /// <param name="jobId">The job id of the job. Null if not found.</param>
        /// <param name="jobType">The job type of the job. Null if not found.</param>
        /// <returns>True if a job is found. False otherwise.</returns>
        private bool PullJobAndMark(out int? jobId, out JobType? jobType)
        {
            List<SqlParameter> paramters = new List<SqlParameter>()
            {
                new SqlParameter("@CurrentDate", DateTime.Now)
            };

            int? jobTypeMask = this.GetJobTypeMask();
            if (jobTypeMask != null)
            {
                paramters.Add(new SqlParameter("@JobTypeMask", jobTypeMask));
            }

            if (ConstAppDavid.CurrentServerLocation.EqualsOneOf(ServerLocation.Development, ServerLocation.LocalHost)
                && !string.IsNullOrEmpty(BackgroundJob.DevJobFilter))
            {
                paramters.Add(new SqlParameter("@Filter", BackgroundJob.DevJobFilter));
            }

            bool jobFound = false;
            jobId = null;
            jobType = null;

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(conn, null, BackgroundJobsGetNextJobAndMarkSp, paramters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    jobFound = true;
                    jobId = (int)reader["JobId"];
                    jobType = (JobType)reader["JobType"];
                }
            }

            return jobFound;
        }

        /// <summary>
        /// Generates the Job Type Mask from ConstSite.BackgroundJobTypeFilter.
        /// </summary>
        /// <returns>Null if ConstSite.BackgroundJobTypeFilter is null or invalid. An int bitmask of the parsed values, otherwise.</returns>
        private int? GetJobTypeMask()
        {
            if (string.IsNullOrWhiteSpace(ConstSite.BackgroundJobTypeFilter))
            {
                return null;
            }

            int mask = 0;
            string[] typeStrings = ConstSite.BackgroundJobTypeFilter.Split(',');
            foreach (string typeString in typeStrings)
            {
                JobType type;
                if (!typeString.TryParseDefine(out type, ignoreCase: true))
                {
                    Tools.LogWarning($"[BackgroundJobProcessor.GetJobTypeMask] ConstSite.BackgroundJobTypeFilter is invalid. ConstSite.BackgroundJobTypeFilter=[{ConstSite.BackgroundJobTypeFilter}].");
                    return null;
                }

                mask |= 1 << (int)type;
            }

            if (mask == 0)
            {
                Tools.LogWarning($"[BackgroundJobProcessor.GetJobTypeMask] Mask calculated to 0, which is not valid. ConstSite.BackgroundJobTypeFilter=[{ConstSite.BackgroundJobTypeFilter}].");
                return null;
            }

            return mask;
        }

        /// <summary>
        /// Changes the ProcessingStatus of the background job.
        /// </summary>
        /// <param name="status">The status to change to.</param>
        /// <param name="jobId">The job id.</param>
        private void ModifyJobProcessingStatus(ProcessingStatus status, int jobId)
        {
            SqlParameter[] paramters = new SqlParameter[]
            {
                new SqlParameter("@ProcessingStatus", status),
                new SqlParameter("@JobId", jobId)
            };

            using (var conn = DbAccessUtils.GetConnection(DataSrc.LOShare))
            {
                StoredProcedureDriverHelper.ExecuteNonQuery(conn, null, BackgroundJobsChangeProcessingStatusSp, paramters, TimeoutInSeconds.Thirty);
            }
        }
    }
}
