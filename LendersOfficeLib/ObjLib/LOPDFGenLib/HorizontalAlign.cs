using System;

namespace LOPDFGenLib
{
	public enum HorizontalAlign
	{
        Left,
        Center,
        Right
	}
}
