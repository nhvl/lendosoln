using System;
using System.Drawing;

namespace LOPDFGenLib
{
	public abstract class AbstractItem
	{
        // If point.x = 0 then use the global x position, if point.y = 0 then use global y.
        // If point is not zero than use either x, or y coordinate base on relative position.
        private Point m_location = Point.Empty; 

        public Point Location
        {
            get { return m_location; }
            set { m_location = value; }
        }

        public abstract void Draw(iTextSharp.text.pdf.PdfWriter pdfWriter);
        public abstract Size GetPreferredSize(iTextSharp.text.pdf.PdfWriter pdfWriter);
	}
}
