using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace LOPDFGenLib
{

	public class RowItem : AbstractItem
	{
        private List<AbstractItem> m_list = new List<AbstractItem>();

        public RowItem() {}
		public RowItem(int x, int y)
		{
            Location = new Point(x, y);
		}

        public void Add(AbstractItem item) 
        {
            if (null != item) 
            {
                m_list.Add(item);
            }
        }
        public override void Draw(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            foreach (AbstractItem obj in m_list)
            {
                obj.Location = new Point(obj.Location.X, this.Location.Y);
                obj.Draw(pdfWriter);
            }
        }

        public override Size GetPreferredSize(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            int maxHeight = 0;
            foreach (AbstractItem obj in m_list)
            {
                int h = obj.GetPreferredSize(pdfWriter).Height;
                maxHeight = maxHeight < h ? h : maxHeight;

            }
            return new Size(0, maxHeight);
        }
	}
}
