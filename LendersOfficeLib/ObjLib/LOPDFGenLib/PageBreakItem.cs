using System;
using System.Drawing;

namespace LOPDFGenLib
{
	public class PageBreakItem : AbstractItem
	{
		public PageBreakItem()
		{
            Location = new Point(0, int.MaxValue);
		}

        public override void Draw(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {

        }

        public override Size GetPreferredSize(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            return new Size(0, 0);

        }
    }
}
