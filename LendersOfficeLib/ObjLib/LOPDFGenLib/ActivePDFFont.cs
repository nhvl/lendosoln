using System;
using iTextSharp.text.pdf;

namespace LOPDFGenLib
{
    public enum FontName 
    {
        Helvetica,
        HelveticaBold,
        HelveticaItalic,
        TimesRomanTimesItalic,
        TimesBold,
        Courier,
        CourierOblique,
        CourierBold,
        Symbol,
        ZapfDingbats


    }
	/// <summary>
	/// Summary description for ActivePDFFont.
	/// </summary>
	public class ActivePDFFont
	{
        private FontName m_font;

		public ActivePDFFont(FontName font, short size)
		{
            m_font = font;
            Size = size;
		}
        public short Size { get; private set;}

        public string Name 
        {
            get 
            {
                switch (m_font) 
                {
                    case FontName.Helvetica: return "Helvetica";
                    case FontName.HelveticaBold: return "Helvetica-Bold";
                    case FontName.HelveticaItalic: return "Helvetica-Italic";
                    case FontName.TimesRomanTimesItalic: return "Times-Roman Times-Italic";
                    case FontName.TimesBold: return "Times-Bold";
                    case FontName.Courier: return "Courier";
                    case FontName.CourierOblique: return "Courier-Oblique";
                    case FontName.CourierBold: return "Courier-Bold";
                    case FontName.Symbol: return "Symbol";
                    case FontName.ZapfDingbats: return "ZapfDingbats";
                }

                return "";
            }
        }
        public string iTextSharpFont
        {
            get
            {

                switch (m_font)
                {
                    case FontName.Helvetica:
                        return BaseFont.HELVETICA;
                    case FontName.HelveticaBold:
                        return BaseFont.HELVETICA_BOLD;
                    case FontName.HelveticaItalic:
                        return BaseFont.HELVETICA_OBLIQUE;
                    case FontName.TimesRomanTimesItalic:
                        return BaseFont.TIMES_ROMAN;
                    case FontName.TimesBold:
                        return BaseFont.TIMES_BOLD;
                    case FontName.Courier:
                        return BaseFont.COURIER;
                    case FontName.CourierOblique:
                        return BaseFont.COURIER_OBLIQUE;
                    case FontName.CourierBold:
                        return BaseFont.COURIER_BOLD;
                    case FontName.Symbol:
                        return BaseFont.SYMBOL;
                    case FontName.ZapfDingbats:
                        return BaseFont.ZAPFDINGBATS;
                    default:
                        return BaseFont.HELVETICA;
                }
            }
        }
        /// <summary>
        /// Return the height for this font.
        /// </summary>
        public int Height 
        {
            get 
            {
                return (int) Size + (int) (Size / 10);
            }

        }
	}
}
