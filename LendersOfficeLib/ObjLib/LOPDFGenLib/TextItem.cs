using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using iTextSharp.text.pdf;
using LendersOffice.Constants;
using System.IO;
using LendersOffice.Common;

namespace LOPDFGenLib
{
	public class TextItem : AbstractItem
	{
        private List<string> m_lines = new List<string>();

        public TextItem(string text, int x, int y) : this(text, x, y, FontName.Helvetica, 16)
        {
        }

        public TextItem(string text, int x, int y, FontName fontName, short fontSize) : this (text, x, y, 0, fontName, fontSize)
        {
        }
        public TextItem(string text, int x, int y, int width, FontName fontName, short fontSize) : this (text,x ,y, width, fontName, fontSize, HorizontalAlign.Left) 
        {
        }
        public TextItem(string text, int x, int y, int width, FontName fontName, short fontSize, HorizontalAlign hAlign) 
        {
            m_font = new ActivePDFFont(fontName, fontSize);
            m_text = text;
            m_width = width;
            Location = new Point(x, y);
            m_horizontalAlign = hAlign;

        }
        private HorizontalAlign m_horizontalAlign = HorizontalAlign.Left;
        private VerticalAlign m_verticalAlign = VerticalAlign.Top;
        private int m_width;
        private ActivePDFFont m_font;
        private string m_text;


        public IEnumerable<string> Lines 
        {
            get { return m_lines; }
        }
        public string Text
        {
            get { return m_text; }
            set { m_text = value; }
        }

        public int Width
        {
            get { return m_width; }
            set { m_width = value; }
        }

        public VerticalAlign VerticalAlign
        {
            get { return m_verticalAlign; }
            set { m_verticalAlign = value; }
        }

        public HorizontalAlign HorizontalAlign
        {
            get { return m_horizontalAlign; }
            set { m_horizontalAlign = value; }
        }

        public Size GetPreferredSize()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                iTextSharp.text.Document document = new iTextSharp.text.Document();
                PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                document.Open();
                return GetPreferredSize(pdfWriter);
            }
        }

        public override Size GetPreferredSize(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            var pdfContentByte = pdfWriter.DirectContent;
            BaseFont font = BaseFont.CreateFont(m_font.iTextSharpFont, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
            pdfContentByte.SetFontAndSize(font, (float)m_font.Size);

            int h = m_font.Height + 5;
            int w = (int)pdfContentByte.GetEffectiveStringWidth(m_text, false);

            bool hasLineBreak = m_text.IndexOf(Environment.NewLine) != -1 || m_text.IndexOf('\n') != -1;

            if (hasLineBreak || (w > m_width && m_width != 0))
            {
                // Do word wraping.
                w = m_width;
                WrapString(pdfWriter);
                h = (m_font.Height) * m_lines.Count + 5;
            }
            else
            {
                m_lines.Add(m_text);
            }
            return new Size(w, h);

        }
        private void WrapString(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            var pdfContentByte = pdfWriter.DirectContent;
            StringBuilder word = new StringBuilder(10);

            if (m_text == null || m_text.TrimWhitespaceAndBOM() == "") return; // Do nothing on empty string

            int length = m_text.Length;
            StringBuilder line = new StringBuilder(80);

            for (int i = 0; i < length; i++)
            {

                char ch = m_text[i];
                //                PaulBunyanHelper.Log("ch = " + ch + ", int = " + (int) ch);
                if (ch == ' ')
                {
                    int w = (int)pdfContentByte.GetEffectiveStringWidth(line.ToString() + word.ToString(), false);
                    //DataAccess.Tools.LogError("W = " + w + " for [" + line.ToString() + word.ToString() + "], m_width=" + m_width );
                    if (w > m_width)
                    {
                        // Line exceed the width, wrap for this word.
                        m_lines.Add(line.ToString());
                        line = new StringBuilder(80);
                    }
                    line.Append(word).Append(" "); // Append word + the space.
                    word = new StringBuilder(80);
                }
                else if (ch == Environment.NewLine[0] || ch == '\n')
                {
                    m_lines.Add(line.Append(word).ToString());
                    line = new StringBuilder(80);
                    word = new StringBuilder(80);
                }
                else
                {
                    word.Append(ch);
                }

            }
            if (line.Length + word.Length > 0)
            {
                m_lines.Add(line.ToString() + word.ToString());
            }

        }



        public override void Draw(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            var pdfContentByte = pdfWriter.DirectContent;
            pdfContentByte.BeginText();
            BaseFont font = BaseFont.CreateFont(m_font.iTextSharpFont, BaseFont.WINANSI, BaseFont.NOT_EMBEDDED);
            pdfContentByte.SetFontAndSize(font, (float)m_font.Size);

            short y = (short)Location.Y;
            foreach (string str in m_lines)
            {
                //                PaulBunyanHelper.Log("Text = " + str + ", Point = " + Location);

                short x = (short)Location.X;
                if (m_horizontalAlign == HorizontalAlign.Right && m_width > 0)
                {
                    x = (short)(x + (short)m_width - (short)pdfContentByte.GetEffectiveStringWidth(str, false));
                }
                else if (m_horizontalAlign == HorizontalAlign.Center && m_width > 0)
                {
                    x = (short)((x + m_width - pdfContentByte.GetEffectiveStringWidth(str, false)) / 2);
                }
                pdfContentByte.SetTextMatrix((float)x, (float)(pdfWriter.PageSize.Height - y));
                pdfContentByte.ShowText(str);
                y += (short)m_font.Height;
            }
            pdfContentByte.EndText();
        }


	}
}
