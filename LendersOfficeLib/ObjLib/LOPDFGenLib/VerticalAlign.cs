using System;

namespace LOPDFGenLib
{
	public enum VerticalAlign
	{
        Top,
        Middle,
        Bottom
	}
}
