using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Xml;

using iTextSharp.text.pdf;
using System.IO;
using DataAccess;
using LendersOffice.Constants;

namespace LOPDFGenLib
{
	public class PDFGenerator
	{
        private PageOrientation m_orientation = PageOrientation.Portrait;
        private PaperSize m_size = PaperSize.Legal;
        private IEnumerable<AbstractItem> m_list = null;
        private short m_secondPageTopMargin = 40;

        public bool IsPrintPoint { get; set;}

        public PDFGenerator(IEnumerable<AbstractItem> list)
        {
            m_list = list;
        }

        private void PrintPoint() 
        {


        }
        public byte[] GeneratePDF(string ownerPassword, string userPassword) 
        {
            return GeneratePDFUsingITextSharp(ownerPassword, userPassword);
        }

        private byte[] GeneratePDFUsingITextSharp(string ownerPassword, string userPassword)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                GeneratePDFUsingITextSharpImpl(stream, ownerPassword, userPassword);
                return stream.ToArray();
            }
        }
        private void GeneratePDFUsingITextSharpImpl(Stream stream, string ownerPassword, string userPassword)
        {
            float width = 0;
            float height = 0;
            short bottomMargin = 50;

            if (m_size == PaperSize.Letter)
            {
                width = m_orientation == PageOrientation.Portrait ? Constants.LetterWidth : Constants.LetterHeight;
                height = m_orientation == PageOrientation.Portrait ? Constants.LetterHeight : Constants.LetterWidth;
            }
            else if (m_size == PaperSize.Legal)
            {
                width = m_orientation == PageOrientation.Portrait ? Constants.LegalWidth : Constants.LegalHeight;
                height = m_orientation == PageOrientation.Portrait ? Constants.LegalHeight : Constants.LegalWidth;

            }


            iTextSharp.text.Document document = new iTextSharp.text.Document(new iTextSharp.text.Rectangle(width, height));
            int pageNum = 1;

            PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
            if (!string.IsNullOrEmpty(ownerPassword) || !string.IsNullOrEmpty(userPassword))
            {
                if (string.IsNullOrEmpty(ownerPassword))
                {
                    ownerPassword = userPassword;
                }
                pdfWriter.SetEncryption(PdfWriter.STANDARD_ENCRYPTION_128, userPassword, ownerPassword, PdfWriter.AllowPrinting);
            }
            document.Open();
            int newY = 0;
            WritePageNumber(pdfWriter, pageNum++);
            foreach (AbstractItem obj in m_list)
            {
                Point p = obj.Location;

                if (p.Y == 0 && newY != 0)
                {
                    obj.Location = new Point(p.X, newY);
                }
                else
                {
                    newY = p.Y;
                }
                Size size = obj.GetPreferredSize(pdfWriter);
                if (newY + size.Height > height - bottomMargin)
                {
                    // Row list flow to new page.
                    document.NewPage();
                    WritePageNumber(pdfWriter, pageNum++);

                    newY = m_secondPageTopMargin + size.Height;
                    obj.Location = new Point(p.X, newY);
                }
                obj.Draw(pdfWriter);
                newY += size.Height;
            }


            document.Close();
            pdfWriter.Close();

        }
        private void WritePageNumber(PdfWriter writer, int pageNum)
        {
            BaseFont font = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.WINANSI, BaseFont.EMBEDDED);
            var pdfContentByte = writer.DirectContent;
            pdfContentByte.BeginText();
            pdfContentByte.SetFontAndSize(font, (float)8);

            pdfContentByte.SetTextMatrix((float)(writer.PageSize.Width / 2 - 30), (float)20);
            pdfContentByte.ShowText("Page " + pageNum);
            pdfContentByte.EndText();

        }

	}
}
