using System;

namespace LOPDFGenLib
{
	public enum PageOrientation
	{
        Portrait,
        Landscape
	}
}
