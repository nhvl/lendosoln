using System;

namespace LOPDFGenLib
{
	public class Constants
	{
        public const short LetterWidth = 612; // 8.5" in term of pixels
        public const short LetterHeight = 792; // 11" in term of pixels
        public const short LegalWidth = 612; // 8.5" in term of pixels
        public const short LegalHeight = 1008; // 14" in term of pixels
	}
}
