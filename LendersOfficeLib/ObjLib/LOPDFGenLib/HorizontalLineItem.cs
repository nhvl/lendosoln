using System;
using System.Drawing;
using iTextSharp.text;

namespace LOPDFGenLib 
{
	public class HorizontalLineItem : AbstractItem
	{
        private int m_leftMargin = 40;
        private int m_rightMargin = 40;

		public HorizontalLineItem()
		{
		}
        public HorizontalLineItem(int x, int y) 
        {
            Location = new Point(x, y);
        }

        public override void Draw(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            var pdfContentByte = pdfWriter.DirectContent;
            pdfContentByte.SaveState();
            pdfContentByte.SetColorStroke(BaseColor.BLACK);
            pdfContentByte.SetLineWidth(.1f);
            pdfContentByte.MoveTo(m_leftMargin, (float)(pdfWriter.PageSize.Height - (float)Location.Y + 8));
            pdfContentByte.LineTo(pdfWriter.PageSize.Width - (float)m_rightMargin, (float)(pdfWriter.PageSize.Height - (float)Location.Y + 8));
            pdfContentByte.Stroke();
            pdfContentByte.RestoreState();
        }

        public override Size GetPreferredSize(iTextSharp.text.pdf.PdfWriter pdfWriter)
        {
            return new Size((int)pdfWriter.PageSize.Width, 15);
        }
    }
}
