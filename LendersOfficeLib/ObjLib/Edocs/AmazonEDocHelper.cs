﻿// <copyright file="AmazonEDocHelper.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: David D
// Date: 5/3/2016
// </summary>
namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Web;
    using DataAccess;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Security;
    using Utils;

    /// <summary>
    /// A list of helper methods that communicate with Edocs hosting on Amazon.
    /// </summary>
    public static class AmazonEDocHelper
    {
        /// <summary>
        /// Generate a token to view shipping package.
        /// </summary>
        /// <param name="principal">User principal making request.</param>
        /// <param name="outputType">The output type. Valid values are pdf or zip.</param>
        /// <param name="documentList">A list of documents to include in shipping package.</param>
        /// <returns>Amazon token.</returns>
        public static string GenerateShippingPackageRequest(AbstractUserPrincipal principal, string outputType, IEnumerable<EDocument> documentList)
        {
            string op = string.Empty;

            if (outputType == "pdf")
            {
                op = "GeneratePdfShippingPackageRequest";
            }
            else if (outputType == "zip")
            {
                op = "GenerateZipShippingPackageRequest";
            }
            else if (outputType == "zip_mergewithoutsig")
            {
                op = "GenerateZipMergingNoSigDocsShippingPackageRequest";
            }
            else
            {
                throw CBaseException.GenericException("Unsupported output type=[" + outputType + "]");
            }
            
            if (documentList == null || !documentList.Any())
            {
                throw CBaseException.GenericException("Document list is null or empty.");
            }

            List<AwsModels.AwsEDocsDocument> list = new List<AwsModels.AwsEDocsDocument>();

            foreach (var o in documentList)
            {
                if (o == null || o.EdocsMetadata == null)
                {
                    throw CBaseException.GenericException($"Document {o?.DocumentId} is null or missing metadata.");
                }

                AwsModels.AwsEDocsDocument document = new AwsModels.AwsEDocsDocument();
                document.DocTypeName = o.DocTypeName;
                document.PublicDescription = o.PublicDescription;
                document.IsESigned = o.IsESigned;
                document.Pages = new List<AwsModels.AwsEDocsPage>();

                foreach (var p in o.EdocsMetadata.Pages)
                {
                    AwsModels.AwsEDocsPage page = new AwsModels.AwsEDocsPage();
                    page.ReferenceDocumentId = p.ReferenceDocumentId;
                    page.ReferenceDocumentPageNumber = p.ReferenceDocumentPageNumber;
                    page.Rotation = p.Rotation;

                    document.Pages.Add(page);
                }

                list.Add(document);
            }

            string json = SerializationHelper.JsonNetSerialize(list);

            string clientIdentifier = GetClientIdentifier();
            string clientIp = RequestHelper.ClientIP;
            string hostName = string.Empty;
            string userAgent = string.Empty;

            if (HttpContext.Current != null)
            {
                hostName = HttpContext.Current.Request.Url.Host;
                userAgent = HttpContext.Current.Request.UserAgent;
            }

            return GenerateAmazonToken(principal, op, json, hostName, clientIdentifier, clientIp, userAgent);
        }

        /// <summary>
        /// Generate a token to view pdf on Amazon.
        /// </summary>
        /// <param name="principal">User principal making request.</param>
        /// <param name="doc">LendingQB eDoc object.</param>
        /// <param name="useOriginalVersion">Whether to render the original or current version of PDF.</param>
        /// <returns>Amazon token.</returns>
        public static string GenerateViewPdfRequest(AbstractUserPrincipal principal, EDocument doc, bool useOriginalVersion)
        {
            EdocsMetadata metaData = useOriginalVersion ? doc.OriginalEdocsMetadata : doc.EdocsMetadata;
            if (metaData == null)
            {
                throw CBaseException.GenericException("Edocs metadata is null. useOriginalVersion=" + useOriginalVersion);
            }

            return GenerateViewPdfRequest(principal, doc, metaData);
        }

        /// <summary>
        /// Generate a token to view pdf on Amazon.
        /// </summary>
        /// <param name="principal">User principal making request.</param>
        /// <param name="doc">LendingQB eDoc object.</param>
        /// <param name="metaData">The metadata for the request.</param>
        /// <returns>Amazon token.</returns>
        public static string GenerateViewPdfRequest(AbstractUserPrincipal principal, EDocument doc, EdocsMetadata metaData)
        {
            AwsModels.AwsEDocsDocument document = new AwsModels.AwsEDocsDocument();
            document.DocTypeName = doc.DocTypeName;
            document.Pages = new List<AwsModels.AwsEDocsPage>();
            document.IsESigned = doc.IsESigned;

            if (metaData == null)
            {
                throw CBaseException.GenericException("Edocs metadata is null.");
            }

            foreach (var page in metaData.Pages)
            {
                AwsModels.AwsEDocsPage p = new AwsModels.AwsEDocsPage();
                p.ReferenceDocumentId = page.ReferenceDocumentId;
                p.ReferenceDocumentPageNumber = page.ReferenceDocumentPageNumber;
                p.Rotation = page.Rotation;

                document.Pages.Add(p);
            }

            return GenerateViewPdfRequest(principal, document);
        }

        /// <summary>
        /// Generate a token to view pdf on Amazon.
        /// </summary>
        /// <param name="principal">User principal making request.</param>
        /// <param name="docTypeName">Document type name.</param>
        /// <param name="pageList">A list of pdf pages.</param>
        /// <param name="annotationList">A list of annotations.</param>
        /// <returns>Amazon token.</returns>
        public static string GenerateViewPdfRequest(AbstractUserPrincipal principal, string docTypeName, IEnumerable<PdfPageItem> pageList, IEnumerable<EDocumentAnnotationItem> annotationList)
        {
            AwsModels.AwsEDocsDocument document = new AwsModels.AwsEDocsDocument();
            document.Pages = new List<AwsModels.AwsEDocsPage>();
            document.Annotations = new List<AwsModels.AwsEDocsAnnotation>();
            document.DocTypeName = docTypeName;

            foreach (var o in pageList)
            {
                PNGEncryptedData encryptedData = new PNGEncryptedData(o.PngKey);
                
                AwsModels.AwsEDocsPage p = new AwsModels.AwsEDocsPage();
                p.ReferenceDocumentId = encryptedData.DocumentId;
                p.ReferenceDocumentPageNumber = encryptedData.PageNumber;
                p.Rotation = o.Rotation;

                document.Pages.Add(p);
            }

            if (annotationList != null)
            {
                foreach (var o in annotationList)
                {
                    AwsModels.AwsEDocsAnnotation annotation = new AwsModels.AwsEDocsAnnotation();

                    annotation.Bottom = o.Rectangle.Bottom;
                    annotation.CreatedBy = o.CreatedBy;
                    annotation.CreatedDate = o.CreatedDate;
                    annotation.Height = o.Rectangle.Height;
                    annotation.Id = o.Id;
                    annotation.LastModifiedBy = o.LastModifiedBy;
                    annotation.LastModifiedDate = o.LastModifiedDate;
                    annotation.Left = o.Rectangle.Left;
                    annotation.NoteState = o.NoteStateType;
                    annotation.Page = o.PageNumber - 1; // Amazon use 0-index for page.
                    annotation.Text = o.Text;
                    annotation.AnnotationType = o.ItemType;
                    annotation.Width = o.Rectangle.Width;

                    document.Annotations.Add(annotation);
                }
            }

            return GenerateViewPdfRequest(principal, document);
        }

        /// <summary>
        /// Generate a token to upload a PDF to Amazon with the document id as name.
        /// </summary>
        /// <param name="principal">User principal making request.</param>
        /// <param name="originalDocumentId">Name to be store on Amazon size.</param>
        /// <returns>Amazon token.</returns>
        public static string GenerateUploadPdfRequest(AbstractUserPrincipal principal, Guid originalDocumentId)
        {
            var json = SerializationHelper.JsonNetAnonymousSerialize(new { document_id = originalDocumentId });

            return GenerateAmazonToken(principal, "GenerateUploadPdfRequest", json, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        /// <summary>
        /// Upload the original PDF to Amazon.
        /// </summary>
        /// <param name="brokerId">The id of broker of the original pdf.</param>
        /// <param name="originalDocumentId">The id of the original pdf.</param>
        public static void UploadOriginalPdfToAmazon(Guid brokerId, Guid originalDocumentId)
        {
            // This method will be call by our internal background process such as Pdf Rasterizer Processor or migration process.
            // Therefore use a generic system principal.
            AbstractUserPrincipal principal = SystemUserPrincipal.TaskSystemUser;

            int numberOfTries = 3;
            bool isUploadSuccessful = false;

            for (int i = 0; i < numberOfTries; i++)
            {
                try
                {
                    isUploadSuccessful = false;
                    string token = GenerateUploadPdfRequest(principal, originalDocumentId);

                    using (WebClientWithTimeout webClient = new WebClientWithTimeout())
                    {
                        webClient.Timeout = 600000; // Timeout in 10 minutes.
                        string url = ConstStage.AmazonEdocsClientUrl + "/edocupload.aspx?key=" + token;
                        try
                        {
                            FileDBTools.UseFile(E_FileDB.EdmsPdf, originalDocumentId.ToString(), fileInfo => webClient.UploadFile(url, fileInfo.FullName));
                        }
                        catch (FileNotFoundException)
                        {
                            FileDBTools.UseFile(E_FileDB.EDMS, originalDocumentId.ToString(), fileInfo => webClient.UploadFile(url, fileInfo.FullName));
                        }
                    }

                    isUploadSuccessful = true;
                    break;
                }
                catch (WebException exc)
                {
                    if ((i + 1) < numberOfTries)
                    {
                        Tools.LogWarning("Unable to upload pdf to AWS. Try #" + i, exc);
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            if (isUploadSuccessful)
            {
                // Mark file as upload to Amazon.
                SqlParameter[] parameters = 
                    {
                    new SqlParameter("@BrokerId", brokerId),
                    new SqlParameter("@DocumentId", originalDocumentId)
                    };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_DOCUMENT_ORIGINAL_MarkAwsUploaded", 2, parameters);
            }
        }

        /// <summary>
        /// Retrieves a PDF from Amazon.
        /// </summary>
        /// <param name="token">
        /// The token to retrieve the PDF.
        /// </param>
        /// <returns>
        /// The path to the PDF file.
        /// </returns>
        public static string RetrievePdf(string token)
        {
            var localFilePath = TempFileUtils.NewTempFile().Value + ".pdf";

            using (var webClient = new WebClientWithTimeout())
            {
                webClient.Timeout = 120000; // Timeout in 2 minutes.

                var requestUrl = ConstStage.AmazonEdocsClientUrl + "/edocpdf.aspx?key=" + token;
                webClient.DownloadFile(requestUrl, localFilePath);
            }

            return localFilePath;
        }

        /// <summary>
        /// Generate a token to view pdf on Amazon.
        /// </summary>
        /// <param name="principal">User principal making request.</param>
        /// <param name="document">Meta data of the document.</param>
        /// <returns>Amazon token.</returns>
        private static string GenerateViewPdfRequest(AbstractUserPrincipal principal, AwsModels.AwsEDocsDocument document)
        {
            string json = SerializationHelper.JsonNetSerialize(document);

            string clientIdentifier = GetClientIdentifier();
            string clientIp = RequestHelper.ClientIP;

            string hostName = string.Empty;
            string userAgent = string.Empty;

            if (HttpContext.Current != null)
            {
                hostName = HttpContext.Current.Request.Url.Host;
                userAgent = HttpContext.Current.Request.UserAgent;
            }

            return GenerateAmazonToken(principal, "GenerateViewPdfRequest", json, hostName, clientIdentifier, clientIp, userAgent);
        }

        /// <summary>
        /// Gets the unique id of the client from fingerprint cookie.
        /// </summary>
        /// <returns>A unique id of the client.</returns>
        private static string GetClientIdentifier()
        {
            if (HttpContext.Current == null)
            {
                return Guid.NewGuid().ToString();
            }

            var cookie = HttpContext.Current.Request.Cookies[ConstApp.CookieFingerPrint];

            if (cookie == null)
            {
                return Guid.NewGuid().ToString();
            }
            else
            {
                return cookie.Value;
            }
        }

        /// <summary>
        /// Generate a token to Amazon.
        /// </summary>
        /// <param name="principal">User principal making request.</param>
        /// <param name="op">Operation on Amazon.</param>
        /// <param name="jsonData">Json data to send to Amazon.</param>
        /// <param name="requestApplication">Application initiates the request (LendingQB, PML, etc...).</param>
        /// <param name="clientIdentifier">A unique id of the client that generate the request.</param>
        /// <param name="clientIp">IP address of the client.</param>
        /// <param name="clientUserAgent">User agent from client's browser.</param>
        /// <returns>Amazon token.</returns>
        private static string GenerateAmazonToken(AbstractUserPrincipal principal, string op, string jsonData, string requestApplication, string clientIdentifier, string clientIp, string clientUserAgent)
        {
            if (principal == null)
            {
                throw CBaseException.GenericException("principal cannot be null.");
            }

            string url = ConstStage.AmazonEdocsApiUrl + "?op=" + op;

            using (System.Net.WebClient webClient = new System.Net.WebClient())
            {
                string customerCode = string.Empty;

                if (principal.BrokerDB != null)
                {
                    customerCode = principal.BrokerDB.CustomerCode;
                }

                webClient.Headers.Add("LendingQB-CustomerCode", customerCode);
                webClient.Headers.Add("LendingQB-User", principal.LoginNm);
                webClient.Headers.Add("LendingQB-AWS-API-Secret", ConstStage.AmazonApiSecretKey);
                webClient.Headers.Add("LendingQB-ClientIdentifier", clientIdentifier);
                webClient.Headers.Add("LendingQB-RequestApp", requestApplication);
                webClient.Headers.Add("LendingQB-ClientIP", clientIp);
                webClient.Headers.Add("LendingQB-ClientUserAgent", clientUserAgent);
                string data = webClient.UploadString(url, jsonData);
                return data.Substring(1, data.Length - 2);
            }
        }

        /// <summary>
        /// WebClient extension that support longer timeout.
        /// </summary>
        private class WebClientWithTimeout : WebClient
        {
            /// <summary>
            /// Gets or sets the timeout in milliseconds.
            /// </summary>
            /// <value>The timeout in milliseconds.</value>
            public int Timeout { get; set; }

            /// <summary>
            /// Create the web request.
            /// </summary>
            /// <param name="address">The request address.</param>
            /// <returns>A web request object.</returns>
            protected override WebRequest GetWebRequest(Uri address)
            {
                var request = base.GetWebRequest(address);
                request.Timeout = this.Timeout;
                return request;
            }
        }
    }
}