﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EDocs.UI
{
    public class ShippingReviewView
    {
        public string Folder { get; set; }
        public string DocType { get; set; }
        public string InternalDescription { get; set; }
        public string Description { get; set; }
        public string LastModified { get; set; }
        public int PageCount { get; set; }
        public E_ShippingReviewEntryStatus Status { get; set; }
        public Guid? Id { get; set; }
        public string Application { get; set; }
        public bool HasOwnerPassword { get; set; }
        public bool PdfHasUnsupportedFeatures { get; set; }
        public E_EDocStatus DocStatus { get; set; }
        public bool IsESigned { get; set; }
    }

    public enum E_ShippingReviewEntryStatus
    {
        OK,
        MISSING,
        DUPLICATE
    }
}
