﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LendersOffice.ObjLib.Edocs
{
    public interface IDocVendorDocType
    {
        string Description { get; }
        int Id { get; }
    }
}
