﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using DataAccess;
using LendersOffice.Common;
using LendersOffice.Drivers.Encryption;
using LqbGrammar;
using LqbGrammar.DataTypes;
using LqbGrammar.Drivers;

namespace EDocs
{
    public class EDocsFaxNumber
    {
        private EncryptionKeyIdentifier? encryptionKeyId;
        private Lazy<string> lazyDocRouterPassword;

        public string FaxNumber { get; set; }
        public string DocRouterLogin { get; set; }
        public string DocRouterPassword
        {
            get
            {
                return this.lazyDocRouterPassword?.Value ?? string.Empty;
            }

            set
            {
                this.lazyDocRouterPassword = new Lazy<string>(() => value);
            }
        }

        public bool HasNumber { get; private set; }
        public bool IsSharedLine { get; private set; }
        public Guid? BrokerId { get; private set; }

        private EDocsFaxNumber()
        {
        }

        public EDocsFaxNumber(Guid? brokerId, bool isSharedLine)
        {
            this.HasNumber = true;
            this.IsSharedLine = isSharedLine;
            this.BrokerId = brokerId;
        }

        public static EDocsFaxNumber RetrievePrivate(Guid brokerId)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@BrokerId", brokerId)
            };

            IReadOnlyDictionary<string, object> readFields = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_RetrieveFaxNumberByBrokerId", parameters))
            {
                if (reader.Read())
                {
                    readFields = reader.ToDictionary();
                }
            }

            if (readFields == null)
            {
                return null;
            }
            else
            {
                // Private Line
                EDocsFaxNumber number = new EDocsFaxNumber();
                number.HasNumber = true;
                number.PopulateFromReader(readFields, isSharedLine: false);
                number.BrokerId = brokerId;
                return number;
            }
        }

        public static EDocsFaxNumber RetrieveShared()
        {
            IReadOnlyDictionary<string, object> readFields = null;
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "EDOCS_RetrieveSharedFaxNumber"))
            {
                if (reader.Read())
                {
                    readFields = reader.ToDictionary();
                }
            }

            if (readFields == null)
            {
               return null;
            }
            else
            {
                EDocsFaxNumber number = new EDocsFaxNumber();
                number.HasNumber = true;
                number.PopulateFromReader(readFields, isSharedLine: true);
                return number;
            }
        }

        public static EDocsFaxNumber Retrieve(Guid brokerId)
        {
            var number = RetrievePrivate(brokerId);
            if (number == null)
            {
                number = RetrieveShared();
            }

            if (number == null)
            {
                number = new EDocsFaxNumber();
                number.HasNumber = false;
            }

            return number;
        }
        
        /// <summary>
        /// Returns the fax number associated with the login/password,
        /// returning null if no such combination exists.
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static string GetFaxNumber(string login, string password)
        {
            List<IReadOnlyDictionary<string, object>> readFields = new List<IReadOnlyDictionary<string, object>>();
            foreach (DbConnectionInfo connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = 
                {
                    new SqlParameter("@DocRouterLogin", login)
                };

                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "EDOCS_RetrieveFaxNumberByLogin", parameters))
                {
                    while (reader.Read())
                    {
                        readFields.Add(reader.ToDictionary());
                    }
                }
            }

            foreach (var fields in readFields)
            {
                var faxNumber = fields["FaxNumber"].ToString();

                var encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)fields["EncryptionKeyId"]);
                var decryptedPassword = LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyId.Value, fields.GetValueOrNull("EncryptedDocRouterPassword") as byte[]);
                if (decryptedPassword == password)
                {
                    return faxNumber;
                }
            }

            return null;
        }

        public void Save()
        {
            if (!this.encryptionKeyId.HasValue)
            {
                this.encryptionKeyId = GenericLocator<IEncryptionKeyDriverFactory>.Factory.Create().GenerateKey().Key;
            }

            var encryptedPassword = LendersOffice.Drivers.Encryption.EncryptionHelper.EncryptString(this.encryptionKeyId.Value, this.DocRouterPassword);
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter("@FaxNumber", this.FaxNumber),
                new SqlParameter("@DocRouterLogin", this.DocRouterLogin),
                new SqlParameter("@EncryptedDocRouterPassword", encryptedPassword),
                new SqlParameter("@EncryptionKeyId", this.encryptionKeyId.Value.Value)
            };

            if (!this.IsSharedLine)
            {
                parameters.Add(new SqlParameter("@BrokerId", this.BrokerId.Value));
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId.Value, "EDOCS_SetFaxNumber", 3, parameters);
            }
            else
            {
                StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShareROnly, "EDOCS_SetSharedFaxNumber", 3, parameters);
            }
        }

        private void PopulateFromReader(IReadOnlyDictionary<string, object> readFields, bool isSharedLine)
        {
            this.IsSharedLine = isSharedLine;
            this.FaxNumber = readFields["FaxNumber"].ToString();
            this.DocRouterLogin = readFields["DocRouterLogin"].ToString();

            this.encryptionKeyId = EncryptionKeyIdentifier.Create((Guid)readFields["EncryptionKeyId"]);
            this.lazyDocRouterPassword = new Lazy<string>(() =>
            {
                var passwordBytes = readFields.GetValueOrNull("EncryptedDocRouterPassword") as byte[];
                return LendersOffice.Drivers.Encryption.EncryptionHelper.DecryptString(encryptionKeyId.Value, passwordBytes);
            });
        }
    }
}
