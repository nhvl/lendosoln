﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using System.Data.Common;
using System.Data.SqlClient;
using LendersOffice.Common;
using LendersOffice.Security;
using System.IO;
using System.Net.Sockets;

namespace EDocs
{

    public enum E_LostDocSource
    {
        Fax = 0,
        BarcodeUploader = 1
    }

    public sealed class LostEDocument
    {

        private static string CREATE_SP = "EDOCS_CreateLostDocument";
        
        private Guid? m_DocumentId;
        private Guid? m_LoanId;
        private Guid? m_BrokerId;
        private int? m_DocTypeId; 
        private string m_sFaxNumber;
        private string m_sDescription;
        private int m_NumPages;
        private DateTime m_CreatedDate;
        private E_LostDocSource m_documentSource; 
        private string m_sDocTypeName;
        private string m_sBrokerNm;
        private Guid m_fileDbkey; 
        private byte[] m_PdfData;
        private bool isESigned;

        public Guid FileDBKey
        {
            get { return m_fileDbkey; }
        }

        public string BrokerNm
        {
            get { return m_sBrokerNm; }
        }

        public string DocTypeName
        {
            get { return m_sDocTypeName; }
        }

        public Guid? LoanId
        {
            get { return m_LoanId; }
            set { m_LoanId = value; }
        }

        public Guid? BrokerId
        {
            get { return m_BrokerId; }
            set { m_BrokerId = value; }
        }

        public int? DocTypeId
        {
            get { return m_DocTypeId; }
            set { m_DocTypeId = value; }
        }

        public string FaxNumber
        {
            get { return m_sFaxNumber;  }
            set { m_sFaxNumber = value; }
        }

        public string Description
        {
            get { return m_sDescription; }
            set { m_sDescription = value; }
        }

        public E_LostDocSource DocumentSource
        {
            get { return m_documentSource; }
            set { m_documentSource = value; }
        }

        public Guid DocumentId
        {
            get
            {
                if (m_DocumentId.HasValue)
                {
                    return m_DocumentId.Value;
                }
                throw new InvalidOperationException("Object is new, no id exist.");
            }
        }

        public DateTime CreatedDate
        {
            get { return m_CreatedDate; }
        }

        private static readonly E_FileDB FileDBLocation = E_FileDB.EDMS;

        public byte[] PDFData
        {
            get
            {
                if (m_PdfData == null || m_PdfData.Length == 0)
                {
                    m_PdfData = FileDBTools.ReadData(LostEDocument.FileDBLocation ,m_fileDbkey.ToString());
                }
                return m_PdfData;
            }

            set
            {
                var reader = EDocumentViewer.CreatePdfReader(value);
                this.m_NumPages = reader.NumberOfPages;
                this.isESigned = EDocument.ContainsSignature(reader);

                this.m_PdfData = value;
            }

        }

        public int PageCount
        {
            get { return this.m_NumPages; }
        }

        public bool IsESigned
        {
            get { return this.isESigned; }
        }

        public LostEDocument()
        {
            m_LoanId = null;
            m_BrokerId = null;
            m_DocTypeId = null;
            m_sFaxNumber = "";
            m_sDescription = "";
        }

        private LostEDocument(DbDataReader reader)
        {
            m_DocumentId = (Guid)reader["DocumentId"];
            m_BrokerId = reader["BrokerId"] == DBNull.Value ? new Guid?() : (Guid)reader["BrokerId"];
            m_LoanId = reader["sLId"] == DBNull.Value ? new Guid?() : (Guid)reader["sLId"];
            m_DocTypeId = reader["DocTypeId"] == DBNull.Value ? new int?() : (int)reader["DocTypeId"];
            m_sFaxNumber = (string)reader["FaxNumber"];
            m_sDescription = (string)reader["Description"];
            m_NumPages = (int)reader["NumPages"];
            m_CreatedDate  = (DateTime)reader["CreatedDate"];
            m_fileDbkey = (Guid)reader["FileDBKey_OriginalDocument"];
            m_sDocTypeName = reader["DocTypeName"] == DBNull.Value ? "N/A" : (string)reader["DocTypeName"];
            m_sBrokerNm = reader["BrokerNm"] == DBNull.Value ? "N/A" : (string)reader["BrokerNm"];
            m_documentSource = (E_LostDocSource)reader["DocumentSource"];
            this.isESigned = (bool)reader["IsESigned"];
        }

        public string GetPathForFile()
        {
            return FileDBTools.CreateCopy(LostEDocument.FileDBLocation, this.FileDBKey.ToString());
        }

        public void Save()
        {
            if (m_DocumentId.HasValue)
            {
                throw new InvalidOperationException("Cannot modify a lost document once its saved.");
            }
            m_CreatedDate = DateTime.Now;
            m_DocumentId = Guid.NewGuid();
            m_fileDbkey = Guid.NewGuid();

            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@DocumentId", m_DocumentId ),
                new SqlParameter("@BrokerId", m_BrokerId),
                new SqlParameter("@LoanId", m_LoanId),
                new SqlParameter("@DocTypeId", m_DocTypeId),
                new SqlParameter("@FaxNumber", m_sFaxNumber),
                new SqlParameter("@Description", m_sDescription),
                new SqlParameter("@NumPages", m_NumPages),
                new SqlParameter("@CreatedDate", m_CreatedDate),
                new SqlParameter("@FileDbKey", m_fileDbkey ),
                new SqlParameter("@DocumentSource", m_documentSource),
                new SqlParameter("@IsESigned", this.isESigned)
            };



            if (m_PdfData == null || m_PdfData.Length == 0)
            {
                throw new CBaseException(ErrorMessages.Generic, "BUG-Trying to save a document w/o a pdf.");
            }


            CStoredProcedureExec exce;
            if (BrokerId.HasValue)
            {
                exce = new CStoredProcedureExec(BrokerId.Value);
            }
            else
            {
                exce = new CStoredProcedureExec(DataSrc.LOShare);
            }

            try
            {
                exce.BeginTransactionForWrite();
                exce.ExecuteNonQuery(CREATE_SP, 3, parameters);
                FileDBTools.WriteData(LostEDocument.FileDBLocation, m_fileDbkey.ToString(), m_PdfData);
                exce.CommitTransaction();

            }
            catch (Exception)
            {
                exce.RollbackTransaction();
                throw;
            }

            finally
            {
                if (exce != null)
                {
                    exce.Dispose();
                }
            }
        }

        
        public static IList<LostEDocument> GetDocuments(int page, int pageSize, out int documentCount ) 
        {
            if ( page < 0 || pageSize < 0 ) 
            {
                throw new ArgumentException();
            }

            List<LostEDocument> docs = new List<LostEDocument>();

            int startingRecordNumber = page * pageSize +1 ;
            int stoppingRecordNumber = startingRecordNumber + pageSize; 


            SqlParameter[] queryParameters = new SqlParameter[] { 
                new SqlParameter("@StartingRecord", startingRecordNumber),
                new SqlParameter("@EndingRecord", stoppingRecordNumber),
                new SqlParameter("@TotalCount", System.Data.SqlDbType.Int)
            };
      
            queryParameters[2].Direction = System.Data.ParameterDirection.Output;

            using (DbDataReader results = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "EDOCS_RetrieveLostDocuments", queryParameters))
            {

                while (results.Read())
                {
                    docs.Add(new LostEDocument(results));
                }
             
            }

            documentCount = (int)queryParameters[2].Value;


            return docs;
        }


        public static LostEDocument GetDocumentById(Guid id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocumentId", id)
                                        };

            using (DbDataReader results = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "EDOCS_GetLostDocumentById", parameters))
            {
                if (results.Read())
                {
                    return new LostEDocument(results);
                }
            }

            throw new NotFoundException(ErrorMessages.EDocs.DocumentNotFoundException);
        }

        public static LostEDocument GetDocumentById(Guid id, Guid brokerId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocumentId", id)
                                        };

            using (DbDataReader results = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetLostDocumentById", parameters))
            {
                if (results.Read())
                {
                    return new LostEDocument(results);
                }
            }

            throw new NotFoundException(ErrorMessages.EDocs.DocumentNotFoundException);
        }

        /// <summary>
        /// Doesnt do any permission checks.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="brokerId"></param>
        /// <returns></returns>
        public static bool DeleteDocument(Guid id, AbstractUserPrincipal principal)
        {
            LostEDocument document = LostEDocument.GetDocumentById(id, principal.BrokerId);

            if (principal.BrokerId != document.BrokerId || !principal.HasPermission(Permission.AllowManagingFailedDocs))
            {
                throw new AccessDenied();
            }

            SqlParameter[] parameters = { 
                                          new SqlParameter("@Id", id),
                                          new SqlParameter("@BrokerId", principal.BrokerId)
                                      };

            int count = 0;

            try
            {
                count = StoredProcedureHelper.ExecuteNonQuery(principal.BrokerId, "EDOCS_LostDocumentDelete", 1, parameters);
            }
            catch (SqlException e)
            {
                Tools.LogErrorWithCriticalTracking(e);

            }
            if (count > 0)
            {
                try
                {
                    FileDBTools.Delete(FileDBLocation, document.FileDBKey.ToString());
                }
                catch (FileNotFoundException e)
                {
                    Tools.LogError(document.FileDBKey.ToString(), e);
                }
                catch (SocketException e)
                {
                    Tools.LogErrorWithCriticalTracking("FileDB does not appear to be running. Tried to delete key " + document.FileDBKey , e);                    
                }

                return true;
            }

            return false;
        }

        public static IEnumerable<LostEDocument> GetBarcodeDocumentsByBrokerId(Guid brokerId)
        {
            SqlParameter[] parameters = new[] { 
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@DocumentSource", E_LostDocSource.BarcodeUploader)
            };
            
            List<LostEDocument> docs = new List<LostEDocument>();

            using (DbDataReader results = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_RetrieveAllLostDocuments", parameters)) 
            {
                while (results.Read())
                {
                    LostEDocument doc = new LostEDocument(results);
                    if (doc.BrokerId != brokerId)
                    {
                        Tools.LogWarning("Doc loaded but its in wrong broker!"); 
                        continue;
                    }

                    docs.Add(doc);
                }
            }

            return docs;
        }

        public static int GetBarcodeDocumentCount(DbConnectionInfo connInfo, Guid brokerId)
        {
            SqlParameter[] parameters = new[] { 
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@DocumentSource", E_LostDocSource.BarcodeUploader)
            };

            return (int)StoredProcedureHelper.ExecuteScalar(connInfo, "EDOCS_LostDocumentCount", parameters); 
        }
        
    }
}
