﻿using System;
using DataAccess;
using LendersOffice.ObjLib.Edocs.DocMagic;
using LendersOffice.ObjLib.Edocs.DocuTech;
using LendersOffice.ObjLib.Edocs.IDS;

namespace LendersOffice.ObjLib.Edocs
{
    public class DocTypeMapperFactory
    {
        public static IDocTypeMapper Create(E_DocBarcodeFormatT format, Guid brokerId)
        {
            switch (format)
            {
                case E_DocBarcodeFormatT.DocMagic:
                    return new DocMagicDocTypeMapper(brokerId);
                case E_DocBarcodeFormatT.DocuTech:
                    return new DocuTechDocTypeMapper(brokerId);
                case E_DocBarcodeFormatT.IDS:
                    return new IDSDocTypeMapper(brokerId);
                default:
                    throw new UnhandledEnumException(format);
            }
        }
    }
}
