﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;

namespace LendersOffice.ObjLib.Edocs
{
    /// <summary>
    /// Provides for mapping broker document types to document vendor document types/classes.
    /// </summary>
    public interface IDocTypeMapper
    {
        /// <summary>
        /// Associate doc vendor doc types with a broker doc type id.
        /// </summary>
        /// <param name="docTypeID">The broker doc type id.</param>
        /// <param name="assocDocTypes">The doc vendor doc types to associate with the broker doc type.</param>
        void SetAssociation(int docTypeID, IEnumerable<IDocVendorDocType> assocDocTypes);
        
        /// <summary>
        /// Associate doc vendor doc type ids with a broker doc type id.
        /// </summary>
        /// <param name="docTypeID">The broker doc type id.</param>
        /// <param name="assocDocTypes">The doc vendor doc type ids to associate with the broker doc type.</param>
        void SetAssociation(int docTypeId, IEnumerable<int> associatedDocTypes);
        
        /// <summary>
        /// Gets the doc vendor doc types associated with a broker doc type id.
        /// </summary>
        /// <param name="docTypeId">The broker doc type id.</param>
        /// <returns>The doc vendor doc types associated with the broker doc type id.</returns>
        IEnumerable<IDocVendorDocType> GetTypesFor(int docTypeId);
        
        /// <summary>
        /// Gets the doc vendor doc types that are not associated with a broker doc type id.
        /// </summary>
        /// <returns>The doc vendor doc types that are not associated with a broker doc type id.</returns>
        IEnumerable<IDocVendorDocType> GetDisassociatedTypes();
        
        /// <summary>
        /// Gets a mapping from broker doc type id to doc vendor doc types.
        /// </summary>
        /// <returns>Mapping from broker doc type id to doc vendor doc types.</returns>
        ILookup<int, IDocVendorDocType> GetBrokerDocTypeIdsWithDocVendorMappings();

        /// <summary>
        /// Gets a mapping of all doc vendor doc type descriptions to doc vendor doc type ids.
        /// </summary>
        /// <returns>Mapping of all doc vendor doc type descriptions to doc vendor doc type ids.</returns>
        IDictionary<string, int> GetVendorDocTypeIdsByDescription();

        /// <summary>
        /// Gets a map from barcode classification to broker doc type id. Use when splitting documents based on barcode.
        /// </summary>
        /// <returns></returns>
        IDictionary<string, int> GetBrokerDocTypeIdsByBarcodeClassification();

        /// <summary>
        /// Gets the special doctype ids for doc vendor doc types that are not associated with a broker doc type id and
        /// unreadable barcodes.
        /// </summary>
        /// <param name="unassignedDocTypeId"></param>
        /// <param name="unreadableBarcodeDocTypeId"></param>
        void GetUnassignedDocTypeAndUnreadableDocType(out int unassignedDocTypeId, out int unreadableBarcodeDocTypeId);

        /// <summary>
        /// The name of the vendor this mapper maps to
        /// </summary>
        string VendorName { get; }

        E_DocBarcodeFormatT DocumentFormat { get; }
    }
}
