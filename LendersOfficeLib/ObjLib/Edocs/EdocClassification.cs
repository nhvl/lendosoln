﻿namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using DataAccess;
    using LendersOffice.Drivers.SqlServerDB;
    using LqbGrammar.DataTypes;

    /// <summary>
    /// Provides a container for electronic document classification data.
    /// </summary>
    public class EdocClassification
    {
        /// <summary>
        /// A readonly instance of the <see cref="EdocClassification"/> class
        /// representing no classification.
        /// </summary>
        public static readonly EdocClassification NoClassification = new EdocClassification(0, string.Empty);

        /// <summary>
        /// Initializes a new instance of the <see cref="EdocClassification"/> class.
        /// </summary>
        /// <param name="id">
        /// The ID of the classification.
        /// </param>
        /// <param name="name">
        /// The name of the classification.
        /// </param>
        private EdocClassification(int id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        /// <summary>
        /// Gets the ID of the classification.
        /// </summary>
        /// <value>
        /// The ID of the classification.
        /// </value>
        public int Id { get; private set; }

        /// <summary>
        /// Gets the name of the classification.
        /// </summary>
        /// <value>
        /// The name of the classification.
        /// </value>
        public string Name { get; private set; }

        /// <summary>
        /// Retrieves the classification with the specified ID.
        /// </summary>
        /// <param name="classificationId">
        /// The ID of the classification.
        /// </param>
        /// <returns>
        /// The classification.
        /// </returns>
        public static EdocClassification Retrieve(int? classificationId)
        {
            if (!classificationId.HasValue ||
                classificationId.Value == NoClassification.Id)
            {
                return NoClassification;
            }

            var procedureName = StoredProcedureName.Create("EDOCS_OCR_SYSTEM_DOC_TYPES_RetrieveById").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@Id", classificationId.Value)
            };

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return new EdocClassification(classificationId.Value, reader.SafeString("Name"));
                }
            }

            return NoClassification;
        }

        /// <summary>
        /// Adds a new system classification.
        /// </summary>
        /// <param name="name">
        /// The name for the new classification.
        /// </param>
        /// <returns>
        /// The ID of the new classification.
        /// </returns>
        public static int AddClassification(string name)
        {
            var docTypeName = name?.Trim();
            if (string.IsNullOrEmpty(docTypeName))
            {
                throw new ArgumentException("New classification name cannot be blank.", nameof(name));
            }

            var procedureName = StoredProcedureName.Create("EDOCS_OCR_SYSTEM_DOC_TYPES_AddNew").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@Name", docTypeName)
            };

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty))
            {
                if (reader.Read())
                {
                    return (int)reader["Id"];
                }
            }

            throw new CBaseException("Failed to add new classification.", "Failed to add new classification.");
        }

        /// <summary>
        /// Edits an existing classification.
        /// </summary>
        /// <param name="id">
        /// The ID of the existing classification.
        /// </param>
        /// <param name="newName">
        /// The new classification name.
        /// </param>
        /// <returns>
        /// True if the edit was successful, false otherwise.
        /// </returns>
        public static bool EditClassification(int id, string newName)
        {
            var docTypeName = newName?.Trim();
            if (string.IsNullOrEmpty(docTypeName))
            {
                throw new ArgumentException("New classification name cannot be blank.", nameof(newName));
            }

            var procedureName = StoredProcedureName.Create("EDOCS_OCR_SYSTEM_DOC_TYPES_Edit").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@Id", id),
                new SqlParameter("@NewName", docTypeName)
            };

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare))
            {
                var affectedRows = StoredProcedureDriverHelper.ExecuteNonQuery(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
                return affectedRows.Value > 0;
            }
        }

        /// <summary>
        /// Deletes a classification.
        /// </summary>
        /// <param name="id">
        /// The ID of the classification.
        /// </param>
        /// <returns>
        /// True if the deletion was successful, false otherwise.
        /// </returns>
        public static bool DeleteClassification(int id)
        {
            var procedureName = StoredProcedureName.Create("EDOCS_OCR_SYSTEM_DOC_TYPES_Delete").Value;
            SqlParameter[] parameters =
            {
                new SqlParameter("@Id", id)
            };

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare))
            {
                var affectedRows = StoredProcedureDriverHelper.ExecuteNonQuery(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
                return affectedRows.Value > 0;
            }
        }

        /// <summary>
        /// Gets the mappings between LQB system e-doc classifications ids 
        /// and the classifications.
        /// </summary>
        /// <returns>
        /// The mappings.
        /// </returns>
        public static Dictionary<int, EdocClassification> GetSystemClassifications()
        {
            var procedureName = StoredProcedureName.Create("EDOCS_OCR_SYSTEM_DOC_TYPES_Retrieve").Value;
            var classifications = new Dictionary<int, EdocClassification>();

            using (var connection = DbAccessUtils.GetConnection(DataSrc.LOShare))
            using (var reader = StoredProcedureDriverHelper.ExecuteReader(connection, null, procedureName, null, TimeoutInSeconds.Thirty))
            {
                while (reader.Read())
                {
                    var id = Convert.ToInt32(reader["Id"]);
                    var name = reader["Name"].ToString();

                    classifications.Add(id, new EdocClassification(id, name));
                }
            }

            return classifications;
        }
    }
}
