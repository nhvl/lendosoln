﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using CommonProjectLib.Common.Lib;
using DataAccess;
using LendersOffice.Edocs.DocMagic;

namespace LendersOffice.ObjLib.Edocs.DocMagic
{
    public class DocMagicDocTypeMapper : BaseDocTypeMapper, IDocTypeMapper
    {
        public string VendorName
        {
            get
            {
                return "DocMagic";
            }
        }

        public DocMagicDocTypeMapper(Guid brokerId)
            : base(brokerId)
        {
        }

        public void SetAssociation(int docTypeID, IEnumerable<IDocVendorDocType> assocDocTypes)
        {
            SetAssociation(docTypeID, from t in assocDocTypes select t.Id);
        }

        public void SetAssociation(int docTypeId, IEnumerable<int> associatedDocTypes)
        {
            using (CStoredProcedureExec spExec = new CStoredProcedureExec(BrokerId))
            {
                spExec.BeginTransactionForWrite();

                spExec.ExecuteNonQuery("EDOCS_RemoveDocMagicAssociations", new SqlParameter("@BrokerId", BrokerId), new SqlParameter("@docTypeId", docTypeId));

                foreach (int docMagicDocTypeId in associatedDocTypes)
                {
                    spExec.ExecuteNonQuery("EDOCS_AssociateDocMagicDocType", new SqlParameter("@DocMagicDocTypeId", docMagicDocTypeId),
                        new SqlParameter("@BrokerId", BrokerId),
                        new SqlParameter("@DocTypeId", docTypeId));
                }
                spExec.CommitTransaction();
            }
        }

        public IEnumerable<IDocVendorDocType> GetTypesFor(int docTypeId)
        {
            List<IDocVendorDocType> docTypes = new List<IDocVendorDocType>();

            Dictionary<int, DocMagicDocType> dictionary = DocMagicDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId),
                                            new SqlParameter("@DocTypeId", docTypeId)
                                        };
            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int docMagicDocTypeId = (int)reader["DocMagicDocTypeId"];

                    DocMagicDocType docType = null;

                    if (dictionary.TryGetValue(docMagicDocTypeId, out docType))
                    {
                        docTypes.Add(docType);
                    }
                }
            }

            return docTypes;
        }

        public IEnumerable<IDocVendorDocType> GetDisassociatedTypes()
        {
            List<IDocVendorDocType> docTypes = new List<IDocVendorDocType>();

            Dictionary<int, DocMagicDocType> dictionary = DocMagicDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            HashSet<int> brokerDocMagicDocTypeSet = new HashSet<int>();

            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int docMagicDocTypeId = (int)reader["DocMagicDocTypeId"];

                    brokerDocMagicDocTypeSet.Add(docMagicDocTypeId);
                }
            }

            // Add all unmapped IDS doc types.
            foreach (var o in dictionary)
            {
                if (brokerDocMagicDocTypeSet.Contains(o.Key) == false)
                {
                    docTypes.Add(o.Value);
                }
            }

            return docTypes.Where(dt => !dt.Description.EndsWith("- OBSOLETE"));
        }

        public ILookup<int, IDocVendorDocType> GetBrokerDocTypeIdsWithDocVendorMappings()
        {
            List<Tuple<int, IDocVendorDocType>> docTypes = new List<Tuple<int, IDocVendorDocType>>();

            Dictionary<int, DocMagicDocType> dictionary = DocMagicDocType.ListAllAsDictionary();

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };
            using (var reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int docTypeId = (int)reader["doctypeid"];
                    int docMagicDocTypeId = (int)reader["DocMagicDocTypeId"];

                    DocMagicDocType docType = null;

                    if (dictionary.TryGetValue(docMagicDocTypeId, out docType))
                    {
                        docTypes.Add(Tuple.Create<int, IDocVendorDocType>(docTypeId, docType));
                    }
                }
            }
            return docTypes.ToLookup(p => p.Item1, p => p.Item2);
        }

        public IDictionary<string, int> GetVendorDocTypeIdsByDescription()
        {
            Dictionary<string, int> magDocTypeID_By_magDocTypeName = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            foreach (var o in DocMagicDocType.ListAll())
            {
                magDocTypeID_By_magDocTypeName.Add(o.Description, o.Id);
            }

            return magDocTypeID_By_magDocTypeName;
        }

        public IDictionary<string, int> GetBrokerDocTypeIdsByBarcodeClassification()
        {
            Dictionary<string, int> mapping = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);

            var dictionary = DocMagicDocType.ListAllAsDictionary();
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(BrokerId, "EDOCS_DOCMAGIC_DOCTYPE_X_DOCTYPE_GetByBrokerId", parameters))
            {
                while (reader.Read())
                {
                    int docTypeId = (int)reader["DocTypeId"];
                    int docMagicDocTypeId = (int)reader["DocMagicDocTypeId"];

                    DocMagicDocType docType = null;

                    if (dictionary.TryGetValue(docMagicDocTypeId, out docType))
                    {
                        mapping.Add(docType.BarcodeClassification, docTypeId);
                    }
                }
            }

            return mapping;
        }

        public E_DocBarcodeFormatT DocumentFormat
        {
            get { return E_DocBarcodeFormatT.DocMagic; }
        }
    }
}
