﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using EDocs;
using DataAccess;
using CommonProjectLib.Common.Lib;
using LendersOffice.ObjLib.Edocs;
using LendersOffice.Common;

namespace LendersOffice.Edocs.DocMagic
{
    public class DocMagicDocType : IDocVendorDocType, IDocVendorDocTypeRecord
    {
        public string Description { get; private set; }
        public int Id { get; private set; }
        public string BarcodeClassification { get; private set; }
        public string InternalDescription { get { return this.Description; } }

        public DocMagicDocType(int id, string description, string barcodeClassification)
        {
            this.Id = id;
            this.Description = description;
            this.BarcodeClassification = barcodeClassification;
        }

        public DocMagicDocType(DbDataReader reader)
        {
            Description = (string)reader["DocumentClass"];
            Id = (int)reader["id"];
        }

        private static TimeBasedCacheObject<IEnumerable<DocMagicDocType>> docMagicDocTypeList;

        static DocMagicDocType()
        {
            ReloadAll();
        }

        private static void ReloadAll()
        {
            // 4/21/2015 dd - Cache the list for 5 minutes.
            docMagicDocTypeList = new TimeBasedCacheObject<IEnumerable<DocMagicDocType>>(new TimeSpan(0, 5, 0), DocMagicDocType.ListAllImpl);
        }

        public static IEnumerable<DocMagicDocType> ListAll()
        {
            return docMagicDocTypeList.Value;
        }

        public static IEnumerable<IDocVendorDocTypeRecord> ListRecords()
        {
            return docMagicDocTypeList.Value;
        }

        public static Dictionary<int, DocMagicDocType> ListAllAsDictionary()
        {
            return docMagicDocTypeList.Value.ToDictionary(o => o.Id);
        }

        private static IEnumerable<DocMagicDocType> ListAllImpl()
        {
            List<DocMagicDocType> list = new List<DocMagicDocType>();

            using (var reader = StoredProcedureHelper.ExecuteReader(DataSrc.LOShareROnly, "EDOCS_DOCMAGIC_DOCTYPE_ListAll"))
            {
                while (reader.Read())
                {
                    int id = (int)reader["Id"];
                    string description = (string)reader["DocumentClass"];
                    string barcodeClassification = (string)reader["BarcodeClassification"];

                    list.Add(new DocMagicDocType(id, description, barcodeClassification));
                }
            }

            return list;
        }

        public static int Create(string desc, string barcode)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@DocumentClass", desc),
                new SqlParameter("@BarcodeClassification", barcode)
            };

            var result = (int)StoredProcedureHelper.ExecuteScalar(DataSrc.LOShare, "EDOCS_DOCMAGIC_DOCTYPE_Insert", parameters);
            ReloadAll();

            return result;
        }

        public static void Delete(int recordId)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Id", recordId),
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "EDOCS_DOCMAGIC_DOCTYPE_Delete", 3, parameters);
            ReloadAll();
        }

        public static void Update(int recordId, string desc, string barcode)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@Id", recordId),
                new SqlParameter("@DocumentClass", desc),
                new SqlParameter("@BarcodeClassification", barcode)
            };

            StoredProcedureHelper.ExecuteNonQuery(DataSrc.LOShare, "EDOCS_DOCMAGIC_DOCTYPE_Update", 3, parameters);
            ReloadAll();
        }

    }
}
