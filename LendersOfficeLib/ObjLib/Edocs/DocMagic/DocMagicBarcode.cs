﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.ObjLib.Edocs;

namespace LendersOffice.Edocs.DocMagic
{
    public class DocMagicBarcode : IDocVendorBarcodeWithDocPageNum
    {

        public string sLNm { get; private set; }
        public string DocumentClass { get; private set; }
        public string DocID { get; private set; }
        public int DocumentPageNumber { get; private set; }
        public int DocumentTotalPageNumber { get; private set; }
        public int PackagePageNumber { get; private set;  }

        public static bool MatchesFormat(Barcode barcode)
        {
            string[] data = barcode.Data.Split(new char[] { '|' });
            return data.Length == 5;
        }


        public DocMagicBarcode(Barcode barcode)
        {
            PackagePageNumber = barcode.Page;

            string[] data = barcode.Data.Split(new char[] { '|'});
            if (data.Length != 5)
            {
                throw new CBaseException("Unexpected barcode format", "Invalid data format for docmagic barcode " + barcode.Data  + " " + barcode.Page);
            }

            sLNm = data[0];
            DocumentClass = data[1];
            DocID = data[2];
            DocumentPageNumber = int.Parse(data[3]);
            DocumentTotalPageNumber = int.Parse(data[4]);

        }


    }
}
