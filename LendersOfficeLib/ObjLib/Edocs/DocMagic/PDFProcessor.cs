﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using CommonProjectLib.Logging;
using DataAccess;
using EDocs;
using LendersOffice.Admin;
using LendersOffice.Common;
using LendersOffice.Constants;
using LendersOffice.Edocs.DocuTech;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.ObjLib.Edocs;
using LendersOffice.ObjLib.Edocs.BarcodeScanning;
using LendersOffice.ObjLib.Edocs.IDS;

namespace LendersOffice.Edocs.DocMagic
{
    /// <summary>
    /// PDFProcessor
    /// 
    /// 1/26/2018 - dd - This class will be remove once PDFCancellableRunnable is verify on production.
    /// DO NOT UPDATE this class.
    /// </summary>
    /// <remarks>
    /// OPM 179728 - PDFProcessor expanded to handle Dropbox documents
    /// NOTE: PDFProcessor assumes all docs split from a single PDF belong or should be
    /// assigned to the same loan. Because of this, for Dropbox files we will assume that the first
    /// LoanId found in the barcodes is the correct one, with the primary app as the default app.
    /// If no loanId is found the entire doc will be added to the Dropbox and barcodes will be ignored
    /// (previous default behavior).
    /// </remarks>
    public class PDFProcessor : CommonProjectLib.Runnable.IRunnable
    {
        static PDFProcessor()
        {
            string msMqLogConnectStr = ConstStage.MsMqLogConnectStr;
            if (string.IsNullOrEmpty(msMqLogConnectStr) == false)
            {
                LogManager.Add(new LendersOffice.Common.MSMQLogger(msMqLogConnectStr, ConstStage.EnvironmentName));
            }

            LogManager.Add(new EmailLogger(ConstStage.SmtpServer, 
                ConstStage.SmtpServerPortNumber,
                ConfigurationManager.AppSettings["NotifyOnErrorEmailAddress"],
                ConfigurationManager.AppSettings["NotifyFromOnError"]));
        }

        public string Description
        {
            get
            {
                return "Splits PDFs by barcode and turns them into edocs.";
            }
        }

        public void Run()
        {
            Tools.SetupServicePointManager();
            DBMessageQueue dbQueue = new DBMessageQueue(ConstMsg.DocMagicDocSplitterQueue);
            DBMessage message;

            using (var workerTiming = new WorkerExecutionTiming("DocMagic.PDFProcessor"))
            {
                do
                {
                    try
                    {
                        message = dbQueue.Receive();
                        if (message == null)
                        {
                            return;
                        }
                    }
                    catch (DBMessageQueueException e)
                    {
                        if (e.MessageQueueErrorCode != DBMessageQueueErrorCode.IOTimeout && e.MessageQueueErrorCode != DBMessageQueueErrorCode.EmptyQueue)
                        {
                            LogManager.Error("[QP]DBMQException " + e.Message);
                            throw;
                        }

                        return;
                    }
                    using (var itemTiming = workerTiming.RecordItem(message.Subject1, message.InsertionTime))
                    {
                        try
                        {
                            HandleMessage(message);
                        }
                        catch (Exception e)
                        {
                            itemTiming.RecordError(e);
                            throw;
                        }
                    }
                } while (true);
            }
        }

        private LendersOffice.ObjLib.Edocs.Barcode[] GetBarcodesFromFileDBKey(string filedbname, string filedbkey)
        {
            StringBuilder debugMessage = new StringBuilder("GetBarcodesFromFileDBKey\n");

            try
            {
                string url = string.Format("{0}?DBName={1}&dbfilekey={2}", ConstStage.BarcodeReaderUrl, filedbname, filedbkey);
                debugMessage.AppendLine(url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Timeout = 600000; //10 minutes
                using( WebResponse response = webRequest.GetResponse())
                using( Stream sr = response.GetResponseStream())
                using (XmlReader xmlReader = XmlReader.Create(sr))
                {
                    XDocument doc = XDocument.Load(xmlReader);
                    debugMessage.AppendLine(doc.ToString());

                    if (doc.Root.Attribute("status").Value == "Error")
                    {
                        throw CBaseException.GenericException(doc.Root.Attribute("error_message").Value);
                    }

                    List<LendersOffice.ObjLib.Edocs.Barcode> pdfPagesWithBarcode = new List<LendersOffice.ObjLib.Edocs.Barcode>();

                    foreach (XElement page in doc.Root.Elements("page"))
                    {
                        int number = int.Parse(page.Attribute("number").Value);

                        foreach (XElement barcode in page.Elements("barcode"))
                        {
                            string value = barcode.Attribute("value").Value;
                            LendersOffice.ObjLib.Edocs.Barcode b = new LendersOffice.ObjLib.Edocs.Barcode(number, value);
                            pdfPagesWithBarcode.Add(b);
                        }
                    }

                    return pdfPagesWithBarcode.OrderBy(p => p.Page).ToArray();
                }
            }
            finally
            {
                Tools.LogInfo(debugMessage.ToString());
            }
        }

        private void HandleMessage(DBMessage message)
        {
            DocMagicPDFData pdfData = ObsoleteSerializationHelper.JsonDeserialize<DocMagicPDFData>(message.Data);
            try
            {
                Trace.CorrelationManager.ActivityId = pdfData.FileDBKey;
                Split(pdfData);
            }
            catch (CBaseException e)
            {
                UpdateToErrorStatus(pdfData, e.UserMessage, e.Message);
                LogManager.Error(e.DeveloperMessage, e);
                throw;
            }
            catch (Exception e)
            {
                UpdateToErrorStatus(pdfData, ErrorMessages.Generic, e.ToString());
                LogManager.Error("HandleMessage - PDF Parser", e);
                throw;
            }
        }

        private static bool BarcodesContainLendingQBBarcode(IEnumerable<LendersOffice.ObjLib.Edocs.Barcode> barcodes)
        {
            int docTypeId;
            Func<LendersOffice.ObjLib.Edocs.Barcode, bool> isLendingQbBarcode =
                (b) => int.TryParse(b.Data, out docTypeId) || EdocFaxCover.IsBarcodeType(E_FaxCoverType.PDFProcessor, b.Data);

            return barcodes.Any(b => isLendingQbBarcode(b));
        }

        /// <summary>
        /// For Dropbox files with no barcodes. Addes file to Dropbox, and cleans up after itself.
        /// </summary>
        /// <param name="pdfData">PDF Data Object taken from message queue</param>
        private void HandleDropbox(DocMagicPDFData pdfData)
        {
            try
            {
                CreateDropboxFile(pdfData);
            }
            catch (Exception e)
            {
                LogManager.Error(pdfData.FileDBKey.ToString() + " PDf Errors " + e.Message);
            }
            
            FileDBTools.Delete(pdfData.FileDB, pdfData.FileDBKey.ToString());
        }

        private static E_DocBarcodeFormatT DetermineBarcodeFormat(IEnumerable<LendersOffice.ObjLib.Edocs.Barcode> barcodes)
        {
            // Code formerly checked for and handled LQB barcodes before getting
            // here. I'm leaving the check for LQB barcodes exclusive from the
            // check for vendor barcodes just to be safe. Not sure if it's
            // necessary.
            if (BarcodesContainLendingQBBarcode(barcodes))
            {
                return E_DocBarcodeFormatT.LendingQB;
            }

            foreach (var bc in barcodes)
            {
                if (DocuTechBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.DocuTech;
                else if (DocMagicBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.DocMagic;
                else if (IDSBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.IDS;
            }

            //This'll error out anyway, since none of them look like a supported barcode.
            return E_DocBarcodeFormatT.DocMagic;
        }

        private bool LenderCanScanBarcodeFormat(BrokerDB broker, E_DocBarcodeFormatT format)
        {
            return format == E_DocBarcodeFormatT.LendingQB ||
                (format == E_DocBarcodeFormatT.DocMagic && broker.IsDocMagicBarcodeScannerEnabled) ||
                (format == E_DocBarcodeFormatT.DocuTech && broker.IsDocuTechBarcodeScannerEnabled) ||
                (format == E_DocBarcodeFormatT.IDS && broker.IsIDSBarcodeScannerEnabled);
        }

        private void HandleUploadWithBarcodes(LendersOffice.ObjLib.Edocs.Barcode[] barcodes, DocMagicPDFData pdfData)
        {
            var format = DetermineBarcodeFormat(barcodes);
            var broker = BrokerDB.RetrieveById(pdfData.BrokerId);

            if (this.LenderCanScanBarcodeFormat(broker, format))
            {
                // Prior to the refactor of this class, the merge feature check didn't occur 
                // for LQB pdfs.
                if (format != E_DocBarcodeFormatT.LendingQB && broker.IsEDocDoctypeMergeEnabled)
                {
                    // dd 7/30/2015 - As of right now no lender is using this feature. The feature was request by PML0177 (OPM 83484), 
                    // however PML0177 is no active and also right before they disable the feature was turn off in 2012.
                    throw new NotSupportedException("IsEDocDoctypeMergeEnabled feature is not support.");
                }

                this.SplitUploadWithBarcodes(pdfData, barcodes, format);
            }
            else
            {
                this.HandleUnsupportedBarcodeFormat(pdfData, format);
            }
        }

        private void SplitUploadWithBarcodes(DocMagicPDFData pdfData, LendersOffice.ObjLib.Edocs.Barcode[] barcodes, E_DocBarcodeFormatT format)
        {
            this.UpdateStatus(pdfData, E_DocMagicPDFStatus.CREATING_EDOCS);

            var splitter = DocumentSplitterFactory.GetDocumentSplitter(pdfData, barcodes, format);
            var result = splitter.SplitDocument();

            if (result.UploadSourceFileToDropbox)
            {
                HandleDropbox(pdfData);
            }
            else if (result.SplitDocuments.Any())
            {
                var uploadResult = this.UploadDocuments(pdfData, result.SplitDocuments, format);

                if (uploadResult.HasError)
                {
                    UpdateToErrorStatus(pdfData, ErrorMessages.Generic, uploadResult.DeveloperErrorMessage);
                    LogManager.Error(pdfData.FileDBKey.ToString() + " PDf Errors " + uploadResult.DeveloperErrorMessage);
                }
                else
                {
                    UpdateStatus(pdfData, E_DocMagicPDFStatus.COMPLETE);
                    FileDBTools.Delete(pdfData.FileDB, pdfData.FileDBKey.ToString());
                }
            }
            else
            {
                var msg = "Barcode scanner processed a PDF, didn't receive " +
                    "errors, but took no action on the file.";
                Tools.LogError(msg, new Exception());
            }
        }

        private void HandleUnsupportedBarcodeFormat(DocMagicPDFData pdfData, E_DocBarcodeFormatT format)
        {
            string usrMsg = "This document does not include LendingQB barcode sheets";

            string formatName = "DocMagic";
            if (format == E_DocBarcodeFormatT.DocuTech)
            {
                formatName = "DocuTech";
            }
            else if (format == E_DocBarcodeFormatT.IDS)
            {
                formatName = "IDS";
            }

            string devMsg = "and the broker does not allow " + formatName + " scanning, and the user opted to scan this document.";
            UpdateToErrorStatus(pdfData, usrMsg, devMsg);
        }

        private void HandleUploadWithoutBarcodes(DocMagicPDFData pdfData)
        {
            if (pdfData.isDropboxFile || pdfData.IsBarcodeUpload)
            {
                this.HandleDropbox(pdfData);
            }
            else
            {
                this.AddEDocWithUnreadableDocType(pdfData);
            }
        }

        private void AddEDocWithUnreadableDocType(DocMagicPDFData pdfData)
        {
            var mapper = new BaseDocTypeMapper(pdfData.BrokerId);
            var pdfPath = FileDBTools.CreateCopy(pdfData.FileDB, pdfData.FileDBKey.ToString());
            var isUploadedByPmlUser = IsUploadedByPmlUser(pdfData.BrokerId, pdfData.UploadingUserId);
            int unassignedDocTypeId, unreadableDocTypeId;
            mapper.GetUnassignedDocTypeAndUnreadableDocType(out unassignedDocTypeId, out unreadableDocTypeId);

            this.CreateEdoc(pdfPath, pdfData, pdfData.AppId, unreadableDocTypeId, pdfData.Description, isUploadedByPmlUser);
            this.UpdateStatus(pdfData, E_DocMagicPDFStatus.COMPLETE);
            // TODO: are we supposed to delete the file from FileDB??
        }
        
        public void Split(DocMagicPDFData pdfData)
        {
            this.UpdateStatus(pdfData, E_DocMagicPDFStatus.SCAN_BARCODES);

            LendersOffice.ObjLib.Edocs.Barcode[] barcodes = this.GetBarcodesFromFileDBKey(
                pdfData.FileDB.GetDatabaseName(),
                pdfData.FileDBKey.ToString());

            if (barcodes.Any())
            {
                this.HandleUploadWithBarcodes(barcodes, pdfData);
            }
            else
            {
                this.HandleUploadWithoutBarcodes(pdfData);
            }
        }

        private DocumentUploadResult UploadDocuments(DocMagicPDFData pdfData, IEnumerable<DocumentUpload> documentsToUpload, E_DocBarcodeFormatT format)
        {
            bool flagError = false;
            StringBuilder sb = new StringBuilder();

            foreach (var documentUpload in documentsToUpload)
            {
                try
                {
                    if (documentUpload.LoanId == Guid.Empty)
                    {
                        CreateDropboxFile(pdfData, documentUpload);
                    }
                    else
                    {
                        CreateEdoc(documentUpload);
                    }
                }
                catch (Exception e)
                {
                    flagError = true;
                    sb.AppendLine(e.Message);
                }
            }

            return new DocumentUploadResult(flagError, sb.ToString());
        }

        private void CreateEdoc(DocumentUpload upload)
        {
            bool isUploadedByPmlUser = IsUploadedByPmlUser(upload.BrokerId, upload.UploadingUserId);

            EDocumentRepository repo = EDocumentRepository.GetSystemRepository(upload.BrokerId);
            EDocument doc = repo.CreateDocument(E_EDocumentSource.UserUpload);
            doc.AppId = upload.AppId;
            doc.LoanId = upload.LoanId;
            doc.PublicDescription = upload.Description;
            doc.InternalDescription = upload.InternalDescription;
            doc.DocumentTypeId = upload.DocTypeId;
            doc.IsUploadedByPmlUser = isUploadedByPmlUser;
            doc.UpdatePDFContentOnSave(upload.PdfPath);
            // Not using repo.Save() because I need to save as a different user.
            doc.Save(upload.UploadingUserId);
        }

        private Guid CreateEdoc(string newDocpath, DocMagicPDFData data, Guid appId, int docTypeId, string description, bool isPmluser)
        {
            EDocumentRepository repo = EDocumentRepository.GetSystemRepository(data.BrokerId);
            EDocument doc = repo.CreateDocument(E_EDocumentSource.UserUpload);
            doc.AppId = appId;
            doc.LoanId = data.LoanId;
            doc.PublicDescription = description;
            doc.InternalDescription = data.InternalNotes;
            doc.DocumentTypeId = docTypeId;
            doc.IsUploadedByPmlUser = isPmluser;
            doc.UpdatePDFContentOnSave(newDocpath);
            doc.Save(data.UploadingUserId);  //sigh not using repo.save because I need to save as a different user
            return doc.DocumentId;
        }

        /// <summary>
        /// Adds doc to dropbox.
        /// </summary>
        /// <param name="data">The data associated with the document upload.</param>
        private void CreateDropboxFile(DocMagicPDFData data)
        {
            if (data.IsBarcodeUpload)
            {
                LostEDocument edoc = new LostEDocument();
                edoc.BrokerId = data.BrokerId;
                edoc.Description = data.DropboxFileName;
                edoc.DocumentSource = E_LostDocSource.BarcodeUploader;
                edoc.FaxNumber = "";
                edoc.PDFData = FileDBTools.ReadData(data.FileDB, data.FileDBKey.ToString());
                edoc.Save();
                return;
            }

            byte[] FileBytes = FileDBTools.ReadData(data.FileDB, data.FileDBKey.ToString());
            LendersOffice.ObjLib.UserDropbox.Dropbox.AddFile(data.UploadingUserId, data.BrokerId, data.DropboxFileName, data.DropboxUploadD, FileBytes);
        }

        private void CreateDropboxFile(DocMagicPDFData data, DocumentUpload upload)
        {
            if (data.IsBarcodeUpload)
            {
                LostEDocument edoc = new LostEDocument();
                edoc.BrokerId = upload.BrokerId;
                edoc.Description = data.DropboxFileName;
                edoc.DocumentSource = E_LostDocSource.BarcodeUploader;
                edoc.FaxNumber = "";
                edoc.PDFData = File.ReadAllBytes(upload.PdfPath);
                edoc.Save();
            }
            else
            {
                byte[] FileBytes = File.ReadAllBytes(upload.PdfPath);
                LendersOffice.ObjLib.UserDropbox.Dropbox.AddFile(data.UploadingUserId, data.BrokerId, data.DropboxFileName, data.DropboxUploadD, FileBytes);
            }
        }

        /// <summary>
        /// Determine if upload was performed by PML user.
        /// </summary>
        /// <param name="pdfData">The data associated with the upload.</param>
        /// <returns>True if uploaded by a PML user. Otherwise, false.</returns>
        private bool IsUploadedByPmlUser(Guid brokerId, Guid uploadingUserId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@userid", uploadingUserId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "Users_GetUserType", parameters))
            {
                if (reader.Read())
                {
                    string userType = (String)reader["UserType"];
                    return userType.Equals("P", StringComparison.OrdinalIgnoreCase);
                }
            }

            return false;
        }

        /// <summary>
        /// Update document status. Ignores dropbox files.
        /// </summary>
        /// <param name="pdfData">The data associated with the document upload.</param>
        /// <param name="status">DocMagic PDF Status.</param>
        private void UpdateStatus(DocMagicPDFData pdfData, E_DocMagicPDFStatus status)
        {
            // Don't update status for Dropbox files
            if (pdfData.isDropboxFile || pdfData.IsBarcodeUpload)
            {
                return;
            }

            DocMagicPDFManager.UpdateStatus(pdfData.BrokerId, pdfData.FileDBKey, status);
        }

        /// <summary>
        /// Set DocMagic PDF Status to Error. Ignores dropbox files.
        /// </summary>
        /// <param name="pdfData">The data associated with the document upload.</param>
        /// <param name="status">DocMagic PDF Status.</param>
        private void UpdateToErrorStatus(DocMagicPDFData pdfData, string userErrorMessage, string devErrorMessage)
        {
            // Don't update status for Dropbox files
            if (pdfData.isDropboxFile || pdfData.IsBarcodeUpload)
            {
                return;
            }

            DocMagicPDFManager.UpdateToErrorStatus(pdfData.BrokerId, pdfData.FileDBKey, userErrorMessage, devErrorMessage);
        }
    }

    /// <summary>
    /// PDFProcessor
    /// </summary>
    /// <remarks>
    /// OPM 179728 - PDFProcessor expanded to handle Dropbox documents
    /// NOTE: PDFProcessor assumes all docs split from a single PDF belong or should be
    /// assigned to the same loan. Because of this, for Dropbox files we will assume that the first
    /// LoanId found in the barcodes is the correct one, with the primary app as the default app.
    /// If no loanId is found the entire doc will be added to the Dropbox and barcodes will be ignored
    /// (previous default behavior).
    /// </remarks>
    public class PDFCancellableRunnable : CommonProjectLib.Runnable.ICancellableRunnable
    {
        static PDFCancellableRunnable()
        {
            string msMqLogConnectStr = ConstStage.MsMqLogConnectStr;
            if (string.IsNullOrEmpty(msMqLogConnectStr) == false)
            {
                LogManager.Add(new LendersOffice.Common.MSMQLogger(msMqLogConnectStr, ConstStage.EnvironmentName));
            }

            LogManager.Add(new EmailLogger(ConstStage.SmtpServer,
                ConstStage.SmtpServerPortNumber,
                ConfigurationManager.AppSettings["NotifyOnErrorEmailAddress"],
                ConfigurationManager.AppSettings["NotifyFromOnError"]));
        }

        public string Description
        {
            get
            {
                return "Splits PDFs by barcode and turns them into edocs.";
            }
        }

        public void Run(CancellationToken cancellationToken)
        {
            Tools.SetupServicePointManager();
            DBMessageQueue dbQueue = new DBMessageQueue(ConstMsg.DocMagicDocSplitterQueue);
            DBMessage message;

            using (var workerTiming = new WorkerExecutionTiming("DocMagic.PDFProcessor"))
            {
                do
                {
                    try
                    {
                        message = dbQueue.Receive();
                        if (message == null)
                        {
                            return;
                        }
                    }
                    catch (DBMessageQueueException e)
                    {
                        if (e.MessageQueueErrorCode != DBMessageQueueErrorCode.IOTimeout && e.MessageQueueErrorCode != DBMessageQueueErrorCode.EmptyQueue)
                        {
                            LogManager.Error("[QP]DBMQException " + e.Message);
                            throw;
                        }

                        return;
                    }
                    using (var itemTiming = workerTiming.RecordItem(message.Subject1, message.InsertionTime))
                    {
                        try
                        {
                            HandleMessage(message);
                        }
                        catch (Exception e)
                        {
                            itemTiming.RecordError(e);
                            throw;
                        }
                    }
                } while (!cancellationToken.IsCancellationRequested);
            }
        }

        private LendersOffice.ObjLib.Edocs.Barcode[] GetBarcodesFromFileDBKey(string filedbname, string filedbkey)
        {
            StringBuilder debugMessage = new StringBuilder("GetBarcodesFromFileDBKey\n");

            try
            {
                string url = string.Format("{0}?DBName={1}&dbfilekey={2}", ConstStage.BarcodeReaderUrl, filedbname, filedbkey);
                debugMessage.AppendLine(url);
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
                webRequest.Timeout = 600000; //10 minutes
                using (WebResponse response = webRequest.GetResponse())
                using (Stream sr = response.GetResponseStream())
                using (XmlReader xmlReader = XmlReader.Create(sr))
                {
                    XDocument doc = XDocument.Load(xmlReader);
                    debugMessage.AppendLine(doc.ToString());

                    if (doc.Root.Attribute("status").Value == "Error")
                    {
                        throw CBaseException.GenericException(doc.Root.Attribute("error_message").Value);
                    }

                    List<LendersOffice.ObjLib.Edocs.Barcode> pdfPagesWithBarcode = new List<LendersOffice.ObjLib.Edocs.Barcode>();

                    foreach (XElement page in doc.Root.Elements("page"))
                    {
                        int number = int.Parse(page.Attribute("number").Value);

                        foreach (XElement barcode in page.Elements("barcode"))
                        {
                            string value = barcode.Attribute("value").Value;
                            LendersOffice.ObjLib.Edocs.Barcode b = new LendersOffice.ObjLib.Edocs.Barcode(number, value);
                            pdfPagesWithBarcode.Add(b);
                        }
                    }

                    return pdfPagesWithBarcode.OrderBy(p => p.Page).ToArray();
                }
            }
            finally
            {
                Tools.LogInfo(debugMessage.ToString());
            }
        }

        private void HandleMessage(DBMessage message)
        {
            DocMagicPDFData pdfData = ObsoleteSerializationHelper.JsonDeserialize<DocMagicPDFData>(message.Data);
            try
            {
                Trace.CorrelationManager.ActivityId = pdfData.FileDBKey;
                Split(pdfData);
            }
            catch (CBaseException e)
            {
                UpdateToErrorStatus(pdfData, e.UserMessage, e.Message);
                LogManager.Error(e.DeveloperMessage, e);
                throw;
            }
            catch (Exception e)
            {
                UpdateToErrorStatus(pdfData, ErrorMessages.Generic, e.ToString());
                LogManager.Error("HandleMessage - PDF Parser", e);
                throw;
            }
        }

        private static bool BarcodesContainLendingQBBarcode(IEnumerable<LendersOffice.ObjLib.Edocs.Barcode> barcodes)
        {
            int docTypeId;
            Func<LendersOffice.ObjLib.Edocs.Barcode, bool> isLendingQbBarcode =
                (b) => int.TryParse(b.Data, out docTypeId) || EdocFaxCover.IsBarcodeType(E_FaxCoverType.PDFProcessor, b.Data);

            return barcodes.Any(b => isLendingQbBarcode(b));
        }

        /// <summary>
        /// For Dropbox files with no barcodes. Addes file to Dropbox, and cleans up after itself.
        /// </summary>
        /// <param name="pdfData">PDF Data Object taken from message queue</param>
        private void HandleDropbox(DocMagicPDFData pdfData)
        {
            try
            {
                CreateDropboxFile(pdfData);
            }
            catch (Exception e)
            {
                LogManager.Error(pdfData.FileDBKey.ToString() + " PDf Errors " + e.Message);
                throw; // Bubble exception up.
            }
            finally
            {
                FileDBTools.Delete(pdfData.FileDB, pdfData.FileDBKey.ToString());
            }
        }

        private static E_DocBarcodeFormatT DetermineBarcodeFormat(IEnumerable<LendersOffice.ObjLib.Edocs.Barcode> barcodes)
        {
            // Code formerly checked for and handled LQB barcodes before getting
            // here. I'm leaving the check for LQB barcodes exclusive from the
            // check for vendor barcodes just to be safe. Not sure if it's
            // necessary.
            if (BarcodesContainLendingQBBarcode(barcodes))
            {
                return E_DocBarcodeFormatT.LendingQB;
            }

            foreach (var bc in barcodes)
            {
                if (DocuTechBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.DocuTech;
                else if (DocMagicBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.DocMagic;
                else if (IDSBarcode.MatchesFormat(bc)) return E_DocBarcodeFormatT.IDS;
            }

            //This'll error out anyway, since none of them look like a supported barcode.
            return E_DocBarcodeFormatT.DocMagic;
        }

        private bool LenderCanScanBarcodeFormat(BrokerDB broker, E_DocBarcodeFormatT format)
        {
            return format == E_DocBarcodeFormatT.LendingQB ||
                (format == E_DocBarcodeFormatT.DocMagic && broker.IsDocMagicBarcodeScannerEnabled) ||
                (format == E_DocBarcodeFormatT.DocuTech && broker.IsDocuTechBarcodeScannerEnabled) ||
                (format == E_DocBarcodeFormatT.IDS && broker.IsIDSBarcodeScannerEnabled);
        }

        private void HandleUploadWithBarcodes(LendersOffice.ObjLib.Edocs.Barcode[] barcodes, DocMagicPDFData pdfData)
        {
            var format = DetermineBarcodeFormat(barcodes);
            var broker = BrokerDB.RetrieveById(pdfData.BrokerId);

            if (this.LenderCanScanBarcodeFormat(broker, format))
            {
                // Prior to the refactor of this class, the merge feature check didn't occur 
                // for LQB pdfs.
                if (format != E_DocBarcodeFormatT.LendingQB && broker.IsEDocDoctypeMergeEnabled)
                {
                    // dd 7/30/2015 - As of right now no lender is using this feature. The feature was request by PML0177 (OPM 83484), 
                    // however PML0177 is no active and also right before they disable the feature was turn off in 2012.
                    throw new NotSupportedException("IsEDocDoctypeMergeEnabled feature is not support.");
                }

                this.SplitUploadWithBarcodes(pdfData, barcodes, format);
            }
            else
            {
                this.HandleUnsupportedBarcodeFormat(pdfData, format);
            }
        }

        private void SplitUploadWithBarcodes(DocMagicPDFData pdfData, LendersOffice.ObjLib.Edocs.Barcode[] barcodes, E_DocBarcodeFormatT format)
        {
            this.UpdateStatus(pdfData, E_DocMagicPDFStatus.CREATING_EDOCS);

            var splitter = DocumentSplitterFactory.GetDocumentSplitter(pdfData, barcodes, format);
            var result = splitter.SplitDocument();

            if (result.UploadSourceFileToDropbox)
            {
                HandleDropbox(pdfData);
            }
            else if (result.SplitDocuments.Any())
            {
                var uploadResult = this.UploadDocuments(pdfData, result.SplitDocuments, format);

                if (uploadResult.HasError)
                {
                    UpdateToErrorStatus(pdfData, ErrorMessages.Generic, uploadResult.DeveloperErrorMessage);
                    LogManager.Error(pdfData.FileDBKey.ToString() + " PDf Errors " + uploadResult.DeveloperErrorMessage);
                }
                else
                {
                    UpdateStatus(pdfData, E_DocMagicPDFStatus.COMPLETE);
                    FileDBTools.Delete(pdfData.FileDB, pdfData.FileDBKey.ToString());
                }
            }
            else
            {
                var msg = "Barcode scanner processed a PDF, didn't receive " +
                    "errors, but took no action on the file.";
                Tools.LogError(msg, new Exception());
            }
        }

        private void HandleUnsupportedBarcodeFormat(DocMagicPDFData pdfData, E_DocBarcodeFormatT format)
        {
            string usrMsg = "This document does not include LendingQB barcode sheets";

            string formatName = "DocMagic";
            if (format == E_DocBarcodeFormatT.DocuTech)
            {
                formatName = "DocuTech";
            }
            else if (format == E_DocBarcodeFormatT.IDS)
            {
                formatName = "IDS";
            }

            string devMsg = "and the broker does not allow " + formatName + " scanning, and the user opted to scan this document.";
            UpdateToErrorStatus(pdfData, usrMsg, devMsg);
        }

        private void HandleUploadWithoutBarcodes(DocMagicPDFData pdfData)
        {
            if (pdfData.isDropboxFile || pdfData.IsBarcodeUpload)
            {
                this.HandleDropbox(pdfData);
            }
            else
            {
                this.AddEDocWithUnreadableDocType(pdfData);
            }
        }

        private void AddEDocWithUnreadableDocType(DocMagicPDFData pdfData)
        {
            var mapper = new BaseDocTypeMapper(pdfData.BrokerId);
            var pdfPath = FileDBTools.CreateCopy(pdfData.FileDB, pdfData.FileDBKey.ToString());
            var isUploadedByPmlUser = IsUploadedByPmlUser(pdfData.BrokerId, pdfData.UploadingUserId);
            int unassignedDocTypeId, unreadableDocTypeId;
            mapper.GetUnassignedDocTypeAndUnreadableDocType(out unassignedDocTypeId, out unreadableDocTypeId);

            this.CreateEdoc(pdfPath, pdfData, pdfData.AppId, unreadableDocTypeId, pdfData.Description, isUploadedByPmlUser);
            this.UpdateStatus(pdfData, E_DocMagicPDFStatus.COMPLETE);
            // TODO: are we supposed to delete the file from FileDB??
        }

        public void Split(DocMagicPDFData pdfData)
        {
            this.UpdateStatus(pdfData, E_DocMagicPDFStatus.SCAN_BARCODES);

            LendersOffice.ObjLib.Edocs.Barcode[] barcodes = this.GetBarcodesFromFileDBKey(
                pdfData.FileDB.GetDatabaseName(),
                pdfData.FileDBKey.ToString());

            if (barcodes.Any())
            {
                this.HandleUploadWithBarcodes(barcodes, pdfData);
            }
            else
            {
                this.HandleUploadWithoutBarcodes(pdfData);
            }
        }

        private DocumentUploadResult UploadDocuments(DocMagicPDFData pdfData, IEnumerable<DocumentUpload> documentsToUpload, E_DocBarcodeFormatT format)
        {
            bool flagError = false;
            StringBuilder sb = new StringBuilder();

            foreach (var documentUpload in documentsToUpload)
            {
                try
                {
                    if (documentUpload.LoanId == Guid.Empty)
                    {
                        CreateDropboxFile(pdfData, documentUpload);
                    }
                    else
                    {
                        CreateEdoc(documentUpload);
                    }
                }
                catch (Exception e)
                {
                    flagError = true;
                    sb.AppendLine(e.Message);
                }
            }

            return new DocumentUploadResult(flagError, sb.ToString());
        }

        private void CreateEdoc(DocumentUpload upload)
        {
            bool isUploadedByPmlUser = IsUploadedByPmlUser(upload.BrokerId, upload.UploadingUserId);

            EDocumentRepository repo = EDocumentRepository.GetSystemRepository(upload.BrokerId);
            EDocument doc = repo.CreateDocument(E_EDocumentSource.UserUpload);
            doc.AppId = upload.AppId;
            doc.LoanId = upload.LoanId;
            doc.PublicDescription = upload.Description;
            doc.InternalDescription = upload.InternalDescription;
            doc.DocumentTypeId = upload.DocTypeId;
            doc.IsUploadedByPmlUser = isUploadedByPmlUser;
            doc.UpdatePDFContentOnSave(upload.PdfPath);
            // Not using repo.Save() because I need to save as a different user.
            doc.Save(upload.UploadingUserId);
        }

        private Guid CreateEdoc(string newDocpath, DocMagicPDFData data, Guid appId, int docTypeId, string description, bool isPmluser)
        {
            EDocumentRepository repo = EDocumentRepository.GetSystemRepository(data.BrokerId);
            EDocument doc = repo.CreateDocument(E_EDocumentSource.UserUpload);
            doc.AppId = appId;
            doc.LoanId = data.LoanId;
            doc.PublicDescription = description;
            doc.InternalDescription = data.InternalNotes;
            doc.DocumentTypeId = docTypeId;
            doc.IsUploadedByPmlUser = isPmluser;
            doc.UpdatePDFContentOnSave(newDocpath);
            doc.Save(data.UploadingUserId);  //sigh not using repo.save because I need to save as a different user
            return doc.DocumentId;
        }

        /// <summary>
        /// Adds doc to dropbox.
        /// </summary>
        /// <param name="data">The data associated with the document upload.</param>
        private void CreateDropboxFile(DocMagicPDFData data)
        {
            if (data.IsBarcodeUpload)
            {
                LostEDocument edoc = new LostEDocument();
                edoc.BrokerId = data.BrokerId;
                edoc.Description = data.DropboxFileName;
                edoc.DocumentSource = E_LostDocSource.BarcodeUploader;
                edoc.FaxNumber = "";
                edoc.PDFData = FileDBTools.ReadData(data.FileDB, data.FileDBKey.ToString());
                edoc.Save();
                return;
            }

            byte[] FileBytes = FileDBTools.ReadData(data.FileDB, data.FileDBKey.ToString());
            LendersOffice.ObjLib.UserDropbox.Dropbox.AddFile(data.UploadingUserId, data.BrokerId, data.DropboxFileName, data.DropboxUploadD, FileBytes);
        }

        private void CreateDropboxFile(DocMagicPDFData data, DocumentUpload upload)
        {
            if (data.IsBarcodeUpload)
            {
                LostEDocument edoc = new LostEDocument();
                edoc.BrokerId = upload.BrokerId;
                edoc.Description = data.DropboxFileName;
                edoc.DocumentSource = E_LostDocSource.BarcodeUploader;
                edoc.FaxNumber = "";
                edoc.PDFData = File.ReadAllBytes(upload.PdfPath);
                edoc.Save();
            }
            else
            {
                byte[] FileBytes = File.ReadAllBytes(upload.PdfPath);
                LendersOffice.ObjLib.UserDropbox.Dropbox.AddFile(data.UploadingUserId, data.BrokerId, data.DropboxFileName, data.DropboxUploadD, FileBytes);
            }
        }

        /// <summary>
        /// Determine if upload was performed by PML user.
        /// </summary>
        /// <param name="pdfData">The data associated with the upload.</param>
        /// <returns>True if uploaded by a PML user. Otherwise, false.</returns>
        private bool IsUploadedByPmlUser(Guid brokerId, Guid uploadingUserId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@userid", uploadingUserId)
                                        };

            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "Users_GetUserType", parameters))
            {
                if (reader.Read())
                {
                    string userType = (String)reader["UserType"];
                    return userType.Equals("P", StringComparison.OrdinalIgnoreCase);
                }
            }

            return false;
        }

        /// <summary>
        /// Update document status. Ignores dropbox files.
        /// </summary>
        /// <param name="pdfData">The data associated with the document upload.</param>
        /// <param name="status">DocMagic PDF Status.</param>
        private void UpdateStatus(DocMagicPDFData pdfData, E_DocMagicPDFStatus status)
        {
            // Don't update status for Dropbox files
            if (pdfData.isDropboxFile || pdfData.IsBarcodeUpload)
            {
                return;
            }

            DocMagicPDFManager.UpdateStatus(pdfData.BrokerId, pdfData.FileDBKey, status);
        }

        /// <summary>
        /// Set DocMagic PDF Status to Error. Ignores dropbox files.
        /// </summary>
        /// <param name="pdfData">The data associated with the document upload.</param>
        /// <param name="status">DocMagic PDF Status.</param>
        private void UpdateToErrorStatus(DocMagicPDFData pdfData, string userErrorMessage, string devErrorMessage)
        {
            // Don't update status for Dropbox files
            if (pdfData.isDropboxFile || pdfData.IsBarcodeUpload)
            {
                return;
            }

            DocMagicPDFManager.UpdateToErrorStatus(pdfData.BrokerId, pdfData.FileDBKey, userErrorMessage, devErrorMessage);
        }
    }
}
