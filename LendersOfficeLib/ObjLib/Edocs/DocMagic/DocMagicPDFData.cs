﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccess;
using LendersOffice.Security;
using System.Data.Common;
using System.Data.SqlClient;
using EDocs;
using LendersOffice.ObjLib.DatabaseMessageQueue;
using LendersOffice.ObjLib.Edocs.BarcodeScanning;
using LendersOffice.Common;

namespace LendersOffice.Edocs.DocMagic
{
    public class DocMagicPDFData : ICloneable, ISplitDocumentUploadSettings
    {
        public Guid UploadingUserId { get; set; }
        public Guid AppId { get; set; }
        public Guid BrokerId { get; set; }
        public Guid LoanId { get; set; }
        public string InternalNotes { get; set; }
        public string Description { get; set; }
        public Guid FileDBKey { get; set; }
        public E_FileDB FileDB { get; set; }
        public int PageCount { get; set; }
        
        public bool isDropboxFile { get; set; }
        public string DropboxFileName { get; set; }
        public DateTime DropboxUploadD { get; set; }

        public bool IsBarcodeUpload { get; set; }

        public string Serialize()
        {
            return ObsoleteSerializationHelper.JsonSerialize(this);
        }

        public static DocMagicPDFData Deserialize(string data)
        {
            return ObsoleteSerializationHelper.JavascriptJsonDeserializer<DocMagicPDFData>(data);            
        }

        public object Clone()
        {
            return MemberwiseClone();
        }

        public bool MustDetermineLoanIdFromBarcodes()
        {
            return this.LoanId == Guid.Empty && 
                (this.isDropboxFile || this.IsBarcodeUpload);
        }
    }


    public class DocMagicPDFStatusData
    {
        public string Application { get; private set; }
        public string Description { get; private set; }
        public string InternalComments { get; private set; }
        public int Pages { get; private set; }
        public string UploadedOn { get; private set; }
        public E_DocMagicPDFStatus Status { get; private set; }
        public string StatusNotes { get; private set; }
        public string Id { get; private set; }

        public DocMagicPDFStatusData( string id, string app, string desc, string comments, int pagecount, DateTime uploadTime, E_DocMagicPDFStatus status, string notes )
        {
            Application = app;
            Description = desc;
            InternalComments = comments;
            Pages = pagecount;
            UploadedOn = uploadTime.ToShortDateString();
            Status = status;
            StatusNotes = notes;
            Id = id;
        }
        
    }

    public enum E_DocMagicPDFStatus
    {
        IN_Q = 0,
        CONVERT_TIFF = 1,
        SCAN_BARCODES = 2,
        CREATING_EDOCS = 3, 
        COMPLETE = 4,
        ERROR = 100
    }

    public static class DocMagicPDFManager
    {
        private static E_FileDB DEFAULT_FILE_DB = E_FileDB.Temp;

        public static void RemoveDocMagicError(Guid brokerId, Guid id)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@Id", id)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_RemoveDocMagicErrorStatus", 3, parameters);
        }

        public static void UploadDocument(AbstractUserPrincipal principal, Guid sLId, Guid sAppId, string internalNotes, string publicNotes, string srcFileName)
        {
            UploadDocument(principal.UserId, principal.BrokerId, sLId, sAppId, internalNotes, publicNotes, srcFileName);
        }
        public static void UploadDocument(Guid UserId, Guid BrokerId, Guid sLId, Guid sAppId, string internalNotes, string publicNotes, string srcFileName)
        {
            //make sure the doc is a pdf
            var pdf = EDocumentViewer.CreatePdfReader(srcFileName);


            Guid key = Guid.NewGuid();
            FileDBTools.WriteFile(E_FileDB.Temp, key.ToString(), srcFileName);


            DocMagicPDFData uploadData = new DocMagicPDFData()
            {
                UploadingUserId = UserId,
                AppId = sAppId,
                BrokerId = BrokerId,
                LoanId = sLId,
                InternalNotes = internalNotes,
                Description = publicNotes,
                FileDB = DEFAULT_FILE_DB,
                FileDBKey = key,
                PageCount = pdf.NumberOfPages
            };

            SqlParameter[] parameters = {
                                            new SqlParameter("@Id", key), 
                                            new SqlParameter("@sLId", sLId), 
                                            new SqlParameter("@Data", uploadData.Serialize())
                                        };
            StoredProcedureHelper.ExecuteNonQuery(BrokerId, "EDOCS_AddDocMagicPDFStatus", 3, parameters);

            DBMessageQueue mainQ = new DBMessageQueue(ConstMsg.DocMagicDocSplitterQueue);
            mainQ.Send(key.ToString(), uploadData.Serialize());
        }


        public static IList<DocMagicPDFStatusData> GetUploadsForLoan(Guid brokerId, Guid sLId)
        {
            var resultSet = new List<Dictionary<string, object>>();
            SqlParameter[] parameters = {
                                            new SqlParameter("@LoanId", sLId), 
                                            new SqlParameter("@BrokerId", brokerId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetDocMagicPDFForLoanId", parameters))
            {
                while (reader.Read())
                {
                    resultSet.Add(reader.ToDictionary());
                }
            }

            //check dates and email if something is taking too long ?
            List<DocMagicPDFStatusData> statusData = new List<DocMagicPDFStatusData>(resultSet.Count);

            CPageData loanData = CPageData.CreateUsingSmartDependency(sLId, typeof(DocMagicPDFManager));
            bool isLoaded = false;
            foreach (var reader in resultSet)
            {
                if (false == isLoaded)
                {
                    loanData.InitLoad();
                    isLoaded = true;
                }

                DocMagicPDFData data = ObsoleteSerializationHelper.JavascriptJsonDeserializer<DocMagicPDFData>(((string)reader["Data"]));
                CAppData app = loanData.GetAppData(data.AppId);

                string aBNm_aCNm = string.Empty;
                if (app == null)
                {
                    aBNm_aCNm = string.Empty; // Application is delete in our system.
                }
                else
                {
                    aBNm_aCNm = app.aBNm_aCNm;
                }
                statusData.Add(new DocMagicPDFStatusData(reader["Id"].ToString(), aBNm_aCNm, data.Description, data.InternalNotes, data.PageCount,
                    (DateTime)reader["UploadD"], (E_DocMagicPDFStatus)Convert.ToInt32(reader["CurrentStatus"]), (string)reader["PublicNotes"]));
            }

            return statusData;
        }

        public static void UpdateStatus(Guid brokerId, Guid key, E_DocMagicPDFStatus status)
        {
            if (status == E_DocMagicPDFStatus.ERROR)
            {
                throw CBaseException.GenericException("Use UpdateToErrorStatus(string,string,string) instead.");
            }

            if (status == E_DocMagicPDFStatus.IN_Q)
            {
                throw CBaseException.GenericException("This status is the first one. Theres no going back.");
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@Id", key),
                                            new SqlParameter("@CurrentStatus", status)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_UpdateDocMagicPDFStatus", 3, parameters);
        }

        public static void UpdateToErrorStatus(Guid brokerId, Guid key, string userErrorMessage, string devErrorMessage)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@Id", key),
                                            new SqlParameter("@CurrentStatus", E_DocMagicPDFStatus.ERROR), 
                                            new SqlParameter("@PublicNotes", userErrorMessage),
                                            new SqlParameter("@DevNotes", devErrorMessage)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_UpdateDocMagicPDFStatus", 3, parameters);
        }

    }
}
