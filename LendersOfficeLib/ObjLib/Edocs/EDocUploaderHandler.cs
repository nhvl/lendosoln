﻿using System;
using System.Web;
using LendersOffice.Security;
using System.IO;
using DataAccess;
using EDocs;

namespace LendersOffice.ObjLib.Edocs
{
    public class EDocUploaderHandler : IHttpHandler
    {
        /// <summary>
        /// You will need to configure this handler in the web.config file of your 
        /// web and register it with IIS before being able to use it. For more information
        /// see the following link: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            string name = context.Request.Form["name"];
            if (string.IsNullOrEmpty(name))
            {
                return; //error
            }

            int chunk = int.Parse(context.Request.Form["chunk"]);
            int chunks = int.Parse(context.Request.Form["chunks"]);
            name = TempFileUtils.Name2Path(PrincipalFactory.CurrentPrincipal.UserId.ToString("N") +"_"+ name);

            Tools.LogError(name);
            using (FileStream fs = File.Open(name, FileMode.Append, FileAccess.Write))
            {
              
                    byte[] buffer = new byte[0x10000];
                    int n;
                    while ((n = context.Request.Files[0].InputStream.Read(buffer, 0, buffer.Length)) != 0)
                        fs.Write(buffer, 0, n);
            }

            if (chunk+1 == chunks)
            {
                var repo = EDocumentRepository.GetUserRepository();
                var doc = repo.CreateDocument(E_EDocumentSource.UserUpload);
                doc.InternalDescription = context.Request.Form["internalcomments"];
                doc.DocumentTypeId = int.Parse(context.Request.Form["picker"]);
                doc.LoanId = new Guid(context.Request.Form["loanid"]);
                doc.AppId = new Guid(context.Request.Form["appid"]);
                doc.IsUploadedByPmlUser = PrincipalFactory.CurrentPrincipal.Type == "P";
                doc.PublicDescription = context.Request.Form["description"];
                doc.UpdatePDFContentOnSave(name);
                doc.EDocOrigin = doc.IsUploadedByPmlUser ? E_EDocOrigin.PML : E_EDocOrigin.LO;
                repo.Save(doc);
            }
        }

        #endregion
    }
}
