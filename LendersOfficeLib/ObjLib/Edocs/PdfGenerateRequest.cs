﻿namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>
    /// JSON/Protobuf representation of the Non Destructive <code>Edocs</code>. I used .NET serialization attribute instead of Json.net
    /// and Protobuf-net because it require less code and remove dependency on the serialization library.
    /// Note: Protobuf protocol require numeric ordering. Therefore DO NOT change the numeric once define.
    /// </summary>
    [DataContract]
    public class PdfGenerateRequest
    {
        /// <summary>
        /// Gets or sets an eDocs metadata object.
        /// </summary>
        [DataMember(Name = "edocs", Order = 1)]
        public EdocsMetadata EdocsMetadata { get; set; }

        /// <summary>
        /// Gets or sets a dictionary contains mapping of document id and local file path.
        /// </summary>
        [DataMember(Name = "path_list", Order = 2)]
        public Dictionary<Guid, string> FilePathDictionary { get; set; }
    }
}
