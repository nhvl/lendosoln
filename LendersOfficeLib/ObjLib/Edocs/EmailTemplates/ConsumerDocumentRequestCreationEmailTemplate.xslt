﻿<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="DocumentNotificationData">
    <html>
      <body>
        <xsl:if test="string-length(LogoCID) &gt; 0">
          <p>
            <img>
              <xsl:attribute name="src">
                <xsl:value-of select="LogoCID"/>
              </xsl:attribute>
            </img>
          </p>
        </xsl:if>
        <xsl:if  test="string-length(ConsumerName) &gt; 0">
          <p>
            Dear <xsl:value-of select="ConsumerName" ></xsl:value-of>,
          </p>
        </xsl:if>
        <p>
          Your signature is needed on 
          <xsl:choose>
            <xsl:when test="IsForMultipleDocuments = 'true'">
              multiple documents
            </xsl:when>
            <xsl:otherwise>
              a document 
            </xsl:otherwise>
          </xsl:choose>
          to process your application.
        </p>
        <p>
          To retrieve and sign the 
          <xsl:choose>
            <xsl:when test="IsForMultipleDocuments = 'true'">
              documents,&#160;
            </xsl:when>
            <xsl:otherwise>
              document,&#160;
            </xsl:otherwise>
          </xsl:choose>
          please go to the website below<xsl:if  test="string-length(CPTempPassword) &gt; 0">
            within <xsl:value-of select="TempPasswordValidLength"/> days
          </xsl:if>:
        </p>
        <p>
          <xsl:value-of select="LoginUrl" />
        </p>

        <xsl:if  test="string-length(CPTempPassword) &gt; 0">
          <p>
            You will need to enter your email address (<xsl:value-of select="Email"></xsl:value-of>) and the password below to login to the Website:
          </p>
          <p>
            Temporary Password: <xsl:value-of select="CPTempPassword" ></xsl:value-of>
          </p>
          <p>
            For security of your information, you will be prompted to change your password after login.
          </p>
        </xsl:if>
        <p>
          Regards,
        </p>
          <xsl:value-of select="RequesterName"/>
          <xsl:text>&#10;</xsl:text>
        <br />
          <xsl:value-of select="CompanyName"/>
          <xsl:text>&#10;</xsl:text>
        <br />
          <xsl:value-of select="PhoneNumber"/>

          <xsl:if test="string-length(NMLS) > 0">
              <br/>
              NMLS#: <xsl:value-of select="NMLS"/>
          </xsl:if>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
