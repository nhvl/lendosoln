﻿<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="DocumentNotificationData">
    <html>
      <body>
        <xsl:if test="string-length(LogoCID) &gt; 0">
          <img>
            <xsl:attribute name="src">
              <xsl:value-of select="LogoCID"/>
            </xsl:attribute>
          </img>
        </xsl:if>
        <xsl:if  test="string-length(ConsumerName) &gt; 0">
          <p>
            Dear <xsl:value-of select="ConsumerName" ></xsl:value-of>,
          </p>
        </xsl:if>

        <p>
          To view the borrower portal for your loan application, please go to the website below within <xsl:value-of select="TempPasswordValidLength"/> days:
        </p>
        <p>
          <xsl:value-of select="LoginUrl" />
        </p>
        <p>
          You will need to enter your email address (<xsl:value-of select="Email"></xsl:value-of>) and the password below to login to the Website:
        </p>
        <p>
          Temporary Password: <xsl:value-of select="CPTempPassword" ></xsl:value-of>
        </p>
        <p>
          For security of your information, you will be prompted to change your password after login.
        </p>
        <p>
          Regards,
        </p>
        <xsl:value-of select="RequesterName"/>
        <xsl:text>&#10;</xsl:text>
        <br />
        <xsl:value-of select="CompanyName"/>
        <xsl:text>&#10;</xsl:text>
        <br />
        <xsl:value-of select="PhoneNumber"/>

          <xsl:if test="string-length(NMLS) > 0">

              <br/>
              NMLS#: <xsl:value-of select="NMLS"/>
          </xsl:if>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
