﻿<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
<xsl:output method="html" encoding="utf-8" omit-xml-declaration="yes" />
<xsl:template match="DocumentNotificationData">
<html>
  <body>
    <xsl:value-of select="RecipentName"/>,<br /><br />
    We have received the following electronic document<xsl:if test="Count &gt; 1">s</xsl:if> for loan &#x22;<xsl:value-of select="sLNm"/>&#x22; on <xsl:value-of select="ReceivedOn"/>.<br /><br />
    <table>
      <xsl:for-each select="doc">
      <tr>
        <td>
          <xsl:value-of select="position()" />.
        </td>
        <td colspan="2">
          Document Type - <xsl:value-of select="DocumentType"/>
        </td>
      </tr>
      <tr>
        <td></td>
        <td>* </td>
        <td>
          Folder - <xsl:value-of select="FolderName"/>
        </td>
      </tr>
      <tr>
        <td></td>
        <td>* </td>
        <td>
          Application - <xsl:value-of select="ApplicationName"/>
        </td>
      </tr>
      <tr>
        <td></td>
        <td valign="top">* </td>
        <td>
          Description - <xsl:value-of select="PublicDescription"/><br /><br />
        </td>
      </tr>
    </xsl:for-each>
    </table>
    To view the document<xsl:if test="Count &gt; 1">s</xsl:if>:<br />
    1. Login to LendingQB.<br />
    2. Open loan: &#x22;<xsl:value-of select="sLNm"/>&#x22;<br />
    3. In the loan file window, open the "EDocs" folder and click on the<br />
    "Documents" link.<br />
    4. Click "actions" then the "view pdf" link for the desired document.<br />
    <br />
    This notification was automatically generated for you by LendingQB.<br />
    Please do not directly reply to this email. Please contact your <br />
    LendingQB system administrator for any questions.
  </body>
</html>
</xsl:template>
</xsl:stylesheet>
