﻿<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="DocumentNotificationData">
    <html>
      <body>
        <xsl:if test="string-length(LogoCID) &gt; 0">
          <p>
            <img>
              <xsl:attribute name="src">
                <xsl:value-of select="LogoCID"/>
              </xsl:attribute>
            </img>
          </p>
        </xsl:if>
        <p>
          Dear <xsl:value-of select="ConsumerName" ></xsl:value-of>,
        </p>
        <p>
          <xsl:choose>
            <xsl:when test="IsForMultipleDocuments = 'true'">
              Documents are requested
            </xsl:when>
            <xsl:otherwise>
              A document is requested
            </xsl:otherwise>
          </xsl:choose>
          to process your application.
        </p>
        <p>
          To upload your documents electronically or retrieve the cover 
          <xsl:choose>
            <xsl:when test="IsForMultipleDocuments = 'true'">
              sheets
            </xsl:when>
            <xsl:otherwise>
              sheet 
            </xsl:otherwise>
          </xsl:choose>
          for faxing, please go to the website below<xsl:if  test="string-length(CPTempPassword) &gt; 0">
            within <xsl:value-of select="TempPasswordValidLength"/> days
          </xsl:if>:
        </p>

        <xsl:value-of select="LoginUrl" />
        <xsl:if  test="string-length(CPTempPassword) &gt; 0">
          <p>
            You will need to enter your email address (<xsl:value-of select="Email"></xsl:value-of>) and the password below to login to the Website:
          </p>
          <p>
            Temporary Password: <xsl:value-of select="CPTempPassword" ></xsl:value-of>
          </p>
          <p>
            For security of your information, you will be prompted to change your password after login.
          </p>
        </xsl:if>
        <p>
          Regards,
        </p>
          <xsl:value-of select="RequesterName"/>
          <xsl:text>&#10;</xsl:text>
        <br />
          <xsl:value-of select="CompanyName"/>
          <xsl:text>&#10;</xsl:text>
        <br />
          <xsl:value-of select="PhoneNumber"/>
          <xsl:if test="string-length(NMLS) > 0">
            NMLS#: <xsl:value-of select="NMLS"/>
          </xsl:if>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
