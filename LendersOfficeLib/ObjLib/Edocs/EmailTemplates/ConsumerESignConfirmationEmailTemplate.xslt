﻿<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="text" encoding="utf-8" omit-xml-declaration="yes" />
    <xsl:template match="DocumentNotificationData">
This email confirms that <xsl:value-of select="WhoSigned"></xsl:value-of> has signed the following document on <xsl:value-of select="SignedOn"></xsl:value-of>.

Document Type : <xsl:value-of select="DocTypeName"></xsl:value-of>
Description   : <xsl:value-of select ="DocDescription"></xsl:value-of>

Regards,

<xsl:value-of select="CompanyName"/><xsl:text>&#10;</xsl:text>
<xsl:value-of select="PhoneNumber"/>
</xsl:template>
</xsl:stylesheet>
