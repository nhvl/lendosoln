﻿<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl">
    <xsl:output method="text" encoding="utf-8" omit-xml-declaration="yes" />
    <xsl:template match="DocumentNotificationData">
The following document has been <xsl:value-of select="ReceivalType"/> for loan &#x22;<xsl:value-of select="sLNm"/>&#x22; application &#x22;<xsl:value-of select="ApplicationNames"/>&#x22; 

Document Type     - <xsl:value-of select="DocumentType"/>
Folder            - <xsl:value-of select="FolderName"/>
Description       - <xsl:value-of select="PublicDescription"/>

To view all consumer request including recently completed request:
  1) Log into LendingQB
  2) Open loan &#x22;<xsl:value-of select="sLNm"/>&#x22;
  3) Under EDocs in the loan navigation click on Document Request

Thank you for choosing LendingQB, we appreciate your business.

Best regards,

The LendingQB Team
    </xsl:template>
</xsl:stylesheet>
