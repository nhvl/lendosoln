﻿namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using DataAccess;
    using iTextSharp.text;
    using iTextSharp.text.pdf;

    /// <summary>
    /// This class contains the current in-process pdf generation using iTextSharp. We could remove this class once we move all iTextSharp or PDF operation to the new WebAPI.
    /// </summary>
    public class LegacyITextSharpPdfService : IPdfService
    {
        /// <summary>
        /// Generate a pdf and return a local path of new pdf.
        /// </summary>
        /// <param name="metaData">Metadata of an edocs to generate PDF.</param>
        /// <returns>The path of the output pdf.</returns>
        public string GeneratePdf(EdocsMetadata metaData)
        {
            Dictionary<Guid, PdfReader> pdfReaderDictionary = new Dictionary<Guid, PdfReader>();

            foreach (var page in metaData.Pages)
            {
                if (!pdfReaderDictionary.ContainsKey(page.ReferenceDocumentId))
                {
                    string path = EDocument.GetNonDestructivePdfFromFileDb(page.ReferenceDocumentId);
                    PdfReader reader = EDocumentViewer.CreatePdfReader(path);
                    pdfReaderDictionary.Add(page.ReferenceDocumentId, reader);
                }
            }

            string newPath = TempFileUtils.NewTempFilePath() + ".pdf";

            Document document = new Document();
            using (FileStream fs = File.OpenWrite(newPath))
            {
                PdfCopy pdfWriter = new PdfCopy(document, fs);
                document.Open();

                foreach (var page in metaData.Pages)
                {
                    PdfReader pdfReader = pdfReaderDictionary[page.ReferenceDocumentId];

                    if (page.Rotation != 0)
                    {
                        // 12/7/2015 - dd - Case 233252. The original PDF does not neccessary have the PdfName.ROTATE value at 0.
                        //    Since page.Rotation contains the final desire roation, we need to add this to the existing rotation to
                        //    get the right effect.
                        int rotation = (pdfReader.GetPageRotation(page.ReferenceDocumentPageNumber + 1) + page.Rotation) % 360;
                        pdfReader.GetPageN(page.ReferenceDocumentPageNumber + 1).Put(PdfName.ROTATE, new PdfNumber(rotation));
                    }

                    PdfImportedPage importedPage = pdfWriter.GetImportedPage(pdfReader, page.ReferenceDocumentPageNumber + 1);
                    pdfWriter.AddPage(importedPage);
                }

                document.Close();
            }

            foreach (var pdfReader in pdfReaderDictionary.Values)
            {
                pdfReader.Close();
            }

            return newPath;
        }
    }
}