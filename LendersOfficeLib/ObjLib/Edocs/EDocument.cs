﻿namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Drawing.Drawing2D;
    using System.IO;
    using System.Linq;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;
    using System.Web;
    using System.Xml;
    using DataAccess;
    using EDocs.Utils;
    using iTextSharp.text.pdf;
    using LendersOffice.Admin;
    using LendersOffice.Common;
    using LendersOffice.Constants;
    using LendersOffice.Drivers.SqlServerDB;
    using LendersOffice.Integration.DocumentCapture;
    using LendersOffice.ObjLib.DatabaseMessageQueue;
    using LendersOffice.PdfLayout;
    using LendersOffice.Security;
    using LqbGrammar.DataTypes;
    using Newtonsoft.Json;

    public enum E_EDocumentSource
    {
        FromIntegration,
        UserUpload,
        AcceptedFromConsumer,
        Fax,
        SplitDoc, 
        GeneratedDocs,
        CBCDrive,
        FloodService,
        ESignPOST,
        UserCopy, // If using UserCopy, should also set m_copiedFromFileName
        NotApplicable, // this is default 
        DeletedPages, // m_copiedFromFileName also required
        EditableCopyOfSignedDoc,
    }

    public enum E_EDocStatus
    {
        Blank = 0,
        Obsolete,
        Approved,
        Rejected,
        Screened
    }

    /// <summary>
    /// Encapsulates an electronic document that is sent to the system.
    /// </summary>
    public sealed class EDocument
    {
        public const string POST_ACTION_UPDATE_STATUS_ONLY = "StatusOnly";
        public const string POST_ACTION_UPDATE_METADATA = "Metadata";
        public const string POST_ACTION_UPDATE_METADATA_EXCLUDE_ORIGINAL = "MetaDataNoOriginal";
        public const string POST_ACTION_UPDATE_MIGRATION = "MigrationUpdate";

        #region SP vars
        private static string UPDATE_SP = "EDOCS_UpdateDocument";
        private static string CREATE_SP = "EDOCS_CreateDocument";
        private static string FETCH_SP_NEW = "EDOCS_FetchDocsOptimized";
        private static string FETCH_SHIPPING_TEMPLATE_DOCS_SP = "EDOCS_GetDocumentsInShippingTemplate";
        private static string INVALIDATE_SP = "EDOCS_InvalidateDoc";
        private static string RESTORE_SP = "EDOCS_Restore";
        #endregion

        #region member variables
        #region declerations
        private Guid? m_sLId;
        private Guid? m_aAppId;
        private string m_sFaxNumber;
        private Guid m_fileDbKey_Original;
        private Guid m_fileDbKey_CurrentRevision;
        private string m_sInternalDescription;
        private string m_sPublicDescription;
        private string m_sComments;
        private int m_DocTypeId;

        private bool m_IsUploadedByPmlUser;
        private DateTime m_LastModified;
        private DateTime m_CreatedDate;
        private Guid? m_createdFromExistingDocumentId = new Guid?();

        private EDocumentFolder m_Folder;
        private bool m_isEditable;
        private EDocumentAnnotationContainer m_annotations;
        private string m_sAppName = "";
        private bool m_requiresRegeneration = false;
        private string m_sNewPdfPath = null;
        private string m_sOriginalPdfPath = null;
        private string m_slastDeletedBy;
        private DateTime? m_lastDeletedOn;
        private E_EDocumentSource m_creationSource = E_EDocumentSource.NotApplicable; //only used on creation
        private string m_copiedFromFileName; // only used for copied docs on creation
        private bool m_hideFromPMLUsers = false; // OPM 64817

        private E_EDocStatus m_eStatus;
        private string m_sStatusDescription = string.Empty;

        private readonly bool generateNonDestructiveEdocsUsingAmazon = ConstStage.GenerateNonDestructiveEdocsUsingAmazon;

        public bool PdfHasUnsupportedFeatures { get; private set; }

        public E_EDocumentSource CreationSource
        {
            get { return m_creationSource; }
        }


        private IList<EDocumentAuditEvent> m_auditEvents;

        private LinkedList<EDocumentIntermediateAuditEvent> m_internalNewAuditEvents = new LinkedList<EDocumentIntermediateAuditEvent>(); //opm 67127 av add more granular logging to edocs
        private LinkedList<EDocumentIntermediateAuditEvent> m_externalNewAuditEvents = new LinkedList<EDocumentIntermediateAuditEvent>(); //opm 67127 av add more granular logging to edocs
        #endregion

        public string LastDeletedBy
        {
            get
            {
                if (string.IsNullOrEmpty(m_slastDeletedBy))
                {
                    EDocumentAuditEvent auditEvent = AuditEvents.First();
                    m_slastDeletedBy = auditEvent.Name;
                    m_lastDeletedOn = auditEvent.ModifiedDate;
                }

                return m_slastDeletedBy;
            }

        }

        public DateTime LastDeletedOn
        {
            get
            {
                if (false == m_lastDeletedOn.HasValue)
                {
                    EDocumentAuditEvent auditEvent = AuditEvents.First();
                    m_slastDeletedBy = auditEvent.Name;
                    m_lastDeletedOn = auditEvent.ModifiedDate;
                }

                return m_lastDeletedOn.Value;
            }

        }

        private DocType x_DocType = null;
        public DocType DocType
        {
            get
            {
                if (x_DocType != null)
                {
                    return x_DocType;
                }

                x_DocType = EDocumentDocType.GetDocTypeById(this.BrokerId, m_DocTypeId);

                return x_DocType;
            }
        }

        /// <summary>
        /// Upon save new electronic documents require some configuration changes.
        /// </summary>
        private bool m_hasInternalAnnotation;

        /// <summary>
        /// Indicates if an EDoc has any esign annotations. Only used if the esign tags container has not yet been loaded.
        /// </summary>
        private bool m_hasESignTags;

        /// <summary>
        /// Separate container containing the esign tags.
        /// </summary>
        private EDocumentESignTagContainer m_eSignTags;


        private DateTime? m_timeToRequeueForImageGeneration;

        private DocumentMetaData m_newMetaData = null;

        private string m_metadataJsonContentFromDatabase = null;

        private EdocsMetadata x_edocsMetaData = null; // 1/27/2016 - dd - Do not use this variable directly. Only m_edocsMetaData can access it.
        private bool x_edocsMetaDataInitialized = false;

        private byte[] x_MetadataProtobufContent = null;
        private byte[] x_OriginalMetadataProtobufContent = null;

        private EdocsMetadata m_edocsMetaData
        {
            get
            {
                if (this.x_edocsMetaDataInitialized == false)
                {
                    if (this.IsNew == false)
                    {
                        bool isLoad = false;

                        if (this.x_MetadataProtobufContent != null)
                        {
                            this.x_edocsMetaData = SerializationHelper.ProtobufDeserialize<EdocsMetadata>(x_MetadataProtobufContent);
                            isLoad = true;
                        }

                        // dd 9/17/2018 - We will be able to remove the below code if all rows in EDOCS_DOCUMENT MetadataProtobufContent is not null.
                        if (!isLoad)
                        {
                            // 1/27/2016 - dd - Only need to check if we need to deserialize EdocsMetaData on existing document.
                            string json = string.Empty;

                            try
                            {
                                json = FileDBTools.ReadDataText(E_FileDB.EDMS, this.MetadataJsonContentFileDBKey);
                            }
                            catch (FileNotFoundException)
                            {
                                json = this.m_metadataJsonContentFromDatabase;
                            }

                            if (string.IsNullOrEmpty(json) == false)
                            {
                                this.x_edocsMetaData = SerializationHelper.JsonNetDeserialize<EdocsMetadata>(json);
                            }
                        }
                    }

                    this.x_edocsMetaDataInitialized = true;
                }

                return x_edocsMetaData;
            }
            set
            {
                this.x_edocsMetaData = value;
                this.x_edocsMetaDataInitialized = true;
            }
        }

        private string m_originalMetadataJsonContentFromDatabase = null;

        private EdocsMetadata x_originalEdocsMetaData = null; // 1/27/2016 - dd - Do not use this variable directly. Only m_originalEdocsMetaData can access it.
        private bool x_originalEdocsMetaDataIntialized = false;

        private EdocsMetadata m_originalEdocsMetaData
        {
            get
            {
                if (this.x_originalEdocsMetaDataIntialized == false)
                {
                    if (this.IsNew == false)
                    {
                        bool isLoad = false;

                        if (this.x_OriginalMetadataProtobufContent != null)
                        {
                            this.x_originalEdocsMetaData = SerializationHelper.ProtobufDeserialize<EdocsMetadata>(x_OriginalMetadataProtobufContent);
                            isLoad = true;
                        }

                        // dd 9/17/2018 - We will be able to remove the below code if all rows in EDOCS_DOCUMENT OriginalMetadataProtobufContent is not null.
                        if (!isLoad)
                        {
                            // 1/27/2016 - dd - Only need to check if we need to deserialize OriginalEdocsMetaData on existing document.
                            string json = string.Empty;

                            try
                            {
                                json = FileDBTools.ReadDataText(E_FileDB.EDMS, this.OriginalMetadataJsonContentFileDBKey);
                            }
                            catch (FileNotFoundException)
                            {
                                json = this.m_originalMetadataJsonContentFromDatabase;
                            }

                            if (string.IsNullOrEmpty(json) == false)
                            {
                                this.x_originalEdocsMetaData = SerializationHelper.JsonNetDeserialize<EdocsMetadata>(json);
                            }
                        }
                    }

                    this.x_originalEdocsMetaDataIntialized = true;
                }

                return x_originalEdocsMetaData;
            }
            set
            {
                this.x_originalEdocsMetaData = value;
                this.x_originalEdocsMetaDataIntialized = true;
            }
        }

        #endregion

        #region accessors

        public EdocsMetadata OriginalEdocsMetadata { get { return m_originalEdocsMetaData; } }

        public EdocsMetadata EdocsMetadata { get { return m_edocsMetaData; } }
        private DocumentMetaData PdfMetaData { get; set; }

        public E_EDocOrigin EDocOrigin { get; set; }

        /// <summary>
        /// Returns true if the document is new ( still needs to save ) or was loaded from existing data.
        /// </summary>
        public bool IsNew { get; private set; }

        public bool RequiresDocTypeChange { get; private set; }

        public string AppName
        {
            get
            {
                return m_sAppName;
            }
        }

        public IList<EDocumentAuditEvent> AuditEvents
        {
            get
            {
                if (this.IsNew)
                {
                    throw new CBaseException(ErrorMessages.Generic, "DEV ERROR  : Trying to access audit events for a new object.");
                }
                if (m_auditEvents == null)
                {
                    m_auditEvents = EDocumentAuditEvent.RetrieveAuditEventsForDoc(this.BrokerId, this.DocumentId).OrderByDescending(p => p.ModifiedDate).ToList();
                }

                return m_auditEvents;
            }
        }

        public string PdfFileDBKeyCurrentRevision
        {
            get
            {
                return m_fileDbKey_CurrentRevision.ToString();
            }
        }

        public string GetPDFTempFile_Original()
        {
            if (this.generateNonDestructiveEdocsUsingAmazon && this.m_originalEdocsMetaData != null)
            {
                var broker = PrincipalFactory.CurrentPrincipal?.BrokerDB ?? BrokerDB.RetrieveById(this.BrokerId);
                if (broker.IsTestingAmazonEdocs)
                {
                    try
                    {
                        var token = this.GenerateAmazonViewToken(useOriginalVersion: true);
                        return AmazonEDocHelper.RetrievePdf(token);
                    }
                    catch (System.Net.WebException exc)
                    {
                        // Fall back to non-Amazon behavior.
                        Tools.LogError(exc);
                    }
                }
            }

            if (this.m_originalEdocsMetaData != null)
            {
                // 6/25/2015 dd - NonDestructive Mode - Generate Pdf from metadata.
                return GenerateNonDestructiveEdocs(this.m_originalEdocsMetaData, getOriginal: this.IsESigned);
            }
            else
            {
                return CopyFileDBEntry(m_fileDbKey_Original.ToString());
            }
        }

        /// <summary>
        /// Use this when building shipping package or shipping to user
        /// Returns the original if the pdf has unsupported features or
        /// if the document is e-signed.
        /// </summary>
        public string GetPDFTempFile_Current()
        {
            if (this.PdfHasUnsupportedFeatures || this.IsESigned)
            {
                return GetPDFTempFile_Original();
            }

            if (this.generateNonDestructiveEdocsUsingAmazon && this.m_edocsMetaData != null)
            {
                var broker = PrincipalFactory.CurrentPrincipal?.BrokerDB ?? BrokerDB.RetrieveById(this.BrokerId);
                if (broker.IsTestingAmazonEdocs)
                {
                    try
                    {
                        var token = this.GenerateAmazonViewToken(useOriginalVersion: false);
                        return AmazonEDocHelper.RetrievePdf(token);
                    }
                    catch (System.Net.WebException exc)
                    {
                        // Fall back to non-Amazon behavior.
                        Tools.LogError(exc);
                    }
                }
            }

            if (this.m_edocsMetaData != null)
            {
                // 6/25/2015 dd - NonDestructive Mode - Generate Pdf from metadata.
                return GenerateNonDestructiveEdocs(this.m_edocsMetaData);
            }
            else
            {
                // 6/25/2015 dd - Legacy Mode - Return PDF from filedb.
                return CopyFileDBEntry(m_fileDbKey_CurrentRevision.ToString());
            }
        }

        /// <summary>
        /// Get whether the document is valid.
        /// </summary>
        public bool IsValid { get; private set; }

        /// <summary>
        /// Get/Set whether the document has been accepted by the lender
        /// </summary>
        public bool IsAccepted
        {
            get { return m_isEditable == false; }
        }

        /// <summary>
        /// Gets the file type of the associated generic document, if any.
        /// </summary>
        public Contents.E_FileType? GenericFileType { get; private set; }

        public void MarkDocumentAsAccepted()
        {
            m_isEditable = false;
        }

        private void MarkAsESigned()
        {
            this.IsESigned = true;
            this.m_isEditable = false;
        }

        /// <summary>
        /// Get the current doc type name. (currently out of sync after a save get a new object)
        /// </summary>
        public string DocTypeName { get; private set; }

        public string FolderAndDocTypeName
        {
            get
            {
                return Folder.FolderNm + " - " + DocTypeName;
            }
        }

        public int FolderId { get; private set; }

        public EDocumentFolder Folder
        {
            get
            {
                if (m_Folder == null || m_Folder.FolderId != FolderId)
                {
                    m_Folder = new EDocumentFolder(this.BrokerId, FolderId);
                }

                return m_Folder;
            }
        }

        /// <summary>
        /// Get/Set the document type id
        /// </summary>
        public int DocumentTypeId
        {
            get { return m_DocTypeId; }
            set
            {
                if (value != m_DocTypeId)
                {
                    m_internalNewAuditEvents.AddLast(new EDocumentIntermediateAuditEvent()
                    {
                        ActionD = DateTime.Now,
                        UserAction = E_PdfUserAction.EditDocType
                    });
                }
                m_DocTypeId = value;
            }
        }

        /// <summary>
        /// Get/Set the broker id
        /// </summary>
        public Guid BrokerId { get; private set; }

        /// <summary>
        /// Gets the document id.
        /// </summary>
        public Guid DocumentId { get; private set; }

        /// <summary>
        /// Get the loan id. Returns Guid.Empty if none are set.
        /// </summary>
        public Guid? LoanId
        {
            get
            {
                return m_sLId;
            }
            set
            {
                m_sLId = value;
            }
        }

        /// <summary>
        /// Get the app id. Returns Guid.Empty if none are set.
        /// </summary>
        public Guid? AppId
        {
            get
            {
                return m_aAppId;
            }
            set
            {
                m_aAppId = value;
            }
        }

        /// <summary>
        /// Get/Set the fax number
        /// </summary>
        public string FaxNumber
        {
            get { return m_sFaxNumber; }
            set { m_sFaxNumber = value; }
        }

        /// <summary>
        /// Get/Set the description
        /// </summary>
        public string InternalDescription
        {
            get { return m_sInternalDescription; }
            set
            {
                if (value != m_sInternalDescription)
                {
                    m_internalNewAuditEvents.AddLast(
                        new EDocumentIntermediateAuditEvent()
                        {
                            ActionD = DateTime.Now,
                            UserAction = E_PdfUserAction.EditInternalComments,
                            PreviousValue = m_sInternalDescription,
                            NewValue = value
                        });
                }
                m_sInternalDescription = value;
            }
        }

        public string PublicDescription
        {
            get { return m_sPublicDescription; }
            set
            {
                if (value != m_sPublicDescription)
                {
                    m_internalNewAuditEvents.AddLast(
                        new EDocumentIntermediateAuditEvent()
                        {
                            ActionD = DateTime.Now,
                            UserAction = E_PdfUserAction.EditDescription,
                            PreviousValue = m_sPublicDescription,
                            NewValue = value
                        });
                }
                m_sPublicDescription = value;
            }
        }


        /// <summary>
        /// Get/Set the comments
        /// </summary>
        public string Comments
        {
            get { return m_sComments; }
            set { m_sComments = value; }
        }

        /// <summary>
        /// Gets the version
        /// </summary>
        public int Version { get; private set; }

        /// <summary>
        /// Gets the last modified date
        /// </summary>
        public DateTime LastModifiedDate
        {
            get { return m_LastModified; }
        }

        /// <summary>
        /// Get/Set IsUploadedByPmlUserc. 
        /// </summary>
        public bool IsUploadedByPmlUser
        {
            get { return m_IsUploadedByPmlUser; }
            set { m_IsUploadedByPmlUser = value; }
        }

        /// <summary>
        /// Get/Set Hide from PML User
        /// </summary>
        public bool HideFromPMLUsers
        {
            get { return m_hideFromPMLUsers; }
            set
            {
                if (value != m_hideFromPMLUsers)
                {
                    m_internalNewAuditEvents.AddLast(
                        new EDocumentIntermediateAuditEvent()
                        {
                            ActionD = DateTime.Now,
                            UserAction = E_PdfUserAction.EditHideFromPMLUsers,
                            PreviousValue = m_hideFromPMLUsers ? "HIDE" : "SHOW",
                            NewValue = value ? "HIDE" : "SHOW"
                        });
                }
                m_hideFromPMLUsers = value;
            }
        }

        public E_EDocStatus DocStatusInitial { get; private set; }

        public E_EDocStatus DocStatus
        {
            get { return m_eStatus; }
            set
            {

                if (value != m_eStatus)
                {
                    if (value == E_EDocStatus.Blank)
                    {
                        m_internalNewAuditEvents.AddLast(new EDocumentIntermediateAuditEvent()
                        {
                            ActionD = DateTime.Now,
                            UserAction = E_PdfUserAction.DeleteStatus
                        });
                    }
                    else
                    {
                        m_internalNewAuditEvents.AddLast(new EDocumentIntermediateAuditEvent()
                        {
                            ActionD = DateTime.Now,
                            UserAction = E_PdfUserAction.EditStatus,
                            PreviousValue = StatusText(m_eStatus),
                            NewValue = StatusText(value)
                        });
                    }
                }
                m_eStatus = value;
            }
        }

        public string StatusDescription
        {
            get { return m_sStatusDescription; }
            set { m_sStatusDescription = value; }
        }

        public bool IsESigned { get; private set; }

        /// <summary>
        /// Generate a temporay token to view the PDF on Amazon Web Services.
        /// </summary>
        /// <param name="useOriginalVersion">Whether to render the original or current version of PDF.</param>
        /// <returns></returns>
        public string GenerateAmazonViewToken(bool useOriginalVersion)
        {
            return AmazonEDocHelper.GenerateViewPdfRequest(PrincipalFactory.CurrentPrincipal, this, useOriginalVersion);
        }

        private List<PdfPageItem> GetNonDestructivePdfPageInfoList()
        {
            List<PdfPageItem> pageInfoList = new List<PdfPageItem>();

            int pageIndex = 0;
            string awsToken = string.Empty;

            if (PrincipalFactory.CurrentPrincipal != null)
            {
                if (PrincipalFactory.CurrentPrincipal.BrokerDB.IsTestingAmazonEdocs)
                {
                    awsToken = this.GenerateAmazonViewToken(useOriginalVersion:false);
                }
            }

            foreach (var page in this.m_edocsMetaData.Pages)
            {
                PdfPageItem p = new PdfPageItem();
                p.DocId = Guid.Empty;// Not used. Data will be encrypted. page.ReferenceDocumentId;
                p.Page = pageIndex; // page.ReferenceDocumentPageNumber;
                p.Rotation = page.Rotation;
                p.PageHeight = page.PdfHeight;
                p.PageWidth = page.PdfWidth;
                p.PngKey = PNGEncryptedData.Encrypt(page.ReferenceDocumentId, PNGEncryptedData.NonDestructiveEdocsVersion, page.ReferenceDocumentPageNumber, this.DocumentId.ToString(), PrincipalFactory.CurrentPrincipal.LoginNm);
                p.Token = awsToken;

                pageIndex++;
                pageInfoList.Add(p);
            }
            return pageInfoList;
        }

        /// <summary>
        /// Retrieves the page information needed to load the PNGs.  
        /// TODO clean up.
        /// </summary>
        /// <returns></returns>
        public List<PdfPageItem> GetPdfPageInfoList()
        {
            if (this.m_edocsMetaData != null)
            {
                // 6/23/2015 dd - Pull from NonDestructive format.
                return GetNonDestructivePdfPageInfoList();
            }
            else
            {

                if (PdfMetaData == null || PdfMetaData.Key != m_fileDbKey_CurrentRevision)
                {
                    PdfMetaData = GenerateNewPageMetaData();

                    //dont bother saving if there are no cache images because somethign else will do it later.
                    //It also should not happen. 
                    if (ImageStatus == E_ImageStatus.HasCachedImages)
                    {
                        EDocument.SaveMetaData(this.BrokerId, DocumentId, m_fileDbKey_CurrentRevision, PdfMetaData);
                    }
                }

                List<PdfPageItem> pageInfo = new List<PdfPageItem>();
                for (int i = 0; i < PageCount; i++)
                {
                    PageMetaData page = PdfMetaData.Pages[i];
                    string key = PNGEncryptedData.Encrypt(DocumentId, Version, i + 1, page.Key, PrincipalFactory.CurrentPrincipal.LoginNm);

                    pageInfo.Add(new PdfPageItem()
                    {
                        DocId = DocumentId,
                        Page = i + 1,
                        Version = Version,
                        Rotation = 0,
                        PageHeight = page.Height,
                        PageWidth = page.Width,
                        PngKey = key
                    });
                }

                return pageInfo;
            }
        }

        private DocumentMetaData GenerateNewPageMetaData()
        {
            string tempFile;
            try
            {
                tempFile = FileDBTools.CreateCopy(E_FileDB.EDMS, m_fileDbKey_CurrentRevision.ToString());
            }
            catch (FileNotFoundException)
            {
                throw new GenericUserErrorMessageException(string.Format("Could not find file in file db. FileDbKey  = {0}; DocumentId = {1}; Version = {2}",
                    m_fileDbKey_CurrentRevision, DocumentId, Version));
            }

            DocumentMetaData docMetaData = DocumentMetaData.ReadFromPath(m_fileDbKey_CurrentRevision, tempFile);

            try
            {
                File.Delete(tempFile);
            }
            catch (FileNotFoundException) { }
            catch (UnauthorizedAccessException) { }

            return docMetaData;
        }

        /// <summary>
        /// This will remove all image annotations from a document's pdf file, as well as their references
        /// in EDOCS_PNG_ENTRY
        /// </summary>
        /// <param name="xMonths"></param>
        /// <returns>Number of deleted images</returns>
        public int RemoveCachedImages()
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (m_fileDbKey_CurrentRevision == Guid.Empty)
            {
                Tools.LogInfo("Skipping  Non Destructive EDoc " + this.DocumentId);
                return 0;
            }

            // Get the PDF
            string path;
            try
            {
                path = GetPDFTempFile_Current();
            }
            catch (FileNotFoundException)
            {
                Tools.LogError("EDocument::RemoveCachedImages - File not found for FileDB key " + m_fileDbKey_CurrentRevision.ToString());
                return 0;
            }

            string newPath = TempFileUtils.NewTempFilePath();
            HashSet<string> pageIds = new HashSet<string>();
            using (PDFPageIDManager idManager = new PDFPageIDManager(path))
            {
                // Gather images referenced in this PDF
                int pageCount = idManager.PageCount;
                for (int i = 1; i <= pageCount; i++)
                {
                    string pageId = idManager.GetPageId(i); // returns null when the page doesn't exist. PngKey.PngKey
                    if (!string.IsNullOrEmpty(pageId))
                    {
                        pageIds.Add(pageId);
                        pageIds.Add(pageId + ConstAppDavid.PdfPngThumbFileDbSuffix);
                    }

                    // Remove the PNG keys from the PDF annotations
                    idManager.DeletePageIdOn(i);
                }
                idManager.Save(newPath);
            }

            FileDBTools.WriteFile(E_FileDB.EDMS, m_fileDbKey_CurrentRevision.ToString(), newPath);
            // Set ImageStatus to NoCachedImagesNotInQueue
            EDocument.UpdateImageStatus(this.BrokerId, DocumentId, E_ImageStatus.NoCachedImagesNotInQueue);

            // Now, gather the images in EDOCS_PNG_ENTRY
            // EDOCS_PNG_ENTRY will contain references to images that are not referenced by the PDF
            // These images must also be deleted before their references are removed from the database
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocumentId", DocumentId)
                                        };
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "EDOCS_RetrievePNGEntriesByDocId", parameters))
            {
                while (reader.Read())
                {
                    string pngKey = (string)reader["PngKey"];
                    pageIds.Add(pngKey);
                    pageIds.Add(pngKey + ConstAppDavid.PdfPngThumbFileDbSuffix);
                }
            }

            // Delete all of the images we found in both the PDFPageIDManager and EDOCS_PNG_ENTRY
            foreach (string pageId in pageIds)
            {
                bool fileDeleted = false;
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        FileDBTools.Delete(E_FileDB.EDMS, pageId);
                        fileDeleted = true;
                        break;
                    }
                    catch (FileNotFoundException)
                    {
                        break;
                    }
                    catch (SocketException)
                    {
                        LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(TimeSpan.FromMilliseconds(500));
                    }
                }

                if (!fileDeleted)
                {
                    Tools.LogError("Could not delete file due to many SocketExceptions. gave up " + pageId);
                }

            }

            // Then remove their entries from EDOCS_PNG_ENTRY
            parameters = new SqlParameter[] {
                                                new SqlParameter("@DocId", DocumentId.ToString())
                                            };

            StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, "EDOCS_DeletePNGEntriesByDocId", 3, parameters);
            
            //clean up the temp file
            File.Delete(path);
            File.Delete(newPath);
            return pageIds.Count;
        }

        private void EnqueuePDFConversion(E_EDocOrigin origin)
        {
            EnqueuePDFConversion(origin, false);
        }

        public void EnqueuePDFConversion(E_EDocOrigin origin, bool isErrorRequeue)
        {
            if (this.BrokerId == Guid.Empty)
            {
                var msg = string.Format(
                    "EnqueuePDFConversion called with invalid broker id. BrokerId: {0}, Origin: {1}, isErrorRequeue: {2}",
                    this.BrokerId,
                    origin,
                    isErrorRequeue);
                Tools.LogError(msg, new Exception());
            }

            if (ConstStage.EnableEDocsMSMQ)
            {
                EDocConversionQueue.Enqueue(this, isErrorRequeue);
                return;
            }

            string data = string.Format("FileDbSiteCode={0};Scale={1};DocumentFileDBLocation={2};ThumbScale={3};ThumbSuffix={4};ErrorRequeue={5};BrokerId={6}", ConstStage.FileDBSiteCodeForEdms, ConstAppDavid.PdfPngScaling, m_fileDbKey_CurrentRevision, ConstAppDavid.PdfPngThumbRequeueScaling, ConstAppDavid.PdfPngThumbFileDbSuffix,isErrorRequeue, BrokerId);
            DBMessageQueue pngQueue = new DBMessageQueue(ConstMsg.PNGConversionPMLQueue);

            E_EDocumentSource[] userActionSources = { E_EDocumentSource.NotApplicable, E_EDocumentSource.UserUpload, E_EDocumentSource.UserCopy, E_EDocumentSource.SplitDoc };
            
            if (PageCount >= ConstStage.BatchQueuePageCountCutOff)
            {
                pngQueue = new DBMessageQueue(ConstMsg.PNGConversionFaxQueue);
            }
            else if ( HttpContext.Current != null  && userActionSources.Contains(m_creationSource))
            {
                //most likely  user action use regular queue
                pngQueue = new DBMessageQueue(ConstMsg.PNGConversionQueue);
            }

            //we dont want this to fail, specially if called withing save 
            try
            {
                pngQueue.Send(this.DocumentId.ToString(), data);
            }
            catch (InvalidOperationException e)
            {
                Tools.LogError(e);
                EDocument.UpdateImageStatus(this.BrokerId, DocumentId, E_ImageStatus.Error);
                
            }
            catch (SqlException e)
            {
                Tools.LogError(e);
                EDocument.UpdateImageStatus(this.BrokerId, DocumentId, E_ImageStatus.Error);
            }
        }

        /// <summary>
        /// Only works on error docs.
        /// </summary>
        public void RequeueErrorDoc()
        {
            if (ImageStatus != E_ImageStatus.Error)
            {
                return;
            }


            if (this.FileDbKey_CurrentRevision == Guid.Empty)
            {
                RequeueNonDestructiveEdocs();
                return;
            }
            else
            {

                UpdateImageStatus(this.BrokerId, DocumentId, E_ImageStatus.NoCachedImagesNotInQueue);
                EnqueuePDFConversion(E_EDocOrigin.LO, true);
                this.ImageStatus = E_ImageStatus.NoCachedImagesButInQueue;
            }
        }

        private void RequeueNonDestructiveEdocs()
        {
            // 8/3/2015 - When this happen, try to requeue the original document.
            Guid originalDocumentId = m_edocsMetaData.OriginalDocumentId;
            if (originalDocumentId != Guid.Empty)
            {
                string json = string.Empty;

                SqlParameter[] parameters = {
                                                    new SqlParameter("@BrokerId", this.BrokerId),
                                                    new SqlParameter("@DocumentId", originalDocumentId)
                                                };
                using (var reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "EDOCS_DOCUMENT_ORIGINAL_GetOriginalRequestJson", parameters))
                {
                    if (reader.Read())
                    {
                        json = (string)reader["OriginalRequestJson"];
                    }
                }

                NonDestructiveQueueItem queueItem = SerializationHelper.JsonNetDeserialize<NonDestructiveQueueItem>(json);

                LendersOffice.Drivers.Gateways.MessageQueueHelper.SendJSON(ConstStage.MSMQ_PdfRasterize, null, queueItem);

                UpdateImageStatus(this.BrokerId, DocumentId, E_ImageStatus.NoCachedImagesNotInQueue);
            }
        }

        public void RegenerateImages()
        {
            if (this.m_edocsMetaData != null)
            {
                // Do not regenerate for the non-destructive edocs.
                return;
            }

            EnqueuePDFConversion(E_EDocOrigin.LO);
            UpdateImageStatus(this.BrokerId, DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
        }

        private string AnnotationsXmlFileDbKey
        {
            get
            {
                return this.DocumentId.ToString("N") + "_EDOCS_ANNOTATIONS_XML";
            }
        }

        private string ESignTagsXmlFileDbKey
        {
            get
            {
                return this.DocumentId.ToString("N") + "_EDOCS_ESIGN_TAGS_XML";
            }
        }

        /// <summary>
        /// Get the created date
        /// </summary>
        public DateTime CreatedDate
        {
            get { return m_CreatedDate; }
        }

        /// <summary>
        /// Gets the ID of the user that created the document.
        /// </summary>
        public Guid? CreatedByUserId { get; private set; }

        /// <summary>
        /// Gets the total page count in the pdf
        /// </summary>
        public int PageCount { get; private set; }

        /// <summary>
        /// Get annotation object, lazy loaded.
        /// </summary>
        public EDocumentAnnotationContainer InternalAnnotations
        {
            get
            {
                if (m_annotations == null)
                {
                    m_annotations = new EDocumentAnnotationContainer();
                    if (m_hasInternalAnnotation)
                    {
                        byte[] data = FileDBTools.ReadData(E_FileDB.EDMS, AnnotationsXmlFileDbKey);
                        using (MemoryStream ms = new MemoryStream(data, false))
                        {
                            m_annotations.Load(ms);
                        }
                    }
                }
                return m_annotations;
            }
        }

        public EDocumentESignTagContainer ESignTags
        {
            get
            {
                if (this.m_eSignTags == null)
                {
                    this.m_eSignTags = new EDocumentESignTagContainer();
                    if (m_hasESignTags)
                    {
                        byte[] data = FileDBTools.ReadData(E_FileDB.EDMS, ESignTagsXmlFileDbKey);
                        using (MemoryStream ms = new MemoryStream(data, false))
                        {
                            m_eSignTags.Load(ms);
                        }
                    }
                }

                return this.m_eSignTags;
            }
        }

        /// <summary>
        /// Check to see if the annotation count > 0 
        /// </summary>
        public bool HasInternalAnnotations
        {
            get
            {
                //if we have not loaded the annotations return the DB field we did load.
                if (m_annotations == null)
                {
                    return m_hasInternalAnnotation;
                }

                //else just check the object count
                return InternalAnnotations.AnnotationCount > 0;
            }
        }

        public bool HasESignTags
        {
            get
            {
                if(this.m_eSignTags == null)
                {
                    return this.m_hasESignTags;
                }
                else
                {
                    return ESignTags.AnnotationList.Any((tags) => tags.ItemType == E_EDocumentAnnotationItemType.SignedDate ||
                                                                     tags.ItemType == E_EDocumentAnnotationItemType.Initial ||
                                                                     tags.ItemType == E_EDocumentAnnotationItemType.Signature);
                }
            }
        }

        public List<int> GetPagesWithAnnotationsAndTags()
        {
            return this.InternalAnnotations.WhichPagesHaveAnnotations().Union(this.ESignTags.WhichPagesHaveAnnotations()).ToList();
        }

        public E_ImageStatus ImageStatus { get; private set; }

        public DateTime? TimeToRequeueForImageGeneration
        {
            get { return m_timeToRequeueForImageGeneration; }
        }

        /// <summary>
        /// Set this when the document is being copied from another loan file.
        /// Used to provide descriptive audit event. Only used on creation.
        /// </summary>
        public string CopiedFromFileName
        {
            get { return m_copiedFromFileName; }
            set { m_copiedFromFileName = value; }
        }

        public Guid FileDbKey_CurrentRevision
        {
            get { return this.m_fileDbKey_CurrentRevision; }
        }

        #endregion


        internal EDocument(E_EDocumentSource source, Guid brokerId, Guid? documentId)
            : this(source, brokerId)
        {
            m_createdFromExistingDocumentId = documentId;
        }


        /// <summary>
        /// Instantiates a new EDocument object. 
        /// </summary>
        /// <param name="pdfData"></param>
        internal EDocument(E_EDocumentSource creationSource, Guid brokerId)
        {
            this.BrokerId = brokerId;
            m_creationSource = creationSource;
            this.IsNew = true;
            this.Version = 0;
            m_fileDbKey_CurrentRevision = Guid.Empty;
            m_fileDbKey_Original = Guid.Empty;
            m_sComments = string.Empty;
            m_sInternalDescription = string.Empty;
            m_sPublicDescription = string.Empty;
            m_sLId = new Guid?();
            m_aAppId = new Guid?();
            this.IsValid = true;
            m_isEditable = true;
            m_hasInternalAnnotation = false;
            m_hasESignTags = false;
            this.ImageStatus = E_ImageStatus.NoCachedImagesNotInQueue;
            m_timeToRequeueForImageGeneration = new DateTime?();
        }


        /// <summary>
        /// Used to instantiate documents using  a sql data reader. Only static functions in this class should be able to load existing documents. 
        /// Electronic Documents instatiated by the other constructor will always be seen as new. 
        /// </summary>
        /// <param name="reader"></param>
        private EDocument(IReadOnlyDictionary<string, object> reader, string app)
        {
            LoadFromReader(reader, app);
        }

        public void RequeueCheck()
        {
            // 02/19/2016 - For Non Destructive eDocs the FileDBKey_CurrentRevision will be Guid.Empty.
            if (this.FileDbKey_CurrentRevision == Guid.Empty)
            {
                // 8/13/2015 - Only handle where there is NoCacheImage but InQueue.
                // This could happen if the PdfRasterizeProcessor faile.
                if (ImageStatus == E_ImageStatus.NoCachedImagesButInQueue &&
                 (TimeToRequeueForImageGeneration.HasValue == false ||
                    DateTime.Now > TimeToRequeueForImageGeneration))
                {
                    RequeueNonDestructiveEdocs();
                }
                return;
            }

            if (ImageStatus == E_ImageStatus.NoCachedImagesNotInQueue)
            {
                EnqueuePDFConversion(E_EDocOrigin.LO);
                UpdateImageStatus(this.BrokerId, this.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
                this.ImageStatus = E_ImageStatus.NoCachedImagesButInQueue;
            }
            else if (ImageStatus == E_ImageStatus.NoCachedImagesButInQueue &&
                 (TimeToRequeueForImageGeneration.HasValue == false ||
                    DateTime.Now > TimeToRequeueForImageGeneration))
            {
                EnqueuePDFConversion(E_EDocOrigin.LO);
                UpdateImageStatus(this.BrokerId, this.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
                this.ImageStatus = E_ImageStatus.NoCachedImagesButInQueue;
            }

        }

        private static string GetOriginalMetadataJsonContentFileDBKey(Guid brokerId, Guid documentId)
        {
            if (documentId == Guid.Empty)
            {
                throw new ArgumentException("DocumentId cannot be empty.");
            }

            if (brokerId == Guid.Empty)
            {
                throw new ArgumentException("BrokerId cannot be empty.");
            }

            return documentId + "_" + brokerId + "_OriginalMetadataJsonContent";
        }

        private static string GetMetadataJsonContentFileDBKey(Guid brokerId, Guid documentId)
        {
            if (documentId == Guid.Empty)
            {
                throw new ArgumentException("DocumentId cannot be empty.");
            }

            if (brokerId == Guid.Empty)
            {
                throw new ArgumentException("BrokerId cannot be empty.");
            }

            return documentId + "_" + brokerId + "_MetadataJsonContent";
        }

        /// <summary>
        /// Get the file db key for MetadataJsonContent store in EDOCS_DOCUMENT_ORIGINAL table.
        /// </summary>
        /// <param name="brokerId"></param>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public static string GetOriginalDocumentMetadataJsonContentFileDBKey(Guid brokerId, Guid documentId)
        {
            if (documentId == Guid.Empty)
            {
                throw new ArgumentException("DocumentId cannot be empty.");
            }

            if (brokerId == Guid.Empty)
            {
                throw new ArgumentException("BrokerId cannot be empty.");
            }

            return documentId + "_" + brokerId + "_EDOCS_DOCUMENT_ORIGINAL_MetadataJsonContent";
        }

        /// <summary>
        /// Gets the FileDB key for MetadataJsonContent. The key is composite of document id and broker id.
        /// Exception will throw if DocumentId or BrokerId is empty.
        /// </summary>
        private string MetadataJsonContentFileDBKey
        {
            get
            {
                return GetMetadataJsonContentFileDBKey(this.BrokerId, this.DocumentId);
            }
        }

        private string OriginalMetadataJsonContentFileDBKey
        {
            get
            {
                return GetOriginalMetadataJsonContentFileDBKey(this.BrokerId, this.DocumentId);
            }
        }

        private void LoadFromReader(IReadOnlyDictionary<string, object> reader, string app)
        {
            this.IsNew = false;
            this.DocumentId = (Guid)reader["DocumentId"];
            m_sLId = reader["sLId"] == DBNull.Value ? new Guid?() : (Guid)reader["sLId"];
            m_aAppId = reader["aAppId"] == DBNull.Value ? new Guid?() : (Guid)reader["aAppId"]; // db - adding for consumer portal project
            this.BrokerId = (Guid) reader["BrokerId"];
            m_sFaxNumber = (string)reader["FaxNumber"];
            m_fileDbKey_Original = (Guid)reader["FileDBKey_OriginalDocument"];
            m_fileDbKey_CurrentRevision = (Guid)reader["fileDbKey_CurrentRevision"];
            m_sInternalDescription = (string)reader["InternalDescription"];
            m_sPublicDescription = (string)reader["PublicDescription"];
            m_sComments = (string)reader["Comments"];
            m_DocTypeId = (int)reader["DocTypeId"];
            this.PageCount = (int)reader["NumPages"];
            m_IsUploadedByPmlUser = (bool)reader["IsUploadedByPmlUser"];
            this.Version = (int)reader["Version"];
            m_LastModified = (DateTime)reader["LastModifiedDate"];
            m_CreatedDate = (DateTime)reader["CreatedDate"];
            this.DocTypeName = (string)reader["DocTypeName"];
            this.IsValid = (bool)reader["IsValid"];
            m_isEditable = (bool)reader["IsEditable"]; // db - adding for consumer portal project
            m_hasInternalAnnotation = (bool)reader["HasInternalAnnotation"];  //11/18/10 - Will decide if we should load from PDF for spec 51942
            
            this.m_hasESignTags = reader["HasESignTags"] == DBNull.Value ? false : (bool)reader["HasESignTags"];
            this.ImageStatus = (E_ImageStatus)Convert.ToInt32(reader["ImageStatus"]);
            m_timeToRequeueForImageGeneration = reader["TimeToRequeueForImageGeneration"] == DBNull.Value ? new DateTime?() : (DateTime)reader["TimeToRequeueForImageGeneration"];

            this.PdfHasUnsupportedFeatures = (bool)reader["PdfHasUnsupportedFeatures"];
            m_hideFromPMLUsers = (bool)reader["HideFromPmlUsers"];

            if (reader.ContainsKey("GenericFileType") && reader["GenericFileType"] != DBNull.Value)
            {
                this.GenericFileType = (Contents.E_FileType)reader["GenericFileType"];
            }
            else
            {
                this.GenericFileType = null;
            }

            if (reader.ContainsKey("MetadataJsonContent"))
            {
                object metadataJsonContent = reader["MetadataJsonContent"];

                if (metadataJsonContent != DBNull.Value)
                {
                    this.m_metadataJsonContentFromDatabase = metadataJsonContent.ToString();
                }
            }

            if (reader.ContainsKey("OriginalMetadataJsonContent"))
            {
                object metadataJsonContent = reader["OriginalMetadataJsonContent"];

                if (metadataJsonContent != DBNull.Value)
                {
                    this.m_originalMetadataJsonContentFromDatabase = metadataJsonContent.ToString();
                }
            }

            if (reader.ContainsKey("MetadataProtobufContent"))
            {
                object value = reader["MetadataProtobufContent"];
                if (value != DBNull.Value)
                {
                    this.x_MetadataProtobufContent = (byte[])value;
                }
            }

            if (reader.ContainsKey("OriginalMetadataProtobufContent"))
            {
                object value = reader["OriginalMetadataProtobufContent"];
                if (value != DBNull.Value)
                {
                    this.x_OriginalMetadataProtobufContent = (byte[])value;
                }
            }

            m_sAppName = app;

            FolderId = (int)reader["FolderId"];     //is nullable in DB not sure if this is possible though
            RequiresDocTypeChange = !(bool)reader["DocTypeIsValid"];

            m_eStatus = (E_EDocStatus)Convert.ToInt32(reader["Status"]);
            DocStatusInitial = m_eStatus;
            m_sStatusDescription = (string)reader["StatusDescription"];

            if (reader["PdfPageInfo"] != DBNull.Value)
            {
                string pageInfo = reader["PdfPageInfo"].ToString();
                PdfMetaData = DocumentMetaData.Parse(pageInfo);
            }

            if (reader.ContainsKey("FolderName") && reader.ContainsKey("FolderRoles"))
            {
                m_Folder = new EDocumentFolder(this.BrokerId, reader);
            }

            this.IsESigned = (bool)reader["IsESigned"];

            if (reader.ContainsKey("CreatedByUserId") && reader["CreatedByUserId"] != DBNull.Value)
            {
                this.CreatedByUserId = (Guid)reader["CreatedByUserId"];
            }
        }

        public void AddAuditEntry(E_PdfUserAction action, DateTime dt, Guid? userId, string customDescription = null)
        {
            m_externalNewAuditEvents.AddLast(new EDocumentIntermediateAuditEvent()
            {
                ActionD = dt,
                UserAction = action,
                UserId = userId,
                CustomDescription = customDescription
            });
        }

        public void AddAuditEntries(EDocumentIntermediateAuditTracker auditTracker)
        {
            if (auditTracker == null)
            {
                return;
            }
            if (auditTracker.DocumentId != this.DocumentId)
            {
                Tools.LogError("The user actions doc id does not match this document.");
                return;
            }
            foreach (EDocumentIntermediateAuditEvent auditEvent in auditTracker.Items)
            {
                m_externalNewAuditEvents.AddLast(auditEvent);
            }

        }

        /// <summary>
        /// Saves the document into the database. It first stores the pdf data into FILEDB if it needs to. Then it tries saving to the db
        /// Do not use this instance of the document once you save. New Creation may be fine, but not update.
        /// </summary>
        internal void Save(Guid? userId)
        {
            using (PerformanceStopwatch.Start("EDocument.Save()"))
            {
                if (this.IsNew)
                {
                    Create(userId);
                    EDocumentNotifier.AddNewEDocumentNotificationToQueue(this);
                }
                else
                {
                    Update(userId);
                }
            }
        }

        private void Create(Guid? userId)
        {
            if (m_sLId.HasValue && m_aAppId.HasValue == false)
            {
                throw new CBaseException(ErrorMessages.Generic, "Bug - Trying to create a document with a LoanId but missing an AppId.");
            }

            if (m_sLId.HasValue == false && m_aAppId.HasValue)
            {
                LoadLoanIdFromAppId();
            }

            this.DocumentId = Guid.NewGuid(); // 11/11/2010 dd - Set this here because PngZipDataFileDbKey require valid documentid

            if (this.m_isNonDestructiveEdocs)
            {
                CreateVersion2(userId);
            }
            else
            {
                CreateLegacy(userId);
            }

            this.Version = -1;
            this.IsNew = false;
            m_CreatedDate = DateTime.Today;
            m_LastModified = DateTime.Today;
            this.CreatedByUserId = userId;

            //update the dates because when the internal audit events are changing we do not know who is doing them.
            DateTime now = DateTime.Now;
            foreach (EDocumentIntermediateAuditEvent ev in m_internalNewAuditEvents)
            {
                ev.ActionD = now;
                ev.UserId = userId;
            }

            StoreIntermediateEvents(m_internalNewAuditEvents);
            StoreIntermediateEvents(m_externalNewAuditEvents);
        }

        private void CreateLegacy(Guid? userId)
        {
            if (string.IsNullOrEmpty(m_sNewPdfPath))
            {
                throw new CBaseException(ErrorMessages.Generic, "BUG-Trying to save a document w/o a pdf.");
            }

            this.ImageStatus = E_ImageStatus.NoCachedImagesButInQueue;
            m_timeToRequeueForImageGeneration = DateTime.Now.AddMinutes(ConstStage.WaitBeforeRequeuingForImageGeneration);

            Guid originalRevisionFileDBKey = Guid.NewGuid();
            Guid currentRevisionFileDBKey = Guid.NewGuid();

            int saveState = 0;
            try
            {
                if (string.IsNullOrEmpty(m_sOriginalPdfPath))
                {
                    m_sOriginalPdfPath = m_sNewPdfPath;
                }
                WritePathToFileDB(originalRevisionFileDBKey.ToString(), m_sOriginalPdfPath);
                saveState++;
                WritePathToFileDB(currentRevisionFileDBKey.ToString(), m_sNewPdfPath);
                saveState++;
                if (m_annotations != null)
                {
                    FileDBTools.WriteData(E_FileDB.EDMS, this.AnnotationsXmlFileDbKey, s => m_annotations.Save(s));
                }
                saveState++;
                if (this.m_eSignTags != null)
                {
                    FileDBTools.WriteData(E_FileDB.EDMS, this.ESignTagsXmlFileDbKey, s => m_eSignTags.Save(s));
                }
                saveState++;

                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, CREATE_SP, 3, GenerateSaveParameters(originalRevisionFileDBKey, currentRevisionFileDBKey, this.PageCount, this.DocumentId, userId));
            }
            catch
            {
                switch (saveState)
                {
                    case 4:
                        if (m_eSignTags != null)
                        {
                            DeleteFileDBEntry(this.ESignTagsXmlFileDbKey);
                        }
                        goto case 3;
                    case 3:
                        if (m_annotations != null)
                        {
                            DeleteFileDBEntry(AnnotationsXmlFileDbKey);
                        }
                        goto case 2;
                    case 2:
                        DeleteFileDBEntry(currentRevisionFileDBKey.ToString());
                        goto case 1;
                    case 1:
                        DeleteFileDBEntry(originalRevisionFileDBKey.ToString());
                        goto default;
                    default:
                        throw;
                }
            }

            m_fileDbKey_CurrentRevision = currentRevisionFileDBKey;
            m_fileDbKey_Original = originalRevisionFileDBKey;

            if (m_requiresRegeneration)
            {
                EnqueuePDFConversion(EDocOrigin);
                UpdateImageStatus(this.BrokerId, this.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
            }
            else
            {
                UpdateImageStatus(this.BrokerId, this.DocumentId, E_ImageStatus.HasCachedImages);
            }
        }

        private void Update(Guid? userId)
        {
            if (m_sLId.HasValue && m_aAppId.HasValue == false)
            {
                throw new CBaseException(ErrorMessages.Generic, "Bug - Trying to create a document with a LoanId but missing an AppId.");
            }

            if (m_sLId.HasValue == false && m_aAppId.HasValue)
            {
                LoadLoanIdFromAppId();
            }

            Guid localBrokerId = this.BrokerId;

            Guid fileDBKey = m_fileDbKey_CurrentRevision;
            bool removeAnnotationData = false; //will be set when we write annotation data for the firs time and the SP save crashes. 
            bool removeESignTagData = false;
            string annotationTempFile = null;
            string eSignTempFile = null;
            bool updatePdf = false;
            try
            {
                if (false == string.IsNullOrEmpty(m_sNewPdfPath))
                {
                    if (false == File.Exists(m_sNewPdfPath))
                    {
                        throw CBaseException.GenericException("PDF File no longer exist. Did you wait too long to call save? " + DocumentId);
                    }
                    updatePdf = true;
                    fileDBKey = Guid.NewGuid();
                    FileDBTools.WriteFile(E_FileDB.EdmsPdf, fileDBKey.ToString(), m_sNewPdfPath);

                    if (m_newMetaData != null)
                    {
                        m_newMetaData.Key = fileDBKey;
                    }

                }
                if (m_annotations != null)
                {
                    try
                    {
                        annotationTempFile = FileDBTools.CreateCopy(E_FileDB.EDMS, AnnotationsXmlFileDbKey);
                    }
                    catch (FileNotFoundException)
                    {
                        removeAnnotationData = true;
                    }
                    FileDBTools.WriteData(E_FileDB.EDMS, AnnotationsXmlFileDbKey, s => m_annotations.Save(s));
                }
                if (this.m_eSignTags != null)
                {
                    try
                    {
                        eSignTempFile = FileDBTools.CreateCopy(E_FileDB.EDMS, ESignTagsXmlFileDbKey);
                    }
                    catch (FileNotFoundException)
                    {
                        removeESignTagData = true;
                    }
                    FileDBTools.WriteData(E_FileDB.EDMS, ESignTagsXmlFileDbKey, s => m_eSignTags.Save(s));
                }

                Guid currentRevisionKey = Guid.Empty;

                if (m_fileDbKey_CurrentRevision != Guid.Empty)
                {
                    // 2/28/2017 - dd- All lenders are using a non destructive edocs mode. Therefore we should not update filedbkey_currentrevision.
                    //        However just in case some rare scenario where user load up a legacy edocs file then we need to set value for filedb current revision.
                    //        Should remove this block of code in few years where we no longer have non-empty guid in FileDBKey_CurrentRevision columns.
                    currentRevisionKey = fileDBKey;
                }
                // This will also delete document-condition associations through a trigger, if the document has been set to rejected
                StoredProcedureHelper.ExecuteNonQuery(localBrokerId, UPDATE_SP, 3, GenerateSaveParameters(m_fileDbKey_Original, currentRevisionKey, this.PageCount, this.DocumentId, userId));
                
                if (updatePdf)
                {
                    if (m_fileDbKey_CurrentRevision != Guid.Empty)
                    {
                        DeleteFileDBEntry(m_fileDbKey_CurrentRevision.ToString());
                    }
                }

            }
            catch (SqlException e)
            {
                RollbackUpdate(updatePdf, fileDBKey, removeAnnotationData, annotationTempFile, removeESignTagData, eSignTempFile);
                if (e.Message == "Versions do not match.")
                {
                    throw new InvalidEDocumentVersion(e);
                }
                throw;

            }
            catch (Exception)
            {
                RollbackUpdate(updatePdf, fileDBKey, removeAnnotationData, annotationTempFile, removeESignTagData, eSignTempFile);
                throw;
            }
            finally
            {
                if (false == string.IsNullOrEmpty(annotationTempFile) && File.Exists(annotationTempFile))
                {
                    File.Delete(annotationTempFile);
                }

                if (!string.IsNullOrEmpty(eSignTempFile) && File.Exists(eSignTempFile))
                {
                    File.Delete(eSignTempFile);
                }
            }

            if (m_requiresRegeneration)
            {
                m_fileDbKey_CurrentRevision = fileDBKey;
                RegenerateImages();
            }

            //update the dates because when the internal audit events are changing we do not know who is doing them.
            DateTime now = DateTime.Now;
            foreach (EDocumentIntermediateAuditEvent ev in m_internalNewAuditEvents)
            {
                ev.ActionD = now;
                ev.UserId = userId;
            }

            if (m_annotations != null && m_annotations.NotesHaveChanged)
            {
                AddAuditEntry(E_PdfUserAction.ModifiedAnnotations, DateTime.Now, userId); 
            }

            if (this.m_eSignTags != null && this.m_eSignTags.NotesHaveChanged)
            {
                AddAuditEntry(E_PdfUserAction.ModifiedESignTags, DateTime.Now, userId);
            }

            StoreIntermediateEvents(m_internalNewAuditEvents);
            StoreIntermediateEvents(m_externalNewAuditEvents);
        }

        private void StoreIntermediateEvents(IEnumerable<EDocumentIntermediateAuditEvent> auditEvents)
        {
            foreach (EDocumentIntermediateAuditEvent auditEvent in auditEvents)
            {
                try
                {
                    EDocumentAuditEvent.InsertEntry(this.BrokerId, this.DocumentId, auditEvent);
                }
                catch (SqlException e)
                {
                    Tools.LogError("Could not save audit :(", e);
                }
            }
        }

        private void RollbackUpdate(bool updatePdf, Guid newRevisionFileDbKey, bool removeAnnotations, string annotationBackupPath, bool removeESignTags, string eSignTagsBackupPath)
        {
            if (updatePdf)
            {
                FileDBTools.Delete(E_FileDB.EDMS, newRevisionFileDbKey.ToString());
            }

            if (removeAnnotations)
            {
                DeleteFileDBEntry(AnnotationsXmlFileDbKey);
            }
            else if (false == string.IsNullOrEmpty(annotationBackupPath))
            {
                WritePathToFileDB(AnnotationsXmlFileDbKey, annotationBackupPath);
            }

            // The remove booleans are only true if the file didn't exist in the first place.
            // Thus rolling back in that scenerio means removing the entry.
            // If it's false and there is an old file saved temporarily, then it needs to be put back.
            if (removeESignTags)
            {
                DeleteFileDBEntry(ESignTagsXmlFileDbKey);
            }
            else if (!string.IsNullOrEmpty(eSignTagsBackupPath))
            {
                WritePathToFileDB(ESignTagsXmlFileDbKey, eSignTagsBackupPath);
            }
        }

        private IEnumerable<SqlParameter> GenerateSaveParameters(Guid originalRevision, Guid currentRevision, int numberOfPages, Guid documentId, Guid? userId)
        {
            List<SqlParameter> parameters = new List<SqlParameter>(15);


            parameters.Add(new SqlParameter("@DocumentId", documentId));


            if (m_sLId.HasValue)
            {
                parameters.Add(new SqlParameter("@sLId", m_sLId.Value));
            }

            if (m_aAppId.HasValue)
            {
                parameters.Add(new SqlParameter("@aAppId", m_aAppId.Value));
            }

            if (userId.HasValue && userId.Value != Guid.Empty)
            {
                parameters.Add(new SqlParameter("@ModifiedByUserId", userId.Value));

                if (this.IsNew)
                {
                    parameters.Add(new SqlParameter("@CreatedByUserId", userId.Value));
                }
            }

            if (this.IsNew)
            {
                parameters.Add(new SqlParameter("@CreatedFromExistingDocId", m_createdFromExistingDocumentId));
                parameters.Add(new SqlParameter("@OriginalRevisionKey", originalRevision));
                parameters.Add(new SqlParameter("@CurrentRevisionKey", currentRevision));
                parameters.Add(new SqlParameter("@Reason", GetCreationReasonFromSource(m_creationSource)));
            }
            else
            {
                parameters.Add(new SqlParameter("@Version", this.Version));
                parameters.Add(new SqlParameter("@FileDBKey", currentRevision));

                if (m_newMetaData != null)
                {
                    parameters.Add(new SqlParameter("@PdfPageInfo", m_newMetaData.Serialize()));
                }

            }

            parameters.Add(new SqlParameter("@CommentS", m_sComments));

            parameters.Add(new SqlParameter("@BrokerId", this.BrokerId));
            parameters.Add(new SqlParameter("@DocTypeId", m_DocTypeId));
            parameters.Add(new SqlParameter("@FaxNumber", m_sFaxNumber ?? ""));
            parameters.Add(new SqlParameter("@PublicDescription", m_sPublicDescription));
            parameters.Add(new SqlParameter("@InternalDescription", m_sInternalDescription));

            parameters.Add(new SqlParameter("@NumPages", numberOfPages));
            parameters.Add(new SqlParameter("@IsUploadedByPmlUser", IsUploadedByPmlUser));
            parameters.Add(new SqlParameter("@IsEditable", m_isEditable));
            parameters.Add(new SqlParameter("@HasInternalAnnotation", HasInternalAnnotations));

            parameters.Add(new SqlParameter("@HasESignTags", this.HasESignTags));

            parameters.Add(new SqlParameter("@PdfhasUnsupportedFeatures", this.PdfHasUnsupportedFeatures));

            parameters.Add(new SqlParameter("@Status", m_eStatus));
            parameters.Add(new SqlParameter("@StatusDescription", m_sStatusDescription));

            parameters.Add(new SqlParameter("@HideFromPMLUsers", m_hideFromPMLUsers));

            parameters.Add(new SqlParameter("@IsESigned", this.IsESigned));


            SqlParameter metadataProtobufContentParameter = new SqlParameter("@MetadataProtobufContent", System.Data.SqlDbType.Image);

            if (this.m_edocsMetaData != null)
            {
                metadataProtobufContentParameter.Value = SerializationHelper.ProtobufSerialize(this.m_edocsMetaData);
                parameters.Add(metadataProtobufContentParameter);
            }

            SqlParameter originalMetadataProtobufContentParameter = new SqlParameter("@OriginalMetadataProtobufContent", System.Data.SqlDbType.Image);
            if (this.m_originalEdocsMetaData != null)
            {
                originalMetadataProtobufContentParameter.Value = SerializationHelper.ProtobufSerialize(this.m_originalEdocsMetaData);
                parameters.Add(originalMetadataProtobufContentParameter);
            }

            return parameters;

        }

        private string GetCreationReasonFromSource(E_EDocumentSource source)
        {
            switch (source)
            {
                case E_EDocumentSource.FromIntegration:
                    return "Created by Integration With " + PrincipalFactory.CurrentPrincipal.FirstName + " (" + PrincipalFactory.CurrentPrincipal.LoginNm + ")";
                case E_EDocumentSource.UserUpload:
                    return "Uploaded by User";
                case E_EDocumentSource.AcceptedFromConsumer:
                    return "Received from Consumer";
                case E_EDocumentSource.Fax:
                    return "Received Via Fax";
                case E_EDocumentSource.SplitDoc:
                    return "Split from Document";
                case E_EDocumentSource.GeneratedDocs:
                    return "Generated Document";
                case E_EDocumentSource.CBCDrive:
                    return "Received from CBC DRIVE";
                case E_EDocumentSource.FloodService:
                    return "Uploaded via Flood service";
                case E_EDocumentSource.ESignPOST:
                    return "Received via vendor integration";
                case E_EDocumentSource.UserCopy:
                    return String.Format("Copied from loan {0}", m_copiedFromFileName);
                case E_EDocumentSource.DeletedPages:
                    return String.Format("Created from pages removed from {0}", m_copiedFromFileName);
                case E_EDocumentSource.EditableCopyOfSignedDoc:
                    return "Created Editable Copy of Signed Document";
                default:
                    throw new UnhandledEnumException(source);
            }
        }

        /// <summary>
        /// Sets the loan id by looking up the app id from the database.
        /// </summary>
        private void LoadLoanIdFromAppId()
        {
            SqlParameter[] parameters = {
                                             new SqlParameter("@aAppId", m_aAppId.Value)
                                        };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "EDOCS_GetLoanIdByAppId", parameters))
            {
                if (reader.Read())
                {
                    m_sLId = (Guid)reader["sLId"];
                }
                else
                {
                    throw new CBaseException(ErrorMessages.Generic, "Bug - Trying to create a document with an invalid AppId: " + m_aAppId.Value);
                }
            }
        }


        /// <summary>
        /// Set this when changing the PDF content. 
        /// </summary>
        /// <param name="path">The path of the pdf file.</param>
        /// <param name="requiresRegeneration">Whether the pdf file is missing any page ids</param>
        /// <param name="pageCount">The number of pages in the PDF file. IF you pass in 0 or less it will be looked up from the file.</param>
        /// <param name="hasOwnerPassword">Whether the new pdf has owner password</param>
        public void UpdatePDFContentOnSave(string path, bool requiresRegeneration, int pageCount)
        {
            if (false == File.Exists(path))
            {
                throw CBaseException.GenericException("The PDF file does not exist.");
            }
            if (pageCount < 1)
            {
                int n = 0;
                EDocumentViewer.GetPdfInfo(path, out n);
                this.PageCount = n;
            }
            else
            {
                this.PageCount = pageCount;
            }
            m_sNewPdfPath = path;
            m_requiresRegeneration = requiresRegeneration;

            //this means the pdf was modified but images are not needed 
            //in this case regenerate the meta data right now
            if (!m_requiresRegeneration)
            {
                m_newMetaData = DocumentMetaData.ReadFromPath(Guid.Empty, path);
            }
        }

        public void UpdateNonDestructiveContentOnSave(List<PdfPageItem> pages, IEnumerable<EDocumentAnnotationItem> annotationsToApply, IEnumerable<PdfField> signatureFields)
        {
            this.PageCount = pages.Count;
            this.m_isNonDestructiveEdocs = true;

            var oldEdocsMetaData = this.m_edocsMetaData;

            this.m_edocsMetaData = new EdocsMetadata();

            HashSet<string> uniquePageSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);

            foreach (var p in pages)
            {
                PNGEncryptedData encryptedData = new PNGEncryptedData(p.PngKey);

                EdocsPageMetadata metaPage = new EdocsPageMetadata();
                // 6/24/2015 dd - Reference document id and page number are inside encrypted portion.
                metaPage.ReferenceDocumentId = encryptedData.DocumentId;
                metaPage.ReferenceDocumentPageNumber = encryptedData.PageNumber;

                metaPage.PdfWidth = p.PageWidth;
                metaPage.PdfHeight = p.PageHeight;
                metaPage.Rotation = p.Rotation;

                m_edocsMetaData.AddPage(metaPage);

                uniquePageSet.Add(metaPage.ReferenceDocumentId + "::" + metaPage.ReferenceDocumentPageNumber);
            }

            if ((annotationsToApply != null && annotationsToApply.Count() > 0) || (signatureFields != null && signatureFields.Count() > 0))
            {
                // Extract out only pages contains pasted annotation / signature and create new pdf.
                EdocsMetadata annotationEdocsPages = new EdocsMetadata();
                Dictionary<int, int> oldNewPagesMapping = new Dictionary<int, int>();

                Guid tempDocumentId = Guid.NewGuid();
                if (annotationsToApply != null)
                {
                    foreach (var annotation in annotationsToApply)
                    {
                        // Page number are 1-index
                        int oldPageNumber = annotation.PageNumber;
                        int newPageNumber = -1;

                        if (oldNewPagesMapping.TryGetValue(oldPageNumber, out newPageNumber) == false)
                        {
                            newPageNumber = oldNewPagesMapping.Count + 1;
                            oldNewPagesMapping.Add(oldPageNumber, newPageNumber);

                            var p = m_edocsMetaData.Pages.ElementAt(oldPageNumber - 1);

                            annotationEdocsPages.AddPage(p);
                        }

                    }
                }

                if (null != signatureFields)
                {
                    if (signatureFields.Count() > 0)
                    {
                        foreach (var f in signatureFields)
                        {
                            int oldPageNumber = f.PageNumber;
                            int newPageNumber = -1;
                            if (oldNewPagesMapping.TryGetValue(oldPageNumber, out newPageNumber) == false)
                            {
                                newPageNumber = oldNewPagesMapping.Count + 1;
                                oldNewPagesMapping.Add(oldPageNumber, newPageNumber);

                                annotationEdocsPages.AddPage(m_edocsMetaData.Pages.ElementAt(oldPageNumber - 1));
                            }
                        }
                    }
                }

                string pdf = this.GetPdfPathForNonDestructiveUpdate(annotationEdocsPages);

                if (null != signatureFields)
                {
                    if (signatureFields.Count() > 0)
                    {
                        AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

                        EmployeeSignatureInfo signInfo = EmployeeSignatureInfo.Load(principal.BrokerId, principal.EmployeeId, principal.BrokerDB.EnableLqbNonCompliantDocumentSignatureStamp);
                        if (signInfo.HasUploadedSignature == false)
                        {
                            throw CBaseException.GenericException("Signature does not exist. User cannot sign.");
                        }
                        string sigTempPath = TempFileUtils.NewTempFilePath();
                        using (FileStream fs1 = File.Create(sigTempPath))
                        {
                            PdfReader reader = EDocumentViewer.CreatePdfReader(pdf);
                            PdfStamper stamper = new PdfStamper(reader, fs1);
                            byte[] dateImg = File.ReadAllBytes(EDocumentViewer.GeneratePngForToday());
                            byte[] sigImg = FileDBTools.ReadData(E_FileDB.Normal, signInfo.SignatureKey.ToString());
                            foreach (var field in signatureFields)
                            {
                                byte[] imgData = field.Type == PdfFieldType.Signature ? sigImg : dateImg;
                                var pdfcontentbyte = stamper.GetOverContent(oldNewPagesMapping[field.PageNumber]);
                                var img = iTextSharp.text.Image.GetInstance(imgData);
                                img.ScaleToFit(field.Rectangle.Width, field.Rectangle.Height);
                                img.SetAbsolutePosition(field.Rectangle.Left, field.Rectangle.Bottom);
                                pdfcontentbyte.AddImage(img);
                            }
                            stamper.Close();
                            pdf = sigTempPath;
                        }

                    }
                }

                var pageAnnotations = annotationsToApply.ToLookup(o => oldNewPagesMapping[o.PageNumber]);

                string annotatedPdf = GeneratePDFDataWithPastedAnnotation(pdf, pageAnnotations);

                string fileDbKey = tempDocumentId.ToString();
                FileDBTools.WriteFile(E_FileDB.EdmsPdf, fileDbKey, annotatedPdf);

                EnqueueNonDestructivePdfRasterizeRequest(this.BrokerId, this.DocumentId, fileDbKey, E_NonDestructiveSourceT.PasteAnnotation, POST_ACTION_UPDATE_STATUS_ONLY);
                UpdateImageStatus(this.BrokerId, this.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);

                // Update the references of the metadata to point to new page.
                foreach (var kvp in oldNewPagesMapping)
                {
                    var page = this.m_edocsMetaData.Pages.ElementAt(kvp.Key - 1);

                    // New Document Id and page number.
                    page.ReferenceDocumentId = tempDocumentId;
                    page.ReferenceDocumentPageNumber = kvp.Value - 1; // 0-based index.

                    if (page.Rotation % 180 == 90)
                    {
                        // We need to switch the width and height.
                        int temp = page.PdfHeight;
                        page.PdfHeight = page.PdfWidth;
                        page.PdfWidth = temp;

                        temp = page.PngFullHeight;
                        page.PngFullHeight = page.PngFullWidth;
                        page.PngFullWidth = temp;

                        temp = page.PngThumbHeight;
                        page.PngThumbHeight = page.PngThumbWidth;
                        page.PngThumbWidth = temp;
                    }

                    page.Rotation = 0; // Reset Rotation.
                }
            }

            // 7/29/2015 dd - Get a list of delete pages and create a new edocs for auditing purpose.
            //                Base from case 198591.
            var deleteEdocsMetaData = new EdocsMetadata();
            bool hasDeletePage = false;

            if (oldEdocsMetaData != null)
            {
                foreach (var p in oldEdocsMetaData.Pages)
                {
                    string uniqueId = p.ReferenceDocumentId + "::" + p.ReferenceDocumentPageNumber;
                    if (uniquePageSet.Contains(uniqueId) == false)
                    {
                        hasDeletePage = true;
                        deleteEdocsMetaData.AddPage(p);
                    }
                }
            }

            if (hasDeletePage)
            {
                EDocumentRepository systemRepo = EDocumentRepository.GetSystemRepository(this.BrokerId);
                AbstractUserPrincipal principal = PrincipalFactory.CurrentPrincipal;

                var deletedPageEdoc = systemRepo.CreateDocument(E_EDocumentSource.UserCopy);
                deletedPageEdoc.CopiedFromFileName = this.FolderAndDocTypeName + ". Pages deleted by " + principal.FirstName + " " + principal.LastName;
                deletedPageEdoc.m_isNonDestructiveEdocs = true;
                deletedPageEdoc.AppId = this.AppId;
                deletedPageEdoc.PageCount = deleteEdocsMetaData.Pages.Count();
                deletedPageEdoc.Comments = this.Comments;
                deletedPageEdoc.DocumentTypeId = this.DocumentTypeId;
                deletedPageEdoc.EDocOrigin = E_EDocOrigin.LO;
                deletedPageEdoc.InternalDescription = this.InternalDescription;
                deletedPageEdoc.PublicDescription = this.PublicDescription + " (deleted pages)";
                deletedPageEdoc.LoanId = this.LoanId;
                deletedPageEdoc.m_edocsMetaData = deleteEdocsMetaData;
                deletedPageEdoc.m_originalEdocsMetaData = deleteEdocsMetaData;

                systemRepo.Save(deletedPageEdoc);
                systemRepo.DeleteDocument(deletedPageEdoc.DocumentId, "All Pages Deleted");
            }

            if (this.m_originalEdocsMetaData == null)
            {
                // 6/29/2015 dd - If original meta data is missing then set the current meta as original.
                // One possible scenario where this happen is document create from splitting existing document.
                this.m_originalEdocsMetaData = this.m_edocsMetaData;
            }
        }

        /// <summary>
        /// Gets the path to the PDF for the non-destructive PDF update.
        /// </summary>
        /// <param name="annotationEdocsPages">
        /// The metadata for the annotations.
        /// </param>
        /// <returns>
        /// The PDF path.
        /// </returns>
        private string GetPdfPathForNonDestructiveUpdate(EdocsMetadata annotationEdocsPages)
        {
            if (this.generateNonDestructiveEdocsUsingAmazon)
            {
                var broker = PrincipalFactory.CurrentPrincipal?.BrokerDB ?? BrokerDB.RetrieveById(this.BrokerId);
                if (broker.IsTestingAmazonEdocs)
                {
                    try
                    {
                        var token = AmazonEDocHelper.GenerateViewPdfRequest(PrincipalFactory.CurrentPrincipal, this, annotationEdocsPages);
                        return AmazonEDocHelper.RetrievePdf(token);
                    }
                    catch (System.Net.WebException exc)
                    {
                        // Fall back to non-Amazon behavior.
                        Tools.LogError(exc);
                    }
                }
            }

            return GenerateNonDestructiveEdocs(annotationEdocsPages);
        }

        /// <summary>
        /// Using SetPDFContent instead of UpdatePDFContentOnSave will by pass the document scan for PNG ids.
        /// This is beneficial when the PDF was generated via code and the IDs were wiped on creation.
        /// This method does not run the virus scanner. Only the splitter can use this method.
        /// </summary>
        /// <param name="path">The PDF file path.</param>
        /// <param name="pageCount">The number of pages in the PDF.</param>
        internal void SetPDFContent(string path, int pageCount)
        {
            if (false == File.Exists(path))
            {
                throw CBaseException.GenericException("The PDF file does not exist.");
            }
            if (false == this.IsNew)
            {
                throw CBaseException.GenericException("Cannot use UpdatePDFContentOnSave(string) when the edoc is not new.");
            }

            this.PageCount = pageCount;
            m_sOriginalPdfPath = path;
            m_sNewPdfPath = path;
            m_requiresRegeneration = true;
        }

        /// <summary>
        /// USE THIS WHEN IMPORTING NEW PDF FROM CLIENT OR ANY OTHER SOURCES NOT FROM EDOCS.
        /// </summary>
        /// <remarks>
        /// The code for legacy EDocs is still in the code base, which is why this has the optional
        /// suppressSignatureDetection parameter. When support for legacy EDocs is removed from the
        /// code base we can and should remove this parameter.
        /// </remarks>
        /// <param name="path">The path to the pdf.</param>
        /// <param name="suppressSignatureDetection">
        /// Indicates whether we should prevent flagging the document as signed. Try to avoid
        /// using this, it is only exposed because of legacy EDoc support.
        /// </param>
        public void UpdatePDFContentOnSave(string path, bool suppressSignatureDetection = false)
        {
            if (false == File.Exists(path))
            {
                throw CBaseException.GenericException("The PDF file does not exist.");
            }
            if (false == this.IsNew)
            {
                throw CBaseException.GenericException("Cannot use UpdatePDFContentOnSave(string) when the edoc is not new.");
            }

            if (AntiVirusScanner.Scan(path)  == E_AntiVirusResult.HasVirus)
            {
                throw new CBaseException("The given pdf contains a virus.", "Virus found in PDF");
            }

            this.m_isNonDestructiveEdocs = true;
            this.m_hasOriginalPdfToConvert = true;

            var pdfReader = EDocumentViewer.CreateRandomAccessPdfReader(path);
            this.PageCount = pdfReader.NumberOfPages;

            bool containsSignature = false;

            if (!suppressSignatureDetection)
            {
                try
                {
                    containsSignature = ContainsSignatureImpl(pdfReader);
                }
                catch (InvalidCastException e)
                {
                    Tools.LogError($"Unable to determine if document is signed due to exception in iTextSharp.", e);
                    this.m_internalNewAuditEvents.AddLast(new EDocumentIntermediateAuditEvent
                    {
                        ActionD = DateTime.Now,
                        UserAction = E_PdfUserAction.UnableToDetermineSignatureStatus
                    });
                }
            }

            if (containsSignature)
            {
                this.MarkAsESigned();
            }

            pdfReader.Close();

            this.m_sNewPdfPath = path;
        }

        public static bool ContainsSignature(PdfReader reader)
        {
            try
            {
                return ContainsSignatureImpl(reader);
            }
            catch (InvalidCastException e)
            {
                Tools.LogError($"Unable to determine if document is signed due to exception in iTextSharp.", e);
                return false;
            }
        }

        private static bool ContainsSignatureImpl(PdfReader reader)
        {
            if (reader == null)
            {
                throw new ArgumentNullException(nameof(reader));
            }

            // GetSignatureNames() will get the field names that have signatures and are signed.
            // Note: I'm not 100% sure how we can catch all signatures. This method seems to be
            // what we want and worked for all of my test cases.
            // If needed, we can try to perform signature verification. A more detailed example of
            // this can be found at: 
            // https://developers.itextpdf.com/examples/security/digital-signatures-white-paper/digital-signatures-chapter-5
            return reader.AcroFields?.GetSignatureNames()?.Any() ?? false;
        }

        public string OverwritePDFContentForExistingDocs(string path)
        {
            if (true == this.IsNew)
            {
                throw CBaseException.GenericException("Cannot use OverwritePDFContentForExistingDocs(string) when the edoc is new.");
            }

            this.IsNew = true;
            UpdatePDFContentOnSave(path);
            string nonDestructivePdfDocumentId = string.Empty;
            if (this.m_isNonDestructiveEdocs == true)
            {
                // 8/13/2015 - For non destructive edocs, we need to put this new PDF in the PdfRastizer queue and mark ImageStatus as InQueue.
                nonDestructivePdfDocumentId = Guid.NewGuid().ToString();

                try
                {
                    FileDBTools.WriteFile(E_FileDB.EdmsPdf, nonDestructivePdfDocumentId, path);
                    this.m_edocsMetaData = new EdocsMetadata();
                    this.m_edocsMetaData.OriginalDocumentId = new Guid(nonDestructivePdfDocumentId);
                }
                catch 
                {
                    FileDBTools.Delete(E_FileDB.EdmsPdf, nonDestructivePdfDocumentId);
                    throw;
                }
            }

            this.IsNew = false;
            return nonDestructivePdfDocumentId;
        }

        /// <summary>
        /// Processes the specified metadata to create an LQB e-doc.
        /// </summary>
        /// <param name="request">
        /// The OCR request.
        /// </param>
        /// <param name="processedDocument">
        /// The xml node containing document metadata.
        /// </param>
        public void ProcessDocumentCaptureMetadata(OcrRequest request, XmlNode processedDocument, Dictionary<string, DocType> docTypeDictionary)
        {
            var documentMetadata = new EdocsMetadata();
            var pages = processedDocument.SelectNodes("pages/page");

            foreach (XmlNode page in pages)
            {
                var pageNum = int.Parse(page.Attributes["num"].Value);
                var pageHeight = int.Parse(page.Attributes["height"].Value);
                var pageWidth = int.Parse(page.Attributes["width"].Value);

                var pageMetadata = new EdocsPageMetadata()
                {
                    ReferenceDocumentId = request.RequestId.Value,
                    ReferenceDocumentPageNumber = pageNum,
                    PdfHeight = pageHeight,
                    PdfWidth = pageWidth
                };

                documentMetadata.AddPage(pageMetadata);
            }

            this.PageCount = documentMetadata.Pages.Count();
            this.LoanId = request.LoanId;
            this.AppId = request.AppId;
            this.EDocOrigin = request.Origin;
            this.IsUploadedByPmlUser = request.UploadingUserType == "P";

            var ktaDocType = processedDocument.SelectSingleNode("ktaDocType").InnerText;
            if (docTypeDictionary.ContainsKey(ktaDocType))
            {
                this.m_DocTypeId = docTypeDictionary[ktaDocType].Id;
            }
            else
            {
                var unclassifiedDocTypeId = EDocumentDocType.GetOrCreateDocType(this.BrokerId, "UNCLASSIFIED", EDocumentFolder.UNCLASSIFIED_FOLDERNAME);
                this.m_DocTypeId = unclassifiedDocTypeId;
                Tools.LogWarning($"The KTA DocType {ktaDocType} has no mapping for lender {this.BrokerId}.");
            }

            // The document uploaded to KTA will be the original and current document
            // until the user makes further changes.
            this.m_originalEdocsMetaData = this.m_edocsMetaData = documentMetadata;
            this.m_isNonDestructiveEdocs = true;
        }

        public bool CanRead()
        {
            AbstractUserPrincipal principal = Thread.CurrentPrincipal as AbstractUserPrincipal;
            return CanRead(principal);
        }

        public bool CanRead(AbstractUserPrincipal principal)
        {
            bool canRead = false;
            if (principal.IsInRole(ConstApp.ROLE_CONSUMER))
            {
                // Consumers can only read documents that they have the proper role to read AND only after they've been accepted by the lender (IsEditable = false)
                canRead = (m_isEditable == false) && Folder.CanRead(principal);
            }
            else
            {
                canRead = Folder.CanRead(principal);
            }

            return canRead;
        }

        public bool CanEdit
        {
            get { return m_isEditable && CanRead(); }
        }

        public bool CanDelete
        {
            get { return CanRead(); }
        }

        /// <summary>
        /// Copy PDF content of source edocs to this edocs. For non destructive edocs only meta data is copy.
        /// </summary>
        /// <param name="sourceDocument"></param>
        /// <returns></returns>
        internal bool CopyPdf(EDocument sourceDocument, bool suppressSignatureDetection)
        {
            if (sourceDocument.m_edocsMetaData != null)
            {
                return CopyPdfNonDestructive(sourceDocument, suppressSignatureDetection);
            }
            else
            {
                return CopyPdfLegacy(sourceDocument, suppressSignatureDetection);
            }
        }

        private bool CopyPdfNonDestructive(EDocument sourceDocument, bool suppressSignatureDetection)
        {
            this.m_edocsMetaData = sourceDocument.m_edocsMetaData;
            this.m_originalEdocsMetaData = sourceDocument.m_originalEdocsMetaData;
            this.m_isNonDestructiveEdocs = true;
            this.PageCount = sourceDocument.PageCount;

            if (!suppressSignatureDetection && sourceDocument.IsESigned)
            {
                this.MarkAsESigned();
            }

            return true;
        }

        private bool CopyPdfLegacy(EDocument sourceDocument, bool suppressSignatureDetection)
        {
            string filePath = null;
            try
            {
                filePath = sourceDocument.GetPDFTempFile_Current();
            }
            catch (FileNotFoundException exc)
            {
                Tools.LogError("[EDocsCopyQueueProcessor] - Failed to get temp PDF file for existing doc. DocId: " + sourceDocument.DocumentId, exc);
                return false;
            }

            try
            {
                this.UpdatePDFContentOnSave(filePath, suppressSignatureDetection);
            }
            catch (InvalidPDFFileException exc)
            {
                var msg = String.Format("[EDocsCopyQueueProcessor] - Invalid PDF for copied doc. Source DocId: {0}, Destination DocId: {1}, Destination Loan Id: {2}.", sourceDocument.DocumentId, sourceDocument.DocumentId, this.LoanId);
                Tools.LogError(msg, exc);
                return false;
            }
            catch (InsufficientPdfPermissionException exc)
            {
                var msg = String.Format("[EDocsCopyQueueProcessor] - Failed copy for password protected doc. Source DocId: {0}, Destination DocId: {1}, Destination Loan Id: {2}.", sourceDocument.DocumentId, sourceDocument.DocumentId, this.LoanId);
                Tools.LogError(msg, exc);
                return false;
            }
            catch (CBaseException exc)
            {
                var msg = String.Format("[EDocsCopyQueueProcessor] - Unexpected exception when trying to update PDF content. Source DocId: {0}, Destination DocId: {1}, Destination Loan Id: {2}.", sourceDocument.DocumentId, this.DocumentId, this.LoanId);
                Tools.LogError(msg, exc);
                return false;
            }

            return true;
        }

        public string GetImageReadyETA()
        {
            return GetImageReadyETA(this.ImageStatus, this.PageCount);
        }

        public static string GetImageReadyETA(E_ImageStatus imageStatus, int pageCount)
        {
            if (imageStatus != E_ImageStatus.NoCachedImagesButInQueue)
            {
                return "no estimate";
            }

            var conversionTime = new TimeSpan(0, 0, 0, 0, ConstStage.EDocImageGeneratingMillisecondPerPageEstimate * pageCount);
            var queueTime = new TimeSpan(0, 0, 0, 0, ConstStage.EDocImageGeneratingQueueWaitingMillisecondAvgEstimate);

            // We add 2 minutes here due to sampling aliasing
            TimeSpan estimatedTime = conversionTime + queueTime + new TimeSpan(0, 2, 0);

            // Round up to nearest minute
            int estimatedMinutes = estimatedTime.Minutes;
            string eta;
            if (estimatedTime.Minutes <= 0)
            {
                eta = "< 1 minute";
            }
            else
            {
                eta = (estimatedTime.Minutes + 1).ToString() + " minutes";
            }

            return eta;
        }

        /// <summary>
        /// Retrieves the version of the document with the given ID.
        /// </summary>
        /// <param name="documentId">
        /// The ID of the document.
        /// </param>
        /// <param name="brokerId">
        /// The ID of the broker for the document.
        /// </param>
        /// <returns>
        /// The document version.
        /// </returns>
        public static int GetDocumentVersionById(Guid documentId, Guid brokerId)
        {
            var procedureName = StoredProcedureName.Create("EDOCS_GetDocumentVersionById").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@DocumentId", documentId),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            {
                var result = StoredProcedureDriverHelper.ExecuteScalar(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
                if (result == DBNull.Value)
                {
                    // Use the -1 default for new documents if no version is found.
                    return -1;
                }

                return Convert.ToInt32(result);
            }
        }

        #region loading methods   -- these methods are used for loading from the DB -- should probably be in the repo class.

        /// <summary>
        /// Get the broker id that this doc belong.
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        private static Guid GetBrokerIdByDocIdSlow(Guid documentId)
        {
            Guid brokerId = Guid.Empty;
            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                SqlParameter[] parameters = {
                                            new SqlParameter("@DocumentId", documentId)
                                        };
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "EDOCS_GetBrokerIdByDocId", parameters))
                {
                    if (reader.Read())
                    {
                        brokerId = (Guid)reader["BrokerId"];
                        break;
                    }
                }
            }
            return brokerId;
        }

        /// <summary>
        ///  Retrieves the document with the given id.
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException">Thrown when the document id does not exist in the database.</exception>
        public static EDocument GetDocumentByIdObsolete(Guid documentId)
        {
            Guid brokerId = GetBrokerIdByDocIdSlow(documentId);
            SqlParameter[] parameters = {
                new SqlParameter("@DocumentId", documentId),
                new SqlParameter("@IsValid", true),
                new SqlParameter("@BrokerId", brokerId)
            };

            IEnumerable<EDocument> docs = GetDocuments(brokerId, false, parameters);
            if (docs.Count() == 0)
            {
                throw new NotFoundException(ErrorMessages.Generic, "The electronic document was not found. " + documentId);

            }

            return docs.First();
        }

        /// <summary>
        ///  Retrieves the document with the given id.
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        /// <exception cref="NotFoundException">Thrown when the document id does not exist in the database.</exception>
        public static EDocument GetDocumentById(Guid documentId, Guid brokerId, bool? isDeleted = null)
        {
            IEnumerable<EDocument> docs = GetDocuments(brokerId, false,
                new SqlParameter("@DocumentId", documentId),
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@IsValid", !isDeleted)
                );
            if (docs.Count() == 0)
            {
                throw new NotFoundException(ErrorMessages.Generic, "The electronic document was not found. DocumentId=[" + documentId + "], BrokerId=[" + brokerId + "]");

            }
            return docs.First();
        }

        /// <summary>
        /// Gets a document's description by ID.
        /// </summary>
        /// <param name="documentId">
        /// The ID of the document.
        /// </param>
        /// <param name="brokerId">
        /// The ID of the document's broker.
        /// </param>
        /// <returns>
        /// The document's description.
        /// </returns>
        public static string GetDescriptionById(Guid documentId, Guid brokerId)
        {
            var procedureName = StoredProcedureName.Create("EDOCS_DOCUMENT_GetDescriptionById").ForceValue();
            SqlParameter[] parameters =
            {
                new SqlParameter("@DocumentId", documentId),
                new SqlParameter("@BrokerId", brokerId)
            };

            using (var connection = DbConnectionInfo.GetConnection(brokerId))
            {
                var result = StoredProcedureDriverHelper.ExecuteScalar(connection, null, procedureName, parameters, TimeoutInSeconds.Thirty);
                return result.ToString();
            }
        }

        /// <summary>
        /// Gets documents that have images older that xMonths months.  Returns only top 10000
        /// </summary>
        /// <param name="xMonths">How old the images should be.</param>
        /// <returns></returns>
        public static IList<EDocument> GetDocumentsWithImagesOlderThanXMonths(int xMonths)
        {
            List<EDocument> documents = new List<EDocument>();

            foreach (var connInfo in DbConnectionInfo.ListAll())
            {
                var resultSet = new List<Dictionary<string, object>>();
                List<SqlParameter> parameters = new List<SqlParameter>();
                parameters.Add(new SqlParameter("@months", xMonths));
                using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(connInfo, "EDOCS_GetDocumentsWithImagesOlderThanXMonths", parameters))
                {
                    while (reader.Read())
                    {
                        resultSet.Add(reader.ToDictionary());
                    }
                }

                foreach (var reader in resultSet)
                {
                    documents.Add(new EDocument(reader, null));
                }
            }

            return documents;
        }

        /// <summary>
        /// Gets the document IDd and Loan Id of a document.
        /// </summary>
        /// <param name="documentId">The document ID of the document to find loan and documents for.</param>
        /// <param name="brokerId">The broker ID, for selecting the right database.</param>
        /// <param name="loanId">The loan ID of the loan associated with the provided document.</param>
        /// <returns>A list of document IDs representing the other documents on the same loan file.</returns>
        public static List<Guid> GetDocumentIdsOnSameLoan(Guid documentId, Guid brokerId, out Guid loanId)
        {
            List<Guid> documentIds;
            SqlParameter loanIdParam = new SqlParameter
            {
                ParameterName = "@LoanId",
                SqlDbType = System.Data.SqlDbType.UniqueIdentifier,
                Direction = System.Data.ParameterDirection.Output
            };
            using (var reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetDocumentsOnSameLoan", new SqlParameter[] { new SqlParameter("@DocumentId", documentId), loanIdParam }))
            {
                documentIds = reader.Select(val => (Guid)val["DocumentId"]).ToList();
            }

            loanId = (Guid)loanIdParam.Value;
            return documentIds;
        }

        private static IList<EDocument> GetDocuments(Guid brokerId, bool isShippingRequest, params SqlParameter[] parameters)
        {
            if (isShippingRequest)
            {
                return GetDocumentsOld(brokerId, FETCH_SHIPPING_TEMPLATE_DOCS_SP, parameters);
            }

            return GetDocumentsNew(brokerId, FETCH_SP_NEW, parameters);
        }

        /// <summary>
        /// Fetches documents from the database using a stored procedure (EDOCS_FetchDocs, usually). 
        /// Supported parameters are 'DocumentId', 'BrokerId', 'LoanId', and 'IsValid' (defaults to true)
        /// </summary>
        /// <param name="parameter"></param>
        /// <returns></returns>
        private static IList<EDocument> GetDocumentsOld(Guid brokerId, string storedProcedure, params SqlParameter[] parameters)
        {
            var firstResultSet = new List<Dictionary<string, object>>();
            var secondResultSet = new List<Dictionary<string, object>>();
            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, storedProcedure, parameters))
            {
                while (reader.Read())
                {
                    firstResultSet.Add(reader.ToDictionary());
                }

                reader.NextResult();
                while (reader.Read())
                {
                    secondResultSet.Add(reader.ToDictionary());
                }
            }

            Dictionary<Guid, string> m_apps = new Dictionary<Guid, string>(firstResultSet.Count);
            foreach (var reader in firstResultSet)
            {
                Guid appId = (Guid)reader["aappid"];
                string aBFirstNm = reader["aBFirstNm"] == DBNull.Value ? "" : (string)reader["aBFirstNm"];
                string aCFirstNm = reader["aCFirstNm"] == DBNull.Value ? "" : (string)reader["aCFirstNm"];
                string aBLastNm = reader["aBLastNm"] == DBNull.Value ? "" : (string)reader["aBLastNm"];
                string aCLastNm = reader["aCLastNm"] == DBNull.Value ? "" : (string)reader["aCLastNm"];

                string appName = aBFirstNm + " " + aBLastNm;
                if (false == string.IsNullOrEmpty(aCFirstNm) || false == string.IsNullOrEmpty(aCLastNm))
                {
                    appName = appName.TrimWhitespaceAndBOM();
                    appName += " " + aCFirstNm + " " + aCLastNm;
                    appName = appName.TrimWhitespaceAndBOM();
                }
                m_apps.Add(appId, appName);
            }

            List<EDocument> documents = new List<EDocument>(secondResultSet.Count);
            foreach (var reader in secondResultSet)
            {
                string name;
                object appId = reader["aAppId"];
                if (appId == DBNull.Value || false == m_apps.TryGetValue((Guid)appId, out name))
                {
                    name = "";
                }

                documents.Add(new EDocument(reader, name));
            }

            return documents;
        }


        private static IList<EDocument> GetDocumentsNew(Guid brokerId, string storedProcedure, params SqlParameter[] parameter)
        {
            var appData = new List<Dictionary<string, object>>();
            var edocData = new List<Dictionary<string, object>>();
            var folderData = new List<Dictionary<string, object>>();

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, storedProcedure, parameter))
            {
                while (reader.Read())
                {
                    appData.Add(reader.ToDictionary());
                }

                reader.NextResult();
                while (reader.Read())
                {
                    edocData.Add(reader.ToDictionary());
                }

                if (reader.NextResult())
                {
                    while (reader.Read())
                    {
                        folderData.Add(reader.ToDictionary());
                    }
                }
            }

            Dictionary<Guid, string> m_apps = new Dictionary<Guid, string>(appData.Count);
            foreach (var reader in appData)
            {
                Guid appId = (Guid)reader["aappid"];
                string aBFirstNm = reader["aBFirstNm"] == DBNull.Value ? "" : (string)reader["aBFirstNm"];
                string aCFirstNm = reader["aCFirstNm"] == DBNull.Value ? "" : (string)reader["aCFirstNm"];
                string aBLastNm = reader["aBLastNm"] == DBNull.Value ? "" : (string)reader["aBLastNm"];
                string aCLastNm = reader["aCLastNm"] == DBNull.Value ? "" : (string)reader["aCLastNm"];

                string appName = aBFirstNm + " " + aBLastNm;
                if (false == string.IsNullOrEmpty(aCFirstNm) || false == string.IsNullOrEmpty(aCLastNm))
                {
                    appName = appName.TrimWhitespaceAndBOM();
                    appName += " " + aCFirstNm + " " + aCLastNm;
                    appName = appName.TrimWhitespaceAndBOM();
                }
                m_apps.Add(appId, appName);
            }

            Dictionary<int, EDocumentFolder> folders = null;
            if (folderData.Count > 0)
            {
                //we have folders! This is only available in the document listing.
                folders = EDocumentFolder.GetFoldersById(folderData);
            }

            List<EDocument> documents = new List<EDocument>(edocData.Count);
            foreach (var reader in edocData)
            {
                string name;
                object appId = reader["aAppId"];
                if (appId == DBNull.Value || false == m_apps.TryGetValue((Guid)appId, out name))
                {
                    name = "";
                }

                var edoc = new EDocument(reader, name);
                EDocumentFolder folder; 
                if (folders != null && folders.TryGetValue(edoc.FolderId, out folder))
                {
                    edoc.m_Folder = folder;
                }

                documents.Add(edoc);
            }

            return documents;
        }

        /// <summary>
        /// Retrieves documents with given loan.
        /// </summary>
        /// <param name="loanId"></param>
        /// <returns></returns>
        internal static IList<EDocument> GetDocumentsByLoanId(Guid loanId, Guid brokerId)
        {
            return GetDocuments(brokerId, false, new SqlParameter("@LoanId", loanId), new SqlParameter("@BrokerId", brokerId), new SqlParameter("@IsValid", true));
        }

        internal static IList<EDocument> GetDeletedDocumentsByLoanId(Guid loanId, Guid brokerId)
        {
            return GetDocuments(brokerId, false, new SqlParameter("@LoanId", loanId), new SqlParameter("@BrokerId", brokerId), new SqlParameter("@IsValid", false));
        }

        /// <summary>
        /// Retrieves documents by shipping template id. Database responsible of making sure the template is accessible by the given broker.
        /// </summary>
        /// <param name="shippingTemplateId"></param>
        /// <param name="brokerId"></param>
        /// <param name="loanId"></param>
        /// <returns></returns>
        internal static IList<EDocument> GetDocumentsByShippingTemplate(int shippingTemplateId, Guid brokerId, Guid loanId)
        {

            return GetDocuments(brokerId, true, 
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@LoanId", loanId),
                new SqlParameter("@ShippingTemplateId", shippingTemplateId));

        }
        #endregion

        #region filedb stuff that we should delete.

        private static void WritePathToFileDB(string key, string path)
        {
            using (PerformanceStopwatch.Start("EDocument.WritePathToFileDB()"))
            {
                //FileDB Server intermittenly takes way too long to write the file 
                //so instead of completely sleep a bit and check the file length to see if matches. 
                bool verifyFileDBContents = false;

                if (ConstStage.IsEnableEDocVerify) // dd 5/29/2018 - This is currently false on production. Consider remove the block of code that perform verifyFileDB.
                {
                    verifyFileDBContents = true;
                }

                long retryTimeoutInMilliseconds = 300000; //5 minutes

                FileDBTools.WriteFile(E_FileDB.EDMS, key, path);

                if (verifyFileDBContents)
                {
                    bool success = false;
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    try
                    {
                        do
                        {
                            try
                            {
                                string fileDBPath = FileDBTools.CreateCopy(E_FileDB.EDMS, key);
                                FileInfo dstFile = new FileInfo(fileDBPath);
                                FileInfo srcFile = new FileInfo(path);
                                if (dstFile.Length != srcFile.Length)
                                {
                                    throw CBaseException.GenericException("File lengths do not match FileDB: " + dstFile.Length + " " + srcFile.Length);
                                }
                                success = true;
                                Tools.LogInfo("[verifyFileDBContents] Save Confirmed after filedb file in use error.");
                                break;
                            }
                            catch (IOException e)
                            {
                                Tools.LogError(e);
                                LendersOffice.Sleeping.PerformanceLoggingSleeper.Instance.Sleep(TimeSpan.FromSeconds(10));
                            }

                        } while (sw.ElapsedMilliseconds <= retryTimeoutInMilliseconds);

                        if (!success)
                        {
                            throw CBaseException.GenericException("Could not confirm if file saved successfully timeout expired.");
                        }
                    }
                    finally
                    {
                        sw.Stop();
                        Tools.LogWarning("[verifyFileDBContents] Took " + sw.ElapsedMilliseconds + " ms to complete.");
                    }
                }
            }
        }

        private void DeleteFileDBEntry(string key)
        {
            FileDBTools.Delete(E_FileDB.EDMS, key);

            Tools.LogInfoWithStackTrace("FileDBDelete key : " + key + " for document id " + DocumentId);
        }

        private string CopyFileDBEntry(string key)
        {
            return FileDBTools.CreateCopy(E_FileDB.EDMS, key);
        }

        #endregion

        #region stuff that should not be in this class

        /// <summary>
        /// Sets the isvalid of a document to false. The audit table is also updated.
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="userId"></param>
        /// <param name="reason"></param>
        internal static void InvalidateDocument(Guid brokerId, Guid documentId, Guid? userId, string reason)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocumentId", documentId), 
                                            new SqlParameter("@UserId", userId), 
                                            new SqlParameter("@Reason", reason)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, INVALIDATE_SP, 2, parameters);
            // Also delete document-condition associations. This is done in a trigger
        }

        internal static void RestoreDocument(Guid brokerId, Guid documentId, Guid userId, string reason)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@DocumentId", documentId), 
                                            new SqlParameter("@UserId", userId), 
                                            new SqlParameter("@Reason", reason)
                                        };
            StoredProcedureHelper.ExecuteNonQuery(brokerId, RESTORE_SP, 2, parameters);
        }


        public static string GetCurrentPDFKey(Guid brokerId, Guid documentId)
        {
            SqlParameter[] parameters = new SqlParameter[] {
                new SqlParameter("@BrokerId", brokerId),
                new SqlParameter("@DocId", documentId)
            };

            using (DbDataReader reader = StoredProcedureHelper.ExecuteReader(brokerId, "EDOCS_GetCurrentPDFKey", parameters))
            {
                if (reader.Read())
                {
                    return ((Guid)reader["FileDBKey_CurrentRevision"]).ToString();
                }
            }

            return string.Empty;
        }

        public static void UpdateImageStatus(Guid brokerId, Guid docId, E_ImageStatus status)
        {

            DateTime? expirationTime;
            if (status == E_ImageStatus.NoCachedImagesButInQueue)
            {
                expirationTime = DateTime.Now.AddMinutes(ConstStage.WaitBeforeRequeuingForImageGeneration);
            }
            else
            {
                expirationTime = new DateTime?();
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@DocId", docId),
                                            new SqlParameter("@ImageStatus", status),
                                            new SqlParameter("@TimeToRequeueForImageGeneration", expirationTime)
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_UpdateStatus", 3, parameters);
        }

        private static EdocsMetadata ConvertToEdocsMetadata(PdfRasterizerLib.DocumentMetadata metadata)
        {
            EdocsMetadata edocsMetadata = new EDocs.EdocsMetadata();

            if (metadata != null)
            {
                foreach (var o in metadata.Pages)
                {
                    EdocsPageMetadata page = new EdocsPageMetadata();
                    page.PdfHeight = o.PdfHeight;
                    page.PdfWidth = o.PdfWidth;
                    page.PngFullHeight = o.PngFullHeight;
                    page.PngFullWidth = o.PngFullWidth;
                    page.PngThumbHeight = o.PngThumbHeight;
                    page.PngThumbWidth = o.PngThumbWidth;
                    page.ReferenceDocumentId = o.ReferenceDocumentId;
                    page.ReferenceDocumentPageNumber = o.ReferenceDocumentPageNumber;
                    page.Rotation = o.Rotation;
                    edocsMetadata.AddPage(page);
                }
            }

            return edocsMetadata;
        }

        public static void UpdateImageStatusWithMetadataJson(Guid brokerId, Guid docId, E_ImageStatus status, PdfRasterizerLib.DocumentMetadata documentMetaData)
        {
            SqlParameter metadataProtobufContentParameter = new SqlParameter("@MetadataProtobufContent", System.Data.SqlDbType.Image);
            metadataProtobufContentParameter.Value = null;

            SqlParameter originalMetadataProtobufContentParameter = new SqlParameter("@OriginalMetadataProtobufContent", System.Data.SqlDbType.Image);
            originalMetadataProtobufContentParameter.Value = null;

            if (documentMetaData != null)
            {
                var metadata = ConvertToEdocsMetadata(documentMetaData);
                byte[] bytes = SerializationHelper.ProtobufSerialize(metadata);
                metadataProtobufContentParameter.Value = bytes;
                originalMetadataProtobufContentParameter.Value = bytes;
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@DocId", docId),
                                            new SqlParameter("@ImageStatus", status),
                                            new SqlParameter("@MetadataJsonContent", string.Empty),
                                            new SqlParameter("@OriginalMetadataJsonContent", string.Empty),
                                            metadataProtobufContentParameter,
                                            originalMetadataProtobufContentParameter
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_UpdateStatusWithMetadata", 3, parameters);
        }

        public static void UpdateImageStatusAfterSuccessfulMigration(Guid brokerId, Guid docId, PdfRasterizerLib.DocumentMetadata documentMetaData)
        {
            SqlParameter metadataProtobufContentParameter = new SqlParameter("@MetadataProtobufContent", System.Data.SqlDbType.Image);
            metadataProtobufContentParameter.Value = null;

            if (documentMetaData != null)
            {
                var metadata = ConvertToEdocsMetadata(documentMetaData);
                metadataProtobufContentParameter.Value = SerializationHelper.ProtobufSerialize(metadata);
            }

            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", brokerId),
                                            new SqlParameter("@DocId", docId),
                                            new SqlParameter("@ImageStatus", E_ImageStatus.HasCachedImages),
                                            new SqlParameter("@MetadataJsonContent", string.Empty),
                                            metadataProtobufContentParameter
                                        };

            StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_UpdateStatusAfterMigration", 3, parameters);
        }

        public static void SaveMetaData(Guid brokerId, Guid docId, Guid key, DocumentMetaData data)
        {
            try
            {
                string content = data.Serialize();

                SqlParameter[] parameters = new SqlParameter[] { 
                new SqlParameter("@DocumentId", docId),
                new SqlParameter("@PageInfoXml", content),
                new SqlParameter("@RevisionKey", key)
            };

                StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_UpdatePageInfo", 1, parameters);
            }
            catch (ArgumentNullException)
            {
                //no -op this happens when theres no key for a page. Dont bother saving.
            }
        }


        // db - Creating for consumer portal project
        /// <summary>
        /// Determines which documents associated with the current loan file the given user has access to see.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="loanId"></param>
        /// <returns>The list of EDocument objects that the given user has permission to see.</returns>
        public static IList<EDocument> GetDocsAvailableForDownload(AbstractUserPrincipal user, Guid loanId)
        {
            List<EDocument> documents = new List<EDocument>();

            IList<EDocument> documentsInLoan = GetDocumentsByLoanId(loanId, user.BrokerId);

            UserEDocumentPermissions docPermissions;
            foreach (EDocument doc in documentsInLoan)
            {
                docPermissions = new UserEDocumentPermissions(user);
                if (docPermissions.CanReadDocument(doc))
                {
                    documents.Add(doc);
                }
            }

            return documents;
        }

        public static void InsertNewPNG(Guid brokerId, Guid docId, IEnumerable<string> pngKeys)
        {
            if (pngKeys.Count() == 0)
            {
                return;
            }
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (string pngkey in pngKeys)
                {
                    sb.Append(pngkey);
                    sb.Append(",");
                }

                SqlParameter[] parameters = {
                                                new SqlParameter("@DocumentId", docId),
                                                new SqlParameter("@CommaSeparatedPngKeys", sb.ToString())
                                            };
                StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_AddPngEntry", 3, parameters);
            }
            catch (SqlException e)
            {
                Tools.LogError("[QP] Could not store pngkeys Ignoring Error " + docId, e);

            }

        }

        public static string GeneratePDFDataWithPastedAnnotation(string path, ILookup<int, EDocumentAnnotationItem> annotations)
        {
            PdfReader reader = new PdfReader(path);
            string newPath = TempFileUtils.NewTempFilePath();

            using (FileStream fs = File.OpenWrite(newPath))
            {
                PdfStamper stamper = new PdfStamper(reader, fs);
                for (int i = 1; i <= reader.NumberOfPages; i++)
                {
                    if (annotations.Contains(i))
                    {
                        PdfContentByte contentByte = stamper.GetOverContent(i);

                        foreach (EDocumentAnnotationItem item in annotations[i])
                        {
                            OverlayAnnotation(contentByte, item, true);
                        }
                    }
                }
                stamper.Close();
            }

            return newPath;
        }

        public static EDocument CreateEdoc(string path, int docType, Guid brokerId, Guid loanId, Guid appId, Guid? userId, bool isPmlUser, E_EDocumentSource source, string description)
        {
            var repository = EDocumentRepository.GetSystemRepository(brokerId);
            var edoc = repository.CreateDocument(source);
            edoc.AppId = appId;
            edoc.PublicDescription = description;
            edoc.LoanId = loanId;
            edoc.DocumentTypeId = docType;
            edoc.IsUploadedByPmlUser = isPmlUser;
            edoc.UpdatePDFContentOnSave(path);
            edoc.Save(userId);

            return edoc;
        }

        /// <summary>
        /// Creates and saves an editable copy of an e-signed document.
        /// </summary>
        /// <param name="repository">The EDoc repository.</param>
        /// <param name="signedDocument">The signed document to copy.</param>
        /// <returns>
        /// The ID of the editable copy.
        /// </returns>
        public static Guid CreateEditableCopyOfSignedDoc(EDocumentRepository repository, EDocument signedDocument)
        {
            if (repository == null)
            {
                throw new ArgumentNullException(nameof(repository));
            }
            else if (signedDocument == null)
            {
                throw new ArgumentNullException(nameof(signedDocument));
            }
            else if (!signedDocument.IsESigned)
            {
                throw new InvalidOperationException("Source document is not signed.");
            }

            var newDoc = repository.CreateDocumentFromExisting(
                E_EDocumentSource.EditableCopyOfSignedDoc,
                signedDocument.DocumentId);

            newDoc.LoanId = signedDocument.LoanId;
            newDoc.AppId = signedDocument.AppId;
            newDoc.IsUploadedByPmlUser = signedDocument.IsUploadedByPmlUser;
            newDoc.CopyPdf(signedDocument, suppressSignatureDetection: true);

            // We are setting the private members here to avoid duplicate audit items.
            // We are copying the values and the audit history from the source file,
            // and the history of where these fields came from should be in the source
            // audit.
            newDoc.m_DocTypeId = signedDocument.DocumentTypeId;
            newDoc.m_eStatus = signedDocument.DocStatus;
            newDoc.m_sPublicDescription = signedDocument.PublicDescription;
            newDoc.m_sInternalDescription = signedDocument.InternalDescription;

            repository.Save(newDoc);
            return newDoc.DocumentId;
        }

        public static void GeneratePDFDataWithAnnotation(AbstractUserPrincipal principal, IEnumerable<PdfPageItem> pages, EDocumentAnnotationContainer annotationContainer, Stream outputPdfStream)
        {
            if (principal == null || principal.HasPermission(Permission.CanViewEDocsInternalNotes) == false)
            {
                throw new AccessDenied("User does not have permission to view E-Docs Internal Notes");
            }
            var repo = EDocumentRepository.GetUserRepository();

            Dictionary<Guid, iTextSharp.text.pdf.PdfReader> hash = new Dictionary<Guid, iTextSharp.text.pdf.PdfReader>();
            List<string> tempFilesToRemove = new List<string>();
            // Load all different docs in memory.
            foreach (PdfPageItem item in pages)
            {
                Guid docId = item.DocId;
                bool isNonDestructiveEdocs = false;

                if (docId == Guid.Empty)
                {
                    // 6/26/2015 dd - This is NonDestructiveEdocs.
                    PNGEncryptedData encryptedData = new PNGEncryptedData(item.PngKey);
                    item.DocId = encryptedData.DocumentId;
                    item.Page = encryptedData.PageNumber + 1;
                    isNonDestructiveEdocs = true;
                }
                if (!hash.ContainsKey(item.DocId))
                {
                    string path = string.Empty;

                    if (isNonDestructiveEdocs)
                    {
                        path = GetNonDestructivePdfFromFileDb(item.DocId);
                        //path = FileDBTools.CreateCopy(E_FileDB.EDMS, item.DocId.ToString());
                    }
                    else
                    {
                        EDocument d = repo.GetDocumentById(item.DocId);
                        if (d.Version != item.Version)
                        {
                            throw new InvalidEDocumentVersion(item.Version, d.Version);
                        }

                        path = d.GetPDFTempFile_Current();
                    }
                    tempFilesToRemove.Add(path);
                    iTextSharp.text.pdf.PdfReader reader = EDocumentViewer.CreatePdfReader(path);
                    hash.Add(item.DocId, reader);
                }
            }

            iTextSharp.text.Document document = new iTextSharp.text.Document();

            iTextSharp.text.pdf.PdfWriter pdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(document, outputPdfStream);
            document.Open();

            int currentPage = 1;
            foreach (var page in pages)
            {
                iTextSharp.text.pdf.PdfReader pdfReader = hash[page.DocId];

                int rotation = pdfReader.GetPageRotation(page.Page) + page.Rotation;
                pdfReader.GetPageN(page.Page).Put(iTextSharp.text.pdf.PdfName.ROTATE, new iTextSharp.text.pdf.PdfNumber(rotation));
                iTextSharp.text.Rectangle pageSize = pdfReader.GetPageSizeWithRotation(page.Page);

                // 4/4/2011 dd - The SetPageSize method must occur before NewPage method, otherwise the first page will not have correct size.
                document.SetPageSize(pageSize);
                document.NewPage();

                Matrix matrix = GetTransformationMatrixHelper(rotation, pageSize.Width, pageSize.Height);

                iTextSharp.text.pdf.PdfImportedPage p = pdfWriter.GetImportedPage(pdfReader, page.Page);

                iTextSharp.text.pdf.PdfContentByte cp = pdfWriter.DirectContent;
                cp.AddTemplate(p, matrix.Elements[0], matrix.Elements[1], matrix.Elements[2], matrix.Elements[3], matrix.Elements[4], matrix.Elements[5]);

                OverlayAnnotation(cp, annotationContainer, currentPage);
                currentPage++;

            }
            document.Close();

            foreach (string path in tempFilesToRemove)
            {
                try
                {
                    File.Delete(path);
                }
                catch (FileNotFoundException)
                {

                }
            }
        }
        private static Matrix GetTransformationMatrixHelper(int rotation, float width, float height)
        {
            // 4/5/2011 dd - This method will perform 4 types of rotation and then translate so 0, 0 will fit in view port.
            // Rotation is in clockwise
            while (rotation < 0)
            {
                rotation += 360;
            }

            rotation = rotation % 360;

            if (rotation == 0)
            {
                return new Matrix(); // Return identity
            }
            else if (rotation == 90)
            {
                return new Matrix(0, -1, 1, 0, 0, height);
            }
            else if (rotation == 180)
            {
                return new Matrix(-1, 0, 0, -1, width, height);
            }
            else if (rotation == 270)
            {
                return new Matrix(0, 1, -1, 0, width, 0);
            }
            else
            {
                throw new CBaseException(ErrorMessages.Generic, rotation + " is not support for rotation");
            }
        }
        private static void OverlayAnnotation(iTextSharp.text.pdf.PdfContentByte contentByte, EDocumentAnnotationContainer annotationContainer, int page)
        {
            // 12/3/2010 dd - Note - page number is index of 1.
            foreach (var annotation in annotationContainer.AnnotationList)
            {
                if (annotation.PageNumber == page)
                {
                    OverlayAnnotation(contentByte, annotation, false);
                }
            }
        }
        private static void OverlayAnnotation(PdfContentByte contentByte, EDocumentAnnotationItem annotation, bool isForPastingAnnotations)
        {
            switch (annotation.ItemType)
            {
                case E_EDocumentAnnotationItemType.Highlight:
                    OverlayHighlight(contentByte, annotation.Rectangle.Left, annotation.Rectangle.Bottom, annotation.Rectangle.Width, annotation.Rectangle.Height);
                    break;
                case E_EDocumentAnnotationItemType.Notes:
                    if (annotation.NoteStateType == E_DocumentAnnotationNoteStateType.Maximized)
                    {
                        string content;
                        if (isForPastingAnnotations)
                        {
                            content = annotation.Text;
                            OverlayPasteNote(contentByte, content, annotation.Rectangle.Left, annotation.Rectangle.Bottom, annotation.Rectangle.Width, annotation.Rectangle.Height);
                        }
                        else
                        {
                            content = string.Format("Last Updated: {0}{1}{1}{2}", annotation.LastModifiedDate.ToShortDateString(), Environment.NewLine, annotation.Text);
                            OverlayNote(contentByte, content, annotation.Rectangle.Left, annotation.Rectangle.Bottom, annotation.Rectangle.Width, annotation.Rectangle.Height);
                        }
                    }
                    else
                    {
                        OverlayMinimizeNoteIcon(contentByte, annotation.Rectangle.Left, annotation.Rectangle.Bottom);

                    }
                    break;
                default:
                    throw new UnhandledEnumException(annotation.ItemType);
            }
        }
        private static void OverlayMinimizeNoteIcon(iTextSharp.text.pdf.PdfContentByte contentByte, float x, float y)
        {
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(typeof(EdocFaxCover).Assembly.GetManifestResourceStream("LendersOffice.ObjLib.Edocs.note-thumb-min.png"));
            image.SetAbsolutePosition(x, y);
            contentByte.AddImage(image);
        }
        private static void OverlayHighlight(iTextSharp.text.pdf.PdfContentByte contentByte, float x, float y, float width, float height)
        {
            contentByte.SaveState();

            iTextSharp.text.pdf.PdfGState gs = new iTextSharp.text.pdf.PdfGState();
            gs.FillOpacity = .5f;
            contentByte.SetGState(gs);

            contentByte.SetColorFill(iTextSharp.text.BaseColor.YELLOW);
            contentByte.SetColorStroke(iTextSharp.text.BaseColor.YELLOW);
            contentByte.Rectangle(x, y, width, height);
            contentByte.FillStroke();

            contentByte.RestoreState();
        }

        private static void OverlayPasteNote(PdfContentByte contentByte, string content, float x, float y, float width, float height)
        {
            contentByte.SaveState();

            #region Draw Text
            contentByte.SetColorFill(iTextSharp.text.BaseColor.BLACK);

            iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Chunk chunk = new iTextSharp.text.Chunk(content, font);

            iTextSharp.text.pdf.ColumnText columnText = new iTextSharp.text.pdf.ColumnText(contentByte);
            columnText.SetSimpleColumn(x , y , x + width , y + height , 9, iTextSharp.text.Element.ALIGN_TOP | iTextSharp.text.Element.ALIGN_LEFT);
            columnText.AddText(chunk);
            columnText.Go();
            #endregion

            contentByte.RestoreState();
        }

        private static void OverlayNote(iTextSharp.text.pdf.PdfContentByte contentByte, string content, float x, float y, float width, float height)
        {
            float padding = 5;

            contentByte.SaveState();


            contentByte.SetColorFill(iTextSharp.text.BaseColor.WHITE);
            contentByte.SetColorStroke(iTextSharp.text.BaseColor.BLACK);
            contentByte.Rectangle(x, y, width, height);
            contentByte.FillStroke();

            #region Draw Text
            contentByte.SetColorFill(iTextSharp.text.BaseColor.BLACK);

            iTextSharp.text.Font font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9, iTextSharp.text.Font.NORMAL);
            iTextSharp.text.Chunk chunk = new iTextSharp.text.Chunk(content, font);

            iTextSharp.text.pdf.ColumnText columnText = new iTextSharp.text.pdf.ColumnText(contentByte);
            columnText.SetSimpleColumn(x + padding, y + padding, x + width - padding, y + height - padding, 9, iTextSharp.text.Element.ALIGN_TOP | iTextSharp.text.Element.ALIGN_LEFT);
            columnText.AddText(chunk);
            columnText.Go();
            #endregion

            contentByte.RestoreState();
        }

        public static string StatusText(E_EDocStatus status)
        {
            switch (status)
            {
                case E_EDocStatus.Approved:
                    return "Protected";
                case E_EDocStatus.Rejected:
                    return "Rejected";
                case E_EDocStatus.Screened:
                    return "Screened";
                case E_EDocStatus.Obsolete:
                    return "Obsolete";
                case E_EDocStatus.Blank:
                default:
                    return string.Empty;
            }
        }

        public static E_EDocStatus StatusFromText(string status)
        {
            if (string.IsNullOrEmpty(status)) return E_EDocStatus.Blank;

            switch (status.ToLower().TrimWhitespaceAndBOM())
            {
                case "approved":
                    return E_EDocStatus.Approved;
                case "rejected":
                    return E_EDocStatus.Rejected;
                case "screened":
                    return E_EDocStatus.Screened;
                case "obsolete":
                    return E_EDocStatus.Obsolete;
                case "protected":
                    return E_EDocStatus.Approved;
                case "":
                default:
                    return E_EDocStatus.Blank;
            }
        }
        #endregion

        #region Non destructive Edocs feature.
        private bool m_isNonDestructiveEdocs = false;
        private bool m_hasOriginalPdfToConvert = false;

        /// <summary>
        /// Use this when work with Non Destructive Edocs.
        /// </summary>
        /// <param name="userId"></param>
        private void CreateVersion2(Guid? userId)
        {
            if (this.m_hasOriginalPdfToConvert && string.IsNullOrEmpty(m_sNewPdfPath))
            {
                throw new CBaseException(ErrorMessages.Generic, "BUG-Trying to save a document w/o a pdf.");
            }

            string nonDestructivePdfDocumentId = Guid.NewGuid().ToString();

            int saveState = 0;
            try
            {
                // 6/18/2015 dd - Write the PDF to FileDB.
                if (this.m_hasOriginalPdfToConvert)
                {
                    FileDBTools.WriteFile(E_FileDB.EdmsPdf, nonDestructivePdfDocumentId, this.m_sNewPdfPath);

                    this.m_edocsMetaData = new EdocsMetadata();
                    this.m_edocsMetaData.OriginalDocumentId = new Guid(nonDestructivePdfDocumentId);

                    this.m_originalEdocsMetaData = new EdocsMetadata();
                    this.m_originalEdocsMetaData.OriginalDocumentId = new Guid(nonDestructivePdfDocumentId);
                }
                saveState++;
                if (this.m_eSignTags != null)
                {
                    FileDBTools.WriteData(E_FileDB.EDMS, this.ESignTagsXmlFileDbKey, s => this.m_eSignTags.Save(s));
                }

                saveState++;

                var parameters = GenerateSaveParameters(Guid.Empty, Guid.Empty, this.PageCount, this.DocumentId, userId);
                StoredProcedureHelper.ExecuteNonQuery(this.BrokerId, CREATE_SP, 3, parameters);
            }
            catch
            {
                switch (saveState)
                {
                    case 2:
                        if (this.m_eSignTags != null)
                        {
                            DeleteFileDBEntry(this.ESignTagsXmlFileDbKey);
                        }

                        goto case 1;
                    case 1:
                        FileDBTools.Delete(E_FileDB.EdmsPdf, nonDestructivePdfDocumentId);
                        goto default;
                    default:
                        throw;
                }
            }

            m_fileDbKey_CurrentRevision = Guid.Empty;
            m_fileDbKey_Original = Guid.Empty;

            if (this.m_hasOriginalPdfToConvert)
            {
                // The source is only for statistical purpose. We only track interesting source of eDocs.
                E_NonDestructiveSourceT sourceT = E_NonDestructiveSourceT.Upload;

                if (this.CreationSource == E_EDocumentSource.CBCDrive)
                {
                    sourceT = E_NonDestructiveSourceT.CBCDrive;
                }
                else if (this.CreationSource == E_EDocumentSource.Fax)
                {
                    sourceT = E_NonDestructiveSourceT.Fax;
                }
                else if (this.CreationSource == E_EDocumentSource.FloodService)
                {
                    sourceT = E_NonDestructiveSourceT.FloodService;
                }
                else if (this.CreationSource == E_EDocumentSource.FromIntegration)
                {
                    sourceT = E_NonDestructiveSourceT.FromIntegration;
                }
                else if (this.CreationSource == E_EDocumentSource.GeneratedDocs)
                {
                    sourceT = E_NonDestructiveSourceT.GeneratedDocs;
                }

                EnqueueNonDestructivePdfRasterizeRequest(this.BrokerId, this.DocumentId, nonDestructivePdfDocumentId, sourceT, POST_ACTION_UPDATE_METADATA);
                UpdateImageStatus(this.BrokerId, this.DocumentId, E_ImageStatus.NoCachedImagesButInQueue);
            }
            else
            {
                UpdateImageStatus(this.BrokerId, this.DocumentId, E_ImageStatus.HasCachedImages);
            }
        }

        [JsonObject(MemberSerialization.OptIn)]
        public class NonDestructiveQueueItem
        {
            [JsonProperty("filedb")]
            public string FileDbSiteCode { get; set; }

            [JsonProperty("filedbkey")]
            public string FileDbKey { get; set; }

            [JsonProperty("brokerid")]
            public Guid BrokerId { get; set; }

            [JsonProperty("edocs_document_id")]
            public Guid EdocsDocumentId { get; set; }

            [JsonProperty("full_scale")]
            public float FullPngScale { get; set; }

            [JsonProperty("thumb_scale")]
            public float ThumbPngScale { get; set; }

            [JsonProperty("post_action")]
            public string PostAction { get; set; }

            [JsonProperty("source")]
            public E_NonDestructiveSourceT SourceT { get; set; }

            [JsonProperty("is_upload_aws")]
            public bool IsUploadToAmazon { get; set; }

            [JsonProperty("is_disable_png")]
            public bool IsDisablePngGeneration { get; set; }
        }

        public enum E_NonDestructiveSourceT
        {
            Upload = 0,
            PasteAnnotation = 1,
            Migration = 2,
            FromIntegration = 3,
            GeneratedDocs = 4,
            Fax = 5,
            CBCDrive = 6,
            FloodService = 7,
            OverwriteExisting = 8

        }

        public static void EnqueueNonDestructivePdfRasterizeRequest(Guid brokerId, Guid documentId, string originalFileDbKey, E_NonDestructiveSourceT sourceT, string postAction)
        {
            if (brokerId == Guid.Empty)
            {
                throw new ArgumentException("BrokerId cannot be empty");
            }

            BrokerDB broker = BrokerDB.RetrieveById(brokerId);

            NonDestructiveQueueItem item = new NonDestructiveQueueItem();
            item.FileDbSiteCode = ConstStage.FileDBSiteCodeForEdmsPdf;
            item.FileDbKey = originalFileDbKey;
            item.BrokerId = brokerId;
            item.EdocsDocumentId = documentId;
            item.FullPngScale = ConstAppDavid.PdfPngScaling;
            item.ThumbPngScale = ConstAppDavid.PdfPngThumbRequeueScaling;
            item.PostAction = postAction;
            item.SourceT = sourceT;

            if (broker.IsTestingAmazonEdocs)
            {
                item.IsUploadToAmazon = true;
                item.IsDisablePngGeneration = broker.IsDisablePngGeneration;
            }

            try
            {
                SqlParameter[] parameters = {
                                                new SqlParameter("@BrokerId", brokerId),
                                                new SqlParameter("@DocumentId", new Guid(originalFileDbKey)),
                                                new SqlParameter("@SourceT", sourceT),
                                                new SqlParameter("@OriginalRequestJson", SerializationHelper.JsonNetSerialize(item))
                                            };
                StoredProcedureHelper.ExecuteNonQuery(brokerId, "EDOCS_DOCUMENT_ORIGINAL_Create", 3, parameters);

                LendersOffice.Drivers.Gateways.MessageQueueHelper.SendJSON(ConstStage.MSMQ_PdfRasterize, null, item);
            }
            catch (Exception e)
            {
                Tools.LogError(e);
                EDocument.UpdateImageStatus(brokerId, documentId, E_ImageStatus.Error);
                throw;
            }
        }

        private static IPdfService GetPdfService()
        {
            if (ConstStage.UseiTextSharpWebApi)
            {
                return new WebApiPdfService();
            }
            else
            {
                return new LegacyITextSharpPdfService();
            }
        }

        /// <summary>
        /// Construct PDF from the current EdocsMetaData.
        /// </summary>
        /// <param name="edocsMetadata"></param>
        /// <returns></returns>
        private static string GenerateNonDestructiveEdocs(EdocsMetadata metaData, bool getOriginal = false)
        {
            if (metaData == null)
            {
                throw new ArgumentNullException("metaData");
            }

            if (metaData.Pages == null || metaData.Pages.Count() == 0)
            {
                // 6/26/2015 dd - This could happen when user just upload PDF and our PDF rasterizer did not get to it yet.
                if (metaData.OriginalDocumentId != Guid.Empty)
                {
                    return GetNonDestructivePdfFromFileDb(metaData.OriginalDocumentId);
                }
            }
            else if (getOriginal)
            {
                var firstPageDocumentId = metaData.Pages.First().ReferenceDocumentId;
                if (metaData.Pages.Any(page => page.ReferenceDocumentId != firstPageDocumentId))
                {
                    throw new CBaseException(
                        ErrorMessages.Generic,
                        "Cannot retrieve original when document was sourced from multiple reference docs.");
                }

                return GetNonDestructivePdfFromFileDb(firstPageDocumentId);
            }

            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                var pdfService = GetPdfService();

                return pdfService.GeneratePdf(metaData);
            }
            catch (Exception exc)
            {
                string json = SerializationHelper.JsonNetSerialize(metaData);
                Tools.LogError($"Unable to generate eDocs. MetaData {json}", exc);
                throw;
            }
            finally
            {
                Tools.LogInfo("GenerateEDocs", $"Generate pdf in {sw.ElapsedMilliseconds:#,#}ms.", (int)sw.ElapsedMilliseconds);
            }
        }

        public static string GetNonDestructivePdfFromFileDb(Guid originalDocumentId)
        {
            try
            {
                // dd 5/29/2018 - case 47025 - Attempts to retrieve from EDMS Pdf filedb first. If the PDF is not at the new location then look at the old Filedb.
                return FileDBTools.CreateCopy(E_FileDB.EdmsPdf, originalDocumentId.ToString());
            }
            catch (FileNotFoundException)
            {
                return FileDBTools.CreateCopy(E_FileDB.EDMS, originalDocumentId.ToString());
            }
        }
        #endregion
    }
}