﻿// <copyright file="EdocFaxCoversheetBuilder.cs" company="MeridianLink">
//     Copyright (c) MeridianLink. All rights reserved.
// </copyright>
// <summary>
// Author: Michael Leinweaver
// Date: 10/20/2016
// </summary>
namespace EDocs
{
    using System;
    using System.Collections.Generic;
    using DataAccess;

    /// <summary>
    /// Provides a means for building fax coversheets.
    /// </summary>
    public class EdocFaxCoversheetBuilder
    {
        /// <summary>
        /// Build the coversheet list for the hardcoded doc types as 
        /// described in OPM 82755.  Always for the primary app. Note 
        /// that there is no doc-type loading here at all--it all comes 
        /// from a hardcoded list of doc type ids and alias.
        /// </summary>
        /// <param name="loanId">
        /// The id of the loan to build coversheets.
        /// </param>
        /// <returns>
        /// A list of coversheet data for the loan.
        /// </returns>
        public static List<EdocFaxCover.FaxCoverData> BuildCoverSheets(Guid loanId)
        {
            var user = LendersOffice.Security.PrincipalFactory.CurrentPrincipal;

            // Safety Throw.  Page only applicable to certain lenders.
            if (!(user?.BrokerDB.IsEnableBrokerFaxPackages ?? false))
            {
                throw new LendersOffice.Security.AccessDenied($"Broker not set up for Broker Fax Packages: {user.BrokerDB.Name}");
            }

            var coverDataList = new List<EdocFaxCover.FaxCoverData>();

            var number = EDocsFaxNumber.Retrieve(user.BrokerId);
            var broker = LendersOffice.Admin.BrokerDB.RetrieveById(user.BrokerId);

            var dataLoan = new CPageData(loanId, new[] { "sLNm", "aBNm", "aCNm" });
            dataLoan.InitLoad();

            CAppData dataApp = dataLoan.GetAppData(0);
            string appName = dataApp.aBNm;
            if (!string.IsNullOrEmpty(dataApp.aCNm))
            {
                appName += " & " + dataApp.aCNm;
            }

            foreach (KeyValuePair<int, string> pair in GetDocTypes())
            {
                var coverData = new EdocFaxCover.FaxCoverData()
                {
                    Type = E_FaxCoverType.EDocs,
                    LoanId = loanId,
                    AppId = dataApp.aAppId,
                    LenderName = broker.Name,
                    Description = string.Empty,
                    DocTypeId = pair.Key,
                    DocTypeDescription = pair.Value,
                    FaxNumber = number.FaxNumber,
                    ApplicationDescription = appName,
                    LoanNumber = dataLoan.sLNm
                };

                coverDataList.Add(coverData);
            }

            return coverDataList;
        }

        /// <summary>
        /// Obtains the hardcoded mapping of document types.
        /// </summary>
        /// <remarks>
        /// OPM 82775 contains this mapping.  Basically, these are 
        /// hard-coded aliases for the doc types PML0132 wants to 
        /// appear in their broker cover sheet package.
        /// </remarks>
        /// <returns>
        /// A mapping of hardcoded document types.
        /// </returns>
        private static IEnumerable<KeyValuePair<int, string>> GetDocTypes()
        {
            // DocTypeId, Alias for cover sheet text
            Dictionary<int, string> aliasMap = new Dictionary<int, string>();

            // 1003/1008   - *APPLICATION:SIGNED/DATED 1003
            aliasMap.Add(4136, "1003/1008");

            // Credit Reports  - *CREDIT:*CREDITREPORT
            aliasMap.Add(4140, "Credit Reports");

            // Fed Disclosures  - *DISCLOSURES:*FEDERAL DISCLOSURES
            aliasMap.Add(4181, "Fed Disclosures");

            // FHA Disclosures  - *FHA:*FHA SPECIFIC DISCLOSURES
            aliasMap.Add(4179, "FHA Disclosures");

            // Escrow Instructions  - *ESCROW:*ESCROW INSTRUCTIONS
            aliasMap.Add(4167, "Escrow Instructions");

            // Title  - *TITLE:*PRELIM
            aliasMap.Add(4172, "Title");

            // Pay Stubs  - *INCOME:*PAYSTUBS
            aliasMap.Add(4144, "Pay Stubs");

            // W2's  - *INCOME:*W2's (2 YEARS)
            aliasMap.Add(4146, "W2's");

            // 1040's  - *INCOME:*1040'S (FULL RETURNS - NO STATE RETURNS)
            aliasMap.Add(4148, "1040's");

            // Purchase Agreement  - *PROPERTY:*PURCHASE AGREEMENT
            aliasMap.Add(4155, "Purchase Agreement");

            // Appraisal  - *PROPERTY:*APPRAISAL
            aliasMap.Add(4156, "Appraisal");

            // Hazard Insurance  - *PROPERTY:*HAZARD INSURANCE (DEC PAGE)
            aliasMap.Add(4158, "Hazard Insurance");

            // Drivers License/SS - *APPLICATION:SOCIAL SECURITY CARD
            aliasMap.Add(4138, "Drivers License/SS");

            // Miscellaneous  - UNCLASSSIFIED:UNCLASSIFIED
            aliasMap.Add(96, "Miscellaneous");

            // PTD  - *IN BASKET:PTD
            aliasMap.Add(4433, "PTD");

            // PTF  - *IN BASKET:PTF
            aliasMap.Add(4432, "PTF");

            return aliasMap;
        }
    }
}
