﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Xml;
using System.IO;
using LendersOffice.Common;
using DataAccess;
using iTextSharp.text.pdf;

namespace EDocs
{

    public sealed class PageMetaData
    {
        public int Height { get; private set; }
        public int Width { get; private set; }
        public string Key { get; set; }
        /// <summary>
        /// 1 Base.
        /// </summary>
        public int Number { get; private set; }

        public PageMetaData(int height, int width, string key, int number)
        {
            Height = height;
            Width = width;
            Key = key;
            Number = number;
        }
    }

    /// <summary>
    /// Represents the meta data needed to render the edocument viewer. 
    /// Assumes a pdf will not be edited and stored with the same key.
    /// </summary>
    public sealed class DocumentMetaData 
    {
        public DocumentMetaData(Guid key, PageMetaData[] pageData )
        {
            Key = key;
            Pages = pageData;
        }
        public PageMetaData[] Pages { get; private set; }
        
        /// <summary>
        /// The filedbkey the document belongs to.
        /// </summary>
        public Guid Key { get; set; }

        public string Serialize()
        {
            XElement[] xmlpages = new XElement[Pages.Length];

            for(var i = 0; i < Pages.Length; i++)
            {
                xmlpages[i] = new XElement("Page", 
                    new XAttribute("Height", Pages[i].Height),
                    new XAttribute("Width", Pages[i].Width),
                    new XAttribute("Number", Pages[i].Number),
                    new XAttribute("Key", Pages[i].Key));
            }

            XDocument doc = new XDocument(
                new XElement("DocumentMetaData",
                    new XAttribute("SerializedDate", DateTime.Now.ToString()),
                    new XElement("Key", Key.ToString()),
                    new XElement("Pages", xmlpages)));

            return doc.ToString();
            
        }

        public static DocumentMetaData Parse(string xmlDoc)
        {
            if (string.IsNullOrEmpty(xmlDoc))
            {
                return null;
            }

            XDocument doc = XDocument.Parse(xmlDoc);
            XElement keyElement = doc.Root.Element("Key");

            if (keyElement == null)
            {
                return null;
            }

            Guid key = new Guid(keyElement.Value);

            DocumentMetaData metaData = new DocumentMetaData(key, null);
            

            List<PageMetaData> pages = new List<PageMetaData>();

            foreach (XElement element in doc.Root.Element("Pages").Elements("Page"))
            {
                int width = int.Parse(element.Attribute("Width").Value);
                int height = int.Parse(element.Attribute("Height").Value);
                string pkey = element.Attribute("Key").Value;
                int number = int.Parse(element.Attribute("Number").Value);

                PageMetaData pdata = new PageMetaData(height, width, pkey, number);
                pages.Add(pdata);
            }

            metaData.Pages = pages.OrderBy(p => p.Number).ToArray();
            return metaData;
        }

        public static DocumentMetaData ReadFromPath(Guid key, string path)
        {
            RandomAccessFileOrArray rf = new RandomAccessFileOrArray(path);
            iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(rf, null);
            PDFPageIDManager idManager = new PDFPageIDManager(pdfReader);

            int pageCount = pdfReader.NumberOfPages;
            List<PageMetaData> pages = new List<PageMetaData>();

            for (int i = 1; i <= pageCount; i++)
            {
                var size = pdfReader.GetPageSizeWithRotation(i);

                PageMetaData data = new PageMetaData(
                    (int)size.Height,
                    (int)size.Width,
                    idManager.GetPageId(i),
                    i);

                pages.Add(data);
            }
            pdfReader.Close();
            DocumentMetaData metaData = new DocumentMetaData(key, pages.ToArray());
            return metaData;
        }

        
    }
}
