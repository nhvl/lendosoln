﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.SqlClient;
using DataAccess;

namespace LendersOffice.ObjLib.Edocs
{
    public class BaseDocTypeMapper
    {
        public Guid BrokerId { get; protected set; }

        public BaseDocTypeMapper(Guid brokerId)
        {
            this.BrokerId = brokerId;
        }

        public void GetUnassignedDocTypeAndUnreadableDocType(out int unassignedDocTypeId, out int unreadableBarcodeDocTypeId)
        {
            SqlParameter[] parameters = {
                                            new SqlParameter("@BrokerId", BrokerId)
                                        };
            using (var reader = StoredProcedureHelper.ExecuteReader(this.BrokerId, "EDOCS_GetUnassignedDocTypeIdAndUnreadableDocTypeId", parameters))
            {
                if (reader.Read())
                {
                    unreadableBarcodeDocTypeId = (int)reader["UnreadableBarCodeDocTypeId"];
                    unassignedDocTypeId = (int)reader["UnassignedDocTypeId"];
                }
                else
                {
                    throw CBaseException.GenericException("Error - did not return unassigned doc types");
                }
            }
        }
    }
}
